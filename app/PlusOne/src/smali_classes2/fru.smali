.class public Lfru;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/LinkedHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedHashMap",
            "<",
            "Ljava/lang/Long;",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Lfrv;

    const/4 v1, 0x4

    const/high16 v2, 0x3f400000    # 0.75f

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lfrv;-><init>(IFZ)V

    iput-object v0, p0, Lfru;->b:Ljava/util/LinkedHashMap;

    .line 41
    iput-object p1, p0, Lfru;->a:Landroid/content/Context;

    .line 42
    return-void
.end method

.method private static a([III)[I
    .locals 4

    .prologue
    .line 205
    rem-int/lit8 v0, p1, 0x2

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 206
    const/4 v0, 0x0

    move v1, v0

    move-object v0, p0

    :goto_0
    const/4 v2, 0x3

    if-ge v1, v2, :cond_1

    .line 207
    div-int/lit8 v2, p1, 0x2

    div-int/lit8 v3, p1, 0x2

    invoke-static {v0, v2, v3, p2}, Lfru;->a([IIII)[I

    move-result-object p0

    .line 206
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-object v0, p0

    goto :goto_0

    .line 210
    :cond_0
    div-int/lit8 v0, p1, 0x2

    div-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, -0x1

    invoke-static {p0, v0, v1, p2}, Lfru;->a([IIII)[I

    move-result-object v0

    .line 211
    div-int/lit8 v1, p1, 0x2

    add-int/lit8 v1, v1, -0x1

    div-int/lit8 v2, p1, 0x2

    invoke-static {v0, v1, v2, p2}, Lfru;->a([IIII)[I

    move-result-object v0

    .line 212
    div-int/lit8 v1, p1, 0x2

    div-int/lit8 v2, p1, 0x2

    invoke-static {v0, v1, v2, p2}, Lfru;->a([IIII)[I

    move-result-object v0

    .line 214
    :cond_1
    return-object v0
.end method

.method private static a([IIII)[I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 176
    add-int v1, p1, p2

    add-int/lit8 v4, v1, 0x1

    .line 177
    array-length v1, p0

    new-array v5, v1, [I

    move v2, v0

    move v1, v0

    .line 179
    :goto_0
    if-ge v2, p2, :cond_2

    .line 180
    aget v3, p0, v2

    add-int/2addr v3, v1

    .line 179
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_0

    :cond_0
    move v2, p3

    .line 183
    :goto_1
    add-int/2addr v1, v2

    .line 184
    div-int v2, v1, v4

    aput v2, v5, v0

    .line 185
    sub-int v2, v0, p1

    if-ltz v2, :cond_1

    .line 186
    sub-int v2, v0, p1

    aget v2, p0, v2

    sub-int/2addr v1, v2

    .line 182
    :cond_1
    add-int/lit8 v0, v0, 0x1

    :cond_2
    array-length v2, p0

    if-ge v0, v2, :cond_3

    .line 183
    add-int v2, v0, p2

    array-length v3, p0

    if-ge v2, v3, :cond_0

    add-int v2, v0, p2

    aget v2, p0, v2

    goto :goto_1

    .line 189
    :cond_3
    return-object v5
.end method

.method private b(II)Landroid/graphics/drawable/Drawable;
    .locals 14

    .prologue
    .line 67
    ushr-int/lit8 v3, p2, 0x18

    .line 68
    const v0, 0xffffff

    and-int v4, v0, p2

    .line 71
    int-to-float v0, p1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    .line 74
    const/high16 v1, 0x40400000    # 3.0f

    mul-float/2addr v0, v1

    float-to-double v0, v0

    const-wide v6, 0x401921fb54442d18L    # 6.283185307179586

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    mul-double/2addr v0, v6

    const-wide/high16 v6, 0x4010000000000000L    # 4.0

    div-double/2addr v0, v6

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    add-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v5, v0

    .line 76
    shl-int/lit8 v0, p1, 0x1

    .line 78
    mul-int/lit8 v1, v0, 0x2

    add-int/lit8 v6, v1, 0x1

    .line 79
    int-to-double v8, v6

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v8

    double-to-int v1, v8

    new-array v7, v1, [I

    .line 83
    add-int/lit8 v0, v0, 0x1

    new-array v2, v0, [I

    .line 84
    const/4 v0, 0x0

    :goto_0
    array-length v1, v2

    if-ge v0, v1, :cond_1

    .line 85
    array-length v1, v2

    div-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_0

    const/4 v1, 0x0

    :goto_1
    aput v1, v2, v0

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 85
    :cond_0
    const/16 v1, 0xff

    goto :goto_1

    .line 88
    :cond_1
    const/16 v0, 0xff

    invoke-static {v2, v5, v0}, Lfru;->a([III)[I

    move-result-object v8

    .line 91
    const/4 v1, 0x0

    .line 92
    const/4 v0, 0x0

    move v2, v0

    move-object v0, v1

    :goto_2
    array-length v1, v8

    if-ge v2, v1, :cond_6

    .line 93
    array-length v0, v8

    new-array v9, v0, [I

    .line 94
    const/4 v0, 0x0

    :goto_3
    array-length v1, v8

    if-ge v0, v1, :cond_3

    .line 95
    array-length v1, v8

    div-int/lit8 v1, v1, 0x2

    if-ge v0, v1, :cond_2

    const/4 v1, 0x0

    :goto_4
    aput v1, v9, v0

    .line 94
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 95
    :cond_2
    aget v1, v8, v2

    goto :goto_4

    .line 98
    :cond_3
    aget v0, v8, v2

    invoke-static {v9, v5, v0}, Lfru;->a([III)[I

    move-result-object v1

    .line 101
    mul-int v9, v2, v6

    .line 102
    sub-int v0, v6, v2

    add-int/lit8 v0, v0, -0x1

    mul-int v10, v0, v6

    .line 103
    const/4 v0, 0x0

    :goto_5
    array-length v11, v1

    if-ge v0, v11, :cond_5

    .line 104
    aget v11, v1, v0

    mul-int/2addr v11, v3

    div-int/lit16 v11, v11, 0xff

    shl-int/lit8 v11, v11, 0x18

    or-int/2addr v11, v4

    .line 105
    add-int v12, v9, v0

    aput v11, v7, v12

    .line 106
    add-int v12, v9, v6

    sub-int/2addr v12, v0

    add-int/lit8 v12, v12, -0x1

    aput v11, v7, v12

    .line 107
    array-length v12, v1

    add-int/lit8 v12, v12, -0x1

    if-ne v2, v12, :cond_4

    .line 110
    mul-int v12, v6, v0

    array-length v13, v1

    add-int/2addr v12, v13

    aput v11, v7, v12

    .line 112
    sub-int v12, v6, v0

    add-int/lit8 v12, v12, -0x1

    mul-int/2addr v12, v6

    array-length v13, v1

    add-int/2addr v12, v13

    aput v11, v7, v12

    .line 103
    :goto_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 114
    :cond_4
    add-int v12, v10, v0

    aput v11, v7, v12

    .line 115
    add-int v12, v10, v6

    sub-int/2addr v12, v0

    add-int/lit8 v12, v12, -0x1

    aput v11, v7, v12

    goto :goto_6

    .line 92
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-object v0, v1

    goto :goto_2

    .line 121
    :cond_6
    array-length v1, v7

    div-int/lit8 v1, v1, 0x2

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    aget v0, v0, v2

    mul-int/2addr v0, v3

    div-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x18

    or-int/2addr v0, v4

    aput v0, v7, v1

    .line 124
    const/16 v0, 0x54

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v3

    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    const/4 v0, 0x2

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    const/16 v0, 0x9

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->put(B)Ljava/nio/ByteBuffer;

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    div-int/lit8 v0, v6, 0x2

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    div-int/lit8 v0, v6, 0x2

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    div-int/lit8 v0, v6, 0x2

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    div-int/lit8 v0, v6, 0x2

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v3, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    const/4 v0, 0x0

    :goto_7
    const/16 v1, 0x9

    if-ge v0, v1, :cond_7

    const/4 v1, 0x1

    invoke-virtual {v3, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_7
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v7, v6, v6, v0}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    new-instance v0, Landroid/graphics/drawable/NinePatchDrawable;

    iget-object v1, p0, Lfru;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/NinePatchDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;[BLandroid/graphics/Rect;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a(II)Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    .line 50
    if-gtz p1, :cond_0

    .line 51
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x2a

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Cannot create shadow with size "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_0
    int-to-long v0, p1

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    int-to-long v2, p2

    or-long/2addr v2, v0

    .line 54
    iget-object v0, p0, Lfru;->b:Ljava/util/LinkedHashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 55
    iget-object v0, p0, Lfru;->b:Ljava/util/LinkedHashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 61
    :cond_1
    :goto_0
    return-object v0

    .line 57
    :cond_2
    invoke-direct {p0, p1, p2}, Lfru;->b(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 58
    const/16 v1, 0x64

    if-ge p1, v1, :cond_1

    .line 59
    iget-object v1, p0, Lfru;->b:Ljava/util/LinkedHashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
