.class public final Lipr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Liqc;


# instance fields
.field private a:Lgzs;


# direct methods
.method public constructor <init>(Lgzs;)V
    .locals 2

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    if-nez p1, :cond_0

    .line 17
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "null CircleBuffer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 19
    :cond_0
    iput-object p1, p0, Lipr;->a:Lgzs;

    .line 20
    return-void
.end method


# virtual methods
.method public a(I)Liqb;
    .locals 2

    .prologue
    .line 24
    new-instance v0, Lips;

    iget-object v1, p0, Lipr;->a:Lgzs;

    invoke-virtual {v1, p1}, Lgzs;->b(I)Lgzr;

    move-result-object v1

    invoke-direct {v0, v1}, Lips;-><init>(Lgzr;)V

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lipr;->a:Lgzs;

    invoke-virtual {v0}, Lgzs;->c()V

    .line 30
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lipr;->a:Lgzs;

    invoke-virtual {v0}, Lgzs;->a()I

    move-result v0

    return v0
.end method

.method public synthetic b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    invoke-virtual {p0, p1}, Lipr;->a(I)Liqb;

    move-result-object v0

    return-object v0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Liqb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    new-instance v0, Liqe;

    invoke-direct {v0, p0}, Liqe;-><init>(Liqd;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lipr;->a:Lgzs;

    invoke-virtual {v0}, Lgzs;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
