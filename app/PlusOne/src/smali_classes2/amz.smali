.class public final Lamz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Z

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I


# direct methods
.method private constructor <init>(ZLjava/util/Map;IIII)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            ">;IIII)V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-boolean p1, p0, Lamz;->a:Z

    .line 57
    iput-object p2, p0, Lamz;->b:Ljava/util/Map;

    .line 58
    iput p3, p0, Lamz;->c:I

    .line 59
    iput p4, p0, Lamz;->d:I

    .line 60
    iput p5, p0, Lamz;->e:I

    .line 61
    iput p6, p0, Lamz;->f:I

    .line 62
    return-void
.end method

.method public static a()Lamz;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 46
    new-instance v0, Lamz;

    const/4 v2, 0x0

    move v3, v1

    move v4, v1

    move v5, v1

    move v6, v1

    invoke-direct/range {v0 .. v6}, Lamz;-><init>(ZLjava/util/Map;IIII)V

    return-object v0
.end method

.method public static a(Ljava/util/Map;IIII)Lamz;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Landroid/net/Uri;",
            ">;IIII)",
            "Lamz;"
        }
    .end annotation

    .prologue
    .line 35
    const-string v0, "downloadUriToCacheUris"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 36
    new-instance v0, Lamz;

    const/4 v1, 0x1

    .line 38
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2, p0}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v2

    move v3, p1

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lamz;-><init>(ZLjava/util/Map;IIII)V

    return-object v0
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 66
    const-class v0, Lamz;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-boolean v3, p0, Lamz;->a:Z

    .line 67
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lamz;->b:Ljava/util/Map;

    aput-object v3, v1, v2

    .line 66
    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
