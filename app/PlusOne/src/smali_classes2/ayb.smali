.class public abstract Layb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Layj;


# static fields
.field private static final a:[F


# instance fields
.field private b:Layj;

.field private c:Layj;

.field private d:Layj;

.field private e:Layj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const/4 v0, 0x4

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Layb;->a:[F

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Layj;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Layb;->b:Layj;

    if-nez v0, :cond_0

    .line 29
    invoke-virtual {p0}, Layb;->f()Layj;

    move-result-object v0

    iput-object v0, p0, Layb;->b:Layj;

    .line 31
    :cond_0
    iget-object v0, p0, Layb;->b:Layj;

    return-object v0
.end method

.method public final a(Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Layb;->c:Layj;

    if-nez v0, :cond_0

    .line 50
    invoke-virtual {p0, p0, p1, p2}, Layb;->a(Layj;Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;

    move-result-object v0

    iput-object v0, p0, Layb;->c:Layj;

    .line 52
    :cond_0
    iget-object v0, p0, Layb;->c:Layj;

    return-object v0
.end method

.method protected a(Layj;Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;
    .locals 3

    .prologue
    .line 106
    new-instance v0, Layu;

    invoke-direct {v0}, Layu;-><init>()V

    .line 107
    invoke-virtual {v0, p1}, Layu;->a(Layj;)Layu;

    move-result-object v0

    const/4 v1, -0x1

    .line 108
    invoke-virtual {v0, v1}, Layu;->a(I)Layu;

    move-result-object v0

    const/high16 v1, 0x42100000    # 36.0f

    .line 109
    invoke-virtual {v0, v1}, Layu;->a(F)Layu;

    move-result-object v0

    const-string v1, "sans-serif"

    const/4 v2, 0x0

    .line 110
    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Layu;->a(Landroid/graphics/Typeface;)Layu;

    move-result-object v0

    const/4 v1, 0x1

    .line 111
    invoke-virtual {v0, v1}, Layu;->b(Z)Layu;

    move-result-object v0

    const v1, 0x7f0b00b2

    .line 112
    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Layu;->g(I)Layu;

    move-result-object v0

    .line 113
    invoke-virtual {v0}, Layu;->a()Layt;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbof;Layl;Lbgd;)V
    .locals 0

    .prologue
    .line 66
    invoke-virtual {p3}, Lbgd;->a()V

    .line 67
    return-void
.end method

.method public final b()Layj;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Layb;->e:Layj;

    if-nez v0, :cond_0

    .line 37
    invoke-virtual {p0}, Layb;->g()Layj;

    move-result-object v0

    iput-object v0, p0, Layb;->e:Layj;

    .line 39
    :cond_0
    iget-object v0, p0, Layb;->e:Layj;

    return-object v0
.end method

.method public final b(Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Layb;->d:Layj;

    if-nez v0, :cond_0

    .line 58
    invoke-virtual {p0, p1, p2}, Layb;->c(Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;

    move-result-object v0

    iput-object v0, p0, Layb;->d:Layj;

    .line 60
    :cond_0
    iget-object v0, p0, Layb;->d:Layj;

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return v0
.end method

.method protected c(Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0}, Layb;->a()Layj;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2}, Layb;->a(Layj;Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;

    move-result-object v0

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method

.method public e()[F
    .locals 1

    .prologue
    .line 76
    sget-object v0, Layb;->a:[F

    return-object v0
.end method

.method protected f()Layj;
    .locals 1

    .prologue
    .line 85
    new-instance v0, Laye;

    invoke-direct {v0, p0}, Laye;-><init>(Layj;)V

    return-object v0
.end method

.method protected g()Layj;
    .locals 1

    .prologue
    .line 94
    new-instance v0, Lays;

    invoke-direct {v0}, Lays;-><init>()V

    invoke-virtual {v0}, Lays;->a()Layr;

    move-result-object v0

    return-object v0
.end method
