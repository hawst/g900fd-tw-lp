.class final enum Lavh;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lavh;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lavh;

.field public static final enum b:Lavh;

.field public static final enum c:Lavh;

.field public static final enum d:Lavh;

.field public static final enum e:Lavh;

.field private static final synthetic f:[Lavh;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 64
    new-instance v0, Lavh;

    const-string v1, "NOT_BUSY"

    invoke-direct {v0, v1, v2}, Lavh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavh;->a:Lavh;

    .line 67
    new-instance v0, Lavh;

    const-string v1, "INITIALIZING"

    invoke-direct {v0, v1, v3}, Lavh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavh;->b:Lavh;

    .line 70
    new-instance v0, Lavh;

    const-string v1, "DOWNLOADING_DETAILED_SOUNDTRACK"

    invoke-direct {v0, v1, v4}, Lavh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavh;->c:Lavh;

    .line 76
    new-instance v0, Lavh;

    const-string v1, "DOWNLOADING_PREVIEW_AUDIO"

    invoke-direct {v0, v1, v5}, Lavh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavh;->d:Lavh;

    .line 82
    new-instance v0, Lavh;

    const-string v1, "DOWNLOADING_STORYBOARD_AUDIO"

    invoke-direct {v0, v1, v6}, Lavh;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavh;->e:Lavh;

    .line 62
    const/4 v0, 0x5

    new-array v0, v0, [Lavh;

    sget-object v1, Lavh;->a:Lavh;

    aput-object v1, v0, v2

    sget-object v1, Lavh;->b:Lavh;

    aput-object v1, v0, v3

    sget-object v1, Lavh;->c:Lavh;

    aput-object v1, v0, v4

    sget-object v1, Lavh;->d:Lavh;

    aput-object v1, v0, v5

    sget-object v1, Lavh;->e:Lavh;

    aput-object v1, v0, v6

    sput-object v0, Lavh;->f:[Lavh;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lavh;
    .locals 1

    .prologue
    .line 62
    const-class v0, Lavh;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lavh;

    return-object v0
.end method

.method public static values()[Lavh;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lavh;->f:[Lavh;

    invoke-virtual {v0}, [Lavh;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lavh;

    return-object v0
.end method
