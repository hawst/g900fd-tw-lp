.class public final Lbmm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lbml;

.field public final b:Z

.field public final c:J


# direct methods
.method public constructor <init>(Lbmw;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string v0, "photo"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbml;

    iput-object v0, p0, Lbmm;->a:Lbml;

    .line 30
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbmm;->c:J

    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbmm;->b:Z

    .line 32
    return-void
.end method

.method public constructor <init>(Lbon;J)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const-string v0, "video"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbml;

    iput-object v0, p0, Lbmm;->a:Lbml;

    .line 24
    iput-wide p2, p0, Lbmm;->c:J

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbmm;->b:Z

    .line 26
    return-void
.end method


# virtual methods
.method public a()Lbmw;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lbmm;->a:Lbml;

    instance-of v0, v0, Lbmw;

    if-eqz v0, :cond_0

    .line 37
    iget-object v0, p0, Lbmm;->a:Lbml;

    check-cast v0, Lbmw;

    return-object v0

    .line 39
    :cond_0
    const-string v0, "displayable frame is not a photo"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method public b()Lbon;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lbmm;->a:Lbml;

    instance-of v0, v0, Lbon;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lbmm;->a:Lbml;

    check-cast v0, Lbon;

    return-object v0

    .line 47
    :cond_0
    const-string v0, "displayable frame is not a video"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 52
    if-ne p1, p0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return v0

    .line 55
    :cond_1
    instance-of v2, p1, Lbmm;

    if-nez v2, :cond_2

    move v0, v1

    .line 56
    goto :goto_0

    .line 58
    :cond_2
    check-cast p1, Lbmm;

    .line 59
    iget-wide v2, p0, Lbmm;->c:J

    iget-wide v4, p1, Lbmm;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lbmm;->a:Lbml;

    iget-object v3, p1, Lbmm;->a:Lbml;

    invoke-static {v2, v3}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 64
    iget-wide v0, p0, Lbmm;->c:J

    iget-object v2, p0, Lbmm;->a:Lbml;

    const/16 v3, 0x11

    invoke-static {v2, v3}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v2

    invoke-static {v0, v1, v2}, Lbqh;->a(JI)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 69
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lbmm;->a:Lbml;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lbmm;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
