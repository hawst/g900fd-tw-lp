.class final Lgoa;
.super Lghj;

# interfaces
.implements Lgyz;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lghh",
        "<",
        "Lgns;",
        ">.ghj<",
        "Lgfy",
        "<",
        "Lgyz;",
        ">;>;",
        "Lgyz;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/common/api/Status;

.field private final b:Lgzx;


# direct methods
.method public constructor <init>(Lgny;Lgfy;Lcom/google/android/gms/common/api/Status;Lgzx;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfy",
            "<",
            "Lgyz;",
            ">;",
            "Lcom/google/android/gms/common/api/Status;",
            "Lgzx;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lghj;-><init>(Lghh;Ljava/lang/Object;)V

    iput-object p3, p0, Lgoa;->a:Lcom/google/android/gms/common/api/Status;

    iput-object p4, p0, Lgoa;->b:Lgzx;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, Lgoa;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method protected a(Lgfy;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfy",
            "<",
            "Lgyz;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Lgfy;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method protected synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lgfy;

    invoke-virtual {p0, p1}, Lgoa;->a(Lgfy;)V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lgoa;->b:Lgzx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgoa;->b:Lgzx;

    invoke-virtual {v0}, Lggy;->b()V

    :cond_0
    return-void
.end method

.method protected c()V
    .locals 0

    invoke-virtual {p0}, Lgoa;->b()V

    return-void
.end method

.method public g()Lgzx;
    .locals 1

    iget-object v0, p0, Lgoa;->b:Lgzx;

    return-object v0
.end method
