.class public final Lbib;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lbht;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbht",
            "<",
            "Lbhk;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Lbhh;

.field public final d:Lbma;


# direct methods
.method public constructor <init>(Lbht;Ljava/util/List;Lbhh;Lbma;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbht",
            "<",
            "Lbhk;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;",
            ">;",
            "Lbhh;",
            "Lbma;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-string v0, "sequencePollable"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbht;

    iput-object v0, p0, Lbib;->a:Lbht;

    .line 40
    const-string v0, "resamplers"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lbib;->b:Ljava/util/List;

    .line 41
    const-string v0, "seeker"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhh;

    iput-object v0, p0, Lbib;->c:Lbhh;

    .line 42
    const-string v0, "clipSequence"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbma;

    iput-object v0, p0, Lbib;->d:Lbma;

    .line 43
    return-void
.end method
