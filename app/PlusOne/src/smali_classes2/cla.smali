.class public final Lcla;
.super Lcje;
.source "PG"

# interfaces
.implements Lhbk;
.implements Lhbl;


# instance fields
.field private O:Lhap;

.field private P:Landroid/widget/LinearLayout;

.field private Q:Landroid/widget/HorizontalScrollView;

.field private R:Landroid/widget/FrameLayout;

.field private S:Lhbf;

.field private T:I

.field private U:I

.field private V:I

.field private W:Landroid/support/v4/view/ViewPager;

.field private X:Z

.field private Y:Lip;

.field private Z:Landroid/view/animation/Animation;

.field private aa:Landroid/view/animation/Animation;

.field private ab:Landroid/view/animation/Animation$AnimationListener;

.field private ac:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

.field private ad:Lhdd;

.field private final ae:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcmx;",
            ">;"
        }
    .end annotation
.end field

.field private af:Lclg;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Lcje;-><init>()V

    .line 70
    const/4 v0, -0x1

    iput v0, p0, Lcla;->T:I

    .line 80
    new-instance v0, Lclb;

    invoke-direct {v0, p0}, Lclb;-><init>(Lcla;)V

    iput-object v0, p0, Lcla;->ab:Landroid/view/animation/Animation$AnimationListener;

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcla;->ae:Ljava/util/List;

    .line 570
    return-void
.end method

.method private X()V
    .locals 4

    .prologue
    .line 384
    iget-object v0, p0, Lcla;->R:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v0

    .line 385
    iget-object v1, p0, Lcla;->R:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v1

    .line 387
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 406
    :cond_0
    :goto_0
    return-void

    .line 391
    :cond_1
    invoke-virtual {p0}, Lcla;->ah()Lchp;

    move-result-object v2

    invoke-virtual {v2}, Lchp;->h()Lcom/google/android/libraries/photoeditor/core/FilterChain;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getPostRotation()I

    move-result v2

    .line 394
    if-eqz v2, :cond_2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    .line 395
    :cond_2
    iget v2, p0, Lcla;->U:I

    iget v3, p0, Lcla;->V:I

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->a(IIII)Landroid/graphics/Point;

    move-result-object v0

    .line 400
    :goto_1
    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    iget v2, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v1, v2, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 401
    const/16 v0, 0x11

    iput v0, v1, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 403
    iget-object v0, p0, Lcla;->O:Lhap;

    invoke-virtual {v0, v1}, Lhap;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 405
    invoke-virtual {p0}, Lcla;->ak()V

    goto :goto_0

    .line 397
    :cond_3
    iget v2, p0, Lcla;->V:I

    iget v3, p0, Lcla;->U:I

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->a(IIII)Landroid/graphics/Point;

    move-result-object v0

    goto :goto_1
.end method

.method static synthetic a(Lcla;)Landroid/support/v4/view/ViewPager;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcla;->W:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic a(Lcla;I)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0, p1}, Lcla;->e(I)V

    return-void
.end method

.method static synthetic a(Lcla;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 57
    iget-object v0, p0, Lcla;->af:Lclg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcla;->af:Lclg;

    iput-boolean v2, v0, Lclg;->a:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcla;->af:Lclg;

    :cond_0
    iget-boolean v0, p0, Lcla;->X:Z

    if-eq v0, p1, :cond_1

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcla;->W:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcla;->Z:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->startAnimation(Landroid/view/animation/Animation;)V

    iput-boolean v2, p0, Lcla;->X:Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    new-instance v0, Lclg;

    invoke-direct {v0, p0}, Lclg;-><init>(Lcla;)V

    iput-object v0, p0, Lcla;->af:Lclg;

    iget-object v0, p0, Lcla;->W:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcla;->af:Lclg;

    const-wide/16 v2, 0x14d

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/view/ViewPager;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method static synthetic b(Lcla;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcla;->Z:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic b(Lcla;Z)Z
    .locals 0

    .prologue
    .line 57
    iput-boolean p1, p0, Lcla;->X:Z

    return p1
.end method

.method static synthetic c(Lcla;)Landroid/view/animation/Animation;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcla;->aa:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    invoke-static {p0}, Lcla;->d(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcla;)Lhap;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcla;->O:Lhap;

    return-object v0
.end method

.method private static d(I)Ljava/lang/String;
    .locals 4

    .prologue
    .line 329
    const-string v0, "%02d"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    add-int/lit8 v3, p0, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private e(I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 413
    iget-object v0, p0, Lcla;->O:Lhap;

    if-eqz v0, :cond_0

    iget v0, p0, Lcla;->T:I

    if-ne v0, p1, :cond_1

    .line 451
    :cond_0
    :goto_0
    return-void

    .line 417
    :cond_1
    iget-object v0, p0, Lcla;->ae:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmx;

    .line 418
    invoke-virtual {v0}, Lcmx;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 419
    if-ne v1, p1, :cond_4

    .line 420
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcmx;->a(Z)V

    .line 422
    iget-object v1, p0, Lcla;->Q:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v1}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v1

    .line 423
    iget-object v3, p0, Lcla;->Q:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v3}, Landroid/widget/HorizontalScrollView;->getWidth()I

    move-result v3

    add-int/2addr v3, v1

    .line 424
    invoke-virtual {v0}, Lcmx;->getLeft()I

    move-result v4

    if-ge v4, v1, :cond_3

    .line 427
    invoke-virtual {v0}, Lcmx;->getLeft()I

    move-result v3

    invoke-virtual {v0}, Lcmx;->getWidth()I

    move-result v0

    sub-int v0, v1, v0

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 428
    iget-object v1, p0, Lcla;->Q:Landroid/widget/HorizontalScrollView;

    iget-object v3, p0, Lcla;->Q:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v3}, Landroid/widget/HorizontalScrollView;->getScrollY()I

    move-result v3

    invoke-virtual {v1, v0, v3}, Landroid/widget/HorizontalScrollView;->smoothScrollTo(II)V

    goto :goto_1

    .line 429
    :cond_3
    invoke-virtual {v0}, Lcmx;->getRight()I

    move-result v4

    if-le v4, v3, :cond_2

    .line 431
    invoke-virtual {v0}, Lcmx;->getRight()I

    move-result v3

    iget-object v4, p0, Lcla;->Q:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v4}, Landroid/widget/HorizontalScrollView;->getWidth()I

    move-result v4

    sub-int/2addr v3, v4

    .line 432
    invoke-virtual {v0}, Lcmx;->getWidth()I

    move-result v0

    add-int/2addr v0, v1

    .line 431
    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 433
    iget-object v1, p0, Lcla;->Q:Landroid/widget/HorizontalScrollView;

    iget-object v3, p0, Lcla;->Q:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v3}, Landroid/widget/HorizontalScrollView;->getScrollY()I

    move-result v3

    invoke-virtual {v1, v0, v3}, Landroid/widget/HorizontalScrollView;->smoothScrollTo(II)V

    goto :goto_1

    .line 436
    :cond_4
    invoke-virtual {v0, v5}, Lcmx;->a(Z)V

    goto :goto_1

    .line 440
    :cond_5
    iget-object v0, p0, Lcla;->O:Lhap;

    invoke-virtual {p0}, Lcla;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhap;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    .line 443
    invoke-static {p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilmFilterParameter;->e(I)I

    move-result v0

    .line 444
    const/4 v1, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v1, v0, v5}, Lcla;->a(ILjava/lang/Object;Z)Z

    .line 445
    invoke-virtual {p0}, Lcla;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/photoeditor/filterparameters/FilmFilterParameter;

    const/4 v1, 0x6

    invoke-static {v0, v1}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->contextAction(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;I)I

    .line 447
    iput p1, p0, Lcla;->T:I

    .line 448
    iget-object v0, p0, Lcla;->W:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p1, v5}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 450
    invoke-virtual {p0}, Lcla;->ak()V

    goto/16 :goto_0
.end method

.method static synthetic e(Lcla;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Lcla;->X()V

    return-void
.end method

.method static synthetic f(Lcla;)Landroid/widget/HorizontalScrollView;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcla;->Q:Landroid/widget/HorizontalScrollView;

    return-object v0
.end method

.method static synthetic g(Lcla;)Ljava/util/List;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcla;->ae:Ljava/util/List;

    return-object v0
.end method


# virtual methods
.method public C_()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 488
    invoke-virtual {p0}, Lcla;->s()Z

    move-result v0

    if-nez v0, :cond_1

    .line 491
    iget-object v0, p0, Lcla;->O:Lhap;

    if-eqz v0, :cond_0

    .line 492
    iget-object v0, p0, Lcla;->O:Lhap;

    invoke-virtual {v0, p0, v7}, Lhap;->a(Lhbl;Z)V

    .line 493
    iget-object v0, p0, Lcla;->O:Lhap;

    invoke-virtual {v0}, Lhap;->requestRender()V

    .line 519
    :cond_0
    :goto_0
    return-void

    .line 498
    :cond_1
    invoke-virtual {p0}, Lcla;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 499
    const v1, 0x7f0d0036

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 501
    iget-object v0, p0, Lcla;->S:Lhbf;

    invoke-virtual {v0}, Lhbf;->c()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 502
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v1, v0

    .line 505
    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_3

    .line 506
    int-to-float v1, v3

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 511
    :goto_1
    iget-object v0, p0, Lcla;->O:Lhap;

    if-eqz v0, :cond_2

    .line 512
    iget-object v0, p0, Lcla;->O:Lhap;

    iget-object v1, p0, Lcla;->S:Lhbf;

    .line 513
    invoke-virtual {p0}, Lcla;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v4

    const/4 v5, 0x3

    move-object v6, p0

    .line 512
    invoke-virtual/range {v0 .. v6}, Lhap;->a(Lhbi;IILcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;ILhbk;)Z

    .line 516
    :cond_2
    iget-object v0, p0, Lcla;->ad:Lhdd;

    if-eqz v0, :cond_0

    .line 517
    iget-object v0, p0, Lcla;->ad:Lhdd;

    invoke-virtual {v0, v7}, Lhdd;->a(Z)V

    goto :goto_0

    .line 508
    :cond_3
    int-to-float v1, v3

    div-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    move v2, v3

    move v3, v0

    goto :goto_1
.end method

.method protected V()V
    .locals 0

    .prologue
    .line 343
    invoke-super {p0}, Lcje;->V()V

    .line 348
    return-void
.end method

.method protected W()Lhbi;
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcla;->S:Lhbf;

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 117
    iget-object v0, p0, Lcla;->at:Llnl;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 119
    const v1, 0x7f0401b0

    invoke-virtual {v0, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 121
    if-nez v1, :cond_0

    .line 122
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to inflate the presets fragment"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_0
    const v0, 0x7f10050c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcla;->R:Landroid/widget/FrameLayout;

    .line 126
    const v0, 0x7f10050a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    iput-object v0, p0, Lcla;->Q:Landroid/widget/HorizontalScrollView;

    .line 127
    iget-object v0, p0, Lcla;->Q:Landroid/widget/HorizontalScrollView;

    const v2, 0x7f10050b

    invoke-virtual {v0, v2}, Landroid/widget/HorizontalScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcla;->P:Landroid/widget/LinearLayout;

    .line 129
    iget-object v0, p0, Lcla;->R:Landroid/widget/FrameLayout;

    new-instance v2, Lclc;

    invoke-direct {v2, p0}, Lclc;-><init>(Lcla;)V

    invoke-virtual {v0, v2}, Landroid/widget/FrameLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 143
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v0, v2, :cond_1

    .line 144
    iget-object v0, p0, Lcla;->Q:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v3}, Landroid/widget/HorizontalScrollView;->setLayoutDirection(I)V

    .line 147
    :cond_1
    iget-object v0, p0, Lcla;->at:Llnl;

    invoke-static {v0}, Lhea;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 148
    iget-object v0, p0, Lcla;->Q:Landroid/widget/HorizontalScrollView;

    new-instance v2, Lcld;

    invoke-direct {v2, p0}, Lcld;-><init>(Lcla;)V

    invoke-virtual {v0, v2}, Landroid/widget/HorizontalScrollView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 158
    :cond_2
    const v0, 0x7f10050d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcla;->W:Landroid/support/v4/view/ViewPager;

    .line 159
    new-instance v0, Lclh;

    invoke-direct {v0, p0}, Lclh;-><init>(Lcla;)V

    iput-object v0, p0, Lcla;->Y:Lip;

    .line 160
    iget-object v0, p0, Lcla;->W:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcla;->Y:Lip;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->a(Lip;)V

    .line 162
    invoke-virtual {p0}, Lcla;->n()Lz;

    move-result-object v0

    .line 163
    const v2, 0x7f05001f

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcla;->Z:Landroid/view/animation/Animation;

    .line 164
    iget-object v2, p0, Lcla;->Z:Landroid/view/animation/Animation;

    iget-object v3, p0, Lcla;->ab:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v2, v3}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 165
    const v2, 0x7f050015

    invoke-static {v0, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcla;->aa:Landroid/view/animation/Animation;

    .line 166
    iget-object v0, p0, Lcla;->aa:Landroid/view/animation/Animation;

    iget-object v2, p0, Lcla;->ab:Landroid/view/animation/Animation$AnimationListener;

    invoke-virtual {v0, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 167
    iget-object v0, p0, Lcla;->W:Landroid/support/v4/view/ViewPager;

    new-instance v2, Lcle;

    invoke-direct {v2, p0}, Lcle;-><init>(Lcla;)V

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->a(Lkc;)V

    .line 185
    const v0, 0x7f1002d2

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    iput-object v0, p0, Lcla;->ac:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    .line 187
    return-object v1
.end method

.method protected a(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V
    .locals 3

    .prologue
    .line 248
    new-instance v0, Lhdd;

    invoke-direct {v0, p1}, Lhdd;-><init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V

    iput-object v0, p0, Lcla;->ad:Lhdd;

    .line 249
    iget-object v0, p0, Lcla;->ad:Lhdd;

    invoke-virtual {p0}, Lcla;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0024

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lhdd;->a(I)V

    .line 251
    iget-object v0, p0, Lcla;->ad:Lhdd;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a(Lhdx;)I

    .line 252
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    .line 461
    invoke-virtual {p0}, Lcla;->s()Z

    move-result v0

    if-nez v0, :cond_1

    .line 484
    :cond_0
    return-void

    .line 465
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lcla;->ae:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_2

    .line 466
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Wrong number of images for Preset Previews."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 469
    :cond_2
    invoke-virtual {p0}, Lcla;->o()Landroid/content/res/Resources;

    move-result-object v8

    .line 470
    const v0, 0x7f0d0036

    invoke-virtual {v8, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    move v7, v6

    .line 472
    :goto_0
    iget-object v0, p0, Lcla;->ae:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v7, v0, :cond_0

    .line 473
    invoke-interface {p1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 475
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v1, v3

    div-int/lit8 v1, v1, 0x2

    .line 476
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    const/4 v5, 0x0

    move v4, v3

    .line 474
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 481
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1, v8, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 482
    iget-object v0, p0, Lcla;->ae:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmx;

    invoke-virtual {v0, v1}, Lcmx;->a(Landroid/graphics/drawable/Drawable;)V

    .line 472
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0
.end method

.method public aO_()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 213
    invoke-super {p0}, Lcje;->aO_()V

    .line 215
    new-instance v0, Lhap;

    invoke-virtual {p0}, Lcla;->n()Lz;

    move-result-object v1

    invoke-direct {v0, v1}, Lhap;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcla;->O:Lhap;

    .line 216
    iget-object v0, p0, Lcla;->O:Lhap;

    invoke-virtual {p0}, Lcla;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0024

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lhap;->setBackgroundColor(I)V

    .line 217
    iget-object v0, p0, Lcla;->O:Lhap;

    const/4 v1, 0x1

    invoke-virtual {v0, p0, v1}, Lhap;->a(Lhbl;Z)V

    .line 219
    iget-object v0, p0, Lcla;->R:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcla;->O:Lhap;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 221
    iget-object v0, p0, Lcla;->ac:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    iget-object v1, p0, Lcla;->O:Lhap;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a(Landroid/view/View;)V

    .line 222
    iget-object v0, p0, Lcla;->ac:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->c()V

    .line 224
    iget-object v0, p0, Lcla;->ac:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {p0, v0}, Lcla;->a(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V

    .line 226
    invoke-virtual {p0}, Lcla;->ah()Lchp;

    move-result-object v0

    .line 227
    invoke-virtual {v0}, Lchp;->g()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0}, Lchp;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_2

    new-instance v2, Lhbf;

    invoke-direct {v2}, Lhbf;-><init>()V

    invoke-virtual {v2, v1, v0}, Lhbf;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcla;->O:Lhap;

    invoke-virtual {v1, v2}, Lhap;->a(Lhbi;)V

    iput-object v2, p0, Lcla;->S:Lhbf;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iput v1, p0, Lcla;->U:I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iput v1, p0, Lcla;->V:I

    iget-object v1, p0, Lcla;->ae:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p0, Lcla;->P:Landroid/widget/LinearLayout;

    invoke-virtual {v1}, Landroid/widget/LinearLayout;->removeAllViews()V

    invoke-virtual {p0}, Lcla;->o()Landroid/content/res/Resources;

    move-result-object v6

    const v1, 0x7f0d0036

    invoke-virtual {v6, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    move v2, v1

    move v4, v3

    move v5, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->createCenterCropBitmap(Landroid/graphics/Bitmap;IIIII)Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v2, v6, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    const v0, 0x7f0d0037

    invoke-virtual {v6, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-direct {p0}, Lcla;->X()V

    iget-object v0, p0, Lcla;->at:Llnl;

    invoke-static {v0}, Lhea;->b(Landroid/content/Context;)Z

    move-result v5

    invoke-virtual {p0}, Lcla;->n()Lz;

    move-result-object v6

    move v0, v3

    :goto_0
    invoke-static {}, Lcom/google/android/libraries/photoeditor/filterparameters/FilmFilterParameter;->c()I

    move-result v7

    if-ge v0, v7, :cond_1

    invoke-static {v0}, Lcla;->d(I)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Lcmx;

    invoke-direct {v8, v6, v2, v7}, Lcmx;-><init>(Landroid/content/Context;Landroid/graphics/drawable/Drawable;Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v8, v7}, Lcmx;->setTag(Ljava/lang/Object;)V

    add-int/lit16 v7, v0, 0x3e8

    invoke-virtual {v8, v7}, Lcmx;->setId(I)V

    new-instance v7, Lclf;

    invoke-direct {v7, p0}, Lclf;-><init>(Lcla;)V

    invoke-virtual {v8, v7}, Lcmx;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v7, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v7, v1, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    div-int/lit8 v9, v4, 0x2

    iput v9, v7, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    div-int/lit8 v9, v4, 0x2

    iput v9, v7, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    if-eqz v5, :cond_0

    iget-object v9, p0, Lcla;->P:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v8, v3, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    :goto_1
    iget-object v7, p0, Lcla;->ae:Ljava/util/List;

    invoke-interface {v7, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v9, p0, Lcla;->P:Landroid/widget/LinearLayout;

    invoke-virtual {v9, v8, v7}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcla;->Y:Lip;

    invoke-virtual {v0}, Lip;->d()V

    invoke-virtual {p0}, Lcla;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilmFilterParameter;->f(I)I

    move-result v0

    invoke-direct {p0, v0}, Lcla;->e(I)V

    .line 229
    :cond_2
    iget-object v0, p0, Lcla;->ad:Lhdd;

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcla;->ah()Lchp;

    move-result-object v1

    invoke-virtual {v1}, Lchp;->h()Lcom/google/android/libraries/photoeditor/core/FilterChain;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getPostRotation()I

    move-result v0

    invoke-static {v0}, Lhbg;->a(I)F

    move-result v0

    :goto_2
    iget-object v2, p0, Lcla;->ad:Lhdd;

    invoke-virtual {v2, v0}, Lhdd;->a(F)V

    iget-object v0, p0, Lcla;->ad:Lhdd;

    invoke-virtual {v1}, Lchp;->g()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhdd;->a(Landroid/graphics/Bitmap;)V

    .line 231
    :cond_3
    iget-object v0, p0, Lcla;->O:Lhap;

    invoke-virtual {p0}, Lcla;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhap;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    .line 233
    invoke-direct {p0}, Lcla;->X()V

    .line 234
    return-void

    .line 229
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 111
    new-instance v0, Lhmk;

    sget-object v1, Lonc;->r:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method protected ad()Lhbn;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcla;->O:Lhap;

    return-object v0
.end method

.method protected af()Lhbj;
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lcla;->O:Lhap;

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 373
    const/16 v0, 0xc8

    return v0
.end method

.method protected c(Z)V
    .locals 1

    .prologue
    .line 367
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a:Lcom/google/android/libraries/photoeditor/core/NativeCore;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->setCompare(Z)V

    .line 368
    invoke-virtual {p0}, Lcla;->ak()V

    .line 369
    return-void
.end method

.method public z()V
    .locals 2

    .prologue
    .line 238
    invoke-super {p0}, Lcje;->z()V

    .line 240
    iget-object v0, p0, Lcla;->O:Lhap;

    invoke-virtual {v0}, Lhap;->b()V

    .line 241
    iget-object v0, p0, Lcla;->O:Lhap;

    invoke-virtual {v0, p0}, Lhap;->a(Lhbl;)V

    .line 243
    iget-object v0, p0, Lcla;->R:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcla;->O:Lhap;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 244
    const/4 v0, 0x0

    iput-object v0, p0, Lcla;->O:Lhap;

    .line 245
    return-void
.end method
