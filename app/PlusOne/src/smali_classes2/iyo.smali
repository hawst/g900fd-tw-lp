.class final Liyo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Liwp;


# instance fields
.field private a:Lmcb;

.field private b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lixl;",
            ">;"
        }
    .end annotation
.end field

.field private synthetic c:Liyn;


# direct methods
.method constructor <init>(Liyn;)V
    .locals 0

    .prologue
    .line 85
    iput-object p1, p0, Liyo;->c:Liyn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    return-void
.end method


# virtual methods
.method public a(Lhem;)I
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 115
    iget-object v0, p0, Liyo;->a:Lmcb;

    iget-object v4, v0, Lmcb;->a:Lnog;

    iget-object v5, v4, Lnog;->b:Lnoj;

    iget-object v0, v5, Lnoj;->c:Lnok;

    iget-object v0, v0, Lnok;->b:Ljava/lang/String;

    const-string v3, "gaia_id"

    invoke-interface {p1, v3, v0}, Lhem;->c(Ljava/lang/String;Ljava/lang/String;)Lhem;

    move-result-object v0

    const-string v3, "is_plus_page"

    iget-object v6, v5, Lnoj;->b:Ljava/lang/Boolean;

    invoke-static {v6}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v6

    invoke-interface {v0, v3, v6}, Lhem;->c(Ljava/lang/String;Z)Lhem;

    move-result-object v0

    const-string v3, "display_name"

    iget-object v6, v5, Lnoj;->c:Lnok;

    iget-object v6, v6, Lnok;->d:Ljava/lang/String;

    invoke-interface {v0, v3, v6}, Lhem;->c(Ljava/lang/String;Ljava/lang/String;)Lhem;

    move-result-object v0

    const-string v3, "given_name"

    iget-object v6, v5, Lnoj;->c:Lnok;

    iget-object v6, v6, Lnok;->i:Ljava/lang/String;

    invoke-interface {v0, v3, v6}, Lhem;->c(Ljava/lang/String;Ljava/lang/String;)Lhem;

    move-result-object v0

    const-string v3, "family_name"

    iget-object v6, v5, Lnoj;->c:Lnok;

    iget-object v6, v6, Lnok;->j:Ljava/lang/String;

    invoke-interface {v0, v3, v6}, Lhem;->c(Ljava/lang/String;Ljava/lang/String;)Lhem;

    move-result-object v0

    const-string v3, "domain_name"

    iget-object v6, v5, Lnoj;->c:Lnok;

    iget-object v6, v6, Lnok;->g:Ljava/lang/String;

    invoke-interface {v0, v3, v6}, Lhem;->c(Ljava/lang/String;Ljava/lang/String;)Lhem;

    move-result-object v3

    const-string v6, "is_dasher_account"

    iget-object v0, v5, Lnoj;->c:Lnok;

    iget-object v0, v0, Lnok;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-interface {v3, v6, v0}, Lhem;->c(Ljava/lang/String;Z)Lhem;

    move-result-object v0

    const-string v3, "is_child"

    iget-object v6, v5, Lnoj;->a:Ljava/lang/Boolean;

    invoke-static {v6}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v6

    invoke-interface {v0, v3, v6}, Lhem;->c(Ljava/lang/String;Z)Lhem;

    move-result-object v3

    const-string v6, "is_default_restricted"

    iget v0, v4, Lnog;->d:I

    const/4 v7, 0x2

    if-ne v0, v7, :cond_1

    move v0, v1

    :goto_1
    invoke-interface {v3, v6, v0}, Lhem;->c(Ljava/lang/String;Z)Lhem;

    move-result-object v0

    const-string v3, "email_gaia_id"

    iget-object v6, v5, Lnoj;->c:Lnok;

    iget-object v6, v6, Lnok;->c:Ljava/lang/String;

    invoke-interface {v0, v3, v6}, Lhem;->c(Ljava/lang/String;Ljava/lang/String;)Lhem;

    iget-object v0, v5, Lnoj;->c:Lnok;

    iget-object v0, v0, Lnok;->f:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "profile_photo_url"

    invoke-interface {p1, v0}, Lhem;->g(Ljava/lang/String;)Lhem;

    :goto_2
    const-string v0, "page_count"

    invoke-interface {p1, v0, v2}, Lhem;->a(Ljava/lang/String;I)I

    move-result v3

    move v0, v2

    :goto_3
    if-ge v0, v3, :cond_3

    const-string v6, "page"

    invoke-interface {p1, v6}, Lhem;->h(Ljava/lang/String;)Lhem;

    move-result-object v6

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-interface {v6, v7}, Lhem;->h(Ljava/lang/String;)Lhem;

    move-result-object v6

    const-string v7, "gaia_id"

    invoke-interface {v6, v7}, Lhem;->g(Ljava/lang/String;)Lhem;

    move-result-object v6

    const-string v7, "display_name"

    invoke-interface {v6, v7}, Lhem;->g(Ljava/lang/String;)Lhem;

    move-result-object v6

    const-string v7, "avatar_url"

    invoke-interface {v6, v7}, Lhem;->g(Ljava/lang/String;)Lhem;

    move-result-object v6

    const-string v7, "email_gaia_id"

    invoke-interface {v6, v7}, Lhem;->g(Ljava/lang/String;)Lhem;

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    const-string v0, "profile_photo_url"

    iget-object v3, v5, Lnoj;->c:Lnok;

    iget-object v3, v3, Lnok;->e:Ljava/lang/String;

    invoke-static {v3}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1, v0, v3}, Lhem;->c(Ljava/lang/String;Ljava/lang/String;)Lhem;

    goto :goto_2

    :cond_3
    iget-object v0, v5, Lnoj;->d:[Lnok;

    if-nez v0, :cond_4

    move v0, v2

    :goto_4
    move v3, v2

    :goto_5
    if-ge v3, v0, :cond_5

    iget-object v6, v5, Lnoj;->d:[Lnok;

    aget-object v6, v6, v3

    const-string v7, "page"

    invoke-interface {p1, v7}, Lhem;->h(Ljava/lang/String;)Lhem;

    move-result-object v7

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-interface {v7, v8}, Lhem;->h(Ljava/lang/String;)Lhem;

    move-result-object v7

    const-string v8, "gaia_id"

    iget-object v9, v6, Lnok;->b:Ljava/lang/String;

    invoke-interface {v7, v8, v9}, Lhem;->c(Ljava/lang/String;Ljava/lang/String;)Lhem;

    move-result-object v7

    const-string v8, "display_name"

    iget-object v9, v6, Lnok;->d:Ljava/lang/String;

    invoke-interface {v7, v8, v9}, Lhem;->c(Ljava/lang/String;Ljava/lang/String;)Lhem;

    move-result-object v7

    const-string v8, "avatar_url"

    iget-object v9, v6, Lnok;->e:Ljava/lang/String;

    invoke-static {v9}, Llsy;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Lhem;->c(Ljava/lang/String;Ljava/lang/String;)Lhem;

    move-result-object v7

    const-string v8, "email_gaia_id"

    iget-object v6, v6, Lnok;->c:Ljava/lang/String;

    invoke-interface {v7, v8, v6}, Lhem;->c(Ljava/lang/String;Ljava/lang/String;)Lhem;

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_4
    iget-object v0, v5, Lnoj;->d:[Lnok;

    array-length v0, v0

    goto :goto_4

    :cond_5
    const-string v3, "page_count"

    invoke-interface {p1, v3, v0}, Lhem;->c(Ljava/lang/String;I)Lhem;

    iget-object v3, v5, Lnoj;->e:Ljava/lang/Boolean;

    invoke-static {v3}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v3

    if-nez v3, :cond_7

    move v3, v1

    :goto_6
    iget v4, v4, Lnog;->a:I

    if-ne v4, v1, :cond_8

    move v5, v1

    :goto_7
    if-nez v3, :cond_9

    if-lez v0, :cond_9

    move v0, v1

    :goto_8
    const-string v6, "is_google_plus"

    if-eqz v3, :cond_a

    if-nez v5, :cond_a

    move v4, v1

    :goto_9
    invoke-interface {p1, v6, v4}, Lhem;->c(Ljava/lang/String;Z)Lhem;

    const-string v6, "gplus_no_mobile_tos"

    if-eqz v3, :cond_b

    if-eqz v5, :cond_b

    move v4, v1

    :goto_a
    invoke-interface {p1, v6, v4}, Lhem;->c(Ljava/lang/String;Z)Lhem;

    const-string v4, "gplus_skinny_page"

    invoke-interface {p1, v4, v0}, Lhem;->c(Ljava/lang/String;Z)Lhem;

    const-string v0, "PlusiAccountUpdateExtension.oob_required"

    invoke-interface {p1, v0, v5}, Lhem;->c(Ljava/lang/String;Z)Lhem;

    const-string v0, "PlusiAccountUpdateExtension.wants_full_update"

    invoke-interface {p1, v0, v3}, Lhem;->c(Ljava/lang/String;Z)Lhem;

    .line 117
    iget-object v0, p0, Liyo;->c:Liyn;

    invoke-static {v0}, Liyn;->b(Liyn;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 118
    :goto_b
    if-ge v2, v3, :cond_c

    .line 119
    iget-object v0, p0, Liyo;->c:Liyn;

    invoke-static {v0}, Liyn;->b(Liyn;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lixl;

    .line 120
    iget-object v4, p0, Liyo;->b:Ljava/util/Set;

    invoke-interface {v4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 121
    iget-object v4, p0, Liyo;->a:Lmcb;

    invoke-interface {v0, p1, v4}, Lixl;->a(Lhem;Lmcb;)V

    .line 118
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_b

    :cond_7
    move v3, v2

    .line 115
    goto :goto_6

    :cond_8
    move v5, v2

    goto :goto_7

    :cond_9
    move v0, v2

    goto :goto_8

    :cond_a
    move v4, v2

    goto :goto_9

    :cond_b
    move v4, v2

    goto :goto_a

    .line 125
    :cond_c
    return v1
.end method

.method public a(Lhej;Lkfo;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lhej;",
            "Lkfo;",
            "Ljava/util/List",
            "<",
            "Lkff;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 92
    new-instance v4, Lmca;

    invoke-direct {v4}, Lmca;-><init>()V

    .line 93
    new-instance v0, Lnoa;

    invoke-direct {v0}, Lnoa;-><init>()V

    iput-object v0, v4, Lmca;->a:Lnoa;

    .line 94
    iget-object v0, v4, Lmca;->a:Lnoa;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnoa;->a:Ljava/lang/Boolean;

    .line 95
    iget-object v0, v4, Lmca;->a:Lnoa;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnoa;->d:Ljava/lang/Boolean;

    .line 97
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Liyo;->b:Ljava/util/Set;

    .line 99
    iget-object v0, p0, Liyo;->c:Liyn;

    invoke-static {v0}, Liyn;->b(Liyn;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 100
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 101
    iget-object v0, p0, Liyo;->c:Liyn;

    invoke-static {v0}, Liyn;->b(Liyn;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lixl;

    .line 102
    invoke-interface {v0, p1, v4}, Lixl;->a(Lhej;Lmca;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 103
    iget-object v3, p0, Liyo;->b:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 100
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 107
    :cond_1
    new-instance v0, Lmcb;

    invoke-direct {v0}, Lmcb;-><init>()V

    iput-object v0, p0, Liyo;->a:Lmcb;

    .line 108
    new-instance v0, Liyp;

    iget-object v1, p0, Liyo;->c:Liyn;

    invoke-static {v1}, Liyn;->a(Liyn;)Landroid/content/Context;

    move-result-object v1

    const-string v3, "getmobilesettings"

    iget-object v5, p0, Liyo;->a:Lmcb;

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Liyp;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    invoke-interface {p3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 110
    return-void
.end method
