.class final Lbpw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Lbmp;",
        ">;"
    }
.end annotation


# instance fields
.field private a:I

.field private synthetic b:Lbpu;


# direct methods
.method constructor <init>(Lbpu;)V
    .locals 0

    .prologue
    .line 263
    iput-object p1, p0, Lbpw;->b:Lbpu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Lbmp;
    .locals 2

    .prologue
    .line 273
    invoke-virtual {p0}, Lbpw;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 274
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 276
    :cond_0
    iget-object v0, p0, Lbpw;->b:Lbpu;

    iget-object v0, v0, Lbpu;->a:Landroid/util/LongSparseArray;

    iget v1, p0, Lbpw;->a:I

    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmp;

    .line 277
    iget v1, p0, Lbpw;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lbpw;->a:I

    .line 278
    return-object v0
.end method

.method public hasNext()Z
    .locals 2

    .prologue
    .line 268
    iget v0, p0, Lbpw;->a:I

    iget-object v1, p0, Lbpw;->b:Lbpu;

    iget-object v1, v1, Lbpu;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v1}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 263
    invoke-virtual {p0}, Lbpw;->a()Lbmp;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 283
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
