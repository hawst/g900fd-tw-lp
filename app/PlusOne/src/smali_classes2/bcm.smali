.class public final Lbcm;
.super Lloj;
.source "PG"

# interfaces
.implements Lanl;
.implements Lcdw;


# instance fields
.field private Q:Lanj;

.field private R:Lant;

.field private S:Landroid/widget/ProgressBar;

.field private T:Landroid/widget/TextView;

.field private U:Ljava/text/NumberFormat;

.field private V:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lloj;-><init>()V

    .line 131
    return-void
.end method

.method public static b(Lanm;)Lbcm;
    .locals 4

    .prologue
    .line 62
    new-instance v0, Lbcm;

    invoke-direct {v0}, Lbcm;-><init>()V

    .line 63
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 64
    const-string v2, "initial_message"

    invoke-virtual {p0}, Lanm;->ordinal()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 65
    invoke-virtual {v0, v1}, Lbcm;->f(Landroid/os/Bundle;)V

    .line 66
    return-object v0
.end method


# virtual methods
.method public a(Lanm;)V
    .locals 3

    .prologue
    .line 123
    sget-object v0, Lbcn;->a:[I

    invoke-virtual {p1}, Lanm;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 131
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x15

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unsupported message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 125
    :pswitch_0
    iget-object v0, p0, Lbcm;->V:Landroid/widget/TextView;

    const v1, 0x7f0a015f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 129
    :goto_0
    return-void

    .line 128
    :pswitch_1
    iget-object v0, p0, Lbcm;->V:Landroid/widget/TextView;

    const v1, 0x7f0a0117

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 123
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lant;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lbcm;->R:Lant;

    .line 113
    return-void
.end method

.method public a_(I)V
    .locals 4

    .prologue
    .line 117
    iget-object v0, p0, Lbcm;->S:Landroid/widget/ProgressBar;

    invoke-virtual {v0, p1}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 118
    iget-object v0, p0, Lbcm;->T:Landroid/widget/TextView;

    iget-object v1, p0, Lbcm;->U:Ljava/text/NumberFormat;

    int-to-float v2, p1

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    float-to-double v2, v2

    invoke-virtual {v1, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    return-void
.end method

.method public ae_()V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lbcm;->Q:Lanj;

    invoke-virtual {v0, p0}, Lanj;->b(Lanl;)V

    .line 99
    invoke-super {p0}, Lloj;->ae_()V

    .line 100
    return-void
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 79
    invoke-virtual {p0}, Lbcm;->n()Lz;

    move-result-object v1

    .line 80
    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 81
    const v2, 0x7f0400f2

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 82
    const v0, 0x7f100343

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lbcm;->S:Landroid/widget/ProgressBar;

    .line 83
    const v0, 0x7f100344

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbcm;->T:Landroid/widget/TextView;

    .line 84
    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iput-object v0, p0, Lbcm;->U:Ljava/text/NumberFormat;

    .line 85
    iget-object v0, p0, Lbcm;->U:Ljava/text/NumberFormat;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 86
    const v0, 0x7f100139

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbcm;->V:Landroid/widget/TextView;

    .line 87
    invoke-virtual {p0}, Lbcm;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "initial_message"

    sget-object v4, Lanm;->a:Lanm;

    invoke-virtual {v4}, Lanm;->ordinal()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 88
    invoke-static {}, Lanm;->values()[Lanm;

    move-result-object v3

    aget-object v0, v3, v0

    .line 89
    invoke-virtual {p0, v0}, Lbcm;->a(Lanm;)V

    .line 90
    const/4 v0, 0x1

    invoke-virtual {v2, v0}, Landroid/view/View;->setKeepScreenOn(Z)V

    .line 91
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 92
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 93
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public d()Lcdz;
    .locals 1

    .prologue
    .line 137
    sget-object v0, Lcdz;->i:Lcdz;

    return-object v0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 71
    invoke-super {p0, p1}, Lloj;->d(Landroid/os/Bundle;)V

    .line 73
    invoke-static {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->b(Lu;)Lcom/google/android/apps/moviemaker/MovieMakerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o()Laol;

    move-result-object v0

    invoke-virtual {v0}, Laol;->l()Lanj;

    move-result-object v0

    iput-object v0, p0, Lbcm;->Q:Lanj;

    .line 74
    iget-object v0, p0, Lbcm;->Q:Lanj;

    invoke-virtual {v0, p0}, Lanj;->a(Lanl;)V

    .line 75
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lbcm;->R:Lant;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lbcm;->R:Lant;

    invoke-interface {v0}, Lant;->a()V

    .line 107
    :cond_0
    invoke-super {p0, p1}, Lloj;->onCancel(Landroid/content/DialogInterface;)V

    .line 108
    return-void
.end method
