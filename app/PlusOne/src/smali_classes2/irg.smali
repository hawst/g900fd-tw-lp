.class public final Lirg;
.super Lirc;
.source "PG"

# interfaces
.implements Lire;


# instance fields
.field private final a:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Lirc;-><init>()V

    .line 27
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lirg;->a:Landroid/content/Intent;

    .line 28
    iget-object v0, p0, Lirg;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 29
    iget-object v0, p0, Lirg;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.people.smart_profile.ACTION_SHOW_PROFILE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 30
    return-void
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lirg;->a:Landroid/content/Intent;

    return-object v0
.end method

.method public synthetic a(I)Lire;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lirg;->b(I)Lirg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic a(Ljava/lang/String;)Lire;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lirg;->d(Ljava/lang/String;)Lirg;

    move-result-object v0

    return-object v0
.end method

.method public synthetic b(Ljava/lang/String;)Lire;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lirg;->e(Ljava/lang/String;)Lirg;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lirg;
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lirg;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.people.smart_profile.APPLICATION_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 53
    return-object p0
.end method

.method public synthetic c(Ljava/lang/String;)Lire;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0, p1}, Lirg;->f(Ljava/lang/String;)Lirg;

    move-result-object v0

    return-object v0
.end method

.method public d(Ljava/lang/String;)Lirg;
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lirg;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.people.smart_profile.VIEWER_ACCOUNT_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 35
    return-object p0
.end method

.method public e(Ljava/lang/String;)Lirg;
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lirg;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.people.smart_profile.VIEWER_PAGE_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 41
    return-object p0
.end method

.method public f(Ljava/lang/String;)Lirg;
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lirg;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms.people.smart_profile.QUALIFIED_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 47
    return-object p0
.end method
