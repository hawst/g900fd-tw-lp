.class public final Laht;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lahw;


# instance fields
.field private final a:Lahv;

.field private final b:Lanh;

.field private final c:Lcgt;


# direct methods
.method public constructor <init>(Lahv;Lanh;Lcgt;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    const-string v0, "storage"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahv;

    iput-object v0, p0, Laht;->a:Lahv;

    .line 97
    const-string v0, "gservicesSettings"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lanh;

    iput-object v0, p0, Laht;->b:Lanh;

    .line 98
    const-string v0, "systemClock"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgt;

    iput-object v0, p0, Laht;->c:Lcgt;

    .line 99
    return-void
.end method

.method private b()V
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 133
    iget-object v1, p0, Laht;->a:Lahv;

    invoke-interface {v1}, Lahv;->b()J

    move-result-wide v2

    .line 134
    iget-object v1, p0, Laht;->c:Lcgt;

    invoke-virtual {v1}, Lcgt;->b()J

    move-result-wide v4

    .line 136
    const/4 v1, 0x0

    .line 137
    const-wide/16 v6, -0x1

    cmp-long v6, v2, v6

    if-nez v6, :cond_2

    .line 147
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 148
    iget-object v0, p0, Laht;->a:Lahv;

    invoke-interface {v0, v4, v5}, Lahv;->b(J)V

    .line 149
    iget-object v0, p0, Laht;->a:Lahv;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v2, v3}, Lahv;->a(J)V

    .line 151
    :cond_1
    return-void

    .line 140
    :cond_2
    iget-object v6, p0, Laht;->b:Lanh;

    .line 141
    invoke-virtual {v6}, Lanh;->B()J

    move-result-wide v6

    .line 142
    add-long/2addr v2, v6

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 6

    .prologue
    .line 117
    invoke-direct {p0}, Laht;->b()V

    .line 119
    iget-object v0, p0, Laht;->b:Lanh;

    .line 120
    invoke-virtual {v0}, Lanh;->C()J

    move-result-wide v0

    .line 121
    iget-object v2, p0, Laht;->a:Lahv;

    invoke-interface {v2}, Lahv;->a()J

    move-result-wide v2

    .line 122
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    .line 123
    const-wide/16 v4, 0x0

    sub-long/2addr v0, v2

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 125
    :cond_0
    return-wide v0
.end method

.method public a(J)V
    .locals 5

    .prologue
    .line 103
    iget-object v0, p0, Laht;->a:Lahv;

    invoke-interface {v0}, Lahv;->a()J

    move-result-wide v0

    .line 104
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 105
    iget-object v0, p0, Laht;->a:Lahv;

    invoke-interface {v0, p1, p2}, Lahv;->a(J)V

    .line 112
    :goto_0
    invoke-direct {p0}, Laht;->b()V

    .line 113
    return-void

    .line 107
    :cond_0
    iget-object v2, p0, Laht;->a:Lahv;

    add-long/2addr v0, p1

    invoke-interface {v2, v0, v1}, Lahv;->a(J)V

    goto :goto_0
.end method
