.class public final Laxv;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljdz;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljfb;

.field private final b:Laxx;


# direct methods
.method constructor <init>(Ljfb;Laxx;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 38
    const-string v0, "movieMakerProvider"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfb;

    iput-object v0, p0, Laxv;->a:Ljfb;

    .line 39
    const-string v0, "listener"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxx;

    iput-object v0, p0, Laxv;->b:Laxx;

    .line 40
    return-void
.end method


# virtual methods
.method protected varargs a([Ljdz;)Ljava/lang/Void;
    .locals 4

    .prologue
    .line 44
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    .line 45
    iget-object v3, p0, Laxv;->a:Ljfb;

    invoke-virtual {v3, v2}, Ljfb;->a(Ljdz;)V

    .line 44
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 47
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a()V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Laxv;->b:Laxx;

    invoke-interface {v0, p0}, Laxx;->a(Laxv;)V

    .line 53
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 10
    check-cast p1, [Ljdz;

    invoke-virtual {p0, p1}, Laxv;->a([Ljdz;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 10
    invoke-virtual {p0}, Laxv;->a()V

    return-void
.end method
