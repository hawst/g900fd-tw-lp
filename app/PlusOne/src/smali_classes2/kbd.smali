.class public final Lkbd;
.super Lkaz;
.source "PG"


# static fields
.field private static g:Z

.field private static h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lkaz;-><init>(Landroid/content/Context;)V

    .line 45
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 72
    invoke-super {p0, p1}, Lkaz;->a(Landroid/content/Context;)V

    .line 73
    sget-boolean v0, Lkbd;->g:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lkbd;->h:I

    const/4 v0, 0x1

    sput-boolean v0, Lkbd;->g:Z

    .line 74
    :cond_0
    return-void
.end method

.method c()I
    .locals 4

    .prologue
    .line 244
    iget v0, p0, Lkbd;->c:I

    int-to-double v0, v0

    const-wide/high16 v2, 0x3fe2000000000000L    # 0.5625

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method c(I)V
    .locals 4

    .prologue
    .line 224
    invoke-super {p0, p1}, Lkaz;->c(I)V

    .line 225
    iget-object v0, p0, Lkbd;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 227
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    .line 226
    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 229
    iget-object v1, p0, Lkbd;->m:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 230
    const/4 v3, -0x2

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->measure(II)V

    .line 234
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    .line 235
    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    .line 237
    sget v3, Lkbd;->k:I

    invoke-virtual {p0, v1, v0, v3, v2}, Lkbd;->b(Landroid/view/View;Landroid/view/View;II)V

    .line 240
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    .prologue
    .line 148
    sget-object v0, Lkbd;->a:Llct;

    iget v3, v0, Llct;->m:I

    .line 150
    iget-object v0, p0, Lkbd;->e:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 152
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v1

    add-int v2, v3, v1

    iget-object v1, p0, Lkbd;->n:Ljava/util/ArrayList;

    const/4 v4, 0x0

    .line 153
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    sub-int v1, v2, v1

    sget v2, Lkbd;->h:I

    sub-int v2, v1, v2

    .line 156
    const/4 v1, 0x0

    .line 159
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredWidth()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    .line 160
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v5, v3

    .line 156
    invoke-virtual {v0, v1, v3, v4, v5}, Lcom/google/android/libraries/social/media/ui/MediaView;->layout(IIII)V

    .line 162
    iget-object v1, p0, Lkbd;->f:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 164
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v3

    .line 165
    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    sget-object v5, Lkbd;->a:Llct;

    iget v5, v5, Llct;->m:I

    sub-int/2addr v4, v5

    sget v5, Lkbd;->i:I

    add-int/2addr v4, v5

    .line 169
    const/4 v5, 0x0

    .line 171
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v3

    .line 172
    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredWidth()I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    .line 169
    invoke-virtual {v1, v5, v0, v3, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->layout(IIII)V

    .line 175
    iget-object v0, p0, Lkbd;->b:Lkzw;

    invoke-virtual {v0}, Lkzw;->f()I

    move-result v4

    .line 176
    sget v5, Lkbd;->i:I

    .line 177
    const/4 v0, 0x0

    move v3, v2

    move v2, v0

    :goto_0
    iget v0, p0, Lkbd;->r:I

    if-ge v2, v0, :cond_1

    .line 179
    iget-object v0, p0, Lkbd;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 180
    sget v1, Lkbd;->h:I

    add-int/2addr v1, v3

    .line 183
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v5

    .line 184
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v3

    sget v8, Lkbd;->h:I

    add-int/2addr v7, v8

    .line 180
    invoke-virtual {v0, v5, v1, v6, v7}, Landroid/view/View;->layout(IIII)V

    .line 188
    iget-object v1, p0, Lkbd;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 189
    sget v6, Lkbd;->h:I

    add-int/2addr v6, v3

    .line 192
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v5

    .line 193
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v3

    sget v9, Lkbd;->h:I

    add-int/2addr v8, v9

    .line 189
    invoke-virtual {v1, v5, v6, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 197
    invoke-virtual {p0, v2}, Lkbd;->c(I)V

    .line 200
    invoke-virtual {p0}, Lkbd;->m()Z

    move-result v1

    if-nez v1, :cond_0

    .line 201
    iget-object v1, p0, Lkbd;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 202
    sget v6, Lkbd;->h:I

    add-int/2addr v6, v3

    .line 205
    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v7

    add-int/2addr v7, v5

    .line 206
    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v3

    sget v9, Lkbd;->h:I

    add-int/2addr v8, v9

    .line 202
    invoke-virtual {v1, v5, v6, v7, v8}, Landroid/widget/TextView;->layout(IIII)V

    .line 212
    :goto_1
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    sget v1, Lkbd;->h:I

    add-int/2addr v0, v1

    add-int v1, v3, v0

    .line 177
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v3, v1

    goto :goto_0

    .line 209
    :cond_0
    invoke-virtual {p0, v2}, Lkbd;->d(I)V

    goto :goto_1

    .line 215
    :cond_1
    const/4 v0, -0x1

    if-eq v4, v0, :cond_2

    .line 216
    invoke-virtual {p0, v4}, Lkbd;->f(I)V

    .line 218
    :cond_2
    invoke-virtual {p0}, Lkbd;->d()V

    .line 219
    iget v0, p0, Lkbd;->c:I

    int-to-float v1, v0

    iget-object v0, p0, Lkbd;->n:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v1, v0

    iput v0, p0, Lkbd;->t:F

    .line 220
    return-void
.end method

.method protected onMeasure(II)V
    .locals 13

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    const/4 v11, -0x2

    const/4 v3, 0x0

    .line 78
    iget-object v0, p0, Lkbd;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, v11, v11}, Landroid/widget/ImageView;->measure(II)V

    .line 80
    iget v0, p0, Lkbd;->c:I

    .line 81
    invoke-static {v0, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 82
    iget-object v0, p0, Lkbd;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 86
    iget v1, p0, Lkbd;->d:I

    invoke-virtual {v0, v6, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->measure(II)V

    .line 87
    iget v1, p0, Lkbd;->c:I

    sget v2, Lkbd;->i:I

    shl-int/lit8 v2, v2, 0x1

    sub-int v7, v1, v2

    .line 88
    invoke-static {v7, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 91
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v0

    add-int/lit8 v9, v0, 0x0

    move v2, v3

    move v4, v3

    .line 94
    :goto_0
    iget v0, p0, Lkbd;->r:I

    if-ge v2, v0, :cond_1

    .line 95
    iget v0, p0, Lkbd;->q:I

    if-ne v2, v0, :cond_0

    iget-object v0, p0, Lkbd;->p:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    move v5, v0

    .line 96
    :goto_1
    iget-object v0, p0, Lkbd;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 97
    invoke-virtual {v0, v11, v11}, Landroid/widget/TextView;->measure(II)V

    .line 99
    iget-object v1, p0, Lkbd;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 101
    sub-int v5, v7, v5

    sget v10, Lkbd;->k:I

    div-int/lit8 v10, v10, 0x2

    sub-int/2addr v5, v10

    .line 104
    invoke-virtual {v0}, Landroid/widget/TextView;->getWidth()I

    move-result v0

    sub-int v0, v5, v0

    .line 101
    invoke-static {v0, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 106
    invoke-virtual {v1, v0, v11}, Landroid/widget/TextView;->measure(II)V

    .line 108
    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    if-le v0, v4, :cond_3

    .line 109
    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    .line 94
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v4, v1

    goto :goto_0

    :cond_0
    move v5, v3

    .line 95
    goto :goto_1

    :cond_1
    move v1, v3

    .line 113
    :goto_3
    iget v0, p0, Lkbd;->r:I

    if-ge v1, v0, :cond_2

    .line 114
    iget-object v0, p0, Lkbd;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 116
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    invoke-static {v2, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 117
    invoke-static {v4, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 121
    invoke-virtual {v0, v2, v5}, Landroid/widget/TextView;->measure(II)V

    .line 125
    iget-object v0, p0, Lkbd;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v8, v5}, Landroid/view/View;->measure(II)V

    .line 129
    iget-object v0, p0, Lkbd;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v11, v5}, Landroid/view/View;->measure(II)V

    .line 113
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 132
    :cond_2
    iget-object v0, p0, Lkbd;->l:Ljava/util/ArrayList;

    .line 133
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    iget v1, p0, Lkbd;->r:I

    add-int/lit8 v1, v1, -0x1

    mul-int/2addr v0, v1

    sget v1, Lkbd;->h:I

    iget v2, p0, Lkbd;->r:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    sget v1, Lkbd;->i:I

    add-int/2addr v0, v1

    .line 135
    invoke-static {v0, v12}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 138
    iget-object v0, p0, Lkbd;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 139
    invoke-virtual {v0, v6, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->measure(II)V

    .line 141
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v0

    sget-object v1, Lkbd;->a:Llct;

    iget v1, v1, Llct;->m:I

    add-int/2addr v0, v1

    add-int/2addr v0, v9

    .line 142
    iget v1, p0, Lkbd;->c:I

    invoke-virtual {p0, v1, v0}, Lkbd;->setMeasuredDimension(II)V

    .line 143
    return-void

    :cond_3
    move v1, v4

    goto :goto_2
.end method
