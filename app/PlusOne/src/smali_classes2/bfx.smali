.class public final Lbfx;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lbgf;IIZ)Lbgx;
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0, p3}, Lbfx;->a(Lbgf;IIZZ)Lbgx;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lbgf;IIZZ)Lbgx;
    .locals 19

    .prologue
    .line 76
    shl-int/lit8 v2, p1, 0x1

    add-int/lit8 v2, v2, 0x1

    new-array v5, v2, [F

    const v2, 0x3ecccccd    # 0.4f

    move/from16 v0, p1

    int-to-float v3, v0

    mul-float/2addr v2, v3

    const v3, 0x3f19999a    # 0.6f

    add-float/2addr v2, v3

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide v8, 0x401921fb54442d18L    # 6.283185307179586

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    float-to-double v10, v2

    mul-double/2addr v8, v10

    div-double/2addr v6, v8

    double-to-float v4, v6

    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v6, 0x40000000    # 2.0f

    mul-float/2addr v6, v2

    mul-float/2addr v2, v6

    div-float v6, v3, v2

    const/4 v3, 0x0

    move/from16 v0, p1

    neg-int v2, v0

    :goto_0
    if-gtz v2, :cond_0

    add-int v7, v2, p1

    float-to-double v8, v4

    mul-int v10, v2, v2

    int-to-float v10, v10

    mul-float/2addr v10, v6

    float-to-double v10, v10

    invoke-static {v10, v11}, Ljava/lang/Math;->exp(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    double-to-float v8, v8

    aput v8, v5, v7

    add-int v7, v2, p1

    aget v7, v5, v7

    add-float/2addr v3, v7

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    const/4 v2, 0x1

    :goto_1
    move/from16 v0, p1

    if-gt v2, v0, :cond_1

    add-int v4, v2, p1

    sub-int v6, p1, v2

    aget v6, v5, v6

    aput v6, v5, v4

    add-int v4, v2, p1

    aget v4, v5, v4

    add-float/2addr v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    const/high16 v2, 0x3f800000    # 1.0f

    div-float v3, v2, v3

    move/from16 v0, p1

    neg-int v2, v0

    :goto_2
    move/from16 v0, p1

    if-gt v2, v0, :cond_2

    add-int v4, v2, p1

    aget v6, v5, v4

    mul-float/2addr v6, v3

    aput v6, v5, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 77
    :cond_2
    const/high16 v2, 0x3f800000    # 1.0f

    move/from16 v0, p2

    int-to-float v3, v0

    div-float v6, v2, v3

    array-length v2, v5

    rem-int/lit8 v2, v2, 0x2

    if-eqz v2, :cond_7

    const/4 v2, 0x1

    :goto_3
    const-string v3, "original mask cannot have even size"

    invoke-static {v2, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    array-length v2, v5

    div-int/lit8 v7, v2, 0x2

    aget v8, v5, v7

    rem-int/lit8 v2, v7, 0x2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    aget v2, v5, v7

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    aput v2, v5, v7

    :cond_3
    add-int/lit8 v9, v7, 0x1

    div-int/lit8 v10, v9, 0x2

    new-instance v11, Lbfy;

    invoke-direct {v11, v9}, Lbfy;-><init>(I)V

    const/4 v2, 0x0

    rem-int/lit8 v3, v9, 0x2

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    iget-object v2, v11, Lbfy;->a:[F

    aget v3, v5, v7

    aput v3, v2, v10

    iget-object v2, v11, Lbfy;->b:[F

    const/4 v3, 0x0

    aput v3, v2, v10

    const/4 v2, 0x1

    :cond_4
    const/4 v4, 0x0

    const/4 v3, 0x0

    move/from16 v18, v3

    move v3, v4

    move/from16 v4, v18

    :goto_4
    if-ge v4, v9, :cond_8

    if-ne v4, v10, :cond_5

    const/4 v3, -0x1

    if-nez v2, :cond_6

    :cond_5
    mul-int/lit8 v12, v4, 0x2

    add-int/2addr v12, v3

    add-int/lit8 v13, v12, 0x1

    aget v14, v5, v12

    aget v15, v5, v13

    sub-int/2addr v12, v7

    int-to-float v12, v12

    mul-float/2addr v12, v6

    sub-int/2addr v13, v7

    int-to-float v13, v13

    mul-float/2addr v13, v6

    add-float v16, v14, v15

    iget-object v0, v11, Lbfy;->a:[F

    move-object/from16 v17, v0

    aput v16, v17, v4

    iget-object v0, v11, Lbfy;->b:[F

    move-object/from16 v17, v0

    mul-float/2addr v12, v14

    mul-float/2addr v13, v15

    add-float/2addr v12, v13

    div-float v12, v12, v16

    aput v12, v17, v4

    :cond_6
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_7
    const/4 v2, 0x0

    goto :goto_3

    :cond_8
    aput v8, v5, v7

    .line 78
    invoke-virtual/range {p0 .. p0}, Lbgf;->f()Lbgr;

    move-result-object v6

    if-eqz p4, :cond_a

    const v2, 0x7f080032

    .line 80
    :goto_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz p4, :cond_b

    const-string v3, "sampler_source_a"

    move-object v5, v3

    :goto_6
    if-eqz p4, :cond_c

    const-string v3, "v_texcoord_a"

    :goto_7
    const/4 v4, 0x0

    :goto_8
    iget-object v8, v11, Lbfy;->a:[F

    array-length v8, v8

    if-ge v4, v8, :cond_e

    if-lez v4, :cond_9

    const-string v8, " + "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_9
    if-eqz p3, :cond_d

    iget-object v8, v11, Lbfy;->a:[F

    aget v8, v8, v4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v8, " * texture2D("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ", vec2("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ".x + "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, v11, Lbfy;->b:[F

    aget v8, v8, v4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v8, ", "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ".y))"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_9
    add-int/lit8 v4, v4, 0x1

    goto :goto_8

    .line 78
    :cond_a
    const v2, 0x7f08002e

    goto :goto_5

    .line 80
    :cond_b
    const-string v3, "sampler_source"

    move-object v5, v3

    goto :goto_6

    :cond_c
    const-string v3, "v_texcoord"

    goto :goto_7

    :cond_d
    iget-object v8, v11, Lbfy;->a:[F

    aget v8, v8, v4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v8, " * texture2D("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ", vec2("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ".x, "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v8, ".y + "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v8, v11, Lbfy;->b:[F

    aget v8, v8, v4

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v8, "))"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_9

    :cond_e
    if-eqz p4, :cond_f

    const-string v3, "precision mediump float;\nuniform sampler2D sampler_source_a;\nuniform sampler2D sampler_source_b;\nuniform float blend_weight;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 base_color = texture2D(sampler_source_b, v_texcoord);\n  vec4 convolved_color = __CONVOLUTION__;\n  gl_FragColor = mix(base_color, convolved_color, blend_weight);\n}\n"

    const-string v4, "__CONVOLUTION__"

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    .line 78
    :goto_a
    invoke-virtual {v6, v2, v3}, Lbgr;->a(ILjava/lang/String;)Lbgx;

    move-result-object v2

    return-object v2

    .line 80
    :cond_f
    const-string v3, "precision mediump float;\nuniform sampler2D sampler_source;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = __CONVOLUTION__;\n}\n"

    const-string v4, "__CONVOLUTION__"

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    goto :goto_a
.end method

.method public static b(Lbgf;IIZ)Lbgx;
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0, p3}, Lbfx;->a(Lbgf;IIZZ)Lbgx;

    move-result-object v0

    return-object v0
.end method
