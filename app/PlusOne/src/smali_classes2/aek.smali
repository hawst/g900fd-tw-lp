.class public final Laek;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/media/MediaPlayer$OnVideoSizeChangedListener;


# instance fields
.field private synthetic a:Landroidx/media/filterpacks/video/MediaPlayerSource;


# direct methods
.method public constructor <init>(Landroidx/media/filterpacks/video/MediaPlayerSource;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Laek;->a:Landroidx/media/filterpacks/video/MediaPlayerSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onVideoSizeChanged(Landroid/media/MediaPlayer;II)V
    .locals 3

    .prologue
    .line 363
    iget-object v0, p0, Laek;->a:Landroidx/media/filterpacks/video/MediaPlayerSource;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "MediaPlayer sent dimensions: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " x "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroidx/media/filterpacks/video/MediaPlayerSource;->a(Landroidx/media/filterpacks/video/MediaPlayerSource;Ljava/lang/String;)V

    .line 364
    iget-object v0, p0, Laek;->a:Landroidx/media/filterpacks/video/MediaPlayerSource;

    invoke-static {v0}, Landroidx/media/filterpacks/video/MediaPlayerSource;->b(Landroidx/media/filterpacks/video/MediaPlayerSource;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 365
    :try_start_0
    iget-object v0, p0, Laek;->a:Landroidx/media/filterpacks/video/MediaPlayerSource;

    invoke-static {v0, p2}, Landroidx/media/filterpacks/video/MediaPlayerSource;->a(Landroidx/media/filterpacks/video/MediaPlayerSource;I)I

    .line 366
    iget-object v0, p0, Laek;->a:Landroidx/media/filterpacks/video/MediaPlayerSource;

    invoke-static {v0, p3}, Landroidx/media/filterpacks/video/MediaPlayerSource;->b(Landroidx/media/filterpacks/video/MediaPlayerSource;I)I

    .line 367
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
