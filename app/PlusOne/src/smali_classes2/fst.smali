.class public final Lfst;
.super Lgbz;
.source "PG"


# instance fields
.field private A:Lfnz;

.field private final y:Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfst;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lfst;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2, p3}, Lgbz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    new-instance v0, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v0, p0, Lfst;->y:Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;

    .line 37
    iget-object v0, p0, Lfst;->y:Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 38
    iget-object v0, p0, Lfst;->y:Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;

    invoke-virtual {p0, v0}, Lfst;->addView(Landroid/view/View;)V

    .line 39
    return-void
.end method


# virtual methods
.method protected a(III)I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 58
    const/high16 v0, -0x80000000

    invoke-static {p3, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 59
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 60
    iget-object v2, p0, Lfst;->y:Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->measure(II)V

    .line 61
    iget-object v0, p0, Lfst;->y:Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->getMeasuredHeight()I

    move-result v0

    .line 62
    iget-object v1, p0, Lfst;->y:Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;

    add-int v2, p1, p3

    add-int v3, p2, v0

    invoke-virtual {v1, p1, p2, v2, v3}, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->layout(IIII)V

    .line 64
    add-int/2addr v0, p2

    return v0
.end method

.method protected a(Landroid/graphics/Canvas;I)I
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lfst;->y:Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->getHeight()I

    move-result v0

    add-int/2addr v0, p2

    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 74
    invoke-super {p0}, Lgbz;->a()V

    .line 75
    iget-object v0, p0, Lfst;->y:Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->a()V

    .line 77
    iget-object v0, p0, Lfst;->y:Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;

    invoke-virtual {p0, v0}, Lfst;->addView(Landroid/view/View;)V

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lfst;->A:Lfnz;

    .line 79
    return-void
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 43
    const/16 v0, 0x17

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 45
    if-eqz v0, :cond_0

    .line 46
    invoke-static {v0}, Lfnz;->a([B)Lfnz;

    move-result-object v0

    iput-object v0, p0, Lfst;->A:Lfnz;

    .line 47
    iget-object v0, p0, Lfst;->y:Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;

    iget-object v1, p0, Lfst;->A:Lfnz;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->a(Lfnz;)V

    .line 49
    :cond_0
    return-void
.end method

.method protected aD_()Z
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 83
    iget-object v0, p0, Lfst;->y:Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;

    if-ne p1, v0, :cond_2

    .line 84
    iget-object v0, p0, Lfst;->A:Lfnz;

    invoke-virtual {v0}, Lfnz;->c()Ljava/lang/String;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lfst;->A:Lfnz;

    invoke-virtual {v1}, Lfnz;->e()Ljava/lang/String;

    move-result-object v1

    .line 86
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lfst;->i:Lfdp;

    if-eqz v2, :cond_1

    .line 87
    iget-object v2, p0, Lfst;->i:Lfdp;

    invoke-virtual {v2, v0, v1}, Lfdp;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    :cond_0
    :goto_0
    return-void

    .line 88
    :cond_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 89
    iget-object v0, p0, Lfst;->i:Lfdp;

    goto :goto_0

    .line 94
    :cond_2
    invoke-super {p0, p1}, Lgbz;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method
