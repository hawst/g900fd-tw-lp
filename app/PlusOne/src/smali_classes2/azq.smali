.class public final Lazq;
.super Layr;
.source "PG"


# static fields
.field private static final a:F


# instance fields
.field private b:Lbal;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 26
    const/high16 v0, 0x3f800000    # 1.0f

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v1, v2

    div-float/2addr v0, v1

    sput v0, Lazq;->a:F

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    .line 31
    new-instance v0, Lays;

    invoke-direct {v0}, Lays;-><init>()V

    const v1, 0x7f02042f

    .line 32
    invoke-virtual {v0, v1}, Lays;->a(I)Lays;

    move-result-object v0

    const-wide/32 v2, 0xc3500

    .line 33
    invoke-virtual {v0, v2, v3}, Lays;->b(J)Lays;

    move-result-object v0

    const v1, 0x7f0205ac

    .line 34
    invoke-virtual {v0, v1}, Lays;->d(I)Lays;

    move-result-object v0

    const v1, 0x3f4ccccd    # 0.8f

    .line 35
    invoke-virtual {v0, v1}, Lays;->b(F)Lays;

    move-result-object v0

    const/4 v1, 0x1

    .line 36
    invoke-virtual {v0, v1}, Lays;->e(I)Lays;

    move-result-object v0

    .line 31
    invoke-direct {p0, v0}, Layr;-><init>(Lays;)V

    .line 38
    new-instance v1, Lazr;

    const-wide/32 v2, 0x61a80

    const/4 v4, -0x1

    const/16 v5, 0x100

    const/16 v6, 0x3d6

    const/4 v7, 0x2

    invoke-direct/range {v1 .. v7}, Lazr;-><init>(JIIII)V

    iput-object v1, p0, Lazq;->b:Lbal;

    .line 48
    return-void
.end method

.method private A(Layl;)Z
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lazq;->b:Lbal;

    invoke-virtual {v0, p1}, Lbal;->a(Layl;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lbag;->e(Layl;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private B(Layl;)F
    .locals 3

    .prologue
    .line 170
    const/high16 v0, 0x3f800000    # 1.0f

    iget-object v1, p0, Lazq;->b:Lbal;

    .line 171
    invoke-virtual {v1, p1}, Lbal;->b(Layl;)F

    move-result v1

    const/high16 v2, 0x3f000000    # 0.5f

    sub-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    return v0
.end method

.method private C(Layl;)Landroid/graphics/Matrix;
    .locals 5

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    .line 184
    iget v0, p1, Layl;->h:I

    iget v1, p1, Layl;->f:I

    mul-int/lit8 v1, v1, 0x7

    add-int/2addr v0, v1

    .line 185
    iget-object v1, p0, Lazq;->b:Lbal;

    invoke-virtual {v1, p1}, Lbal;->c(Layl;)Landroid/graphics/Matrix;

    move-result-object v1

    .line 186
    const/4 v2, 0x0

    const/high16 v3, 0x43b40000    # 360.0f

    invoke-static {v2, v3, v0}, Lbag;->a(FFI)F

    move-result v0

    .line 187
    sget v2, Lazq;->a:F

    sget v3, Lazq;->a:F

    invoke-virtual {v1, v2, v3, v4, v4}, Landroid/graphics/Matrix;->preScale(FFFF)Z

    .line 188
    invoke-virtual {v1, v0, v4, v4}, Landroid/graphics/Matrix;->preRotate(FFF)Z

    .line 189
    return-object v1
.end method


# virtual methods
.method protected a(Layj;Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;
    .locals 2

    .prologue
    .line 145
    new-instance v0, Layu;

    invoke-direct {v0}, Layu;-><init>()V

    .line 146
    invoke-virtual {v0, p1}, Layu;->a(Layj;)Layu;

    move-result-object v0

    const v1, 0x7f0b00ac

    .line 147
    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Layu;->a(I)Layu;

    move-result-object v0

    const/high16 v1, 0x42100000    # 36.0f

    .line 148
    invoke-virtual {v0, v1}, Layu;->a(F)Layu;

    move-result-object v0

    const/high16 v1, 0x42300000    # 44.0f

    .line 149
    invoke-virtual {v0, v1}, Layu;->b(F)Layu;

    move-result-object v0

    const-string v1, "fonts/RobotoSlab-Regular.ttf"

    .line 150
    invoke-static {p2, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Layu;->a(Landroid/graphics/Typeface;)Layu;

    move-result-object v0

    const/4 v1, 0x0

    .line 151
    invoke-virtual {v0, v1}, Layu;->a(Z)Layu;

    move-result-object v0

    const/16 v1, 0x32

    .line 152
    invoke-virtual {v0, v1}, Layu;->c(I)Layu;

    move-result-object v0

    const/4 v1, 0x1

    .line 153
    invoke-virtual {v0, v1}, Layu;->b(Z)Layu;

    move-result-object v0

    const v1, 0x7f0b00b2

    .line 154
    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Layu;->g(I)Layu;

    move-result-object v0

    const v1, 0x7f0b00ad

    .line 155
    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Layu;->h(I)Layu;

    move-result-object v0

    .line 156
    invoke-virtual {v0}, Layu;->a()Layt;

    move-result-object v0

    return-object v0
.end method

.method public f(Layl;)I
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lazq;->A(Layl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    const v0, 0x7f02040b

    .line 63
    :goto_0
    return v0

    :cond_0
    const v0, 0x7f020114

    goto :goto_0
.end method

.method protected f()Layj;
    .locals 1

    .prologue
    .line 161
    new-instance v0, Lazs;

    invoke-direct {v0, p0}, Lazs;-><init>(Layj;)V

    return-object v0
.end method

.method public f_()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 139
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const v2, 0x7f020115

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const v2, 0x7f02040b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public g(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0, p1}, Lazq;->A(Layl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    invoke-direct {p0, p1}, Lazq;->C(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 71
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Layr;->g(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    goto :goto_0
.end method

.method public h(Layl;)F
    .locals 1

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lazq;->A(Layl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    invoke-direct {p0, p1}, Lazq;->B(Layl;)F

    move-result v0

    .line 87
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public i(Layl;)I
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lazq;->A(Layl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    const/4 v0, 0x2

    .line 79
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public k(Layl;)Landroid/graphics/Matrix;
    .locals 5

    .prologue
    .line 127
    iget-wide v0, p1, Layl;->a:J

    long-to-int v0, v0

    invoke-static {v0}, Lbag;->a(I)F

    move-result v0

    const v1, 0x3f2aaaaa

    mul-float/2addr v0, v1

    .line 129
    iget-wide v2, p1, Layl;->a:J

    long-to-int v1, v2

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Lbag;->a(I)F

    move-result v1

    const/high16 v2, 0x3f400000    # 0.75f

    mul-float/2addr v1, v2

    .line 131
    invoke-super {p0, p1}, Layr;->k(Layl;)Landroid/graphics/Matrix;

    move-result-object v2

    .line 132
    const v3, 0x3eaaaaab

    const/high16 v4, 0x3e800000    # 0.25f

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 133
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 134
    return-object v2
.end method

.method public n(Layl;)I
    .locals 2

    .prologue
    .line 100
    iget-object v0, p1, Layl;->i:Lbmg;

    sget-object v1, Lbmg;->c:Lbmg;

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1}, Lazq;->A(Layl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    const v0, 0x7f02040b

    .line 106
    :goto_0
    return v0

    .line 103
    :cond_0
    invoke-direct {p0, p1}, Lazq;->A(Layl;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 104
    const v0, 0x7f020114

    goto :goto_0

    .line 106
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o(Layl;)Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 119
    iget-object v0, p1, Layl;->i:Lbmg;

    sget-object v1, Lbmg;->c:Lbmg;

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1}, Lazq;->A(Layl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    invoke-direct {p0, p1}, Lazq;->C(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 122
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Layr;->o(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    goto :goto_0
.end method

.method public p(Layl;)F
    .locals 2

    .prologue
    .line 92
    iget-object v0, p1, Layl;->i:Lbmg;

    sget-object v1, Lbmg;->c:Lbmg;

    if-ne v0, v1, :cond_0

    invoke-direct {p0, p1}, Lazq;->A(Layl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    invoke-direct {p0, p1}, Lazq;->B(Layl;)F

    move-result v0

    .line 95
    :goto_0
    return v0

    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method public q(Layl;)I
    .locals 2

    .prologue
    .line 111
    iget-object v0, p1, Layl;->i:Lbmg;

    sget-object v1, Lbmg;->c:Lbmg;

    if-ne v0, v1, :cond_0

    .line 112
    const/4 v0, 0x2

    .line 114
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method
