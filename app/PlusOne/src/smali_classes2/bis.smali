.class public Lbis;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbij;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbij",
        "<",
        "Lbmx;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/ContentResolver;

.field private final c:Landroid/net/Uri;

.field private d:J

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lbis;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbis;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;Landroid/net/Uri;Ljfb;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x3e8

    const-wide/16 v6, -0x1

    const/4 v0, -0x1

    const/4 v3, 0x0

    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-wide v6, p0, Lbis;->d:J

    .line 38
    iput v0, p0, Lbis;->e:I

    .line 39
    iput v0, p0, Lbis;->f:I

    .line 40
    iput v0, p0, Lbis;->g:I

    .line 46
    const-string v0, "contentResolver"

    invoke-static {p1, v0, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    iput-object v0, p0, Lbis;->b:Landroid/content/ContentResolver;

    .line 47
    const-string v0, "uri"

    invoke-static {p2, v0, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lbis;->c:Landroid/net/Uri;

    .line 48
    const-string v0, "movieMakerProvider"

    invoke-static {p3, v0, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 51
    invoke-virtual {p3, p2}, Ljfb;->a(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 52
    invoke-virtual {p3, p2}, Ljfb;->f(Landroid/net/Uri;)J

    move-result-wide v0

    .line 53
    cmp-long v2, v0, v6

    if-eqz v2, :cond_0

    .line 54
    mul-long/2addr v0, v8

    iput-wide v0, p0, Lbis;->d:J

    .line 74
    :cond_0
    :goto_0
    iget-wide v0, p0, Lbis;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lbis;->d:J

    cmp-long v0, v0, v6

    if-nez v0, :cond_2

    .line 83
    :cond_1
    invoke-virtual {p2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v0

    const-string v1, "file"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 84
    invoke-virtual {p2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbis;->a(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lbis;->d:J

    .line 91
    :cond_2
    return-void

    .line 57
    :cond_3
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "datetaken"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "orientation"

    aput-object v1, v2, v0

    move-object v0, p1

    move-object v1, p2

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 63
    if-eqz v0, :cond_0

    .line 64
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 65
    const-string v1, "datetaken"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    mul-long/2addr v2, v8

    iput-wide v2, p0, Lbis;->d:J

    .line 67
    const-string v1, "orientation"

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, p0, Lbis;->e:I

    .line 70
    :cond_4
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)J
    .locals 6

    .prologue
    .line 99
    const-wide/16 v0, -0x1

    .line 100
    new-instance v2, Landroid/media/ExifInterface;

    invoke-direct {v2, p0}, Landroid/media/ExifInterface;-><init>(Ljava/lang/String;)V

    .line 101
    const-string v3, "DateTime"

    invoke-virtual {v2, v3}, Landroid/media/ExifInterface;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 107
    if-eqz v2, :cond_0

    .line 108
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyy:MM:dd HH:mm:ss"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v3, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 110
    :try_start_0
    invoke-virtual {v3, v2}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    .line 116
    :cond_0
    :goto_0
    return-wide v0

    .line 113
    :catch_0
    move-exception v2

    sget-object v2, Lbis;->a:Ljava/lang/String;

    const-string v2, "Creation time cannot be parsed For File : "

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_1
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 143
    .line 145
    :try_start_0
    iget-object v1, p0, Lbis;->b:Landroid/content/ContentResolver;

    iget-object v2, p0, Lbis;->c:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 146
    :try_start_1
    new-instance v1, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v1}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 147
    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 148
    const/4 v2, 0x0

    invoke-static {v0, v2, v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 149
    iget v2, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iput v2, p0, Lbis;->f:I

    .line 150
    iget v1, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iput v1, p0, Lbis;->g:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 154
    invoke-static {v0}, Lcek;->a(Ljava/io/Closeable;)V

    .line 155
    :goto_0
    return-void

    .line 152
    :catch_0
    move-exception v1

    :try_start_2
    sget-object v1, Lbis;->a:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 154
    invoke-static {v0}, Lcek;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    :goto_1
    invoke-static {v1}, Lcek;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v3

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 126
    return-void
.end method

.method public b()Lbmx;
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 121
    new-instance v1, Lbmx;

    iget-wide v2, p0, Lbis;->d:J

    iget v4, p0, Lbis;->e:I

    iget v0, p0, Lbis;->f:I

    if-ne v0, v6, :cond_0

    invoke-direct {p0}, Lbis;->c()V

    :cond_0
    iget v5, p0, Lbis;->f:I

    iget v0, p0, Lbis;->g:I

    if-ne v0, v6, :cond_1

    invoke-direct {p0}, Lbis;->c()V

    :cond_1
    iget v6, p0, Lbis;->g:I

    invoke-direct/range {v1 .. v6}, Lbmx;-><init>(JIII)V

    return-object v1
.end method

.method public synthetic i()Lbmu;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lbis;->b()Lbmx;

    move-result-object v0

    return-object v0
.end method
