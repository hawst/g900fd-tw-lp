.class public interface abstract Lgth;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a()Lcom/google/android/gms/maps/model/CameraPosition;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/PolylineOptions;)Lgxc;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/CircleOptions;)Lgxi;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)Lgxl;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lgxr;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/PolygonOptions;)Lgxu;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/TileOverlayOptions;)Lgxx;
.end method

.method public abstract a(I)V
.end method

.method public abstract a(IIII)V
.end method

.method public abstract a(Lgjb;)V
.end method

.method public abstract a(Lgjb;ILguj;)V
.end method

.method public abstract a(Lgjb;Lguj;)V
.end method

.method public abstract a(Lgtk;)V
.end method

.method public abstract a(Lgup;)V
.end method

.method public abstract a(Lgus;)V
.end method

.method public abstract a(Lguv;)V
.end method

.method public abstract a(Lguy;)V
.end method

.method public abstract a(Lgve;)V
.end method

.method public abstract a(Lgvh;)V
.end method

.method public abstract a(Lgvk;)V
.end method

.method public abstract a(Lgvn;)V
.end method

.method public abstract a(Lgvq;)V
.end method

.method public abstract a(Lgvt;)V
.end method

.method public abstract a(Lgvw;)V
.end method

.method public abstract a(Lgwi;Lgjb;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b()F
.end method

.method public abstract b(Lgjb;)V
.end method

.method public abstract b(Z)Z
.end method

.method public abstract c()F
.end method

.method public abstract c(Z)V
.end method

.method public abstract d()V
.end method

.method public abstract d(Z)V
.end method

.method public abstract e()V
.end method

.method public abstract f()I
.end method

.method public abstract g()Z
.end method

.method public abstract h()Z
.end method

.method public abstract i()Z
.end method

.method public abstract j()Landroid/location/Location;
.end method

.method public abstract k()Lguf;
.end method

.method public abstract l()Lgtt;
.end method

.method public abstract m()Z
.end method

.method public abstract n()Lgxo;
.end method
