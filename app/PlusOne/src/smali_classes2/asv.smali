.class public final enum Lasv;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lasv;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lasv;

.field public static final enum b:Lasv;

.field public static final enum c:Lasv;

.field public static final enum d:Lasv;

.field public static final enum e:Lasv;

.field public static final enum f:Lasv;

.field public static final enum g:Lasv;

.field public static final enum h:Lasv;

.field public static final enum i:Lasv;

.field public static final enum j:Lasv;

.field public static final enum k:Lasv;

.field public static final enum l:Lasv;

.field public static final enum m:Lasv;

.field private static final synthetic p:[Lasv;


# instance fields
.field private final n:Z

.field private final o:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 19
    new-instance v0, Lasv;

    const-string v1, "UNACCEPTABLE_URI_PATH"

    invoke-direct {v0, v1, v4, v4, v6}, Lasv;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, Lasv;->a:Lasv;

    .line 20
    new-instance v0, Lasv;

    const-string v1, "DELETED_INPUT"

    invoke-direct {v0, v1, v3, v4, v6}, Lasv;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, Lasv;->b:Lasv;

    .line 22
    new-instance v0, Lasv;

    const-string v1, "ACCEPTABLE_LANDSCAPE_VIDEO"

    invoke-direct {v0, v1, v5, v3, v3}, Lasv;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, Lasv;->c:Lasv;

    .line 23
    new-instance v0, Lasv;

    const-string v1, "ACCEPTABLE_PORTRAIT_VIDEO"

    invoke-direct {v0, v1, v6, v3, v3}, Lasv;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, Lasv;->d:Lasv;

    .line 24
    new-instance v0, Lasv;

    const-string v1, "SHORT_VIDEO"

    invoke-direct {v0, v1, v7, v4, v3}, Lasv;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, Lasv;->e:Lasv;

    .line 25
    new-instance v0, Lasv;

    const-string v1, "LONG_VIDEO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v4, v3}, Lasv;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, Lasv;->f:Lasv;

    .line 26
    new-instance v0, Lasv;

    const-string v1, "TALL_VIDEO"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v4, v3}, Lasv;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, Lasv;->g:Lasv;

    .line 27
    new-instance v0, Lasv;

    const-string v1, "UNKNOWN_LENGTH_VIDEO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v4, v3}, Lasv;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, Lasv;->h:Lasv;

    .line 28
    new-instance v0, Lasv;

    const-string v1, "UNSUPPORTED_VIDEO_CONTAINER"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v4, v3}, Lasv;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, Lasv;->i:Lasv;

    .line 29
    new-instance v0, Lasv;

    const-string v1, "UNSUPPORTED_VIDEO_CODEC"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v4, v3}, Lasv;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, Lasv;->j:Lasv;

    .line 30
    new-instance v0, Lasv;

    const-string v1, "BAD_VIDEO_FILE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v4, v3}, Lasv;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, Lasv;->k:Lasv;

    .line 32
    new-instance v0, Lasv;

    const-string v1, "ACCEPTABLE_PHOTO"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v3, v5}, Lasv;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, Lasv;->l:Lasv;

    .line 33
    new-instance v0, Lasv;

    const-string v1, "BAD_PHOTO_FILE"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v4, v5}, Lasv;-><init>(Ljava/lang/String;IZI)V

    sput-object v0, Lasv;->m:Lasv;

    .line 16
    const/16 v0, 0xd

    new-array v0, v0, [Lasv;

    sget-object v1, Lasv;->a:Lasv;

    aput-object v1, v0, v4

    sget-object v1, Lasv;->b:Lasv;

    aput-object v1, v0, v3

    sget-object v1, Lasv;->c:Lasv;

    aput-object v1, v0, v5

    sget-object v1, Lasv;->d:Lasv;

    aput-object v1, v0, v6

    sget-object v1, Lasv;->e:Lasv;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lasv;->f:Lasv;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lasv;->g:Lasv;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lasv;->h:Lasv;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lasv;->i:Lasv;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lasv;->j:Lasv;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lasv;->k:Lasv;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lasv;->l:Lasv;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lasv;->m:Lasv;

    aput-object v2, v0, v1

    sput-object v0, Lasv;->p:[Lasv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZI)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZI)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 41
    iput-boolean p3, p0, Lasv;->n:Z

    .line 42
    iput p4, p0, Lasv;->o:I

    .line 43
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lasv;
    .locals 1

    .prologue
    .line 16
    const-class v0, Lasv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lasv;

    return-object v0
.end method

.method public static values()[Lasv;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lasv;->p:[Lasv;

    invoke-virtual {v0}, [Lasv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lasv;

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lasv;->n:Z

    return v0
.end method

.method public b()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 50
    iget v1, p0, Lasv;->o:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 54
    iget v0, p0, Lasv;->o:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
