.class final Lhfe;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhej;


# instance fields
.field private final a:Ljava/lang/String;

.field private synthetic b:Lhev;


# direct methods
.method constructor <init>(Lhev;I)V
    .locals 1

    .prologue
    .line 745
    invoke-static {p2}, Lhev;->g(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lhfe;-><init>(Lhev;Ljava/lang/String;)V

    .line 746
    return-void
.end method

.method constructor <init>(Lhev;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 740
    iput-object p1, p0, Lhfe;->b:Lhev;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 741
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhfe;->a:Ljava/lang/String;

    .line 742
    return-void
.end method

.method private e(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 805
    iget-object v0, p0, Lhfe;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 778
    invoke-direct {p0, p1}, Lhfe;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 779
    iget-object v1, p0, Lhfe;->b:Lhev;

    invoke-static {v1}, Lhev;->b(Lhev;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, v0, p2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 784
    invoke-direct {p0, p1}, Lhfe;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 785
    iget-object v1, p0, Lhfe;->b:Lhev;

    invoke-static {v1}, Lhev;->b(Lhev;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, v0, p2, p3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 756
    invoke-direct {p0, p1}, Lhfe;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 757
    iget-object v1, p0, Lhfe;->b:Lhev;

    invoke-static {v1}, Lhev;->b(Lhev;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, v0, p2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 796
    const-string v0, "logged_in"

    invoke-virtual {p0, v0}, Lhfe;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 750
    invoke-direct {p0, p1}, Lhfe;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 751
    iget-object v1, p0, Lhfe;->b:Lhev;

    invoke-static {v1}, Lhev;->b(Lhev;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;Z)Z
    .locals 2

    .prologue
    .line 767
    invoke-direct {p0, p1}, Lhfe;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 768
    iget-object v1, p0, Lhfe;->b:Lhev;

    invoke-static {v1}, Lhev;->b(Lhev;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1, v0, p2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 762
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lhfe;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 773
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lhfe;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public d(Ljava/lang/String;)Lhej;
    .locals 5

    .prologue
    .line 801
    new-instance v1, Lhfe;

    iget-object v2, p0, Lhfe;->b:Lhev;

    iget-object v0, p0, Lhfe;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v2, v0}, Lhfe;-><init>(Lhev;Ljava/lang/String;)V

    return-object v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method
