.class public final Lbzs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbzp;


# instance fields
.field private final a:Lbzn;


# direct methods
.method constructor <init>(Lama;Lbza;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Lbzn;

    invoke-direct {v0, p1, p2}, Lbzn;-><init>(Lama;Lbza;)V

    iput-object v0, p0, Lbzs;->a:Lbzn;

    .line 49
    return-void
.end method

.method private a(Ljava/util/Map;Ljava/util/List;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcac;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 110
    const-string v0, "displayableByMediaId"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 111
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v5

    .line 128
    :cond_0
    return-object v5

    .line 114
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v1, v2

    .line 116
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_0

    .line 117
    add-int/lit8 v0, v1, 0x1

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    add-int/lit8 v0, v1, 0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-boolean v0, v0, Lbmd;->j:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    .line 118
    :goto_1
    if-eqz v0, :cond_3

    .line 119
    invoke-static {}, Lcaa;->a()Lcaa;

    move-result-object v0

    move v3, v1

    move-object v4, v5

    .line 123
    :goto_2
    invoke-interface {v4, v3, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 116
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v2

    .line 117
    goto :goto_1

    .line 121
    :cond_3
    invoke-static {p2, v1}, Lbyi;->a(Ljava/util/List;I)J

    move-result-wide v6

    .line 123
    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-nez v0, :cond_4

    .line 124
    invoke-static {}, Lcaa;->a()Lcaa;

    move-result-object v0

    move v3, v1

    move-object v4, v5

    goto :goto_2

    :cond_4
    new-instance v0, Lcab;

    invoke-direct {v0, v6, v7}, Lcab;-><init>(J)V

    move v3, v1

    move-object v4, v5

    goto :goto_2
.end method


# virtual methods
.method public a(Ljava/util/Map;Ljava/util/Map;Ljava/util/List;ZLbmd;Lbmd;)Lboj;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbmb;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;Z",
            "Lbmd;",
            "Lbmd;",
            ")",
            "Lboj;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 61
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, -0x1

    move v1, v2

    move v3, v2

    move v4, v0

    :goto_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    invoke-virtual {v0}, Lbmd;->a()Lbmf;

    move-result-object v7

    invoke-virtual {v0}, Lbmd;->c()Look;

    move-result-object v8

    if-eqz v8, :cond_1

    iget-object v0, v8, Look;->g:Looh;

    if-eqz v0, :cond_1

    iget-object v0, v8, Look;->g:Looh;

    iget-object v0, v0, Looh;->c:[Looi;

    iget-object v5, v8, Look;->g:Looh;

    iget-object v5, v5, Looh;->b:Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    array-length v0, v0

    invoke-virtual {v7, v0}, Lbmf;->c(I)Lbmf;

    if-ne v4, v5, :cond_2

    const/4 v0, 0x2

    if-eq v3, v0, :cond_0

    const/4 v0, 0x3

    if-ne v3, v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v7, v0}, Lbmf;->c(Z)Lbmf;

    if-eq v4, v5, :cond_1

    iget-object v0, v8, Look;->g:Looh;

    iget v3, v0, Looh;->a:I

    move v4, v5

    :cond_1
    invoke-virtual {v7}, Lbmf;->a()Lbmd;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    .line 62
    :cond_3
    iget-object v0, p0, Lbzs;->a:Lbzn;

    invoke-virtual {v0, v6, p4, p5, p6}, Lbzn;->a(Ljava/util/List;ZLbmd;Lbmd;)Ljava/util/List;

    move-result-object v0

    .line 65
    invoke-direct {p0, p1, v0}, Lbzs;->a(Ljava/util/Map;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 67
    iget-object v2, p0, Lbzs;->a:Lbzn;

    invoke-virtual {v2, p1, p2, v0, v1}, Lbzn;->a(Ljava/util/Map;Ljava/util/Map;Ljava/util/List;Ljava/util/List;)Lboj;

    move-result-object v0

    .line 69
    const/16 v1, 0x500

    const/16 v2, 0x2d0

    invoke-virtual {v0, v1, v2}, Lboj;->a(II)Lboj;

    .line 70
    return-object v0
.end method
