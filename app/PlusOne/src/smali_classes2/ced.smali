.class public final Lced;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Lcef;

.field private final b:Landroid/media/AudioManager;

.field private final c:Landroid/media/AudioManager$OnAudioFocusChangeListener;


# direct methods
.method public constructor <init>(Landroid/media/AudioManager;)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Lcee;

    invoke-direct {v0, p0}, Lcee;-><init>(Lced;)V

    iput-object v0, p0, Lced;->c:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 46
    const-string v0, "audioManager"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lced;->b:Landroid/media/AudioManager;

    .line 47
    return-void
.end method


# virtual methods
.method public a(Lcef;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 56
    const-string v1, "listener"

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 57
    iget-object v1, p0, Lced;->a:Lcef;

    const-string v2, "mClientListener"

    const-string v3, "audio focus already requested"

    invoke-static {v1, v2, v3}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 58
    iget-object v1, p0, Lced;->b:Landroid/media/AudioManager;

    iget-object v2, p0, Lced;->c:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3, v0}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v1

    .line 60
    if-ne v1, v0, :cond_0

    .line 61
    iput-object p1, p0, Lced;->a:Lcef;

    .line 64
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcef;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 74
    iget-object v0, p0, Lced;->a:Lcef;

    const-string v1, "requestFocus must be called before calling abandonFocus()"

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 76
    const-string v0, "listener"

    iget-object v1, p0, Lced;->a:Lcef;

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    .line 77
    iput-object v2, p0, Lced;->a:Lcef;

    .line 78
    iget-object v0, p0, Lced;->b:Landroid/media/AudioManager;

    iget-object v1, p0, Lced;->c:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 79
    return-void
.end method
