.class final Lcmf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:F

.field final b:F

.field final c:F

.field final d:J

.field final e:J

.field private f:Landroid/animation/TimeInterpolator;

.field private g:Landroid/graphics/Path;

.field private h:Landroid/graphics/PathMeasure;

.field private i:F

.field private j:F

.field private k:J


# direct methods
.method constructor <init>(Landroid/animation/TimeInterpolator;Landroid/graphics/Path;JJFFF)V
    .locals 3

    .prologue
    .line 150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 151
    const-wide/16 v0, 0x0

    cmp-long v0, p5, v0

    if-gtz v0, :cond_0

    .line 152
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Duration must be a positive value."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154
    :cond_0
    iput-object p1, p0, Lcmf;->f:Landroid/animation/TimeInterpolator;

    .line 155
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0, p2}, Landroid/graphics/Path;-><init>(Landroid/graphics/Path;)V

    iput-object v0, p0, Lcmf;->g:Landroid/graphics/Path;

    .line 156
    new-instance v0, Landroid/graphics/PathMeasure;

    iget-object v1, p0, Lcmf;->g:Landroid/graphics/Path;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/graphics/PathMeasure;-><init>(Landroid/graphics/Path;Z)V

    iput-object v0, p0, Lcmf;->h:Landroid/graphics/PathMeasure;

    .line 157
    iget-object v0, p0, Lcmf;->h:Landroid/graphics/PathMeasure;

    invoke-virtual {v0}, Landroid/graphics/PathMeasure;->getLength()F

    move-result v0

    iput v0, p0, Lcmf;->a:F

    .line 159
    iput p9, p0, Lcmf;->c:F

    .line 160
    iput p7, p0, Lcmf;->i:F

    .line 161
    iput p8, p0, Lcmf;->b:F

    .line 162
    sub-float v0, p8, p7

    iput v0, p0, Lcmf;->j:F

    .line 164
    iput-wide p3, p0, Lcmf;->d:J

    .line 165
    iput-wide p5, p0, Lcmf;->k:J

    .line 166
    add-long v0, p3, p5

    iput-wide v0, p0, Lcmf;->e:J

    .line 167
    return-void
.end method


# virtual methods
.method a(F)Landroid/graphics/PointF;
    .locals 4

    .prologue
    .line 211
    const/4 v0, 0x2

    new-array v0, v0, [F

    .line 212
    iget-object v1, p0, Lcmf;->h:Landroid/graphics/PathMeasure;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v0, v2}, Landroid/graphics/PathMeasure;->getPosTan(F[F[F)Z

    .line 213
    new-instance v1, Landroid/graphics/PointF;

    const/4 v2, 0x0

    aget v2, v0, v2

    const/4 v3, 0x1

    aget v0, v0, v3

    invoke-direct {v1, v2, v0}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v1
.end method

.method a(J)Lcmf;
    .locals 11

    .prologue
    .line 170
    new-instance v1, Lcmf;

    iget-object v2, p0, Lcmf;->f:Landroid/animation/TimeInterpolator;

    iget-object v3, p0, Lcmf;->g:Landroid/graphics/Path;

    iget-wide v4, p0, Lcmf;->d:J

    add-long/2addr v4, p1

    iget-wide v6, p0, Lcmf;->k:J

    iget v8, p0, Lcmf;->i:F

    iget v9, p0, Lcmf;->b:F

    iget v10, p0, Lcmf;->c:F

    invoke-direct/range {v1 .. v10}, Lcmf;-><init>(Landroid/animation/TimeInterpolator;Landroid/graphics/Path;JJFFF)V

    return-object v1
.end method

.method b(J)Landroid/view/MotionEvent$PointerCoords;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 183
    iget-wide v2, p0, Lcmf;->d:J

    cmp-long v0, p1, v2

    if-gtz v0, :cond_0

    move v0, v1

    .line 194
    :goto_0
    iget v2, p0, Lcmf;->a:F

    mul-float/2addr v2, v0

    iget v3, p0, Lcmf;->a:F

    invoke-static {v2, v1, v3}, Lhcg;->a(FFF)F

    move-result v1

    .line 195
    invoke-virtual {p0, v1}, Lcmf;->a(F)Landroid/graphics/PointF;

    move-result-object v1

    .line 198
    iget v2, p0, Lcmf;->i:F

    iget v3, p0, Lcmf;->j:F

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    .line 201
    new-instance v2, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v2}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    .line 202
    iget v3, v1, Landroid/graphics/PointF;->x:F

    iput v3, v2, Landroid/view/MotionEvent$PointerCoords;->x:F

    .line 203
    iget v1, v1, Landroid/graphics/PointF;->y:F

    iput v1, v2, Landroid/view/MotionEvent$PointerCoords;->y:F

    .line 204
    iput v0, v2, Landroid/view/MotionEvent$PointerCoords;->touchMinor:F

    .line 205
    iput v0, v2, Landroid/view/MotionEvent$PointerCoords;->touchMajor:F

    .line 206
    iget v1, p0, Lcmf;->c:F

    div-float/2addr v0, v1

    iput v0, v2, Landroid/view/MotionEvent$PointerCoords;->pressure:F

    .line 207
    return-object v2

    .line 185
    :cond_0
    iget-wide v2, p0, Lcmf;->e:J

    cmp-long v0, p1, v2

    if-ltz v0, :cond_1

    .line 186
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    .line 188
    :cond_1
    iget-wide v2, p0, Lcmf;->d:J

    sub-long v2, p1, v2

    long-to-float v0, v2

    .line 189
    iget-wide v2, p0, Lcmf;->k:J

    long-to-float v2, v2

    div-float/2addr v0, v2

    .line 190
    iget-object v2, p0, Lcmf;->f:Landroid/animation/TimeInterpolator;

    invoke-interface {v2, v0}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 218
    if-ne p0, p1, :cond_1

    .line 227
    :cond_0
    :goto_0
    return v0

    .line 221
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 222
    goto :goto_0

    .line 225
    :cond_3
    check-cast p1, Lcmf;

    .line 227
    iget v2, p0, Lcmf;->c:F

    iget v3, p1, Lcmf;->c:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget v2, p0, Lcmf;->b:F

    iget v3, p1, Lcmf;->b:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget v2, p0, Lcmf;->i:F

    iget v3, p1, Lcmf;->i:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget v2, p0, Lcmf;->a:F

    iget v3, p1, Lcmf;->a:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_4

    iget-wide v2, p0, Lcmf;->d:J

    iget-wide v4, p1, Lcmf;->d:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_4

    iget-wide v2, p0, Lcmf;->e:J

    iget-wide v4, p1, Lcmf;->e:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 237
    iget v0, p0, Lcmf;->a:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_1

    iget v0, p0, Lcmf;->a:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    .line 238
    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget v0, p0, Lcmf;->i:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_2

    iget v0, p0, Lcmf;->i:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    .line 239
    mul-int/lit8 v2, v0, 0x1f

    iget v0, p0, Lcmf;->b:F

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_3

    iget v0, p0, Lcmf;->b:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    .line 240
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcmf;->c:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    iget v1, p0, Lcmf;->c:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 241
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcmf;->d:J

    iget-wide v4, p0, Lcmf;->d:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 242
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcmf;->e:J

    iget-wide v4, p0, Lcmf;->e:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 243
    return v0

    :cond_1
    move v0, v1

    .line 237
    goto :goto_0

    :cond_2
    move v0, v1

    .line 238
    goto :goto_1

    :cond_3
    move v0, v1

    .line 239
    goto :goto_2
.end method
