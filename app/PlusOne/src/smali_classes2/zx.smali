.class abstract Lzx;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field public c:Laaf;

.field public d:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Laai;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljavax/microedition/khronos/egl/EGLContext;",
            "Lacm;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljavax/microedition/khronos/egl/EGLContext;",
            "Lada;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljavax/microedition/khronos/egl/EGLContext;",
            "Ljava/util/Set",
            "<",
            "Laai;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lzx;->e:Ljava/util/HashMap;

    .line 89
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lzx;->f:Ljava/util/HashMap;

    .line 93
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lzx;->g:Ljava/util/HashMap;

    .line 97
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Lzx;->d:Ljava/util/Vector;

    return-void
.end method


# virtual methods
.method protected a(Ljavax/microedition/khronos/egl/EGLContext;)Ljava/util/Set;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljavax/microedition/khronos/egl/EGLContext;",
            ")",
            "Ljava/util/Set",
            "<",
            "Laai;",
            ">;"
        }
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lzx;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 143
    if-nez v0, :cond_0

    .line 144
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 145
    iget-object v1, p0, Lzx;->g:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 147
    :cond_0
    return-object v0
.end method

.method public abstract a()V
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 104
    iput p1, p0, Lzx;->a:I

    .line 105
    iput p2, p0, Lzx;->b:I

    .line 106
    return-void
.end method

.method public a(Laaf;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lzx;->c:Laaf;

    .line 101
    return-void
.end method

.method public a(Laai;)V
    .locals 2

    .prologue
    .line 109
    invoke-static {}, Lacw;->h()Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v0

    .line 110
    invoke-virtual {p0, v0}, Lzx;->a(Ljavax/microedition/khronos/egl/EGLContext;)Ljava/util/Set;

    move-result-object v1

    .line 111
    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 112
    iget-object v1, p0, Lzx;->d:Ljava/util/Vector;

    invoke-virtual {v1, p1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 113
    invoke-virtual {p0, v0}, Lzx;->b(Ljavax/microedition/khronos/egl/EGLContext;)V

    .line 114
    return-void
.end method

.method public abstract a(Laas;)V
.end method

.method public abstract b()V
.end method

.method public b(Laai;)V
    .locals 2

    .prologue
    .line 117
    invoke-static {}, Lacw;->h()Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v0

    .line 118
    invoke-virtual {p0, v0}, Lzx;->a(Ljavax/microedition/khronos/egl/EGLContext;)Ljava/util/Set;

    move-result-object v1

    .line 119
    invoke-interface {v1, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 120
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 121
    invoke-virtual {p0, v0}, Lzx;->c(Ljavax/microedition/khronos/egl/EGLContext;)V

    .line 123
    :cond_0
    iget-object v0, p0, Lzx;->d:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->remove(Ljava/lang/Object;)Z

    .line 124
    return-void
.end method

.method protected b(Ljavax/microedition/khronos/egl/EGLContext;)V
    .locals 0

    .prologue
    .line 151
    return-void
.end method

.method public abstract c()V
.end method

.method protected c(Ljavax/microedition/khronos/egl/EGLContext;)V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lzx;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lada;

    .line 155
    iget-object v1, p0, Lzx;->e:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lacm;

    .line 156
    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {v0}, Lada;->g()V

    .line 158
    iget-object v0, p0, Lzx;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 160
    :cond_0
    if-eqz v1, :cond_1

    .line 161
    iget-object v0, p0, Lzx;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    :cond_1
    return-void
.end method

.method protected d()Lacm;
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x0

    return-object v0
.end method

.method protected d(Ljavax/microedition/khronos/egl/EGLContext;)Lada;
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lzx;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lada;

    .line 167
    if-nez v0, :cond_0

    .line 168
    invoke-virtual {p0}, Lzx;->e()Lada;

    move-result-object v0

    .line 169
    iget-object v1, p0, Lzx;->f:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 171
    :cond_0
    return-object v0
.end method

.method protected e(Ljavax/microedition/khronos/egl/EGLContext;)Lacm;
    .locals 2

    .prologue
    .line 175
    iget-object v0, p0, Lzx;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacm;

    .line 176
    if-nez v0, :cond_0

    .line 177
    invoke-virtual {p0}, Lzx;->d()Lacm;

    move-result-object v0

    .line 178
    iget-object v1, p0, Lzx;->e:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    :cond_0
    return-object v0
.end method

.method protected e()Lada;
    .locals 1

    .prologue
    .line 188
    const/4 v0, 0x0

    return-object v0
.end method
