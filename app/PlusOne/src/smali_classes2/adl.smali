.class abstract Ladl;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final a:I

.field private final b:Landroid/media/MediaFormat;

.field private final c:Ladm;

.field private final d:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private e:Landroid/media/MediaCodec;

.field private f:Landroid/media/MediaFormat;

.field private g:[Ljava/nio/ByteBuffer;

.field private h:[Ljava/nio/ByteBuffer;

.field private i:Z


# direct methods
.method protected constructor <init>(ILandroid/media/MediaFormat;Ladm;)V
    .locals 2

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput p1, p0, Ladl;->a:I

    .line 76
    if-nez p2, :cond_0

    .line 77
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "mediaFormat cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_0
    iput-object p2, p0, Ladl;->b:Landroid/media/MediaFormat;

    .line 81
    if-nez p3, :cond_1

    .line 82
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 84
    :cond_1
    iput-object p3, p0, Ladl;->c:Ladm;

    .line 86
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Ladl;->d:Ljava/util/Queue;

    .line 87
    return-void
.end method

.method private a()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 156
    iget-object v0, p0, Ladl;->e:Landroid/media/MediaCodec;

    const-wide/16 v4, 0x32

    invoke-virtual {v0, v4, v5}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v1

    .line 160
    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    .line 161
    iget-object v0, p0, Ladl;->e:Landroid/media/MediaCodec;

    const-wide/16 v4, 0x0

    const/4 v6, 0x4

    move v3, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 163
    iput-boolean v2, p0, Ladl;->i:Z

    .line 165
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/media/MediaFormat;)Landroid/media/MediaCodec;
.end method

.method protected abstract a(Landroid/media/MediaCodec;[Ljava/nio/ByteBuffer;ILandroid/media/MediaCodec$BufferInfo;Z)Z
.end method

.method public a(Landroid/media/MediaExtractor;)Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 118
    iget-object v0, p0, Ladl;->e:Landroid/media/MediaCodec;

    const-wide/16 v4, 0x32

    invoke-virtual {v0, v4, v5}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v1

    .line 121
    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    .line 122
    iget-object v0, p0, Ladl;->g:[Ljava/nio/ByteBuffer;

    aget-object v0, v0, v1

    .line 123
    invoke-virtual {p1, v0, v2}, Landroid/media/MediaExtractor;->readSampleData(Ljava/nio/ByteBuffer;I)I

    move-result v3

    .line 125
    if-gez v3, :cond_1

    .line 126
    iget-object v0, p0, Ladl;->e:Landroid/media/MediaCodec;

    const-wide/16 v4, 0x0

    const/4 v6, 0x4

    move v3, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 152
    :cond_0
    :goto_0
    return v2

    .line 134
    :cond_1
    invoke-virtual {p1}, Landroid/media/MediaExtractor;->getSampleTime()J

    move-result-wide v4

    .line 136
    invoke-virtual {p1}, Landroid/media/MediaExtractor;->getSampleFlags()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    .line 138
    iget-object v0, p0, Ladl;->d:Ljava/util/Queue;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v0, v6}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    move v6, v7

    .line 142
    :goto_1
    iget-object v0, p0, Ladl;->e:Landroid/media/MediaCodec;

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 149
    invoke-virtual {p1}, Landroid/media/MediaExtractor;->advance()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/media/MediaExtractor;->getSampleTrackIndex()I

    move-result v0

    iget v1, p0, Ladl;->a:I

    if-ne v0, v1, :cond_0

    move v2, v7

    goto :goto_0

    :cond_2
    move v6, v2

    .line 140
    goto :goto_1
.end method

.method public b()Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 168
    new-instance v4, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v4}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    .line 169
    iget-object v0, p0, Ladl;->e:Landroid/media/MediaCodec;

    const-wide/16 v6, 0x32

    invoke-virtual {v0, v4, v6, v7}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I

    move-result v3

    .line 171
    iget v0, v4, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_1

    .line 172
    iget-object v0, p0, Ladl;->c:Ladm;

    invoke-interface {v0, p0}, Ladm;->b(Ladl;)V

    .line 202
    :cond_0
    :goto_0
    return v2

    .line 175
    :cond_1
    iget-boolean v0, p0, Ladl;->i:Z

    if-eqz v0, :cond_2

    .line 176
    invoke-direct {p0}, Ladl;->a()V

    .line 178
    :cond_2
    if-ltz v3, :cond_6

    .line 180
    iget-object v0, p0, Ladl;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 181
    iget-object v0, p0, Ladl;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 182
    iget-wide v8, v4, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    cmp-long v0, v8, v6

    if-nez v0, :cond_4

    move v0, v1

    .line 183
    :goto_1
    iget-wide v8, v4, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    cmp-long v1, v8, v6

    if-ltz v1, :cond_3

    .line 186
    iget-object v1, p0, Ladl;->d:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    :cond_3
    move v5, v0

    .line 192
    :goto_2
    iget-object v1, p0, Ladl;->e:Landroid/media/MediaCodec;

    iget-object v2, p0, Ladl;->h:[Ljava/nio/ByteBuffer;

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Ladl;->a(Landroid/media/MediaCodec;[Ljava/nio/ByteBuffer;ILandroid/media/MediaCodec$BufferInfo;Z)Z

    move-result v2

    goto :goto_0

    :cond_4
    move v0, v2

    .line 182
    goto :goto_1

    :cond_5
    move v5, v2

    .line 189
    goto :goto_2

    .line 194
    :cond_6
    const/4 v0, -0x3

    if-ne v3, v0, :cond_7

    .line 195
    iget-object v0, p0, Ladl;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Ladl;->h:[Ljava/nio/ByteBuffer;

    move v2, v1

    .line 196
    goto :goto_0

    .line 197
    :cond_7
    const/4 v0, -0x2

    if-ne v3, v0, :cond_0

    .line 198
    iget-object v0, p0, Ladl;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v0

    iput-object v0, p0, Ladl;->f:Landroid/media/MediaFormat;

    .line 199
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Output format has changed to "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Ladl;->f:Landroid/media/MediaFormat;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move v2, v1

    .line 200
    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Ladl;->e:Landroid/media/MediaCodec;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, p0, Ladl;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V

    .line 105
    iget-object v0, p0, Ladl;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V

    .line 107
    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x1

    iput-boolean v0, p0, Ladl;->i:Z

    .line 99
    invoke-direct {p0}, Ladl;->a()V

    .line 100
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Ladl;->b:Landroid/media/MediaFormat;

    invoke-virtual {p0, v0}, Ladl;->a(Landroid/media/MediaFormat;)Landroid/media/MediaCodec;

    move-result-object v0

    iput-object v0, p0, Ladl;->e:Landroid/media/MediaCodec;

    .line 91
    iget-object v0, p0, Ladl;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->start()V

    .line 92
    iget-object v0, p0, Ladl;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Ladl;->g:[Ljava/nio/ByteBuffer;

    .line 93
    iget-object v0, p0, Ladl;->e:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Ladl;->h:[Ljava/nio/ByteBuffer;

    .line 94
    iget-object v0, p0, Ladl;->d:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 95
    return-void
.end method

.method protected f()Landroid/media/MediaCodec;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Ladl;->e:Landroid/media/MediaCodec;

    return-object v0
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Ladl;->c:Ladm;

    invoke-interface {v0, p0}, Ladm;->a(Ladl;)V

    .line 115
    return-void
.end method
