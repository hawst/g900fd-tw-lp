.class public Lgkr;
.super Lgka;


# static fields
.field private static final h:Ljava/lang/String;

.field private static final i:J

.field private static final j:J

.field private static final k:J

.field private static final l:J


# instance fields
.field public d:Lgef;

.field public final e:Lgkv;

.field public final f:Lgkv;

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lgkv;",
            ">;"
        }
    .end annotation
.end field

.field private m:J

.field private final n:Landroid/os/Handler;

.field private final o:Lgkv;

.field private final p:Lgkv;

.field private final q:Lgkv;

.field private final r:Lgkv;

.field private final s:Lgkv;

.field private final t:Lgkv;

.field private final u:Lgkv;

.field private final v:Lgkv;

.field private final w:Ljava/lang/Runnable;

.field private x:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const-wide/16 v4, 0x18

    const-string v0, "com.google.cast.media"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "urn:x-cast:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lgkr;->h:Ljava/lang/String;

    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lgkr;->i:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lgkr;->j:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lgkr;->k:J

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lgkr;->l:J

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgkr;-><init>(Ljava/lang/String;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    sget-object v0, Lgkr;->h:Ljava/lang/String;

    const-string v1, "MediaControlChannel"

    invoke-direct {p0, v0, v1, p1}, Lgka;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lgkr;->n:Landroid/os/Handler;

    new-instance v0, Lgks;

    invoke-direct {v0, p0}, Lgks;-><init>(Lgkr;)V

    iput-object v0, p0, Lgkr;->w:Ljava/lang/Runnable;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgkr;->g:Ljava/util/List;

    new-instance v0, Lgkv;

    sget-wide v2, Lgkr;->j:J

    invoke-direct {v0, v2, v3}, Lgkv;-><init>(J)V

    iput-object v0, p0, Lgkr;->e:Lgkv;

    iget-object v0, p0, Lgkr;->g:Ljava/util/List;

    iget-object v1, p0, Lgkr;->e:Lgkv;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lgkv;

    sget-wide v2, Lgkr;->i:J

    invoke-direct {v0, v2, v3}, Lgkv;-><init>(J)V

    iput-object v0, p0, Lgkr;->o:Lgkv;

    iget-object v0, p0, Lgkr;->g:Ljava/util/List;

    iget-object v1, p0, Lgkr;->o:Lgkv;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lgkv;

    sget-wide v2, Lgkr;->i:J

    invoke-direct {v0, v2, v3}, Lgkv;-><init>(J)V

    iput-object v0, p0, Lgkr;->p:Lgkv;

    iget-object v0, p0, Lgkr;->g:Ljava/util/List;

    iget-object v1, p0, Lgkr;->p:Lgkv;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lgkv;

    sget-wide v2, Lgkr;->i:J

    invoke-direct {v0, v2, v3}, Lgkv;-><init>(J)V

    iput-object v0, p0, Lgkr;->q:Lgkv;

    iget-object v0, p0, Lgkr;->g:Ljava/util/List;

    iget-object v1, p0, Lgkr;->q:Lgkv;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lgkv;

    sget-wide v2, Lgkr;->k:J

    invoke-direct {v0, v2, v3}, Lgkv;-><init>(J)V

    iput-object v0, p0, Lgkr;->r:Lgkv;

    iget-object v0, p0, Lgkr;->g:Ljava/util/List;

    iget-object v1, p0, Lgkr;->r:Lgkv;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lgkv;

    sget-wide v2, Lgkr;->i:J

    invoke-direct {v0, v2, v3}, Lgkv;-><init>(J)V

    iput-object v0, p0, Lgkr;->s:Lgkv;

    iget-object v0, p0, Lgkr;->g:Ljava/util/List;

    iget-object v1, p0, Lgkr;->s:Lgkv;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lgkv;

    sget-wide v2, Lgkr;->i:J

    invoke-direct {v0, v2, v3}, Lgkv;-><init>(J)V

    iput-object v0, p0, Lgkr;->t:Lgkv;

    iget-object v0, p0, Lgkr;->g:Ljava/util/List;

    iget-object v1, p0, Lgkr;->t:Lgkv;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lgkv;

    sget-wide v2, Lgkr;->i:J

    invoke-direct {v0, v2, v3}, Lgkv;-><init>(J)V

    iput-object v0, p0, Lgkr;->f:Lgkv;

    iget-object v0, p0, Lgkr;->g:Ljava/util/List;

    iget-object v1, p0, Lgkr;->f:Lgkv;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lgkv;

    sget-wide v2, Lgkr;->i:J

    invoke-direct {v0, v2, v3}, Lgkv;-><init>(J)V

    iput-object v0, p0, Lgkr;->u:Lgkv;

    iget-object v0, p0, Lgkr;->g:Ljava/util/List;

    iget-object v1, p0, Lgkr;->u:Lgkv;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lgkv;

    sget-wide v2, Lgkr;->i:J

    invoke-direct {v0, v2, v3}, Lgkv;-><init>(J)V

    iput-object v0, p0, Lgkr;->v:Lgkv;

    iget-object v0, p0, Lgkr;->g:Ljava/util/List;

    iget-object v1, p0, Lgkr;->v:Lgkv;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lgkr;->j()V

    return-void
.end method

.method static synthetic a(Lgkr;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lgkr;->g:Ljava/util/List;

    return-object v0
.end method

.method private a(Z)V
    .locals 4

    iget-boolean v0, p0, Lgkr;->x:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lgkr;->x:Z

    if-eqz p1, :cond_1

    iget-object v0, p0, Lgkr;->n:Landroid/os/Handler;

    iget-object v1, p0, Lgkr;->w:Ljava/lang/Runnable;

    sget-wide v2, Lgkr;->l:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lgkr;->n:Landroid/os/Handler;

    iget-object v1, p0, Lgkr;->w:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic a(Lgkr;Z)Z
    .locals 0

    iput-boolean p1, p0, Lgkr;->x:Z

    return p1
.end method

.method static synthetic b(Lgkr;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lgkr;->a(Z)V

    return-void
.end method

.method private j()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lgkr;->a(Z)V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lgkr;->m:J

    const/4 v0, 0x0

    iput-object v0, p0, Lgkr;->d:Lgef;

    iget-object v0, p0, Lgkr;->e:Lgkv;

    invoke-virtual {v0}, Lgkv;->a()V

    iget-object v0, p0, Lgkr;->r:Lgkv;

    invoke-virtual {v0}, Lgkv;->a()V

    iget-object v0, p0, Lgkr;->s:Lgkv;

    invoke-virtual {v0}, Lgkv;->a()V

    return-void
.end method


# virtual methods
.method public a(Lgku;JILorg/json/JSONObject;)J
    .locals 10

    const/4 v8, 0x1

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Lgkr;->c()J

    move-result-wide v2

    iget-object v1, p0, Lgkr;->r:Lgkv;

    invoke-virtual {v1, v2, v3, p1}, Lgkv;->a(JLgku;)V

    invoke-direct {p0, v8}, Lgkr;->a(Z)V

    :try_start_0
    const-string v1, "requestId"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "type"

    const-string v4, "SEEK"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "mediaSessionId"

    invoke-virtual {p0}, Lgkr;->i()J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "currentTime"

    long-to-double v4, p2

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    if-ne p4, v8, :cond_2

    const-string v1, "resumeState"

    const-string v4, "PLAYBACK_START"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_0
    :goto_0
    if-eqz p5, :cond_1

    const-string v1, "customData"

    invoke-virtual {v0, v1, p5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v2, v3, v1}, Lgkr;->a(Ljava/lang/String;JLjava/lang/String;)V

    return-wide v2

    :cond_2
    const/4 v1, 0x2

    if-ne p4, v1, :cond_0

    :try_start_1
    const-string v1, "resumeState"

    const-string v4, "PLAYBACK_PAUSE"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public a(Lgku;Lgeb;ZJ[JLorg/json/JSONObject;)J
    .locals 8

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Lgkr;->c()J

    move-result-wide v2

    iget-object v0, p0, Lgkr;->e:Lgkv;

    invoke-virtual {v0, v2, v3, p1}, Lgkv;->a(JLgku;)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lgkr;->a(Z)V

    :try_start_0
    const-string v0, "requestId"

    invoke-virtual {v1, v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v0, "type"

    const-string v4, "LOAD"

    invoke-virtual {v1, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "media"

    invoke-virtual {p2}, Lgeb;->c()Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v0, "autoplay"

    invoke-virtual {v1, v0, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    const-string v0, "currentTime"

    long-to-double v4, p4

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-virtual {v1, v0, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    if-eqz p6, :cond_1

    array-length v0, p6

    if-lez v0, :cond_1

    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4}, Lorg/json/JSONArray;-><init>()V

    const/4 v0, 0x0

    :goto_0
    array-length v5, p6

    if-ge v0, v5, :cond_0

    aget-wide v6, p6, v0

    invoke-virtual {v4, v0, v6, v7}, Lorg/json/JSONArray;->put(IJ)Lorg/json/JSONArray;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v0, "activeTrackIds"

    invoke-virtual {v1, v0, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    :cond_1
    if-eqz p7, :cond_2

    const-string v0, "customData"

    invoke-virtual {v1, v0, p7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_1
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v2, v3, v1}, Lgkr;->a(Ljava/lang/String;JLjava/lang/String;)V

    return-wide v2

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public a(Lgku;Lorg/json/JSONObject;)J
    .locals 6

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Lgkr;->c()J

    move-result-wide v2

    iget-object v1, p0, Lgkr;->o:Lgkv;

    invoke-virtual {v1, v2, v3, p1}, Lgkv;->a(JLgku;)V

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lgkr;->a(Z)V

    :try_start_0
    const-string v1, "requestId"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "type"

    const-string v4, "PAUSE"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "mediaSessionId"

    invoke-virtual {p0}, Lgkr;->i()J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    if-eqz p2, :cond_0

    const-string v1, "customData"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v2, v3, v1}, Lgkr;->a(Ljava/lang/String;JLjava/lang/String;)V

    return-wide v2

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public a()V
    .locals 0

    return-void
.end method

.method public a(JI)V
    .locals 3

    iget-object v0, p0, Lgkr;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkv;

    invoke-virtual {v0, p1, p2, p3}, Lgkv;->a(JI)Z

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a(JLorg/json/JSONObject;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lgkr;->e:Lgkv;

    invoke-virtual {v0, p1, p2}, Lgkv;->a(J)Z

    move-result v3

    iget-object v0, p0, Lgkr;->r:Lgkv;

    invoke-virtual {v0}, Lgkv;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lgkr;->r:Lgkv;

    invoke-virtual {v0, p1, p2}, Lgkv;->a(J)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    :goto_0
    iget-object v4, p0, Lgkr;->s:Lgkv;

    invoke-virtual {v4}, Lgkv;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lgkr;->s:Lgkv;

    invoke-virtual {v4, p1, p2}, Lgkv;->a(J)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    iget-object v4, p0, Lgkr;->t:Lgkv;

    invoke-virtual {v4}, Lgkv;->b()Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lgkr;->t:Lgkv;

    invoke-virtual {v4, p1, p2}, Lgkv;->a(J)Z

    move-result v4

    if-nez v4, :cond_8

    :cond_1
    :goto_1
    if-eqz v0, :cond_b

    const/4 v0, 0x2

    :goto_2
    if-eqz v1, :cond_2

    or-int/lit8 v0, v0, 0x1

    :cond_2
    if-nez v3, :cond_3

    iget-object v1, p0, Lgkr;->d:Lgef;

    if-nez v1, :cond_9

    :cond_3
    new-instance v0, Lgef;

    invoke-direct {v0, p3}, Lgef;-><init>(Lorg/json/JSONObject;)V

    iput-object v0, p0, Lgkr;->d:Lgef;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lgkr;->m:J

    const/4 v0, 0x7

    :goto_3
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lgkr;->m:J

    invoke-virtual {p0}, Lgkr;->a()V

    :cond_4
    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_5

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lgkr;->m:J

    invoke-virtual {p0}, Lgkr;->a()V

    :cond_5
    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lgkr;->b()V

    :cond_6
    iget-object v0, p0, Lgkr;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkv;

    invoke-virtual {v0, p1, p2, v2}, Lgkv;->a(JI)Z

    goto :goto_4

    :cond_7
    move v0, v2

    goto :goto_0

    :cond_8
    move v1, v2

    goto :goto_1

    :cond_9
    iget-object v1, p0, Lgkr;->d:Lgef;

    invoke-virtual {v1, p3, v0}, Lgef;->a(Lorg/json/JSONObject;I)I

    move-result v0

    goto :goto_3

    :cond_a
    return-void

    :cond_b
    move v0, v2

    goto :goto_2
.end method

.method public b(Lgku;Lorg/json/JSONObject;)J
    .locals 6

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    invoke-virtual {p0}, Lgkr;->c()J

    move-result-wide v2

    iget-object v1, p0, Lgkr;->p:Lgkv;

    invoke-virtual {v1, v2, v3, p1}, Lgkv;->a(JLgku;)V

    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lgkr;->a(Z)V

    :try_start_0
    const-string v1, "requestId"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    const-string v1, "type"

    const-string v4, "PLAY"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v1, "mediaSessionId"

    invoke-virtual {p0}, Lgkr;->i()J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    if-eqz p2, :cond_0

    const-string v1, "customData"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v2, v3, v1}, Lgkr;->a(Ljava/lang/String;JLjava/lang/String;)V

    return-wide v2

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public b()V
    .locals 0

    return-void
.end method

.method public d()V
    .locals 0

    invoke-direct {p0}, Lgkr;->j()V

    return-void
.end method

.method public e()J
    .locals 12

    const-wide/16 v2, 0x0

    invoke-virtual {p0}, Lgkr;->h()Lgeb;

    move-result-object v8

    if-nez v8, :cond_1

    :cond_0
    :goto_0
    return-wide v2

    :cond_1
    iget-wide v0, p0, Lgkr;->m:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgkr;->d:Lgef;

    invoke-virtual {v0}, Lgef;->d()D

    move-result-wide v10

    iget-object v0, p0, Lgkr;->d:Lgef;

    invoke-virtual {v0}, Lgef;->f()J

    move-result-wide v4

    iget-object v0, p0, Lgkr;->d:Lgef;

    invoke-virtual {v0}, Lgef;->b()I

    move-result v0

    const-wide/16 v6, 0x0

    cmpl-double v1, v10, v6

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    :cond_2
    move-wide v2, v4

    goto :goto_0

    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v6, p0, Lgkr;->m:J

    sub-long/2addr v0, v6

    cmp-long v6, v0, v2

    if-gez v6, :cond_7

    move-wide v6, v2

    :goto_1
    cmp-long v0, v6, v2

    if-nez v0, :cond_4

    move-wide v2, v4

    goto :goto_0

    :cond_4
    invoke-virtual {v8}, Lgeb;->a()J

    move-result-wide v0

    long-to-double v6, v6

    mul-double/2addr v6, v10

    double-to-long v6, v6

    add-long/2addr v4, v6

    cmp-long v6, v0, v2

    if-lez v6, :cond_5

    cmp-long v6, v4, v0

    if-lez v6, :cond_5

    :goto_2
    move-wide v2, v0

    goto :goto_0

    :cond_5
    cmp-long v0, v4, v2

    if-gez v0, :cond_6

    move-wide v0, v2

    goto :goto_2

    :cond_6
    move-wide v0, v4

    goto :goto_2

    :cond_7
    move-wide v6, v0

    goto :goto_1
.end method

.method public f()J
    .locals 2

    invoke-virtual {p0}, Lgkr;->h()Lgeb;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lgeb;->a()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public g()Lgef;
    .locals 1

    iget-object v0, p0, Lgkr;->d:Lgef;

    return-object v0
.end method

.method public h()Lgeb;
    .locals 1

    iget-object v0, p0, Lgkr;->d:Lgef;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lgkr;->d:Lgef;

    invoke-virtual {v0}, Lgef;->e()Lgeb;

    move-result-object v0

    goto :goto_0
.end method

.method public i()J
    .locals 2

    iget-object v0, p0, Lgkr;->d:Lgef;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No current media session"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lgkr;->d:Lgef;

    invoke-virtual {v0}, Lgef;->a()J

    move-result-wide v0

    return-wide v0
.end method
