.class public final Lhfq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhfp;


# instance fields
.field private final a:Lhei;

.field private final b:Lher;

.field private final c:Lhfr;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lhfq;->a:Lhei;

    .line 35
    const-class v0, Lher;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lher;

    iput-object v0, p0, Lhfq;->b:Lher;

    .line 36
    const-class v0, Lhfr;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhfr;

    iput-object v0, p0, Lhfq;->c:Lhfr;

    .line 37
    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 9

    .prologue
    .line 41
    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 42
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 43
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 44
    iget-object v0, p0, Lhfq;->b:Lher;

    invoke-interface {v0}, Lher;->a()[Ljava/lang/String;

    move-result-object v1

    array-length v5, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v5, :cond_1

    aget-object v6, v1, v0

    .line 45
    iget-object v7, p0, Lhfq;->c:Lhfr;

    invoke-interface {v7, v6}, Lhfr;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 46
    if-eqz v7, :cond_0

    .line 47
    invoke-interface {v3, v7, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    invoke-interface {v2, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    :cond_0
    invoke-interface {v4, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 44
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_1
    iget-object v0, p0, Lhfq;->a:Lhei;

    invoke-interface {v0}, Lhei;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 54
    iget-object v0, p0, Lhfq;->a:Lhei;

    invoke-interface {v0, v6}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 56
    const-string v1, "effective_gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    .line 57
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 60
    const-string v7, "stable_account_id"

    invoke-interface {v0, v7}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 61
    if-nez v0, :cond_3

    .line 62
    invoke-interface {v2, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 63
    iget-object v7, p0, Lhfq;->a:Lhei;

    invoke-interface {v7, v6}, Lhei;->b(I)Lhek;

    move-result-object v7

    const-string v8, "stable_account_id"

    .line 64
    invoke-interface {v7, v8, v0}, Lhek;->b(Ljava/lang/String;Ljava/lang/String;)Lhek;

    move-result-object v7

    .line 65
    invoke-interface {v7}, Lhek;->c()I

    .line 69
    :cond_3
    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 70
    if-eqz v0, :cond_5

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_5

    .line 71
    iget-object v7, p0, Lhfq;->a:Lhei;

    invoke-interface {v7, v1, v0}, Lhei;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    :goto_2
    invoke-interface {v4, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 75
    iget-object v0, p0, Lhfq;->a:Lhei;

    invoke-interface {v0, v6}, Lhei;->e(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 41
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 78
    :cond_4
    monitor-exit p0

    return-void

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 82
    iget-object v0, p0, Lhfq;->a:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 84
    const-string v1, "effective_gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 92
    :goto_0
    return-void

    .line 87
    :cond_0
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lhfq;->c:Lhfr;

    invoke-interface {v1, v0}, Lhfr;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 89
    iget-object v1, p0, Lhfq;->a:Lhei;

    invoke-interface {v1, p1}, Lhei;->b(I)Lhek;

    move-result-object v1

    const-string v2, "stable_account_id"

    .line 90
    invoke-interface {v1, v2, v0}, Lhek;->b(Ljava/lang/String;Ljava/lang/String;)Lhek;

    move-result-object v0

    .line 91
    invoke-interface {v0}, Lhek;->c()I

    goto :goto_0
.end method
