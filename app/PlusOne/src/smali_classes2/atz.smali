.class final Latz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final a:Laue;

.field final synthetic b:Latw;


# direct methods
.method public constructor <init>(Latw;Laue;)V
    .locals 2

    .prologue
    .line 796
    iput-object p1, p0, Latz;->b:Latw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 797
    const-string v0, "params"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laue;

    iput-object v0, p0, Latz;->a:Laue;

    .line 798
    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 802
    iget-object v0, p0, Latz;->b:Latw;

    invoke-static {v0}, Latw;->c(Latw;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 803
    :try_start_0
    iget-object v0, p0, Latz;->b:Latw;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Latw;->a(Latw;Z)Z

    .line 806
    iget-object v0, p0, Latz;->b:Latw;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Latw;->b(Latw;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 812
    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 813
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0
    :try_end_1
    .catch Lbgm; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_4

    .line 887
    :catch_0
    move-exception v0

    .line 858
    :try_start_2
    invoke-static {}, Latw;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "render context not initialized"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_4

    .line 877
    :try_start_3
    iget-object v0, p0, Latz;->b:Latw;

    invoke-static {v0}, Latw;->a(Latw;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 878
    :try_start_4
    iget-object v0, p0, Latz;->b:Latw;

    invoke-static {v0}, Latw;->b(Latw;)Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    if-ne v0, v3, :cond_0

    .line 879
    iget-object v0, p0, Latz;->b:Latw;

    const/4 v3, 0x0

    invoke-static {v0, v3}, Latw;->a(Latw;Ljava/lang/Thread;)Ljava/lang/Thread;

    .line 881
    :cond_0
    iget-object v0, p0, Latz;->b:Latw;

    invoke-static {v0}, Latw;->f(Latw;)Ljava/util/concurrent/Executor;

    move-result-object v0

    iget-object v3, p0, Latz;->b:Latw;

    invoke-static {v3}, Latw;->g(Latw;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 886
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 888
    :goto_0
    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    return-void

    .line 816
    :cond_1
    :try_start_6
    iget-object v0, p0, Latz;->a:Laue;

    iget-object v0, v0, Laue;->e:Ljava/io/File;

    if-eqz v0, :cond_2

    .line 817
    iget-object v0, p0, Latz;->b:Latw;

    iget-object v2, p0, Latz;->a:Laue;

    invoke-static {v0, v2}, Latw;->a(Latw;Laue;)V

    .line 820
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 821
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicLong;-><init>()V

    .line 822
    iget-object v4, p0, Latz;->b:Latw;

    invoke-static {v4}, Latw;->d(Latw;)Lbgf;

    move-result-object v4

    new-instance v5, Laua;

    invoke-direct {v5, p0, v0}, Laua;-><init>(Latz;Ljava/util/concurrent/atomic/AtomicLong;)V

    invoke-virtual {v4, v5}, Lbgf;->a(Ljava/lang/Runnable;)V

    .line 846
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 849
    iget-object v6, p0, Latz;->b:Latw;

    const-wide/16 v8, 0x3e8

    iget-object v7, p0, Latz;->a:Laue;

    iget-object v7, v7, Laue;->a:Lboi;

    invoke-virtual {v7}, Lboi;->n()J

    move-result-wide v10

    .line 850
    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v12

    sub-long/2addr v10, v12

    mul-long/2addr v8, v10

    sub-long v2, v4, v2

    div-long v2, v8, v2

    .line 849
    invoke-static {v6, v2, v3}, Latw;->a(Latw;J)J
    :try_end_6
    .catch Lbgm; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_4

    .line 856
    :try_start_7
    iget-object v0, p0, Latz;->b:Latw;

    invoke-static {v0}, Latw;->a(Latw;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 878
    :try_start_8
    iget-object v0, p0, Latz;->b:Latw;

    invoke-static {v0}, Latw;->b(Latw;)Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    if-ne v0, v3, :cond_3

    .line 879
    iget-object v0, p0, Latz;->b:Latw;

    const/4 v3, 0x0

    invoke-static {v0, v3}, Latw;->a(Latw;Ljava/lang/Thread;)Ljava/lang/Thread;

    .line 881
    :cond_3
    iget-object v0, p0, Latz;->b:Latw;

    invoke-static {v0}, Latw;->f(Latw;)Ljava/util/concurrent/Executor;

    move-result-object v0

    iget-object v3, p0, Latz;->b:Latw;

    invoke-static {v3}, Latw;->e(Latw;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 884
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    throw v0

    .line 888
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    throw v0

    .line 886
    :catchall_2
    move-exception v0

    :try_start_a
    monitor-exit v2
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    :try_start_b
    throw v0
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 865
    :catch_1
    move-exception v0

    :try_start_c
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 877
    :try_start_d
    iget-object v0, p0, Latz;->b:Latw;

    invoke-static {v0}, Latw;->a(Latw;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 878
    :try_start_e
    iget-object v0, p0, Latz;->b:Latw;

    invoke-static {v0}, Latw;->b(Latw;)Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    if-ne v0, v3, :cond_4

    .line 879
    iget-object v0, p0, Latz;->b:Latw;

    const/4 v3, 0x0

    invoke-static {v0, v3}, Latw;->a(Latw;Ljava/lang/Thread;)Ljava/lang/Thread;

    .line 881
    :cond_4
    iget-object v0, p0, Latz;->b:Latw;

    invoke-static {v0}, Latw;->f(Latw;)Ljava/util/concurrent/Executor;

    move-result-object v0

    iget-object v3, p0, Latz;->b:Latw;

    invoke-static {v3}, Latw;->g(Latw;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 886
    monitor-exit v2

    goto/16 :goto_0

    :catchall_3
    move-exception v0

    monitor-exit v2
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    :try_start_f
    throw v0
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 887
    :catch_2
    move-exception v0

    .line 870
    :try_start_10
    const-string v2, "unexpected exception running writeMovie in context"

    invoke-static {v2, v0}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/Throwable;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_4

    .line 873
    :catchall_4
    move-exception v0

    .line 877
    :try_start_11
    iget-object v2, p0, Latz;->b:Latw;

    invoke-static {v2}, Latw;->a(Latw;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    .line 878
    :try_start_12
    iget-object v3, p0, Latz;->b:Latw;

    invoke-static {v3}, Latw;->b(Latw;)Ljava/lang/Thread;

    move-result-object v3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v4

    if-ne v3, v4, :cond_5

    .line 879
    iget-object v3, p0, Latz;->b:Latw;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Latw;->a(Latw;Ljava/lang/Thread;)Ljava/lang/Thread;

    .line 881
    :cond_5
    iget-object v3, p0, Latz;->b:Latw;

    invoke-static {v3}, Latw;->f(Latw;)Ljava/util/concurrent/Executor;

    move-result-object v3

    iget-object v4, p0, Latz;->b:Latw;

    invoke-static {v4}, Latw;->g(Latw;)Ljava/lang/Runnable;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 886
    monitor-exit v2
    :try_end_12
    .catchall {:try_start_12 .. :try_end_12} :catchall_5

    :try_start_13
    throw v0
    :try_end_13
    .catchall {:try_start_13 .. :try_end_13} :catchall_1

    :catchall_5
    move-exception v0

    :try_start_14
    monitor-exit v2
    :try_end_14
    .catchall {:try_start_14 .. :try_end_14} :catchall_5

    :try_start_15
    throw v0
    :try_end_15
    .catchall {:try_start_15 .. :try_end_15} :catchall_1
.end method
