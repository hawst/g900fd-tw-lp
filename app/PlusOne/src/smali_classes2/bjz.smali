.class final Lbjz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbjr;


# instance fields
.field private a:I

.field private synthetic b:[Ljava/lang/String;


# direct methods
.method constructor <init>([Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lbjz;->b:[Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/media/MediaFormat;)Lbjs;
    .locals 5

    .prologue
    .line 172
    const-string v0, "mime"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbjt;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 173
    invoke-static {v2}, Lbjt;->f(Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 174
    iget-object v1, p0, Lbjz;->b:[Ljava/lang/String;

    iget v3, p0, Lbjz;->a:I

    aget-object v1, v1, v3

    .line 177
    invoke-static {v0, v1, v2}, Lbjt;->a(Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)Lbjs;

    move-result-object v0

    .line 178
    if-nez v0, :cond_2

    .line 180
    :try_start_0
    new-instance v0, Lbkb;

    .line 181
    invoke-static {v2}, Landroid/media/MediaCodec;->createDecoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Lbkb;-><init>(Landroid/media/MediaCodec;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    :cond_0
    :goto_0
    return-object v0

    .line 182
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 183
    invoke-static {}, Lbjt;->e()Ljava/lang/String;

    move-result-object v3

    const-string v4, "Problem creating codec by type: "

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v3, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 185
    const/4 v0, 0x0

    goto :goto_0

    .line 183
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 190
    :cond_2
    iget v1, p0, Lbjz;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lbjz;->a:I

    .line 191
    iget v1, p0, Lbjz;->a:I

    iget-object v2, p0, Lbjz;->b:[Ljava/lang/String;

    array-length v2, v2

    if-ne v1, v2, :cond_0

    .line 192
    const/4 v1, 0x0

    iput v1, p0, Lbjz;->a:I

    goto :goto_0
.end method
