.class public final enum Lbzc;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbzc;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lbzc;

.field public static final enum b:Lbzc;

.field public static final enum c:Lbzc;

.field private static final synthetic d:[Lbzc;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 51
    new-instance v0, Lbzc;

    const-string v1, "NAIVE"

    invoke-direct {v0, v1, v2}, Lbzc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbzc;->a:Lbzc;

    .line 52
    new-instance v0, Lbzc;

    const-string v1, "TOP_QUALITY"

    invoke-direct {v0, v1, v3}, Lbzc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbzc;->b:Lbzc;

    .line 53
    new-instance v0, Lbzc;

    const-string v1, "CLUSTERING"

    invoke-direct {v0, v1, v4}, Lbzc;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbzc;->c:Lbzc;

    .line 50
    const/4 v0, 0x3

    new-array v0, v0, [Lbzc;

    sget-object v1, Lbzc;->a:Lbzc;

    aput-object v1, v0, v2

    sget-object v1, Lbzc;->b:Lbzc;

    aput-object v1, v0, v3

    sget-object v1, Lbzc;->c:Lbzc;

    aput-object v1, v0, v4

    sput-object v0, Lbzc;->d:[Lbzc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 58
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbzc;
    .locals 1

    .prologue
    .line 50
    const-class v0, Lbzc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbzc;

    return-object v0
.end method

.method public static values()[Lbzc;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lbzc;->d:[Lbzc;

    invoke-virtual {v0}, [Lbzc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbzc;

    return-object v0
.end method
