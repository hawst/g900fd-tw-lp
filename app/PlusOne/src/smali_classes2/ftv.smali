.class public final Lftv;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lfsl;
.implements Lkdd;
.implements Lljh;


# static fields
.field private static a:Lizs;

.field private static b:Lkdv;

.field private static final c:Landroid/graphics/Paint;


# instance fields
.field private final A:Landroid/graphics/Paint;

.field private final B:Landroid/graphics/Path;

.field private C:Z

.field private D:F

.field private E:Lfty;

.field private d:I

.field private e:I

.field private f:F

.field private g:Lfrg;

.field private h:Lizu;

.field private i:I

.field private j:I

.field private k:Lcom/google/android/libraries/social/media/MediaResource;

.field private l:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<",
            "Landroid/graphics/Bitmap;",
            "Ljava/lang/Void;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private m:Landroid/graphics/Bitmap;

.field private n:Z

.field private o:Z

.field private p:Landroid/graphics/Rect;

.field private q:Landroid/graphics/Rect;

.field private r:Z

.field private s:Landroid/graphics/Paint;

.field private t:Landroid/graphics/Rect;

.field private u:Landroid/graphics/Paint;

.field private final v:Landroid/graphics/Matrix;

.field private final w:Landroid/graphics/Matrix;

.field private final x:Landroid/graphics/Rect;

.field private final y:Landroid/graphics/Rect;

.field private final z:Landroid/graphics/Paint;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 62
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lftv;->c:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lftv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 109
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lftv;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 113
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 116
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    iput v1, p0, Lftv;->e:I

    .line 66
    const/4 v0, 0x0

    iput v0, p0, Lftv;->f:F

    .line 69
    const/4 v0, 0x5

    iput v0, p0, Lftv;->i:I

    .line 76
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lftv;->p:Landroid/graphics/Rect;

    .line 77
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lftv;->q:Landroid/graphics/Rect;

    .line 78
    iput-boolean v1, p0, Lftv;->r:Z

    .line 79
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lftv;->s:Landroid/graphics/Paint;

    .line 80
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lftv;->t:Landroid/graphics/Rect;

    .line 81
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lftv;->u:Landroid/graphics/Paint;

    .line 96
    iput-boolean v1, p0, Lftv;->C:Z

    .line 97
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lftv;->D:F

    .line 117
    sget-object v0, Lftv;->a:Lizs;

    if-nez v0, :cond_0

    .line 118
    const-class v0, Lizs;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    sput-object v0, Lftv;->a:Lizs;

    .line 119
    const-class v0, Lkdv;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    sput-object v0, Lftv;->b:Lkdv;

    .line 121
    :cond_0
    iget-object v0, p0, Lftv;->s:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 123
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lftv;->v:Landroid/graphics/Matrix;

    .line 124
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lftv;->w:Landroid/graphics/Matrix;

    .line 125
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lftv;->x:Landroid/graphics/Rect;

    .line 126
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lftv;->y:Landroid/graphics/Rect;

    .line 127
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lftv;->z:Landroid/graphics/Paint;

    .line 128
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lftv;->A:Landroid/graphics/Paint;

    .line 129
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lftv;->B:Landroid/graphics/Path;

    .line 130
    return-void
.end method

.method static synthetic a(Lftv;)Landroid/os/AsyncTask;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lftv;->l:Landroid/os/AsyncTask;

    return-object v0
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 236
    invoke-direct {p0}, Lftv;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lftv;->n:Z

    if-eqz v0, :cond_1

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 239
    :cond_1
    iput-boolean v7, p0, Lftv;->n:Z

    .line 241
    new-instance v0, Lftx;

    invoke-virtual {p0}, Lftv;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v3, p0, Lftv;->d:I

    iget v4, p0, Lftv;->e:I

    iget-object v5, p0, Lftv;->g:Lfrg;

    iget v6, p0, Lftv;->f:F

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lftx;-><init>(Landroid/content/Context;Lftv;IILfrg;F)V

    iput-object v0, p0, Lftv;->l:Landroid/os/AsyncTask;

    .line 243
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_2

    .line 244
    iget-object v0, p0, Lftv;->l:Landroid/os/AsyncTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    new-array v2, v7, [Landroid/graphics/Bitmap;

    aput-object p1, v2, v8

    invoke-virtual {v0, v1, v2}, Landroid/os/AsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 248
    :cond_2
    iget-object v0, p0, Lftv;->l:Landroid/os/AsyncTask;

    new-array v1, v7, [Landroid/graphics/Bitmap;

    aput-object p1, v1, v8

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method static synthetic a(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 0

    .prologue
    .line 50
    invoke-static {p0, p1, p2}, Lftv;->b(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    return-void
.end method

.method private a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 329
    iget-object v0, p0, Lftv;->m:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 330
    iget-object v0, p0, Lftv;->m:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lftv;->p:Landroid/graphics/Rect;

    iget-object v2, p0, Lftv;->q:Landroid/graphics/Rect;

    sget-object v3, Lftv;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 334
    :goto_0
    iget-boolean v0, p0, Lftv;->r:Z

    if-eqz v0, :cond_0

    .line 335
    iget-object v0, p0, Lftv;->t:Landroid/graphics/Rect;

    iget-object v1, p0, Lftv;->s:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lftv;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 337
    :cond_0
    return-void

    .line 332
    :cond_1
    iget-object v0, p0, Lftv;->q:Landroid/graphics/Rect;

    iget-object v1, p0, Lftv;->u:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lftv;->a(Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/Matrix;Landroid/graphics/Rect;Landroid/graphics/Paint;)V
    .locals 1

    .prologue
    .line 317
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 318
    invoke-virtual {p1, p2}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 319
    sget-object v0, Landroid/graphics/Region$Op;->REPLACE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, p3, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    .line 320
    invoke-direct {p0, p1}, Lftv;->a(Landroid/graphics/Canvas;)V

    .line 322
    invoke-virtual {p4}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lftv;->B:Landroid/graphics/Path;

    invoke-virtual {p1, v0, p4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 325
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 326
    return-void
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Paint;)V
    .locals 4

    .prologue
    .line 296
    iget v0, p0, Lftv;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 297
    invoke-virtual {p2}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    int-to-float v1, v1

    .line 298
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v2

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    .line 297
    invoke-virtual {p1, v0, v1, v2, p3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 302
    :goto_0
    return-void

    .line 300
    :cond_0
    invoke-virtual {p1, p2, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method static synthetic a(Lftv;Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    .line 50
    iput-object p1, p0, Lftv;->m:Landroid/graphics/Bitmap;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lftv;->o:Z

    iget-object v0, p0, Lftv;->m:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lftv;->q:Landroid/graphics/Rect;

    iget-object v2, p0, Lftv;->p:Landroid/graphics/Rect;

    invoke-static {v0, v1, v2}, Lftv;->b(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    invoke-virtual {p0}, Lftv;->invalidate()V

    invoke-direct {p0}, Lftv;->h()V

    new-instance v0, Lftw;

    invoke-direct {v0, p0}, Lftw;-><init>(Lftv;)V

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lftv;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private static b(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 365
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    .line 366
    invoke-virtual {p0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 367
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v2

    .line 368
    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v3

    .line 370
    int-to-float v4, v0

    int-to-float v5, v1

    div-float/2addr v4, v5

    .line 371
    int-to-float v2, v2

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 373
    cmpl-float v3, v4, v2

    if-lez v3, :cond_0

    .line 374
    int-to-float v3, v1

    mul-float/2addr v2, v3

    float-to-int v2, v2

    sub-int v2, v0, v2

    div-int/lit8 v2, v2, 0x2

    .line 375
    sub-int/2addr v0, v2

    invoke-virtual {p2, v2, v6, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 383
    :goto_0
    return-void

    .line 377
    :cond_0
    int-to-float v3, v0

    div-float v2, v3, v2

    float-to-int v2, v2

    .line 379
    int-to-float v1, v1

    const v3, 0x3ecccccd    # 0.4f

    mul-float/2addr v1, v3

    float-to-int v1, v1

    .line 380
    div-int/lit8 v3, v2, 0x2

    sub-int/2addr v1, v3

    invoke-static {v1, v6}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 381
    add-int/2addr v2, v1

    invoke-virtual {p2, v6, v1, v0, v2}, Landroid/graphics/Rect;->set(IIII)V

    goto :goto_0
.end method

.method static synthetic f()Lkdv;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lftv;->b:Lkdv;

    return-object v0
.end method

.method private g()Z
    .locals 2

    .prologue
    .line 178
    iget v0, p0, Lftv;->d:I

    if-nez v0, :cond_0

    iget v0, p0, Lftv;->e:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lftv;->g:Lfrg;

    if-nez v0, :cond_0

    iget v0, p0, Lftv;->f:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lftv;->E:Lfty;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lftv;->E:Lfty;

    invoke-interface {v0}, Lfty;->a()V

    .line 215
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 341
    invoke-virtual {p0}, Lftv;->c()V

    .line 342
    iget-boolean v0, p0, Lftv;->o:Z

    if-eqz v0, :cond_0

    .line 343
    sget-object v0, Lftv;->b:Lkdv;

    iget-object v1, p0, Lftv;->m:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1}, Lkdv;->a(Landroid/graphics/Bitmap;)V

    .line 344
    iput-boolean v2, p0, Lftv;->o:Z

    .line 346
    :cond_0
    iput-object v3, p0, Lftv;->m:Landroid/graphics/Bitmap;

    .line 347
    iget-object v0, p0, Lftv;->l:Landroid/os/AsyncTask;

    if-eqz v0, :cond_1

    .line 348
    iget-object v0, p0, Lftv;->l:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 349
    iput-object v3, p0, Lftv;->l:Landroid/os/AsyncTask;

    .line 351
    :cond_1
    iput-boolean v2, p0, Lftv;->C:Z

    .line 353
    invoke-virtual {p0, v2, v2}, Lftv;->setMeasuredDimension(II)V

    .line 355
    iput v2, p0, Lftv;->e:I

    .line 356
    const/4 v0, 0x0

    iput v0, p0, Lftv;->f:F

    .line 357
    iput-object v3, p0, Lftv;->E:Lfty;

    .line 358
    return-void
.end method

.method public a(F)V
    .locals 0

    .prologue
    .line 154
    iput p1, p0, Lftv;->f:F

    .line 155
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 137
    iput p1, p0, Lftv;->d:I

    .line 138
    return-void
.end method

.method public a(IF)V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lftv;->s:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 142
    iget-object v0, p0, Lftv;->s:Landroid/graphics/Paint;

    invoke-virtual {v0, p2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 143
    return-void
.end method

.method public a(Lfrg;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lftv;->g:Lfrg;

    .line 159
    return-void
.end method

.method public a(Lfty;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lftv;->E:Lfty;

    .line 134
    return-void
.end method

.method public a(Lizu;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lftv;->h:Lizu;

    .line 163
    return-void
.end method

.method public a(Lkda;)V
    .locals 3

    .prologue
    .line 219
    invoke-virtual {p1}, Lkda;->getStatus()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 222
    const/4 v0, 0x0

    iput-boolean v0, p0, Lftv;->n:Z

    .line 223
    invoke-direct {p0}, Lftv;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 224
    check-cast p1, Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {p1}, Lcom/google/android/libraries/social/media/MediaResource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lftv;->m:Landroid/graphics/Bitmap;

    .line 225
    invoke-direct {p0}, Lftv;->h()V

    .line 226
    iget-object v0, p0, Lftv;->m:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lftv;->q:Landroid/graphics/Rect;

    iget-object v2, p0, Lftv;->p:Landroid/graphics/Rect;

    invoke-static {v0, v1, v2}, Lftv;->b(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 227
    invoke-virtual {p0}, Lftv;->invalidate()V

    .line 232
    :cond_0
    :goto_0
    return-void

    .line 228
    :cond_1
    invoke-virtual {p0}, Lftv;->getMeasuredHeight()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p0}, Lftv;->getMeasuredWidth()I

    move-result v0

    if-lez v0, :cond_0

    .line 229
    check-cast p1, Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {p1}, Lcom/google/android/libraries/social/media/MediaResource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, Lftv;->a(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 146
    iput-boolean p1, p0, Lftv;->r:Z

    .line 147
    return-void
.end method

.method public b()V
    .locals 8

    .prologue
    .line 196
    iget-object v0, p0, Lftv;->k:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_0

    .line 201
    :goto_0
    return-void

    .line 199
    :cond_0
    sget-object v0, Lftv;->a:Lizs;

    iget-object v1, p0, Lftv;->h:Lizu;

    iget v2, p0, Lftv;->i:I

    .line 200
    invoke-virtual {p0}, Lftv;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lftv;->getHeight()I

    move-result v4

    const/4 v5, 0x0

    iget v6, p0, Lftv;->j:I

    move-object v7, p0

    invoke-virtual/range {v0 .. v7}, Lizs;->a(Lizu;IIILizo;ILkdd;)Lcom/google/android/libraries/social/media/MediaResource;

    move-result-object v0

    iput-object v0, p0, Lftv;->k:Lcom/google/android/libraries/social/media/MediaResource;

    goto :goto_0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 150
    iput p1, p0, Lftv;->e:I

    .line 151
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lftv;->k:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lftv;->k:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/media/MediaResource;->unregister(Lkdd;)V

    .line 207
    const/4 v0, 0x0

    iput-object v0, p0, Lftv;->k:Lcom/google/android/libraries/social/media/MediaResource;

    .line 209
    :cond_0
    return-void
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 166
    iput p1, p0, Lftv;->i:I

    .line 167
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 491
    const/4 v0, 0x0

    iput-boolean v0, p0, Lftv;->C:Z

    .line 492
    return-void
.end method

.method public d(I)V
    .locals 0

    .prologue
    .line 170
    iput p1, p0, Lftv;->j:I

    .line 171
    return-void
.end method

.method public e()F
    .locals 1

    .prologue
    .line 390
    iget v0, p0, Lftv;->D:F

    return v0
.end method

.method public e(I)V
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lftv;->u:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 175
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 184
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 185
    invoke-virtual {p0}, Lftv;->b()V

    .line 186
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 190
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 191
    invoke-virtual {p0}, Lftv;->c()V

    .line 192
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 306
    iget-boolean v0, p0, Lftv;->C:Z

    if-eqz v0, :cond_0

    .line 307
    iget-object v0, p0, Lftv;->v:Landroid/graphics/Matrix;

    iget-object v1, p0, Lftv;->y:Landroid/graphics/Rect;

    iget-object v2, p0, Lftv;->A:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1, v2}, Lftv;->a(Landroid/graphics/Canvas;Landroid/graphics/Matrix;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 308
    iget-object v0, p0, Lftv;->w:Landroid/graphics/Matrix;

    iget-object v1, p0, Lftv;->x:Landroid/graphics/Rect;

    iget-object v2, p0, Lftv;->z:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1, v2}, Lftv;->a(Landroid/graphics/Canvas;Landroid/graphics/Matrix;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 312
    :goto_0
    return-void

    .line 310
    :cond_0
    invoke-direct {p0, p1}, Lftv;->a(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 270
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 271
    iget-object v0, p0, Lftv;->q:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lftv;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lftv;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 272
    iget-boolean v0, p0, Lftv;->r:Z

    if-eqz v0, :cond_0

    .line 275
    iget-object v0, p0, Lftv;->s:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    float-to-int v0, v0

    div-int/lit8 v0, v0, 0x2

    .line 276
    iget-object v1, p0, Lftv;->t:Landroid/graphics/Rect;

    iget-object v2, p0, Lftv;->q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    add-int/2addr v2, v0

    iget-object v3, p0, Lftv;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    add-int/2addr v3, v0

    iget-object v4, p0, Lftv;->q:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->right:I

    sub-int/2addr v4, v0

    iget-object v5, p0, Lftv;->q:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->bottom:I

    sub-int v0, v5, v0

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/graphics/Rect;->set(IIII)V

    .line 282
    :cond_0
    iget-object v0, p0, Lftv;->m:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    iget-object v0, p0, Lftv;->k:Lcom/google/android/libraries/social/media/MediaResource;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lftv;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lftv;->k:Lcom/google/android/libraries/social/media/MediaResource;

    .line 283
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getStatus()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 284
    iget-object v0, p0, Lftv;->k:Lcom/google/android/libraries/social/media/MediaResource;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/MediaResource;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, Lftv;->a(Landroid/graphics/Bitmap;)V

    .line 286
    :cond_1
    iget-object v0, p0, Lftv;->m:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    .line 287
    iget-object v0, p0, Lftv;->m:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lftv;->q:Landroid/graphics/Rect;

    iget-object v2, p0, Lftv;->p:Landroid/graphics/Rect;

    invoke-static {v0, v1, v2}, Lftv;->b(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    .line 290
    :cond_2
    iget-object v0, p0, Lftv;->B:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 291
    iget-object v0, p0, Lftv;->B:Landroid/graphics/Path;

    iget-object v1, p0, Lftv;->q:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerX()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lftv;->q:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lftv;->q:Landroid/graphics/Rect;

    .line 292
    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    iget-object v4, p0, Lftv;->q:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    sget-object v4, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    .line 291
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 293
    return-void
.end method

.method public setVerticalFold(F)V
    .locals 12
    .annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
    .end annotation

    .prologue
    .line 400
    iput p1, p0, Lftv;->D:F

    .line 401
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    .line 404
    const p1, 0x3c23d70a    # 0.01f

    .line 407
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lftv;->C:Z

    .line 410
    const/high16 v0, 0x3f800000    # 1.0f

    cmpl-float v0, p1, v0

    if-lez v0, :cond_1

    .line 411
    iget-object v0, p0, Lftv;->z:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float v1, p1, v1

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    const v2, 0x3f4ccccd    # 0.8f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 412
    iget-object v0, p0, Lftv;->A:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 418
    :goto_0
    invoke-virtual {p0}, Lftv;->getMeasuredWidth()I

    move-result v6

    .line 419
    invoke-virtual {p0}, Lftv;->getMeasuredHeight()I

    move-result v7

    .line 420
    int-to-float v0, v7

    const/high16 v1, 0x3e800000    # 0.25f

    mul-float/2addr v0, v1

    float-to-int v8, v0

    .line 422
    int-to-float v0, v6

    const/high16 v1, 0x40000000    # 2.0f

    div-float v9, v0, v1

    .line 423
    int-to-float v0, v8

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    mul-float/2addr v0, v1

    neg-float v10, v0

    .line 424
    int-to-float v0, v7

    int-to-float v1, v8

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v2, p1

    mul-float/2addr v1, v2

    add-float v11, v0, v1

    .line 427
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_2

    const/high16 v0, 0x3f800000    # 1.0f

    sub-float/2addr v0, p1

    mul-float/2addr v0, v9

    .line 430
    :goto_1
    const/16 v1, 0x8

    new-array v1, v1, [F

    .line 431
    const/4 v2, 0x0

    const/4 v3, 0x0

    aput v3, v1, v2

    .line 432
    const/4 v2, 0x1

    int-to-float v3, v7

    aput v3, v1, v2

    .line 433
    const/4 v2, 0x2

    const/4 v3, 0x0

    aput v3, v1, v2

    .line 434
    const/4 v2, 0x3

    const/4 v3, 0x0

    aput v3, v1, v2

    .line 435
    const/4 v2, 0x4

    aput v9, v1, v2

    .line 436
    const/4 v2, 0x5

    int-to-float v3, v7

    aput v3, v1, v2

    .line 437
    const/4 v2, 0x6

    aput v9, v1, v2

    .line 438
    const/4 v2, 0x7

    const/4 v3, 0x0

    aput v3, v1, v2

    .line 441
    const/16 v2, 0x8

    new-array v3, v2, [F

    .line 442
    const/4 v2, 0x0

    aput v0, v3, v2

    .line 443
    const/4 v2, 0x1

    aput v11, v3, v2

    .line 444
    const/4 v2, 0x2

    aput v0, v3, v2

    .line 445
    const/4 v0, 0x3

    aput v10, v3, v0

    .line 446
    const/4 v0, 0x4

    aput v9, v3, v0

    .line 447
    const/4 v0, 0x5

    int-to-float v2, v7

    aput v2, v3, v0

    .line 448
    const/4 v0, 0x6

    aput v9, v3, v0

    .line 449
    const/4 v0, 0x7

    const/4 v2, 0x0

    aput v2, v3, v0

    .line 453
    iget-object v0, p0, Lftv;->v:Landroid/graphics/Matrix;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Matrix;->setPolyToPoly([FI[FII)Z

    .line 455
    iget-object v0, p0, Lftv;->y:Landroid/graphics/Rect;

    const/4 v2, 0x0

    const/4 v4, 0x0

    float-to-int v5, v9

    invoke-virtual {v0, v2, v4, v5, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 458
    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p1, v0

    if-gez v0, :cond_3

    const/high16 v0, 0x3f800000    # 1.0f

    add-float/2addr v0, p1

    mul-float/2addr v0, v9

    .line 461
    :goto_2
    const/4 v2, 0x0

    aput v9, v1, v2

    .line 462
    const/4 v2, 0x1

    int-to-float v4, v7

    aput v4, v1, v2

    .line 463
    const/4 v2, 0x2

    aput v9, v1, v2

    .line 464
    const/4 v2, 0x3

    const/4 v4, 0x0

    aput v4, v1, v2

    .line 465
    const/4 v2, 0x4

    int-to-float v4, v6

    aput v4, v1, v2

    .line 466
    const/4 v2, 0x5

    int-to-float v4, v7

    aput v4, v1, v2

    .line 467
    const/4 v2, 0x6

    int-to-float v4, v6

    aput v4, v1, v2

    .line 468
    const/4 v2, 0x7

    const/4 v4, 0x0

    aput v4, v1, v2

    .line 471
    const/4 v2, 0x0

    aput v9, v3, v2

    .line 472
    const/4 v2, 0x1

    int-to-float v4, v7

    aput v4, v3, v2

    .line 473
    const/4 v2, 0x2

    aput v9, v3, v2

    .line 474
    const/4 v2, 0x3

    const/4 v4, 0x0

    aput v4, v3, v2

    .line 475
    const/4 v2, 0x4

    aput v0, v3, v2

    .line 476
    const/4 v2, 0x5

    aput v11, v3, v2

    .line 477
    const/4 v2, 0x6

    aput v0, v3, v2

    .line 478
    const/4 v0, 0x7

    aput v10, v3, v0

    .line 482
    iget-object v0, p0, Lftv;->w:Landroid/graphics/Matrix;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x4

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Matrix;->setPolyToPoly([FI[FII)Z

    .line 484
    iget-object v0, p0, Lftv;->x:Landroid/graphics/Rect;

    float-to-int v1, v9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 486
    const/4 v0, 0x0

    neg-int v1, v8

    add-int v2, v7, v8

    invoke-virtual {p0, v0, v1, v6, v2}, Lftv;->invalidate(IIII)V

    .line 487
    return-void

    .line 414
    :cond_1
    iget-object v0, p0, Lftv;->z:Landroid/graphics/Paint;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 415
    iget-object v0, p0, Lftv;->A:Landroid/graphics/Paint;

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, p1

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    const v2, 0x3f4ccccd    # 0.8f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-static {v1, v2, v3, v4}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_0

    .line 427
    :cond_2
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v0, p1, v0

    mul-float/2addr v0, v9

    goto/16 :goto_1

    .line 458
    :cond_3
    const/high16 v0, 0x40400000    # 3.0f

    sub-float/2addr v0, p1

    mul-float/2addr v0, v9

    goto :goto_2
.end method
