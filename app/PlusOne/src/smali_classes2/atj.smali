.class public final Latj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lats;


# instance fields
.field final a:Ljava/lang/Object;

.field b:I

.field c:Z

.field d:Latl;

.field private final e:J

.field private final f:Ljava/util/concurrent/Executor;

.field private final g:[J

.field private final h:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Ljava/util/concurrent/Executor;Latl;IJ)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Latj;->a:Ljava/lang/Object;

    .line 24
    iput v1, p0, Latj;->b:I

    .line 31
    iput-boolean v1, p0, Latj;->c:Z

    .line 59
    new-instance v0, Latk;

    invoke-direct {v0, p0}, Latk;-><init>(Latj;)V

    iput-object v0, p0, Latj;->h:Ljava/lang/Runnable;

    .line 52
    const-string v0, "mainThreadExecutor"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Latj;->f:Ljava/util/concurrent/Executor;

    .line 53
    const-string v0, "updateUiListener"

    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Latl;

    iput-object v0, p0, Latj;->d:Latl;

    .line 54
    iput-wide p4, p0, Latj;->e:J

    .line 55
    new-array v0, p3, [J

    iput-object v0, p0, Latj;->g:[J

    .line 57
    return-void
.end method


# virtual methods
.method public a(IJ)V
    .locals 12

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 76
    const-wide/16 v2, 0x0

    .line 79
    iget-object v6, p0, Latj;->a:Ljava/lang/Object;

    monitor-enter v6

    .line 80
    :try_start_0
    iget-object v4, p0, Latj;->g:[J

    aput-wide p2, v4, p1

    .line 82
    iget-object v7, p0, Latj;->g:[J

    array-length v8, v7

    move-wide v4, v2

    move v2, v1

    :goto_0
    if-ge v2, v8, :cond_0

    aget-wide v10, v7, v2

    .line 83
    add-long/2addr v4, v10

    .line 82
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 86
    :cond_0
    const-wide/16 v2, 0x64

    mul-long/2addr v2, v4

    iget-wide v4, p0, Latj;->e:J

    div-long/2addr v2, v4

    long-to-int v2, v2

    .line 88
    iget v3, p0, Latj;->b:I

    if-le v2, v3, :cond_2

    .line 89
    iput v2, p0, Latj;->b:I

    .line 90
    iget-boolean v2, p0, Latj;->c:Z

    if-nez v2, :cond_2

    .line 92
    const/4 v1, 0x1

    iput-boolean v1, p0, Latj;->c:Z

    .line 95
    :goto_1
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    if-eqz v0, :cond_1

    .line 98
    iget-object v0, p0, Latj;->f:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Latj;->h:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 100
    :cond_1
    return-void

    .line 95
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method
