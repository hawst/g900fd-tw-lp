.class final Ljhq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/app/NotificationManager;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Ljhq;->b:Landroid/content/Context;

    .line 31
    const-string v0, "notification"

    .line 32
    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Ljhq;->a:Landroid/app/NotificationManager;

    .line 33
    return-void
.end method

.method private a(Landroid/app/Notification;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 98
    iget-object v1, p0, Ljhq;->a:Landroid/app/NotificationManager;

    iget-object v0, p0, Ljhq;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0, p3, p1}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 99
    return-void

    .line 98
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method a(I)V
    .locals 4

    .prologue
    .line 37
    iget-object v1, p0, Ljhq;->a:Landroid/app/NotificationManager;

    iget-object v0, p0, Ljhq;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v0, ":notifications:share_queue"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0, p1}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 39
    return-void

    .line 37
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method a(III)V
    .locals 10

    .prologue
    const v9, 0x7f0b01a1

    const v8, 0x7f02053d

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 48
    iget-object v0, p0, Ljhq;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 49
    if-lez p3, :cond_1

    .line 51
    const v1, 0x7f0a01a6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 56
    :goto_0
    iget-object v1, p0, Ljhq;->b:Landroid/content/Context;

    invoke-static {v1, p1}, Ljhy;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    .line 57
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    long-to-int v2, v2

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Ljhq;->b:Landroid/content/Context;

    const-class v5, Lcom/google/android/libraries/social/networkqueue/impl/PendingItemsActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v4, "account_id"

    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v4, p0, Ljhq;->b:Landroid/content/Context;

    invoke-static {v4, v2, v3, v7}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 59
    invoke-static {}, Llsj;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 62
    new-instance v3, Landroid/app/Notification$Builder;

    iget-object v4, p0, Ljhq;->b:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {v3, v1}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {v3, v8}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    invoke-virtual {v3, v2}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    invoke-virtual {v3, v6}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    invoke-virtual {v3, v7}, Landroid/app/Notification$Builder;->setVisibility(I)Landroid/app/Notification$Builder;

    move-result-object v1

    iget-object v2, p0, Ljhq;->b:Landroid/content/Context;

    new-instance v4, Landroid/app/Notification$Builder;

    invoke-direct {v4, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    invoke-virtual {v4, v8}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    invoke-virtual {v4, v6}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    invoke-virtual {v4}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setPublicVersion(Landroid/app/Notification;)Landroid/app/Notification$Builder;

    move-result-object v0

    const-string v1, "social"

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setCategory(Ljava/lang/String;)Landroid/app/Notification$Builder;

    invoke-static {}, Llsj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljhq;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v3, v0}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    :cond_0
    invoke-virtual {v3}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v0

    const-string v1, ":notifications:share_queue"

    invoke-direct {p0, v0, v1, p1}, Ljhq;->a(Landroid/app/Notification;Ljava/lang/String;I)V

    .line 74
    :goto_1
    return-void

    .line 53
    :cond_1
    iget-object v0, p0, Ljhq;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110005

    new-array v2, v6, [Ljava/lang/Object;

    .line 54
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v7

    invoke-virtual {v0, v1, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 66
    :cond_2
    new-instance v3, Lbs;

    iget-object v4, p0, Ljhq;->b:Landroid/content/Context;

    invoke-direct {v3, v4}, Lbs;-><init>(Landroid/content/Context;)V

    .line 67
    invoke-virtual {v3, v0}, Lbs;->a(Ljava/lang/CharSequence;)Lbs;

    .line 68
    invoke-virtual {v3, v1}, Lbs;->b(Ljava/lang/CharSequence;)Lbs;

    .line 69
    invoke-virtual {v3, v8}, Lbs;->a(I)Lbs;

    .line 70
    invoke-virtual {v3, v2}, Lbs;->a(Landroid/app/PendingIntent;)Lbs;

    .line 71
    invoke-virtual {v3, v6}, Lbs;->a(Z)Lbs;

    .line 72
    invoke-virtual {v3}, Lbs;->c()Landroid/app/Notification;

    move-result-object v0

    const-string v1, ":notifications:share_queue"

    invoke-direct {p0, v0, v1, p1}, Ljhq;->a(Landroid/app/Notification;Ljava/lang/String;I)V

    goto :goto_1
.end method
