.class final Lbgt;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lcgc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcgc",
            "<",
            "Lbgs;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcgc",
            "<[F>;>;"
        }
    .end annotation
.end field

.field private final d:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcgc",
            "<[I>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 357
    const-class v0, Lbgt;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbgt;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/16 v3, 0x8

    const/16 v2, 0x40

    .line 430
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 431
    new-instance v0, Lcgi;

    new-instance v1, Lbgw;

    invoke-direct {v1}, Lbgw;-><init>()V

    invoke-direct {v0, v3, v1}, Lcgi;-><init>(ILcgj;)V

    iput-object v0, p0, Lbgt;->b:Lcgc;

    .line 434
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v3}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lbgt;->c:Landroid/util/SparseArray;

    .line 435
    invoke-direct {p0, v4, v2}, Lbgt;->a(II)Lcgc;

    .line 436
    invoke-direct {p0, v5, v2}, Lbgt;->a(II)Lcgc;

    .line 437
    invoke-direct {p0, v6, v2}, Lbgt;->a(II)Lcgc;

    .line 438
    const/4 v0, 0x4

    invoke-direct {p0, v0, v2}, Lbgt;->a(II)Lcgc;

    .line 439
    const/16 v0, 0x9

    invoke-direct {p0, v0, v2}, Lbgt;->a(II)Lcgc;

    .line 441
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v3}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lbgt;->d:Landroid/util/SparseArray;

    .line 442
    invoke-direct {p0, v4, v2}, Lbgt;->b(II)Lcgc;

    .line 443
    invoke-direct {p0, v5, v2}, Lbgt;->b(II)Lcgc;

    .line 444
    invoke-direct {p0, v6, v2}, Lbgt;->b(II)Lcgc;

    .line 445
    const/4 v0, 0x4

    invoke-direct {p0, v0, v2}, Lbgt;->b(II)Lcgc;

    .line 446
    return-void
.end method

.method private a(II)Lcgc;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lcgc",
            "<[F>;"
        }
    .end annotation

    .prologue
    .line 497
    new-instance v0, Lcgi;

    new-instance v1, Lbgu;

    invoke-direct {v1, p1}, Lbgu;-><init>(I)V

    invoke-direct {v0, p2, v1}, Lcgi;-><init>(ILcgj;)V

    .line 499
    iget-object v1, p0, Lbgt;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 500
    return-object v0
.end method

.method private b(II)Lcgc;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Lcgc",
            "<[I>;"
        }
    .end annotation

    .prologue
    .line 504
    new-instance v0, Lcgi;

    new-instance v1, Lbgv;

    invoke-direct {v1, p1}, Lbgv;-><init>(I)V

    invoke-direct {v0, p2, v1}, Lcgi;-><init>(ILcgj;)V

    .line 506
    iget-object v1, p0, Lbgt;->d:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 507
    return-object v0
.end method


# virtual methods
.method public a()Lbgs;
    .locals 2

    .prologue
    .line 449
    iget-object v1, p0, Lbgt;->b:Lcgc;

    monitor-enter v1

    .line 450
    :try_start_0
    iget-object v0, p0, Lbgt;->b:Lcgc;

    invoke-virtual {v0}, Lcgc;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgs;

    monitor-exit v1

    return-object v0

    .line 451
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lbgs;)V
    .locals 2

    .prologue
    .line 455
    iget-object v1, p0, Lbgt;->b:Lcgc;

    monitor-enter v1

    .line 456
    :try_start_0
    iget-object v0, p0, Lbgt;->b:Lcgc;

    invoke-virtual {v0, p1}, Lcgc;->a(Ljava/lang/Object;)V

    .line 457
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a([F)V
    .locals 3

    .prologue
    .line 472
    iget-object v1, p0, Lbgt;->c:Landroid/util/SparseArray;

    monitor-enter v1

    .line 473
    :try_start_0
    iget-object v0, p0, Lbgt;->c:Landroid/util/SparseArray;

    array-length v2, p1

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgc;

    .line 474
    invoke-virtual {v0, p1}, Lcgc;->a(Ljava/lang/Object;)V

    .line 475
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a([I)V
    .locals 3

    .prologue
    .line 490
    iget-object v1, p0, Lbgt;->d:Landroid/util/SparseArray;

    monitor-enter v1

    .line 491
    :try_start_0
    iget-object v0, p0, Lbgt;->d:Landroid/util/SparseArray;

    array-length v2, p1

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgc;

    .line 492
    invoke-virtual {v0, p1}, Lcgc;->a(Ljava/lang/Object;)V

    .line 493
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(I)[F
    .locals 4

    .prologue
    .line 461
    iget-object v1, p0, Lbgt;->c:Landroid/util/SparseArray;

    monitor-enter v1

    .line 462
    :try_start_0
    iget-object v0, p0, Lbgt;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgc;

    .line 463
    if-nez v0, :cond_0

    .line 464
    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Lbgt;->a(II)Lcgc;

    move-result-object v0

    .line 465
    sget-object v2, Lbgt;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x35

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Created a new float pool of element count "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 467
    :cond_0
    invoke-virtual {v0}, Lcgc;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    monitor-exit v1

    return-object v0

    .line 468
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(I)[I
    .locals 4

    .prologue
    .line 479
    iget-object v1, p0, Lbgt;->d:Landroid/util/SparseArray;

    monitor-enter v1

    .line 480
    :try_start_0
    iget-object v0, p0, Lbgt;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgc;

    .line 481
    if-nez v0, :cond_0

    .line 482
    const/4 v0, 0x4

    invoke-direct {p0, p1, v0}, Lbgt;->b(II)Lcgc;

    move-result-object v0

    .line 483
    sget-object v2, Lbgt;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x37

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Created a new integer pool of element count "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 485
    :cond_0
    invoke-virtual {v0}, Lcgc;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    monitor-exit v1

    return-object v0

    .line 486
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
