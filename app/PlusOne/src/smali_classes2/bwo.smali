.class final Lbwo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lbwn;


# direct methods
.method constructor <init>(Lbwn;)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lbwo;->a:Lbwn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 228
    iget-object v0, p0, Lbwo;->a:Lbwn;

    invoke-static {v0}, Lbwn;->a(Lbwn;)Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_1

    .line 249
    :cond_0
    :goto_0
    return-void

    .line 232
    :cond_1
    iget-object v0, p0, Lbwo;->a:Lbwn;

    invoke-static {v0}, Lbwn;->b(Lbwn;)Lbvs;

    move-result-object v0

    invoke-interface {v0}, Lbvs;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 233
    iget-object v0, p0, Lbwo;->a:Lbwn;

    invoke-static {v0}, Lbwn;->c(Lbwn;)Livc;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 234
    iget-object v0, p0, Lbwo;->a:Lbwn;

    invoke-static {v0}, Lbwn;->c(Lbwn;)Livc;

    move-result-object v0

    const-string v1, "post capture analysis aborted: battery became too low"

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    .line 236
    :cond_2
    iget-object v0, p0, Lbwo;->a:Lbwn;

    invoke-static {v0}, Lbwn;->d(Lbwn;)Laim;

    move-result-object v0

    invoke-virtual {v0}, Laim;->a()V

    goto :goto_0

    .line 241
    :cond_3
    iget-object v0, p0, Lbwo;->a:Lbwn;

    invoke-static {v0}, Lbwn;->b(Lbwn;)Lbvs;

    move-result-object v0

    invoke-interface {v0}, Lbvs;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p0, Lbwo;->a:Lbwn;

    invoke-static {v0}, Lbwn;->d(Lbwn;)Laim;

    move-result-object v0

    invoke-virtual {v0}, Laim;->a()V

    .line 243
    iget-object v0, p0, Lbwo;->a:Lbwn;

    invoke-static {v0}, Lbwn;->c(Lbwn;)Livc;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 244
    iget-object v0, p0, Lbwo;->a:Lbwn;

    invoke-static {v0}, Lbwn;->c(Lbwn;)Livc;

    move-result-object v0

    const-string v1, "post capture analysis rescheduled: screen turned on"

    invoke-interface {v0, v1}, Livc;->a(Ljava/lang/String;)V

    .line 246
    :cond_4
    iget-object v0, p0, Lbwo;->a:Lbwn;

    invoke-static {v0}, Lbwn;->g(Lbwn;)Lbwp;

    move-result-object v0

    iget-object v1, p0, Lbwo;->a:Lbwn;

    invoke-static {v1}, Lbwn;->e(Lbwn;)Lbwm;

    move-result-object v1

    invoke-interface {v1}, Lbwm;->b()J

    move-result-wide v2

    iget-object v1, p0, Lbwo;->a:Lbwn;

    invoke-static {v1}, Lbwn;->a(Lbwn;)Landroid/net/Uri;

    move-result-object v1

    iget-object v4, p0, Lbwo;->a:Lbwn;

    invoke-static {v4}, Lbwn;->f(Lbwn;)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    invoke-interface {v0, v2, v3, v1, v4}, Lbwp;->a(JLandroid/net/Uri;I)V

    goto :goto_0
.end method
