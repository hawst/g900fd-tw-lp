.class public final Liys;
.super Lloj;
.source "PG"

# interfaces
.implements Liyv;


# instance fields
.field private Q:Landroid/view/LayoutInflater;

.field private R:Liwa;

.field private S:Lhei;

.field private T:Liyt;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Lloj;-><init>()V

    .line 27
    new-instance v0, Liyt;

    iget-object v1, p0, Liys;->P:Llqm;

    invoke-direct {v0, v1}, Liyt;-><init>(Llqr;)V

    iput-object v0, p0, Liys;->T:Liyt;

    .line 30
    const/4 v0, 0x0

    const v1, 0x7f090066

    invoke-virtual {p0, v0, v1}, Liys;->a(II)V

    .line 31
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 50
    iget-object v0, p0, Liys;->Q:Landroid/view/LayoutInflater;

    const v1, 0x7f0401d1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 51
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 52
    iget-object v2, p0, Liys;->T:Liyt;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 53
    iget-object v0, p0, Liys;->T:Liyt;

    invoke-virtual {v0, p0}, Liyt;->a(Liyv;)V

    .line 54
    return-object v1
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Liys;->b()V

    .line 60
    iget-object v0, p0, Liys;->R:Liwa;

    invoke-interface {v0, p1, p2}, Liwa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 65
    invoke-virtual {p0}, Liys;->b()V

    .line 66
    iget-object v0, p0, Liys;->S:Lhei;

    invoke-interface {v0, p1, p2}, Lhei;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 67
    iget-object v1, p0, Liys;->R:Liwa;

    invoke-interface {v1, v0}, Liwa;->a(I)V

    .line 68
    return-void
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 35
    invoke-super {p0, p1}, Lloj;->c(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 36
    const v1, 0x7f0a01a9

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    .line 37
    return-object v0
.end method

.method protected k(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 42
    invoke-super {p0, p1}, Lloj;->k(Landroid/os/Bundle;)V

    .line 43
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Liys;->N:Llnl;

    invoke-virtual {p0}, Liys;->j_()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Liys;->Q:Landroid/view/LayoutInflater;

    .line 44
    iget-object v0, p0, Liys;->O:Llnh;

    const-class v1, Liwa;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwa;

    iput-object v0, p0, Liys;->R:Liwa;

    .line 45
    iget-object v0, p0, Liys;->O:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Liys;->S:Lhei;

    .line 46
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 72
    invoke-super {p0, p1}, Lloj;->onCancel(Landroid/content/DialogInterface;)V

    .line 73
    iget-object v0, p0, Liys;->R:Liwa;

    invoke-interface {v0}, Liwa;->c()V

    .line 74
    return-void
.end method
