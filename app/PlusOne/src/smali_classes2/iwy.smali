.class public final Liwy;
.super Llol;
.source "PG"

# interfaces
.implements Lhes;
.implements Lhob;
.implements Liwa;


# instance fields
.field private N:Lixd;

.field private O:Lhei;

.field private P:Lher;

.field private final Q:Lhoc;

.field private final R:Lhov;

.field private S:Liwg;

.field private T:Ljava/lang/String;

.field private U:Ljava/lang/String;

.field private V:Ljava/lang/String;

.field private W:I

.field private X:Z

.field private Y:Z

.field private Z:Z

.field private aa:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Llol;-><init>()V

    .line 70
    new-instance v0, Lhoc;

    iget-object v1, p0, Liwy;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    .line 71
    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    move-result-object v0

    iput-object v0, p0, Liwy;->Q:Lhoc;

    .line 72
    new-instance v0, Lhov;

    iget-object v1, p0, Liwy;->av:Llqm;

    invoke-direct {v0, v1}, Lhov;-><init>(Llqr;)V

    iput-object v0, p0, Liwy;->R:Lhov;

    .line 78
    const/4 v0, -0x1

    iput v0, p0, Liwy;->W:I

    .line 694
    return-void
.end method

.method public static a(Lae;)Liwy;
    .locals 3

    .prologue
    .line 86
    const-string v0, "login.fragment"

    .line 87
    invoke-virtual {p0, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Liwy;

    .line 88
    if-nez v0, :cond_0

    .line 89
    new-instance v0, Liwy;

    invoke-direct {v0}, Liwy;-><init>()V

    .line 90
    invoke-virtual {p0}, Lae;->a()Lat;

    move-result-object v1

    .line 91
    const-string v2, "login.fragment"

    invoke-virtual {v1, v0, v2}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    .line 92
    invoke-virtual {v1}, Lat;->b()I

    .line 93
    invoke-virtual {p0}, Lae;->b()Z

    .line 95
    :cond_0
    return-object v0
.end method

.method private af()V
    .locals 1

    .prologue
    .line 106
    invoke-virtual {p0}, Liwy;->e()V

    .line 107
    const/4 v0, 0x0

    iput-boolean v0, p0, Liwy;->Z:Z

    .line 108
    invoke-virtual {p0}, Liwy;->p()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v0

    invoke-virtual {v0, p0}, Lat;->a(Lu;)Lat;

    move-result-object v0

    invoke-virtual {v0}, Lat;->b()I

    .line 109
    return-void
.end method

.method private ag()V
    .locals 2

    .prologue
    .line 169
    const/4 v0, 0x1

    iput-boolean v0, p0, Liwy;->X:Z

    .line 170
    invoke-virtual {p0}, Liwy;->u()Z

    move-result v0

    if-nez v0, :cond_0

    .line 180
    :goto_0
    return-void

    .line 174
    :cond_0
    iget-object v0, p0, Liwy;->R:Lhov;

    new-instance v1, Liwz;

    invoke-direct {v1, p0}, Liwz;-><init>(Liwy;)V

    invoke-virtual {v0, v1}, Lhov;->a(Ljava/lang/Runnable;)Lhox;

    goto :goto_0
.end method

.method private ah()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 203
    iget-boolean v2, p0, Liwy;->Y:Z

    if-eqz v2, :cond_0

    .line 217
    :goto_0
    return v0

    .line 207
    :cond_0
    iget-object v2, p0, Liwy;->N:Lixd;

    invoke-virtual {v2}, Lixd;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 208
    goto :goto_0

    .line 211
    :cond_1
    iput-boolean v1, p0, Liwy;->Y:Z

    .line 212
    invoke-virtual {p0}, Liwy;->n()Lz;

    move-result-object v1

    .line 213
    iget-object v2, p0, Liwy;->S:Liwg;

    iget-object v2, v2, Liwg;->a:Ljava/lang/String;

    invoke-direct {p0, v2}, Liwy;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Liwy;->b(Ljava/lang/String;)V

    .line 214
    new-instance v2, Lixb;

    const-string v3, "prepare_accounts"

    iget-object v4, p0, Liwy;->N:Lixd;

    invoke-direct {v2, v1, v3, v4}, Lixb;-><init>(Landroid/content/Context;Ljava/lang/String;Lixd;)V

    .line 216
    iget-object v1, p0, Liwy;->Q:Lhoc;

    invoke-virtual {v1, v2}, Lhoc;->b(Lhny;)V

    goto :goto_0
.end method

.method private ai()V
    .locals 2

    .prologue
    .line 611
    iget-object v0, p0, Liwy;->S:Liwg;

    iget-object v0, v0, Liwg;->b:Ljava/lang/String;

    .line 612
    const-string v1, "com.google.android.libraries.social.login.LoginRequest.DEFAULT_ERROR_TOAST"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 613
    iget-object v0, p0, Liwy;->at:Llnl;

    const v1, 0x7f0a01b1

    invoke-virtual {v0, v1}, Llnl;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 615
    :cond_0
    invoke-virtual {p0, v0}, Liwy;->a(Ljava/lang/String;)V

    .line 616
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 379
    iget-object v0, p0, Liwy;->O:Lhei;

    invoke-interface {v0, p1}, Lhei;->b(Ljava/lang/String;)I

    move-result v0

    .line 380
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 381
    invoke-direct {p0}, Liwy;->ai()V

    .line 387
    :goto_0
    return-void

    .line 385
    :cond_0
    iget-object v1, p0, Liwy;->O:Lhei;

    invoke-interface {v1, v0}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 386
    invoke-virtual {p0, v0, p2}, Liwy;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 712
    if-eqz p1, :cond_0

    .line 714
    :goto_0
    return-object p1

    .line 712
    :cond_0
    iget-object v0, p0, Liwy;->at:Llnl;

    const v1, 0x7f0a01a8

    .line 714
    invoke-virtual {v0, v1}, Llnl;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private e(I)V
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 511
    .line 512
    iget-object v0, p0, Liwy;->au:Llnh;

    const-class v2, Liwr;

    invoke-virtual {v0, v2}, Llnh;->c(Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v0, v1

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwr;

    .line 513
    iget-object v3, p0, Liwy;->S:Liwg;

    invoke-interface {v0, p1}, Liwr;->a(I)I

    move-result v0

    .line 514
    if-eq v0, v1, :cond_0

    .line 515
    :cond_1
    if-eq v0, v1, :cond_2

    if-eq v0, p1, :cond_2

    .line 520
    invoke-virtual {p0, v0, p1}, Liwy;->a(II)V

    .line 530
    :goto_0
    return-void

    .line 524
    :cond_2
    iget-object v0, p0, Liwy;->S:Liwg;

    iget-boolean v0, v0, Liwg;->g:Z

    if-eqz v0, :cond_3

    .line 525
    iget-object v0, p0, Liwy;->O:Lhei;

    iget-object v1, p0, Liwy;->S:Liwg;

    iget-object v1, v1, Liwg;->e:Ljava/lang/String;

    invoke-interface {v0, v1, p1}, Lhei;->a(Ljava/lang/String;I)V

    .line 528
    :cond_3
    invoke-direct {p0}, Liwy;->af()V

    .line 529
    iget-object v0, p0, Liwy;->N:Lixd;

    iget-object v1, p0, Liwy;->S:Liwg;

    iget-object v2, p0, Liwy;->T:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, p1}, Lixd;->a(Liwg;Ljava/lang/String;I)V

    goto :goto_0
.end method


# virtual methods
.method U()V
    .locals 1

    .prologue
    .line 191
    invoke-virtual {p0}, Liwy;->u()Z

    move-result v0

    if-nez v0, :cond_1

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 195
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Liwy;->X:Z

    .line 197
    invoke-direct {p0}, Liwy;->ah()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    invoke-virtual {p0}, Liwy;->V()V

    goto :goto_0
.end method

.method V()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 230
    invoke-virtual {p0}, Liwy;->e()V

    .line 232
    invoke-virtual {p0}, Liwy;->W()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    iget-object v1, p0, Liwy;->S:Liwg;

    iget v1, v1, Liwg;->c:I

    invoke-virtual {p0, v1}, Liwy;->c(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 240
    invoke-virtual {p0}, Liwy;->Z()Z

    move-result v1

    if-nez v1, :cond_0

    .line 244
    invoke-virtual {p0}, Liwy;->aa()Z

    move-result v1

    if-nez v1, :cond_0

    .line 248
    iget-object v1, p0, Liwy;->S:Liwg;

    iget-object v1, v1, Liwg;->p:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Liwy;->S:Liwg;

    iget-object v1, v1, Liwg;->p:Ljava/lang/String;

    iget-object v3, p0, Liwy;->S:Liwg;

    iget-object v3, v3, Liwg;->n:Ljava/lang/String;

    invoke-direct {p0, v1, v3}, Liwy;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    :goto_1
    if-nez v1, :cond_0

    .line 252
    invoke-virtual {p0}, Liwy;->Y()Z

    move-result v1

    if-nez v1, :cond_0

    .line 256
    invoke-virtual {p0}, Liwy;->X()Z

    move-result v1

    if-nez v1, :cond_0

    .line 260
    iget-object v1, p0, Liwy;->S:Liwg;

    iget-object v1, v1, Liwg;->q:Ljava/lang/Class;

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Liwy;->e()V

    iget-object v0, p0, Liwy;->au:Llnh;

    iget-object v1, p0, Liwy;->S:Liwg;

    iget-object v1, v1, Liwg;->q:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Livv;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Liwy;->q()Lae;

    move-result-object v4

    invoke-virtual {v4, v3}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v1

    if-nez v1, :cond_4

    iget-object v1, p0, Liwy;->S:Liwg;

    iget-object v1, v1, Liwg;->r:Landroid/os/Bundle;

    invoke-interface {v0}, Livv;->a()Lu;

    move-result-object v0

    invoke-virtual {v0, v1}, Lu;->f(Landroid/os/Bundle;)V

    invoke-virtual {v4}, Lae;->a()Lat;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    move-result-object v1

    invoke-virtual {v1}, Lat;->b()I

    invoke-virtual {v4}, Lae;->b()Z

    :goto_2
    check-cast v0, Livw;

    iget-object v1, p0, Liwy;->S:Liwg;

    invoke-interface {v0, v1}, Livw;->a(Liwg;)V

    move v0, v2

    :cond_2
    if-nez v0, :cond_0

    .line 264
    invoke-virtual {p0}, Liwy;->c()V

    goto/16 :goto_0

    :cond_3
    move v1, v0

    .line 248
    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method W()Z
    .locals 2

    .prologue
    .line 268
    iget-object v0, p0, Liwy;->U:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Liwy;->U:Ljava/lang/String;

    iget-object v1, p0, Liwy;->V:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Liwy;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const/4 v0, 0x1

    .line 272
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method X()Z
    .locals 3

    .prologue
    .line 294
    iget-object v0, p0, Liwy;->S:Liwg;

    iget-boolean v0, v0, Liwg;->f:Z

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Liwy;->O:Lhei;

    iget-object v1, p0, Liwy;->S:Liwg;

    iget-object v1, v1, Liwg;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Lhei;->c(Ljava/lang/String;)I

    move-result v0

    .line 297
    iget-object v1, p0, Liwy;->N:Lixd;

    iget-object v2, p0, Liwy;->S:Liwg;

    invoke-virtual {v1, v2, v0}, Lixd;->a(Liwg;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 298
    invoke-virtual {p0, v0}, Liwy;->c(I)Z

    move-result v0

    .line 301
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method Y()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 305
    iget-object v2, p0, Liwy;->S:Liwg;

    iget-boolean v2, v2, Liwg;->h:Z

    if-nez v2, :cond_1

    .line 324
    :cond_0
    :goto_0
    return v0

    .line 309
    :cond_1
    iget-object v2, p0, Liwy;->S:Liwg;

    iget v2, v2, Liwg;->i:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_2

    .line 310
    iget-object v0, p0, Liwy;->S:Liwg;

    iget v0, v0, Liwg;->i:I

    invoke-virtual {p0, v0}, Liwy;->d(I)V

    move v0, v1

    .line 311
    goto :goto_0

    .line 314
    :cond_2
    iget-object v2, p0, Liwy;->S:Liwg;

    iget-object v2, v2, Liwg;->j:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 315
    iget-object v0, p0, Liwy;->S:Liwg;

    iget-object v0, v0, Liwg;->j:Ljava/lang/String;

    iget-object v2, p0, Liwy;->S:Liwg;

    iget-object v2, v2, Liwg;->k:Ljava/lang/String;

    invoke-virtual {p0, v0, v2}, Liwy;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 316
    goto :goto_0

    .line 319
    :cond_3
    iget-object v2, p0, Liwy;->S:Liwg;

    iget-object v2, v2, Liwg;->l:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 320
    iget-object v0, p0, Liwy;->S:Liwg;

    iget-object v0, v0, Liwg;->l:Ljava/lang/String;

    iget-object v2, p0, Liwy;->S:Liwg;

    iget-object v2, v2, Liwg;->k:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Liwy;->b(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 321
    goto :goto_0
.end method

.method Z()Z
    .locals 4

    .prologue
    .line 328
    iget-object v0, p0, Liwy;->S:Liwg;

    iget-object v0, v0, Liwg;->m:Ljava/lang/String;

    .line 329
    iget-object v1, p0, Liwy;->S:Liwg;

    iget-object v1, v1, Liwg;->n:Ljava/lang/String;

    .line 330
    if-eqz v0, :cond_1

    .line 331
    iget-object v2, p0, Liwy;->O:Lhei;

    invoke-interface {v2, v0, v1}, Lhei;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 332
    invoke-virtual {p0, v0, v1}, Liwy;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    :goto_0
    const/4 v0, 0x1

    .line 338
    :goto_1
    return v0

    .line 334
    :cond_0
    invoke-direct {p0}, Liwy;->ai()V

    goto :goto_0

    .line 338
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a()V
    .locals 2

    .prologue
    .line 581
    invoke-virtual {p0}, Liwy;->n()Lz;

    move-result-object v0

    const-class v1, Lher;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lher;

    .line 582
    new-instance v1, Ljava/util/HashSet;

    invoke-interface {v0}, Lher;->a()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 583
    iget-object v0, p0, Liwy;->aa:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->removeAll(Ljava/util/Collection;)Z

    .line 584
    invoke-interface {v1}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 585
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Liwy;->U:Ljava/lang/String;

    .line 586
    invoke-direct {p0}, Liwy;->ah()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 587
    iget-object v0, p0, Liwy;->U:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Liwy;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    :cond_0
    :goto_0
    return-void

    .line 590
    :cond_1
    invoke-virtual {p0}, Liwy;->c()V

    goto :goto_0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 432
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Liwy;->a(II)V

    .line 433
    return-void
.end method

.method a(II)V
    .locals 4

    .prologue
    .line 533
    iput p2, p0, Liwy;->W:I

    .line 534
    new-instance v0, Lixa;

    invoke-virtual {p0}, Liwy;->n()Lz;

    move-result-object v1

    const-string v2, "logout_during_login"

    iget-object v3, p0, Liwy;->N:Lixd;

    invoke-direct {v0, v1, v2, p1, v3}, Lixa;-><init>(Landroid/content/Context;Ljava/lang/String;ILixd;)V

    .line 536
    iget-object v1, p0, Liwy;->Q:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    .line 537
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 124
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 126
    if-eqz p1, :cond_0

    .line 127
    const-string v0, "logging_in"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Liwy;->Z:Z

    .line 128
    const-string v0, "login_pending"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Liwy;->X:Z

    .line 129
    const-string v0, "preparing_accounts"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Liwy;->Y:Z

    .line 130
    const-string v0, "account_names_snapshot"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liwy;->aa:[Ljava/lang/String;

    .line 131
    const-string v0, "login_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Liwg;

    iput-object v0, p0, Liwy;->S:Liwg;

    .line 132
    const-string v0, "tag"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liwy;->T:Ljava/lang/String;

    .line 133
    const-string v0, "selected_account_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liwy;->U:Ljava/lang/String;

    .line 134
    const-string v0, "selected_effective_gaia_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liwy;->V:Ljava/lang/String;

    .line 135
    const-string v0, "account_id_to_login"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Liwy;->W:I

    .line 137
    :cond_0
    return-void
.end method

.method public a(Liwg;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 154
    iget-boolean v0, p0, Liwy;->Z:Z

    if-eqz v0, :cond_0

    .line 166
    :goto_0
    return-void

    .line 158
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Liwy;->Z:Z

    .line 159
    iput-object p1, p0, Liwy;->S:Liwg;

    .line 160
    iput-object p2, p0, Liwy;->T:Ljava/lang/String;

    .line 161
    iput-object v1, p0, Liwy;->U:Ljava/lang/String;

    .line 162
    iput-object v1, p0, Liwy;->V:Ljava/lang/String;

    .line 163
    const/4 v0, -0x1

    iput v0, p0, Liwy;->W:I

    .line 165
    invoke-direct {p0}, Liwy;->ag()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 604
    if-eqz p1, :cond_0

    .line 605
    iget-object v0, p0, Liwy;->at:Llnl;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 607
    :cond_0
    invoke-virtual {p0}, Liwy;->c()V

    .line 608
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 560
    invoke-virtual {p3, v1}, Lhos;->a(Z)V

    .line 562
    const-string v0, "update_account"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 563
    invoke-virtual {p2}, Lhoz;->b()Ljava/lang/Exception;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Liwy;->d()V

    .line 571
    :cond_0
    :goto_0
    return-void

    .line 563
    :cond_1
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iget-object v0, p0, Liwy;->O:Lhei;

    invoke-interface {v0, v1}, Lhei;->c(I)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Liwy;->ai()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Liwy;->N:Lixd;

    iget-object v2, p0, Liwy;->S:Liwg;

    invoke-virtual {v0, v2, v1}, Lixd;->b(Liwg;I)Liwi;

    move-result-object v2

    if-eqz v2, :cond_5

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Liwy;->q()Lae;

    move-result-object v4

    invoke-virtual {v4, v3}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    if-nez v0, :cond_3

    invoke-interface {v2}, Liwi;->a()Lu;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v4}, Lae;->a()Lat;

    move-result-object v2

    invoke-virtual {v2, v0, v3}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    move-result-object v2

    invoke-virtual {v2}, Lat;->b()I

    invoke-virtual {v4}, Lae;->b()Z

    :cond_3
    check-cast v0, Liwj;

    if-eqz v0, :cond_4

    invoke-interface {v0, v1}, Liwj;->a(I)V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Liwy;->ai()V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Liwy;->S:Liwg;

    iget-boolean v0, v0, Liwg;->d:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Liwy;->O:Lhei;

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v2, "logged_in"

    invoke-interface {v0, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-direct {p0}, Liwy;->ai()V

    goto :goto_0

    :cond_6
    invoke-direct {p0, v1}, Liwy;->e(I)V

    goto :goto_0

    .line 564
    :cond_7
    const-string v0, "prepare_accounts"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 565
    iput-boolean v1, p0, Liwy;->Y:Z

    if-eqz p2, :cond_8

    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_9

    :cond_8
    invoke-virtual {p0}, Liwy;->ae()V

    goto/16 :goto_0

    :cond_9
    invoke-virtual {p0}, Liwy;->V()V

    goto/16 :goto_0

    .line 566
    :cond_a
    const-string v0, "logout_during_login"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 567
    iget v0, p0, Liwy;->W:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_b

    iget v0, p0, Liwy;->W:I

    invoke-direct {p0, v0}, Liwy;->e(I)V

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p0}, Liwy;->c()V

    goto/16 :goto_0

    .line 568
    :cond_c
    const-string v0, "logout"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 569
    invoke-direct {p0}, Liwy;->af()V

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 420
    iput-object p1, p0, Liwy;->U:Ljava/lang/String;

    .line 421
    iput-object p2, p0, Liwy;->V:Ljava/lang/String;

    .line 423
    iget-object v0, p0, Liwy;->O:Lhei;

    invoke-interface {v0, p1, p2}, Lhei;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iget-object v1, p0, Liwy;->O:Lhei;

    invoke-interface {v1, v0}, Lhei;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Liwy;->N:Lixd;

    iget-object v2, p0, Liwy;->S:Liwg;

    invoke-virtual {v1, v2, v0}, Lixd;->a(Liwg;I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0}, Liwy;->e(I)V

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 428
    :goto_1
    return-void

    .line 423
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 427
    :cond_1
    invoke-virtual {p0}, Liwy;->ab()V

    goto :goto_1
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 184
    invoke-super {p0}, Llol;->aO_()V

    .line 185
    iget-boolean v0, p0, Liwy;->X:Z

    if-eqz v0, :cond_0

    .line 186
    invoke-direct {p0}, Liwy;->ag()V

    .line 188
    :cond_0
    return-void
.end method

.method aa()Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 342
    iget-object v1, p0, Liwy;->S:Liwg;

    iget-object v3, v1, Liwg;->o:Ljava/lang/String;

    .line 344
    if-eqz v3, :cond_0

    .line 345
    iget-object v1, p0, Liwy;->O:Lhei;

    invoke-interface {v1}, Lhei;->a()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    const/4 v1, -0x1

    move v2, v0

    :goto_0
    if-ge v2, v5, :cond_3

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v6, p0, Liwy;->O:Lhei;

    invoke-interface {v6, v0}, Lhei;->a(I)Lhej;

    move-result-object v6

    const-string v7, "email_gaia_id"

    invoke-interface {v6, v7}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v6}, Lhej;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 346
    :goto_1
    invoke-virtual {p0, v0}, Liwy;->d(I)V

    .line 347
    const/4 v0, 0x1

    .line 349
    :cond_0
    return v0

    :cond_1
    move v0, v1

    .line 345
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method ab()V
    .locals 7

    .prologue
    .line 447
    invoke-virtual {p0}, Liwy;->n()Lz;

    move-result-object v1

    .line 448
    invoke-virtual {p0}, Liwy;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Lixf;->b(Lz;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 449
    iget-object v0, p0, Liwy;->S:Liwg;

    iget-object v0, v0, Liwg;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Liwy;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Liwy;->b(Ljava/lang/String;)V

    .line 452
    :cond_0
    new-instance v0, Lixc;

    const-string v2, "update_account"

    iget-object v3, p0, Liwy;->U:Ljava/lang/String;

    iget-object v4, p0, Liwy;->V:Ljava/lang/String;

    iget-object v5, p0, Liwy;->N:Lixd;

    iget-object v6, p0, Liwy;->S:Liwg;

    invoke-direct/range {v0 .. v6}, Lixc;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lixd;Liwg;)V

    .line 457
    iget-object v1, p0, Liwy;->Q:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    .line 458
    return-void
.end method

.method ac()V
    .locals 0

    .prologue
    .line 638
    invoke-virtual {p0}, Liwy;->U()V

    .line 639
    return-void
.end method

.method ad()V
    .locals 0

    .prologue
    .line 642
    invoke-virtual {p0}, Liwy;->c()V

    .line 643
    return-void
.end method

.method ae()V
    .locals 1

    .prologue
    .line 646
    invoke-virtual {p0}, Liwy;->e()V

    .line 647
    invoke-virtual {p0}, Liwy;->p()Lae;

    move-result-object v0

    invoke-static {v0}, Lixg;->a(Lae;)V

    .line 648
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 575
    iget-object v0, p0, Liwy;->P:Lher;

    invoke-interface {v0}, Lher;->a()[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liwy;->aa:[Ljava/lang/String;

    .line 576
    iget-object v0, p0, Liwy;->P:Lher;

    invoke-interface {v0, p0}, Lher;->a(Lu;)V

    .line 577
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 625
    invoke-virtual {p0}, Liwy;->n()Lz;

    move-result-object v0

    invoke-static {v0, p1}, Lixf;->a(Lz;Ljava/lang/String;)V

    .line 626
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 596
    iget-boolean v0, p0, Liwy;->Z:Z

    if-eqz v0, :cond_0

    .line 597
    invoke-direct {p0}, Liwy;->af()V

    .line 598
    iget-object v0, p0, Liwy;->N:Lixd;

    iget-object v1, p0, Liwy;->S:Liwg;

    iget-object v2, p0, Liwy;->T:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lixd;->a(Liwg;Ljava/lang/String;)V

    .line 600
    :cond_0
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 113
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 114
    iget-object v0, p0, Liwy;->au:Llnh;

    const-class v1, Liwc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lixd;

    iput-object v0, p0, Liwy;->N:Lixd;

    .line 115
    iget-object v0, p0, Liwy;->au:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Liwy;->O:Lhei;

    .line 116
    iget-object v0, p0, Liwy;->au:Llnh;

    const-class v1, Lihf;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    .line 117
    iget-object v0, p0, Liwy;->au:Llnh;

    const-class v1, Lher;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lher;

    iput-object v0, p0, Liwy;->P:Lher;

    .line 119
    iget-object v0, p0, Liwy;->au:Llnh;

    const-class v1, Liwa;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 120
    return-void
.end method

.method c(I)Z
    .locals 1

    .prologue
    .line 276
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 277
    invoke-virtual {p0, p1}, Liwy;->d(I)V

    .line 278
    const/4 v0, 0x1

    .line 280
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 620
    invoke-virtual {p0}, Liwy;->ae()V

    .line 621
    return-void
.end method

.method d(I)V
    .locals 3

    .prologue
    .line 284
    iget-object v0, p0, Liwy;->O:Lhei;

    invoke-interface {v0, p1}, Lhei;->c(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 285
    invoke-direct {p0}, Liwy;->ai()V

    .line 291
    :goto_0
    return-void

    .line 288
    :cond_0
    iget-object v0, p0, Liwy;->O:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 289
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "effective_gaia_id"

    .line 290
    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 289
    invoke-virtual {p0, v1, v0}, Liwy;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 630
    invoke-virtual {p0}, Liwy;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Lixf;->a(Lz;)V

    .line 631
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 141
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 142
    const-string v0, "logging_in"

    iget-boolean v1, p0, Liwy;->Z:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 143
    const-string v0, "login_pending"

    iget-boolean v1, p0, Liwy;->X:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 144
    const-string v0, "preparing_accounts"

    iget-boolean v1, p0, Liwy;->Y:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 145
    const-string v0, "login_request"

    iget-object v1, p0, Liwy;->S:Liwg;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 146
    const-string v0, "tag"

    iget-object v1, p0, Liwy;->T:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    const-string v0, "selected_account_name"

    iget-object v1, p0, Liwy;->U:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    const-string v0, "selected_effective_gaia_id"

    iget-object v1, p0, Liwy;->V:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const-string v0, "account_id_to_login"

    iget v1, p0, Liwy;->W:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 150
    const-string v0, "account_names_snapshot"

    iget-object v1, p0, Liwy;->aa:[Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 151
    return-void
.end method
