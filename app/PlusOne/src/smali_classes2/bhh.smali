.class public final Lbhh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcgk;


# instance fields
.field private final a:Lbma;

.field private final b:[Lbiv;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcgk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lbma;)V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string v0, "sequence"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbma;

    iput-object v0, p0, Lbhh;->a:Lbma;

    .line 27
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbhh;->c:Ljava/util/List;

    .line 28
    invoke-interface {p1}, Lbma;->a()I

    move-result v0

    new-array v0, v0, [Lbiv;

    iput-object v0, p0, Lbhh;->b:[Lbiv;

    .line 29
    return-void
.end method


# virtual methods
.method public a(J)I
    .locals 13

    .prologue
    .line 59
    iget-object v0, p0, Lbhh;->a:Lbma;

    invoke-interface {v0}, Lbma;->a()I

    move-result v6

    .line 62
    const-wide/16 v2, 0x0

    .line 63
    const/4 v0, 0x0

    move-wide v4, v2

    move v2, v0

    :goto_0
    if-ge v2, v6, :cond_1

    .line 64
    iget-object v0, p0, Lbhh;->a:Lbma;

    invoke-interface {v0, v2}, Lbma;->a(I)Lbmd;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lbhh;->a:Lbma;

    invoke-interface {v1, v2}, Lbma;->b(I)J

    move-result-wide v8

    invoke-virtual {v0}, Lbmd;->b()J

    move-result-wide v10

    add-long/2addr v8, v10

    cmp-long v1, p1, v8

    if-lez v1, :cond_0

    .line 66
    invoke-virtual {v0}, Lbmd;->b()J

    move-result-wide v0

    add-long/2addr v0, v4

    .line 63
    :goto_1
    add-int/lit8 v2, v2, 0x1

    move-wide v4, v0

    goto :goto_0

    .line 67
    :cond_0
    iget-object v0, p0, Lbhh;->a:Lbma;

    invoke-interface {v0, v2}, Lbma;->b(I)J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-lez v0, :cond_1

    .line 68
    iget-object v0, p0, Lbhh;->a:Lbma;

    invoke-interface {v0, v2}, Lbma;->b(I)J

    move-result-wide v0

    sub-long v0, p1, v0

    add-long/2addr v0, v4

    goto :goto_1

    .line 74
    :cond_1
    const/4 v1, 0x0

    .line 75
    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v6, :cond_2

    .line 76
    iget-object v0, p0, Lbhh;->a:Lbma;

    invoke-interface {v0, v2}, Lbma;->a(I)Lbmd;

    move-result-object v0

    iget-object v0, v0, Lbmd;->f:Lbmp;

    iget-wide v8, v0, Lbmp;->b:J

    .line 77
    iget-object v0, p0, Lbhh;->a:Lbma;

    invoke-interface {v0, v2}, Lbma;->c(I)J

    move-result-wide v10

    sub-long v8, v10, v8

    .line 78
    iget-object v0, p0, Lbhh;->b:[Lbiv;

    aget-object v0, v0, v2

    .line 81
    if-eqz v0, :cond_7

    .line 82
    const-wide/16 v10, 0x0

    sub-long v8, v4, v8

    invoke-static {v10, v11, v8, v9}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    invoke-interface {v0, v8, v9}, Lbhs;->a(J)Z

    move-result v0

    or-int/2addr v0, v1

    .line 75
    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_2

    .line 86
    :cond_2
    iget-object v0, p0, Lbhh;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgk;

    .line 87
    invoke-interface {v0}, Lcgk;->a()V

    goto :goto_4

    .line 90
    :cond_3
    if-nez v1, :cond_5

    .line 91
    iget-object v0, p0, Lbhh;->a:Lbma;

    invoke-interface {v0}, Lbma;->a()I

    move-result v0

    .line 104
    :cond_4
    :goto_5
    return v0

    .line 95
    :cond_5
    const/4 v0, 0x0

    :goto_6
    if-ge v0, v6, :cond_6

    .line 96
    iget-object v1, p0, Lbhh;->a:Lbma;

    invoke-interface {v1, v0}, Lbma;->a(I)Lbmd;

    move-result-object v1

    .line 99
    iget-object v2, p0, Lbhh;->a:Lbma;

    invoke-interface {v2, v0}, Lbma;->b(I)J

    move-result-wide v2

    sub-long v2, p1, v2

    invoke-virtual {v1}, Lbmd;->b()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-ltz v1, :cond_4

    .line 95
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 104
    :cond_6
    iget-object v0, p0, Lbhh;->a:Lbma;

    invoke-interface {v0}, Lbma;->a()I

    move-result v0

    goto :goto_5

    :cond_7
    move v0, v1

    goto :goto_3
.end method

.method public a()V
    .locals 4

    .prologue
    .line 109
    iget-object v1, p0, Lbhh;->b:[Lbiv;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 110
    invoke-static {v3}, Lcgl;->a(Lcgk;)V

    .line 109
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 112
    :cond_0
    iget-object v0, p0, Lbhh;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgk;

    .line 113
    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    goto :goto_1

    .line 115
    :cond_1
    return-void
.end method

.method public a(ILbiv;)V
    .locals 3

    .prologue
    .line 38
    iget-object v1, p0, Lbhh;->b:[Lbiv;

    const-string v0, "source"

    const/4 v2, 0x0

    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbiv;

    aput-object v0, v1, p1

    .line 39
    return-void
.end method

.method public a(Lcgk;)V
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Lbhh;->c:Ljava/util/List;

    const-string v1, "releaseable"

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 48
    return-void
.end method
