.class final Lacn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(II)V
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 191
    new-array v0, v2, [I

    .line 192
    const v1, 0x8b87

    invoke-static {p1, v1, v0, v4}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    .line 194
    new-array v7, v2, [I

    .line 195
    new-array v5, v2, [I

    .line 196
    aget v1, v0, v4

    new-array v9, v1, [B

    .line 197
    new-array v3, v2, [I

    .line 199
    aget v2, v0, v4

    move v0, p1

    move v1, p2

    move v6, v4

    move v8, v4

    move v10, v4

    invoke-static/range {v0 .. v10}, Landroid/opengl/GLES20;->glGetActiveUniform(III[II[II[II[BI)V

    .line 200
    new-instance v0, Ljava/lang/String;

    invoke-static {v9}, Lacm;->a([B)I

    move-result v1

    invoke-direct {v0, v9, v4, v1}, Ljava/lang/String;-><init>([BII)V

    iput-object v0, p0, Lacn;->a:Ljava/lang/String;

    .line 201
    iget-object v0, p0, Lacn;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lacn;->b:I

    .line 202
    aget v0, v7, v4

    iput v0, p0, Lacn;->c:I

    .line 203
    aget v0, v5, v4

    iput v0, p0, Lacn;->d:I

    .line 204
    const-string v0, "Initializing uniform"

    invoke-static {v0}, Labi;->a(Ljava/lang/String;)V

    .line 205
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lacn;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 212
    iget v0, p0, Lacn;->c:I

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 216
    iget v0, p0, Lacn;->b:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 220
    iget v0, p0, Lacn;->d:I

    return v0
.end method
