.class final Laco;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:I

.field private c:Z

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:Ljava/nio/FloatBuffer;


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    iput-object p1, p0, Laco;->a:Ljava/lang/String;

    .line 92
    iput p2, p0, Laco;->b:I

    .line 93
    const/4 v0, -0x1

    iput v0, p0, Laco;->g:I

    .line 94
    return-void
.end method


# virtual methods
.method public a(ZIII[F)V
    .locals 2

    .prologue
    .line 97
    iput-boolean p1, p0, Laco;->c:Z

    .line 99
    iput p2, p0, Laco;->d:I

    .line 100
    iput p3, p0, Laco;->e:I

    .line 101
    iput p4, p0, Laco;->f:I

    .line 102
    iget v0, p0, Laco;->g:I

    array-length v1, p5

    if-eq v0, v1, :cond_0

    .line 104
    array-length v0, p5

    shl-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Laco;->h:Ljava/nio/FloatBuffer;

    .line 105
    array-length v0, p5

    iput v0, p0, Laco;->g:I

    .line 107
    :cond_0
    iget-object v0, p0, Laco;->h:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, p5}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 108
    return-void
.end method

.method public a()Z
    .locals 6

    .prologue
    const v1, 0x8892

    const/4 v5, 0x0

    .line 123
    iget-object v0, p0, Laco;->h:Ljava/nio/FloatBuffer;

    if-eqz v0, :cond_0

    .line 146
    invoke-static {v1, v5}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 147
    iget v0, p0, Laco;->b:I

    iget v1, p0, Laco;->e:I

    iget v2, p0, Laco;->f:I

    iget-boolean v3, p0, Laco;->c:Z

    iget v4, p0, Laco;->d:I

    iget-object v5, p0, Laco;->h:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 162
    :goto_0
    iget v0, p0, Laco;->b:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 164
    const-string v0, "Set vertex-attribute values"

    invoke-static {v0}, Labi;->a(Ljava/lang/String;)V

    .line 165
    const/4 v0, 0x1

    return v0

    .line 154
    :cond_0
    invoke-static {v1, v5}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    .line 155
    iget v0, p0, Laco;->b:I

    iget v1, p0, Laco;->e:I

    iget v2, p0, Laco;->f:I

    iget-boolean v3, p0, Laco;->c:Z

    iget v4, p0, Laco;->d:I

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Laco;->a:Ljava/lang/String;

    return-object v0
.end method
