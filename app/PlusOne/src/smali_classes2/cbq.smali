.class final Lcbq;
.super Landroid/util/Property;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/Property",
        "<",
        "Lcbk;",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[F


# instance fields
.field private b:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 683
    const/16 v0, 0xa

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcbq;->a:[F

    return-void

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x3dcccccd    # 0.1f
        0x3f800000    # 1.0f
        0x3f000000    # 0.5f
        0x3dcccccd    # 0.1f
        0x3f19999a    # 0.6f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3dcccccd    # 0.1f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 694
    const-class v0, Ljava/lang/Float;

    const-string v1, "progress"

    invoke-direct {p0, v0, v1}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    .line 695
    return-void
.end method

.method private a(I)F
    .locals 3

    .prologue
    .line 748
    sget-object v0, Lcbq;->a:[F

    shl-int/lit8 v1, p1, 0x1

    aget v0, v0, v1

    .line 749
    sget-object v1, Lcbq;->a:[F

    shl-int/lit8 v2, p1, 0x1

    add-int/lit8 v2, v2, 0x1

    aget v1, v1, v2

    .line 750
    iget v2, p0, Lcbq;->b:F

    invoke-static {v0, v1, v2}, Lcfn;->c(FFF)F

    move-result v0

    return v0
.end method


# virtual methods
.method public a()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 744
    iget v0, p0, Lcbq;->b:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcbk;Ljava/lang/Float;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 699
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcbq;->b:F

    .line 701
    invoke-static {p1}, Lcbk;->b(Lcbk;)Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a()Z

    move-result v2

    .line 703
    invoke-static {p1}, Lcbk;->d(Lcbk;)Landroid/widget/TextView;

    move-result-object v3

    .line 704
    invoke-static {p1}, Lcbk;->b(Lcbk;)Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    move-result-object v4

    .line 705
    invoke-static {p1}, Lcbk;->e(Lcbk;)Landroid/view/View;

    move-result-object v5

    .line 706
    invoke-static {p1}, Lcbk;->f(Lcbk;)Landroid/widget/TextView;

    move-result-object v6

    .line 709
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcbq;->a(I)F

    move-result v7

    .line 714
    if-eqz v2, :cond_0

    .line 715
    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v0

    invoke-virtual {v4}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->getRight()I

    move-result v1

    invoke-static {v0, v1, v7}, Lcfn;->a(IIF)I

    move-result v0

    move v1, v0

    .line 717
    :goto_0
    if-eqz v2, :cond_1

    .line 718
    invoke-virtual {p1}, Lcbk;->getHeight()I

    move-result v0

    .line 720
    :goto_1
    int-to-float v2, v1

    int-to-float v7, v0

    invoke-static {p1, v8, v8, v2, v7}, Lcbk;->a(Lcbk;FFFF)V

    .line 724
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcbq;->a(I)F

    move-result v2

    .line 725
    invoke-virtual {v4}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 726
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-static {v0, v1, v2}, Lcfn;->a(IIF)I

    move-result v0

    .line 727
    int-to-float v1, v0

    invoke-virtual {v5, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 728
    int-to-float v0, v0

    invoke-virtual {v3, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 729
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcbq;->a(I)F

    move-result v0

    invoke-virtual {v6, v0}, Landroid/view/View;->setAlpha(F)V

    .line 737
    :goto_2
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcbq;->a(I)F

    move-result v0

    invoke-virtual {v3, v0}, Landroid/view/View;->setAlpha(F)V

    .line 739
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcbq;->b:F

    .line 740
    return-void

    .line 716
    :cond_0
    invoke-virtual {p1}, Lcbk;->getWidth()I

    move-result v0

    move v1, v0

    goto :goto_0

    .line 719
    :cond_1
    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v0

    invoke-virtual {v4}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->getBottom()I

    move-result v2

    invoke-static {v0, v2, v7}, Lcfn;->a(IIF)I

    move-result v0

    goto :goto_1

    .line 731
    :cond_2
    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v1

    invoke-static {v1, v0, v2}, Lcfn;->a(IIF)I

    move-result v0

    .line 732
    int-to-float v1, v0

    invoke-virtual {v5, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 733
    int-to-float v0, v0

    invoke-virtual {v3, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 734
    invoke-virtual {v4}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->getTop()I

    move-result v0

    neg-int v0, v0

    int-to-float v0, v0

    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcbq;->a(I)F

    move-result v1

    mul-float/2addr v0, v1

    invoke-virtual {v4, v0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->setTranslationY(F)V

    goto :goto_2
.end method

.method public synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 675
    invoke-virtual {p0}, Lcbq;->a()Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 675
    check-cast p1, Lcbk;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p0, p1, p2}, Lcbq;->a(Lcbk;Ljava/lang/Float;)V

    return-void
.end method
