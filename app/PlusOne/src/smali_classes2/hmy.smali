.class public final Lhmy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhms;


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lhmy;->a:Landroid/content/Context;

    .line 40
    return-void
.end method

.method static synthetic a(Lhmy;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lhmy;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public a(I)Lkff;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lhmy;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lhnb;->a(Landroid/content/Context;I)Lkff;

    move-result-object v0

    return-object v0
.end method

.method public a(Lhmr;)V
    .locals 18

    .prologue
    .line 44
    invoke-virtual/range {p1 .. p1}, Lhmr;->h()Landroid/content/Context;

    move-result-object v3

    .line 46
    const-class v2, Lhmq;

    .line 47
    invoke-static {v3, v2}, Llnh;->d(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v2

    .line 48
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhmq;

    .line 49
    invoke-virtual/range {p1 .. p1}, Lhmr;->c()Lhmw;

    move-result-object v5

    if-nez v5, :cond_0

    .line 50
    invoke-interface {v2}, Lhmq;->F_()Lhmw;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lhmr;->a(Lhmw;)Lhmr;

    .line 52
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lhmr;->g()Landroid/os/Bundle;

    move-result-object v5

    invoke-interface {v2, v5}, Lhmq;->b(Landroid/os/Bundle;)V

    goto :goto_0

    .line 55
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lhmr;->b()I

    move-result v2

    const/4 v4, -0x1

    if-ne v2, v4, :cond_2

    .line 56
    const-class v2, Lhee;

    invoke-static {v3, v2}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhee;

    .line 57
    if-eqz v2, :cond_2

    .line 58
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    .line 59
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lhmr;->a(I)Lhmr;

    .line 63
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lhmr;->b()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_4

    .line 105
    :cond_3
    :goto_1
    return-void

    .line 67
    :cond_4
    invoke-virtual/range {p1 .. p1}, Lhmr;->e()Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 68
    invoke-virtual/range {p1 .. p1}, Lhmr;->e()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 70
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lhmr;->f()Ljava/lang/Long;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 71
    invoke-virtual/range {p1 .. p1}, Lhmr;->f()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 76
    :goto_3
    invoke-virtual/range {p1 .. p1}, Lhmr;->a()Lhmv;

    move-result-object v3

    .line 77
    invoke-virtual/range {p1 .. p1}, Lhmr;->c()Lhmw;

    move-result-object v4

    .line 78
    invoke-virtual/range {p1 .. p1}, Lhmr;->d()Lhmw;

    move-result-object v5

    .line 81
    invoke-virtual/range {p1 .. p1}, Lhmr;->g()Landroid/os/Bundle;

    move-result-object v10

    .line 75
    invoke-static/range {v3 .. v10}, Lhmt;->a(Lhmv;Lhmw;Lhmw;JJLandroid/os/Bundle;)Lmth;

    move-result-object v3

    .line 83
    const-string v2, "EsAnalytics"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 84
    invoke-virtual/range {p1 .. p1}, Lhmr;->a()Lhmv;

    move-result-object v2

    invoke-static {v2}, Lhmv;->a(Lhmv;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 85
    invoke-virtual/range {p1 .. p1}, Lhmr;->c()Lhmw;

    move-result-object v2

    invoke-static {v2}, Lhmw;->a(Lhmw;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 86
    invoke-virtual/range {p1 .. p1}, Lhmr;->d()Lhmw;

    move-result-object v2

    invoke-static {v2}, Lhmw;->a(Lhmw;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 89
    invoke-virtual/range {p1 .. p1}, Lhmr;->g()Landroid/os/Bundle;

    move-result-object v11

    if-nez v11, :cond_8

    const-string v2, "none"

    :goto_4
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, 0x71

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v12, v13

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v12, v13

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v12, v13

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v12, "recordEvent action: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v11, " startView: "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " endView: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " startTime: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " endTime: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " extras: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 84
    :cond_5
    invoke-virtual/range {p1 .. p1}, Lhmr;->b()I

    move-result v2

    .line 93
    const/4 v4, -0x1

    if-eq v2, v4, :cond_3

    .line 94
    invoke-static {}, Llsx;->a()Z

    move-result v4

    if-eqz v4, :cond_a

    .line 95
    move-object/from16 v0, p0

    iget-object v4, v0, Lhmy;->a:Landroid/content/Context;

    invoke-static {v4, v2, v3}, Lhnb;->a(Landroid/content/Context;ILmth;)V

    goto/16 :goto_1

    .line 69
    :cond_6
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    goto/16 :goto_2

    :cond_7
    move-wide v8, v6

    .line 71
    goto/16 :goto_3

    .line 89
    :cond_8
    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_5
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v11, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v16

    add-int/lit8 v16, v16, 0x3

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/String;->length()I

    move-result v17

    add-int v16, v16, v17

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v16, "("

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v15, ":"

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v14, ")"

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    :cond_9
    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_4

    .line 97
    :cond_a
    new-instance v4, Lhmz;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v2, v3}, Lhmz;-><init>(Lhmy;ILmth;)V

    invoke-static {v4}, Llsx;->a(Ljava/lang/Runnable;)V

    goto/16 :goto_1
.end method
