.class public final Lbua;
.super Loxu;
.source "PG"


# instance fields
.field public a:J

.field public b:[Lbub;

.field public c:[Lbuc;

.field public d:Lbud;

.field public e:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 10
    invoke-direct {p0}, Loxu;-><init>()V

    .line 13
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbua;->a:J

    .line 16
    sget-object v0, Lbub;->a:[Lbub;

    iput-object v0, p0, Lbua;->b:[Lbub;

    .line 19
    sget-object v0, Lbuc;->a:[Lbuc;

    iput-object v0, p0, Lbua;->c:[Lbuc;

    .line 22
    const/4 v0, 0x0

    iput-object v0, p0, Lbua;->d:Lbud;

    .line 25
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbua;->e:Z

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 67
    .line 68
    iget-wide v2, p0, Lbua;->a:J

    const-wide/16 v4, -0x1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_6

    .line 69
    const/4 v0, 0x1

    iget-wide v2, p0, Lbua;->a:J

    .line 70
    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 72
    :goto_0
    iget-object v2, p0, Lbua;->b:[Lbub;

    if-eqz v2, :cond_1

    .line 73
    iget-object v3, p0, Lbua;->b:[Lbub;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 74
    if-eqz v5, :cond_0

    .line 75
    const/4 v6, 0x2

    .line 76
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 73
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 80
    :cond_1
    iget-object v2, p0, Lbua;->c:[Lbuc;

    if-eqz v2, :cond_3

    .line 81
    iget-object v2, p0, Lbua;->c:[Lbuc;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 82
    if-eqz v4, :cond_2

    .line 83
    const/4 v5, 0x3

    .line 84
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 81
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 88
    :cond_3
    iget-object v1, p0, Lbua;->d:Lbud;

    if-eqz v1, :cond_4

    .line 89
    const/4 v1, 0x4

    iget-object v2, p0, Lbua;->d:Lbud;

    .line 90
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    :cond_4
    iget-boolean v1, p0, Lbua;->e:Z

    if-eqz v1, :cond_5

    .line 93
    const/4 v1, 0x5

    iget-boolean v2, p0, Lbua;->e:Z

    .line 94
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 96
    :cond_5
    iput v0, p0, Lbua;->ai:I

    .line 97
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lbua;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 105
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 106
    sparse-switch v0, :sswitch_data_0

    .line 110
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 111
    :sswitch_0
    return-object p0

    .line 116
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    iput-wide v2, p0, Lbua;->a:J

    goto :goto_0

    .line 120
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 121
    iget-object v0, p0, Lbua;->b:[Lbub;

    if-nez v0, :cond_2

    move v0, v1

    .line 122
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lbub;

    .line 123
    iget-object v3, p0, Lbua;->b:[Lbub;

    if-eqz v3, :cond_1

    .line 124
    iget-object v3, p0, Lbua;->b:[Lbub;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 126
    :cond_1
    iput-object v2, p0, Lbua;->b:[Lbub;

    .line 127
    :goto_2
    iget-object v2, p0, Lbua;->b:[Lbub;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_3

    .line 128
    iget-object v2, p0, Lbua;->b:[Lbub;

    new-instance v3, Lbub;

    invoke-direct {v3}, Lbub;-><init>()V

    aput-object v3, v2, v0

    .line 129
    iget-object v2, p0, Lbua;->b:[Lbub;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 130
    invoke-virtual {p1}, Loxn;->a()I

    .line 127
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 121
    :cond_2
    iget-object v0, p0, Lbua;->b:[Lbub;

    array-length v0, v0

    goto :goto_1

    .line 133
    :cond_3
    iget-object v2, p0, Lbua;->b:[Lbub;

    new-instance v3, Lbub;

    invoke-direct {v3}, Lbub;-><init>()V

    aput-object v3, v2, v0

    .line 134
    iget-object v2, p0, Lbua;->b:[Lbub;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 138
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 139
    iget-object v0, p0, Lbua;->c:[Lbuc;

    if-nez v0, :cond_5

    move v0, v1

    .line 140
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lbuc;

    .line 141
    iget-object v3, p0, Lbua;->c:[Lbuc;

    if-eqz v3, :cond_4

    .line 142
    iget-object v3, p0, Lbua;->c:[Lbuc;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 144
    :cond_4
    iput-object v2, p0, Lbua;->c:[Lbuc;

    .line 145
    :goto_4
    iget-object v2, p0, Lbua;->c:[Lbuc;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 146
    iget-object v2, p0, Lbua;->c:[Lbuc;

    new-instance v3, Lbuc;

    invoke-direct {v3}, Lbuc;-><init>()V

    aput-object v3, v2, v0

    .line 147
    iget-object v2, p0, Lbua;->c:[Lbuc;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 148
    invoke-virtual {p1}, Loxn;->a()I

    .line 145
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 139
    :cond_5
    iget-object v0, p0, Lbua;->c:[Lbuc;

    array-length v0, v0

    goto :goto_3

    .line 151
    :cond_6
    iget-object v2, p0, Lbua;->c:[Lbuc;

    new-instance v3, Lbuc;

    invoke-direct {v3}, Lbuc;-><init>()V

    aput-object v3, v2, v0

    .line 152
    iget-object v2, p0, Lbua;->c:[Lbuc;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 156
    :sswitch_4
    iget-object v0, p0, Lbua;->d:Lbud;

    if-nez v0, :cond_7

    .line 157
    new-instance v0, Lbud;

    invoke-direct {v0}, Lbud;-><init>()V

    iput-object v0, p0, Lbua;->d:Lbud;

    .line 159
    :cond_7
    iget-object v0, p0, Lbua;->d:Lbud;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 163
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    iput-boolean v0, p0, Lbua;->e:Z

    goto/16 :goto_0

    .line 106
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 40
    iget-wide v2, p0, Lbua;->a:J

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 41
    const/4 v1, 0x1

    iget-wide v2, p0, Lbua;->a:J

    invoke-virtual {p1, v1, v2, v3}, Loxo;->b(IJ)V

    .line 43
    :cond_0
    iget-object v1, p0, Lbua;->b:[Lbub;

    if-eqz v1, :cond_2

    .line 44
    iget-object v2, p0, Lbua;->b:[Lbub;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 45
    if-eqz v4, :cond_1

    .line 46
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 44
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 50
    :cond_2
    iget-object v1, p0, Lbua;->c:[Lbuc;

    if-eqz v1, :cond_4

    .line 51
    iget-object v1, p0, Lbua;->c:[Lbuc;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_4

    aget-object v3, v1, v0

    .line 52
    if-eqz v3, :cond_3

    .line 53
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 51
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 57
    :cond_4
    iget-object v0, p0, Lbua;->d:Lbud;

    if-eqz v0, :cond_5

    .line 58
    const/4 v0, 0x4

    iget-object v1, p0, Lbua;->d:Lbud;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 60
    :cond_5
    iget-boolean v0, p0, Lbua;->e:Z

    if-eqz v0, :cond_6

    .line 61
    const/4 v0, 0x5

    iget-boolean v1, p0, Lbua;->e:Z

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 63
    :cond_6
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lbua;->a(Loxn;)Lbua;

    move-result-object v0

    return-object v0
.end method
