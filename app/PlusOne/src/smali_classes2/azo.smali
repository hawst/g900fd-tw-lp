.class public final Lazo;
.super Layr;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 20
    new-instance v0, Lays;

    invoke-direct {v0}, Lays;-><init>()V

    const v1, 0x7f02042e

    .line 21
    invoke-virtual {v0, v1}, Lays;->a(I)Lays;

    move-result-object v0

    const v1, 0x7f02058c

    .line 22
    invoke-virtual {v0, v1}, Lays;->b(I)Lays;

    move-result-object v0

    const/high16 v1, 0x3f000000    # 0.5f

    .line 23
    invoke-virtual {v0, v1}, Lays;->a(F)Lays;

    move-result-object v0

    .line 24
    invoke-virtual {v0, v2}, Lays;->c(I)Lays;

    move-result-object v0

    const v1, 0x7f0205d0

    .line 25
    invoke-virtual {v0, v1}, Lays;->d(I)Lays;

    move-result-object v0

    const v1, 0x3f19999a    # 0.6f

    .line 26
    invoke-virtual {v0, v1}, Lays;->b(F)Lays;

    move-result-object v0

    .line 27
    invoke-virtual {v0, v2}, Lays;->e(I)Lays;

    move-result-object v0

    .line 20
    invoke-direct {p0, v0}, Layr;-><init>(Lays;)V

    .line 29
    return-void
.end method


# virtual methods
.method protected a(Layj;Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;
    .locals 4

    .prologue
    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 46
    new-instance v0, Layu;

    invoke-direct {v0}, Layu;-><init>()V

    .line 47
    invoke-virtual {v0, p1}, Layu;->a(Layj;)Layu;

    move-result-object v0

    .line 48
    invoke-virtual {v0, v3}, Layu;->e(I)Layu;

    move-result-object v0

    .line 49
    invoke-virtual {v0, v2}, Layu;->f(I)Layu;

    move-result-object v0

    const/high16 v1, 0x42300000    # 44.0f

    .line 50
    invoke-virtual {v0, v1}, Layu;->b(F)Layu;

    move-result-object v0

    const v1, 0x7f0b00a9

    .line 51
    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Layu;->a(I)Layu;

    move-result-object v0

    const/high16 v1, 0x42100000    # 36.0f

    .line 52
    invoke-virtual {v0, v1}, Layu;->a(F)Layu;

    move-result-object v0

    const-string v1, "sans-serif"

    .line 53
    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Layu;->a(Landroid/graphics/Typeface;)Layu;

    move-result-object v0

    const/4 v1, 0x0

    .line 54
    invoke-virtual {v0, v1}, Layu;->a(Z)Layu;

    move-result-object v0

    const/16 v1, 0xc8

    .line 55
    invoke-virtual {v0, v1}, Layu;->c(I)Layu;

    move-result-object v0

    .line 56
    invoke-virtual {v0, v3}, Layu;->b(Z)Layu;

    move-result-object v0

    const v1, 0x7f0b00b2

    .line 57
    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Layu;->g(I)Layu;

    move-result-object v0

    .line 58
    new-instance v1, Lazp;

    const v2, 0x7f02058d

    invoke-direct {v1, v0, v2, v3}, Lazp;-><init>(Layu;IZ)V

    return-object v1
.end method

.method public g(Layl;)Landroid/graphics/Matrix;
    .locals 5

    .prologue
    .line 33
    iget-wide v0, p1, Layl;->a:J

    long-to-int v0, v0

    invoke-static {v0}, Lbag;->a(I)F

    move-result v0

    const v1, 0x3f0674fc

    mul-float/2addr v0, v1

    .line 35
    iget-wide v2, p1, Layl;->a:J

    long-to-int v1, v2

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Lbag;->a(I)F

    move-result v1

    const v2, 0x3f2aaaaa

    mul-float/2addr v1, v2

    .line 37
    invoke-super {p0, p1}, Layr;->g(Layl;)Landroid/graphics/Matrix;

    move-result-object v2

    .line 38
    const v3, 0x3ef31608

    const v4, 0x3eaaaaab

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 39
    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 40
    return-object v2
.end method
