.class public final Lbrn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:[J

.field private b:[F

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/16 v1, 0x1f4

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    new-array v0, v1, [J

    iput-object v0, p0, Lbrn;->a:[J

    .line 90
    new-array v0, v1, [F

    iput-object v0, p0, Lbrn;->b:[F

    return-void
.end method


# virtual methods
.method public a()Lbrm;
    .locals 4

    .prologue
    .line 126
    new-instance v0, Lbrm;

    iget-object v1, p0, Lbrn;->a:[J

    iget v2, p0, Lbrn;->c:I

    .line 127
    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v1

    iget-object v2, p0, Lbrn;->b:[F

    iget v3, p0, Lbrn;->c:I

    .line 128
    invoke-static {v2, v3}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbrm;-><init>([J[F)V

    return-object v0
.end method

.method public a(JF)Lbrn;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 99
    .line 100
    iget v0, p0, Lbrn;->c:I

    if-lez v0, :cond_4

    .line 101
    iget-object v0, p0, Lbrn;->a:[J

    iget v3, p0, Lbrn;->c:I

    add-int/lit8 v3, v3, -0x1

    aget-wide v4, v0, v3

    .line 102
    cmp-long v0, p1, v4

    if-ltz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must be >= previous cut point"

    invoke-static {v0, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 103
    cmp-long v0, v4, p1

    if-nez v0, :cond_1

    .line 107
    :goto_1
    if-eqz v1, :cond_2

    .line 108
    iget-object v0, p0, Lbrn;->b:[F

    iget v1, p0, Lbrn;->c:I

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lbrn;->b:[F

    iget v3, p0, Lbrn;->c:I

    add-int/lit8 v3, v3, -0x1

    aget v2, v2, v3

    invoke-static {p3, v2}, Ljava/lang/Math;->max(FF)F

    move-result v2

    aput v2, v0, v1

    .line 119
    :goto_2
    return-object p0

    :cond_0
    move v0, v2

    .line 102
    goto :goto_0

    :cond_1
    move v1, v2

    .line 103
    goto :goto_1

    .line 111
    :cond_2
    iget v0, p0, Lbrn;->c:I

    iget-object v1, p0, Lbrn;->a:[J

    array-length v1, v1

    if-ne v0, v1, :cond_3

    .line 112
    iget-object v0, p0, Lbrn;->a:[J

    iget v1, p0, Lbrn;->c:I

    shl-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v0

    iput-object v0, p0, Lbrn;->a:[J

    .line 113
    iget-object v0, p0, Lbrn;->b:[F

    iget v1, p0, Lbrn;->c:I

    shl-int/lit8 v1, v1, 0x1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v0

    iput-object v0, p0, Lbrn;->b:[F

    .line 115
    :cond_3
    iget-object v0, p0, Lbrn;->a:[J

    iget v1, p0, Lbrn;->c:I

    aput-wide p1, v0, v1

    .line 116
    iget-object v0, p0, Lbrn;->b:[F

    iget v1, p0, Lbrn;->c:I

    aput p3, v0, v1

    .line 117
    iget v0, p0, Lbrn;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbrn;->c:I

    goto :goto_2

    :cond_4
    move v1, v2

    goto :goto_1
.end method
