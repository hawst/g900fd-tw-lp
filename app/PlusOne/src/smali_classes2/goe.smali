.class final Lgoe;
.super Lghj;

# interfaces
.implements Lgzb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lghh",
        "<",
        "Lgns;",
        ">.ghj<",
        "Lgfy",
        "<",
        "Lgzb;",
        ">;>;",
        "Lgzb;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/common/api/Status;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lgny;Lgfy;Lcom/google/android/gms/common/api/Status;Ljava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfy",
            "<",
            "Lgzb;",
            ">;",
            "Lcom/google/android/gms/common/api/Status;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lghj;-><init>(Lghh;Ljava/lang/Object;)V

    iput-object p3, p0, Lgoe;->a:Lcom/google/android/gms/common/api/Status;

    iput-object p4, p0, Lgoe;->b:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, Lgoe;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method protected a(Lgfy;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfy",
            "<",
            "Lgzb;",
            ">;)V"
        }
    .end annotation

    if-eqz p1, :cond_0

    invoke-interface {p1, p0}, Lgfy;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method protected synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lgfy;

    invoke-virtual {p0, p1}, Lgoe;->a(Lgfy;)V

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgoe;->b:Ljava/lang/String;

    return-object v0
.end method

.method protected c()V
    .locals 0

    return-void
.end method
