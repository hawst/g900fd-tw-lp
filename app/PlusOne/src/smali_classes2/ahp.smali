.class public final Lahp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lbku;

.field private final b:[J

.field private final c:Lbmz;


# direct methods
.method public constructor <init>(Lbku;[JLbmz;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string v0, "metrics"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 20
    const-string v0, "timestamps"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 21
    const-string v0, "field"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 22
    iput-object p1, p0, Lahp;->a:Lbku;

    .line 23
    iput-object p2, p0, Lahp;->b:[J

    .line 24
    iput-object p3, p0, Lahp;->c:Lbmz;

    .line 25
    return-void
.end method

.method private a(Z)Lbmn;
    .locals 12

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 44
    .line 46
    const/4 v3, 0x0

    .line 48
    iget-object v8, p0, Lahp;->b:[J

    array-length v9, v8

    move v7, v4

    move v6, v4

    move-object v2, v1

    :goto_0
    if-ge v7, v9, :cond_2

    aget-wide v10, v8, v7

    .line 49
    iget-object v0, p0, Lahp;->a:Lbku;

    iget-object v5, p0, Lahp;->c:Lbmz;

    invoke-virtual {v0, v5, v10, v11}, Lbku;->a(Lbmz;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmn;

    .line 50
    if-eqz v0, :cond_5

    .line 51
    if-nez v2, :cond_0

    .line 52
    new-instance v2, Lbmn;

    invoke-virtual {v0}, Lbmn;->c()I

    move-result v1

    invoke-virtual {v0}, Lbmn;->d()I

    move-result v5

    invoke-direct {v2, v1, v5}, Lbmn;-><init>(II)V

    .line 53
    invoke-virtual {v2}, Lbmn;->e()[F

    move-result-object v1

    :cond_0
    move v5, v3

    move v3, v4

    .line 55
    :goto_1
    array-length v10, v1

    if-ge v3, v10, :cond_1

    .line 56
    invoke-virtual {v0, v3}, Lbmn;->a(I)F

    move-result v10

    .line 57
    aget v11, v1, v3

    add-float/2addr v11, v10

    aput v11, v1, v3

    .line 58
    add-float/2addr v5, v10

    .line 55
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 60
    :cond_1
    add-int/lit8 v0, v6, 0x1

    .line 48
    :goto_2
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move v6, v0

    move v3, v5

    goto :goto_0

    .line 63
    :cond_2
    if-eqz v1, :cond_4

    .line 64
    :goto_3
    array-length v0, v1

    if-ge v4, v0, :cond_4

    .line 65
    if-eqz p1, :cond_3

    aget v0, v1, v4

    div-float/2addr v0, v3

    :goto_4
    aput v0, v1, v4

    .line 64
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 65
    :cond_3
    aget v0, v1, v4

    int-to-float v5, v6

    div-float/2addr v0, v5

    goto :goto_4

    .line 68
    :cond_4
    return-object v2

    :cond_5
    move v0, v6

    move v5, v3

    goto :goto_2
.end method


# virtual methods
.method public a()Lbmn;
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lahp;->a(Z)Lbmn;

    move-result-object v0

    return-object v0
.end method

.method public b()Lbmn;
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lahp;->a(Z)Lbmn;

    move-result-object v0

    return-object v0
.end method

.method public c()Lbmn;
    .locals 14

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 75
    iget-object v8, p0, Lahp;->b:[J

    array-length v9, v8

    move v7, v5

    move v4, v5

    move-object v2, v1

    move-object v3, v1

    :goto_0
    if-ge v7, v9, :cond_2

    aget-wide v10, v8, v7

    iget-object v0, p0, Lahp;->a:Lbku;

    iget-object v6, p0, Lahp;->c:Lbmz;

    invoke-virtual {v0, v6, v10, v11}, Lbku;->a(Lbmz;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmn;

    if-eqz v0, :cond_1

    add-int/lit8 v6, v4, 0x1

    const/4 v4, 0x1

    if-ne v6, v4, :cond_0

    new-instance v3, Lbmn;

    invoke-virtual {v0}, Lbmn;->c()I

    move-result v1

    invoke-virtual {v0}, Lbmn;->d()I

    move-result v2

    invoke-direct {v3, v1, v2}, Lbmn;-><init>(II)V

    invoke-virtual {v0}, Lbmn;->b()I

    move-result v1

    new-array v2, v1, [F

    invoke-virtual {v0}, Lbmn;->b()I

    move-result v1

    new-array v1, v1, [F

    :cond_0
    move v4, v5

    :goto_1
    array-length v10, v2

    if-ge v4, v10, :cond_4

    invoke-virtual {v0, v4}, Lbmn;->a(I)F

    move-result v10

    aget v11, v2, v4

    sub-float/2addr v10, v11

    aget v11, v2, v4

    int-to-float v12, v6

    div-float v12, v10, v12

    add-float/2addr v11, v12

    aput v11, v2, v4

    aget v11, v1, v4

    invoke-virtual {v0, v4}, Lbmn;->a(I)F

    move-result v12

    aget v13, v2, v4

    sub-float/2addr v12, v13

    mul-float/2addr v10, v12

    add-float/2addr v10, v11

    aput v10, v1, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :cond_1
    move v0, v4

    :goto_2
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    move v4, v0

    goto :goto_0

    :cond_2
    if-eqz v3, :cond_3

    :goto_3
    array-length v0, v2

    if-ge v5, v0, :cond_3

    aget v0, v1, v5

    int-to-float v6, v4

    div-float/2addr v0, v6

    float-to-double v6, v0

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-float v0, v6

    invoke-virtual {v3, v5, v0}, Lbmn;->a(IF)V

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_3
    return-object v3

    :cond_4
    move v0, v6

    goto :goto_2
.end method
