.class final Lgsz;
.super Ljava/lang/Object;

# interfaces
.implements Lgis;


# instance fields
.field private final a:Lu;

.field private final b:Lgtn;


# direct methods
.method public constructor <init>(Lu;Lgtn;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lgih;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgtn;

    iput-object v0, p0, Lgsz;->b:Lgtn;

    invoke-static {p1}, Lgih;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lu;

    iput-object v0, p0, Lgsz;->a:Lu;

    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    :try_start_0
    iget-object v0, p0, Lgsz;->b:Lgtn;

    invoke-static {p1}, Lgje;->a(Ljava/lang/Object;)Lgjb;

    move-result-object v1

    invoke-static {p2}, Lgje;->a(Ljava/lang/Object;)Lgjb;

    move-result-object v2

    invoke-interface {v0, v1, v2, p3}, Lgtn;->a(Lgjb;Lgjb;Landroid/os/Bundle;)Lgjb;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    invoke-static {v0}, Lgje;->a(Lgjb;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public a()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lgsz;->b:Lgtn;

    invoke-interface {v0}, Lgtn;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public a(Landroid/app/Activity;Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 3

    const-string v0, "MapOptions"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/GoogleMapOptions;

    :try_start_0
    iget-object v1, p0, Lgsz;->b:Lgtn;

    invoke-static {p1}, Lgje;->a(Ljava/lang/Object;)Lgjb;

    move-result-object v2

    invoke-interface {v1, v2, v0, p3}, Lgtn;->a(Lgjb;Lcom/google/android/gms/maps/GoogleMapOptions;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    if-nez p1, :cond_0

    :try_start_0
    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    :cond_0
    iget-object v0, p0, Lgsz;->a:Lu;

    iget-object v0, v0, Lu;->h:Landroid/os/Bundle;

    if-eqz v0, :cond_1

    const-string v1, "MapOptions"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "MapOptions"

    const-string v2, "MapOptions"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lgwl;->a(Landroid/os/Bundle;Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    iget-object v0, p0, Lgsz;->b:Lgtn;

    invoke-interface {v0, p1}, Lgtn;->a(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public b()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lgsz;->b:Lgtn;

    invoke-interface {v0}, Lgtn;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lgsz;->b:Lgtn;

    invoke-interface {v0, p1}, Lgtn;->b(Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public c()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lgsz;->b:Lgtn;

    invoke-interface {v0}, Lgtn;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public d()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lgsz;->b:Lgtn;

    invoke-interface {v0}, Lgtn;->e()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public e()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lgsz;->b:Lgtn;

    invoke-interface {v0}, Lgtn;->f()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public f()Lgtn;
    .locals 1

    iget-object v0, p0, Lgsz;->b:Lgtn;

    return-object v0
.end method
