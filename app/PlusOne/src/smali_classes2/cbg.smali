.class final Lcbg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcap;


# instance fields
.field private synthetic a:Lcah;

.field private synthetic b:Lcbd;


# direct methods
.method constructor <init>(Lcbd;Lcah;)V
    .locals 0

    .prologue
    .line 89
    iput-object p1, p0, Lcbg;->b:Lcbd;

    iput-object p2, p0, Lcbg;->a:Lcah;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 110
    iget-object v0, p0, Lcbg;->b:Lcbd;

    iget-object v0, v0, Lcbd;->e:Landroid/graphics/Point;

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 111
    iget-object v0, p0, Lcbg;->b:Lcbd;

    iget-object v0, v0, Lcbd;->e:Landroid/graphics/Point;

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 112
    iget-object v0, p0, Lcbg;->b:Lcbd;

    iget-object v0, v0, Lcbd;->d:Landroid/graphics/Point;

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 113
    iget-object v0, p0, Lcbg;->b:Lcbd;

    iget-object v0, v0, Lcbd;->d:Landroid/graphics/Point;

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 114
    iget-object v0, p0, Lcbg;->b:Lcbd;

    const/4 v1, 0x0

    iput v1, v0, Lcbd;->i:F

    .line 115
    iget-object v0, p0, Lcbg;->a:Lcah;

    invoke-virtual {v0}, Lcah;->a()V

    .line 116
    return-void
.end method

.method public a(II)V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcbg;->b:Lcbd;

    iget-object v0, v0, Lcbd;->e:Landroid/graphics/Point;

    iget-object v1, p0, Lcbg;->b:Lcbd;

    iget-object v1, v1, Lcbd;->a:Lcam;

    invoke-interface {v1}, Lcam;->getScrollX()I

    move-result v1

    sub-int v1, p1, v1

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 93
    iget-object v0, p0, Lcbg;->b:Lcbd;

    iget-object v0, v0, Lcbd;->e:Landroid/graphics/Point;

    iget-object v1, p0, Lcbg;->b:Lcbd;

    iget-object v1, v1, Lcbd;->a:Lcam;

    invoke-interface {v1}, Lcam;->getScrollY()I

    move-result v1

    sub-int v1, p2, v1

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 94
    iget-object v0, p0, Lcbg;->b:Lcbd;

    const/4 v1, 0x0

    iput v1, v0, Lcbd;->i:F

    .line 95
    return-void
.end method

.method public b(II)V
    .locals 4

    .prologue
    .line 99
    iget-object v0, p0, Lcbg;->b:Lcbd;

    iget-object v0, v0, Lcbd;->d:Landroid/graphics/Point;

    iget-object v1, p0, Lcbg;->b:Lcbd;

    iget-object v1, v1, Lcbd;->a:Lcam;

    invoke-interface {v1}, Lcam;->getScrollX()I

    move-result v1

    sub-int v1, p1, v1

    iput v1, v0, Landroid/graphics/Point;->x:I

    .line 100
    iget-object v0, p0, Lcbg;->b:Lcbd;

    iget-object v0, v0, Lcbd;->d:Landroid/graphics/Point;

    iget-object v1, p0, Lcbg;->b:Lcbd;

    iget-object v1, v1, Lcbd;->a:Lcam;

    invoke-interface {v1}, Lcam;->getScrollY()I

    move-result v1

    sub-int v1, p2, v1

    iput v1, v0, Landroid/graphics/Point;->y:I

    .line 101
    iget-object v0, p0, Lcbg;->b:Lcbd;

    iget-object v0, v0, Lcbd;->f:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcbg;->b:Lcbd;

    .line 102
    iget-object v0, v0, Lcbd;->d:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iget-object v1, p0, Lcbg;->b:Lcbd;

    iget-object v1, v1, Lcbd;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v1

    .line 104
    :goto_0
    iget-object v1, p0, Lcbg;->b:Lcbd;

    int-to-float v0, v0

    iget-object v2, p0, Lcbg;->b:Lcbd;

    .line 105
    iget v2, v2, Lcbd;->g:I

    int-to-float v2, v2

    div-float/2addr v0, v2

    const/high16 v2, -0x40800000    # -1.0f

    const/high16 v3, 0x3f800000    # 1.0f

    .line 104
    invoke-static {v0, v2, v3}, Lcfn;->a(FFF)F

    move-result v0

    iput v0, v1, Lcbd;->i:F

    .line 106
    return-void

    .line 102
    :cond_0
    iget-object v0, p0, Lcbg;->b:Lcbd;

    .line 103
    iget-object v0, v0, Lcbd;->d:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->y:I

    iget-object v1, p0, Lcbg;->b:Lcbd;

    iget-object v1, v1, Lcbd;->e:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->y:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method
