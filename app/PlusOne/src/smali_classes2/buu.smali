.class public final Lbuu;
.super Loxu;
.source "PG"


# instance fields
.field public a:I

.field public b:[Lbty;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 523
    invoke-direct {p0}, Loxu;-><init>()V

    .line 526
    const/4 v0, 0x0

    iput v0, p0, Lbuu;->a:I

    .line 529
    sget-object v0, Lbty;->a:[Lbty;

    iput-object v0, p0, Lbuu;->b:[Lbty;

    .line 523
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 555
    .line 556
    iget v0, p0, Lbuu;->a:I

    if-eqz v0, :cond_2

    .line 557
    const/4 v0, 0x1

    iget v2, p0, Lbuu;->a:I

    .line 558
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 560
    :goto_0
    iget-object v2, p0, Lbuu;->b:[Lbty;

    if-eqz v2, :cond_1

    .line 561
    iget-object v2, p0, Lbuu;->b:[Lbty;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 562
    if-eqz v4, :cond_0

    .line 563
    const/4 v5, 0x2

    .line 564
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 561
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 568
    :cond_1
    iput v0, p0, Lbuu;->ai:I

    .line 569
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lbuu;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 577
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 578
    sparse-switch v0, :sswitch_data_0

    .line 582
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 583
    :sswitch_0
    return-object p0

    .line 588
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    iput v0, p0, Lbuu;->a:I

    goto :goto_0

    .line 592
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 593
    iget-object v0, p0, Lbuu;->b:[Lbty;

    if-nez v0, :cond_2

    move v0, v1

    .line 594
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lbty;

    .line 595
    iget-object v3, p0, Lbuu;->b:[Lbty;

    if-eqz v3, :cond_1

    .line 596
    iget-object v3, p0, Lbuu;->b:[Lbty;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 598
    :cond_1
    iput-object v2, p0, Lbuu;->b:[Lbty;

    .line 599
    :goto_2
    iget-object v2, p0, Lbuu;->b:[Lbty;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_3

    .line 600
    iget-object v2, p0, Lbuu;->b:[Lbty;

    new-instance v3, Lbty;

    invoke-direct {v3}, Lbty;-><init>()V

    aput-object v3, v2, v0

    .line 601
    iget-object v2, p0, Lbuu;->b:[Lbty;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 602
    invoke-virtual {p1}, Loxn;->a()I

    .line 599
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 593
    :cond_2
    iget-object v0, p0, Lbuu;->b:[Lbty;

    array-length v0, v0

    goto :goto_1

    .line 605
    :cond_3
    iget-object v2, p0, Lbuu;->b:[Lbty;

    new-instance v3, Lbty;

    invoke-direct {v3}, Lbty;-><init>()V

    aput-object v3, v2, v0

    .line 606
    iget-object v2, p0, Lbuu;->b:[Lbty;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 578
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 541
    iget v0, p0, Lbuu;->a:I

    if-eqz v0, :cond_0

    .line 542
    const/4 v0, 0x1

    iget v1, p0, Lbuu;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 544
    :cond_0
    iget-object v0, p0, Lbuu;->b:[Lbty;

    if-eqz v0, :cond_2

    .line 545
    iget-object v1, p0, Lbuu;->b:[Lbty;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 546
    if-eqz v3, :cond_1

    .line 547
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 545
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 551
    :cond_2
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 519
    invoke-virtual {p0, p1}, Lbuu;->a(Loxn;)Lbuu;

    move-result-object v0

    return-object v0
.end method
