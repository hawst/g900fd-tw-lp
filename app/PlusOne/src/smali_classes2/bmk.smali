.class public final Lbmk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lboh;

.field private final b:[J

.field private final c:J

.field private final d:I

.field private final e:Landroid/net/Uri;

.field private final f:J

.field private final g:I


# direct methods
.method public constructor <init>(Lboh;[JJJILandroid/net/Uri;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const-string v0, "beatTimesUs"

    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 43
    invoke-static {p2}, Lbmk;->a([J)Z

    move-result v0

    const-string v1, "beat times must be strictly increasing"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 44
    array-length v0, p2

    if-lez v0, :cond_0

    .line 45
    array-length v0, p2

    add-int/lit8 v0, v0, -0x1

    aget-wide v0, p2, v0

    cmp-long v0, v0, p3

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "final beat time must be less than duration"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 48
    :cond_0
    const-string v0, "soundtrack"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboh;

    iput-object v0, p0, Lbmk;->a:Lboh;

    .line 49
    iput-object p2, p0, Lbmk;->b:[J

    .line 50
    iput-wide p3, p0, Lbmk;->c:J

    .line 51
    iput-wide p5, p0, Lbmk;->f:J

    .line 52
    iput p7, p0, Lbmk;->d:I

    .line 53
    const-string v0, "fileUri"

    invoke-static {p8, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lbmk;->e:Landroid/net/Uri;

    .line 54
    iput p9, p0, Lbmk;->g:I

    .line 55
    return-void

    .line 45
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a([J)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 127
    move v0, v1

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 128
    aget-wide v2, p0, v0

    add-int/lit8 v4, v0, -0x1

    aget-wide v4, p0, v4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_1

    .line 129
    const/4 v1, 0x0

    .line 132
    :cond_0
    return v1

    .line 127
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lbmk;->e:Landroid/net/Uri;

    return-object v0
.end method

.method public b()Lboh;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lbmk;->a:Lboh;

    return-object v0
.end method

.method public c()[J
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbmk;->b:[J

    invoke-virtual {v0}, [J->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    return-object v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 94
    iget-wide v0, p0, Lbmk;->c:J

    return-wide v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 101
    iget v0, p0, Lbmk;->d:I

    return v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 108
    iget-wide v0, p0, Lbmk;->f:J

    return-wide v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lbmk;->g:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lbmk;->a:Lboh;

    invoke-virtual {v0}, Lboh;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
