.class public final Lbmn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:[F


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 29
    mul-int v0, p1, p2

    new-array v0, v0, [F

    invoke-direct {p0, p1, p2, v0}, Lbmn;-><init>(II[F)V

    .line 30
    return-void
.end method

.method public constructor <init>(II[F)V
    .locals 4

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    array-length v0, p3

    const-string v1, "value.length"

    mul-int v2, p1, p2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 42
    iput p1, p0, Lbmn;->a:I

    .line 43
    iput p2, p0, Lbmn;->b:I

    .line 44
    iput-object p3, p0, Lbmn;->c:[F

    .line 45
    return-void
.end method

.method public static a(Lbvj;)Lbmn;
    .locals 4

    .prologue
    .line 54
    new-instance v0, Lbmn;

    iget v1, p0, Lbvj;->a:I

    iget v2, p0, Lbvj;->b:I

    iget-object v3, p0, Lbvj;->c:[F

    invoke-direct {v0, v1, v2, v3}, Lbmn;-><init>(II[F)V

    return-object v0
.end method


# virtual methods
.method public a(I)F
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lbmn;->c:[F

    aget v0, v0, p1

    return v0
.end method

.method public a()Lbvj;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Lbvj;

    invoke-direct {v0}, Lbvj;-><init>()V

    .line 67
    iget v1, p0, Lbmn;->b:I

    iput v1, v0, Lbvj;->b:I

    .line 68
    iget v1, p0, Lbmn;->a:I

    iput v1, v0, Lbvj;->a:I

    .line 69
    iget-object v1, p0, Lbmn;->c:[F

    iput-object v1, v0, Lbvj;->c:[F

    .line 70
    return-object v0
.end method

.method public a(IF)V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lbmn;->c:[F

    aput p2, v0, p1

    .line 121
    return-void
.end method

.method public b()I
    .locals 2

    .prologue
    .line 77
    iget v0, p0, Lbmn;->a:I

    iget v1, p0, Lbmn;->b:I

    mul-int/2addr v0, v1

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lbmn;->a:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lbmn;->b:I

    return v0
.end method

.method public e()[F
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lbmn;->c:[F

    return-object v0
.end method
