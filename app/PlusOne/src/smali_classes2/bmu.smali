.class public abstract Lbmu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:I

.field public final b:J

.field public final c:I

.field public final d:I

.field public final e:Lbmv;


# direct methods
.method protected constructor <init>(JIIILbmv;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-wide p1, p0, Lbmu;->b:J

    .line 41
    iput p3, p0, Lbmu;->a:I

    .line 42
    iput p4, p0, Lbmu;->c:I

    .line 43
    iput p5, p0, Lbmu;->d:I

    .line 44
    iput-object p6, p0, Lbmu;->e:Lbmv;

    .line 45
    return-void
.end method

.method protected constructor <init>(Lbmv;)V
    .locals 8

    .prologue
    const/4 v4, -0x1

    .line 35
    const-wide/16 v2, -0x1

    move-object v1, p0

    move v5, v4

    move v6, v4

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, Lbmu;-><init>(JIIILbmv;)V

    .line 36
    return-void
.end method


# virtual methods
.method public final a()Lboo;
    .locals 3

    .prologue
    .line 61
    iget-object v0, p0, Lbmu;->e:Lbmv;

    const-string v1, "type"

    sget-object v2, Lbmv;->a:Lbmv;

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    .line 62
    check-cast p0, Lboo;

    return-object p0
.end method

.method public final b()F
    .locals 3

    .prologue
    .line 79
    iget v0, p0, Lbmu;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lbmu;->a:I

    :goto_0
    iget v1, p0, Lbmu;->c:I

    iget v2, p0, Lbmu;->d:I

    invoke-static {v0, v1, v2}, Lbqu;->a(III)F

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 56
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lbmu;->a:I

    .line 57
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lbmu;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lbmu;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lbmu;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 56
    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
