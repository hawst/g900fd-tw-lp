.class public final Lftz;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lljh;


# instance fields
.field private a:Landroid/graphics/Paint;

.field private b:Landroid/graphics/Path;

.field private c:Z

.field private d:F

.field private e:[F

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lftz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lftz;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lftz;->b:Landroid/graphics/Path;

    .line 51
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lftz;->setLayerType(ILandroid/graphics/Paint;)V

    .line 52
    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 91
    new-instance v0, Landroid/graphics/PathMeasure;

    iget-object v1, p0, Lftz;->b:Landroid/graphics/Path;

    invoke-direct {v0, v1, v4}, Landroid/graphics/PathMeasure;-><init>(Landroid/graphics/Path;Z)V

    invoke-virtual {v0}, Landroid/graphics/PathMeasure;->getLength()F

    move-result v0

    iput v0, p0, Lftz;->d:F

    iget-boolean v0, p0, Lftz;->c:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lftz;->d:F

    iget v1, p0, Lftz;->h:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    shl-int/lit8 v0, v0, 0x1

    new-array v0, v0, [F

    iput-object v0, p0, Lftz;->e:[F

    .line 92
    :cond_0
    iget-boolean v0, p0, Lftz;->c:Z

    if-eqz v0, :cond_1

    .line 93
    iget-object v0, p0, Lftz;->a:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/DashPathEffect;

    const/4 v2, 0x2

    new-array v2, v2, [F

    iget v3, p0, Lftz;->f:I

    int-to-float v3, v3

    aput v3, v2, v4

    const/4 v3, 0x1

    iget v4, p0, Lftz;->g:I

    int-to-float v4, v4

    aput v4, v2, v3

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 95
    :cond_1
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 186
    const/4 v0, 0x0

    iput-object v0, p0, Lftz;->a:Landroid/graphics/Paint;

    .line 187
    iget-object v0, p0, Lftz;->b:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 188
    iput v1, p0, Lftz;->f:I

    .line 189
    iput v1, p0, Lftz;->g:I

    .line 190
    iput-boolean v1, p0, Lftz;->c:Z

    .line 191
    return-void
.end method

.method public a(II)V
    .locals 2

    .prologue
    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lftz;->c:Z

    .line 62
    iput p1, p0, Lftz;->f:I

    .line 63
    iput p2, p0, Lftz;->g:I

    .line 64
    add-int v0, p1, p2

    iput v0, p0, Lftz;->h:I

    .line 65
    iget-object v0, p0, Lftz;->a:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 66
    return-void
.end method

.method public a(Landroid/graphics/Paint;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lftz;->a:Landroid/graphics/Paint;

    .line 57
    return-void
.end method

.method public a(Lfri;)V
    .locals 3

    .prologue
    .line 70
    iget-object v0, p0, Lftz;->b:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 71
    iget-object v0, p0, Lftz;->b:Landroid/graphics/Path;

    iget v1, p1, Lfri;->a:F

    iget v2, p1, Lfri;->b:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 72
    iget-object v0, p0, Lftz;->b:Landroid/graphics/Path;

    iget v1, p1, Lfri;->c:F

    iget v2, p1, Lfri;->d:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 73
    invoke-direct {p0}, Lftz;->b()V

    .line 74
    return-void
.end method

.method public a([Lhng;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 78
    iget-object v1, p0, Lftz;->b:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 79
    array-length v1, p1

    if-nez v1, :cond_0

    .line 87
    :goto_0
    return-void

    .line 82
    :cond_0
    iget-object v1, p0, Lftz;->b:Landroid/graphics/Path;

    aget-object v2, p1, v0

    iget v2, v2, Lhng;->a:F

    aget-object v3, p1, v0

    iget v3, v3, Lhng;->b:F

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    .line 83
    array-length v8, p1

    move v7, v0

    :goto_1
    if-ge v7, v8, :cond_1

    aget-object v6, p1, v7

    .line 84
    iget-object v0, p0, Lftz;->b:Landroid/graphics/Path;

    iget v1, v6, Lhng;->c:F

    iget v2, v6, Lhng;->d:F

    iget v3, v6, Lhng;->e:F

    iget v4, v6, Lhng;->f:F

    iget v5, v6, Lhng;->g:F

    iget v6, v6, Lhng;->h:F

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 83
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    .line 86
    :cond_1
    invoke-direct {p0}, Lftz;->b()V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 174
    iget-object v0, p0, Lftz;->b:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lftz;->a:Landroid/graphics/Paint;

    if-nez v0, :cond_1

    .line 182
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 179
    invoke-virtual {p0}, Lftz;->getPaddingTop()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lftz;->getPaddingLeft()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 180
    iget-object v0, p0, Lftz;->b:Landroid/graphics/Path;

    iget-object v1, p0, Lftz;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 181
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method public setLinePercentDrawn(F)V
    .locals 7
    .annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
    .end annotation

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 115
    iget-object v1, p0, Lftz;->a:Landroid/graphics/Paint;

    if-nez v1, :cond_0

    .line 132
    :goto_0
    return-void

    .line 119
    :cond_0
    iget v1, p0, Lftz;->d:F

    mul-float v3, p1, v1

    .line 120
    iget-boolean v1, p0, Lftz;->c:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lftz;->e:[F

    array-length v1, v1

    if-lez v1, :cond_5

    move v1, v2

    .line 121
    :cond_1
    sub-float v4, v3, v1

    iget v5, p0, Lftz;->h:I

    int-to-float v5, v5

    cmpl-float v5, v4, v5

    if-lez v5, :cond_2

    iget-object v4, p0, Lftz;->e:[F

    add-int/lit8 v5, v0, 0x1

    iget v6, p0, Lftz;->f:I

    int-to-float v6, v6

    aput v6, v4, v0

    iget-object v4, p0, Lftz;->e:[F

    add-int/lit8 v0, v5, 0x1

    iget v6, p0, Lftz;->g:I

    int-to-float v6, v6

    aput v6, v4, v5

    iget v4, p0, Lftz;->h:I

    int-to-float v4, v4

    add-float/2addr v1, v4

    :goto_1
    cmpg-float v4, v1, v3

    if-ltz v4, :cond_1

    :goto_2
    iget-object v1, p0, Lftz;->e:[F

    array-length v1, v1

    if-ge v0, v1, :cond_4

    iget-object v1, p0, Lftz;->e:[F

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget v5, p0, Lftz;->f:I

    int-to-float v5, v5

    cmpl-float v5, v4, v5

    if-lez v5, :cond_3

    iget-object v4, p0, Lftz;->e:[F

    add-int/lit8 v5, v0, 0x1

    iget v6, p0, Lftz;->f:I

    int-to-float v6, v6

    aput v6, v4, v0

    iget v0, p0, Lftz;->f:I

    int-to-float v0, v0

    add-float/2addr v1, v0

    iget-object v4, p0, Lftz;->e:[F

    add-int/lit8 v0, v5, 0x1

    iget v6, p0, Lftz;->d:F

    sub-float v1, v6, v1

    aput v1, v4, v5

    iget v1, p0, Lftz;->d:F

    goto :goto_1

    :cond_3
    iget-object v5, p0, Lftz;->e:[F

    add-int/lit8 v6, v0, 0x1

    aput v4, v5, v0

    add-float/2addr v1, v4

    iget-object v4, p0, Lftz;->e:[F

    add-int/lit8 v0, v6, 0x1

    iget v5, p0, Lftz;->d:F

    sub-float v1, v5, v1

    aput v1, v4, v6

    iget v1, p0, Lftz;->d:F

    goto :goto_1

    .line 122
    :cond_4
    iget-object v0, p0, Lftz;->a:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/DashPathEffect;

    iget-object v3, p0, Lftz;->e:[F

    invoke-direct {v1, v3, v2}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 131
    :goto_3
    invoke-virtual {p0}, Lftz;->postInvalidate()V

    goto/16 :goto_0

    .line 126
    :cond_5
    iget-object v1, p0, Lftz;->a:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/DashPathEffect;

    const/4 v4, 0x2

    new-array v4, v4, [F

    iget v5, p0, Lftz;->d:F

    aput v5, v4, v0

    const/4 v0, 0x1

    iget v5, p0, Lftz;->d:F

    aput v5, v4, v0

    iget v0, p0, Lftz;->d:F

    sub-float/2addr v0, v3

    invoke-direct {v2, v4, v0}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    goto :goto_3
.end method
