.class final Lblf;
.super Lblh;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 232
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lblh;-><init>(F)V

    .line 233
    return-void
.end method


# virtual methods
.method protected a(Lbku;J)Ljava/lang/Float;
    .locals 6

    .prologue
    .line 250
    sget-object v0, Lbmz;->g:Lbmz;

    invoke-virtual {p1, v0, p2, p3}, Lbku;->a(Lbmz;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbbo;

    .line 252
    if-nez v0, :cond_0

    .line 254
    const/4 v0, 0x0

    .line 256
    :goto_0
    return-object v0

    :cond_0
    const/4 v2, 0x0

    const/4 v1, 0x0

    :goto_1
    array-length v3, v0

    if-ge v1, v3, :cond_1

    aget-object v3, v0, v1

    invoke-virtual {v3}, Lbbo;->b()Landroid/hardware/Camera$Face;

    move-result-object v3

    iget-object v3, v3, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v4

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    mul-int/2addr v3, v4

    int-to-float v3, v3

    add-float/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lbuw;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 267
    iget v0, p1, Lbuw;->i:F

    invoke-static {v0}, Lbkz;->a(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Lbuw;)V
    .locals 1

    .prologue
    .line 262
    check-cast p1, Ljava/lang/Float;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p2, Lbuw;->i:F

    .line 263
    return-void
.end method
