.class abstract Lzr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:[I

.field public b:Z

.field c:I

.field private d:I

.field private e:I


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 331
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 332
    const/4 v0, 0x0

    iput-object v0, p0, Lzr;->a:[I

    .line 335
    iput-boolean v1, p0, Lzr;->b:Z

    .line 337
    iput v1, p0, Lzr;->c:I

    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public abstract a(I)Ljava/lang/Object;
.end method

.method public abstract a(Labf;)V
.end method

.method public a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 364
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Internal error: Setting data on frame backing "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", which does not support setting data directly!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public abstract a(Lzr;)V
.end method

.method public a([I)V
    .locals 0

    .prologue
    .line 369
    iput-object p1, p0, Lzr;->a:[I

    .line 370
    return-void
.end method

.method public abstract b()I
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 373
    iput p1, p0, Lzr;->d:I

    .line 374
    return-void
.end method

.method protected b(Labf;)V
    .locals 2

    .prologue
    .line 405
    invoke-virtual {p1}, Labf;->e()I

    move-result v0

    const/16 v1, 0x12d

    if-eq v0, v1, :cond_0

    .line 406
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot allocate texture with non-RGBA data type!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 407
    :cond_0
    iget-object v0, p0, Lzr;->a:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lzr;->a:[I

    array-length v0, v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    .line 408
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot allocate non 2-dimensional texture!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 410
    :cond_2
    return-void
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 377
    iput p1, p0, Lzr;->e:I

    .line 378
    return-void
.end method

.method public abstract c()Z
.end method

.method public d()V
    .locals 0

    .prologue
    .line 361
    return-void
.end method

.method public abstract e()I
.end method

.method public abstract f()Z
.end method

.method public abstract g()V
.end method

.method public abstract h()I
.end method

.method public j()[I
    .locals 1

    .prologue
    .line 381
    iget-object v0, p0, Lzr;->a:[I

    return-object v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 385
    iget v0, p0, Lzr;->d:I

    return v0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 389
    iget v0, p0, Lzr;->e:I

    return v0
.end method

.method public m()Z
    .locals 1

    .prologue
    .line 393
    const/4 v0, 0x0

    return v0
.end method

.method public n()V
    .locals 1

    .prologue
    .line 397
    const/4 v0, 0x1

    iput-boolean v0, p0, Lzr;->b:Z

    .line 398
    return-void
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 401
    iget-boolean v0, p0, Lzr;->b:Z

    return v0
.end method
