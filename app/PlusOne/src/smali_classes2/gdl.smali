.class final Lgdl;
.super Ljava/lang/Object;

# interfaces
.implements Lgfs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lgfs",
        "<",
        "Lgkb;",
        "Lgdr;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const v0, 0x7fffffff

    return v0
.end method

.method public bridge synthetic a(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/lang/Object;Lggb;Lggc;)Lgfr;
    .locals 6

    move-object v3, p4

    check-cast v3, Lgdr;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p5

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Lgdl;->a(Landroid/content/Context;Landroid/os/Looper;Lgdr;Lggb;Lggc;)Lgkb;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;Landroid/os/Looper;Lgdr;Lggb;Lggc;)Lgkb;
    .locals 9

    const-string v0, "Setting the API options is required."

    invoke-static {p3, v0}, Lgih;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lgkb;

    iget-object v3, p3, Lgdr;->a:Lcom/google/android/gms/cast/CastDevice;

    const-wide/16 v4, 0x0

    iget-object v6, p3, Lgdr;->b:Lgdt;

    move-object v1, p1

    move-object v2, p2

    move-object v7, p4

    move-object v8, p5

    invoke-direct/range {v0 .. v8}, Lgkb;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/cast/CastDevice;JLgdt;Lggb;Lggc;)V

    return-object v0
.end method
