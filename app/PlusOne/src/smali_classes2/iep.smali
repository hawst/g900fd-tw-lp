.class public final Liep;
.super Liei;
.source "PG"

# interfaces
.implements Lieg;


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lief;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:Z

.field private final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lieg;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Lien;

.field private final h:Lier;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lien;Lier;)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0, p1}, Liei;-><init>(Landroid/content/Context;)V

    .line 29
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Liep;->e:Ljava/util/HashMap;

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Liep;->f:Ljava/util/ArrayList;

    .line 39
    iput-object p2, p0, Liep;->g:Lien;

    .line 40
    iput-object p3, p0, Liep;->h:Lier;

    .line 41
    iget-object v0, p0, Liep;->g:Lien;

    invoke-interface {v0, p0}, Lien;->a(Lieg;)V

    .line 42
    return-void
.end method

.method static synthetic a(Liep;)V
    .locals 3

    .prologue
    .line 22
    const/4 v0, 0x0

    iget-object v1, p0, Liep;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Liep;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieg;

    invoke-interface {v0}, Lieg;->l()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method private b()V
    .locals 4

    .prologue
    .line 127
    monitor-enter p0

    .line 128
    :try_start_0
    iget-boolean v0, p0, Liep;->d:Z

    if-eqz v0, :cond_0

    .line 129
    monitor-exit p0

    .line 148
    :goto_0
    return-void

    .line 131
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    iget-object v0, p0, Liep;->a:Landroid/content/Context;

    const-class v1, Lief;

    invoke-static {v0, v1}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 135
    monitor-enter p0

    .line 136
    :try_start_1
    iget-boolean v1, p0, Liep;->d:Z

    if-nez v1, :cond_2

    .line 137
    iput-object v0, p0, Liep;->b:Ljava/util/List;

    .line 139
    iget-object v0, p0, Liep;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 140
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Liep;->c:Ljava/util/HashSet;

    .line 142
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_1

    .line 143
    iget-object v3, p0, Liep;->c:Ljava/util/HashSet;

    iget-object v0, p0, Liep;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lief;

    invoke-virtual {v0}, Lief;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 142
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 131
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 146
    :cond_1
    const/4 v0, 0x1

    :try_start_3
    iput-boolean v0, p0, Liep;->d:Z

    .line 148
    :cond_2
    monitor-exit p0

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method


# virtual methods
.method public a(Lief;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 46
    invoke-virtual {p1}, Lief;->d()Ljava/lang/String;

    move-result-object v1

    .line 47
    invoke-direct {p0}, Liep;->b()V

    .line 50
    iget-object v0, p0, Liep;->c:Ljava/util/HashSet;

    invoke-virtual {p1}, Lief;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 51
    const-string v0, "ExperimentServiceImpl"

    const-string v2, "Experiment %s has not been registered"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 52
    invoke-virtual {p1}, Lief;->e()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 51
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    :cond_0
    iget-object v2, p0, Liep;->e:Ljava/util/HashMap;

    monitor-enter v2

    .line 56
    :try_start_0
    iget-object v0, p0, Liep;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    .line 57
    if-nez v0, :cond_1

    .line 58
    iget-object v0, p0, Liep;->g:Lien;

    invoke-interface {v0, p2}, Lien;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_1

    .line 61
    iget-object v3, p0, Liep;->e:Ljava/util/HashMap;

    invoke-virtual {v3, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 66
    if-eqz v0, :cond_3

    .line 67
    invoke-virtual {p1}, Lief;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 68
    if-eqz v0, :cond_2

    .line 71
    :goto_0
    iget-object v1, p0, Liep;->h:Lier;

    invoke-interface {v1, p1, v0}, Lier;->a(Lief;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 73
    return-object v0

    .line 64
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    move-object v0, v1

    .line 68
    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lief;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    invoke-direct {p0}, Liep;->b()V

    .line 79
    iget-object v0, p0, Liep;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Lieg;)V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Liep;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 113
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 5

    .prologue
    .line 84
    invoke-virtual {p0}, Liep;->a()Ljava/util/List;

    move-result-object v2

    .line 86
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 87
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 89
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 90
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lief;

    invoke-virtual {v0}, Lief;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 89
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 93
    :cond_0
    iget-object v0, p0, Liep;->g:Lien;

    invoke-interface {v0, v4, p1}, Lien;->a(Ljava/util/List;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b(Lieg;)V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Liep;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 118
    return-void
.end method

.method public l()V
    .locals 2

    .prologue
    .line 98
    iget-object v1, p0, Liep;->e:Ljava/util/HashMap;

    monitor-enter v1

    .line 99
    :try_start_0
    iget-object v0, p0, Liep;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 100
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    new-instance v0, Lieq;

    invoke-direct {v0, p0}, Lieq;-><init>(Liep;)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    .line 108
    return-void

    .line 100
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
