.class final Ljyn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field private synthetic a:Ljym;


# direct methods
.method constructor <init>(Ljym;)V
    .locals 0

    .prologue
    .line 303
    iput-object p1, p0, Ljyn;->a:Ljym;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 2

    .prologue
    .line 306
    const/4 v0, 0x0

    .line 307
    iget-object v1, p0, Ljyn;->a:Ljym;

    invoke-virtual {v1}, Ljym;->x()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 308
    iget-object v0, p0, Ljyn;->a:Ljym;

    invoke-virtual {v0}, Ljym;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 310
    :cond_0
    if-lez v0, :cond_2

    .line 311
    iget-object v1, p0, Ljyn;->a:Ljym;

    invoke-virtual {v1}, Ljym;->x()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 312
    iget-object v1, p0, Ljyn;->a:Ljym;

    invoke-virtual {v1}, Ljym;->x()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 314
    :cond_1
    iget-object v1, p0, Ljyn;->a:Ljym;

    invoke-static {v1, v0}, Ljym;->a(Ljym;I)V

    .line 315
    iget-object v0, p0, Ljyn;->a:Ljym;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljym;->a(Ljym;Z)Z

    .line 317
    :cond_2
    return-void
.end method
