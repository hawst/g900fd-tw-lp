.class public final Lcls;
.super Lcjy;
.source "PG"

# interfaces
.implements Lhdl;
.implements Lhdm;


# static fields
.field private static final W:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final X:[I

.field private static final Y:[I


# instance fields
.field private final Z:Lckf;

.field private final aa:Lcmv;

.field private ab:Lhdk;

.field private ac:I

.field private final ad:Landroid/graphics/PointF;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 46
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcls;->X:[I

    .line 51
    new-array v0, v6, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcls;->Y:[I

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 58
    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Integer;

    const/16 v2, 0x11

    .line 60
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/16 v2, 0x13

    .line 61
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    .line 62
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x3

    .line 63
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 64
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    .line 58
    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 66
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcls;->W:Ljava/util/List;

    .line 67
    return-void

    .line 46
    nop

    :array_0
    .array-data 4
        0x7f0a0053
        0x7f0a0054
    .end array-data

    .line 51
    :array_1
    .array-data 4
        0x7f020233
        0x7f020232
        0x7f020231
        0x7f020230
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 34
    invoke-direct {p0}, Lcjy;-><init>()V

    .line 69
    new-instance v0, Lckf;

    const/4 v1, 0x3

    sget-object v2, Lcls;->Y:[I

    invoke-direct {v0, p0, v1, v2}, Lckf;-><init>(Lcjy;I[I)V

    iput-object v0, p0, Lcls;->Z:Lckf;

    .line 71
    new-instance v0, Lclu;

    invoke-direct {v0, p0}, Lclu;-><init>(Lcls;)V

    iput-object v0, p0, Lcls;->aa:Lcmv;

    .line 206
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcls;->ad:Landroid/graphics/PointF;

    .line 400
    return-void
.end method

.method static synthetic a(Lcls;)Lckf;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcls;->Z:Lckf;

    return-object v0
.end method

.method private static a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;F)Z
    .locals 2

    .prologue
    .line 387
    const/16 v0, 0x12

    const v1, 0x49742400    # 1000000.0f

    mul-float/2addr v1, p1

    .line 388
    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    .line 387
    invoke-virtual {p0, v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(ILjava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private aw()V
    .locals 3

    .prologue
    .line 392
    invoke-virtual {p0}, Lcls;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getActiveParameterKey()I

    move-result v0

    .line 393
    iget-object v1, p0, Lcls;->ab:Lhdk;

    const/16 v2, 0x11

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lhdk;->a(Z)V

    .line 395
    return-void

    .line 393
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcls;)Lcmv;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcls;->aa:Lcmv;

    return-object v0
.end method


# virtual methods
.method protected U()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
    .locals 4

    .prologue
    .line 97
    invoke-super {p0}, Lcjy;->U()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    .line 99
    invoke-virtual {p0}, Lcls;->ah()Lchp;

    move-result-object v1

    invoke-virtual {v1}, Lchp;->h()Lcom/google/android/libraries/photoeditor/core/FilterChain;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getPostRotation()I

    move-result v1

    iput v1, p0, Lcls;->ac:I

    .line 100
    iget v1, p0, Lcls;->ac:I

    .line 101
    invoke-static {v1}, Lhbg;->a(I)F

    move-result v1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 102
    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_0

    .line 105
    neg-float v1, v1

    invoke-static {v0, v1}, Lcls;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;F)Z

    .line 108
    :cond_0
    return-object v0
.end method

.method protected X()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 113
    sget-object v0, Lcls;->W:Ljava/util/List;

    return-object v0
.end method

.method public a(FF)V
    .locals 5

    .prologue
    const v4, 0x477fff00    # 65535.0f

    const/high16 v2, 0x3f800000    # 1.0f

    .line 210
    iget-object v0, p0, Lcls;->ad:Landroid/graphics/PointF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 211
    iget-object v0, p0, Lcls;->ad:Landroid/graphics/PointF;

    iget v1, p0, Lcls;->ac:I

    invoke-static {v0, v1, v2, v2}, Lhbg;->a(Landroid/graphics/PointF;IFF)V

    .line 213
    invoke-virtual {p0}, Lcls;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    .line 214
    const/16 v1, 0x18

    iget-object v2, p0, Lcls;->ad:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    mul-float/2addr v2, v4

    .line 215
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .line 214
    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(ILjava/lang/Object;)Z

    move-result v1

    .line 216
    const/16 v2, 0x19

    iget-object v3, p0, Lcls;->ad:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    mul-float/2addr v3, v4

    .line 217
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    .line 216
    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(ILjava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 220
    :goto_0
    if-eqz v0, :cond_1

    .line 221
    invoke-virtual {p0}, Lcls;->ak()V

    .line 223
    :cond_1
    return-void

    .line 216
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(FII)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 234
    invoke-virtual {p0}, Lcls;->av()Z

    move-result v0

    if-eqz v0, :cond_0

    move p2, p3

    .line 238
    :cond_0
    invoke-virtual {p0}, Lcls;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v3

    .line 239
    invoke-static {v3, p1}, Lcls;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;F)Z

    move-result v0

    .line 240
    const/16 v4, 0xc9

    int-to-float v5, p2

    .line 241
    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    move-result v4

    if-nez v4, :cond_1

    if-eqz v0, :cond_5

    :cond_1
    move v0, v2

    .line 242
    :goto_0
    const/16 v4, 0xca

    int-to-float v5, p3

    .line 243
    invoke-virtual {v3, v4, v5}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(IF)Z

    move-result v3

    if-nez v3, :cond_2

    if-eqz v0, :cond_3

    :cond_2
    move v1, v2

    .line 245
    :cond_3
    if-eqz v1, :cond_4

    .line 246
    invoke-virtual {p0}, Lcls;->ak()V

    .line 248
    :cond_4
    return-void

    :cond_5
    move v0, v1

    .line 241
    goto :goto_0
.end method

.method public a(Landroid/graphics/PointF;)V
    .locals 2

    .prologue
    .line 169
    if-nez p1, :cond_0

    .line 176
    :goto_0
    return-void

    .line 173
    :cond_0
    invoke-virtual {p0}, Lcls;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    .line 174
    const/16 v1, 0xc9

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v1

    int-to-float v1, v1

    iput v1, p1, Landroid/graphics/PointF;->x:F

    .line 175
    const/16 v1, 0xca

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    iput v0, p1, Landroid/graphics/PointF;->y:F

    goto :goto_0
.end method

.method protected a(Lckp;)V
    .locals 3

    .prologue
    .line 136
    const v0, 0x7f0203a3

    const v1, 0x7f0a009b

    .line 138
    invoke-virtual {p0, v1}, Lcls;->e_(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lclt;

    invoke-direct {v2, p0}, Lclt;-><init>(Lcls;)V

    .line 136
    invoke-virtual {p1, v0, v1, v2}, Lckp;->a(ILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 146
    return-void
.end method

.method protected a(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V
    .locals 6

    .prologue
    const v5, 0x477fff00    # 65535.0f

    const/high16 v4, 0x3f800000    # 1.0f

    .line 118
    invoke-super {p0, p1}, Lcjy;->a(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V

    .line 120
    new-instance v0, Lhdk;

    invoke-direct {v0, p1}, Lhdk;-><init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V

    iput-object v0, p0, Lcls;->ab:Lhdk;

    .line 121
    iget-object v0, p0, Lcls;->ab:Lhdk;

    invoke-virtual {v0, p0}, Lhdk;->a(Lhdl;)V

    .line 122
    iget-object v0, p0, Lcls;->ab:Lhdk;

    invoke-virtual {v0, p0}, Lhdk;->a(Lhdm;)V

    .line 124
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    .line 125
    invoke-virtual {p0}, Lcls;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    const/16 v2, 0x18

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v2

    div-float/2addr v2, v5

    const/16 v3, 0x19

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v5

    invoke-virtual {v0, v2, v1}, Landroid/graphics/PointF;->set(FF)V

    iget v1, p0, Lcls;->ac:I

    invoke-static {v0, v1, v4, v4}, Lhbg;->b(Landroid/graphics/PointF;IFF)V

    .line 126
    iget-object v1, p0, Lcls;->ab:Lhdk;

    iget v2, v0, Landroid/graphics/PointF;->x:F

    iget v0, v0, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v2, v0}, Lhdk;->g(FF)Z

    .line 128
    iget-object v0, p0, Lcls;->ab:Lhdk;

    iget v1, p0, Lcls;->ac:I

    .line 129
    invoke-static {v1}, Lhbg;->a(I)F

    move-result v1

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 128
    invoke-virtual {v0, v1}, Lhdk;->a(F)V

    .line 131
    iget-object v0, p0, Lcls;->ab:Lhdk;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a(Lhdx;)I

    .line 132
    return-void
.end method

.method public aO_()V
    .locals 0

    .prologue
    .line 80
    invoke-super {p0}, Lcjy;->aO_()V

    .line 82
    invoke-direct {p0}, Lcls;->aw()V

    .line 83
    return-void
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 87
    new-instance v0, Lhmk;

    sget-object v1, Lonc;->x:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method public at()F
    .locals 2

    .prologue
    .line 164
    invoke-virtual {p0}, Lcls;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    const/16 v1, 0x12

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v0

    const v1, 0x49742400    # 1000000.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public au()F
    .locals 2

    .prologue
    .line 180
    invoke-virtual {p0}, Lcls;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    const/16 v1, 0x11

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public av()Z
    .locals 2

    .prologue
    .line 185
    invoke-virtual {p0}, Lcls;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    .line 186
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(ILjava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 150
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 151
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_0

    sget-object v1, Lcls;->X:[I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    sget-object v1, Lcls;->X:[I

    aget v0, v1, v0

    invoke-virtual {p0, v0}, Lcls;->e_(I)Ljava/lang/String;

    move-result-object v0

    .line 153
    :goto_0
    return-object v0

    .line 151
    :cond_0
    const-string v0, "*UNKNOWN*"

    goto :goto_0

    .line 153
    :cond_1
    invoke-super {p0, p1, p2}, Lcjy;->b(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected b(Z)V
    .locals 1

    .prologue
    .line 227
    invoke-super {p0, p1}, Lcjy;->b(Z)V

    .line 229
    iget-object v0, p0, Lcls;->P:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    .line 230
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 92
    const/16 v0, 0xe

    return v0
.end method

.method public c(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 191
    invoke-super {p0, p1, p2}, Lcjy;->c(ILjava/lang/Object;)V

    .line 193
    iget-object v0, p0, Lcls;->R:Lhcs;

    invoke-virtual {v0, p1}, Lhcs;->f(I)I

    move-result v0

    .line 194
    const/16 v1, 0x11

    if-ne v0, v1, :cond_0

    .line 195
    iget-object v0, p0, Lcls;->P:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    .line 197
    :cond_0
    return-void
.end method

.method protected e()Ljava/util/List;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 252
    move-object/from16 v0, p0

    iget-object v2, v0, Lcls;->P:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcls;->U:Lhcz;

    if-nez v2, :cond_1

    .line 253
    :cond_0
    const/4 v2, 0x0

    .line 371
    :goto_0
    return-object v2

    .line 256
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcls;->n()Lz;

    move-result-object v2

    .line 257
    if-nez v2, :cond_2

    .line 258
    const/4 v2, 0x0

    goto :goto_0

    .line 261
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcls;->o()Landroid/content/res/Resources;

    move-result-object v4

    .line 262
    invoke-virtual {v2}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-static {v2, v4}, Lcmh;->a(Landroid/view/Window;Landroid/content/res/Resources;)Landroid/graphics/Rect;

    move-result-object v2

    .line 263
    invoke-virtual {v2}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 264
    const/4 v2, 0x0

    goto :goto_0

    .line 268
    :cond_3
    new-instance v5, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v5}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 269
    new-instance v6, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v6}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 272
    const v3, 0x7f0d004b

    invoke-virtual {v4, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 273
    const v7, 0x7f0d0049

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 274
    div-int/lit8 v8, v7, 0x2

    .line 277
    move-object/from16 v0, p0

    iget-object v9, v0, Lcls;->P:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-static {v9}, Lcmp;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v9

    .line 278
    new-instance v10, Landroid/graphics/Rect;

    invoke-direct {v10}, Landroid/graphics/Rect;-><init>()V

    .line 279
    move-object/from16 v0, p0

    iget-object v11, v0, Lcls;->U:Lhcz;

    invoke-virtual {v11, v10}, Lhcz;->a(Landroid/graphics/Rect;)V

    .line 280
    iget v11, v9, Landroid/graphics/Rect;->left:I

    iget v9, v9, Landroid/graphics/Rect;->top:I

    invoke-virtual {v10, v11, v9}, Landroid/graphics/Rect;->offset(II)V

    .line 281
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v9

    if-le v9, v3, :cond_4

    .line 282
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v9

    sub-int/2addr v9, v3

    div-int/lit8 v9, v9, 0x2

    const/4 v11, 0x0

    invoke-virtual {v2, v9, v11}, Landroid/graphics/Rect;->inset(II)V

    .line 284
    :cond_4
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v9

    if-le v9, v3, :cond_5

    .line 285
    const/4 v9, 0x0

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v11

    sub-int v3, v11, v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v2, v9, v3}, Landroid/graphics/Rect;->inset(II)V

    .line 288
    :cond_5
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 289
    const v9, 0x7f0c0002

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    int-to-long v12, v4

    .line 292
    iget v4, v2, Landroid/graphics/Rect;->right:I

    iget v9, v10, Landroid/graphics/Rect;->right:I

    iget v10, v2, Landroid/graphics/Rect;->right:I

    add-int/2addr v9, v10

    div-int/lit8 v9, v9, 0x2

    .line 293
    invoke-static {v4, v9}, Ljava/lang/Math;->min(II)I

    move-result v4

    .line 294
    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v9

    div-int/lit8 v9, v9, 0x2

    sub-int/2addr v9, v8

    .line 296
    const/16 v10, 0x3e8

    int-to-float v11, v8

    invoke-static {v10, v11}, Lcmd;->a(IF)Lcme;

    move-result-object v10

    .line 297
    invoke-virtual {v10, v12, v13}, Lcme;->a(J)Lcme;

    move-result-object v11

    int-to-float v14, v8

    const v15, 0x3e99999a    # 0.3f

    mul-float/2addr v14, v15

    invoke-virtual {v11, v14}, Lcme;->a(F)Lcme;

    move-result-object v11

    int-to-float v14, v4

    iget v15, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v15, v15

    .line 298
    invoke-virtual {v11, v14, v15}, Lcme;->a(FF)Lcme;

    .line 299
    const-wide/16 v14, 0x96

    invoke-virtual {v10, v14, v15}, Lcme;->b(J)Lcmg;

    move-result-object v11

    int-to-float v14, v8

    invoke-virtual {v11, v14}, Lcmg;->a(F)Lcmg;

    move-result-object v11

    invoke-virtual {v11}, Lcmg;->a()Lcme;

    .line 300
    int-to-float v11, v4

    iget v14, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v14, v9

    int-to-float v14, v14

    const-wide/16 v16, 0x44c

    move-wide/from16 v0, v16

    invoke-virtual {v10, v11, v14, v0, v1}, Lcme;->b(FFJ)Lcmg;

    move-result-object v11

    .line 301
    invoke-virtual {v11, v5}, Lcmg;->a(Landroid/animation/TimeInterpolator;)Lcmg;

    move-result-object v11

    invoke-virtual {v11}, Lcmg;->a()Lcme;

    .line 302
    const-wide/16 v14, 0x12c

    invoke-virtual {v10, v14, v15}, Lcme;->b(J)Lcmg;

    move-result-object v11

    int-to-float v14, v8

    const v15, 0x3f666666    # 0.9f

    mul-float/2addr v14, v15

    invoke-virtual {v11, v14}, Lcmg;->a(F)Lcmg;

    move-result-object v11

    invoke-virtual {v11}, Lcmg;->a()Lcme;

    .line 303
    const-wide/16 v14, 0x64

    invoke-virtual {v10, v14, v15}, Lcme;->b(J)Lcmg;

    move-result-object v11

    invoke-virtual {v11}, Lcmg;->a()Lcme;

    .line 304
    const-wide/16 v14, 0x32

    invoke-virtual {v10, v14, v15}, Lcme;->b(J)Lcmg;

    move-result-object v11

    int-to-float v14, v8

    const v15, 0x3f6b851f    # 0.92f

    mul-float/2addr v14, v15

    invoke-virtual {v11, v14}, Lcmg;->a(F)Lcmg;

    move-result-object v11

    invoke-virtual {v11}, Lcmg;->a()Lcme;

    .line 305
    const/4 v11, 0x0

    const/high16 v14, 0x3fc00000    # 1.5f

    int-to-float v15, v7

    mul-float/2addr v14, v15

    const-wide/16 v16, 0x30c

    move-wide/from16 v0, v16

    invoke-virtual {v10, v11, v14, v0, v1}, Lcme;->a(FFJ)Lcmg;

    move-result-object v11

    .line 306
    invoke-virtual {v11, v5}, Lcmg;->a(Landroid/animation/TimeInterpolator;)Lcmg;

    move-result-object v11

    int-to-float v14, v8

    invoke-virtual {v11, v14}, Lcmg;->a(F)Lcmg;

    move-result-object v11

    invoke-virtual {v11}, Lcmg;->a()Lcme;

    .line 307
    const-wide/16 v14, 0x96

    invoke-virtual {v10, v14, v15}, Lcme;->b(J)Lcmg;

    move-result-object v11

    int-to-float v14, v8

    const v15, 0x3e99999a    # 0.3f

    mul-float/2addr v14, v15

    invoke-virtual {v11, v14}, Lcmg;->a(F)Lcmg;

    move-result-object v11

    invoke-virtual {v11}, Lcmg;->a()Lcme;

    .line 308
    invoke-virtual {v10}, Lcme;->a()Lcmd;

    move-result-object v10

    invoke-interface {v3, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 310
    const/16 v10, 0x3e9

    int-to-float v11, v8

    invoke-static {v10, v11}, Lcmd;->a(IF)Lcme;

    move-result-object v10

    .line 311
    invoke-virtual {v10, v12, v13}, Lcme;->a(J)Lcme;

    move-result-object v11

    int-to-float v12, v8

    const v13, 0x3e99999a    # 0.3f

    mul-float/2addr v12, v13

    invoke-virtual {v11, v12}, Lcme;->a(F)Lcme;

    move-result-object v11

    int-to-float v12, v4

    iget v13, v2, Landroid/graphics/Rect;->top:I

    int-to-float v13, v13

    .line 312
    invoke-virtual {v11, v12, v13}, Lcme;->a(FF)Lcme;

    .line 313
    const-wide/16 v12, 0x96

    invoke-virtual {v10, v12, v13}, Lcme;->b(J)Lcmg;

    move-result-object v11

    int-to-float v12, v8

    invoke-virtual {v11, v12}, Lcmg;->a(F)Lcmg;

    move-result-object v11

    invoke-virtual {v11}, Lcmg;->a()Lcme;

    .line 314
    int-to-float v4, v4

    iget v11, v2, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, v11

    int-to-float v9, v9

    const-wide/16 v12, 0x44c

    invoke-virtual {v10, v4, v9, v12, v13}, Lcme;->b(FFJ)Lcmg;

    move-result-object v4

    .line 315
    invoke-virtual {v4, v5}, Lcmg;->a(Landroid/animation/TimeInterpolator;)Lcmg;

    move-result-object v4

    invoke-virtual {v4}, Lcmg;->a()Lcme;

    .line 316
    const-wide/16 v12, 0x12c

    invoke-virtual {v10, v12, v13}, Lcme;->b(J)Lcmg;

    move-result-object v4

    int-to-float v9, v8

    const v11, 0x3f666666    # 0.9f

    mul-float/2addr v9, v11

    invoke-virtual {v4, v9}, Lcmg;->a(F)Lcmg;

    move-result-object v4

    invoke-virtual {v4}, Lcmg;->a()Lcme;

    .line 317
    const-wide/16 v12, 0x64

    invoke-virtual {v10, v12, v13}, Lcme;->b(J)Lcmg;

    move-result-object v4

    invoke-virtual {v4}, Lcmg;->a()Lcme;

    .line 318
    const-wide/16 v12, 0x32

    invoke-virtual {v10, v12, v13}, Lcme;->b(J)Lcmg;

    move-result-object v4

    int-to-float v9, v8

    const v11, 0x3f6b851f    # 0.92f

    mul-float/2addr v9, v11

    invoke-virtual {v4, v9}, Lcmg;->a(F)Lcmg;

    move-result-object v4

    invoke-virtual {v4}, Lcmg;->a()Lcme;

    .line 319
    const/4 v4, 0x0

    const/high16 v9, -0x40400000    # -1.5f

    int-to-float v11, v7

    mul-float/2addr v9, v11

    const-wide/16 v12, 0x30c

    invoke-virtual {v10, v4, v9, v12, v13}, Lcme;->a(FFJ)Lcmg;

    move-result-object v4

    .line 320
    invoke-virtual {v4, v5}, Lcmg;->a(Landroid/animation/TimeInterpolator;)Lcmg;

    move-result-object v4

    int-to-float v9, v8

    invoke-virtual {v4, v9}, Lcmg;->a(F)Lcmg;

    move-result-object v4

    invoke-virtual {v4}, Lcmg;->a()Lcme;

    .line 321
    const-wide/16 v12, 0x96

    invoke-virtual {v10, v12, v13}, Lcme;->b(J)Lcmg;

    move-result-object v4

    int-to-float v9, v8

    const v11, 0x3e99999a    # 0.3f

    mul-float/2addr v9, v11

    invoke-virtual {v4, v9}, Lcmg;->a(F)Lcmg;

    move-result-object v4

    invoke-virtual {v4}, Lcmg;->a()Lcme;

    .line 322
    invoke-virtual {v10}, Lcme;->a()Lcmd;

    move-result-object v4

    .line 323
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 326
    invoke-virtual {v4}, Lcmd;->c()J

    move-result-wide v10

    const-wide/16 v12, 0x514

    add-long/2addr v10, v12

    .line 329
    iget v4, v2, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v9

    add-int/2addr v4, v9

    div-int/lit8 v4, v4, 0x2

    .line 330
    iget v9, v2, Landroid/graphics/Rect;->top:I

    .line 332
    const/16 v12, 0x3e8

    int-to-float v13, v8

    invoke-static {v12, v13}, Lcmd;->a(IF)Lcme;

    move-result-object v12

    .line 333
    invoke-virtual {v12, v10, v11}, Lcme;->a(J)Lcme;

    move-result-object v13

    int-to-float v14, v8

    const v15, 0x3e99999a    # 0.3f

    mul-float/2addr v14, v15

    invoke-virtual {v13, v14}, Lcme;->a(F)Lcme;

    move-result-object v13

    iget v14, v2, Landroid/graphics/Rect;->left:I

    int-to-float v14, v14

    int-to-float v15, v4

    .line 334
    invoke-virtual {v13, v14, v15}, Lcme;->a(FF)Lcme;

    .line 335
    const-wide/16 v14, 0x96

    invoke-virtual {v12, v14, v15}, Lcme;->b(J)Lcmg;

    move-result-object v13

    int-to-float v14, v8

    invoke-virtual {v13, v14}, Lcmg;->a(F)Lcmg;

    move-result-object v13

    invoke-virtual {v13}, Lcmg;->a()Lcme;

    .line 336
    iget v13, v2, Landroid/graphics/Rect;->right:I

    int-to-float v13, v13

    int-to-float v4, v4

    const-wide/16 v14, 0x4b0

    invoke-virtual {v12, v13, v4, v14, v15}, Lcme;->b(FFJ)Lcmg;

    move-result-object v4

    .line 337
    invoke-virtual {v4, v5}, Lcmg;->a(Landroid/animation/TimeInterpolator;)Lcmg;

    move-result-object v4

    invoke-virtual {v4}, Lcmg;->a()Lcme;

    .line 338
    const-wide/16 v14, 0x12c

    invoke-virtual {v12, v14, v15}, Lcme;->b(J)Lcmg;

    move-result-object v4

    int-to-float v13, v8

    const v14, 0x3f666666    # 0.9f

    mul-float/2addr v13, v14

    invoke-virtual {v4, v13}, Lcmg;->a(F)Lcmg;

    move-result-object v4

    invoke-virtual {v4}, Lcmg;->a()Lcme;

    .line 339
    const-wide/16 v14, 0x64

    invoke-virtual {v12, v14, v15}, Lcme;->b(J)Lcmg;

    move-result-object v4

    invoke-virtual {v4}, Lcmg;->a()Lcme;

    .line 340
    const-wide/16 v14, 0x32

    invoke-virtual {v12, v14, v15}, Lcme;->b(J)Lcmg;

    move-result-object v4

    int-to-float v13, v8

    const v14, 0x3f6b851f    # 0.92f

    mul-float/2addr v13, v14

    invoke-virtual {v4, v13}, Lcmg;->a(F)Lcmg;

    move-result-object v4

    invoke-virtual {v4}, Lcmg;->a()Lcme;

    .line 341
    const/high16 v4, -0x40400000    # -1.5f

    int-to-float v13, v7

    mul-float/2addr v4, v13

    const/4 v13, 0x0

    const-wide/16 v14, 0x2d0

    invoke-virtual {v12, v4, v13, v14, v15}, Lcme;->a(FFJ)Lcmg;

    move-result-object v4

    .line 342
    invoke-virtual {v4, v6}, Lcmg;->a(Landroid/animation/TimeInterpolator;)Lcmg;

    move-result-object v4

    int-to-float v13, v8

    invoke-virtual {v4, v13}, Lcmg;->a(F)Lcmg;

    move-result-object v4

    invoke-virtual {v4}, Lcmg;->a()Lcme;

    .line 343
    const-wide/16 v14, 0x96

    invoke-virtual {v12, v14, v15}, Lcme;->b(J)Lcmg;

    move-result-object v4

    int-to-float v13, v8

    const v14, 0x3e99999a    # 0.3f

    mul-float/2addr v13, v14

    invoke-virtual {v4, v13}, Lcmg;->a(F)Lcmg;

    move-result-object v4

    invoke-virtual {v4}, Lcmg;->a()Lcme;

    .line 344
    invoke-virtual {v12}, Lcme;->a()Lcmd;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 346
    const/16 v4, 0x3e9

    int-to-float v12, v8

    invoke-static {v4, v12}, Lcmd;->a(IF)Lcme;

    move-result-object v4

    .line 347
    invoke-virtual {v4, v10, v11}, Lcme;->a(J)Lcme;

    move-result-object v10

    int-to-float v11, v8

    const v12, 0x3e99999a    # 0.3f

    mul-float/2addr v11, v12

    invoke-virtual {v10, v11}, Lcme;->a(F)Lcme;

    move-result-object v10

    iget v11, v2, Landroid/graphics/Rect;->right:I

    int-to-float v11, v11

    int-to-float v12, v9

    .line 348
    invoke-virtual {v10, v11, v12}, Lcme;->a(FF)Lcme;

    .line 349
    const-wide/16 v10, 0x96

    invoke-virtual {v4, v10, v11}, Lcme;->b(J)Lcmg;

    move-result-object v10

    int-to-float v11, v8

    invoke-virtual {v10, v11}, Lcmg;->a(F)Lcmg;

    move-result-object v10

    invoke-virtual {v10}, Lcmg;->a()Lcme;

    .line 350
    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    int-to-float v9, v9

    const-wide/16 v10, 0x4b0

    invoke-virtual {v4, v2, v9, v10, v11}, Lcme;->b(FFJ)Lcmg;

    move-result-object v2

    .line 351
    invoke-virtual {v2, v5}, Lcmg;->a(Landroid/animation/TimeInterpolator;)Lcmg;

    move-result-object v2

    invoke-virtual {v2}, Lcmg;->a()Lcme;

    .line 352
    const-wide/16 v10, 0x12c

    invoke-virtual {v4, v10, v11}, Lcme;->b(J)Lcmg;

    move-result-object v2

    int-to-float v5, v8

    const v9, 0x3f666666    # 0.9f

    mul-float/2addr v5, v9

    invoke-virtual {v2, v5}, Lcmg;->a(F)Lcmg;

    move-result-object v2

    invoke-virtual {v2}, Lcmg;->a()Lcme;

    .line 353
    const-wide/16 v10, 0x64

    invoke-virtual {v4, v10, v11}, Lcme;->b(J)Lcmg;

    move-result-object v2

    invoke-virtual {v2}, Lcmg;->a()Lcme;

    .line 354
    const-wide/16 v10, 0x32

    invoke-virtual {v4, v10, v11}, Lcme;->b(J)Lcmg;

    move-result-object v2

    int-to-float v5, v8

    const v9, 0x3f6b851f    # 0.92f

    mul-float/2addr v5, v9

    invoke-virtual {v2, v5}, Lcmg;->a(F)Lcmg;

    move-result-object v2

    invoke-virtual {v2}, Lcmg;->a()Lcme;

    .line 355
    const/high16 v2, 0x3fc00000    # 1.5f

    int-to-float v5, v7

    mul-float/2addr v2, v5

    const/4 v5, 0x0

    const-wide/16 v10, 0x2d0

    invoke-virtual {v4, v2, v5, v10, v11}, Lcme;->a(FFJ)Lcmg;

    move-result-object v2

    .line 356
    invoke-virtual {v2, v6}, Lcmg;->a(Landroid/animation/TimeInterpolator;)Lcmg;

    move-result-object v2

    int-to-float v5, v8

    invoke-virtual {v2, v5}, Lcmg;->a(F)Lcmg;

    move-result-object v2

    invoke-virtual {v2}, Lcmg;->a()Lcme;

    .line 357
    const-wide/16 v6, 0x96

    invoke-virtual {v4, v6, v7}, Lcme;->b(J)Lcmg;

    move-result-object v2

    int-to-float v5, v8

    const v6, 0x3e99999a    # 0.3f

    mul-float/2addr v5, v6

    invoke-virtual {v2, v5}, Lcmg;->a(F)Lcmg;

    move-result-object v2

    invoke-virtual {v2}, Lcmg;->a()Lcme;

    .line 358
    invoke-virtual {v4}, Lcme;->a()Lcmd;

    move-result-object v2

    .line 359
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 362
    invoke-virtual {v2}, Lcmd;->c()J

    move-result-wide v4

    .line 363
    invoke-super/range {p0 .. p0}, Lcjy;->e()Ljava/util/List;

    move-result-object v2

    .line 364
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcmd;

    .line 365
    invoke-virtual {v2}, Lcmd;->a()Lcme;

    move-result-object v7

    .line 366
    invoke-virtual {v2}, Lcmd;->b()J

    move-result-wide v8

    add-long/2addr v8, v4

    invoke-virtual {v7, v8, v9}, Lcme;->a(J)Lcme;

    move-result-object v2

    .line 367
    invoke-virtual {v2}, Lcme;->a()Lcmd;

    move-result-object v2

    .line 368
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_6
    move-object v2, v3

    .line 371
    goto/16 :goto_0
.end method

.method public e(I)V
    .locals 0

    .prologue
    .line 201
    invoke-super {p0, p1}, Lcjy;->e(I)V

    .line 203
    invoke-direct {p0}, Lcls;->aw()V

    .line 204
    return-void
.end method
