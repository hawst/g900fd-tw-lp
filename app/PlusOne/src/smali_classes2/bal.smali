.class public Lbal;
.super Lbaf;
.source "PG"


# instance fields
.field private final a:I

.field private final b:F

.field private c:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(JIIII)V
    .locals 3

    .prologue
    const v2, 0x3fe38e39

    .line 36
    invoke-direct {p0, p1, p2, p3}, Lbaf;-><init>(JI)V

    .line 32
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lbal;->c:Landroid/graphics/Matrix;

    .line 37
    iput p6, p0, Lbal;->a:I

    .line 38
    const-string v0, "width"

    invoke-static {p4, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    const-string v0, "height"

    invoke-static {p5, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    sget-object v0, Lbam;->a:[I

    add-int/lit8 v1, p6, -0x1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Direction not recognised."

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :pswitch_0
    int-to-float v0, p4

    int-to-float v1, p5

    mul-float/2addr v1, v2

    div-float/2addr v0, v1

    :goto_0
    iput v0, p0, Lbal;->b:F

    .line 40
    return-void

    .line 38
    :pswitch_1
    int-to-float v0, p5

    mul-float/2addr v0, v2

    int-to-float v1, p4

    div-float/2addr v0, v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public c(Layl;)Landroid/graphics/Matrix;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 47
    invoke-virtual {p0, p1}, Lbal;->b(Layl;)F

    move-result v0

    .line 48
    iget v1, p0, Lbal;->b:F

    div-float v1, v4, v1

    .line 49
    sget-object v2, Lbam;->a:[I

    iget v3, p0, Lbal;->a:I

    add-int/lit8 v3, v3, -0x1

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 67
    :goto_0
    iget-object v0, p0, Lbal;->c:Landroid/graphics/Matrix;

    return-object v0

    .line 51
    :pswitch_0
    iget-object v2, p0, Lbal;->c:Landroid/graphics/Matrix;

    invoke-virtual {v2, v1, v4}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 52
    iget-object v2, p0, Lbal;->c:Landroid/graphics/Matrix;

    sub-float v0, v4, v0

    sub-float v1, v4, v1

    mul-float/2addr v0, v1

    invoke-virtual {v2, v0, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    .line 55
    :pswitch_1
    iget-object v2, p0, Lbal;->c:Landroid/graphics/Matrix;

    invoke-virtual {v2, v1, v4}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 56
    iget-object v2, p0, Lbal;->c:Landroid/graphics/Matrix;

    sub-float v1, v4, v1

    mul-float/2addr v0, v1

    invoke-virtual {v2, v0, v5}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    .line 59
    :pswitch_2
    iget-object v2, p0, Lbal;->c:Landroid/graphics/Matrix;

    invoke-virtual {v2, v4, v1}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 60
    iget-object v2, p0, Lbal;->c:Landroid/graphics/Matrix;

    sub-float v0, v4, v0

    sub-float v1, v4, v1

    mul-float/2addr v0, v1

    invoke-virtual {v2, v5, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    .line 63
    :pswitch_3
    iget-object v2, p0, Lbal;->c:Landroid/graphics/Matrix;

    invoke-virtual {v2, v4, v1}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 64
    iget-object v2, p0, Lbal;->c:Landroid/graphics/Matrix;

    sub-float v1, v4, v1

    mul-float/2addr v0, v1

    invoke-virtual {v2, v5, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    .line 49
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
