.class final Liyt;
.super Landroid/widget/BaseAdapter;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lhep;
.implements Llnx;
.implements Llqi;
.implements Llrb;
.implements Llrc;
.implements Llrg;


# static fields
.field private static a:Landroid/graphics/drawable/Drawable;

.field private static b:Landroid/graphics/drawable/Drawable;

.field private static c:Landroid/graphics/drawable/Drawable;

.field private static d:Landroid/graphics/drawable/Drawable;


# instance fields
.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Liyx;",
            ">;"
        }
    .end annotation
.end field

.field private f:Landroid/app/Activity;

.field private g:Landroid/view/LayoutInflater;

.field private h:Liyv;

.field private i:Lhei;

.field private j:Ljava/lang/String;

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Llqr;)V
    .locals 1

    .prologue
    .line 168
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 154
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Liyt;->e:Ljava/util/List;

    .line 169
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 170
    return-void
.end method

.method private a(Lhej;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 272
    const-string v1, "page_count"

    invoke-interface {p1, v1, v0}, Lhej;->a(Ljava/lang/String;I)I

    move-result v1

    if-lez v1, :cond_0

    const-string v1, "logged_out"

    .line 273
    invoke-interface {p1, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method static synthetic d()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Liyt;->a:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic e()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Liyt;->b:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic f()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Liyt;->c:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic g()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Liyt;->d:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 14

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 211
    iget-object v0, p0, Liyt;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 213
    iget-object v0, p0, Liyt;->i:Lhei;

    invoke-interface {v0}, Lhei;->a()Ljava/util/List;

    move-result-object v7

    .line 214
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v5

    move v4, v3

    move v1, v3

    :goto_0
    if-ge v4, v5, :cond_2

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v6, p0, Liyt;->i:Lhei;

    invoke-interface {v6, v0}, Lhei;->a(I)Lhej;

    move-result-object v0

    invoke-direct {p0, v0}, Liyt;->a(Lhej;)Z

    move-result v6

    if-eqz v6, :cond_d

    const-string v6, "gplus_skinny_page"

    invoke-interface {v0, v6}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v2

    .line 215
    :goto_1
    iput-object v8, p0, Liyt;->j:Ljava/lang/String;

    .line 216
    iput-object v8, p0, Liyt;->k:Ljava/lang/String;

    move v4, v3

    .line 218
    :goto_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_b

    .line 219
    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 220
    iget-object v5, p0, Liyt;->i:Lhei;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v5, v0}, Lhei;->a(I)Lhej;

    move-result-object v8

    .line 221
    invoke-direct {p0, v8}, Liyt;->a(Lhej;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 222
    const-string v0, "account_name"

    invoke-interface {v8, v0}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 223
    if-eqz v1, :cond_0

    .line 224
    new-instance v0, Liyu;

    invoke-direct {v0, p0}, Liyu;-><init>(Liyt;)V

    .line 225
    iput-object v9, v0, Liyu;->e:Ljava/lang/String;

    .line 226
    const-string v5, "display_name"

    invoke-interface {v8, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Liyu;->f:Ljava/lang/String;

    .line 227
    const-string v5, "profile_photo_url"

    invoke-interface {v8, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Liyu;->g:Ljava/lang/String;

    .line 228
    iget-object v5, p0, Liyt;->e:Ljava/util/List;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 231
    :cond_0
    const-string v0, "page_count"

    invoke-interface {v8, v0, v3}, Lhej;->a(Ljava/lang/String;I)I

    move-result v10

    move v6, v3

    .line 232
    :goto_3
    if-ge v6, v10, :cond_a

    .line 234
    if-ne v10, v2, :cond_4

    move v0, v3

    .line 245
    :goto_4
    const-string v5, "page"

    invoke-interface {v8, v5}, Lhej;->d(Ljava/lang/String;)Lhej;

    move-result-object v5

    .line 246
    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v11

    invoke-interface {v5, v11}, Lhej;->d(Ljava/lang/String;)Lhej;

    move-result-object v11

    .line 247
    const-string v5, "gaia_id"

    invoke-interface {v11, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    .line 248
    iget-object v5, p0, Liyt;->i:Lhei;

    invoke-interface {v5, v9, v12}, Lhei;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 249
    const/4 v13, -0x1

    if-eq v5, v13, :cond_c

    iget-object v13, p0, Liyt;->i:Lhei;

    .line 250
    invoke-interface {v13, v5}, Lhei;->a(I)Lhej;

    move-result-object v5

    invoke-interface {v5}, Lhej;->a()Z

    move-result v5

    if-eqz v5, :cond_c

    .line 251
    iput-object v9, p0, Liyt;->j:Ljava/lang/String;

    .line 252
    iput-object v12, p0, Liyt;->k:Ljava/lang/String;

    move v5, v2

    .line 256
    :goto_5
    new-instance v13, Liyw;

    invoke-direct {v13, p0}, Liyw;-><init>(Liyt;)V

    .line 257
    iput v0, v13, Liyw;->a:I

    .line 258
    iput-object v9, v13, Liyw;->e:Ljava/lang/String;

    .line 259
    iput-object v12, v13, Liyw;->b:Ljava/lang/String;

    .line 260
    const-string v0, "display_name"

    invoke-interface {v11, v0}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v13, Liyw;->f:Ljava/lang/String;

    .line 261
    const-string v0, "avatar_url"

    invoke-interface {v11, v0}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v13, Liyw;->g:Ljava/lang/String;

    .line 262
    iput-boolean v5, v13, Liyw;->c:Z

    .line 263
    if-eqz v5, :cond_9

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v11

    move v5, v3

    :goto_6
    if-ge v5, v11, :cond_8

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v12, p0, Liyt;->i:Lhei;

    invoke-interface {v12, v0}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v12, "is_managed_account"

    invoke-interface {v0, v12}, Lhej;->c(Ljava/lang/String;)Z

    move-result v12

    if-nez v12, :cond_7

    invoke-interface {v0}, Lhej;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v2

    :goto_7
    if-eqz v0, :cond_9

    move v0, v2

    :goto_8
    iput-boolean v0, v13, Liyw;->d:Z

    .line 264
    iget-object v0, p0, Liyt;->e:Ljava/util/List;

    invoke-interface {v0, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_3

    .line 214
    :cond_1
    add-int/lit8 v0, v1, 0x1

    :goto_9
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v0

    goto/16 :goto_0

    :cond_2
    if-le v1, v2, :cond_3

    move v1, v2

    goto/16 :goto_1

    :cond_3
    move v1, v3

    goto/16 :goto_1

    .line 236
    :cond_4
    if-nez v6, :cond_5

    move v0, v2

    .line 237
    goto/16 :goto_4

    .line 238
    :cond_5
    add-int/lit8 v0, v10, -0x1

    if-ne v6, v0, :cond_6

    .line 239
    const/4 v0, 0x3

    goto/16 :goto_4

    .line 241
    :cond_6
    const/4 v0, 0x2

    goto/16 :goto_4

    .line 263
    :cond_7
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_6

    :cond_8
    move v0, v3

    goto :goto_7

    :cond_9
    move v0, v3

    goto :goto_8

    .line 218
    :cond_a
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto/16 :goto_2

    .line 268
    :cond_b
    invoke-virtual {p0}, Liyt;->notifyDataSetChanged()V

    .line 269
    return-void

    :cond_c
    move v5, v3

    goto/16 :goto_5

    :cond_d
    move v0, v1

    goto :goto_9
.end method

.method a(I)V
    .locals 3

    .prologue
    .line 365
    iget-object v0, p0, Liyt;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyw;

    .line 366
    iget-object v1, p0, Liyt;->j:Ljava/lang/String;

    iget-object v2, v0, Liyw;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Liyt;->k:Ljava/lang/String;

    iget-object v2, v0, Liyw;->b:Ljava/lang/String;

    .line 367
    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 368
    :cond_0
    iget-object v1, p0, Liyt;->h:Liyv;

    iget-object v2, v0, Liyw;->e:Ljava/lang/String;

    iget-object v0, v0, Liyw;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Liyv;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    :cond_1
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Liyt;->f:Landroid/app/Activity;

    .line 175
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 179
    new-instance v0, Landroid/view/ContextThemeWrapper;

    iget-object v1, p0, Liyt;->f:Landroid/app/Activity;

    const v2, 0x7f090065

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Liyt;->g:Landroid/view/LayoutInflater;

    .line 180
    const-class v0, Lhei;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Liyt;->i:Lhei;

    .line 181
    sget-object v0, Liyt;->a:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 182
    iget-object v0, p0, Liyt;->f:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 183
    const v1, 0x7f02040c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Liyt;->a:Landroid/graphics/drawable/Drawable;

    .line 184
    const v1, 0x7f02040f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Liyt;->b:Landroid/graphics/drawable/Drawable;

    .line 185
    const v1, 0x7f02040d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Liyt;->c:Landroid/graphics/drawable/Drawable;

    .line 186
    const v1, 0x7f02040e

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    sput-object v0, Liyt;->d:Landroid/graphics/drawable/Drawable;

    .line 188
    :cond_0
    return-void
.end method

.method public a(Liyv;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Liyt;->h:Liyv;

    .line 208
    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 309
    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Liyt;->i:Lhei;

    invoke-interface {v0, p0}, Lhei;->a(Lhep;)V

    .line 193
    invoke-virtual {p0}, Liyt;->a()V

    .line 194
    return-void
.end method

.method b(I)V
    .locals 3

    .prologue
    .line 373
    iget-object v0, p0, Liyt;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyw;

    .line 374
    iget-object v1, p0, Liyt;->h:Liyv;

    iget-object v2, v0, Liyw;->e:Ljava/lang/String;

    iget-object v0, v0, Liyw;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Liyv;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Liyt;->i:Lhei;

    invoke-interface {v0, p0}, Lhei;->b(Lhep;)V

    .line 199
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 324
    iget-object v0, p0, Liyt;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Liyt;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 334
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Liyt;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyx;

    invoke-virtual {v0}, Liyx;->a()I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 344
    iget-object v0, p0, Liyt;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyx;

    .line 346
    if-nez p2, :cond_0

    .line 347
    iget-object v1, p0, Liyt;->g:Landroid/view/LayoutInflater;

    invoke-virtual {v0, v1, p3}, Liyx;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 350
    :cond_0
    invoke-virtual {v0, p2, p1}, Liyx;->a(Landroid/view/View;I)V

    .line 351
    return-object p2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 319
    const/4 v0, 0x2

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 314
    const/4 v0, 0x0

    return v0
.end method

.method public l()V
    .locals 0

    .prologue
    .line 203
    invoke-virtual {p0}, Liyt;->a()V

    .line 204
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 356
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 357
    const v1, 0x7f100360

    if-ne v0, v1, :cond_1

    .line 358
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Liyt;->a(I)V

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 359
    :cond_1
    const v1, 0x7f100361

    if-ne v0, v1, :cond_0

    .line 360
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Liyt;->b(I)V

    goto :goto_0
.end method
