.class public final Ljui;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmfg;",
        "Lmfh;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lnbp;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkfo;Lnbp;)V
    .locals 6

    .prologue
    .line 22
    const-string v3, "mutatephotofilters"

    new-instance v4, Lmfg;

    invoke-direct {v4}, Lmfg;-><init>()V

    new-instance v5, Lmfh;

    invoke-direct {v5}, Lmfh;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 28
    iget-object v0, p3, Lnbp;->a:Lnwy;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing photoRef"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p3, Lnbp;->b:Lnbo;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Missing params"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p3, Lnbp;->a:Lnwy;

    iget-object v0, v0, Lnwy;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p3, Lnbp;->a:Lnwy;

    iget-object v0, v0, Lnwy;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ownerId without photoId"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, p3, Lnbp;->a:Lnwy;

    iget-object v0, v0, Lnwy;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p3, Lnbp;->a:Lnwy;

    iget-object v0, v0, Lnwy;->a:Ljava/lang/String;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "photoId without ownerId"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iget-object v0, p3, Lnbp;->b:Lnbo;

    iget-object v0, v0, Lnbo;->a:Ljava/lang/String;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MutateFilterParams without versionId"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_4
    iput-object p3, p0, Ljui;->a:Lnbp;

    .line 30
    return-void
.end method


# virtual methods
.method protected a(Lmfg;)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Ljui;->a:Lnbp;

    iput-object v0, p1, Lmfg;->a:Lnbp;

    .line 35
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Lmfg;

    invoke-virtual {p0, p1}, Ljui;->a(Lmfg;)V

    return-void
.end method
