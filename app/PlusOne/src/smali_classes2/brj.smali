.class public final Lbrj;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lbrj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbrj;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 368
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 369
    return-void
.end method

.method static synthetic a(JJ)F
    .locals 2

    .prologue
    .line 22
    invoke-static {p0, p1, p2, p3}, Lbrj;->b(JJ)F

    move-result v0

    return v0
.end method

.method public static a([JJ)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 74
    .line 75
    const-wide v2, 0x7fffffffffffffffL

    move v1, v0

    .line 78
    :goto_0
    array-length v4, p0

    if-ge v0, v4, :cond_1

    .line 79
    aget-wide v4, p0, v0

    sub-long/2addr v4, p1

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    .line 80
    cmp-long v6, v4, v2

    if-gez v6, :cond_0

    move-wide v2, v4

    move v1, v0

    .line 78
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 85
    :cond_1
    return v1
.end method

.method public static final a(Lbmk;)J
    .locals 6

    .prologue
    .line 61
    const-wide/16 v0, 0x0

    invoke-virtual {p0}, Lbmk;->f()J

    move-result-wide v2

    .line 62
    invoke-static {p0}, Lbrj;->b(Lbmk;)F

    move-result v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-long v4, v4

    sub-long/2addr v2, v4

    .line 61
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method static a(Landroid/util/LongSparseArray;J)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Lbrl;",
            ">;J)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 310
    invoke-virtual {p0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "solution array should be non-empty"

    invoke-static {v0, v2}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 314
    const v0, -0x800001

    move v2, v1

    move v3, v0

    .line 316
    :goto_1
    invoke-virtual {p0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 317
    invoke-virtual {p0, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    .line 318
    invoke-virtual {p0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrl;

    .line 319
    iget v0, v0, Lbrl;->a:F

    invoke-static {v4, v5, p1, p2}, Lbrj;->b(JJ)F

    move-result v4

    sub-float/2addr v0, v4

    .line 320
    cmpl-float v4, v0, v3

    if-lez v4, :cond_3

    move v2, v0

    move v0, v1

    .line 316
    :goto_2
    add-int/lit8 v1, v1, 0x1

    move v3, v2

    move v2, v0

    goto :goto_1

    :cond_0
    move v0, v1

    .line 310
    goto :goto_0

    .line 327
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 328
    invoke-virtual {p0, v2}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrl;

    .line 329
    :goto_3
    iget-object v2, v0, Lbrl;->c:Lbrl;

    if-eqz v2, :cond_2

    .line 330
    iget-wide v2, v0, Lbrl;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 332
    iget-object v0, v0, Lbrl;->c:Lbrl;

    goto :goto_3

    .line 334
    :cond_2
    invoke-static {v1}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    .line 336
    return-object v1

    :cond_3
    move v0, v2

    move v2, v3

    goto :goto_2
.end method

.method public static a(Ljava/util/List;Lbrm;)Ljava/util/List;
    .locals 21
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbqv;",
            ">;",
            "Lbrm;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 143
    new-instance v11, Landroid/util/LongSparseArray;

    .line 144
    invoke-virtual/range {p1 .. p1}, Lbrm;->a()I

    move-result v2

    shl-int/lit8 v2, v2, 0x1

    invoke-direct {v11, v2}, Landroid/util/LongSparseArray;-><init>(I)V

    .line 146
    new-instance v10, Landroid/util/LongSparseArray;

    .line 147
    invoke-virtual/range {p1 .. p1}, Lbrm;->a()I

    move-result v2

    shl-int/lit8 v2, v2, 0x1

    invoke-direct {v10, v2}, Landroid/util/LongSparseArray;-><init>(I)V

    .line 150
    const-wide/16 v2, 0x0

    new-instance v4, Lbrl;

    const/4 v5, 0x0

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    invoke-direct {v4, v5, v6, v7, v8}, Lbrl;-><init>(FJLbrl;)V

    invoke-virtual {v11, v2, v3, v4}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 154
    const-wide/16 v2, 0x0

    .line 156
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lbqv;

    .line 160
    invoke-virtual {v10}, Landroid/util/LongSparseArray;->clear()V

    .line 164
    const/16 v4, 0x3c

    invoke-static {v11, v4, v2, v3}, Lbrj;->a(Landroid/util/LongSparseArray;IJ)V

    .line 165
    iget-wide v6, v5, Lbqv;->b:J

    add-long v12, v2, v6

    .line 170
    const/4 v3, -0x1

    .line 172
    const/4 v2, 0x0

    move v14, v2

    move v2, v3

    :goto_1
    invoke-virtual {v11}, Landroid/util/LongSparseArray;->size()I

    move-result v3

    if-ge v14, v3, :cond_3

    .line 173
    invoke-virtual {v11, v14}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lbrl;

    .line 178
    invoke-virtual {v11, v14}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v8

    .line 180
    const/4 v3, -0x1

    if-ne v2, v3, :cond_5

    .line 181
    move-object/from16 v0, p1

    invoke-virtual {v0, v8, v9}, Lbrm;->a(J)I

    move-result v15

    .line 183
    :goto_2
    invoke-virtual/range {p1 .. p1}, Lbrm;->a()I

    move-result v2

    if-ge v15, v2, :cond_0

    .line 184
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lbrm;->a(I)J

    move-result-wide v2

    cmp-long v2, v2, v8

    if-gtz v2, :cond_0

    .line 185
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    :cond_0
    move v4, v15

    .line 192
    :goto_3
    invoke-virtual/range {p1 .. p1}, Lbrm;->a()I

    move-result v2

    if-ge v4, v2, :cond_2

    .line 194
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Lbrm;->a(I)J

    move-result-wide v2

    sub-long/2addr v2, v8

    .line 198
    iget-wide v0, v5, Lbqv;->c:J

    move-wide/from16 v18, v0

    cmp-long v6, v2, v18

    if-gtz v6, :cond_2

    .line 199
    iget-wide v0, v5, Lbqv;->a:J

    move-wide/from16 v18, v0

    cmp-long v6, v2, v18

    if-ltz v6, :cond_1

    move-object/from16 v6, p1

    .line 205
    invoke-static/range {v2 .. v10}, Lbrj;->a(JILbqv;Lbrm;Lbrl;JLandroid/util/LongSparseArray;)V

    .line 192
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 212
    :cond_2
    iget-wide v2, v5, Lbqv;->b:J

    const/4 v4, -0x1

    move-object/from16 v6, p1

    invoke-static/range {v2 .. v10}, Lbrj;->a(JILbqv;Lbrm;Lbrl;JLandroid/util/LongSparseArray;)V

    .line 172
    add-int/lit8 v2, v14, 0x1

    move v14, v2

    move v2, v15

    goto :goto_1

    :cond_3
    move-wide v2, v12

    move-object/from16 v20, v11

    move-object v11, v10

    move-object/from16 v10, v20

    .line 215
    goto/16 :goto_0

    .line 217
    :cond_4
    invoke-static {v11, v2, v3}, Lbrj;->a(Landroid/util/LongSparseArray;J)Ljava/util/List;

    move-result-object v2

    .line 218
    return-object v2

    :cond_5
    move v15, v2

    goto :goto_2
.end method

.method private static a(JILbqv;Lbrm;Lbrl;JLandroid/util/LongSparseArray;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI",
            "Lbqv;",
            "Lbrm;",
            "Lbrl;",
            "J",
            "Landroid/util/LongSparseArray",
            "<",
            "Lbrl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 230
    const-string v2, "constraint"

    const/4 v3, 0x0

    invoke-static {p3, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 231
    const-string v5, "lengthUs"

    iget-wide v6, p3, Lbqv;->a:J

    iget-wide v8, p3, Lbqv;->c:J

    move-wide v3, p0

    invoke-static/range {v3 .. v9}, Lcec;->a(JLjava/lang/CharSequence;JJ)J

    .line 233
    const/4 v2, -0x1

    if-ne p2, v2, :cond_2

    const/4 v2, 0x0

    :goto_0
    iget-wide v4, p3, Lbqv;->c:J

    iget-wide v6, p3, Lbqv;->a:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    iget-wide v4, p3, Lbqv;->b:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    iget-wide v4, p3, Lbqv;->b:J

    sub-long/2addr v4, p0

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    long-to-float v3, v4

    iget-wide v4, p3, Lbqv;->b:J

    long-to-float v4, v4

    div-float/2addr v3, v4

    sub-float/2addr v2, v3

    .line 234
    :cond_0
    iget v3, p5, Lbrl;->a:F

    add-float/2addr v3, v2

    .line 235
    add-long v4, p6, p0

    .line 236
    move-object/from16 v0, p8

    invoke-virtual {v0, v4, v5}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbrl;

    .line 239
    if-nez v2, :cond_3

    .line 240
    new-instance v2, Lbrl;

    invoke-direct {v2, v3, p0, p1, p5}, Lbrl;-><init>(FJLbrl;)V

    move-object/from16 v0, p8

    invoke-virtual {v0, v4, v5, v2}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 249
    :cond_1
    :goto_1
    return-void

    .line 233
    :cond_2
    invoke-virtual {p4, p2}, Lbrm;->b(I)F

    move-result v2

    goto :goto_0

    .line 242
    :cond_3
    iget v4, v2, Lbrl;->a:F

    cmpg-float v4, v4, v3

    if-gez v4, :cond_1

    .line 245
    iput v3, v2, Lbrl;->a:F

    .line 246
    iput-wide p0, v2, Lbrl;->b:J

    .line 247
    iput-object p5, v2, Lbrl;->c:Lbrl;

    goto :goto_1
.end method

.method static a(Landroid/util/LongSparseArray;IJ)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Lbrl;",
            ">;IJ)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 274
    invoke-virtual {p0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    .line 303
    :cond_0
    return-void

    .line 284
    :cond_1
    invoke-virtual {p0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/Long;

    move v0, v1

    .line 286
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_2

    .line 287
    invoke-virtual {p0, v0}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v2, v0

    .line 286
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 289
    :cond_2
    new-instance v0, Lbrk;

    invoke-direct {v0, p0, p2, p3}, Lbrk;-><init>(Landroid/util/LongSparseArray;J)V

    invoke-static {v2, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 299
    invoke-virtual {p0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    sub-int/2addr v0, p1

    .line 300
    :goto_1
    if-ge v1, v0, :cond_0

    .line 301
    aget-object v3, v2, v1

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Landroid/util/LongSparseArray;->remove(J)V

    .line 300
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private static b(JJ)F
    .locals 2

    .prologue
    .line 341
    sub-long v0, p0, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    long-to-float v0, v0

    const v1, 0x358637bd    # 1.0E-6f

    mul-float/2addr v0, v1

    return v0
.end method

.method public static b(Lbmk;)F
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 94
    invoke-virtual {p0}, Lbmk;->c()[J

    move-result-object v1

    .line 96
    array-length v2, v1

    const/4 v3, 0x4

    if-ge v2, v3, :cond_1

    .line 97
    sget-object v2, Lbrj;->a:Ljava/lang/String;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    array-length v1, v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x47

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Track "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " had only "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " beats so  not able to calculate beat times."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    :cond_0
    :goto_0
    return v0

    .line 103
    :cond_1
    array-length v2, v1

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    const v3, 0x3d4cccd0    # 0.050000012f

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 106
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    const v4, 0x3f733333    # 0.95f

    mul-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 107
    if-eq v2, v3, :cond_0

    .line 110
    aget-wide v4, v1, v3

    aget-wide v0, v1, v2

    sub-long v0, v4, v0

    long-to-float v0, v0

    sub-int v1, v3, v2

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_0
.end method
