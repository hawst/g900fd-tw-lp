.class final enum Lasx;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lasx;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lasx;

.field public static final enum b:Lasx;

.field public static final enum c:Lasx;

.field public static final enum d:Lasx;

.field public static final enum e:Lasx;

.field public static final enum f:Lasx;

.field public static final enum g:Lasx;

.field public static final enum h:Lasx;

.field public static final enum i:Lasx;

.field public static final enum j:Lasx;

.field public static final enum k:Lasx;

.field public static final enum l:Lasx;

.field public static final enum m:Lasx;

.field public static final enum n:Lasx;

.field public static final enum o:Lasx;

.field public static final enum p:Lasx;

.field public static final enum q:Lasx;

.field public static final enum r:Lasx;

.field public static final enum s:Lasx;

.field public static final enum t:Lasx;

.field public static final enum u:Lasx;

.field private static enum v:Lasx;

.field private static enum w:Lasx;

.field private static enum x:Lasx;

.field private static final synthetic z:[Lasx;


# instance fields
.field private y:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 26
    new-instance v0, Lasx;

    const-string v1, "UP_SYNC_BEGIN"

    const-string v2, "Up sync has begun for %s."

    invoke-direct {v0, v1, v4, v2}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->v:Lasx;

    .line 27
    new-instance v0, Lasx;

    const-string v1, "UP_SYNC_END"

    const-string v2, "Up sync has finished for %s."

    invoke-direct {v0, v1, v5, v2}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->w:Lasx;

    .line 28
    new-instance v0, Lasx;

    const-string v1, "DOWN_SYNC_BEGIN"

    const-string v2, "Down sync has begun."

    invoke-direct {v0, v1, v6, v2}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->a:Lasx;

    .line 29
    new-instance v0, Lasx;

    const-string v1, "DOWN_SYNC_END"

    const-string v2, "Down sync has finished."

    invoke-direct {v0, v1, v7, v2}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->b:Lasx;

    .line 30
    new-instance v0, Lasx;

    const-string v1, "NO_CLUSTERS_SELECTED_FOR_ANALYSIS"

    const-string v2, "No clusters selected for analysis."

    invoke-direct {v0, v1, v8, v2}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->c:Lasx;

    .line 31
    new-instance v0, Lasx;

    const-string v1, "CLUSTERS_SELECTED_FOR_ANALYSIS"

    const/4 v2, 0x5

    const-string v3, "Clusters selected for analysis. Count = %s."

    invoke-direct {v0, v1, v2, v3}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->d:Lasx;

    .line 32
    new-instance v0, Lasx;

    const-string v1, "PLUGGED_IN_ANALYSIS_BEGIN"

    const/4 v2, 0x6

    const-string v3, "Plugged-in analysis has begun."

    invoke-direct {v0, v1, v2, v3}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->e:Lasx;

    .line 33
    new-instance v0, Lasx;

    const-string v1, "PLUGGED_IN_ANALYSIS_END"

    const/4 v2, 0x7

    const-string v3, "Plugged-in analysis has finished. Successful = %s."

    invoke-direct {v0, v1, v2, v3}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->f:Lasx;

    .line 34
    new-instance v0, Lasx;

    const-string v1, "PHOTO_ANALYSIS_BEGIN"

    const/16 v2, 0x8

    const-string v3, "Photo analysis has begun for %s."

    invoke-direct {v0, v1, v2, v3}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->g:Lasx;

    .line 35
    new-instance v0, Lasx;

    const-string v1, "PHOTO_ANALYSIS_END"

    const/16 v2, 0x9

    const-string v3, "Photo analysis has finished for %s. Successful = %s."

    invoke-direct {v0, v1, v2, v3}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->h:Lasx;

    .line 36
    new-instance v0, Lasx;

    const-string v1, "VIDEO_ANALYSIS_BEGIN"

    const/16 v2, 0xa

    const-string v3, "Video analysis has begun for %s."

    invoke-direct {v0, v1, v2, v3}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->i:Lasx;

    .line 37
    new-instance v0, Lasx;

    const-string v1, "VIDEO_ANALYSIS_END"

    const/16 v2, 0xb

    const-string v3, "Video analysis has finished for %s. Successful = %s."

    invoke-direct {v0, v1, v2, v3}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->j:Lasx;

    .line 38
    new-instance v0, Lasx;

    const-string v1, "CLUSTER_ANALYSIS_END"

    const/16 v2, 0xc

    const-string v3, "Cluster analysis has finished for %s."

    invoke-direct {v0, v1, v2, v3}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->k:Lasx;

    .line 39
    new-instance v0, Lasx;

    const-string v1, "ATTRIBUTE_SET"

    const/16 v2, 0xd

    const-string v3, "Cluster attribute set for %s."

    invoke-direct {v0, v1, v2, v3}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->l:Lasx;

    .line 40
    new-instance v0, Lasx;

    const-string v1, "ATTRIBUTE_CLEARED"

    const/16 v2, 0xe

    const-string v3, "Cluster attribute cleared for %s."

    invoke-direct {v0, v1, v2, v3}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->m:Lasx;

    .line 41
    new-instance v0, Lasx;

    const-string v1, "CAN_SHOW_NEW_AAM_SYSTEM_NOTIFICATION"

    const/16 v2, 0xf

    const-string v3, "Can show new aam system notification = %s, last dismiss = %s."

    invoke-direct {v0, v1, v2, v3}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->n:Lasx;

    .line 43
    new-instance v0, Lasx;

    const-string v1, "CAN_SHOW_NEW_AAM_IN_APP_NOTIFICATION"

    const/16 v2, 0x10

    const-string v3, "Can show new aam in app notification = %s, last dismiss = %s."

    invoke-direct {v0, v1, v2, v3}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->o:Lasx;

    .line 45
    new-instance v0, Lasx;

    const-string v1, "INSPECTED_CLUSTER_TIMESTAMP"

    const/16 v2, 0x11

    const-string v3, "Cluster \'%s\' timestamp inspected. Min timestamp = %s."

    invoke-direct {v0, v1, v2, v3}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->p:Lasx;

    .line 46
    new-instance v0, Lasx;

    const-string v1, "NEW_AAM_COUNT_UPDATED"

    const/16 v2, 0x12

    const-string v3, "New aam count updated. Count = %s. Max viewed timestamp = %s."

    invoke-direct {v0, v1, v2, v3}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->q:Lasx;

    .line 47
    new-instance v0, Lasx;

    const-string v1, "NEW_AAM_COUNT_RESET"

    const/16 v2, 0x13

    const-string v3, "New aam count reset. Max viewed timestamp = %s."

    invoke-direct {v0, v1, v2, v3}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->r:Lasx;

    .line 48
    new-instance v0, Lasx;

    const-string v1, "NEW_AAM_IN_APP_NOTIFICATION_SHOWN"

    const/16 v2, 0x14

    const-string v3, "New aam in-app notification shown."

    invoke-direct {v0, v1, v2, v3}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->s:Lasx;

    .line 49
    new-instance v0, Lasx;

    const-string v1, "NEW_AAM_SYSTEM_NOTIFICATION_SHOWN"

    const/16 v2, 0x15

    const-string v3, "New aam system notification shown. Count = %s."

    invoke-direct {v0, v1, v2, v3}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->t:Lasx;

    .line 50
    new-instance v0, Lasx;

    const-string v1, "NEW_AAM_SYSTEM_NOTIFICATION_DISMISSED"

    const/16 v2, 0x16

    const-string v3, "New aam system notification dismissed."

    invoke-direct {v0, v1, v2, v3}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->u:Lasx;

    .line 51
    new-instance v0, Lasx;

    const-string v1, "NEW_AAM_IN_APP_NOTIFICATION_DISMISSED"

    const/16 v2, 0x17

    const-string v3, "New aam in-app notification dismissed."

    invoke-direct {v0, v1, v2, v3}, Lasx;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lasx;->x:Lasx;

    .line 25
    const/16 v0, 0x18

    new-array v0, v0, [Lasx;

    sget-object v1, Lasx;->v:Lasx;

    aput-object v1, v0, v4

    sget-object v1, Lasx;->w:Lasx;

    aput-object v1, v0, v5

    sget-object v1, Lasx;->a:Lasx;

    aput-object v1, v0, v6

    sget-object v1, Lasx;->b:Lasx;

    aput-object v1, v0, v7

    sget-object v1, Lasx;->c:Lasx;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lasx;->d:Lasx;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lasx;->e:Lasx;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lasx;->f:Lasx;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lasx;->g:Lasx;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lasx;->h:Lasx;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lasx;->i:Lasx;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lasx;->j:Lasx;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lasx;->k:Lasx;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lasx;->l:Lasx;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lasx;->m:Lasx;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lasx;->n:Lasx;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lasx;->o:Lasx;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lasx;->p:Lasx;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lasx;->q:Lasx;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lasx;->r:Lasx;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lasx;->s:Lasx;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lasx;->t:Lasx;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lasx;->u:Lasx;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lasx;->x:Lasx;

    aput-object v2, v0, v1

    sput-object v0, Lasx;->z:[Lasx;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    iput-object p3, p0, Lasx;->y:Ljava/lang/String;

    .line 57
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lasx;
    .locals 1

    .prologue
    .line 25
    const-class v0, Lasx;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lasx;

    return-object v0
.end method

.method public static values()[Lasx;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lasx;->z:[Lasx;

    invoke-virtual {v0}, [Lasx;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lasx;

    return-object v0
.end method


# virtual methods
.method public varargs a([Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lasx;->y:Ljava/lang/String;

    invoke-static {v0, p1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
