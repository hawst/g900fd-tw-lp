.class public final Lftm;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Lljh;


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:Landroid/graphics/Path;

.field private g:Landroid/graphics/Paint;

.field private h:Landroid/graphics/Paint;

.field private i:Landroid/widget/TextView;

.field private j:I

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lftm;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lftm;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 55
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 58
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    invoke-virtual {p0, v6}, Lftm;->setWillNotDraw(Z)V

    .line 61
    invoke-virtual {p0}, Lftm;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00dc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 64
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v1, p0, Lftm;->g:Landroid/graphics/Paint;

    .line 65
    iget-object v1, p0, Lftm;->g:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 66
    iget-object v1, p0, Lftm;->g:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 67
    iget-object v1, p0, Lftm;->g:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/DashPathEffect;

    const/4 v3, 0x2

    new-array v3, v3, [F

    int-to-float v4, v0

    aput v4, v3, v6

    int-to-float v0, v0

    aput v0, v3, v5

    const/4 v0, 0x0

    invoke-direct {v2, v3, v0}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 69
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v5}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lftm;->h:Landroid/graphics/Paint;

    .line 70
    iget-object v0, p0, Lftm;->h:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 72
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lftm;->f:Landroid/graphics/Path;

    .line 73
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lftm;->i:Landroid/widget/TextView;

    .line 75
    invoke-direct {p0}, Lftm;->c()V

    .line 77
    iget-object v0, p0, Lftm;->i:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lftm;->addView(Landroid/view/View;)V

    .line 78
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 235
    iget-object v0, p0, Lftm;->g:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lftm;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00cf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 236
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 240
    invoke-direct {p0}, Lftm;->c()V

    .line 241
    const v0, 0x7f100040

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lftm;->setTag(ILjava/lang/Object;)V

    .line 242
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lftm;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 204
    return-void
.end method

.method public a(IF)V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lftm;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 200
    return-void
.end method

.method public a(Landroid/graphics/Typeface;)V
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lftm;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 208
    return-void
.end method

.method public a(Lfre;)V
    .locals 1

    .prologue
    .line 224
    iget v0, p1, Lfre;->a:I

    iput v0, p0, Lftm;->a:I

    .line 225
    iget v0, p1, Lfre;->b:I

    iput v0, p0, Lftm;->b:I

    .line 226
    iget v0, p1, Lfre;->c:I

    iput v0, p0, Lftm;->d:I

    .line 227
    iget v0, p1, Lfre;->c:I

    iput v0, p0, Lftm;->e:I

    .line 228
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lftm;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 188
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 220
    iput-boolean p1, p0, Lftm;->k:Z

    .line 221
    return-void
.end method

.method public b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lftm;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lftm;->i:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setGravity(I)V

    .line 212
    return-void
.end method

.method public c(I)V
    .locals 2

    .prologue
    .line 215
    iput p1, p0, Lftm;->c:I

    .line 216
    iget-object v0, p0, Lftm;->g:Landroid/graphics/Paint;

    int-to-float v1, p1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 217
    return-void
.end method

.method public d(I)V
    .locals 1

    .prologue
    .line 231
    iget-object v0, p0, Lftm;->h:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 232
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 182
    iget-object v0, p0, Lftm;->f:Landroid/graphics/Path;

    iget-object v1, p0, Lftm;->h:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 183
    iget-object v0, p0, Lftm;->f:Landroid/graphics/Path;

    iget-object v1, p0, Lftm;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 184
    return-void
.end method

.method public onLayout(ZIIII)V
    .locals 10

    .prologue
    .line 130
    sub-int v0, p5, p3

    .line 131
    sub-int v1, p4, p2

    .line 132
    div-int/lit8 v1, v1, 0x2

    .line 133
    iget v2, p0, Lftm;->j:I

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v1, v2

    .line 134
    iget v3, p0, Lftm;->j:I

    add-int/2addr v3, v2

    .line 135
    invoke-virtual {p0}, Lftm;->getPaddingTop()I

    move-result v4

    .line 136
    invoke-virtual {p0}, Lftm;->getPaddingBottom()I

    move-result v5

    sub-int v5, v0, v5

    .line 137
    iget v0, p0, Lftm;->c:I

    add-int/2addr v0, v2

    iget v6, p0, Lftm;->b:I

    add-int/2addr v6, v0

    .line 138
    iget v0, p0, Lftm;->c:I

    add-int/2addr v0, v4

    iget v7, p0, Lftm;->a:I

    add-int/2addr v7, v0

    iget-boolean v0, p0, Lftm;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v7

    .line 143
    iget v7, p0, Lftm;->c:I

    int-to-float v7, v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v7, v8

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v8

    double-to-int v7, v8

    .line 144
    add-int/2addr v2, v7

    .line 145
    sub-int/2addr v3, v7

    .line 146
    add-int/2addr v4, v7

    .line 147
    sub-int/2addr v5, v7

    .line 149
    iget-boolean v7, p0, Lftm;->k:Z

    if-eqz v7, :cond_1

    .line 151
    iget-object v7, p0, Lftm;->f:Landroid/graphics/Path;

    invoke-virtual {v7}, Landroid/graphics/Path;->reset()V

    .line 152
    iget-object v7, p0, Lftm;->f:Landroid/graphics/Path;

    int-to-float v8, v2

    int-to-float v9, v4

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 153
    iget-object v7, p0, Lftm;->f:Landroid/graphics/Path;

    int-to-float v2, v2

    iget v8, p0, Lftm;->d:I

    sub-int v8, v5, v8

    int-to-float v8, v8

    invoke-virtual {v7, v2, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 154
    iget-object v2, p0, Lftm;->f:Landroid/graphics/Path;

    iget v7, p0, Lftm;->e:I

    sub-int v7, v1, v7

    int-to-float v7, v7

    iget v8, p0, Lftm;->d:I

    sub-int v8, v5, v8

    int-to-float v8, v8

    invoke-virtual {v2, v7, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 155
    iget-object v2, p0, Lftm;->f:Landroid/graphics/Path;

    int-to-float v7, v1

    int-to-float v8, v5

    invoke-virtual {v2, v7, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 156
    iget-object v2, p0, Lftm;->f:Landroid/graphics/Path;

    iget v7, p0, Lftm;->e:I

    add-int/2addr v1, v7

    int-to-float v1, v1

    iget v7, p0, Lftm;->d:I

    sub-int v7, v5, v7

    int-to-float v7, v7

    invoke-virtual {v2, v1, v7}, Landroid/graphics/Path;->lineTo(FF)V

    .line 157
    iget-object v1, p0, Lftm;->f:Landroid/graphics/Path;

    int-to-float v2, v3

    iget v7, p0, Lftm;->d:I

    sub-int/2addr v5, v7

    int-to-float v5, v5

    invoke-virtual {v1, v2, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 158
    iget-object v1, p0, Lftm;->f:Landroid/graphics/Path;

    int-to-float v2, v3

    int-to-float v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 159
    iget-object v1, p0, Lftm;->f:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    .line 173
    :goto_1
    iget-object v1, p0, Lftm;->i:Landroid/widget/TextView;

    iget-object v2, p0, Lftm;->i:Landroid/widget/TextView;

    .line 176
    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v6

    iget-object v3, p0, Lftm;->i:Landroid/widget/TextView;

    .line 177
    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    .line 173
    invoke-virtual {v1, v6, v0, v2, v3}, Landroid/widget/TextView;->layout(IIII)V

    .line 178
    return-void

    .line 138
    :cond_0
    iget v0, p0, Lftm;->d:I

    goto :goto_0

    .line 162
    :cond_1
    iget-object v7, p0, Lftm;->f:Landroid/graphics/Path;

    invoke-virtual {v7}, Landroid/graphics/Path;->reset()V

    .line 163
    iget-object v7, p0, Lftm;->f:Landroid/graphics/Path;

    int-to-float v8, v2

    iget v9, p0, Lftm;->d:I

    add-int/2addr v9, v4

    int-to-float v9, v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Path;->moveTo(FF)V

    .line 164
    iget-object v7, p0, Lftm;->f:Landroid/graphics/Path;

    int-to-float v2, v2

    int-to-float v8, v5

    invoke-virtual {v7, v2, v8}, Landroid/graphics/Path;->lineTo(FF)V

    .line 165
    iget-object v2, p0, Lftm;->f:Landroid/graphics/Path;

    int-to-float v7, v3

    int-to-float v5, v5

    invoke-virtual {v2, v7, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 166
    iget-object v2, p0, Lftm;->f:Landroid/graphics/Path;

    int-to-float v3, v3

    iget v5, p0, Lftm;->d:I

    add-int/2addr v5, v4

    int-to-float v5, v5

    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 167
    iget-object v2, p0, Lftm;->f:Landroid/graphics/Path;

    iget v3, p0, Lftm;->e:I

    add-int/2addr v3, v1

    int-to-float v3, v3

    iget v5, p0, Lftm;->d:I

    add-int/2addr v5, v4

    int-to-float v5, v5

    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 168
    iget-object v2, p0, Lftm;->f:Landroid/graphics/Path;

    int-to-float v3, v1

    int-to-float v5, v4

    invoke-virtual {v2, v3, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 169
    iget-object v2, p0, Lftm;->f:Landroid/graphics/Path;

    iget v3, p0, Lftm;->e:I

    sub-int/2addr v1, v3

    int-to-float v1, v1

    iget v3, p0, Lftm;->d:I

    add-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v2, v1, v3}, Landroid/graphics/Path;->lineTo(FF)V

    .line 170
    iget-object v1, p0, Lftm;->f:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->close()V

    goto :goto_1
.end method

.method public onMeasure(II)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v5, -0x80000000

    .line 82
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 83
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 85
    iget v2, p0, Lftm;->b:I

    iget v3, p0, Lftm;->a:I

    invoke-virtual {p0}, Lftm;->getPaddingLeft()I

    move-result v4

    sub-int/2addr v0, v4

    invoke-virtual {p0}, Lftm;->getPaddingRight()I

    move-result v4

    sub-int/2addr v0, v4

    iget v4, p0, Lftm;->c:I

    shl-int/lit8 v4, v4, 0x1

    sub-int/2addr v0, v4

    shl-int/lit8 v2, v2, 0x1

    sub-int/2addr v0, v2

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {p0}, Lftm;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lftm;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lftm;->c:I

    shl-int/lit8 v2, v2, 0x1

    sub-int/2addr v1, v2

    shl-int/lit8 v2, v3, 0x1

    sub-int/2addr v1, v2

    iget v2, p0, Lftm;->d:I

    sub-int/2addr v1, v2

    invoke-static {v6, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    iget-object v2, p0, Lftm;->i:Landroid/widget/TextView;

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/widget/TextView;->measure(II)V

    .line 86
    iget-object v0, p0, Lftm;->i:Landroid/widget/TextView;

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {v0}, Landroid/widget/TextView;->getLineCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2, v1}, Landroid/widget/TextView;->getLineBounds(ILandroid/graphics/Rect;)I

    iget v0, v1, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lftm;->i:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    if-le v0, v1, :cond_0

    .line 89
    const v0, 0x7f100040

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lftm;->setTag(ILjava/lang/Object;)V

    .line 94
    :cond_0
    iget-object v0, p0, Lftm;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lftm;->b:I

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    iget v1, p0, Lftm;->c:I

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    iput v0, p0, Lftm;->j:I

    .line 96
    iget-object v0, p0, Lftm;->i:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    iget v1, p0, Lftm;->a:I

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    iget v1, p0, Lftm;->c:I

    shl-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    iget v1, p0, Lftm;->d:I

    add-int/2addr v0, v1

    .line 99
    iget v1, p0, Lftm;->j:I

    invoke-virtual {p0}, Lftm;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    invoke-virtual {p0}, Lftm;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    .line 100
    invoke-virtual {p0}, Lftm;->getPaddingBottom()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p0}, Lftm;->getPaddingTop()I

    move-result v2

    add-int/2addr v0, v2

    .line 99
    invoke-virtual {p0, v1, v0}, Lftm;->setMeasuredDimension(II)V

    .line 101
    return-void
.end method
