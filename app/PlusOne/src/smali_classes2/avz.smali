.class public final Lavz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lavz;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 112
    new-instance v0, Lawa;

    invoke-direct {v0}, Lawa;-><init>()V

    sput-object v0, Lavz;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/net/Uri;Z)V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const-string v0, "uri"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lavz;->a:Landroid/net/Uri;

    .line 68
    iput-boolean p2, p0, Lavz;->b:Z

    .line 69
    return-void
.end method

.method public static a(Landroid/net/Uri;)Lavz;
    .locals 2

    .prologue
    .line 31
    new-instance v0, Lavz;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lavz;-><init>(Landroid/net/Uri;Z)V

    return-object v0
.end method

.method public static a(Lbuf;)Lavz;
    .locals 3

    .prologue
    .line 55
    new-instance v0, Lavz;

    iget-object v1, p0, Lbuf;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-boolean v2, p0, Lbuf;->c:Z

    invoke-direct {v0, v1, v2}, Lavz;-><init>(Landroid/net/Uri;Z)V

    return-object v0
.end method

.method public static b(Landroid/net/Uri;)Lavz;
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lavz;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lavz;-><init>(Landroid/net/Uri;Z)V

    return-object v0
.end method


# virtual methods
.method public a()Lbuf;
    .locals 2

    .prologue
    .line 72
    new-instance v0, Lbuf;

    invoke-direct {v0}, Lbuf;-><init>()V

    .line 73
    iget-object v1, p0, Lavz;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lbuf;->b:Ljava/lang/String;

    .line 74
    iget-boolean v1, p0, Lavz;->b:Z

    iput-boolean v1, v0, Lbuf;->c:Z

    .line 75
    return-object v0
.end method

.method public c(Landroid/net/Uri;)Lavz;
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lavz;->a:Landroid/net/Uri;

    invoke-virtual {v0, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lavz;

    iget-boolean v1, p0, Lavz;->b:Z

    invoke-direct {v0, p1, v1}, Lavz;-><init>(Landroid/net/Uri;Z)V

    move-object p0, v0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 101
    if-ne p1, p0, :cond_1

    .line 109
    :cond_0
    :goto_0
    return v0

    .line 104
    :cond_1
    instance-of v2, p1, Lavz;

    if-nez v2, :cond_2

    move v0, v1

    .line 105
    goto :goto_0

    .line 107
    :cond_2
    check-cast p1, Lavz;

    .line 108
    iget-boolean v2, p0, Lavz;->b:Z

    iget-boolean v3, p1, Lavz;->b:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lavz;->a:Landroid/net/Uri;

    iget-object v3, p1, Lavz;->a:Landroid/net/Uri;

    .line 109
    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 96
    iget-boolean v0, p0, Lavz;->b:Z

    iget-object v1, p0, Lavz;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->hashCode()I

    move-result v1

    invoke-static {v0, v1}, Lbqh;->a(ZI)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 91
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lavz;->a:Landroid/net/Uri;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-boolean v3, p0, Lavz;->b:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lavz;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 86
    iget-boolean v0, p0, Lavz;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 87
    return-void

    .line 86
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
