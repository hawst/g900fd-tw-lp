.class public Lgjm;
.super Ljava/lang/Object;


# instance fields
.field private a:Landroid/graphics/Bitmap;

.field private b:Landroid/os/Bundle;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lgjm;->b:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method public a()Lgjl;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lgjl;

    invoke-direct {v0}, Lgjl;-><init>()V

    iget-object v1, p0, Lgjm;->a:Landroid/graphics/Bitmap;

    invoke-static {v0, v1}, Lgjl;->a(Lgjl;Landroid/graphics/Bitmap;)Lgjl;

    move-result-object v0

    invoke-static {v0, v2}, Lgjl;->a(Lgjl;Ljava/lang/String;)Lgjl;

    move-result-object v0

    iget-object v1, p0, Lgjm;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lgjl;->b(Lgjl;Ljava/lang/String;)Lgjl;

    move-result-object v0

    iget-object v1, p0, Lgjm;->b:Landroid/os/Bundle;

    invoke-static {v0, v1}, Lgjl;->a(Lgjl;Landroid/os/Bundle;)Lgjl;

    move-result-object v0

    invoke-static {v0, v2}, Lgjl;->c(Lgjl;Ljava/lang/String;)Lgjl;

    move-result-object v0

    invoke-static {v0, v2}, Lgjl;->d(Lgjl;Ljava/lang/String;)Lgjl;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/graphics/Bitmap;)Lgjm;
    .locals 0

    iput-object p1, p0, Lgjm;->a:Landroid/graphics/Bitmap;

    return-object p0
.end method

.method public a(Landroid/os/Bundle;)Lgjm;
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lgjm;->b:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lgjm;
    .locals 0

    iput-object p1, p0, Lgjm;->c:Ljava/lang/String;

    return-object p0
.end method
