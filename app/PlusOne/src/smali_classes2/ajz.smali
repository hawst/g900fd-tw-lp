.class public final Lajz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Laan;

.field private final b:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Laak;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Laak;",
            "Ljava/util/HashSet",
            "<",
            "Laak;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Laan;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lajz;->b:Ljava/util/LinkedList;

    .line 24
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lajz;->c:Ljava/util/HashMap;

    .line 30
    iput-object p1, p0, Lajz;->a:Laan;

    .line 31
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 122
    :cond_0
    :goto_0
    iget-object v0, p0, Lajz;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 123
    iget-object v0, p0, Lajz;->b:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laak;

    .line 126
    iget-object v3, v0, Laak;->mConnectedOutputPortArray:[Lacv;

    array-length v4, v3

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 127
    invoke-virtual {v5}, Lacv;->d()Lacp;

    move-result-object v5

    invoke-virtual {v5}, Lacp;->g()Laak;

    move-result-object v5

    invoke-virtual {v5}, Laak;->e()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 128
    const/4 v1, 0x1

    .line 133
    :goto_2
    if-nez v1, :cond_0

    .line 137
    invoke-direct {p0, v0}, Lajz;->b(Laak;)V

    goto :goto_0

    .line 126
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 140
    :cond_2
    return-void

    :cond_3
    move v1, v2

    goto :goto_2
.end method

.method private a(Laak;)V
    .locals 6

    .prologue
    .line 46
    iget-object v2, p1, Laak;->mConnectedOutputPortArray:[Lacv;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 47
    invoke-virtual {v0}, Lacv;->d()Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->g()Laak;

    move-result-object v4

    .line 48
    iget-object v0, p0, Lajz;->c:Ljava/util/HashMap;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lajz;->c:Ljava/util/HashMap;

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lajz;->c:Ljava/util/HashMap;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 49
    invoke-direct {p0, v4}, Lajz;->a(Laak;)V

    .line 46
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 51
    :cond_1
    return-void
.end method

.method private b(Laak;)V
    .locals 4

    .prologue
    .line 91
    invoke-virtual {p1}, Laak;->f()V

    .line 93
    iget-object v0, p0, Lajz;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 111
    :cond_0
    return-void

    .line 98
    :cond_1
    iget-object v0, p0, Lajz;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laak;

    .line 99
    iget-object v2, v0, Laak;->mConnectedOutputPortArray:[Lacv;

    .line 101
    array-length v2, v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 103
    invoke-direct {p0, v0}, Lajz;->b(Laak;)V

    goto :goto_0

    .line 104
    :cond_3
    iget-object v2, p0, Lajz;->b:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 107
    iget-object v2, p0, Lajz;->b:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lajz;->a:Laan;

    invoke-virtual {v0, p1}, Laan;->a(Ljava/lang/String;)Laak;

    move-result-object v0

    invoke-direct {p0, v0}, Lajz;->a(Laak;)V

    .line 39
    return-void
.end method

.method public varargs a([Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 71
    iget-object v0, p0, Lajz;->c:Ljava/util/HashMap;

    const-string v1, "mInputMapping"

    const-string v2, "cannot deactivate filters before analyzeConnections is called."

    const/4 v3, 0x0

    invoke-static {v0, v1, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must not be empty"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v0, v2}, Lcec;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    .line 74
    :cond_0
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    aget-object v2, p1, v0

    .line 75
    iget-object v3, p0, Lajz;->a:Laan;

    invoke-virtual {v3, v2}, Laan;->a(Ljava/lang/String;)Laak;

    move-result-object v2

    invoke-direct {p0, v2}, Lajz;->b(Laak;)V

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80
    :cond_1
    invoke-direct {p0}, Lajz;->a()V

    .line 81
    return-void
.end method
