.class public final Lggs;
.super Lu;

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lu;",
        "Landroid/content/DialogInterface$OnCancelListener;",
        "Lbc",
        "<",
        "Lgfd;",
        ">;"
    }
.end annotation


# instance fields
.field private N:Z

.field private O:I

.field private P:Lgfd;

.field private final Q:Landroid/os/Handler;

.field private final R:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lggu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lu;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lggs;->O:I

    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lggs;->Q:Landroid/os/Handler;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lggs;->R:Landroid/util/SparseArray;

    return-void
.end method

.method public static a(Lz;)Lggs;
    .locals 4

    const-string v0, "Must be called from main thread of process"

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    if-eq v1, v2, :cond_0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-virtual {p0}, Lz;->f()Lae;

    move-result-object v1

    :try_start_0
    const-string v0, "GmsSupportLifecycleFragment"

    invoke-virtual {v1, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lggs;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    if-eqz v0, :cond_1

    iget-boolean v2, v0, Lu;->m:Z

    if-eqz v2, :cond_2

    :cond_1
    new-instance v0, Lggs;

    invoke-direct {v0}, Lggs;-><init>()V

    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v2

    const-string v3, "GmsSupportLifecycleFragment"

    invoke-virtual {v2, v0, v3}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    move-result-object v2

    invoke-virtual {v2}, Lat;->b()I

    invoke-virtual {v1}, Lae;->b()Z

    :cond_2
    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Fragment with tag GmsSupportLifecycleFragment is not a SupportLifecycleFragment"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a()V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lggs;->N:Z

    const/4 v1, -0x1

    iput v1, p0, Lggs;->O:I

    iput-object v4, p0, Lggs;->P:Lgfd;

    invoke-virtual {p0}, Lggs;->w()Lbb;

    move-result-object v1

    :goto_0
    iget-object v2, p0, Lggs;->R:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    iget-object v2, p0, Lggs;->R:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lggs;->e(I)Lggt;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lggt;->b()V

    :cond_0
    invoke-virtual {v1, v2, v4, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-void
.end method

.method private a(ILgfd;)V
    .locals 1

    iget-object v0, p0, Lggs;->R:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggu;

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lggs;->d(I)V

    iget-object v0, v0, Lggu;->b:Lggc;

    if-eqz v0, :cond_0

    invoke-interface {v0, p2}, Lggc;->a(Lgfd;)V

    :cond_0
    invoke-direct {p0}, Lggs;->a()V

    return-void
.end method

.method static synthetic a(Lggs;)V
    .locals 0

    invoke-direct {p0}, Lggs;->a()V

    return-void
.end method

.method static synthetic a(Lggs;ILgfd;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lggs;->a(ILgfd;)V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Lgfd;",
            ">;"
        }
    .end annotation

    new-instance v1, Lggt;

    iget-object v2, p0, Lu;->t:Lz;

    iget-object v0, p0, Lggs;->R:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggu;

    iget-object v0, v0, Lggu;->a:Lgfz;

    invoke-direct {v1, v2, v0}, Lggt;-><init>(Landroid/content/Context;Lgfz;)V

    return-object v1
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    invoke-direct {p0}, Lggs;->a()V

    :goto_1
    return-void

    :pswitch_0
    iget-object v2, p0, Lu;->t:Lz;

    invoke-static {v2}, Lgfk;->a(Landroid/content/Context;)I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    :pswitch_1
    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    goto :goto_0

    :cond_1
    iget v0, p0, Lggs;->O:I

    iget-object v1, p0, Lggs;->P:Lgfd;

    invoke-direct {p0, v0, v1}, Lggs;->a(ILgfd;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public a(ILgfz;Lggc;)V
    .locals 3

    const-string v0, "GoogleApiClient instance cannot be null"

    invoke-static {p2, v0}, Lgih;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lggs;->R:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Already managing a GoogleApiClient with id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgih;->a(ZLjava/lang/Object;)V

    new-instance v0, Lggu;

    invoke-direct {v0, p2, p3}, Lggu;-><init>(Lgfz;Lggc;)V

    iget-object v1, p0, Lggs;->R:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    iget-object v0, p0, Lu;->t:Lz;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lggs;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lu;->a(Landroid/app/Activity;)V

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lggs;->R:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lggs;->R:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lggs;->e(I)Lggt;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v0, p0, Lggs;->R:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggu;

    iget-object v0, v0, Lggu;->a:Lgfz;

    iget-object v3, v3, Lggt;->b:Lgfz;

    if-eq v0, v3, :cond_0

    invoke-virtual {p0}, Lggs;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v2, v4, p0}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lggs;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, v2, v4, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_1

    :cond_1
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lu;->a(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string v0, "resolving_error"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lggs;->N:Z

    const-string v0, "failed_client_id"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lggs;->O:I

    iget v0, p0, Lggs;->O:I

    if-ltz v0, :cond_0

    new-instance v1, Lgfd;

    const-string v0, "failed_status"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v0, "failed_resolution"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    invoke-direct {v1, v2, v0}, Lgfd;-><init>(ILandroid/app/PendingIntent;)V

    iput-object v1, p0, Lggs;->P:Lgfd;

    :cond_0
    return-void
.end method

.method public a(Ldo;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Lgfd;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    iget v1, p0, Lggs;->O:I

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lggs;->a()V

    :cond_0
    return-void
.end method

.method public a(Ldo;Lgfd;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Lgfd;",
            ">;",
            "Lgfd;",
            ")V"
        }
    .end annotation

    invoke-virtual {p2}, Lgfd;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    iget v1, p0, Lggs;->O:I

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lggs;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    iget-boolean v1, p0, Lggs;->N:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lggs;->N:Z

    iput v0, p0, Lggs;->O:I

    iput-object p2, p0, Lggs;->P:Lgfd;

    iget-object v1, p0, Lggs;->Q:Landroid/os/Handler;

    new-instance v2, Lggv;

    invoke-direct {v2, p0, v0, p2}, Lggv;-><init>(Lggs;ILgfd;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Lgfd;

    invoke-virtual {p0, p1, p2}, Lggs;->a(Ldo;Lgfd;)V

    return-void
.end method

.method public c(I)Lgfz;
    .locals 1

    iget-object v0, p0, Lu;->t:Lz;

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lggs;->e(I)Lggt;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lggt;->b:Lgfz;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(I)V
    .locals 1

    invoke-virtual {p0}, Lggs;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbb;->a(I)V

    iget-object v0, p0, Lggs;->R:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    return-void
.end method

.method e(I)Lggt;
    .locals 3

    :try_start_0
    invoke-virtual {p0}, Lggs;->w()Lbb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbb;->b(I)Ldo;

    move-result-object v0

    check-cast v0, Lggt;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unknown loader in SupportLifecycleFragment"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lu;->e(Landroid/os/Bundle;)V

    const-string v0, "resolving_error"

    iget-boolean v1, p0, Lggs;->N:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget v0, p0, Lggs;->O:I

    if-ltz v0, :cond_0

    const-string v0, "failed_client_id"

    iget v1, p0, Lggs;->O:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "failed_status"

    iget-object v1, p0, Lggs;->P:Lgfd;

    invoke-virtual {v1}, Lgfd;->c()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v0, "failed_resolution"

    iget-object v1, p0, Lggs;->P:Lgfd;

    invoke-virtual {v1}, Lgfd;->d()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public g()V
    .locals 4

    invoke-super {p0}, Lu;->g()V

    iget-boolean v0, p0, Lggs;->N:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lggs;->R:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    invoke-virtual {p0}, Lggs;->w()Lbb;

    move-result-object v1

    iget-object v2, p0, Lggs;->R:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    iget v0, p0, Lggs;->O:I

    iget-object v1, p0, Lggs;->P:Lgfd;

    invoke-direct {p0, v0, v1}, Lggs;->a(ILgfd;)V

    return-void
.end method
