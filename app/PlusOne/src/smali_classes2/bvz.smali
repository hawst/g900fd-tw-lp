.class final Lbvz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private synthetic a:Lbvy;


# direct methods
.method constructor <init>(Lbvy;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lbvz;->a:Lbvy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 69
    iget-object v0, p0, Lbvz;->a:Lbvy;

    iget-object v1, v0, Lbvy;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 70
    :try_start_0
    iget-object v0, p0, Lbvz;->a:Lbvy;

    iget-boolean v0, v0, Lbvy;->c:Z

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lbvz;->a:Lbvy;

    iput-object p2, v0, Lbvy;->b:Ljava/lang/Object;

    .line 72
    iget-object v0, p0, Lbvz;->a:Lbvy;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lbvy;->c:Z

    .line 73
    iget-object v0, p0, Lbvz;->a:Lbvy;

    iget-object v0, v0, Lbvy;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 75
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    .line 57
    iget-object v0, p0, Lbvz;->a:Lbvy;

    iget-object v1, v0, Lbvy;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 58
    :try_start_0
    iget-object v0, p0, Lbvz;->a:Lbvy;

    const/4 v2, 0x0

    iput-object v2, v0, Lbvy;->b:Ljava/lang/Object;

    .line 59
    iget-object v0, p0, Lbvz;->a:Lbvy;

    iget-boolean v0, v0, Lbvy;->c:Z

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lbvz;->a:Lbvy;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lbvy;->c:Z

    .line 61
    iget-object v0, p0, Lbvz;->a:Lbvy;

    iget-object v0, v0, Lbvy;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 63
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
