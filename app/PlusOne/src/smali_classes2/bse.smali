.class final Lbse;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lbsc;

.field private synthetic b:Lbss;

.field private synthetic c:Lbge;


# direct methods
.method constructor <init>(Lbsc;Lbss;Lbge;)V
    .locals 0

    .prologue
    .line 203
    iput-object p1, p0, Lbse;->a:Lbsc;

    iput-object p2, p0, Lbse;->b:Lbss;

    iput-object p3, p0, Lbse;->c:Lbge;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 207
    iget-object v0, p0, Lbse;->a:Lbsc;

    invoke-static {v0}, Lbsc;->a(Lbsc;)Lasy;

    move-result-object v0

    invoke-virtual {v0}, Lasy;->a()V

    .line 208
    const/4 v1, 0x0

    .line 210
    :try_start_0
    new-instance v0, Lbry;

    invoke-direct {v0}, Lbry;-><init>()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 211
    const/4 v1, 0x2

    const v2, 0xac44

    const/4 v3, 0x2

    :try_start_1
    invoke-interface {v0, v1, v2, v3}, Lbrw;->a(III)V

    .line 214
    iget-object v1, p0, Lbse;->a:Lbsc;

    invoke-static {v1}, Lbsc;->b(Lbsc;)Lbsu;

    move-result-object v1

    iget-object v2, p0, Lbse;->b:Lbss;

    iget-object v3, p0, Lbse;->c:Lbge;

    invoke-virtual {v1, v2, v0, v3}, Lbsu;->a(Lbss;Lbrw;Lbge;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 236
    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 237
    iget-object v0, p0, Lbse;->c:Lbge;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 238
    :goto_0
    return-void

    .line 236
    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_1
    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 237
    iget-object v0, p0, Lbse;->c:Lbge;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    goto :goto_0

    .line 217
    :catch_1
    move-exception v0

    .line 219
    :goto_2
    :try_start_2
    invoke-static {}, Lbsc;->g()Ljava/lang/String;

    move-result-object v2

    const-string v3, "exception during playback"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 223
    iget-object v2, p0, Lbse;->a:Lbsc;

    invoke-static {v2}, Lbsc;->c(Lbsc;)Lbjp;

    move-result-object v2

    invoke-static {v2}, Lcgl;->a(Lcgk;)V

    .line 224
    iget-object v2, p0, Lbse;->a:Lbsc;

    invoke-static {v2}, Lbsc;->d(Lbsc;)Lbjp;

    move-result-object v2

    invoke-static {v2}, Lcgl;->a(Lcgk;)V

    .line 226
    iget-object v2, p0, Lbse;->a:Lbsc;

    invoke-static {v2}, Lbsc;->f(Lbsc;)Ljava/util/concurrent/Executor;

    move-result-object v2

    new-instance v3, Lbsf;

    invoke-direct {v3, p0, v0}, Lbsf;-><init>(Lbse;Ljava/lang/Exception;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 236
    invoke-static {v1}, Lcgl;->a(Lcgk;)V

    .line 237
    iget-object v0, p0, Lbse;->c:Lbge;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    goto :goto_0

    .line 236
    :catchall_0
    move-exception v0

    :goto_3
    invoke-static {v1}, Lcgl;->a(Lcgk;)V

    .line 237
    iget-object v1, p0, Lbse;->c:Lbge;

    invoke-static {v1}, Lcgl;->a(Lcgk;)V

    throw v0

    .line 236
    :catchall_1
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_3

    .line 217
    :catch_2
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_2

    .line 236
    :catch_3
    move-exception v1

    goto :goto_1
.end method
