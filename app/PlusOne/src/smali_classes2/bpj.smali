.class final Lbpj;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Lbmp;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 29
    new-instance v0, Lbmp;

    const-wide/16 v2, 0x0

    const-wide/32 v4, 0x1e8480

    invoke-direct {v0, v2, v3, v4, v5}, Lbmp;-><init>(JJ)V

    sput-object v0, Lbpj;->a:Lbmp;

    return-void
.end method

.method private static a(Lbpy;Ljava/util/List;)Lbpy;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbpy",
            "<",
            "Ljeg;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)",
            "Lbpy",
            "<",
            "Ljeg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 258
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 259
    iget-object v2, v0, Lbmd;->d:Lbmg;

    sget-object v3, Lbmg;->a:Lbmg;

    if-ne v2, v3, :cond_0

    .line 260
    iget-object v2, v0, Lbmd;->e:Ljeg;

    iget-object v0, v0, Lbmd;->f:Lbmp;

    invoke-virtual {p0, v2, v0}, Lbpy;->a(Ljava/lang/Object;Lbmp;)V

    goto :goto_0

    .line 263
    :cond_1
    return-object p0
.end method

.method private static a(Lbor;Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbor;",
            "Ljava/util/Set",
            "<",
            "Ljeg;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljeg;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljeg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 196
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 197
    invoke-static {v1, p2, p1}, Lbpj;->a(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    .line 199
    invoke-virtual {p0}, Lbor;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbov;

    .line 200
    iget-object v3, v0, Lbov;->a:Lbmd;

    iget-object v3, v3, Lbmd;->d:Lbmg;

    sget-object v4, Lbmg;->c:Lbmg;

    if-ne v3, v4, :cond_0

    iget-object v3, v0, Lbov;->a:Lbmd;

    iget-object v3, v3, Lbmd;->e:Ljeg;

    .line 201
    invoke-interface {p2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 202
    iget-object v0, v0, Lbov;->a:Lbmd;

    iget-object v0, v0, Lbmd;->e:Ljeg;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 205
    :cond_1
    return-object v1
.end method

.method private static a(Ljava/util/List;)Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Ljeg;",
            ">;"
        }
    .end annotation

    .prologue
    .line 209
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 210
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 211
    iget-object v3, v0, Lbmd;->d:Lbmg;

    sget-object v4, Lbmg;->c:Lbmg;

    if-ne v3, v4, :cond_0

    .line 212
    iget-object v0, v0, Lbmd;->e:Ljeg;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 215
    :cond_1
    return-object v1
.end method

.method public static a(Lbor;Ljava/util/List;Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbor;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Ljava/util/List",
            "<+",
            "Lbph;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 49
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 50
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 51
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 52
    invoke-static {p2}, Lbpj;->b(Ljava/util/List;)Lbph;

    move-result-object v5

    .line 53
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbph;

    .line 54
    invoke-interface {v0, v4}, Lbph;->a(Ljava/util/List;)V

    .line 55
    instance-of v1, v0, Lboz;

    if-eqz v1, :cond_2

    move-object v1, v0

    .line 56
    check-cast v1, Lboz;

    iget-object v1, v1, Lboz;->a:Lbmd;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 60
    :cond_1
    :goto_1
    if-ne v0, v5, :cond_0

    .line 62
    invoke-static {v4}, Lbox;->a(Ljava/util/List;)Lbox;

    move-result-object v0

    .line 61
    invoke-virtual {p0, v0}, Lbor;->a(Lbox;)V

    goto :goto_0

    .line 57
    :cond_2
    instance-of v1, v0, Lbpk;

    if-eqz v1, :cond_1

    move-object v1, v0

    .line 58
    check-cast v1, Lbpk;

    iget-object v1, v1, Lbpk;->a:Ljava/util/List;

    invoke-interface {v3, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    .line 66
    :cond_3
    invoke-static {p0, p1, v4, v2, v3}, Lbpj;->a(Lbor;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 69
    return-void
.end method

.method private static a(Lbor;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbor;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 81
    new-instance v1, Lbpy;

    invoke-direct {v1}, Lbpy;-><init>()V

    .line 82
    invoke-static {v1, p2}, Lbpj;->a(Lbpy;Ljava/util/List;)Lbpy;

    .line 85
    new-instance v3, Lbpy;

    invoke-direct {v3}, Lbpy;-><init>()V

    .line 86
    invoke-static {v3, p1}, Lbpj;->a(Lbpy;Ljava/util/List;)Lbpy;

    .line 87
    invoke-static {v3, p4}, Lbpj;->a(Lbpy;Ljava/util/List;)Lbpy;

    .line 90
    invoke-virtual {p0}, Lbor;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbot;

    .line 91
    iget-object v0, v0, Lbot;->a:Lbmd;

    .line 92
    iget-object v5, v0, Lbmd;->d:Lbmg;

    sget-object v6, Lbmg;->a:Lbmg;

    if-ne v5, v6, :cond_0

    .line 93
    iget-object v5, v0, Lbmd;->e:Ljeg;

    iget-object v0, v0, Lbmd;->f:Lbmp;

    invoke-virtual {v3, v5, v0}, Lbpy;->a(Ljava/lang/Object;Lbmp;)V

    goto :goto_0

    .line 97
    :cond_1
    invoke-virtual {v3, v1}, Lbpy;->a(Lbpy;)V

    .line 100
    new-instance v4, Lbpy;

    invoke-direct {v4}, Lbpy;-><init>()V

    .line 101
    invoke-virtual {p0}, Lbor;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_2
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbov;

    .line 102
    iget-object v0, v0, Lbov;->a:Lbmd;

    .line 103
    iget-object v6, v0, Lbmd;->d:Lbmg;

    sget-object v7, Lbmg;->a:Lbmg;

    if-ne v6, v7, :cond_2

    iget-object v6, v0, Lbmd;->e:Ljeg;

    iget-object v7, v0, Lbmd;->f:Lbmp;

    .line 104
    invoke-virtual {v1, v6, v7}, Lbpy;->b(Ljava/lang/Object;Lbmp;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 105
    iget-object v6, v0, Lbmd;->e:Ljeg;

    iget-object v0, v0, Lbmd;->f:Lbmp;

    invoke-virtual {v4, v6, v0}, Lbpy;->a(Ljava/lang/Object;Lbmp;)V

    goto :goto_1

    .line 110
    :cond_3
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 111
    iget-object v1, v0, Lbmd;->d:Lbmg;

    sget-object v6, Lbmg;->a:Lbmg;

    if-ne v1, v6, :cond_4

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_5
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmd;

    invoke-virtual {v0, v1}, Lbmd;->a(Lbmd;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v2

    :goto_3
    if-eqz v1, :cond_4

    .line 113
    iget-object v1, v0, Lbmd;->e:Ljeg;

    iget-object v0, v0, Lbmd;->f:Lbmp;

    invoke-virtual {v4, v1, v0}, Lbpy;->a(Ljava/lang/Object;Lbmp;)V

    goto :goto_2

    .line 111
    :cond_6
    invoke-interface {p4}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmd;

    invoke-virtual {v0, v1}, Lbmd;->a(Lbmd;)Z

    move-result v1

    if-eqz v1, :cond_7

    move v1, v2

    goto :goto_3

    :cond_8
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_9
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmd;

    invoke-virtual {v0, v1}, Lbmd;->a(Lbmd;)Z

    move-result v1

    if-eqz v1, :cond_9

    move v1, v2

    goto :goto_3

    :cond_a
    const/4 v1, 0x1

    goto :goto_3

    .line 119
    :cond_b
    invoke-static {p1}, Lbpj;->a(Ljava/util/List;)Ljava/util/Set;

    move-result-object v1

    .line 120
    invoke-static {p2}, Lbpj;->a(Ljava/util/List;)Ljava/util/Set;

    move-result-object v2

    .line 122
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    invoke-static {v5, v1, v2}, Lbpj;->a(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V

    invoke-virtual {p0}, Lbor;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_c
    :goto_4
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbot;

    iget-object v7, v0, Lbot;->a:Lbmd;

    iget-object v7, v7, Lbmd;->d:Lbmg;

    sget-object v8, Lbmg;->c:Lbmg;

    if-ne v7, v8, :cond_c

    iget-object v0, v0, Lbot;->a:Lbmd;

    iget-object v0, v0, Lbmd;->e:Ljeg;

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 126
    :cond_d
    invoke-static {p0, v1, v2}, Lbpj;->a(Lbor;Ljava/util/Set;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    .line 133
    invoke-virtual {p0}, Lbor;->h()V

    .line 134
    invoke-virtual {p0}, Lbor;->i()V

    .line 136
    new-instance v0, Lbmf;

    invoke-direct {v0}, Lbmf;-><init>()V

    sget-object v1, Lbmg;->a:Lbmg;

    .line 137
    invoke-virtual {v0, v1}, Lbmf;->a(Lbmg;)Lbmf;

    move-result-object v6

    .line 139
    invoke-virtual {v3}, Lbpy;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 140
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljeg;

    .line 141
    invoke-virtual {v6, v1}, Lbmf;->a(Ljeg;)Lbmf;

    move-result-object v1

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lbmp;

    .line 142
    invoke-virtual {v1, v0}, Lbmf;->a(Lbmp;)Lbmf;

    move-result-object v0

    .line 143
    invoke-virtual {v0}, Lbmf;->a()Lbmd;

    move-result-object v0

    .line 144
    new-instance v1, Lbot;

    invoke-direct {v1, v0}, Lbot;-><init>(Lbmd;)V

    invoke-virtual {p0, v1}, Lbor;->a(Lbot;)V

    goto :goto_5

    .line 147
    :cond_e
    invoke-virtual {v4}, Lbpy;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_6
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 148
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljeg;

    .line 149
    invoke-virtual {v6, v1}, Lbmf;->a(Ljeg;)Lbmf;

    move-result-object v1

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lbmp;

    .line 150
    invoke-virtual {v1, v0}, Lbmf;->a(Lbmp;)Lbmf;

    move-result-object v0

    .line 151
    invoke-virtual {v0}, Lbmf;->a()Lbmd;

    move-result-object v0

    .line 152
    new-instance v1, Lbov;

    invoke-direct {v1, v0}, Lbov;-><init>(Lbmd;)V

    invoke-virtual {p0, v1}, Lbor;->a(Lbov;)V

    goto :goto_6

    .line 155
    :cond_f
    sget-object v0, Lbmg;->c:Lbmg;

    invoke-virtual {v6, v0}, Lbmf;->a(Lbmg;)Lbmf;

    move-result-object v0

    sget-object v1, Lbpj;->a:Lbmp;

    .line 156
    invoke-virtual {v0, v1}, Lbmf;->a(Lbmp;)Lbmf;

    .line 158
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljeg;

    .line 160
    invoke-virtual {v6, v0}, Lbmf;->a(Ljeg;)Lbmf;

    move-result-object v0

    .line 161
    invoke-virtual {v0}, Lbmf;->a()Lbmd;

    move-result-object v0

    .line 162
    new-instance v3, Lbot;

    invoke-direct {v3, v0}, Lbot;-><init>(Lbmd;)V

    invoke-virtual {p0, v3}, Lbor;->a(Lbot;)V

    goto :goto_7

    .line 165
    :cond_10
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_8
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljeg;

    .line 167
    invoke-virtual {v6, v0}, Lbmf;->a(Ljeg;)Lbmf;

    move-result-object v0

    .line 168
    invoke-virtual {v0}, Lbmf;->a()Lbmd;

    move-result-object v0

    .line 169
    new-instance v2, Lbov;

    invoke-direct {v2, v0}, Lbov;-><init>(Lbmd;)V

    invoke-virtual {p0, v2}, Lbor;->a(Lbov;)V

    goto :goto_8

    .line 171
    :cond_11
    return-void
.end method

.method private static a(Ljava/util/Set;Ljava/util/Set;Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/Set",
            "<-TT;>;",
            "Ljava/util/Set",
            "<+TT;>;",
            "Ljava/util/Set",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 271
    invoke-interface {p0}, Ljava/util/Set;->clear()V

    .line 273
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 274
    invoke-interface {p2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 275
    invoke-interface {p0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 278
    :cond_1
    return-void
.end method

.method private static b(Ljava/util/List;)Lbph;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lbph;",
            ">;)",
            "Lbph;"
        }
    .end annotation

    .prologue
    .line 287
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_2

    .line 288
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbph;

    .line 289
    instance-of v2, v0, Lbpf;

    if-nez v2, :cond_0

    instance-of v2, v0, Lboz;

    if-eqz v2, :cond_1

    .line 293
    :cond_0
    :goto_1
    return-object v0

    .line 287
    :cond_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 293
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
