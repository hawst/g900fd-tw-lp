.class public final Lhcj;
.super Lhck;
.source "PG"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:Landroid/graphics/drawable/Drawable;

.field private final f:Landroid/graphics/drawable/Drawable;

.field private final g:Landroid/graphics/drawable/Drawable;

.field private final h:Landroid/graphics/Paint;

.field private final i:Landroid/graphics/Paint;

.field private final j:Landroid/graphics/Rect;

.field private final k:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 35
    invoke-direct {p0}, Lhck;-><init>()V

    .line 66
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lhcj;->j:Landroid/graphics/Rect;

    .line 121
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lhcj;->k:Landroid/graphics/Rect;

    .line 36
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 38
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lhcj;->h:Landroid/graphics/Paint;

    .line 39
    iget-object v1, p0, Lhcj;->h:Landroid/graphics/Paint;

    const v2, 0x7f0b001c

    .line 40
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 39
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 41
    iget-object v1, p0, Lhcj;->h:Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 42
    const v1, 0x7f0d0006

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    .line 43
    iget-object v2, p0, Lhcj;->h:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 44
    iget-object v1, p0, Lhcj;->h:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 46
    new-instance v1, Landroid/graphics/Paint;

    iget-object v2, p0, Lhcj;->h:Landroid/graphics/Paint;

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v1, p0, Lhcj;->i:Landroid/graphics/Paint;

    .line 47
    iget-object v1, p0, Lhcj;->i:Landroid/graphics/Paint;

    const v2, 0x7f0b001d

    .line 48
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    .line 47
    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 51
    const v1, 0x7f020487

    .line 52
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lhcj;->e:Landroid/graphics/drawable/Drawable;

    .line 53
    const v1, 0x7f020485

    .line 54
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lhcj;->f:Landroid/graphics/drawable/Drawable;

    .line 55
    const v1, 0x7f020486

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lhcj;->g:Landroid/graphics/drawable/Drawable;

    .line 58
    const v1, 0x7f0d0007

    .line 59
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lhcj;->a:I

    .line 60
    const v1, 0x7f0d0008

    .line 61
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lhcj;->b:I

    .line 62
    const v1, 0x7f0d0009

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lhcj;->c:I

    .line 63
    const v1, 0x7f0d0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lhcj;->d:I

    .line 64
    return-void
.end method

.method private a(Landroid/graphics/Canvas;Ljava/lang/CharSequence;ZLandroid/graphics/Rect;F)V
    .locals 7

    .prologue
    .line 171
    if-eqz p3, :cond_0

    .line 172
    iget-object v0, p0, Lhcj;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p4}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 173
    iget-object v0, p0, Lhcj;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 174
    iget-object v6, p0, Lhcj;->h:Landroid/graphics/Paint;

    .line 181
    :goto_0
    const/4 v2, 0x0

    invoke-interface {p2}, Ljava/lang/CharSequence;->length()I

    move-result v3

    .line 182
    invoke-virtual {p4}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v4, v0

    invoke-virtual {p4}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, p5, v1

    add-float v5, v0, v1

    move-object v0, p1

    move-object v1, p2

    .line 181
    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 183
    return-void

    .line 176
    :cond_0
    iget-object v0, p0, Lhcj;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p4}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 177
    iget-object v0, p0, Lhcj;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 178
    iget-object v6, p0, Lhcj;->i:Landroid/graphics/Paint;

    goto :goto_0
.end method


# virtual methods
.method protected a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lhcl;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 71
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 72
    iget-object v1, p0, Lhcj;->h:Landroid/graphics/Paint;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    iget-object v3, p0, Lhcj;->j:Landroid/graphics/Rect;

    invoke-virtual {v1, v0, v4, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 74
    new-instance v1, Lhcl;

    iget-object v2, p0, Lhcj;->j:Landroid/graphics/Rect;

    invoke-direct {v1, v0, v2}, Lhcl;-><init>(Ljava/lang/CharSequence;Landroid/graphics/Rect;)V

    .line 75
    iget-object v0, v1, Lhcl;->a:Landroid/graphics/Rect;

    iget-object v2, p0, Lhcj;->j:Landroid/graphics/Rect;

    invoke-virtual {v0, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 76
    iget-object v0, v1, Lhcl;->a:Landroid/graphics/Rect;

    iget-object v2, v1, Lhcl;->a:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    neg-int v2, v2

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 77
    iget-object v0, v1, Lhcl;->a:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Rect;->right:I

    iget v3, p0, Lhcj;->a:I

    shl-int/lit8 v3, v3, 0x1

    add-int/2addr v2, v3

    iput v2, v0, Landroid/graphics/Rect;->right:I

    .line 78
    iget-object v0, v1, Lhcl;->a:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lhcj;->b:I

    sub-int/2addr v2, v3

    iput v2, v0, Landroid/graphics/Rect;->top:I

    .line 79
    iget-object v0, v1, Lhcl;->a:Landroid/graphics/Rect;

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    iget v3, p0, Lhcj;->b:I

    add-int/2addr v2, v3

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 81
    return-object v1
.end method

.method protected a(Ljava/util/List;)Lhcm;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lhcl;",
            ">;)",
            "Lhcm;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 86
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 87
    :cond_0
    sget-object v0, Lhcm;->a:Lhcm;

    .line 112
    :goto_0
    return-object v0

    .line 92
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    move v3, v2

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhcl;

    .line 93
    iget-object v5, v0, Lhcl;->a:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 94
    iget-object v0, v0, Lhcl;->a:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    .line 95
    goto :goto_1

    .line 97
    :cond_2
    new-instance v4, Lhcm;

    invoke-direct {v4, p1}, Lhcm;-><init>(Ljava/util/List;)V

    .line 98
    iget-object v0, v4, Lhcm;->c:Landroid/graphics/Rect;

    iget v5, p0, Lhcj;->d:I

    shl-int/lit8 v5, v5, 0x1

    add-int/2addr v5, v3

    iget v6, p0, Lhcj;->c:I

    add-int/2addr v6, v1

    .line 102
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    mul-int/2addr v6, v7

    iget v7, p0, Lhcj;->c:I

    sub-int/2addr v6, v7

    iget v7, p0, Lhcj;->d:I

    shl-int/lit8 v7, v7, 0x1

    add-int/2addr v6, v7

    .line 98
    invoke-virtual {v0, v2, v2, v5, v6}, Landroid/graphics/Rect;->set(IIII)V

    .line 103
    iget-object v0, v4, Lhcm;->c:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    iput v0, v4, Lhcm;->d:F

    .line 105
    new-instance v5, Landroid/graphics/Rect;

    invoke-direct {v5, v2, v2, v3, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 106
    iget v0, p0, Lhcj;->d:I

    iget v3, p0, Lhcj;->d:I

    invoke-virtual {v5, v0, v3}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 107
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhcl;

    .line 108
    iget-object v0, v0, Lhcl;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v5}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 109
    iget v0, p0, Lhcj;->c:I

    add-int/2addr v0, v1

    invoke-virtual {v5, v2, v0}, Landroid/graphics/Rect;->offset(II)V

    goto :goto_2

    :cond_3
    move-object v0, v4

    .line 112
    goto :goto_0
.end method

.method public a(Landroid/graphics/Canvas;Lhcm;II)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 126
    invoke-virtual {p2}, Lhcm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 166
    :goto_0
    return-void

    .line 130
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 134
    iget-object v0, p2, Lhcm;->c:Landroid/graphics/Rect;

    .line 135
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    sub-int v0, p3, v0

    int-to-float v0, v0

    iget-object v1, p2, Lhcm;->c:Landroid/graphics/Rect;

    .line 136
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int v1, p4, v1

    int-to-float v1, v1

    iget v2, p2, Lhcm;->e:F

    sub-float/2addr v1, v2

    .line 134
    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 138
    iget-object v0, p0, Lhcj;->g:Landroid/graphics/drawable/Drawable;

    iget-object v1, p2, Lhcm;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 139
    iget-object v0, p0, Lhcj;->g:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 141
    const/4 v6, 0x0

    move v7, v3

    .line 142
    :goto_1
    iget-object v0, p2, Lhcm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v7, v0, :cond_2

    .line 143
    iget-object v0, p2, Lhcm;->b:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhcl;

    .line 144
    invoke-virtual {p2, v7}, Lhcm;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 142
    :goto_2
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move-object v6, v0

    goto :goto_1

    .line 150
    :cond_1
    iget-object v2, v0, Lhcl;->b:Ljava/lang/CharSequence;

    iget-object v4, v0, Lhcl;->a:Landroid/graphics/Rect;

    iget v5, v0, Lhcl;->d:F

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lhcj;->a(Landroid/graphics/Canvas;Ljava/lang/CharSequence;ZLandroid/graphics/Rect;F)V

    move-object v0, v6

    goto :goto_2

    .line 154
    :cond_2
    if-eqz v6, :cond_3

    .line 157
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 158
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 160
    iget-object v0, p0, Lhcj;->k:Landroid/graphics/Rect;

    iget-object v1, v6, Lhcl;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 161
    iget-object v0, p0, Lhcj;->k:Landroid/graphics/Rect;

    iget-object v1, p0, Lhcj;->k:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    sub-int v1, p3, v1

    iget-object v2, p0, Lhcj;->k:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int v2, p4, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 162
    iget-object v2, v6, Lhcl;->b:Ljava/lang/CharSequence;

    const/4 v3, 0x1

    iget-object v4, p0, Lhcj;->k:Landroid/graphics/Rect;

    iget v5, v6, Lhcl;->d:F

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lhcj;->a(Landroid/graphics/Canvas;Ljava/lang/CharSequence;ZLandroid/graphics/Rect;F)V

    .line 165
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0
.end method
