.class public Laxy;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lkfd;

.field private final c:Lhei;

.field private final d:I

.field private final e:Lood;

.field private final f:Ljava/lang/String;

.field private final h:Ljdz;

.field private final i:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Laxy;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Lkfd;Lhei;ILood;[BLjava/lang/String;Ljdz;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 103
    const-string v0, "SaveCloudStoryboardTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 104
    const-string v0, "context"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Laxy;->a:Landroid/content/Context;

    .line 105
    const-string v0, "httpExecutor"

    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    iput-object v0, p0, Laxy;->b:Lkfd;

    .line 106
    const-string v0, "accountStore"

    invoke-static {p3, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Laxy;->c:Lhei;

    .line 107
    iput p4, p0, Laxy;->d:I

    .line 108
    const-string v0, "storyboard"

    invoke-static {p5, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lood;

    iput-object v0, p0, Laxy;->e:Lood;

    .line 109
    const-string v0, "storyboardToken"

    invoke-static {p6, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Laxy;->i:[B

    .line 110
    const-string v0, "lastVersionId"

    invoke-static {p7, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Laxy;->f:Ljava/lang/String;

    .line 111
    iget-object v0, p8, Ljdz;->b:Ljava/lang/String;

    const-string v1, "photoId.obfuscatedUserId"

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    iget-object v0, p8, Ljdz;->a:Ljava/lang/Long;

    const-string v1, "photoId.photoId"

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    iput-object p8, p0, Laxy;->h:Ljdz;

    .line 112
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 123
    new-instance v0, Lnbp;

    invoke-direct {v0}, Lnbp;-><init>()V

    new-instance v1, Lnwy;

    invoke-direct {v1}, Lnwy;-><init>()V

    iput-object v1, v0, Lnbp;->a:Lnwy;

    iget-object v1, v0, Lnbp;->a:Lnwy;

    iget-object v2, p0, Laxy;->h:Ljdz;

    iget-object v2, v2, Ljdz;->b:Ljava/lang/String;

    iput-object v2, v1, Lnwy;->a:Ljava/lang/String;

    iget-object v1, v0, Lnbp;->a:Lnwy;

    iget-object v2, p0, Laxy;->h:Ljdz;

    iget-object v2, v2, Ljdz;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lnwy;->b:Ljava/lang/String;

    new-instance v1, Lnbo;

    invoke-direct {v1}, Lnbo;-><init>()V

    iput-object v1, v0, Lnbp;->b:Lnbo;

    iget-object v1, v0, Lnbp;->b:Lnbo;

    iget-object v2, p0, Laxy;->f:Ljava/lang/String;

    iput-object v2, v1, Lnbo;->a:Ljava/lang/String;

    iget-object v1, v0, Lnbp;->b:Lnbo;

    new-instance v2, Lnbs;

    invoke-direct {v2}, Lnbs;-><init>()V

    iput v5, v2, Lnbs;->a:I

    new-instance v3, Lnbu;

    invoke-direct {v3}, Lnbu;-><init>()V

    iput-object v3, v2, Lnbs;->b:Lnbu;

    iget-object v3, v2, Lnbs;->b:Lnbu;

    new-instance v4, Lopf;

    invoke-direct {v4}, Lopf;-><init>()V

    iput-object v4, v3, Lnbu;->a:Lopf;

    iget-object v3, v2, Lnbs;->b:Lnbu;

    iget-object v3, v3, Lnbu;->a:Lopf;

    new-instance v4, Lope;

    invoke-direct {v4}, Lope;-><init>()V

    iput-object v4, v3, Lopf;->a:Lope;

    iget-object v3, v2, Lnbs;->b:Lnbu;

    iget-object v3, v3, Lnbu;->a:Lopf;

    iget-object v3, v3, Lopf;->a:Lope;

    const/4 v4, 0x2

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    iput-object v4, v3, Lope;->a:[I

    iget-object v3, v2, Lnbs;->b:Lnbu;

    iget-object v3, v3, Lnbu;->a:Lopf;

    const/16 v4, 0x8

    iput v4, v3, Lopf;->b:I

    iget-object v3, v2, Lnbs;->b:Lnbu;

    iget-object v3, v3, Lnbu;->a:Lopf;

    new-instance v4, Lpxr;

    invoke-direct {v4}, Lpxr;-><init>()V

    iput-object v4, v3, Lopf;->c:Lpxr;

    iget-object v3, v2, Lnbs;->b:Lnbu;

    iget-object v3, v3, Lnbu;->a:Lopf;

    iget-object v3, v3, Lopf;->c:Lpxr;

    iget-object v4, p0, Laxy;->e:Lood;

    iput-object v4, v3, Lpxr;->a:Lood;

    iget-object v3, v2, Lnbs;->b:Lnbu;

    iput v5, v3, Lnbu;->b:I

    iput-object v2, v1, Lnbo;->b:Lnbs;

    .line 124
    iget-object v1, p0, Laxy;->c:Lhei;

    iget v2, p0, Laxy;->d:I

    invoke-interface {v1, v2}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 125
    new-instance v2, Lkfo;

    const-string v3, "account_name"

    .line 126
    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "effective_gaia_id"

    .line 127
    invoke-interface {v1, v4}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v3, v1}, Lkfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    new-instance v1, Ljui;

    iget-object v3, p0, Laxy;->a:Landroid/content/Context;

    invoke-direct {v1, v3, v2, v0}, Ljui;-><init>(Landroid/content/Context;Lkfo;Lnbp;)V

    .line 133
    iget-object v0, p0, Laxy;->b:Lkfd;

    invoke-interface {v0, v1}, Lkfd;->a(Lkff;)V

    .line 134
    new-instance v2, Lhoz;

    .line 135
    iget v0, v1, Lkff;->i:I

    iget-object v3, v1, Lkff;->k:Ljava/lang/Exception;

    iget-object v4, v1, Lkff;->j:Ljava/lang/String;

    invoke-direct {v2, v0, v3, v4}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 137
    invoke-virtual {v2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v3

    .line 138
    invoke-virtual {v1}, Ljui;->D()Loxu;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 139
    invoke-virtual {v1}, Ljui;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmfh;

    .line 140
    iget-object v0, v0, Lmfh;->a:Lnbq;

    .line 141
    if-eqz v0, :cond_0

    .line 142
    const-string v1, "mutate_filters_response"

    .line 143
    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    .line 142
    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 146
    :cond_0
    const-string v0, "storyboard_token"

    iget-object v1, p0, Laxy;->i:[B

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 147
    return-object v2

    .line 123
    :array_0
    .array-data 4
        0x1
        0x3
    .end array-data
.end method
