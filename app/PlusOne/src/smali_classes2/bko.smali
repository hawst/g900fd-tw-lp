.class public final Lbko;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:J

.field private b:J

.field private c:J

.field private d:Lbbs;

.field private e:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbko;->a:J

    .line 114
    return-void
.end method

.method public constructor <init>(Lbkn;)V
    .locals 2

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbko;->a:J

    .line 117
    invoke-virtual {p1}, Lbkn;->c()J

    move-result-wide v0

    iput-wide v0, p0, Lbko;->a:J

    .line 118
    invoke-virtual {p1}, Lbkn;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lbko;->b:J

    .line 119
    invoke-virtual {p1}, Lbkn;->b()J

    move-result-wide v0

    iput-wide v0, p0, Lbko;->c:J

    .line 120
    invoke-virtual {p1}, Lbkn;->d()Lbbs;

    move-result-object v0

    iput-object v0, p0, Lbko;->d:Lbbs;

    .line 121
    invoke-virtual {p1}, Lbkn;->e()Z

    move-result v0

    iput-boolean v0, p0, Lbko;->e:Z

    .line 122
    return-void
.end method


# virtual methods
.method public a(J)Lbko;
    .locals 3

    .prologue
    .line 132
    const-string v0, "durationUs"

    invoke-static {p1, p2, v0}, Lcec;->b(JLjava/lang/CharSequence;)J

    move-result-wide v0

    iput-wide v0, p0, Lbko;->a:J

    .line 133
    return-object p0
.end method

.method public a(Lbbs;)Lbko;
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lbko;->d:Lbbs;

    .line 172
    return-object p0
.end method

.method public a(Z)Lbko;
    .locals 0

    .prologue
    .line 182
    iput-boolean p1, p0, Lbko;->e:Z

    .line 183
    return-object p0
.end method

.method public a()Z
    .locals 4

    .prologue
    .line 138
    iget-wide v0, p0, Lbko;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lbkn;
    .locals 10

    .prologue
    .line 192
    invoke-virtual {p0}, Lbko;->a()Z

    move-result v0

    const-string v1, "Required field durationUs not set."

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 193
    new-instance v1, Lbkn;

    iget-wide v2, p0, Lbko;->a:J

    iget-wide v4, p0, Lbko;->b:J

    iget-wide v6, p0, Lbko;->c:J

    iget-object v8, p0, Lbko;->d:Lbbs;

    iget-boolean v9, p0, Lbko;->e:Z

    invoke-direct/range {v1 .. v9}, Lbkn;-><init>(JJJLbbs;Z)V

    return-object v1
.end method

.method public b(J)Lbko;
    .locals 1

    .prologue
    .line 148
    iput-wide p1, p0, Lbko;->b:J

    .line 149
    return-object p0
.end method

.method public c(J)Lbko;
    .locals 1

    .prologue
    .line 159
    iput-wide p1, p0, Lbko;->c:J

    .line 160
    return-object p0
.end method
