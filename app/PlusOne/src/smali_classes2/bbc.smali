.class public final Lbbc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbaz;


# instance fields
.field private final a:F

.field private final b:F


# direct methods
.method public constructor <init>(FF)V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    iput p1, p0, Lbbc;->a:F

    .line 118
    iput p2, p0, Lbbc;->b:F

    .line 119
    return-void
.end method


# virtual methods
.method public a(F)F
    .locals 4

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 123
    iget v0, p0, Lbbc;->a:F

    neg-float v0, v0

    iget v1, p0, Lbbc;->b:F

    sub-float v1, p1, v1

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    add-double/2addr v0, v2

    div-double v0, v2, v0

    double-to-float v0, v0

    return v0
.end method
