.class final Lbip;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:[J

.field private final b:[I

.field private final c:[J

.field private final d:[Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lbip;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method private constructor <init>([J[I[J[Z)V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput-object p1, p0, Lbip;->a:[J

    .line 155
    iput-object p2, p0, Lbip;->b:[I

    .line 156
    iput-object p3, p0, Lbip;->c:[J

    .line 157
    iput-object p4, p0, Lbip;->d:[Z

    .line 158
    return-void
.end method

.method public static a(Lbio;)Lbip;
    .locals 39

    .prologue
    .line 22
    move-object/from16 v0, p0

    iget-object v4, v0, Lbio;->b:Lbil;

    .line 25
    sget v5, Lbik;->t:I

    invoke-virtual {v4, v5}, Lbil;->a(I)Lbim;

    move-result-object v5

    iget-object v0, v5, Lbim;->b:Lbir;

    move-object/from16 v23, v0

    .line 28
    sget v5, Lbik;->u:I

    invoke-virtual {v4, v5}, Lbil;->a(I)Lbim;

    move-result-object v5

    iget-object v0, v5, Lbim;->b:Lbir;

    move-object/from16 v24, v0

    .line 30
    sget v5, Lbik;->s:I

    invoke-virtual {v4, v5}, Lbil;->a(I)Lbim;

    move-result-object v5

    iget-object v0, v5, Lbim;->b:Lbir;

    move-object/from16 v25, v0

    .line 32
    sget v5, Lbik;->q:I

    invoke-virtual {v4, v5}, Lbil;->a(I)Lbim;

    move-result-object v5

    iget-object v0, v5, Lbim;->b:Lbir;

    move-object/from16 v26, v0

    .line 34
    sget v5, Lbik;->r:I

    invoke-virtual {v4, v5}, Lbil;->a(I)Lbim;

    move-result-object v4

    .line 35
    if-eqz v4, :cond_3

    iget-object v4, v4, Lbim;->b:Lbir;

    .line 38
    :goto_0
    const/4 v5, 0x4

    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Lbir;->a(I)V

    .line 39
    invoke-virtual/range {v23 .. v23}, Lbir;->f()I

    move-result v8

    .line 40
    invoke-virtual/range {v23 .. v23}, Lbir;->f()I

    move-result v27

    .line 42
    move/from16 v0, v27

    new-array v0, v0, [I

    move-object/from16 v28, v0

    .line 43
    move/from16 v0, v27

    new-array v0, v0, [J

    move-object/from16 v29, v0

    .line 44
    move/from16 v0, v27

    new-array v0, v0, [J

    move-object/from16 v30, v0

    .line 45
    move/from16 v0, v27

    new-array v0, v0, [Z

    move-object/from16 v31, v0

    .line 48
    const/4 v5, 0x4

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Lbir;->a(I)V

    .line 49
    invoke-virtual/range {v24 .. v24}, Lbir;->f()I

    move-result v32

    .line 51
    const/4 v5, 0x4

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lbir;->a(I)V

    .line 52
    invoke-virtual/range {v25 .. v25}, Lbir;->f()I

    move-result v5

    add-int/lit8 v18, v5, -0x1

    .line 53
    invoke-virtual/range {v25 .. v25}, Lbir;->f()I

    move-result v5

    const-string v6, "stsc.readInt()"

    const/4 v7, 0x1

    const-string v9, "stsc first chunk must be 1"

    invoke-static {v5, v6, v7, v9}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 54
    invoke-virtual/range {v25 .. v25}, Lbir;->f()I

    move-result v12

    .line 55
    const/4 v5, 0x4

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lbir;->b(I)V

    .line 56
    const/4 v5, -0x1

    .line 57
    if-lez v18, :cond_0

    .line 59
    invoke-virtual/range {v25 .. v25}, Lbir;->f()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    .line 62
    :cond_0
    const/16 v17, 0x0

    .line 66
    const/4 v6, 0x4

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Lbir;->a(I)V

    .line 67
    invoke-virtual/range {v26 .. v26}, Lbir;->f()I

    move-result v6

    add-int/lit8 v16, v6, -0x1

    .line 68
    invoke-virtual/range {v26 .. v26}, Lbir;->f()I

    move-result v15

    .line 69
    invoke-virtual/range {v26 .. v26}, Lbir;->f()I

    move-result v14

    .line 71
    const/4 v7, -0x1

    .line 72
    const/4 v6, 0x0

    .line 73
    if-eqz v4, :cond_1

    .line 74
    const/4 v6, 0x4

    invoke-virtual {v4, v6}, Lbir;->a(I)V

    .line 75
    invoke-virtual {v4}, Lbir;->f()I

    move-result v6

    .line 76
    invoke-virtual {v4}, Lbir;->f()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    .line 80
    :cond_1
    invoke-virtual/range {v24 .. v24}, Lbir;->f()I

    move-result v13

    .line 81
    const-wide/16 v10, 0x0

    .line 82
    const/4 v9, 0x0

    move/from16 v19, v9

    move-wide/from16 v20, v10

    move/from16 v22, v12

    move/from16 v9, v16

    move v10, v5

    move v11, v12

    move/from16 v16, v15

    move/from16 v12, v18

    move v15, v7

    move v7, v14

    move v14, v6

    move/from16 v6, v17

    :goto_1
    move/from16 v0, v19

    move/from16 v1, v27

    if-ge v0, v1, :cond_7

    .line 83
    int-to-long v0, v13

    move-wide/from16 v34, v0

    aput-wide v34, v30, v19

    .line 84
    if-nez v8, :cond_4

    invoke-virtual/range {v23 .. v23}, Lbir;->f()I

    move-result v5

    :goto_2
    aput v5, v28, v19

    .line 85
    const-wide v34, 0x412e848000000000L    # 1000000.0

    move-wide/from16 v0, v20

    long-to-double v0, v0

    move-wide/from16 v36, v0

    mul-double v34, v34, v36

    move-object/from16 v0, p0

    iget-wide v0, v0, Lbio;->a:J

    move-wide/from16 v36, v0

    move-wide/from16 v0, v36

    long-to-double v0, v0

    move-wide/from16 v36, v0

    div-double v34, v34, v36

    .line 86
    invoke-static/range {v34 .. v35}, Ljava/lang/Math;->round(D)J

    move-result-wide v34

    aput-wide v34, v29, v19

    .line 89
    if-nez v4, :cond_5

    const/4 v5, 0x1

    :goto_3
    aput-boolean v5, v31, v19

    .line 90
    move/from16 v0, v19

    if-ne v0, v15, :cond_11

    .line 91
    const/4 v5, 0x1

    aput-boolean v5, v31, v19

    .line 92
    add-int/lit8 v5, v14, -0x1

    .line 93
    if-lez v5, :cond_10

    .line 94
    invoke-virtual {v4}, Lbir;->f()I

    move-result v14

    add-int/lit8 v14, v14, -0x1

    move/from16 v17, v5

    move/from16 v18, v14

    .line 99
    :goto_4
    int-to-long v14, v7

    add-long v20, v20, v14

    .line 100
    add-int/lit8 v5, v16, -0x1

    .line 101
    if-nez v5, :cond_f

    if-lez v9, :cond_f

    .line 102
    invoke-virtual/range {v26 .. v26}, Lbir;->f()I

    move-result v7

    .line 103
    invoke-virtual/range {v26 .. v26}, Lbir;->f()I

    move-result v5

    .line 104
    add-int/lit8 v9, v9, -0x1

    move v14, v5

    move v15, v7

    move/from16 v16, v9

    .line 108
    :goto_5
    add-int/lit8 v7, v22, -0x1

    .line 109
    if-nez v7, :cond_6

    .line 110
    add-int/lit8 v9, v6, 0x1

    .line 111
    move/from16 v0, v32

    if-ge v9, v0, :cond_e

    .line 112
    invoke-virtual/range {v24 .. v24}, Lbir;->f()I

    move-result v5

    .line 116
    :goto_6
    if-ne v9, v10, :cond_d

    .line 117
    invoke-virtual/range {v25 .. v25}, Lbir;->f()I

    move-result v6

    .line 118
    const/4 v11, 0x4

    move-object/from16 v0, v25

    invoke-virtual {v0, v11}, Lbir;->b(I)V

    .line 119
    add-int/lit8 v11, v12, -0x1

    .line 120
    if-lez v11, :cond_2

    .line 121
    invoke-virtual/range {v25 .. v25}, Lbir;->f()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    .line 126
    :cond_2
    :goto_7
    move/from16 v0, v32

    if-ge v9, v0, :cond_c

    move v7, v9

    move v9, v10

    move v10, v6

    .line 82
    :goto_8
    add-int/lit8 v12, v19, 0x1

    move/from16 v19, v12

    move v13, v5

    move/from16 v22, v6

    move v6, v7

    move v12, v11

    move v11, v10

    move v7, v14

    move/from16 v14, v17

    move v10, v9

    move/from16 v9, v16

    move/from16 v16, v15

    move/from16 v15, v18

    goto/16 :goto_1

    .line 35
    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_0

    :cond_4
    move v5, v8

    .line 84
    goto/16 :goto_2

    .line 89
    :cond_5
    const/4 v5, 0x0

    goto :goto_3

    .line 131
    :cond_6
    aget v5, v28, v19

    add-int/2addr v5, v13

    move v9, v10

    move v10, v11

    move v11, v12

    move/from16 v38, v6

    move v6, v7

    move/from16 v7, v38

    goto :goto_8

    .line 136
    :cond_7
    if-nez v14, :cond_8

    const/4 v4, 0x1

    :goto_9
    const-string v5, "Expected no more synchronization samples at end of file."

    invoke-static {v4, v5}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 138
    if-nez v16, :cond_9

    const/4 v4, 0x1

    :goto_a
    const-string v5, "Expected no more samples at the current timestamp delta at the end of file."

    invoke-static {v4, v5}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 140
    if-nez v22, :cond_a

    const/4 v4, 0x1

    :goto_b
    const-string v5, "Expected no more samples in the current chunk at the end of file."

    invoke-static {v4, v5}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 142
    if-nez v9, :cond_b

    const/4 v4, 0x1

    :goto_c
    const-string v5, "Expected no more timestamp delta changes at the end of file."

    invoke-static {v4, v5}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 144
    new-instance v4, Lbip;

    move-object/from16 v0, v30

    move-object/from16 v1, v28

    move-object/from16 v2, v29

    move-object/from16 v3, v31

    invoke-direct {v4, v0, v1, v2, v3}, Lbip;-><init>([J[I[J[Z)V

    return-object v4

    .line 136
    :cond_8
    const/4 v4, 0x0

    goto :goto_9

    .line 138
    :cond_9
    const/4 v4, 0x0

    goto :goto_a

    .line 140
    :cond_a
    const/4 v4, 0x0

    goto :goto_b

    .line 142
    :cond_b
    const/4 v4, 0x0

    goto :goto_c

    :cond_c
    move/from16 v38, v7

    move v7, v9

    move v9, v10

    move v10, v6

    move/from16 v6, v38

    goto :goto_8

    :cond_d
    move v6, v11

    move v11, v12

    goto :goto_7

    :cond_e
    move v5, v13

    goto/16 :goto_6

    :cond_f
    move v14, v7

    move v15, v5

    move/from16 v16, v9

    goto/16 :goto_5

    :cond_10
    move/from16 v17, v5

    move/from16 v18, v15

    goto/16 :goto_4

    :cond_11
    move/from16 v17, v14

    move/from16 v18, v15

    goto/16 :goto_4
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lbip;->b:[I

    array-length v0, v0

    return v0
.end method

.method public a(J)I
    .locals 5

    .prologue
    .line 192
    iget-object v0, p0, Lbip;->c:[J

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 193
    iget-object v1, p0, Lbip;->c:[J

    aget-wide v2, v1, v0

    cmp-long v1, v2, p1

    if-gez v1, :cond_0

    iget-object v1, p0, Lbip;->d:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_0

    .line 198
    :goto_1
    return v0

    .line 192
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 198
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public a(I)J
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lbip;->a:[J

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public b(I)I
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lbip;->b:[I

    aget v0, v0, p1

    return v0
.end method

.method public b(J)I
    .locals 5

    .prologue
    .line 209
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lbip;->c:[J

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 210
    iget-object v1, p0, Lbip;->c:[J

    aget-wide v2, v1, v0

    cmp-long v1, v2, p1

    if-ltz v1, :cond_0

    iget-object v1, p0, Lbip;->d:[Z

    aget-boolean v1, v1, v0

    if-eqz v1, :cond_0

    .line 215
    :goto_1
    return v0

    .line 209
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 215
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public c(J)I
    .locals 7

    .prologue
    const/4 v2, -0x1

    .line 227
    invoke-virtual {p0, p1, p2}, Lbip;->a(J)I

    move-result v1

    .line 228
    invoke-virtual {p0, p1, p2}, Lbip;->b(J)I

    move-result v0

    .line 230
    if-ne v1, v2, :cond_1

    .line 240
    :cond_0
    :goto_0
    return v0

    .line 232
    :cond_1
    if-ne v0, v2, :cond_2

    move v0, v1

    .line 233
    goto :goto_0

    .line 235
    :cond_2
    invoke-virtual {p0, v1}, Lbip;->c(I)J

    move-result-wide v2

    .line 236
    invoke-virtual {p0, v0}, Lbip;->c(I)J

    move-result-wide v4

    .line 237
    sub-long v2, p1, v2

    sub-long/2addr v4, p1

    cmp-long v2, v2, v4

    if-gtz v2, :cond_0

    move v0, v1

    .line 240
    goto :goto_0
.end method

.method public c(I)J
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lbip;->c:[J

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public d(I)I
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Lbip;->d:[Z

    aget-boolean v0, v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
