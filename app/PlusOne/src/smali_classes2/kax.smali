.class final Lkax;
.super Landroid/widget/LinearLayout;
.source "PG"


# static fields
.field private static a:Llct;


# instance fields
.field private b:Landroid/widget/TextView;

.field private c:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 33
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 25
    new-instance v0, Lkay;

    invoke-direct {v0, p0}, Lkay;-><init>(Lkax;)V

    iput-object v0, p0, Lkax;->c:Ljava/lang/Runnable;

    .line 34
    sget-object v0, Lkax;->a:Llct;

    if-nez v0, :cond_0

    .line 35
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lkax;->a:Llct;

    .line 38
    :cond_0
    invoke-virtual {p0, v4}, Lkax;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 39
    sget-object v0, Lkax;->a:Llct;

    iget-object v0, v0, Llct;->al:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {p0, v0}, Lkax;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 40
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lkax;->setOrientation(I)V

    .line 41
    sget-object v0, Lkax;->a:Llct;

    iget v0, v0, Llct;->m:I

    sget-object v1, Lkax;->a:Llct;

    iget v1, v1, Llct;->m:I

    sget-object v2, Lkax;->a:Llct;

    iget v2, v2, Llct;->m:I

    sget-object v3, Lkax;->a:Llct;

    iget v3, v3, Llct;->m:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lkax;->setPadding(IIII)V

    .line 44
    const/4 v0, 0x0

    const/16 v1, 0x25

    invoke-static {p1, v4, v0, v1}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lkax;->b:Landroid/widget/TextView;

    .line 46
    iget-object v0, p0, Lkax;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lkax;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0a0189

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 47
    iget-object v0, p0, Lkax;->b:Landroid/widget/TextView;

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    const/4 v3, -0x2

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lkax;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 50
    iget-object v0, p0, Lkax;->c:Ljava/lang/Runnable;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 51
    iget-object v0, p0, Lkax;->c:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    .line 52
    return-void
.end method

.method static synthetic a(Lkax;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 16
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lkax;->setClickable(Z)V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    invoke-virtual {v0, v4}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    invoke-virtual {v0, v4}, Landroid/view/animation/AlphaAnimation;->setFillEnabled(Z)V

    invoke-virtual {p0, v0}, Lkax;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method
