.class public final Lccu;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Long;",
        "Lbqt;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lbmm;

.field private final b:I

.field private final c:I

.field private synthetic d:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;Lbmm;II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 425
    iput-object p1, p0, Lccu;->d:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 426
    const-string v0, "displayableFrame"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmm;

    iput-object v0, p0, Lccu;->a:Lbmm;

    .line 427
    const-string v0, "targetWidth"

    invoke-static {p3, v0, v1}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lccu;->b:I

    .line 428
    const-string v0, "targetHeight"

    invoke-static {p4, v0, v1}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lccu;->c:I

    .line 429
    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "one of target width/height must be zero"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 431
    return-void

    .line 429
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected varargs a()Lbqt;
    .locals 4

    .prologue
    .line 436
    iget-object v0, p0, Lccu;->d:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;)Lbqo;

    move-result-object v0

    iget-object v1, p0, Lccu;->a:Lbmm;

    iget v2, p0, Lccu;->b:I

    iget v3, p0, Lccu;->c:I

    invoke-virtual {v0, v1, v2, v3}, Lbqo;->a(Lbmm;II)Lbqt;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lbqt;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 441
    if-eqz p1, :cond_0

    .line 442
    invoke-virtual {p1}, Lbqt;->b()V

    .line 444
    :cond_0
    iget-object v0, p0, Lccu;->d:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->b(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;)Lccu;

    move-result-object v0

    if-ne v0, p0, :cond_1

    .line 445
    iget-object v0, p0, Lccu;->d:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    const/4 v1, 0x0

    invoke-static {v0, v2, v1}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;Lbqt;Z)V

    .line 446
    iget-object v0, p0, Lccu;->d:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    invoke-static {v0, v2}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;Lccu;)Lccu;

    .line 448
    :cond_1
    return-void
.end method

.method protected b(Lbqt;)V
    .locals 2

    .prologue
    .line 452
    iget-object v0, p0, Lccu;->d:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->b(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;)Lccu;

    move-result-object v0

    if-ne v0, p0, :cond_0

    .line 453
    iget-object v0, p0, Lccu;->d:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;Lbqt;Z)V

    .line 454
    iget-object v0, p0, Lccu;->d:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;Lccu;)Lccu;

    .line 456
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 419
    invoke-virtual {p0}, Lccu;->a()Lbqt;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 419
    check-cast p1, Lbqt;

    invoke-virtual {p0, p1}, Lccu;->a(Lbqt;)V

    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 419
    check-cast p1, Lbqt;

    invoke-virtual {p0, p1}, Lccu;->b(Lbqt;)V

    return-void
.end method
