.class public final Lciz;
.super Lcjy;
.source "PG"


# static fields
.field private static final W:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final X:[I


# instance fields
.field private Y:Lckj;

.field private final Z:Lcmv;

.field private final aa:Lcjc;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 30
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lciz;->X:[I

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 41
    new-array v1, v4, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/16 v3, 0xc

    .line 43
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 44
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 41
    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 46
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lciz;->W:Ljava/util/List;

    .line 47
    return-void

    .line 30
    :array_0
    .array-data 4
        0x7f0a004c
        0x7f0a004d
        0x7f0a004e
        0x7f0a004f
        0x7f0a0050
        0x7f0a0051
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcjy;-><init>()V

    .line 50
    new-instance v0, Lcjd;

    invoke-direct {v0, p0}, Lcjd;-><init>(Lciz;)V

    iput-object v0, p0, Lciz;->Z:Lcmv;

    .line 52
    new-instance v0, Lcjc;

    invoke-direct {v0, p0}, Lcjc;-><init>(Lciz;)V

    iput-object v0, p0, Lciz;->aa:Lcjc;

    .line 160
    return-void
.end method

.method static synthetic a(Lciz;)Lcjc;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lciz;->aa:Lcjc;

    return-object v0
.end method

.method static synthetic b(Lciz;)Lckj;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lciz;->Y:Lckj;

    return-object v0
.end method

.method static synthetic c(Lciz;)Lcmv;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lciz;->Z:Lcmv;

    return-object v0
.end method


# virtual methods
.method protected X()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    sget-object v0, Lciz;->W:Ljava/util/List;

    return-object v0
.end method

.method protected a(Lckp;)V
    .locals 3

    .prologue
    .line 115
    const v0, 0x7f0203a3

    const v1, 0x7f0a009b

    .line 117
    invoke-virtual {p0, v1}, Lciz;->e_(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcjb;

    invoke-direct {v2, p0}, Lcjb;-><init>(Lciz;)V

    .line 115
    invoke-virtual {p1, v0, v1, v2}, Lckp;->a(ILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 127
    return-void
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 56
    new-instance v0, Lhmk;

    sget-object v1, Lonc;->k:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method public b(ILjava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 131
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 132
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v1, Lciz;->X:[I

    aget v0, v1, v0

    invoke-virtual {p0, v0}, Lciz;->e_(I)Ljava/lang/String;

    move-result-object v0

    .line 134
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcjy;->b(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 61
    const/16 v0, 0x9

    return v0
.end method

.method protected y_()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 71
    invoke-super {p0}, Lcjy;->y_()V

    .line 73
    iget-object v0, p0, Lciz;->Y:Lckj;

    if-eqz v0, :cond_0

    .line 111
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-virtual {p0}, Lciz;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 78
    const v1, 0x7f0d0047

    .line 79
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 83
    invoke-virtual {p0}, Lciz;->ah()Lchp;

    move-result-object v0

    invoke-virtual {v0}, Lchp;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    move v2, v1

    move v4, v3

    move v5, v3

    .line 82
    invoke-static/range {v0 .. v5}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->createCenterCropBitmap(Landroid/graphics/Bitmap;IIIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 84
    new-instance v2, Lckj;

    .line 85
    invoke-virtual {p0}, Lciz;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v3

    const/4 v4, 0x3

    invoke-direct {v2, p0, v3, v4, v0}, Lckj;-><init>(Lcjy;Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;ILandroid/graphics/Bitmap;)V

    iput-object v2, p0, Lciz;->Y:Lckj;

    .line 88
    invoke-virtual {p0}, Lciz;->ah()Lchp;

    move-result-object v0

    invoke-virtual {v0}, Lchp;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lciz;->a(ILandroid/graphics/Bitmap;)Landroid/graphics/Rect;

    move-result-object v1

    .line 90
    invoke-virtual {p0}, Lciz;->an()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lhap;

    .line 92
    new-instance v2, Lcja;

    invoke-direct {v2, p0, v1}, Lcja;-><init>(Lciz;Landroid/graphics/Rect;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lhap;->a(Lhbl;Z)V

    goto :goto_0
.end method
