.class public final Lbqi;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lbqi;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x500
        0x2d0
        0x500
        0x2d0
    .end array-data
.end method

.method static a(Ljava/util/List;)[I
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbqj;",
            ">;)[I"
        }
    .end annotation

    .prologue
    .line 69
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    sget-object v0, Lbqi;->a:[I

    .line 128
    :goto_0
    return-object v0

    .line 75
    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 76
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 77
    const-wide/16 v2, 0x0

    .line 78
    const-wide/16 v0, 0x0

    .line 79
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move-wide v4, v2

    move-wide v2, v0

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqj;

    .line 80
    iget v1, v0, Lbqj;->a:I

    iget v9, v0, Lbqj;->b:I

    if-ge v1, v9, :cond_1

    .line 81
    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    iget-wide v0, v0, Lbqj;->c:J

    add-long/2addr v0, v4

    move-wide v4, v0

    goto :goto_1

    .line 84
    :cond_1
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    iget-wide v0, v0, Lbqj;->c:J

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 87
    goto :goto_1

    .line 89
    :cond_2
    long-to-float v0, v4

    add-long/2addr v2, v4

    long-to-float v1, v2

    div-float/2addr v0, v1

    const v1, 0x3f0ccccd    # 0.55f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_5

    const/4 v0, 0x1

    .line 94
    :goto_2
    if-eqz v0, :cond_6

    move-object v0, v6

    .line 95
    :goto_3
    new-instance v8, Ljava/util/HashMap;

    invoke-direct {v8}, Ljava/util/HashMap;-><init>()V

    .line 96
    const-wide/16 v6, 0x0

    .line 97
    const/4 v4, 0x0

    .line 98
    const/4 v3, 0x0

    .line 99
    const/4 v2, 0x0

    .line 100
    const/4 v1, 0x0

    .line 101
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v5, v4

    move v4, v3

    move v3, v2

    move v2, v1

    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqj;

    .line 102
    new-instance v10, Landroid/util/Pair;

    iget v1, v0, Lbqj;->a:I

    .line 103
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v11, v0, Lbqj;->b:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-direct {v10, v1, v11}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 104
    invoke-interface {v8, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 105
    if-nez v1, :cond_7

    .line 106
    iget-wide v12, v0, Lbqj;->c:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 110
    :goto_5
    invoke-interface {v8, v10, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 111
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v10, v10, v6

    if-gtz v10, :cond_3

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v10, v10, v6

    if-nez v10, :cond_4

    iget v10, v0, Lbqj;->a:I

    iget v11, v0, Lbqj;->b:I

    mul-int/2addr v10, v11

    mul-int v11, v5, v4

    if-le v10, v11, :cond_4

    .line 113
    :cond_3
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 114
    iget v5, v0, Lbqj;->a:I

    .line 115
    iget v4, v0, Lbqj;->b:I

    .line 117
    :cond_4
    iget v1, v0, Lbqj;->a:I

    iget v10, v0, Lbqj;->b:I

    mul-int/2addr v1, v10

    mul-int v10, v3, v2

    if-le v1, v10, :cond_a

    .line 118
    iget v1, v0, Lbqj;->a:I

    .line 119
    iget v0, v0, Lbqj;->b:I

    :goto_6
    move v2, v0

    move v3, v1

    .line 121
    goto :goto_4

    .line 89
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    :cond_6
    move-object v0, v7

    .line 94
    goto :goto_3

    .line 108
    :cond_7
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    iget-wide v14, v0, Lbqj;->c:J

    add-long/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    goto :goto_5

    .line 123
    :cond_8
    if-ge v3, v2, :cond_9

    .line 128
    :goto_7
    const/4 v0, 0x4

    new-array v0, v0, [I

    const/4 v1, 0x0

    aput v5, v0, v1

    const/4 v1, 0x1

    aput v4, v0, v1

    const/4 v1, 0x2

    aput v2, v0, v1

    const/4 v1, 0x3

    aput v3, v0, v1

    goto/16 :goto_0

    :cond_9
    move/from16 v16, v2

    move v2, v3

    move/from16 v3, v16

    goto :goto_7

    :cond_a
    move v0, v2

    move v1, v3

    goto :goto_6
.end method

.method static a(Ljava/util/List;II)[I
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbqj;",
            ">;II)[I"
        }
    .end annotation

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 139
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    new-array v0, v11, [I

    aput p1, v0, v3

    aput p2, v0, v2

    aput p1, v0, v9

    aput p2, v0, v10

    .line 167
    :goto_0
    return-object v0

    .line 143
    :cond_0
    if-ge p1, p2, :cond_1

    move v1, v2

    .line 147
    :goto_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v4, v3

    move v5, v3

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqj;

    .line 148
    iget v6, v0, Lbqj;->a:I

    iget v8, v0, Lbqj;->b:I

    if-ge v6, v8, :cond_2

    move v6, v2

    :goto_3
    if-ne v6, v1, :cond_7

    iget v6, v0, Lbqj;->a:I

    iget v8, v0, Lbqj;->b:I

    mul-int/2addr v6, v8

    mul-int v8, v5, v4

    if-le v6, v8, :cond_7

    .line 150
    iget v5, v0, Lbqj;->a:I

    .line 151
    iget v4, v0, Lbqj;->b:I

    move v0, v4

    move v4, v5

    :goto_4
    move v5, v4

    move v4, v0

    .line 153
    goto :goto_2

    :cond_1
    move v1, v3

    .line 143
    goto :goto_1

    :cond_2
    move v6, v3

    .line 148
    goto :goto_3

    .line 156
    :cond_3
    if-ge v5, v4, :cond_6

    move v0, v5

    move v1, v4

    .line 161
    :goto_5
    if-eqz v1, :cond_4

    if-nez v0, :cond_5

    :cond_4
    move v0, p2

    move v1, p1

    .line 167
    :cond_5
    new-array v4, v11, [I

    aput p1, v4, v3

    aput p2, v4, v2

    aput v1, v4, v9

    aput v0, v4, v10

    move-object v0, v4

    goto :goto_0

    :cond_6
    move v0, v4

    move v1, v5

    goto :goto_5

    :cond_7
    move v0, v4

    move v4, v5

    goto :goto_4
.end method

.method public static a(Ljava/util/List;Ljava/util/Map;II)[I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;II)[I"
        }
    .end annotation

    .prologue
    .line 50
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 51
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 52
    iget-object v1, v0, Lbmd;->d:Lbmg;

    sget-object v4, Lbmg;->a:Lbmg;

    if-eq v1, v4, :cond_1

    iget-object v1, v0, Lbmd;->d:Lbmg;

    sget-object v4, Lbmg;->c:Lbmg;

    if-ne v1, v4, :cond_0

    .line 53
    :cond_1
    new-instance v4, Lbqj;

    iget-object v1, v0, Lbmd;->e:Ljeg;

    invoke-interface {p1, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbml;

    .line 54
    invoke-interface {v1}, Lbml;->b()Lbmu;

    move-result-object v1

    invoke-direct {v4, v0, v1}, Lbqj;-><init>(Lbmd;Lbmu;)V

    .line 53
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 57
    :cond_2
    if-lez p2, :cond_3

    if-lez p3, :cond_3

    .line 58
    invoke-static {v2, p2, p3}, Lbqi;->a(Ljava/util/List;II)[I

    move-result-object v0

    .line 59
    :goto_1
    return-object v0

    :cond_3
    invoke-static {v2}, Lbqi;->a(Ljava/util/List;)[I

    move-result-object v0

    goto :goto_1
.end method
