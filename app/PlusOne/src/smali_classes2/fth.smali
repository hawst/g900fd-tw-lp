.class final Lfth;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field private synthetic a:Lftf;


# direct methods
.method constructor <init>(Lftf;)V
    .locals 0

    .prologue
    .line 1380
    iput-object p1, p0, Lfth;->a:Lftf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(FF)F
    .locals 2

    .prologue
    .line 1386
    mul-float v0, p2, p1

    const/high16 v1, -0x3ef00000    # -9.0f

    mul-float/2addr v1, p1

    mul-float/2addr v1, p1

    add-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 4

    .prologue
    .line 1390
    iget-object v0, p0, Lfth;->a:Lftf;

    iget-object v0, v0, Lftf;->c:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->computeScroll()V

    .line 1391
    float-to-double v0, p1

    const-wide v2, 0x3fe51eb851eb851fL    # 0.66

    cmpg-double v0, v0, v2

    if-gez v0, :cond_0

    .line 1392
    const/high16 v0, 0x40c00000    # 6.0f

    invoke-direct {p0, p1, v0}, Lfth;->a(FF)F

    move-result v0

    .line 1394
    :goto_0
    return v0

    :cond_0
    const v0, 0x3f28f5c3    # 0.66f

    sub-float v0, p1, v0

    const/high16 v1, 0x40400000    # 3.0f

    invoke-direct {p0, v0, v1}, Lfth;->a(FF)F

    move-result v0

    goto :goto_0
.end method
