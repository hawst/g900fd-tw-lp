.class public final Lipa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lioz;


# static fields
.field private static final a:Likg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Likg",
            "<",
            "Lijs;",
            "Lggf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lipb;

    invoke-direct {v0}, Lipb;-><init>()V

    sput-object v0, Lipa;->a:Likg;

    .line 28
    new-instance v0, Lipd;

    invoke-direct {v0}, Lipd;-><init>()V

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lipj;Lipf;I)Lijq;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lipj;",
            "Lipf;",
            "I)",
            "Lijq",
            "<",
            "Lijs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 40
    instance-of v0, p2, Lins;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "listener must be an instance of BaseGcoreOnDataChangedImpl."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_0
    check-cast p2, Lins;

    .line 46
    sget-object v1, Lgzf;->g:Lgzd;

    .line 48
    instance-of v0, p1, Lipk;

    if-eqz v0, :cond_1

    check-cast p1, Lipk;

    invoke-virtual {p1}, Lipk;->e()Lgfz;

    move-result-object v0

    :goto_0
    invoke-virtual {p2}, Lins;->a()Lgze;

    move-result-object v2

    .line 47
    invoke-interface {v1, v0, v2, p3}, Lgzd;->a(Lgfz;Lgze;I)Lggd;

    move-result-object v0

    .line 50
    new-instance v1, Lijr;

    sget-object v2, Lipa;->a:Likg;

    invoke-direct {v1, v0, v2}, Lijr;-><init>(Lggd;Likg;)V

    return-object v1

    .line 48
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
