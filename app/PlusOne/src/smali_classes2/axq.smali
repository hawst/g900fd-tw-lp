.class public final enum Laxq;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Laxq;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Laxq;

.field public static final enum b:Laxq;

.field public static final enum c:Laxq;

.field public static final enum d:Laxq;

.field public static final enum e:Laxq;

.field public static final enum f:Laxq;

.field public static final enum g:Laxq;

.field public static final enum h:Laxq;

.field public static final enum i:Laxq;

.field private static final synthetic j:[Laxq;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 43
    new-instance v0, Laxq;

    const-string v1, "USED_VIDEOS"

    invoke-direct {v0, v1, v3}, Laxq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laxq;->a:Laxq;

    .line 44
    new-instance v0, Laxq;

    const-string v1, "USED_PHOTOS"

    invoke-direct {v0, v1, v4}, Laxq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laxq;->b:Laxq;

    .line 45
    new-instance v0, Laxq;

    const-string v1, "PLAYED"

    invoke-direct {v0, v1, v5}, Laxq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laxq;->c:Laxq;

    .line 46
    new-instance v0, Laxq;

    const-string v1, "ADDED_TITLE"

    invoke-direct {v0, v1, v6}, Laxq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laxq;->d:Laxq;

    .line 47
    new-instance v0, Laxq;

    const-string v1, "CHANGED_THEME"

    invoke-direct {v0, v1, v7}, Laxq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laxq;->e:Laxq;

    .line 48
    new-instance v0, Laxq;

    const-string v1, "CHANGED_TARGET_DURATION"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Laxq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laxq;->f:Laxq;

    .line 49
    new-instance v0, Laxq;

    const-string v1, "EDITED_STORYBOARD"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Laxq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laxq;->g:Laxq;

    .line 50
    new-instance v0, Laxq;

    const-string v1, "ADDED_SOUNDTRACK"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Laxq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laxq;->h:Laxq;

    .line 51
    new-instance v0, Laxq;

    const-string v1, "SAVED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Laxq;-><init>(Ljava/lang/String;I)V

    sput-object v0, Laxq;->i:Laxq;

    .line 41
    const/16 v0, 0x9

    new-array v0, v0, [Laxq;

    sget-object v1, Laxq;->a:Laxq;

    aput-object v1, v0, v3

    sget-object v1, Laxq;->b:Laxq;

    aput-object v1, v0, v4

    sget-object v1, Laxq;->c:Laxq;

    aput-object v1, v0, v5

    sget-object v1, Laxq;->d:Laxq;

    aput-object v1, v0, v6

    sget-object v1, Laxq;->e:Laxq;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Laxq;->f:Laxq;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Laxq;->g:Laxq;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Laxq;->h:Laxq;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Laxq;->i:Laxq;

    aput-object v2, v0, v1

    sput-object v0, Laxq;->j:[Laxq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 41
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Laxq;
    .locals 1

    .prologue
    .line 41
    const-class v0, Laxq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Laxq;

    return-object v0
.end method

.method public static values()[Laxq;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Laxq;->j:[Laxq;

    invoke-virtual {v0}, [Laxq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laxq;

    return-object v0
.end method
