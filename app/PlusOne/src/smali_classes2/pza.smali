.class public final Lpza;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Float;

.field public b:Ljava/lang/Float;

.field public c:Ljava/lang/Float;

.field public d:Ljava/lang/Float;

.field public e:Ljava/lang/Float;

.field public f:Ljava/lang/Float;

.field private g:Ljava/lang/Float;

.field private h:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 160
    const/4 v0, 0x0

    .line 161
    iget-object v1, p0, Lpza;->a:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 162
    const/4 v0, 0x1

    iget-object v1, p0, Lpza;->a:Ljava/lang/Float;

    .line 163
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 165
    :cond_0
    iget-object v1, p0, Lpza;->b:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 166
    const/4 v1, 0x2

    iget-object v2, p0, Lpza;->b:Ljava/lang/Float;

    .line 167
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 169
    :cond_1
    iget-object v1, p0, Lpza;->c:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 170
    const/4 v1, 0x3

    iget-object v2, p0, Lpza;->c:Ljava/lang/Float;

    .line 171
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 173
    :cond_2
    iget-object v1, p0, Lpza;->d:Ljava/lang/Float;

    if-eqz v1, :cond_3

    .line 174
    const/4 v1, 0x4

    iget-object v2, p0, Lpza;->d:Ljava/lang/Float;

    .line 175
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 177
    :cond_3
    iget-object v1, p0, Lpza;->e:Ljava/lang/Float;

    if-eqz v1, :cond_4

    .line 178
    const/4 v1, 0x5

    iget-object v2, p0, Lpza;->e:Ljava/lang/Float;

    .line 179
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 181
    :cond_4
    iget-object v1, p0, Lpza;->f:Ljava/lang/Float;

    if-eqz v1, :cond_5

    .line 182
    const/4 v1, 0x6

    iget-object v2, p0, Lpza;->f:Ljava/lang/Float;

    .line 183
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 185
    :cond_5
    iget-object v1, p0, Lpza;->g:Ljava/lang/Float;

    if-eqz v1, :cond_6

    .line 186
    const/4 v1, 0x7

    iget-object v2, p0, Lpza;->g:Ljava/lang/Float;

    .line 187
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 189
    :cond_6
    iget-object v1, p0, Lpza;->h:Ljava/lang/Float;

    if-eqz v1, :cond_7

    .line 190
    const/16 v1, 0x8

    iget-object v2, p0, Lpza;->h:Ljava/lang/Float;

    .line 191
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 193
    :cond_7
    iget-object v1, p0, Lpza;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 194
    iput v0, p0, Lpza;->ai:I

    .line 195
    return v0
.end method

.method public a(Loxn;)Lpza;
    .locals 2

    .prologue
    .line 203
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 204
    sparse-switch v0, :sswitch_data_0

    .line 208
    iget-object v1, p0, Lpza;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 209
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpza;->ah:Ljava/util/List;

    .line 212
    :cond_1
    iget-object v1, p0, Lpza;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 214
    :sswitch_0
    return-object p0

    .line 219
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpza;->a:Ljava/lang/Float;

    goto :goto_0

    .line 223
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpza;->b:Ljava/lang/Float;

    goto :goto_0

    .line 227
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpza;->c:Ljava/lang/Float;

    goto :goto_0

    .line 231
    :sswitch_4
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpza;->d:Ljava/lang/Float;

    goto :goto_0

    .line 235
    :sswitch_5
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpza;->e:Ljava/lang/Float;

    goto :goto_0

    .line 239
    :sswitch_6
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpza;->f:Ljava/lang/Float;

    goto :goto_0

    .line 243
    :sswitch_7
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpza;->g:Ljava/lang/Float;

    goto :goto_0

    .line 247
    :sswitch_8
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpza;->h:Ljava/lang/Float;

    goto :goto_0

    .line 204
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
        0x3d -> :sswitch_7
        0x45 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lpza;->a:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 131
    const/4 v0, 0x1

    iget-object v1, p0, Lpza;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 133
    :cond_0
    iget-object v0, p0, Lpza;->b:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 134
    const/4 v0, 0x2

    iget-object v1, p0, Lpza;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 136
    :cond_1
    iget-object v0, p0, Lpza;->c:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 137
    const/4 v0, 0x3

    iget-object v1, p0, Lpza;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 139
    :cond_2
    iget-object v0, p0, Lpza;->d:Ljava/lang/Float;

    if-eqz v0, :cond_3

    .line 140
    const/4 v0, 0x4

    iget-object v1, p0, Lpza;->d:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 142
    :cond_3
    iget-object v0, p0, Lpza;->e:Ljava/lang/Float;

    if-eqz v0, :cond_4

    .line 143
    const/4 v0, 0x5

    iget-object v1, p0, Lpza;->e:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 145
    :cond_4
    iget-object v0, p0, Lpza;->f:Ljava/lang/Float;

    if-eqz v0, :cond_5

    .line 146
    const/4 v0, 0x6

    iget-object v1, p0, Lpza;->f:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 148
    :cond_5
    iget-object v0, p0, Lpza;->g:Ljava/lang/Float;

    if-eqz v0, :cond_6

    .line 149
    const/4 v0, 0x7

    iget-object v1, p0, Lpza;->g:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 151
    :cond_6
    iget-object v0, p0, Lpza;->h:Ljava/lang/Float;

    if-eqz v0, :cond_7

    .line 152
    const/16 v0, 0x8

    iget-object v1, p0, Lpza;->h:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 154
    :cond_7
    iget-object v0, p0, Lpza;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 156
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Lpza;->a(Loxn;)Lpza;

    move-result-object v0

    return-object v0
.end method
