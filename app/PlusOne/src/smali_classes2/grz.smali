.class public final Lgrz;
.super Ljava/lang/Object;


# instance fields
.field private final a:Landroid/content/Intent;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.location.places.ui.PICK_PLACE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lgrz;->a:Landroid/content/Intent;

    iget-object v0, p0, Lgrz;->a:Landroid/content/Intent;

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 1

    invoke-static {p1}, Lgfk;->b(Landroid/content/Context;)V

    iget-object v0, p0, Lgrz;->a:Landroid/content/Intent;

    return-object v0
.end method

.method public a(Lcom/google/android/gms/maps/model/LatLngBounds;)Lgrz;
    .locals 3

    const/4 v0, 0x0

    invoke-static {p1}, Lgih;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lgrz;->a:Landroid/content/Intent;

    const-string v2, "mode"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    const-string v1, "Setting LatLngBounds only allowed in exploration mode"

    invoke-static {v0, v1}, Lgih;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lgrz;->a:Landroid/content/Intent;

    const-string v1, "latlng_bounds"

    invoke-static {p1, v0, v1}, Lgil;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;Landroid/content/Intent;Ljava/lang/String;)V

    return-object p0
.end method
