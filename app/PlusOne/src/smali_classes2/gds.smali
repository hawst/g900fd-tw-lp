.class public final Lgds;
.super Ljava/lang/Object;


# instance fields
.field a:Lcom/google/android/gms/cast/CastDevice;

.field b:Lgdt;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/CastDevice;Lgdt;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "CastDevice parameter cannot be null"

    invoke-static {p1, v0}, Lgih;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "CastListener parameter cannot be null"

    invoke-static {p2, v0}, Lgih;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iput-object p1, p0, Lgds;->a:Lcom/google/android/gms/cast/CastDevice;

    iput-object p2, p0, Lgds;->b:Lgdt;

    return-void
.end method


# virtual methods
.method public a()Lgdr;
    .locals 1

    new-instance v0, Lgdr;

    invoke-direct {v0, p0}, Lgdr;-><init>(Lgds;)V

    return-object v0
.end method
