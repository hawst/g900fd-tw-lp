.class final Lbik;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:I

.field public static final b:I

.field public static final c:I

.field public static final d:I

.field public static final e:I

.field public static final f:I

.field public static final g:I

.field public static final h:I

.field public static final i:I

.field public static final j:I

.field public static final k:I

.field public static final l:I

.field public static final m:I

.field public static final n:I

.field public static final o:I

.field public static final p:I

.field public static final q:I

.field public static final r:I

.field public static final s:I

.field public static final t:I

.field public static final u:I

.field public static final v:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    const-string v0, "moov"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->a:I

    .line 87
    const-string v0, "mvhd"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->b:I

    .line 89
    const-string v0, "trak"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->c:I

    .line 91
    const-string v0, "tkhd"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    .line 93
    const-string v0, "mdia"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->d:I

    .line 95
    const-string v0, "mdhd"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->e:I

    .line 97
    const-string v0, "hdlr"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->f:I

    .line 99
    const-string v0, "minf"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->g:I

    .line 101
    const-string v0, "vmhd"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->h:I

    .line 103
    const-string v0, "smhd"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->i:I

    .line 105
    const-string v0, "stbl"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->j:I

    .line 107
    const-string v0, "stsd"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->k:I

    .line 111
    const-string v0, "avc1"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->l:I

    .line 113
    const-string v0, "avcC"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->m:I

    .line 117
    const-string v0, "mp4v"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->n:I

    .line 121
    const-string v0, "mp4a"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->o:I

    .line 124
    const-string v0, "esds"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->p:I

    .line 128
    const-string v0, "stts"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->q:I

    .line 130
    const-string v0, "stss"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->r:I

    .line 132
    const-string v0, "stsc"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->s:I

    .line 134
    const-string v0, "stsz"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->t:I

    .line 136
    const-string v0, "stco"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->u:I

    .line 140
    const-string v0, "mdat"

    invoke-static {v0}, Lbik;->a(Ljava/lang/String;)I

    move-result v0

    sput v0, Lbik;->v:I

    return-void
.end method

.method static final a(Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v0, 0x0

    .line 145
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const-string v2, "typeName.length()"

    const/4 v3, 0x0

    invoke-static {v1, v2, v4, v3}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    move v1, v0

    .line 147
    :goto_0
    if-ge v0, v4, :cond_0

    .line 148
    shl-int/lit8 v1, v1, 0x8

    .line 149
    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v2

    or-int/2addr v1, v2

    .line 147
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 151
    :cond_0
    return v1
.end method
