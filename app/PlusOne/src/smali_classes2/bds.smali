.class final Lbds;
.super Landroid/util/Property;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/Property",
        "<",
        "Landroid/view/View;",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# instance fields
.field private a:F

.field private b:I

.field private synthetic c:Lbco;


# direct methods
.method constructor <init>(Lbco;Ljava/lang/Class;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 385
    iput-object p1, p0, Lbds;->c:Lbco;

    invoke-direct {p0, p2, p3}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    .line 387
    const/4 v0, 0x0

    iput v0, p0, Lbds;->a:F

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 439
    iget v0, p0, Lbds;->a:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Float;)V
    .locals 4

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    const/high16 v3, 0x3f800000    # 1.0f

    .line 397
    iget v0, p0, Lbds;->b:I

    if-nez v0, :cond_0

    .line 398
    iget-object v0, p0, Lbds;->c:Lbco;

    invoke-virtual {v0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0094

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lbds;->b:I

    .line 401
    :cond_0
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lbds;->a:F

    .line 406
    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 407
    sget-object v0, Lcak;->a:Lcak;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v1, v2

    sub-float/2addr v1, v3

    invoke-virtual {v0, v1}, Lcak;->getInterpolation(F)F

    move-result v0

    .line 410
    iget-object v1, p0, Lbds;->c:Lbco;

    invoke-static {v1}, Lbco;->a(Lbco;)Landroid/view/View;

    move-result-object v1

    iget v2, p0, Lbds;->b:I

    int-to-float v2, v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 411
    iget-object v1, p0, Lbds;->c:Lbco;

    invoke-static {v1}, Lbco;->a(Lbco;)Landroid/view/View;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 413
    iget-object v1, p0, Lbds;->c:Lbco;

    invoke-static {v1}, Lbco;->h(Lbco;)Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    move-result-object v1

    sub-float v2, v3, v0

    invoke-virtual {v1, v2}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->setAlpha(F)V

    .line 414
    iget-object v1, p0, Lbds;->c:Lbco;

    iget-object v1, v1, Lbco;->N:Landroid/view/View;

    if-eqz v1, :cond_1

    .line 415
    iget-object v1, p0, Lbds;->c:Lbco;

    iget-object v1, v1, Lbco;->N:Landroid/view/View;

    sub-float v2, v3, v0

    invoke-virtual {v1, v2}, Landroid/view/View;->setAlpha(F)V

    .line 417
    :cond_1
    iget-object v1, p0, Lbds;->c:Lbco;

    invoke-static {v1}, Lbco;->i(Lbco;)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 418
    iget-object v1, p0, Lbds;->c:Lbco;

    invoke-static {v1}, Lbco;->i(Lbco;)Landroid/view/View;

    move-result-object v1

    sub-float v0, v3, v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 435
    :cond_2
    :goto_0
    return-void

    .line 421
    :cond_3
    sget-object v0, Lcak;->a:Lcak;

    invoke-virtual {p1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    mul-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lcak;->getInterpolation(F)F

    move-result v0

    .line 423
    iget-object v1, p0, Lbds;->c:Lbco;

    invoke-static {v1}, Lbco;->a(Lbco;)Landroid/view/View;

    move-result-object v1

    iget v2, p0, Lbds;->b:I

    int-to-float v2, v2

    mul-float/2addr v2, v0

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationY(F)V

    .line 425
    iget-object v1, p0, Lbds;->c:Lbco;

    invoke-static {v1}, Lbco;->a(Lbco;)Landroid/view/View;

    move-result-object v1

    sub-float v0, v3, v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 427
    iget-object v0, p0, Lbds;->c:Lbco;

    invoke-static {v0}, Lbco;->h(Lbco;)Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->setAlpha(F)V

    .line 428
    iget-object v0, p0, Lbds;->c:Lbco;

    iget-object v0, v0, Lbco;->N:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 429
    iget-object v0, p0, Lbds;->c:Lbco;

    iget-object v0, v0, Lbco;->N:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 431
    :cond_4
    iget-object v0, p0, Lbds;->c:Lbco;

    invoke-static {v0}, Lbco;->i(Lbco;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 432
    iget-object v0, p0, Lbds;->c:Lbco;

    invoke-static {v0}, Lbco;->i(Lbco;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method

.method public synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 385
    invoke-virtual {p0}, Lbds;->a()Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 385
    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p0, p2}, Lbds;->a(Ljava/lang/Float;)V

    return-void
.end method
