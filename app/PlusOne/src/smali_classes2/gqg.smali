.class public final Lgqg;
.super Lghh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lghh",
        "<",
        "Lgqm;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Lggb;Lggc;)V
    .locals 1

    invoke-direct {p0, p1, p2, p4, p5}, Lghh;-><init>(Landroid/content/Context;Landroid/os/Looper;Lggb;Lggc;)V

    invoke-virtual {p3}, Lcom/google/android/gms/common/internal/ClientSettings;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lgih;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p3}, Lcom/google/android/gms/common/internal/ClientSettings;->a()Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, Lgqg;->b(Landroid/os/IBinder;)Lgqm;

    move-result-object v0

    return-object v0
.end method

.method public a(Lgfy;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfy",
            "<",
            "Lhac;",
            ">;)V"
        }
    .end annotation

    invoke-virtual {p0}, Lgqg;->h()V

    new-instance v1, Lgqh;

    invoke-direct {v1, p0, p1}, Lgqh;-><init>(Lgqg;Lgfy;)V

    :try_start_0
    invoke-virtual {p0}, Lgqg;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lgqm;

    invoke-interface {v0, v1}, Lgqm;->b(Lgqj;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lgqh;->a(ILandroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method protected a(Lgic;Lghl;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const v1, 0x5e3530

    iget-object v2, p0, Lghh;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lgic;->n(Lghz;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method protected b(Landroid/os/IBinder;)Lgqm;
    .locals 1

    invoke-static {p1}, Lgqn;->a(Landroid/os/IBinder;)Lgqm;

    move-result-object v0

    return-object v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.photos.autobackup.service.START"

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.photos.autobackup.internal.IAutoBackupService"

    return-object v0
.end method
