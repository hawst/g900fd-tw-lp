.class public final Lcej;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcgm;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Loxu;",
        ">",
        "Ljava/lang/Object;",
        "Lcgm",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lcgm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcgm",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:Lcei;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcei",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcgm;Lcei;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcgm",
            "<TT;>;",
            "Lcei",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-string v0, "serializer"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgm;

    iput-object v0, p0, Lcej;->a:Lcgm;

    .line 34
    const-string v0, "extractor"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcei;

    iput-object v0, p0, Lcej;->b:Lcei;

    .line 35
    return-void
.end method


# virtual methods
.method public a(Loxu;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)I"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lcej;->b:Lcei;

    const/4 v1, 0x0

    iget-object v2, p0, Lcej;->a:Lcgm;

    invoke-interface {v0, p1, v1, v2}, Lcei;->a(Loxu;Loxo;Lcgm;)I

    move-result v0

    return v0
.end method

.method public a(Loxu;Loxn;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Loxn;",
            ")V"
        }
    .end annotation

    .prologue
    .line 51
    :goto_0
    invoke-virtual {p2}, Loxn;->v()Z

    move-result v0

    if-nez v0, :cond_1

    .line 52
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 53
    :cond_0
    iget-object v0, p0, Lcej;->a:Lcgm;

    invoke-interface {v0, p1, p2}, Lcgm;->a(Loxu;Loxn;)V

    goto :goto_0

    .line 55
    :cond_1
    return-void
.end method

.method public a(Loxu;Loxo;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Loxo;",
            ")V"
        }
    .end annotation

    .prologue
    .line 45
    iget-object v0, p0, Lcej;->b:Lcei;

    iget-object v1, p0, Lcej;->a:Lcgm;

    invoke-interface {v0, p1, p2, v1}, Lcei;->a(Loxu;Loxo;Lcgm;)I

    .line 46
    return-void
.end method
