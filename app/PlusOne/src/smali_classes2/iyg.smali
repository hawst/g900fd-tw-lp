.class final Liyg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/view/ViewGroup;

.field private final b:Landroid/view/ViewGroup;

.field private final c:Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;

.field private final d:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>(Landroid/view/ViewGroup;Landroid/view/ViewGroup;Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Liyg;->a:Landroid/view/ViewGroup;

    .line 43
    iput-object p2, p0, Liyg;->b:Landroid/view/ViewGroup;

    .line 44
    iput-object p3, p0, Liyg;->c:Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;

    .line 45
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Liyg;->d:Landroid/view/LayoutInflater;

    .line 46
    return-void
.end method


# virtual methods
.method public a(Lnqg;Liyf;)V
    .locals 13

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    const/4 v7, 0x0

    .line 54
    iget-object v0, p0, Liyg;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 56
    iget-object v0, p1, Lnqg;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Liyg;->a:Landroid/view/ViewGroup;

    const v1, 0x7f10017e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 58
    iget-object v1, p1, Lnqg;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 61
    :cond_0
    iget-object v0, p1, Lnqg;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 62
    iget-object v0, p0, Liyg;->a:Landroid/view/ViewGroup;

    const v1, 0x7f100471

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 63
    iget-object v1, p1, Lnqg;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    :cond_1
    const v4, 0x7f10000a

    .line 68
    iget-object v9, p1, Lnqg;->b:[Lnqa;

    .line 72
    array-length v3, v9

    .line 73
    array-length v0, v9

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_2

    .line 74
    aget-object v1, v9, v0

    .line 75
    iget-object v1, v1, Lnqa;->e:Lnpx;

    if-eqz v1, :cond_2

    .line 76
    add-int/lit8 v3, v3, -0x1

    .line 73
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_2
    move v8, v7

    move-object v1, v2

    .line 82
    :goto_1
    if-ge v8, v3, :cond_c

    .line 83
    aget-object v10, v9, v8

    .line 84
    iget-object v0, v10, Lnqa;->c:Lnqe;

    if-eqz v0, :cond_4

    iget-object v0, v10, Lnqa;->c:Lnqe;

    iget v5, v0, Lnqe;->c:I

    packed-switch v5, :pswitch_data_0

    const-string v5, "OutOfBoxInflater"

    iget v0, v0, Lnqe;->c:I

    new-instance v11, Ljava/lang/StringBuilder;

    const/16 v12, 0x2d

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v12, "Input field has unsupported type: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    :goto_2
    iget-object v5, v10, Lnqa;->b:Lnqf;

    if-nez v5, :cond_3

    iget-object v5, v10, Lnqa;->e:Lnpx;

    if-eqz v5, :cond_9

    :cond_3
    move v5, v6

    :goto_3
    if-eqz v5, :cond_b

    invoke-virtual {v0, v10, v4, p2}, Lixo;->a(Lnqa;ILiyf;)V

    :goto_4
    invoke-virtual {v0}, Lixo;->getChildCount()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0, v4}, Lixo;->setId(I)V

    iget-object v4, p0, Liyg;->b:Landroid/view/ViewGroup;

    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 85
    invoke-virtual {v0}, Lixo;->getId()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    .line 86
    invoke-virtual {v0}, Lixo;->b()Landroid/view/View;

    move-result-object v0

    .line 87
    iget-object v5, v10, Lnqa;->c:Lnqe;

    if-eqz v5, :cond_f

    iget-object v5, v10, Lnqa;->c:Lnqe;

    iget v5, v5, Lnqe;->c:I

    if-ne v5, v6, :cond_f

    .line 88
    check-cast v0, Landroid/widget/EditText;

    .line 89
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 82
    :goto_5
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    move-object v1, v0

    goto :goto_1

    .line 84
    :pswitch_0
    const v0, 0x7f04015c

    :goto_6
    iget-object v5, p0, Liyg;->d:Landroid/view/LayoutInflater;

    iget-object v11, p0, Liyg;->b:Landroid/view/ViewGroup;

    invoke-virtual {v5, v0, v11, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lixo;

    goto :goto_2

    :pswitch_1
    const v0, 0x7f040159

    goto :goto_6

    :pswitch_2
    const v0, 0x7f040150

    goto :goto_6

    :pswitch_3
    const v0, 0x7f040155

    goto :goto_6

    :pswitch_4
    const v0, 0x7f04014e

    goto :goto_6

    :cond_4
    iget-object v0, v10, Lnqa;->b:Lnqf;

    if-eqz v0, :cond_5

    iget-object v0, p0, Liyg;->d:Landroid/view/LayoutInflater;

    const v5, 0x7f04015b

    iget-object v11, p0, Liyg;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5, v11, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lixo;

    goto :goto_2

    :cond_5
    iget-object v0, v10, Lnqa;->d:Lnpz;

    if-eqz v0, :cond_6

    iget-object v0, p0, Liyg;->d:Landroid/view/LayoutInflater;

    const v5, 0x7f040153

    iget-object v11, p0, Liyg;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5, v11, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lixo;

    goto :goto_2

    :cond_6
    iget-object v0, v10, Lnqa;->e:Lnpx;

    if-eqz v0, :cond_7

    iget-object v0, p0, Liyg;->d:Landroid/view/LayoutInflater;

    const v5, 0x7f04014f

    iget-object v11, p0, Liyg;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5, v11, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lixo;

    goto/16 :goto_2

    :cond_7
    iget-object v0, v10, Lnqa;->f:Lnqd;

    if-eqz v0, :cond_8

    iget-object v0, p0, Liyg;->d:Landroid/view/LayoutInflater;

    const v5, 0x7f040156

    iget-object v11, p0, Liyg;->b:Landroid/view/ViewGroup;

    invoke-virtual {v0, v5, v11, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lixo;

    goto/16 :goto_2

    :cond_8
    const-string v0, "OutOfBoxInflater"

    const-string v5, "Field doesn\'t have content."

    invoke-static {v0, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    goto/16 :goto_2

    :cond_9
    iget-object v5, v10, Lnqa;->c:Lnqe;

    if-eqz v5, :cond_a

    iget-object v5, v10, Lnqa;->c:Lnqe;

    iget-object v5, v5, Lnqe;->g:Ljava/lang/Boolean;

    if-eqz v5, :cond_a

    iget-object v5, v10, Lnqa;->c:Lnqe;

    iget-object v5, v5, Lnqe;->g:Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_a

    move v5, v6

    goto/16 :goto_3

    :cond_a
    move v5, v7

    goto/16 :goto_3

    :cond_b
    invoke-virtual {v0, v10, v4, v2}, Lixo;->a(Lnqa;ILiyf;)V

    goto/16 :goto_4

    .line 93
    :cond_c
    if-eqz v1, :cond_d

    .line 94
    const/4 v0, 0x6

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 97
    :cond_d
    iget-object v0, p0, Liyg;->c:Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->a()V

    move v0, v3

    move v1, v4

    .line 98
    :goto_7
    array-length v2, v9

    if-ge v0, v2, :cond_e

    .line 99
    aget-object v2, v9, v0

    .line 100
    iget-object v3, v2, Lnqa;->e:Lnpx;

    .line 101
    iget-object v4, p0, Liyg;->c:Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;

    add-int/lit8 v2, v1, 0x1

    iget-object v5, v3, Lnpx;->c:Ljava/lang/String;

    new-instance v6, Liyh;

    invoke-direct {v6, p2, v3}, Liyh;-><init>(Liyf;Lnpx;)V

    invoke-virtual {v4, v1, v5, v3, v6}, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->a(ILjava/lang/String;Ljava/lang/Object;Landroid/view/View$OnClickListener;)V

    .line 98
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_7

    .line 110
    :cond_e
    return-void

    :cond_f
    move-object v0, v1

    goto/16 :goto_5

    .line 84
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method
