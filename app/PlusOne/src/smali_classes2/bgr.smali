.class public final Lbgr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lbgq;",
            "Lbhf;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lbgq;",
            "Lbfv;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lbgy;",
            "Lbgx;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbgr;->a:Ljava/util/Map;

    .line 18
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbgr;->b:Ljava/util/Map;

    .line 19
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbgr;->c:Ljava/util/Map;

    .line 30
    iput-object p1, p0, Lbgr;->d:Landroid/content/Context;

    .line 31
    return-void
.end method

.method private a(Lbgy;)Lbgx;
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lbgr;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgx;

    return-object v0
.end method

.method private a(Lbgy;Lbhf;Lbfv;)Lbgx;
    .locals 2

    .prologue
    .line 233
    new-instance v0, Lbgx;

    invoke-direct {v0, p2, p3}, Lbgx;-><init>(Lbhf;Lbfv;)V

    .line 234
    iget-object v1, p0, Lbgr;->c:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 235
    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lbfv;
    .locals 3

    .prologue
    .line 121
    new-instance v1, Lbgq;

    invoke-direct {v1, p1}, Lbgq;-><init>(Ljava/lang/String;)V

    .line 122
    iget-object v0, p0, Lbgr;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbfv;

    .line 123
    if-nez v0, :cond_0

    .line 124
    new-instance v0, Lbfv;

    invoke-direct {v0, p1}, Lbfv;-><init>(Ljava/lang/String;)V

    .line 125
    iget-object v2, p0, Lbgr;->b:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    :cond_0
    return-object v0
.end method

.method public a(II)Lbgx;
    .locals 3

    .prologue
    .line 154
    new-instance v1, Lbgy;

    new-instance v0, Lbgq;

    invoke-direct {v0, p1}, Lbgq;-><init>(I)V

    new-instance v2, Lbgq;

    invoke-direct {v2, p2}, Lbgq;-><init>(I)V

    invoke-direct {v1, v0, v2}, Lbgy;-><init>(Lbgq;Lbgq;)V

    .line 158
    invoke-direct {p0, v1}, Lbgr;->a(Lbgy;)Lbgx;

    move-result-object v0

    .line 159
    if-eqz v0, :cond_0

    .line 160
    :goto_0
    return-object v0

    .line 161
    :cond_0
    invoke-virtual {p0, p1}, Lbgr;->a(I)Lbhf;

    move-result-object v0

    .line 162
    invoke-virtual {p0, p2}, Lbgr;->b(I)Lbfv;

    move-result-object v2

    .line 160
    invoke-direct {p0, v1, v0, v2}, Lbgr;->a(Lbgy;Lbhf;Lbfv;)Lbgx;

    move-result-object v0

    goto :goto_0
.end method

.method public a(ILjava/lang/String;)Lbgx;
    .locals 3

    .prologue
    .line 190
    new-instance v1, Lbgy;

    new-instance v0, Lbgq;

    invoke-direct {v0, p1}, Lbgq;-><init>(I)V

    new-instance v2, Lbgq;

    invoke-direct {v2, p2}, Lbgq;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v0, v2}, Lbgy;-><init>(Lbgq;Lbgq;)V

    .line 194
    invoke-direct {p0, v1}, Lbgr;->a(Lbgy;)Lbgx;

    move-result-object v0

    .line 195
    if-eqz v0, :cond_0

    .line 196
    :goto_0
    return-object v0

    .line 197
    :cond_0
    invoke-virtual {p0, p1}, Lbgr;->a(I)Lbhf;

    move-result-object v0

    .line 198
    invoke-virtual {p0, p2}, Lbgr;->a(Ljava/lang/String;)Lbfv;

    move-result-object v2

    .line 196
    invoke-direct {p0, v1, v0, v2}, Lbgr;->a(Lbgy;Lbhf;Lbfv;)Lbgx;

    move-result-object v0

    goto :goto_0
.end method

.method public a(I)Lbhf;
    .locals 3

    .prologue
    .line 60
    iget-object v0, p0, Lbgr;->d:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot load vertex shader from resource id without a context"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :cond_0
    new-instance v1, Lbgq;

    invoke-direct {v1, p1}, Lbgq;-><init>(I)V

    .line 66
    iget-object v0, p0, Lbgr;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhf;

    .line 67
    if-nez v0, :cond_1

    .line 68
    new-instance v0, Lbhf;

    iget-object v2, p0, Lbgr;->d:Landroid/content/Context;

    invoke-direct {v0, v2, p1}, Lbhf;-><init>(Landroid/content/Context;I)V

    .line 69
    iget-object v2, p0, Lbgr;->a:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    :cond_1
    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lbgr;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgx;

    .line 39
    invoke-virtual {v0}, Lbgx;->a()V

    goto :goto_0

    .line 41
    :cond_0
    iget-object v0, p0, Lbgr;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhf;

    .line 42
    invoke-virtual {v0}, Lbhf;->a()V

    goto :goto_1

    .line 44
    :cond_1
    iget-object v0, p0, Lbgr;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbfv;

    .line 45
    invoke-virtual {v0}, Lbfv;->a()V

    goto :goto_2

    .line 47
    :cond_2
    iget-object v0, p0, Lbgr;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 48
    iget-object v0, p0, Lbgr;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 49
    iget-object v0, p0, Lbgr;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 50
    return-void
.end method

.method public b(I)Lbfv;
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lbgr;->d:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot load fragment shader from resource id without a context"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 104
    :cond_0
    new-instance v1, Lbgq;

    invoke-direct {v1, p1}, Lbgq;-><init>(I)V

    .line 105
    iget-object v0, p0, Lbgr;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbfv;

    .line 106
    if-nez v0, :cond_1

    .line 107
    new-instance v0, Lbfv;

    iget-object v2, p0, Lbgr;->d:Landroid/content/Context;

    invoke-direct {v0, v2, p1}, Lbfv;-><init>(Landroid/content/Context;I)V

    .line 108
    iget-object v2, p0, Lbgr;->b:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    :cond_1
    return-object v0
.end method
