.class final Laaf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Laaj;

.field private b:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Laah;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:[I

.field private i:I

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Landroid/hardware/Camera;

.field private m:I

.field private n:Lzx;

.field private o:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Laae;",
            ">;"
        }
    .end annotation
.end field

.field private p:Ljava/util/concurrent/locks/ReentrantLock;

.field private q:Ljava/util/concurrent/locks/Condition;

.field private r:Laag;

.field private s:Lacw;

.field private t:Lacs;

.field private synthetic u:Lzw;


# direct methods
.method public constructor <init>(Lzw;Lacs;)V
    .locals 7

    .prologue
    const/16 v6, 0x280

    const/16 v5, 0x1e0

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 884
    iput-object p1, p0, Laaf;->u:Lzw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 769
    new-instance v0, Laaj;

    invoke-direct {v0}, Laaj;-><init>()V

    iput-object v0, p0, Laaf;->a:Laaj;

    .line 772
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    iput-object v0, p0, Laaf;->b:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 775
    const/16 v0, 0x1e

    iput v0, p0, Laaf;->c:I

    .line 778
    iput v6, p0, Laaf;->d:I

    .line 782
    iput v5, p0, Laaf;->e:I

    .line 785
    iput v6, p0, Laaf;->f:I

    .line 786
    iput v5, p0, Laaf;->g:I

    .line 789
    iput-object v2, p0, Laaf;->h:[I

    .line 792
    iput v3, p0, Laaf;->i:I

    .line 798
    iput-boolean v4, p0, Laaf;->j:Z

    .line 801
    const-string v0, "off"

    iput-object v0, p0, Laaf;->k:Ljava/lang/String;

    .line 814
    iput-object v2, p0, Laaf;->l:Landroid/hardware/Camera;

    .line 816
    iput v3, p0, Laaf;->m:I

    .line 822
    iput-object v2, p0, Laaf;->n:Lzx;

    .line 825
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Laaf;->o:Ljava/util/Set;

    .line 827
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0, v4}, Ljava/util/concurrent/locks/ReentrantLock;-><init>(Z)V

    iput-object v0, p0, Laaf;->p:Ljava/util/concurrent/locks/ReentrantLock;

    .line 829
    iget-object v0, p0, Laaf;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Laaf;->q:Ljava/util/concurrent/locks/Condition;

    .line 831
    new-instance v0, Laag;

    invoke-direct {v0}, Laag;-><init>()V

    iput-object v0, p0, Laaf;->r:Laag;

    .line 885
    iput-object p2, p0, Laaf;->t:Lacs;

    .line 886
    invoke-virtual {p0}, Laaf;->a()Lacs;

    move-result-object v0

    iget-boolean v0, v0, Lacs;->b:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempting to use OpenGL ES 2 in a context that does not support it!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    new-instance v0, Laad;

    iget-object v1, p0, Laaf;->u:Lzw;

    invoke-direct {v0, v1}, Laad;-><init>(Lzw;)V

    iput-object v0, p0, Laaf;->n:Lzx;

    .line 887
    :goto_0
    iget-object v0, p0, Laaf;->n:Lzx;

    invoke-virtual {v0, p0}, Lzx;->a(Laaf;)V

    .line 888
    invoke-virtual {p0}, Laaf;->d()V

    .line 889
    return-void

    .line 886
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-lt v0, v1, :cond_2

    new-instance v0, Laaa;

    iget-object v1, p0, Laaf;->u:Lzw;

    invoke-direct {v0, v1}, Laaa;-><init>(Lzw;)V

    iput-object v0, p0, Laaf;->n:Lzx;

    goto :goto_0

    :cond_2
    new-instance v0, Lzy;

    iget-object v1, p0, Laaf;->u:Lzw;

    invoke-direct {v0, v1}, Lzy;-><init>(Lzw;)V

    iput-object v0, p0, Laaf;->n:Lzx;

    goto :goto_0
.end method

.method static synthetic a(Laaf;)Landroid/hardware/Camera;
    .locals 1

    .prologue
    .line 763
    iget-object v0, p0, Laaf;->l:Landroid/hardware/Camera;

    return-object v0
.end method

.method private a(IILjava/util/List;)[I
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/List",
            "<",
            "Landroid/hardware/Camera$Size;",
            ">;)[I"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v4, -0x1

    .line 1326
    .line 1328
    invoke-interface {p3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    iget v1, v0, Landroid/hardware/Camera$Size;->width:I

    .line 1329
    invoke-interface {p3, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    .line 1330
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v1

    move v3, v4

    move v5, v4

    move v1, v0

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    .line 1335
    iget v7, v0, Landroid/hardware/Camera$Size;->width:I

    if-gt v7, p1, :cond_0

    iget v7, v0, Landroid/hardware/Camera$Size;->height:I

    if-gt v7, p2, :cond_0

    iget v7, v0, Landroid/hardware/Camera$Size;->width:I

    if-lt v7, v5, :cond_0

    iget v7, v0, Landroid/hardware/Camera$Size;->height:I

    if-lt v7, v3, :cond_0

    .line 1339
    iget v5, v0, Landroid/hardware/Camera$Size;->width:I

    .line 1340
    iget v3, v0, Landroid/hardware/Camera$Size;->height:I

    .line 1342
    :cond_0
    iget v7, v0, Landroid/hardware/Camera$Size;->width:I

    if-ge v7, v2, :cond_3

    iget v7, v0, Landroid/hardware/Camera$Size;->height:I

    if-ge v7, v1, :cond_3

    .line 1344
    iget v2, v0, Landroid/hardware/Camera$Size;->width:I

    .line 1345
    iget v1, v0, Landroid/hardware/Camera$Size;->height:I

    move v0, v1

    move v1, v2

    :goto_1
    move v2, v1

    move v1, v0

    .line 1347
    goto :goto_0

    .line 1348
    :cond_1
    if-ne v5, v4, :cond_2

    .line 1353
    :goto_2
    const/4 v0, 0x2

    new-array v0, v0, [I

    aput v2, v0, v8

    const/4 v2, 0x1

    aput v1, v0, v2

    .line 1354
    return-object v0

    :cond_2
    move v1, v3

    move v2, v5

    goto :goto_2

    :cond_3
    move v0, v1

    move v1, v2

    goto :goto_1
.end method

.method private a(ILandroid/hardware/Camera$Parameters;)[I
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1358
    invoke-virtual {p2}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewFpsRange()Ljava/util/List;

    move-result-object v2

    .line 1359
    invoke-interface {v2, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 1360
    mul-int/lit16 v4, p1, 0x3e8

    .line 1361
    const v1, 0xf4240

    .line 1362
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v2, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 1363
    aget v3, v0, v7

    .line 1364
    const/4 v6, 0x1

    aget v6, v0, v6

    .line 1365
    if-gt v3, v4, :cond_1

    if-lt v6, v4, :cond_1

    .line 1366
    sub-int v3, v4, v3

    sub-int/2addr v6, v4

    add-int/2addr v3, v6

    .line 1367
    if-ge v3, v1, :cond_1

    move-object v1, v0

    move v0, v3

    :goto_1
    move-object v2, v1

    move v1, v0

    .line 1372
    goto :goto_0

    .line 1373
    :cond_0
    return-object v2

    :cond_1
    move v0, v1

    move-object v1, v2

    goto :goto_1
.end method

.method static synthetic b(Laaf;)I
    .locals 1

    .prologue
    .line 763
    iget v0, p0, Laaf;->i:I

    return v0
.end method

.method static synthetic c(Laaf;)Z
    .locals 1

    .prologue
    .line 763
    iget-boolean v0, p0, Laaf;->j:Z

    return v0
.end method

.method private f()Laah;
    .locals 1

    .prologue
    .line 1133
    :try_start_0
    iget-object v0, p0, Laaf;->b:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laah;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1136
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()Lacw;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1242
    iget-object v0, p0, Laaf;->s:Lacw;

    if-nez v0, :cond_0

    .line 1243
    invoke-static {v1, v1}, Lacw;->a(II)Lacw;

    move-result-object v0

    iput-object v0, p0, Laaf;->s:Lacw;

    .line 1245
    :cond_0
    iget-object v0, p0, Laaf;->s:Lacw;

    return-object v0
.end method

.method private h()V
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 1403
    :try_start_0
    sget-object v1, Lzw;->b:Ljava/util/concurrent/locks/ReentrantLock;

    sget-wide v2, Lzw;->a:J

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock(JLjava/util/concurrent/TimeUnit;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1404
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Timed out while waiting to acquire camera!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1407
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Interrupted while waiting to acquire camera!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1412
    :cond_0
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    .line 1413
    iget-object v2, p0, Laaf;->r:Laag;

    invoke-virtual {v2, v1}, Laag;->a(Ljava/lang/Object;)Z

    .line 1416
    monitor-enter p0

    .line 1417
    :try_start_1
    iget-object v2, p0, Laaf;->a:Laaj;

    monitor-enter v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v3

    if-nez v3, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Device does not have any cameras!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0

    .line 1420
    :catchall_1
    move-exception v0

    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 1417
    :cond_1
    const/4 v3, 0x0

    :try_start_4
    iput v3, p0, Laaf;->m:I

    new-instance v3, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v3}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    const/4 v4, 0x0

    invoke-static {v4, v3}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget v4, v3, Landroid/hardware/Camera$CameraInfo;->orientation:I

    iget v3, v3, Landroid/hardware/Camera$CameraInfo;->facing:I

    if-ne v3, v0, :cond_3

    :goto_0
    iput v0, p0, Laaf;->i:I

    const/4 v0, 0x0

    invoke-static {v0}, Landroid/hardware/Camera;->open(I)Landroid/hardware/Camera;

    move-result-object v0

    iput-object v0, p0, Laaf;->l:Landroid/hardware/Camera;

    iget-object v0, p0, Laaf;->l:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    iget v3, p0, Laaf;->d:I

    iget v4, p0, Laaf;->e:I

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5}, Laaf;->a(IILjava/util/List;)[I

    move-result-object v3

    iput-object v3, p0, Laaf;->h:[I

    iget-object v3, p0, Laaf;->n:Lzx;

    iget-object v4, p0, Laaf;->h:[I

    const/4 v5, 0x0

    aget v4, v4, v5

    iget-object v5, p0, Laaf;->h:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    invoke-virtual {v3, v4, v5}, Lzx;->a(II)V

    iget-object v3, p0, Laaf;->h:[I

    const/4 v4, 0x0

    aget v3, v3, v4

    iget-object v4, p0, Laaf;->h:[I

    const/4 v5, 0x1

    aget v4, v4, v5

    invoke-virtual {v0, v3, v4}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    iget v3, p0, Laaf;->f:I

    iget v4, p0, Laaf;->g:I

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v5

    invoke-direct {p0, v3, v4, v5}, Laaf;->a(IILjava/util/List;)[I

    move-result-object v3

    const/4 v4, 0x0

    aget v4, v3, v4

    const/4 v5, 0x1

    aget v3, v3, v5

    invoke-virtual {v0, v4, v3}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    iget v3, p0, Laaf;->c:I

    invoke-direct {p0, v3, v0}, Laaf;->a(ILandroid/hardware/Camera$Parameters;)[I

    move-result-object v3

    const/4 v4, 0x0

    aget v4, v3, v4

    const/4 v5, 0x1

    aget v3, v3, v5

    invoke-virtual {v0, v4, v3}, Landroid/hardware/Camera$Parameters;->setPreviewFpsRange(II)V

    invoke-virtual {v0}, Landroid/hardware/Camera$Parameters;->getFlashMode()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Laaf;->k:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/hardware/Camera$Parameters;->setFlashMode(Ljava/lang/String;)V

    :cond_2
    iget-object v3, p0, Laaf;->l:Landroid/hardware/Camera;

    invoke-virtual {v3, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1418
    :try_start_5
    iget-object v0, p0, Laaf;->n:Lzx;

    invoke-virtual {v0}, Lzx;->a()V

    .line 1420
    monitor-exit p0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1422
    iget-object v0, p0, Laaf;->l:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V

    .line 1425
    iget-object v2, p0, Laaf;->o:Ljava/util/Set;

    monitor-enter v2

    .line 1426
    :try_start_6
    iget-object v0, p0, Laaf;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 1427
    iget-object v3, p0, Laaf;->u:Lzw;

    goto :goto_1

    .line 1417
    :cond_3
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 1429
    :cond_4
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 1430
    iget-object v0, p0, Laaf;->r:Laag;

    invoke-virtual {v0, v1}, Laag;->b(Ljava/lang/Object;)V

    .line 1432
    iget-object v0, p0, Laaf;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 1433
    iget-object v0, p0, Laaf;->q:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V

    .line 1434
    iget-object v0, p0, Laaf;->p:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1435
    return-void

    .line 1429
    :catchall_2
    move-exception v0

    :try_start_7
    monitor-exit v2
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    throw v0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 1501
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    .line 1502
    iget-object v1, p0, Laaf;->r:Laag;

    invoke-virtual {v1, v0}, Laag;->a(Ljava/lang/Object;)Z

    .line 1503
    iget-object v1, p0, Laaf;->l:Landroid/hardware/Camera;

    if-eqz v1, :cond_0

    .line 1504
    iget-object v1, p0, Laaf;->l:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->stopPreview()V

    .line 1505
    iget-object v1, p0, Laaf;->l:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->release()V

    .line 1506
    const/4 v1, 0x0

    iput-object v1, p0, Laaf;->l:Landroid/hardware/Camera;

    .line 1508
    :cond_0
    sget-object v1, Lzw;->b:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 1509
    iget-object v1, p0, Laaf;->n:Lzx;

    invoke-virtual {v1}, Lzx;->c()V

    .line 1510
    iget-object v1, p0, Laaf;->r:Laag;

    invoke-virtual {v1, v0}, Laag;->b(Ljava/lang/Object;)V

    .line 1512
    iget-object v1, p0, Laaf;->o:Ljava/util/Set;

    monitor-enter v1

    .line 1513
    :try_start_0
    iget-object v0, p0, Laaf;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    .line 1514
    iget-object v2, p0, Laaf;->u:Lzw;

    goto :goto_0

    .line 1516
    :cond_1
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a()Lacs;
    .locals 1

    .prologue
    .line 892
    iget-object v0, p0, Laaf;->t:Lacs;

    return-object v0
.end method

.method public a(IZ)V
    .locals 3

    .prologue
    .line 940
    if-eqz p2, :cond_0

    .line 941
    :try_start_0
    iget-object v0, p0, Laaf;->b:Ljava/util/concurrent/LinkedBlockingQueue;

    new-instance v1, Laah;

    invoke-direct {v1, p1}, Laah;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V

    .line 950
    :goto_0
    return-void

    .line 943
    :cond_0
    iget-object v0, p0, Laaf;->b:Ljava/util/concurrent/LinkedBlockingQueue;

    new-instance v1, Laah;

    invoke-direct {v1, p1}, Laah;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;->offer(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 948
    :catch_0
    move-exception v0

    const-string v0, "CameraStreamer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Dropping event "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public a(Laae;)V
    .locals 2

    .prologue
    .line 1006
    iget-object v1, p0, Laaf;->o:Ljava/util/Set;

    monitor-enter v1

    .line 1007
    :try_start_0
    iget-object v0, p0, Laaf;->o:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1008
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Laas;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1109
    iget-object v2, p0, Laaf;->a:Laaj;

    monitor-enter v2

    .line 1110
    :try_start_0
    iget-object v3, p0, Laaf;->a:Laaj;

    invoke-virtual {v3}, Laaj;->a()I

    move-result v3

    if-eq v3, v1, :cond_0

    .line 1111
    monitor-exit v2

    .line 1119
    :goto_0
    return v0

    .line 1115
    :cond_0
    iget-object v3, p0, Laaf;->l:Landroid/hardware/Camera;

    if-nez v3, :cond_1

    .line 1116
    monitor-exit v2

    goto :goto_0

    .line 1120
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 1118
    :cond_1
    :try_start_1
    iget-object v0, p0, Laaf;->n:Lzx;

    invoke-virtual {v0, p1}, Lzx;->a(Laas;)V

    .line 1119
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    goto :goto_0
.end method

.method public b()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 898
    :cond_0
    :goto_0
    :try_start_0
    invoke-direct {p0}, Laaf;->f()Laah;

    move-result-object v0

    .line 899
    if-eqz v0, :cond_0

    .line 900
    iget v0, v0, Laah;->a:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 902
    :pswitch_0
    iget-object v0, p0, Laaf;->a:Laaj;

    invoke-virtual {v0}, Laaj;->a()I

    move-result v0

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Laaf;->a:Laaj;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Laaj;->a(I)V

    invoke-direct {p0}, Laaf;->g()Lacw;

    move-result-object v0

    invoke-virtual {v0}, Lacw;->d()V

    invoke-direct {p0}, Laaf;->h()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 923
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 905
    :pswitch_1
    :try_start_1
    iget-object v0, p0, Laaf;->a:Laaj;

    invoke-virtual {v0}, Laaj;->a()I

    move-result v0

    if-ne v0, v2, :cond_1

    invoke-direct {p0}, Laaf;->i()V

    invoke-static {}, Lacw;->e()V

    :cond_1
    iget-object v0, p0, Laaf;->a:Laaj;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Laaj;->a(I)V

    goto :goto_0

    .line 908
    :pswitch_2
    iget-object v0, p0, Laaf;->a:Laaj;

    invoke-virtual {v0}, Laaj;->a()I

    move-result v0

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Laaf;->n:Lzx;

    invoke-virtual {v0}, Lzx;->b()V

    goto :goto_0

    .line 911
    :pswitch_3
    iget-object v0, p0, Laaf;->a:Laaj;

    invoke-virtual {v0}, Laaj;->a()I

    move-result v0

    if-ne v0, v2, :cond_0

    invoke-direct {p0}, Laaf;->i()V

    invoke-static {}, Lacw;->e()V

    iget-object v0, p0, Laaf;->a:Laaj;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Laaj;->a(I)V

    goto :goto_0

    .line 914
    :pswitch_4
    iget-object v0, p0, Laaf;->a:Laaj;

    invoke-virtual {v0}, Laaj;->a()I

    move-result v0

    if-ne v0, v4, :cond_0

    iget-object v0, p0, Laaf;->a:Laaj;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Laaj;->a(I)V

    invoke-direct {p0}, Laaf;->g()Lacw;

    move-result-object v0

    invoke-virtual {v0}, Lacw;->d()V

    invoke-direct {p0}, Laaf;->h()V

    goto :goto_0

    .line 917
    :pswitch_5
    iget-object v0, p0, Laaf;->a:Laaj;

    invoke-virtual {v0}, Laaj;->a()I

    move-result v0

    if-ne v0, v2, :cond_0

    const/4 v0, 0x3

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Laaf;->a(IZ)V

    const/4 v0, 0x1

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Laaf;->a(IZ)V

    goto/16 :goto_0

    .line 920
    :pswitch_6
    iget-object v0, p0, Laaf;->a:Laaj;

    invoke-virtual {v0}, Laaj;->a()I

    move-result v0

    if-ne v0, v3, :cond_3

    iget-object v0, p0, Laaf;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laae;

    invoke-virtual {p0, v0}, Laaf;->a(Laae;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Laaf;->o:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    goto/16 :goto_0

    :cond_3
    const-string v0, "CameraStreamer"

    const-string v1, "Could not tear-down CameraStreamer as camera still seems to be running!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 900
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public c()V
    .locals 2

    .prologue
    .line 935
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Laaf;->a(IZ)V

    .line 936
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 953
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 954
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 955
    return-void
.end method

.method public e()Lzx;
    .locals 1

    .prologue
    .line 1124
    iget-object v0, p0, Laaf;->n:Lzx;

    return-object v0
.end method

.method public run()V
    .locals 0

    .prologue
    .line 931
    invoke-virtual {p0}, Laaf;->b()V

    .line 932
    return-void
.end method
