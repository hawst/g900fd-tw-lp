.class public final Lgqx;
.super Lghh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lghh",
        "<",
        "Lgqp;",
        ">;"
    }
.end annotation


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:Lgqv;

.field private final f:Lgqs;

.field private final g:Ljava/lang/Object;

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgqv;)V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p2, v0}, Lghh;-><init>(Landroid/content/Context;Lgfg;Lgfh;[Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lgqx;->d:Ljava/lang/String;

    invoke-static {p2}, Lgih;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgqv;

    iput-object v0, p0, Lgqx;->e:Lgqv;

    iget-object v0, p0, Lgqx;->e:Lgqv;

    invoke-virtual {v0, p0}, Lgqv;->a(Lgqx;)V

    new-instance v0, Lgqs;

    invoke-direct {v0}, Lgqs;-><init>()V

    iput-object v0, p0, Lgqx;->f:Lgqs;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lgqx;->g:Ljava/lang/Object;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgqx;->h:Z

    return-void
.end method

.method private b(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V
    .locals 1

    iget-object v0, p0, Lgqx;->f:Lgqs;

    invoke-virtual {v0, p1, p2}, Lgqs;->a(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V

    return-void
.end method

.method private b(Lcom/google/android/gms/internal/qu;Lpyo;)V
    .locals 1

    iget-object v0, p0, Lgqx;->f:Lgqs;

    invoke-virtual {v0, p1, p2}, Lgqs;->a(Lcom/google/android/gms/internal/qu;Lpyo;)V

    return-void
.end method

.method private l()V
    .locals 7

    iget-boolean v0, p0, Lgqx;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lgqx;->f:Lgqs;

    invoke-virtual {v0}, Lgqs;->e()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :try_start_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p0, Lgqx;->f:Lgqs;

    invoke-virtual {v1}, Lgqs;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgqt;

    iget-object v1, v0, Lgqt;->c:Lpyo;

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lgqx;->i()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lgqp;

    iget-object v5, p0, Lgqx;->d:Ljava/lang/String;

    iget-object v6, v0, Lgqt;->a:Lcom/google/android/gms/internal/qu;

    iget-object v0, v0, Lgqt;->c:Lpyo;

    invoke-static {v0}, Lgrf;->a(Lgrf;)[B

    move-result-object v0

    invoke-interface {v1, v5, v6, v0}, Lgqp;->a(Ljava/lang/String;Lcom/google/android/gms/internal/qu;[B)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "PlayLoggerImpl"

    const-string v1, "Couldn\'t send cached log events to AndroidLog service.  Retaining in memory cache."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_2
    return-void

    :cond_3
    :try_start_1
    iget-object v1, v0, Lgqt;->a:Lcom/google/android/gms/internal/qu;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/internal/qu;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v0, v0, Lgqt;->b:Lcom/google/android/gms/internal/qq;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {p0}, Lgqx;->i()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lgqp;

    iget-object v5, p0, Lgqx;->d:Ljava/lang/String;

    invoke-interface {v1, v5, v2, v3}, Lgqp;->a(Ljava/lang/String;Lcom/google/android/gms/internal/qu;Ljava/util/List;)V

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    :cond_5
    iget-object v1, v0, Lgqt;->a:Lcom/google/android/gms/internal/qu;

    iget-object v0, v0, Lgqt;->b:Lcom/google/android/gms/internal/qq;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v2, v1

    goto :goto_1

    :cond_6
    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p0}, Lgqx;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lgqp;

    iget-object v1, p0, Lgqx;->d:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lgqp;->a(Ljava/lang/String;Lcom/google/android/gms/internal/qu;Ljava/util/List;)V

    :cond_7
    iget-object v0, p0, Lgqx;->f:Lgqs;

    invoke-virtual {v0}, Lgqs;->b()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method


# virtual methods
.method protected synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, Lgqx;->b(Landroid/os/IBinder;)Lgqp;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V
    .locals 3

    iget-object v1, p0, Lgqx;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lgqx;->h:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lgqx;->b(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_0
    :try_start_1
    invoke-direct {p0}, Lgqx;->l()V

    invoke-virtual {p0}, Lgqx;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lgqp;

    iget-object v2, p0, Lgqx;->d:Ljava/lang/String;

    invoke-interface {v0, v2, p1, p2}, Lgqp;->a(Ljava/lang/String;Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "PlayLoggerImpl"

    const-string v2, "Couldn\'t send log event.  Will try caching."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lgqx;->b(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v0, "PlayLoggerImpl"

    const-string v2, "Service was disconnected.  Will try caching."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lgqx;->b(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/internal/qu;Lpyo;)V
    .locals 4

    iget-object v1, p0, Lgqx;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lgqx;->h:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2}, Lgqx;->b(Lcom/google/android/gms/internal/qu;Lpyo;)V

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_0
    :try_start_1
    invoke-direct {p0}, Lgqx;->l()V

    invoke-virtual {p0}, Lgqx;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lgqp;

    iget-object v2, p0, Lgqx;->d:Ljava/lang/String;

    invoke-static {p2}, Lgrf;->a(Lgrf;)[B

    move-result-object v3

    invoke-interface {v0, v2, p1, v3}, Lgqp;->a(Ljava/lang/String;Lcom/google/android/gms/internal/qu;[B)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "PlayLoggerImpl"

    const-string v2, "Couldn\'t send log event.  Will try caching."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lgqx;->b(Lcom/google/android/gms/internal/qu;Lpyo;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_1
    move-exception v0

    :try_start_3
    const-string v0, "PlayLoggerImpl"

    const-string v2, "Service was disconnected.  Will try caching."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, p1, p2}, Lgqx;->b(Lcom/google/android/gms/internal/qu;Lpyo;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method protected a(Lgic;Lghl;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const v1, 0x5e3530

    iget-object v2, p0, Lghh;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lgic;->f(Lghz;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method a(Z)V
    .locals 2

    iget-object v1, p0, Lgqx;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lgqx;->h:Z

    iput-boolean p1, p0, Lgqx;->h:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lgqx;->h:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lgqx;->l()V

    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected b(Landroid/os/IBinder;)Lgqp;
    .locals 1

    invoke-static {p1}, Lgqq;->a(Landroid/os/IBinder;)Lgqp;

    move-result-object v0

    return-object v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.playlog.service.START"

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.playlog.internal.IPlayLogService"

    return-object v0
.end method

.method public j()V
    .locals 3

    iget-object v1, p0, Lgqx;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lgqx;->g()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lgqx;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lgqx;->e:Lgqv;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lgqv;->a(Z)V

    invoke-virtual {p0}, Lgqx;->a()V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public k()V
    .locals 3

    iget-object v1, p0, Lgqx;->g:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lgqx;->e:Lgqv;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lgqv;->a(Z)V

    invoke-virtual {p0}, Lgqx;->b()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
