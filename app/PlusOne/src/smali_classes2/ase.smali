.class public final Lase;
.super Lamn;
.source "PG"


# instance fields
.field final a:J

.field final b:Lcdu;

.field final c:Lawn;

.field d:Z

.field e:Z

.field f:Z

.field g:J

.field h:J

.field j:I

.field k:J

.field l:J

.field m:J

.field n:J

.field final o:Lasn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lasn",
            "<",
            "Lasl;",
            ">;"
        }
    .end annotation
.end field

.field private final p:J

.field private final q:J

.field private final r:Lasm;

.field private final s:Lawk;

.field private final t:Lawk;

.field private u:J

.field private v:I

.field private final w:Lbtd;

.field private final x:Lasn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lasn",
            "<",
            "Lasi;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lawn;Lbtd;Lcdu;)V
    .locals 10

    .prologue
    const-wide/32 v4, 0x4c4b40

    .line 186
    const-wide/32 v8, 0x1c9c380

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v6, v4

    invoke-direct/range {v0 .. v9}, Lase;-><init>(Lawn;Lbtd;Lcdu;JJJ)V

    .line 188
    return-void
.end method

.method constructor <init>(Lawn;Lbtd;Lcdu;JJJ)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 205
    invoke-direct {p0}, Lamn;-><init>()V

    .line 141
    new-instance v0, Lasj;

    invoke-direct {v0, p0}, Lasj;-><init>(Lase;)V

    iput-object v0, p0, Lase;->s:Lawk;

    .line 142
    new-instance v0, Lask;

    invoke-direct {v0, p0}, Lask;-><init>(Lase;)V

    iput-object v0, p0, Lase;->t:Lawk;

    .line 162
    new-instance v0, Lasf;

    const-class v1, Lasl;

    invoke-direct {v0, p0, v1}, Lasf;-><init>(Lase;Ljava/lang/Class;)V

    iput-object v0, p0, Lase;->o:Lasn;

    .line 174
    new-instance v0, Lasg;

    const-class v1, Lasi;

    invoke-direct {v0, p0, v1}, Lasg;-><init>(Lase;Ljava/lang/Class;)V

    iput-object v0, p0, Lase;->x:Lasn;

    .line 206
    const-string v0, "state"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawn;

    iput-object v0, p0, Lase;->c:Lawn;

    .line 207
    const-string v0, "playerLimits"

    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbtd;

    iput-object v0, p0, Lase;->w:Lbtd;

    .line 208
    const-string v0, "analyticsSession"

    invoke-static {p3, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    iput-object v0, p0, Lase;->b:Lcdu;

    .line 209
    const-string v0, "minDurationUs"

    invoke-static {p4, p5, v0}, Lcec;->b(JLjava/lang/CharSequence;)J

    move-result-wide v0

    iput-wide v0, p0, Lase;->a:J

    .line 210
    const-string v0, "durationIncrementUs"

    invoke-static {p6, p7, v0}, Lcec;->a(JLjava/lang/CharSequence;)J

    move-result-wide v0

    iput-wide v0, p0, Lase;->p:J

    .line 211
    const-string v0, "defaultToShortDurationThresholdUs"

    invoke-static {p8, p9, v0}, Lcec;->a(JLjava/lang/CharSequence;)J

    move-result-wide v0

    iput-wide v0, p0, Lase;->q:J

    .line 214
    new-instance v0, Lash;

    invoke-direct {v0, p0}, Lash;-><init>(Lase;)V

    iput-object v0, p0, Lase;->r:Lasm;

    .line 262
    return-void
.end method

.method static synthetic a(Lase;Lasl;J)V
    .locals 0

    .prologue
    .line 15
    invoke-virtual {p0}, Lase;->e()V

    invoke-virtual {p0, p2, p3}, Lase;->a(J)V

    invoke-direct {p0, p1, p2, p3}, Lase;->b(Lasl;J)V

    invoke-virtual {p0}, Lase;->e()V

    invoke-interface {p1, p2, p3}, Lasl;->a(J)V

    invoke-virtual {p0, p1, p2, p3}, Lase;->a(Lasl;J)V

    return-void
.end method

.method private b(Lasl;J)V
    .locals 6

    .prologue
    .line 395
    invoke-virtual {p0}, Lase;->e()V

    .line 396
    iget v0, p0, Lase;->j:I

    iget-wide v2, p0, Lase;->a:J

    iget-wide v4, p0, Lase;->k:J

    sub-long v2, p2, v2

    long-to-double v2, v2

    long-to-double v4, v4

    div-double/2addr v2, v4

    int-to-double v0, v0

    mul-double/2addr v0, v2

    double-to-int v0, v0

    .line 398
    invoke-interface {p1, v0}, Lasl;->b(I)V

    .line 399
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 266
    invoke-super {p0}, Lamn;->a()V

    .line 267
    iget-object v0, p0, Lase;->c:Lawn;

    iget-object v1, p0, Lase;->s:Lawk;

    invoke-interface {v0, v1}, Lawn;->x(Lawk;)V

    .line 268
    iget-object v0, p0, Lase;->c:Lawn;

    iget-object v1, p0, Lase;->t:Lawk;

    invoke-interface {v0, v1}, Lawn;->J(Lawk;)V

    .line 269
    iget-object v0, p0, Lase;->w:Lbtd;

    invoke-interface {v0}, Lbtd;->A()I

    move-result v0

    iput v0, p0, Lase;->v:I

    .line 270
    iget-object v0, p0, Lase;->o:Lasn;

    invoke-virtual {v0}, Lasn;->a()V

    .line 271
    iget-object v0, p0, Lase;->x:Lasn;

    invoke-virtual {v0}, Lasn;->a()V

    .line 272
    invoke-virtual {p0}, Lase;->c()V

    .line 273
    return-void
.end method

.method a(J)V
    .locals 3

    .prologue
    .line 416
    invoke-virtual {p0}, Lase;->e()V

    .line 417
    iget-wide v0, p0, Lase;->g:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    .line 418
    const/4 v0, 0x1

    iput-boolean v0, p0, Lase;->d:Z

    .line 419
    iput-wide p1, p0, Lase;->g:J

    .line 421
    :cond_0
    return-void
.end method

.method public a(Lasi;)V
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lase;->x:Lasn;

    invoke-virtual {v0, p1}, Lasn;->c(Ljava/lang/Object;)V

    .line 294
    return-void
.end method

.method public a(Lasl;)V
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lase;->o:Lasn;

    invoke-virtual {v0, p1}, Lasn;->c(Ljava/lang/Object;)V

    .line 286
    return-void
.end method

.method a(Lasl;J)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 388
    invoke-virtual {p0}, Lase;->e()V

    .line 389
    iget-wide v4, p0, Lase;->l:J

    cmp-long v0, p2, v4

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-interface {p1, v0}, Lasl;->e_(Z)V

    .line 390
    iget-wide v4, p0, Lase;->m:J

    cmp-long v0, p2, v4

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    invoke-interface {p1, v0}, Lasl;->f_(Z)V

    .line 391
    iget-wide v4, p0, Lase;->n:J

    cmp-long v0, p2, v4

    if-nez v0, :cond_2

    :goto_2
    invoke-interface {p1, v1}, Lasl;->c(Z)V

    .line 392
    return-void

    :cond_0
    move v0, v2

    .line 389
    goto :goto_0

    :cond_1
    move v0, v2

    .line 390
    goto :goto_1

    :cond_2
    move v1, v2

    .line 391
    goto :goto_2
.end method

.method public b()V
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, Lase;->c:Lawn;

    iget-object v1, p0, Lase;->s:Lawk;

    invoke-interface {v0, v1}, Lawn;->y(Lawk;)V

    .line 278
    iget-object v0, p0, Lase;->c:Lawn;

    iget-object v1, p0, Lase;->t:Lawk;

    invoke-interface {v0, v1}, Lawn;->K(Lawk;)V

    .line 279
    iget-object v0, p0, Lase;->x:Lasn;

    invoke-virtual {v0}, Lasn;->b()V

    .line 280
    iget-object v0, p0, Lase;->o:Lasn;

    invoke-virtual {v0}, Lasn;->b()V

    .line 281
    invoke-super {p0}, Lamn;->b()V

    .line 282
    return-void
.end method

.method public b(Lasi;)V
    .locals 1

    .prologue
    .line 297
    iget-object v0, p0, Lase;->x:Lasn;

    invoke-virtual {v0, p1}, Lasn;->d(Ljava/lang/Object;)V

    .line 298
    return-void
.end method

.method public b(Lasl;)V
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lase;->o:Lasn;

    invoke-virtual {v0, p1}, Lasn;->d(Ljava/lang/Object;)V

    .line 290
    return-void
.end method

.method c()V
    .locals 12

    .prologue
    const-wide/16 v10, 0x3

    const-wide/16 v8, -0x1

    .line 301
    invoke-virtual {p0}, Lase;->e()V

    .line 302
    iget-wide v0, p0, Lase;->a:J

    invoke-virtual {p0}, Lase;->d()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iget-wide v2, p0, Lase;->a:J

    sub-long v2, v0, v2

    iput-wide v2, p0, Lase;->k:J

    const/16 v2, 0x3e8

    iput v2, p0, Lase;->j:I

    iget-wide v2, p0, Lase;->a:J

    iget-wide v4, p0, Lase;->p:J

    div-long v4, v0, v4

    iget-wide v6, p0, Lase;->p:J

    mul-long/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lase;->n:J

    iget-wide v2, p0, Lase;->a:J

    iget-wide v4, p0, Lase;->n:J

    div-long/2addr v4, v10

    iget-wide v6, p0, Lase;->p:J

    div-long/2addr v4, v6

    iget-wide v6, p0, Lase;->p:J

    mul-long/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lase;->m:J

    iget-wide v2, p0, Lase;->a:J

    iget-wide v4, p0, Lase;->m:J

    div-long/2addr v4, v10

    iget-wide v6, p0, Lase;->p:J

    div-long/2addr v4, v6

    iget-wide v6, p0, Lase;->p:J

    mul-long/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iput-wide v2, p0, Lase;->l:J

    iget-wide v2, p0, Lase;->a:J

    long-to-double v0, v0

    const-wide v4, 0x3feb333333333333L    # 0.85

    mul-double/2addr v0, v4

    double-to-long v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lase;->u:J

    .line 303
    iget-object v0, p0, Lase;->c:Lawn;

    invoke-interface {v0}, Lawn;->Y()J

    move-result-wide v0

    cmp-long v0, v0, v8

    if-eqz v0, :cond_0

    iget-object v0, p0, Lase;->c:Lawn;

    .line 304
    invoke-interface {v0}, Lawn;->X()J

    move-result-wide v0

    cmp-long v0, v0, v8

    if-nez v0, :cond_0

    .line 305
    iget-object v0, p0, Lase;->c:Lawn;

    invoke-interface {v0}, Lawn;->aV()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lase;->u:J

    :goto_0
    iput-wide v0, p0, Lase;->g:J

    iget-object v0, p0, Lase;->c:Lawn;

    iget-wide v2, p0, Lase;->g:J

    invoke-interface {v0, v2, v3}, Lawn;->c(J)V

    .line 307
    :cond_0
    iget-object v0, p0, Lase;->o:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lasl;

    invoke-virtual {p0, v0}, Lase;->c(Lasl;)V

    .line 308
    iget-object v0, p0, Lase;->x:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lasi;

    invoke-virtual {p0, v0}, Lase;->c(Lasi;)V

    .line 309
    return-void

    .line 305
    :cond_1
    iget-wide v0, p0, Lase;->l:J

    iget-wide v2, p0, Lase;->q:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    iget-wide v0, p0, Lase;->l:J

    goto :goto_0

    :cond_2
    iget-wide v0, p0, Lase;->m:J

    goto :goto_0
.end method

.method c(Lasi;)V
    .locals 4

    .prologue
    .line 403
    iget-object v0, p0, Lase;->c:Lawn;

    invoke-interface {v0}, Lawn;->bd()Lawg;

    move-result-object v0

    sget-object v1, Lawg;->c:Lawg;

    if-eq v0, v1, :cond_1

    .line 405
    iget-wide v0, p0, Lase;->l:J

    iget-wide v2, p0, Lase;->m:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Lase;->m:J

    iget-wide v2, p0, Lase;->n:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 412
    :goto_0
    invoke-interface {p1, v0}, Lasi;->o(Z)V

    .line 413
    return-void

    .line 405
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c(Lasl;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 353
    invoke-virtual {p0}, Lase;->e()V

    .line 354
    iget-object v0, p0, Lase;->r:Lasm;

    invoke-interface {p1, v0}, Lasl;->a(Lasm;)V

    .line 357
    iget-object v0, p0, Lase;->c:Lawn;

    invoke-interface {v0}, Lawn;->X()J

    move-result-wide v4

    iput-wide v4, p0, Lase;->h:J

    .line 358
    iget v0, p0, Lase;->j:I

    invoke-interface {p1, v0}, Lasl;->f_(I)V

    .line 360
    iget-wide v4, p0, Lase;->h:J

    iput-wide v4, p0, Lase;->g:J

    .line 361
    iget-wide v4, p0, Lase;->g:J

    invoke-direct {p0, p1, v4, v5}, Lase;->b(Lasl;J)V

    .line 362
    iget-wide v4, p0, Lase;->g:J

    invoke-interface {p1, v4, v5}, Lasl;->a(J)V

    .line 364
    iget-wide v4, p0, Lase;->l:J

    iget-wide v6, p0, Lase;->m:J

    cmp-long v0, v4, v6

    if-eqz v0, :cond_2

    move v0, v1

    .line 365
    :goto_0
    iget-wide v4, p0, Lase;->m:J

    iget-wide v6, p0, Lase;->n:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    move v3, v1

    .line 368
    :goto_1
    if-nez v0, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    move v2, v1

    .line 371
    :cond_1
    invoke-interface {p1, v2}, Lasl;->g_(Z)V

    .line 372
    invoke-interface {p1, v0}, Lasl;->h_(Z)V

    .line 373
    invoke-interface {p1, v3}, Lasl;->f(Z)V

    .line 374
    iget-wide v0, p0, Lase;->g:J

    invoke-virtual {p0, p1, v0, v1}, Lase;->a(Lasl;J)V

    .line 375
    return-void

    :cond_2
    move v0, v2

    .line 364
    goto :goto_0

    :cond_3
    move v3, v2

    .line 365
    goto :goto_1
.end method

.method d()J
    .locals 4

    .prologue
    .line 333
    iget-object v0, p0, Lase;->c:Lawn;

    invoke-interface {v0}, Lawn;->Y()J

    move-result-wide v0

    iget v2, p0, Lase;->v:I

    mul-int/lit16 v2, v2, 0x3e8

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method
