.class public abstract Lhck;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    return-void
.end method


# virtual methods
.method protected abstract a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lhcl;
.end method

.method public a(Lhdc;)Lhcm;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 26
    if-nez p1, :cond_0

    move v1, v0

    .line 27
    :goto_0
    if-gtz v1, :cond_1

    .line 28
    sget-object v0, Lhcm;->a:Lhcm;

    .line 37
    :goto_1
    return-object v0

    .line 26
    :cond_0
    invoke-interface {p1}, Lhdc;->c()I

    move-result v1

    goto :goto_0

    .line 31
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 32
    :goto_2
    if-ge v0, v1, :cond_2

    .line 34
    invoke-interface {p1, v0}, Lhdc;->b(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {p1}, Lhdc;->b()Ljava/lang/CharSequence;

    move-result-object v4

    .line 33
    invoke-virtual {p0, v3, v4}, Lhck;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lhcl;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 32
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 37
    :cond_2
    invoke-virtual {p0, v2}, Lhck;->a(Ljava/util/List;)Lhcm;

    move-result-object v0

    goto :goto_1
.end method

.method protected abstract a(Ljava/util/List;)Lhcm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lhcl;",
            ">;)",
            "Lhcm;"
        }
    .end annotation
.end method

.method public abstract a(Landroid/graphics/Canvas;Lhcm;II)V
.end method
