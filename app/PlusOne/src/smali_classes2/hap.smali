.class public final Lhap;
.super Landroid/opengl/GLSurfaceView;
.source "PG"

# interfaces
.implements Lhbj;
.implements Lhbn;
.implements Lhbp;


# instance fields
.field private a:Lhax;

.field private b:Z

.field private c:Z

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lhbl;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lhbl;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

.field private g:Lhbf;

.field private h:Z

.field private i:Z

.field private j:I

.field private k:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/16 v1, 0x8

    .line 75
    invoke-direct {p0, p1}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;)V

    .line 58
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lhap;->d:Ljava/util/Set;

    .line 60
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lhap;->e:Ljava/util/Set;

    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhap;->h:Z

    .line 72
    const/high16 v0, -0x1000000

    iput v0, p0, Lhap;->j:I

    .line 188
    new-instance v0, Lhaq;

    invoke-direct {v0, p0}, Lhaq;-><init>(Lhap;)V

    iput-object v0, p0, Lhap;->k:Ljava/lang/Runnable;

    .line 77
    invoke-virtual {p0}, Lhap;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 78
    if-nez v0, :cond_0

    .line 79
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to get the surface holder"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_0
    const/4 v2, 0x3

    invoke-interface {v0, v2}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 83
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lhap;->setEGLContextClientVersion(I)V

    move-object v0, p0

    move v2, v1

    move v3, v1

    move v4, v1

    move v6, v5

    .line 87
    invoke-virtual/range {v0 .. v6}, Lhap;->setEGLConfigChooser(IIIIII)V

    .line 89
    new-instance v0, Lhax;

    invoke-direct {v0, p0}, Lhax;-><init>(Lhap;)V

    iput-object v0, p0, Lhap;->a:Lhax;

    .line 90
    iget-object v0, p0, Lhap;->a:Lhax;

    invoke-virtual {p0, v0}, Lhap;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 92
    invoke-virtual {p0, v5}, Lhap;->setRenderMode(I)V

    .line 93
    return-void
.end method

.method static synthetic a(Lhap;Lhbf;)Lhbf;
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lhap;->g:Lhbf;

    return-object p1
.end method

.method static synthetic a(Lhap;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lhap;->e()V

    return-void
.end method

.method static synthetic a(Lhap;ZI)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lhap;->a(ZI)V

    return-void
.end method

.method private declared-synchronized a(ZI)V
    .locals 4

    .prologue
    .line 210
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lhap;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 211
    iget-object v0, p0, Lhap;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbl;

    .line 212
    invoke-direct {p0, v0}, Lhap;->b(Lhbl;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 210
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 215
    :cond_0
    :try_start_1
    iget-object v0, p0, Lhap;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 218
    :cond_1
    iget-object v0, p0, Lhap;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 219
    iget-object v0, p0, Lhap;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhbl;

    .line 220
    invoke-direct {p0, v0}, Lhap;->b(Lhbl;)V

    goto :goto_1

    .line 224
    :cond_2
    if-nez p1, :cond_3

    invoke-direct {p0, p2}, Lhap;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 225
    iget-object v0, p0, Lhap;->k:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lhap;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 226
    iget-object v0, p0, Lhap;->k:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {p0, v0, v2, v3}, Lhap;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 229
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhap;->b:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 230
    monitor-exit p0

    return-void
.end method

.method static synthetic a(Lhap;Z)Z
    .locals 0

    .prologue
    .line 51
    iput-boolean p1, p0, Lhap;->i:Z

    return p1
.end method

.method static synthetic b(Lhap;)Lhax;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lhap;->a:Lhax;

    return-object v0
.end method

.method private b(Lhbl;)V
    .locals 1

    .prologue
    .line 236
    new-instance v0, Lhar;

    invoke-direct {v0, p1}, Lhar;-><init>(Lhbl;)V

    invoke-virtual {p0, v0}, Lhap;->post(Ljava/lang/Runnable;)Z

    .line 242
    return-void
.end method

.method private b(I)Z
    .locals 1

    .prologue
    .line 206
    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    const/4 v0, 0x7

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lhap;Z)Z
    .locals 0

    .prologue
    .line 51
    iput-boolean p1, p0, Lhap;->h:Z

    return p1
.end method

.method static synthetic c(Lhap;)Lhbf;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lhap;->g:Lhbf;

    return-object v0
.end method

.method static synthetic c(Lhap;Z)Z
    .locals 0

    .prologue
    .line 51
    iput-boolean p1, p0, Lhap;->c:Z

    return p1
.end method

.method static synthetic d(Lhap;)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lhap;->f:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    return-object v0
.end method

.method static synthetic d()Lhbo;
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return-object v0
.end method

.method private declared-synchronized e()V
    .locals 1

    .prologue
    .line 197
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lhap;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 198
    monitor-exit p0

    return-void

    .line 197
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic e(Lhap;)Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lhap;->c:Z

    return v0
.end method

.method static synthetic f(Lhap;)Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lhap;->i:Z

    return v0
.end method

.method static synthetic g(Lhap;)I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lhap;->j:I

    return v0
.end method

.method static synthetic h(Lhap;)Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lhap;->h:Z

    return v0
.end method


# virtual methods
.method public a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lhap;->f:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 147
    return-void
.end method

.method public a(Lhbi;)V
    .locals 2

    .prologue
    .line 151
    check-cast p1, Lhbf;

    iput-object p1, p0, Lhap;->g:Lhbf;

    .line 152
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a:Lcom/google/android/libraries/photoeditor/core/NativeCore;

    iget-object v1, p0, Lhap;->g:Lhbf;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a(Lhbf;)V

    .line 153
    return-void
.end method

.method public a(Lhbl;)V
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lhap;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 309
    iget-object v0, p0, Lhap;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 310
    return-void
.end method

.method public a(Lhbl;Z)V
    .locals 1

    .prologue
    .line 297
    if-eqz p2, :cond_0

    .line 298
    iget-object v0, p0, Lhap;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 302
    :goto_0
    return-void

    .line 300
    :cond_0
    iget-object v0, p0, Lhap;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, Lhap;->c:Z

    if-eqz v0, :cond_0

    .line 172
    const/4 v0, 0x0

    .line 177
    :goto_0
    return v0

    .line 175
    :cond_0
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->requestRender()V

    .line 177
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public declared-synchronized a(I)Z
    .locals 1

    .prologue
    .line 201
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lhap;->b:Z

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a:Lcom/google/android/libraries/photoeditor/core/NativeCore;

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->getCompare()Z

    move-result v0

    if-nez v0, :cond_0

    .line 202
    invoke-direct {p0, p1}, Lhap;->b(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 201
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lhbi;IILcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;ILhbk;)Z
    .locals 8

    .prologue
    .line 134
    if-nez p6, :cond_0

    .line 135
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 138
    :cond_0
    new-instance v0, Lhbe;

    .line 139
    invoke-virtual {p4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->e()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v5

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lhbe;-><init>(Lhap;Lhbi;IILcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;ILhbk;)V

    .line 138
    invoke-virtual {p0, v0}, Lhap;->queueEvent(Ljava/lang/Runnable;)V

    .line 141
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lhbi;Landroid/graphics/Rect;Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Lhbm;)Z
    .locals 6

    .prologue
    .line 115
    if-nez p4, :cond_0

    .line 116
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :cond_0
    new-instance v0, Lhat;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lhat;-><init>(Lhap;Lhbi;Landroid/graphics/Rect;Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Lhbm;)V

    invoke-virtual {p0, v0}, Lhap;->queueEvent(Ljava/lang/Runnable;)V

    .line 121
    const/4 v0, 0x1

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 280
    iget-boolean v0, p0, Lhap;->c:Z

    if-eqz v0, :cond_0

    .line 289
    :goto_0
    return-void

    .line 284
    :cond_0
    new-instance v0, Lhas;

    invoke-direct {v0, p0}, Lhas;-><init>(Lhap;)V

    invoke-virtual {p0, v0}, Lhap;->queueEvent(Ljava/lang/Runnable;)V

    .line 285
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhap;->c:Z

    .line 288
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->onPause()V

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Lhap;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 317
    iget-object v0, p0, Lhap;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 318
    return-void
.end method

.method public queueEvent(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 270
    iget-boolean v0, p0, Lhap;->c:Z

    if-nez v0, :cond_0

    .line 271
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->queueEvent(Ljava/lang/Runnable;)V

    .line 273
    :cond_0
    return-void
.end method

.method public final setBackgroundColor(I)V
    .locals 1

    .prologue
    .line 182
    iput p1, p0, Lhap;->j:I

    .line 183
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhap;->i:Z

    .line 185
    invoke-virtual {p0}, Lhap;->requestRender()V

    .line 186
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1

    .prologue
    .line 246
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->surfaceCreated(Landroid/view/SurfaceHolder;)V

    .line 247
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a:Lcom/google/android/libraries/photoeditor/core/NativeCore;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a(Lhbn;)V

    .line 248
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 252
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a:Lcom/google/android/libraries/photoeditor/core/NativeCore;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a(Lhbn;)V

    .line 253
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a:Lcom/google/android/libraries/photoeditor/core/NativeCore;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a(Lhbf;)V

    .line 255
    invoke-virtual {p0}, Lhap;->c()V

    .line 257
    iput-object v1, p0, Lhap;->g:Lhbf;

    .line 258
    iput-object v1, p0, Lhap;->f:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 260
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->surfaceDestroyed(Landroid/view/SurfaceHolder;)V

    .line 266
    return-void
.end method
