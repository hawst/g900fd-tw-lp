.class public final Lbxh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:J

.field private b:F

.field private c:F

.field private d:F


# direct methods
.method private constructor <init>(JFFF)V
    .locals 3

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-string v0, "timestampUs"

    invoke-static {p1, p2, v0}, Lcec;->b(JLjava/lang/CharSequence;)J

    move-result-wide v0

    iput-wide v0, p0, Lbxh;->a:J

    .line 36
    iput p3, p0, Lbxh;->b:F

    .line 37
    iput p4, p0, Lbxh;->c:F

    .line 38
    const-string v0, "zoom"

    invoke-static {p5, v0}, Lcec;->a(FLjava/lang/CharSequence;)F

    move-result v0

    iput v0, p0, Lbxh;->d:F

    .line 39
    return-void
.end method

.method public static a(JFFF)Lbxh;
    .locals 8

    .prologue
    .line 23
    new-instance v1, Lbxh;

    move-wide v2, p0

    move v4, p2

    move v5, p3

    move v6, p4

    invoke-direct/range {v1 .. v6}, Lbxh;-><init>(JFFF)V

    return-object v1
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 43
    if-ne p0, p1, :cond_1

    .line 51
    :cond_0
    :goto_0
    return v0

    .line 46
    :cond_1
    instance-of v2, p1, Lbxh;

    if-nez v2, :cond_2

    move v0, v1

    .line 47
    goto :goto_0

    .line 50
    :cond_2
    check-cast p1, Lbxh;

    .line 51
    iget-wide v2, p0, Lbxh;->a:J

    iget-wide v4, p1, Lbxh;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget v2, p0, Lbxh;->b:F

    iget v3, p1, Lbxh;->b:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lbxh;->c:F

    iget v3, p1, Lbxh;->c:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lbxh;->d:F

    iget v3, p1, Lbxh;->d:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 6

    .prologue
    .line 57
    iget-wide v0, p0, Lbxh;->a:J

    iget v2, p0, Lbxh;->b:F

    iget v3, p0, Lbxh;->c:F

    iget v4, p0, Lbxh;->d:F

    const/16 v5, 0x11

    .line 60
    invoke-static {v4, v5}, Lbqh;->a(FI)I

    move-result v4

    .line 59
    invoke-static {v3, v4}, Lbqh;->a(FI)I

    move-result v3

    .line 58
    invoke-static {v2, v3}, Lbqh;->a(FI)I

    move-result v2

    .line 57
    invoke-static {v0, v1, v2}, Lbqh;->a(JI)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 65
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lbxh;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lbxh;->b:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lbxh;->c:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lbxh;->d:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
