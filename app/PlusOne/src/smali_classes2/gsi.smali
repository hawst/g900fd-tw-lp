.class public final Lgsi;
.super Ljava/lang/Object;


# instance fields
.field public final a:Landroid/accounts/Account;

.field public final b:Ljava/lang/String;

.field public final c:J

.field public d:J

.field public e:J


# direct methods
.method public constructor <init>(Landroid/accounts/Account;Ljava/lang/String;J)V
    .locals 3

    const-wide v0, 0x7fffffffffffffffL

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lgsi;->d:J

    iput-wide v0, p0, Lgsi;->e:J

    const-string v0, "account"

    invoke-static {p1, v0}, Lgih;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lgsi;->a:Landroid/accounts/Account;

    const-string v0, "reason"

    invoke-static {p2, v0}, Lgih;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lgsi;->b:Ljava/lang/String;

    iput-wide p3, p0, Lgsi;->c:J

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/gms/location/reporting/UploadRequest;
    .locals 1

    new-instance v0, Lcom/google/android/gms/location/reporting/UploadRequest;

    invoke-direct {v0, p0}, Lcom/google/android/gms/location/reporting/UploadRequest;-><init>(Lgsi;)V

    return-object v0
.end method

.method public a(J)Lgsi;
    .locals 1

    iput-wide p1, p0, Lgsi;->d:J

    return-object p0
.end method

.method public b(J)Lgsi;
    .locals 1

    iput-wide p1, p0, Lgsi;->e:J

    return-object p0
.end method

.method public c(J)Lgsi;
    .locals 1

    invoke-virtual {p0, p1, p2}, Lgsi;->a(J)Lgsi;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lgsi;->b(J)Lgsi;

    move-result-object v0

    return-object v0
.end method
