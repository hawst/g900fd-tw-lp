.class final Lcdl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcau;


# instance fields
.field private final a:Lcau;

.field private final b:[I

.field private c:I

.field private d:J

.field private synthetic e:Lcdj;


# direct methods
.method public constructor <init>(Lcdj;Lcau;)V
    .locals 2

    .prologue
    .line 292
    iput-object p1, p0, Lcdl;->e:Lcdj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 290
    const-wide v0, -0x3333333400000000L    # -9.255959211743199E61

    iput-wide v0, p0, Lcdl;->d:J

    .line 293
    const-string v0, "inner"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcau;

    iput-object v0, p0, Lcdl;->a:Lcau;

    .line 294
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcdl;->b:[I

    .line 295
    return-void
.end method

.method private a(I)I
    .locals 1

    .prologue
    .line 455
    iget v0, p0, Lcdl;->c:I

    invoke-static {p1, v0}, Lcdj;->a(II)I

    move-result v0

    return v0
.end method

.method private a(JJZ[I)Z
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 440
    iget-object v2, p0, Lcdl;->a:Lcau;

    invoke-interface {v2, p1, p2}, Lcau;->a(J)I

    move-result v2

    .line 441
    iget-object v3, p0, Lcdl;->a:Lcau;

    invoke-interface {v3, p3, p4}, Lcau;->a(J)I

    move-result v3

    .line 443
    if-eq v2, v4, :cond_1

    if-eq v3, v4, :cond_1

    .line 445
    aput v2, p6, v0

    .line 446
    if-eqz p5, :cond_0

    move v0, v1

    :cond_0
    add-int/2addr v0, v3

    invoke-static {v0, v2}, Lcdj;->a(II)I

    move-result v0

    aput v0, p6, v1

    .line 451
    :goto_0
    return v1

    :cond_1
    move v1, v0

    goto :goto_0
.end method


# virtual methods
.method public a(J)I
    .locals 3

    .prologue
    .line 420
    iget-wide v0, p0, Lcdl;->d:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    iget v0, p0, Lcdl;->c:I

    .line 422
    :goto_0
    return v0

    .line 420
    :cond_0
    iget-object v0, p0, Lcdl;->a:Lcau;

    .line 422
    invoke-interface {v0, p1, p2}, Lcau;->a(J)I

    move-result v0

    iget v1, p0, Lcdl;->c:I

    invoke-static {v0, v1}, Lcdj;->b(II)I

    move-result v0

    goto :goto_0
.end method

.method public a()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcdl;->a:Lcau;

    return-object v0
.end method

.method public a(JZ)V
    .locals 5

    .prologue
    .line 308
    iget-object v0, p0, Lcdl;->a:Lcau;

    invoke-interface {v0, p1, p2}, Lcau;->a(J)I

    move-result v1

    if-eqz p3, :cond_1

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    .line 309
    iget v1, p0, Lcdl;->c:I

    if-eq v1, v0, :cond_0

    .line 310
    iput v0, p0, Lcdl;->c:I

    .line 311
    iget-wide v0, p0, Lcdl;->d:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcdl;->d:J

    .line 313
    :cond_0
    return-void

    .line 308
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(JJZ)Z
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v8, 0x0

    .line 331
    iget-object v7, p0, Lcdl;->b:[I

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move v6, p5

    invoke-direct/range {v1 .. v7}, Lcdl;->a(JJZ[I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcdl;->e:Lcdj;

    .line 332
    invoke-static {v1}, Lcdj;->a(Lcdj;)Lcdk;

    move-result-object v1

    iget-object v2, p0, Lcdl;->b:[I

    aget v2, v2, v8

    iget-object v3, p0, Lcdl;->b:[I

    aget v3, v3, v0

    invoke-interface {v1, v2, v3}, Lcdk;->b(II)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v8

    goto :goto_0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 319
    iget-wide v0, p0, Lcdl;->d:J

    return-wide v0
.end method

.method public b(JJZ)V
    .locals 11

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 343
    iget-object v7, p0, Lcdl;->b:[I

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move/from16 v6, p5

    invoke-direct/range {v1 .. v7}, Lcdl;->a(JJZ[I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 350
    :cond_0
    :goto_0
    return-void

    .line 347
    :cond_1
    iget-object v0, p0, Lcdl;->e:Lcdj;

    invoke-static {v0}, Lcdj;->a(Lcdj;)Lcdk;

    move-result-object v0

    iget-object v1, p0, Lcdl;->b:[I

    aget v1, v1, v8

    iget-object v2, p0, Lcdl;->b:[I

    aget v2, v2, v9

    invoke-interface {v0, v1, v2}, Lcdk;->b(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcdl;->e:Lcdj;

    invoke-static {v0}, Lcdj;->a(Lcdj;)Lcdk;

    move-result-object v0

    iget-object v1, p0, Lcdl;->b:[I

    aget v1, v1, v8

    iget-object v2, p0, Lcdl;->b:[I

    aget v2, v2, v9

    invoke-interface {v0, v1, v2}, Lcdk;->a(II)V

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lcdl;->a:Lcau;

    invoke-interface {v0}, Lcau;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 371
    iget v0, p0, Lcdl;->c:I

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    .line 373
    :goto_0
    return-object v0

    .line 371
    :cond_0
    iget-object v0, p0, Lcdl;->a:Lcau;

    .line 373
    invoke-direct {p0, p1}, Lcdl;->a(I)I

    move-result v1

    invoke-interface {v0, v1}, Lcau;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 378
    iget v0, p0, Lcdl;->c:I

    if-ne p1, v0, :cond_0

    iget-wide v0, p0, Lcdl;->d:J

    .line 380
    :goto_0
    return-wide v0

    .line 378
    :cond_0
    iget-object v0, p0, Lcdl;->a:Lcau;

    .line 380
    invoke-direct {p0, p1}, Lcdl;->a(I)I

    move-result v1

    invoke-interface {v0, v1}, Lcau;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 2

    .prologue
    .line 401
    iget v0, p0, Lcdl;->c:I

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcdl;->a:Lcau;

    .line 402
    invoke-interface {v0}, Lcau;->getViewTypeCount()I

    move-result v0

    .line 403
    :goto_0
    return v0

    .line 402
    :cond_0
    iget-object v0, p0, Lcdl;->a:Lcau;

    .line 403
    invoke-direct {p0, p1}, Lcdl;->a(I)I

    move-result v1

    invoke-interface {v0, v1}, Lcau;->getItemViewType(I)I

    move-result v0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 392
    iget-object v0, p0, Lcdl;->e:Lcdj;

    invoke-static {v0}, Lcdj;->b(Lcdj;)[Landroid/view/View;

    move-result-object v0

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcdl;->e:Lcdj;

    .line 393
    invoke-static {v0}, Lcdj;->b(Lcdj;)[Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x1

    aget-object v0, v0, v1

    .line 394
    :goto_0
    iget v1, p0, Lcdl;->c:I

    if-ne p1, v1, :cond_1

    .line 396
    :goto_1
    return-object v0

    .line 393
    :cond_0
    iget-object v0, p0, Lcdl;->e:Lcdj;

    invoke-static {v0}, Lcdj;->b(Lcdj;)[Landroid/view/View;

    move-result-object v0

    aget-object v0, v0, v1

    goto :goto_0

    .line 394
    :cond_1
    iget-object v0, p0, Lcdl;->a:Lcau;

    .line 396
    invoke-direct {p0, p1}, Lcdl;->a(I)I

    move-result v1

    invoke-interface {v0, v1, p2, p3}, Lcau;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lcdl;->a:Lcau;

    invoke-interface {v0}, Lcau;->getViewTypeCount()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lcdl;->a:Lcau;

    invoke-interface {v0}, Lcau;->hasStableIds()Z

    move-result v0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcdl;->a:Lcau;

    invoke-interface {v0}, Lcau;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 356
    iget-object v0, p0, Lcdl;->a:Lcau;

    invoke-interface {v0, p1}, Lcau;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 357
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Lcdl;->a:Lcau;

    invoke-interface {v0, p1}, Lcau;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 362
    return-void
.end method
