.class public final Ljza;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljyu;

.field private final b:Ljava/lang/String;

.field private final c:Ljye;


# direct methods
.method constructor <init>(Landroid/content/Context;Ljyu;Ljye;)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p2, p0, Ljza;->a:Ljyu;

    .line 34
    iput-object p3, p0, Ljza;->c:Ljye;

    .line 35
    const-class v0, Lhee;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljza;->b:Ljava/lang/String;

    .line 36
    return-void
.end method


# virtual methods
.method public a()Loya;
    .locals 14

    .prologue
    .line 56
    iget-object v0, p0, Ljza;->a:Ljyu;

    invoke-virtual {v0}, Ljyu;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljza;->a:Ljyu;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljyu;->c(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 57
    :cond_0
    const/4 v0, 0x0

    .line 70
    :goto_0
    return-object v0

    .line 60
    :cond_1
    iget-object v0, p0, Ljza;->a:Ljyu;

    invoke-virtual {v0}, Ljyu;->a()I

    move-result v0

    new-array v1, v0, [Lizu;

    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Ljza;->a:Ljyu;

    invoke-virtual {v2}, Ljyu;->a()I

    move-result v2

    if-ge v0, v2, :cond_2

    iget-object v2, p0, Ljza;->a:Ljyu;

    invoke-virtual {v2, v0}, Ljyu;->a(I)Ljyw;

    move-result-object v2

    invoke-virtual {v2}, Ljyw;->a()Lizu;

    move-result-object v2

    aput-object v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    iget-object v0, p0, Ljza;->c:Ljye;

    invoke-virtual {v0, v1}, Ljye;->a([Lizu;)[Ljava/lang/Long;

    .line 62
    new-instance v3, Lpcd;

    invoke-direct {v3}, Lpcd;-><init>()V

    .line 63
    const-string v0, ""

    iput-object v0, v3, Lpcd;->b:Ljava/lang/String;

    .line 64
    iget-object v0, p0, Ljza;->a:Ljyu;

    invoke-virtual {v0}, Ljyu;->j()Z

    move-result v4

    const/4 v1, 0x0

    iget-object v0, p0, Ljza;->a:Ljyu;

    invoke-virtual {v0}, Ljyu;->g()Ljava/util/ArrayList;

    move-result-object v5

    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyw;

    invoke-virtual {v0, v4}, Ljyw;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_3

    add-int/lit8 v2, v2, 0x1

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_4
    new-array v6, v2, [Loya;

    const/4 v1, 0x0

    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_3
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_b

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyw;

    invoke-virtual {v0, v4}, Ljyw;->a(Z)Z

    move-result v7

    if-eqz v7, :cond_c

    new-instance v7, Lpca;

    invoke-direct {v7}, Lpca;-><init>()V

    invoke-virtual {v0}, Ljyw;->c()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v7, Lpca;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljyw;->a()Lizu;

    move-result-object v8

    if-nez v8, :cond_5

    const/4 v0, 0x0

    :goto_4
    iput-object v0, v7, Lpca;->e:Loya;

    new-instance v8, Loya;

    invoke-direct {v8}, Loya;-><init>()V

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v9, 0x0

    const/16 v10, 0x199

    aput v10, v0, v9

    iput-object v0, v8, Loya;->b:[I

    sget-object v0, Lpca;->a:Loxr;

    invoke-virtual {v8, v0, v7}, Loya;->a(Loxr;Ljava/lang/Object;)V

    add-int/lit8 v0, v1, 0x1

    aput-object v8, v6, v1

    :goto_5
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_3

    :cond_5
    new-instance v9, Lpbu;

    invoke-direct {v9}, Lpbu;-><init>()V

    sget-object v0, Ljac;->b:Ljac;

    invoke-virtual {v8}, Lizu;->g()Ljac;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljac;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    const/4 v0, 0x2

    :goto_6
    iput v0, v9, Lpbu;->j:I

    invoke-virtual {v8}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {v8}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lpbu;->f:Ljava/lang/String;

    :goto_7
    iget-object v0, p0, Ljza;->b:Ljava/lang/String;

    iput-object v0, v9, Lpbu;->g:Ljava/lang/String;

    invoke-virtual {v8}, Lizu;->c()J

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmp-long v0, v10, v12

    if-nez v0, :cond_a

    iget-object v0, p0, Ljza;->c:Ljye;

    invoke-virtual {v0, v8}, Ljye;->b(Lizu;)Ljyc;

    move-result-object v0

    if-nez v0, :cond_9

    const/4 v0, 0x0

    goto :goto_4

    :cond_6
    sget-object v0, Ljac;->c:Ljac;

    invoke-virtual {v8}, Lizu;->g()Ljac;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljac;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Ljac;->d:Ljac;

    invoke-virtual {v8}, Lizu;->g()Ljac;

    move-result-object v10

    invoke-virtual {v0, v10}, Ljac;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Ljac;->d:Ljac;

    iget v0, v0, Ljac;->e:I

    goto :goto_6

    :cond_7
    const/4 v0, 0x1

    goto :goto_6

    :cond_8
    invoke-virtual {v8}, Lizu;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lpbu;->f:Ljava/lang/String;

    goto :goto_7

    :cond_9
    invoke-virtual {v0}, Ljyc;->c()Ljava/lang/String;

    move-result-object v8

    iput-object v8, v9, Lpbu;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljyc;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lpbu;->i:Ljava/lang/String;

    :goto_8
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    const/4 v8, 0x1

    new-array v8, v8, [I

    const/4 v10, 0x0

    const/16 v11, 0x158

    aput v11, v8, v10

    iput-object v8, v0, Loya;->b:[I

    sget-object v8, Lpbu;->a:Loxr;

    invoke-virtual {v0, v8, v9}, Loya;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_4

    :cond_a
    invoke-virtual {v8}, Lizu;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lpbu;->b:Ljava/lang/String;

    invoke-virtual {v8}, Lizu;->c()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v9, Lpbu;->i:Ljava/lang/String;

    goto :goto_8

    :cond_b
    iput-object v6, v3, Lpcd;->e:[Loya;

    .line 66
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    .line 67
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    const/16 v4, 0x198

    aput v4, v1, v2

    iput-object v1, v0, Loya;->b:[I

    .line 68
    sget-object v1, Lpcd;->a:Loxr;

    invoke-virtual {v0, v1, v3}, Loya;->a(Loxr;Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_c
    move v0, v1

    goto/16 :goto_5
.end method
