.class public Lbco;
.super Llol;
.source "PG"

# interfaces
.implements Laqv;
.implements Larg;
.implements Lasi;
.implements Lbfq;
.implements Lbfs;
.implements Lcdw;


# static fields
.field private static final O:Laqw;

.field private static final P:Larh;


# instance fields
.field N:Landroid/view/View;

.field private final Q:Lamk;

.field private R:Laqw;

.field private S:Larh;

.field private T:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

.field private U:Landroid/view/ViewGroup;

.field private V:Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;

.field private W:Landroid/view/TextureView;

.field private X:Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;

.field private Y:Landroid/widget/TextView;

.field private Z:Landroid/widget/ImageView;

.field private aA:Landroid/widget/TextView;

.field private aB:Landroid/view/View;

.field private aC:Landroid/view/View;

.field private aD:Landroid/view/View;

.field private aE:Landroid/view/View;

.field private aF:Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;

.field private aG:Landroid/view/View;

.field private aH:Landroid/widget/TextView;

.field private aI:Landroid/widget/TextView;

.field private aJ:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbza;",
            ">;"
        }
    .end annotation
.end field

.field private aK:Z

.field private aL:Z

.field private aM:Z

.field private aN:Z

.field private aO:Landroid/animation/ObjectAnimator;

.field private aP:Landroid/animation/ObjectAnimator;

.field private aQ:Landroid/animation/Animator;

.field private aR:Z

.field private aS:Z

.field private aT:Z

.field private aU:Landroid/animation/ObjectAnimator;

.field private aV:Z

.field private aW:Landroid/animation/Animator$AnimatorListener;

.field private final aX:Landroid/os/Handler;

.field private aY:Lbdv;

.field private aZ:I

.field private aa:Landroid/widget/ImageView;

.field private ab:Landroid/view/View;

.field private ac:Landroid/widget/TextView;

.field private ad:Landroid/view/View;

.field private ae:Landroid/view/View;

.field private af:Landroid/view/View;

.field private ag:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

.field private ah:Landroid/widget/TextView;

.field private ai:Landroid/widget/TextView;

.field private aj:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

.field private ak:Landroid/view/View;

.field private al:Landroid/view/View;

.field private am:Landroid/widget/ImageButton;

.field private an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

.field private ao:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

.field private ap:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

.field private aq:Landroid/view/View;

.field private ar:Landroid/widget/SeekBar;

.field private as:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

.field private aw:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

.field private ax:Lcom/google/android/apps/moviemaker/ui/ThemeItemView;

.field private ay:Landroid/widget/FrameLayout;

.field private az:Landroid/view/View;

.field private ba:Z

.field private bb:Laqc;

.field private bc:Laqy;

.field private bd:Lase;

.field private be:Lapu;

.field private final bf:Landroid/view/View$OnLayoutChangeListener;

.field private final bg:Landroid/widget/TextView$OnEditorActionListener;

.field private final bh:Lcal;

.field private final bi:Landroid/animation/AnimatorListenerAdapter;

.field private final bj:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private bk:Lamb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 142
    const-class v0, Laqw;

    .line 143
    invoke-static {v0}, Lcgf;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laqw;

    sput-object v0, Lbco;->O:Laqw;

    .line 145
    const-class v0, Larh;

    .line 146
    invoke-static {v0}, Lcgf;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larh;

    sput-object v0, Lbco;->P:Larh;

    .line 145
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 87
    invoke-direct {p0}, Llol;-><init>()V

    .line 148
    new-instance v0, Lbdu;

    invoke-direct {v0, p0}, Lbdu;-><init>(Lbco;)V

    iput-object v0, p0, Lbco;->Q:Lamk;

    .line 150
    sget-object v0, Lbco;->O:Laqw;

    iput-object v0, p0, Lbco;->R:Laqw;

    .line 151
    sget-object v0, Lbco;->P:Larh;

    iput-object v0, p0, Lbco;->S:Larh;

    .line 221
    new-instance v0, Lbcp;

    invoke-direct {v0, p0}, Lbcp;-><init>(Lbco;)V

    iput-object v0, p0, Lbco;->aW:Landroid/animation/Animator$AnimatorListener;

    .line 228
    new-instance v0, Lbdb;

    invoke-direct {v0, p0}, Lbdb;-><init>(Lbco;)V

    iput-object v0, p0, Lbco;->aX:Landroid/os/Handler;

    .line 260
    new-instance v0, Lbdn;

    invoke-direct {v0, p0}, Lbdn;-><init>(Lbco;)V

    iput-object v0, p0, Lbco;->aY:Lbdv;

    .line 325
    new-instance v0, Lbdo;

    invoke-direct {v0, p0}, Lbdo;-><init>(Lbco;)V

    iput-object v0, p0, Lbco;->bf:Landroid/view/View$OnLayoutChangeListener;

    .line 338
    new-instance v0, Lbdp;

    invoke-direct {v0, p0}, Lbdp;-><init>(Lbco;)V

    iput-object v0, p0, Lbco;->bg:Landroid/widget/TextView$OnEditorActionListener;

    .line 356
    new-instance v0, Lbdq;

    invoke-direct {v0, p0}, Lbdq;-><init>(Lbco;)V

    iput-object v0, p0, Lbco;->bh:Lcal;

    .line 366
    new-instance v0, Lbdr;

    invoke-direct {v0, p0}, Lbdr;-><init>(Lbco;)V

    iput-object v0, p0, Lbco;->bi:Landroid/animation/AnimatorListenerAdapter;

    .line 384
    new-instance v0, Lbds;

    const-class v1, Ljava/lang/Float;

    const-string v2, "progress"

    invoke-direct {v0, p0, v1, v2}, Lbds;-><init>(Lbco;Ljava/lang/Class;Ljava/lang/String;)V

    iput-object v0, p0, Lbco;->bj:Landroid/util/Property;

    .line 446
    new-instance v0, Lhmf;

    iget-object v1, p0, Lbco;->av:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 1854
    return-void
.end method

.method static synthetic A(Lbco;)Landroid/view/View;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->aC:Landroid/view/View;

    return-object v0
.end method

.method static synthetic B(Lbco;)Landroid/view/View;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->az:Landroid/view/View;

    return-object v0
.end method

.method private W()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 715
    invoke-direct {p0}, Lbco;->ad()Z

    move-result v0

    if-nez v0, :cond_0

    .line 716
    iput-object v2, p0, Lbco;->aj:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    .line 741
    :goto_0
    return-void

    .line 719
    :cond_0
    invoke-direct {p0}, Lbco;->Z()Loo;

    move-result-object v0

    const-string v1, "actionBar"

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loo;

    .line 721
    invoke-virtual {v0}, Loo;->a()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 722
    invoke-virtual {v0}, Loo;->a()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f10038c

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_2

    .line 726
    :cond_1
    iget-object v1, p0, Lbco;->aj:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    if-nez v1, :cond_3

    .line 727
    const v1, 0x7f040112

    invoke-virtual {v0, v1}, Loo;->a(I)V

    .line 733
    :cond_2
    :goto_1
    invoke-virtual {v0}, Loo;->a()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    iput-object v1, p0, Lbco;->aj:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    .line 734
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Loo;->e(Z)V

    .line 735
    const/high16 v1, 0x41800000    # 16.0f

    invoke-virtual {v0, v1}, Loo;->a(F)V

    .line 739
    invoke-virtual {v0}, Loo;->f()V

    invoke-virtual {v0}, Loo;->e()V

    .line 740
    iget-object v0, p0, Lbco;->aj:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    iput-object v0, p0, Lbco;->ag:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    goto :goto_0

    .line 729
    :cond_3
    iget-object v1, p0, Lbco;->aj:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    invoke-virtual {v0, v1}, Loo;->a(Landroid/view/View;)V

    goto :goto_1
.end method

.method private X()V
    .locals 2

    .prologue
    .line 1249
    iget-object v1, p0, Lbco;->ad:Landroid/view/View;

    iget-boolean v0, p0, Lbco;->aL:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbco;->aR:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbco;->aS:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1251
    return-void

    .line 1249
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private Y()V
    .locals 4

    .prologue
    const/16 v1, 0x3e8

    .line 1350
    iget-object v0, p0, Lbco;->aX:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1351
    invoke-direct {p0}, Lbco;->aa()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1352
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbco;->r(Z)V

    .line 1361
    :goto_0
    return-void

    .line 1354
    :cond_0
    invoke-direct {p0}, Lbco;->ab()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1355
    iget-object v0, p0, Lbco;->aX:Landroid/os/Handler;

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 1358
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbco;->r(Z)V

    goto :goto_0
.end method

.method private Z()Loo;
    .locals 1

    .prologue
    .line 1364
    invoke-virtual {p0}, Lbco;->n()Lz;

    move-result-object v0

    check-cast v0, Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;F)F
    .locals 2

    .prologue
    .line 977
    invoke-virtual {p1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->c()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 981
    :goto_0
    return p2

    .line 980
    :cond_0
    iget-object v0, p0, Lbco;->X:Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->c()F

    move-result v1

    div-float/2addr v0, v1

    .line 981
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, v0

    mul-float/2addr v1, p2

    add-float p2, v1, v0

    goto :goto_0
.end method

.method static synthetic a(Lbco;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lbco;->aP:Landroid/animation/ObjectAnimator;

    return-object p1
.end method

.method static synthetic a(Lbco;)Landroid/view/View;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->ak:Landroid/view/View;

    return-object v0
.end method

.method private a(F)V
    .locals 2

    .prologue
    .line 1611
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    .line 1612
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 1614
    const v0, 0x3fe38e39

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 1618
    :goto_0
    iget-object v1, p0, Lbco;->as:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->a(F)V

    .line 1619
    iget-object v0, p0, Lbco;->aw:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->a(F)V

    .line 1620
    return-void

    .line 1616
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto :goto_0
.end method

.method private a(Laxs;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 1657
    iget-object v0, p0, Lbco;->aQ:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbco;->aQ:Landroid/animation/Animator;

    .line 1658
    invoke-virtual {v0}, Landroid/animation/Animator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1659
    iget-object v0, p0, Lbco;->aQ:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 1662
    :cond_0
    iget-boolean v0, p0, Lbco;->ba:Z

    if-nez v0, :cond_1

    .line 1663
    const v0, 0x7f10038b

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lbco;->ba:Z

    const v0, 0x7f100274

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbco;->az:Landroid/view/View;

    const v0, 0x7f100275

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbco;->aA:Landroid/widget/TextView;

    const v0, 0x7f100276

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbco;->aB:Landroid/view/View;

    const v0, 0x7f100279

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbco;->aC:Landroid/view/View;

    const v0, 0x7f100277

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbco;->aD:Landroid/view/View;

    const v0, 0x7f100278

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbco;->aE:Landroid/view/View;

    iget-object v0, p0, Lbco;->aE:Landroid/view/View;

    new-instance v3, Lbdk;

    invoke-direct {v3, p0}, Lbdk;-><init>(Lbco;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lbco;->aD:Landroid/view/View;

    new-instance v3, Lbdl;

    invoke-direct {v3, p0}, Lbdl;-><init>(Lbco;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lbco;->aC:Landroid/view/View;

    new-instance v3, Lbdm;

    invoke-direct {v3, p0}, Lbdm;-><init>(Lbco;)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1666
    :cond_1
    iget-object v0, p0, Lbco;->az:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1667
    iget-object v0, p0, Lbco;->az:Landroid/view/View;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3}, Landroid/view/View;->setAlpha(F)V

    .line 1669
    if-eqz p1, :cond_3

    .line 1670
    iget-object v0, p0, Lbco;->aA:Landroid/widget/TextView;

    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p1, Laxs;->c:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1671
    iget-object v0, p0, Lbco;->aA:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1673
    iget-object v0, p0, Lbco;->aA:Landroid/widget/TextView;

    const v3, 0x7f050012

    invoke-static {v0, v3}, Lcad;->a(Landroid/view/View;I)Landroid/animation/Animator;

    move-result-object v0

    .line 1675
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 1679
    :goto_0
    iget-object v3, p0, Lbco;->aD:Landroid/view/View;

    if-eqz p2, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1680
    iget-object v0, p0, Lbco;->aE:Landroid/view/View;

    if-eqz p2, :cond_2

    move v1, v2

    :cond_2
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1682
    iget-object v0, p0, Lbco;->aB:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setAlpha(F)V

    .line 1683
    iget-object v0, p0, Lbco;->aB:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1684
    iget-object v0, p0, Lbco;->aB:Landroid/view/View;

    const v1, 0x7f05000d

    invoke-static {v0, v1}, Lcad;->a(Landroid/view/View;I)Landroid/animation/Animator;

    move-result-object v1

    .line 1686
    if-eqz p1, :cond_5

    .line 1687
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0c001d

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 1689
    :goto_2
    int-to-long v4, v0

    invoke-virtual {v1, v4, v5}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 1690
    invoke-virtual {v1}, Landroid/animation/Animator;->start()V

    .line 1692
    iget-object v0, p0, Lbco;->aC:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setAlpha(F)V

    .line 1693
    iget-object v0, p0, Lbco;->aC:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1694
    iget-object v0, p0, Lbco;->aC:Landroid/view/View;

    const v1, 0x7f05000f

    invoke-static {v0, v1}, Lcad;->a(Landroid/view/View;I)Landroid/animation/Animator;

    move-result-object v1

    .line 1696
    if-eqz p1, :cond_6

    .line 1697
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c001e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 1699
    :goto_3
    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 1700
    invoke-virtual {v1}, Landroid/animation/Animator;->start()V

    .line 1701
    return-void

    .line 1677
    :cond_3
    iget-object v0, p0, Lbco;->aB:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1679
    goto :goto_1

    :cond_5
    move v0, v2

    .line 1687
    goto :goto_2

    .line 1698
    :cond_6
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c001f

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    goto :goto_3
.end method

.method static synthetic a(Lbco;Z)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lbco;->r(Z)V

    return-void
.end method

.method static synthetic a(Lbco;ZZ)V
    .locals 2

    .prologue
    .line 87
    if-eqz p1, :cond_2

    iget-object v0, p0, Lbco;->X:Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p2, :cond_1

    iget-object v0, p0, Lbco;->X:Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lbco;->X:Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->setVisibility(I)V

    goto :goto_0

    :cond_2
    if-nez p1, :cond_0

    iget-object v0, p0, Lbco;->X:Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;FI)V
    .locals 4

    .prologue
    .line 957
    sget-object v0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->a:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [F

    const/4 v2, 0x0

    .line 958
    invoke-virtual {p1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->b()F

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    .line 957
    invoke-static {p1, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lbco;->aP:Landroid/animation/ObjectAnimator;

    .line 959
    iget-object v0, p0, Lbco;->aP:Landroid/animation/ObjectAnimator;

    sget-object v1, Lcak;->a:Lcak;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 960
    iget-object v0, p0, Lbco;->aP:Landroid/animation/ObjectAnimator;

    int-to-long v2, p3

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 961
    iget-object v0, p0, Lbco;->aP:Landroid/animation/ObjectAnimator;

    new-instance v1, Lbdf;

    invoke-direct {v1, p0}, Lbdf;-><init>(Lbco;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 967
    iget-object v0, p0, Lbco;->aP:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 968
    return-void
.end method

.method private a(Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;I)V
    .locals 3

    .prologue
    .line 854
    int-to-float v0, p2

    const/high16 v1, 0x42c80000    # 100.0f

    div-float/2addr v0, v1

    .line 855
    const/16 v1, 0x64

    if-eq p2, v1, :cond_1

    iget-object v1, p0, Lbco;->aP:Landroid/animation/ObjectAnimator;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbco;->aP:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v1

    if-nez v1, :cond_1

    .line 856
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->b()F

    move-result v1

    sub-float v1, v0, v1

    const v2, 0x3d4ccccd    # 0.05f

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    .line 862
    :cond_1
    :goto_0
    return-void

    .line 860
    :cond_2
    invoke-direct {p0, p1, v0}, Lbco;->a(Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;F)F

    move-result v0

    .line 861
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c000a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 859
    invoke-direct {p0, p1, v0, v1}, Lbco;->a(Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;FI)V

    goto :goto_0
.end method

.method private aa()Z
    .locals 1

    .prologue
    .line 1473
    invoke-direct {p0}, Lbco;->ad()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbco;->aM:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbco;->aN:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lbco;->ag:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    .line 1476
    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ab()Z
    .locals 1

    .prologue
    .line 1484
    invoke-virtual {p0}, Lbco;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbco;->aL:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lbco;->aZ:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ac()V
    .locals 1

    .prologue
    .line 1634
    iget-object v0, p0, Lbco;->U:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestFocus()Z

    .line 1635
    invoke-direct {p0}, Lbco;->Y()V

    .line 1636
    return-void
.end method

.method private ad()Z
    .locals 2

    .prologue
    .line 1831
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lbco;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lbco;->aO:Landroid/animation/ObjectAnimator;

    return-object p1
.end method

.method static synthetic b(Lbco;)Z
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Lbco;->aa()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lbco;Z)Z
    .locals 0

    .prologue
    .line 87
    iput-boolean p1, p0, Lbco;->aV:Z

    return p1
.end method

.method static synthetic c(Lbco;)I
    .locals 1

    .prologue
    .line 87
    iget v0, p0, Lbco;->aZ:I

    return v0
.end method

.method static synthetic c(Lbco;Z)Z
    .locals 0

    .prologue
    .line 87
    iput-boolean p1, p0, Lbco;->aN:Z

    return p1
.end method

.method static synthetic d(Lbco;)Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->V:Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;

    return-object v0
.end method

.method static synthetic d(Lbco;Z)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lbco;->q(Z)V

    return-void
.end method

.method static synthetic e(Lbco;)Landroid/view/TextureView;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->W:Landroid/view/TextureView;

    return-object v0
.end method

.method static synthetic e(Lbco;Z)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lbco;->p(Z)V

    return-void
.end method

.method static synthetic f(Lbco;)Laqw;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->R:Laqw;

    return-object v0
.end method

.method static synthetic f(Lbco;Z)Z
    .locals 0

    .prologue
    .line 87
    iput-boolean p1, p0, Lbco;->aK:Z

    return p1
.end method

.method private g(I)Landroid/view/View;
    .locals 4

    .prologue
    .line 1850
    iget-object v0, p0, Lbco;->U:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1851
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x13

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "View "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1850
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method static synthetic g(Lbco;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lbco;->ac()V

    return-void
.end method

.method static synthetic h(Lbco;)Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->as:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    return-object v0
.end method

.method static synthetic i(Lbco;)Landroid/view/View;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->al:Landroid/view/View;

    return-object v0
.end method

.method static synthetic j(Lbco;)V
    .locals 0

    .prologue
    .line 87
    invoke-direct {p0}, Lbco;->Y()V

    return-void
.end method

.method static synthetic k(Lbco;)V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbco;->r(Z)V

    invoke-direct {p0}, Lbco;->Y()V

    return-void
.end method

.method static synthetic l(Lbco;)Ljava/util/List;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->aJ:Ljava/util/List;

    return-object v0
.end method

.method static synthetic m(Lbco;)V
    .locals 4

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->ay:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbco;->ax:Lcom/google/android/apps/moviemaker/ui/ThemeItemView;

    invoke-static {v0}, Lcad;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lbdi;

    invoke-direct {v1, p0}, Lbdi;-><init>(Lbco;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    :cond_0
    return-void
.end method

.method static synthetic n(Lbco;)Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->T:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    return-object v0
.end method

.method static synthetic o(Lbco;)Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->U:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic p(Lbco;)Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    return-object v0
.end method

.method private p(Z)V
    .locals 1

    .prologue
    .line 749
    iget-object v0, p0, Lbco;->aY:Lbdv;

    invoke-interface {v0, p1}, Lbdv;->d(Z)V

    .line 750
    return-void
.end method

.method static synthetic q(Lbco;)Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->ap:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    return-object v0
.end method

.method private q(Z)V
    .locals 2

    .prologue
    .line 753
    iget-object v0, p0, Lbco;->aY:Lbdv;

    invoke-interface {v0, p1}, Lbdv;->c(Z)V

    .line 754
    if-nez p1, :cond_0

    .line 755
    iget-object v0, p0, Lbco;->ap:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->setVisibility(I)V

    .line 757
    :cond_0
    return-void
.end method

.method static synthetic r(Lbco;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->Z:Landroid/widget/ImageView;

    return-object v0
.end method

.method private r(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1388
    invoke-direct {p0}, Lbco;->ab()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lbco;->aU:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbco;->aU:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lbco;->aU:Landroid/animation/ObjectAnimator;

    :cond_0
    iget-object v0, p0, Lbco;->ak:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v4, p0, Lbco;->aT:Z

    .line 1390
    :cond_1
    :goto_0
    invoke-direct {p0}, Lbco;->Z()Loo;

    move-result-object v0

    .line 1391
    if-eqz v0, :cond_2

    .line 1392
    if-eqz p1, :cond_b

    .line 1393
    invoke-virtual {v0}, Loo;->e()V

    .line 1399
    :cond_2
    :goto_1
    invoke-direct {p0}, Lbco;->ad()Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz p1, :cond_c

    iget-object v0, p0, Lbco;->as:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->getSystemUiVisibility()I

    move-result v0

    and-int/lit8 v0, v0, -0x6

    iget-object v1, p0, Lbco;->as:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->setSystemUiVisibility(I)V

    .line 1400
    :cond_3
    :goto_2
    return-void

    .line 1388
    :cond_4
    iget-boolean v0, p0, Lbco;->aT:Z

    if-eq p1, v0, :cond_1

    iget-object v0, p0, Lbco;->aU:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lbco;->aU:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    :cond_5
    iget-boolean v0, p0, Lbco;->aV:Z

    if-nez v0, :cond_a

    new-instance v2, Landroid/animation/ObjectAnimator;

    invoke-direct {v2}, Landroid/animation/ObjectAnimator;-><init>()V

    iget-object v0, p0, Lbco;->ak:Landroid/view/View;

    invoke-virtual {v2, v0}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    sget-object v0, Landroid/view/View;->ALPHA:Landroid/util/Property;

    invoke-virtual {v2, v0}, Landroid/animation/ObjectAnimator;->setProperty(Landroid/util/Property;)V

    const/4 v0, 0x2

    new-array v1, v0, [F

    iget-object v0, p0, Lbco;->ak:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAlpha()F

    move-result v0

    aput v0, v1, v4

    const/4 v3, 0x1

    if-eqz p1, :cond_7

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_3
    aput v0, v1, v3

    invoke-virtual {v2, v1}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    if-eqz p1, :cond_8

    const-wide/16 v0, 0xc8

    :goto_4
    invoke-virtual {v2, v0, v1}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    if-eqz p1, :cond_9

    iget-object v0, p0, Lbco;->ak:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_5
    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->start()V

    iput-object v2, p0, Lbco;->aU:Landroid/animation/ObjectAnimator;

    :cond_6
    :goto_6
    iput-boolean p1, p0, Lbco;->aT:Z

    goto :goto_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    :cond_8
    const-wide/16 v0, 0x1f4

    goto :goto_4

    :cond_9
    iget-object v0, p0, Lbco;->aW:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v2, v0}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_5

    :cond_a
    if-eqz p1, :cond_6

    iget-object v0, p0, Lbco;->ak:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_6

    .line 1395
    :cond_b
    invoke-virtual {v0}, Loo;->f()V

    goto :goto_1

    .line 1399
    :cond_c
    iget-object v0, p0, Lbco;->as:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->getSystemUiVisibility()I

    move-result v0

    or-int/lit8 v0, v0, 0x5

    iget-object v1, p0, Lbco;->as:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->setSystemUiVisibility(I)V

    goto :goto_2
.end method

.method static synthetic s(Lbco;)Larh;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->S:Larh;

    return-object v0
.end method

.method private s(Z)V
    .locals 2

    .prologue
    .line 1493
    iget-object v1, p0, Lbco;->Y:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1494
    return-void

    .line 1493
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method static synthetic t(Lbco;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->aa:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic u(Lbco;)Landroid/view/View;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->aq:Landroid/view/View;

    return-object v0
.end method

.method static synthetic v(Lbco;)Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->ao:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    return-object v0
.end method

.method static synthetic w(Lbco;)V
    .locals 3

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->ax:Lcom/google/android/apps/moviemaker/ui/ThemeItemView;

    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0049

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->setBackgroundColor(I)V

    iget-object v0, p0, Lbco;->ay:Landroid/widget/FrameLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setVisibility(I)V

    invoke-virtual {p0}, Lbco;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/high16 v1, 0x7f060000

    invoke-static {v0, v1}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    check-cast v0, Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lbco;->ax:Lcom/google/android/apps/moviemaker/ui/ThemeItemView;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setTarget(Ljava/lang/Object;)V

    new-instance v1, Lbdh;

    invoke-direct {v1, p0}, Lbdh;-><init>(Lbco;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method static synthetic x(Lbco;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->ay:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic y(Lbco;)Landroid/view/View;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->aB:Landroid/view/View;

    return-object v0
.end method

.method static synthetic z(Lbco;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lbco;->aA:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public U()Z
    .locals 1

    .prologue
    .line 1209
    iget-object v0, p0, Lbco;->S:Larh;

    invoke-interface {v0}, Larh;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1210
    const/4 v0, 0x1

    .line 1212
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lbco;->R:Laqw;

    invoke-interface {v0}, Laqw;->n()Z

    move-result v0

    goto :goto_0
.end method

.method public V()V
    .locals 1

    .prologue
    .line 1217
    invoke-direct {p0}, Lbco;->W()V

    .line 1218
    iget-object v0, p0, Lbco;->R:Laqw;

    invoke-interface {v0}, Laqw;->m()V

    .line 1219
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 473
    const v0, 0x7f040199

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lbco;->U:Landroid/view/ViewGroup;

    .line 475
    const v0, 0x7f100380

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    iput-object v0, p0, Lbco;->T:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    .line 476
    const v0, 0x7f10037b

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;

    iput-object v0, p0, Lbco;->V:Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;

    .line 477
    const v0, 0x7f10037c

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    iput-object v0, p0, Lbco;->W:Landroid/view/TextureView;

    .line 478
    const v0, 0x7f100383

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;

    iput-object v0, p0, Lbco;->X:Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;

    .line 479
    const v0, 0x7f100387

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbco;->Y:Landroid/widget/TextView;

    .line 480
    const v0, 0x7f100385

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbco;->Z:Landroid/widget/ImageView;

    .line 481
    const v0, 0x7f100386

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lbco;->aa:Landroid/widget/ImageView;

    .line 483
    const v0, 0x7f100467

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lbco;->am:Landroid/widget/ImageButton;

    .line 484
    const v0, 0x7f10037d

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    iput-object v0, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    .line 485
    const v0, 0x7f10037f

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    iput-object v0, p0, Lbco;->ao:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    .line 486
    const v0, 0x7f100384

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    iput-object v0, p0, Lbco;->ap:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    .line 487
    const v0, 0x7f10037e

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbco;->aq:Landroid/view/View;

    .line 488
    const v0, 0x7f1004d8

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbco;->ak:Landroid/view/View;

    .line 489
    const v0, 0x7f100389

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbco;->ab:Landroid/view/View;

    .line 490
    const v0, 0x7f10038a

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbco;->ac:Landroid/widget/TextView;

    .line 492
    const v0, 0x7f1004db

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbco;->ah:Landroid/widget/TextView;

    .line 493
    const v0, 0x7f1004d9

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbco;->ai:Landroid/widget/TextView;

    .line 495
    invoke-direct {p0}, Lbco;->ad()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 496
    invoke-direct {p0}, Lbco;->W()V

    .line 501
    :goto_0
    const v0, 0x7f100398

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;

    iput-object v0, p0, Lbco;->aF:Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;

    .line 502
    const v0, 0x7f1004dc

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbco;->ad:Landroid/view/View;

    .line 503
    const v0, 0x7f1004de

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbco;->ae:Landroid/view/View;

    .line 504
    const v0, 0x7f1004dd

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbco;->af:Landroid/view/View;

    .line 505
    const v0, 0x7f100379

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    iput-object v0, p0, Lbco;->as:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    .line 506
    const v0, 0x7f10037a

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    iput-object v0, p0, Lbco;->aw:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    .line 507
    const v0, 0x7f1004da

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lbco;->ar:Landroid/widget/SeekBar;

    .line 509
    const v0, 0x7f100382

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;

    iput-object v0, p0, Lbco;->ax:Lcom/google/android/apps/moviemaker/ui/ThemeItemView;

    .line 510
    const v0, 0x7f100381

    .line 511
    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lbco;->ay:Landroid/widget/FrameLayout;

    .line 513
    const v0, 0x7f1003ad

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbco;->aG:Landroid/view/View;

    .line 514
    const v0, 0x7f1003ae

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbco;->aH:Landroid/widget/TextView;

    .line 515
    const v0, 0x7f1003af

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbco;->aI:Landroid/widget/TextView;

    .line 517
    invoke-direct {p0}, Lbco;->ad()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 519
    iput-object v1, p0, Lbco;->N:Landroid/view/View;

    .line 520
    iput-object v1, p0, Lbco;->al:Landroid/view/View;

    .line 526
    :goto_1
    iget-object v0, p0, Lbco;->ar:Landroid/widget/SeekBar;

    new-instance v1, Lbdt;

    invoke-direct {v1, p0}, Lbdt;-><init>(Lbco;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 549
    iget-object v0, p0, Lbco;->am:Landroid/widget/ImageButton;

    new-instance v1, Lbcq;

    invoke-direct {v1, p0}, Lbcq;-><init>(Lbco;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 557
    iget-object v0, p0, Lbco;->T:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    new-instance v1, Lbcr;

    invoke-direct {v1, p0}, Lbcr;-><init>(Lbco;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->a(Lccs;)V

    .line 572
    iget-object v0, p0, Lbco;->T:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    new-instance v1, Lbcs;

    invoke-direct {v1, p0}, Lbcs;-><init>(Lbco;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 595
    iget-object v0, p0, Lbco;->ad:Landroid/view/View;

    new-instance v1, Lbct;

    invoke-direct {v1, p0}, Lbct;-><init>(Lbco;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 604
    iget-object v0, p0, Lbco;->ae:Landroid/view/View;

    new-instance v1, Lbcu;

    invoke-direct {v1, p0}, Lbcu;-><init>(Lbco;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 612
    iget-object v0, p0, Lbco;->af:Landroid/view/View;

    new-instance v1, Lbcv;

    invoke-direct {v1, p0}, Lbcv;-><init>(Lbco;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 622
    iput-boolean v2, p0, Lbco;->ba:Z

    .line 623
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbco;->aT:Z

    .line 625
    iget-object v0, p0, Lbco;->U:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->requestFocus()Z

    .line 626
    iget-object v0, p0, Lbco;->U:Landroid/view/ViewGroup;

    new-instance v1, Lbcw;

    invoke-direct {v1, p0}, Lbcw;-><init>(Lbco;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->post(Ljava/lang/Runnable;)Z

    .line 633
    iget-object v0, p0, Lbco;->U:Landroid/view/ViewGroup;

    return-object v0

    .line 498
    :cond_0
    const v0, 0x7f10038c

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    iput-object v0, p0, Lbco;->ag:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    goto/16 :goto_0

    .line 522
    :cond_1
    const v0, 0x7f1004d7

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbco;->N:Landroid/view/View;

    .line 523
    const v0, 0x7f1004d6

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbco;->al:Landroid/view/View;

    goto :goto_1
.end method

.method public a(Lbgf;)Lbge;
    .locals 2

    .prologue
    .line 1519
    iget v0, p0, Lbco;->aZ:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    new-instance v0, Lbtr;

    iget-object v1, p0, Lbco;->W:Landroid/view/TextureView;

    invoke-direct {v0, p1, v1}, Lbtr;-><init>(Lbgf;Landroid/view/TextureView;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lbtn;

    iget-object v1, p0, Lbco;->V:Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;

    invoke-direct {v0, p1, v1}, Lbtn;-><init>(Lbgf;Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;)V

    goto :goto_0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 1003
    iget-object v0, p0, Lbco;->Y:Landroid/widget/TextView;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbco;->s(Z)V

    .line 1004
    return-void
.end method

.method public a(FJ)V
    .locals 6

    .prologue
    .line 1294
    iget-boolean v0, p0, Lbco;->aN:Z

    if-nez v0, :cond_0

    .line 1295
    iget-object v0, p0, Lbco;->ar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getMax()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    .line 1296
    iget-object v1, p0, Lbco;->ar:Landroid/widget/SeekBar;

    .line 1297
    invoke-virtual {v1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    const-wide/16 v2, 0x0

    move-wide v4, p2

    .line 1296
    invoke-static/range {v0 .. v5}, Lchf;->a(IIJJ)J

    move-result-wide v2

    .line 1299
    iget-object v1, p0, Lbco;->ar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 1300
    iget-object v0, p0, Lbco;->ai:Landroid/widget/TextView;

    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v2, v3, v1}, Lchf;->a(JLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1302
    :cond_0
    return-void
.end method

.method public a(II)V
    .locals 2

    .prologue
    .line 998
    int-to-float v0, p1

    int-to-float v1, p2

    div-float/2addr v0, v1

    invoke-direct {p0, v0}, Lbco;->a(F)V

    .line 999
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 1262
    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "duration"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 1263
    iget-object v0, p0, Lbco;->ah:Landroid/widget/TextView;

    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p1, p2, v1}, Lchf;->a(JLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1264
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 664
    iget-object v0, p0, Lbco;->ap:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->a(Landroid/graphics/Bitmap;)V

    .line 665
    iget-object v0, p0, Lbco;->ap:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->a(F)V

    .line 666
    iget-object v0, p0, Lbco;->ap:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->setVisibility(I)V

    .line 668
    iget-object v0, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    .line 669
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d008b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    .line 668
    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->b(F)F

    move-result v0

    .line 672
    iget-object v1, p0, Lbco;->ap:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    sget-object v2, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->a:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    aput v4, v3, v5

    const/4 v4, 0x1

    aput v0, v3, v4

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 674
    sget-object v2, Lcak;->a:Lcak;

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 675
    new-instance v2, Lbcx;

    invoke-direct {v2, p0, p1, v0}, Lbcx;-><init>(Lbco;Landroid/graphics/Bitmap;F)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 691
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c000c

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v2, v0

    .line 690
    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 692
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 693
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;I)V
    .locals 10

    .prologue
    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v8, 0x2

    const/4 v6, 0x0

    .line 1031
    invoke-virtual {p0}, Lbco;->s()Z

    move-result v0

    const-string v2, "isAdded()"

    invoke-static {v0, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 1033
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    const/4 v0, 0x4

    if-eq p2, v0, :cond_1

    .line 1034
    :cond_0
    iget-object v0, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->c(F)V

    .line 1035
    iget-object v0, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->a(Landroid/graphics/Bitmap;)V

    .line 1036
    iget-object v0, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    invoke-virtual {v0, v9}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->a(F)V

    .line 1144
    :goto_0
    return-void

    .line 1040
    :cond_1
    iget-object v0, p0, Lbco;->aO:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_2

    .line 1041
    iget-object v0, p0, Lbco;->aO:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 1042
    const/4 v0, 0x0

    iput-object v0, p0, Lbco;->aO:Landroid/animation/ObjectAnimator;

    .line 1045
    :cond_2
    const/4 v0, 0x3

    if-ne p2, v0, :cond_3

    .line 1046
    invoke-direct {p0, v6}, Lbco;->q(Z)V

    .line 1049
    :cond_3
    iget-object v0, p0, Lbco;->ao:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->a(Landroid/graphics/Bitmap;)V

    .line 1050
    iget-object v0, p0, Lbco;->ao:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    invoke-virtual {v0, v6}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->setVisibility(I)V

    .line 1059
    packed-switch p2, :pswitch_data_0

    .line 1085
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c0006

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 1087
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c0007

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 1091
    iget-object v0, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    .line 1092
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v4

    const v7, 0x7f0d0075

    invoke-virtual {v4, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    int-to-float v4, v4

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v4, v7

    .line 1091
    invoke-virtual {v0, v4}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->b(F)F

    move-result v0

    .line 1096
    :goto_1
    iget-object v7, p0, Lbco;->ao:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    if-ne p2, v8, :cond_6

    move v4, v5

    :goto_2
    invoke-virtual {v7, v4}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->a(Z)V

    .line 1098
    sget-object v4, Lcak;->a:Lcak;

    int-to-float v3, v3

    int-to-float v7, v2

    div-float/2addr v3, v7

    invoke-virtual {v4, v3}, Lcak;->getInterpolation(F)F

    move-result v3

    .line 1103
    sub-float v4, v9, v0

    div-float v3, v4, v3

    add-float/2addr v3, v0

    .line 1104
    sget-object v4, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->a:Landroid/util/Property;

    new-array v7, v8, [F

    aput v0, v7, v6

    aput v3, v7, v5

    .line 1105
    invoke-static {v4, v7}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 1106
    if-ne p2, v8, :cond_7

    .line 1107
    iget-object v3, p0, Lbco;->ao:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    new-array v4, v5, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lbco;->aO:Landroid/animation/ObjectAnimator;

    .line 1115
    :goto_3
    iget-object v0, p0, Lbco;->aO:Landroid/animation/ObjectAnimator;

    int-to-long v2, v2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1116
    iget-object v0, p0, Lbco;->aO:Landroid/animation/ObjectAnimator;

    sget-object v2, Lcak;->a:Lcak;

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1118
    iget-object v0, p0, Lbco;->aO:Landroid/animation/ObjectAnimator;

    new-instance v2, Lbdg;

    invoke-direct {v2, p0, p1, p2}, Lbdg;-><init>(Lbco;Landroid/graphics/Bitmap;I)V

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1137
    if-eq p2, v5, :cond_4

    if-ne p2, v8, :cond_5

    .line 1138
    :cond_4
    iget-object v0, p0, Lbco;->T:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->l()V

    .line 1139
    iget-object v0, p0, Lbco;->ao:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->c(F)V

    .line 1140
    iget-object v0, p0, Lbco;->aO:Landroid/animation/ObjectAnimator;

    .line 1141
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0010

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    .line 1140
    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 1143
    :cond_5
    iget-object v0, p0, Lbco;->aO:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto/16 :goto_0

    .line 1061
    :pswitch_0
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c0008

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 1063
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0c0009

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    move v3, v2

    move v2, v0

    move v0, v1

    .line 1066
    goto/16 :goto_1

    .line 1068
    :pswitch_1
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c0006

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 1070
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c0007

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 1072
    iget-object v0, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    iget-object v4, p0, Lbco;->Z:Landroid/widget/ImageView;

    .line 1073
    invoke-virtual {v4}, Landroid/widget/ImageView;->getWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    .line 1072
    invoke-virtual {v0, v4}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->b(F)F

    move-result v0

    goto/16 :goto_1

    .line 1076
    :pswitch_2
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0c0006

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    .line 1078
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0c0007

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    move v3, v2

    move v2, v0

    move v0, v1

    .line 1081
    goto/16 :goto_1

    :cond_6
    move v4, v6

    .line 1096
    goto/16 :goto_2

    .line 1110
    :cond_7
    sget-object v3, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->b:Landroid/util/Property;

    new-array v4, v8, [F

    fill-array-data v4, :array_0

    .line 1111
    invoke-static {v3, v4}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    .line 1112
    iget-object v4, p0, Lbco;->ao:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    new-array v7, v8, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v7, v6

    aput-object v3, v7, v5

    invoke-static {v4, v7}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lbco;->aO:Landroid/animation/ObjectAnimator;

    goto/16 :goto_3

    .line 1059
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 1110
    :array_0
    .array-data 4
        0x3f833333    # 1.025f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public a(Laqw;)V
    .locals 1

    .prologue
    .line 1498
    if-nez p1, :cond_0

    .line 1499
    sget-object v0, Lbco;->O:Laqw;

    iput-object v0, p0, Lbco;->R:Laqw;

    .line 1503
    :goto_0
    return-void

    .line 1501
    :cond_0
    iput-object p1, p0, Lbco;->R:Laqw;

    goto :goto_0
.end method

.method public a(Larh;)V
    .locals 0

    .prologue
    .line 973
    if-nez p1, :cond_0

    sget-object p1, Lbco;->P:Larh;

    :cond_0
    iput-object p1, p0, Lbco;->S:Larh;

    .line 974
    return-void
.end method

.method public a(Laxs;)V
    .locals 1

    .prologue
    .line 1647
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lbco;->a(Laxs;Z)V

    .line 1648
    return-void
.end method

.method public a(Lbza;)V
    .locals 2

    .prologue
    .line 1782
    iget-object v0, p0, Lbco;->T:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    iget-object v1, p0, Lbco;->aJ:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->a(I)V

    .line 1783
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1255
    const-string v0, "title"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 1256
    iget-object v0, p0, Lbco;->ag:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;->setText(Ljava/lang/CharSequence;)V

    .line 1257
    invoke-direct {p0}, Lbco;->ac()V

    .line 1258
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbza;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1326
    iput-object p1, p0, Lbco;->aJ:Ljava/util/List;

    .line 1327
    iget-object v0, p0, Lbco;->T:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->a(Ljava/util/List;)V

    .line 1328
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 1240
    iput-boolean p1, p0, Lbco;->aL:Z

    .line 1241
    invoke-direct {p0}, Lbco;->Y()V

    .line 1242
    invoke-direct {p0}, Lbco;->X()V

    .line 1243
    invoke-direct {p0}, Lbco;->ad()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1244
    iget-object v1, p0, Lbco;->ag:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;->setVisibility(I)V

    .line 1246
    :cond_0
    return-void

    .line 1244
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public a(ZI)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1332
    if-eqz p1, :cond_1

    .line 1333
    iget-object v0, p0, Lbco;->ac:Landroid/widget/TextView;

    if-nez p2, :cond_0

    const p2, 0x7f0a00f6

    :cond_0
    invoke-virtual {p0, p2}, Lbco;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1335
    iget-object v0, p0, Lbco;->ab:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 1336
    iget-object v0, p0, Lbco;->ab:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1337
    iget-object v0, p0, Lbco;->ab:Landroid/view/View;

    invoke-static {v0}, Lcad;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 1341
    :goto_0
    return-void

    .line 1339
    :cond_1
    iget-object v0, p0, Lbco;->ab:Landroid/view/View;

    invoke-static {v0}, Lcad;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public aO_()V
    .locals 3

    .prologue
    .line 638
    invoke-super {p0}, Llol;->aO_()V

    .line 642
    invoke-direct {p0}, Lbco;->ad()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbco;->as:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->a()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 643
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d006f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 644
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d006e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    div-float/2addr v0, v1

    .line 645
    invoke-direct {p0, v0}, Lbco;->a(F)V

    .line 649
    :cond_0
    invoke-direct {p0}, Lbco;->Z()Loo;

    move-result-object v0

    const/high16 v1, 0x41800000    # 16.0f

    invoke-virtual {v0, v1}, Loo;->a(F)V

    .line 650
    return-void
.end method

.method public a_(Z)V
    .locals 1

    .prologue
    .line 697
    iget-object v0, p0, Lbco;->aY:Lbdv;

    invoke-interface {v0, p1}, Lbdv;->b(Z)V

    .line 698
    if-nez p1, :cond_0

    .line 699
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbco;->aK:Z

    .line 701
    :cond_0
    return-void
.end method

.method public ae_()V
    .locals 2

    .prologue
    .line 1194
    invoke-virtual {p0}, Lbco;->m()V

    .line 1195
    iget-object v0, p0, Lbco;->aO:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 1196
    iget-object v0, p0, Lbco;->aO:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 1197
    const/4 v0, 0x0

    iput-object v0, p0, Lbco;->aO:Landroid/animation/ObjectAnimator;

    .line 1199
    :cond_0
    iget-object v0, p0, Lbco;->bb:Laqc;

    invoke-virtual {v0, p0}, Laqc;->b(Laqv;)V

    .line 1200
    iget-object v0, p0, Lbco;->be:Lapu;

    iget-object v1, p0, Lbco;->aF:Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;

    invoke-virtual {v0, v1}, Lapu;->b(Lapy;)V

    .line 1201
    iget-object v0, p0, Lbco;->bc:Laqy;

    invoke-virtual {v0, p0}, Laqy;->b(Larg;)V

    .line 1202
    iget-object v0, p0, Lbco;->bk:Lamb;

    iget-object v1, p0, Lbco;->Q:Lamk;

    invoke-virtual {v0, v1}, Lamb;->b(Lamk;)V

    .line 1203
    iget-object v0, p0, Lbco;->bd:Lase;

    invoke-virtual {v0, p0}, Lase;->b(Lasi;)V

    .line 1204
    invoke-super {p0}, Llol;->ae_()V

    .line 1205
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 1148
    iget-object v0, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->a(Landroid/graphics/Bitmap;)V

    .line 1149
    return-void
.end method

.method public b(Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 781
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 782
    iget-object v1, p0, Lbco;->Z:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 783
    iget-object v1, p0, Lbco;->Z:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 784
    iget-object v1, p0, Lbco;->Z:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 785
    iget-object v1, p0, Lbco;->Z:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 786
    iget-object v1, p0, Lbco;->Z:Landroid/widget/ImageView;

    invoke-static {v1}, Lcad;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 787
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 788
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 789
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-long v2, v0

    .line 790
    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lbcy;

    invoke-direct {v2, p0, p1, v0}, Lbcy;-><init>(Lbco;Landroid/graphics/Bitmap;I)V

    .line 791
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 807
    return-void
.end method

.method public b(Laxs;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1640
    iget-boolean v0, p0, Lbco;->ba:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbco;->az:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbco;->aE:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_0

    .line 1641
    invoke-direct {p0, p1, v1}, Lbco;->a(Laxs;Z)V

    .line 1643
    :cond_0
    return-void

    .line 1640
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 1278
    iget-object v1, p0, Lbco;->am:Landroid/widget/ImageButton;

    if-eqz p1, :cond_1

    const v0, 0x7f0202a9

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 1282
    iput-boolean p1, p0, Lbco;->aM:Z

    .line 1283
    invoke-direct {p0}, Lbco;->Y()V

    .line 1285
    iget v0, p0, Lbco;->aZ:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 1286
    iget-object v0, p0, Lbco;->V:Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->setKeepScreenOn(Z)V

    .line 1290
    :cond_0
    :goto_1
    return-void

    .line 1278
    :cond_1
    const v0, 0x7f0202aa

    goto :goto_0

    .line 1287
    :cond_2
    iget v0, p0, Lbco;->aZ:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1288
    iget-object v0, p0, Lbco;->W:Landroid/view/TextureView;

    invoke-virtual {v0, p1}, Landroid/view/TextureView;->setKeepScreenOn(Z)V

    goto :goto_1
.end method

.method public b_(I)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 811
    iget-boolean v0, p0, Lbco;->aK:Z

    if-eqz v0, :cond_0

    .line 812
    invoke-direct {p0, v3}, Lbco;->s(Z)V

    .line 813
    iget-object v0, p0, Lbco;->Y:Landroid/widget/TextView;

    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a011e

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 814
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 813
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 816
    :cond_0
    return-void
.end method

.method public b_(Z)V
    .locals 1

    .prologue
    .line 1008
    iget-object v0, p0, Lbco;->aY:Lbdv;

    invoke-interface {v0, p1}, Lbdv;->e(Z)V

    .line 1009
    return-void
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 1507
    iget-object v0, p0, Lbco;->V:Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->a(I)V

    .line 1508
    iget-object v0, p0, Lbco;->R:Laqw;

    invoke-interface {v0}, Laqw;->h()V

    .line 1509
    return-void
.end method

.method public c(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 820
    iget-object v0, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->a(F)V

    .line 821
    iget-object v0, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    iget-object v1, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lbco;->a(Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;F)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->c(F)V

    .line 822
    iget-object v0, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->a(Landroid/graphics/Bitmap;)V

    .line 823
    iget-object v0, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->setVisibility(I)V

    .line 824
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 451
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 452
    invoke-static {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->b(Lu;)Lcom/google/android/apps/moviemaker/MovieMakerActivity;

    move-result-object v0

    .line 453
    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o()Laol;

    move-result-object v1

    if-nez v1, :cond_0

    .line 467
    :goto_0
    return-void

    .line 458
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o()Laol;

    move-result-object v0

    invoke-virtual {v0}, Laol;->c()Laqc;

    move-result-object v0

    invoke-virtual {v0}, Laqc;->m()Ljava/lang/String;

    move-result-object v0

    .line 459
    new-instance v1, Lhmg;

    new-instance v2, Lkrb;

    sget-object v3, Lomx;->c:Lhmn;

    new-array v4, v6, [Lkrc;

    const/4 v5, 0x0

    if-nez v0, :cond_1

    const-string v0, ""

    .line 461
    :cond_1
    invoke-static {v6, v0}, Lkrc;->a(ILjava/lang/String;)Lkrc;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-direct {v2, v3, v4}, Lkrb;-><init>(Lhmn;[Lkrc;)V

    invoke-direct {v1, v2}, Lhmg;-><init>(Lhmk;)V

    iget-object v0, p0, Lbco;->au:Llnh;

    .line 466
    invoke-virtual {v1, v0}, Lhmg;->a(Llnh;)Lhmg;

    goto :goto_0
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 705
    iget-object v0, p0, Lbco;->aY:Lbdv;

    invoke-interface {v0, p1}, Lbdv;->a(Z)V

    .line 706
    return-void
.end method

.method public c_(I)V
    .locals 1

    .prologue
    .line 1513
    iput p1, p0, Lbco;->aZ:I

    .line 1514
    iget-boolean v0, p0, Lbco;->aL:Z

    invoke-virtual {p0, v0}, Lbco;->a(Z)V

    .line 1515
    return-void
.end method

.method public c_(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/16 v3, 0x8

    .line 1306
    if-eqz p1, :cond_0

    iget v0, p0, Lbco;->aZ:I

    if-eq v0, v5, :cond_0

    iget v0, p0, Lbco;->aZ:I

    if-ne v0, v2, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    const-string v4, "mPlayerViewType must be SURFACE_VIEW or TEXTURE_VIEW when showing."

    invoke-static {v0, v4}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 1309
    if-eqz p1, :cond_1

    .line 1310
    iget-object v0, p0, Lbco;->ao:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->setVisibility(I)V

    .line 1311
    iget-object v0, p0, Lbco;->ay:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 1313
    :cond_1
    iget-object v4, p0, Lbco;->V:Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;

    if-eqz p1, :cond_3

    iget v0, p0, Lbco;->aZ:I

    if-ne v0, v5, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->setVisibility(I)V

    .line 1315
    iget-object v0, p0, Lbco;->W:Landroid/view/TextureView;

    if-eqz p1, :cond_4

    iget v4, p0, Lbco;->aZ:I

    if-ne v4, v2, :cond_4

    :goto_2
    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setVisibility(I)V

    .line 1317
    return-void

    :cond_2
    move v0, v1

    .line 1306
    goto :goto_0

    :cond_3
    move v0, v3

    .line 1313
    goto :goto_1

    :cond_4
    move v1, v3

    .line 1315
    goto :goto_2
.end method

.method public d()Lcdz;
    .locals 1

    .prologue
    .line 1223
    sget-object v0, Lcdz;->a:Lcdz;

    return-object v0
.end method

.method public d(I)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1526
    iget-object v0, p0, Lbco;->aH:Landroid/widget/TextView;

    .line 1527
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0136

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1526
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1528
    iget-object v0, p0, Lbco;->aI:Landroid/widget/TextView;

    .line 1529
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f110001

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    .line 1530
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    .line 1529
    invoke-virtual {v1, v2, p1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1528
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1531
    iget-object v0, p0, Lbco;->aG:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 1532
    return-void
.end method

.method public d_(Z)V
    .locals 2

    .prologue
    .line 1321
    iget-object v1, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->setVisibility(I)V

    .line 1322
    return-void

    .line 1321
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public e()V
    .locals 3

    .prologue
    .line 1536
    iget-object v0, p0, Lbco;->aH:Landroid/widget/TextView;

    .line 1537
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0139

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1536
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1538
    iget-object v0, p0, Lbco;->aI:Landroid/widget/TextView;

    .line 1539
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a013a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1538
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1540
    iget-object v0, p0, Lbco;->aG:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1541
    return-void
.end method

.method public e(I)V
    .locals 1

    .prologue
    .line 828
    iget-object v0, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    invoke-direct {p0, v0, p1}, Lbco;->a(Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;I)V

    .line 829
    return-void
.end method

.method public f(I)V
    .locals 1

    .prologue
    .line 849
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbco;->p(Z)V

    .line 850
    iget-object v0, p0, Lbco;->ao:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    invoke-direct {p0, v0, p1}, Lbco;->a(Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;I)V

    .line 851
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 1180
    invoke-super {p0}, Llol;->g()V

    .line 1181
    iget-object v0, p0, Lbco;->W:Landroid/view/TextureView;

    iget-object v1, p0, Lbco;->bf:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1182
    iget-object v0, p0, Lbco;->V:Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;

    iget-object v1, p0, Lbco;->bf:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1183
    return-void
.end method

.method public g(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1153
    invoke-super {p0, p1}, Llol;->g(Landroid/os/Bundle;)V

    .line 1155
    iget-object v0, p0, Lbco;->ag:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    iget-object v1, p0, Lbco;->bg:Landroid/widget/TextView$OnEditorActionListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 1156
    iget-object v0, p0, Lbco;->ag:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    iget-object v1, p0, Lbco;->bh:Lcal;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;->a(Lcal;)V

    .line 1158
    invoke-static {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->b(Lu;)Lcom/google/android/apps/moviemaker/MovieMakerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o()Laol;

    move-result-object v0

    invoke-virtual {v0}, Laol;->c()Laqc;

    move-result-object v0

    iput-object v0, p0, Lbco;->bb:Laqc;

    .line 1159
    iget-object v0, p0, Lbco;->bb:Laqc;

    invoke-virtual {v0, p0}, Laqc;->a(Laqv;)V

    .line 1161
    invoke-static {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->b(Lu;)Lcom/google/android/apps/moviemaker/MovieMakerActivity;

    move-result-object v0

    .line 1162
    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o()Laol;

    move-result-object v0

    invoke-virtual {v0}, Laol;->j()Lapu;

    move-result-object v0

    iput-object v0, p0, Lbco;->be:Lapu;

    .line 1163
    iget-object v0, p0, Lbco;->be:Lapu;

    iget-object v1, p0, Lbco;->aF:Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;

    invoke-virtual {v0, v1}, Lapu;->a(Lapy;)V

    .line 1165
    invoke-static {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->b(Lu;)Lcom/google/android/apps/moviemaker/MovieMakerActivity;

    move-result-object v0

    .line 1166
    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o()Laol;

    move-result-object v0

    invoke-virtual {v0}, Laol;->h()Laqy;

    move-result-object v0

    iput-object v0, p0, Lbco;->bc:Laqy;

    .line 1167
    iget-object v0, p0, Lbco;->bc:Laqy;

    invoke-virtual {v0, p0}, Laqy;->a(Larg;)V

    .line 1169
    invoke-static {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->b(Lu;)Lcom/google/android/apps/moviemaker/MovieMakerActivity;

    move-result-object v0

    .line 1170
    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o()Laol;

    move-result-object v0

    invoke-virtual {v0}, Laol;->i()Lamb;

    move-result-object v0

    iput-object v0, p0, Lbco;->bk:Lamb;

    .line 1171
    iget-object v0, p0, Lbco;->bk:Lamb;

    iget-object v1, p0, Lbco;->Q:Lamk;

    invoke-virtual {v0, v1}, Lamb;->a(Lamk;)V

    .line 1173
    invoke-static {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->b(Lu;)Lcom/google/android/apps/moviemaker/MovieMakerActivity;

    move-result-object v0

    .line 1174
    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o()Laol;

    move-result-object v0

    invoke-virtual {v0}, Laol;->k()Lase;

    move-result-object v0

    iput-object v0, p0, Lbco;->bd:Lase;

    .line 1175
    iget-object v0, p0, Lbco;->bd:Lase;

    invoke-virtual {v0, p0}, Lase;->a(Lasi;)V

    .line 1176
    return-void
.end method

.method public h(Z)Landroid/view/animation/Animation;
    .locals 8

    .prologue
    const/4 v5, 0x2

    const-wide/16 v6, 0x1f4

    const/high16 v4, 0x3f800000    # 1.0f

    .line 1750
    invoke-static {}, Lcad;->a()Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 1751
    iget-object v0, p0, Lbco;->bj:Landroid/util/Property;

    invoke-virtual {v1, v0}, Landroid/animation/ObjectAnimator;->setProperty(Landroid/util/Property;)V

    .line 1752
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbco;->aV:Z

    .line 1754
    if-eqz p1, :cond_0

    .line 1755
    new-array v0, v5, [F

    fill-array-data v0, :array_0

    invoke-virtual {v1, v0}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 1756
    iget-object v0, p0, Lbco;->bj:Landroid/util/Property;

    invoke-virtual {p0}, Lbco;->x()Landroid/view/View;

    move-result-object v2

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/util/Property;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1757
    invoke-virtual {v1, v6, v7}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 1758
    invoke-virtual {v1, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1759
    const/16 v0, 0x3e8

    .line 1766
    :goto_0
    iget-object v2, p0, Lbco;->bi:Landroid/animation/AnimatorListenerAdapter;

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1767
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 1770
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, v4, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1771
    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1772
    return-object v1

    .line 1761
    :cond_0
    const-wide/16 v2, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 1762
    invoke-virtual {v1, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1763
    const/16 v0, 0x1f4

    .line 1764
    new-array v2, v5, [F

    fill-array-data v2, :array_1

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    goto :goto_0

    .line 1755
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 1764
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public h()V
    .locals 2

    .prologue
    .line 1187
    iget-object v0, p0, Lbco;->V:Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;

    iget-object v1, p0, Lbco;->bf:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1188
    iget-object v0, p0, Lbco;->W:Landroid/view/TextureView;

    iget-object v1, p0, Lbco;->bf:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 1189
    invoke-super {p0}, Llol;->h()V

    .line 1190
    return-void
.end method

.method public i(Z)V
    .locals 2

    .prologue
    .line 1025
    iget-object v1, p0, Lbco;->as:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->setVisibility(I)V

    .line 1026
    return-void

    .line 1025
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public j(Z)V
    .locals 2

    .prologue
    .line 1777
    iget-object v1, p0, Lbco;->T:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->setVisibility(I)V

    .line 1778
    return-void

    .line 1777
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public k(Z)V
    .locals 1

    .prologue
    .line 710
    iget-object v0, p0, Lbco;->aY:Lbdv;

    invoke-interface {v0, p1}, Lbdv;->f(Z)V

    .line 711
    return-void
.end method

.method public k_()V
    .locals 3

    .prologue
    .line 833
    iget-object v0, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 834
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 835
    iget-object v1, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {p0, v1, v2, v0}, Lbco;->a(Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;FI)V

    .line 837
    :cond_0
    return-void
.end method

.method public l()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 1013
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1014
    iget-object v1, p0, Lbco;->aw:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 1015
    return-object v0
.end method

.method public l(Z)V
    .locals 0

    .prologue
    .line 1228
    iput-boolean p1, p0, Lbco;->aS:Z

    .line 1229
    invoke-direct {p0}, Lbco;->X()V

    .line 1230
    return-void
.end method

.method public l_()V
    .locals 3

    .prologue
    .line 841
    iget-object v0, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 842
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 843
    iget-object v1, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, v0}, Lbco;->a(Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;FI)V

    .line 845
    :cond_0
    return-void
.end method

.method public m()V
    .locals 3

    .prologue
    .line 1268
    iget-object v0, p0, Lbco;->ag:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    if-eqz v0, :cond_0

    .line 1271
    iget-object v0, p0, Lbco;->R:Laqw;

    iget-object v1, p0, Lbco;->ag:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    invoke-virtual {v1}, Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Laqw;->a(Ljava/lang/String;)V

    .line 1273
    :cond_0
    invoke-virtual {p0}, Lbco;->x()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lbco;->n()Lz;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Lz;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 1274
    :cond_1
    return-void

    .line 1273
    :cond_2
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0
.end method

.method public m(Z)V
    .locals 2

    .prologue
    .line 1842
    const v0, 0x7f100388

    invoke-direct {p0, v0}, Lbco;->g(I)Landroid/view/View;

    move-result-object v1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1843
    return-void

    .line 1842
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public m_()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    .line 866
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 868
    iget-object v1, p0, Lbco;->ao:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    invoke-direct {p0, v1, v3, v0}, Lbco;->a(Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;FI)V

    .line 871
    iget-object v1, p0, Lbco;->Z:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 872
    iget-object v1, p0, Lbco;->Z:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 873
    iget-object v1, p0, Lbco;->Z:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 874
    iget-object v1, p0, Lbco;->Z:Landroid/widget/ImageView;

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 875
    iget-object v1, p0, Lbco;->Z:Landroid/widget/ImageView;

    invoke-static {v1}, Lcad;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 876
    invoke-virtual {v1, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 877
    invoke-virtual {v1, v3}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 878
    invoke-virtual {v1, v3}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-long v2, v0

    .line 879
    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lbcz;

    invoke-direct {v2, p0, v0}, Lbcz;-><init>(Lbco;I)V

    .line 880
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 904
    return-void
.end method

.method public n(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 986
    invoke-direct {p0, p1}, Lbco;->p(Z)V

    .line 987
    iget-object v3, p0, Lbco;->aq:Landroid/view/View;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 988
    iget-object v0, p0, Lbco;->ao:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    if-eqz p1, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->setVisibility(I)V

    .line 989
    iget-object v0, p0, Lbco;->ao:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    iget-object v1, p0, Lbco;->ao:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lbco;->a(Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;F)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->c(F)V

    .line 990
    if-eqz p1, :cond_0

    .line 991
    iget-object v0, p0, Lbco;->an:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 992
    iget-object v1, p0, Lbco;->ao:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->a(Landroid/graphics/Bitmap;)V

    .line 994
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 987
    goto :goto_0

    :cond_2
    move v1, v2

    .line 988
    goto :goto_1
.end method

.method public n_()V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 908
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 910
    iget-object v1, p0, Lbco;->ao:Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    invoke-direct {p0, v1, v3, v0}, Lbco;->a(Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;FI)V

    .line 913
    iget-object v1, p0, Lbco;->aa:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 914
    iget-object v1, p0, Lbco;->aa:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 915
    iget-object v1, p0, Lbco;->aa:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 916
    iget-object v1, p0, Lbco;->aa:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 917
    iget-object v1, p0, Lbco;->aa:Landroid/widget/ImageView;

    invoke-static {v1}, Lcad;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 918
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 919
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    .line 920
    invoke-virtual {v1, v4}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-long v2, v0

    .line 921
    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    new-instance v2, Lbdc;

    invoke-direct {v2, p0, v0}, Lbdc;-><init>(Lbco;I)V

    .line 922
    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 954
    return-void
.end method

.method public o(Z)V
    .locals 0

    .prologue
    .line 1234
    iput-boolean p1, p0, Lbco;->aR:Z

    .line 1235
    invoke-direct {p0}, Lbco;->X()V

    .line 1236
    return-void
.end method

.method public o_()V
    .locals 3

    .prologue
    .line 1545
    iget-object v0, p0, Lbco;->aH:Landroid/widget/TextView;

    .line 1546
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a013b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1545
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1547
    iget-object v0, p0, Lbco;->aI:Landroid/widget/TextView;

    .line 1548
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a013c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1547
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1549
    iget-object v0, p0, Lbco;->aG:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1550
    return-void
.end method

.method public p_()V
    .locals 3

    .prologue
    .line 1554
    iget-object v0, p0, Lbco;->aH:Landroid/widget/TextView;

    .line 1555
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0137

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1554
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1556
    iget-object v0, p0, Lbco;->aI:Landroid/widget/TextView;

    .line 1557
    invoke-virtual {p0}, Lbco;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0138

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1556
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1558
    iget-object v0, p0, Lbco;->aG:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1559
    return-void
.end method

.method public q_()V
    .locals 2

    .prologue
    .line 1563
    iget-object v0, p0, Lbco;->aG:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1564
    return-void
.end method

.method public r_()V
    .locals 2

    .prologue
    .line 1710
    iget-object v0, p0, Lbco;->az:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbco;->az:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lbco;->ba:Z

    if-nez v0, :cond_1

    .line 1741
    :cond_0
    :goto_0
    return-void

    .line 1715
    :cond_1
    iget-object v0, p0, Lbco;->aB:Landroid/view/View;

    const v1, 0x7f05000e

    invoke-static {v0, v1}, Lcad;->a(Landroid/view/View;I)Landroid/animation/Animator;

    move-result-object v0

    .line 1717
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 1719
    iget-object v0, p0, Lbco;->aC:Landroid/view/View;

    const v1, 0x7f050010

    invoke-static {v0, v1}, Lcad;->a(Landroid/view/View;I)Landroid/animation/Animator;

    move-result-object v0

    .line 1721
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 1724
    invoke-virtual {p0}, Lbco;->n()Lz;

    move-result-object v0

    const v1, 0x7f050011

    .line 1723
    invoke-static {v0, v1}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    iput-object v0, p0, Lbco;->aQ:Landroid/animation/Animator;

    .line 1725
    iget-object v0, p0, Lbco;->aQ:Landroid/animation/Animator;

    iget-object v1, p0, Lbco;->az:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setTarget(Ljava/lang/Object;)V

    .line 1726
    iget-object v0, p0, Lbco;->aQ:Landroid/animation/Animator;

    new-instance v1, Lbdj;

    invoke-direct {v1, p0}, Lbdj;-><init>(Lbco;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 1740
    iget-object v0, p0, Lbco;->aQ:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0
.end method

.method public s_()V
    .locals 1

    .prologue
    .line 1745
    iget-object v0, p0, Lbco;->T:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->l()V

    .line 1746
    return-void
.end method

.method public t_()V
    .locals 2

    .prologue
    .line 1837
    const/4 v0, 0x1

    const v1, 0x7f0a011a

    invoke-virtual {p0, v0, v1}, Lbco;->a(ZI)V

    .line 1838
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1624
    iget-object v0, p0, Lbco;->as:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    if-eqz v0, :cond_0

    .line 1625
    iget-object v0, p0, Lbco;->as:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 1626
    const-class v1, Lbco;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 1627
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    .line 1626
    invoke-static {v1, v2}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1629
    :goto_0
    return-object v0

    :cond_0
    const-class v0, Lbco;

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public z()V
    .locals 2

    .prologue
    .line 654
    invoke-virtual {p0}, Lbco;->m()V

    .line 656
    iget-object v0, p0, Lbco;->aX:Landroid/os/Handler;

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 657
    invoke-super {p0}, Llol;->z()V

    .line 658
    return-void
.end method
