.class public final Lazn;
.super Layr;
.source "PG"


# instance fields
.field private a:Lbaf;

.field private b:Landroid/animation/TimeInterpolator;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 33
    new-instance v0, Lays;

    invoke-direct {v0}, Lays;-><init>()V

    const v1, 0x7f02042d

    .line 34
    invoke-virtual {v0, v1}, Lays;->a(I)Lays;

    move-result-object v0

    const v1, 0x7f020594

    .line 35
    invoke-virtual {v0, v1}, Lays;->b(I)Lays;

    move-result-object v0

    const/4 v1, 0x2

    .line 36
    invoke-virtual {v0, v1}, Lays;->c(I)Lays;

    move-result-object v0

    const v1, 0x3dcccccd    # 0.1f

    .line 37
    invoke-virtual {v0, v1}, Lays;->a(F)Lays;

    move-result-object v0

    const v1, 0x7f02058b

    .line 38
    invoke-virtual {v0, v1}, Lays;->d(I)Lays;

    move-result-object v0

    const/4 v1, 0x5

    .line 39
    invoke-virtual {v0, v1}, Lays;->e(I)Lays;

    move-result-object v0

    sget-object v1, Layc;->b:Layc;

    .line 40
    invoke-virtual {v0, v1}, Lays;->a(Layc;)Lays;

    move-result-object v0

    .line 33
    invoke-direct {p0, v0}, Layr;-><init>(Lays;)V

    .line 28
    new-instance v0, Lbao;

    const-wide/32 v2, 0x61a80

    invoke-direct {v0, v2, v3}, Lbao;-><init>(J)V

    iput-object v0, p0, Lazn;->a:Lbaf;

    .line 30
    new-instance v0, Lbai;

    invoke-direct {v0}, Lbai;-><init>()V

    iput-object v0, p0, Lazn;->b:Landroid/animation/TimeInterpolator;

    .line 42
    return-void
.end method


# virtual methods
.method protected a(Layj;Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;
    .locals 4

    .prologue
    .line 47
    new-instance v0, Layu;

    invoke-direct {v0}, Layu;-><init>()V

    .line 48
    invoke-virtual {v0, p1}, Layu;->a(Layj;)Layu;

    move-result-object v0

    const v1, 0x7f0b00aa

    .line 49
    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Layu;->a(I)Layu;

    move-result-object v0

    const/high16 v1, 0x42100000    # 36.0f

    .line 50
    invoke-virtual {v0, v1}, Layu;->a(F)Layu;

    move-result-object v0

    const/high16 v1, 0x42300000    # 44.0f

    .line 51
    invoke-virtual {v0, v1}, Layu;->b(F)Layu;

    move-result-object v0

    const-string v1, "sans-serif-condensed"

    const/4 v2, 0x1

    .line 52
    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Layu;->a(Landroid/graphics/Typeface;)Layu;

    move-result-object v0

    const v1, 0x7f0b00ab

    .line 53
    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Layu;->h(I)Layu;

    move-result-object v0

    const-wide/32 v2, 0xf4240

    .line 54
    invoke-virtual {v0, v2, v3}, Layu;->b(J)Layu;

    move-result-object v0

    sget-object v1, Layc;->d:Layc;

    .line 55
    invoke-virtual {v0, v1}, Layu;->a(Layc;)Layu;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Layu;->a()Layt;

    move-result-object v0

    return-object v0
.end method

.method public b(Lbmd;)Lool;
    .locals 9

    .prologue
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 62
    iget-object v2, p1, Lbmd;->d:Lbmg;

    sget-object v3, Lbmg;->c:Lbmg;

    if-eq v2, v3, :cond_0

    .line 63
    const/4 v0, 0x0

    .line 65
    :goto_0
    return-object v0

    :cond_0
    const-wide v2, 0x3fa999999999999aL    # 0.05

    const-wide/16 v4, 0x0

    move-wide v6, v0

    move-object v8, p1

    invoke-static/range {v0 .. v8}, Lbah;->a(DDDDLbmd;)Lool;

    move-result-object v0

    goto :goto_0
.end method

.method protected f()Layj;
    .locals 1

    .prologue
    .line 84
    new-instance v0, Lazc;

    invoke-direct {v0, p0}, Lazc;-><init>(Layj;)V

    return-object v0
.end method

.method public v(Layl;)F
    .locals 2

    .prologue
    .line 71
    iget-boolean v0, p1, Layl;->w:Z

    if-eqz v0, :cond_0

    .line 72
    const/4 v0, 0x0

    .line 79
    :goto_0
    return v0

    .line 75
    :cond_0
    iget-object v0, p0, Lazn;->a:Lbaf;

    invoke-virtual {v0, p1}, Lbaf;->a(Layl;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    iget-object v0, p0, Lazn;->b:Landroid/animation/TimeInterpolator;

    iget-object v1, p0, Lazn;->a:Lbaf;

    invoke-virtual {v1, p1}, Lbaf;->b(Layl;)F

    move-result v1

    invoke-interface {v0, v1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    goto :goto_0

    .line 79
    :cond_1
    invoke-super {p0, p1}, Layr;->v(Layl;)F

    move-result v0

    goto :goto_0
.end method
