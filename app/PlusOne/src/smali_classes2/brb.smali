.class public final Lbrb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILandroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-string v0, "musicCacheSize"

    invoke-static {p1, v0, v1}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lbrb;->a:I

    .line 21
    const-string v0, "context"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbrb;->b:Landroid/content/Context;

    .line 22
    const-string v0, "dirName"

    invoke-static {p3, v0}, Lcec;->a(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbrb;->c:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public a(Lbqz;)Lbqw;
    .locals 5

    .prologue
    .line 30
    new-instance v0, Lbqw;

    iget v1, p0, Lbrb;->a:I

    iget-object v2, p0, Lbrb;->b:Landroid/content/Context;

    iget-object v3, p0, Lbrb;->c:Ljava/lang/String;

    const/4 v4, 0x0

    .line 31
    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v2

    invoke-direct {v0, v1, p1, v2}, Lbqw;-><init>(ILbqz;Ljava/io/File;)V

    return-object v0
.end method
