.class public Lbeb;
.super Llol;
.source "PG"

# interfaces
.implements Lauw;
.implements Lbfs;


# static fields
.field private static final N:Laux;


# instance fields
.field private O:Laum;

.field private P:Laux;

.field private Q:Z

.field private R:Z

.field private S:Z

.field private T:Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;

.field private U:Lboh;

.field private V:Lbei;

.field private W:Landroid/view/View;

.field private X:Landroid/widget/FrameLayout;

.field private Y:Landroid/view/View;

.field private Z:Lccc;

.field private aa:Landroid/widget/LinearLayout;

.field private ab:Landroid/view/View;

.field private ac:Landroid/view/View;

.field private ad:Ljava/lang/Runnable;

.field private ae:Lcdn;

.field private final af:Landroid/view/View$OnClickListener;

.field private final ag:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    const-class v0, Lbeb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 76
    const-class v0, Laux;

    .line 77
    invoke-static {v0}, Lcgf;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laux;

    sput-object v0, Lbeb;->N:Laux;

    .line 76
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 55
    invoke-direct {p0}, Llol;-><init>()V

    .line 80
    sget-object v0, Lbeb;->N:Laux;

    iput-object v0, p0, Lbeb;->P:Laux;

    .line 83
    iput-boolean v2, p0, Lbeb;->S:Z

    .line 105
    new-instance v0, Lbec;

    invoke-direct {v0, p0}, Lbec;-><init>(Lbeb;)V

    iput-object v0, p0, Lbeb;->ae:Lcdn;

    .line 114
    new-instance v0, Lbed;

    invoke-direct {v0, p0}, Lbed;-><init>(Lbeb;)V

    iput-object v0, p0, Lbeb;->af:Landroid/view/View$OnClickListener;

    .line 121
    new-instance v0, Lbee;

    invoke-direct {v0, p0}, Lbee;-><init>(Lbeb;)V

    iput-object v0, p0, Lbeb;->ag:Landroid/view/View$OnClickListener;

    .line 129
    new-instance v0, Lhmg;

    sget-object v1, Lomx;->a:Lhmn;

    invoke-direct {v0, v1}, Lhmg;-><init>(Lhmn;)V

    iget-object v1, p0, Lbeb;->au:Llnh;

    invoke-virtual {v0, v1}, Lhmg;->a(Llnh;)Lhmg;

    .line 130
    new-instance v0, Lhmf;

    iget-object v1, p0, Lbeb;->av:Llqm;

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 288
    return-void
.end method

.method static synthetic a(Lbeb;)Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lbeb;->T:Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;

    return-object v0
.end method

.method static synthetic a(Lbeb;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lbeb;->ad:Ljava/lang/Runnable;

    return-object p1
.end method

.method private a()Loo;
    .locals 1

    .prologue
    .line 275
    invoke-virtual {p0}, Lbeb;->n()Lz;

    move-result-object v0

    check-cast v0, Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbeb;Lcbk;Lauq;ZLcbs;ZLcbr;)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v3, -0x2

    const/4 v2, 0x0

    .line 55
    const-string v0, "view"

    invoke-static {p1, v0, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    const-string v0, "genreEntry"

    invoke-static {p2, v0, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    const v0, 0x7f100004

    invoke-virtual {p1, v0, p2}, Lcbk;->setTag(ILjava/lang/Object;)V

    invoke-virtual {p1, p4}, Lcbk;->a(Lcbs;)V

    iget-object v0, p0, Lbeb;->af:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcbk;->a(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lbeb;->ag:Landroid/view/View$OnClickListener;

    invoke-virtual {p1, v0}, Lcbk;->b(Landroid/view/View$OnClickListener;)V

    if-eqz p3, :cond_0

    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v0}, Lcbk;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    iget-object v0, p2, Lauq;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcbk;->a(Ljava/lang/String;)V

    iget-object v0, p2, Lauq;->c:Ljava/util/List;

    iget-object v3, p0, Lbeb;->U:Lboh;

    iget-boolean v4, p0, Lbeb;->Q:Z

    iget-boolean v5, p0, Lbeb;->R:Z

    invoke-virtual {p1, v0, v3, v4, v5}, Lcbk;->a(Ljava/util/List;Lboh;ZZ)V

    if-nez p5, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Lcbk;->c(Z)V

    iget-wide v4, p2, Lauq;->a:J

    invoke-virtual {p1, v4, v5}, Lcbk;->a(J)V

    invoke-virtual {p1, p6}, Lcbk;->a(Lcbr;)V

    invoke-static {p2}, Lbeb;->b(Lauq;)Z

    move-result v0

    invoke-virtual {p1, v0}, Lcbk;->d(Z)V

    invoke-static {p2}, Lbeb;->b(Lauq;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lbeb;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0d005a

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_1
    invoke-virtual {p0}, Lbeb;->o()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_3

    :goto_2
    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lcbk;->getPaddingTop()I

    move-result v1

    invoke-virtual {p1}, Lcbk;->getPaddingBottom()I

    move-result v3

    invoke-virtual {p1, v2, v1, v0, v3}, Lcbk;->setPadding(IIII)V

    :goto_3
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v1, v2

    goto :goto_2

    :cond_4
    invoke-virtual {p1}, Lcbk;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p1}, Lcbk;->getPaddingRight()I

    move-result v3

    invoke-virtual {p1, v1, v2, v3, v0}, Lcbk;->setPadding(IIII)V

    goto :goto_3
.end method

.method static synthetic a(Lbeb;ZZ)V
    .locals 5

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 55
    if-eqz p1, :cond_0

    if-eqz p2, :cond_1

    iget-object v0, p0, Lbeb;->aa:Landroid/widget/LinearLayout;

    move-object v3, v0

    :goto_0
    if-eqz p2, :cond_2

    iget-object v0, p0, Lbeb;->ab:Landroid/view/View;

    :goto_1
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-static {v3}, Lcad;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    invoke-static {v0}, Lcad;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v3

    new-instance v4, Lbeh;

    invoke-direct {v4, v0}, Lbeh;-><init>(Landroid/view/View;)V

    invoke-virtual {v3, v4}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    :cond_0
    iget-object v3, p0, Lbeb;->ac:Landroid/view/View;

    if-eqz p1, :cond_3

    if-eqz p2, :cond_3

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v3, v0}, Landroid/view/View;->setClickable(Z)V

    iget-object v3, p0, Lbeb;->aa:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_4

    if-eqz p2, :cond_4

    move v0, v1

    :goto_3
    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lbeb;->ab:Landroid/view/View;

    if-eqz p1, :cond_5

    if-nez p2, :cond_5

    :goto_4
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_1
    iget-object v0, p0, Lbeb;->ab:Landroid/view/View;

    move-object v3, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lbeb;->aa:Landroid/widget/LinearLayout;

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_3

    :cond_5
    move v1, v2

    goto :goto_4
.end method

.method static synthetic a(Lauq;)Z
    .locals 1

    .prologue
    .line 55
    invoke-static {p0}, Lbeb;->b(Lauq;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lbeb;Lauq;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-static {p1}, Lbeb;->b(Lauq;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p1, Lauq;->c:Ljava/util/List;

    iget-object v2, p0, Lbeb;->U:Lboh;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lbeb;)Lccc;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lbeb;->Z:Lccc;

    return-object v0
.end method

.method private static b(Lauq;)Z
    .locals 4

    .prologue
    .line 689
    iget-wide v0, p0, Lauq;->a:J

    const-wide/16 v2, -0x2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lauq;->a:J

    const-wide/16 v2, -0x3

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)Landroid/view/View;
    .locals 4

    .prologue
    .line 698
    iget-object v0, p0, Lbeb;->W:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 699
    invoke-virtual {p0}, Lbeb;->o()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x13

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "View "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 698
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method static synthetic c(Lbeb;)Laux;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lbeb;->P:Laux;

    return-object v0
.end method

.method static synthetic d(Lbeb;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lbeb;->ad:Ljava/lang/Runnable;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    .line 279
    invoke-direct {p0}, Lbeb;->a()Loo;

    move-result-object v0

    const-string v1, "actionBar"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loo;

    .line 280
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Loo;->e(Z)V

    .line 281
    return-void
.end method

.method static synthetic e(Lbeb;)Z
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    return v0
.end method

.method private i(Z)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x320

    const/4 v4, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x2

    .line 622
    invoke-static {}, Lcad;->a()Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 623
    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setProperty(Landroid/util/Property;)V

    .line 624
    iget-object v1, p0, Lbeb;->Y:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 625
    const-wide/16 v2, 0x96

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 626
    if-eqz p1, :cond_0

    .line 627
    new-array v1, v5, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 632
    :goto_0
    invoke-static {}, Lcad;->a()Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 633
    sget-object v2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setProperty(Landroid/util/Property;)V

    .line 634
    iget-object v2, p0, Lbeb;->X:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 635
    invoke-virtual {v1, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 636
    invoke-virtual {p0}, Lbeb;->o()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v2, v2

    const v3, 0x3f28f5c3    # 0.66f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    .line 638
    if-eqz p1, :cond_1

    .line 639
    new-array v3, v5, [F

    int-to-float v2, v2

    aput v2, v3, v6

    aput v4, v3, v7

    invoke-virtual {v1, v3}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 644
    :goto_1
    invoke-static {}, Lcad;->a()Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 645
    sget-object v3, Landroid/view/View;->ALPHA:Landroid/util/Property;

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setProperty(Landroid/util/Property;)V

    .line 646
    iget-object v3, p0, Lbeb;->X:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 647
    invoke-virtual {v2, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 648
    if-eqz p1, :cond_2

    .line 649
    new-array v3, v5, [F

    fill-array-data v3, :array_1

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 654
    :goto_2
    new-instance v3, Landroid/animation/AnimatorSet;

    invoke-direct {v3}, Landroid/animation/AnimatorSet;-><init>()V

    .line 655
    const/4 v4, 0x3

    new-array v4, v4, [Landroid/animation/Animator;

    aput-object v0, v4, v6

    aput-object v1, v4, v7

    aput-object v2, v4, v5

    invoke-virtual {v3, v4}, Landroid/animation/AnimatorSet;->playTogether([Landroid/animation/Animator;)V

    .line 656
    invoke-virtual {v3}, Landroid/animation/AnimatorSet;->start()V

    .line 657
    return-void

    .line 629
    :cond_0
    new-array v1, v5, [F

    fill-array-data v1, :array_2

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    goto :goto_0

    .line 641
    :cond_1
    new-array v3, v5, [F

    aput v4, v3, v6

    int-to-float v2, v2

    aput v2, v3, v7

    invoke-virtual {v1, v3}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    goto :goto_1

    .line 651
    :cond_2
    new-array v3, v5, [F

    fill-array-data v3, :array_3

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    goto :goto_2

    .line 627
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 649
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 629
    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 651
    :array_3
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method


# virtual methods
.method public V()V
    .locals 0

    .prologue
    .line 197
    invoke-direct {p0}, Lbeb;->d()V

    .line 198
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 165
    const v0, 0x7f0401ed

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbeb;->W:Landroid/view/View;

    .line 167
    const v0, 0x7f1005ab

    invoke-direct {p0, v0}, Lbeb;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbeb;->Y:Landroid/view/View;

    .line 168
    const v0, 0x7f1005ac

    invoke-direct {p0, v0}, Lbeb;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lbeb;->X:Landroid/widget/FrameLayout;

    .line 169
    const v0, 0x7f1005ad

    invoke-direct {p0, v0}, Lbeb;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lccc;

    iput-object v0, p0, Lbeb;->Z:Lccc;

    .line 171
    const v0, 0x7f1005ae

    invoke-direct {p0, v0}, Lbeb;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbeb;->ab:Landroid/view/View;

    .line 172
    const v0, 0x7f1005af

    .line 173
    invoke-direct {p0, v0}, Lbeb;->c(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbeb;->aa:Landroid/widget/LinearLayout;

    .line 174
    const v0, 0x7f1005b0

    invoke-direct {p0, v0}, Lbeb;->c(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbeb;->ac:Landroid/view/View;

    .line 175
    iget-object v0, p0, Lbeb;->ac:Landroid/view/View;

    new-instance v1, Lbef;

    invoke-direct {v1, p0}, Lbef;-><init>(Lbeb;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 184
    invoke-direct {p0}, Lbeb;->d()V

    .line 186
    iget-object v0, p0, Lbeb;->W:Landroid/view/View;

    const v1, 0x7f1005b1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;

    iput-object v0, p0, Lbeb;->T:Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;

    .line 188
    invoke-direct {p0}, Lbeb;->a()Loo;

    move-result-object v0

    .line 189
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Loo;->a(F)V

    .line 191
    iget-object v0, p0, Lbeb;->W:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 192
    iget-object v0, p0, Lbeb;->W:Landroid/view/View;

    return-object v0
.end method

.method public a(Laux;)V
    .locals 0

    .prologue
    .line 215
    if-nez p1, :cond_0

    sget-object p1, Lbeb;->N:Laux;

    :cond_0
    iput-object p1, p0, Lbeb;->P:Laux;

    .line 216
    return-void
.end method

.method public a(Lboh;ZZ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 220
    iget-object v0, p0, Lbeb;->U:Lboh;

    if-eq v0, p1, :cond_1

    const/4 v0, 0x1

    move v1, v0

    .line 221
    :goto_0
    iput-object p1, p0, Lbeb;->U:Lboh;

    .line 222
    iput-boolean p2, p0, Lbeb;->Q:Z

    .line 223
    iput-boolean p3, p0, Lbeb;->R:Z

    .line 224
    :goto_1
    iget-object v0, p0, Lbeb;->Z:Lccc;

    invoke-virtual {v0}, Lccc;->getChildCount()I

    move-result v0

    if-ge v2, v0, :cond_2

    .line 225
    iget-object v0, p0, Lbeb;->Z:Lccc;

    invoke-virtual {v0, v2}, Lccc;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lcbk;

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lbeb;->Z:Lccc;

    invoke-virtual {v0, v2}, Lccc;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcbk;

    invoke-virtual {v0, p1, v1, p2, p3}, Lcbk;->a(Lboh;ZZZ)V

    .line 224
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move v1, v2

    .line 220
    goto :goto_0

    .line 230
    :cond_2
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lauq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 204
    if-eqz p1, :cond_0

    .line 205
    iget-object v0, p0, Lbeb;->V:Lbei;

    invoke-virtual {v0, p1}, Lbei;->a(Ljava/util/List;)V

    .line 209
    :cond_0
    iget-object v0, p0, Lbeb;->V:Lbei;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbei;->c(Z)V

    .line 210
    iget-object v0, p0, Lbeb;->V:Lbei;

    invoke-virtual {v0}, Lbei;->notifyDataSetChanged()V

    .line 211
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 746
    iget-object v0, p0, Lbeb;->V:Lbei;

    if-eqz v0, :cond_0

    .line 747
    iget-object v0, p0, Lbeb;->V:Lbei;

    invoke-virtual {v0, p1}, Lbei;->a(Z)V

    .line 749
    :cond_0
    return-void
.end method

.method public a(ZZ)V
    .locals 4

    .prologue
    .line 252
    iget-object v0, p0, Lbeb;->ad:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lbeb;->W:Landroid/view/View;

    iget-object v1, p0, Lbeb;->ad:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 254
    const/4 v0, 0x0

    iput-object v0, p0, Lbeb;->ad:Ljava/lang/Runnable;

    .line 256
    :cond_0
    new-instance v0, Lbeg;

    invoke-direct {v0, p0, p1, p2}, Lbeg;-><init>(Lbeb;ZZ)V

    .line 265
    if-eqz p2, :cond_1

    if-eqz p1, :cond_1

    .line 266
    iput-object v0, p0, Lbeb;->ad:Ljava/lang/Runnable;

    .line 268
    iget-object v1, p0, Lbeb;->W:Landroid/view/View;

    const-wide/16 v2, 0x320

    invoke-virtual {v1, v0, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 272
    :goto_0
    return-void

    .line 270
    :cond_1
    iget-object v1, p0, Lbeb;->W:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public ae_()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lbeb;->O:Laum;

    invoke-virtual {v0, p0}, Laum;->b(Lauw;)V

    .line 140
    invoke-direct {p0}, Lbeb;->a()Loo;

    move-result-object v0

    .line 141
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Loo;->a(Ljava/lang/CharSequence;)V

    .line 142
    invoke-super {p0}, Llol;->ae_()V

    .line 143
    return-void
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 753
    iget-object v0, p0, Lbeb;->V:Lbei;

    if-eqz v0, :cond_0

    .line 754
    iget-object v0, p0, Lbeb;->V:Lbei;

    invoke-virtual {v0, p1}, Lbei;->b(Z)V

    .line 756
    :cond_0
    return-void
.end method

.method public c(Z)V
    .locals 0

    .prologue
    .line 134
    iput-boolean p1, p0, Lbeb;->S:Z

    .line 135
    return-void
.end method

.method public g(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 234
    invoke-super {p0, p1}, Llol;->g(Landroid/os/Bundle;)V

    .line 236
    new-instance v0, Lbei;

    invoke-virtual {p0}, Lbeb;->n()Lz;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lbei;-><init>(Lbeb;Landroid/content/Context;)V

    iput-object v0, p0, Lbeb;->V:Lbei;

    .line 237
    iget-object v0, p0, Lbeb;->Z:Lccc;

    iget-object v1, p0, Lbeb;->V:Lbei;

    invoke-virtual {v0, v1}, Lccc;->setAdapter(Landroid/widget/Adapter;)V

    .line 238
    iget-object v0, p0, Lbeb;->Z:Lccc;

    const-class v1, Lcdm;

    invoke-virtual {v0, v1}, Lccc;->a(Ljava/lang/Class;)Lcch;

    move-result-object v0

    check-cast v0, Lcdm;

    iget-object v1, p0, Lbeb;->ae:Lcdn;

    invoke-virtual {v0, v1}, Lcdm;->a(Lcdn;)V

    .line 241
    invoke-static {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->b(Lu;)Lcom/google/android/apps/moviemaker/MovieMakerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o()Laol;

    move-result-object v0

    invoke-virtual {v0}, Laol;->d()Laum;

    move-result-object v0

    iput-object v0, p0, Lbeb;->O:Laum;

    .line 242
    iget-object v0, p0, Lbeb;->O:Laum;

    invoke-virtual {v0, p0}, Laum;->a(Lauw;)V

    .line 243
    iget-boolean v0, p0, Lbeb;->S:Z

    if-eqz v0, :cond_0

    .line 244
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbeb;->S:Z

    .line 245
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbeb;->i(Z)V

    .line 247
    :cond_0
    return-void
.end method

.method public h(Z)Landroid/view/animation/Animation;
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 147
    if-eqz p1, :cond_0

    .line 148
    const/4 v0, 0x0

    .line 158
    :goto_0
    return-object v0

    .line 150
    :cond_0
    iget-object v0, p0, Lbeb;->T:Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;

    if-eqz v0, :cond_1

    .line 151
    iget-object v0, p0, Lbeb;->T:Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;->a(Z)V

    .line 153
    :cond_1
    invoke-direct {p0, v2}, Lbeb;->i(Z)V

    .line 156
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v1, v1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 157
    const-wide/16 v2, 0x320

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    goto :goto_0
.end method
