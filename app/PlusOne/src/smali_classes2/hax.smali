.class final Lhax;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# instance fields
.field final synthetic a:Lhap;

.field private final b:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

.field private c:Ljavax/microedition/khronos/egl/EGLContext;

.field private d:Ljavax/microedition/khronos/egl/EGLSurface;

.field private e:Ljavax/microedition/khronos/egl/EGLDisplay;

.field private f:Lhau;

.field private final g:Landroid/graphics/Rect;

.field private final h:Landroid/graphics/Matrix;

.field private final i:[F


# direct methods
.method constructor <init>(Lhap;)V
    .locals 1

    .prologue
    .line 389
    iput-object p1, p0, Lhax;->a:Lhap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 390
    const/4 v0, 0x1

    .line 391
    invoke-static {v0}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    iput-object v0, p0, Lhax;->b:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 392
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    iput-object v0, p0, Lhax;->c:Ljavax/microedition/khronos/egl/EGLContext;

    .line 393
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    iput-object v0, p0, Lhax;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 394
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    iput-object v0, p0, Lhax;->e:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 398
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lhax;->g:Landroid/graphics/Rect;

    .line 401
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 610
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lhax;->h:Landroid/graphics/Matrix;

    .line 611
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lhax;->i:[F

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 882
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    .line 884
    invoke-interface {v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetCurrentContext()Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v1

    iput-object v1, p0, Lhax;->c:Ljavax/microedition/khronos/egl/EGLContext;

    .line 885
    const/16 v1, 0x3059

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetCurrentSurface(I)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v1

    iput-object v1, p0, Lhax;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 886
    invoke-interface {v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetCurrentDisplay()Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v0

    iput-object v0, p0, Lhax;->e:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 888
    iget-object v0, p0, Lhax;->c:Ljavax/microedition/khronos/egl/EGLContext;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lhax;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lhax;->e:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-ne v0, v1, :cond_1

    .line 891
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to store the EGL context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 893
    :cond_1
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 861
    const/4 v0, 0x7

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 862
    :cond_0
    invoke-static {}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->deactivateOffScreenFilter()V

    .line 865
    :cond_1
    invoke-direct {p0}, Lhax;->a()V

    .line 866
    invoke-static {}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->initOffscreenContext()I

    .line 867
    invoke-static {}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->offscreenContextMakeCurrent()V

    .line 868
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a:Lcom/google/android/libraries/photoeditor/core/NativeCore;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->activateOffScreenFilter(I)I

    .line 869
    return-void
.end method

.method static synthetic a(Lhax;Lhbi;IILcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;ILhbk;)V
    .locals 9

    .prologue
    .line 389
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->setRenderScaleMode(I)V

    invoke-virtual {p4, p5}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->d(I)[Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getFilterType()I

    move-result v0

    invoke-direct {p0, v0}, Lhax;->a(I)V

    check-cast p1, Lhbf;

    invoke-virtual {p1}, Lhbf;->f()Lcom/google/android/libraries/photoeditor/core/TilesProvider;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    array-length v0, v2

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    array-length v0, v2

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    aget-object v0, v2, v0

    invoke-virtual {p4, p5, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(ILjava/lang/Object;)Z

    :cond_0
    array-length v5, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_2

    aget-object v0, v2, v1

    invoke-virtual {p4, p5, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(ILjava/lang/Object;)Z

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-static {p4, p5, v0}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->activateStyleOrPreset(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;II)Z

    invoke-static {p4, v3, p2, p3}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->offscreenFilterPreviewToBitmap(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Lcom/google/android/libraries/photoeditor/core/TilesProvider;II)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p2, p3, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    const/high16 v6, -0x10000

    invoke-virtual {v0, v6}, Landroid/graphics/Bitmap;->eraseColor(I)V

    :cond_1
    iget-object v6, p0, Lhax;->a:Lhap;

    invoke-static {v6}, Lhap;->d(Lhap;)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v6

    const/16 v7, 0x268

    invoke-virtual {v6, v7}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v6

    sget-object v7, Lhan;->a:Lhao;

    const/4 v8, 0x1

    invoke-interface {v7, v0, v6, v8}, Lhao;->rotateImage(Landroid/graphics/Bitmap;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lhax;->a:Lhap;

    invoke-static {v0}, Lhap;->e(Lhap;)Z

    move-result v0

    if-nez v0, :cond_2

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    invoke-virtual {p4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getFilterType()I

    move-result v0

    invoke-direct {p0, v0}, Lhax;->b(I)V

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->setRenderScaleMode(I)V

    iget-object v0, p0, Lhax;->a:Lhap;

    invoke-static {v0}, Lhap;->e(Lhap;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhax;->a:Lhap;

    new-instance v1, Lhba;

    invoke-direct {v1, p6}, Lhba;-><init>(Lhbk;)V

    invoke-virtual {v0, v1}, Lhap;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_3

    iget-object v0, p0, Lhax;->a:Lhap;

    new-instance v1, Lhbd;

    invoke-direct {v1, p6, v4}, Lhbd;-><init>(Lhbk;Ljava/util/List;)V

    invoke-virtual {v0, v1}, Lhap;->post(Ljava/lang/Runnable;)Z

    :cond_3
    return-void

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a(Lhax;Lhbi;Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Lhbm;)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 389
    invoke-direct {p0, p3}, Lhax;->a(Lhbm;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v4}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->setRenderScaleMode(I)V

    invoke-direct {p0}, Lhax;->a()V

    invoke-static {}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->offscreenPrepareToApplyImage()V

    invoke-static {}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->deactivateOffScreenFilter()V

    invoke-static {}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->deleteOffscreenContext()V

    invoke-direct {p0}, Lhax;->b()V

    check-cast p1, Lhbf;

    new-instance v0, Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-direct {v0, p2}, Lcom/google/android/libraries/photoeditor/core/FilterChain;-><init>(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getFilterNodeAt(I)Lcom/google/android/libraries/photoeditor/core/FilterChainNode;

    move-result-object v1

    invoke-virtual {p1}, Lhbf;->d()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/photoeditor/core/FilterChainNode;->a(Landroid/graphics/Bitmap;)V

    sget-object v1, Lhan;->a:Lhao;

    invoke-virtual {p1}, Lhbf;->c()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-interface {v1, v2, v0, v4, v3}, Lhao;->renderFilterChain(Landroid/graphics/Bitmap;Lcom/google/android/libraries/photoeditor/core/FilterChain;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0}, Lhax;->b()V

    invoke-virtual {p0, v3}, Lhax;->a(Z)V

    invoke-direct {p0}, Lhax;->b()V

    invoke-direct {p0, p3}, Lhax;->a(Lhbm;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lhax;->a:Lhap;

    const/4 v2, 0x1

    invoke-static {v1, v2}, Lhap;->c(Lhap;Z)Z

    iget-object v1, p0, Lhax;->a:Lhap;

    new-instance v2, Lhbb;

    invoke-direct {v2, p3, v0}, Lhbb;-><init>(Lhbm;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Lhap;->post(Ljava/lang/Runnable;)Z

    :cond_0
    return-void
.end method

.method private a(Lhbm;)Z
    .locals 2

    .prologue
    .line 655
    iget-object v0, p0, Lhax;->a:Lhap;

    invoke-static {v0}, Lhap;->e(Lhap;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 656
    iget-object v0, p0, Lhax;->a:Lhap;

    new-instance v1, Lhaz;

    invoke-direct {v1, p1}, Lhaz;-><init>(Lhbm;)V

    invoke-virtual {v0, v1}, Lhap;->post(Ljava/lang/Runnable;)Z

    .line 662
    const/4 v0, 0x1

    .line 665
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 5

    .prologue
    .line 896
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    .line 898
    iget-object v1, p0, Lhax;->e:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, Lhax;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v3, p0, Lhax;->d:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v4, p0, Lhax;->c:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 899
    const-string v1, "ImageViewGL"

    const-string v2, "ImageViewGL restoreEGL failed: %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 900
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to restore the EGL context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 902
    :cond_0
    return-void
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 872
    const/4 v0, 0x7

    if-eq p1, v0, :cond_0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 873
    :cond_0
    invoke-static {}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->deactivateOffScreenFilter()V

    .line 878
    :cond_1
    invoke-direct {p0}, Lhax;->b()V

    .line 879
    return-void
.end method


# virtual methods
.method public a(Lhaw;I)V
    .locals 1

    .prologue
    .line 408
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lhax;->a(Lhaw;IZ)V

    .line 409
    return-void
.end method

.method public a(Lhaw;IZ)V
    .locals 2

    .prologue
    .line 413
    iget-object v0, p0, Lhax;->f:Lhau;

    if-eqz v0, :cond_0

    .line 414
    iget-object v0, p0, Lhax;->f:Lhau;

    invoke-static {v0}, Lhau;->a(Lhau;)V

    .line 415
    const/4 v0, 0x0

    iput-object v0, p0, Lhax;->f:Lhau;

    .line 418
    :cond_0
    if-eqz p1, :cond_1

    if-lez p2, :cond_1

    .line 419
    new-instance v0, Lhau;

    iget-object v1, p0, Lhax;->a:Lhap;

    invoke-direct {v0, v1, p1, p2}, Lhau;-><init>(Lhap;Lhaw;I)V

    iput-object v0, p0, Lhax;->f:Lhau;

    .line 422
    :cond_1
    iget-object v1, p0, Lhax;->a:Lhap;

    iget-object v0, p0, Lhax;->f:Lhau;

    if-eqz v0, :cond_2

    if-eqz p3, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lhap;->setRenderMode(I)V

    .line 424
    return-void

    .line 422
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lhbi;Landroid/graphics/Rect;Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Lhbm;)V
    .locals 6

    .prologue
    .line 752
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->setRenderScaleMode(I)V

    .line 754
    invoke-virtual {p3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getFilterType()I

    move-result v0

    invoke-direct {p0, v0}, Lhax;->a(I)V

    .line 755
    check-cast p1, Lhbf;

    .line 758
    invoke-virtual {p1}, Lhbf;->c()Landroid/graphics/Bitmap;

    move-result-object v1

    iget v2, p2, Landroid/graphics/Rect;->left:I

    iget v3, p2, Landroid/graphics/Rect;->top:I

    .line 761
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v4

    .line 762
    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v5

    move-object v0, p3

    .line 756
    invoke-static/range {v0 .. v5}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->offscreenFilterHundredPercentRegion(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Landroid/graphics/Bitmap;IIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 763
    invoke-virtual {p3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getFilterType()I

    move-result v1

    invoke-direct {p0, v1}, Lhax;->b(I)V

    .line 765
    iget-object v1, p0, Lhax;->a:Lhap;

    new-instance v2, Lhbc;

    invoke-direct {v2, p4, v0}, Lhbc;-><init>(Lhbm;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Lhap;->post(Ljava/lang/Runnable;)Z

    .line 771
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 905
    if-eqz p1, :cond_0

    .line 906
    invoke-direct {p0}, Lhax;->a()V

    .line 909
    :cond_0
    iget-object v0, p0, Lhax;->a:Lhap;

    invoke-static {v0}, Lhap;->c(Lhap;)Lhbf;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 910
    iget-object v0, p0, Lhax;->a:Lhap;

    invoke-static {v0}, Lhap;->c(Lhap;)Lhbf;

    move-result-object v0

    invoke-virtual {v0}, Lhbf;->b()V

    .line 911
    iget-object v0, p0, Lhax;->a:Lhap;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lhap;->a(Lhap;Lhbf;)Lhbf;

    .line 914
    :cond_1
    invoke-static {}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->cleanupJNI()V

    .line 916
    if-eqz p1, :cond_2

    .line 917
    invoke-direct {p0}, Lhax;->b()V

    .line 919
    :cond_2
    return-void
.end method

.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 14

    .prologue
    const/high16 v3, 0x437f0000    # 255.0f

    const/4 v5, 0x0

    const/4 v6, 0x1

    .line 520
    :try_start_0
    iget-object v0, p0, Lhax;->a:Lhap;

    invoke-static {v0}, Lhap;->c(Lhap;)Lhbf;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhax;->a:Lhap;

    invoke-static {v0}, Lhap;->d(Lhap;)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhax;->a:Lhap;

    invoke-static {v0}, Lhap;->e(Lhap;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 608
    :cond_0
    :goto_0
    return-void

    .line 524
    :cond_1
    iget-object v0, p0, Lhax;->a:Lhap;

    invoke-static {v0}, Lhap;->f(Lhap;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 525
    iget-object v0, p0, Lhax;->a:Lhap;

    .line 526
    invoke-static {v0}, Lhap;->g(Lhap;)I

    move-result v0

    shr-int/lit8 v0, v0, 0x10

    and-int/lit16 v0, v0, 0xff

    int-to-float v0, v0

    div-float/2addr v0, v3

    iget-object v1, p0, Lhax;->a:Lhap;

    .line 527
    invoke-static {v1}, Lhap;->g(Lhap;)I

    move-result v1

    shr-int/lit8 v1, v1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-float v1, v1

    div-float/2addr v1, v3

    iget-object v2, p0, Lhax;->a:Lhap;

    .line 528
    invoke-static {v2}, Lhap;->g(Lhap;)I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    int-to-float v2, v2

    div-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    .line 525
    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 530
    iget-object v0, p0, Lhax;->a:Lhap;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lhap;->a(Lhap;Z)Z

    .line 533
    :cond_2
    const/16 v0, 0x4000

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    .line 535
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->setRenderScaleMode(I)V

    .line 538
    iget-object v0, p0, Lhax;->a:Lhap;

    invoke-static {v0}, Lhap;->c(Lhap;)Lhbf;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v0, p0, Lhax;->a:Lhap;

    invoke-static {v0}, Lhap;->h(Lhap;)Z

    move-result v0

    if-nez v0, :cond_8

    move v0, v6

    :goto_1
    invoke-virtual {v1, v2, v0}, Lhbf;->a(ZZ)V

    .line 540
    iget-object v0, p0, Lhax;->a:Lhap;

    .line 541
    invoke-static {v0}, Lhap;->d(Lhap;)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    const/16 v1, 0x268

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v7

    .line 543
    iget-object v0, p0, Lhax;->a:Lhap;

    invoke-static {v0}, Lhap;->d(Lhap;)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    .line 544
    iget-object v1, p0, Lhax;->a:Lhap;

    invoke-static {v1}, Lhap;->h(Lhap;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 545
    iget-object v0, p0, Lhax;->b:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 548
    const/16 v1, 0x268

    invoke-virtual {v0, v1, v7}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterInteger(II)Z

    .line 550
    iget-object v1, p0, Lhax;->a:Lhap;

    invoke-virtual {v1}, Lhap;->requestRender()V

    .line 553
    :cond_3
    iget-object v1, p0, Lhax;->a:Lhap;

    invoke-static {v1}, Lhap;->e(Lhap;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 557
    sget-object v1, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a:Lcom/google/android/libraries/photoeditor/core/NativeCore;

    const/4 v2, 0x0

    .line 558
    invoke-virtual {v1, v0, v2}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->activateOnScreenFilterChecked(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Z)Z

    move-result v1

    .line 560
    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getFilterType()I

    move-result v11

    .line 561
    if-ne v11, v6, :cond_9

    .line 562
    invoke-static {}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->deactivateOffScreenFilter()V

    .line 575
    :cond_4
    :goto_2
    iget-object v1, p0, Lhax;->a:Lhap;

    invoke-static {v1}, Lhap;->c(Lhap;)Lhbf;

    move-result-object v1

    invoke-virtual {v1}, Lhbf;->f()Lcom/google/android/libraries/photoeditor/core/TilesProvider;

    move-result-object v1

    .line 576
    iget-object v2, p0, Lhax;->a:Lhap;

    invoke-static {v2}, Lhap;->c(Lhap;)Lhbf;

    move-result-object v2

    invoke-virtual {v2}, Lhbf;->g()Lcom/google/android/libraries/photoeditor/core/Tile;

    move-result-object v2

    .line 578
    const/16 v3, 0x268

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v3

    iget-object v4, p0, Lhax;->h:Landroid/graphics/Matrix;

    invoke-virtual {v4}, Landroid/graphics/Matrix;->reset()V

    packed-switch v3, :pswitch_data_0

    :goto_3
    iget-object v3, p0, Lhax;->h:Landroid/graphics/Matrix;

    iget-object v4, p0, Lhax;->i:[F

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->getValues([F)V

    iget-object v3, p0, Lhax;->i:[F

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/photoeditor/core/Tile;->a([F)V

    .line 580
    iget-object v3, p0, Lhax;->a:Lhap;

    invoke-virtual {v3}, Lhap;->getWidth()I

    move-result v3

    .line 581
    iget-object v4, p0, Lhax;->a:Lhap;

    invoke-virtual {v4}, Lhap;->getHeight()I

    move-result v4

    .line 582
    if-eq v7, v6, :cond_5

    const/4 v8, 0x3

    if-ne v7, v8, :cond_6

    :cond_5
    move v5, v6

    .line 584
    :cond_6
    if-eqz v5, :cond_a

    invoke-virtual {v1}, Lcom/google/android/libraries/photoeditor/core/TilesProvider;->getScaledHeight()I

    move-result v5

    int-to-float v6, v5

    invoke-virtual {v1}, Lcom/google/android/libraries/photoeditor/core/TilesProvider;->getScaledWidth()I

    move-result v5

    int-to-float v5, v5

    :goto_4
    int-to-float v7, v3

    div-float/2addr v7, v6

    int-to-float v8, v4

    div-float/2addr v8, v5

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v7

    float-to-double v8, v7

    float-to-double v6, v6

    mul-double/2addr v6, v8

    double-to-int v6, v6

    float-to-double v12, v5

    mul-double/2addr v8, v12

    double-to-int v5, v8

    iget-object v7, p0, Lhax;->g:Landroid/graphics/Rect;

    sub-int v8, v3, v6

    div-int/lit8 v8, v8, 0x2

    iput v8, v7, Landroid/graphics/Rect;->left:I

    iget-object v7, p0, Lhax;->g:Landroid/graphics/Rect;

    sub-int v8, v4, v5

    div-int/lit8 v8, v8, 0x2

    iput v8, v7, Landroid/graphics/Rect;->top:I

    iget-object v7, p0, Lhax;->g:Landroid/graphics/Rect;

    iget-object v8, p0, Lhax;->g:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    add-int/2addr v6, v8

    iput v6, v7, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Lhax;->g:Landroid/graphics/Rect;

    iget-object v7, p0, Lhax;->g:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->top:I

    add-int/2addr v5, v7

    iput v5, v6, Landroid/graphics/Rect;->bottom:I

    iget-object v10, p0, Lhax;->g:Landroid/graphics/Rect;

    .line 587
    iget-object v5, p0, Lhax;->a:Lhap;

    invoke-virtual {v5, v11}, Lhap;->a(I)Z

    move-result v6

    .line 589
    const/4 v5, 0x1

    iget v7, v10, Landroid/graphics/Rect;->left:I

    iget v8, v10, Landroid/graphics/Rect;->top:I

    .line 593
    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v9

    invoke-virtual {v10}, Landroid/graphics/Rect;->height()I

    move-result v10

    .line 589
    invoke-static/range {v0 .. v10}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->onscreenFilterTileToScreen(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Lcom/google/android/libraries/photoeditor/core/TilesProvider;Lcom/google/android/libraries/photoeditor/core/Tile;IIZZIIII)I

    move-result v0

    .line 596
    iget-object v1, p0, Lhax;->a:Lhap;

    invoke-static {v1}, Lhap;->h(Lhap;)Z

    move-result v1

    if-nez v1, :cond_7

    if-ne v0, v11, :cond_7

    .line 597
    iget-object v0, p0, Lhax;->a:Lhap;

    invoke-static {v0, v6, v11}, Lhap;->a(Lhap;ZI)V

    .line 600
    :cond_7
    iget-object v0, p0, Lhax;->a:Lhap;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lhap;->b(Lhap;Z)Z

    .line 602
    iget-object v0, p0, Lhax;->f:Lhau;

    if-eqz v0, :cond_0

    .line 603
    iget-object v0, p0, Lhax;->f:Lhau;

    invoke-virtual {v0}, Lhau;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 605
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    :cond_8
    move v0, v5

    .line 538
    goto/16 :goto_1

    .line 563
    :cond_9
    if-eqz v1, :cond_4

    :try_start_1
    iget-object v1, p0, Lhax;->a:Lhap;

    invoke-static {}, Lhap;->d()Lhbo;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 567
    iget-object v1, p0, Lhax;->a:Lhap;

    new-instance v2, Lhay;

    invoke-direct {v2, p0}, Lhay;-><init>(Lhax;)V

    invoke-virtual {v1, v2}, Lhap;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_2

    .line 578
    :pswitch_0
    const/high16 v3, 0x42b40000    # 90.0f

    invoke-virtual {v4, v3}, Landroid/graphics/Matrix;->preRotate(F)Z

    goto/16 :goto_3

    :pswitch_1
    const/high16 v3, 0x43340000    # 180.0f

    invoke-virtual {v4, v3}, Landroid/graphics/Matrix;->preRotate(F)Z

    goto/16 :goto_3

    :pswitch_2
    const/high16 v3, 0x43870000    # 270.0f

    invoke-virtual {v4, v3}, Landroid/graphics/Matrix;->preRotate(F)Z

    goto/16 :goto_3

    .line 584
    :cond_a
    invoke-virtual {v1}, Lcom/google/android/libraries/photoeditor/core/TilesProvider;->getScaledWidth()I

    move-result v5

    int-to-float v6, v5

    invoke-virtual {v1}, Lcom/google/android/libraries/photoeditor/core/TilesProvider;->getScaledHeight()I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v5

    int-to-float v5, v5

    goto/16 :goto_4

    .line 578
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 441
    iget-object v0, p0, Lhax;->a:Lhap;

    invoke-virtual {v0}, Lhap;->getWidth()I

    move-result v0

    iget-object v1, p0, Lhax;->a:Lhap;

    invoke-virtual {v1}, Lhap;->getHeight()I

    move-result v1

    invoke-static {v2, v2, v0, v1}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 442
    iget-object v0, p0, Lhax;->a:Lhap;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lhap;->a(Lhap;Z)Z

    .line 444
    invoke-static {}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->onSurfaceChanged()V

    .line 446
    iget-object v0, p0, Lhax;->f:Lhau;

    if-eqz v0, :cond_0

    .line 447
    iget-object v0, p0, Lhax;->f:Lhau;

    invoke-virtual {v0}, Lhau;->b()V

    .line 449
    :cond_0
    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 428
    const/16 v0, 0xd05

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glPixelStorei(II)V

    .line 429
    const/16 v0, 0xcf5

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glPixelStorei(II)V

    .line 430
    const/16 v0, 0xb44

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 431
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 432
    const/16 v0, 0xbd0

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 433
    const/16 v0, 0xb90

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 434
    const/16 v0, 0xb71

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 435
    const/16 v0, 0xc11

    invoke-static {v0}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 436
    const/4 v0, 0x0

    invoke-static {v0}, Landroid/opengl/GLES20;->glDepthMask(Z)V

    .line 437
    return-void
.end method
