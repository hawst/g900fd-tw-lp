.class public final Lbhv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcgk;


# instance fields
.field private final a:Lbho;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbjb;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbhw",
            "<",
            "Lbhl;",
            ">;>;"
        }
    .end annotation
.end field

.field private d:Lbop;

.field private e:[Lbiv;

.field private f:Lbhm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lbhv;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbop;Lbho;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    const-string v0, "emptyVideoSourceFactory"

    .line 78
    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbho;

    iput-object v0, p0, Lbhv;->a:Lbho;

    .line 79
    const-string v0, "videoTimeline"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbop;

    iput-object v0, p0, Lbhv;->d:Lbop;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbhv;->b:Ljava/util/List;

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbhv;->c:Ljava/util/List;

    .line 82
    invoke-interface {p1}, Lbop;->e()I

    move-result v0

    new-array v0, v0, [Lbiv;

    iput-object v0, p0, Lbhv;->e:[Lbiv;

    .line 83
    return-void
.end method

.method private a(Lbhr;IJ)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbhr",
            "<",
            "Lbhl;",
            ">;IJ)Z"
        }
    .end annotation

    .prologue
    const-wide/16 v4, -0x1

    const/4 v1, 0x0

    .line 305
    iget-object v0, p0, Lbhv;->d:Lbop;

    invoke-interface {v0, p2}, Lbop;->b(I)Lbmd;

    move-result-object v0

    iget-object v0, v0, Lbmd;->d:Lbmg;

    .line 306
    sget-object v2, Lbmg;->a:Lbmg;

    if-eq v0, v2, :cond_0

    sget-object v2, Lbmg;->e:Lbmg;

    if-eq v0, v2, :cond_0

    move v0, v1

    .line 357
    :goto_0
    return v0

    .line 315
    :cond_0
    iget-object v0, p0, Lbhv;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhw;

    .line 316
    if-ne v0, p1, :cond_1

    .line 317
    invoke-interface {v0}, Lbhw;->b()J

    move-result-wide v2

    .line 318
    cmp-long v0, v2, v4

    if-nez v0, :cond_3

    move v0, v1

    .line 322
    goto :goto_0

    :cond_2
    move-wide v2, v4

    .line 328
    :cond_3
    cmp-long v0, v2, v4

    if-nez v0, :cond_4

    .line 329
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x18

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "video source not found: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 333
    :cond_4
    iget-object v0, p0, Lbhv;->d:Lbop;

    invoke-interface {v0, p2}, Lbop;->b(I)Lbmd;

    move-result-object v0

    .line 334
    iget-object v4, p0, Lbhv;->d:Lbop;

    .line 335
    invoke-interface {v4, p3, p4, p2}, Lbop;->a(JI)J

    move-result-wide v4

    iget-object v0, v0, Lbmd;->f:Lbmp;

    iget-wide v6, v0, Lbmp;->b:J

    .line 334
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 336
    const-wide/32 v6, 0x186a0

    add-long/2addr v6, v4

    cmp-long v0, v2, v6

    if-lez v0, :cond_5

    move v0, v1

    .line 340
    goto :goto_0

    .line 344
    :cond_5
    iget-object v0, p0, Lbhv;->d:Lbop;

    invoke-interface {v0, p2}, Lbop;->c(I)Lbmh;

    move-result-object v0

    .line 345
    invoke-virtual {v0, v2, v3}, Lbmh;->b(J)Ljava/lang/Long;

    move-result-object v0

    .line 346
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    cmp-long v4, v6, v4

    if-gtz v4, :cond_6

    .line 347
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v0, v4, v2

    if-eqz v0, :cond_6

    move v0, v1

    .line 353
    goto/16 :goto_0

    .line 357
    :cond_6
    const/4 v0, 0x1

    goto/16 :goto_0
.end method


# virtual methods
.method public a(JLjava/util/List;I)I
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J",
            "Ljava/util/List",
            "<",
            "Lbhr",
            "<",
            "Lbhl;",
            ">;>;I)I"
        }
    .end annotation

    .prologue
    .line 211
    const-string v0, "timestampUs"

    invoke-static {p1, p2, v0}, Lcec;->b(JLjava/lang/CharSequence;)J

    .line 212
    const-string v0, "videoSources"

    const/4 v1, 0x0

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 214
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    const-string v1, "videoSources.size()"

    iget-object v2, p0, Lbhv;->d:Lbop;

    invoke-interface {v2}, Lbop;->e()I

    move-result v2

    .line 213
    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 216
    iget-object v0, p0, Lbhv;->d:Lbop;

    const/4 v1, 0x1

    invoke-interface {v0, p1, p2, v1}, Lbop;->a(JZ)I

    move-result v2

    .line 217
    const/4 v1, -0x1

    .line 218
    const/4 v3, -0x1

    .line 219
    if-ne v2, p4, :cond_b

    iget-object v0, p0, Lbhv;->d:Lbop;

    invoke-interface {v0}, Lbop;->e()I

    move-result v0

    if-gt v2, v0, :cond_b

    .line 220
    invoke-interface {p3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    invoke-direct {p0, v0, v2, p1, p2}, Lbhv;->a(Lbhr;IJ)Z

    move-result v0

    if-eqz v0, :cond_0

    move v1, v2

    .line 224
    :cond_0
    add-int/lit8 v0, v2, 0x1

    iget-object v4, p0, Lbhv;->d:Lbop;

    invoke-interface {v4}, Lbop;->e()I

    move-result v4

    if-ge v0, v4, :cond_b

    add-int/lit8 v0, v2, 0x1

    .line 225
    invoke-interface {p3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    add-int/lit8 v4, v2, 0x1

    iget-object v5, p0, Lbhv;->d:Lbop;

    add-int/lit8 v6, v2, 0x1

    .line 226
    invoke-interface {v5, v6}, Lbop;->f(I)J

    move-result-wide v6

    .line 225
    invoke-direct {p0, v0, v4, v6, v7}, Lbhv;->a(Lbhr;IJ)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 227
    add-int/lit8 v0, v2, 0x1

    move v3, v1

    move v1, v0

    .line 233
    :goto_0
    const/4 v0, 0x0

    move v4, v0

    :goto_1
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_3

    .line 234
    iget-object v0, p0, Lbhv;->d:Lbop;

    invoke-interface {v0, v4}, Lbop;->b(I)Lbmd;

    move-result-object v0

    iget-object v0, v0, Lbmd;->d:Lbmg;

    .line 235
    sget-object v5, Lbmg;->a:Lbmg;

    if-eq v0, v5, :cond_1

    sget-object v5, Lbmg;->e:Lbmg;

    if-ne v0, v5, :cond_2

    .line 236
    :cond_1
    if-eq v4, v3, :cond_2

    if-eq v4, v1, :cond_2

    .line 240
    invoke-interface {p3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    invoke-interface {v0}, Lbhr;->a()V

    .line 233
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 244
    :cond_3
    const/4 v4, 0x0

    const/4 v0, 0x0

    :goto_2
    iget-object v5, p0, Lbhv;->e:[Lbiv;

    array-length v5, v5

    if-ge v0, v5, :cond_7

    iget-object v5, p0, Lbhv;->e:[Lbiv;

    aget-object v5, v5, v0

    if-eqz v5, :cond_5

    if-eq v0, v3, :cond_4

    if-ne v0, v1, :cond_6

    :cond_4
    const/4 v4, 0x1

    :cond_5
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_6
    iget-object v6, p0, Lbhv;->d:Lbop;

    invoke-interface {v6, v0}, Lbop;->b(I)Lbmd;

    move-result-object v6

    iget-object v7, p0, Lbhv;->d:Lbop;

    invoke-interface {v7, v0}, Lbop;->f(I)J

    move-result-wide v8

    iget-object v6, v6, Lbmd;->f:Lbmp;

    iget-wide v6, v6, Lbmp;->b:J

    sub-long v6, v8, v6

    const-wide/16 v8, 0x0

    sub-long v6, p1, v6

    invoke-static {v8, v9, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lbiv;->a(J)Z

    move-result v5

    or-int/2addr v4, v5

    goto :goto_3

    :cond_7
    iget-object v0, p0, Lbhv;->f:Lbhm;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lbhv;->f:Lbhm;

    invoke-virtual {v0, p1, p2}, Lbhm;->a(J)Z

    move-result v0

    or-int/2addr v4, v0

    :cond_8
    iget-object v0, p0, Lbhv;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhs;

    invoke-interface {v0, p1, p2}, Lbhs;->a(J)Z

    move-result v0

    or-int/2addr v4, v0

    goto :goto_4

    :cond_9
    if-nez v4, :cond_a

    .line 245
    iget-object v0, p0, Lbhv;->d:Lbop;

    invoke-interface {v0}, Lbop;->e()I

    move-result v2

    .line 248
    :cond_a
    return v2

    :cond_b
    move v10, v3

    move v3, v1

    move v1, v10

    goto/16 :goto_0
.end method

.method public a(Lbop;JLjava/util/List;I)I
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbop;",
            "J",
            "Ljava/util/List",
            "<",
            "Lbhr",
            "<",
            "Lbhl;",
            ">;>;I)I"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 149
    const-string v0, "timeline"

    invoke-static {p1, v0, v8}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 151
    iget-object v0, p0, Lbhv;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjb;

    .line 152
    invoke-virtual {v0, p1}, Lbjb;->a(Lbop;)V

    goto :goto_0

    .line 156
    :cond_0
    invoke-interface {p1}, Lbop;->m()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lbhv;->d:Lbop;

    invoke-interface {v0}, Lbop;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 157
    :goto_1
    iget-object v3, p0, Lbhv;->d:Lbop;

    invoke-interface {v3}, Lbop;->m()Z

    move-result v3

    if-nez v3, :cond_2

    invoke-interface {p1}, Lbop;->m()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 159
    :goto_2
    invoke-interface {p1}, Lbop;->e()I

    move-result v2

    new-array v3, v2, [Lbiv;

    move v2, v0

    .line 160
    :goto_3
    invoke-interface {p1}, Lbop;->e()I

    move-result v0

    if-ge v1, v0, :cond_8

    .line 161
    invoke-interface {p1, v1}, Lbop;->b(I)Lbmd;

    move-result-object v0

    .line 162
    iget-object v4, p0, Lbhv;->d:Lbop;

    invoke-interface {v4}, Lbop;->e()I

    move-result v4

    if-ne v2, v4, :cond_3

    .line 163
    iget-object v1, v0, Lbmd;->d:Lbmg;

    sget-object v2, Lbmg;->d:Lbmg;

    if-eq v1, v2, :cond_8

    .line 164
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x26

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "cannot replace timeline with new clip "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    :cond_1
    move v0, v2

    .line 156
    goto :goto_1

    :cond_2
    move v1, v2

    .line 157
    goto :goto_2

    .line 168
    :cond_3
    iget-object v4, p0, Lbhv;->d:Lbop;

    invoke-interface {v4, v2}, Lbop;->b(I)Lbmd;

    move-result-object v4

    .line 170
    iget-object v5, v0, Lbmd;->d:Lbmg;

    const-string v6, "newClip.type"

    iget-object v7, v4, Lbmd;->d:Lbmg;

    invoke-static {v5, v6, v7}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    .line 171
    iget-object v5, v0, Lbmd;->e:Ljeg;

    const-string v6, "newClip.mediaIdentifier"

    iget-object v7, v4, Lbmd;->e:Ljeg;

    invoke-static {v5, v6, v7}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    .line 174
    iget-object v5, v0, Lbmd;->d:Lbmg;

    sget-object v6, Lbmg;->a:Lbmg;

    if-eq v5, v6, :cond_4

    iget-object v5, v0, Lbmd;->d:Lbmg;

    sget-object v6, Lbmg;->e:Lbmg;

    if-ne v5, v6, :cond_7

    .line 175
    :cond_4
    iget-object v5, p0, Lbhv;->e:[Lbiv;

    aget-object v5, v5, v2

    .line 176
    if-nez v5, :cond_5

    .line 177
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x28

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "no media source found for existing clip "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 180
    :cond_5
    iget-object v0, v0, Lbmd;->f:Lbmp;

    invoke-virtual {v5, v0}, Lbiv;->a(Lbmp;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 183
    invoke-interface {p4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    invoke-interface {v0}, Lbhr;->a()V

    .line 185
    :cond_6
    aput-object v5, v3, v1

    .line 190
    :goto_4
    add-int/lit8 v0, v2, 0x1

    .line 191
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    .line 192
    goto/16 :goto_3

    .line 187
    :cond_7
    aput-object v8, v3, v1

    goto :goto_4

    .line 194
    :cond_8
    iput-object p1, p0, Lbhv;->d:Lbop;

    .line 195
    iput-object v3, p0, Lbhv;->e:[Lbiv;

    .line 197
    invoke-virtual {p0, p2, p3, p4, p5}, Lbhv;->a(JLjava/util/List;I)I

    move-result v0

    return v0
.end method

.method public a(Lbop;)Lbhm;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lbhv;->f:Lbhm;

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lbhv;->a:Lbho;

    invoke-virtual {v0}, Lbho;->a()Lbhm;

    move-result-object v0

    iput-object v0, p0, Lbhv;->f:Lbhm;

    .line 96
    :cond_0
    iget-object v0, p0, Lbhv;->f:Lbhm;

    invoke-virtual {v0, p1}, Lbhm;->a(Lbop;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 97
    iget-object v0, p0, Lbhv;->f:Lbhm;

    invoke-virtual {v0}, Lbhm;->a()V

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lbhv;->f:Lbhm;

    .line 100
    :cond_1
    iget-object v0, p0, Lbhv;->f:Lbhm;

    return-object v0
.end method

.method public a()V
    .locals 4

    .prologue
    .line 362
    iget-object v1, p0, Lbhv;->e:[Lbiv;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 363
    if-eqz v3, :cond_0

    .line 364
    invoke-interface {v3}, Lcgk;->a()V

    .line 362
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 367
    :cond_1
    return-void
.end method

.method public a(Lbhw;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbhw",
            "<",
            "Lbhl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Lbhv;->c:Ljava/util/List;

    const-string v1, "source"

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    return-void
.end method

.method public a(Lbiv;I)V
    .locals 3

    .prologue
    .line 112
    iget-object v0, p0, Lbhv;->d:Lbop;

    invoke-interface {v0, p2}, Lbop;->b(I)Lbmd;

    move-result-object v0

    .line 113
    iget-object v1, v0, Lbmd;->d:Lbmg;

    sget-object v2, Lbmg;->a:Lbmg;

    if-eq v1, v2, :cond_0

    iget-object v1, v0, Lbmd;->d:Lbmg;

    sget-object v2, Lbmg;->e:Lbmg;

    if-eq v1, v2, :cond_0

    .line 114
    iget-object v0, v0, Lbmd;->d:Lbmg;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x16

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unexpected clip type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 116
    :cond_0
    iget-object v1, p0, Lbhv;->e:[Lbiv;

    const-string v0, "source"

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbiv;

    aput-object v0, v1, p2

    .line 117
    return-void
.end method

.method public a(Lbjb;)V
    .locals 2

    .prologue
    .line 121
    const-string v0, "photoDecoder"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 122
    iget-object v0, p0, Lbhv;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    return-void
.end method
