.class public final Lato;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcgq;


# instance fields
.field private final a:Lchl;

.field private final b:Lbjs;

.field private final c:Latm;

.field private final d:I

.field private final e:Landroid/media/MediaFormat;

.field private final f:J

.field private final g:Latr;

.field private final h:Lats;

.field private final i:Ljava/lang/Runnable;

.field private j:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lato;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Latm;Lbjs;Latr;Lchl;ILandroid/media/MediaFormat;JLats;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 216
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 171
    new-instance v0, Latp;

    invoke-direct {v0, p0}, Latp;-><init>(Lato;)V

    iput-object v0, p0, Lato;->i:Ljava/lang/Runnable;

    .line 217
    const-string v0, "threadFactory"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchl;

    iput-object v0, p0, Lato;->a:Lchl;

    .line 218
    const-string v0, "encoder"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjs;

    iput-object v0, p0, Lato;->b:Lbjs;

    .line 219
    const-string v0, "mediaMuxer"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Latm;

    iput-object v0, p0, Lato;->c:Latm;

    .line 220
    const-string v0, "exceptionHandler"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Latr;

    iput-object v0, p0, Lato;->g:Latr;

    .line 221
    iput p5, p0, Lato;->d:I

    .line 222
    iput-object p6, p0, Lato;->e:Landroid/media/MediaFormat;

    .line 223
    const-string v0, "firstPresentationTimeUs"

    invoke-static {p7, p8, v0}, Lcec;->b(JLjava/lang/CharSequence;)J

    move-result-wide v0

    iput-wide v0, p0, Lato;->f:J

    .line 225
    iput-object p9, p0, Lato;->h:Lats;

    .line 226
    return-void
.end method

.method static synthetic a(Lato;)V
    .locals 12

    .prologue
    const/4 v10, -0x1

    .line 31
    iget v2, p0, Lato;->d:I

    const/4 v1, 0x0

    iget-object v0, p0, Lato;->b:Lbjs;

    invoke-interface {v0}, Lbjs;->h()[Ljava/nio/ByteBuffer;

    move-result-object v0

    new-instance v3, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v3}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    move-object v11, v0

    move v0, v2

    move-object v2, v1

    move-object v1, v11

    :cond_0
    :goto_0
    :try_start_0
    iget-object v4, p0, Lato;->b:Lbjs;

    invoke-interface {v4, v3}, Lbjs;->a(Landroid/media/MediaCodec$BufferInfo;)I

    move-result v4

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v5

    if-eqz v5, :cond_1

    :goto_1
    return-void

    :cond_1
    if-eq v4, v10, :cond_0

    const/4 v5, -0x3

    if-ne v4, v5, :cond_2

    iget-object v1, p0, Lato;->b:Lbjs;

    invoke-interface {v1}, Lbjs;->h()[Ljava/nio/ByteBuffer;

    move-result-object v1

    goto :goto_0

    :cond_2
    const/4 v5, -0x2

    if-ne v4, v5, :cond_4

    iget-object v2, p0, Lato;->b:Lbjs;

    invoke-interface {v2}, Lbjs;->i()Landroid/media/MediaFormat;

    move-result-object v2

    iget v4, p0, Lato;->d:I

    if-ne v4, v10, :cond_3

    const-string v4, "outputTrack"

    const/4 v5, -0x1

    const-string v6, "format changed during encoding"

    invoke-static {v0, v4, v5, v6}, Lcgp;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    :cond_3
    :try_start_1
    const-string v4, "mime"

    invoke-virtual {v2, v4}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "format.getString(MediaFormat.KEY_MIME)"

    iget-object v6, p0, Lato;->e:Landroid/media/MediaFormat;

    const-string v7, "mime"

    invoke-virtual {v6, v7}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "format does not match preselected track"

    invoke-static {v4, v5, v6, v7}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/lang/CharSequence;)Ljava/lang/Object;

    goto :goto_0

    :cond_4
    aget-object v5, v1, v4
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    iget v6, v3, Landroid/media/MediaCodec$BufferInfo;->flags:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    and-int/lit8 v6, v6, 0x2

    if-eqz v6, :cond_5

    :try_start_3
    iget-object v5, p0, Lato;->b:Lbjs;

    const/4 v6, 0x0

    invoke-interface {v5, v4, v6}, Lbjs;->a(IZ)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_0

    :cond_5
    :try_start_4
    iget v6, v3, Landroid/media/MediaCodec$BufferInfo;->size:I

    if-lez v6, :cond_8

    if-ne v0, v10, :cond_6

    const-string v0, "format"

    const-string v6, "should receive a format change before the first frame"

    invoke-static {v2, v0, v6}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    iget-object v0, p0, Lato;->c:Latm;

    invoke-virtual {v0, v2}, Latm;->a(Landroid/media/MediaFormat;)I

    move-result v0

    :cond_6
    iget-wide v6, v3, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iget-wide v8, p0, Lato;->f:J

    cmp-long v6, v6, v8

    if-ltz v6, :cond_8

    iget-object v6, p0, Lato;->h:Lats;

    if-eqz v6, :cond_7

    iget-object v6, p0, Lato;->h:Lats;

    iget-wide v8, v3, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-interface {v6, v0, v8, v9}, Lats;->a(IJ)V

    :cond_7
    iget-object v6, p0, Lato;->c:Latm;

    invoke-virtual {v6, v0, v5, v3}, Latm;->a(ILjava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    :cond_8
    iget v5, v3, Landroid/media/MediaCodec$BufferInfo;->flags:I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    and-int/lit8 v5, v5, 0x4

    if-eqz v5, :cond_9

    :try_start_5
    iget-object v0, p0, Lato;->b:Lbjs;

    const/4 v1, 0x0

    invoke-interface {v0, v4, v1}, Lbjs;->a(IZ)V

    goto/16 :goto_1

    :cond_9
    iget-object v5, p0, Lato;->b:Lbjs;

    const/4 v6, 0x0

    invoke-interface {v5, v4, v6}, Lbjs;->a(IZ)V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lato;->b:Lbjs;

    const/4 v2, 0x0

    invoke-interface {v1, v4, v2}, Lbjs;->a(IZ)V

    throw v0
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_0
.end method

.method static synthetic b(Lato;)Latr;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lato;->g:Latr;

    return-object v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 267
    :try_start_0
    iget-object v0, p0, Lato;->j:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 274
    :goto_0
    return-void

    .line 269
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 234
    iget-object v0, p0, Lato;->j:Ljava/lang/Thread;

    const-string v1, "mFeederThread"

    const-string v2, "already started"

    invoke-static {v0, v1, v2}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 235
    iget-object v0, p0, Lato;->a:Lchl;

    iget-object v1, p0, Lato;->i:Ljava/lang/Runnable;

    const-string v2, "feeder"

    invoke-interface {v0, v1, v2}, Lchl;->a(Ljava/lang/Runnable;Ljava/lang/String;)Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lato;->j:Ljava/lang/Thread;

    .line 236
    iget-object v0, p0, Lato;->j:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 237
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 246
    iget-object v0, p0, Lato;->j:Ljava/lang/Thread;

    const-string v1, "mFeederThread"

    const-string v2, "not started"

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 247
    iget-object v0, p0, Lato;->j:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 248
    invoke-direct {p0}, Lato;->d()V

    .line 249
    const/4 v0, 0x0

    iput-object v0, p0, Lato;->j:Ljava/lang/Thread;

    .line 250
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 259
    invoke-direct {p0}, Lato;->d()V

    .line 260
    return-void
.end method
