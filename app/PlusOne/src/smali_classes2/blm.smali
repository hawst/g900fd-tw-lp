.class abstract Lblm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lblk;


# instance fields
.field public final a:Lbmz;

.field private final b:F

.field private final c:F

.field private final d:I


# direct methods
.method public constructor <init>(FFILbmz;)V
    .locals 0

    .prologue
    .line 399
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 400
    iput p1, p0, Lblm;->b:F

    .line 401
    iput p2, p0, Lblm;->c:F

    .line 402
    iput p3, p0, Lblm;->d:I

    .line 403
    iput-object p4, p0, Lblm;->a:Lbmz;

    .line 404
    return-void
.end method


# virtual methods
.method public a(Lbku;[J)Ljava/lang/Object;
    .locals 5

    .prologue
    .line 408
    invoke-virtual {p0, p1, p2}, Lblm;->b(Lbku;[J)Ljava/util/ArrayList;

    move-result-object v0

    .line 409
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 410
    iget v0, p0, Lblm;->d:I

    new-array v0, v0, [F

    .line 418
    :goto_0
    return-object v0

    .line 416
    :cond_0
    new-instance v1, Lbpt;

    iget v2, p0, Lblm;->b:F

    iget v3, p0, Lblm;->c:F

    iget v4, p0, Lblm;->d:I

    invoke-direct {v1, v2, v3, v4}, Lbpt;-><init>(FFI)V

    .line 417
    invoke-virtual {v1, v0}, Lbpt;->a(Ljava/util/ArrayList;)V

    .line 418
    invoke-virtual {v1}, Lbpt;->a()[F

    move-result-object v0

    goto :goto_0
.end method

.method protected b(Lbku;[J)Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbku;",
            "[J)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 431
    iget-object v0, p0, Lblm;->a:Lbmz;

    if-nez v0, :cond_0

    .line 432
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Field type is null. Cannot extract values."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 434
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 435
    array-length v3, p2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    aget-wide v4, p2, v1

    .line 436
    iget-object v0, p0, Lblm;->a:Lbmz;

    invoke-virtual {p1, v0, v4, v5}, Lbku;->a(Lbmz;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 437
    if-eqz v0, :cond_1

    .line 438
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 435
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 441
    :cond_2
    return-object v2
.end method
