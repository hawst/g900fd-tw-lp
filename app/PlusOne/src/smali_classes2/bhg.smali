.class public final Lbhg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:I

.field public final b:I

.field public final c:Ljava/nio/Buffer;


# direct methods
.method public constructor <init>(I[F)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    if-lez p1, :cond_0

    const/4 v0, 0x4

    if-gt p1, v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Invalid element count for VertexStream"

    invoke-static {v0, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 41
    array-length v0, p2

    rem-int/2addr v0, p1

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Vertex data length is not a multiple of elementCount"

    invoke-static {v1, v0}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 44
    iput p1, p0, Lbhg;->a:I

    .line 46
    array-length v0, p2

    div-int/2addr v0, p1

    iput v0, p0, Lbhg;->b:I

    .line 47
    array-length v0, p2

    shl-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 48
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 49
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    .line 50
    invoke-virtual {v0, p2}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    .line 51
    invoke-virtual {v0, v2}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    move-result-object v0

    iput-object v0, p0, Lbhg;->c:Ljava/nio/Buffer;

    .line 52
    return-void

    :cond_0
    move v0, v2

    .line 39
    goto :goto_0

    :cond_1
    move v1, v2

    .line 41
    goto :goto_1
.end method
