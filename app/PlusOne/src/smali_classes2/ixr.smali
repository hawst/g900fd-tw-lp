.class public final Lixr;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private final a:[Lnqb;


# direct methods
.method public constructor <init>([Lnqb;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 22
    iput-object p1, p0, Lixr;->a:[Lnqb;

    .line 23
    return-void
.end method

.method private a(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 50
    check-cast p2, Landroid/widget/TextView;

    .line 51
    if-nez p2, :cond_0

    .line 52
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 53
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 54
    const/4 v1, 0x0

    invoke-virtual {v0, p4, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 56
    :goto_0
    invoke-virtual {p0, p1}, Lixr;->a(I)Lnqb;

    move-result-object v1

    .line 57
    iget-object v1, v1, Lnqb;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 58
    return-object v0

    :cond_0
    move-object v0, p2

    goto :goto_0
.end method


# virtual methods
.method public a(Lnqc;)I
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 77
    invoke-virtual {p0}, Lixr;->getCount()I

    move-result v4

    move v0, v3

    :goto_0
    if-ge v0, v4, :cond_5

    .line 78
    invoke-virtual {p0, v0}, Lixr;->a(I)Lnqb;

    move-result-object v1

    .line 79
    iget-object v1, v1, Lnqb;->b:Lnqc;

    if-eqz v1, :cond_0

    if-nez p1, :cond_2

    :cond_0
    if-ne v1, p1, :cond_1

    move v1, v2

    :goto_1
    if-eqz v1, :cond_4

    .line 83
    :goto_2
    return v0

    :cond_1
    move v1, v3

    .line 79
    goto :goto_1

    :cond_2
    iget-object v5, v1, Lnqc;->a:Ljava/lang/String;

    iget-object v6, p1, Lnqc;->a:Ljava/lang/String;

    invoke-static {v5, v6}, Liyi;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, v1, Lnqc;->b:Ljava/lang/Boolean;

    iget-object v6, p1, Lnqc;->b:Ljava/lang/Boolean;

    invoke-static {v5, v6}, Liyi;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v5, v1, Lnqc;->c:Lnpv;

    iget-object v6, p1, Lnqc;->c:Lnpv;

    invoke-static {v5, v6}, Liyi;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v1, v1, Lnqc;->d:Lnpw;

    iget-object v5, p1, Lnqc;->d:Lnpw;

    invoke-static {v1, v5}, Liyi;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_1

    .line 77
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_5
    const/4 v0, -0x1

    goto :goto_2
.end method

.method public a(I)Lnqb;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lixr;->a:[Lnqb;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lixr;->a:[Lnqb;

    array-length v0, v0

    return v0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 68
    const v0, 0x1090009

    invoke-direct {p0, p1, p2, p3, v0}, Lixr;->a(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0, p1}, Lixr;->a(I)Lnqb;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 46
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 63
    const v0, 0x7f04015a

    invoke-direct {p0, p1, p2, p3, v0}, Lixr;->a(ILandroid/view/View;Landroid/view/ViewGroup;I)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method
