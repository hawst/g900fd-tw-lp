.class public Lbem;
.super Llol;
.source "PG"

# interfaces
.implements Lano;
.implements Lasc;
.implements Lbfq;
.implements Lbft;
.implements Lcdw;


# static fields
.field private static final N:Ljava/lang/String;

.field private static final O:Lasd;


# instance fields
.field private P:Landroid/graphics/Rect;

.field private Q:Lbmd;

.field private R:Landroid/graphics/RectF;

.field private S:I

.field private T:Lasd;

.field private U:Lccc;

.field private V:Lcby;

.field private W:Laru;

.field private X:Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;

.field private Y:Lbqo;

.field private Z:Ljava/util/concurrent/Executor;

.field private aa:Landroid/widget/LinearLayout;

.field private ab:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

.field private ac:Landroid/view/View;

.field private ad:Landroid/view/inputmethod/InputMethodManager;

.field private ae:F

.field private final af:Landroid/view/View$OnClickListener;

.field private final ag:Landroid/widget/TextView$OnEditorActionListener;

.field private final ah:Lcal;

.field private final ai:Landroid/view/View$OnClickListener;

.field private final aj:Lcdp;

.field private final ak:Lcdn;

.field private final al:Lcds;

.field private final am:Lcdk;

.field private an:Lcdf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    const-class v0, Lbem;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbem;->N:Ljava/lang/String;

    .line 96
    const-class v0, Lasd;

    .line 97
    invoke-static {v0}, Lcgf;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lasd;

    sput-object v0, Lbem;->O:Lasd;

    .line 96
    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 80
    invoke-direct {p0}, Llol;-><init>()V

    .line 108
    const/4 v0, -0x1

    iput v0, p0, Lbem;->S:I

    .line 110
    sget-object v0, Lbem;->O:Lasd;

    iput-object v0, p0, Lbem;->T:Lasd;

    .line 124
    new-instance v0, Lben;

    invoke-direct {v0, p0}, Lben;-><init>(Lbem;)V

    iput-object v0, p0, Lbem;->af:Landroid/view/View$OnClickListener;

    .line 131
    new-instance v0, Lbeq;

    invoke-direct {v0, p0}, Lbeq;-><init>(Lbem;)V

    iput-object v0, p0, Lbem;->ag:Landroid/widget/TextView$OnEditorActionListener;

    .line 145
    new-instance v0, Lber;

    invoke-direct {v0, p0}, Lber;-><init>(Lbem;)V

    iput-object v0, p0, Lbem;->ah:Lcal;

    .line 155
    new-instance v0, Lbes;

    invoke-direct {v0, p0}, Lbes;-><init>(Lbem;)V

    iput-object v0, p0, Lbem;->ai:Landroid/view/View$OnClickListener;

    .line 161
    new-instance v0, Lbet;

    invoke-direct {v0, p0}, Lbet;-><init>(Lbem;)V

    iput-object v0, p0, Lbem;->aj:Lcdp;

    .line 170
    new-instance v0, Lbeu;

    invoke-direct {v0, p0}, Lbeu;-><init>(Lbem;)V

    iput-object v0, p0, Lbem;->ak:Lcdn;

    .line 178
    new-instance v0, Lbev;

    invoke-direct {v0, p0}, Lbev;-><init>(Lbem;)V

    iput-object v0, p0, Lbem;->al:Lcds;

    .line 200
    new-instance v0, Lbew;

    invoke-direct {v0, p0}, Lbew;-><init>(Lbem;)V

    iput-object v0, p0, Lbem;->am:Lcdk;

    .line 231
    new-instance v0, Lhmf;

    iget-object v1, p0, Lbem;->av:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 238
    new-instance v0, Lbex;

    invoke-direct {v0, p0}, Lbex;-><init>(Lbem;)V

    iput-object v0, p0, Lbem;->an:Lcdf;

    .line 251
    new-instance v0, Lhmg;

    sget-object v1, Lomx;->d:Lhmn;

    invoke-direct {v0, v1}, Lhmg;-><init>(Lhmn;)V

    iget-object v1, p0, Lbem;->au:Llnh;

    invoke-virtual {v0, v1}, Lhmg;->a(Llnh;)Lhmg;

    .line 552
    return-void
.end method

.method private V()Loo;
    .locals 1

    .prologue
    .line 235
    invoke-virtual {p0}, Lbem;->n()Lz;

    move-result-object v0

    check-cast v0, Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    return-object v0
.end method

.method private W()V
    .locals 3

    .prologue
    .line 522
    iget-object v0, p0, Lbem;->ab:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    if-eqz v0, :cond_0

    .line 524
    invoke-virtual {p0}, Lbem;->n()Lz;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Lz;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 526
    iget-object v1, p0, Lbem;->ab:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    invoke-virtual {v1}, Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 528
    :cond_0
    return-void
.end method

.method private X()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 727
    iput-object v0, p0, Lbem;->Q:Lbmd;

    .line 728
    iput-object v0, p0, Lbem;->P:Landroid/graphics/Rect;

    .line 729
    iput-object v0, p0, Lbem;->R:Landroid/graphics/RectF;

    .line 730
    return-void
.end method

.method private Y()V
    .locals 1

    .prologue
    .line 763
    iget-object v0, p0, Lbem;->T:Lasd;

    invoke-interface {v0}, Lasd;->b()V

    .line 764
    iget-object v0, p0, Lbem;->V:Lcby;

    invoke-virtual {v0}, Lcby;->e()V

    .line 765
    iget-object v0, p0, Lbem;->V:Lcby;

    invoke-virtual {v0}, Lcby;->d()V

    .line 766
    return-void
.end method

.method static synthetic a(Lbem;I)I
    .locals 0

    .prologue
    .line 80
    iput p1, p0, Lbem;->S:I

    return p1
.end method

.method static synthetic a(Lbem;Landroid/view/View;)Landroid/view/View;
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lbem;->ac:Landroid/view/View;

    return-object p1
.end method

.method static synthetic a(Lbem;Landroid/view/inputmethod/InputMethodManager;)Landroid/view/inputmethod/InputMethodManager;
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lbem;->ad:Landroid/view/inputmethod/InputMethodManager;

    return-object p1
.end method

.method static synthetic a(Lbem;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lbem;->aa:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic a(Lbem;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lbem;->aa:Landroid/widget/LinearLayout;

    return-object p1
.end method

.method static synthetic a(Lbem;Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;)Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lbem;->ab:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    return-object p1
.end method

.method static synthetic a(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method static synthetic a(Landroid/view/View;Z)V
    .locals 2

    .prologue
    .line 80
    const v0, 0x7f100003

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Landroid/widget/EditText;)V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/EditText;->setFocusable(Z)V

    invoke-virtual {p0}, Landroid/widget/EditText;->clearFocus()V

    return-void
.end method

.method static synthetic a(Lbem;II)V
    .locals 3

    .prologue
    .line 80
    iget-object v0, p0, Lbem;->V:Lcby;

    invoke-virtual {v0, p1}, Lcby;->e(I)I

    move-result v0

    iget-object v1, p0, Lbem;->V:Lcby;

    invoke-virtual {v1, p2}, Lcby;->e(I)I

    move-result v1

    iget-object v2, p0, Lbem;->T:Lasd;

    invoke-interface {v2, v0, v1}, Lasd;->a(II)V

    return-void
.end method

.method static synthetic a(Lbem;Landroid/widget/EditText;Landroid/view/inputmethod/InputMethodManager;)V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    invoke-virtual {p1}, Landroid/widget/EditText;->requestFocus()Z

    const/4 v0, 0x0

    invoke-virtual {p2, p1, v0}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    invoke-direct {p0}, Lbem;->Y()V

    return-void
.end method

.method static synthetic a(Lbem;[J)V
    .locals 6

    .prologue
    .line 80
    new-instance v1, Landroid/util/SparseArray;

    array-length v0, p1

    invoke-direct {v1, v0}, Landroid/util/SparseArray;-><init>(I)V

    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-wide v4, p1, v0

    iget-object v3, p0, Lbem;->V:Lcby;

    invoke-virtual {v3, v4, v5}, Lcby;->a(J)I

    move-result v3

    if-ltz v3, :cond_0

    iget-object v4, p0, Lbem;->V:Lcby;

    invoke-virtual {v4, v3}, Lcby;->b(I)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lbem;->V:Lcby;

    invoke-virtual {v4, v3}, Lcby;->e(I)I

    move-result v4

    iget-object v5, p0, Lbem;->V:Lcby;

    invoke-virtual {v5, v3}, Lcby;->f(I)Lbmd;

    move-result-object v3

    invoke-virtual {v1, v4, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lbem;->V:Lcby;

    invoke-virtual {v4, v3}, Lcby;->a(I)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-direct {p0}, Lbem;->W()V

    iget-object v3, p0, Lbem;->T:Lasd;

    const-string v4, ""

    invoke-interface {v3, v4}, Lasd;->a(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lbem;->V:Lcby;

    invoke-virtual {v4, v3}, Lcby;->c(I)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lbem;->V:Lcby;

    invoke-virtual {v3}, Lcby;->d()V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lbem;->V:Lcby;

    invoke-virtual {v0, v1}, Lcby;->a(Landroid/util/SparseArray;)V

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lbem;->T:Lasd;

    invoke-interface {v0, v1}, Lasd;->a(Landroid/util/SparseArray;)V

    :cond_4
    return-void
.end method

.method static synthetic b(Lbem;)Landroid/view/View;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lbem;->ac:Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Landroid/view/View;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 80
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method static synthetic c(Lbem;)Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lbem;->ab:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    return-object v0
.end method

.method static synthetic c(Landroid/view/View;)Z
    .locals 1

    .prologue
    .line 80
    const v0, 0x7f100003

    invoke-virtual {p0, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lbem;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lbem;->ad:Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method

.method static synthetic e(Lbem;)Lasd;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lbem;->T:Lasd;

    return-object v0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    sget-object v0, Lbem;->N:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lbem;)Lcby;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lbem;->V:Lcby;

    return-object v0
.end method

.method static synthetic g(Lbem;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lbem;->Y()V

    return-void
.end method

.method static synthetic h(Lbem;)Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lbem;->X:Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;

    return-object v0
.end method

.method static synthetic i(Lbem;)Lccc;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lbem;->U:Lccc;

    return-object v0
.end method

.method static synthetic j(Lbem;)Lbqo;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lbem;->Y:Lbqo;

    return-object v0
.end method

.method static synthetic k(Lbem;)Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lbem;->Z:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic l(Lbem;)Lbmd;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lbem;->Q:Lbmd;

    return-object v0
.end method

.method static synthetic m(Lbem;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lbem;->R:Landroid/graphics/RectF;

    return-object v0
.end method

.method static synthetic n(Lbem;)Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lbem;->P:Landroid/graphics/Rect;

    return-object v0
.end method

.method static synthetic o(Lbem;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Lbem;->X()V

    return-void
.end method

.method static synthetic p(Lbem;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lbem;->af:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic q(Lbem;)Landroid/widget/TextView$OnEditorActionListener;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lbem;->ag:Landroid/widget/TextView$OnEditorActionListener;

    return-object v0
.end method

.method static synthetic r(Lbem;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lbem;->ai:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic s(Lbem;)Lcal;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lbem;->ah:Lcal;

    return-object v0
.end method


# virtual methods
.method public U()Z
    .locals 1

    .prologue
    .line 456
    iget-object v0, p0, Lbem;->T:Lasd;

    invoke-interface {v0}, Lasd;->d()V

    .line 457
    iget-object v0, p0, Lbem;->T:Lasd;

    invoke-interface {v0}, Lasd;->b()V

    .line 458
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 257
    const v0, 0x7f040211

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 258
    const v0, 0x7f1005e7

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lccc;

    iput-object v0, p0, Lbem;->U:Lccc;

    .line 260
    new-instance v0, Lcby;

    invoke-virtual {p0}, Lbem;->n()Lz;

    move-result-object v2

    new-instance v3, Lbey;

    invoke-direct {v3, p0}, Lbey;-><init>(Lbem;)V

    invoke-direct {v0, v2, v3}, Lcby;-><init>(Landroid/content/Context;Lcca;)V

    iput-object v0, p0, Lbem;->V:Lcby;

    .line 262
    const v0, 0x7f1005e8

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 264
    const v0, 0x7f1005b1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;

    iput-object v0, p0, Lbem;->X:Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;

    .line 266
    iget-object v0, p0, Lbem;->U:Lccc;

    iget-object v2, p0, Lbem;->V:Lcby;

    invoke-virtual {v0, v2}, Lccc;->setAdapter(Landroid/widget/Adapter;)V

    .line 267
    iget-object v0, p0, Lbem;->U:Lccc;

    const-class v2, Lcdo;

    invoke-virtual {v0, v2}, Lccc;->a(Ljava/lang/Class;)Lcch;

    move-result-object v0

    check-cast v0, Lcdo;

    iget-object v2, p0, Lbem;->aj:Lcdp;

    .line 268
    invoke-virtual {v0, v2}, Lcdo;->a(Lcdp;)V

    .line 269
    iget-object v0, p0, Lbem;->U:Lccc;

    const-class v2, Lcdm;

    invoke-virtual {v0, v2}, Lccc;->a(Ljava/lang/Class;)Lcch;

    move-result-object v0

    check-cast v0, Lcdm;

    iget-object v2, p0, Lbem;->ak:Lcdn;

    .line 270
    invoke-virtual {v0, v2}, Lcdm;->a(Lcdn;)V

    .line 271
    iget-object v0, p0, Lbem;->U:Lccc;

    const-class v2, Lcdq;

    invoke-virtual {v0, v2}, Lccc;->a(Ljava/lang/Class;)Lcch;

    move-result-object v0

    check-cast v0, Lcdq;

    iget-object v2, p0, Lbem;->al:Lcds;

    .line 272
    invoke-virtual {v0, v2}, Lcdq;->a(Lcds;)V

    .line 273
    iget-object v0, p0, Lbem;->U:Lccc;

    const-class v2, Lcdj;

    invoke-virtual {v0, v2}, Lccc;->a(Ljava/lang/Class;)Lcch;

    move-result-object v0

    check-cast v0, Lcdj;

    iget-object v2, p0, Lbem;->am:Lcdk;

    .line 274
    invoke-virtual {v0, v2}, Lcdj;->a(Lcdk;)V

    .line 275
    iget-object v0, p0, Lbem;->U:Lccc;

    const-class v2, Lcde;

    invoke-virtual {v0, v2}, Lccc;->a(Ljava/lang/Class;)Lcch;

    move-result-object v0

    check-cast v0, Lcde;

    iget-object v2, p0, Lbem;->an:Lcdf;

    .line 276
    invoke-virtual {v0, v2}, Lcde;->a(Lcdf;)V

    .line 278
    if-eqz p3, :cond_0

    .line 279
    const-string v0, "StoryboardEditorScreenScrollPosition"

    invoke-virtual {p3, v0, v4}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;F)F

    move-result v0

    iput v0, p0, Lbem;->ae:F

    .line 280
    const-string v0, "StoryboardEditorClipPositionLastEdited"

    const/4 v2, -0x1

    .line 281
    invoke-virtual {p3, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lbem;->S:I

    .line 284
    :cond_0
    invoke-direct {p0}, Lbem;->V()Loo;

    move-result-object v0

    invoke-virtual {v0, v4}, Loo;->a(F)V

    .line 286
    return-object v1
.end method

.method public a()V
    .locals 2

    .prologue
    .line 449
    iget-object v0, p0, Lbem;->V:Lcby;

    invoke-virtual {v0}, Lcby;->c()V

    .line 450
    iget-object v0, p0, Lbem;->U:Lccc;

    const-class v1, Lcdm;

    invoke-virtual {v0, v1}, Lccc;->a(Ljava/lang/Class;)Lcch;

    move-result-object v0

    check-cast v0, Lcdm;

    iget-object v1, p0, Lbem;->V:Lcby;

    .line 451
    invoke-virtual {v1}, Lcby;->f()I

    move-result v1

    .line 450
    invoke-virtual {v0, v1}, Lcdm;->a(I)V

    .line 452
    return-void
.end method

.method public a(Landroid/graphics/Rect;Lbmd;Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 340
    const-string v0, "fanOutFromRect"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    iput-object v0, p0, Lbem;->P:Landroid/graphics/Rect;

    .line 341
    const-string v0, "fanOutFromClip"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iput-object v0, p0, Lbem;->Q:Lbmd;

    .line 342
    const-string v0, "fanOutFromFrameWindow"

    .line 343
    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    iput-object v0, p0, Lbem;->R:Landroid/graphics/RectF;

    .line 344
    return-void
.end method

.method public a(Lasd;)V
    .locals 0

    .prologue
    .line 336
    if-nez p1, :cond_0

    sget-object p1, Lbem;->O:Lasd;

    :cond_0
    iput-object p1, p0, Lbem;->T:Lasd;

    .line 337
    return-void
.end method

.method public a(Lbph;)V
    .locals 1

    .prologue
    .line 439
    iget-object v0, p0, Lbem;->V:Lcby;

    invoke-virtual {v0, p1}, Lcby;->a(Lbph;)V

    .line 440
    return-void
.end method

.method public a(Lbqo;Ljava/util/concurrent/Executor;Ljava/util/List;Ljava/util/Map;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbqo;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 352
    iput-object p1, p0, Lbem;->Y:Lbqo;

    .line 353
    iput-object p2, p0, Lbem;->Z:Ljava/util/concurrent/Executor;

    .line 354
    iget-object v0, p0, Lbem;->V:Lcby;

    invoke-virtual {v0, p3, p4}, Lcby;->a(Ljava/util/List;Ljava/util/Map;)V

    .line 356
    iget v0, p0, Lbem;->S:I

    if-eq v0, v5, :cond_1

    .line 357
    iget-object v0, p0, Lbem;->U:Lccc;

    const-class v1, Lcdm;

    invoke-virtual {v0, v1}, Lccc;->a(Ljava/lang/Class;)Lcch;

    move-result-object v0

    check-cast v0, Lcdm;

    iget v1, p0, Lbem;->S:I

    .line 358
    invoke-virtual {v0, v1}, Lcdm;->a(I)V

    .line 359
    iget-object v0, p0, Lbem;->U:Lccc;

    const-class v1, Lccw;

    invoke-virtual {v0, v1}, Lccc;->a(Ljava/lang/Class;)Lcch;

    move-result-object v0

    check-cast v0, Lccw;

    const-wide/16 v2, 0x1f4

    iget v1, p0, Lbem;->S:I

    .line 360
    invoke-virtual {v0, v2, v3, v1}, Lccw;->a(JI)V

    .line 377
    :cond_0
    :goto_0
    iput v4, p0, Lbem;->ae:F

    .line 378
    iput v5, p0, Lbem;->S:I

    .line 379
    return-void

    .line 361
    :cond_1
    iget-object v0, p0, Lbem;->P:Landroid/graphics/Rect;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbem;->Q:Lbmd;

    if-eqz v0, :cond_3

    .line 362
    iget-object v0, p0, Lbem;->V:Lcby;

    iget-object v1, p0, Lbem;->Q:Lbmd;

    invoke-virtual {v0, v1}, Lcby;->b(Lbmd;)I

    move-result v1

    .line 363
    if-ltz v1, :cond_2

    .line 364
    iget-object v0, p0, Lbem;->U:Lccc;

    const-class v2, Lcdm;

    invoke-virtual {v0, v2}, Lccc;->a(Ljava/lang/Class;)Lcch;

    move-result-object v0

    check-cast v0, Lcdm;

    iget-object v2, p0, Lbem;->P:Landroid/graphics/Rect;

    .line 365
    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    iget-object v3, p0, Lbem;->P:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    .line 364
    invoke-virtual {v0, v1, v2, v3}, Lcdm;->a(III)V

    goto :goto_0

    .line 367
    :cond_2
    sget-object v0, Lbem;->N:Ljava/lang/String;

    iget-object v0, p0, Lbem;->Q:Lbmd;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "showPosters: Fan out clip "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " not found in adapter"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 369
    invoke-direct {p0}, Lbem;->X()V

    goto :goto_0

    .line 371
    :cond_3
    iget v0, p0, Lbem;->ae:F

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_0

    .line 373
    iget-object v0, p0, Lbem;->U:Lccc;

    const-class v1, Lcdm;

    invoke-virtual {v0, v1}, Lccc;->a(Ljava/lang/Class;)Lcch;

    move-result-object v0

    check-cast v0, Lcdm;

    iget v1, p0, Lbem;->ae:F

    invoke-virtual {v0, v1}, Lcdm;->a(F)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 398
    iget-object v0, p0, Lbem;->V:Lcby;

    invoke-virtual {v0, p1}, Lcby;->a(Ljava/lang/String;)V

    .line 399
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 463
    if-eqz p1, :cond_0

    .line 464
    iget-object v0, p0, Lbem;->T:Lasd;

    invoke-interface {v0}, Lasd;->d()V

    .line 468
    :goto_0
    iget-object v0, p0, Lbem;->T:Lasd;

    invoke-interface {v0}, Lasd;->b()V

    .line 469
    return-void

    .line 466
    :cond_0
    iget-object v0, p0, Lbem;->T:Lasd;

    invoke-interface {v0}, Lasd;->e()V

    goto :goto_0
.end method

.method public ae_()V
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Lbem;->ab:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Lbem;->T:Lasd;

    iget-object v1, p0, Lbem;->ab:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    invoke-virtual {v1}, Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lasd;->a(Ljava/lang/String;)V

    .line 325
    :cond_0
    invoke-direct {p0}, Lbem;->W()V

    .line 326
    iget-object v0, p0, Lbem;->W:Laru;

    invoke-virtual {v0, p0}, Laru;->b(Lasc;)V

    .line 327
    invoke-static {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->b(Lu;)Lcom/google/android/apps/moviemaker/MovieMakerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o()Laol;

    move-result-object v0

    invoke-virtual {v0}, Laol;->l()Lanj;

    move-result-object v0

    .line 328
    invoke-virtual {v0, p0}, Lanj;->b(Lano;)V

    .line 329
    invoke-super {p0}, Llol;->ae_()V

    .line 331
    invoke-direct {p0}, Lbem;->V()Loo;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Loo;->a(Ljava/lang/CharSequence;)V

    .line 332
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lbem;->V:Lcby;

    invoke-virtual {v0}, Lcby;->a()V

    .line 389
    return-void
.end method

.method public b(I)V
    .locals 5

    .prologue
    .line 408
    iget-object v0, p0, Lbem;->V:Lcby;

    invoke-virtual {v0, p1}, Lcby;->d(I)I

    move-result v1

    .line 409
    if-gez v1, :cond_0

    .line 435
    :goto_0
    return-void

    .line 413
    :cond_0
    iget-object v0, p0, Lbem;->V:Lcby;

    invoke-virtual {v0, v1}, Lcby;->getItemId(I)J

    move-result-wide v2

    .line 415
    new-instance v4, Lbeo;

    invoke-direct {v4, p0, v2, v3}, Lbeo;-><init>(Lbem;J)V

    .line 433
    iget-object v0, p0, Lbem;->U:Lccc;

    const-class v2, Lccw;

    invoke-virtual {v0, v2}, Lccc;->a(Ljava/lang/Class;)Lcch;

    move-result-object v0

    check-cast v0, Lccw;

    .line 434
    invoke-virtual {v0, v1, v4}, Lccw;->a(ILjava/lang/Runnable;)V

    goto :goto_0
.end method

.method public d()Lcdz;
    .locals 1

    .prologue
    .line 444
    sget-object v0, Lcdz;->g:Lcdz;

    return-object v0
.end method

.method public d_(I)V
    .locals 1

    .prologue
    .line 383
    iget-object v0, p0, Lbem;->V:Lcby;

    invoke-virtual {v0, p1}, Lcby;->h(I)V

    .line 384
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 311
    iget-object v0, p0, Lbem;->U:Lccc;

    if-eqz v0, :cond_0

    .line 312
    const-string v1, "StoryboardEditorScreenScrollPosition"

    iget-object v0, p0, Lbem;->U:Lccc;

    const-class v2, Lcdm;

    .line 313
    invoke-virtual {v0, v2}, Lccc;->a(Ljava/lang/Class;)Lcch;

    move-result-object v0

    check-cast v0, Lcdm;

    invoke-virtual {v0}, Lcdm;->a()F

    move-result v0

    .line 312
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 315
    :cond_0
    const-string v0, "StoryboardEditorClipPositionLastEdited"

    iget v1, p0, Lbem;->S:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    .line 316
    return-void
.end method

.method public g(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 301
    invoke-super {p0, p1}, Llol;->g(Landroid/os/Bundle;)V

    .line 303
    invoke-static {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->b(Lu;)Lcom/google/android/apps/moviemaker/MovieMakerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o()Laol;

    move-result-object v0

    invoke-virtual {v0}, Laol;->f()Laru;

    move-result-object v0

    iput-object v0, p0, Lbem;->W:Laru;

    .line 304
    iget-object v0, p0, Lbem;->W:Laru;

    invoke-virtual {v0, p0}, Laru;->a(Lasc;)V

    .line 305
    invoke-static {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->b(Lu;)Lcom/google/android/apps/moviemaker/MovieMakerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o()Laol;

    move-result-object v0

    invoke-virtual {v0}, Laol;->l()Lanj;

    move-result-object v0

    .line 306
    invoke-virtual {v0, p0}, Lanj;->a(Lano;)V

    .line 307
    return-void
.end method

.method public h(Z)Landroid/view/animation/Animation;
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f4

    const/high16 v4, 0x3f800000    # 1.0f

    .line 532
    if-eqz p1, :cond_0

    .line 533
    const/4 v0, 0x0

    .line 546
    :goto_0
    return-object v0

    .line 535
    :cond_0
    invoke-static {}, Lcad;->a()Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 536
    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setProperty(Landroid/util/Property;)V

    .line 537
    invoke-virtual {v0, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 538
    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 539
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 540
    invoke-virtual {p0}, Lbem;->x()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 541
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 544
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v4, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 545
    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    goto :goto_0

    .line 538
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public w_()V
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lbem;->V:Lcby;

    invoke-virtual {v0}, Lcby;->b()V

    .line 394
    return-void
.end method

.method public z()V
    .locals 2

    .prologue
    .line 291
    iget-object v0, p0, Lbem;->ab:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lbem;->T:Lasd;

    iget-object v1, p0, Lbem;->ab:Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    invoke-virtual {v1}, Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lasd;->a(Ljava/lang/String;)V

    .line 296
    :cond_0
    invoke-super {p0}, Llol;->z()V

    .line 297
    return-void
.end method
