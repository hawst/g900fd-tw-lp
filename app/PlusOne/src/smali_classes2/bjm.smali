.class public final Lbjm;
.super Lbjl;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbjl",
        "<",
        "Lbhk;",
        ">;"
    }
.end annotation


# instance fields
.field private c:Lbjr;


# direct methods
.method public constructor <init>(Lbiw;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 128
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lbjm;-><init>(Lbiw;Ljava/util/concurrent/Executor;Lbjr;)V

    .line 129
    return-void
.end method

.method public constructor <init>(Lbiw;Ljava/util/concurrent/Executor;Lbjr;)V
    .locals 0

    .prologue
    .line 136
    invoke-direct {p0, p1, p2}, Lbjl;-><init>(Lbiw;Ljava/util/concurrent/Executor;)V

    .line 137
    iput-object p3, p0, Lbjm;->c:Lbjr;

    .line 138
    return-void
.end method


# virtual methods
.method public a(Landroid/media/MediaFormat;)Lbji;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/media/MediaFormat;",
            ")",
            "Lbji",
            "<",
            "Lbhk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 142
    invoke-static {p1}, Lbkf;->a(Landroid/media/MediaFormat;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "invalid media format for audio decoder factory "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 147
    :cond_0
    iget-object v0, p0, Lbjm;->c:Lbjr;

    if-nez v0, :cond_1

    .line 148
    iget-object v0, p0, Lbjm;->a:Lbiw;

    .line 149
    invoke-interface {v0}, Lbiw;->i()[Ljava/lang/String;

    move-result-object v0

    .line 148
    invoke-static {v0}, Lbjt;->a([Ljava/lang/String;)Lbjr;

    move-result-object v0

    iput-object v0, p0, Lbjm;->c:Lbjr;

    .line 152
    :cond_1
    iget-object v0, p0, Lbjm;->c:Lbjr;

    .line 153
    invoke-interface {v0, p1}, Lbjr;->a(Landroid/media/MediaFormat;)Lbjs;

    move-result-object v0

    iget-object v1, p0, Lbjm;->b:Ljava/util/concurrent/Executor;

    .line 152
    invoke-static {v0, v1}, Lbjk;->a(Lbjs;Ljava/util/concurrent/Executor;)Lbjk;

    move-result-object v0

    return-object v0
.end method
