.class public final Lfta;
.super Landroid/database/DataSetObserver;
.source "PG"


# instance fields
.field private synthetic a:Lcom/google/android/apps/plus/stories/views/StoryLayout;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/stories/views/StoryLayout;)V
    .locals 0

    .prologue
    .line 973
    iput-object p1, p0, Lfta;->a:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    return-void
.end method


# virtual methods
.method public onChanged()V
    .locals 2

    .prologue
    .line 976
    iget-object v0, p0, Lfta;->a:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(Lcom/google/android/apps/plus/stories/views/StoryLayout;I)I

    .line 977
    iget-object v0, p0, Lfta;->a:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-static {v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->b(Lcom/google/android/apps/plus/stories/views/StoryLayout;)Landroid/graphics/Path;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 978
    iget-object v0, p0, Lfta;->a:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-static {v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c(Lcom/google/android/apps/plus/stories/views/StoryLayout;)Lfrq;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 979
    iget-object v0, p0, Lfta;->a:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-static {v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c(Lcom/google/android/apps/plus/stories/views/StoryLayout;)Lfrq;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lfrq;->c(I)V

    .line 981
    :cond_0
    iget-object v0, p0, Lfta;->a:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->removeAllViewsInLayout()V

    .line 982
    iget-object v0, p0, Lfta;->a:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->requestLayout()V

    .line 983
    return-void
.end method

.method public onInvalidated()V
    .locals 0

    .prologue
    .line 987
    return-void
.end method
