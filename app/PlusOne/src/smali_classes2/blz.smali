.class public Lblz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbma;


# instance fields
.field private final a:[Lbmd;

.field private final b:[J

.field private final c:[Lboq;

.field private final d:J


# direct methods
.method public constructor <init>([Lbmd;[JJ[Lboq;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const-string v0, "clips"

    invoke-static {p1, v0, v8}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 32
    const-string v0, "startTimesUs"

    invoke-static {p2, v0, v8}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 33
    array-length v0, p1

    const-string v2, "clips.length"

    array-length v4, p2

    invoke-static {v0, v2, v4, v8}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    move v0, v1

    .line 34
    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_1

    .line 35
    add-int/lit8 v2, v0, -0x1

    aget-wide v4, p2, v2

    add-int/lit8 v2, v0, -0x1

    aget-object v2, p1, v2

    .line 36
    invoke-virtual {v2}, Lbmd;->b()J

    move-result-wide v6

    add-long/2addr v4, v6

    aget-wide v6, p2, v0

    cmp-long v2, v4, v6

    if-gtz v2, :cond_0

    move v2, v1

    :goto_1
    const-string v4, "Next clip must not start sooner than the previous one finishes"

    .line 35
    invoke-static {v2, v4}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 34
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v3

    .line 36
    goto :goto_1

    .line 40
    :cond_1
    const-string v0, "volumePoints"

    invoke-static {p5, v0, v8}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 42
    array-length v0, p1

    if-lez v0, :cond_2

    .line 43
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    .line 44
    aget-wide v4, p2, v0

    aget-object v0, p1, v0

    .line 45
    invoke-virtual {v0}, Lbmd;->b()J

    move-result-wide v6

    add-long/2addr v4, v6

    cmp-long v0, v4, p3

    if-gtz v0, :cond_3

    move v0, v1

    :goto_2
    const-string v2, "Last clip must not end after the duration of the sequence"

    .line 44
    invoke-static {v0, v2}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 49
    array-length v0, p5

    const/4 v2, 0x2

    if-lt v0, v2, :cond_4

    :goto_3
    const-string v0, "volumePoints must have at least two values"

    invoke-static {v1, v0}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 50
    aget-object v0, p5, v3

    iget-wide v0, v0, Lboq;->a:J

    const-string v2, "volumePoints[0].timestampUs"

    const-wide/16 v3, 0x0

    const-string v5, "First volume point must specify the volume for the timestamp 0"

    invoke-static/range {v0 .. v5}, Lcec;->a(JLjava/lang/CharSequence;JLjava/lang/CharSequence;)V

    .line 52
    array-length v0, p5

    add-int/lit8 v0, v0, -0x1

    aget-object v0, p5, v0

    iget-wide v0, v0, Lboq;->a:J

    const-string v2, "volumePoints[volumePoints.length - 1].timestampUs"

    const-string v5, "Last volume point must specify the volume for the last possible timestamp (equal to the duration of the sequence)"

    move-wide v3, p3

    invoke-static/range {v0 .. v5}, Lcec;->a(JLjava/lang/CharSequence;JLjava/lang/CharSequence;)V

    .line 58
    :cond_2
    iput-object p1, p0, Lblz;->a:[Lbmd;

    .line 59
    iput-object p2, p0, Lblz;->b:[J

    .line 60
    iput-object p5, p0, Lblz;->c:[Lboq;

    .line 61
    iput-wide p3, p0, Lblz;->d:J

    .line 62
    return-void

    :cond_3
    move v0, v3

    .line 45
    goto :goto_2

    :cond_4
    move v1, v3

    .line 49
    goto :goto_3
.end method

.method public static a(Ljava/util/List;J)Lblz;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;J)",
            "Lblz;"
        }
    .end annotation

    .prologue
    .line 191
    const-string v0, "clips"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 192
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Lbmd;

    .line 193
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [J

    .line 194
    const-wide/16 v4, 0x0

    .line 195
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 196
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 197
    iget-object v6, v0, Lbmd;->d:Lbmg;

    sget-object v7, Lbmg;->b:Lbmg;

    if-ne v6, v7, :cond_0

    const/4 v6, 0x1

    :goto_1
    const-string v7, "must be an audio clip"

    invoke-static {v6, v7}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 198
    aput-object v0, v2, v1

    .line 199
    aput-wide v4, v3, v1

    .line 200
    invoke-virtual {v0}, Lbmd;->b()J

    move-result-wide v6

    add-long/2addr v4, v6

    .line 195
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 197
    :cond_0
    const/4 v6, 0x0

    goto :goto_1

    .line 202
    :cond_1
    cmp-long v0, p1, v4

    if-ltz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    const-string v1, "sequenceDurationUs must be >= total duration of audio clips"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 204
    const/4 v0, 0x2

    new-array v6, v0, [Lboq;

    .line 205
    const/4 v0, 0x0

    new-instance v1, Lboq;

    const-wide/16 v4, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-direct {v1, v4, v5, v7}, Lboq;-><init>(JF)V

    aput-object v1, v6, v0

    .line 206
    const/4 v0, 0x1

    new-instance v1, Lboq;

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-direct {v1, p1, p2, v4}, Lboq;-><init>(JF)V

    aput-object v1, v6, v0

    .line 208
    new-instance v1, Lblz;

    move-wide v4, p1

    invoke-direct/range {v1 .. v6}, Lblz;-><init>([Lbmd;[JJ[Lboq;)V

    return-object v1

    .line 202
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public static b(J)Lblz;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 216
    new-array v2, v0, [Lbmd;

    .line 217
    new-array v3, v0, [J

    .line 218
    new-array v6, v0, [Lboq;

    .line 219
    new-instance v1, Lblz;

    move-wide v4, p0

    invoke-direct/range {v1 .. v6}, Lblz;-><init>([Lbmd;[JJ[Lboq;)V

    return-object v1
.end method


# virtual methods
.method public a(J)F
    .locals 9

    .prologue
    .line 125
    const-string v3, "timestampUs"

    const-wide/16 v4, 0x0

    iget-wide v6, p0, Lblz;->d:J

    move-wide v1, p1

    invoke-static/range {v1 .. v7}, Lcec;->a(JLjava/lang/CharSequence;JJ)J

    .line 127
    iget-object v0, p0, Lblz;->a:[Lbmd;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 129
    const/4 v0, 0x0

    .line 144
    :goto_0
    return v0

    .line 132
    :cond_0
    const/4 v0, 0x0

    .line 133
    :goto_1
    iget-object v1, p0, Lblz;->c:[Lboq;

    add-int/lit8 v2, v0, 0x1

    aget-object v1, v1, v2

    iget-wide v2, v1, Lboq;->a:J

    cmp-long v1, v2, p1

    if-gez v1, :cond_1

    .line 134
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 136
    :cond_1
    iget-object v1, p0, Lblz;->c:[Lboq;

    aget-object v1, v1, v0

    .line 137
    iget-object v2, p0, Lblz;->c:[Lboq;

    add-int/lit8 v0, v0, 0x1

    aget-object v0, v2, v0

    .line 138
    iget-wide v2, v0, Lboq;->a:J

    iget-wide v4, v1, Lboq;->a:J

    sub-long/2addr v2, v4

    .line 140
    iget-wide v4, v0, Lboq;->a:J

    sub-long/2addr v4, p1

    long-to-float v4, v4

    long-to-float v5, v2

    div-float/2addr v4, v5

    .line 142
    iget-wide v6, v1, Lboq;->a:J

    sub-long v6, p1, v6

    long-to-float v5, v6

    long-to-float v2, v2

    div-float v2, v5, v2

    .line 144
    iget v1, v1, Lboq;->b:F

    mul-float/2addr v1, v4

    iget v0, v0, Lboq;->b:F

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    goto :goto_0
.end method

.method public a()I
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lblz;->a:[Lbmd;

    array-length v0, v0

    return v0
.end method

.method public a(I)Lbmd;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lblz;->a:[Lbmd;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 81
    iget-wide v0, p0, Lblz;->d:J

    return-wide v0
.end method

.method public b(I)J
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lblz;->b:[J

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public c(I)J
    .locals 6

    .prologue
    .line 150
    const-string v0, "index"

    iget-object v1, p0, Lblz;->a:[Lbmd;

    invoke-static {p1, v0, v1}, Lcec;->a(ILjava/lang/CharSequence;[Ljava/lang/Object;)I

    .line 152
    const-wide/16 v2, 0x0

    .line 153
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    .line 154
    invoke-virtual {p0, v0}, Lblz;->a(I)Lbmd;

    move-result-object v1

    invoke-virtual {v1}, Lbmd;->b()J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 153
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 156
    :cond_0
    return-wide v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 96
    if-ne p1, p0, :cond_1

    move v1, v2

    .line 120
    :cond_0
    :goto_0
    return v1

    .line 99
    :cond_1
    if-eqz p1, :cond_0

    instance-of v0, p1, Lbma;

    if-eqz v0, :cond_0

    .line 100
    check-cast p1, Lbma;

    .line 101
    invoke-virtual {p0}, Lblz;->a()I

    move-result v0

    invoke-interface {p1}, Lbma;->a()I

    move-result v3

    if-ne v0, v3, :cond_0

    .line 104
    invoke-virtual {p0}, Lblz;->b()J

    move-result-wide v4

    invoke-interface {p1}, Lbma;->b()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    move v0, v1

    .line 108
    :goto_1
    invoke-virtual {p0}, Lblz;->a()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 109
    invoke-virtual {p0, v0}, Lblz;->a(I)Lbmd;

    move-result-object v3

    invoke-interface {p1, v0}, Lbma;->a(I)Lbmd;

    move-result-object v4

    invoke-static {v3, v4}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 108
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 113
    :goto_2
    invoke-virtual {p0}, Lblz;->a()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 114
    invoke-virtual {p0, v0}, Lblz;->b(I)J

    move-result-wide v4

    invoke-interface {p1, v0}, Lbma;->b(I)J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 113
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    move v1, v2

    .line 118
    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 87
    const-class v0, Lblz;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lblz;->a:[Lbmd;

    .line 88
    invoke-static {v3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lblz;->b:[J

    .line 89
    invoke-static {v3}, Ljava/util/Arrays;->toString([J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-wide v4, p0, Lblz;->d:J

    .line 90
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 87
    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
