.class public final Latg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbrw;


# instance fields
.field private final a:Lbjs;

.field private b:J

.field private c:I

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Latg;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbjs;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Latg;->a:Lbjs;

    .line 42
    return-void
.end method

.method private i()I
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 170
    move v0, v1

    .line 171
    :goto_0
    if-ne v0, v1, :cond_1

    .line 173
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 179
    :cond_0
    iget-object v0, p0, Latg;->a:Lbjs;

    invoke-interface {v0}, Lbjs;->f()I

    move-result v0

    goto :goto_0

    .line 181
    :cond_1
    return v0
.end method


# virtual methods
.method public a([BII)I
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 70
    :try_start_0
    invoke-direct {p0}, Latg;->i()I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 81
    iget-object v0, p0, Latg;->a:Lbjs;

    invoke-interface {v0}, Lbjs;->g()[Ljava/nio/ByteBuffer;

    move-result-object v0

    aget-object v0, v0, v1

    .line 85
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    invoke-static {v3, p3}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-virtual {v0, p1, p2, v3}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 89
    invoke-virtual {p0}, Latg;->d()J

    move-result-wide v4

    .line 93
    iget-object v0, p0, Latg;->a:Lbjs;

    move v6, v2

    invoke-interface/range {v0 .. v6}, Lbjs;->a(IIIJI)V

    .line 95
    iget v0, p0, Latg;->f:I

    add-int/2addr v0, v3

    iput v0, p0, Latg;->f:I

    .line 96
    :goto_0
    return v3

    .line 75
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    move v3, v2

    .line 76
    goto :goto_0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 54
    return-void
.end method

.method public a(III)V
    .locals 0

    .prologue
    .line 46
    iput p1, p0, Latg;->c:I

    .line 47
    iput p2, p0, Latg;->d:I

    .line 48
    iput p3, p0, Latg;->e:I

    .line 49
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 127
    const-string v0, "presentationTimeUs"

    .line 128
    invoke-static {p1, p2, v0}, Lcec;->b(JLjava/lang/CharSequence;)J

    move-result-wide v0

    iput-wide v0, p0, Latg;->b:J

    .line 129
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 59
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method public d()J
    .locals 6

    .prologue
    .line 118
    iget-wide v0, p0, Latg;->b:J

    iget v2, p0, Latg;->f:I

    iget v3, p0, Latg;->c:I

    iget v4, p0, Latg;->e:I

    iget v5, p0, Latg;->d:I

    invoke-static {v2, v3, v4, v5}, Lceg;->a(IIII)J

    move-result-wide v2

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    return v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 146
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 134
    const/4 v0, 0x1

    return v0
.end method

.method public h()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 156
    :try_start_0
    invoke-direct {p0}, Latg;->i()I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 166
    iget-object v0, p0, Latg;->a:Lbjs;

    const-wide/16 v4, 0x0

    const/4 v6, 0x4

    move v3, v2

    invoke-interface/range {v0 .. v6}, Lbjs;->a(IIIJI)V

    .line 167
    :goto_0
    return-void

    .line 160
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method
