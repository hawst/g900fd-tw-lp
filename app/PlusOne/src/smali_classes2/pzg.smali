.class public final Lpzg;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Float;

.field public b:Ljava/lang/Float;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 356
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 377
    const/4 v0, 0x0

    .line 378
    iget-object v1, p0, Lpzg;->a:Ljava/lang/Float;

    if-eqz v1, :cond_0

    .line 379
    const/4 v0, 0x1

    iget-object v1, p0, Lpzg;->a:Ljava/lang/Float;

    .line 380
    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 382
    :cond_0
    iget-object v1, p0, Lpzg;->b:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 383
    const/4 v1, 0x2

    iget-object v2, p0, Lpzg;->b:Ljava/lang/Float;

    .line 384
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 386
    :cond_1
    iget-object v1, p0, Lpzg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 387
    iput v0, p0, Lpzg;->ai:I

    .line 388
    return v0
.end method

.method public a(Loxn;)Lpzg;
    .locals 2

    .prologue
    .line 396
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 397
    sparse-switch v0, :sswitch_data_0

    .line 401
    iget-object v1, p0, Lpzg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 402
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpzg;->ah:Ljava/util/List;

    .line 405
    :cond_1
    iget-object v1, p0, Lpzg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 407
    :sswitch_0
    return-object p0

    .line 412
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpzg;->a:Ljava/lang/Float;

    goto :goto_0

    .line 416
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lpzg;->b:Ljava/lang/Float;

    goto :goto_0

    .line 397
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, Lpzg;->a:Ljava/lang/Float;

    if-eqz v0, :cond_0

    .line 366
    const/4 v0, 0x1

    iget-object v1, p0, Lpzg;->a:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 368
    :cond_0
    iget-object v0, p0, Lpzg;->b:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 369
    const/4 v0, 0x2

    iget-object v1, p0, Lpzg;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 371
    :cond_1
    iget-object v0, p0, Lpzg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 373
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 352
    invoke-virtual {p0, p1}, Lpzg;->a(Loxn;)Lpzg;

    move-result-object v0

    return-object v0
.end method
