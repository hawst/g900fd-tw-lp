.class public final enum Lbmg;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbmg;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lbmg;

.field public static final enum b:Lbmg;

.field public static final enum c:Lbmg;

.field public static final enum d:Lbmg;

.field public static final enum e:Lbmg;

.field private static final synthetic g:[Lbmg;


# instance fields
.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 40
    new-instance v0, Lbmg;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v7, v3}, Lbmg;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbmg;->a:Lbmg;

    .line 41
    new-instance v0, Lbmg;

    const-string v1, "AUDIO"

    invoke-direct {v0, v1, v3, v4}, Lbmg;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbmg;->b:Lbmg;

    .line 42
    new-instance v0, Lbmg;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v4, v5}, Lbmg;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbmg;->c:Lbmg;

    .line 43
    new-instance v0, Lbmg;

    const-string v1, "EMPTY_VIDEO"

    invoke-direct {v0, v1, v5, v6}, Lbmg;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbmg;->d:Lbmg;

    .line 44
    new-instance v0, Lbmg;

    const-string v1, "END_CARD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v6, v2}, Lbmg;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lbmg;->e:Lbmg;

    .line 39
    const/4 v0, 0x5

    new-array v0, v0, [Lbmg;

    sget-object v1, Lbmg;->a:Lbmg;

    aput-object v1, v0, v7

    sget-object v1, Lbmg;->b:Lbmg;

    aput-object v1, v0, v3

    sget-object v1, Lbmg;->c:Lbmg;

    aput-object v1, v0, v4

    sget-object v1, Lbmg;->d:Lbmg;

    aput-object v1, v0, v5

    sget-object v1, Lbmg;->e:Lbmg;

    aput-object v1, v0, v6

    sput-object v0, Lbmg;->g:[Lbmg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 49
    iput p3, p0, Lbmg;->f:I

    .line 50
    return-void
.end method

.method public static a(I)Lbmg;
    .locals 2

    .prologue
    .line 57
    packed-switch p0, :pswitch_data_0

    .line 69
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x22

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unsupported clip type: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 59
    :pswitch_1
    sget-object v0, Lbmg;->a:Lbmg;

    .line 67
    :goto_0
    return-object v0

    .line 61
    :pswitch_2
    sget-object v0, Lbmg;->b:Lbmg;

    goto :goto_0

    .line 63
    :pswitch_3
    sget-object v0, Lbmg;->c:Lbmg;

    goto :goto_0

    .line 65
    :pswitch_4
    sget-object v0, Lbmg;->e:Lbmg;

    goto :goto_0

    .line 67
    :pswitch_5
    sget-object v0, Lbmg;->d:Lbmg;

    goto :goto_0

    .line 57
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lbmg;
    .locals 1

    .prologue
    .line 39
    const-class v0, Lbmg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbmg;

    return-object v0
.end method

.method public static values()[Lbmg;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lbmg;->g:[Lbmg;

    invoke-virtual {v0}, [Lbmg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbmg;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lbmg;->f:I

    return v0
.end method
