.class public final Lkac;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljxv;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lhei;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-object p1, p0, Lkac;->a:Landroid/content/Context;

    .line 32
    return-void
.end method

.method private a()Lhei;
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lkac;->b:Lhei;

    if-nez v0, :cond_0

    .line 78
    iget-object v0, p0, Lkac;->a:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lkac;->b:Lhei;

    .line 80
    :cond_0
    iget-object v0, p0, Lkac;->b:Lhei;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/content/Context;Lhem;)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lheq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    return-void
.end method

.method public a(I)Z
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0}, Lkac;->a()Lhei;

    move-result-object v0

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "SEEN_FIRST_TIME_VOTER_IPH"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public b(I)V
    .locals 3

    .prologue
    .line 51
    invoke-direct {p0}, Lkac;->a()Lhei;

    move-result-object v0

    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "SEEN_FIRST_TIME_VOTER_IPH"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    invoke-interface {v0}, Lhek;->c()I

    .line 52
    return-void
.end method

.method public c(I)Z
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Lkac;->a()Lhei;

    move-result-object v0

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "SEEN_ADD_PHOTOS_TO_INDIVIDUAL_CHOICES_IPH"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public d(I)V
    .locals 3

    .prologue
    .line 62
    invoke-direct {p0}, Lkac;->a()Lhei;

    move-result-object v0

    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "SEEN_ADD_PHOTOS_TO_INDIVIDUAL_CHOICES_IPH"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    .line 63
    invoke-interface {v0}, Lhek;->c()I

    .line 64
    return-void
.end method
