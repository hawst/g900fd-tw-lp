.class public Layt;
.super Laye;
.source "PG"

# interfaces
.implements Layj;


# static fields
.field public static final a:Landroid/animation/TimeInterpolator;


# instance fields
.field private final b:Landroid/graphics/Typeface;

.field private final c:I

.field private final d:F

.field private final e:F

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:Z

.field private final j:I

.field private final k:I

.field private final l:F

.field private final m:Z

.field private final n:F

.field private final o:F

.field private final p:F

.field private final q:I

.field private final r:I

.field private final s:J

.field private final t:J

.field private final u:Layc;

.field private final v:I

.field private w:Landroid/graphics/Bitmap;

.field private x:Landroid/graphics/Matrix;

.field private y:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcak;->a:Lcak;

    sput-object v0, Layt;->a:Landroid/animation/TimeInterpolator;

    return-void
.end method

.method public constructor <init>(Layu;)V
    .locals 3

    .prologue
    .line 93
    iget-object v0, p1, Layu;->d:Layj;

    invoke-direct {p0, v0}, Laye;-><init>(Layj;)V

    .line 94
    iget-object v0, p1, Layu;->k:Landroid/graphics/Typeface;

    const-string v1, "builder.mTypeface"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Typeface;

    iput-object v0, p0, Layt;->b:Landroid/graphics/Typeface;

    .line 95
    iget v0, p1, Layu;->f:I

    iput v0, p0, Layt;->c:I

    .line 96
    iget v0, p1, Layu;->e:I

    iput v0, p0, Layt;->h:I

    .line 97
    iget v0, p1, Layu;->g:F

    iput v0, p0, Layt;->d:F

    .line 98
    iget v0, p1, Layu;->h:F

    iput v0, p0, Layt;->e:F

    .line 99
    iget v0, p1, Layu;->i:I

    iput v0, p0, Layt;->f:I

    .line 100
    iget v0, p1, Layu;->j:I

    iput v0, p0, Layt;->g:I

    .line 101
    iget v0, p1, Layu;->l:F

    iput v0, p0, Layt;->l:F

    .line 102
    iget-boolean v0, p1, Layu;->a:Z

    iput-boolean v0, p0, Layt;->i:Z

    .line 103
    iget v0, p1, Layu;->c:I

    iput v0, p0, Layt;->j:I

    .line 104
    iget v0, p1, Layu;->b:I

    iput v0, p0, Layt;->k:I

    .line 105
    iget-boolean v0, p1, Layu;->m:Z

    iput-boolean v0, p0, Layt;->m:Z

    .line 106
    iget v0, p1, Layu;->n:F

    iput v0, p0, Layt;->n:F

    .line 107
    iget v0, p1, Layu;->o:F

    iput v0, p0, Layt;->o:F

    .line 108
    iget v0, p1, Layu;->p:F

    iput v0, p0, Layt;->p:F

    .line 109
    iget v0, p1, Layu;->q:I

    iput v0, p0, Layt;->q:I

    .line 110
    iget v0, p1, Layu;->r:I

    iput v0, p0, Layt;->r:I

    .line 111
    iget-object v0, p1, Layu;->s:Layc;

    iput-object v0, p0, Layt;->u:Layc;

    .line 112
    iget v0, p1, Layu;->t:I

    iput v0, p0, Layt;->v:I

    .line 113
    iget-wide v0, p1, Layu;->u:J

    iput-wide v0, p0, Layt;->s:J

    .line 114
    iget-wide v0, p1, Layu;->v:J

    iput-wide v0, p0, Layt;->t:J

    .line 115
    return-void
.end method

.method private a(C)Z
    .locals 1

    .prologue
    .line 367
    const/16 v0, 0xa0

    if-eq p1, v0, :cond_0

    invoke-static {p1}, Ljava/lang/Character;->isLetterOrDigit(C)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lbaj;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 354
    invoke-virtual {p1}, Lbaj;->getLayout()Landroid/text/Layout;

    move-result-object v2

    .line 355
    invoke-virtual {p1}, Lbaj;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    move v0, v1

    .line 356
    :goto_0
    invoke-virtual {p1}, Lbaj;->getLineCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_0

    .line 358
    invoke-virtual {v2, v0}, Landroid/text/Layout;->getLineEnd(I)I

    move-result v4

    .line 359
    add-int/lit8 v5, v4, -0x1

    invoke-interface {v3, v5}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v5

    invoke-direct {p0, v5}, Layt;->a(C)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-interface {v3, v4}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    invoke-direct {p0, v4}, Layt;->a(C)Z

    move-result v4

    if-nez v4, :cond_1

    .line 360
    const/4 v1, 0x1

    .line 363
    :cond_0
    return v1

    .line 357
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private a(Lbaj;IIIFI)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 322
    int-to-float v2, p2

    mul-float/2addr v2, p5

    const/high16 v3, 0x44440000    # 784.0f

    div-float/2addr v2, v3

    .line 323
    new-instance v3, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x2

    invoke-direct {v3, p3, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v3}, Lbaj;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 325
    invoke-virtual {p1, v0, v2}, Lbaj;->setTextSize(IF)V

    .line 327
    iget-boolean v3, p0, Layt;->m:Z

    if-eqz v3, :cond_0

    .line 328
    iget v3, p0, Layt;->n:F

    mul-float/2addr v3, v2

    iget v4, p0, Layt;->o:F

    mul-float/2addr v4, v2

    iget v5, p0, Layt;->p:F

    mul-float/2addr v2, v5

    iget v5, p0, Layt;->q:I

    invoke-virtual {p1, v3, v4, v2, v5}, Lbaj;->setShadowLayer(FFFI)V

    .line 332
    :cond_0
    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {p3, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {p1, v2, v0}, Lbaj;->measure(II)V

    .line 335
    invoke-direct {p0, p1}, Layt;->a(Lbaj;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 347
    :cond_1
    :goto_0
    return v0

    .line 338
    :cond_2
    const/4 v2, -0x1

    if-eq p6, v2, :cond_3

    .line 339
    invoke-virtual {p1}, Lbaj;->getLineCount()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lbaj;->getLineCount()I

    move-result v2

    if-gt v2, p6, :cond_1

    move v0, v1

    .line 340
    goto :goto_0

    .line 343
    :cond_3
    invoke-virtual {p1}, Lbaj;->getMeasuredHeight()I

    move-result v2

    invoke-static {v2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    if-gt v2, p4, :cond_1

    move v0, v1

    .line 344
    goto :goto_0
.end method


# virtual methods
.method protected A(Layl;)F
    .locals 6

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 173
    invoke-static {p1}, Lbag;->e(Layl;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 174
    const/4 v0, 0x0

    .line 183
    :cond_0
    :goto_0
    return v0

    .line 177
    :cond_1
    iget-wide v2, p1, Layl;->a:J

    iget-object v1, p1, Layl;->r:Lbmp;

    iget-wide v4, v1, Lbmp;->b:J

    sub-long/2addr v2, v4

    .line 179
    iget-wide v4, p0, Layt;->s:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 180
    long-to-float v1, v2

    mul-float/2addr v0, v1

    iget-wide v2, p0, Layt;->s:J

    long-to-float v1, v2

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method protected B(Layl;)F
    .locals 6

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 194
    invoke-static {p1}, Lbag;->e(Layl;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 195
    const/4 v0, 0x0

    .line 204
    :cond_0
    :goto_0
    return v0

    .line 198
    :cond_1
    iget-object v1, p1, Layl;->r:Lbmp;

    iget-wide v2, v1, Lbmp;->c:J

    iget-wide v4, p1, Layl;->a:J

    sub-long/2addr v2, v4

    .line 200
    iget-wide v4, p0, Layt;->t:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 201
    long-to-float v1, v2

    mul-float/2addr v0, v1

    iget-wide v2, p0, Layt;->t:J

    long-to-float v1, v2

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method protected C(Layl;)F
    .locals 2

    .prologue
    .line 220
    invoke-virtual {p0, p1}, Layt;->A(Layl;)F

    move-result v0

    invoke-virtual {p0, p1}, Layt;->B(Layl;)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method protected a(Landroid/content/Context;Landroid/graphics/Bitmap;Layl;II)Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 226
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 227
    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p4, p5, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p2

    .line 231
    :cond_1
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 232
    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v6, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 233
    iget v1, p0, Layt;->r:I

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 235
    new-instance v1, Landroid/widget/LinearLayout;

    invoke-direct {v1, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 236
    new-instance v2, Lbaj;

    invoke-direct {v2, p1}, Lbaj;-><init>(Landroid/content/Context;)V

    .line 238
    int-to-float v3, p4

    int-to-float v4, p5

    invoke-static {v3, v4}, Lcfn;->a(FF)F

    move-result v3

    .line 239
    iget v4, p0, Layt;->g:I

    int-to-float v4, v4

    mul-float/2addr v4, v3

    const/high16 v5, 0x44440000    # 784.0f

    div-float/2addr v4, v5

    float-to-int v4, v4

    .line 240
    invoke-virtual {v2, v4, v4, v4, v4}, Lbaj;->setPadding(IIII)V

    .line 241
    invoke-virtual {v2, v6}, Lbaj;->setVisibility(I)V

    .line 242
    iget v4, p0, Layt;->c:I

    invoke-virtual {v2, v4}, Lbaj;->setGravity(I)V

    .line 244
    iget-object v4, p3, Layl;->p:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lbaj;->setText(Ljava/lang/CharSequence;)V

    .line 245
    iget v4, p0, Layt;->h:I

    invoke-virtual {v2, v4}, Lbaj;->setTextColor(I)V

    .line 246
    iget-boolean v4, p0, Layt;->i:Z

    invoke-virtual {v2, v4}, Lbaj;->setAllCaps(Z)V

    .line 248
    iget-object v4, p0, Layt;->b:Landroid/graphics/Typeface;

    invoke-virtual {v2, v4}, Lbaj;->setTypeface(Landroid/graphics/Typeface;)V

    .line 249
    iget v4, p0, Layt;->f:I

    int-to-float v4, v4

    const v5, 0x3b83126f    # 0.004f

    mul-float/2addr v4, v5

    invoke-virtual {v2, v4}, Lbaj;->a(F)V

    .line 251
    iget v4, p0, Layt;->e:F

    cmpl-float v4, v4, v7

    if-eqz v4, :cond_2

    .line 252
    iget v4, p0, Layt;->e:F

    iget v5, p0, Layt;->d:F

    div-float/2addr v4, v5

    invoke-virtual {v2, v7, v4}, Lbaj;->setLineSpacing(FF)V

    .line 255
    :cond_2
    invoke-virtual {v0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v4

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getHeight()I

    move-result v5

    invoke-virtual {p0, v2, v4, v5, v3}, Layt;->a(Lbaj;IIF)V

    .line 257
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 258
    invoke-virtual {v0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 259
    invoke-virtual {v0}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    invoke-static {v3, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 258
    invoke-virtual {v1, v2, v3}, Landroid/widget/LinearLayout;->measure(II)V

    .line 260
    invoke-virtual {v0}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    invoke-virtual {v1, v6, v6, v2, v3}, Landroid/widget/LinearLayout;->layout(IIII)V

    .line 261
    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 263
    return-object p2
.end method

.method public a(Landroid/content/Context;Layl;II)Landroid/graphics/Bitmap;
    .locals 6

    .prologue
    .line 120
    iget-object v0, p2, Layl;->p:Ljava/lang/String;

    const-string v1, "title"

    invoke-static {v0, v1}, Lcec;->a(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/String;

    .line 121
    iget-object v0, p0, Layt;->w:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Layt;->w:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    if-ne v0, p3, :cond_0

    iget-object v0, p0, Layt;->w:Landroid/graphics/Bitmap;

    .line 122
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    if-eq v0, p4, :cond_1

    .line 123
    :cond_0
    iget-object v0, p0, Layt;->w:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Layt;->w:Landroid/graphics/Bitmap;

    .line 126
    :cond_1
    iget-object v0, p0, Layt;->w:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iget-object v0, p2, Layl;->p:Ljava/lang/String;

    iget-object v1, p0, Layt;->y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 127
    :cond_2
    iget-object v2, p0, Layt;->w:Landroid/graphics/Bitmap;

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-virtual/range {v0 .. v5}, Layt;->a(Landroid/content/Context;Landroid/graphics/Bitmap;Layl;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Layt;->w:Landroid/graphics/Bitmap;

    .line 128
    iget-object v0, p2, Layl;->p:Ljava/lang/String;

    iput-object v0, p0, Layt;->y:Ljava/lang/String;

    .line 130
    :cond_3
    iget-object v0, p0, Layt;->w:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method protected a(Lbaj;IIF)V
    .locals 7

    .prologue
    .line 280
    iget v0, p0, Layt;->j:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget v0, p0, Layt;->d:F

    float-to-int v2, v0

    :goto_0
    if-lez v2, :cond_1

    int-to-float v0, v2

    iget v1, p0, Layt;->d:F

    div-float/2addr v0, v1

    const v1, 0x3f4ccccd    # 0.8f

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_1

    iget v6, p0, Layt;->j:I

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, Layt;->a(Lbaj;IIIFI)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_2

    .line 282
    iget v0, p0, Layt;->d:F

    float-to-int v2, v0

    :goto_2
    if-lez v2, :cond_2

    .line 283
    iget v6, p0, Layt;->k:I

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, Layt;->a(Lbaj;IIIFI)Z

    move-result v0

    if-nez v0, :cond_2

    .line 285
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 280
    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    .line 290
    :cond_2
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, p2, p3}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v0}, Lbaj;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 291
    return-void
.end method

.method public g_()Layc;
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Layt;->u:Layc;

    if-eqz v0, :cond_0

    .line 154
    iget-object v0, p0, Layt;->u:Layc;

    .line 156
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Laye;->g_()Layc;

    move-result-object v0

    goto :goto_0
.end method

.method public o(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Layt;->x:Landroid/graphics/Matrix;

    if-nez v0, :cond_0

    .line 136
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Layt;->x:Landroid/graphics/Matrix;

    .line 138
    :cond_0
    iget-object v0, p0, Layt;->x:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public p(Layl;)F
    .locals 2

    .prologue
    .line 143
    iget v0, p0, Layt;->l:F

    invoke-virtual {p0, p1}, Layt;->C(Layl;)F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method public q(Layl;)I
    .locals 1

    .prologue
    .line 148
    iget v0, p0, Layt;->v:I

    return v0
.end method

.method public u(Layl;)F
    .locals 3

    .prologue
    .line 161
    invoke-virtual {p0}, Layt;->h()Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->u(Layl;)F

    move-result v0

    .line 162
    invoke-virtual {p0}, Layt;->g_()Layc;

    move-result-object v1

    invoke-virtual {v1}, Layc;->b()F

    move-result v1

    invoke-virtual {p0, p1}, Layt;->B(Layl;)F

    move-result v2

    .line 161
    sub-float/2addr v1, v0

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method
