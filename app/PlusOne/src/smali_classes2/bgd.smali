.class public final Lbgd;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:[F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lbgd;->a:[F

    return-void
.end method

.method public static a(FFFF)Lbgd;
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 16
    new-instance v0, Lbgd;

    invoke-direct {v0}, Lbgd;-><init>()V

    .line 17
    iget-object v1, v0, Lbgd;->a:[F

    const/4 v2, 0x0

    aput p0, v1, v2

    .line 18
    iget-object v1, v0, Lbgd;->a:[F

    const/4 v2, 0x5

    aput p1, v1, v2

    .line 19
    iget-object v1, v0, Lbgd;->a:[F

    const/16 v2, 0xa

    aput v3, v1, v2

    .line 20
    iget-object v1, v0, Lbgd;->a:[F

    const/16 v2, 0xf

    aput v3, v1, v2

    .line 22
    iget-object v1, v0, Lbgd;->a:[F

    const/16 v2, 0xc

    aput p2, v1, v2

    .line 23
    iget-object v1, v0, Lbgd;->a:[F

    const/16 v2, 0xd

    aput p3, v1, v2

    .line 24
    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v2, 0x0

    .line 57
    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_2

    move v1, v2

    .line 58
    :goto_1
    if-ge v1, v6, :cond_1

    .line 59
    iget-object v4, p0, Lbgd;->a:[F

    shl-int/lit8 v0, v3, 0x2

    add-int v5, v0, v1

    if-ne v3, v1, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_2
    aput v0, v4, v5

    .line 58
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 59
    :cond_0
    const/4 v0, 0x0

    goto :goto_2

    .line 57
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 62
    :cond_2
    return-void
.end method

.method public a(Lbgd;)V
    .locals 3

    .prologue
    .line 42
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lbgd;->a:[F

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 43
    iget-object v1, p0, Lbgd;->a:[F

    iget-object v2, p1, Lbgd;->a:[F

    aget v2, v2, v0

    aput v2, v1, v0

    .line 42
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 45
    :cond_0
    return-void
.end method

.method public a(Lbgd;Lbgd;F)V
    .locals 4

    .prologue
    .line 51
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lbgd;->a:[F

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 52
    iget-object v1, p0, Lbgd;->a:[F

    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v2, p3

    iget-object v3, p1, Lbgd;->a:[F

    aget v3, v3, v0

    mul-float/2addr v2, v3

    iget-object v3, p2, Lbgd;->a:[F

    aget v3, v3, v0

    mul-float/2addr v3, p3

    add-float/2addr v2, v3

    aput v2, v1, v0

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54
    :cond_0
    return-void
.end method

.method public a([F)V
    .locals 2

    .prologue
    .line 30
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lbgd;->a:[F

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 31
    iget-object v1, p0, Lbgd;->a:[F

    aget v1, v1, v0

    aput v1, p1, v0

    .line 30
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 33
    :cond_0
    return-void
.end method

.method public b()F
    .locals 2

    .prologue
    .line 66
    iget-object v0, p0, Lbgd;->a:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public c()F
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lbgd;->a:[F

    const/4 v1, 0x5

    aget v0, v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 76
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lbgd;->a:[F

    invoke-static {v1}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
