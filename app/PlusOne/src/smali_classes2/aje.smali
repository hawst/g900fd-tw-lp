.class final Laje;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lahy;


# instance fields
.field private final a:Lahy;

.field private final b:[B

.field private synthetic c:Lajd;


# direct methods
.method constructor <init>(Lajd;Lahy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 249
    iput-object p1, p0, Laje;->c:Lajd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 250
    const-string v0, "callback"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahy;

    iput-object v0, p0, Laje;->a:Lahy;

    .line 251
    iput-object v1, p0, Laje;->b:[B

    .line 252
    return-void
.end method

.method constructor <init>(Lajd;Lahy;[B)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 254
    iput-object p1, p0, Laje;->c:Lajd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 255
    const-string v0, "callback"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahy;

    iput-object v0, p0, Laje;->a:Lahy;

    .line 256
    if-nez p3, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v0, p0, Laje;->b:[B

    .line 257
    return-void

    .line 256
    :cond_0
    invoke-virtual {p3}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    goto :goto_0
.end method

.method private a(Lbkr;)Lbkr;
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Laje;->b:[B

    if-eqz v0, :cond_0

    .line 301
    invoke-static {p1}, Lbkl;->a(Lbkr;)Lbkm;

    move-result-object v0

    iget-object v1, p0, Laje;->b:[B

    invoke-virtual {v0, v1}, Lbkm;->a([B)Lbkm;

    move-result-object v0

    invoke-virtual {v0}, Lbkm;->c()Lbkr;

    move-result-object p1

    .line 303
    :cond_0
    return-object p1
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Laje;->a:Lahy;

    invoke-interface {v0, p1}, Lahy;->a(Landroid/graphics/Bitmap;)V

    .line 291
    return-void
.end method

.method public a(Landroid/net/Uri;J)V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Laje;->a:Lahy;

    invoke-interface {v0, p1, p2, p3}, Lahy;->a(Landroid/net/Uri;J)V

    .line 286
    return-void
.end method

.method public a(Landroid/net/Uri;Laic;Lbkr;)V
    .locals 2

    .prologue
    .line 261
    invoke-direct {p0, p3}, Laje;->a(Lbkr;)Lbkr;

    move-result-object v0

    .line 263
    iget-object v1, p0, Laje;->c:Lajd;

    invoke-static {v1, v0}, Lajd;->a(Lajd;Lbkr;)V

    .line 264
    iget-object v1, p0, Laje;->c:Lajd;

    invoke-static {v1}, Lajd;->a(Lajd;)Lajl;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lbks;->a(Landroid/net/Uri;Lbkr;Lajl;)Lbkg;

    move-result-object v0

    .line 265
    iget-object v1, p0, Laje;->a:Lahy;

    invoke-interface {v1, p1, p2, v0}, Lahy;->a(Landroid/net/Uri;Laic;Lbkr;)V

    .line 266
    return-void
.end method

.method public a(Landroid/net/Uri;Laic;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Laje;->a:Lahy;

    invoke-interface {v0, p1, p2, p3}, Lahy;->a(Landroid/net/Uri;Laic;Ljava/lang/Exception;)V

    .line 281
    return-void
.end method

.method public a(Landroid/net/Uri;Lbkr;)V
    .locals 2

    .prologue
    .line 295
    invoke-direct {p0, p2}, Laje;->a(Lbkr;)Lbkr;

    move-result-object v0

    .line 296
    iget-object v1, p0, Laje;->a:Lahy;

    invoke-interface {v1, p1, v0}, Lahy;->a(Landroid/net/Uri;Lbkr;)V

    .line 297
    return-void
.end method

.method public b(Landroid/net/Uri;Laic;Lbkr;)V
    .locals 2

    .prologue
    .line 270
    invoke-direct {p0, p3}, Laje;->a(Lbkr;)Lbkr;

    move-result-object v0

    .line 273
    iget-object v1, p0, Laje;->c:Lajd;

    invoke-static {v1}, Lajd;->a(Lajd;)Lajl;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lbks;->a(Landroid/net/Uri;Lbkr;Lajl;)Lbkg;

    move-result-object v0

    .line 274
    iget-object v1, p0, Laje;->a:Lahy;

    invoke-interface {v1, p1, p2, v0}, Lahy;->b(Landroid/net/Uri;Laic;Lbkr;)V

    .line 275
    return-void
.end method
