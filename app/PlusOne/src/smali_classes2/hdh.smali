.class public final Lhdh;
.super Lhdx;
.source "PG"


# instance fields
.field private a:Lhdj;

.field private b:Lhdi;

.field private final c:Landroid/graphics/PointF;

.field private final d:Landroid/graphics/PointF;

.field private e:F

.field private g:F

.field private h:I

.field private i:F

.field private j:F

.field private k:F


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lhdx;-><init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V

    .line 24
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lhdh;->c:Landroid/graphics/PointF;

    .line 25
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lhdh;->d:Landroid/graphics/PointF;

    .line 30
    const/4 v0, 0x1

    iput v0, p0, Lhdh;->h:I

    .line 38
    return-void
.end method


# virtual methods
.method public a(Lhdi;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lhdh;->b:Lhdi;

    .line 54
    return-void
.end method

.method public a(Lhdj;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    iput-object p1, p0, Lhdh;->a:Lhdj;

    .line 43
    iget-object v0, p0, Lhdh;->a:Lhdj;

    if-eqz v0, :cond_0

    .line 44
    iget-object v0, p0, Lhdh;->a:Lhdj;

    invoke-interface {v0}, Lhdj;->a()F

    move-result v0

    iput v0, p0, Lhdh;->e:F

    .line 45
    iget-object v0, p0, Lhdh;->a:Lhdj;

    invoke-interface {v0}, Lhdj;->b()F

    move-result v0

    iput v0, p0, Lhdh;->g:F

    .line 50
    :goto_0
    return-void

    .line 47
    :cond_0
    iput v1, p0, Lhdh;->e:F

    .line 48
    iput v1, p0, Lhdh;->g:F

    goto :goto_0
.end method

.method public c(FF)Z
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lhdh;->c:Landroid/graphics/PointF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 59
    iget-object v0, p0, Lhdh;->d:Landroid/graphics/PointF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 61
    const/4 v0, 0x1

    return v0
.end method

.method public d(FF)Z
    .locals 8

    .prologue
    const/4 v1, 0x3

    const/high16 v7, 0x3f400000    # 0.75f

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 66
    iget-object v0, p0, Lhdh;->d:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    sub-float v0, p1, v0

    .line 67
    iget-object v4, p0, Lhdh;->d:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float v4, p2, v4

    .line 69
    iget v5, p0, Lhdh;->h:I

    if-ne v5, v2, :cond_0

    iget-object v5, p0, Lhdh;->d:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    iget-object v6, p0, Lhdh;->d:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    .line 70
    invoke-static {p1, p2, v5, v6}, Lhdo;->b(FFFF)F

    move-result v5

    const/high16 v6, 0x42c80000    # 100.0f

    cmpg-float v5, v5, v6

    if-gez v5, :cond_0

    .line 107
    :goto_0
    return v2

    .line 75
    :cond_0
    iget v5, p0, Lhdh;->h:I

    if-ne v5, v2, :cond_4

    move v5, v2

    .line 76
    :goto_1
    if-eqz v5, :cond_1

    .line 77
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v0, v0, v4

    if-lez v0, :cond_5

    const/4 v0, 0x2

    :goto_2
    iput v0, p0, Lhdh;->h:I

    .line 86
    :cond_1
    iget v0, p0, Lhdh;->h:I

    if-ne v0, v1, :cond_7

    .line 87
    iget-object v0, p0, Lhdh;->c:Landroid/graphics/PointF;

    iget v4, v0, Landroid/graphics/PointF;->y:F

    .line 88
    iget-object v0, p0, Lhdh;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget-object v1, p0, Lhdh;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v1}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v6, p0, Lhdh;->c:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v6

    cmpl-float v0, v0, v1

    if-lez v0, :cond_6

    move v0, v2

    .line 89
    :goto_3
    iget-object v1, p0, Lhdh;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v1}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v7

    move v3, v4

    move v4, v0

    move v0, p2

    .line 98
    :goto_4
    if-eqz v5, :cond_a

    .line 99
    iput v3, p0, Lhdh;->k:F

    iget-object v0, p0, Lhdh;->a:Lhdj;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lhdh;->a:Lhdj;

    invoke-interface {v0}, Lhdj;->c()F

    move-result v0

    :goto_5
    iput v0, p0, Lhdh;->j:F

    iget v0, p0, Lhdh;->g:F

    iget v3, p0, Lhdh;->e:F

    sub-float/2addr v0, v3

    div-float/2addr v0, v1

    iput v0, p0, Lhdh;->i:F

    if-eqz v4, :cond_2

    iget v0, p0, Lhdh;->i:F

    neg-float v0, v0

    iput v0, p0, Lhdh;->i:F

    :cond_2
    iget-object v0, p0, Lhdh;->b:Lhdi;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhdh;->b:Lhdi;

    invoke-interface {v0}, Lhdi;->a()V

    .line 104
    :cond_3
    :goto_6
    iget-object v0, p0, Lhdh;->d:Landroid/graphics/PointF;

    iput p1, v0, Landroid/graphics/PointF;->x:F

    .line 105
    iget-object v0, p0, Lhdh;->d:Landroid/graphics/PointF;

    iput p2, v0, Landroid/graphics/PointF;->y:F

    goto :goto_0

    :cond_4
    move v5, v3

    .line 75
    goto :goto_1

    :cond_5
    move v0, v1

    .line 77
    goto :goto_2

    :cond_6
    move v0, v3

    .line 88
    goto :goto_3

    .line 92
    :cond_7
    iget-object v0, p0, Lhdh;->c:Landroid/graphics/PointF;

    iget v1, v0, Landroid/graphics/PointF;->x:F

    .line 93
    iget-object v0, p0, Lhdh;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iget-object v4, p0, Lhdh;->c:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    cmpl-float v0, v0, v4

    if-lez v0, :cond_8

    move v3, v2

    .line 94
    :cond_8
    iget-object v0, p0, Lhdh;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v7

    move v4, v3

    move v3, v1

    move v1, v0

    move v0, p1

    .line 95
    goto :goto_4

    .line 99
    :cond_9
    const/4 v0, 0x0

    goto :goto_5

    .line 101
    :cond_a
    iget v1, p0, Lhdh;->j:F

    iget v3, p0, Lhdh;->k:F

    sub-float/2addr v0, v3

    iget v3, p0, Lhdh;->i:F

    mul-float/2addr v0, v3

    add-float/2addr v0, v1

    iget v1, p0, Lhdh;->e:F

    iget v3, p0, Lhdh;->g:F

    invoke-static {v0, v1, v3}, Lhcg;->a(FFF)F

    move-result v0

    iget v1, p0, Lhdh;->j:F

    sub-float v1, v0, v1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v3, 0x3dcccccd    # 0.1f

    cmpl-float v1, v1, v3

    if-ltz v1, :cond_3

    iget-object v1, p0, Lhdh;->b:Lhdi;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lhdh;->b:Lhdi;

    invoke-interface {v1, v0}, Lhdi;->a(F)V

    goto :goto_6
.end method

.method public g()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 112
    iget-object v0, p0, Lhdh;->b:Lhdi;

    if-eqz v0, :cond_0

    iget v0, p0, Lhdh;->h:I

    if-eq v0, v1, :cond_0

    .line 113
    iget-object v0, p0, Lhdh;->b:Lhdi;

    invoke-interface {v0}, Lhdi;->b()V

    .line 116
    :cond_0
    iput v1, p0, Lhdh;->h:I

    .line 118
    return v1
.end method
