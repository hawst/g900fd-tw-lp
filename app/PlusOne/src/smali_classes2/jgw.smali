.class public Ljgw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljhd;


# static fields
.field private static final a:Lloy;


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljgx;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljgy;

.field private d:I

.field private e:D

.field private f:D

.field private g:D

.field private h:J

.field private i:J

.field private j:J


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 61
    new-instance v0, Lloy;

    const-string v1, "visual_debug_network_req"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lloy;-><init>(Ljava/lang/String;Z)V

    sput-object v0, Ljgw;->a:Lloy;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    .line 65
    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ljgw;->b:Ljava/util/List;

    .line 70
    const/4 v0, 0x0

    iput v0, p0, Ljgw;->d:I

    .line 72
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljgw;->e:D

    .line 74
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Ljgw;->g:D

    .line 76
    iput-wide v2, p0, Ljgw;->h:J

    .line 77
    iput-wide v2, p0, Ljgw;->i:J

    .line 78
    iput-wide v2, p0, Ljgw;->j:J

    .line 84
    new-instance v0, Ljha;

    invoke-direct {v0, p1}, Ljha;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ljgw;->c:Ljgy;

    .line 86
    const/4 v0, 0x1

    iput v0, p0, Ljgw;->d:I

    .line 91
    return-void
.end method

.method private a(DD)V
    .locals 5

    .prologue
    .line 145
    iget-wide v0, p0, Ljgw;->f:D

    iget-wide v2, p0, Ljgw;->f:D

    sub-double v2, p1, v2

    mul-double/2addr v2, p3

    add-double/2addr v0, v2

    iput-wide v0, p0, Ljgw;->f:D

    .line 147
    return-void
.end method

.method private c()J
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 418
    iget-wide v2, p0, Ljgw;->h:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_0

    iget-wide v2, p0, Ljgw;->i:J

    cmp-long v2, v2, v0

    if-nez v2, :cond_1

    .line 421
    :cond_0
    :goto_0
    return-wide v0

    :cond_1
    iget-wide v0, p0, Ljgw;->i:J

    long-to-double v0, v0

    iget-wide v2, p0, Ljgw;->h:J

    long-to-double v2, v2

    const-wide v4, 0x408f400000000000L    # 1000.0

    div-double/2addr v2, v4

    div-double/2addr v0, v2

    double-to-long v0, v0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 261
    iget-wide v0, p0, Ljgw;->f:D

    double-to-int v0, v0

    return v0
.end method

.method public a(I[Ljava/lang/String;Ljava/lang/String;JJJJJILjava/lang/String;)V
    .locals 16

    .prologue
    .line 154
    new-instance v3, Ljgx;

    move/from16 v4, p1

    move-object/from16 v5, p2

    move-wide/from16 v6, p4

    move-wide/from16 v8, p6

    move-wide/from16 v10, p8

    move-wide/from16 v12, p10

    move-wide/from16 v14, p12

    invoke-direct/range {v3 .. v15}, Ljgx;-><init>(I[Ljava/lang/String;JJJJJ)V

    .line 156
    invoke-virtual {v3}, Ljgx;->b()Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v5, v0, Ljgw;->b:Ljava/util/List;

    monitor-enter v5

    const/4 v4, 0x0

    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Ljgw;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/ListIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_18

    invoke-interface {v6}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljgx;

    iget-wide v8, v2, Ljgx;->b:J

    iget-wide v10, v3, Ljgx;->d:J

    cmp-long v7, v8, v10

    if-gtz v7, :cond_18

    iget-wide v8, v3, Ljgx;->b:J

    iget-wide v10, v2, Ljgx;->b:J

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    iget-wide v10, v3, Ljgx;->d:J

    iget-wide v12, v2, Ljgx;->d:J

    invoke-static {v10, v11, v12, v13}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-gez v7, :cond_0

    iget-wide v8, v3, Ljgx;->b:J

    iget-wide v10, v2, Ljgx;->b:J

    cmp-long v4, v8, v10

    if-gez v4, :cond_1

    iget-wide v8, v2, Ljgx;->b:J

    iget-wide v10, v3, Ljgx;->b:J

    iput-wide v10, v2, Ljgx;->b:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Ljgw;->h:J

    iget-wide v12, v3, Ljgx;->b:J

    sub-long/2addr v8, v12

    add-long/2addr v8, v10

    move-object/from16 v0, p0

    iput-wide v8, v0, Ljgw;->h:J

    :cond_1
    iget-wide v8, v3, Ljgx;->d:J

    iget-wide v10, v2, Ljgx;->d:J

    cmp-long v4, v8, v10

    if-lez v4, :cond_2

    iget-wide v8, v2, Ljgx;->d:J

    iget-wide v10, v3, Ljgx;->d:J

    iput-wide v10, v2, Ljgx;->d:J

    move-object/from16 v0, p0

    iget-wide v10, v0, Ljgw;->h:J

    iget-wide v12, v3, Ljgx;->d:J

    sub-long v8, v12, v8

    add-long/2addr v8, v10

    move-object/from16 v0, p0

    iput-wide v8, v0, Ljgw;->h:J

    :cond_2
    iget-wide v8, v2, Ljgx;->e:J

    iget-wide v10, v3, Ljgx;->e:J

    add-long/2addr v8, v10

    iput-wide v8, v2, Ljgx;->e:J

    iget-wide v8, v2, Ljgx;->f:J

    iget-wide v10, v3, Ljgx;->f:J

    add-long/2addr v8, v10

    iput-wide v8, v2, Ljgx;->f:J

    const/4 v2, 0x1

    :goto_0
    if-nez v2, :cond_5

    :cond_3
    invoke-interface {v6}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v6}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljgx;

    iget-wide v8, v2, Ljgx;->b:J

    iget-wide v10, v3, Ljgx;->b:J

    cmp-long v2, v8, v10

    if-gez v2, :cond_3

    invoke-interface {v6}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    :cond_4
    invoke-interface {v6, v3}, Ljava/util/ListIterator;->add(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-wide v6, v0, Ljgw;->h:J

    iget-wide v8, v3, Ljgx;->d:J

    iget-wide v10, v3, Ljgx;->b:J

    sub-long/2addr v8, v10

    add-long/2addr v6, v8

    move-object/from16 v0, p0

    iput-wide v6, v0, Ljgw;->h:J

    :cond_5
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object/from16 v0, p0

    iget-wide v4, v0, Ljgw;->j:J

    iget-wide v6, v3, Ljgx;->e:J

    add-long/2addr v4, v6

    move-object/from16 v0, p0

    iput-wide v4, v0, Ljgw;->j:J

    move-object/from16 v0, p0

    iget-wide v4, v0, Ljgw;->i:J

    iget-wide v6, v3, Ljgx;->f:J

    add-long/2addr v4, v6

    move-object/from16 v0, p0

    iput-wide v4, v0, Ljgw;->i:J

    move-object/from16 v0, p0

    iget-object v5, v0, Ljgw;->b:Ljava/util/List;

    monitor-enter v5

    const/4 v4, 0x0

    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Ljgw;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 157
    :cond_6
    :goto_1
    const-string v2, "NetworkLogImpl"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string v4, "R: %s (%d) in %d - @ %d\n lat: %d : bytes: %d / %d."

    const/4 v2, 0x7

    new-array v5, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget v2, v3, Ljgx;->a:I

    packed-switch v2, :pswitch_data_0

    const-string v2, "unknown"

    :goto_2
    aput-object v2, v5, v6

    const/4 v2, 0x1

    iget-object v6, v3, Ljgx;->c:[Ljava/lang/String;

    array-length v6, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v2, 0x2

    invoke-virtual {v3}, Ljgx;->c()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v2, 0x3

    invoke-direct/range {p0 .. p0}, Ljgw;->c()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v2, 0x4

    invoke-virtual {v3}, Ljgx;->d()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v2, 0x5

    iget-wide v6, v3, Ljgx;->e:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v2

    const/4 v2, 0x6

    iget-wide v6, v3, Ljgx;->f:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    sget-object v2, Ljgw;->a:Lloy;

    .line 160
    :cond_7
    const-wide/16 v2, 0x2710

    cmp-long v2, p12, v2

    if-lez v2, :cond_8

    .line 161
    move-wide/from16 v0, p12

    long-to-double v2, v0

    sub-long v4, p8, p4

    long-to-double v4, v4

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    div-double/2addr v2, v4

    double-to-long v2, v2

    const-wide/32 v4, 0x7a120

    cmp-long v4, v2, v4

    if-gtz v4, :cond_8

    move-object/from16 v0, p0

    iget-wide v4, v0, Ljgw;->e:D

    const-wide/16 v6, 0x0

    cmpl-double v4, v4, v6

    if-nez v4, :cond_f

    long-to-double v2, v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Ljgw;->f:D

    move-object/from16 v0, p0

    iget-wide v2, v0, Ljgw;->e:D

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    add-double/2addr v2, v4

    move-object/from16 v0, p0

    iput-wide v2, v0, Ljgw;->e:D

    :goto_3
    move-object/from16 v0, p0

    iget-wide v2, v0, Ljgw;->f:D

    const-wide v4, 0x40ed4c0000000000L    # 60000.0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_14

    const/4 v2, 0x2

    :goto_4
    move-object/from16 v0, p0

    iget v3, v0, Ljgw;->d:I

    if-eq v2, v3, :cond_8

    move-object/from16 v0, p0

    iput v2, v0, Ljgw;->d:I

    .line 163
    :cond_8
    return-void

    .line 156
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v2

    :cond_9
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Ljgw;->b:Ljava/util/List;

    const/4 v6, 0x0

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljgx;

    if-eqz v2, :cond_17

    invoke-virtual {v2}, Ljgx;->a()Z

    move-result v2

    if-eqz v2, :cond_17

    const/4 v2, 0x1

    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Ljgw;->b:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const/16 v6, 0x19

    if-lt v4, v6, :cond_a

    const/4 v2, 0x1

    :cond_a
    if-eqz v2, :cond_e

    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    iput-wide v6, v0, Ljgw;->i:J

    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    iput-wide v6, v0, Ljgw;->j:J

    const-wide/16 v6, 0x0

    move-object/from16 v0, p0

    iput-wide v6, v0, Ljgw;->h:J

    move-object/from16 v0, p0

    iget-object v2, v0, Ljgw;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    const/4 v2, 0x0

    move v4, v2

    :goto_6
    if-ge v4, v6, :cond_c

    move-object/from16 v0, p0

    iget-object v2, v0, Ljgw;->b:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljgx;

    invoke-virtual {v2}, Ljgx;->a()Z

    move-result v7

    if-nez v7, :cond_b

    move-object/from16 v0, p0

    iget-wide v8, v0, Ljgw;->j:J

    iget-wide v10, v2, Ljgx;->e:J

    add-long/2addr v8, v10

    move-object/from16 v0, p0

    iput-wide v8, v0, Ljgw;->j:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Ljgw;->i:J

    iget-wide v10, v2, Ljgx;->f:J

    add-long/2addr v8, v10

    move-object/from16 v0, p0

    iput-wide v8, v0, Ljgw;->i:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Ljgw;->h:J

    iget-wide v10, v2, Ljgx;->d:J

    iget-wide v12, v2, Ljgx;->b:J

    sub-long/2addr v10, v12

    add-long/2addr v8, v10

    move-object/from16 v0, p0

    iput-wide v8, v0, Ljgw;->h:J

    :cond_b
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_6

    :cond_c
    invoke-direct/range {p0 .. p0}, Ljgw;->c()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v2, v6, v8

    if-lez v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Ljgw;->c:Ljgy;

    invoke-interface {v2, v6, v7}, Ljgy;->a(J)V

    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Ljgw;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    :cond_e
    monitor-exit v5

    goto/16 :goto_1

    :catchall_1
    move-exception v2

    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v2

    .line 157
    :pswitch_0
    const-string v2, "image download"

    goto/16 :goto_2

    :pswitch_1
    const-string v2, "image upload"

    goto/16 :goto_2

    :pswitch_2
    const-string v2, "remote api call"

    goto/16 :goto_2

    :pswitch_3
    const-string v2, "video download"

    goto/16 :goto_2

    :pswitch_4
    const-string v2, "video upload"

    goto/16 :goto_2

    .line 161
    :cond_f
    move-object/from16 v0, p0

    iget-wide v4, v0, Ljgw;->e:D

    const-wide/high16 v6, 0x4014000000000000L    # 5.0

    cmpg-double v4, v4, v6

    if-gtz v4, :cond_10

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    move-object/from16 v0, p0

    iget-wide v6, v0, Ljgw;->e:D

    div-double/2addr v4, v6

    move-object/from16 v0, p0

    iput-wide v4, v0, Ljgw;->g:D

    long-to-double v2, v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Ljgw;->g:D

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v5}, Ljgw;->a(DD)V

    move-object/from16 v0, p0

    iget-wide v2, v0, Ljgw;->e:D

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    add-double/2addr v2, v4

    move-object/from16 v0, p0

    iput-wide v2, v0, Ljgw;->e:D

    goto/16 :goto_3

    :cond_10
    move-object/from16 v0, p0

    iget-wide v4, v0, Ljgw;->f:D

    long-to-double v6, v2

    sub-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    const-wide v6, 0x40f86a0000000000L    # 100000.0

    cmpl-double v6, v4, v6

    if-lez v6, :cond_11

    long-to-double v2, v2

    const-wide v4, 0x3fe999999999999aL    # 0.8

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v5}, Ljgw;->a(DD)V

    goto/16 :goto_3

    :cond_11
    const-wide v6, 0x40c3880000000000L    # 10000.0

    cmpl-double v6, v4, v6

    if-lez v6, :cond_12

    long-to-double v2, v2

    const-wide v4, 0x3fd999999999999aL    # 0.4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v5}, Ljgw;->a(DD)V

    goto/16 :goto_3

    :cond_12
    const-wide v6, 0x408f400000000000L    # 1000.0

    cmpl-double v4, v4, v6

    if-lez v4, :cond_13

    long-to-double v2, v2

    const-wide v4, 0x3fd3333333333333L    # 0.3

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v5}, Ljgw;->a(DD)V

    goto/16 :goto_3

    :cond_13
    long-to-double v2, v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Ljgw;->g:D

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v5}, Ljgw;->a(DD)V

    goto/16 :goto_3

    :cond_14
    move-object/from16 v0, p0

    iget-wide v2, v0, Ljgw;->f:D

    const-wide v4, 0x40e3880000000000L    # 40000.0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_15

    const/4 v2, 0x1

    goto/16 :goto_4

    :cond_15
    move-object/from16 v0, p0

    iget-wide v2, v0, Ljgw;->f:D

    const-wide v4, 0x40d3880000000000L    # 20000.0

    cmpl-double v2, v2, v4

    if-lez v2, :cond_16

    const/4 v2, -0x1

    goto/16 :goto_4

    :cond_16
    const/4 v2, -0x2

    goto/16 :goto_4

    :cond_17
    move v2, v4

    goto/16 :goto_5

    :cond_18
    move v2, v4

    goto/16 :goto_0

    .line 157
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method public b()I
    .locals 1

    .prologue
    .line 450
    iget v0, p0, Ljgw;->d:I

    return v0
.end method
