.class public final Layh;
.super Layr;
.source "PG"


# instance fields
.field private a:Lbaf;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 31
    new-instance v0, Lays;

    invoke-direct {v0}, Lays;-><init>()V

    const v1, 0x7f020422

    .line 32
    invoke-virtual {v0, v1}, Lays;->a(I)Lays;

    move-result-object v0

    const v1, 0x7f020112

    .line 33
    invoke-virtual {v0, v1}, Lays;->b(I)Lays;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 34
    invoke-virtual {v0, v1}, Lays;->a(F)Lays;

    move-result-object v0

    const/4 v1, 0x1

    .line 35
    invoke-virtual {v0, v1}, Lays;->c(I)Lays;

    move-result-object v0

    .line 31
    invoke-direct {p0, v0}, Layr;-><init>(Lays;)V

    .line 27
    new-instance v0, Lbap;

    const-wide/32 v2, 0x493e0

    invoke-direct {v0, v2, v3}, Lbap;-><init>(J)V

    iput-object v0, p0, Layh;->a:Lbaf;

    .line 37
    return-void
.end method


# virtual methods
.method protected a(Layj;Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 57
    new-instance v0, Layi;

    new-instance v1, Layu;

    invoke-direct {v1}, Layu;-><init>()V

    .line 58
    invoke-virtual {v1, p1}, Layu;->a(Layj;)Layu;

    move-result-object v1

    const v2, 0x7f0b009e

    .line 59
    invoke-virtual {p3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Layu;->a(I)Layu;

    move-result-object v1

    const/high16 v2, 0x42100000    # 36.0f

    .line 60
    invoke-virtual {v1, v2}, Layu;->a(F)Layu;

    move-result-object v1

    const/high16 v2, 0x42300000    # 44.0f

    .line 61
    invoke-virtual {v1, v2}, Layu;->b(F)Layu;

    move-result-object v1

    const-string v2, "sans-serif-condensed"

    .line 62
    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Layu;->a(Landroid/graphics/Typeface;)Layu;

    move-result-object v1

    .line 63
    invoke-virtual {v1, v3}, Layu;->a(Z)Layu;

    move-result-object v1

    const/16 v2, 0x64

    .line 64
    invoke-virtual {v1, v2}, Layu;->c(I)Layu;

    move-result-object v1

    const v2, 0x7f0b009f

    .line 65
    invoke-virtual {p3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Layu;->h(I)Layu;

    move-result-object v1

    const/4 v2, 0x0

    .line 66
    invoke-virtual {v1, v2}, Layu;->b(Z)Layu;

    move-result-object v1

    invoke-direct {v0, v1}, Layi;-><init>(Layu;)V

    return-object v0
.end method

.method public b(Lbmd;)Lool;
    .locals 9

    .prologue
    .line 42
    iget-object v0, p1, Lbmd;->d:Lbmg;

    sget-object v1, Lbmg;->c:Lbmg;

    if-eq v0, v1, :cond_0

    .line 43
    const/4 v0, 0x0

    .line 45
    :goto_0
    return-object v0

    :cond_0
    const-wide v0, 0x3ff0cccccccccccdL    # 1.05

    const-wide v2, 0x3f9eb851eb851eb8L    # 0.03

    const-wide v4, 0x3fc999999999999aL    # 0.2

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    move-object v8, p1

    invoke-static/range {v0 .. v8}, Lbah;->a(DDDDLbmd;)Lool;

    move-result-object v0

    goto :goto_0
.end method

.method protected f()Layj;
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lazc;

    invoke-direct {v0, p0}, Lazc;-><init>(Layj;)V

    return-object v0
.end method

.method public v(Layl;)F
    .locals 2

    .prologue
    .line 71
    iget-boolean v0, p1, Layl;->w:Z

    if-nez v0, :cond_0

    .line 72
    iget-object v0, p0, Layh;->a:Lbaf;

    invoke-virtual {v0, p1}, Lbaf;->b(Layl;)F

    move-result v0

    .line 73
    const/high16 v1, 0x3f000000    # 0.5f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    .line 74
    const/high16 v0, -0x40800000    # -1.0f

    .line 77
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Layr;->v(Layl;)F

    move-result v0

    goto :goto_0
.end method
