.class public final Ladb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lacr;


# instance fields
.field private synthetic a:Landroidx/media/filterfw/ViewFilter;


# direct methods
.method public constructor <init>(Landroidx/media/filterfw/ViewFilter;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Ladb;->a:Landroidx/media/filterfw/ViewFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lacp;Laap;)V
    .locals 4

    .prologue
    .line 143
    invoke-virtual {p2}, Laap;->a()Labg;

    move-result-object v0

    invoke-virtual {v0}, Labg;->j()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 144
    iget-object v1, p0, Ladb;->a:Landroidx/media/filterfw/ViewFilter;

    invoke-static {v1}, Landroidx/media/filterfw/ViewFilter;->a(Landroidx/media/filterfw/ViewFilter;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 145
    iget-object v1, p0, Ladb;->a:Landroidx/media/filterfw/ViewFilter;

    invoke-static {v1, v0}, Landroidx/media/filterfw/ViewFilter;->a(Landroidx/media/filterfw/ViewFilter;Ljava/lang/String;)Ljava/lang/String;

    .line 146
    const-string v1, "stretch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 147
    iget-object v0, p0, Ladb;->a:Landroidx/media/filterfw/ViewFilter;

    const/4 v1, 0x1

    iput v1, v0, Landroidx/media/filterfw/ViewFilter;->mScaleMode:I

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    const-string v1, "fit"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 149
    iget-object v0, p0, Ladb;->a:Landroidx/media/filterfw/ViewFilter;

    const/4 v1, 0x2

    iput v1, v0, Landroidx/media/filterfw/ViewFilter;->mScaleMode:I

    goto :goto_0

    .line 150
    :cond_2
    const-string v1, "fill"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 151
    iget-object v0, p0, Ladb;->a:Landroidx/media/filterfw/ViewFilter;

    const/4 v1, 0x3

    iput v1, v0, Landroidx/media/filterfw/ViewFilter;->mScaleMode:I

    goto :goto_0

    .line 153
    :cond_3
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown scale-mode \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
