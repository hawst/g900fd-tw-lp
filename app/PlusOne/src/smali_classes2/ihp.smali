.class public abstract Lihp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Liii;


# instance fields
.field public final a:Lgfz;

.field public final b:Lgeh;


# direct methods
.method public constructor <init>(Lihr;)V
    .locals 4

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    check-cast p1, Lihw;

    invoke-virtual {p1}, Lihw;->e()Lgfz;

    move-result-object v0

    iput-object v0, p0, Lihp;->a:Lgfz;

    .line 27
    new-instance v0, Lgeh;

    invoke-direct {v0}, Lgeh;-><init>()V

    iput-object v0, p0, Lihp;->b:Lgeh;

    .line 30
    :try_start_0
    sget-object v0, Lgdk;->c:Lgdn;

    iget-object v1, p0, Lihp;->a:Lgfz;

    iget-object v2, p0, Lihp;->b:Lgeh;

    invoke-virtual {v2}, Lgeh;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lihp;->b:Lgeh;

    invoke-interface {v0, v1, v2, v3}, Lgdn;->a(Lgfz;Ljava/lang/String;Lgdu;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    :goto_0
    return-void

    .line 32
    :catch_0
    move-exception v0

    .line 33
    const-string v1, "BaseGcoreRemoteMediaPlayerImpl"

    const-string v2, "Error registering with message received callback"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public a(J)V
    .locals 7

    .prologue
    .line 61
    iget-object v0, p0, Lihp;->b:Lgeh;

    iget-object v1, p0, Lihp;->a:Lgfz;

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-wide v2, p1

    invoke-virtual/range {v0 .. v5}, Lgeh;->a(Lgfz;JILorg/json/JSONObject;)Lggd;

    .line 62
    return-void
.end method

.method public a(Liij;)V
    .locals 2

    .prologue
    .line 39
    iget-object v0, p0, Lihp;->b:Lgeh;

    new-instance v1, Lihq;

    invoke-direct {v1, p1}, Lihq;-><init>(Liij;)V

    invoke-virtual {v0, v1}, Lgeh;->a(Lgeo;)V

    .line 45
    return-void
.end method

.method public a(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    .line 51
    new-instance v0, Lgec;

    const-string v1, "none"

    invoke-direct {v0, v1}, Lgec;-><init>(Ljava/lang/String;)V

    .line 52
    invoke-virtual {v0, p1}, Lgec;->a(Ljava/lang/String;)Lgec;

    move-result-object v0

    .line 53
    invoke-virtual {v0, v3}, Lgec;->a(I)Lgec;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Lgec;->a()Lgeb;

    move-result-object v2

    .line 55
    iget-object v0, p0, Lihp;->b:Lgeh;

    iget-object v1, p0, Lihp;->a:Lgfz;

    const-wide/16 v4, 0x0

    move-object v6, p2

    invoke-virtual/range {v0 .. v6}, Lgeh;->a(Lgfz;Lgeb;ZJLorg/json/JSONObject;)Lggd;

    .line 57
    return-void
.end method

.method public a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 66
    iget-object v1, p0, Lihp;->b:Lgeh;

    invoke-virtual {v1}, Lgeh;->c()Lgef;

    move-result-object v1

    .line 67
    invoke-virtual {v1}, Lgef;->b()I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 68
    invoke-virtual {v1}, Lgef;->c()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lihp;->b:Lgeh;

    invoke-virtual {v0}, Lgeh;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lihp;->b:Lgeh;

    invoke-virtual {v0}, Lgeh;->b()J

    move-result-wide v0

    return-wide v0
.end method
