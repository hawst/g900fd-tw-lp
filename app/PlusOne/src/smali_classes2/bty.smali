.class public final Lbty;
.super Loxu;
.source "PG"


# static fields
.field public static final a:[Lbty;


# instance fields
.field public b:I

.field public c:I

.field public d:I

.field public e:Ljava/lang/String;

.field public f:Lbug;

.field public g:Z

.field public h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    new-array v0, v0, [Lbty;

    sput-object v0, Lbty;->a:[Lbty;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 9
    invoke-direct {p0}, Loxu;-><init>()V

    .line 20
    iput v0, p0, Lbty;->b:I

    .line 23
    iput v0, p0, Lbty;->c:I

    .line 26
    iput v1, p0, Lbty;->d:I

    .line 29
    const-string v0, ""

    iput-object v0, p0, Lbty;->e:Ljava/lang/String;

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Lbty;->f:Lbug;

    .line 35
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbty;->g:Z

    .line 38
    iput-boolean v1, p0, Lbty;->h:Z

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    .line 80
    const/4 v0, 0x0

    .line 81
    iget v1, p0, Lbty;->b:I

    if-eq v1, v2, :cond_0

    .line 82
    iget v0, p0, Lbty;->b:I

    .line 83
    invoke-static {v3, v0}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 85
    :cond_0
    iget v1, p0, Lbty;->c:I

    if-eq v1, v2, :cond_1

    .line 86
    const/4 v1, 0x2

    iget v2, p0, Lbty;->c:I

    .line 87
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    :cond_1
    iget v1, p0, Lbty;->d:I

    if-eqz v1, :cond_2

    .line 90
    const/4 v1, 0x3

    iget v2, p0, Lbty;->d:I

    .line 91
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    :cond_2
    iget-object v1, p0, Lbty;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 94
    const/4 v1, 0x4

    iget-object v2, p0, Lbty;->e:Ljava/lang/String;

    .line 95
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    :cond_3
    iget-object v1, p0, Lbty;->f:Lbug;

    if-eqz v1, :cond_4

    .line 98
    const/4 v1, 0x5

    iget-object v2, p0, Lbty;->f:Lbug;

    .line 99
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    :cond_4
    iget-boolean v1, p0, Lbty;->g:Z

    if-eq v1, v3, :cond_5

    .line 102
    const/4 v1, 0x6

    iget-boolean v2, p0, Lbty;->g:Z

    .line 103
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 105
    :cond_5
    iget-boolean v1, p0, Lbty;->h:Z

    if-eqz v1, :cond_6

    .line 106
    const/4 v1, 0x7

    iget-boolean v2, p0, Lbty;->h:Z

    .line 107
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 109
    :cond_6
    iput v0, p0, Lbty;->ai:I

    .line 110
    return v0
.end method

.method public a(Loxn;)Lbty;
    .locals 2

    .prologue
    .line 118
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 119
    sparse-switch v0, :sswitch_data_0

    .line 123
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    :sswitch_0
    return-object p0

    .line 129
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    iput v0, p0, Lbty;->b:I

    goto :goto_0

    .line 133
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    iput v0, p0, Lbty;->c:I

    goto :goto_0

    .line 137
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 138
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 143
    :cond_1
    iput v0, p0, Lbty;->d:I

    goto :goto_0

    .line 145
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lbty;->d:I

    goto :goto_0

    .line 150
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbty;->e:Ljava/lang/String;

    goto :goto_0

    .line 154
    :sswitch_5
    iget-object v0, p0, Lbty;->f:Lbug;

    if-nez v0, :cond_3

    .line 155
    new-instance v0, Lbug;

    invoke-direct {v0}, Lbug;-><init>()V

    iput-object v0, p0, Lbty;->f:Lbug;

    .line 157
    :cond_3
    iget-object v0, p0, Lbty;->f:Lbug;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 161
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    iput-boolean v0, p0, Lbty;->g:Z

    goto :goto_0

    .line 165
    :sswitch_7
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    iput-boolean v0, p0, Lbty;->h:Z

    goto :goto_0

    .line 119
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 55
    iget v0, p0, Lbty;->b:I

    if-eq v0, v1, :cond_0

    .line 56
    iget v0, p0, Lbty;->b:I

    invoke-virtual {p1, v2, v0}, Loxo;->a(II)V

    .line 58
    :cond_0
    iget v0, p0, Lbty;->c:I

    if-eq v0, v1, :cond_1

    .line 59
    const/4 v0, 0x2

    iget v1, p0, Lbty;->c:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 61
    :cond_1
    iget v0, p0, Lbty;->d:I

    if-eqz v0, :cond_2

    .line 62
    const/4 v0, 0x3

    iget v1, p0, Lbty;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 64
    :cond_2
    iget-object v0, p0, Lbty;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 65
    const/4 v0, 0x4

    iget-object v1, p0, Lbty;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 67
    :cond_3
    iget-object v0, p0, Lbty;->f:Lbug;

    if-eqz v0, :cond_4

    .line 68
    const/4 v0, 0x5

    iget-object v1, p0, Lbty;->f:Lbug;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 70
    :cond_4
    iget-boolean v0, p0, Lbty;->g:Z

    if-eq v0, v2, :cond_5

    .line 71
    const/4 v0, 0x6

    iget-boolean v1, p0, Lbty;->g:Z

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 73
    :cond_5
    iget-boolean v0, p0, Lbty;->h:Z

    if-eqz v0, :cond_6

    .line 74
    const/4 v0, 0x7

    iget-boolean v1, p0, Lbty;->h:Z

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 76
    :cond_6
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lbty;->a(Loxn;)Lbty;

    move-result-object v0

    return-object v0
.end method
