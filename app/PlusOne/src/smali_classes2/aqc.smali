.class public Laqc;
.super Lamn;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private A:Z

.field private B:Z

.field private final C:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private D:Laql;

.field private E:Laqu;

.field private F:Z

.field private G:Landroid/graphics/Bitmap;

.field private H:Z

.field private I:Z

.field private J:J

.field private K:Z

.field private L:Z

.field private M:I

.field private N:I

.field private final O:Laqw;

.field private final P:Lawk;

.field private final Q:Lawk;

.field private final R:Lawk;

.field private final S:Lawk;

.field private final T:Lawk;

.field private final U:Lawk;

.field private final V:Lawk;

.field private final W:Lawk;

.field private final X:Lcef;

.field private final b:Landroid/content/Context;

.field private final c:Lawf;

.field private final d:Lbgf;

.field private final e:Lbtp;

.field private final f:Lasy;

.field private final g:Lalg;

.field private final h:Lbzb;

.field private final j:Laqx;

.field private final k:Lanh;

.field private final l:Lcdu;

.field private final m:Laqm;

.field private final n:Laqb;

.field private final o:Laxp;

.field private final p:Lced;

.field private final q:Laqt;

.field private final r:Lavj;

.field private final s:Lbjf;

.field private final t:Lasn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lasn",
            "<",
            "Laqv;",
            ">;"
        }
    .end annotation
.end field

.field private u:Z

.field private v:J

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    const-class v0, Laqc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Laqc;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lawf;Lbgf;Lbtp;Lasy;Lalg;Lbzb;Laqx;Lanh;Lcdu;Laqm;Laqb;Laxp;Lced;Laqt;Lavj;)V
    .locals 3

    .prologue
    .line 598
    invoke-direct {p0}, Lamn;-><init>()V

    .line 320
    new-instance v1, Laqd;

    const-class v2, Laqv;

    invoke-direct {v1, p0, v2}, Laqd;-><init>(Laqc;Ljava/lang/Class;)V

    iput-object v1, p0, Laqc;->t:Lasn;

    .line 338
    const/4 v1, 0x0

    iput-boolean v1, p0, Laqc;->w:Z

    .line 359
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    iput-object v1, p0, Laqc;->C:Ljava/util/Queue;

    .line 392
    const/4 v1, 0x1

    iput v1, p0, Laqc;->M:I

    .line 394
    new-instance v1, Laqe;

    invoke-direct {v1, p0}, Laqe;-><init>(Laqc;)V

    iput-object v1, p0, Laqc;->O:Laqw;

    .line 534
    new-instance v1, Laqs;

    invoke-direct {v1, p0}, Laqs;-><init>(Laqc;)V

    iput-object v1, p0, Laqc;->P:Lawk;

    .line 535
    new-instance v1, Laqr;

    invoke-direct {v1, p0}, Laqr;-><init>(Laqc;)V

    iput-object v1, p0, Laqc;->Q:Lawk;

    .line 536
    new-instance v1, Laqq;

    invoke-direct {v1, p0}, Laqq;-><init>(Laqc;)V

    iput-object v1, p0, Laqc;->R:Lawk;

    .line 537
    new-instance v1, Laqn;

    invoke-direct {v1, p0}, Laqn;-><init>(Laqc;)V

    iput-object v1, p0, Laqc;->S:Lawk;

    .line 539
    new-instance v1, Laqp;

    invoke-direct {v1, p0}, Laqp;-><init>(Laqc;)V

    iput-object v1, p0, Laqc;->T:Lawk;

    .line 540
    new-instance v1, Laqf;

    invoke-direct {v1, p0}, Laqf;-><init>(Laqc;)V

    iput-object v1, p0, Laqc;->U:Lawk;

    .line 547
    new-instance v1, Laqg;

    invoke-direct {v1, p0}, Laqg;-><init>(Laqc;)V

    iput-object v1, p0, Laqc;->V:Lawk;

    .line 553
    new-instance v1, Laqh;

    invoke-direct {v1, p0}, Laqh;-><init>(Laqc;)V

    iput-object v1, p0, Laqc;->W:Lawk;

    .line 564
    new-instance v1, Laqo;

    invoke-direct {v1, p0}, Laqo;-><init>(Laqc;)V

    iput-object v1, p0, Laqc;->X:Lcef;

    .line 599
    const-string v1, "context"

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Laqc;->b:Landroid/content/Context;

    .line 600
    const-string v1, "playerScreenState"

    const/4 v2, 0x0

    invoke-static {p2, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lawf;

    iput-object v1, p0, Laqc;->c:Lawf;

    .line 601
    const-string v1, "renderContext"

    const/4 v2, 0x0

    invoke-static {p3, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbgf;

    iput-object v1, p0, Laqc;->d:Lbgf;

    .line 602
    const-string v1, "storyboardPlayer"

    const/4 v2, 0x0

    invoke-static {p4, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbtp;

    iput-object v1, p0, Laqc;->e:Lbtp;

    .line 603
    const-string v1, "stateTracker"

    const/4 v2, 0x0

    invoke-static {p5, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lasy;

    iput-object v1, p0, Laqc;->f:Lasy;

    .line 604
    const-string v1, "serialAsyncTaskRunner"

    .line 605
    const/4 v2, 0x0

    invoke-static {p6, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lalg;

    iput-object v1, p0, Laqc;->g:Lalg;

    .line 606
    const-string v1, "themesLibrary"

    const/4 v2, 0x0

    invoke-static {p7, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbzb;

    iput-object v1, p0, Laqc;->h:Lbzb;

    .line 607
    const-string v1, "prefsSettings"

    const/4 v2, 0x0

    invoke-static {p8, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laqx;

    iput-object v1, p0, Laqc;->j:Laqx;

    .line 608
    const-string v1, "gservicesSettings"

    const/4 v2, 0x0

    invoke-static {p9, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lanh;

    iput-object v1, p0, Laqc;->k:Lanh;

    .line 609
    const-string v1, "analyticsSession"

    const/4 v2, 0x0

    invoke-static {p10, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcdu;

    iput-object v1, p0, Laqc;->l:Lcdu;

    .line 610
    const-string v1, "listener"

    const/4 v2, 0x0

    invoke-static {p11, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laqm;

    iput-object v1, p0, Laqc;->m:Laqm;

    .line 611
    const-string v1, "onInvalidUriDetectedListener"

    const/4 v2, 0x0

    invoke-static {p12, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laqb;

    iput-object v1, p0, Laqc;->n:Laqb;

    .line 613
    const-string v1, "userHintDecider"

    const/4 v2, 0x0

    move-object/from16 v0, p13

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laxp;

    iput-object v1, p0, Laqc;->o:Laxp;

    .line 614
    const-string v1, "audioFocusHelper"

    const/4 v2, 0x0

    move-object/from16 v0, p14

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lced;

    iput-object v1, p0, Laqc;->p:Lced;

    .line 615
    const-string v1, "display"

    const/4 v2, 0x0

    move-object/from16 v0, p15

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laqt;

    iput-object v1, p0, Laqc;->q:Laqt;

    .line 616
    const-string v1, "soundtrackUsageRecorder"

    const/4 v2, 0x0

    move-object/from16 v0, p16

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lavj;

    iput-object v1, p0, Laqc;->r:Lavj;

    .line 619
    new-instance v1, Lbjf;

    invoke-direct {v1}, Lbjf;-><init>()V

    iput-object v1, p0, Laqc;->s:Lbjf;

    .line 620
    iget-object v1, p0, Laqc;->e:Lbtp;

    new-instance v2, Laqi;

    invoke-direct {v2, p0}, Laqi;-><init>(Laqc;)V

    invoke-interface {v1, v2}, Lbtp;->a(Lbtq;)V

    .line 803
    return-void
.end method

.method static synthetic A(Laqc;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Laqc;->p()V

    return-void
.end method

.method static synthetic B(Laqc;)Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Laqc;->K:Z

    return v0
.end method

.method static synthetic C(Laqc;)Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Laqc;->u:Z

    return v0
.end method

.method static synthetic D(Laqc;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Laqc;->q()V

    return-void
.end method

.method static synthetic E(Laqc;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Laqc;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic F(Laqc;)Lbjf;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Laqc;->s:Lbjf;

    return-object v0
.end method

.method static synthetic G(Laqc;)Lcef;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Laqc;->X:Lcef;

    return-object v0
.end method

.method static synthetic H(Laqc;)Lced;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Laqc;->p:Lced;

    return-object v0
.end method

.method static synthetic a(Laqc;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Laqc;->G:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic a(Laqc;Laql;)Laql;
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Laqc;->D:Laql;

    return-object p1
.end method

.method static synthetic a(Laqc;Laqu;)Laqu;
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Laqc;->E:Laqu;

    return-object p1
.end method

.method static synthetic a(Laqc;)Laxp;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Laqc;->o:Laxp;

    return-object v0
.end method

.method private a(J)V
    .locals 5

    .prologue
    .line 1162
    invoke-virtual {p0}, Laqc;->e()V

    .line 1163
    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1164
    long-to-float v0, p1

    iget-object v1, p0, Laqc;->c:Lawf;

    .line 1165
    invoke-interface {v1}, Lawf;->F()Lboi;

    move-result-object v1

    invoke-virtual {v1}, Lboi;->n()J

    move-result-wide v2

    long-to-float v1, v2

    div-float v1, v0, v1

    .line 1166
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    iget-object v2, p0, Laqc;->c:Lawf;

    invoke-interface {v2}, Lawf;->W()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Laqv;->a(FJ)V

    .line 1168
    :cond_0
    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0, p1, p2}, Lawf;->b(J)V

    .line 1169
    return-void
.end method

.method static synthetic a(Laqc;J)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Laqc;->b(J)V

    return-void
.end method

.method static synthetic a(Laqc;Laqv;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 61
    invoke-virtual {p0}, Laqc;->e()V

    iget-object v0, p0, Laqc;->O:Laqw;

    invoke-interface {p1, v0}, Laqv;->a(Laqw;)V

    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->A()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Laqv;->a(Ljava/lang/String;)V

    invoke-direct {p0, p1}, Laqc;->c(Laqv;)V

    iget-object v0, p0, Laqc;->k:Lanh;

    invoke-virtual {v0}, Lanh;->w()I

    move-result v0

    invoke-interface {p1, v0}, Laqv;->c_(I)V

    invoke-interface {p1}, Laqv;->a()V

    invoke-interface {p1, v2}, Laqv;->c_(Z)V

    invoke-interface {p1, v2}, Laqv;->b(Z)V

    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->L()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Laqc;->b(J)V

    invoke-interface {p1, v2}, Laqv;->c(Z)V

    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->p()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-interface {p1, v0, v2}, Laqv;->a(Landroid/graphics/Bitmap;I)V

    invoke-interface {p1, v1}, Laqv;->d_(Z)V

    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->aZ()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Laqv;->r_()V

    :cond_0
    invoke-interface {p1, v2}, Laqv;->j(Z)V

    iget-object v0, p0, Laqc;->k:Lanh;

    invoke-virtual {v0}, Lanh;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    packed-switch v0, :pswitch_data_0

    const v0, 0xe1000

    :goto_1
    invoke-interface {p1, v0}, Laqv;->c(I)V

    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->G()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    invoke-interface {p1, v0}, Laqv;->b_(Z)V

    iget-boolean v0, p0, Laqc;->L:Z

    invoke-interface {p1, v0}, Laqv;->m(Z)V

    sget-object v0, Laqj;->a:[I

    iget v3, p0, Laqc;->M:I

    add-int/lit8 v3, v3, -0x1

    aget v0, v0, v3

    packed-switch v0, :pswitch_data_1

    :goto_3
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :pswitch_0
    const v0, 0x38400

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :pswitch_1
    invoke-interface {p1, v2}, Laqv;->i(Z)V

    invoke-interface {p1, v2}, Laqv;->a(Z)V

    invoke-interface {p1, v2}, Laqv;->a_(Z)V

    invoke-interface {p1, v2}, Laqv;->l(Z)V

    goto :goto_3

    :pswitch_2
    invoke-interface {p1, v1}, Laqv;->i(Z)V

    invoke-interface {p1, v2}, Laqv;->a(Z)V

    invoke-interface {p1, v1}, Laqv;->a_(Z)V

    invoke-interface {p1, v2}, Laqv;->l(Z)V

    goto :goto_3

    :pswitch_3
    invoke-interface {p1, v1}, Laqv;->i(Z)V

    invoke-direct {p0}, Laqc;->q()V

    invoke-direct {p0}, Laqc;->r()V

    iget-boolean v0, p0, Laqc;->A:Z

    if-nez v0, :cond_3

    move v2, v1

    :cond_3
    invoke-interface {p1, v2}, Laqv;->a_(Z)V

    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->E()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->F()Lboi;

    move-result-object v0

    invoke-virtual {v0}, Lboi;->o()I

    move-result v2

    invoke-virtual {v0}, Lboi;->p()I

    move-result v0

    invoke-interface {p1, v2, v0}, Laqv;->a(II)V

    invoke-direct {p0}, Laqc;->t()V

    :cond_4
    invoke-interface {p1, v1}, Laqv;->l(Z)V

    goto :goto_3

    :pswitch_4
    iget v0, p0, Laqc;->N:I

    invoke-interface {p1, v0}, Laqv;->d(I)V

    goto :goto_3

    :pswitch_5
    invoke-interface {p1}, Laqv;->p_()V

    goto :goto_3

    :pswitch_6
    invoke-interface {p1}, Laqv;->e()V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method static synthetic a(Laqc;Z)Z
    .locals 0

    .prologue
    .line 61
    iput-boolean p1, p0, Laqc;->u:Z

    return p1
.end method

.method static synthetic b(Laqc;J)J
    .locals 1

    .prologue
    .line 61
    iput-wide p1, p0, Laqc;->v:J

    return-wide p1
.end method

.method static synthetic b(Laqc;)Lawf;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Laqc;->c:Lawf;

    return-object v0
.end method

.method private b(J)V
    .locals 1

    .prologue
    .line 1175
    invoke-virtual {p0}, Laqc;->e()V

    .line 1176
    iget-object v0, p0, Laqc;->e:Lbtp;

    invoke-interface {v0}, Lbtp;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1177
    iget-object v0, p0, Laqc;->e:Lbtp;

    invoke-interface {v0, p1, p2}, Lbtp;->a(J)V

    .line 1181
    :goto_0
    invoke-direct {p0, p1, p2}, Laqc;->a(J)V

    .line 1182
    return-void

    .line 1179
    :cond_0
    iput-wide p1, p0, Laqc;->v:J

    goto :goto_0
.end method

.method static synthetic b(Laqc;Laqv;)V
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Laqc;->e()V

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Laqv;->a(Laqw;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Laqc;->u:Z

    iget-object v0, p0, Laqc;->d:Lbgf;

    invoke-virtual {v0}, Lbgf;->a()V

    return-void
.end method

.method static synthetic b(Laqc;Z)Z
    .locals 0

    .prologue
    .line 61
    iput-boolean p1, p0, Laqc;->w:Z

    return p1
.end method

.method static synthetic c(Laqc;)Lcdu;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Laqc;->l:Lcdu;

    return-object v0
.end method

.method static synthetic c(Laqc;J)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Laqc;->a(J)V

    return-void
.end method

.method static synthetic c(Laqc;Laqv;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Laqc;->c(Laqv;)V

    return-void
.end method

.method private c(Laqv;)V
    .locals 1

    .prologue
    .line 1317
    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->o()Lbza;

    move-result-object v0

    iget-boolean v0, v0, Lbza;->v:Z

    if-nez v0, :cond_0

    sget-object v0, Lcgd;->a:Lcgd;

    invoke-virtual {v0}, Lcgd;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1318
    :cond_0
    iget-object v0, p0, Laqc;->h:Lbzb;

    invoke-virtual {v0}, Lbzb;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {p1, v0}, Laqv;->a(Ljava/util/List;)V

    .line 1319
    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->o()Lbza;

    move-result-object v0

    invoke-interface {p1, v0}, Laqv;->a(Lbza;)V

    .line 1323
    :goto_0
    return-void

    .line 1321
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1, v0}, Laqv;->a(Ljava/util/List;)V

    goto :goto_0
.end method

.method static synthetic c(Laqc;Z)Z
    .locals 0

    .prologue
    .line 61
    iput-boolean p1, p0, Laqc;->H:Z

    return p1
.end method

.method static synthetic d(Laqc;J)J
    .locals 1

    .prologue
    .line 61
    iput-wide p1, p0, Laqc;->J:J

    return-wide p1
.end method

.method static synthetic d(Laqc;)Laqx;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Laqc;->j:Laqx;

    return-object v0
.end method

.method static synthetic d(Laqc;Z)Z
    .locals 0

    .prologue
    .line 61
    iput-boolean p1, p0, Laqc;->y:Z

    return p1
.end method

.method static synthetic e(Laqc;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Laqc;->v()V

    return-void
.end method

.method static synthetic e(Laqc;Z)Z
    .locals 0

    .prologue
    .line 61
    iput-boolean p1, p0, Laqc;->I:Z

    return p1
.end method

.method static synthetic f(Laqc;)Lasn;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Laqc;->t:Lasn;

    return-object v0
.end method

.method static synthetic f(Laqc;Z)Z
    .locals 0

    .prologue
    .line 61
    iput-boolean p1, p0, Laqc;->K:Z

    return p1
.end method

.method static synthetic g(Laqc;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 61
    invoke-virtual {p0}, Laqc;->e()V

    invoke-direct {p0}, Laqc;->v()V

    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->w()Lboh;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqc;->r:Lavj;

    iget-object v1, p0, Laqc;->c:Lawf;

    invoke-interface {v1}, Lawf;->w()Lboh;

    move-result-object v1

    invoke-virtual {v1}, Lboh;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lavj;->a(J)V

    :cond_0
    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->E()Z

    move-result v0

    const-string v1, "storyboard should always exist"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    iget-boolean v1, p0, Laqc;->u:Z

    if-eqz v1, :cond_2

    iput-boolean v4, p0, Laqc;->u:Z

    invoke-interface {v0, v4}, Laqv;->b(Z)V

    invoke-interface {v0, v5}, Laqv;->d_(Z)V

    invoke-interface {v0, v4}, Laqv;->c_(Z)V

    invoke-interface {v0, v4}, Laqv;->c(Z)V

    invoke-direct {p0}, Laqc;->r()V

    :goto_0
    invoke-interface {v0, v4, v4}, Laqv;->a(ZI)V

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Laqc;->e:Lbtp;

    invoke-interface {v1}, Lbtp;->f()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Laqc;->e:Lbtp;

    invoke-interface {v1}, Lbtp;->b()V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Laqc;->c:Lawf;

    invoke-interface {v1}, Lawf;->ad()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-interface {v0, v5}, Laqv;->b(Z)V

    invoke-interface {v0, v5}, Laqv;->c(Z)V

    iput-boolean v5, p0, Laqc;->u:Z

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Laqc;->r()V

    invoke-interface {v0, v5}, Laqv;->d_(Z)V

    invoke-direct {p0}, Laqc;->s()V

    goto :goto_0
.end method

.method static synthetic g(Laqc;Z)Z
    .locals 0

    .prologue
    .line 61
    iput-boolean p1, p0, Laqc;->B:Z

    return p1
.end method

.method static synthetic h(Laqc;)Lbtp;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Laqc;->e:Lbtp;

    return-object v0
.end method

.method static synthetic i(Laqc;)Laqm;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Laqc;->m:Laqm;

    return-object v0
.end method

.method static synthetic j(Laqc;)Ljava/util/Queue;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Laqc;->C:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic k(Laqc;)Laql;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Laqc;->D:Laql;

    return-object v0
.end method

.method static synthetic l(Laqc;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Laqc;->G:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic m(Laqc;)V
    .locals 4

    .prologue
    .line 61
    invoke-virtual {p0}, Laqc;->e()V

    iget-object v0, p0, Laqc;->l:Lcdu;

    invoke-virtual {v0}, Lcdu;->g()V

    iget-object v0, p0, Laqc;->o:Laxp;

    sget-object v1, Laxq;->g:Laxq;

    invoke-virtual {v0, v1}, Laxp;->a(Laxq;)V

    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Laqc;->h()V

    invoke-virtual {p0}, Laqc;->j()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p0}, Laqc;->k()Lbmd;

    move-result-object v1

    invoke-virtual {p0}, Laqc;->l()Landroid/graphics/RectF;

    move-result-object v2

    iget-object v3, p0, Laqc;->q:Laqt;

    invoke-interface {v3, v0, v1, v2}, Laqt;->a(Landroid/graphics/Rect;Lbmd;Landroid/graphics/RectF;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Laqc;->a:Ljava/lang/String;

    const-string v1, "Can\'t edit storyboard if the storyboard was null or not ready."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic n(Laqc;)V
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Laqc;->e()V

    invoke-virtual {p0}, Laqc;->h()V

    iget-object v0, p0, Laqc;->l:Lcdu;

    invoke-virtual {v0}, Lcdu;->h()V

    iget-object v0, p0, Laqc;->q:Laqt;

    invoke-interface {v0}, Laqt;->m()V

    return-void
.end method

.method static synthetic o(Laqc;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Laqc;->t()V

    return-void
.end method

.method private o()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1071
    iget-object v1, p0, Laqc;->D:Laql;

    if-eqz v1, :cond_1

    .line 1074
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Laqc;->C:Ljava/util/Queue;

    iget-object v1, p0, Laqc;->C:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic p(Laqc;)Laqt;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Laqc;->q:Laqt;

    return-object v0
.end method

.method private p()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x3

    const/4 v8, 0x4

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1188
    invoke-virtual {p0}, Laqc;->e()V

    .line 1189
    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->G()Z

    move-result v0

    const-string v1, "mState.isStoryboardReady()"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 1191
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    .line 1192
    iget-object v1, p0, Laqc;->c:Lawf;

    invoke-interface {v1}, Lawf;->F()Lboi;

    move-result-object v5

    .line 1193
    if-nez v5, :cond_3

    .line 1194
    iget-object v1, p0, Laqc;->c:Lawf;

    invoke-interface {v1, v10}, Lawf;->a(Landroid/graphics/Bitmap;)V

    .line 1195
    if-eqz v0, :cond_0

    .line 1196
    invoke-interface {v0}, Laqv;->b()V

    .line 1198
    :cond_0
    iget v1, p0, Laqc;->M:I

    if-eq v1, v9, :cond_1

    iget v1, p0, Laqc;->M:I

    if-ne v1, v8, :cond_2

    .line 1200
    :cond_1
    const/4 v1, 0x6

    iput v1, p0, Laqc;->M:I

    .line 1201
    if-eqz v0, :cond_2

    .line 1202
    invoke-interface {v0}, Laqv;->p_()V

    .line 1279
    :cond_2
    :goto_0
    return-void

    .line 1208
    :cond_3
    iget-object v1, p0, Laqc;->c:Lawf;

    invoke-interface {v1}, Lawf;->aW()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1212
    iget-object v1, p0, Laqc;->c:Lawf;

    invoke-interface {v1}, Lawf;->ad()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Laqc;->c:Lawf;

    invoke-interface {v1}, Lawf;->bk()Z

    move-result v1

    if-nez v1, :cond_5

    .line 1213
    :cond_4
    iput-boolean v3, p0, Laqc;->K:Z

    .line 1216
    :cond_5
    iget-object v1, p0, Laqc;->t:Lasn;

    iget-object v1, v1, Lasn;->a:Ljava/lang/Object;

    check-cast v1, Laqv;

    iget-object v2, p0, Laqc;->c:Lawf;

    .line 1217
    invoke-interface {v2}, Lawf;->bk()Z

    move-result v2

    if-nez v2, :cond_9

    iget-object v2, p0, Laqc;->c:Lawf;

    invoke-interface {v2}, Lawf;->bl()Z

    move-result v2

    if-nez v2, :cond_9

    iget-boolean v2, p0, Laqc;->K:Z

    if-eqz v2, :cond_9

    move v2, v3

    .line 1216
    :goto_1
    invoke-interface {v1, v2}, Laqv;->k(Z)V

    .line 1220
    iget-boolean v1, p0, Laqc;->K:Z

    if-nez v1, :cond_2

    .line 1224
    iput-boolean v3, p0, Laqc;->A:Z

    .line 1226
    if-eqz v0, :cond_7

    .line 1227
    invoke-direct {p0}, Laqc;->q()V

    .line 1228
    invoke-virtual {v5}, Lboi;->o()I

    move-result v1

    invoke-virtual {v5}, Lboi;->p()I

    move-result v2

    invoke-interface {v0, v1, v2}, Laqv;->a(II)V

    .line 1229
    iget-object v1, p0, Laqc;->c:Lawf;

    invoke-interface {v1}, Lawf;->W()J

    move-result-wide v6

    invoke-interface {v0, v6, v7}, Laqv;->a(J)V

    .line 1231
    iget-object v1, p0, Laqc;->c:Lawf;

    invoke-interface {v1}, Lawf;->N()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 1233
    invoke-interface {v0, v10}, Laqv;->a(Laxs;)V

    .line 1240
    :cond_6
    :goto_2
    invoke-interface {v0, v4, v4}, Laqv;->a(ZI)V

    .line 1243
    :cond_7
    iget-object v1, p0, Laqc;->e:Lbtp;

    invoke-interface {v1}, Lbtp;->e()Z

    move-result v1

    if-eqz v1, :cond_b

    .line 1244
    iget-object v1, p0, Laqc;->e:Lbtp;

    invoke-interface {v1, v5}, Lbtp;->a(Lboi;)V

    .line 1266
    :goto_3
    iget v1, p0, Laqc;->M:I

    if-eq v1, v8, :cond_8

    .line 1267
    iput v8, p0, Laqc;->M:I

    .line 1268
    if-eqz v0, :cond_8

    .line 1269
    invoke-interface {v0}, Laqv;->q_()V

    .line 1270
    iget-boolean v1, p0, Laqc;->y:Z

    if-nez v1, :cond_8

    .line 1271
    invoke-interface {v0, v4}, Laqv;->a_(Z)V

    .line 1275
    :cond_8
    if-eqz v0, :cond_2

    .line 1276
    invoke-direct {p0}, Laqc;->r()V

    goto/16 :goto_0

    :cond_9
    move v2, v4

    .line 1217
    goto :goto_1

    .line 1236
    :cond_a
    iget-object v1, p0, Laqc;->c:Lawf;

    invoke-interface {v1}, Lawf;->aZ()Z

    move-result v1

    if-nez v1, :cond_6

    .line 1237
    invoke-direct {p0}, Laqc;->v()V

    goto :goto_2

    .line 1245
    :cond_b
    iget-boolean v1, p0, Laqc;->u:Z

    if-eqz v1, :cond_d

    .line 1246
    if-eqz v0, :cond_c

    .line 1247
    invoke-interface {v0, v3}, Laqv;->d_(Z)V

    .line 1249
    :cond_c
    invoke-direct {p0}, Laqc;->s()V

    goto :goto_3

    .line 1250
    :cond_d
    iget-object v1, p0, Laqc;->c:Lawf;

    invoke-interface {v1}, Lawf;->aX()Z

    move-result v1

    if-eqz v1, :cond_f

    .line 1251
    iget-object v1, p0, Laqc;->c:Lawf;

    invoke-interface {v1, v4}, Lawf;->q(Z)V

    .line 1252
    iget-object v1, p0, Laqc;->c:Lawf;

    invoke-interface {v1}, Lawf;->L()J

    move-result-wide v6

    invoke-direct {p0, v6, v7}, Laqc;->b(J)V

    .line 1253
    if-eqz v0, :cond_e

    .line 1254
    invoke-interface {v0, v3}, Laqv;->d_(Z)V

    .line 1256
    :cond_e
    invoke-direct {p0}, Laqc;->s()V

    goto :goto_3

    .line 1258
    :cond_f
    iget-object v1, p0, Laqc;->e:Lbtp;

    invoke-interface {v1}, Lbtp;->a()V

    .line 1259
    iget v1, p0, Laqc;->M:I

    if-ne v1, v9, :cond_10

    iget-object v1, p0, Laqc;->c:Lawf;

    .line 1260
    invoke-interface {v1}, Lawf;->aC()Z

    move-result v1

    if-nez v1, :cond_10

    .line 1261
    iput-boolean v3, p0, Laqc;->y:Z

    .line 1263
    :cond_10
    invoke-direct {p0}, Laqc;->t()V

    goto :goto_3
.end method

.method private q()V
    .locals 2

    .prologue
    .line 1305
    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->F()Lboi;

    move-result-object v0

    .line 1306
    if-eqz v0, :cond_0

    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->az()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    .line 1307
    :goto_0
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    invoke-interface {v0, v1}, Laqv;->a(Z)V

    .line 1308
    return-void

    .line 1306
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0
.end method

.method static synthetic q(Laqc;)Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Laqc;->H:Z

    return v0
.end method

.method private r()V
    .locals 2

    .prologue
    .line 1311
    iget v0, p0, Laqc;->M:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Laqc;->c:Lawf;

    .line 1312
    invoke-interface {v0}, Lawf;->az()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->N()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    move v1, v0

    .line 1313
    :goto_0
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    invoke-interface {v0, v1}, Laqv;->j(Z)V

    .line 1314
    return-void

    .line 1312
    :cond_0
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0
.end method

.method static synthetic r(Laqc;)Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Laqc;->y:Z

    return v0
.end method

.method private s()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1378
    invoke-virtual {p0}, Laqc;->e()V

    .line 1380
    iget-object v0, p0, Laqc;->d:Lbgf;

    invoke-virtual {v0}, Lbgf;->a()V

    .line 1381
    iput-boolean v2, p0, Laqc;->u:Z

    .line 1382
    iput v2, p0, Laqc;->z:I

    .line 1383
    iget-boolean v0, p0, Laqc;->B:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Laqc;->p:Lced;

    iget-object v1, p0, Laqc;->X:Lcef;

    invoke-virtual {v0, v1}, Lced;->a(Lcef;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1384
    :cond_0
    iput-boolean v3, p0, Laqc;->B:Z

    .line 1385
    iget-object v0, p0, Laqc;->e:Lbtp;

    invoke-interface {v0}, Lbtp;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1387
    iget-object v0, p0, Laqc;->e:Lbtp;

    invoke-interface {v0}, Lbtp;->c()V

    .line 1388
    iget-boolean v0, p0, Laqc;->x:Z

    if-eqz v0, :cond_1

    .line 1389
    invoke-direct {p0}, Laqc;->u()V

    .line 1390
    iput-boolean v2, p0, Laqc;->x:Z

    .line 1410
    :cond_1
    :goto_0
    return-void

    .line 1394
    :cond_2
    invoke-direct {p0}, Laqc;->u()V

    .line 1395
    iput-boolean v2, p0, Laqc;->x:Z

    .line 1396
    iget-object v0, p0, Laqc;->t:Lasn;

    invoke-virtual {v0}, Lasn;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1397
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    .line 1398
    invoke-interface {v0, v3}, Laqv;->c_(Z)V

    .line 1400
    iget-object v2, p0, Laqc;->e:Lbtp;

    iget-object v1, p0, Laqc;->c:Lawf;

    invoke-interface {v1}, Lawf;->F()Lboi;

    move-result-object v3

    iget-boolean v1, p0, Laqc;->w:Z

    if-eqz v1, :cond_3

    iget-wide v4, p0, Laqc;->v:J

    .line 1401
    invoke-static {v4, v5}, Lbss;->b(J)Lbss;

    move-result-object v1

    .line 1402
    :goto_1
    iget-object v4, p0, Laqc;->d:Lbgf;

    .line 1403
    invoke-interface {v0, v4}, Laqv;->a(Lbgf;)Lbge;

    move-result-object v0

    .line 1400
    invoke-interface {v2, v3, v1, v0}, Lbtp;->a(Lboi;Lbss;Lbge;)V

    .line 1404
    invoke-direct {p0}, Laqc;->v()V

    goto :goto_0

    .line 1401
    :cond_3
    iget-wide v4, p0, Laqc;->v:J

    .line 1402
    invoke-static {v4, v5}, Lbss;->a(J)Lbss;

    move-result-object v1

    goto :goto_1

    .line 1408
    :cond_4
    sget-object v0, Laqc;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic s(Laqc;)Z
    .locals 1

    .prologue
    .line 61
    iget-boolean v0, p0, Laqc;->I:Z

    return v0
.end method

.method private t()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 1413
    invoke-virtual {p0}, Laqc;->e()V

    .line 1417
    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->F()Lboi;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqc;->q:Laqt;

    .line 1418
    invoke-interface {v0}, Laqt;->q()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Laqc;->e:Lbtp;

    invoke-interface {v0}, Lbtp;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1431
    :cond_0
    :goto_0
    return-void

    .line 1422
    :cond_1
    iget-object v0, p0, Laqc;->d:Lbgf;

    invoke-virtual {v0}, Lbgf;->a()V

    .line 1423
    iput-boolean v1, p0, Laqc;->x:Z

    .line 1424
    iget-object v0, p0, Laqc;->t:Lasn;

    invoke-virtual {v0}, Lasn;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1425
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    .line 1426
    invoke-interface {v0, v1}, Laqv;->c_(Z)V

    .line 1427
    iget-object v1, p0, Laqc;->e:Lbtp;

    iget-object v2, p0, Laqc;->c:Lawf;

    invoke-interface {v2}, Lawf;->F()Lboi;

    move-result-object v2

    iget-object v3, p0, Laqc;->c:Lawf;

    .line 1428
    invoke-interface {v3}, Lawf;->L()J

    move-result-wide v4

    invoke-static {v4, v5}, Lbss;->b(J)Lbss;

    move-result-object v3

    iget-object v4, p0, Laqc;->d:Lbgf;

    .line 1429
    invoke-interface {v0, v4}, Laqv;->a(Lbgf;)Lbge;

    move-result-object v0

    .line 1427
    invoke-interface {v1, v2, v3, v0}, Lbtp;->a(Lboi;Lbss;Lbge;)V

    goto :goto_0
.end method

.method static synthetic t(Laqc;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Laqc;->w()V

    return-void
.end method

.method static synthetic u(Laqc;)Laqb;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Laqc;->n:Laqb;

    return-object v0
.end method

.method private u()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1434
    invoke-virtual {p0}, Laqc;->e()V

    .line 1435
    iget-object v1, p0, Laqc;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    .line 1436
    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v0, :cond_0

    .line 1437
    :goto_0
    iget-object v1, p0, Laqc;->l:Lcdu;

    iget-object v2, p0, Laqc;->c:Lawf;

    invoke-interface {v2}, Lawf;->o()Lbza;

    move-result-object v2

    iget-object v3, p0, Laqc;->c:Lawf;

    invoke-interface {v3}, Lawf;->w()Lboh;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v0}, Lcdu;->a(Lbza;Lboh;Z)V

    .line 1438
    return-void

    .line 1436
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private v()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1441
    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0, v1}, Lawf;->i(Z)V

    .line 1442
    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0, v1}, Lawf;->r(Z)V

    .line 1443
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    invoke-interface {v0}, Laqv;->r_()V

    .line 1444
    invoke-direct {p0}, Laqc;->r()V

    .line 1445
    return-void
.end method

.method static synthetic v(Laqc;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 61
    iget v1, p0, Laqc;->z:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Laqc;->z:I

    if-le v1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Laqc;->s()V

    iget-object v2, p0, Laqc;->c:Lawf;

    invoke-interface {v2}, Lawf;->L()J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Laqc;->b(J)V

    iput v1, p0, Laqc;->z:I

    goto :goto_0
.end method

.method private w()V
    .locals 2

    .prologue
    .line 1448
    iget-boolean v0, p0, Laqc;->B:Z

    if-eqz v0, :cond_0

    .line 1449
    iget-object v0, p0, Laqc;->p:Lced;

    iget-object v1, p0, Laqc;->X:Lcef;

    invoke-virtual {v0, v1}, Lced;->b(Lcef;)V

    .line 1450
    const/4 v0, 0x0

    iput-boolean v0, p0, Laqc;->B:Z

    .line 1452
    :cond_0
    return-void
.end method

.method static synthetic w(Laqc;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Laqc;->r()V

    return-void
.end method

.method static synthetic x(Laqc;)J
    .locals 2

    .prologue
    .line 61
    iget-wide v0, p0, Laqc;->J:J

    return-wide v0
.end method

.method static synthetic y(Laqc;)Laqu;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Laqc;->E:Laqu;

    return-object v0
.end method

.method static synthetic z(Laqc;)Lalg;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Laqc;->g:Lalg;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 892
    invoke-super {p0}, Lamn;->a()V

    .line 893
    iget-object v0, p0, Laqc;->t:Lasn;

    invoke-virtual {v0}, Lasn;->a()V

    .line 894
    iget-object v0, p0, Laqc;->c:Lawf;

    iget-object v1, p0, Laqc;->P:Lawk;

    invoke-interface {v0, v1}, Lawf;->e(Lawk;)V

    .line 895
    iget-object v0, p0, Laqc;->c:Lawf;

    iget-object v1, p0, Laqc;->R:Lawk;

    invoke-interface {v0, v1}, Lawf;->n(Lawk;)V

    .line 896
    iget-object v0, p0, Laqc;->c:Lawf;

    iget-object v1, p0, Laqc;->S:Lawk;

    invoke-interface {v0, v1}, Lawf;->p(Lawk;)V

    .line 897
    iget-object v0, p0, Laqc;->c:Lawf;

    iget-object v1, p0, Laqc;->S:Lawk;

    invoke-interface {v0, v1}, Lawf;->L(Lawk;)V

    .line 898
    iget-object v0, p0, Laqc;->c:Lawf;

    iget-object v1, p0, Laqc;->Q:Lawk;

    invoke-interface {v0, v1}, Lawf;->i(Lawk;)V

    .line 899
    iget-object v0, p0, Laqc;->c:Lawf;

    iget-object v1, p0, Laqc;->U:Lawk;

    invoke-interface {v0, v1}, Lawf;->t(Lawk;)V

    .line 900
    iget-object v0, p0, Laqc;->c:Lawf;

    iget-object v1, p0, Laqc;->V:Lawk;

    invoke-interface {v0, v1}, Lawf;->g(Lawk;)V

    .line 901
    iget-object v0, p0, Laqc;->c:Lawf;

    iget-object v1, p0, Laqc;->W:Lawk;

    invoke-interface {v0, v1}, Lawf;->F(Lawk;)V

    .line 902
    iget-object v0, p0, Laqc;->c:Lawf;

    iget-object v1, p0, Laqc;->T:Lawk;

    invoke-interface {v0, v1}, Lawf;->B(Lawk;)V

    .line 906
    iget-boolean v0, p0, Laqc;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 907
    new-instance v0, Laqu;

    invoke-direct {v0, p0}, Laqu;-><init>(Laqc;)V

    iput-object v0, p0, Laqc;->E:Laqu;

    .line 908
    iget-object v0, p0, Laqc;->g:Lalg;

    iget-object v1, p0, Laqc;->E:Laqu;

    const/4 v2, 0x1

    new-array v2, v2, [Lboi;

    iget-object v3, p0, Laqc;->c:Lawf;

    .line 909
    invoke-interface {v3}, Lawf;->F()Lboi;

    move-result-object v3

    aput-object v3, v2, v4

    .line 908
    invoke-interface {v0, v1, v2}, Lalg;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 911
    :cond_0
    iput-boolean v4, p0, Laqc;->F:Z

    .line 914
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 950
    invoke-virtual {p0}, Laqc;->e()V

    .line 951
    iput p1, p0, Laqc;->N:I

    .line 952
    const/4 v0, 0x5

    iput v0, p0, Laqc;->M:I

    .line 953
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    iget v1, p0, Laqc;->N:I

    invoke-interface {v0, v1}, Laqv;->d(I)V

    .line 954
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 1119
    invoke-virtual {p0}, Laqc;->e()V

    .line 1120
    invoke-direct {p0}, Laqc;->o()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1121
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    const/4 v1, 0x3

    invoke-interface {v0, p1, v1}, Laqv;->a(Landroid/graphics/Bitmap;I)V

    .line 1125
    :goto_0
    return-void

    .line 1123
    :cond_0
    iput-object p1, p0, Laqc;->G:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public a(Laqv;)V
    .locals 1

    .prologue
    .line 806
    iget-object v0, p0, Laqc;->t:Lasn;

    invoke-virtual {v0, p1}, Lasn;->c(Ljava/lang/Object;)V

    .line 807
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 1065
    new-instance v0, Laql;

    invoke-direct {v0, p0}, Laql;-><init>(Laqc;)V

    iput-object v0, p0, Laqc;->D:Laql;

    .line 1066
    iget-object v0, p0, Laqc;->g:Lalg;

    iget-object v1, p0, Laqc;->D:Laql;

    .line 1067
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Landroid/net/Uri;

    invoke-interface {p1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    .line 1066
    invoke-interface {v0, v1, v2}, Lalg;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 1068
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1141
    iput-boolean v1, p0, Laqc;->L:Z

    .line 1142
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    invoke-interface {v0, v1}, Laqv;->m(Z)V

    .line 1143
    if-eqz p1, :cond_0

    .line 1144
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    invoke-interface {v0}, Laqv;->k_()V

    .line 1148
    :goto_0
    return-void

    .line 1146
    :cond_0
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    invoke-interface {v0}, Laqv;->l_()V

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 919
    const/4 v0, 0x0

    iput-boolean v0, p0, Laqc;->u:Z

    .line 920
    iget-object v0, p0, Laqc;->t:Lasn;

    invoke-virtual {v0}, Lasn;->b()V

    .line 922
    iget-object v0, p0, Laqc;->E:Laqu;

    if-eqz v0, :cond_0

    .line 923
    iget-object v0, p0, Laqc;->E:Laqu;

    invoke-virtual {v0, v1}, Laqu;->cancel(Z)Z

    .line 924
    iput-boolean v1, p0, Laqc;->F:Z

    .line 925
    iput-object v2, p0, Laqc;->E:Laqu;

    .line 927
    :cond_0
    iget-object v0, p0, Laqc;->D:Laql;

    if-eqz v0, :cond_1

    .line 928
    iget-object v0, p0, Laqc;->D:Laql;

    invoke-virtual {v0, v1}, Laql;->cancel(Z)Z

    .line 929
    iput-object v2, p0, Laqc;->D:Laql;

    .line 931
    :cond_1
    invoke-virtual {p0}, Laqc;->h()V

    .line 932
    iget-object v0, p0, Laqc;->f:Lasy;

    invoke-virtual {v0, v2}, Lasy;->a(Ljava/lang/Object;)V

    .line 933
    iget-object v0, p0, Laqc;->c:Lawf;

    iget-object v1, p0, Laqc;->P:Lawk;

    invoke-interface {v0, v1}, Lawf;->f(Lawk;)V

    .line 934
    iget-object v0, p0, Laqc;->c:Lawf;

    iget-object v1, p0, Laqc;->R:Lawk;

    invoke-interface {v0, v1}, Lawf;->o(Lawk;)V

    .line 935
    iget-object v0, p0, Laqc;->c:Lawf;

    iget-object v1, p0, Laqc;->S:Lawk;

    invoke-interface {v0, v1}, Lawf;->q(Lawk;)V

    .line 936
    iget-object v0, p0, Laqc;->c:Lawf;

    iget-object v1, p0, Laqc;->S:Lawk;

    invoke-interface {v0, v1}, Lawf;->M(Lawk;)V

    .line 937
    iget-object v0, p0, Laqc;->c:Lawf;

    iget-object v1, p0, Laqc;->Q:Lawk;

    invoke-interface {v0, v1}, Lawf;->j(Lawk;)V

    .line 938
    iget-object v0, p0, Laqc;->c:Lawf;

    iget-object v1, p0, Laqc;->U:Lawk;

    invoke-interface {v0, v1}, Lawf;->u(Lawk;)V

    .line 939
    iget-object v0, p0, Laqc;->c:Lawf;

    iget-object v1, p0, Laqc;->V:Lawk;

    invoke-interface {v0, v1}, Lawf;->h(Lawk;)V

    .line 940
    iget-object v0, p0, Laqc;->c:Lawf;

    iget-object v1, p0, Laqc;->W:Lawk;

    invoke-interface {v0, v1}, Lawf;->G(Lawk;)V

    .line 941
    iget-object v0, p0, Laqc;->c:Lawf;

    iget-object v1, p0, Laqc;->T:Lawk;

    invoke-interface {v0, v1}, Lawf;->C(Lawk;)V

    .line 942
    invoke-direct {p0}, Laqc;->w()V

    .line 943
    invoke-super {p0}, Lamn;->b()V

    .line 944
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 1106
    invoke-virtual {p0}, Laqc;->e()V

    .line 1108
    invoke-direct {p0}, Laqc;->o()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1109
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    invoke-interface {v0, p1}, Laqv;->b_(I)V

    .line 1111
    :cond_0
    return-void
.end method

.method public b(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 1133
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    invoke-interface {v0, p1}, Laqv;->c(Landroid/graphics/Bitmap;)V

    .line 1134
    return-void
.end method

.method public b(Laqv;)V
    .locals 1

    .prologue
    .line 810
    iget-object v0, p0, Laqc;->t:Lasn;

    invoke-virtual {v0, p1}, Lasn;->d(Ljava/lang/Object;)V

    .line 811
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 957
    invoke-virtual {p0}, Laqc;->e()V

    .line 959
    const/4 v0, 0x7

    iput v0, p0, Laqc;->M:I

    .line 960
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    invoke-interface {v0}, Laqv;->e()V

    .line 961
    return-void
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 1137
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    invoke-interface {v0, p1}, Laqv;->e(I)V

    .line 1138
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 964
    invoke-virtual {p0}, Laqc;->e()V

    .line 966
    const/4 v0, 0x7

    iput v0, p0, Laqc;->M:I

    .line 967
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    invoke-interface {v0}, Laqv;->o_()V

    .line 968
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 975
    invoke-virtual {p0}, Laqc;->e()V

    .line 978
    const/4 v0, 0x3

    iput v0, p0, Laqc;->M:I

    .line 979
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    .line 980
    invoke-interface {v0}, Laqv;->a()V

    .line 981
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Laqv;->l(Z)V

    .line 983
    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 984
    invoke-direct {p0}, Laqc;->p()V

    .line 986
    :cond_0
    return-void
.end method

.method public g()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 996
    invoke-virtual {p0}, Laqc;->e()V

    .line 997
    iget v0, p0, Laqc;->M:I

    const/4 v3, 0x5

    if-eq v0, v3, :cond_0

    iget v0, p0, Laqc;->M:I

    const/4 v3, 0x7

    if-ne v0, v3, :cond_1

    :cond_0
    move v0, v2

    .line 1018
    :goto_0
    return v0

    .line 1001
    :cond_1
    iput-boolean v2, p0, Laqc;->A:Z

    .line 1002
    const/4 v0, 0x2

    iput v0, p0, Laqc;->M:I

    .line 1003
    iput-boolean v2, p0, Laqc;->u:Z

    .line 1005
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    .line 1006
    invoke-interface {v0, v1}, Laqv;->a_(Z)V

    .line 1007
    invoke-interface {v0}, Laqv;->a()V

    .line 1008
    invoke-interface {v0, v2}, Laqv;->a(Z)V

    .line 1009
    invoke-interface {v0}, Laqv;->b()V

    .line 1010
    invoke-interface {v0, v1}, Laqv;->i(Z)V

    .line 1012
    iget-object v3, p0, Laqc;->c:Lawf;

    invoke-interface {v3}, Lawf;->aZ()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1013
    invoke-interface {v0}, Laqv;->r_()V

    .line 1015
    :cond_2
    invoke-direct {p0}, Laqc;->r()V

    .line 1016
    invoke-interface {v0, v2}, Laqv;->l(Z)V

    .line 1017
    invoke-interface {v0}, Laqv;->q_()V

    move v0, v1

    .line 1018
    goto :goto_0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 1025
    invoke-virtual {p0}, Laqc;->e()V

    .line 1026
    const/4 v0, 0x0

    iput-boolean v0, p0, Laqc;->u:Z

    .line 1027
    iget-object v0, p0, Laqc;->e:Lbtp;

    invoke-interface {v0}, Lbtp;->a()V

    .line 1028
    return-void
.end method

.method public i()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1031
    invoke-direct {p0}, Laqc;->v()V

    .line 1032
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    .line 1033
    invoke-interface {v0}, Laqv;->m()V

    .line 1034
    invoke-interface {v0, v1, v1}, Laqv;->a(ZI)V

    .line 1035
    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0, v1}, Lawf;->h(Z)V

    .line 1036
    return-void
.end method

.method public j()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 1040
    invoke-virtual {p0}, Laqc;->e()V

    .line 1041
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    invoke-interface {v0}, Laqv;->l()Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public k()Lbmd;
    .locals 4

    .prologue
    .line 1052
    invoke-virtual {p0}, Laqc;->e()V

    .line 1053
    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->F()Lboi;

    move-result-object v0

    .line 1054
    if-eqz v0, :cond_0

    iget-object v1, p0, Laqc;->c:Lawf;

    .line 1055
    invoke-interface {v1}, Lawf;->L()J

    move-result-wide v2

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, Lboi;->b(JZ)Lbmd;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public l()Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 1087
    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->M()Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1095
    iget-object v0, p0, Laqc;->c:Lawf;

    invoke-interface {v0}, Lawf;->aT()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public n()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1128
    iput-boolean v1, p0, Laqc;->L:Z

    .line 1129
    iget-object v0, p0, Laqc;->t:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    invoke-interface {v0, v1}, Laqv;->m(Z)V

    .line 1130
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1152
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Laqc;->t:Lasn;

    .line 1153
    iget-object v3, v3, Lasn;->a:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Laqc;->c:Lawf;

    .line 1154
    invoke-interface {v3}, Lawf;->o()Lbza;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Laqc;->c:Lawf;

    .line 1155
    invoke-interface {v3}, Lawf;->r()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1152
    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
