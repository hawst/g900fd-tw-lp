.class public final Lbsb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcgk;


# instance fields
.field private final a:Lbgf;

.field private final b:Lbgx;

.field private final c:Lbgx;

.field private d:I

.field private e:I

.field private f:Layc;

.field private g:Lbgx;

.field private h:Lbgx;

.field private i:Lbfw;

.field private j:Lbfw;

.field private k:Lbfw;

.field private l:Lbfw;

.field private m:I

.field private n:I

.field private o:Z


# direct methods
.method public constructor <init>(Lbgf;Lbgx;Lbgx;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    const-string v0, "renderContext"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgf;

    iput-object v0, p0, Lbsb;->a:Lbgf;

    .line 40
    const-string v0, "mixShaderProgram"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgx;

    iput-object v0, p0, Lbsb;->c:Lbgx;

    .line 41
    const-string v0, "identityShader"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgx;

    iput-object v0, p0, Lbsb;->b:Lbgx;

    .line 42
    sget-object v0, Layc;->a:Layc;

    iput-object v0, p0, Lbsb;->f:Layc;

    .line 43
    return-void
.end method

.method private a(Lbhb;Lbfw;Lbgn;F)V
    .locals 3

    .prologue
    .line 135
    iget-boolean v0, p0, Lbsb;->o:Z

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Lbsb;->i:Lbfw;

    iget-object v1, p0, Lbsb;->b:Lbgx;

    invoke-static {v0, p1, v1}, Lbfu;->a(Lbgn;Lbhb;Lbgx;)V

    .line 139
    iget-object v0, p0, Lbsb;->j:Lbfw;

    iget-object v1, p0, Lbsb;->i:Lbfw;

    iget-object v2, p0, Lbsb;->g:Lbgx;

    invoke-static {v0, v1, v2}, Lbfu;->a(Lbgn;Lbhb;Lbgx;)V

    .line 142
    iget-object v0, p0, Lbsb;->j:Lbfw;

    iget-object v1, p0, Lbsb;->h:Lbgx;

    invoke-static {p2, v0, v1}, Lbfu;->a(Lbgn;Lbhb;Lbgx;)V

    .line 144
    iget-object v0, p0, Lbsb;->c:Lbgx;

    invoke-static {p3, p1, p2, p4, v0}, Lbfu;->a(Lbgn;Lbhb;Lbhb;FLbgx;)V

    .line 153
    :goto_0
    return-void

    .line 147
    :cond_0
    iget-object v0, p0, Lbsb;->i:Lbfw;

    iget-object v1, p0, Lbsb;->g:Lbgx;

    invoke-static {v0, p1, v1}, Lbfu;->a(Lbgn;Lbhb;Lbgx;)V

    .line 149
    iget-object v0, p0, Lbsb;->i:Lbfw;

    iget-object v1, p0, Lbsb;->h:Lbgx;

    invoke-static {p2, v0, v1}, Lbfu;->a(Lbgn;Lbhb;Lbgx;)V

    .line 151
    iget-object v0, p0, Lbsb;->c:Lbgx;

    invoke-static {p3, p1, p2, p4, v0}, Lbfu;->a(Lbgn;Lbhb;Lbhb;FLbgx;)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lbsb;->i:Lbfw;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 128
    iget-object v0, p0, Lbsb;->j:Lbfw;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 129
    iget-object v0, p0, Lbsb;->k:Lbfw;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 130
    iget-object v0, p0, Lbsb;->l:Lbfw;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 131
    return-void
.end method

.method public a(II)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 61
    const-string v0, "textureWidth"

    invoke-static {p1, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lbsb;->d:I

    .line 62
    const-string v0, "textureHeight"

    invoke-static {p2, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lbsb;->e:I

    .line 64
    iget-object v0, p0, Lbsb;->f:Layc;

    sget-object v2, Layc;->a:Layc;

    if-ne v0, v2, :cond_0

    .line 107
    :goto_0
    return-void

    .line 68
    :cond_0
    iget-object v0, p0, Lbsb;->f:Layc;

    invoke-virtual {v0}, Layc;->c()F

    move-result v0

    .line 69
    iget v2, p0, Lbsb;->d:I

    int-to-float v2, v2

    div-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    iput v2, p0, Lbsb;->n:I

    .line 70
    iget v2, p0, Lbsb;->e:I

    int-to-float v2, v2

    div-float v0, v2, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iput v0, p0, Lbsb;->m:I

    .line 71
    iget v0, p0, Lbsb;->n:I

    iget v2, p0, Lbsb;->d:I

    if-ne v0, v2, :cond_1

    iget v0, p0, Lbsb;->m:I

    iget v2, p0, Lbsb;->e:I

    if-eq v0, v2, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lbsb;->o:Z

    .line 75
    invoke-virtual {p0}, Lbsb;->a()V

    .line 77
    iget-boolean v0, p0, Lbsb;->o:Z

    if-eqz v0, :cond_3

    .line 78
    iget-object v0, p0, Lbsb;->a:Lbgf;

    iget v2, p0, Lbsb;->n:I

    iget v3, p0, Lbsb;->m:I

    invoke-virtual {v0, v2, v3}, Lbgf;->b(II)Lbfw;

    move-result-object v0

    iput-object v0, p0, Lbsb;->i:Lbfw;

    .line 80
    iget-object v0, p0, Lbsb;->a:Lbgf;

    iget v2, p0, Lbsb;->n:I

    iget v3, p0, Lbsb;->m:I

    invoke-virtual {v0, v2, v3}, Lbgf;->b(II)Lbfw;

    move-result-object v0

    iput-object v0, p0, Lbsb;->j:Lbfw;

    .line 82
    iget-object v0, p0, Lbsb;->a:Lbgf;

    iget v2, p0, Lbsb;->n:I

    iget v3, p0, Lbsb;->m:I

    invoke-virtual {v0, v2, v3}, Lbgf;->b(II)Lbfw;

    move-result-object v0

    iput-object v0, p0, Lbsb;->k:Lbfw;

    .line 84
    iget-object v0, p0, Lbsb;->a:Lbgf;

    iget v2, p0, Lbsb;->n:I

    iget v3, p0, Lbsb;->m:I

    invoke-virtual {v0, v2, v3}, Lbgf;->b(II)Lbfw;

    move-result-object v0

    iput-object v0, p0, Lbsb;->l:Lbfw;

    .line 86
    iget-object v0, p0, Lbsb;->a:Lbgf;

    iget-object v2, p0, Lbsb;->f:Layc;

    .line 88
    invoke-virtual {v2}, Layc;->a()I

    move-result v2

    iget v3, p0, Lbsb;->m:I

    .line 87
    invoke-static {v0, v2, v3, v1}, Lbfx;->a(Lbgf;IIZ)Lbgx;

    move-result-object v0

    iput-object v0, p0, Lbsb;->g:Lbgx;

    .line 89
    iget-object v0, p0, Lbsb;->a:Lbgf;

    iget-object v2, p0, Lbsb;->f:Layc;

    .line 91
    invoke-virtual {v2}, Layc;->a()I

    move-result v2

    iget v3, p0, Lbsb;->n:I

    .line 90
    invoke-static {v0, v2, v3, v1}, Lbfx;->b(Lbgf;IIZ)Lbgx;

    move-result-object v0

    iput-object v0, p0, Lbsb;->h:Lbgx;

    goto/16 :goto_0

    :cond_2
    move v0, v1

    .line 71
    goto :goto_1

    .line 93
    :cond_3
    iget-object v0, p0, Lbsb;->a:Lbgf;

    iget v2, p0, Lbsb;->d:I

    iget v3, p0, Lbsb;->e:I

    .line 94
    invoke-virtual {v0, v2, v3}, Lbgf;->b(II)Lbfw;

    move-result-object v0

    iput-object v0, p0, Lbsb;->i:Lbfw;

    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lbsb;->j:Lbfw;

    .line 96
    iget-object v0, p0, Lbsb;->a:Lbgf;

    iget v2, p0, Lbsb;->d:I

    iget v3, p0, Lbsb;->e:I

    .line 97
    invoke-virtual {v0, v2, v3}, Lbgf;->b(II)Lbfw;

    move-result-object v0

    iput-object v0, p0, Lbsb;->k:Lbfw;

    .line 98
    iget-object v0, p0, Lbsb;->a:Lbgf;

    iget v2, p0, Lbsb;->d:I

    iget v3, p0, Lbsb;->e:I

    .line 99
    invoke-virtual {v0, v2, v3}, Lbgf;->b(II)Lbfw;

    move-result-object v0

    iput-object v0, p0, Lbsb;->l:Lbfw;

    .line 100
    iget-object v0, p0, Lbsb;->a:Lbgf;

    iget-object v2, p0, Lbsb;->f:Layc;

    .line 102
    invoke-virtual {v2}, Layc;->a()I

    move-result v2

    iget v3, p0, Lbsb;->e:I

    .line 101
    invoke-static {v0, v2, v3, v1}, Lbfx;->a(Lbgf;IIZ)Lbgx;

    move-result-object v0

    iput-object v0, p0, Lbsb;->g:Lbgx;

    .line 103
    iget-object v0, p0, Lbsb;->a:Lbgf;

    iget-object v2, p0, Lbsb;->f:Layc;

    .line 105
    invoke-virtual {v2}, Layc;->a()I

    move-result v2

    iget v3, p0, Lbsb;->n:I

    .line 104
    invoke-static {v0, v2, v3, v1}, Lbfx;->b(Lbgf;IIZ)Lbgx;

    move-result-object v0

    iput-object v0, p0, Lbsb;->h:Lbgx;

    goto/16 :goto_0
.end method

.method public a(Layc;)V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lbsb;->f:Layc;

    if-ne v0, p1, :cond_0

    .line 58
    :goto_0
    return-void

    .line 54
    :cond_0
    iput-object p1, p0, Lbsb;->f:Layc;

    .line 57
    iget v0, p0, Lbsb;->d:I

    iget v1, p0, Lbsb;->e:I

    invoke-virtual {p0, v0, v1}, Lbsb;->a(II)V

    goto :goto_0
.end method

.method public a(Lbhb;Lbgn;F)V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lbsb;->k:Lbfw;

    invoke-direct {p0, p1, v0, p2, p3}, Lbsb;->a(Lbhb;Lbfw;Lbgn;F)V

    .line 111
    return-void
.end method

.method public b()Layc;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lbsb;->f:Layc;

    return-object v0
.end method

.method public b(Lbhb;Lbgn;F)V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lbsb;->l:Lbfw;

    invoke-direct {p0, p1, v0, p2, p3}, Lbsb;->a(Lbhb;Lbfw;Lbgn;F)V

    .line 119
    return-void
.end method

.method public c()Lbhb;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lbsb;->k:Lbfw;

    return-object v0
.end method

.method public d()Lbhb;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lbsb;->l:Lbfw;

    return-object v0
.end method
