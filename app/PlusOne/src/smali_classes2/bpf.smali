.class public final Lbpf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbph;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lbpf;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    new-instance v0, Lbpg;

    invoke-direct {v0}, Lbpg;-><init>()V

    sput-object v0, Lbpf;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(II)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput p1, p0, Lbpf;->a:I

    .line 25
    iput p2, p0, Lbpf;->b:I

    .line 26
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lbpf;->a:I

    .line 30
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lbpf;->b:I

    .line 31
    return-void
.end method


# virtual methods
.method public a()Lbut;
    .locals 3

    .prologue
    .line 42
    new-instance v0, Lbut;

    invoke-direct {v0}, Lbut;-><init>()V

    .line 43
    const/4 v1, 0x4

    iput v1, v0, Lbut;->b:I

    .line 44
    new-instance v1, Lbus;

    invoke-direct {v1}, Lbus;-><init>()V

    .line 45
    iget v2, p0, Lbpf;->a:I

    iput v2, v1, Lbus;->a:I

    .line 46
    iget v2, p0, Lbpf;->b:I

    iput v2, v1, Lbus;->b:I

    .line 47
    iput-object v1, v0, Lbut;->f:Lbus;

    .line 48
    return-object v0
.end method

.method public a(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 35
    iget v0, p0, Lbpf;->a:I

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 36
    iget v1, p0, Lbpf;->a:I

    invoke-interface {p1, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 37
    iget v1, p0, Lbpf;->b:I

    invoke-interface {p1, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 38
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lbpf;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 59
    iget v0, p0, Lbpf;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 60
    return-void
.end method
