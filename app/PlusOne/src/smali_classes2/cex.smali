.class public final Lcex;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:[[F

.field private final d:[[I

.field private final e:Lcey;

.field private final f:Lcey;

.field private final g:[Z

.field private final h:[I

.field private i:[[F


# direct methods
.method public constructor <init>(I)V
    .locals 3

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput p1, p0, Lcex;->a:I

    .line 25
    iget v0, p0, Lcex;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcex;->b:I

    .line 26
    iget v0, p0, Lcex;->a:I

    iget v1, p0, Lcex;->a:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[F

    iput-object v0, p0, Lcex;->c:[[F

    .line 27
    new-instance v0, Lcey;

    iget v1, p0, Lcex;->a:I

    iget v2, p0, Lcex;->a:I

    mul-int/2addr v1, v2

    invoke-direct {v0, v1}, Lcey;-><init>(I)V

    iput-object v0, p0, Lcex;->f:Lcey;

    .line 28
    new-instance v0, Lcey;

    iget v1, p0, Lcex;->a:I

    invoke-direct {v0, v1}, Lcey;-><init>(I)V

    iput-object v0, p0, Lcex;->e:Lcey;

    .line 29
    iget v0, p0, Lcex;->a:I

    iget v1, p0, Lcex;->a:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, p0, Lcex;->d:[[I

    .line 30
    iget v0, p0, Lcex;->a:I

    new-array v0, v0, [Z

    iput-object v0, p0, Lcex;->g:[Z

    .line 31
    iget v0, p0, Lcex;->a:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcex;->h:[I

    .line 32
    return-void
.end method

.method private a()F
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 63
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    move v2, v0

    move v0, v1

    .line 66
    :goto_0
    iget-object v3, p0, Lcex;->e:Lcey;

    invoke-virtual {v3}, Lcey;->d()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 67
    iget-object v3, p0, Lcex;->e:Lcey;

    invoke-virtual {v3, v0}, Lcey;->b(I)I

    move-result v3

    .line 68
    iget-object v4, p0, Lcex;->e:Lcey;

    add-int/lit8 v5, v0, -0x1

    invoke-virtual {v4, v5}, Lcey;->b(I)I

    move-result v4

    .line 69
    iget-object v5, p0, Lcex;->i:[[F

    aget-object v5, v5, v3

    aget v5, v5, v4

    iget-object v6, p0, Lcex;->c:[[F

    aget-object v3, v6, v3

    aget v3, v3, v4

    sub-float v3, v5, v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 66
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    :cond_0
    :goto_1
    iget-object v0, p0, Lcex;->e:Lcey;

    invoke-virtual {v0}, Lcey;->d()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 73
    iget-object v0, p0, Lcex;->e:Lcey;

    invoke-virtual {v0, v1}, Lcey;->b(I)I

    move-result v0

    .line 74
    iget-object v3, p0, Lcex;->e:Lcey;

    add-int/lit8 v4, v1, -0x1

    invoke-virtual {v3, v4}, Lcey;->b(I)I

    move-result v3

    .line 75
    iget-object v4, p0, Lcex;->c:[[F

    aget-object v4, v4, v0

    aget v5, v4, v3

    add-float/2addr v5, v2

    aput v5, v4, v3

    .line 76
    iget-object v4, p0, Lcex;->c:[[F

    aget-object v3, v4, v3

    aget v4, v3, v0

    sub-float/2addr v4, v2

    aput v4, v3, v0

    .line 72
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 78
    :cond_1
    return v2
.end method

.method private a(Z)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 82
    iget-object v0, p0, Lcex;->g:[Z

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([ZZ)V

    .line 84
    iget-object v0, p0, Lcex;->e:Lcey;

    invoke-virtual {v0}, Lcey;->c()V

    .line 85
    iget-object v0, p0, Lcex;->f:Lcey;

    invoke-virtual {v0}, Lcey;->c()V

    .line 86
    iget-object v0, p0, Lcex;->f:Lcey;

    invoke-virtual {v0, v1}, Lcey;->a(I)V

    .line 87
    :goto_0
    iget-object v0, p0, Lcex;->f:Lcey;

    invoke-virtual {v0}, Lcey;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 88
    iget-object v0, p0, Lcex;->f:Lcey;

    invoke-virtual {v0}, Lcey;->a()I

    move-result v2

    move v0, v1

    .line 91
    :goto_1
    iget v4, p0, Lcex;->a:I

    if-ge v0, v4, :cond_7

    iget-object v4, p0, Lcex;->d:[[I

    aget-object v4, v4, v2

    aget v4, v4, v0

    if-eqz v4, :cond_7

    .line 92
    iget-object v4, p0, Lcex;->d:[[I

    aget-object v4, v4, v2

    aget v5, v4, v0

    .line 93
    iget-object v4, p0, Lcex;->g:[Z

    aget-boolean v4, v4, v5

    if-nez v4, :cond_6

    iget-object v4, p0, Lcex;->i:[[F

    aget-object v4, v4, v2

    aget v4, v4, v5

    iget-object v6, p0, Lcex;->c:[[F

    aget-object v6, v6, v2

    aget v6, v6, v5

    sub-float/2addr v4, v6

    cmpl-float v4, v4, v7

    if-lez v4, :cond_2

    move v4, v3

    :goto_2
    if-eqz p1, :cond_0

    iget-object v6, p0, Lcex;->i:[[F

    aget-object v6, v6, v2

    aget v6, v6, v5

    cmpl-float v6, v6, v7

    if-lez v6, :cond_3

    if-eqz v4, :cond_3

    move v4, v3

    :cond_0
    :goto_3
    if-eqz v4, :cond_6

    .line 94
    iget v4, p0, Lcex;->b:I

    if-ne v5, v4, :cond_5

    .line 96
    iget-object v0, p0, Lcex;->e:Lcey;

    invoke-virtual {v0, v5}, Lcey;->a(I)V

    move v0, v2

    .line 98
    :goto_4
    iget-object v1, p0, Lcex;->e:Lcey;

    invoke-virtual {v1, v0}, Lcey;->a(I)V

    .line 99
    if-nez v0, :cond_4

    move v1, v3

    .line 111
    :cond_1
    return v1

    :cond_2
    move v4, v1

    .line 93
    goto :goto_2

    :cond_3
    move v4, v1

    goto :goto_3

    .line 102
    :cond_4
    iget-object v1, p0, Lcex;->h:[I

    aget v0, v1, v0

    goto :goto_4

    .line 105
    :cond_5
    iget-object v4, p0, Lcex;->h:[I

    aput v2, v4, v5

    .line 106
    iget-object v4, p0, Lcex;->f:Lcey;

    invoke-virtual {v4, v5}, Lcey;->a(I)V

    .line 91
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 109
    :cond_7
    iget-object v0, p0, Lcex;->g:[Z

    aput-boolean v3, v0, v2

    goto :goto_0
.end method


# virtual methods
.method public a([[F)F
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 38
    array-length v0, p1

    const-string v2, "capacity.length"

    iget v4, p0, Lcex;->a:I

    const-string v5, "mNumVertices"

    invoke-static {v0, v2, v4, v5}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 39
    aget-object v0, p1, v1

    array-length v0, v0

    const-string v2, "capacity[0].length"

    iget v4, p0, Lcex;->a:I

    const-string v5, "mNumVertices"

    invoke-static {v0, v2, v4, v5}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 41
    iput-object p1, p0, Lcex;->i:[[F

    .line 42
    iget-object v2, p0, Lcex;->d:[[I

    array-length v4, v2

    move v0, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v2, v0

    invoke-static {v5, v1}, Ljava/util/Arrays;->fill([II)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v0, v1

    :goto_1
    iget v2, p0, Lcex;->a:I

    if-ge v0, v2, :cond_4

    iget v2, p0, Lcex;->a:I

    add-int/lit8 v2, v2, -0x1

    move v4, v2

    move v2, v1

    :goto_2
    if-lez v4, :cond_3

    iget-object v5, p0, Lcex;->i:[[F

    aget-object v5, v5, v0

    aget v5, v5, v4

    cmpl-float v5, v5, v3

    if-gtz v5, :cond_1

    iget-object v5, p0, Lcex;->i:[[F

    aget-object v5, v5, v4

    aget v5, v5, v0

    cmpl-float v5, v5, v3

    if-lez v5, :cond_2

    :cond_1
    iget-object v5, p0, Lcex;->d:[[I

    aget-object v5, v5, v0

    aput v4, v5, v2

    add-int/lit8 v2, v2, 0x1

    :cond_2
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 43
    :cond_4
    iget-object v2, p0, Lcex;->c:[[F

    array-length v4, v2

    move v0, v1

    :goto_3
    if-ge v0, v4, :cond_5

    aget-object v5, v2, v0

    .line 44
    invoke-static {v5, v3}, Ljava/util/Arrays;->fill([FF)V

    .line 43
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_5
    move v0, v3

    .line 50
    :goto_4
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcex;->a(Z)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 51
    invoke-direct {p0}, Lcex;->a()F

    move-result v2

    add-float/2addr v0, v2

    goto :goto_4

    .line 55
    :cond_6
    :goto_5
    invoke-direct {p0, v1}, Lcex;->a(Z)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 56
    invoke-direct {p0}, Lcex;->a()F

    move-result v2

    add-float/2addr v0, v2

    goto :goto_5

    .line 59
    :cond_7
    return v0
.end method
