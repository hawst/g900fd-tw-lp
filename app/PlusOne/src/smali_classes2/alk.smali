.class public final Lalk;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final i:Ljava/lang/String;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmu;",
            ">;"
        }
    .end annotation
.end field

.field public final h:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lalk;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lalk;->i:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lall;)V
    .locals 2

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    iget v0, p1, Lall;->a:I

    iput v0, p0, Lalk;->a:I

    .line 113
    iget v0, p1, Lall;->b:I

    iput v0, p0, Lalk;->b:I

    .line 114
    iget v0, p1, Lall;->c:I

    iput v0, p0, Lalk;->c:I

    .line 115
    iget v0, p1, Lall;->d:I

    iput v0, p0, Lalk;->d:I

    .line 116
    iget v0, p1, Lall;->e:I

    iput v0, p0, Lalk;->e:I

    .line 117
    iget v0, p1, Lall;->f:I

    iput v0, p0, Lalk;->f:I

    .line 118
    iget v0, p1, Lall;->g:I

    .line 119
    iget-object v0, p1, Lall;->h:Ljava/util/List;

    invoke-static {v0}, Lcfi;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lalk;->g:Ljava/util/List;

    .line 120
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p1, Lall;->i:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lalk;->h:Ljava/util/Set;

    .line 121
    iget-object v0, p1, Lall;->j:Ljava/util/List;

    .line 122
    return-void
.end method

.method public static a()Lall;
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lall;

    invoke-direct {v0}, Lall;-><init>()V

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lalk;->i:Ljava/lang/String;

    return-object v0
.end method
