.class public final Libk;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmig;",
        "Lmih;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Libb;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/util/ArrayList;Ljava/util/ArrayList;IJI)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "II",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;IJI)V"
        }
    .end annotation

    .prologue
    .line 83
    new-instance v2, Lkfo;

    invoke-direct {v2, p1, p2}, Lkfo;-><init>(Landroid/content/Context;I)V

    const-string v3, "recordsuggestion"

    new-instance v4, Lmig;

    invoke-direct {v4}, Lmig;-><init>()V

    new-instance v5, Lmih;

    invoke-direct {v5}, Lmih;-><init>()V

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 85
    new-instance v1, Libb;

    move v2, p6

    move-object v3, p4

    move-object v4, p5

    move v5, p3

    move-wide/from16 v6, p7

    move/from16 v8, p9

    invoke-direct/range {v1 .. v8}, Libb;-><init>(ILjava/util/ArrayList;Ljava/util/ArrayList;IJI)V

    iput-object v1, p0, Libk;->a:Libb;

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILibb;)V
    .locals 6

    .prologue
    .line 35
    new-instance v2, Lkfo;

    invoke-direct {v2, p1, p2}, Lkfo;-><init>(Landroid/content/Context;I)V

    const-string v3, "recordsuggestion"

    new-instance v4, Lmig;

    invoke-direct {v4}, Lmig;-><init>()V

    new-instance v5, Lmih;

    invoke-direct {v5}, Lmih;-><init>()V

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 37
    iput-object p3, p0, Libk;->a:Libb;

    .line 39
    return-void
.end method


# virtual methods
.method protected a(Lmig;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 102
    iget-object v1, p0, Libk;->a:Libb;

    if-nez v1, :cond_0

    .line 145
    :goto_0
    return-void

    .line 106
    :cond_0
    new-instance v1, Lojr;

    invoke-direct {v1}, Lojr;-><init>()V

    iput-object v1, p1, Lmig;->a:Lojr;

    .line 107
    iget-object v3, p1, Lmig;->a:Lojr;

    .line 109
    new-instance v1, Loiw;

    invoke-direct {v1}, Loiw;-><init>()V

    iput-object v1, v3, Lojr;->a:Loiw;

    .line 110
    iget-object v1, v3, Lojr;->a:Loiw;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Loiw;->c:Ljava/lang/Boolean;

    .line 111
    iget-object v1, v3, Lojr;->a:Loiw;

    iget-object v2, p0, Libk;->a:Libb;

    invoke-virtual {v2}, Libb;->a()I

    move-result v2

    iput v2, v1, Loiw;->d:I

    .line 112
    iget-object v1, v3, Lojr;->a:Loiw;

    iget-object v2, p0, Libk;->a:Libb;

    invoke-virtual {v2}, Libb;->d()I

    move-result v2

    iput v2, v1, Loiw;->b:I

    .line 113
    iget-object v1, v3, Lojr;->a:Loiw;

    iget-object v2, p0, Libk;->a:Libb;

    invoke-virtual {v2}, Libb;->e()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Loiw;->e:Ljava/lang/Long;

    .line 114
    iget-object v1, v3, Lojr;->a:Loiw;

    iget-object v2, p0, Libk;->a:Libb;

    invoke-virtual {v2}, Libb;->f()I

    move-result v2

    iput v2, v1, Loiw;->f:I

    .line 118
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 120
    iget-object v1, p0, Libk;->a:Libb;

    invoke-virtual {v1}, Libb;->b()Ljava/util/ArrayList;

    move-result-object v5

    .line 121
    iget-object v1, p0, Libk;->a:Libb;

    invoke-virtual {v1}, Libb;->c()Ljava/util/ArrayList;

    move-result-object v6

    .line 123
    if-eqz v5, :cond_3

    if-eqz v6, :cond_3

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    if-ne v1, v2, :cond_3

    move v2, v0

    .line 124
    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 125
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 126
    invoke-interface {v6, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 128
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 129
    new-instance v7, Loit;

    invoke-direct {v7}, Loit;-><init>()V

    .line 130
    invoke-static {v0}, Ljpu;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 131
    iput-object v1, v7, Loit;->d:Ljava/lang/String;

    .line 132
    if-eqz v8, :cond_2

    .line 133
    iput-object v8, v7, Loit;->b:Ljava/lang/String;

    .line 134
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 124
    :cond_1
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 135
    :cond_2
    const-string v1, "e:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 136
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Loit;->c:Ljava/lang/String;

    .line 137
    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 143
    :cond_3
    iget-object v1, v3, Lojr;->a:Loiw;

    .line 144
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Loit;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Loit;

    iput-object v0, v1, Loiw;->a:[Loit;

    goto/16 :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Lmig;

    invoke-virtual {p0, p1}, Libk;->a(Lmig;)V

    return-void
.end method

.method public i()Libb;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Libk;->a:Libb;

    return-object v0
.end method
