.class public final Lbxt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbok;",
            ">;"
        }
    .end annotation
.end field

.field b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbol;",
            ">;"
        }
    .end annotation
.end field

.field c:Lbor;

.field d:J

.field e:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbxt;->a:Ljava/util/List;

    .line 44
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lbxt;->b:Ljava/util/Map;

    .line 45
    new-instance v0, Lbor;

    invoke-direct {v0}, Lbor;-><init>()V

    iput-object v0, p0, Lbxt;->c:Lbor;

    .line 47
    const-wide/32 v0, 0x1e8480

    iput-wide v0, p0, Lbxt;->e:J

    return-void
.end method


# virtual methods
.method public a()Lbxs;
    .locals 1

    .prologue
    .line 76
    new-instance v0, Lbxs;

    invoke-direct {v0, p0}, Lbxs;-><init>(Lbxt;)V

    return-object v0
.end method

.method public a(J)Lbxt;
    .locals 3

    .prologue
    .line 60
    const-string v0, "targetDurationUs"

    invoke-static {p1, p2, v0}, Lcec;->b(JLjava/lang/CharSequence;)J

    move-result-wide v0

    iput-wide v0, p0, Lbxt;->d:J

    .line 61
    return-object p0
.end method

.method public a(Lbor;)Lbxt;
    .locals 2

    .prologue
    .line 65
    const-string v0, "constraints"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbor;

    iput-object v0, p0, Lbxt;->c:Lbor;

    .line 66
    return-object p0
.end method

.method public a(Ljava/util/List;)Lbxt;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lbml;",
            ">;)",
            "Lbxt;"
        }
    .end annotation

    .prologue
    .line 50
    const-string v0, "media"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbml;

    invoke-static {v0}, Lbok;->a(Lbml;)Lbok;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iput-object v1, p0, Lbxt;->a:Ljava/util/List;

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbxt;->b:Ljava/util/Map;

    .line 52
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbml;

    .line 53
    iget-object v2, p0, Lbxt;->b:Ljava/util/Map;

    .line 54
    invoke-interface {v0}, Lbml;->a()Ljeg;

    move-result-object v3

    invoke-interface {v0}, Lbml;->c()Lbol;

    move-result-object v0

    .line 53
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 56
    :cond_1
    return-object p0
.end method

.method public b(J)Lbxt;
    .locals 3

    .prologue
    .line 70
    const-string v0, "singlePhotoDurationUs"

    invoke-static {p1, p2, v0}, Lcec;->a(JLjava/lang/CharSequence;)J

    move-result-wide v0

    iput-wide v0, p0, Lbxt;->e:J

    .line 72
    return-object p0
.end method
