.class public final Lcit;
.super Lcjy;
.source "PG"


# instance fields
.field private final W:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

.field private X:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcjy;-><init>()V

    .line 46
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    iput-object v0, p0, Lcit;->W:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 47
    const/4 v0, -0x1

    iput v0, p0, Lcit;->X:I

    .line 296
    return-void
.end method

.method static synthetic a(Lcit;)Lcom/google/android/libraries/snapseed/ui/views/CropImageView;
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcit;->an()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;

    return-object v0
.end method

.method static synthetic a(Lcit;I)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcit;->i(I)V

    return-void
.end method

.method private at()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 267
    move v0, v1

    :goto_0
    iget-object v2, p0, Lcit;->W:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    const/4 v2, 0x3

    if-ge v0, v2, :cond_2

    .line 268
    iget-object v2, p0, Lcit;->W:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    aget-object v3, v2, v0

    .line 269
    if-eqz v3, :cond_0

    .line 270
    iget v2, p0, Lcit;->X:I

    if-ne v0, v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v3, v2}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setSelected(Z)V

    .line 267
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v2, v1

    .line 270
    goto :goto_1

    .line 273
    :cond_2
    return-void
.end method

.method private i(I)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 232
    invoke-virtual {p0}, Lcit;->an()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;

    .line 233
    if-nez v0, :cond_0

    .line 234
    const-string v0, "CropFragment"

    const-string v1, "Illegal update preview image request: the view is not yet initialized!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    :goto_0
    return-void

    .line 239
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 256
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid aspect ratio index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 242
    :pswitch_0
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a(Z)V

    move v0, v1

    .line 259
    :goto_1
    const/16 v2, 0x2a

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v2, v0, v1}, Lcit;->a(ILjava/lang/Object;Z)Z

    .line 261
    iput p1, p0, Lcit;->X:I

    .line 263
    invoke-direct {p0}, Lcit;->at()V

    goto :goto_0

    .line 246
    :pswitch_1
    const/4 v2, 0x2

    .line 247
    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->b()F

    move-result v4

    invoke-virtual {v0, v4, v3}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a(FZ)V

    move v0, v2

    .line 248
    goto :goto_1

    .line 252
    :pswitch_2
    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a(FZ)V

    move v0, v3

    .line 253
    goto :goto_1

    .line 239
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected B_()Z
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method protected V()V
    .locals 2

    .prologue
    .line 219
    new-instance v0, Lcix;

    invoke-direct {v0, p0}, Lcix;-><init>(Lcit;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcix;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 220
    return-void
.end method

.method protected a(Lckp;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 173
    iget-object v6, p0, Lcit;->W:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    const/4 v7, 0x2

    const v1, 0x7f020389

    const v2, 0x7f020388

    const v0, 0x7f0a0044

    .line 177
    invoke-virtual {p0, v0}, Lcit;->e_(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lciu;

    invoke-direct {v5, p0}, Lciu;-><init>(Lcit;)V

    move-object v0, p1

    .line 173
    invoke-virtual/range {v0 .. v5}, Lckp;->a(IIILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    move-result-object v0

    aput-object v0, v6, v7

    .line 185
    iget-object v6, p0, Lcit;->W:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    const/4 v7, 0x1

    const v1, 0x7f02038f

    const v2, 0x7f02038e

    const v0, 0x7f0a0045

    .line 189
    invoke-virtual {p0, v0}, Lcit;->e_(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lciv;

    invoke-direct {v5, p0}, Lciv;-><init>(Lcit;)V

    move-object v0, p1

    .line 185
    invoke-virtual/range {v0 .. v5}, Lckp;->a(IIILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    move-result-object v0

    aput-object v0, v6, v7

    .line 197
    iget-object v6, p0, Lcit;->W:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    const v1, 0x7f02038c

    const v2, 0x7f02038b

    const v0, 0x7f0a0046

    .line 201
    invoke-virtual {p0, v0}, Lcit;->e_(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lciw;

    invoke-direct {v5, p0}, Lciw;-><init>(Lcit;)V

    move-object v0, p1

    .line 197
    invoke-virtual/range {v0 .. v5}, Lckp;->a(IIILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    move-result-object v0

    aput-object v0, v6, v3

    .line 209
    invoke-direct {p0}, Lcit;->at()V

    .line 210
    return-void
.end method

.method protected a(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V
    .locals 0

    .prologue
    .line 155
    return-void
.end method

.method protected aa()Landroid/view/View;
    .locals 2

    .prologue
    .line 85
    new-instance v0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;

    invoke-virtual {p0}, Lcit;->n()Lz;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected ab()V
    .locals 0

    .prologue
    .line 90
    return-void
.end method

.method protected ac()V
    .locals 0

    .prologue
    .line 94
    return-void
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 70
    new-instance v0, Lhmk;

    sget-object v1, Lonc;->g:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method protected ad()Lhbn;
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return-object v0
.end method

.method protected ae()Lhbp;
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return-object v0
.end method

.method protected af()Lhbj;
    .locals 1

    .prologue
    .line 108
    const/4 v0, 0x0

    return-object v0
.end method

.method protected ag()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 159
    invoke-virtual {p0}, Lcit;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 160
    const v1, 0x7f0d0023

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 161
    const v2, 0x7f0d0024

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 163
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 165
    iput v1, v2, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 166
    iput v0, v2, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 167
    const/16 v0, 0x11

    iput v0, v2, Landroid/widget/FrameLayout$LayoutParams;->gravity:I

    .line 168
    invoke-virtual {p0}, Lcit;->an()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 169
    return-void
.end method

.method public b(ILjava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 214
    const-string v0, ""

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 75
    const/4 v0, 0x6

    return v0
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 224
    const/4 v0, 0x0

    return v0
.end method

.method protected y_()V
    .locals 10

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 113
    invoke-virtual {p0}, Lcit;->ah()Lchp;

    move-result-object v0

    invoke-virtual {v0}, Lchp;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 115
    invoke-virtual {p0}, Lcit;->ah()Lchp;

    move-result-object v2

    invoke-virtual {v2}, Lchp;->h()Lcom/google/android/libraries/photoeditor/core/FilterChain;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getPostRotation()I

    move-result v2

    .line 114
    invoke-static {v2}, Lhbg;->a(I)F

    move-result v2

    .line 116
    const/4 v3, 0x0

    cmpl-float v3, v2, v3

    if-eqz v3, :cond_1

    .line 117
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 119
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v6

    .line 118
    invoke-virtual {v5, v2, v3, v4}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 121
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    move v2, v1

    move v6, v1

    .line 120
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v2, v0

    .line 124
    :goto_0
    invoke-virtual {p0}, Lcit;->an()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;

    .line 125
    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a(Landroid/graphics/Bitmap;)V

    .line 129
    invoke-virtual {p0}, Lcit;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v4

    .line 131
    const/16 v3, 0x2a

    .line 132
    invoke-virtual {v4, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v3

    .line 131
    packed-switch v3, :pswitch_data_0

    const/4 v3, -0x1

    .line 133
    :goto_1
    invoke-direct {p0, v3}, Lcit;->i(I)V

    .line 135
    const/16 v3, 0x2b

    invoke-virtual {v4, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v3

    .line 136
    const/16 v5, 0x2d

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v5

    .line 137
    const/16 v6, 0x2c

    invoke-virtual {v4, v6}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v6

    .line 138
    const/16 v7, 0x2e

    invoke-virtual {v4, v7}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v4

    .line 140
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    .line 141
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 143
    int-to-float v8, v7

    mul-float/2addr v8, v3

    invoke-static {v8}, Ljava/lang/Math;->round(F)I

    move-result v8

    int-to-float v9, v2

    mul-float/2addr v9, v5

    invoke-static {v9}, Ljava/lang/Math;->round(F)I

    move-result v9

    int-to-float v7, v7

    sub-float v3, v6, v3

    mul-float/2addr v3, v7

    .line 144
    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    int-to-float v2, v2

    sub-float/2addr v4, v5

    mul-float/2addr v2, v4

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    .line 143
    invoke-virtual {v0, v8, v9, v3, v2}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a(IIII)V

    .line 149
    :goto_2
    invoke-virtual {p0, v1}, Lcit;->k(Z)V

    .line 150
    invoke-virtual {p0}, Lcit;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Lhdn;->b(Landroid/app/Activity;)V

    .line 151
    return-void

    :pswitch_0
    move v3, v1

    .line 131
    goto :goto_1

    :pswitch_1
    const/4 v3, 0x1

    goto :goto_1

    :pswitch_2
    const/4 v3, 0x2

    goto :goto_1

    .line 146
    :cond_0
    const-string v0, "CropFragment"

    const-string v2, "Illegal update preview image request: the view is not yet initialized!"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_1
    move-object v2, v0

    goto :goto_0

    .line 131
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public z()V
    .locals 4

    .prologue
    .line 54
    invoke-virtual {p0}, Lcit;->an()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;

    .line 55
    if-eqz v0, :cond_0

    .line 56
    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a()Landroid/graphics/RectF;

    move-result-object v0

    .line 58
    invoke-virtual {p0}, Lcit;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    .line 59
    const/16 v2, 0x2b

    iget v3, v0, Landroid/graphics/RectF;->left:F

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterFloat(IF)Z

    .line 60
    const/16 v2, 0x2d

    iget v3, v0, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterFloat(IF)Z

    .line 61
    const/16 v2, 0x2c

    iget v3, v0, Landroid/graphics/RectF;->right:F

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterFloat(IF)Z

    .line 62
    const/16 v2, 0x2e

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v1, v2, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterFloat(IF)Z

    .line 65
    :cond_0
    invoke-super {p0}, Lcjy;->z()V

    .line 66
    return-void
.end method
