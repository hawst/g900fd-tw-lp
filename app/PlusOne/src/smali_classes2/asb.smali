.class final Lasb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lasd;


# instance fields
.field private synthetic a:Laru;


# direct methods
.method constructor <init>(Laru;)V
    .locals 0

    .prologue
    .line 356
    iput-object p1, p0, Lasb;->a:Laru;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 407
    iget-object v0, p0, Lasb;->a:Laru;

    iget-object v0, v0, Laru;->a:Lawl;

    invoke-interface {v0}, Lawl;->P()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 408
    iget-object v0, p0, Lasb;->a:Laru;

    iget-object v0, v0, Laru;->a:Lawl;

    invoke-interface {v0}, Lawl;->R()V

    .line 410
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 402
    iget-object v0, p0, Lasb;->a:Laru;

    iget-object v0, v0, Laru;->d:Lasa;

    invoke-interface {v0, p1}, Lasa;->a(I)V

    .line 403
    return-void
.end method

.method public a(II)V
    .locals 2

    .prologue
    .line 379
    if-ne p1, p2, :cond_0

    .line 386
    :goto_0
    return-void

    .line 382
    :cond_0
    new-instance v1, Lbpf;

    invoke-direct {v1, p1, p2}, Lbpf;-><init>(II)V

    .line 383
    iget-object v0, p0, Lasb;->a:Laru;

    iget-object v0, v0, Laru;->g:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lasc;

    invoke-interface {v0, v1}, Lasc;->a(Lbph;)V

    .line 384
    iget-object v0, p0, Lasb;->a:Laru;

    iget-object v0, v0, Laru;->a:Lawl;

    invoke-interface {v0, v1}, Lawl;->a(Lbph;)V

    .line 385
    iget-object v0, p0, Lasb;->a:Laru;

    iget-object v0, v0, Laru;->c:Lcdu;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcdu;->b(I)V

    goto :goto_0
.end method

.method public a(ILbmd;)V
    .locals 2

    .prologue
    .line 390
    iget-object v0, p0, Lasb;->a:Laru;

    iget-object v0, v0, Laru;->a:Lawl;

    invoke-interface {v0}, Lawl;->F()Lboi;

    move-result-object v0

    .line 391
    if-eqz v0, :cond_0

    .line 392
    iget-object v1, p2, Lbmd;->e:Ljeg;

    invoke-virtual {v0, v1}, Lboi;->a(Ljeg;)Lbon;

    move-result-object v0

    .line 393
    iget-object v1, p0, Lasb;->a:Laru;

    iget-object v1, v1, Laru;->b:Lbze;

    invoke-virtual {v1, v0}, Lbze;->b(Lbon;)Ljava/util/List;

    move-result-object v0

    .line 394
    new-instance v1, Lbpk;

    invoke-direct {v1, p1, v0}, Lbpk;-><init>(ILjava/util/Collection;)V

    .line 395
    iget-object v0, p0, Lasb;->a:Laru;

    iget-object v0, v0, Laru;->g:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lasc;

    invoke-interface {v0, v1}, Lasc;->a(Lbph;)V

    .line 396
    iget-object v0, p0, Lasb;->a:Laru;

    iget-object v0, v0, Laru;->a:Lawl;

    invoke-interface {v0, v1}, Lawl;->a(Lbph;)V

    .line 398
    :cond_0
    return-void
.end method

.method public a(Landroid/util/SparseArray;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lbmd;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 359
    invoke-virtual {p1, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    .line 360
    iget-object v0, p0, Lasb;->a:Laru;

    iget-object v0, v0, Laru;->a:Lawl;

    invoke-interface {v0, v3}, Lawl;->f(I)V

    .line 361
    iget-object v0, p0, Lasb;->a:Laru;

    iget-object v0, v0, Laru;->g:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lasc;

    invoke-interface {v0, v3}, Lasc;->d_(I)V

    .line 365
    const v3, 0x7fffffff

    .line 366
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v6, v0

    move v0, v3

    move v3, v6

    :goto_0
    if-ltz v3, :cond_1

    .line 367
    invoke-virtual {p1, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    .line 368
    if-gt v4, v0, :cond_0

    move v0, v1

    :goto_1
    const-string v5, "Implementation error: SparseArray keys not sorted."

    invoke-static {v0, v5}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 370
    invoke-virtual {p1, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    new-instance v5, Lbpd;

    invoke-direct {v5, v4, v0}, Lbpd;-><init>(ILbmd;)V

    .line 371
    iget-object v0, p0, Lasb;->a:Laru;

    iget-object v0, v0, Laru;->a:Lawl;

    invoke-interface {v0, v5}, Lawl;->a(Lbph;)V

    .line 366
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    move v0, v4

    goto :goto_0

    :cond_0
    move v0, v2

    .line 368
    goto :goto_1

    .line 374
    :cond_1
    iget-object v0, p0, Lasb;->a:Laru;

    iget-object v0, v0, Laru;->c:Lcdu;

    invoke-virtual {v0, v1}, Lcdu;->b(I)V

    .line 375
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Lasb;->a:Laru;

    iget-object v0, v0, Laru;->a:Lawl;

    invoke-interface {v0, p1}, Lawl;->a(Ljava/lang/String;)V

    .line 437
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 414
    iget-object v0, p0, Lasb;->a:Laru;

    iget-object v0, v0, Laru;->a:Lawl;

    invoke-interface {v0}, Lawl;->P()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 415
    iget-object v0, p0, Lasb;->a:Laru;

    iget-object v0, v0, Laru;->a:Lawl;

    invoke-interface {v0}, Lawl;->S()V

    .line 417
    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lasb;->a:Laru;

    iget-object v0, v0, Laru;->d:Lasa;

    invoke-interface {v0, p1}, Lasa;->b(I)V

    .line 442
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 421
    iget-object v0, p0, Lasb;->a:Laru;

    iget-object v0, v0, Laru;->e:Lapl;

    invoke-interface {v0}, Lapl;->a()V

    .line 422
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lasb;->a:Laru;

    iget-object v0, v0, Laru;->a:Lawl;

    invoke-interface {v0}, Lawl;->T()V

    .line 427
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lasb;->a:Laru;

    iget-object v0, v0, Laru;->a:Lawl;

    invoke-interface {v0}, Lawl;->U()V

    .line 432
    return-void
.end method
