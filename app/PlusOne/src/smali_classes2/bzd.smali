.class final Lbzd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lbza;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/text/Collator;

.field private final b:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Ljava/util/Locale;Landroid/content/res/Resources;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    const-string v0, "locale"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/Collator;->getInstance(Ljava/util/Locale;)Ljava/text/Collator;

    move-result-object v0

    iput-object v0, p0, Lbzd;->a:Ljava/text/Collator;

    .line 111
    const-string v0, "resources"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, Lbzd;->b:Landroid/content/res/Resources;

    .line 112
    return-void
.end method


# virtual methods
.method public a(Lbza;Lbza;)I
    .locals 4

    .prologue
    .line 116
    if-ne p1, p2, :cond_0

    .line 117
    const/4 v0, 0x0

    .line 125
    :goto_0
    return v0

    .line 119
    :cond_0
    sget-object v0, Lbza;->a:Lbza;

    if-ne p1, v0, :cond_1

    .line 120
    const/4 v0, -0x1

    goto :goto_0

    .line 122
    :cond_1
    sget-object v0, Lbza;->a:Lbza;

    if-ne p2, v0, :cond_2

    .line 123
    const/4 v0, 0x1

    goto :goto_0

    .line 125
    :cond_2
    iget-object v0, p0, Lbzd;->a:Ljava/text/Collator;

    iget-object v1, p0, Lbzd;->b:Landroid/content/res/Resources;

    iget v2, p1, Lbza;->n:I

    .line 126
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbzd;->b:Landroid/content/res/Resources;

    iget v3, p2, Lbza;->n:I

    .line 127
    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 125
    invoke-virtual {v0, v1, v2}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 104
    check-cast p1, Lbza;

    check-cast p2, Lbza;

    invoke-virtual {p0, p1, p2}, Lbzd;->a(Lbza;Lbza;)I

    move-result v0

    return v0
.end method
