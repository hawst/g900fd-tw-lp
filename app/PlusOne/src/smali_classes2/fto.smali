.class public final Lfto;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lljh;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# instance fields
.field private a:I

.field private b:Lfrg;

.field private c:Landroid/graphics/Rect;

.field private d:Landroid/graphics/Paint;

.field private e:[Lfts;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/social/media/ui/MediaView;",
            ">;"
        }
    .end annotation
.end field

.field private g:I

.field private h:Landroid/view/ViewPropertyAnimator;

.field private i:Landroid/animation/AnimatorSet;

.field private final j:Landroid/os/Handler;

.field private k:I

.field private l:Ljava/lang/Runnable;

.field private m:Lftt;

.field private n:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfto;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 104
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lfto;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 111
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    iput v2, p0, Lfto;->a:I

    .line 51
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfto;->c:Landroid/graphics/Rect;

    .line 52
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lfto;->d:Landroid/graphics/Paint;

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lfto;->f:Ljava/util/List;

    .line 59
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lfto;->i:Landroid/animation/AnimatorSet;

    .line 61
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lfto;->j:Landroid/os/Handler;

    .line 62
    const/4 v0, -0x1

    iput v0, p0, Lfto;->k:I

    .line 64
    new-instance v0, Lftp;

    invoke-direct {v0, p0}, Lftp;-><init>(Lfto;)V

    iput-object v0, p0, Lfto;->l:Ljava/lang/Runnable;

    .line 73
    iput v2, p0, Lfto;->n:I

    .line 112
    return-void
.end method

.method private a(Landroid/view/View;Lfrf;)V
    .locals 13

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    .line 245
    invoke-virtual {p0}, Lfto;->getWidth()I

    move-result v0

    .line 246
    invoke-virtual {p0}, Lfto;->getHeight()I

    move-result v1

    .line 247
    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v2

    .line 248
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 252
    iget-object v4, p2, Lfrf;->a:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget-object v5, p2, Lfrf;->a:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    sub-float/2addr v4, v5

    int-to-float v5, v2

    mul-float/2addr v4, v5

    .line 253
    iget-object v5, p2, Lfrf;->a:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    iget-object v6, p2, Lfrf;->a:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v5, v6

    int-to-float v6, v3

    mul-float/2addr v5, v6

    .line 254
    iget-object v6, p2, Lfrf;->a:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    iget-object v7, p2, Lfrf;->a:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    add-float/2addr v6, v7

    int-to-float v7, v2

    mul-float/2addr v6, v7

    div-float/2addr v6, v12

    .line 255
    iget-object v7, p2, Lfrf;->a:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    iget-object v8, p2, Lfrf;->a:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    add-float/2addr v7, v8

    int-to-float v8, v3

    mul-float/2addr v7, v8

    div-float/2addr v7, v12

    .line 256
    int-to-float v8, v0

    div-float v4, v8, v4

    int-to-float v8, v1

    div-float v5, v8, v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 257
    iget-object v5, p2, Lfrf;->b:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    iget-object v8, p2, Lfrf;->b:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    sub-float/2addr v5, v8

    int-to-float v8, v2

    mul-float/2addr v5, v8

    .line 258
    iget-object v8, p2, Lfrf;->b:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    iget-object v9, p2, Lfrf;->b:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->top:F

    sub-float/2addr v8, v9

    int-to-float v9, v3

    mul-float/2addr v8, v9

    .line 259
    iget-object v9, p2, Lfrf;->b:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->right:F

    iget-object v10, p2, Lfrf;->b:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->left:F

    add-float/2addr v9, v10

    int-to-float v10, v2

    mul-float/2addr v9, v10

    div-float/2addr v9, v12

    .line 260
    iget-object v10, p2, Lfrf;->b:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    iget-object v11, p2, Lfrf;->b:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->top:F

    add-float/2addr v10, v11

    int-to-float v11, v3

    mul-float/2addr v10, v11

    div-float/2addr v10, v12

    .line 261
    int-to-float v11, v0

    div-float v5, v11, v5

    int-to-float v11, v1

    div-float v8, v11, v8

    invoke-static {v5, v8}, Ljava/lang/Math;->max(FF)F

    move-result v5

    .line 263
    sub-int/2addr v0, v2

    int-to-float v0, v0

    div-float/2addr v0, v12

    .line 264
    sub-int/2addr v1, v3

    int-to-float v1, v1

    div-float/2addr v1, v12

    .line 267
    div-int/lit8 v8, v2, 0x2

    int-to-float v8, v8

    sub-float v6, v8, v6

    mul-float/2addr v6, v4

    add-float/2addr v6, v0

    .line 268
    div-int/lit8 v8, v3, 0x2

    int-to-float v8, v8

    sub-float v7, v8, v7

    mul-float/2addr v7, v4

    add-float/2addr v7, v1

    .line 269
    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    sub-float/2addr v2, v9

    mul-float/2addr v2, v5

    add-float/2addr v0, v2

    .line 270
    div-int/lit8 v2, v3, 0x2

    int-to-float v2, v2

    sub-float/2addr v2, v10

    mul-float/2addr v2, v5

    add-float/2addr v1, v2

    .line 271
    invoke-virtual {p1, v4}, Landroid/view/View;->setScaleX(F)V

    .line 272
    invoke-virtual {p1, v4}, Landroid/view/View;->setScaleY(F)V

    .line 273
    invoke-virtual {p1, v6}, Landroid/view/View;->setTranslationX(F)V

    .line 274
    invoke-virtual {p1, v7}, Landroid/view/View;->setTranslationY(F)V

    .line 276
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 277
    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 278
    invoke-virtual {v2, v5}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    .line 279
    invoke-virtual {v2, v0}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 280
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x2710

    .line 281
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lfto;->h:Landroid/view/ViewPropertyAnimator;

    .line 282
    iget-object v0, p0, Lfto;->h:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 283
    return-void
.end method

.method private a(Lfrg;II)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 197
    if-eqz p1, :cond_0

    .line 198
    iget-object v0, p0, Lfto;->c:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v1, p2, p3}, Landroid/graphics/Rect;->set(IIII)V

    .line 199
    iget-object v0, p0, Lfto;->d:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 200
    iget-object v8, p0, Lfto;->d:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/LinearGradient;

    iget-object v1, p1, Lfrg;->a:Lfri;

    iget v1, v1, Lfri;->a:F

    int-to-float v2, p2

    mul-float/2addr v1, v2

    iget-object v2, p1, Lfrg;->a:Lfri;

    iget v2, v2, Lfri;->b:F

    int-to-float v3, p3

    mul-float/2addr v2, v3

    iget-object v3, p1, Lfrg;->a:Lfri;

    iget v3, v3, Lfri;->c:F

    int-to-float v4, p2

    mul-float/2addr v3, v4

    iget-object v4, p1, Lfrg;->a:Lfri;

    iget v4, v4, Lfri;->d:F

    int-to-float v5, p3

    mul-float/2addr v4, v5

    iget v5, p1, Lfrg;->b:I

    iget v6, p1, Lfrg;->c:I

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    invoke-virtual {v8, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 205
    :cond_0
    return-void
.end method

.method static synthetic a(Lfto;)V
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 42
    iget v0, p0, Lfto;->k:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iput v4, p0, Lfto;->k:I

    iget-object v0, p0, Lfto;->f:Ljava/util/List;

    iget v1, p0, Lfto;->k:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    iget-object v1, p0, Lfto;->e:[Lfts;

    iget v2, p0, Lfto;->k:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lfts;->b:Lfrf;

    invoke-direct {p0, v0, v1}, Lfto;->a(Landroid/view/View;Lfrf;)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lfto;->k:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lfto;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    rem-int/2addr v0, v1

    iput v0, p0, Lfto;->k:I

    iget-object v0, p0, Lfto;->f:Ljava/util/List;

    iget v1, p0, Lfto;->k:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    iget-object v1, p0, Lfto;->f:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/media/ui/MediaView;

    iget-object v2, p0, Lfto;->f:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/media/ui/MediaView;

    iget-object v5, p0, Lfto;->e:[Lfts;

    iget v6, p0, Lfto;->k:I

    aget-object v5, v5, v6

    iget-object v5, v5, Lfts;->b:Lfrf;

    invoke-direct {p0, v0, v5}, Lfto;->a(Landroid/view/View;Lfrf;)V

    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->getAlpha()F

    move-result v0

    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v5

    if-eqz v0, :cond_1

    move v0, v3

    :goto_1
    invoke-virtual {p0, v1, v2, v0}, Lfto;->a(Landroid/view/View;Landroid/view/View;Z)V

    goto :goto_0

    :cond_1
    move v0, v4

    goto :goto_1
.end method

.method static synthetic b(Lfto;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lfto;->l:Ljava/lang/Runnable;

    return-object v0
.end method

.method private c(I)Landroid/graphics/Rect;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 230
    invoke-virtual {p0}, Lfto;->getMeasuredWidth()I

    move-result v0

    .line 231
    invoke-virtual {p0}, Lfto;->getMeasuredHeight()I

    move-result v1

    .line 232
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 234
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v3, v3, v4, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 241
    :goto_0
    return-object v0

    .line 236
    :cond_1
    iget-object v0, p0, Lfto;->e:[Lfts;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lfto;->e:[Lfts;

    array-length v0, v0

    if-lt p1, v0, :cond_3

    .line 237
    :cond_2
    const-string v0, "StoryLayoutCoverImageView"

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x2f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid call to getCoverImageScale: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v3, v3, v4, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0

    .line 240
    :cond_3
    iget-object v0, p0, Lfto;->e:[Lfts;

    aget-object v1, v0, p1

    .line 241
    new-instance v0, Landroid/graphics/Rect;

    iget v2, v1, Lfts;->c:I

    iget v1, v1, Lfts;->d:I

    invoke-direct {v0, v3, v3, v2, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    goto :goto_0
.end method

.method static synthetic c(Lfto;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lfto;->j:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic d(Lfto;)I
    .locals 1

    .prologue
    .line 42
    iget v0, p0, Lfto;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lfto;->n:I

    return v0
.end method

.method static synthetic e(Lfto;)Lftt;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lfto;->m:Lftt;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 406
    invoke-virtual {p0}, Lfto;->removeAllViews()V

    .line 407
    iget-object v0, p0, Lfto;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 408
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->c()V

    goto :goto_0

    .line 411
    :cond_0
    iput v2, p0, Lfto;->a:I

    .line 412
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfto;->c:Landroid/graphics/Rect;

    .line 413
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lfto;->d:Landroid/graphics/Paint;

    .line 416
    const/4 v0, -0x1

    iput v0, p0, Lfto;->k:I

    .line 419
    iget-object v0, p0, Lfto;->j:Landroid/os/Handler;

    iget-object v1, p0, Lfto;->l:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 420
    iget-object v0, p0, Lfto;->h:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_1

    .line 421
    iget-object v0, p0, Lfto;->h:Landroid/view/ViewPropertyAnimator;

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 423
    :cond_1
    iget-object v0, p0, Lfto;->i:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 425
    iput v2, p0, Lfto;->n:I

    .line 426
    const/4 v0, 0x0

    iput-object v0, p0, Lfto;->m:Lftt;

    .line 429
    invoke-virtual {p0, v2, v2}, Lfto;->setMeasuredDimension(II)V

    .line 430
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 132
    iput p1, p0, Lfto;->g:I

    .line 133
    return-void
.end method

.method public a(Landroid/view/View;Landroid/view/View;Z)V
    .locals 5

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 286
    if-eqz p3, :cond_0

    move v2, v1

    .line 287
    :goto_0
    if-eqz p3, :cond_1

    .line 288
    :goto_1
    const-string v1, "alpha"

    const/4 v3, 0x2

    new-array v3, v3, [F

    const/4 v4, 0x0

    aput v2, v3, v4

    const/4 v2, 0x1

    aput v0, v3, v2

    invoke-static {p1, v1, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 289
    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 290
    new-instance v1, Lftq;

    invoke-direct {v1, p3, p2}, Lftq;-><init>(ZLandroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 313
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 314
    return-void

    :cond_0
    move v2, v0

    .line 286
    goto :goto_0

    :cond_1
    move v0, v1

    .line 287
    goto :goto_1
.end method

.method public a(Lfrg;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lfto;->b:Lfrg;

    .line 141
    return-void
.end method

.method public a(Lftt;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lfto;->m:Lftt;

    .line 117
    return-void
.end method

.method public a([Lfts;)V
    .locals 2

    .prologue
    .line 121
    array-length v0, p1

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "StoryLayoutCoverImageView requires 1+ cover images"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_0
    array-length v0, p1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 124
    const/4 v0, 0x2

    new-array v0, v0, [Lfts;

    iput-object v0, p0, Lfto;->e:[Lfts;

    .line 125
    iget-object v0, p0, Lfto;->e:[Lfts;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 129
    :goto_0
    return-void

    .line 127
    :cond_1
    iput-object p1, p0, Lfto;->e:[Lfts;

    goto :goto_0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 136
    iput p1, p0, Lfto;->a:I

    .line 137
    return-void
.end method

.method public dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 185
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 187
    iget v0, p0, Lfto;->a:I

    if-eqz v0, :cond_0

    .line 188
    iget v0, p0, Lfto;->a:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 191
    :cond_0
    iget-object v0, p0, Lfto;->b:Lfrg;

    if-eqz v0, :cond_1

    .line 192
    iget-object v0, p0, Lfto;->c:Landroid/graphics/Rect;

    iget-object v1, p0, Lfto;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 194
    :cond_1
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    .line 434
    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    .line 436
    iget-object v0, p0, Lfto;->b:Lfrg;

    invoke-virtual {p0}, Lfto;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lfto;->getHeight()I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lfto;->a(Lfrg;II)V

    .line 437
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 176
    invoke-virtual {p0}, Lfto;->getChildCount()I

    move-result v2

    move v0, v1

    .line 177
    :goto_0
    if-ge v0, v2, :cond_0

    .line 178
    invoke-direct {p0, v0}, Lfto;->c(I)Landroid/graphics/Rect;

    move-result-object v3

    .line 179
    invoke-virtual {p0, v0}, Lfto;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v5

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-virtual {v4, v1, v1, v5, v3}, Landroid/view/View;->layout(IIII)V

    .line 177
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 181
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 13

    .prologue
    .line 145
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 146
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 147
    invoke-virtual {p0, v0, v1}, Lfto;->setMeasuredDimension(II)V

    .line 150
    invoke-virtual {p0}, Lfto;->getChildCount()I

    move-result v0

    if-nez v0, :cond_4

    .line 151
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    const/4 v0, 0x2

    if-ge v2, v0, :cond_3

    iget-object v0, p0, Lfto;->e:[Lfts;

    aget-object v0, v0, v2

    iget-object v1, v0, Lfts;->a:Lizu;

    iget-object v0, p0, Lfto;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v2, v0, :cond_0

    new-instance v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {p0}, Lfto;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->h(Z)V

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->setLayerType(ILandroid/graphics/Paint;)V

    iget-object v3, p0, Lfto;->f:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lfto;->e:[Lfts;

    aget-object v0, v0, v2

    iget-object v0, v0, Lfts;->b:Lfrf;

    iget-object v3, v0, Lfrf;->a:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget-object v4, v0, Lfrf;->b:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    invoke-static {v3, v4}, Ljava/lang/Math;->min(FF)F

    move-result v3

    iget-object v4, v0, Lfrf;->a:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    iget-object v5, v0, Lfrf;->b:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    iget-object v5, v0, Lfrf;->a:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->right:F

    iget-object v6, v0, Lfrf;->b:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->right:F

    invoke-static {v5, v6}, Ljava/lang/Math;->max(FF)F

    move-result v5

    iget-object v6, v0, Lfrf;->a:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    iget-object v7, v0, Lfrf;->b:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    invoke-static {v6, v7}, Ljava/lang/Math;->max(FF)F

    move-result v6

    new-instance v7, Landroid/graphics/RectF;

    invoke-direct {v7, v3, v4, v5, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    sub-float/2addr v5, v3

    sub-float/2addr v6, v4

    iget-object v8, v0, Lfrf;->a:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    sub-float/2addr v8, v3

    div-float/2addr v8, v5

    iget-object v9, v0, Lfrf;->a:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->top:F

    sub-float/2addr v9, v4

    div-float/2addr v9, v6

    iget-object v10, v0, Lfrf;->a:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->right:F

    sub-float/2addr v10, v3

    div-float/2addr v10, v5

    iget-object v11, v0, Lfrf;->a:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v11, v4

    div-float/2addr v11, v6

    new-instance v12, Landroid/graphics/RectF;

    invoke-direct {v12, v8, v9, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v8, v0, Lfrf;->b:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    sub-float/2addr v8, v3

    div-float/2addr v8, v5

    iget-object v9, v0, Lfrf;->b:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->top:F

    sub-float/2addr v9, v4

    div-float/2addr v9, v6

    iget-object v10, v0, Lfrf;->b:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->right:F

    sub-float v3, v10, v3

    div-float/2addr v3, v5

    iget-object v10, v0, Lfrf;->b:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    sub-float v4, v10, v4

    div-float/2addr v4, v6

    new-instance v10, Landroid/graphics/RectF;

    invoke-direct {v10, v8, v9, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p0}, Lfto;->getMeasuredWidth()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, v0, Lfrf;->a:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget-object v8, v0, Lfrf;->a:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    sub-float/2addr v4, v8

    iget-object v8, v0, Lfrf;->b:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->right:F

    iget-object v9, v0, Lfrf;->b:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->left:F

    sub-float/2addr v8, v9

    invoke-static {v4, v8}, Ljava/lang/Math;->max(FF)F

    move-result v4

    div-float v4, v5, v4

    mul-float/2addr v3, v4

    float-to-int v3, v3

    invoke-virtual {p0}, Lfto;->getMeasuredHeight()I

    move-result v4

    int-to-float v4, v4

    iget-object v5, v0, Lfrf;->a:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    iget-object v8, v0, Lfrf;->a:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    sub-float/2addr v5, v8

    iget-object v8, v0, Lfrf;->b:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->bottom:F

    iget-object v0, v0, Lfrf;->b:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    sub-float v0, v8, v0

    invoke-static {v5, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    div-float v0, v6, v0

    mul-float/2addr v0, v4

    float-to-int v4, v0

    iget-object v0, p0, Lfto;->e:[Lfts;

    aget-object v0, v0, v2

    iget-object v0, v0, Lfts;->b:Lfrf;

    iput-object v12, v0, Lfrf;->a:Landroid/graphics/RectF;

    iget-object v0, p0, Lfto;->e:[Lfts;

    aget-object v0, v0, v2

    iget-object v0, v0, Lfts;->b:Lfrf;

    iput-object v10, v0, Lfrf;->b:Landroid/graphics/RectF;

    iget-object v0, p0, Lfto;->e:[Lfts;

    aget-object v0, v0, v2

    iput v3, v0, Lfts;->c:I

    iget-object v0, p0, Lfto;->e:[Lfts;

    aget-object v0, v0, v2

    iput v4, v0, Lfts;->d:I

    iget-object v0, p0, Lfto;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(I)V

    invoke-virtual {v0, v3, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(II)V

    invoke-virtual {v0, v7}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Landroid/graphics/RectF;)V

    iget v3, p0, Lfto;->g:I

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->i(I)V

    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v4, -0x2

    const/4 v5, -0x2

    invoke-direct {v3, v4, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    if-nez v2, :cond_1

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setAlpha(F)V

    if-nez v2, :cond_2

    const/4 v1, 0x1

    :goto_2
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->l(Z)V

    new-instance v1, Lftr;

    invoke-direct {v1, p0}, Lftr;-><init>(Lfto;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Ljbb;)V

    invoke-virtual {p0, v0}, Lfto;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    .line 152
    :cond_3
    iget-object v0, p0, Lfto;->j:Landroid/os/Handler;

    iget-object v1, p0, Lfto;->l:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lfto;->j:Landroid/os/Handler;

    iget-object v1, p0, Lfto;->l:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 156
    :cond_4
    const/4 v0, 0x0

    :goto_3
    invoke-virtual {p0}, Lfto;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_5

    .line 157
    invoke-direct {p0, v0}, Lfto;->c(I)Landroid/graphics/Rect;

    move-result-object v1

    .line 159
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v2, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 161
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 162
    invoke-virtual {p0, v0}, Lfto;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 163
    invoke-virtual {v3, v2, v1}, Landroid/view/View;->measure(II)V

    .line 156
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 165
    :cond_5
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 169
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 171
    iget-object v0, p0, Lfto;->b:Lfrg;

    invoke-direct {p0, v0, p1, p2}, Lfto;->a(Lfrg;II)V

    .line 172
    return-void
.end method
