.class final Laag;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:I

.field private b:Ljava/lang/Object;

.field private final c:Ljava/util/concurrent/locks/ReentrantLock;

.field private final d:Ljava/util/concurrent/locks/Condition;


# direct methods
.method constructor <init>()V
    .locals 2

    .prologue
    .line 843
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 846
    const/4 v0, 0x0

    iput v0, p0, Laag;->a:I

    .line 848
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/locks/ReentrantLock;-><init>(Z)V

    iput-object v0, p0, Laag;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 849
    iget-object v0, p0, Laag;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Laag;->d:Ljava/util/concurrent/locks/Condition;

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 852
    if-nez p1, :cond_0

    .line 853
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Null context when locking"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 855
    :cond_0
    iget-object v1, p0, Laag;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 856
    iget v1, p0, Laag;->a:I

    if-ne v1, v0, :cond_1

    .line 858
    :try_start_0
    iget-object v1, p0, Laag;->d:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 863
    :cond_1
    iput v0, p0, Laag;->a:I

    .line 864
    iput-object p1, p0, Laag;->b:Ljava/lang/Object;

    .line 865
    iget-object v1, p0, Laag;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 866
    :goto_0
    return v0

    .line 860
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 870
    iget-object v0, p0, Laag;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 871
    iget v0, p0, Laag;->a:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 872
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Not in IN_USE state"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 874
    :cond_0
    iget-object v0, p0, Laag;->b:Ljava/lang/Object;

    if-eq p1, v0, :cond_1

    .line 875
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Lock is not owned by this context"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 877
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Laag;->a:I

    .line 878
    const/4 v0, 0x0

    iput-object v0, p0, Laag;->b:Ljava/lang/Object;

    .line 879
    iget-object v0, p0, Laag;->d:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signal()V

    .line 880
    iget-object v0, p0, Laag;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 881
    return-void
.end method
