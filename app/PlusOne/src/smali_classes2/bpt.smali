.class public final Lbpt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:F

.field private final b:F

.field private final c:I

.field private final d:[I


# direct methods
.method public constructor <init>(FFI)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    cmpg-float v0, p1, p2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "min value must be smaller than max"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 44
    iput p1, p0, Lbpt;->a:F

    .line 45
    iput p2, p0, Lbpt;->b:F

    .line 46
    iput p3, p0, Lbpt;->c:I

    .line 47
    iget v0, p0, Lbpt;->c:I

    new-array v0, v0, [I

    iput-object v0, p0, Lbpt;->d:[I

    .line 48
    return-void

    .line 43
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(F)V
    .locals 3

    .prologue
    .line 93
    iget v0, p0, Lbpt;->b:F

    iget v1, p0, Lbpt;->a:F

    sub-float/2addr v0, v1

    iget v1, p0, Lbpt;->c:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 94
    iget v1, p0, Lbpt;->a:F

    sub-float v1, p1, v1

    div-float v0, v1, v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 95
    iget v1, p0, Lbpt;->c:I

    if-lt v0, v1, :cond_1

    .line 96
    iget v0, p0, Lbpt;->c:I

    add-int/lit8 v0, v0, -0x1

    .line 100
    :cond_0
    :goto_0
    iget-object v1, p0, Lbpt;->d:[I

    aget v2, v1, v0

    add-int/lit8 v2, v2, 0x1

    aput v2, v1, v0

    .line 101
    return-void

    .line 97
    :cond_1
    if-gez v0, :cond_0

    .line 98
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Float;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 82
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 83
    invoke-virtual {p0, v0}, Lbpt;->a(F)V

    goto :goto_0

    .line 85
    :cond_0
    return-void
.end method

.method public a()[F
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 61
    move v0, v1

    move v2, v3

    .line 62
    :goto_0
    iget-object v4, p0, Lbpt;->d:[I

    array-length v4, v4

    if-ge v0, v4, :cond_0

    .line 63
    iget-object v4, p0, Lbpt;->d:[I

    aget v4, v4, v0

    int-to-float v4, v4

    add-float/2addr v2, v4

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 65
    :cond_0
    cmpl-float v0, v2, v3

    if-nez v0, :cond_1

    .line 66
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "The histogram has no counts. Hence, it can\'t no normalized."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_1
    iget-object v0, p0, Lbpt;->d:[I

    array-length v0, v0

    new-array v0, v0, [F

    .line 70
    :goto_1
    iget-object v3, p0, Lbpt;->d:[I

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 71
    iget-object v3, p0, Lbpt;->d:[I

    aget v3, v3, v1

    int-to-float v3, v3

    div-float/2addr v3, v2

    aput v3, v0, v1

    .line 70
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 73
    :cond_2
    return-object v0
.end method
