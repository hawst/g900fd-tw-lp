.class Ljzy;
.super Lkas;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field private final f:Lngl;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 14
    const-class v0, Ljzy;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "SetVote"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ljzy;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lngl;IIZ)V
    .locals 9

    .prologue
    .line 20
    sget-object v2, Ljzy;->a:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-direct/range {v0 .. v8}, Lkas;-><init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIZ)V

    .line 22
    iput-object p5, p0, Ljzy;->f:Lngl;

    .line 23
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 6

    .prologue
    .line 27
    new-instance v0, Ljzx;

    iget-object v1, p0, Ljzy;->b:Landroid/content/Context;

    iget v2, p0, Ljzy;->c:I

    iget-object v3, p0, Ljzy;->d:Ljava/lang/String;

    iget-object v4, p0, Ljzy;->e:Ljava/lang/String;

    iget-object v5, p0, Ljzy;->f:Lngl;

    invoke-direct/range {v0 .. v5}, Ljzx;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lngl;)V

    .line 29
    iget-object v1, p0, Ljzy;->b:Landroid/content/Context;

    const-class v2, Lkfd;

    invoke-static {v1, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkfd;

    .line 30
    invoke-interface {v1, v0}, Lkfd;->a(Lkff;)V

    .line 31
    iget v1, v0, Lkff;->i:I

    iget-object v2, v0, Lkff;->k:Ljava/lang/Exception;

    .line 32
    invoke-virtual {v0}, Ljzx;->t()Z

    move-result v3

    invoke-virtual {v0}, Ljzx;->t()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v0, 0x0

    .line 31
    :goto_0
    invoke-virtual {p0, v1, v2, v3, v0}, Ljzy;->a(ILjava/lang/Exception;ZLnge;)Lhoz;

    move-result-object v0

    return-object v0

    .line 33
    :cond_0
    invoke-virtual {v0}, Ljzx;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmhh;

    iget-object v0, v0, Lmhh;->a:Lnhd;

    iget-object v0, v0, Lnhd;->a:Lnge;

    goto :goto_0
.end method
