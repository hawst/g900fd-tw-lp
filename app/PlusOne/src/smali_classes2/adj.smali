.class public final Ladj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ladm;
.implements Ljava/lang/Runnable;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final a:Ladk;

.field private final b:Landroid/net/Uri;

.field private final c:Landroid/content/Context;

.field private final d:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/lang/Thread;

.field private f:Landroid/media/MediaExtractor;

.field private g:Lacw;

.field private h:I

.field private i:I

.field private j:I

.field private k:Lado;

.field private l:Ladd;

.field private m:Z

.field private n:J

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;JLadk;)V
    .locals 3

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Ladj;->o:Z

    .line 125
    if-nez p1, :cond_0

    .line 126
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "context cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_0
    iput-object p1, p0, Ladj;->c:Landroid/content/Context;

    .line 130
    if-nez p2, :cond_1

    .line 131
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "uri cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133
    :cond_1
    iput-object p2, p0, Ladj;->b:Landroid/net/Uri;

    .line 135
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-gez v0, :cond_2

    .line 136
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "startMicros cannot be negative"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 138
    :cond_2
    iput-wide p3, p0, Ladj;->n:J

    .line 140
    if-nez p5, :cond_3

    .line 141
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143
    :cond_3
    iput-object p5, p0, Ladj;->a:Ladk;

    .line 145
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    const/16 v1, 0x20

    invoke-direct {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>(I)V

    iput-object v0, p0, Ladj;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 146
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Ladj;->e:Ljava/lang/Thread;

    .line 147
    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 185
    if-eqz p1, :cond_0

    .line 186
    iget-object v0, p0, Ladj;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;->offer(Ljava/lang/Object;)Z

    .line 187
    iget-object v0, p0, Ladj;->e:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 191
    :goto_0
    return-void

    .line 189
    :cond_0
    iget-object v0, p0, Ladj;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;->offer(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private c(Z)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 302
    iget-object v0, p0, Ladj;->f:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Landroid/media/MediaExtractor;->release()V

    .line 303
    iput-object v1, p0, Ladj;->f:Landroid/media/MediaExtractor;

    .line 305
    iget-object v0, p0, Ladj;->k:Lado;

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Ladj;->k:Lado;

    invoke-virtual {v0}, Lado;->c()V

    .line 307
    iput-object v1, p0, Ladj;->k:Lado;

    .line 310
    :cond_0
    iget-object v0, p0, Ladj;->l:Ladd;

    if-eqz v0, :cond_1

    .line 311
    iget-object v0, p0, Ladj;->l:Ladd;

    invoke-virtual {v0}, Ladd;->c()V

    .line 312
    iput-object v1, p0, Ladj;->l:Ladd;

    .line 315
    :cond_1
    iget-boolean v0, p0, Ladj;->o:Z

    if-eqz v0, :cond_3

    .line 316
    iget-object v0, p0, Ladj;->g:Lacw;

    if-eqz v0, :cond_2

    .line 317
    invoke-direct {p0}, Ladj;->e()Lacw;

    move-result-object v0

    invoke-virtual {v0}, Lacw;->i()V

    .line 319
    :cond_2
    invoke-static {}, Lacw;->e()V

    .line 322
    :cond_3
    iput v2, p0, Ladj;->i:I

    .line 323
    iput v2, p0, Ladj;->j:I

    .line 325
    iget-object v0, p0, Ladj;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 326
    const/4 v0, 0x0

    iput-boolean v0, p0, Ladj;->m:Z

    .line 327
    if-eqz p1, :cond_4

    .line 328
    iget-object v0, p0, Ladj;->a:Ladk;

    invoke-interface {v0}, Ladk;->d_()V

    .line 330
    :cond_4
    return-void
.end method

.method private e()Lacw;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 418
    iget-object v0, p0, Ladj;->g:Lacw;

    if-nez v0, :cond_0

    .line 419
    invoke-static {v1, v1}, Lacw;->a(II)Lacw;

    move-result-object v0

    iput-object v0, p0, Ladj;->g:Lacw;

    .line 421
    :cond_0
    iget-object v0, p0, Ladj;->g:Lacw;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 176
    iget-object v0, p0, Ladj;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;->offer(Ljava/lang/Object;)Z

    .line 177
    iget-object v0, p0, Ladj;->e:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 178
    return-void
.end method

.method public a(Laas;Labg;)V
    .locals 1

    .prologue
    .line 365
    iget v0, p0, Ladj;->h:I

    invoke-virtual {p0, p1, p2, v0}, Ladj;->a(Laas;Labg;I)V

    .line 366
    return-void
.end method

.method public a(Laas;Labg;I)V
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Ladj;->k:Lado;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 380
    iget-object v0, p0, Ladj;->k:Lado;

    invoke-virtual {v0, p1, p2, p3}, Lado;->b(Laas;Labg;I)V

    .line 382
    :cond_0
    return-void
.end method

.method public a(Labg;)V
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Ladj;->l:Ladd;

    if-eqz v0, :cond_0

    .line 391
    iget-object v0, p0, Ladj;->l:Ladd;

    invoke-virtual {v0, p1}, Ladd;->a(Labg;)V

    .line 393
    :cond_0
    return-void
.end method

.method public a(Ladl;)V
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Ladj;->k:Lado;

    if-ne p1, v0, :cond_1

    .line 427
    iget-object v0, p0, Ladj;->a:Ladk;

    invoke-interface {v0}, Ladk;->a()V

    .line 431
    :cond_0
    :goto_0
    return-void

    .line 428
    :cond_1
    iget-object v0, p0, Ladj;->l:Ladd;

    if-ne p1, v0, :cond_0

    .line 429
    iget-object v0, p0, Ladj;->a:Ladk;

    invoke-interface {v0}, Ladk;->b_()V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Ladj;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    iput-boolean p1, p0, Ladj;->o:Z

    return-void

    .line 161
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must call setOpenGLEnabled() before calling start()!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ladj;->b(Z)V

    .line 182
    return-void
.end method

.method public b(Ladl;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 435
    iget-object v0, p0, Ladj;->l:Ladd;

    if-ne p1, v0, :cond_4

    .line 436
    iput-boolean v1, p0, Ladj;->q:Z

    .line 441
    :cond_0
    :goto_0
    iget-object v0, p0, Ladj;->l:Ladd;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Ladj;->q:Z

    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p0, Ladj;->k:Lado;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Ladj;->r:Z

    if-eqz v0, :cond_3

    .line 443
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Ladj;->b(Z)V

    .line 445
    :cond_3
    return-void

    .line 437
    :cond_4
    iget-object v0, p0, Ladj;->k:Lado;

    if-ne p1, v0, :cond_0

    .line 438
    iput-boolean v1, p0, Ladj;->r:Z

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Ladj;->l:Ladd;

    if-eqz v0, :cond_0

    .line 400
    iget-object v0, p0, Ladj;->l:Ladd;

    invoke-virtual {v0}, Ladd;->a()V

    .line 402
    :cond_0
    return-void
.end method

.method public d()J
    .locals 4

    .prologue
    .line 408
    iget-boolean v0, p0, Ladj;->m:Z

    if-nez v0, :cond_0

    .line 409
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MediaDecoder has not been started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 412
    :cond_0
    iget-object v1, p0, Ladj;->f:Landroid/media/MediaExtractor;

    iget v0, p0, Ladj;->i:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    iget v0, p0, Ladj;->i:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/media/MediaExtractor;->getTrackFormat(I)Landroid/media/MediaFormat;

    move-result-object v0

    .line 414
    const-string v1, "durationUs"

    invoke-virtual {v0, v1}, Landroid/media/MediaFormat;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    return-wide v0

    .line 412
    :cond_1
    iget v0, p0, Ladj;->j:I

    goto :goto_0
.end method

.method public run()V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v8, -0x1

    .line 198
    :cond_0
    :try_start_0
    iget-object v0, p0, Ladj;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 200
    if-eqz v0, :cond_10

    .line 201
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_0
    move v0, v2

    .line 218
    :goto_1
    if-eqz v0, :cond_0

    .line 227
    :goto_2
    return-void

    .line 203
    :pswitch_0
    iget-boolean v0, p0, Ladj;->o:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Ladj;->e()Lacw;

    move-result-object v0

    invoke-virtual {v0}, Lacw;->d()V

    :cond_2
    new-instance v0, Landroid/media/MediaExtractor;

    invoke-direct {v0}, Landroid/media/MediaExtractor;-><init>()V

    iput-object v0, p0, Ladj;->f:Landroid/media/MediaExtractor;

    iget-object v0, p0, Ladj;->f:Landroid/media/MediaExtractor;

    iget-object v3, p0, Ladj;->c:Landroid/content/Context;

    iget-object v4, p0, Ladj;->b:Landroid/net/Uri;

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v4, v5}, Landroid/media/MediaExtractor;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    const/4 v0, -0x1

    iput v0, p0, Ladj;->i:I

    const/4 v0, -0x1

    iput v0, p0, Ladj;->j:I

    move v0, v2

    :goto_3
    iget-object v3, p0, Ladj;->f:Landroid/media/MediaExtractor;

    invoke-virtual {v3}, Landroid/media/MediaExtractor;->getTrackCount()I

    move-result v3

    if-ge v0, v3, :cond_5

    iget-object v3, p0, Ladj;->f:Landroid/media/MediaExtractor;

    invoke-virtual {v3, v0}, Landroid/media/MediaExtractor;->getTrackFormat(I)Landroid/media/MediaFormat;

    move-result-object v3

    invoke-static {v3}, Ladg;->b(Landroid/media/MediaFormat;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget v4, p0, Ladj;->i:I

    if-ne v4, v8, :cond_4

    iput v0, p0, Ladj;->i:I

    :cond_3
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-static {v3}, Ladg;->a(Landroid/media/MediaFormat;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget v3, p0, Ladj;->j:I

    if-ne v3, v8, :cond_3

    iput v0, p0, Ladj;->j:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_4

    .line 223
    :catch_0
    move-exception v0

    .line 224
    iget-object v1, p0, Ladj;->a:Ladk;

    invoke-interface {v1, v0}, Ladk;->a(Ljava/lang/Exception;)V

    .line 225
    invoke-direct {p0, v2}, Ladj;->c(Z)V

    goto :goto_2

    .line 203
    :cond_5
    :try_start_1
    iget v0, p0, Ladj;->i:I

    if-ne v0, v8, :cond_6

    iget v0, p0, Ladj;->j:I

    if-ne v0, v8, :cond_6

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Couldn\'t find a video or audio track in the provided file"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    iget v0, p0, Ladj;->i:I

    if-eq v0, v8, :cond_8

    iget-object v0, p0, Ladj;->f:Landroid/media/MediaExtractor;

    iget v3, p0, Ladj;->i:I

    invoke-virtual {v0, v3}, Landroid/media/MediaExtractor;->getTrackFormat(I)Landroid/media/MediaFormat;

    move-result-object v3

    iget-boolean v0, p0, Ladj;->o:Z

    if-eqz v0, :cond_b

    new-instance v0, Ladh;

    iget v4, p0, Ladj;->i:I

    invoke-direct {v0, v4, v3, p0}, Ladh;-><init>(ILandroid/media/MediaFormat;Ladm;)V

    :goto_5
    iput-object v0, p0, Ladj;->k:Lado;

    iget-object v0, p0, Ladj;->k:Lado;

    invoke-virtual {v0}, Lado;->e()V

    iget-object v0, p0, Ladj;->f:Landroid/media/MediaExtractor;

    iget v3, p0, Ladj;->i:I

    invoke-virtual {v0, v3}, Landroid/media/MediaExtractor;->selectTrack(I)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v0, v3, :cond_8

    new-instance v0, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v0}, Landroid/media/MediaMetadataRetriever;-><init>()V

    iget-object v3, p0, Ladj;->b:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v3

    const-string v4, "content"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    const-string v4, "android.resource"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    :cond_7
    iget-object v3, p0, Ladj;->c:Landroid/content/Context;

    iget-object v4, p0, Ladj;->b:Landroid/net/Uri;

    invoke-virtual {v0, v3, v4}, Landroid/media/MediaMetadataRetriever;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    :goto_6
    const/16 v3, 0x18

    invoke-virtual {v0, v3}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_e

    move v0, v2

    :goto_7
    iput v0, p0, Ladj;->h:I

    :cond_8
    iget v0, p0, Ladj;->j:I

    if-eq v0, v8, :cond_9

    iget-object v0, p0, Ladj;->f:Landroid/media/MediaExtractor;

    iget v3, p0, Ladj;->j:I

    invoke-virtual {v0, v3}, Landroid/media/MediaExtractor;->getTrackFormat(I)Landroid/media/MediaFormat;

    move-result-object v0

    new-instance v3, Ladd;

    iget v4, p0, Ladj;->j:I

    invoke-direct {v3, v4, v0, p0}, Ladd;-><init>(ILandroid/media/MediaFormat;Ladm;)V

    iput-object v3, p0, Ladj;->l:Ladd;

    iget-object v0, p0, Ladj;->l:Ladd;

    invoke-virtual {v0}, Ladd;->e()V

    iget-object v0, p0, Ladj;->f:Landroid/media/MediaExtractor;

    iget v3, p0, Ladj;->j:I

    invoke-virtual {v0, v3}, Landroid/media/MediaExtractor;->selectTrack(I)V

    :cond_9
    iget-wide v4, p0, Ladj;->n:J

    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-lez v0, :cond_a

    iget-object v0, p0, Ladj;->f:Landroid/media/MediaExtractor;

    iget-wide v4, p0, Ladj;->n:J

    const/4 v3, 0x2

    invoke-virtual {v0, v4, v5, v3}, Landroid/media/MediaExtractor;->seekTo(JI)V

    :cond_a
    const/4 v0, 0x1

    iput-boolean v0, p0, Ladj;->m:Z

    iget-object v0, p0, Ladj;->a:Ladk;

    invoke-interface {v0}, Ladk;->c_()V

    move v0, v2

    .line 204
    goto/16 :goto_1

    .line 203
    :cond_b
    new-instance v0, Lade;

    iget v4, p0, Ladj;->i:I

    invoke-direct {v0, v4, v3, p0}, Lade;-><init>(ILandroid/media/MediaFormat;Ladm;)V

    goto/16 :goto_5

    :cond_c
    const-string v4, "file"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_d

    iget-object v3, p0, Ladj;->b:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V

    goto :goto_6

    :cond_d
    iget-object v3, p0, Ladj;->b:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v0, v3, v4}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_6

    :cond_e
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_7

    .line 206
    :pswitch_1
    iget-object v0, p0, Ladj;->k:Lado;

    if-eqz v0, :cond_f

    .line 207
    iget-object v0, p0, Ladj;->k:Lado;

    invoke-virtual {v0}, Lado;->a()Z

    .line 211
    :cond_f
    :pswitch_2
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Ladj;->c(Z)V

    move v0, v1

    .line 212
    goto/16 :goto_1

    .line 215
    :cond_10
    iget-boolean v0, p0, Ladj;->m:Z

    if-eqz v0, :cond_1

    .line 216
    iget-object v0, p0, Ladj;->f:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Landroid/media/MediaExtractor;->getSampleTrackIndex()I

    move-result v0

    if-ltz v0, :cond_14

    iget v3, p0, Ladj;->i:I

    if-ne v0, v3, :cond_13

    iget-object v0, p0, Ladj;->k:Lado;

    iget-object v3, p0, Ladj;->f:Landroid/media/MediaExtractor;

    invoke-virtual {v0, v3}, Lado;->a(Landroid/media/MediaExtractor;)Z

    :cond_11
    :goto_8
    iget-object v0, p0, Ladj;->k:Lado;

    if-eqz v0, :cond_12

    iget-object v0, p0, Ladj;->k:Lado;

    invoke-virtual {v0}, Lado;->b()Z

    :cond_12
    iget-object v0, p0, Ladj;->l:Ladd;

    if-eqz v0, :cond_1

    iget-object v0, p0, Ladj;->l:Ladd;

    invoke-virtual {v0}, Ladd;->b()Z

    goto/16 :goto_0

    :cond_13
    iget v3, p0, Ladj;->j:I

    if-ne v0, v3, :cond_11

    iget-object v0, p0, Ladj;->l:Ladd;

    iget-object v3, p0, Ladj;->f:Landroid/media/MediaExtractor;

    invoke-virtual {v0, v3}, Ladd;->a(Landroid/media/MediaExtractor;)Z

    goto :goto_8

    :cond_14
    iget-boolean v0, p0, Ladj;->p:Z

    if-nez v0, :cond_11

    iget-object v0, p0, Ladj;->k:Lado;

    if-eqz v0, :cond_15

    iget-object v0, p0, Ladj;->k:Lado;

    invoke-virtual {v0}, Lado;->d()V

    :cond_15
    iget-object v0, p0, Ladj;->l:Ladd;

    if-eqz v0, :cond_16

    iget-object v0, p0, Ladj;->l:Ladd;

    invoke-virtual {v0}, Ladd;->d()V

    :cond_16
    const/4 v0, 0x1

    iput-boolean v0, p0, Ladj;->p:Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_8

    .line 201
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
