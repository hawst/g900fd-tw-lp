.class public final Lcgh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:J

.field private final b:I

.field private final c:I

.field private final d:I


# direct methods
.method public constructor <init>(JIII)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-wide p1, p0, Lcgh;->a:J

    .line 26
    iput p3, p0, Lcgh;->b:I

    .line 27
    iput p4, p0, Lcgh;->c:I

    .line 28
    iput p5, p0, Lcgh;->d:I

    .line 29
    return-void
.end method


# virtual methods
.method public a(J)J
    .locals 13

    .prologue
    const-wide/16 v6, 0x0

    .line 35
    .line 36
    iget-wide v2, p0, Lcgh;->a:J

    .line 37
    iget v0, p0, Lcgh;->c:I

    move-wide v4, v6

    .line 38
    :goto_0
    sub-long v8, p1, v4

    int-to-long v10, v0

    mul-long/2addr v10, v2

    cmp-long v1, v8, v10

    if-lez v1, :cond_0

    .line 40
    int-to-long v0, v0

    mul-long/2addr v0, v2

    add-long/2addr v4, v0

    .line 41
    iget v0, p0, Lcgh;->b:I

    int-to-long v0, v0

    mul-long/2addr v2, v0

    .line 42
    iget v0, p0, Lcgh;->d:I

    goto :goto_0

    .line 45
    :cond_0
    sub-long v0, p1, v4

    rem-long/2addr v0, v2

    .line 46
    cmp-long v4, v0, v6

    if-eqz v4, :cond_1

    .line 47
    sub-long v0, v2, v0

    add-long/2addr p1, v0

    .line 49
    :cond_1
    return-wide p1
.end method
