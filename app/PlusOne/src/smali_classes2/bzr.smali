.class public final Lbzr;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Ljava/util/Map;Ljava/util/Map;Ljava/util/List;Ljava/util/List;)Landroid/util/Pair;
    .locals 28
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbmb;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcac;",
            ">;)",
            "Landroid/util/Pair",
            "<",
            "Lbma;",
            "Lbma;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    new-instance v22, Ljava/util/ArrayList;

    invoke-direct/range {v22 .. v22}, Ljava/util/ArrayList;-><init>()V

    .line 109
    new-instance v23, Ljava/util/ArrayList;

    invoke-direct/range {v23 .. v23}, Ljava/util/ArrayList;-><init>()V

    .line 110
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 112
    new-instance v24, Ljava/util/ArrayList;

    invoke-direct/range {v24 .. v24}, Ljava/util/ArrayList;-><init>()V

    .line 113
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 114
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 116
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    move-object/from16 v2, p1

    invoke-static {v0, v1, v2}, Lbzr;->a(Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;

    move-result-object v26

    .line 119
    new-instance v4, Lboq;

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    invoke-direct {v4, v6, v7, v8}, Lboq;-><init>(JF)V

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    new-instance v4, Lboq;

    const-wide/16 v6, 0x0

    const/4 v8, 0x0

    invoke-direct {v4, v6, v7, v8}, Lboq;-><init>(JF)V

    invoke-interface {v13, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    const-wide/16 v8, 0x0

    .line 123
    const/4 v7, 0x0

    .line 125
    const/4 v6, 0x1

    .line 127
    const/4 v4, 0x0

    move v12, v4

    move/from16 v20, v7

    move/from16 v27, v6

    move-wide v6, v8

    move/from16 v9, v27

    :goto_0
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v4

    if-ge v12, v4, :cond_7

    .line 128
    move-object/from16 v0, p2

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbmd;

    .line 130
    iget-object v8, v4, Lbmd;->e:Ljeg;

    move-object/from16 v0, p0

    invoke-interface {v0, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lbml;

    .line 131
    if-eqz v8, :cond_c

    iget-object v10, v4, Lbmd;->e:Ljeg;

    move-object/from16 v0, p1

    invoke-interface {v0, v10}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    if-eqz v10, :cond_c

    .line 132
    invoke-static {v8}, Lbzr;->a(Lbml;)Z

    move-result v8

    if-eqz v8, :cond_c

    .line 133
    if-lez v20, :cond_b

    .line 134
    if-eqz v9, :cond_1

    .line 135
    invoke-interface/range {v22 .. v22}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    move-object/from16 v0, v22

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lbmd;

    move-object v10, v8

    .line 137
    :goto_1
    if-eqz v9, :cond_2

    .line 138
    invoke-interface/range {v23 .. v23}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    move-object/from16 v0, v23

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    .line 137
    :goto_2
    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 143
    invoke-virtual {v10}, Lbmd;->b()J

    move-result-wide v10

    add-long/2addr v10, v14

    cmp-long v8, v10, v6

    if-lez v8, :cond_b

    .line 145
    if-nez v9, :cond_3

    const/4 v8, 0x1

    :goto_3
    move/from16 v21, v8

    .line 149
    :goto_4
    new-instance v8, Lbmd;

    sget-object v9, Lbmg;->b:Lbmg;

    iget-object v10, v4, Lbmd;->e:Ljeg;

    iget-object v11, v4, Lbmd;->f:Lbmp;

    invoke-direct {v8, v9, v10, v11}, Lbmd;-><init>(Lbmg;Ljeg;Lbmp;)V

    .line 151
    if-lez v12, :cond_4

    add-int/lit8 v9, v12, -0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcac;

    .line 152
    :goto_5
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    if-ge v12, v10, :cond_5

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcac;

    .line 153
    :goto_6
    move-object/from16 v0, v26

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/lang/Float;

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v11

    .line 155
    if-eqz v21, :cond_6

    .line 156
    move-object/from16 v0, v22

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    move-object/from16 v0, v23

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    invoke-static/range {v5 .. v11}, Lbzr;->a(Ljava/util/List;JLbmd;Lcac;Lcac;F)V

    .line 166
    :goto_7
    add-int/lit8 v20, v20, 0x1

    move/from16 v8, v21

    move/from16 v9, v20

    .line 168
    :goto_8
    invoke-virtual {v4}, Lbmd;->b()J

    move-result-wide v10

    add-long/2addr v6, v10

    .line 169
    invoke-interface/range {p2 .. p2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-eq v12, v4, :cond_0

    .line 170
    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcac;

    invoke-interface {v4}, Lcac;->b()J

    move-result-wide v10

    sub-long/2addr v6, v10

    .line 127
    :cond_0
    add-int/lit8 v4, v12, 0x1

    move v12, v4

    move/from16 v20, v9

    move v9, v8

    goto/16 :goto_0

    .line 136
    :cond_1
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    move-object/from16 v0, v24

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lbmd;

    move-object v10, v8

    goto/16 :goto_1

    .line 139
    :cond_2
    invoke-interface/range {v25 .. v25}, Ljava/util/List;->size()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    move-object/from16 v0, v25

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Long;

    goto/16 :goto_2

    .line 145
    :cond_3
    const/4 v8, 0x0

    goto/16 :goto_3

    .line 151
    :cond_4
    const/4 v9, 0x0

    goto :goto_5

    .line 152
    :cond_5
    const/4 v10, 0x0

    goto :goto_6

    .line 161
    :cond_6
    move-object/from16 v0, v24

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 162
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v14

    move-object/from16 v0, v25

    invoke-interface {v0, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-wide v14, v6

    move-object/from16 v16, v8

    move-object/from16 v17, v9

    move-object/from16 v18, v10

    move/from16 v19, v11

    .line 163
    invoke-static/range {v13 .. v19}, Lbzr;->a(Ljava/util/List;JLbmd;Lcac;Lcac;F)V

    goto :goto_7

    .line 175
    :cond_7
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v5, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lboq;

    iget-wide v8, v4, Lboq;->a:J

    cmp-long v4, v8, v6

    if-eqz v4, :cond_8

    .line 176
    new-instance v4, Lboq;

    const/4 v8, 0x0

    invoke-direct {v4, v6, v7, v8}, Lboq;-><init>(JF)V

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 179
    :cond_8
    new-instance v15, Lblz;

    const/4 v4, 0x0

    new-array v4, v4, [Lbmd;

    .line 180
    move-object/from16 v0, v22

    invoke-interface {v0, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v16

    check-cast v16, [Lbmd;

    .line 181
    invoke-static/range {v23 .. v23}, Lbzr;->a(Ljava/util/List;)[J

    move-result-object v17

    const/4 v4, 0x0

    new-array v4, v4, [Lboq;

    .line 183
    invoke-interface {v5, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v20

    check-cast v20, [Lboq;

    move-wide/from16 v18, v6

    invoke-direct/range {v15 .. v20}, Lblz;-><init>([Lbmd;[JJ[Lboq;)V

    .line 186
    const/4 v9, 0x0

    .line 187
    invoke-interface/range {v24 .. v24}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_a

    .line 188
    invoke-interface {v13}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v13, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lboq;

    iget-wide v4, v4, Lboq;->a:J

    cmp-long v4, v4, v6

    if-eqz v4, :cond_9

    .line 190
    new-instance v4, Lboq;

    const/4 v5, 0x0

    invoke-direct {v4, v6, v7, v5}, Lboq;-><init>(JF)V

    invoke-interface {v13, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    :cond_9
    new-instance v9, Lblz;

    const/4 v4, 0x0

    new-array v4, v4, [Lbmd;

    .line 193
    move-object/from16 v0, v24

    invoke-interface {v0, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v10

    check-cast v10, [Lbmd;

    .line 194
    invoke-static/range {v25 .. v25}, Lbzr;->a(Ljava/util/List;)[J

    move-result-object v11

    const/4 v4, 0x0

    new-array v4, v4, [Lboq;

    .line 196
    invoke-interface {v13, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v14

    check-cast v14, [Lboq;

    move-wide v12, v6

    invoke-direct/range {v9 .. v14}, Lblz;-><init>([Lbmd;[JJ[Lboq;)V

    .line 199
    :cond_a
    invoke-static {v15, v9}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    return-object v4

    :cond_b
    move/from16 v21, v9

    goto/16 :goto_4

    :cond_c
    move v8, v9

    move/from16 v9, v20

    goto/16 :goto_8
.end method

.method private static a(Ljava/util/List;Ljava/util/Map;Ljava/util/Map;)Ljava/util/List;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbmb;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 214
    new-instance v11, Ljava/util/ArrayList;

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v11, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 215
    new-instance v12, Ljava/util/ArrayList;

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v12, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 219
    const/4 v4, 0x0

    .line 220
    const/4 v3, 0x0

    .line 221
    const/4 v2, 0x0

    move v5, v2

    move v6, v3

    move v7, v4

    :goto_0
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v5, v2, :cond_8

    .line 222
    move-object/from16 v0, p0

    invoke-interface {v0, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbmd;

    .line 224
    iget-object v3, v2, Lbmd;->e:Ljeg;

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbml;

    .line 225
    iget-object v4, v2, Lbmd;->e:Ljeg;

    move-object/from16 v0, p2

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbmb;

    .line 226
    if-eqz v3, :cond_7

    if-eqz v4, :cond_7

    invoke-static {v3}, Lbzr;->a(Lbml;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 227
    invoke-virtual {v4}, Lbmb;->a()Landroid/util/LongSparseArray;

    move-result-object v13

    .line 229
    const/4 v9, 0x0

    .line 230
    const/4 v8, 0x0

    .line 232
    const/4 v3, 0x0

    move v10, v9

    move v9, v8

    move v8, v3

    :goto_1
    invoke-virtual {v13}, Landroid/util/LongSparseArray;->size()I

    move-result v3

    if-ge v8, v3, :cond_2

    .line 233
    invoke-virtual {v13, v8}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v14

    .line 234
    iget-object v3, v2, Lbmd;->f:Lbmp;

    iget-wide v0, v3, Lbmp;->b:J

    move-wide/from16 v16, v0

    cmp-long v3, v14, v16

    if-lez v3, :cond_1

    iget-object v3, v2, Lbmd;->f:Lbmp;

    iget-wide v0, v3, Lbmp;->c:J

    move-wide/from16 v16, v0

    cmp-long v3, v14, v16

    if-gez v3, :cond_1

    .line 236
    invoke-virtual {v13, v14, v15}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    add-float/2addr v10, v3

    .line 237
    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v9, v3

    .line 239
    :cond_0
    add-int/lit8 v3, v8, 0x1

    move v8, v3

    goto :goto_1

    .line 238
    :cond_1
    iget-object v3, v2, Lbmd;->f:Lbmp;

    iget-wide v0, v3, Lbmp;->c:J

    move-wide/from16 v16, v0

    cmp-long v3, v14, v16

    if-ltz v3, :cond_0

    .line 242
    :cond_2
    const/4 v3, 0x0

    .line 246
    const/4 v8, 0x0

    cmpl-float v8, v9, v8

    if-lez v8, :cond_3

    .line 247
    div-float v3, v10, v9

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    .line 248
    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v8

    add-float/2addr v7, v8

    .line 249
    add-int/lit8 v6, v6, 0x1

    .line 251
    :cond_3
    invoke-interface {v11, v5, v3}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 253
    invoke-virtual {v4}, Lbmb;->b()Landroid/util/LongSparseArray;

    move-result-object v9

    .line 254
    const/4 v4, 0x0

    .line 255
    const/4 v3, 0x0

    move v8, v4

    move v4, v3

    :goto_2
    invoke-virtual {v9}, Landroid/util/LongSparseArray;->size()I

    move-result v3

    if-ge v4, v3, :cond_6

    .line 257
    invoke-virtual {v9, v4}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v14

    .line 258
    iget-object v3, v2, Lbmd;->f:Lbmp;

    iget-wide v0, v3, Lbmp;->b:J

    move-wide/from16 v16, v0

    cmp-long v3, v14, v16

    if-lez v3, :cond_5

    iget-object v3, v2, Lbmd;->f:Lbmp;

    iget-wide v0, v3, Lbmp;->c:J

    move-wide/from16 v16, v0

    cmp-long v3, v14, v16

    if-gez v3, :cond_5

    .line 260
    invoke-virtual {v9, v14, v15}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 261
    if-le v3, v8, :cond_4

    move v8, v3

    .line 265
    :cond_4
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    .line 264
    :cond_5
    iget-object v3, v2, Lbmd;->f:Lbmp;

    iget-wide v0, v3, Lbmp;->c:J

    move-wide/from16 v16, v0

    cmp-long v3, v14, v16

    if-ltz v3, :cond_4

    .line 268
    :cond_6
    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v12, v5, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    move v3, v6

    move v4, v7

    .line 221
    :goto_3
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v6, v3

    move v7, v4

    goto/16 :goto_0

    .line 270
    :cond_7
    const/4 v2, 0x0

    invoke-interface {v11, v5, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 271
    const/4 v2, 0x0

    invoke-interface {v12, v5, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    move v3, v6

    move v4, v7

    goto :goto_3

    .line 276
    :cond_8
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 277
    int-to-float v2, v6

    div-float v6, v7, v2

    .line 281
    const/4 v2, 0x0

    move v4, v2

    :goto_4
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v4, v2, :cond_b

    .line 282
    invoke-interface {v11, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    .line 283
    invoke-interface {v12, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 285
    if-eqz v2, :cond_a

    if-eqz v3, :cond_a

    .line 286
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-nez v7, :cond_9

    const/high16 v3, 0x3fc00000    # 1.5f

    .line 290
    :goto_5
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    div-float v2, v6, v2

    const/high16 v7, 0x3f000000    # 0.5f

    .line 289
    invoke-static {v2, v7, v3}, Lcfn;->a(FFF)F

    move-result v2

    .line 296
    :goto_6
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v5, v4, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 281
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_4

    .line 286
    :cond_9
    const/high16 v7, 0x3fc00000    # 1.5f

    const v8, 0x46fffe00    # 32767.0f

    .line 288
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    int-to-float v3, v3

    div-float v3, v8, v3

    invoke-static {v7, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    goto :goto_5

    .line 294
    :cond_a
    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_6

    .line 298
    :cond_b
    return-object v5
.end method

.method private static a(Ljava/util/List;JLbmd;Lcac;Lcac;F)V
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lboq;",
            ">;J",
            "Lbmd;",
            "Lcac;",
            "Lcac;",
            "F)V"
        }
    .end annotation

    .prologue
    .line 321
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboq;

    iget-wide v0, v0, Lboq;->a:J

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    .line 322
    new-instance v0, Lboq;

    const/4 v1, 0x0

    invoke-direct {v0, p1, p2, v1}, Lboq;-><init>(JF)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 325
    :cond_0
    if-eqz p4, :cond_2

    .line 326
    invoke-interface {p4}, Lcac;->b()J

    move-result-wide v2

    .line 329
    :goto_0
    if-eqz p5, :cond_3

    .line 330
    invoke-interface {p5}, Lcac;->b()J

    move-result-wide v0

    .line 333
    :goto_1
    add-long v4, v2, v0

    .line 334
    invoke-virtual {p3}, Lbmd;->b()J

    move-result-wide v6

    cmp-long v6, v4, v6

    if-lez v6, :cond_1

    .line 335
    invoke-virtual {p3}, Lbmd;->b()J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 336
    const-wide/16 v6, 0x2

    div-long v6, v4, v6

    sub-long/2addr v2, v6

    .line 337
    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    const-wide/16 v6, 0x2

    div-long/2addr v4, v6

    sub-long/2addr v0, v4

    .line 340
    :cond_1
    new-instance v4, Lboq;

    add-long/2addr v2, p1

    invoke-direct {v4, v2, v3, p6}, Lboq;-><init>(JF)V

    invoke-interface {p0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 341
    new-instance v2, Lboq;

    .line 342
    invoke-virtual {p3}, Lbmd;->b()J

    move-result-wide v4

    add-long/2addr v4, p1

    sub-long v0, v4, v0

    invoke-direct {v2, v0, v1, p6}, Lboq;-><init>(JF)V

    .line 341
    invoke-interface {p0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 343
    new-instance v0, Lboq;

    invoke-virtual {p3}, Lbmd;->b()J

    move-result-wide v2

    add-long/2addr v2, p1

    const/4 v1, 0x0

    invoke-direct {v0, v2, v3, v1}, Lboq;-><init>(JF)V

    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 344
    return-void

    .line 326
    :cond_2
    const-wide/32 v2, 0x186a0

    goto :goto_0

    .line 330
    :cond_3
    const-wide/32 v0, 0x186a0

    goto :goto_1
.end method

.method private static a(Lbml;)Z
    .locals 3

    .prologue
    .line 356
    invoke-interface {p0}, Lbml;->b()Lbmu;

    move-result-object v0

    iget-object v0, v0, Lbmu;->e:Lbmv;

    sget-object v1, Lbmv;->a:Lbmv;

    if-eq v0, v1, :cond_0

    .line 357
    const/4 v0, 0x0

    .line 361
    :goto_0
    return v0

    .line 359
    :cond_0
    const-string v0, "displayable"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 360
    invoke-interface {p0}, Lbml;->b()Lbmu;

    move-result-object v0

    iget-object v0, v0, Lbmu;->e:Lbmv;

    const-string v1, "metadata type"

    sget-object v2, Lbmv;->a:Lbmv;

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    .line 361
    invoke-interface {p0}, Lbml;->b()Lbmu;

    move-result-object v0

    invoke-virtual {v0}, Lbmu;->a()Lboo;

    move-result-object v0

    iget-boolean v0, v0, Lboo;->g:Z

    goto :goto_0
.end method

.method public static a(Ljava/util/Map;Ljava/util/List;)Z
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v8, 0x0

    .line 63
    const-string v0, "displayableByMediaId"

    invoke-static {p0, v0, v8}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 64
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 65
    const-string v4, "clip"

    invoke-static {v0, v4, v8}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 66
    iget-object v4, v0, Lbmd;->d:Lbmg;

    sget-object v5, Lbmg;->a:Lbmg;

    if-ne v4, v5, :cond_0

    iget-object v0, v0, Lbmd;->e:Ljeg;

    .line 67
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbml;

    const-string v4, "displayable"

    invoke-static {v0, v4, v8}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbml;

    invoke-interface {v0}, Lbml;->b()Lbmu;

    move-result-object v0

    iget v4, v0, Lbmu;->c:I

    iget v0, v0, Lbmu;->d:I

    mul-int/2addr v0, v4

    int-to-long v4, v0

    const-wide/32 v6, 0xe1000

    cmp-long v0, v4, v6

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    move v0, v1

    .line 72
    :goto_1
    return v0

    :cond_1
    move v0, v2

    .line 67
    goto :goto_0

    :cond_2
    move v0, v2

    .line 72
    goto :goto_1
.end method

.method private static final a(Ljava/util/List;)[J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)[J"
        }
    .end annotation

    .prologue
    .line 347
    const-string v0, "list"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 348
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [J

    .line 349
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 350
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v2, v1

    .line 349
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 352
    :cond_0
    return-object v2
.end method
