.class public Lavj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lalh;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/PriorityQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/PriorityQueue",
            "<",
            "Lavk;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Lavk;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lavl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lavj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lavj;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lavl;)V
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, Lavj;->b:Ljava/util/PriorityQueue;

    .line 53
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lavj;->c:Landroid/util/LongSparseArray;

    .line 57
    const-string v0, "storage"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavl;

    iput-object v0, p0, Lavj;->d:Lavl;

    .line 58
    return-void
.end method

.method private a(JI)V
    .locals 3

    .prologue
    .line 170
    iget-object v0, p0, Lavj;->c:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavk;

    .line 171
    if-nez v0, :cond_0

    .line 172
    new-instance v0, Lavk;

    invoke-direct {v0, p1, p2}, Lavk;-><init>(J)V

    .line 173
    iget-object v1, p0, Lavj;->c:Landroid/util/LongSparseArray;

    invoke-virtual {v1, p1, p2, v0}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 178
    :goto_0
    iget v1, v0, Lavk;->b:I

    add-int/2addr v1, p3

    iput v1, v0, Lavk;->b:I

    .line 179
    iget-object v1, p0, Lavj;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 180
    invoke-direct {p0}, Lavj;->b()V

    .line 181
    return-void

    .line 175
    :cond_0
    iget-object v1, p0, Lavj;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->remove(Ljava/lang/Object;)Z

    move-result v1

    .line 176
    const-string v2, "found"

    invoke-static {v1, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private b()V
    .locals 8

    .prologue
    .line 187
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Lavj;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavk;

    const-string v3, "%d,%d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-wide v6, v0, Lavk;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    iget v0, v0, Lavk;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 191
    :cond_0
    iget-object v0, p0, Lavj;->d:Lavl;

    invoke-interface {v0, v1}, Lavl;->a(Ljava/util/Set;)V

    .line 192
    return-void
.end method

.method private c(J)V
    .locals 3

    .prologue
    .line 229
    iget-object v0, p0, Lavj;->c:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavk;

    .line 230
    if-eqz v0, :cond_0

    .line 231
    iget-object v1, p0, Lavj;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v1, v0}, Ljava/util/PriorityQueue;->remove(Ljava/lang/Object;)Z

    .line 232
    iget-object v0, p0, Lavj;->c:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->remove(J)V

    .line 234
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 69
    iget-object v0, p0, Lavj;->d:Lavl;

    invoke-interface {v0}, Lavl;->a()Ljava/util/Set;

    move-result-object v0

    .line 73
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v2, v0

    const-string v3, "splitValue.length"

    const/4 v4, 0x2

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lcgp;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    new-instance v2, Lavk;

    const/4 v3, 0x0

    aget-object v3, v0, v3

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-direct {v2, v4, v5}, Lavk;-><init>(J)V

    const/4 v3, 0x1

    aget-object v0, v0, v3

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Lavk;->b:I

    iget-wide v4, v2, Lavk;->a:J

    invoke-direct {p0, v4, v5}, Lavj;->c(J)V

    iget-object v0, p0, Lavj;->c:Landroid/util/LongSparseArray;

    iget-wide v4, v2, Lavk;->a:J

    invoke-virtual {v0, v4, v5, v2}, Landroid/util/LongSparseArray;->append(JLjava/lang/Object;)V

    iget-object v0, p0, Lavj;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v0, v2}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 74
    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lavj;->a(JI)V

    .line 81
    return-void
.end method

.method public a(Lbrc;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 94
    invoke-virtual {p1}, Lbrc;->c()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "!musicLibrary.isEmpty()"

    invoke-static {v0, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 95
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 96
    :goto_1
    iget-object v3, p0, Lavj;->c:Landroid/util/LongSparseArray;

    invoke-virtual {v3}, Landroid/util/LongSparseArray;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 97
    iget-object v3, p0, Lavj;->c:Landroid/util/LongSparseArray;

    invoke-virtual {v3, v0}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    .line 98
    invoke-virtual {p1, v4, v5}, Lbrc;->a(J)Lboh;

    move-result-object v3

    if-nez v3, :cond_0

    .line 99
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    .line 94
    goto :goto_0

    .line 102
    :cond_2
    :goto_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 103
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lavj;->c(J)V

    .line 102
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 105
    :cond_3
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    .line 106
    sget-object v0, Lavj;->a:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x31

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Removed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " deleted tracks from frequently used list"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    :cond_4
    invoke-direct {p0}, Lavj;->b()V

    .line 109
    return-void
.end method

.method public a(I)[J
    .locals 6

    .prologue
    .line 117
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 118
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v0, p1, :cond_0

    .line 119
    iget-object v0, p0, Lavj;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavk;

    .line 120
    if-eqz v0, :cond_0

    .line 121
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 126
    :cond_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [J

    .line 127
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 128
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavk;

    .line 129
    iget-wide v4, v0, Lavk;->a:J

    aput-wide v4, v3, v1

    .line 130
    iget-object v4, p0, Lavj;->b:Ljava/util/PriorityQueue;

    invoke-virtual {v4, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 127
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 132
    :cond_1
    return-object v3
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 87
    const/16 v0, 0xa

    invoke-direct {p0, p1, p2, v0}, Lavj;->a(JI)V

    .line 88
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lavj;->d:Lavl;

    invoke-interface {v0}, Lavl;->b()V

    .line 63
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 196
    if-nez p1, :cond_1

    .line 214
    :cond_0
    :goto_0
    return v1

    .line 199
    :cond_1
    instance-of v0, p1, Lavj;

    if-eqz v0, :cond_0

    .line 202
    check-cast p1, Lavj;

    .line 205
    iget-object v0, p1, Lavj;->c:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    iget-object v2, p0, Lavj;->c:Landroid/util/LongSparseArray;

    invoke-virtual {v2}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 208
    :goto_1
    iget-object v2, p0, Lavj;->c:Landroid/util/LongSparseArray;

    invoke-virtual {v2}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 209
    iget-object v2, p1, Lavj;->c:Landroid/util/LongSparseArray;

    invoke-virtual {v2, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lavj;->c:Landroid/util/LongSparseArray;

    .line 210
    invoke-virtual {v3, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    .line 209
    invoke-static {v2, v3}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 208
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 214
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lavj;->c:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method
