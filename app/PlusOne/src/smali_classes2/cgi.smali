.class public Lcgi;
.super Lcgc;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcgc",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final b:Lcgj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcgj",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    const-class v0, Lcgi;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILcgj;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcgj",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-interface {p2, p1}, Lcgj;->b(I)[Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lcgc;-><init>([Ljava/lang/Object;)V

    .line 43
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    .line 44
    iget-object v1, p0, Lcgi;->a:[Ljava/lang/Object;

    invoke-interface {p2}, Lcgj;->b()Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v1, v0

    .line 43
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 46
    :cond_0
    iput-object p2, p0, Lcgi;->b:Lcgj;

    .line 47
    return-void
.end method


# virtual methods
.method protected b()V
    .locals 4

    .prologue
    .line 51
    iget-object v0, p0, Lcgi;->a:[Ljava/lang/Object;

    array-length v0, v0

    .line 52
    int-to-float v1, v0

    const/high16 v2, 0x3fc00000    # 1.5f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 53
    iget-object v2, p0, Lcgi;->b:Lcgj;

    invoke-interface {v2, v1}, Lcgj;->b(I)[Ljava/lang/Object;

    move-result-object v2

    .line 54
    :goto_0
    if-ge v0, v1, :cond_0

    .line 55
    iget-object v3, p0, Lcgi;->b:Lcgj;

    invoke-interface {v3}, Lcgj;->b()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v2, v0

    .line 54
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 57
    :cond_0
    iput-object v2, p0, Lcgi;->a:[Ljava/lang/Object;

    .line 61
    return-void
.end method
