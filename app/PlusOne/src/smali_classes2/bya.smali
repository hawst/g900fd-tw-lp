.class public Lbya;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lbya;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbya;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    return-void
.end method

.method private static a(Lbmd;Lbmd;)J
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 404
    const-string v0, "clipA"

    invoke-static {p0, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 405
    const-string v0, "clipB"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 406
    iget-object v0, p0, Lbmd;->e:Ljeg;

    iget-object v1, p1, Lbmd;->e:Ljeg;

    invoke-virtual {v0, v1}, Ljeg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 407
    const-wide/high16 v0, -0x8000000000000000L

    .line 411
    :goto_0
    return-wide v0

    .line 409
    :cond_0
    iget-object v0, p0, Lbmd;->f:Lbmp;

    iget-wide v0, v0, Lbmp;->c:J

    iget-object v2, p1, Lbmd;->f:Lbmp;

    iget-wide v2, v2, Lbmp;->c:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 410
    iget-object v2, p0, Lbmd;->f:Lbmp;

    iget-wide v2, v2, Lbmp;->b:J

    iget-object v4, p1, Lbmd;->f:Lbmp;

    iget-wide v4, v4, Lbmp;->b:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 411
    sub-long/2addr v0, v2

    goto :goto_0
.end method

.method public static a(Ljava/util/List;)J
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbxk;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 82
    const-wide/16 v0, 0x0

    .line 83
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxk;

    .line 84
    iget-object v0, v0, Lbxk;->a:Lbmd;

    invoke-virtual {v0}, Lbmd;->b()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 85
    goto :goto_0

    .line 86
    :cond_0
    return-wide v2
.end method

.method public static a(Ljava/util/List;Lbxn;Lbvr;Lbxe;)Landroid/util/Pair;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbxk;",
            ">;",
            "Lbxn;",
            "Lbvr;",
            "Lbxe;",
            ")",
            "Landroid/util/Pair",
            "<[F",
            "Lbxn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    .line 37
    new-array v3, v2, [F

    .line 39
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    .line 40
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 41
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxk;

    iget-object v0, v0, Lbxk;->b:Lbmr;

    .line 42
    invoke-interface {p2, v0}, Lbvr;->a(Lbmr;)F

    move-result v0

    aput v0, v3, v1

    .line 40
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 49
    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    .line 50
    new-instance v0, Lbxn;

    invoke-direct {v0, p0, p3, p1}, Lbxn;-><init>(Ljava/util/List;Lbxe;Lbxn;)V

    .line 57
    invoke-static {v3, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Collection;)Lbmg;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lbmg;"
        }
    .end annotation

    .prologue
    .line 242
    const/4 v0, 0x5

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p0, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lbmg;->c:Lbmg;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lbmg;->a:Lbmg;

    goto :goto_0
.end method

.method private static a(Lbmd;Ljava/util/List;)Lbxk;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbmd;",
            "Ljava/util/List",
            "<",
            "Lbxk;",
            ">;)",
            "Lbxk;"
        }
    .end annotation

    .prologue
    .line 387
    const-wide/16 v4, -0x1

    .line 388
    const/4 v1, 0x0

    .line 389
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxk;

    .line 390
    iget-object v2, v0, Lbxk;->a:Lbmd;

    invoke-static {p0, v2}, Lbya;->a(Lbmd;Lbmd;)J

    move-result-wide v2

    .line 391
    cmp-long v7, v2, v4

    if-lez v7, :cond_1

    :goto_1
    move-object v1, v0

    move-wide v4, v2

    .line 395
    goto :goto_0

    .line 396
    :cond_0
    return-object v1

    :cond_1
    move-object v0, v1

    move-wide v2, v4

    goto :goto_1
.end method

.method public static a(Ljava/util/List;Lbvr;Lbxs;Ljava/util/Collection;)Lbyd;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbxk;",
            ">;",
            "Lbvr;",
            "Lbxs;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Lbyd;"
        }
    .end annotation

    .prologue
    .line 211
    const-string v0, "clipMetrics"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 212
    invoke-static {p3}, Lbya;->a(Ljava/util/Collection;)Lbmg;

    move-result-object v1

    .line 213
    iget-object v0, p2, Lbxs;->d:Lbor;

    invoke-virtual {v0}, Lbor;->g()Ljava/util/List;

    move-result-object v0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-object v4, v0, Lbmd;->d:Lbmg;

    if-ne v1, v4, :cond_0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 221
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 222
    invoke-static {p0, v2}, Lbya;->b(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object p0

    .line 225
    :cond_2
    invoke-static {p0, p1, p2, v2}, Lbya;->a(Ljava/util/List;Lbvr;Lbxs;Ljava/util/List;)Lbyd;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/List;Lbvr;Lbxs;Ljava/util/List;)Lbyd;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbxk;",
            ">;",
            "Lbvr;",
            "Lbxs;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)",
            "Lbyd;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 272
    new-instance v4, Lbyd;

    invoke-direct {v4}, Lbyd;-><init>()V

    .line 273
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxk;

    .line 274
    iget-object v1, v0, Lbxk;->a:Lbmd;

    iget-object v6, v1, Lbmd;->d:Lbmg;

    sget-object v7, Lbmg;->c:Lbmg;

    if-eq v6, v7, :cond_1

    invoke-virtual {v1}, Lbmd;->b()J

    move-result-wide v6

    const-wide/32 v8, 0x16e360

    cmp-long v6, v6, v8

    if-gez v6, :cond_1

    move v1, v3

    :goto_1
    if-eqz v1, :cond_5

    .line 277
    iget-object v1, v0, Lbxk;->b:Lbmr;

    invoke-interface {p1}, Lbvr;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, v0, Lbxk;->b:Lbmr;

    invoke-virtual {v1}, Lbmr;->b()I

    move-result v1

    const/4 v6, 0x6

    if-eq v1, v6, :cond_3

    iget-object v1, v0, Lbxk;->b:Lbmr;

    invoke-virtual {v1}, Lbmr;->b()I

    move-result v1

    const/4 v6, 0x3

    if-eq v1, v6, :cond_3

    move v1, v2

    :goto_2
    if-eqz v1, :cond_4

    .line 278
    const/4 v1, 0x4

    .line 286
    :goto_3
    iget-object v6, v0, Lbxk;->a:Lbmd;

    invoke-interface {p3, v6}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 287
    or-int/lit8 v1, v1, 0x2

    .line 296
    :cond_0
    :goto_4
    invoke-virtual {v4, v0, v1}, Lbyd;->a(Lbxk;I)V

    goto :goto_0

    .line 274
    :cond_1
    invoke-interface {p3, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    iget-object v6, p2, Lbxs;->d:Lbor;

    invoke-virtual {v6, v1}, Lbor;->a(Lbmd;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v3

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    move v1, v3

    .line 277
    goto :goto_2

    .line 284
    :cond_4
    const/16 v1, 0x8

    goto :goto_3

    :cond_5
    move v1, v2

    .line 290
    goto :goto_4

    .line 298
    :cond_6
    return-object v4
.end method

.method public static a(Lbxs;Ljava/util/Collection;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxs;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lbxk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 151
    iget-object v0, p0, Lbxs;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbok;

    .line 152
    iget-object v1, p0, Lbxs;->b:Ljava/util/Map;

    .line 153
    invoke-virtual {v0}, Lbok;->a()Ljeg;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbol;

    invoke-virtual {v1}, Lbol;->a()Lbkp;

    move-result-object v4

    .line 154
    if-eqz v4, :cond_0

    .line 155
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 158
    invoke-virtual {v4, v1}, Lbkp;->a(I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmr;

    .line 159
    new-instance v7, Lbxk;

    iget-wide v8, p0, Lbxs;->e:J

    invoke-direct {v7, v1, v0, v8, v9}, Lbxk;-><init>(Lbmr;Lbok;J)V

    .line 161
    invoke-interface {v2, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 165
    :cond_2
    return-object v2
.end method

.method public static a(Ljava/util/List;J)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;J)",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 67
    const-wide/16 v0, 0x0

    .line 68
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-wide v2, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 69
    cmp-long v1, v2, p1

    if-gez v1, :cond_0

    .line 70
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 73
    invoke-virtual {v0}, Lbmd;->b()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 74
    goto :goto_0

    .line 75
    :cond_0
    return-object v4
.end method

.method public static a(Ljava/util/List;Ljava/util/Collection;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/List",
            "<TT;>;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 179
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 180
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 181
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 183
    :cond_0
    return-object v1
.end method

.method public static a(Ljava/util/List;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbxk;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 253
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxk;

    .line 254
    iget-object v0, v0, Lbxk;->a:Lbmd;

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 256
    :cond_0
    return-void
.end method

.method public static b(Ljava/util/List;)J
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 93
    const-wide/16 v0, 0x0

    .line 94
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 95
    invoke-virtual {v0}, Lbmd;->b()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 96
    goto :goto_0

    .line 97
    :cond_0
    return-wide v2
.end method

.method private static b(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbxk;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lbxk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 328
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, p0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 329
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 333
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 334
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbxk;

    .line 338
    iget-object v6, v1, Lbxk;->a:Lbmd;

    invoke-static {v6, v0}, Lbya;->a(Lbmd;Lbmd;)J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_0

    .line 339
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 346
    :cond_1
    invoke-static {v0, v4}, Lbya;->a(Lbmd;Ljava/util/List;)Lbxk;

    move-result-object v1

    .line 356
    if-nez v1, :cond_2

    .line 357
    sget-object v1, Lbya;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x22

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "No overlapping clip for inclusion "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 359
    :cond_2
    new-instance v4, Lbmr;

    iget-object v5, v0, Lbmd;->f:Lbmp;

    iget-object v6, v1, Lbxk;->b:Lbmr;

    invoke-virtual {v6}, Lbmr;->c()Lbky;

    move-result-object v6

    iget-object v7, v1, Lbxk;->b:Lbmr;

    invoke-virtual {v7}, Lbmr;->b()I

    move-result v7

    invoke-direct {v4, v5, v6, v7}, Lbmr;-><init>(Lbmp;Lbky;I)V

    new-instance v5, Lbxk;

    iget-object v6, v1, Lbxk;->c:Lbok;

    invoke-direct {v5, v0, v4, v6}, Lbxk;-><init>(Lbmd;Lbmr;Lbok;)V

    .line 361
    invoke-interface {v2, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 362
    invoke-interface {v2, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 365
    :cond_3
    return-object v2
.end method

.method public static c(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbxk;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 170
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 171
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxk;

    .line 172
    iget-object v0, v0, Lbxk;->a:Lbmd;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 174
    :cond_0
    return-object v1
.end method
