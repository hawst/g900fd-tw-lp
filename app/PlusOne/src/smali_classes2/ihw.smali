.class public final Lihw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lihr;


# instance fields
.field final a:Ljava/lang/String;

.field b:Z

.field c:Lgfz;

.field d:Lihs;

.field private final e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-object p1, p0, Lihw;->e:Landroid/content/Context;

    .line 37
    iput-object p2, p0, Lihw;->a:Ljava/lang/String;

    .line 38
    iput-boolean p3, p0, Lihw;->b:Z

    .line 39
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lihw;->c:Lgfz;

    invoke-interface {v0}, Lgfz;->a()V

    .line 67
    return-void
.end method

.method public a(D)V
    .locals 3

    .prologue
    .line 113
    :try_start_0
    sget-object v0, Lgdk;->c:Lgdn;

    iget-object v1, p0, Lihw;->c:Lgfz;

    invoke-interface {v0, v1, p1, p2}, Lgdn;->a(Lgfz;D)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    :goto_0
    return-void

    .line 114
    :catch_0
    move-exception v0

    .line 115
    const-string v1, "BaseGcoreCastClientImpl"

    const-string v2, "unable to set volume"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public a(Lihs;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lihw;->d:Lihs;

    .line 44
    return-void
.end method

.method public a(Liic;)V
    .locals 3

    .prologue
    .line 49
    check-cast p1, Liif;

    .line 54
    invoke-virtual {p1}, Liif;->b()Lcom/google/android/gms/cast/CastDevice;

    move-result-object v0

    new-instance v1, Lihz;

    invoke-direct {v1, p0}, Lihz;-><init>(Lihw;)V

    invoke-static {v0, v1}, Lgdr;->a(Lcom/google/android/gms/cast/CastDevice;Lgdt;)Lgds;

    move-result-object v0

    .line 56
    new-instance v1, Lgga;

    iget-object v2, p0, Lihw;->e:Landroid/content/Context;

    invoke-direct {v1, v2}, Lgga;-><init>(Landroid/content/Context;)V

    sget-object v2, Lgdk;->b:Lgfo;

    .line 57
    invoke-virtual {v0}, Lgds;->a()Lgdr;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lgga;->a(Lgfo;Lgfq;)Lgga;

    move-result-object v0

    new-instance v1, Liia;

    invoke-direct {v1, p0}, Liia;-><init>(Lihw;)V

    .line 58
    invoke-virtual {v0, v1}, Lgga;->a(Lggb;)Lgga;

    move-result-object v0

    new-instance v1, Liib;

    invoke-direct {v1, p0}, Liib;-><init>(Lihw;)V

    .line 59
    invoke-virtual {v0, v1}, Lgga;->a(Lggc;)Lgga;

    move-result-object v0

    .line 61
    invoke-virtual {v0}, Lgga;->b()Lgfz;

    move-result-object v0

    iput-object v0, p0, Lihw;->c:Lgfz;

    .line 62
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 99
    :try_start_0
    sget-object v0, Lgdk;->c:Lgdn;

    iget-object v1, p0, Lihw;->c:Lgfz;

    invoke-interface {v0, v1, p1}, Lgdn;->a(Lgfz;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 103
    :goto_0
    return-void

    .line 100
    :catch_0
    move-exception v0

    .line 101
    const-string v1, "BaseGcoreCastClientImpl"

    const-string v2, "Error removing callback."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Liht;)V
    .locals 3

    .prologue
    .line 83
    :try_start_0
    sget-object v0, Lgdk;->c:Lgdn;

    iget-object v1, p0, Lihw;->c:Lgfz;

    new-instance v2, Lihx;

    invoke-direct {v2, p2}, Lihx;-><init>(Liht;)V

    invoke-interface {v0, v1, p1, v2}, Lgdn;->a(Lgfz;Ljava/lang/String;Lgdu;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :goto_0
    return-void

    .line 91
    :catch_0
    move-exception v0

    .line 92
    const-string v1, "BaseGcoreCastClientImpl"

    const-string v2, "unable to register message received callbacks"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 121
    sget-object v0, Lgdk;->c:Lgdn;

    iget-object v1, p0, Lihw;->c:Lgfz;

    invoke-interface {v0, v1, p1, p2}, Lgdn;->a(Lgfz;Ljava/lang/String;Ljava/lang/String;)Lggd;

    .line 122
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lihw;->c:Lgfz;

    invoke-interface {v0}, Lgfz;->c()V

    .line 72
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lihw;->c:Lgfz;

    invoke-interface {v0}, Lgfz;->d()Z

    move-result v0

    return v0
.end method

.method public d()D
    .locals 2

    .prologue
    .line 107
    sget-object v0, Lgdk;->c:Lgdn;

    iget-object v1, p0, Lihw;->c:Lgfz;

    invoke-interface {v0, v1}, Lgdn;->a(Lgfz;)D

    move-result-wide v0

    return-wide v0
.end method

.method public e()Lgfz;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lihw;->c:Lgfz;

    return-object v0
.end method
