.class public final Lgsp;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lgth;

.field private b:Lgtb;


# direct methods
.method protected constructor <init>(Lgth;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lgih;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgth;

    iput-object v0, p0, Lgsp;->a:Lgth;

    return-void
.end method


# virtual methods
.method a()Lgth;
    .locals 1

    iget-object v0, p0, Lgsp;->a:Lgth;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/maps/model/CircleOptions;)Lgwr;
    .locals 2

    :try_start_0
    new-instance v0, Lgwr;

    iget-object v1, p0, Lgsp;->a:Lgth;

    invoke-interface {v1, p1}, Lgth;->a(Lcom/google/android/gms/maps/model/CircleOptions;)Lgxi;

    move-result-object v1

    invoke-direct {v0, v1}, Lgwr;-><init>(Lgxi;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lgwt;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lgsp;->a:Lgth;

    invoke-interface {v0, p1}, Lgth;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lgxr;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Lgwt;

    invoke-direct {v0, v1}, Lgwt;-><init>(Lgxr;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(I)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lgsp;->a:Lgth;

    invoke-interface {v0, p1}, Lgth;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lgsn;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lgsp;->a:Lgth;

    invoke-virtual {p1}, Lgsn;->a()Lgjb;

    move-result-object v1

    invoke-interface {v0, v1}, Lgth;->a(Lgjb;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lgst;)V
    .locals 2

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lgsp;->a:Lgth;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lgth;->a(Lgus;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lgsp;->a:Lgth;

    new-instance v1, Lgsr;

    invoke-direct {v1, p1}, Lgsr;-><init>(Lgst;)V

    invoke-interface {v0, v1}, Lgth;->a(Lgus;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lgsu;)V
    .locals 2

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lgsp;->a:Lgth;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lgth;->a(Lgve;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lgsp;->a:Lgth;

    new-instance v1, Lgss;

    invoke-direct {v1, p1}, Lgss;-><init>(Lgsu;)V

    invoke-interface {v0, v1}, Lgth;->a(Lgve;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lgsv;)V
    .locals 2

    if-nez p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lgsp;->a:Lgth;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lgth;->a(Lgvn;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lgsp;->a:Lgth;

    new-instance v1, Lgsq;

    invoke-direct {v1, p1}, Lgsq;-><init>(Lgsv;)V

    invoke-interface {v0, v1}, Lgth;->a(Lgvn;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Z)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lgsp;->a:Lgth;

    invoke-interface {v0, p1}, Lgth;->c(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final b()Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lgsp;->a:Lgth;

    invoke-interface {v0}, Lgth;->a()Lcom/google/android/gms/maps/model/CameraPosition;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final b(Lgsn;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lgsp;->a:Lgth;

    invoke-virtual {p1}, Lgsn;->a()Lgjb;

    move-result-object v1

    invoke-interface {v0, v1}, Lgth;->b(Lgjb;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final c()I
    .locals 2

    :try_start_0
    iget-object v0, p0, Lgsp;->a:Lgth;

    invoke-interface {v0}, Lgth;->f()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final d()Lgtb;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lgsp;->b:Lgtb;

    if-nez v0, :cond_0

    new-instance v0, Lgtb;

    iget-object v1, p0, Lgsp;->a:Lgth;

    invoke-interface {v1}, Lgth;->k()Lguf;

    move-result-object v1

    invoke-direct {v0, v1}, Lgtb;-><init>(Lguf;)V

    iput-object v0, p0, Lgsp;->b:Lgtb;

    :cond_0
    iget-object v0, p0, Lgsp;->b:Lgtb;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final e()Lgsx;
    .locals 2

    :try_start_0
    new-instance v0, Lgsx;

    iget-object v1, p0, Lgsp;->a:Lgth;

    invoke-interface {v1}, Lgth;->l()Lgtt;

    move-result-object v1

    invoke-direct {v0, v1}, Lgsx;-><init>(Lgtt;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
