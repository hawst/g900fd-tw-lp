.class public final enum Laia;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Laia;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Laia;

.field public static final enum b:Laia;

.field public static final enum c:Laia;

.field public static final enum d:Laia;

.field public static final enum e:Laia;

.field private static final synthetic g:[Laia;


# instance fields
.field public final f:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 61
    new-instance v0, Laia;

    const-string v1, "OPAQUE"

    const-class v2, Ljava/lang/Object;

    invoke-direct {v0, v1, v3, v2}, Laia;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Laia;->a:Laia;

    .line 63
    new-instance v0, Laia;

    const-string v1, "FLOAT_MATRIX"

    const-class v2, Lbmn;

    invoke-direct {v0, v1, v4, v2}, Laia;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Laia;->b:Laia;

    .line 66
    new-instance v0, Laia;

    const-string v1, "STABILIZATION"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v5, v2}, Laia;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Laia;->c:Laia;

    .line 68
    new-instance v0, Laia;

    const-string v1, "COLOR_CORRECT"

    const-class v2, Lbbs;

    invoke-direct {v0, v1, v6, v2}, Laia;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Laia;->d:Laia;

    .line 70
    new-instance v0, Laia;

    const-string v1, "INTEGER"

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-direct {v0, v1, v7, v2}, Laia;-><init>(Ljava/lang/String;ILjava/lang/Class;)V

    sput-object v0, Laia;->e:Laia;

    .line 59
    const/4 v0, 0x5

    new-array v0, v0, [Laia;

    sget-object v1, Laia;->a:Laia;

    aput-object v1, v0, v3

    sget-object v1, Laia;->b:Laia;

    aput-object v1, v0, v4

    sget-object v1, Laia;->c:Laia;

    aput-object v1, v0, v5

    sget-object v1, Laia;->d:Laia;

    aput-object v1, v0, v6

    sget-object v1, Laia;->e:Laia;

    aput-object v1, v0, v7

    sput-object v0, Laia;->g:[Laia;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 77
    iput-object p3, p0, Laia;->f:Ljava/lang/Class;

    .line 78
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Laia;
    .locals 1

    .prologue
    .line 59
    const-class v0, Laia;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Laia;

    return-object v0
.end method

.method public static values()[Laia;
    .locals 1

    .prologue
    .line 59
    sget-object v0, Laia;->g:[Laia;

    invoke-virtual {v0}, [Laia;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laia;

    return-object v0
.end method
