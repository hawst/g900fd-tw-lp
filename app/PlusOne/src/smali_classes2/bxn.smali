.class public Lbxn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:[[F

.field private final b:[Lbxk;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lbxn;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;Lbxe;Lbxn;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbxk;",
            ">;",
            "Lbxe;",
            "Lbxn;",
            ")V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 44
    filled-new-array {v0, v0}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[F

    iput-object v0, p0, Lbxn;->a:[[F

    .line 45
    const/4 v0, 0x0

    new-array v0, v0, [Lbxk;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbxk;

    iput-object v0, p0, Lbxn;->b:[Lbxk;

    .line 46
    if-eqz p3, :cond_0

    invoke-direct {p0, p3}, Lbxn;->a(Lbxn;)V

    :cond_0
    invoke-direct {p0, p2}, Lbxn;->a(Lbxe;)V

    .line 47
    return-void
.end method

.method private a(Lbxe;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 116
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lbxn;->b:[Lbxk;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 117
    iget-object v1, p0, Lbxn;->a:[[F

    aget-object v1, v1, v0

    aput v8, v1, v0

    .line 118
    add-int/lit8 v1, v0, 0x1

    :goto_1
    iget-object v2, p0, Lbxn;->b:[Lbxk;

    array-length v2, v2

    if-ge v1, v2, :cond_2

    .line 119
    iget-object v2, p0, Lbxn;->a:[[F

    aget-object v2, v2, v0

    aget v2, v2, v1

    cmpg-float v2, v2, v8

    if-gtz v2, :cond_0

    .line 120
    iget-object v2, p0, Lbxn;->a:[[F

    aget-object v3, v2, v0

    iget-object v2, p0, Lbxn;->b:[Lbxk;

    aget-object v2, v2, v0

    iget-object v2, v2, Lbxk;->b:Lbmr;

    iget-object v4, p0, Lbxn;->b:[Lbxk;

    aget-object v4, v4, v0

    iget-object v4, v4, Lbxk;->c:Lbok;

    iget-object v5, p0, Lbxn;->b:[Lbxk;

    aget-object v5, v5, v1

    iget-object v5, v5, Lbxk;->b:Lbmr;

    iget-object v6, p0, Lbxn;->b:[Lbxk;

    aget-object v6, v6, v1

    iget-object v6, v6, Lbxk;->c:Lbok;

    invoke-interface {p1, v4, v2, v6, v5}, Lbxe;->b(Lbok;Lbmr;Lbok;Lbmr;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {p1, v4, v2, v6, v5}, Lbxe;->a(Lbok;Lbmr;Lbok;Lbmr;)F

    move-result v2

    :goto_2
    aput v2, v3, v1

    .line 121
    iget-object v2, p0, Lbxn;->a:[[F

    aget-object v2, v2, v1

    iget-object v3, p0, Lbxn;->a:[[F

    aget-object v3, v3, v0

    aget v3, v3, v1

    aput v3, v2, v0

    .line 118
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 120
    :cond_1
    const v2, 0x7f7fffff    # Float.MAX_VALUE

    goto :goto_2

    .line 116
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 128
    :cond_3
    return-void
.end method

.method private a(Lbxn;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 71
    iget-object v0, p1, Lbxn;->b:[Lbxk;

    array-length v0, v0

    new-array v5, v0, [I

    iget-object v0, p0, Lbxn;->b:[Lbxk;

    array-length v0, v0

    invoke-static {v0}, Lcfm;->a(I)Ljava/util/HashMap;

    move-result-object v6

    iget-object v3, p0, Lbxn;->b:[Lbxk;

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v7, v3, v0

    iget-object v7, v7, Lbxk;->a:Lbmd;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v7, p1, Lbxn;->b:[Lbxk;

    array-length v8, v7

    move v2, v1

    move v3, v1

    :goto_1
    if-ge v2, v8, :cond_2

    aget-object v0, v7, v2

    iget-object v0, v0, Lbxk;->a:Lbmd;

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    add-int/lit8 v4, v3, 0x1

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_2
    aput v0, v5, v3

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v3, v4

    goto :goto_1

    :cond_1
    const/4 v0, -0x1

    goto :goto_2

    :cond_2
    move v0, v1

    .line 72
    :goto_3
    iget-object v1, p1, Lbxn;->b:[Lbxk;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 73
    add-int/lit8 v1, v0, 0x1

    :goto_4
    iget-object v2, p1, Lbxn;->b:[Lbxk;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 74
    aget v2, v5, v0

    .line 75
    aget v3, v5, v1

    .line 76
    if-ltz v2, :cond_3

    if-ltz v3, :cond_3

    .line 77
    iget-object v4, p0, Lbxn;->a:[[F

    aget-object v4, v4, v2

    iget-object v6, p1, Lbxn;->a:[[F

    aget-object v6, v6, v0

    aget v6, v6, v1

    aput v6, v4, v3

    .line 78
    iget-object v4, p0, Lbxn;->a:[[F

    aget-object v4, v4, v3

    iget-object v6, p0, Lbxn;->a:[[F

    aget-object v6, v6, v2

    aget v3, v6, v3

    aput v3, v4, v2

    .line 73
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 72
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 82
    :cond_5
    return-void
.end method


# virtual methods
.method public a()[[F
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lbxn;->a:[[F

    return-object v0
.end method
