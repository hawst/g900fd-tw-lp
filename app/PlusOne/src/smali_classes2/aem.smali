.class public final Laem;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;


# instance fields
.field private synthetic a:Landroidx/media/filterpacks/video/MediaPlayerSource;


# direct methods
.method public constructor <init>(Landroidx/media/filterpacks/video/MediaPlayerSource;)V
    .locals 0

    .prologue
    .line 384
    iput-object p1, p0, Laem;->a:Landroidx/media/filterpacks/video/MediaPlayerSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFrameAvailable(Landroid/graphics/SurfaceTexture;)V
    .locals 3

    .prologue
    .line 387
    iget-object v0, p0, Laem;->a:Landroidx/media/filterpacks/video/MediaPlayerSource;

    invoke-static {v0}, Landroidx/media/filterpacks/video/MediaPlayerSource;->d(Landroidx/media/filterpacks/video/MediaPlayerSource;)Z

    .line 388
    iget-object v0, p0, Laem;->a:Landroidx/media/filterpacks/video/MediaPlayerSource;

    invoke-static {v0}, Landroidx/media/filterpacks/video/MediaPlayerSource;->b(Landroidx/media/filterpacks/video/MediaPlayerSource;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 390
    :try_start_0
    iget-object v0, p0, Laem;->a:Landroidx/media/filterpacks/video/MediaPlayerSource;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Landroidx/media/filterpacks/video/MediaPlayerSource;->b(Landroidx/media/filterpacks/video/MediaPlayerSource;Z)Z

    .line 391
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 392
    iget-object v0, p0, Laem;->a:Landroidx/media/filterpacks/video/MediaPlayerSource;

    const-string v1, "New frame: wakeUp"

    invoke-static {v0, v1}, Landroidx/media/filterpacks/video/MediaPlayerSource;->a(Landroidx/media/filterpacks/video/MediaPlayerSource;Ljava/lang/String;)V

    .line 393
    iget-object v0, p0, Laem;->a:Landroidx/media/filterpacks/video/MediaPlayerSource;

    invoke-static {v0}, Landroidx/media/filterpacks/video/MediaPlayerSource;->e(Landroidx/media/filterpacks/video/MediaPlayerSource;)V

    .line 394
    return-void

    .line 391
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
