.class final Larn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Latx;


# instance fields
.field private synthetic a:Laqy;


# direct methods
.method constructor <init>(Laqy;)V
    .locals 0

    .prologue
    .line 736
    iput-object p1, p0, Larn;->a:Laqy;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 825
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->h(Laqy;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Larg;

    invoke-interface {v0}, Larg;->n_()V

    .line 826
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 785
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->b(Laqy;)Lawh;

    move-result-object v0

    invoke-interface {v0}, Lawh;->az()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 786
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->b(Laqy;)Lawh;

    move-result-object v0

    invoke-interface {v0}, Lawh;->aC()Z

    move-result v0

    if-nez v0, :cond_0

    .line 793
    const-string v0, "unexpected incomplete save"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 795
    :cond_0
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->c(Laqy;)Lcdu;

    move-result-object v0

    iget-object v1, p0, Larn;->a:Laqy;

    invoke-static {v1}, Laqy;->b(Laqy;)Lawh;

    move-result-object v1

    invoke-interface {v1}, Lawh;->aI()Latu;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcdu;->a(Latu;)V

    .line 802
    :goto_0
    return-void

    .line 799
    :cond_1
    invoke-static {}, Laqy;->i()Ljava/lang/String;

    .line 800
    invoke-direct {p0}, Larn;->b()V

    goto :goto_0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 740
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->h(Laqy;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Larg;

    invoke-interface {v0, p1}, Larg;->f(I)V

    .line 741
    return-void
.end method

.method public a(J)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 745
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->c(Laqy;)Lcdu;

    move-result-object v0

    iget-object v1, p0, Larn;->a:Laqy;

    .line 746
    invoke-static {v1}, Laqy;->b(Laqy;)Lawh;

    move-result-object v1

    invoke-interface {v1}, Lawh;->aI()Latu;

    move-result-object v1

    .line 745
    invoke-virtual {v0, p1, p2, v1}, Lcdu;->a(JLatu;)V

    .line 748
    new-instance v1, Ljava/io/File;

    iget-object v0, p0, Larn;->a:Laqy;

    .line 749
    invoke-static {v0}, Laqy;->b(Laqy;)Lawh;

    move-result-object v0

    invoke-interface {v0}, Lawh;->aE()Ljava/lang/String;

    move-result-object v0

    const-string v2, "mState.getSavingOutputFileName()"

    .line 748
    invoke-static {v0, v2, v6}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 751
    new-instance v2, Ljava/io/File;

    iget-object v0, p0, Larn;->a:Laqy;

    .line 752
    invoke-static {v0}, Laqy;->b(Laqy;)Lawh;

    move-result-object v0

    invoke-interface {v0}, Lawh;->aF()Ljava/lang/String;

    move-result-object v0

    const-string v3, "mState.getSavingTemporaryFileName()"

    .line 751
    invoke-static {v0, v3, v6}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 754
    invoke-virtual {v2, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    .line 755
    if-nez v0, :cond_1

    .line 756
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    .line 757
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 758
    invoke-static {}, Laqy;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "failed to rename output file"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 762
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->h(Laqy;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Larg;

    invoke-interface {v0}, Larg;->n_()V

    .line 763
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->b(Laqy;)Lawh;

    move-result-object v0

    invoke-interface {v0, v5}, Lawh;->o(Z)V

    .line 764
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->b(Laqy;)Lawh;

    move-result-object v0

    invoke-interface {v0, v4}, Lawh;->n(Z)V

    .line 767
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->b(Laqy;)Lawh;

    move-result-object v0

    invoke-interface {v0}, Lawh;->aB()Z

    move-result v0

    if-nez v0, :cond_0

    .line 768
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->b(Laqy;)Lawh;

    move-result-object v0

    invoke-interface {v0, v5}, Lawh;->m(Z)V

    .line 769
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->h(Laqy;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Larg;

    invoke-interface {v0}, Larg;->t_()V

    .line 772
    :cond_0
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->b(Laqy;)Lawh;

    move-result-object v0

    invoke-interface {v0, v4}, Lawh;->l(Z)V

    .line 773
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-virtual {v0}, Laqy;->d()V

    .line 781
    :goto_0
    return-void

    .line 775
    :cond_1
    iget-object v0, p0, Larn;->a:Laqy;

    .line 776
    invoke-static {v0}, Laqy;->m(Laqy;)Landroid/content/Context;

    move-result-object v0

    new-array v2, v5, [Ljava/lang/String;

    .line 777
    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v4

    new-array v1, v5, [Ljava/lang/String;

    .line 778
    invoke-static {}, Lath;->b()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v4

    new-instance v3, Lard;

    iget-object v4, p0, Larn;->a:Laqy;

    invoke-direct {v3, v4}, Lard;-><init>(Laqy;)V

    .line 775
    invoke-static {v0, v2, v1, v3}, Landroid/media/MediaScannerConnection;->scanFile(Landroid/content/Context;[Ljava/lang/String;[Ljava/lang/String;Landroid/media/MediaScannerConnection$OnScanCompletedListener;)V

    goto :goto_0
.end method

.method public a(ZLcdx;)V
    .locals 2

    .prologue
    .line 807
    invoke-direct {p0}, Larn;->b()V

    .line 808
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->l(Laqy;)V

    .line 809
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->b(Laqy;)Lawh;

    move-result-object v0

    invoke-interface {v0}, Lawh;->az()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 810
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->b(Laqy;)Lawh;

    move-result-object v0

    invoke-interface {v0}, Lawh;->aB()Z

    move-result v0

    if-nez v0, :cond_0

    .line 811
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->b(Laqy;)Lawh;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lawh;->m(Z)V

    .line 812
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->h(Laqy;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Larg;

    invoke-interface {v0}, Larg;->t_()V

    .line 814
    :cond_0
    if-eqz p1, :cond_1

    .line 815
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->n(Laqy;)Laqb;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 816
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->n(Laqy;)Laqb;

    move-result-object v0

    invoke-interface {v0}, Laqb;->a()V

    .line 819
    :cond_1
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->c(Laqy;)Lcdu;

    move-result-object v0

    iget-object v1, p0, Larn;->a:Laqy;

    invoke-static {v1}, Laqy;->b(Laqy;)Lawh;

    move-result-object v1

    invoke-interface {v1}, Lawh;->aI()Latu;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcdu;->a(Latu;Lcdx;)V

    .line 820
    iget-object v0, p0, Larn;->a:Laqy;

    invoke-static {v0}, Laqy;->b(Laqy;)Lawh;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lawh;->l(Z)V

    .line 822
    :cond_2
    return-void
.end method
