.class public final Liwv;
.super Llol;
.source "PG"

# interfaces
.implements Livw;


# instance fields
.field private N:Liwa;

.field private O:Lhei;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Llol;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Liwg;)V
    .locals 5

    .prologue
    .line 36
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 37
    iget-object v0, p0, Liwv;->O:Lhei;

    invoke-interface {v0}, Lhei;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 38
    iget-object v3, p0, Liwv;->O:Lhei;

    invoke-interface {v3, v0}, Lhei;->a(I)Lhej;

    move-result-object v3

    .line 39
    const-string v4, "logged_in"

    invoke-interface {v3, v4}, Lhej;->c(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "is_managed_account"

    .line 40
    invoke-interface {v3, v4}, Lhej;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 41
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 45
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_2

    .line 46
    iget-object v0, p0, Liwv;->N:Liwa;

    invoke-interface {v0}, Liwa;->b()V

    .line 62
    :goto_1
    return-void

    .line 48
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [I

    .line 49
    const/4 v0, 0x0

    move v1, v0

    :goto_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 50
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v3, v1

    .line 49
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 53
    :cond_3
    invoke-virtual {p0}, Liwv;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 54
    if-eqz v0, :cond_4

    const-string v1, "dialog_title"

    .line 55
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 58
    :goto_3
    invoke-virtual {p0}, Liwv;->q()Lae;

    move-result-object v1

    .line 59
    if-eqz v0, :cond_5

    :goto_4
    const/4 v2, 0x1

    .line 58
    invoke-static {v1, v0, v3, v2}, Liwt;->a(Lae;Ljava/lang/String;[IZ)V

    goto :goto_1

    .line 55
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 59
    :cond_5
    iget-object v0, p0, Liwv;->at:Llnl;

    const v2, 0x7f0a01b2

    invoke-virtual {v0, v2}, Llnl;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 29
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 30
    iget-object v0, p0, Liwv;->au:Llnh;

    const-class v1, Liwa;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwa;

    iput-object v0, p0, Liwv;->N:Liwa;

    .line 31
    iget-object v0, p0, Liwv;->au:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Liwv;->O:Lhei;

    .line 32
    return-void
.end method
