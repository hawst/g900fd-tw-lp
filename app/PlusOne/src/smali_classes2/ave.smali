.class final Lave;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laul;


# instance fields
.field private synthetic a:Lava;


# direct methods
.method constructor <init>(Lava;)V
    .locals 0

    .prologue
    .line 364
    iput-object p1, p0, Lave;->a:Lava;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lbrc;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 368
    iget-object v0, p0, Lave;->a:Lava;

    invoke-static {v0}, Lava;->i(Lava;)V

    .line 369
    iget-object v0, p0, Lave;->a:Lava;

    sget-object v1, Lavh;->a:Lavh;

    invoke-static {v0, v1}, Lava;->a(Lava;Lavh;)Lavh;

    .line 370
    iget-object v0, p0, Lave;->a:Lava;

    invoke-static {v0, v2}, Lava;->a(Lava;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    .line 371
    iget-object v0, p0, Lave;->a:Lava;

    invoke-static {v0}, Lava;->b(Lava;)Lawi;

    move-result-object v0

    invoke-interface {v0, p1}, Lawi;->a(Lbrc;)V

    .line 373
    iget-object v0, p0, Lave;->a:Lava;

    invoke-static {v0}, Lava;->b(Lava;)Lawi;

    move-result-object v0

    invoke-interface {v0}, Lawi;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p0, Lave;->a:Lava;

    invoke-static {v0}, Lava;->b(Lava;)Lawi;

    move-result-object v0

    invoke-interface {v0}, Lawi;->q()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Lbrc;->a(J)Lboh;

    move-result-object v0

    .line 375
    if-nez v0, :cond_1

    .line 376
    iget-object v0, p0, Lave;->a:Lava;

    invoke-static {v0}, Lava;->b(Lava;)Lawi;

    move-result-object v0

    invoke-interface {v0, v2}, Lawi;->a(Lboh;)V

    .line 377
    invoke-static {}, Lava;->f()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lave;->a:Lava;

    .line 378
    invoke-static {v1}, Lava;->b(Lava;)Lawi;

    move-result-object v1

    invoke-interface {v1}, Lawi;->q()J

    move-result-wide v2

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x61

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "On restoring state, previously selected soundtrack "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was not found in library."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 377
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    :cond_0
    :goto_0
    iget-object v0, p0, Lave;->a:Lava;

    invoke-virtual {v0}, Lava;->c()V

    .line 384
    iget-object v0, p0, Lave;->a:Lava;

    invoke-static {v0}, Lava;->c(Lava;)V

    .line 385
    return-void

    .line 380
    :cond_1
    iget-object v1, p0, Lave;->a:Lava;

    invoke-static {v1}, Lava;->b(Lava;)Lawi;

    move-result-object v1

    invoke-interface {v1, v0}, Lawi;->a(Lboh;)V

    goto :goto_0
.end method
