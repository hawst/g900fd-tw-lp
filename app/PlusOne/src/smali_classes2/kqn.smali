.class public final Lkqn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnx;
.implements Llqz;
.implements Llrb;
.implements Llrg;


# instance fields
.field private final a:Landroid/app/Activity;

.field private b:Lhee;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lkqn;->a:Landroid/app/Activity;

    .line 34
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 35
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lkqn;->b:Lhee;

    .line 41
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lkqn;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lkqn;->c:I

    .line 46
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lkqn;->a:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v1, v0, Landroid/content/res/Configuration;->orientation:I

    iget v0, p0, Lkqn;->c:I

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput v1, p0, Lkqn;->c:I

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lkqn;->b:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkqn;->a:Landroid/app/Activity;

    const/16 v1, 0x13

    invoke-static {v0, v1}, Lhly;->a(Landroid/content/Context;I)V

    .line 53
    :cond_0
    return-void

    .line 50
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
