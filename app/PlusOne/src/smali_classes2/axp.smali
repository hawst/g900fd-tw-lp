.class public final Laxp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lalh;


# instance fields
.field private final a:Ljava/util/EnumMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumMap",
            "<",
            "Laxq;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Laxs;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Laxr;

.field private final d:Lcgt;

.field private e:J

.field private f:J


# direct methods
.method public constructor <init>(Laxr;Lcgt;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/EnumMap;

    const-class v2, Laxq;

    invoke-direct {v0, v2}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Laxp;->a:Ljava/util/EnumMap;

    .line 57
    const-class v0, Laxs;

    invoke-static {v0}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v0

    iput-object v0, p0, Laxp;->b:Ljava/util/EnumSet;

    .line 68
    const-string v0, "storage"

    invoke-static {p1, v0, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxr;

    iput-object v0, p0, Laxp;->c:Laxr;

    .line 69
    const-string v0, "systemClock"

    invoke-static {p2, v0, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgt;

    iput-object v0, p0, Laxp;->d:Lcgt;

    .line 70
    iget-object v0, p0, Laxp;->b:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->clear()V

    .line 71
    invoke-static {}, Laxq;->values()[Laxq;

    move-result-object v2

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 72
    iget-object v5, p0, Laxp;->a:Ljava/util/EnumMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 74
    :cond_0
    return-void
.end method

.method private b(Laxq;)I
    .locals 3

    .prologue
    .line 148
    iget-object v0, p0, Laxp;->a:Ljava/util/EnumMap;

    invoke-virtual {v0, p1}, Ljava/util/EnumMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    const-string v1, "times event occurred"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method private b()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 163
    iget-object v0, p0, Laxp;->c:Laxr;

    const-string v2, "SaveCount"

    iget-wide v4, p0, Laxp;->f:J

    invoke-interface {v0, v2, v4, v5}, Laxr;->a(Ljava/lang/String;J)V

    .line 164
    iget-object v0, p0, Laxp;->c:Laxr;

    const-string v2, "LastMessageTimestamp"

    iget-wide v4, p0, Laxp;->e:J

    invoke-interface {v0, v2, v4, v5}, Laxr;->a(Ljava/lang/String;J)V

    .line 165
    invoke-static {}, Laxq;->values()[Laxq;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 166
    iget-object v6, p0, Laxp;->c:Laxr;

    const-string v0, "Event_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5}, Laxq;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_0

    invoke-virtual {v7, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-direct {p0, v5}, Laxp;->b(Laxq;)I

    move-result v5

    int-to-long v8, v5

    invoke-interface {v6, v0, v8, v9}, Laxr;->a(Ljava/lang/String;J)V

    .line 165
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 166
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 168
    :cond_1
    invoke-static {}, Laxs;->values()[Laxs;

    move-result-object v4

    array-length v5, v4

    :goto_2
    if-ge v1, v5, :cond_4

    aget-object v2, v4, v1

    .line 169
    iget-object v6, p0, Laxp;->c:Laxr;

    const-string v0, "Message_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2}, Laxs;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    iget-object v3, p0, Laxp;->b:Ljava/util/EnumSet;

    .line 170
    invoke-virtual {v3, v2}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-wide/16 v2, 0x1

    .line 169
    :goto_4
    invoke-interface {v6, v0, v2, v3}, Laxr;->a(Ljava/lang/String;J)V

    .line 168
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 169
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 170
    :cond_3
    const-wide/16 v2, 0x0

    goto :goto_4

    .line 172
    :cond_4
    return-void
.end method

.method private b(Laxs;)Z
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Laxp;->b:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private c(Laxq;)Z
    .locals 1

    .prologue
    .line 153
    invoke-direct {p0, p1}, Laxp;->b(Laxq;)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lawg;)Laxs;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 126
    iget-object v1, p0, Laxp;->d:Lcgt;

    invoke-virtual {v1}, Lcgt;->b()J

    move-result-wide v2

    iget-wide v4, p0, Laxp;->e:J

    sub-long/2addr v2, v4

    .line 127
    sget-object v1, Laxq;->i:Laxq;

    invoke-direct {p0, v1}, Laxp;->b(Laxq;)I

    move-result v1

    int-to-long v4, v1

    iget-wide v6, p0, Laxp;->f:J

    sub-long/2addr v4, v6

    .line 129
    const-wide/32 v6, 0x36ee80

    cmp-long v1, v2, v6

    if-gez v1, :cond_1

    const-wide/16 v2, 0x2

    cmp-long v1, v4, v2

    if-gez v1, :cond_1

    .line 143
    :cond_0
    :goto_0
    return-object v0

    .line 134
    :cond_1
    sget-object v1, Laxq;->e:Laxq;

    invoke-direct {p0, v1}, Laxp;->c(Laxq;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Laxs;->a:Laxs;

    invoke-direct {p0, v1}, Laxp;->b(Laxs;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 135
    sget-object v0, Laxs;->a:Laxs;

    goto :goto_0

    .line 138
    :cond_2
    sget-object v1, Laxq;->i:Laxq;

    invoke-direct {p0, v1}, Laxp;->c(Laxq;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Laxs;->b:Laxs;

    invoke-direct {p0, v1}, Laxp;->b(Laxs;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lawg;->b:Lawg;

    if-ne p1, v1, :cond_0

    .line 140
    sget-object v0, Laxs;->b:Laxs;

    goto :goto_0
.end method

.method public a()V
    .locals 12

    .prologue
    const/4 v1, 0x0

    const-wide/16 v10, 0x0

    .line 83
    iget-object v0, p0, Laxp;->b:Ljava/util/EnumSet;

    invoke-virtual {v0}, Ljava/util/EnumSet;->clear()V

    .line 84
    invoke-static {}, Laxq;->values()[Laxq;

    move-result-object v3

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 85
    iget-object v6, p0, Laxp;->a:Ljava/util/EnumMap;

    iget-object v7, p0, Laxp;->c:Laxr;

    const-string v0, "Event_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 86
    invoke-virtual {v5}, Laxq;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_0

    invoke-virtual {v8, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-interface {v7, v0, v10, v11}, Laxr;->b(Ljava/lang/String;J)J

    move-result-wide v8

    long-to-int v0, v8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 85
    invoke-virtual {v6, v5, v0}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 86
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 88
    :cond_1
    invoke-static {}, Laxs;->values()[Laxs;

    move-result-object v2

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 89
    iget-object v5, p0, Laxp;->c:Laxr;

    const-string v0, "Message_"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4}, Laxs;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_3

    invoke-virtual {v6, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-interface {v5, v0, v10, v11}, Laxr;->b(Ljava/lang/String;J)J

    move-result-wide v6

    cmp-long v0, v6, v10

    if-eqz v0, :cond_2

    .line 90
    iget-object v0, p0, Laxp;->b:Ljava/util/EnumSet;

    invoke-virtual {v0, v4}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 88
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 89
    :cond_3
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v6}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 93
    :cond_4
    iget-object v0, p0, Laxp;->c:Laxr;

    const-string v1, "SaveCount"

    invoke-interface {v0, v1, v10, v11}, Laxr;->b(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Laxp;->f:J

    .line 94
    iget-object v0, p0, Laxp;->c:Laxr;

    const-string v1, "LastMessageTimestamp"

    invoke-interface {v0, v1, v10, v11}, Laxr;->b(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Laxp;->e:J

    .line 95
    return-void
.end method

.method public a(Laxq;)V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Laxp;->a:Ljava/util/EnumMap;

    invoke-direct {p0, p1}, Laxp;->b(Laxq;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/EnumMap;->put(Ljava/lang/Enum;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    invoke-direct {p0}, Laxp;->b()V

    .line 104
    return-void
.end method

.method public a(Laxs;)V
    .locals 2

    .prologue
    .line 111
    if-eqz p1, :cond_0

    .line 112
    iget-object v0, p0, Laxp;->b:Ljava/util/EnumSet;

    invoke-virtual {v0, p1}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 113
    sget-object v0, Laxq;->i:Laxq;

    invoke-direct {p0, v0}, Laxp;->b(Laxq;)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Laxp;->f:J

    .line 114
    iget-object v0, p0, Laxp;->d:Lcgt;

    invoke-virtual {v0}, Lcgt;->b()J

    move-result-wide v0

    iput-wide v0, p0, Laxp;->e:J

    .line 115
    invoke-direct {p0}, Laxp;->b()V

    .line 117
    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Laxp;->c:Laxr;

    invoke-interface {v0}, Laxr;->a()V

    .line 79
    return-void
.end method
