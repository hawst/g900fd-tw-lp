.class public final Lbxx;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static c:Lbxe;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 25
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-static {v0}, Lbxx;->a([I)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lbxx;->a:Ljava/util/Set;

    .line 30
    new-array v0, v4, [I

    const/4 v1, 0x5

    aput v1, v0, v3

    invoke-static {v0}, Lbxx;->a([I)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lbxx;->b:Ljava/util/Set;

    .line 33
    new-instance v0, Lbxb;

    invoke-direct {v0}, Lbxb;-><init>()V

    const v1, 0x3ec5c7ce

    new-instance v2, Lbxd;

    invoke-direct {v2}, Lbxd;-><init>()V

    .line 35
    invoke-virtual {v0, v1, v2}, Lbxb;->a(FLbxe;)Lbxb;

    move-result-object v0

    const v1, 0x3dcaa64c    # 0.09895f

    new-instance v2, Lbxf;

    invoke-direct {v2, v3}, Lbxf;-><init>(I)V

    .line 36
    invoke-virtual {v0, v1, v2}, Lbxb;->a(FLbxe;)Lbxb;

    move-result-object v0

    const v1, 0x3e08509c    # 0.13312f

    new-instance v2, Lbxf;

    invoke-direct {v2, v4}, Lbxf;-><init>(I)V

    .line 38
    invoke-virtual {v0, v1, v2}, Lbxb;->a(FLbxe;)Lbxb;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lbxb;->a()Lbxa;

    move-result-object v0

    sput-object v0, Lbxx;->c:Lbxe;

    .line 33
    return-void

    .line 25
    nop

    :array_0
    .array-data 4
        0x2
        0x6
        0x3
    .end array-data
.end method

.method public static a(Lbvq;)Lbxu;
    .locals 7

    .prologue
    .line 85
    new-instance v0, Lbxu;

    .line 86
    new-instance v1, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;

    invoke-virtual {p0}, Lbvq;->a()Lbvr;

    move-result-object v2

    sget-object v3, Lbxx;->c:Lbxe;

    const/4 v4, 0x1

    sget-object v5, Lbxx;->a:Ljava/util/Set;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;-><init>(Lbvr;Lbxe;FLjava/util/Set;)V

    .line 87
    new-instance v2, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;

    invoke-virtual {p0}, Lbvq;->b()Lbvr;

    move-result-object v3

    sget-object v4, Lbxx;->c:Lbxe;

    const/4 v5, 0x0

    sget-object v6, Lbxx;->b:Ljava/util/Set;

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;-><init>(Lbvr;Lbxe;FLjava/util/Set;)V

    invoke-direct {v0, v1, v2}, Lbxu;-><init>(Lbxr;Lbxr;)V

    return-object v0
.end method

.method private static varargs a([I)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 105
    new-instance v1, Ljava/util/TreeSet;

    invoke-direct {v1}, Ljava/util/TreeSet;-><init>()V

    .line 106
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, p0, v0

    .line 107
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 106
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 109
    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lbvq;)Lbxu;
    .locals 7

    .prologue
    const/high16 v6, 0x3f000000    # 0.5f

    .line 99
    new-instance v0, Lbxu;

    .line 100
    new-instance v1, Lbyb;

    invoke-virtual {p0}, Lbvq;->a()Lbvr;

    move-result-object v2

    sget-object v3, Lbxx;->c:Lbxe;

    sget-object v4, Lbxx;->a:Ljava/util/Set;

    invoke-direct {v1, v2, v3, v6, v4}, Lbyb;-><init>(Lbvr;Lbxe;FLjava/util/Set;)V

    .line 101
    new-instance v2, Lbyb;

    invoke-virtual {p0}, Lbvq;->b()Lbvr;

    move-result-object v3

    sget-object v4, Lbxx;->c:Lbxe;

    sget-object v5, Lbxx;->b:Ljava/util/Set;

    invoke-direct {v2, v3, v4, v6, v5}, Lbyb;-><init>(Lbvr;Lbxe;FLjava/util/Set;)V

    invoke-direct {v0, v1, v2}, Lbxu;-><init>(Lbxr;Lbxr;)V

    return-object v0
.end method
