.class public final Lglg;
.super Lghh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lghh",
        "<",
        "Lglh;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lggb;Lggc;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lghh;-><init>(Landroid/content/Context;Landroid/os/Looper;Lggb;Lggc;)V

    return-void
.end method

.method private c(Lgjl;)Lcom/google/android/gms/feedback/ErrorReport;
    .locals 5

    new-instance v0, Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/ErrorReport;-><init>()V

    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lgjl;->c()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lgjl;->c()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Bundle;->size()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p1}, Lgjl;->c()Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    :cond_0
    invoke-virtual {p1}, Lgjl;->a()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    invoke-virtual {p1}, Lgjl;->a()Landroid/graphics/Bitmap;

    move-result-object v2

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v4, 0x46

    invoke-virtual {v2, v3, v4, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->w:[B

    :cond_1
    invoke-virtual {p1}, Lgjl;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Lgjl;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    :cond_2
    invoke-virtual {p1}, Lgjl;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p1}, Lgjl;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    :cond_3
    invoke-virtual {p1}, Lgjl;->f()Landroid/app/ApplicationErrorReport$CrashInfo;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lgjl;->f()Landroid/app/ApplicationErrorReport$CrashInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->throwMethodName:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->N:Ljava/lang/String;

    invoke-virtual {p1}, Lgjl;->f()Landroid/app/ApplicationErrorReport$CrashInfo;

    move-result-object v1

    iget v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->throwLineNumber:I

    iput v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->L:I

    invoke-virtual {p1}, Lgjl;->f()Landroid/app/ApplicationErrorReport$CrashInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->throwClassName:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->M:Ljava/lang/String;

    invoke-virtual {p1}, Lgjl;->f()Landroid/app/ApplicationErrorReport$CrashInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->O:Ljava/lang/String;

    invoke-virtual {p1}, Lgjl;->f()Landroid/app/ApplicationErrorReport$CrashInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionClassName:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->J:Ljava/lang/String;

    invoke-virtual {p1}, Lgjl;->f()Landroid/app/ApplicationErrorReport$CrashInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionMessage:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->P:Ljava/lang/String;

    invoke-virtual {p1}, Lgjl;->f()Landroid/app/ApplicationErrorReport$CrashInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->throwFileName:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->K:Ljava/lang/String;

    :cond_4
    invoke-virtual {p1}, Lgjl;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {p1}, Lgjl;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->R:Ljava/lang/String;

    :cond_5
    invoke-virtual {p1}, Lgjl;->g()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    invoke-virtual {p1}, Lgjl;->g()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/feedback/ErrorReport;->Q:Ljava/lang/String;

    :cond_6
    return-object v0
.end method


# virtual methods
.method protected synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, Lglg;->b(Landroid/os/IBinder;)Lglh;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lgic;Lghl;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const v1, 0x5e3530

    iget-object v2, p0, Lghh;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lgic;->m(Lghz;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Lgjl;)Z
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lglg;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lglh;

    invoke-direct {p0, p1}, Lglg;->c(Lgjl;)Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v1

    invoke-interface {v0, v1}, Lglh;->a(Lcom/google/android/gms/feedback/ErrorReport;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Landroid/os/IBinder;)Lglh;
    .locals 1

    invoke-static {p1}, Lgli;->a(Landroid/os/IBinder;)Lglh;

    move-result-object v0

    return-object v0
.end method

.method public b(Lgjl;)Z
    .locals 2

    :try_start_0
    invoke-virtual {p0}, Lglg;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lglh;

    invoke-direct {p0, p1}, Lglg;->c(Lgjl;)Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v1

    invoke-interface {v0, v1}, Lglh;->b(Lcom/google/android/gms/feedback/ErrorReport;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.feedback.internal.IFeedbackService"

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.feedback.internal.IFeedbackService"

    return-object v0
.end method
