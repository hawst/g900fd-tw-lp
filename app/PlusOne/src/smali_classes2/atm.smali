.class public final Latm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcgk;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/Object;

.field private final c:Landroid/media/MediaMuxer;

.field private final d:Ljava/util/concurrent/CountDownLatch;

.field private e:Z

.field private volatile f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Latm;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Latm;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/media/MediaMuxer;I)V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Latm;->b:Ljava/lang/Object;

    .line 61
    const-string v0, "mediaMuxer"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaMuxer;

    iput-object v0, p0, Latm;->c:Landroid/media/MediaMuxer;

    .line 62
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const-string v1, "trackCount"

    invoke-static {p2, v1}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Latm;->d:Ljava/util/concurrent/CountDownLatch;

    .line 63
    return-void
.end method


# virtual methods
.method public a(Landroid/media/MediaFormat;)I
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Latm;->c:Landroid/media/MediaMuxer;

    invoke-virtual {v0, p1}, Landroid/media/MediaMuxer;->addTrack(Landroid/media/MediaFormat;)I

    move-result v0

    .line 138
    iget-object v1, p0, Latm;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 139
    return v0
.end method

.method public a()V
    .locals 4

    .prologue
    .line 119
    iget-object v1, p0, Latm;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 121
    :try_start_0
    invoke-virtual {p0}, Latm;->c()V
    :try_end_0
    .catch Latn; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 125
    :goto_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 126
    iget-object v0, p0, Latm;->c:Landroid/media/MediaMuxer;

    invoke-virtual {v0}, Landroid/media/MediaMuxer;->release()V

    .line 127
    return-void

    .line 122
    :catch_0
    move-exception v0

    .line 123
    :try_start_2
    sget-object v2, Latm;->a:Ljava/lang/String;

    const-string v3, "await() failed"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 125
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Latm;->c:Landroid/media/MediaMuxer;

    invoke-virtual {v0, p1}, Landroid/media/MediaMuxer;->setOrientationHint(I)V

    .line 72
    return-void
.end method

.method public a(ILjava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Latm;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 148
    iget-boolean v0, p0, Latm;->f:Z

    if-eqz v0, :cond_0

    .line 154
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 156
    :cond_0
    iget-object v1, p0, Latm;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 159
    :try_start_0
    iget-boolean v0, p0, Latm;->e:Z

    if-nez v0, :cond_1

    .line 163
    const/4 v0, 0x1

    iput-boolean v0, p0, Latm;->e:Z

    .line 164
    iget-object v0, p0, Latm;->c:Landroid/media/MediaMuxer;

    invoke-virtual {v0}, Landroid/media/MediaMuxer;->start()V

    .line 166
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    iget-object v0, p0, Latm;->c:Landroid/media/MediaMuxer;

    invoke-virtual {v0, p1, p2, p3}, Landroid/media/MediaMuxer;->writeSampleData(ILjava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 171
    return-void

    .line 166
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b()V
    .locals 6

    .prologue
    .line 78
    iget-object v1, p0, Latm;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 79
    :try_start_0
    iget-boolean v0, p0, Latm;->e:Z

    if-nez v0, :cond_0

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Latm;->f:Z

    .line 85
    :goto_0
    iget-object v0, p0, Latm;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_0

    .line 86
    iget-object v0, p0, Latm;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 89
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 98
    iget-object v1, p0, Latm;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 99
    :try_start_0
    invoke-virtual {p0}, Latm;->b()V

    .line 101
    iget-boolean v0, p0, Latm;->e:Z

    if-eqz v0, :cond_0

    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Latm;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 104
    :try_start_1
    iget-object v0, p0, Latm;->c:Landroid/media/MediaMuxer;

    invoke-virtual {v0}, Landroid/media/MediaMuxer;->stop()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 109
    :cond_0
    :try_start_2
    monitor-exit v1

    return-void

    .line 105
    :catch_0
    move-exception v0

    .line 106
    new-instance v2, Latn;

    invoke-direct {v2, v0}, Latn;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 109
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method
