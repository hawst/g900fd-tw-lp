.class final Lccx;
.super Landroid/util/Property;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/Property",
        "<",
        "Lccj;",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private d:F


# direct methods
.method public constructor <init>(III)V
    .locals 2

    .prologue
    .line 160
    const-class v0, Ljava/lang/Float;

    const-string v1, "progress"

    invoke-direct {p0, v0, v1}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    .line 162
    iput p1, p0, Lccx;->a:I

    .line 163
    iput p2, p0, Lccx;->b:I

    .line 164
    iput p3, p0, Lccx;->c:I

    .line 165
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Float;
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lccx;->d:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public a(Lccj;Ljava/lang/Float;)V
    .locals 5

    .prologue
    .line 169
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lccx;->d:F

    .line 171
    const/4 v0, 0x0

    iget-object v1, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_3

    .line 172
    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    .line 173
    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    iget-object v4, v0, Lccg;->a:Landroid/view/View;

    .line 174
    iget v0, p0, Lccx;->c:I

    if-ne v3, v0, :cond_0

    .line 175
    const/high16 v0, 0x3f800000    # 1.0f

    iget v3, p0, Lccx;->d:F

    sub-float/2addr v0, v3

    invoke-virtual {v4, v0}, Landroid/view/View;->setAlpha(F)V

    .line 171
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 177
    :cond_0
    iget v0, p0, Lccx;->c:I

    if-ge v3, v0, :cond_1

    iget v0, p0, Lccx;->a:I

    .line 178
    :goto_2
    iget-boolean v3, p1, Lccj;->a:Z

    if-eqz v3, :cond_2

    .line 179
    iget v3, p0, Lccx;->d:F

    int-to-float v0, v0

    mul-float/2addr v0, v3

    invoke-virtual {v4, v0}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_1

    .line 177
    :cond_1
    iget v0, p0, Lccx;->b:I

    goto :goto_2

    .line 181
    :cond_2
    iget v3, p0, Lccx;->d:F

    int-to-float v0, v0

    mul-float/2addr v0, v3

    invoke-virtual {v4, v0}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_1

    .line 185
    :cond_3
    return-void
.end method

.method public synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 152
    invoke-virtual {p0}, Lccx;->a()Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 152
    check-cast p1, Lccj;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p0, p1, p2}, Lccx;->a(Lccj;Ljava/lang/Float;)V

    return-void
.end method
