.class public final Lchu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:Z


# direct methods
.method public constructor <init>(III)V
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lchu;-><init>(IIIZ)V

    .line 150
    return-void
.end method

.method public constructor <init>(IIIZ)V
    .locals 0

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput p1, p0, Lchu;->a:I

    .line 155
    iput p2, p0, Lchu;->b:I

    .line 156
    iput p3, p0, Lchu;->c:I

    .line 157
    iput-boolean p4, p0, Lchu;->d:Z

    .line 159
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lchu;->a:I

    return v0
.end method

.method public a(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 166
    iget v0, p0, Lchu;->b:I

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/content/res/Resources;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 170
    iget v0, p0, Lchu;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lchu;->c:I

    .line 171
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 175
    iget-boolean v0, p0, Lchu;->d:Z

    return v0
.end method
