.class public final Lakw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lajw;


# static fields
.field private static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lakw;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lakw;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/util/LongSparseArray;)F
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;)F"
        }
    .end annotation

    .prologue
    .line 132
    const/4 v1, 0x0

    .line 134
    const/4 v0, 0x0

    move v2, v1

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 135
    invoke-virtual {p0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    add-float/2addr v2, v0

    .line 134
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 138
    :cond_0
    invoke-virtual {p0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v2, v0

    return v0
.end method


# virtual methods
.method public a(Lbkn;Lbku;Lbkq;)V
    .locals 16

    .prologue
    .line 48
    invoke-virtual/range {p1 .. p1}, Lbkn;->c()J

    move-result-wide v2

    const-wide/32 v4, 0x19f0a0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_1

    .line 49
    sget-object v2, Lakw;->b:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lbkn;->c()J

    move-result-wide v2

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x4d

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Video duration was "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " us <= minimum 1700000"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " us"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lbkn;->c()J

    move-result-wide v2

    const-wide/32 v4, 0x19f0a0

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x2

    div-long v8, v2, v4

    .line 56
    sget-object v2, Lbmz;->f:Lbmz;

    .line 57
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lbku;->b(Lbmz;)Landroid/util/LongSparseArray;

    move-result-object v10

    .line 59
    invoke-virtual {v10}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    if-eqz v2, :cond_0

    .line 63
    invoke-static {v10}, Lakw;->a(Landroid/util/LongSparseArray;)F

    move-result v11

    .line 67
    const/4 v6, -0x1

    .line 68
    const/4 v5, -0x1

    .line 71
    const/4 v4, 0x0

    .line 72
    const/4 v3, 0x0

    .line 78
    const/4 v2, 0x0

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v3

    move v3, v2

    :goto_1
    int-to-float v2, v3

    invoke-virtual {v10}, Landroid/util/LongSparseArray;->size()I

    move-result v12

    int-to-float v12, v12

    const v13, 0x3e99999a    # 0.3f

    mul-float/2addr v12, v13

    cmpg-float v2, v2, v12

    if-gez v2, :cond_4

    .line 79
    invoke-virtual {v10, v3}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    add-float/2addr v5, v2

    .line 80
    invoke-virtual {v10}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    sub-int/2addr v2, v3

    invoke-virtual {v10, v2}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    add-float/2addr v4, v2

    .line 82
    add-int/lit8 v2, v3, 0x1

    int-to-float v2, v2

    div-float v2, v5, v2

    .line 83
    add-int/lit8 v12, v3, 0x1

    int-to-float v12, v12

    div-float v12, v4, v12

    .line 87
    const/high16 v13, 0x3f800000    # 1.0f

    int-to-float v14, v3

    invoke-virtual {v10}, Landroid/util/LongSparseArray;->size()I

    move-result v15

    int-to-float v15, v15

    div-float/2addr v14, v15

    sub-float/2addr v13, v14

    mul-float/2addr v13, v11

    .line 89
    cmpg-float v2, v2, v13

    if-gez v2, :cond_2

    move v7, v3

    .line 93
    :cond_2
    cmpg-float v2, v12, v13

    if-gez v2, :cond_3

    move v6, v3

    .line 78
    :cond_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 98
    :cond_4
    new-instance v2, Ljava/util/ArrayList;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 99
    if-ltz v7, :cond_5

    .line 102
    invoke-virtual {v10, v7}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    .line 105
    const-wide/16 v12, 0x0

    cmp-long v3, v4, v12

    if-lez v3, :cond_5

    .line 106
    new-instance v3, Lbmr;

    new-instance v7, Lbmp;

    const-wide/16 v12, 0x0

    invoke-direct {v7, v12, v13, v4, v5}, Lbmp;-><init>(JJ)V

    const/4 v4, 0x3

    invoke-direct {v3, v7, v4}, Lbmr;-><init>(Lbmp;I)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 112
    :cond_5
    if-ltz v6, :cond_6

    .line 116
    invoke-virtual {v10}, Landroid/util/LongSparseArray;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    sub-int/2addr v3, v6

    invoke-virtual {v10, v3}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    .line 117
    invoke-virtual/range {p1 .. p1}, Lbkn;->c()J

    move-result-wide v6

    sub-long/2addr v6, v8

    .line 115
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 119
    invoke-virtual/range {p1 .. p1}, Lbkn;->c()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-gez v3, :cond_6

    .line 120
    new-instance v3, Lbmr;

    new-instance v6, Lbmp;

    .line 123
    invoke-virtual/range {p1 .. p1}, Lbkn;->c()J

    move-result-wide v8

    invoke-direct {v6, v4, v5, v8, v9}, Lbmp;-><init>(JJ)V

    const/4 v4, 0x3

    invoke-direct {v3, v6, v4}, Lbmr;-><init>(Lbmp;I)V

    .line 120
    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 127
    :cond_6
    const/4 v3, 0x3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v2}, Lbkq;->a(ILjava/util/List;)Lbkq;

    goto/16 :goto_0
.end method
