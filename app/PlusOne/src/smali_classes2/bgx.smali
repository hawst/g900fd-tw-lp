.class public final Lbgx;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:[Lbgz;

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private c:[Lbha;

.field private d:I


# direct methods
.method public constructor <init>(Lbhf;Lbfv;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 138
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput v1, p0, Lbgx;->d:I

    .line 139
    new-instance v0, Lbgy;

    invoke-direct {v0, p1, p2}, Lbgy;-><init>(Lbhf;Lbfv;)V

    .line 140
    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lbhf;->b()I

    move-result v2

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glAttachShader(II)V

    const-string v2, "glAttachShader"

    invoke-static {v2}, Lbgc;->a(Ljava/lang/String;)V

    invoke-virtual {p2}, Lbfv;->b()I

    move-result v2

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glAttachShader(II)V

    const-string v2, "glAttachShader"

    invoke-static {v2}, Lbgc;->a(Ljava/lang/String;)V

    invoke-static {v0}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    new-array v2, v6, [I

    const v3, 0x8b82

    invoke-static {v0, v3, v2, v1}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    aget v2, v2, v1

    if-eq v2, v6, :cond_1

    invoke-static {v0}, Landroid/opengl/GLES20;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Could not link program: "

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iput v0, p0, Lbgx;->d:I

    .line 143
    new-array v2, v6, [I

    .line 144
    iget v0, p0, Lbgx;->d:I

    const v3, 0x8b89

    invoke-static {v0, v3, v2, v1}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    .line 145
    aget v0, v2, v1

    new-array v0, v0, [Lbha;

    iput-object v0, p0, Lbgx;->c:[Lbha;

    move v0, v1

    .line 146
    :goto_1
    aget v3, v2, v1

    if-ge v0, v3, :cond_2

    .line 147
    iget-object v3, p0, Lbgx;->c:[Lbha;

    new-instance v4, Lbha;

    iget v5, p0, Lbgx;->d:I

    invoke-direct {v4, v5, v0}, Lbha;-><init>(II)V

    aput-object v4, v3, v0

    .line 146
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 151
    :cond_2
    new-array v2, v6, [I

    .line 152
    iget v0, p0, Lbgx;->d:I

    const v3, 0x8b86

    invoke-static {v0, v3, v2, v1}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    .line 153
    aget v0, v2, v1

    const/16 v3, 0x40

    if-le v0, v3, :cond_3

    .line 154
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Shader uniform count exceeds MAX_UNIFORM_COUNT"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157
    :cond_3
    aget v0, v2, v1

    new-array v0, v0, [Lbgz;

    iput-object v0, p0, Lbgx;->a:[Lbgz;

    .line 158
    aget v0, v2, v1

    invoke-static {v0}, Lcfm;->a(I)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lbgx;->b:Ljava/util/Map;

    move v0, v1

    .line 159
    :goto_2
    aget v3, v2, v1

    if-ge v0, v3, :cond_4

    .line 160
    new-instance v3, Lbgz;

    iget v4, p0, Lbgx;->d:I

    invoke-direct {v3, v4, v0}, Lbgz;-><init>(II)V

    .line 161
    iget-object v4, p0, Lbgx;->a:[Lbgz;

    aput-object v3, v4, v0

    .line 162
    iget-object v4, p0, Lbgx;->b:Ljava/util/Map;

    iget-object v3, v3, Lbgz;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 159
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 164
    :cond_4
    return-void
.end method

.method static synthetic a([B)I
    .locals 2

    .prologue
    .line 18
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    aget-byte v1, p0, v0

    if-nez v1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    array-length v0, p0

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 277
    iget v0, p0, Lbgx;->d:I

    if-nez v0, :cond_0

    .line 278
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "ShaderProgram had destroy() called twice"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 280
    :cond_0
    iget v0, p0, Lbgx;->d:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    .line 281
    const/4 v0, 0x0

    iput v0, p0, Lbgx;->d:I

    .line 282
    return-void
.end method

.method public a(Lbhd;Lbgs;)V
    .locals 13

    .prologue
    const v12, 0x812f

    const/16 v6, 0xde1

    const/4 v3, 0x0

    .line 177
    iget v0, p0, Lbgx;->d:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 178
    const-string v0, "glUseProgram"

    invoke-static {v0}, Lbgc;->a(Ljava/lang/String;)V

    .line 181
    iget-object v8, p0, Lbgx;->c:[Lbha;

    array-length v9, v8

    move v7, v3

    :goto_0
    if-ge v7, v9, :cond_1

    aget-object v10, v8, v7

    .line 182
    iget-object v0, v10, Lbha;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lbhd;->a(Ljava/lang/String;)Lbhg;

    move-result-object v4

    .line 183
    if-nez v4, :cond_0

    .line 184
    new-instance v0, Ljava/lang/RuntimeException;

    iget-object v1, v10, Lbha;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "VertexBuffer missing stream \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 186
    :cond_0
    iget-object v0, v10, Lbha;->a:Ljava/lang/String;

    const/4 v1, 0x1

    iget v2, v4, Lbhg;->a:I

    iget v5, v10, Lbha;->c:I

    iget v11, v10, Lbha;->d:I

    invoke-static {v0, v1, v2, v5, v11}, Lbhc;->a(Ljava/lang/String;IIII)V

    const v0, 0x8892

    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glBindBuffer(II)V

    iget v0, v10, Lbha;->b:I

    iget v1, v10, Lbha;->c:I

    invoke-static {v1}, Lbhc;->b(I)I

    move-result v1

    iget v2, v10, Lbha;->c:I

    sparse-switch v2, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x1d

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unhandled gl type "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    const/16 v2, 0x1406

    :goto_1
    iget-object v5, v4, Lbhg;->c:Ljava/nio/Buffer;

    move v4, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    iget v0, v10, Lbha;->b:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    const-string v0, "Binding VertexAttribute"

    invoke-static {v0}, Lbgc;->a(Ljava/lang/String;)V

    .line 181
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto/16 :goto_0

    .line 186
    :sswitch_1
    const/16 v2, 0x1404

    goto :goto_1

    .line 190
    :cond_1
    invoke-virtual {p2}, Lbgs;->c()[Ljava/lang/Object;

    move-result-object v5

    move v1, v3

    move v2, v3

    .line 192
    :goto_2
    iget-object v0, p0, Lbgx;->a:[Lbgz;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 193
    iget-object v0, p0, Lbgx;->a:[Lbgz;

    aget-object v7, v0, v1

    .line 194
    aget-object v0, v5, v1

    .line 195
    iget v4, v7, Lbgz;->c:I

    sparse-switch v4, :sswitch_data_1

    .line 266
    new-instance v0, Ljava/lang/RuntimeException;

    iget-object v1, v7, Lbgz;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1b

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Uniform "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has unhandled type"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 197
    :sswitch_2
    iget v4, v7, Lbgz;->b:I

    iget v8, v7, Lbgz;->d:I

    check-cast v0, [F

    invoke-static {v4, v8, v0, v3}, Landroid/opengl/GLES20;->glUniform1fv(II[FI)V

    .line 268
    :goto_3
    const-string v4, "Binding uniform "

    iget-object v0, v7, Lbgz;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v7

    if-eqz v7, :cond_6

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    invoke-static {v0}, Lbgc;->a(Ljava/lang/String;)V

    .line 192
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 200
    :sswitch_3
    iget v4, v7, Lbgz;->b:I

    iget v8, v7, Lbgz;->d:I

    check-cast v0, [F

    invoke-static {v4, v8, v0, v3}, Landroid/opengl/GLES20;->glUniform2fv(II[FI)V

    goto :goto_3

    .line 203
    :sswitch_4
    iget v4, v7, Lbgz;->b:I

    iget v8, v7, Lbgz;->d:I

    check-cast v0, [F

    invoke-static {v4, v8, v0, v3}, Landroid/opengl/GLES20;->glUniform3fv(II[FI)V

    goto :goto_3

    .line 206
    :sswitch_5
    iget v4, v7, Lbgz;->b:I

    iget v8, v7, Lbgz;->d:I

    check-cast v0, [F

    invoke-static {v4, v8, v0, v3}, Landroid/opengl/GLES20;->glUniform4fv(II[FI)V

    goto :goto_3

    .line 209
    :sswitch_6
    iget v4, v7, Lbgz;->b:I

    iget v8, v7, Lbgz;->d:I

    check-cast v0, [F

    invoke-static {v4, v8, v3, v0, v3}, Landroid/opengl/GLES20;->glUniformMatrix2fv(IIZ[FI)V

    goto :goto_3

    .line 213
    :sswitch_7
    iget v4, v7, Lbgz;->b:I

    iget v8, v7, Lbgz;->d:I

    check-cast v0, [F

    invoke-static {v4, v8, v3, v0, v3}, Landroid/opengl/GLES20;->glUniformMatrix3fv(IIZ[FI)V

    goto :goto_3

    .line 217
    :sswitch_8
    iget v4, v7, Lbgz;->b:I

    iget v8, v7, Lbgz;->d:I

    check-cast v0, [F

    invoke-static {v4, v8, v3, v0, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    goto :goto_3

    .line 221
    :sswitch_9
    iget v4, v7, Lbgz;->b:I

    iget v8, v7, Lbgz;->d:I

    check-cast v0, [I

    invoke-static {v4, v8, v0, v3}, Landroid/opengl/GLES20;->glUniform1iv(II[II)V

    goto :goto_3

    .line 224
    :sswitch_a
    iget v4, v7, Lbgz;->b:I

    iget v8, v7, Lbgz;->d:I

    check-cast v0, [I

    invoke-static {v4, v8, v0, v3}, Landroid/opengl/GLES20;->glUniform2iv(II[II)V

    goto :goto_3

    .line 227
    :sswitch_b
    iget v4, v7, Lbgz;->b:I

    iget v8, v7, Lbgz;->d:I

    check-cast v0, [I

    invoke-static {v4, v8, v0, v3}, Landroid/opengl/GLES20;->glUniform3iv(II[II)V

    goto :goto_3

    .line 230
    :sswitch_c
    iget v4, v7, Lbgz;->b:I

    iget v8, v7, Lbgz;->d:I

    check-cast v0, [I

    invoke-static {v4, v8, v0, v3}, Landroid/opengl/GLES20;->glUniform4iv(II[II)V

    goto :goto_3

    .line 234
    :sswitch_d
    check-cast v0, Lbhb;

    .line 235
    if-nez v0, :cond_3

    .line 236
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Missing uniform sampler2D "

    iget-object v0, v7, Lbgz;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 238
    :cond_3
    invoke-interface {v0}, Lbhb;->c()I

    move-result v8

    .line 239
    invoke-interface {v0}, Lbhb;->b()Z

    move-result v4

    if-eqz v4, :cond_4

    const v4, 0x8d65

    .line 242
    :goto_6
    invoke-interface {v0}, Lbhb;->g()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x29

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unhandled Texture filter mode "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    move v4, v6

    .line 239
    goto :goto_6

    .line 242
    :pswitch_0
    const/16 v0, 0x2600

    .line 243
    :goto_7
    const-string v9, "texture handle"

    const/4 v10, 0x0

    invoke-static {v8, v9, v3, v10}, Lcgp;->b(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)I

    .line 246
    const v9, 0x84c0

    add-int/2addr v9, v2

    invoke-static {v9}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 247
    invoke-static {v4, v8}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 248
    iget v4, v7, Lbgz;->b:I

    invoke-static {v4, v2}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 249
    const-string v8, "Binding sampler2D "

    iget-object v4, v7, Lbgz;->a:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_5

    invoke-virtual {v8, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_8
    invoke-static {v4}, Lbgc;->a(Ljava/lang/String;)V

    .line 252
    const/16 v4, 0x2800

    invoke-static {v6, v4, v0}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 254
    const/16 v4, 0x2801

    invoke-static {v6, v4, v0}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 257
    const/16 v0, 0x2802

    invoke-static {v6, v0, v12}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 259
    const/16 v0, 0x2803

    invoke-static {v6, v0, v12}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 261
    const-string v0, "glTexParameteri"

    invoke-static {v0}, Lbgc;->a(Ljava/lang/String;)V

    .line 263
    add-int/lit8 v2, v2, 0x1

    .line 264
    goto/16 :goto_3

    .line 242
    :pswitch_1
    const/16 v0, 0x2601

    goto :goto_7

    .line 249
    :cond_5
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_8

    .line 268
    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_4

    .line 270
    :cond_7
    return-void

    .line 186
    nop

    :sswitch_data_0
    .sparse-switch
        0x1404 -> :sswitch_1
        0x1406 -> :sswitch_0
        0x8b50 -> :sswitch_0
        0x8b51 -> :sswitch_0
        0x8b52 -> :sswitch_0
        0x8b53 -> :sswitch_1
        0x8b54 -> :sswitch_1
        0x8b55 -> :sswitch_1
        0x8b5a -> :sswitch_0
        0x8b5b -> :sswitch_0
        0x8b5c -> :sswitch_0
    .end sparse-switch

    .line 195
    :sswitch_data_1
    .sparse-switch
        0x1404 -> :sswitch_9
        0x1406 -> :sswitch_2
        0x8b50 -> :sswitch_3
        0x8b51 -> :sswitch_4
        0x8b52 -> :sswitch_5
        0x8b53 -> :sswitch_a
        0x8b54 -> :sswitch_b
        0x8b55 -> :sswitch_c
        0x8b5a -> :sswitch_6
        0x8b5b -> :sswitch_7
        0x8b5c -> :sswitch_8
        0x8b5e -> :sswitch_d
        0x8d66 -> :sswitch_d
    .end sparse-switch

    .line 242
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
