.class public final Layp;
.super Layr;
.source "PG"


# static fields
.field private static final A:Lbgd;

.field private static final B:Lbgd;

.field private static final C:Lbgd;

.field private static final D:Lbgd;

.field private static final E:Lbgd;

.field private static final F:Lbgd;

.field private static final a:[F

.field private static final b:[F

.field private static final c:[F

.field private static final d:[F

.field private static final e:[F

.field private static final f:Lbgd;

.field private static final g:Lbgd;

.field private static final h:Lbgd;

.field private static final i:Lbgd;

.field private static final j:Lbgd;

.field private static final k:Lbgd;

.field private static final l:[[Lbgd;

.field private static final m:Lbgd;

.field private static final n:Lbgd;

.field private static final o:Lbgd;

.field private static final p:Lbgd;

.field private static final q:Lbgd;

.field private static final r:Lbgd;

.field private static final s:Lbgd;

.field private static final t:Lbgd;

.field private static final u:[[Lbgd;

.field private static final v:Lbgd;

.field private static final w:Lbgd;

.field private static final x:Lbgd;

.field private static final y:Lbgd;

.field private static final z:Lbgd;


# instance fields
.field private G:Landroid/graphics/Matrix;

.field private H:Landroid/graphics/Matrix;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const v9, 0x3f2aaaab

    const v8, 0x3eaaaaab

    const/high16 v7, 0x3f000000    # 0.5f

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 23
    const/4 v0, 0x4

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Layp;->a:[F

    .line 34
    const/16 v0, 0xd

    new-array v0, v0, [F

    fill-array-data v0, :array_1

    sput-object v0, Layp;->b:[F

    .line 42
    const/16 v0, 0x1a

    new-array v0, v0, [F

    fill-array-data v0, :array_2

    sput-object v0, Layp;->c:[F

    .line 52
    const/16 v0, 0x17

    new-array v0, v0, [F

    fill-array-data v0, :array_3

    sput-object v0, Layp;->d:[F

    .line 62
    const/16 v0, 0x28

    new-array v0, v0, [F

    fill-array-data v0, :array_4

    sput-object v0, Layp;->e:[F

    .line 71
    invoke-static {v6, v6, v5, v5}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->f:Lbgd;

    .line 72
    const/high16 v0, -0x41000000    # -0.5f

    invoke-static {v7, v9, v0, v5}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->g:Lbgd;

    .line 74
    invoke-static {v7, v9, v7, v5}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->h:Lbgd;

    .line 76
    const v0, -0x40d55555

    invoke-static {v8, v7, v0, v7}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->i:Lbgd;

    .line 78
    const v0, -0x40d55555

    const/high16 v1, -0x41000000    # -0.5f

    invoke-static {v8, v7, v0, v1}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->j:Lbgd;

    .line 80
    invoke-static {v9, v6, v8, v5}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->k:Lbgd;

    .line 93
    const/4 v0, 0x3

    new-array v0, v0, [[Lbgd;

    const/4 v1, 0x0

    const/4 v2, 0x3

    new-array v2, v2, [Lbgd;

    const/4 v3, 0x0

    sget-object v4, Layp;->f:Lbgd;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x0

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const/4 v4, 0x0

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x3

    new-array v2, v2, [Lbgd;

    const/4 v3, 0x0

    sget-object v4, Layp;->g:Lbgd;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Layp;->h:Lbgd;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const/4 v4, 0x0

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/4 v2, 0x3

    new-array v2, v2, [Lbgd;

    const/4 v3, 0x0

    sget-object v4, Layp;->i:Lbgd;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Layp;->j:Lbgd;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Layp;->k:Lbgd;

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    sput-object v0, Layp;->l:[[Lbgd;

    .line 102
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v6, v6, v0, v5}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->m:Lbgd;

    .line 104
    const/high16 v0, 0x3fc00000    # 1.5f

    invoke-static {v7, v9, v0, v5}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->n:Lbgd;

    .line 106
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v6, v6, v0, v5}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->o:Lbgd;

    .line 108
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v6, v6, v0, v5}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->p:Lbgd;

    .line 110
    const/high16 v0, 0x3fc00000    # 1.5f

    invoke-static {v7, v9, v0, v5}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->q:Lbgd;

    .line 112
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v6, v6, v0, v5}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->r:Lbgd;

    .line 114
    const/high16 v0, 0x3fc00000    # 1.5f

    invoke-static {v7, v9, v0, v5}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->s:Lbgd;

    .line 116
    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v6, v6, v0, v5}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->t:Lbgd;

    .line 127
    const/4 v0, 0x3

    new-array v0, v0, [[Lbgd;

    const/4 v1, 0x0

    const/4 v2, 0x3

    new-array v2, v2, [Lbgd;

    const/4 v3, 0x0

    sget-object v4, Layp;->m:Lbgd;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Layp;->p:Lbgd;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const/4 v4, 0x0

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const/4 v2, 0x3

    new-array v2, v2, [Lbgd;

    const/4 v3, 0x0

    sget-object v4, Layp;->n:Lbgd;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Layp;->q:Lbgd;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Layp;->s:Lbgd;

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const/4 v2, 0x3

    new-array v2, v2, [Lbgd;

    const/4 v3, 0x0

    sget-object v4, Layp;->o:Lbgd;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Layp;->r:Lbgd;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Layp;->t:Lbgd;

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    sput-object v0, Layp;->u:[[Lbgd;

    .line 135
    const/high16 v0, -0x40000000    # -2.0f

    invoke-static {v6, v6, v0, v5}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->v:Lbgd;

    .line 137
    const/high16 v0, -0x3fe00000    # -2.5f

    invoke-static {v7, v6, v0, v5}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->w:Lbgd;

    .line 139
    const/high16 v0, -0x40400000    # -1.5f

    invoke-static {v7, v9, v0, v5}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->x:Lbgd;

    .line 141
    const/high16 v0, -0x40400000    # -1.5f

    invoke-static {v7, v6, v0, v5}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->y:Lbgd;

    .line 143
    const v0, -0x3fd55555

    invoke-static {v8, v7, v0, v7}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->z:Lbgd;

    .line 145
    const v0, -0x40555555

    invoke-static {v8, v8, v0, v8}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->A:Lbgd;

    .line 147
    const v0, -0x3fd55555

    invoke-static {v8, v7, v0, v7}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->B:Lbgd;

    .line 149
    const v0, -0x3fd55555

    const/high16 v1, -0x41000000    # -0.5f

    invoke-static {v8, v7, v0, v1}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->C:Lbgd;

    .line 151
    const v0, -0x40555555

    const v1, -0x41555555

    invoke-static {v8, v8, v0, v1}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->D:Lbgd;

    .line 153
    const v0, -0x402aaaab

    invoke-static {v9, v6, v0, v5}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->E:Lbgd;

    .line 157
    const/high16 v0, 0x3f400000    # 0.75f

    const/high16 v1, -0x41800000    # -0.25f

    invoke-static {v0, v6, v1, v5}, Lbgd;->a(FFFF)Lbgd;

    move-result-object v0

    sput-object v0, Layp;->F:Lbgd;

    return-void

    .line 23
    nop

    :array_0
    .array-data 4
        0x3f6ed917    # 0.933f
        0x3f6ed917    # 0.933f
        0x3f6ed917    # 0.933f
        0x3f800000    # 1.0f
    .end array-data

    .line 34
    :array_1
    .array-data 4
        0x0
        0x3cf5c28f    # 0.03f
        0x3da9fbe7    # 0.083f
        0x3e45a1cb    # 0.193f
        0x3f00c49c    # 0.503f
        0x3f3ef9db    # 0.746f
        0x3f591687    # 0.848f
        0x3f683127    # 0.907f
        0x3f71eb85    # 0.945f
        0x3f7851ec    # 0.97f
        0x3f7c6a7f    # 0.986f
        0x3f7eb852    # 0.995f
        0x3f800000    # 1.0f
    .end array-data

    .line 42
    :array_2
    .array-data 4
        0x0
        0x3b180b24    # 0.00232f
        0x3c267621    # 0.01016f
        0x3ccfaace    # 0.02535f
        0x3d4ffeb0    # 0.05078f
        0x3dbadc0a    # 0.09124f
        0x3e1eb852    # 0.155f
        0x3e826bf8
        0x3ec94c44
        0x3f084578    # 0.53231f
        0x3f2402f3    # 0.64067f
        0x3f389375    # 0.721f
        0x3f48294a
        0x3f544a62    # 0.82926f
        0x3f5dea89
        0x3f65a7b1
        0x3f6bea0c
        0x3f70fc50
        0x3f75143c    # 0.95734f
        0x3f785921    # 0.97011f
        0x3f7aeb1c
        0x3f7ce11e
        0x3f7e501e    # 0.99341f
        0x3f7f4730
        0x3f7fd36f
        0x3f800000    # 1.0f
    .end array-data

    .line 52
    :array_3
    .array-data 4
        0x0
        0x3e9b5b2d    # 0.30343f
        0x3f094270    # 0.53617f
        0x3f30b828    # 0.69031f
        0x3f49de6a
        0x3f5a4207    # 0.85257f
        0x3f654de8    # 0.89572f
        0x3f6cf80e
        0x3f72680a    # 0.9469f
        0x3f76516e
        0x3f792791
        0x3f7b3871
        0x3f7cb924    # 0.9872f
        0x3f7dce5b
        0x3f7e939f
        0x3f7f1d3f    # 0.99654f
        0x3f7f7af6
        0x3f7fb7e9    # 0.9989f
        0x3f7fdd44    # 0.99947f
        0x3f7ff23d
        0x3f7ffc11    # 0.99994f
        0x3f7fff58    # 0.99999f
        0x3f800000    # 1.0f
    .end array-data

    .line 62
    :array_4
    .array-data 4
        0x0
        0x3bd7f0ed    # 0.00659f
        0x3c883ba3    # 0.01663f
        0x3cf4341a    # 0.02981f
        0x3d3bc2b9    # 0.04584f
        0x3d840e17    # 0.06448f
        0x3daf1562    # 0.08549f
        0x3dde83e4    # 0.10865f
        0x3e08f5c3    # 0.13375f
        0x3e2476f3    # 0.16061f
        0x3e4193b4    # 0.18904f
        0x3e602214    # 0.21888f
        0x3e7ff584    # 0.24996f
        0x3e907208
        0x3ea16335
        0x3eb2bbed    # 0.34909f
        0x3ec469d7    # 0.38362f
        0x3ed6594b    # 0.41865f
        0x3ee8793e    # 0.45405f
        0x3efab607    # 0.48967f
        0x3f067ff6
        0x3f0fa0f9    # 0.56105f
        0x3f18b631
        0x3f21b520    # 0.63167f
        0x3f2a949a
        0x3f334a23
        0x3f3bcc8e    # 0.73359f
        0x3f44100e
        0x3f4c0981
        0x3f53ad19
        0x3f5aed14
        0x3f61bcfd
        0x3f680c74
        0x3f6dcc64    # 0.9289f
        0x3f72ea74
        0x3f7753a4    # 0.96612f
        0x3f7af102
        0x3f7daa50
        0x3f7f64ae    # 0.99763f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Layr;-><init>()V

    .line 164
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Layp;->G:Landroid/graphics/Matrix;

    .line 171
    new-instance v0, Lbgd;

    invoke-direct {v0}, Lbgd;-><init>()V

    .line 173
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Layp;->H:Landroid/graphics/Matrix;

    return-void
.end method

.method private a(III)Lbgd;
    .locals 1

    .prologue
    .line 312
    packed-switch p1, :pswitch_data_0

    .line 354
    :goto_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 314
    :pswitch_0
    sget-object v0, Layp;->v:Lbgd;

    .line 350
    :goto_1
    return-object v0

    .line 316
    :pswitch_1
    packed-switch p2, :pswitch_data_1

    goto :goto_0

    .line 318
    :pswitch_2
    packed-switch p3, :pswitch_data_2

    goto :goto_0

    .line 320
    :pswitch_3
    sget-object v0, Layp;->w:Lbgd;

    goto :goto_1

    .line 322
    :pswitch_4
    sget-object v0, Layp;->x:Lbgd;

    goto :goto_1

    .line 326
    :pswitch_5
    sget-object v0, Layp;->y:Lbgd;

    goto :goto_1

    .line 330
    :pswitch_6
    packed-switch p2, :pswitch_data_3

    goto :goto_0

    .line 332
    :pswitch_7
    packed-switch p3, :pswitch_data_4

    goto :goto_0

    .line 334
    :pswitch_8
    sget-object v0, Layp;->z:Lbgd;

    goto :goto_1

    .line 336
    :pswitch_9
    sget-object v0, Layp;->A:Lbgd;

    goto :goto_1

    .line 338
    :pswitch_a
    sget-object v0, Layp;->B:Lbgd;

    goto :goto_1

    .line 342
    :pswitch_b
    packed-switch p3, :pswitch_data_5

    goto :goto_0

    .line 344
    :pswitch_c
    sget-object v0, Layp;->C:Lbgd;

    goto :goto_1

    .line 346
    :pswitch_d
    sget-object v0, Layp;->D:Lbgd;

    goto :goto_1

    .line 350
    :pswitch_e
    sget-object v0, Layp;->E:Lbgd;

    goto :goto_1

    .line 312
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_6
    .end packed-switch

    .line 316
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_5
    .end packed-switch

    .line 318
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 330
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_7
        :pswitch_b
        :pswitch_e
    .end packed-switch

    .line 332
    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 342
    :pswitch_data_5
    .packed-switch 0x1
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method private a(Lbog;)Lbgd;
    .locals 4

    .prologue
    .line 299
    sget-object v0, Layp;->l:[[Lbgd;

    iget v1, p1, Lbog;->b:I

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iget v1, p1, Lbog;->a:I

    aget-object v0, v0, v1

    .line 300
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xf

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "invalid layout "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 301
    return-object v0
.end method

.method private a(Lbog;Lbog;Layl;Lbgd;)V
    .locals 8

    .prologue
    .line 229
    iget v2, p3, Layl;->m:F

    .line 231
    invoke-virtual {p2}, Lbog;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lbog;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 232
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 235
    :cond_0
    iget v0, p3, Layl;->f:I

    if-nez v0, :cond_5

    invoke-virtual {p1}, Lbog;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 237
    iget v1, p3, Layl;->m:F

    iget-wide v2, p3, Layl;->e:J

    const-wide/16 v4, 0x2

    div-long/2addr v2, v4

    const-wide/32 v4, 0x30d40

    sub-long v4, v2, v4

    iget-wide v6, p3, Layl;->d:J

    cmp-long v0, v6, v4

    if-gez v0, :cond_1

    sget-object v0, Layp;->f:Lbgd;

    invoke-virtual {p4, v0}, Lbgd;->a(Lbgd;)V

    .line 257
    :goto_0
    return-void

    .line 237
    :cond_1
    const-wide/32 v6, 0x30d40

    add-long/2addr v2, v6

    iget-wide v6, p3, Layl;->d:J

    cmp-long v0, v6, v2

    if-gez v0, :cond_2

    sget-object v0, Layp;->f:Lbgd;

    sget-object v1, Layp;->F:Lbgd;

    iget-wide v2, p3, Layl;->d:J

    sub-long/2addr v2, v4

    long-to-float v2, v2

    const v3, 0x48c35000    # 400000.0f

    div-float/2addr v2, v3

    sget-object v3, Layp;->b:[F

    invoke-static {v2, v3}, Lbag;->a(F[F)F

    move-result v2

    invoke-virtual {p4, v0, v1, v2}, Lbgd;->a(Lbgd;Lbgd;F)V

    goto :goto_0

    :cond_2
    invoke-virtual {p2}, Lbog;->a()Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    cmpl-float v0, v1, v0

    if-nez v0, :cond_3

    sget-object v0, Layp;->F:Lbgd;

    invoke-virtual {p4, v0}, Lbgd;->a(Lbgd;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p2}, Lbog;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0, p2}, Layp;->a(Lbog;)Lbgd;

    move-result-object v0

    :goto_1
    sget-object v2, Layp;->F:Lbgd;

    sget-object v3, Layp;->b:[F

    invoke-static {v1, v3}, Lbag;->a(F[F)F

    move-result v1

    invoke-virtual {p4, v2, v0, v1}, Lbgd;->a(Lbgd;Lbgd;F)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    const/4 v2, 0x0

    iget v3, p2, Lbog;->b:I

    invoke-direct {p0, v0, v2, v3}, Layp;->a(III)Lbgd;

    move-result-object v0

    goto :goto_1

    .line 241
    :cond_5
    invoke-virtual {p2}, Lbog;->a()Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x0

    cmpl-float v0, v2, v0

    if-nez v0, :cond_6

    .line 243
    invoke-direct {p0, p1}, Layp;->a(Lbog;)Lbgd;

    move-result-object v0

    invoke-virtual {p4, v0}, Lbgd;->a(Lbgd;)V

    goto :goto_0

    .line 248
    :cond_6
    invoke-virtual {p1}, Lbog;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 249
    invoke-direct {p0, p1}, Layp;->a(Lbog;)Lbgd;

    move-result-object v0

    .line 251
    :goto_2
    invoke-virtual {p2}, Lbog;->a()Z

    move-result v1

    if-eqz v1, :cond_8

    .line 252
    invoke-direct {p0, p2}, Layp;->a(Lbog;)Lbgd;

    move-result-object v1

    .line 255
    :goto_3
    sget-object v3, Layp;->b:[F

    .line 256
    invoke-static {v2, v3}, Lbag;->a(F[F)F

    move-result v2

    .line 255
    invoke-virtual {p4, v0, v1, v2}, Lbgd;->a(Lbgd;Lbgd;F)V

    goto/16 :goto_0

    .line 249
    :cond_7
    iget v1, p1, Lbog;->b:I

    iget v3, p2, Lbog;->b:I

    .line 250
    sget-object v0, Layp;->u:[[Lbgd;

    add-int/lit8 v4, v1, -0x1

    aget-object v0, v0, v4

    add-int/lit8 v4, v3, -0x1

    aget-object v0, v0, v4

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x32

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "invalid transition from "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " to "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    invoke-static {v0, v1, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    goto :goto_2

    .line 252
    :cond_8
    iget v1, p1, Lbog;->b:I

    iget v3, p1, Lbog;->a:I

    iget v4, p2, Lbog;->b:I

    .line 253
    invoke-direct {p0, v1, v3, v4}, Layp;->a(III)Lbgd;

    move-result-object v1

    goto :goto_3
.end method


# virtual methods
.method public a(Lbmg;Lbmg;)J
    .locals 2

    .prologue
    .line 177
    const-wide/32 v0, 0x61a80

    return-wide v0
.end method

.method public a(Layl;Lbof;)Landroid/graphics/Matrix;
    .locals 8

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f000000    # 0.5f

    .line 389
    invoke-super {p0, p1, p2}, Layr;->a(Layl;Lbof;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 390
    if-nez p2, :cond_1

    .line 391
    :cond_0
    :goto_0
    iget-wide v2, p1, Layl;->a:J

    const-wide/32 v4, 0xcb735

    cmp-long v1, v2, v4

    if-ltz v1, :cond_3

    .line 393
    :goto_1
    return-object v0

    .line 390
    :cond_1
    new-instance v1, Lbgd;

    invoke-direct {v1}, Lbgd;-><init>()V

    iget-object v2, p2, Lbof;->b:Lbog;

    iget-object v3, p2, Lbof;->c:Lbog;

    invoke-direct {p0, v2, v3, p1, v1}, Layp;->a(Lbog;Lbog;Layl;Lbgd;)V

    invoke-virtual {v1}, Lbgd;->b()F

    move-result v2

    invoke-virtual {v1}, Lbgd;->c()F

    move-result v1

    cmpl-float v3, v2, v1

    if-eqz v3, :cond_0

    iget-object v3, p0, Layp;->G:Landroid/graphics/Matrix;

    invoke-virtual {v3, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    cmpl-float v0, v2, v1

    if-lez v0, :cond_2

    iget-object v0, p0, Layp;->G:Landroid/graphics/Matrix;

    div-float/2addr v1, v2

    invoke-virtual {v0, v7, v1, v6, v6}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    :goto_2
    iget-object v0, p0, Layp;->G:Landroid/graphics/Matrix;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Layp;->G:Landroid/graphics/Matrix;

    div-float v1, v2, v1

    invoke-virtual {v0, v1, v7, v6, v6}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto :goto_2

    .line 391
    :cond_3
    iget-wide v2, p1, Layl;->a:J

    long-to-float v1, v2

    const v2, 0x494b7350    # 833333.0f

    div-float/2addr v1, v2

    sget-object v2, Layp;->d:[F

    invoke-static {v1, v2}, Lbag;->a(F[F)F

    move-result v1

    iget-object v2, p0, Layp;->H:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    sub-float v0, v7, v1

    const v2, 0x3fb33333    # 1.4f

    div-float/2addr v0, v2

    add-float/2addr v0, v1

    iget-object v1, p0, Layp;->H:Landroid/graphics/Matrix;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v0, v2, v6}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    iget-object v0, p0, Layp;->H:Landroid/graphics/Matrix;

    goto :goto_1
.end method

.method protected a(Layj;Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;
    .locals 11

    .prologue
    .line 198
    new-instance v1, Layq;

    const-wide/32 v4, 0x61a80

    const-wide/32 v6, 0xcb735

    const-wide/32 v8, 0x13d620

    sget-object v10, Layp;->b:[F

    move-object v2, p0

    move-object v3, p3

    invoke-direct/range {v1 .. v10}, Layq;-><init>(Layj;Landroid/content/res/Resources;JJJ[F)V

    return-object v1
.end method

.method public a(Lbof;Layl;Lbgd;)V
    .locals 2

    .prologue
    .line 213
    iget-object v0, p1, Lbof;->b:Lbog;

    iget-object v1, p1, Lbof;->c:Lbog;

    invoke-direct {p0, v0, v1, p2, p3}, Layp;->a(Lbog;Lbog;Layl;Lbgd;)V

    .line 214
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 192
    const v0, 0x41eb0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x1

    return v0
.end method

.method public e()[F
    .locals 1

    .prologue
    .line 187
    sget-object v0, Layp;->a:[F

    return-object v0
.end method

.method public s(Layl;)J
    .locals 4

    .prologue
    .line 208
    iget-wide v0, p1, Layl;->e:J

    const-wide/16 v2, 0x2

    div-long/2addr v0, v2

    const-wide/32 v2, 0x30d40

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public v(Layl;)F
    .locals 8

    .prologue
    const-wide/32 v6, 0x13d620

    const/4 v1, 0x0

    const/high16 v0, -0x40800000    # -1.0f

    .line 359
    iget-wide v2, p1, Layl;->a:J

    const-wide/32 v4, 0xcb735

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 361
    iget-wide v2, p1, Layl;->a:J

    long-to-float v1, v2

    const v2, 0x494b7350    # 833333.0f

    div-float/2addr v1, v2

    sget-object v2, Layp;->c:[F

    invoke-static {v1, v2}, Lbag;->a(F[F)F

    move-result v1

    add-float/2addr v0, v1

    .line 384
    :cond_0
    :goto_0
    return v0

    .line 365
    :cond_1
    iget-object v2, p1, Layl;->i:Lbmg;

    sget-object v3, Lbmg;->d:Lbmg;

    if-ne v2, v3, :cond_2

    invoke-static {p1}, Lbag;->e(Layl;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 371
    :cond_2
    iget v0, p1, Layl;->f:I

    iget v2, p1, Layl;->h:I

    add-int/lit8 v2, v2, -0x1

    if-ne v0, v2, :cond_5

    const/4 v0, 0x1

    .line 372
    :goto_1
    if-eqz v0, :cond_3

    iget-object v2, p1, Layl;->i:Lbmg;

    sget-object v3, Lbmg;->e:Lbmg;

    if-ne v2, v3, :cond_4

    :cond_3
    if-nez v0, :cond_7

    iget-object v0, p1, Layl;->k:Lbmg;

    sget-object v2, Lbmg;->d:Lbmg;

    if-ne v0, v2, :cond_7

    .line 374
    :cond_4
    iget-wide v2, p1, Layl;->e:J

    iget-wide v4, p1, Layl;->d:J

    sub-long/2addr v2, v4

    .line 375
    cmp-long v0, v2, v6

    if-lez v0, :cond_6

    move v0, v1

    .line 377
    goto :goto_0

    .line 371
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 379
    :cond_6
    sub-long v0, v6, v2

    long-to-float v0, v0

    const v1, 0x499eb100    # 1300000.0f

    div-float/2addr v0, v1

    sget-object v1, Layp;->e:[F

    invoke-static {v0, v1}, Lbag;->a(F[F)F

    move-result v0

    neg-float v0, v0

    goto :goto_0

    :cond_7
    move v0, v1

    .line 384
    goto :goto_0
.end method

.method public z(Layl;)V
    .locals 8

    .prologue
    .line 218
    iget v0, p1, Layl;->f:I

    if-nez v0, :cond_0

    .line 219
    new-instance v0, Lbmp;

    const-wide/16 v2, 0x0

    iget-wide v4, p1, Layl;->e:J

    iget-wide v6, p1, Layl;->o:J

    add-long/2addr v4, v6

    invoke-direct {v0, v2, v3, v4, v5}, Lbmp;-><init>(JJ)V

    iput-object v0, p1, Layl;->r:Lbmp;

    .line 224
    :goto_0
    return-void

    .line 222
    :cond_0
    sget-object v0, Lbmp;->a:Lbmp;

    iput-object v0, p1, Layl;->r:Lbmp;

    goto :goto_0
.end method
