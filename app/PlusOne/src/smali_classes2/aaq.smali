.class public Laaq;
.super Laap;
.source "PG"


# instance fields
.field private c:I


# direct methods
.method constructor <init>(Lzp;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0, p1}, Laap;-><init>(Lzp;)V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Laaq;->c:I

    .line 80
    invoke-virtual {p1}, Lzp;->e()[I

    move-result-object v0

    invoke-virtual {p0, v0}, Laaq;->b([I)V

    .line 81
    return-void
.end method

.method static a(Lzp;)V
    .locals 4

    .prologue
    .line 84
    invoke-virtual {p0}, Lzp;->a()Labf;

    move-result-object v0

    .line 85
    invoke-virtual {v0}, Labf;->f()I

    move-result v1

    if-nez v1, :cond_0

    .line 86
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot access Frame of type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " as a FrameBuffer instance!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 89
    :cond_0
    invoke-virtual {p0}, Lzp;->e()[I

    move-result-object v0

    .line 90
    if-eqz v0, :cond_1

    array-length v0, v0

    if-nez v0, :cond_2

    .line 91
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot access Frame with no dimensions as a FrameBuffer instance!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 94
    :cond_2
    return-void
.end method


# virtual methods
.method public a(I)Ljava/nio/ByteBuffer;
    .locals 3

    .prologue
    .line 39
    iget-boolean v0, p0, Laap;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Attempting to write to read-only frame "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_0
    iget-object v0, p0, Laaq;->a:Lzp;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lzp;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    return-object v0
.end method

.method public a([I)V
    .locals 0

    .prologue
    .line 70
    invoke-super {p0, p1}, Laap;->a([I)V

    .line 71
    return-void
.end method

.method b([I)V
    .locals 4

    .prologue
    .line 97
    const/4 v0, 0x1

    iput v0, p0, Laaq;->c:I

    .line 98
    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p1, v0

    .line 99
    iget v3, p0, Laaq;->c:I

    mul-int/2addr v2, v3

    iput v2, p0, Laaq;->c:I

    .line 98
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 101
    :cond_0
    return-void
.end method

.method public i()[I
    .locals 1

    .prologue
    .line 62
    invoke-super {p0}, Laap;->i()[I

    move-result-object v0

    return-object v0
.end method
