.class public final Lazb;
.super Layr;
.source "PG"


# instance fields
.field private a:Lbal;

.field private b:Lbal;


# direct methods
.method public constructor <init>()V
    .locals 10

    .prologue
    const/16 v5, 0x800

    const/16 v6, 0x100

    const/4 v9, 0x4

    const/4 v4, 0x3

    const/4 v8, 0x2

    .line 60
    new-instance v0, Lays;

    invoke-direct {v0}, Lays;-><init>()V

    const v1, 0x7f020425

    .line 61
    invoke-virtual {v0, v1}, Lays;->a(I)Lays;

    move-result-object v0

    const v1, 0x7f020136

    .line 62
    invoke-virtual {v0, v1}, Lays;->b(I)Lays;

    move-result-object v0

    .line 63
    invoke-virtual {v0, v8}, Lays;->c(I)Lays;

    move-result-object v0

    .line 64
    invoke-virtual {v0, v9}, Lays;->e(I)Lays;

    move-result-object v0

    const/16 v1, 0xe

    .line 65
    invoke-virtual {v0, v1}, Lays;->f(I)Lays;

    move-result-object v0

    sget-object v1, Layc;->e:Layc;

    .line 66
    invoke-virtual {v0, v1}, Lays;->a(Layc;)Lays;

    move-result-object v0

    .line 60
    invoke-direct {p0, v0}, Layr;-><init>(Lays;)V

    .line 69
    new-instance v1, Lbal;

    const-wide/32 v2, 0x61a80

    move v7, v4

    invoke-direct/range {v1 .. v7}, Lbal;-><init>(JIIII)V

    iput-object v1, p0, Lazb;->a:Lbal;

    .line 73
    new-instance v1, Lbal;

    const-wide/32 v2, 0x61a80

    move v4, v8

    move v7, v9

    invoke-direct/range {v1 .. v7}, Lbal;-><init>(JIIII)V

    iput-object v1, p0, Lazb;->b:Lbal;

    .line 76
    return-void
.end method


# virtual methods
.method protected a(Layj;Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;
    .locals 2

    .prologue
    .line 153
    new-instance v0, Layu;

    invoke-direct {v0}, Layu;-><init>()V

    .line 154
    invoke-virtual {v0, p1}, Layu;->a(Layj;)Layu;

    move-result-object v0

    const v1, 0x7f0b00a2

    .line 155
    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Layu;->a(I)Layu;

    move-result-object v0

    const/high16 v1, 0x42100000    # 36.0f

    .line 156
    invoke-virtual {v0, v1}, Layu;->a(F)Layu;

    move-result-object v0

    const/high16 v1, 0x42300000    # 44.0f

    .line 157
    invoke-virtual {v0, v1}, Layu;->b(F)Layu;

    move-result-object v0

    const-string v1, "fonts/Roboto-Black.ttf"

    .line 158
    invoke-static {p2, v1}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Layu;->a(Landroid/graphics/Typeface;)Layu;

    move-result-object v0

    const/4 v1, 0x1

    .line 159
    invoke-virtual {v0, v1}, Layu;->a(Z)Layu;

    move-result-object v0

    const/16 v1, 0x19

    .line 160
    invoke-virtual {v0, v1}, Layu;->c(I)Layu;

    move-result-object v0

    const/4 v1, 0x0

    .line 161
    invoke-virtual {v0, v1}, Layu;->b(Z)Layu;

    move-result-object v0

    const v1, 0x7f0b00a3

    .line 162
    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Layu;->h(I)Layu;

    move-result-object v0

    .line 163
    invoke-virtual {v0}, Layu;->a()Layt;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbmd;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbmd;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 82
    new-instance v1, Ljava/util/Random;

    invoke-virtual {p1}, Lbmd;->hashCode()I

    move-result v2

    int-to-long v2, v2

    invoke-direct {v1, v2, v3}, Ljava/util/Random;-><init>(J)V

    .line 83
    invoke-virtual {v1}, Ljava/util/Random;->nextDouble()D

    move-result-wide v2

    const-wide v4, 0x3fd99999a0000000L    # 0.4000000059604645

    cmpg-double v2, v2, v4

    if-gez v2, :cond_0

    .line 84
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    :cond_0
    invoke-virtual {v1}, Ljava/util/Random;->nextDouble()D

    move-result-wide v2

    .line 88
    const-wide v4, 0x3fd3333340000000L    # 0.30000001192092896

    cmpg-double v1, v2, v4

    if-gez v1, :cond_2

    .line 89
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 94
    :cond_1
    :goto_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 90
    :cond_2
    const-wide v4, 0x3fe3333340000000L    # 0.6000000238418579

    cmpg-double v1, v2, v4

    if-gez v1, :cond_1

    .line 91
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public b(Lbmd;)Lool;
    .locals 9

    .prologue
    .line 100
    iget-object v0, p1, Lbmd;->d:Lbmg;

    sget-object v1, Lbmg;->c:Lbmg;

    if-eq v0, v1, :cond_0

    .line 101
    const/4 v0, 0x0

    .line 103
    :goto_0
    return-object v0

    :cond_0
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    const-wide v2, 0x3fb999999999999aL    # 0.1

    const-wide/16 v4, 0x0

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    move-object v8, p1

    invoke-static/range {v0 .. v8}, Lbah;->a(DDDDLbmd;)Lool;

    move-result-object v0

    goto :goto_0
.end method

.method protected f()Layj;
    .locals 1

    .prologue
    .line 168
    new-instance v0, Lazc;

    invoke-direct {v0, p0}, Lazc;-><init>(Layj;)V

    return-object v0
.end method

.method public f_()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 145
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const v2, 0x7f020136

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const v2, 0x7f020137

    .line 146
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const v2, 0x7f020138

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const v2, 0x7f020139

    .line 147
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const v2, 0x7f02013a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 145
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public j(Layl;)I
    .locals 3

    .prologue
    const v2, 0x3e99999a    # 0.3f

    .line 109
    iget-object v0, p0, Lazb;->a:Lbal;

    invoke-virtual {v0, p1}, Lbal;->a(Layl;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    iget-wide v0, p1, Layl;->a:J

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lbag;->a(I)F

    move-result v0

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    .line 112
    const v0, 0x7f020138

    .line 123
    :goto_0
    return v0

    .line 114
    :cond_0
    const v0, 0x7f020137

    goto :goto_0

    .line 116
    :cond_1
    iget-object v0, p0, Lazb;->b:Lbal;

    invoke-virtual {v0, p1}, Lbal;->a(Layl;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 117
    iget-wide v0, p1, Layl;->a:J

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Lbag;->a(I)F

    move-result v0

    cmpg-float v0, v0, v2

    if-gez v0, :cond_2

    .line 119
    const v0, 0x7f02013a

    goto :goto_0

    .line 121
    :cond_2
    const v0, 0x7f020139

    goto :goto_0

    .line 123
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lazb;->a:Lbal;

    invoke-virtual {v0, p1}, Lbal;->a(Layl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lazb;->a:Lbal;

    invoke-virtual {v0, p1}, Lbal;->c(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 131
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lazb;->b:Lbal;

    invoke-virtual {v0, p1}, Lbal;->c(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    goto :goto_0
.end method

.method public t(Layl;)F
    .locals 4

    .prologue
    .line 136
    const/4 v0, 0x1

    iget-object v1, p1, Layl;->x:Look;

    invoke-static {v0, v1}, Lbag;->a(ILook;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    const v0, -0x40cccccd    # -0.7f

    const v1, -0x40d9999a    # -0.65f

    iget-wide v2, p1, Layl;->a:J

    long-to-int v2, v2

    invoke-static {v0, v1, v2}, Lbag;->a(FFI)F

    move-result v0

    .line 140
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
