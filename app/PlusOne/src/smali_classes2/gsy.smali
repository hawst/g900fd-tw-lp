.class public Lgsy;
.super Lu;


# instance fields
.field private final N:Lgta;

.field private O:Lgsp;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lu;-><init>()V

    new-instance v0, Lgta;

    invoke-direct {v0, p0}, Lgta;-><init>(Lu;)V

    iput-object v0, p0, Lgsy;->N:Lgta;

    return-void
.end method

.method public static a(Lcom/google/android/gms/maps/GoogleMapOptions;)Lgsy;
    .locals 3

    new-instance v0, Lgsy;

    invoke-direct {v0}, Lgsy;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "MapOptions"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-virtual {v0, v1}, Lgsy;->f(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 1

    iget-object v0, p0, Lgsy;->N:Lgta;

    invoke-virtual {v0}, Lgta;->e()V

    invoke-super {p0}, Lu;->A()V

    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lgsy;->N:Lgta;

    invoke-virtual {v0, p1, p2, p3}, Lgta;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a()Lgtn;
    .locals 1

    iget-object v0, p0, Lgsy;->N:Lgta;

    invoke-virtual {v0}, Lgta;->g()V

    iget-object v0, p0, Lgsy;->N:Lgta;

    invoke-virtual {v0}, Lgta;->a()Lgis;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lgsy;->N:Lgta;

    invoke-virtual {v0}, Lgta;->a()Lgis;

    move-result-object v0

    check-cast v0, Lgsz;

    invoke-virtual {v0}, Lgsz;->f()Lgtn;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1

    invoke-super {p0, p1}, Lu;->a(Landroid/app/Activity;)V

    iget-object v0, p0, Lgsy;->N:Lgta;

    invoke-static {v0, p1}, Lgta;->a(Lgta;Landroid/app/Activity;)V

    return-void
.end method

.method public a(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1, p2, p3}, Lu;->a(Landroid/app/Activity;Landroid/util/AttributeSet;Landroid/os/Bundle;)V

    iget-object v0, p0, Lgsy;->N:Lgta;

    invoke-static {v0, p1}, Lgta;->a(Lgta;Landroid/app/Activity;)V

    invoke-static {p1, p2}, Lcom/google/android/gms/maps/GoogleMapOptions;->a(Landroid/content/Context;Landroid/util/AttributeSet;)Lcom/google/android/gms/maps/GoogleMapOptions;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "MapOptions"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lgsy;->N:Lgta;

    invoke-virtual {v0, p1, v1, p3}, Lgta;->a(Landroid/app/Activity;Landroid/os/Bundle;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lu;->a(Landroid/os/Bundle;)V

    iget-object v0, p0, Lgsy;->N:Lgta;

    invoke-virtual {v0, p1}, Lgta;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method public aO_()V
    .locals 1

    invoke-super {p0}, Lu;->aO_()V

    iget-object v0, p0, Lgsy;->N:Lgta;

    invoke-virtual {v0}, Lgta;->b()V

    return-void
.end method

.method public ae_()V
    .locals 1

    iget-object v0, p0, Lgsy;->N:Lgta;

    invoke-virtual {v0}, Lgta;->d()V

    invoke-super {p0}, Lu;->ae_()V

    return-void
.end method

.method public final b()Lgsp;
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lgsy;->a()Lgtn;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    invoke-interface {v1}, Lgtn;->a()Lgth;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lgsy;->O:Lgsp;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgsy;->O:Lgsp;

    invoke-virtual {v0}, Lgsp;->a()Lgth;

    move-result-object v0

    invoke-interface {v0}, Lgth;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {v1}, Lgth;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    if-eq v0, v2, :cond_3

    :cond_2
    new-instance v0, Lgsp;

    invoke-direct {v0, v1}, Lgsp;-><init>(Lgth;)V

    iput-object v0, p0, Lgsy;->O:Lgsp;

    :cond_3
    iget-object v0, p0, Lgsy;->O:Lgsp;

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_0

    const-class v0, Lgsy;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    :cond_0
    invoke-super {p0, p1}, Lu;->d(Landroid/os/Bundle;)V

    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 1

    if-eqz p1, :cond_0

    const-class v0, Lgsy;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    :cond_0
    invoke-super {p0, p1}, Lu;->e(Landroid/os/Bundle;)V

    iget-object v0, p0, Lgsy;->N:Lgta;

    invoke-virtual {v0, p1}, Lgta;->b(Landroid/os/Bundle;)V

    return-void
.end method

.method public f(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Lu;->f(Landroid/os/Bundle;)V

    return-void
.end method

.method public onLowMemory()V
    .locals 1

    iget-object v0, p0, Lgsy;->N:Lgta;

    invoke-virtual {v0}, Lgta;->f()V

    invoke-super {p0}, Lu;->onLowMemory()V

    return-void
.end method

.method public z()V
    .locals 1

    iget-object v0, p0, Lgsy;->N:Lgta;

    invoke-virtual {v0}, Lgta;->c()V

    invoke-super {p0}, Lu;->z()V

    return-void
.end method
