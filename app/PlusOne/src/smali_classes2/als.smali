.class final Lals;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lalz;


# instance fields
.field private synthetic a:Lalr;


# direct methods
.method constructor <init>(Lalr;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lals;->a:Lalr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lals;->a:Lalr;

    invoke-virtual {v0}, Lalr;->c()V

    .line 219
    return-void
.end method

.method public a(FF)V
    .locals 8

    .prologue
    .line 213
    iget-object v1, p0, Lals;->a:Lalr;

    iget-object v0, p0, Lals;->a:Lalr;

    iget-object v0, v0, Lalr;->h:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laly;

    invoke-virtual {v1}, Lalr;->e()V

    invoke-virtual {v1}, Lalr;->d()Lbon;

    move-result-object v2

    invoke-virtual {v2}, Lbon;->e()J

    move-result-wide v2

    iget-object v4, v1, Lalr;->f:Lavv;

    long-to-float v5, v2

    mul-float/2addr v5, p1

    float-to-long v6, v5

    invoke-interface {v4, v6, v7}, Lavv;->e(J)V

    iget-object v4, v1, Lalr;->f:Lavv;

    long-to-float v5, v2

    mul-float/2addr v5, p2

    float-to-long v6, v5

    invoke-interface {v4, v6, v7}, Lavv;->f(J)V

    iget-object v4, v1, Lalr;->b:Lbtu;

    iget-object v5, v1, Lalr;->f:Lavv;

    invoke-interface {v5}, Lavv;->av()J

    move-result-wide v6

    invoke-interface {v4, v6, v7}, Lbtu;->a(J)V

    iget-object v4, v1, Lalr;->c:Lbtu;

    iget-object v5, v1, Lalr;->f:Lavv;

    invoke-interface {v5}, Lavv;->aw()J

    move-result-wide v6

    invoke-interface {v4, v6, v7}, Lbtu;->a(J)V

    sub-float v4, p2, p1

    long-to-float v2, v2

    mul-float/2addr v2, v4

    float-to-long v2, v2

    invoke-interface {v0, v2, v3}, Laly;->a(J)V

    iget-object v0, v1, Lalr;->f:Lavv;

    invoke-interface {v0}, Lavv;->ax()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v1, Lalr;->f:Lavv;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Lavv;->k(Z)V

    iget-object v0, v1, Lalr;->d:Lcdu;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcdu;->b(I)V

    .line 214
    :cond_0
    return-void
.end method

.method public b()V
    .locals 5

    .prologue
    .line 223
    iget-object v0, p0, Lals;->a:Lalr;

    iget-boolean v0, v0, Lamn;->i:Z

    if-eqz v0, :cond_0

    .line 224
    iget-object v1, p0, Lals;->a:Lalr;

    iget-object v0, p0, Lals;->a:Lalr;

    iget-object v0, v0, Lalr;->h:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laly;

    invoke-virtual {v1}, Lalr;->e()V

    iget-object v2, v1, Lalr;->f:Lavv;

    invoke-interface {v2}, Lavv;->F()Lboi;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Laly;->a(Z)V

    invoke-virtual {v1}, Lalr;->d()Lbon;

    move-result-object v2

    iget-object v3, v1, Lalr;->b:Lbtu;

    iget-object v4, v1, Lalr;->a:Lbgf;

    invoke-interface {v0, v4}, Laly;->a(Lbgf;)Lbge;

    move-result-object v0

    new-instance v4, Lalv;

    invoke-direct {v4, v1, v2}, Lalv;-><init>(Lalr;Lbon;)V

    invoke-interface {v3, v2, v0, v4}, Lbtu;->a(Lbon;Lbge;Lbtv;)V

    .line 226
    :cond_0
    return-void
.end method
