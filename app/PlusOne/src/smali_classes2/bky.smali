.class public final Lbky;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lbkz;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lbkz;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lbky;->a:Ljava/util/Map;

    .line 28
    return-void
.end method

.method public static a(Lbku;Lbmp;)Lbky;
    .locals 4

    .prologue
    .line 74
    iget-wide v0, p1, Lbmp;->b:J

    iget-wide v2, p1, Lbmp;->c:J

    invoke-virtual {p0, v0, v1, v2, v3}, Lbku;->a(JJ)[J

    move-result-object v0

    invoke-static {p0, v0}, Lbky;->a(Lbku;[J)Lbky;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lbku;[J)Lbky;
    .locals 6

    .prologue
    .line 100
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 102
    invoke-static {}, Lbkz;->values()[Lbkz;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 103
    invoke-virtual {v4, p0, p1}, Lbkz;->a(Lbku;[J)Ljava/lang/Object;

    move-result-object v5

    .line 104
    if-eqz v5, :cond_0

    .line 105
    invoke-interface {v1, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 102
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 109
    :cond_1
    new-instance v0, Lbky;

    invoke-direct {v0, v1}, Lbky;-><init>(Ljava/util/Map;)V

    return-object v0
.end method

.method public static a(Lbuw;)Lbky;
    .locals 6

    .prologue
    .line 37
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 39
    if-eqz p0, :cond_1

    .line 40
    invoke-static {}, Lbkz;->values()[Lbkz;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 41
    invoke-virtual {v4, p0}, Lbkz;->a(Lbuw;)Ljava/lang/Object;

    move-result-object v5

    .line 42
    if-eqz v5, :cond_0

    .line 43
    invoke-interface {v1, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 40
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    :cond_1
    new-instance v0, Lbky;

    invoke-direct {v0, v1}, Lbky;-><init>(Ljava/util/Map;)V

    return-object v0
.end method


# virtual methods
.method public a()Lbuw;
    .locals 4

    .prologue
    .line 118
    new-instance v2, Lbuw;

    invoke-direct {v2}, Lbuw;-><init>()V

    .line 119
    iget-object v0, p0, Lbky;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 120
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbkz;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0, v2}, Lbkz;->a(Ljava/lang/Object;Lbuw;)V

    goto :goto_0

    .line 122
    :cond_0
    return-object v2
.end method

.method public a(Lbkz;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lbky;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 144
    if-ne p0, p1, :cond_0

    .line 145
    const/4 v0, 0x1

    .line 151
    :goto_0
    return v0

    .line 147
    :cond_0
    instance-of v0, p1, Lbky;

    if-nez v0, :cond_1

    .line 148
    const/4 v0, 0x0

    goto :goto_0

    .line 150
    :cond_1
    check-cast p1, Lbky;

    .line 151
    iget-object v0, p0, Lbky;->a:Ljava/util/Map;

    iget-object v1, p1, Lbky;->a:Ljava/util/Map;

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lbky;->a:Ljava/util/Map;

    const/16 v1, 0x11

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method
