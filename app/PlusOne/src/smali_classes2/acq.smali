.class final Lacq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lacr;


# instance fields
.field private a:Ljava/lang/reflect/Field;

.field private synthetic b:Lacp;


# direct methods
.method public constructor <init>(Lacp;Ljava/lang/reflect/Field;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lacq;->b:Lacp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p2, p0, Lacq;->a:Ljava/lang/reflect/Field;

    .line 57
    return-void
.end method


# virtual methods
.method public a(Lacp;Laap;)V
    .locals 4

    .prologue
    .line 62
    :try_start_0
    iget-object v0, p1, Lacp;->b:Lacy;

    iget-object v0, v0, Lacy;->a:Labf;

    invoke-virtual {v0}, Labf;->g()I

    move-result v0

    if-lez v0, :cond_0

    .line 63
    invoke-virtual {p2}, Laap;->b()Labh;

    move-result-object v0

    .line 64
    iget-object v1, p0, Lacq;->a:Ljava/lang/reflect/Field;

    iget-object v2, p0, Lacq;->b:Lacp;

    iget-object v2, v2, Lacp;->a:Laak;

    invoke-virtual {v0}, Labh;->k()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 72
    :goto_0
    return-void

    .line 66
    :cond_0
    invoke-virtual {p2}, Laap;->a()Labg;

    move-result-object v0

    .line 67
    iget-object v1, p0, Lacq;->a:Ljava/lang/reflect/Field;

    iget-object v2, p0, Lacq;->b:Lacp;

    iget-object v2, v2, Lacp;->a:Laak;

    invoke-virtual {v0}, Labg;->j()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 69
    :catch_0
    move-exception v0

    .line 70
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Assigning frame "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to field "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lacq;->a:Ljava/lang/reflect/Field;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " of filter "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lacq;->b:Lacp;

    iget-object v3, v3, Lacp;->a:Laak;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " caused exception!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
