.class final Lbgg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lbgn;

.field private synthetic b:Landroid/graphics/Bitmap;

.field private synthetic c:Lbgf;


# direct methods
.method constructor <init>(Lbgf;Lbgn;Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lbgg;->c:Lbgf;

    iput-object p2, p0, Lbgg;->a:Lbgn;

    iput-object p3, p0, Lbgg;->b:Landroid/graphics/Bitmap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Landroid/graphics/Bitmap;
    .locals 13

    .prologue
    const/4 v0, 0x0

    .line 92
    iget-object v1, p0, Lbgg;->a:Lbgn;

    invoke-interface {v1}, Lbgn;->d()I

    move-result v2

    .line 93
    iget-object v1, p0, Lbgg;->a:Lbgn;

    invoke-interface {v1}, Lbgn;->e()I

    move-result v3

    .line 97
    iget-object v1, p0, Lbgg;->b:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbgg;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbgg;->b:Landroid/graphics/Bitmap;

    .line 98
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-ne v2, v1, :cond_0

    iget-object v1, p0, Lbgg;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-eq v3, v1, :cond_1

    .line 99
    :cond_0
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v2, v3, v1}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    move-object v12, v1

    .line 110
    :goto_0
    mul-int v8, v2, v3

    .line 111
    iget-object v1, p0, Lbgg;->c:Lbgf;

    invoke-static {v1, v8}, Lbgf;->a(Lbgf;I)Landroid/util/Pair;

    move-result-object v1

    .line 112
    iget-object v6, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/nio/ByteBuffer;

    .line 113
    iget-object v1, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    move-object v7, v1

    check-cast v7, [I

    .line 115
    const-string v1, "glReadPixels"

    invoke-static {v1}, Laep;->a(Ljava/lang/String;)V

    .line 116
    const/16 v4, 0x1908

    const/16 v5, 0x1401

    move v1, v0

    invoke-static/range {v0 .. v6}, Landroid/opengl/GLES20;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    .line 118
    const-string v1, "glReadPixels"

    invoke-static {v1}, Lbgc;->a(Ljava/lang/String;)V

    .line 119
    invoke-static {}, Laep;->a()V

    .line 120
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v1

    invoke-virtual {v1, v7, v0, v8}, Ljava/nio/IntBuffer;->get([III)Ljava/nio/IntBuffer;

    .line 121
    invoke-static {v7, v2, v3}, Lbgf;->a([III)V

    move-object v4, v12

    move-object v5, v7

    move v6, v0

    move v7, v2

    move v8, v0

    move v9, v0

    move v10, v2

    move v11, v3

    .line 122
    invoke-virtual/range {v4 .. v11}, Landroid/graphics/Bitmap;->setPixels([IIIIIII)V

    .line 124
    return-object v12

    .line 104
    :cond_1
    iget-object v1, p0, Lbgg;->b:Landroid/graphics/Bitmap;

    move-object v12, v1

    goto :goto_0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 88
    invoke-virtual {p0}, Lbgg;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
