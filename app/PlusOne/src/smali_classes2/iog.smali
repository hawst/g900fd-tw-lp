.class public final Liog;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Liof;


# static fields
.field private static final b:Likg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Likg",
            "<",
            "Lior;",
            "Lgyv;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Likg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Likg",
            "<",
            "Liox;",
            "Lgyz;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Likg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Likg",
            "<",
            "Liou;",
            "Lgyx;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lioh;

    invoke-direct {v0}, Lioh;-><init>()V

    sput-object v0, Liog;->b:Likg;

    .line 38
    new-instance v0, Lioi;

    invoke-direct {v0}, Lioi;-><init>()V

    sput-object v0, Liog;->c:Likg;

    .line 47
    new-instance v0, Lioj;

    invoke-direct {v0}, Lioj;-><init>()V

    sput-object v0, Liog;->d:Likg;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Liog;->a:Landroid/content/Context;

    .line 57
    return-void
.end method

.method private static a(Lipj;)Lgfz;
    .locals 1

    .prologue
    .line 104
    instance-of v0, p0, Lipk;

    if-eqz v0, :cond_0

    check-cast p0, Lipk;

    .line 105
    invoke-virtual {p0}, Lipk;->e()Lgfz;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lipj;Liot;)Lijq;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lipj;",
            "Liot;",
            ")",
            "Lijq",
            "<",
            "Liou;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    sget-object v0, Lgzf;->c:Lgyt;

    .line 92
    invoke-static {p1}, Liog;->a(Lipj;)Lgfz;

    move-result-object v1

    new-instance v2, Lgyw;

    invoke-direct {v2}, Lgyw;-><init>()V

    invoke-virtual {p2}, Liot;->a()Z

    move-result v3

    invoke-virtual {v2, v3}, Lgyw;->a(Z)Lgyw;

    move-result-object v3

    invoke-virtual {p2}, Liot;->b()I

    move-result v4

    invoke-virtual {v3, v4}, Lgyw;->a(I)Lgyw;

    .line 91
    invoke-interface {v0, v1, v2}, Lgyt;->a(Lgfz;Lgyw;)Lggd;

    move-result-object v0

    .line 94
    new-instance v1, Lijr;

    sget-object v2, Liog;->d:Likg;

    invoke-direct {v1, v0, v2}, Lijr;-><init>(Lggd;Likg;)V

    return-object v1
.end method

.method public a(Lipj;Ljava/lang/String;Ljava/lang/String;Lioq;)Lijq;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lipj;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lioq;",
            ")",
            "Lijq",
            "<",
            "Lior;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    sget-object v0, Lgzf;->c:Lgyt;

    .line 72
    invoke-static {p1}, Liog;->a(Lipj;)Lgfz;

    move-result-object v1

    new-instance v2, Lgyu;

    invoke-direct {v2}, Lgyu;-><init>()V

    invoke-virtual {p4}, Lioq;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lgyu;->a(Ljava/lang/String;)Lgyu;

    move-result-object v3

    invoke-virtual {p4}, Lioq;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lgyu;->b(Ljava/lang/String;)Lgyu;

    move-result-object v3

    invoke-virtual {p4}, Lioq;->c()Z

    move-result v4

    invoke-virtual {v3, v4}, Lgyu;->a(Z)Lgyu;

    move-result-object v3

    invoke-virtual {p4}, Lioq;->d()I

    move-result v4

    invoke-virtual {v3, v4}, Lgyu;->a(I)Lgyu;

    .line 71
    invoke-interface {v0, v1, p2, p3, v2}, Lgyt;->a(Lgfz;Ljava/lang/String;Ljava/lang/String;Lgyu;)Lggd;

    move-result-object v0

    .line 74
    new-instance v1, Lijr;

    sget-object v2, Liog;->b:Likg;

    invoke-direct {v1, v0, v2}, Lijr;-><init>(Lggd;Likg;)V

    return-object v1
.end method

.method public a(Lipj;Ljava/lang/String;Ljava/lang/String;Liow;)Lijq;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lipj;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Liow;",
            ")",
            "Lijq",
            "<",
            "Liox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 81
    sget-object v0, Lgzf;->c:Lgyt;

    .line 82
    invoke-static {p1}, Liog;->a(Lipj;)Lgfz;

    move-result-object v1

    new-instance v2, Lgyy;

    invoke-direct {v2}, Lgyy;-><init>()V

    invoke-virtual {p4}, Liow;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lgyy;->a(Ljava/lang/String;)Lgyy;

    move-result-object v3

    invoke-virtual {p4}, Liow;->c()Ljava/util/Collection;

    move-result-object v4

    invoke-virtual {v3, v4}, Lgyy;->a(Ljava/util/Collection;)Lgyy;

    move-result-object v3

    invoke-virtual {p4}, Liow;->d()I

    move-result v4

    invoke-virtual {v3, v4}, Lgyy;->a(I)Lgyy;

    move-result-object v3

    invoke-virtual {p4}, Liow;->e()Z

    move-result v4

    invoke-virtual {v3, v4}, Lgyy;->a(Z)Lgyy;

    move-result-object v3

    invoke-virtual {p4}, Liow;->f()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lgyy;->a(J)Lgyy;

    move-result-object v3

    invoke-virtual {p4}, Liow;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lgyy;->b(Ljava/lang/String;)Lgyy;

    move-result-object v3

    invoke-virtual {p4}, Liow;->h()I

    move-result v4

    invoke-virtual {v3, v4}, Lgyy;->b(I)Lgyy;

    move-result-object v3

    invoke-virtual {p4}, Liow;->i()I

    move-result v4

    invoke-virtual {v3, v4}, Lgyy;->c(I)Lgyy;

    move-result-object v3

    invoke-virtual {p4}, Liow;->j()I

    move-result v4

    invoke-virtual {v3, v4}, Lgyy;->d(I)Lgyy;

    .line 81
    invoke-interface {v0, v1, p2, p3, v2}, Lgyt;->a(Lgfz;Ljava/lang/String;Ljava/lang/String;Lgyy;)Lggd;

    move-result-object v0

    .line 84
    new-instance v1, Lijr;

    sget-object v2, Liog;->c:Likg;

    invoke-direct {v1, v0, v2}, Lijr;-><init>(Lggd;Likg;)V

    return-object v1
.end method

.method public a()Lipj;
    .locals 4

    .prologue
    .line 61
    new-instance v0, Lgga;

    iget-object v1, p0, Liog;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lgga;-><init>(Landroid/content/Context;)V

    sget-object v1, Lgzf;->b:Lgfo;

    .line 63
    invoke-static {}, Lgzh;->a()Lgzi;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lgzi;->a(I)Lgzi;

    move-result-object v2

    invoke-virtual {v2}, Lgzi;->a()Lgzh;

    move-result-object v2

    .line 62
    invoke-virtual {v0, v1, v2}, Lgga;->a(Lgfo;Lgfq;)Lgga;

    move-result-object v0

    .line 64
    invoke-virtual {v0}, Lgga;->b()Lgfz;

    move-result-object v0

    .line 65
    new-instance v1, Lipk;

    invoke-direct {v1, v0}, Lipk;-><init>(Lgfz;)V

    return-object v1
.end method
