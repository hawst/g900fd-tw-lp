.class public final Lhma;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lhlw;


# instance fields
.field private final a:Lhlz;

.field private final b:Lhmp;

.field private final c:Landroid/os/HandlerThread;

.field private d:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const-class v0, Lhlz;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlz;

    iput-object v0, p0, Lhma;->a:Lhlz;

    .line 45
    new-instance v0, Lhmb;

    invoke-direct {v0, p1}, Lhmb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhma;->b:Lhmp;

    .line 46
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "AnalyticsLogger"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lhma;->c:Landroid/os/HandlerThread;

    .line 47
    iget-object v0, p0, Lhma;->c:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 48
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lhma;->c:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lhma;->d:Landroid/os/Handler;

    .line 49
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lhlv;)V
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    const/16 v8, 0x10

    const/4 v7, 0x3

    .line 53
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v0, v8, :cond_3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v0

    .line 55
    :goto_0
    iget-object v4, p0, Lhma;->b:Lhmp;

    invoke-interface {p2, v4}, Lhlv;->a(Lhmp;)V

    .line 57
    const-string v4, "AnalyticsLogger"

    invoke-static {v4, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 58
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    .line 61
    :cond_0
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 62
    iget-object v5, p0, Lhma;->a:Lhlz;

    invoke-interface {v5, p1, p2, v4}, Lhlz;->a(Landroid/content/Context;Lhlv;Landroid/os/Bundle;)V

    .line 63
    iget-object v5, p0, Lhma;->d:Landroid/os/Handler;

    const/4 v6, 0x1

    invoke-static {v5, v6}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v5

    .line 64
    iput-object p2, v5, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 65
    invoke-virtual {v5, v4}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 66
    iget-object v4, p0, Lhma;->d:Landroid/os/Handler;

    invoke-virtual {v4, v5}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 68
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v4, v8, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    sub-long/2addr v2, v0

    :cond_1
    const-string v0, "AnalyticsLatency"

    invoke-static {v0, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x1f

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Latency: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "ns"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 69
    :cond_2
    return-void

    :cond_3
    move-wide v0, v2

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 3

    .prologue
    .line 73
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 78
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 75
    :pswitch_0
    iget-object v1, p0, Lhma;->a:Lhlz;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lhlv;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lhlz;->a(Lhlv;Landroid/os/Bundle;)V

    .line 76
    const/4 v0, 0x1

    goto :goto_0

    .line 73
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
