.class final Lcjx;
.super Lckh;
.source "PG"


# instance fields
.field private synthetic a:Lcju;


# direct methods
.method constructor <init>(Lcju;)V
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Lcjx;->a:Lcju;

    invoke-direct {p0, p1}, Lckh;-><init>(Lcjy;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Integer;)Z
    .locals 6

    .prologue
    const/16 v5, 0xdf

    const/16 v4, 0xb

    const/16 v3, 0x9

    const/4 v1, 0x1

    .line 236
    iget-object v0, p0, Lcjx;->a:Lcju;

    invoke-static {v0}, Lcju;->a(Lcju;)Lckf;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Lckf;->a(I)V

    .line 237
    iget-object v0, p0, Lcjx;->a:Lcju;

    iget-object v2, p0, Lcjx;->a:Lcju;

    invoke-static {v2}, Lcju;->a(Lcju;)Lckf;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lcju;->a(Lckg;Z)V

    .line 239
    iget-object v0, p0, Lcjx;->a:Lcju;

    invoke-virtual {v0}, Lcju;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    .line 241
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eq v2, v0, :cond_1

    .line 242
    iget-object v0, p0, Lcjx;->a:Lcju;

    invoke-virtual {v0, v5, p1, v1}, Lcju;->a(ILjava/lang/Object;Z)Z

    .line 245
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v0, v4, :cond_0

    .line 246
    iget-object v0, p0, Lcjx;->a:Lcju;

    .line 247
    invoke-virtual {v0}, Lcju;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    .line 248
    iget-object v2, p0, Lcjx;->a:Lcju;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0, v1}, Lcju;->a(ILjava/lang/Object;Z)Z

    .line 255
    :cond_0
    :goto_0
    iget-object v0, p0, Lcjx;->a:Lcju;

    invoke-static {v0}, Lcju;->c(Lcju;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v0, v4, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setEnabled(Z)V

    .line 257
    return v1

    .line 250
    :cond_1
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->frameShouldShuffle(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcjx;->a:Lcju;

    invoke-virtual {v0}, Lcju;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->contextAction(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;I)I

    .line 252
    iget-object v0, p0, Lcjx;->a:Lcju;

    invoke-virtual {v0}, Lcju;->ak()V

    goto :goto_0

    .line 255
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
