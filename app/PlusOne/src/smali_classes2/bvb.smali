.class public final Lbvb;
.super Loxu;
.source "PG"


# instance fields
.field public a:Lbvd;

.field public b:Lbvc;

.field public c:Lbvc;

.field public d:Lbvc;

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 192
    invoke-direct {p0}, Loxu;-><init>()V

    .line 195
    iput-object v0, p0, Lbvb;->a:Lbvd;

    .line 198
    iput-object v0, p0, Lbvb;->b:Lbvc;

    .line 201
    iput-object v0, p0, Lbvb;->c:Lbvc;

    .line 204
    iput-object v0, p0, Lbvb;->d:Lbvc;

    .line 207
    const/4 v0, 0x0

    iput v0, p0, Lbvb;->e:I

    .line 192
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 241
    const/4 v0, 0x0

    .line 242
    iget-object v1, p0, Lbvb;->a:Lbvd;

    if-eqz v1, :cond_0

    .line 243
    const/4 v0, 0x1

    iget-object v1, p0, Lbvb;->a:Lbvd;

    .line 244
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 246
    :cond_0
    iget-object v1, p0, Lbvb;->b:Lbvc;

    if-eqz v1, :cond_1

    .line 247
    const/4 v1, 0x2

    iget-object v2, p0, Lbvb;->b:Lbvc;

    .line 248
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 250
    :cond_1
    iget-object v1, p0, Lbvb;->c:Lbvc;

    if-eqz v1, :cond_2

    .line 251
    const/4 v1, 0x3

    iget-object v2, p0, Lbvb;->c:Lbvc;

    .line 252
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 254
    :cond_2
    iget-object v1, p0, Lbvb;->d:Lbvc;

    if-eqz v1, :cond_3

    .line 255
    const/4 v1, 0x4

    iget-object v2, p0, Lbvb;->d:Lbvc;

    .line 256
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 258
    :cond_3
    iget v1, p0, Lbvb;->e:I

    if-eqz v1, :cond_4

    .line 259
    const/4 v1, 0x5

    iget v2, p0, Lbvb;->e:I

    .line 260
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 262
    :cond_4
    iput v0, p0, Lbvb;->ai:I

    .line 263
    return v0
.end method

.method public a(Loxn;)Lbvb;
    .locals 1

    .prologue
    .line 271
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 272
    sparse-switch v0, :sswitch_data_0

    .line 276
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 277
    :sswitch_0
    return-object p0

    .line 282
    :sswitch_1
    iget-object v0, p0, Lbvb;->a:Lbvd;

    if-nez v0, :cond_1

    .line 283
    new-instance v0, Lbvd;

    invoke-direct {v0}, Lbvd;-><init>()V

    iput-object v0, p0, Lbvb;->a:Lbvd;

    .line 285
    :cond_1
    iget-object v0, p0, Lbvb;->a:Lbvd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 289
    :sswitch_2
    iget-object v0, p0, Lbvb;->b:Lbvc;

    if-nez v0, :cond_2

    .line 290
    new-instance v0, Lbvc;

    invoke-direct {v0}, Lbvc;-><init>()V

    iput-object v0, p0, Lbvb;->b:Lbvc;

    .line 292
    :cond_2
    iget-object v0, p0, Lbvb;->b:Lbvc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 296
    :sswitch_3
    iget-object v0, p0, Lbvb;->c:Lbvc;

    if-nez v0, :cond_3

    .line 297
    new-instance v0, Lbvc;

    invoke-direct {v0}, Lbvc;-><init>()V

    iput-object v0, p0, Lbvb;->c:Lbvc;

    .line 299
    :cond_3
    iget-object v0, p0, Lbvb;->c:Lbvc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 303
    :sswitch_4
    iget-object v0, p0, Lbvb;->d:Lbvc;

    if-nez v0, :cond_4

    .line 304
    new-instance v0, Lbvc;

    invoke-direct {v0}, Lbvc;-><init>()V

    iput-object v0, p0, Lbvb;->d:Lbvc;

    .line 306
    :cond_4
    iget-object v0, p0, Lbvb;->d:Lbvc;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 310
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    iput v0, p0, Lbvb;->e:I

    goto :goto_0

    .line 272
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Lbvb;->a:Lbvd;

    if-eqz v0, :cond_0

    .line 223
    const/4 v0, 0x1

    iget-object v1, p0, Lbvb;->a:Lbvd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 225
    :cond_0
    iget-object v0, p0, Lbvb;->b:Lbvc;

    if-eqz v0, :cond_1

    .line 226
    const/4 v0, 0x2

    iget-object v1, p0, Lbvb;->b:Lbvc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 228
    :cond_1
    iget-object v0, p0, Lbvb;->c:Lbvc;

    if-eqz v0, :cond_2

    .line 229
    const/4 v0, 0x3

    iget-object v1, p0, Lbvb;->c:Lbvc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 231
    :cond_2
    iget-object v0, p0, Lbvb;->d:Lbvc;

    if-eqz v0, :cond_3

    .line 232
    const/4 v0, 0x4

    iget-object v1, p0, Lbvb;->d:Lbvc;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 234
    :cond_3
    iget v0, p0, Lbvb;->e:I

    if-eqz v0, :cond_4

    .line 235
    const/4 v0, 0x5

    iget v1, p0, Lbvb;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 237
    :cond_4
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 188
    invoke-virtual {p0, p1}, Lbvb;->a(Loxn;)Lbvb;

    move-result-object v0

    return-object v0
.end method
