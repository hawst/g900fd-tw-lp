.class public final Lbmr;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Lbmp;

.field private final c:Lbky;


# direct methods
.method public constructor <init>(Lbmp;I)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Lbmr;-><init>(Lbmp;Lbky;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Lbmp;Lbky;I)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lbmr;->b:Lbmp;

    .line 31
    iput-object p2, p0, Lbmr;->c:Lbky;

    .line 32
    iput p3, p0, Lbmr;->a:I

    .line 33
    return-void
.end method


# virtual methods
.method public a(Lbok;)Lbmd;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lbmr;->b:Lbmp;

    invoke-static {p1, v0}, Lbpm;->a(Lbok;Lbmp;)Lbmd;

    move-result-object v0

    return-object v0
.end method

.method public a()Lbmp;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lbmr;->b:Lbmp;

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lbmr;->a:I

    return v0
.end method

.method public c()Lbky;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lbmr;->c:Lbky;

    return-object v0
.end method

.method public d()Lbve;
    .locals 4

    .prologue
    .line 71
    new-instance v0, Lbve;

    invoke-direct {v0}, Lbve;-><init>()V

    .line 72
    iget-object v1, p0, Lbmr;->b:Lbmp;

    iget-wide v2, v1, Lbmp;->b:J

    iput-wide v2, v0, Lbve;->b:J

    .line 73
    iget-object v1, p0, Lbmr;->b:Lbmp;

    iget-wide v2, v1, Lbmp;->c:J

    iput-wide v2, v0, Lbve;->c:J

    .line 74
    iget v1, p0, Lbmr;->a:I

    iput v1, v0, Lbve;->d:I

    .line 76
    iget-object v1, p0, Lbmr;->c:Lbky;

    if-eqz v1, :cond_0

    .line 77
    iget-object v1, p0, Lbmr;->c:Lbky;

    invoke-virtual {v1}, Lbky;->a()Lbuw;

    move-result-object v1

    iput-object v1, v0, Lbve;->e:Lbuw;

    .line 80
    :cond_0
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 93
    if-ne p0, p1, :cond_1

    .line 103
    :cond_0
    :goto_0
    return v0

    .line 96
    :cond_1
    instance-of v2, p1, Lbmr;

    if-nez v2, :cond_2

    move v0, v1

    .line 97
    goto :goto_0

    .line 99
    :cond_2
    check-cast p1, Lbmr;

    .line 101
    iget v2, p0, Lbmr;->a:I

    iget v3, p1, Lbmr;->a:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lbmr;->b:Lbmp;

    iget-object v3, p1, Lbmr;->b:Lbmp;

    .line 102
    invoke-static {v2, v3}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lbmr;->c:Lbky;

    iget-object v3, p1, Lbmr;->c:Lbky;

    .line 103
    invoke-static {v2, v3}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 86
    iget-object v0, p0, Lbmr;->c:Lbky;

    iget-object v1, p0, Lbmr;->b:Lbmp;

    iget v2, p0, Lbmr;->a:I

    .line 88
    add-int/lit16 v2, v2, 0x20f

    .line 87
    invoke-static {v1, v2}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v1

    .line 86
    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method
