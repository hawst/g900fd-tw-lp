.class public final Lhfs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhfr;


# instance fields
.field private final a:Lihm;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const-class v0, Lihm;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lihm;

    iput-object v0, p0, Lhfs;->a:Lihm;

    .line 25
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v2, 0x6

    .line 29
    invoke-static {}, Llsx;->c()V

    .line 31
    :try_start_0
    iget-object v0, p0, Lhfs;->a:Lihm;

    invoke-interface {v0, p1}, Lihm;->b(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Liho; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lihl; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 45
    :goto_0
    return-object v0

    .line 32
    :catch_0
    move-exception v0

    .line 33
    const-string v1, "StableAccountIdProvider"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 34
    const-string v1, "StableAccountIdProvider"

    const-string v2, "Error when acquiring stable account id"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 45
    :cond_0
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 36
    :catch_1
    move-exception v0

    .line 37
    const-string v1, "StableAccountIdProvider"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38
    const-string v1, "StableAccountIdProvider"

    const-string v2, "Error when acquiring stable account id"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 40
    :catch_2
    move-exception v0

    .line 41
    const-string v1, "StableAccountIdProvider"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 42
    const-string v1, "StableAccountIdProvider"

    const-string v2, "Error when acquiring stable account id"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
