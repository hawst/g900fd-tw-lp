.class public Lbtw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcgq;
.implements Livo;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lbtt;

.field private final c:Lcgw;

.field private final d:Lbtc;

.field private final e:Layl;

.field private f:Z

.field private volatile g:Z

.field private h:Lbhv;

.field private i:Lbop;

.field private volatile j:Lbtx;

.field private k:I

.field private l:Lbmd;

.field private m:Lbmd;

.field private n:J

.field private o:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbhr",
            "<",
            "Lbhl;",
            ">;>;"
        }
    .end annotation
.end field

.field private p:Lbte;

.field private final q:Lbge;

.field private r:Lbhr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbhr",
            "<",
            "Lbhl;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lbhl;

.field private t:Lbhl;

.field private u:J

.field private v:J

.field private w:J

.field private x:J

.field private final y:Lbrz;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lbtw;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbtw;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbtt;Lbge;Lbtc;)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, 0x0

    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    new-instance v0, Lbrz;

    invoke-direct {v0}, Lbrz;-><init>()V

    iput-object v0, p0, Lbtw;->y:Lbrz;

    .line 180
    const-string v0, "listener"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbtt;

    iput-object v0, p0, Lbtw;->b:Lbtt;

    .line 181
    const-string v0, "videoSink"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbge;

    iput-object v0, p0, Lbtw;->q:Lbge;

    .line 182
    iput-object p3, p0, Lbtw;->d:Lbtc;

    .line 184
    new-instance v0, Layl;

    invoke-direct {v0}, Layl;-><init>()V

    iput-object v0, p0, Lbtw;->e:Layl;

    .line 186
    new-instance v0, Lcgw;

    sget-object v1, Lbtw;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcgw;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbtw;->c:Lcgw;

    .line 188
    iput-wide v2, p0, Lbtw;->v:J

    .line 189
    iput-wide v2, p0, Lbtw;->w:J

    .line 190
    return-void
.end method

.method private a(Lbhr;)Lbhl;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbhr",
            "<",
            "Lbhl;",
            ">;)",
            "Lbhl;"
        }
    .end annotation

    .prologue
    .line 870
    invoke-interface {p1}, Lbhr;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhl;

    .line 871
    if-eqz v0, :cond_0

    .line 872
    iget-wide v2, v0, Lbhl;->a:J

    .line 890
    iget-object v1, p0, Lbtw;->r:Lbhr;

    if-ne p1, v1, :cond_1

    .line 891
    iput-wide v2, p0, Lbtw;->v:J

    .line 899
    :cond_0
    :goto_0
    return-object v0

    .line 892
    :cond_1
    iget-object v1, p0, Lbtw;->o:Ljava/util/List;

    iget v4, p0, Lbtw;->k:I

    add-int/lit8 v4, v4, 0x1

    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne p1, v1, :cond_2

    .line 893
    iput-wide v2, p0, Lbtw;->w:J

    goto :goto_0

    .line 895
    :cond_2
    const-string v0, "Pulled frame from an unrecognised source"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 906
    iput p1, p0, Lbtw;->k:I

    .line 907
    iget-object v0, p0, Lbtw;->o:Ljava/util/List;

    iget v1, p0, Lbtw;->k:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    iput-object v0, p0, Lbtw;->r:Lbhr;

    .line 908
    iget-object v0, p0, Lbtw;->i:Lbop;

    invoke-interface {v0, p1}, Lbop;->b(I)Lbmd;

    move-result-object v0

    iput-object v0, p0, Lbtw;->l:Lbmd;

    .line 909
    iget v0, p0, Lbtw;->k:I

    iget-object v1, p0, Lbtw;->i:Lbop;

    invoke-interface {v1}, Lbop;->e()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 910
    iget-object v0, p0, Lbtw;->i:Lbop;

    iget v1, p0, Lbtw;->k:I

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Lbop;->b(I)Lbmd;

    move-result-object v0

    iput-object v0, p0, Lbtw;->m:Lbmd;

    .line 919
    :goto_0
    return-void

    .line 912
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lbtw;->m:Lbmd;

    goto :goto_0
.end method

.method private a(JZ)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 659
    if-eqz p3, :cond_3

    .line 661
    iget v0, p0, Lbtw;->k:I

    iget-object v1, p0, Lbtw;->i:Lbop;

    invoke-interface {v1}, Lbop;->e()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_1

    .line 663
    iget v0, p0, Lbtw;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbtw;->k:I

    .line 664
    iput-object v6, p0, Lbtw;->l:Lbmd;

    .line 665
    iput-object v6, p0, Lbtw;->m:Lbmd;

    .line 667
    iput-object v6, p0, Lbtw;->r:Lbhr;

    .line 668
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, Lbtw;->x:J

    .line 669
    iget-object v0, p0, Lbtw;->b:Lbtt;

    invoke-interface {v0}, Lbtt;->a()V

    .line 708
    :cond_0
    :goto_0
    iget-object v0, p0, Lbtw;->c:Lcgw;

    const-string v1, "Update stream state"

    invoke-virtual {v0, v1}, Lcgw;->b(Ljava/lang/String;)V

    .line 709
    return-void

    .line 672
    :cond_1
    iget-wide v0, p0, Lbtw;->n:J

    iget-object v2, p0, Lbtw;->l:Lbmd;

    .line 673
    invoke-virtual {v2}, Lbmd;->b()J

    move-result-wide v2

    iget-object v4, p0, Lbtw;->l:Lbmd;

    iget-object v4, v4, Lbmd;->f:Lbmp;

    iget-wide v4, v4, Lbmp;->b:J

    add-long/2addr v2, v4

    iget-object v4, p0, Lbtw;->m:Lbmd;

    iget-object v4, v4, Lbmd;->f:Lbmp;

    iget-wide v4, v4, Lbmp;->b:J

    sub-long/2addr v2, v4

    iget v4, p0, Lbtw;->k:I

    .line 676
    invoke-direct {p0, v4}, Lbtw;->b(I)J

    move-result-wide v4

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    iput-wide v0, p0, Lbtw;->n:J

    .line 678
    iget v0, p0, Lbtw;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbtw;->k:I

    .line 679
    iget-object v0, p0, Lbtw;->m:Lbmd;

    iput-object v0, p0, Lbtw;->l:Lbmd;

    .line 680
    iget v0, p0, Lbtw;->k:I

    iget-object v1, p0, Lbtw;->i:Lbop;

    invoke-interface {v1}, Lbop;->e()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 681
    iget-object v0, p0, Lbtw;->i:Lbop;

    iget v1, p0, Lbtw;->k:I

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Lbop;->b(I)Lbmd;

    move-result-object v0

    iput-object v0, p0, Lbtw;->m:Lbmd;

    .line 687
    sget-object v0, Lbtx;->b:Lbtx;

    iput-object v0, p0, Lbtw;->j:Lbtx;

    .line 693
    :goto_1
    invoke-direct {p0}, Lbtw;->e()V

    .line 696
    iget-object v0, p0, Lbtw;->o:Ljava/util/List;

    iget v1, p0, Lbtw;->k:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    iput-object v0, p0, Lbtw;->r:Lbhr;

    .line 697
    iget-object v0, p0, Lbtw;->t:Lbhl;

    iput-object v0, p0, Lbtw;->s:Lbhl;

    .line 698
    iput-object v6, p0, Lbtw;->t:Lbhl;

    .line 699
    iget-wide v0, p0, Lbtw;->w:J

    iput-wide v0, p0, Lbtw;->v:J

    .line 700
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbtw;->w:J

    goto :goto_0

    .line 690
    :cond_2
    iput-object v6, p0, Lbtw;->m:Lbmd;

    .line 691
    sget-object v0, Lbtx;->a:Lbtx;

    iput-object v0, p0, Lbtw;->j:Lbtx;

    goto :goto_1

    .line 702
    :cond_3
    invoke-direct {p0}, Lbtw;->g()J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    iget-object v0, p0, Lbtw;->j:Lbtx;

    sget-object v1, Lbtx;->c:Lbtx;

    if-eq v0, v1, :cond_0

    .line 705
    sget-object v0, Lbtx;->c:Lbtx;

    iput-object v0, p0, Lbtw;->j:Lbtx;

    goto/16 :goto_0
.end method

.method private a(Lbop;)V
    .locals 4

    .prologue
    .line 496
    const-string v0, "timeline"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbop;

    iput-object v0, p0, Lbtw;->i:Lbop;

    .line 497
    iget-object v0, p0, Lbtw;->e:Layl;

    iget-object v1, p0, Lbtw;->i:Lbop;

    invoke-interface {v1}, Lbop;->n()J

    move-result-wide v2

    iput-wide v2, v0, Layl;->v:J

    .line 498
    iget-object v0, p0, Lbtw;->p:Lbte;

    iget-object v1, p0, Lbtw;->i:Lbop;

    invoke-interface {v1}, Lbop;->i()Layj;

    move-result-object v1

    invoke-interface {v0, v1}, Lbte;->a(Layj;)V

    .line 499
    return-void
.end method

.method private b(I)J
    .locals 3

    .prologue
    const-wide/16 v0, 0x0

    .line 1055
    if-ltz p1, :cond_0

    iget-object v2, p0, Lbtw;->i:Lbop;

    invoke-interface {v2}, Lbop;->e()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne p1, v2, :cond_1

    .line 1059
    :cond_0
    :goto_0
    return-wide v0

    .line 1058
    :cond_1
    iget-object v2, p0, Lbtw;->i:Lbop;

    invoke-interface {v2, p1}, Lbop;->d(I)Lcac;

    move-result-object v2

    .line 1059
    if-eqz v2, :cond_0

    invoke-interface {v2}, Lcac;->b()J

    move-result-wide v0

    goto :goto_0
.end method

.method private c(J)Z
    .locals 11

    .prologue
    .line 926
    const-wide/16 v4, 0x0

    .line 927
    const-wide/16 v2, 0x0

    .line 928
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lbtw;->k:I

    if-gt v0, v1, :cond_1

    .line 929
    iget-object v1, p0, Lbtw;->i:Lbop;

    invoke-interface {v1, v0}, Lbop;->b(I)Lbmd;

    move-result-object v1

    .line 931
    invoke-virtual {v1}, Lbmd;->b()J

    move-result-wide v4

    add-long v6, v2, v4

    .line 932
    iget-object v1, p0, Lbtw;->i:Lbop;

    invoke-interface {v1}, Lbop;->e()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lbtw;->i:Lbop;

    .line 933
    invoke-interface {v1, v0}, Lbop;->d(I)Lcac;

    move-result-object v1

    invoke-interface {v1}, Lcac;->b()J

    move-result-wide v4

    :goto_1
    sub-long v4, v6, v4

    .line 928
    add-int/lit8 v0, v0, 0x1

    move-wide v8, v4

    move-wide v4, v2

    move-wide v2, v8

    goto :goto_0

    .line 933
    :cond_0
    const-wide/16 v4, 0x0

    goto :goto_1

    .line 936
    :cond_1
    iget-object v0, p0, Lbtw;->l:Lbmd;

    iget-object v0, v0, Lbmd;->f:Lbmp;

    iget-wide v0, v0, Lbmp;->b:J

    sub-long v0, v4, v0

    iput-wide v0, p0, Lbtw;->n:J

    .line 939
    iget-object v0, p0, Lbtw;->c:Lcgw;

    const-string v1, "seek()"

    invoke-virtual {v0, v1}, Lcgw;->a(Ljava/lang/String;)V

    .line 940
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbtw;->g:Z

    .line 942
    iget-object v0, p0, Lbtw;->c:Lcgw;

    const-string v1, "Initialize state"

    invoke-virtual {v0, v1}, Lcgw;->b(Ljava/lang/String;)V

    .line 943
    sget-object v0, Lbtx;->a:Lbtx;

    iput-object v0, p0, Lbtw;->j:Lbtx;

    .line 947
    :try_start_0
    iget-object v1, p0, Lbtw;->r:Lbhr;

    sub-long v2, p1, v4

    iget-object v0, p0, Lbtw;->l:Lbmd;

    iget-object v0, v0, Lbmd;->f:Lbmp;

    iget-wide v6, v0, Lbmp;->b:J

    add-long/2addr v2, v6

    const/4 v0, 0x0

    :cond_2
    if-eqz v0, :cond_3

    invoke-interface {v1, v0}, Lbhr;->a(Ljava/lang/Object;)V

    :cond_3
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lbtw;->g:Z

    if-eqz v0, :cond_7

    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbtw;->g:Z

    new-instance v0, Ljava/lang/InterruptedException;

    const-string v1, "interrupted while pulling a video frame"

    invoke-direct {v0, v1}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 950
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-object v0, p0, Lbtw;->s:Lbhl;

    .line 951
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 953
    :goto_2
    iget-object v0, p0, Lbtw;->c:Lcgw;

    const-string v1, "Prepare primary source"

    invoke-virtual {v0, v1}, Lcgw;->b(Ljava/lang/String;)V

    .line 956
    iget-object v0, p0, Lbtw;->m:Lbmd;

    if-eqz v0, :cond_5

    .line 957
    iget-object v0, p0, Lbtw;->l:Lbmd;

    invoke-virtual {v0}, Lbmd;->b()J

    move-result-wide v0

    iget-object v2, p0, Lbtw;->i:Lbop;

    iget v3, p0, Lbtw;->k:I

    .line 958
    invoke-interface {v2, v3}, Lbop;->d(I)Lcac;

    move-result-object v2

    invoke-interface {v2}, Lcac;->b()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 959
    sub-long v2, p1, v4

    cmp-long v0, v2, v0

    if-ltz v0, :cond_9

    .line 960
    sget-object v0, Lbtx;->c:Lbtx;

    iput-object v0, p0, Lbtw;->j:Lbtx;

    .line 966
    :goto_3
    iget-object v0, p0, Lbtw;->c:Lcgw;

    const-string v1, "Prepare secondary source"

    invoke-virtual {v0, v1}, Lcgw;->b(Ljava/lang/String;)V

    .line 970
    :cond_5
    iget-object v0, p0, Lbtw;->t:Lbhl;

    if-nez v0, :cond_6

    .line 971
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbtw;->w:J

    .line 973
    :cond_6
    iget-object v0, p0, Lbtw;->s:Lbhl;

    if-eqz v0, :cond_a

    .line 974
    iget-object v0, p0, Lbtw;->s:Lbhl;

    iget-wide v0, v0, Lbhl;->a:J

    iget-wide v2, p0, Lbtw;->n:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lbtw;->u:J

    .line 981
    :goto_4
    invoke-direct {p0}, Lbtw;->e()V

    .line 982
    iget-object v0, p0, Lbtw;->e:Layl;

    const-wide/16 v2, 0x0

    iput-wide v2, v0, Layl;->c:J

    .line 984
    iget-object v0, p0, Lbtw;->c:Lcgw;

    const-string v1, "Updating timestamps and clip parameters"

    invoke-virtual {v0, v1}, Lcgw;->b(Ljava/lang/String;)V

    .line 985
    iget-object v0, p0, Lbtw;->c:Lcgw;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcgw;->a(Z)V

    .line 987
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 988
    new-instance v0, Ljava/lang/InterruptedException;

    const-string v1, "Interrupted while seeking video timeline."

    invoke-direct {v0, v1}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 947
    :cond_7
    :try_start_1
    invoke-direct {p0, v1}, Lbtw;->a(Lbhr;)Lbhl;

    move-result-object v0

    if-eqz v0, :cond_3

    if-eqz v0, :cond_8

    iget-wide v6, v0, Lbhl;->a:J

    cmp-long v6, v6, v2

    if-gez v6, :cond_8

    iget-boolean v6, v0, Lbhl;->b:Z

    if-eqz v6, :cond_2

    :cond_8
    iput-object v0, p0, Lbtw;->s:Lbhl;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 962
    :cond_9
    sget-object v0, Lbtx;->b:Lbtx;

    iput-object v0, p0, Lbtw;->j:Lbtx;

    goto :goto_3

    .line 977
    :cond_a
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbtw;->v:J

    goto :goto_4

    .line 991
    :cond_b
    iget-object v0, p0, Lbtw;->r:Lbhr;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lbtw;->s:Lbhl;

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    :goto_5
    return v0

    :cond_c
    const/4 v0, 0x0

    goto :goto_5
.end method

.method private d()Lbth;
    .locals 15

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 541
    new-instance v10, Lbti;

    invoke-direct {v10}, Lbti;-><init>()V

    .line 543
    iget-object v0, p0, Lbtw;->j:Lbtx;

    sget-object v3, Lbtx;->c:Lbtx;

    if-ne v0, v3, :cond_3

    move v0, v1

    .line 544
    :goto_0
    invoke-virtual {v10, v0}, Lbti;->a(Z)Lbti;

    .line 546
    iget-object v3, p0, Lbtw;->l:Lbmd;

    iget v3, v3, Lbmd;->i:I

    add-int/lit8 v11, v3, 0x1

    .line 547
    if-eqz v0, :cond_4

    iget-object v3, p0, Lbtw;->m:Lbmd;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lbtw;->m:Lbmd;

    iget v3, v3, Lbmd;->i:I

    add-int/lit8 v3, v3, 0x1

    move v9, v3

    .line 549
    :goto_1
    if-eqz v0, :cond_5

    move v3, v1

    :goto_2
    add-int v12, v11, v3

    .line 554
    if-eqz v0, :cond_6

    sub-int v3, v12, v9

    move v8, v3

    .line 555
    :goto_3
    add-int/lit8 v3, v11, 0x1

    if-gt v9, v3, :cond_7

    move v3, v1

    :goto_4
    const-string v4, "new layout should at most include one extra input"

    invoke-static {v3, v4}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    move v7, v2

    .line 557
    :goto_5
    if-ge v7, v12, :cond_e

    .line 558
    sub-int v3, v7, v11

    add-int/lit8 v4, v3, 0x1

    .line 559
    if-gez v4, :cond_0

    iget-object v3, p0, Lbtw;->y:Lbrz;

    invoke-virtual {v3}, Lbrz;->b()I

    move-result v3

    if-ge v7, v3, :cond_2

    .line 561
    :cond_0
    if-eqz v0, :cond_8

    add-int/lit8 v3, v12, -0x1

    if-ne v7, v3, :cond_8

    move v3, v1

    .line 567
    :goto_6
    if-eqz v3, :cond_9

    .line 568
    invoke-static {v11}, Lbog;->a(I)Lbog;

    move-result-object v3

    move-object v6, v3

    .line 570
    :goto_7
    if-eqz v0, :cond_1

    if-ge v7, v8, :cond_a

    .line 571
    :cond_1
    invoke-static {v9}, Lbog;->a(I)Lbog;

    move-result-object v3

    move-object v5, v3

    .line 574
    :goto_8
    if-gez v4, :cond_b

    iget-object v3, p0, Lbtw;->y:Lbrz;

    .line 575
    invoke-virtual {v3, v7}, Lbrz;->a(I)Lbhl;

    move-result-object v3

    .line 577
    :goto_9
    iget-object v13, p0, Lbtw;->i:Lbop;

    if-gez v4, :cond_d

    iget-object v4, p0, Lbtw;->y:Lbrz;

    .line 580
    invoke-virtual {v4, v7}, Lbrz;->b(I)I

    move-result v4

    .line 579
    :goto_a
    invoke-interface {v13, v4}, Lbop;->c(I)Lbmh;

    move-result-object v4

    .line 577
    invoke-virtual {v10, v3, v4, v6, v5}, Lbti;->a(Lbhl;Lbmh;Lbog;Lbog;)Lbti;

    .line 557
    :cond_2
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    goto :goto_5

    :cond_3
    move v0, v2

    .line 543
    goto :goto_0

    :cond_4
    move v9, v2

    .line 547
    goto :goto_1

    :cond_5
    move v3, v2

    .line 549
    goto :goto_2

    :cond_6
    move v8, v2

    .line 554
    goto :goto_3

    :cond_7
    move v3, v2

    .line 555
    goto :goto_4

    :cond_8
    move v3, v2

    .line 561
    goto :goto_6

    .line 569
    :cond_9
    invoke-static {v7, v11}, Lbog;->a(II)Lbog;

    move-result-object v3

    move-object v6, v3

    goto :goto_7

    .line 571
    :cond_a
    sub-int v3, v7, v8

    .line 572
    invoke-static {v3, v9}, Lbog;->a(II)Lbog;

    move-result-object v3

    move-object v5, v3

    goto :goto_8

    .line 575
    :cond_b
    if-nez v4, :cond_c

    iget-object v3, p0, Lbtw;->s:Lbhl;

    goto :goto_9

    :cond_c
    iget-object v3, p0, Lbtw;->t:Lbhl;

    goto :goto_9

    .line 580
    :cond_d
    iget v14, p0, Lbtw;->k:I

    add-int/2addr v4, v14

    goto :goto_a

    .line 586
    :cond_e
    invoke-virtual {v10}, Lbti;->a()Lbth;

    move-result-object v0

    return-object v0
.end method

.method private e()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 630
    iget-object v0, p0, Lbtw;->e:Layl;

    iget v1, p0, Lbtw;->k:I

    iput v1, v0, Layl;->f:I

    .line 631
    iget-object v0, p0, Lbtw;->e:Layl;

    iget-object v1, p0, Lbtw;->i:Lbop;

    invoke-interface {v1}, Lbop;->e()I

    move-result v1

    iput v1, v0, Layl;->h:I

    .line 632
    iget-object v0, p0, Lbtw;->e:Layl;

    iget-object v1, p0, Lbtw;->l:Lbmd;

    invoke-virtual {v1}, Lbmd;->b()J

    move-result-wide v2

    iput-wide v2, v0, Layl;->e:J

    .line 633
    iget-object v0, p0, Lbtw;->e:Layl;

    iget-object v1, p0, Lbtw;->l:Lbmd;

    iget-object v1, v1, Lbmd;->d:Lbmg;

    iput-object v1, v0, Layl;->i:Lbmg;

    .line 634
    iget-object v0, p0, Lbtw;->e:Layl;

    iget v1, p0, Lbtw;->k:I

    invoke-direct {p0, v1}, Lbtw;->b(I)J

    move-result-wide v2

    iput-wide v2, v0, Layl;->o:J

    .line 635
    iget-object v0, p0, Lbtw;->e:Layl;

    iget v1, p0, Lbtw;->k:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lbtw;->b(I)J

    move-result-wide v2

    iput-wide v2, v0, Layl;->n:J

    .line 636
    iget-object v0, p0, Lbtw;->e:Layl;

    iget-object v1, p0, Lbtw;->i:Lbop;

    invoke-interface {v1}, Lbop;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Layl;->p:Ljava/lang/String;

    .line 637
    iget-object v0, p0, Lbtw;->e:Layl;

    iget-object v1, p0, Lbtw;->i:Lbop;

    invoke-interface {v1}, Lbop;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Layl;->q:Ljava/lang/String;

    .line 638
    iget-object v0, p0, Lbtw;->e:Layl;

    iget-object v1, p0, Lbtw;->i:Lbop;

    invoke-interface {v1}, Lbop;->l()Lbmp;

    move-result-object v1

    iput-object v1, v0, Layl;->r:Lbmp;

    .line 639
    iget-object v0, p0, Lbtw;->e:Layl;

    iget-object v1, p0, Lbtw;->i:Lbop;

    iget v2, p0, Lbtw;->k:I

    .line 640
    invoke-interface {v1, v2}, Lbop;->c(I)Lbmh;

    move-result-object v1

    .line 641
    invoke-virtual {v1}, Lbmh;->b()Lbmi;

    move-result-object v1

    iget-object v2, p0, Lbtw;->l:Lbmd;

    invoke-virtual {v2}, Lbmd;->b()J

    move-result-wide v2

    invoke-interface {v1, v4, v5, v2, v3}, Lbmi;->a(JJ)Lbmj;

    move-result-object v1

    iput-object v1, v0, Layl;->s:Lbmj;

    .line 642
    iget-object v0, p0, Lbtw;->e:Layl;

    iget-object v1, p0, Lbtw;->l:Lbmd;

    invoke-virtual {v1}, Lbmd;->c()Look;

    move-result-object v1

    iput-object v1, v0, Layl;->x:Look;

    .line 643
    iget v0, p0, Lbtw;->k:I

    iget-object v1, p0, Lbtw;->i:Lbop;

    invoke-interface {v1}, Lbop;->e()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_0

    .line 644
    iget-object v0, p0, Lbtw;->e:Layl;

    iget-object v1, p0, Lbtw;->m:Lbmd;

    iget-object v1, v1, Lbmd;->d:Lbmg;

    iput-object v1, v0, Layl;->k:Lbmg;

    .line 645
    iget-object v0, p0, Lbtw;->e:Layl;

    iget-object v1, p0, Lbtw;->m:Lbmd;

    invoke-virtual {v1}, Lbmd;->b()J

    move-result-wide v2

    iput-wide v2, v0, Layl;->l:J

    .line 646
    iget-object v0, p0, Lbtw;->e:Layl;

    iget-object v1, p0, Lbtw;->i:Lbop;

    iget v2, p0, Lbtw;->k:I

    add-int/lit8 v2, v2, 0x1

    .line 647
    invoke-interface {v1, v2}, Lbop;->c(I)Lbmh;

    move-result-object v1

    .line 648
    invoke-virtual {v1}, Lbmh;->b()Lbmi;

    move-result-object v1

    iget-object v2, p0, Lbtw;->l:Lbmd;

    invoke-virtual {v2}, Lbmd;->b()J

    move-result-wide v2

    invoke-interface {v1, v4, v5, v2, v3}, Lbmi;->a(JJ)Lbmj;

    move-result-object v1

    iput-object v1, v0, Layl;->t:Lbmj;

    .line 649
    iget-object v0, p0, Lbtw;->e:Layl;

    iget-object v1, p0, Lbtw;->m:Lbmd;

    invoke-virtual {v1}, Lbmd;->c()Look;

    move-result-object v1

    iput-object v1, v0, Layl;->y:Look;

    .line 651
    :cond_0
    iget v0, p0, Lbtw;->k:I

    if-lez v0, :cond_1

    .line 652
    iget-object v0, p0, Lbtw;->e:Layl;

    iget-object v1, p0, Lbtw;->i:Lbop;

    iget v2, p0, Lbtw;->k:I

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Lbop;->b(I)Lbmd;

    move-result-object v1

    iget-object v1, v1, Lbmd;->d:Lbmg;

    iput-object v1, v0, Layl;->j:Lbmg;

    .line 653
    iget-object v0, p0, Lbtw;->e:Layl;

    iget-object v1, p0, Lbtw;->i:Lbop;

    iget v2, p0, Lbtw;->k:I

    add-int/lit8 v2, v2, -0x1

    .line 654
    invoke-interface {v1, v2}, Lbop;->b(I)Lbmd;

    move-result-object v1

    invoke-virtual {v1}, Lbmd;->c()Look;

    move-result-object v1

    iput-object v1, v0, Layl;->z:Look;

    .line 656
    :cond_1
    return-void
.end method

.method private f()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 996
    iget-object v0, p0, Lbtw;->t:Lbhl;

    if-eqz v0, :cond_0

    .line 997
    iget-object v0, p0, Lbtw;->o:Ljava/util/List;

    iget v1, p0, Lbtw;->k:I

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    iget-object v1, p0, Lbtw;->t:Lbhl;

    invoke-interface {v0, v1}, Lbhr;->a(Ljava/lang/Object;)V

    .line 998
    iput-object v5, p0, Lbtw;->t:Lbhl;

    .line 1001
    :cond_0
    iget-object v0, p0, Lbtw;->s:Lbhl;

    if-eqz v0, :cond_1

    .line 1002
    iget-object v0, p0, Lbtw;->s:Lbhl;

    iget-wide v0, v0, Lbhl;->a:J

    .line 1003
    iget-object v2, p0, Lbtw;->s:Lbhl;

    iget-boolean v2, v2, Lbhl;->b:Z

    .line 1006
    iget-object v3, p0, Lbtw;->r:Lbhr;

    iget-object v4, p0, Lbtw;->s:Lbhl;

    invoke-interface {v3, v4}, Lbhr;->a(Ljava/lang/Object;)V

    .line 1007
    iput-object v5, p0, Lbtw;->s:Lbhl;

    .line 1008
    if-eqz v2, :cond_1

    .line 1009
    invoke-direct {p0, v0, v1, v2}, Lbtw;->a(JZ)V

    .line 1012
    :cond_1
    return-void
.end method

.method private g()J
    .locals 4

    .prologue
    .line 1044
    iget-object v0, p0, Lbtw;->l:Lbmd;

    iget-object v0, v0, Lbmd;->f:Lbmp;

    iget-wide v0, v0, Lbmp;->c:J

    iget v2, p0, Lbtw;->k:I

    invoke-direct {p0, v2}, Lbtw;->b(I)J

    move-result-wide v2

    sub-long/2addr v0, v2

    return-wide v0
.end method


# virtual methods
.method public a(Lbgf;)Landroid/graphics/Bitmap;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v2, 0x1

    const/4 v4, -0x1

    const/4 v0, 0x0

    .line 450
    iget-object v1, p0, Lbtw;->q:Lbge;

    invoke-interface {v1}, Lbge;->c()V

    .line 453
    iget-object v1, p0, Lbtw;->e:Layl;

    iput v4, v1, Layl;->g:I

    .line 454
    iget-object v1, p0, Lbtw;->e:Layl;

    iput-wide v6, v1, Layl;->c:J

    .line 455
    iget-object v1, p0, Lbtw;->e:Layl;

    iput-boolean v2, v1, Layl;->w:Z

    .line 457
    iget-boolean v1, p0, Lbtw;->f:Z

    .line 458
    invoke-virtual {p0, v2}, Lbtw;->a(Z)V

    .line 459
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v2

    if-nez v2, :cond_1

    iget-wide v2, p0, Lbtw;->v:J

    .line 460
    invoke-virtual {p0, v2, v3}, Lbtw;->b(J)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lbtw;->r:Lbhr;

    if-nez v2, :cond_0

    .line 466
    :cond_1
    invoke-virtual {p0, v1}, Lbtw;->a(Z)V

    .line 469
    iget-object v1, p0, Lbtw;->e:Layl;

    iput v4, v1, Layl;->g:I

    .line 470
    iget-object v1, p0, Lbtw;->e:Layl;

    iput-wide v6, v1, Layl;->c:J

    .line 471
    iget-object v1, p0, Lbtw;->e:Layl;

    const/4 v2, 0x0

    iput-boolean v2, v1, Layl;->w:Z

    .line 473
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 474
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 477
    :cond_2
    iget-object v1, p0, Lbtw;->r:Lbhr;

    if-nez v1, :cond_3

    .line 491
    :goto_0
    return-object v0

    .line 483
    :cond_3
    :try_start_0
    iget-object v1, p0, Lbtw;->q:Lbge;

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Lbgf;->a(Lbgn;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 486
    iget-object v2, p0, Lbtw;->q:Lbge;

    invoke-interface {v2}, Lbge;->b()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 488
    goto :goto_0

    .line 489
    :catch_0
    move-exception v1

    .line 490
    sget-object v2, Lbtw;->a:Ljava/lang/String;

    const-string v3, "error getting last rendered frame"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 214
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbtw;->g:Z

    .line 215
    return-void
.end method

.method public a(Lbop;Lbhv;Ljava/util/List;Lbte;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbop;",
            "Lbhv;",
            "Ljava/util/List",
            "<",
            "Lbhr",
            "<",
            "Lbhl;",
            ">;>;",
            "Lbte;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 229
    iget-object v0, p0, Lbtw;->o:Ljava/util/List;

    const-string v1, "mVideoSources"

    const-string v2, "already initialized"

    invoke-static {v0, v1, v2}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 231
    iget-object v0, p0, Lbtw;->c:Lcgw;

    const-string v1, "initialize()"

    invoke-virtual {v0, v1}, Lcgw;->a(Ljava/lang/String;)V

    .line 233
    iput-object p2, p0, Lbtw;->h:Lbhv;

    .line 234
    const-string v0, "videoSources"

    invoke-static {p3, v0, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lbtw;->o:Ljava/util/List;

    .line 235
    const-string v0, "renderer"

    invoke-static {p4, v0, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbte;

    iput-object v0, p0, Lbtw;->p:Lbte;

    .line 236
    invoke-direct {p0, p1}, Lbtw;->a(Lbop;)V

    .line 237
    const/4 v0, -0x1

    iput v0, p0, Lbtw;->k:I

    .line 238
    return-void
.end method

.method public a(Livm;)V
    .locals 6

    .prologue
    .line 194
    const-string v0, "State %s. Last pulled frames primary: %d us  (storyboard offset %d us for clip index %d), secondary: %d us"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lbtw;->j:Lbtx;

    .line 196
    invoke-virtual {v3}, Lbtx;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lbtw;->v:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-wide v4, p0, Lbtw;->n:J

    .line 197
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lbtw;->k:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-wide v4, p0, Lbtw;->w:J

    .line 198
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 194
    invoke-interface {p1, v0, v1}, Livm;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 199
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 207
    iput-boolean p1, p0, Lbtw;->f:Z

    .line 208
    return-void
.end method

.method public a(J)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 295
    iget-object v0, p0, Lbtw;->o:Ljava/util/List;

    .line 296
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const-string v2, "mVideoSources.size()"

    iget-object v3, p0, Lbtw;->i:Lbop;

    invoke-interface {v3}, Lbop;->e()I

    move-result v3

    .line 295
    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v4}, Lcgp;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 297
    iget-object v0, p0, Lbtw;->h:Lbhv;

    if-nez v0, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v0, p1, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v2, "either timestampUs must be zero or seeker must be non-null"

    invoke-static {v0, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 304
    invoke-direct {p0}, Lbtw;->f()V

    .line 307
    iget-object v0, p0, Lbtw;->h:Lbhv;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbtw;->h:Lbhv;

    iget-object v2, p0, Lbtw;->o:Ljava/util/List;

    iget v3, p0, Lbtw;->k:I

    .line 308
    invoke-virtual {v0, p1, p2, v2, v3}, Lbhv;->a(JLjava/util/List;I)I

    move-result v0

    .line 309
    :goto_1
    iget-object v2, p0, Lbtw;->o:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ne v0, v2, :cond_3

    .line 316
    :goto_2
    return v1

    :cond_1
    move v0, v1

    .line 297
    goto :goto_0

    :cond_2
    move v0, v1

    .line 308
    goto :goto_1

    .line 315
    :cond_3
    invoke-direct {p0, v0}, Lbtw;->a(I)V

    .line 316
    invoke-direct {p0, p1, p2}, Lbtw;->c(J)Z

    move-result v1

    goto :goto_2
.end method

.method public a(Lbop;IJ)Z
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, 0x0

    .line 252
    const-string v0, "timeline"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 253
    iget-object v0, p0, Lbtw;->h:Lbhv;

    const-string v1, "mSeeker"

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 259
    invoke-direct {p0}, Lbtw;->f()V

    .line 262
    iget-object v0, p0, Lbtw;->h:Lbhv;

    invoke-virtual {v0, p1}, Lbhv;->a(Lbop;)Lbhm;

    move-result-object v0

    .line 263
    and-int/lit8 v1, p2, 0x2

    if-eqz v1, :cond_1

    .line 264
    iget-object v1, p0, Lbtw;->o:Ljava/util/List;

    invoke-interface {v1, v6, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 265
    iget v0, p0, Lbtw;->k:I

    add-int/lit8 v0, v0, 0x1

    move-object v1, p0

    .line 268
    :goto_0
    iput v0, v1, Lbtw;->k:I

    .line 272
    :cond_0
    iget-object v0, p0, Lbtw;->h:Lbhv;

    iget-object v4, p0, Lbtw;->o:Ljava/util/List;

    iget v5, p0, Lbtw;->k:I

    move-object v1, p1

    move-wide v2, p3

    invoke-virtual/range {v0 .. v5}, Lbhv;->a(Lbop;JLjava/util/List;I)I

    move-result v0

    .line 273
    iget-object v1, p0, Lbtw;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_3

    move v0, v6

    .line 281
    :goto_1
    return v0

    .line 266
    :cond_1
    and-int/lit8 v0, p2, 0x4

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lbtw;->o:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 268
    iget v0, p0, Lbtw;->k:I

    if-nez v0, :cond_2

    const/4 v0, -0x1

    move-object v1, p0

    goto :goto_0

    :cond_2
    iget v0, p0, Lbtw;->k:I

    add-int/lit8 v0, v0, -0x1

    move-object v1, p0

    goto :goto_0

    .line 279
    :cond_3
    invoke-direct {p0, p1}, Lbtw;->a(Lbop;)V

    .line 280
    invoke-direct {p0, v0}, Lbtw;->a(I)V

    .line 281
    invoke-direct {p0, p3, p4}, Lbtw;->c(J)Z

    move-result v0

    goto :goto_1
.end method

.method public b()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x0

    .line 321
    iget-object v0, p0, Lbtw;->c:Lcgw;

    const-string v1, "stop()"

    invoke-virtual {v0, v1}, Lcgw;->a(Ljava/lang/String;)V

    .line 323
    iget-object v0, p0, Lbtw;->s:Lbhl;

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lbtw;->r:Lbhr;

    iget-object v1, p0, Lbtw;->s:Lbhl;

    invoke-interface {v0, v1}, Lbhr;->a(Ljava/lang/Object;)V

    .line 325
    iput-object v2, p0, Lbtw;->s:Lbhl;

    .line 328
    :cond_0
    iget-object v0, p0, Lbtw;->t:Lbhl;

    if-eqz v0, :cond_1

    .line 329
    iget-object v0, p0, Lbtw;->o:Ljava/util/List;

    iget v1, p0, Lbtw;->k:I

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    iget-object v1, p0, Lbtw;->t:Lbhl;

    invoke-interface {v0, v1}, Lbhr;->a(Ljava/lang/Object;)V

    .line 330
    iput-object v2, p0, Lbtw;->t:Lbhl;

    .line 333
    :cond_1
    iget-object v0, p0, Lbtw;->y:Lbrz;

    invoke-virtual {v0}, Lbrz;->a()V

    .line 335
    iget-object v0, p0, Lbtw;->o:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 336
    iget-object v0, p0, Lbtw;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    .line 337
    invoke-interface {v0}, Lbhr;->a()V

    goto :goto_0

    .line 339
    :cond_2
    iput-object v2, p0, Lbtw;->o:Ljava/util/List;

    .line 341
    :cond_3
    iput-object v2, p0, Lbtw;->r:Lbhr;

    .line 343
    iget-object v0, p0, Lbtw;->c:Lcgw;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcgw;->a(Z)V

    .line 344
    iget-object v0, p0, Lbtw;->c:Lcgw;

    invoke-virtual {v0}, Lcgw;->b()V

    .line 346
    iput-object v2, p0, Lbtw;->i:Lbop;

    .line 348
    iget-object v0, p0, Lbtw;->h:Lbhv;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 349
    iput-object v2, p0, Lbtw;->h:Lbhv;

    .line 351
    iput-wide v4, p0, Lbtw;->v:J

    .line 352
    iput-wide v4, p0, Lbtw;->w:J

    .line 353
    return-void
.end method

.method public b(J)Z
    .locals 15

    .prologue
    .line 359
    iget-object v0, p0, Lbtw;->r:Lbhr;

    if-nez v0, :cond_0

    .line 360
    iget-object v0, p0, Lbtw;->c:Lcgw;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcgw;->a(Z)V

    .line 361
    const/4 v0, 0x0

    .line 434
    :goto_0
    return v0

    .line 364
    :cond_0
    iget-object v0, p0, Lbtw;->c:Lcgw;

    const-string v1, "step()"

    invoke-virtual {v0, v1}, Lcgw;->a(Ljava/lang/String;)V

    .line 365
    iget-object v0, p0, Lbtw;->s:Lbhl;

    if-nez v0, :cond_1

    iget-object v0, p0, Lbtw;->r:Lbhr;

    invoke-direct {p0, v0}, Lbtw;->a(Lbhr;)Lbhl;

    move-result-object v0

    iput-object v0, p0, Lbtw;->s:Lbhl;

    :cond_1
    iget-object v0, p0, Lbtw;->j:Lbtx;

    sget-object v1, Lbtx;->c:Lbtx;

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lbtw;->t:Lbhl;

    if-nez v0, :cond_2

    iget-object v0, p0, Lbtw;->o:Ljava/util/List;

    iget v1, p0, Lbtw;->k:I

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    invoke-direct {p0, v0}, Lbtw;->a(Lbhr;)Lbhl;

    move-result-object v0

    iput-object v0, p0, Lbtw;->t:Lbhl;

    :cond_2
    iget-object v0, p0, Lbtw;->j:Lbtx;

    sget-object v1, Lbtx;->b:Lbtx;

    if-ne v0, v1, :cond_3

    iget-object v0, p0, Lbtw;->o:Ljava/util/List;

    iget v1, p0, Lbtw;->k:I

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    invoke-direct {p0, v0}, Lbtw;->a(Lbhr;)Lbhl;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-wide v2, v1, Lbhl;->a:J

    iget-object v0, p0, Lbtw;->m:Lbmd;

    iget-object v0, v0, Lbmd;->f:Lbmp;

    iget-wide v4, v0, Lbmp;->b:J

    cmp-long v0, v2, v4

    if-ltz v0, :cond_5

    iput-object v1, p0, Lbtw;->t:Lbhl;

    sget-object v0, Lbtx;->a:Lbtx;

    iput-object v0, p0, Lbtw;->j:Lbtx;

    :cond_3
    :goto_1
    iget-object v0, p0, Lbtw;->c:Lcgw;

    const-string v1, "Advance sources"

    invoke-virtual {v0, v1}, Lcgw;->b(Ljava/lang/String;)V

    .line 366
    iget-object v0, p0, Lbtw;->s:Lbhl;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbtw;->j:Lbtx;

    sget-object v1, Lbtx;->c:Lbtx;

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lbtw;->t:Lbhl;

    if-nez v0, :cond_6

    .line 369
    :cond_4
    iget-object v0, p0, Lbtw;->c:Lcgw;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcgw;->a(Z)V

    .line 370
    const/4 v0, 0x0

    goto :goto_0

    .line 365
    :cond_5
    iget-object v0, p0, Lbtw;->o:Ljava/util/List;

    iget v2, p0, Lbtw;->k:I

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    invoke-interface {v0, v1}, Lbhr;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 373
    :cond_6
    iget-object v0, p0, Lbtw;->j:Lbtx;

    sget-object v1, Lbtx;->c:Lbtx;

    if-ne v0, v1, :cond_9

    .line 374
    iget-object v0, p0, Lbtw;->s:Lbhl;

    iget-wide v0, v0, Lbhl;->a:J

    invoke-direct {p0}, Lbtw;->g()J

    move-result-wide v2

    sub-long/2addr v0, v2

    iget-object v2, p0, Lbtw;->t:Lbhl;

    iget-wide v2, v2, Lbhl;->a:J

    iget-object v4, p0, Lbtw;->m:Lbmd;

    iget-object v4, v4, Lbmd;->f:Lbmp;

    iget-wide v4, v4, Lbmp;->b:J

    sub-long/2addr v2, v4

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xc350

    cmp-long v2, v0, v2

    if-lez v2, :cond_7

    iget-object v0, p0, Lbtw;->o:Ljava/util/List;

    iget v1, p0, Lbtw;->k:I

    add-int/lit8 v1, v1, 0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    iget-object v1, p0, Lbtw;->t:Lbhl;

    invoke-interface {v0, v1}, Lbhr;->a(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lbtw;->t:Lbhl;

    iget-object v0, p0, Lbtw;->c:Lcgw;

    const-string v1, "Release diverged frames"

    invoke-virtual {v0, v1}, Lcgw;->b(Ljava/lang/String;)V

    :goto_2
    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_9

    .line 375
    iget-object v0, p0, Lbtw;->c:Lcgw;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcgw;->a(Z)V

    .line 376
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 374
    :cond_7
    const-wide/32 v2, -0xc350

    cmp-long v0, v0, v2

    if-gez v0, :cond_8

    iget-object v0, p0, Lbtw;->s:Lbhl;

    iget-wide v0, v0, Lbhl;->a:J

    iget-object v2, p0, Lbtw;->s:Lbhl;

    iget-boolean v2, v2, Lbhl;->b:Z

    iget-object v3, p0, Lbtw;->r:Lbhr;

    iget-object v4, p0, Lbtw;->s:Lbhl;

    invoke-interface {v3, v4}, Lbhr;->a(Ljava/lang/Object;)V

    const/4 v3, 0x0

    iput-object v3, p0, Lbtw;->s:Lbhl;

    iget-object v3, p0, Lbtw;->c:Lcgw;

    const-string v4, "Release diverged frames"

    invoke-virtual {v3, v4}, Lcgw;->b(Ljava/lang/String;)V

    invoke-direct {p0, v0, v1, v2}, Lbtw;->a(JZ)V

    goto :goto_2

    :cond_8
    const/4 v0, 0x0

    goto :goto_3

    .line 381
    :cond_9
    iget-object v0, p0, Lbtw;->s:Lbhl;

    iget-wide v8, v0, Lbhl;->a:J

    .line 382
    iget-wide v0, p0, Lbtw;->n:J

    add-long v4, v8, v0

    .line 385
    iget-object v0, p0, Lbtw;->l:Lbmd;

    iget-object v0, v0, Lbmd;->f:Lbmp;

    iget-wide v0, v0, Lbmp;->c:J

    cmp-long v0, v8, v0

    if-gez v0, :cond_a

    const/4 v0, 0x1

    :goto_4
    const-string v1, "primary presentation timestamp must be less than the clip end point"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 389
    iget-boolean v0, p0, Lbtw;->f:Z

    if-nez v0, :cond_b

    cmp-long v0, p1, v4

    if-gez v0, :cond_b

    .line 390
    iget-object v0, p0, Lbtw;->c:Lcgw;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcgw;->a(Z)V

    .line 391
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 385
    :cond_a
    const/4 v0, 0x0

    goto :goto_4

    .line 395
    :cond_b
    iget-wide v0, p0, Lbtw;->u:J

    sub-long v0, v4, v0

    const-wide/32 v2, 0x186a0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_c

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_d

    :cond_c
    sget-object v0, Lbtw;->a:Ljava/lang/String;

    iget-wide v0, p0, Lbtw;->u:J

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    const-wide/16 v2, 0x3e8

    div-long v2, v4, v2

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x50

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Storyboard time discontinuity; from "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    :cond_d
    iput-wide v4, p0, Lbtw;->u:J

    .line 396
    iget-object v0, p0, Lbtw;->c:Lcgw;

    const-string v1, "Compute and check storyboard time"

    invoke-virtual {v0, v1}, Lcgw;->b(Ljava/lang/String;)V

    .line 398
    iget-object v0, p0, Lbtw;->s:Lbhl;

    iget-boolean v10, v0, Lbhl;->b:Z

    .line 399
    const/4 v1, 0x0

    .line 400
    sub-long v2, p1, v4

    .line 401
    iget-boolean v0, p0, Lbtw;->f:Z

    if-nez v0, :cond_11

    const-wide/32 v6, 0x493e0

    cmp-long v0, v2, v6

    if-lez v0, :cond_11

    .line 404
    sget-object v0, Lbtw;->a:Ljava/lang/String;

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long v4, p1, v4

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x5f

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Frame is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms late: requesting resync. Media time is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lbtw;->d:Lbtc;

    if-eqz v0, :cond_e

    iget-object v0, p0, Lbtw;->d:Lbtc;

    .line 405
    :cond_e
    iget-object v0, p0, Lbtw;->c:Lcgw;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcgw;->a(Z)V

    .line 406
    iget-object v0, p0, Lbtw;->b:Lbtt;

    invoke-interface {v0}, Lbtt;->b()V

    .line 431
    :cond_f
    :goto_5
    if-eqz v10, :cond_1a

    iget-object v0, p0, Lbtw;->y:Lbrz;

    iget-object v2, p0, Lbtw;->s:Lbhl;

    iget-object v3, p0, Lbtw;->r:Lbhr;

    iget v4, p0, Lbtw;->k:I

    iget-object v5, p0, Lbtw;->m:Lbmd;

    invoke-virtual {v0, v2, v3, v4, v5}, Lbrz;->a(Lbhl;Lbhr;ILbmd;)V

    :goto_6
    const/4 v0, 0x0

    iput-object v0, p0, Lbtw;->s:Lbhl;

    iget-object v0, p0, Lbtw;->j:Lbtx;

    sget-object v2, Lbtx;->c:Lbtx;

    if-ne v0, v2, :cond_10

    iget-object v0, p0, Lbtw;->o:Ljava/util/List;

    iget v2, p0, Lbtw;->k:I

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    iget-object v2, p0, Lbtw;->t:Lbhl;

    invoke-interface {v0, v2}, Lbhr;->a(Ljava/lang/Object;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lbtw;->t:Lbhl;

    .line 432
    :cond_10
    invoke-direct {p0, v8, v9, v10}, Lbtw;->a(JZ)V

    .line 433
    iget-object v0, p0, Lbtw;->c:Lcgw;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcgw;->a(Z)V

    move v0, v1

    .line 434
    goto/16 :goto_0

    .line 407
    :cond_11
    iget-boolean v0, p0, Lbtw;->f:Z

    if-nez v0, :cond_12

    const-wide/32 v6, 0xc350

    cmp-long v0, v2, v6

    if-lez v0, :cond_12

    .line 409
    sget-object v0, Lbtw;->a:Ljava/lang/String;

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long v4, p1, v4

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v6, 0x59

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Frame is "

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ms late: dropping it. Media time is "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ms"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, p0, Lbtw;->d:Lbtc;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lbtw;->d:Lbtc;

    goto :goto_5

    .line 410
    :cond_12
    iget-object v0, p0, Lbtw;->l:Lbmd;

    iget-object v0, v0, Lbmd;->f:Lbmp;

    iget-wide v6, v0, Lbmp;->b:J

    cmp-long v0, v8, v6

    if-gez v0, :cond_13

    .line 414
    if-eqz v10, :cond_f

    .line 415
    sget-object v0, Lbtw;->a:Ljava/lang/String;

    goto/16 :goto_5

    .line 417
    :cond_13
    iget-object v0, p0, Lbtw;->j:Lbtx;

    sget-object v6, Lbtx;->c:Lbtx;

    if-eq v0, v6, :cond_14

    iget v0, p0, Lbtw;->k:I

    if-lez v0, :cond_14

    iget-object v0, p0, Lbtw;->l:Lbmd;

    iget-object v0, v0, Lbmd;->f:Lbmp;

    iget-wide v6, v0, Lbmp;->b:J

    iget-object v0, p0, Lbtw;->i:Lbop;

    iget v11, p0, Lbtw;->k:I

    add-int/lit8 v11, v11, -0x1

    .line 419
    invoke-interface {v0, v11}, Lbop;->d(I)Lcac;

    move-result-object v0

    invoke-interface {v0}, Lcac;->b()J

    move-result-wide v12

    add-long/2addr v6, v12

    cmp-long v0, v8, v6

    if-ltz v0, :cond_f

    .line 426
    :cond_14
    iget-boolean v0, p0, Lbtw;->f:Z

    if-nez v0, :cond_15

    const-wide/32 v6, 0xc350

    cmp-long v0, v2, v6

    if-gez v0, :cond_f

    .line 427
    :cond_15
    iget-boolean v0, p0, Lbtw;->f:Z

    if-nez v0, :cond_16

    const-wide/32 v0, 0xc350

    cmp-long v0, v2, v0

    if-lez v0, :cond_16

    sget-object v0, Lbtw;->a:Ljava/lang/String;

    const-wide/16 v0, 0x3e8

    div-long v0, v2, v0

    const-wide/16 v2, 0x3e8

    div-long v2, p1, v2

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x4c

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Frame is "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms late. Media time is "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ms"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_16
    iget-object v0, p0, Lbtw;->d:Lbtc;

    if-eqz v0, :cond_17

    iget-object v0, p0, Lbtw;->d:Lbtc;

    .line 428
    :cond_17
    iget-object v0, p0, Lbtw;->e:Layl;

    iput-wide v4, v0, Layl;->b:J

    iget-object v0, p0, Lbtw;->i:Lbop;

    invoke-interface {v0}, Lbop;->i()Layj;

    move-result-object v0

    iget-object v1, p0, Lbtw;->e:Layl;

    invoke-interface {v0, v1}, Layj;->s(Layl;)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v4, v2

    if-nez v2, :cond_19

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_19

    iget-object v2, p0, Lbtw;->e:Layl;

    iput-wide v0, v2, Layl;->a:J

    iget-object v2, p0, Lbtw;->e:Layl;

    iget-object v3, p0, Lbtw;->l:Lbmd;

    iget-object v3, v3, Lbmd;->f:Lbmp;

    iget-wide v6, v3, Lbmp;->b:J

    sub-long/2addr v0, v6

    iput-wide v0, v2, Layl;->d:J

    iget-object v0, p0, Lbtw;->e:Layl;

    :cond_18
    const/4 v1, 0x0

    move v14, v1

    move-object v1, v0

    move v0, v14

    :goto_7
    iput v0, v1, Layl;->m:F

    iget-object v0, p0, Lbtw;->i:Lbop;

    invoke-interface {v0}, Lbop;->i()Layj;

    move-result-object v0

    iget-object v1, p0, Lbtw;->e:Layl;

    invoke-interface {v0, v1}, Layj;->z(Layl;)V

    invoke-direct {p0}, Lbtw;->d()Lbth;

    move-result-object v2

    iget-object v1, p0, Lbtw;->p:Lbte;

    iget-object v3, p0, Lbtw;->q:Lbge;

    iget-object v0, p0, Lbtw;->i:Lbop;

    invoke-interface {v0}, Lbop;->i()Layj;

    move-result-object v6

    iget-object v7, p0, Lbtw;->e:Layl;

    invoke-interface/range {v1 .. v7}, Lbte;->a(Lbth;Lbge;JLayj;Layl;)V

    iget-object v0, p0, Lbtw;->c:Lcgw;

    const-string v1, "Render frames"

    invoke-virtual {v0, v1}, Lcgw;->b(Ljava/lang/String;)V

    .line 429
    const/4 v1, 0x1

    goto/16 :goto_5

    .line 428
    :cond_19
    iget-object v0, p0, Lbtw;->e:Layl;

    iput-wide v4, v0, Layl;->a:J

    iget-object v0, p0, Lbtw;->e:Layl;

    iget-object v1, p0, Lbtw;->s:Lbhl;

    iget-wide v2, v1, Lbhl;->a:J

    iget-object v1, p0, Lbtw;->l:Lbmd;

    iget-object v1, v1, Lbmd;->f:Lbmp;

    iget-wide v6, v1, Lbmp;->b:J

    sub-long/2addr v2, v6

    iput-wide v2, v0, Layl;->d:J

    iget-object v0, p0, Lbtw;->e:Layl;

    iget-object v1, p0, Lbtw;->j:Lbtx;

    sget-object v2, Lbtx;->c:Lbtx;

    if-ne v1, v2, :cond_18

    iget-object v1, p0, Lbtw;->i:Lbop;

    iget v2, p0, Lbtw;->k:I

    invoke-interface {v1, v2}, Lbop;->d(I)Lcac;

    move-result-object v1

    if-eqz v1, :cond_18

    invoke-direct {p0}, Lbtw;->g()J

    move-result-wide v2

    sub-long v2, v8, v2

    invoke-interface {v1, v2, v3}, Lcac;->a(J)F

    move-result v1

    move v14, v1

    move-object v1, v0

    move v0, v14

    goto :goto_7

    .line 431
    :cond_1a
    iget-object v0, p0, Lbtw;->r:Lbhr;

    iget-object v2, p0, Lbtw;->s:Lbhl;

    invoke-interface {v0, v2}, Lbhr;->a(Ljava/lang/Object;)V

    goto/16 :goto_6
.end method

.method public c()J
    .locals 10

    .prologue
    const-wide/16 v4, 0x0

    .line 1020
    iget-object v0, p0, Lbtw;->r:Lbhr;

    if-nez v0, :cond_0

    .line 1022
    const-wide/16 v0, -0x1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    iget-wide v4, p0, Lbtw;->x:J

    sub-long/2addr v2, v4

    mul-long/2addr v0, v2

    .line 1039
    :goto_0
    return-wide v0

    .line 1027
    :cond_0
    iget v0, p0, Lbtw;->k:I

    move-wide v2, v4

    :goto_1
    iget-object v1, p0, Lbtw;->i:Lbop;

    invoke-interface {v1}, Lbop;->e()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 1029
    iget-object v1, p0, Lbtw;->i:Lbop;

    invoke-interface {v1, v0}, Lbop;->b(I)Lbmd;

    move-result-object v1

    iget-object v1, v1, Lbmd;->f:Lbmp;

    iget-wide v6, v1, Lbmp;->c:J

    iget-object v1, p0, Lbtw;->i:Lbop;

    .line 1030
    invoke-interface {v1, v0}, Lbop;->b(I)Lbmd;

    move-result-object v1

    iget-object v1, v1, Lbmd;->f:Lbmp;

    iget-wide v8, v1, Lbmp;->b:J

    sub-long v8, v6, v8

    iget-object v1, p0, Lbtw;->i:Lbop;

    .line 1031
    invoke-interface {v1}, Lbop;->e()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    iget-object v1, p0, Lbtw;->i:Lbop;

    .line 1032
    invoke-interface {v1, v0}, Lbop;->d(I)Lcac;

    move-result-object v1

    invoke-interface {v1}, Lcac;->b()J

    move-result-wide v6

    :goto_2
    sub-long v6, v8, v6

    add-long/2addr v2, v6

    .line 1028
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move-wide v6, v4

    .line 1032
    goto :goto_2

    .line 1036
    :cond_2
    iget-wide v0, p0, Lbtw;->v:J

    iget-object v4, p0, Lbtw;->l:Lbmd;

    iget-object v4, v4, Lbmd;->f:Lbmp;

    iget-wide v4, v4, Lbmp;->b:J

    sub-long/2addr v0, v4

    sub-long v0, v2, v0

    .line 1039
    goto :goto_0
.end method
