.class public Latw;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/Object;

.field private volatile c:Z

.field private volatile d:Lcdx;

.field private volatile e:Z

.field private volatile f:J

.field private final g:Latr;

.field private final h:Ljava/lang/Runnable;

.field private final i:Ljava/lang/Runnable;

.field private final j:Landroid/content/Context;

.field private final k:Lbjp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjp",
            "<",
            "Lbhk;",
            ">;"
        }
    .end annotation
.end field

.field private final l:Lbjp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjp",
            "<",
            "Lbhl;",
            ">;"
        }
    .end annotation
.end field

.field private final m:Lbgf;

.field private final n:Lbte;

.field private final o:Lasy;

.field private final p:Lald;

.field private final q:Lamy;

.field private final r:Lchl;

.field private final s:Ljava/util/concurrent/Executor;

.field private final t:Ljava/util/concurrent/Executor;

.field private final u:Lbjf;

.field private final v:Lbig;

.field private final w:Latx;

.field private final x:Z

.field private final y:Ljava/lang/Object;

.field private z:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    const-class v0, Latw;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Latw;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lbjp;Lbjp;Lbgf;Lbte;Lasy;Lald;Lamy;Lchl;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lbjf;Lbig;Latx;Z)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lbjp",
            "<",
            "Lbhk;",
            ">;",
            "Lbjp",
            "<",
            "Lbhl;",
            ">;",
            "Lbgf;",
            "Lbte;",
            "Lasy;",
            "Lald;",
            "Lamy;",
            "Lchl;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Executor;",
            "Lbjf;",
            "Lbig;",
            "Latx;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Latw;->b:Ljava/lang/Object;

    .line 134
    const/4 v1, 0x0

    iput-boolean v1, p0, Latw;->c:Z

    .line 138
    const/4 v1, 0x0

    iput-boolean v1, p0, Latw;->e:Z

    .line 141
    new-instance v1, Laud;

    invoke-direct {v1, p0}, Laud;-><init>(Latw;)V

    iput-object v1, p0, Latw;->g:Latr;

    .line 143
    new-instance v1, Laty;

    invoke-direct {v1, p0}, Laty;-><init>(Latw;)V

    iput-object v1, p0, Latw;->h:Ljava/lang/Runnable;

    .line 144
    new-instance v1, Laub;

    invoke-direct {v1, p0}, Laub;-><init>(Latw;)V

    iput-object v1, p0, Latw;->i:Ljava/lang/Runnable;

    .line 165
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Latw;->y:Ljava/lang/Object;

    .line 191
    const-string v1, "context"

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    iput-object v1, p0, Latw;->j:Landroid/content/Context;

    .line 192
    const-string v1, "audioDecoderPool"

    const/4 v2, 0x0

    invoke-static {p2, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbjp;

    iput-object v1, p0, Latw;->k:Lbjp;

    .line 193
    const-string v1, "videoDecoderPool"

    const/4 v2, 0x0

    invoke-static {p3, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbjp;

    iput-object v1, p0, Latw;->l:Lbjp;

    .line 194
    const-string v1, "renderContext"

    const/4 v2, 0x0

    invoke-static {p4, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbgf;

    iput-object v1, p0, Latw;->m:Lbgf;

    .line 195
    const-string v1, "renderer"

    const/4 v2, 0x0

    invoke-static {p5, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbte;

    iput-object v1, p0, Latw;->n:Lbte;

    .line 196
    const-string v1, "stateTracker"

    const/4 v2, 0x0

    invoke-static {p6, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lasy;

    iput-object v1, p0, Latw;->o:Lasy;

    .line 197
    const-string v1, "appInfo"

    const/4 v2, 0x0

    invoke-static {p7, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lald;

    iput-object v1, p0, Latw;->p:Lald;

    .line 198
    const-string v1, "deviceInfo"

    const/4 v2, 0x0

    invoke-static {p8, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lamy;

    iput-object v1, p0, Latw;->q:Lamy;

    .line 199
    const-string v1, "threadFactory"

    const/4 v2, 0x0

    invoke-static {p9, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lchl;

    iput-object v1, p0, Latw;->r:Lchl;

    .line 200
    const-string v1, "decoderExecutor"

    const/4 v2, 0x0

    invoke-static {p10, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iput-object v1, p0, Latw;->s:Ljava/util/concurrent/Executor;

    .line 201
    const-string v1, "mainThreadExecutor"

    const/4 v2, 0x0

    invoke-static {p11, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/concurrent/Executor;

    iput-object v1, p0, Latw;->t:Ljava/util/concurrent/Executor;

    .line 202
    const-string v1, "bitmapFactory"

    const/4 v2, 0x0

    invoke-static {p12, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbjf;

    iput-object v1, p0, Latw;->u:Lbjf;

    .line 203
    const-string v1, "mediaExtractorFactory"

    const/4 v2, 0x0

    move-object/from16 v0, p13

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbig;

    iput-object v1, p0, Latw;->v:Lbig;

    .line 205
    const-string v1, "callbacks"

    const/4 v2, 0x0

    move-object/from16 v0, p14

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Latx;

    iput-object v1, p0, Latw;->w:Latx;

    .line 206
    move/from16 v0, p15

    iput-boolean v0, p0, Latw;->x:Z

    .line 207
    return-void
.end method

.method static synthetic a(Latw;J)J
    .locals 1

    .prologue
    .line 58
    iput-wide p1, p0, Latw;->f:J

    return-wide p1
.end method

.method private a(Laue;)J
    .locals 31

    .prologue
    .line 308
    const/16 v17, 0x0

    .line 310
    const/4 v11, 0x0

    .line 311
    const/16 v16, 0x0

    .line 312
    const/4 v10, 0x0

    .line 313
    const/4 v15, 0x0

    .line 315
    const/4 v14, 0x0

    .line 316
    const/4 v13, 0x0

    .line 317
    const/4 v12, 0x0

    .line 323
    move-object/from16 v0, p1

    iget-object v4, v0, Laue;->a:Lboi;

    invoke-static {v4}, Latw;->a(Lboi;)Z

    move-result v30

    .line 328
    :try_start_0
    invoke-static/range {v30 .. v30}, Lath;->a(Z)I

    move-result v7

    .line 330
    move-object/from16 v0, p1

    iget-object v4, v0, Laue;->a:Lboi;

    invoke-virtual {v4}, Lboi;->n()J

    move-result-wide v8

    .line 331
    if-eqz v30, :cond_0

    .line 332
    move-object/from16 v0, p1

    iget-object v4, v0, Laue;->a:Lboi;

    invoke-virtual {v4}, Lboi;->c()J

    move-result-wide v4

    add-long/2addr v8, v4

    .line 334
    :cond_0
    new-instance v4, Latj;

    move-object/from16 v0, p0

    iget-object v5, v0, Latw;->t:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v6, v0, Latw;->w:Latx;

    invoke-direct/range {v4 .. v9}, Latj;-><init>(Ljava/util/concurrent/Executor;Latl;IJ)V

    .line 340
    move-object/from16 v0, p1

    iget-object v5, v0, Laue;->d:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->mkdirs()Z

    .line 341
    new-instance v28, Latm;

    new-instance v5, Landroid/media/MediaMuxer;

    move-object/from16 v0, p1

    iget-object v6, v0, Laue;->d:Ljava/io/File;

    .line 343
    invoke-virtual {v6}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    .line 344
    invoke-static {}, Lath;->a()I

    move-result v8

    invoke-direct {v5, v6, v8}, Landroid/media/MediaMuxer;-><init>(Ljava/lang/String;I)V

    move-object/from16 v0, v28

    invoke-direct {v0, v5, v7}, Latm;-><init>(Landroid/media/MediaMuxer;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 347
    :try_start_1
    move-object/from16 v0, p1

    iget v5, v0, Laue;->c:I

    if-eqz v5, :cond_1

    move-object/from16 v0, p1

    iget v5, v0, Laue;->c:I

    const/16 v6, 0x5a

    if-ne v5, v6, :cond_4

    :cond_1
    const/4 v5, 0x1

    :goto_0
    const-string v6, "params.playbackRotation"

    invoke-static {v5, v6}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 349
    move-object/from16 v0, p1

    iget v5, v0, Laue;->c:I

    move-object/from16 v0, v28

    invoke-virtual {v0, v5}, Latm;->a(I)V

    .line 351
    const/4 v5, 0x0

    .line 352
    move-object/from16 v0, p1

    iget-object v6, v0, Laue;->f:Ljava/io/File;

    if-eqz v6, :cond_d

    move-object/from16 v0, p1

    iget-object v6, v0, Laue;->f:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->exists()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v6

    if-eqz v6, :cond_d

    .line 354
    :try_start_2
    move-object/from16 v0, p1

    iget-object v6, v0, Laue;->f:Ljava/io/File;

    move-object/from16 v0, p0

    move-object/from16 v1, v28

    move/from16 v2, v30

    invoke-direct {v0, v6, v1, v4, v2}, Latw;->a(Ljava/io/File;Latm;Lats;Z)Lauf;
    :try_end_2
    .catch Lauc; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v5

    .line 369
    :try_start_3
    move-object/from16 v0, p1

    iget-object v6, v0, Laue;->f:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    move-object/from16 v29, v5

    .line 374
    :goto_1
    move-object/from16 v0, p1

    iget-object v5, v0, Laue;->b:Latu;

    move-object/from16 v0, p0

    iget-boolean v6, v0, Latw;->x:Z

    .line 375
    invoke-static {v5, v6}, Lath;->a(Latu;Z)Landroid/media/MediaFormat;

    move-result-object v5

    .line 378
    if-eqz v29, :cond_c

    move-object/from16 v0, v29

    iget-object v6, v0, Lauf;->d:Landroid/media/MediaFormat;

    const-string v7, "mediaFormatA"

    const/4 v8, 0x0

    invoke-static {v5, v7, v8}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    const-string v7, "mediaFormatB"

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    const-string v7, "width"

    invoke-virtual {v5, v7}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v7

    const-string v8, "width"

    invoke-virtual {v6, v8}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v8

    if-ne v7, v8, :cond_5

    const-string v7, "height"

    invoke-virtual {v5, v7}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v7

    const-string v8, "height"

    invoke-virtual {v6, v8}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v6

    if-ne v7, v6, :cond_5

    const/4 v6, 0x1

    :goto_2
    if-nez v6, :cond_c

    .line 380
    sget-object v5, Latw;->a:Ljava/lang/String;

    .line 383
    move-object/from16 v0, v29

    iget-object v5, v0, Lauf;->d:Landroid/media/MediaFormat;

    .line 384
    invoke-static {v5}, Lath;->a(Landroid/media/MediaFormat;)Landroid/media/MediaFormat;

    move-result-object v5

    move-object v8, v5

    .line 386
    :goto_3
    invoke-static {}, Lbjt;->d()Lbjr;

    move-result-object v5

    invoke-interface {v5, v8}, Lbjr;->a(Landroid/media/MediaFormat;)Lbjs;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v27

    .line 387
    :try_start_4
    move-object/from16 v0, v27

    invoke-interface {v0, v8}, Lbjs;->b(Landroid/media/MediaFormat;)V

    .line 388
    invoke-interface/range {v27 .. v27}, Lbjs;->d()Landroid/view/Surface;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    move-result-object v6

    .line 392
    :try_start_5
    move-object/from16 v0, p0

    iget-object v5, v0, Latw;->n:Lbte;

    invoke-interface {v5}, Lbte;->a()V

    .line 393
    move-object/from16 v0, p0

    iget-object v5, v0, Latw;->l:Lbjp;

    invoke-virtual {v5}, Lbjp;->a()V

    .line 395
    move-object/from16 v0, p0

    iget-object v5, v0, Latw;->m:Lbgf;

    const-string v7, "width"

    .line 396
    invoke-virtual {v8, v7}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v7

    const-string v9, "height"

    .line 397
    invoke-virtual {v8, v9}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v8

    move-object/from16 v0, p1

    iget-object v9, v0, Laue;->a:Lboi;

    .line 398
    invoke-virtual {v9}, Lboi;->o()I

    move-result v9

    move-object/from16 v0, p1

    iget-object v10, v0, Laue;->a:Lboi;

    invoke-virtual {v10}, Lboi;->p()I

    move-result v10

    move-object/from16 v0, p1

    iget v11, v0, Laue;->c:I

    const/16 v17, 0x5a

    move/from16 v0, v17

    if-ne v11, v0, :cond_6

    const/4 v11, 0x1

    .line 395
    :goto_4
    invoke-virtual/range {v5 .. v11}, Lbgf;->a(Ljava/lang/Object;IIIIZ)Lbge;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_5

    move-result-object v26

    .line 400
    :try_start_6
    move-object/from16 v0, p0

    iget-object v5, v0, Latw;->n:Lbte;

    invoke-interface {v5}, Lbte;->b()V

    .line 402
    invoke-interface/range {v27 .. v27}, Lbjs;->e()V

    .line 404
    new-instance v5, Latq;

    move-object/from16 v0, p0

    iget-object v7, v0, Latw;->g:Latr;

    move-object/from16 v0, p0

    iget-object v8, v0, Latw;->r:Lchl;

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-direct {v5, v0, v1, v7, v8}, Latq;-><init>(Latm;Lbjs;Latr;Lchl;)V

    .line 410
    invoke-virtual {v5, v4}, Latq;->a(Lats;)Latq;

    move-result-object v5

    .line 411
    if-eqz v29, :cond_2

    .line 412
    move-object/from16 v0, v29

    iget v7, v0, Lauf;->c:I

    move-object/from16 v0, v29

    iget-object v8, v0, Lauf;->d:Landroid/media/MediaFormat;

    invoke-virtual {v5, v7, v8}, Latq;->a(ILandroid/media/MediaFormat;)Latq;

    .line 414
    move-object/from16 v0, v29

    iget-wide v8, v0, Lauf;->e:J

    invoke-virtual {v5, v8, v9}, Latq;->a(J)Latq;

    .line 417
    :cond_2
    invoke-virtual {v5}, Latq;->a()Lato;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_6

    move-result-object v25

    .line 419
    :try_start_7
    invoke-virtual/range {v25 .. v25}, Lato;->a()V

    .line 421
    if-eqz v30, :cond_7

    .line 422
    move-object/from16 v0, p1

    iget-boolean v5, v0, Laue;->h:Z

    .line 423
    invoke-static {v5}, Lath;->b(Z)Landroid/media/MediaFormat;

    move-result-object v5

    .line 424
    move-object/from16 v0, p1

    iget-object v7, v0, Laue;->g:Ljava/lang/String;

    .line 425
    invoke-static {v7}, Lbjt;->a(Ljava/lang/String;)Lbjr;

    move-result-object v7

    .line 426
    invoke-interface {v7, v5}, Lbjr;->a(Landroid/media/MediaFormat;)Lbjs;
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_7

    move-result-object v8

    .line 427
    :try_start_8
    invoke-interface {v8, v5}, Lbjs;->b(Landroid/media/MediaFormat;)V

    .line 428
    invoke-interface {v8}, Lbjs;->e()V

    .line 429
    new-instance v7, Latg;

    invoke-direct {v7, v8}, Latg;-><init>(Lbjs;)V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_8

    .line 430
    const/4 v5, 0x2

    const v9, 0xac44

    const/4 v10, 0x2

    :try_start_9
    invoke-interface {v7, v5, v9, v10}, Lbrw;->a(III)V

    .line 433
    new-instance v5, Latq;

    move-object/from16 v0, p0

    iget-object v9, v0, Latw;->g:Latr;

    move-object/from16 v0, p0

    iget-object v10, v0, Latw;->r:Lchl;

    move-object/from16 v0, v28

    invoke-direct {v5, v0, v8, v9, v10}, Latq;-><init>(Latm;Lbjs;Latr;Lchl;)V

    .line 439
    invoke-virtual {v5, v4}, Latq;->a(Lats;)Latq;

    move-result-object v4

    .line 440
    if-eqz v29, :cond_3

    .line 441
    move-object/from16 v0, v29

    iget v5, v0, Lauf;->a:I

    move-object/from16 v0, v29

    iget-object v9, v0, Lauf;->b:Landroid/media/MediaFormat;

    invoke-virtual {v4, v5, v9}, Latq;->a(ILandroid/media/MediaFormat;)Latq;

    .line 443
    move-object/from16 v0, v29

    iget-wide v10, v0, Lauf;->e:J

    invoke-virtual {v4, v10, v11}, Latq;->a(J)Latq;

    .line 446
    :cond_3
    invoke-virtual {v4}, Latq;->a()Lato;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_9

    move-result-object v5

    .line 448
    :try_start_a
    invoke-virtual {v5}, Lato;->a()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_a

    move-object/from16 v22, v5

    move-object/from16 v23, v7

    move-object/from16 v24, v8

    .line 453
    :goto_5
    :try_start_b
    new-instance v7, Lbsu;

    move-object/from16 v0, p0

    iget-object v8, v0, Latw;->j:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v9, v0, Latw;->v:Lbig;

    move-object/from16 v0, p0

    iget-object v10, v0, Latw;->k:Lbjp;

    move-object/from16 v0, p0

    iget-object v11, v0, Latw;->l:Lbjp;

    move-object/from16 v0, p0

    iget-object v12, v0, Latw;->m:Lbgf;

    move-object/from16 v0, p0

    iget-object v13, v0, Latw;->n:Lbte;

    move-object/from16 v0, p0

    iget-object v14, v0, Latw;->o:Lasy;

    move-object/from16 v0, p0

    iget-object v15, v0, Latw;->u:Lbjf;

    move-object/from16 v0, p1

    iget-object v0, v0, Laue;->a:Lboi;

    move-object/from16 v16, v0

    const/16 v17, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Latw;->s:Ljava/util/concurrent/Executor;

    move-object/from16 v18, v0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x0

    invoke-direct/range {v7 .. v21}, Lbsu;-><init>(Landroid/content/Context;Lbig;Lbjp;Lbjp;Lbgf;Lbte;Lasy;Lbjf;Lboi;ZLjava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lbtq;Lbtc;)V

    .line 470
    if-nez v29, :cond_8

    .line 474
    invoke-static {}, Lbss;->a()Lbss;

    move-result-object v4

    .line 483
    :goto_6
    move-object/from16 v0, v23

    move-object/from16 v1, v26

    invoke-virtual {v7, v4, v0, v1}, Lbsu;->a(Lbss;Lbrw;Lbge;)V

    .line 484
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 485
    new-instance v4, Ljava/lang/InterruptedException;

    invoke-direct {v4}, Ljava/lang/InterruptedException;-><init>()V

    throw v4
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 497
    :catchall_0
    move-exception v4

    move-object/from16 v5, v22

    move-object/from16 v7, v24

    move-object/from16 v8, v25

    move-object v9, v6

    move-object/from16 v10, v26

    move-object/from16 v11, v27

    move-object/from16 v12, v28

    move-object/from16 v6, v23

    :goto_7
    invoke-virtual {v12}, Latm;->b()V

    .line 498
    invoke-static {v6}, Lcgl;->a(Lcgk;)V

    .line 499
    invoke-static {v5}, Lcgr;->a(Lcgq;)V

    .line 500
    invoke-static {v7}, Lcgl;->a(Lcgk;)V

    .line 501
    invoke-static {v8}, Lcgr;->a(Lcgq;)V

    .line 504
    move-object/from16 v0, p0

    iget-object v5, v0, Latw;->l:Lbjp;

    invoke-static {v5}, Lcgl;->a(Lcgk;)V

    .line 505
    move-object/from16 v0, p0

    iget-object v5, v0, Latw;->n:Lbte;

    invoke-static {v5}, Lcgl;->a(Lcgk;)V

    .line 509
    :try_start_c
    move-object/from16 v0, p0

    iget-object v5, v0, Latw;->m:Lbgf;

    invoke-virtual {v5}, Lbgf;->c()V
    :try_end_c
    .catch Ljava/lang/RuntimeException; {:try_start_c .. :try_end_c} :catch_3

    .line 514
    :goto_8
    invoke-static {v11}, Lcgl;->a(Lcgk;)V

    .line 515
    invoke-static {v10}, Lcgl;->a(Lcgk;)V

    .line 516
    invoke-static {v9}, Lcgl;->a(Landroid/view/Surface;)V

    .line 517
    invoke-static {v12}, Lcgl;->a(Lcgk;)V

    .line 524
    move-object/from16 v0, p0

    iget-object v5, v0, Latw;->n:Lbte;

    invoke-interface {v5}, Lbte;->b()V

    throw v4

    .line 347
    :cond_4
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 369
    :catch_0
    move-exception v6

    :try_start_d
    move-object/from16 v0, p1

    iget-object v6, v0, Laue;->f:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    move-object/from16 v29, v5

    .line 370
    goto/16 :goto_1

    .line 369
    :catch_1
    move-exception v6

    move-object/from16 v0, p1

    iget-object v6, v0, Laue;->f:Ljava/io/File;

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    move-object/from16 v29, v5

    .line 370
    goto/16 :goto_1

    .line 369
    :catchall_1
    move-exception v4

    move-object/from16 v0, p1

    iget-object v5, v0, Laue;->f:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    throw v4
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_2

    .line 497
    :catchall_2
    move-exception v4

    move-object v5, v12

    move-object v6, v13

    move-object v7, v14

    move-object v8, v15

    move-object v9, v10

    move-object/from16 v10, v16

    move-object/from16 v12, v28

    goto :goto_7

    .line 378
    :cond_5
    const/4 v6, 0x0

    goto/16 :goto_2

    .line 398
    :cond_6
    const/4 v11, 0x0

    goto/16 :goto_4

    .line 450
    :cond_7
    :try_start_e
    new-instance v23, Latf;

    invoke-direct/range {v23 .. v23}, Latf;-><init>()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_7

    move-object/from16 v22, v12

    move-object/from16 v24, v14

    goto/16 :goto_5

    .line 480
    :cond_8
    :try_start_f
    move-object/from16 v0, v29

    iget-wide v4, v0, Lauf;->e:J

    invoke-static {v4, v5}, Lbss;->a(J)Lbss;

    move-result-object v4

    goto/16 :goto_6

    .line 488
    :cond_9
    invoke-interface/range {v27 .. v27}, Lbjs;->j()V

    .line 491
    invoke-virtual/range {v25 .. v25}, Lato;->c()V

    .line 492
    if-eqz v30, :cond_a

    .line 493
    invoke-virtual/range {v22 .. v22}, Lato;->c()V
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 495
    :cond_a
    if-nez v29, :cond_b

    const-wide/16 v4, 0x0

    .line 497
    :goto_9
    invoke-virtual/range {v28 .. v28}, Latm;->b()V

    .line 498
    invoke-static/range {v23 .. v23}, Lcgl;->a(Lcgk;)V

    .line 499
    invoke-static/range {v22 .. v22}, Lcgr;->a(Lcgq;)V

    .line 500
    invoke-static/range {v24 .. v24}, Lcgl;->a(Lcgk;)V

    .line 501
    invoke-static/range {v25 .. v25}, Lcgr;->a(Lcgq;)V

    .line 504
    move-object/from16 v0, p0

    iget-object v7, v0, Latw;->l:Lbjp;

    invoke-static {v7}, Lcgl;->a(Lcgk;)V

    .line 505
    move-object/from16 v0, p0

    iget-object v7, v0, Latw;->n:Lbte;

    invoke-static {v7}, Lcgl;->a(Lcgk;)V

    .line 509
    :try_start_10
    move-object/from16 v0, p0

    iget-object v7, v0, Latw;->m:Lbgf;

    invoke-virtual {v7}, Lbgf;->c()V
    :try_end_10
    .catch Ljava/lang/RuntimeException; {:try_start_10 .. :try_end_10} :catch_2

    .line 514
    :goto_a
    invoke-static/range {v27 .. v27}, Lcgl;->a(Lcgk;)V

    .line 515
    invoke-static/range {v26 .. v26}, Lcgl;->a(Lcgk;)V

    .line 516
    invoke-static {v6}, Lcgl;->a(Landroid/view/Surface;)V

    .line 517
    invoke-static/range {v28 .. v28}, Lcgl;->a(Lcgk;)V

    .line 524
    move-object/from16 v0, p0

    iget-object v6, v0, Latw;->n:Lbte;

    invoke-interface {v6}, Lbte;->b()V

    return-wide v4

    .line 495
    :cond_b
    :try_start_11
    move-object/from16 v0, v29

    iget-wide v4, v0, Lauf;->e:J
    :try_end_11
    .catchall {:try_start_11 .. :try_end_11} :catchall_0

    goto :goto_9

    .line 511
    :catch_2
    move-exception v7

    sget-object v7, Latw;->a:Ljava/lang/String;

    goto :goto_a

    :catch_3
    move-exception v5

    sget-object v5, Latw;->a:Ljava/lang/String;

    goto/16 :goto_8

    .line 497
    :catchall_3
    move-exception v4

    move-object v5, v12

    move-object v6, v13

    move-object v7, v14

    move-object v8, v15

    move-object v9, v10

    move-object/from16 v10, v16

    move-object/from16 v12, v17

    goto/16 :goto_7

    :catchall_4
    move-exception v4

    move-object v5, v12

    move-object v6, v13

    move-object v7, v14

    move-object v8, v15

    move-object v9, v10

    move-object/from16 v11, v27

    move-object/from16 v10, v16

    move-object/from16 v12, v28

    goto/16 :goto_7

    :catchall_5
    move-exception v4

    move-object v5, v12

    move-object v7, v14

    move-object v8, v15

    move-object v9, v6

    move-object/from16 v10, v16

    move-object/from16 v11, v27

    move-object v6, v13

    move-object/from16 v12, v28

    goto/16 :goto_7

    :catchall_6
    move-exception v4

    move-object v5, v12

    move-object v7, v14

    move-object v8, v15

    move-object v9, v6

    move-object/from16 v10, v26

    move-object/from16 v11, v27

    move-object v6, v13

    move-object/from16 v12, v28

    goto/16 :goto_7

    :catchall_7
    move-exception v4

    move-object v5, v12

    move-object v7, v14

    move-object/from16 v8, v25

    move-object v9, v6

    move-object/from16 v10, v26

    move-object/from16 v11, v27

    move-object v6, v13

    move-object/from16 v12, v28

    goto/16 :goto_7

    :catchall_8
    move-exception v4

    move-object v5, v12

    move-object v7, v8

    move-object v9, v6

    move-object/from16 v10, v26

    move-object/from16 v11, v27

    move-object/from16 v8, v25

    move-object v6, v13

    move-object/from16 v12, v28

    goto/16 :goto_7

    :catchall_9
    move-exception v4

    move-object v5, v12

    move-object v9, v6

    move-object/from16 v10, v26

    move-object/from16 v11, v27

    move-object v6, v7

    move-object/from16 v12, v28

    move-object v7, v8

    move-object/from16 v8, v25

    goto/16 :goto_7

    :catchall_a
    move-exception v4

    move-object v9, v6

    move-object/from16 v10, v26

    move-object/from16 v11, v27

    move-object/from16 v12, v28

    move-object v6, v7

    move-object v7, v8

    move-object/from16 v8, v25

    goto/16 :goto_7

    :cond_c
    move-object v8, v5

    goto/16 :goto_3

    :cond_d
    move-object/from16 v29, v5

    goto/16 :goto_1
.end method

.method private a(Ljava/io/File;Latm;Lats;Z)Lauf;
    .locals 20

    .prologue
    .line 537
    const/4 v3, 0x0

    .line 540
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Latw;->v:Lbig;

    .line 541
    invoke-virtual/range {p1 .. p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Lbig;->a(Ljava/lang/String;)Lbih;
    :try_end_0
    .catch Lbpr; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v11

    .line 551
    const/4 v10, -0x1

    .line 552
    const/4 v2, -0x1

    .line 553
    const/4 v4, -0x1

    .line 554
    const/4 v5, 0x0

    .line 555
    const/4 v6, -0x1

    .line 556
    const/4 v7, 0x0

    .line 557
    const/4 v3, 0x0

    :goto_0
    :try_start_1
    invoke-interface {v11}, Lbih;->f()I

    move-result v8

    if-ge v3, v8, :cond_4

    .line 558
    invoke-interface {v11, v3}, Lbih;->a(I)Landroid/media/MediaFormat;

    move-result-object v8

    .line 559
    invoke-static {v8}, Lbkf;->a(Landroid/media/MediaFormat;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 560
    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 561
    new-instance v2, Lauc;

    const-string v3, "there should only be one audio track"

    invoke-direct {v2, v3}, Lauc;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 714
    :catchall_0
    move-exception v2

    move-object v3, v11

    :goto_1
    invoke-static {v3}, Lcgl;->a(Lcgk;)V

    throw v2

    .line 544
    :catch_0
    move-exception v2

    :try_start_2
    const-string v2, "Resume file unexpectedly missing."

    invoke-static {v2}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v2

    throw v2

    .line 714
    :catchall_1
    move-exception v2

    goto :goto_1

    .line 545
    :catch_1
    move-exception v2

    .line 548
    new-instance v4, Lauc;

    invoke-direct {v4, v2}, Lauc;-><init>(Ljava/lang/Throwable;)V

    throw v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 565
    :cond_0
    :try_start_3
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Latm;->a(Landroid/media/MediaFormat;)I

    move-result v4

    .line 566
    invoke-interface {v11, v3}, Lbih;->a(I)Landroid/media/MediaFormat;

    move-result-object v5

    move v2, v3

    move v8, v10

    .line 557
    :goto_2
    add-int/lit8 v3, v3, 0x1

    move v10, v8

    goto :goto_0

    .line 570
    :cond_1
    invoke-static {v8}, Lbkf;->b(Landroid/media/MediaFormat;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 571
    const/4 v6, -0x1

    if-eq v10, v6, :cond_2

    .line 572
    new-instance v2, Lauc;

    const-string v3, "there should only be one video track"

    invoke-direct {v2, v3}, Lauc;-><init>(Ljava/lang/String;)V

    throw v2

    .line 576
    :cond_2
    move-object/from16 v0, p2

    invoke-virtual {v0, v8}, Latm;->a(Landroid/media/MediaFormat;)I

    move-result v6

    .line 577
    invoke-interface {v11, v3}, Lbih;->a(I)Landroid/media/MediaFormat;

    move-result-object v7

    move v8, v3

    goto :goto_2

    .line 582
    :cond_3
    new-instance v2, Lauc;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x19

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "unexpected track format: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lauc;-><init>(Ljava/lang/String;)V

    throw v2

    .line 585
    :cond_4
    if-eqz p4, :cond_5

    const/4 v3, -0x1

    if-ne v2, v3, :cond_5

    .line 586
    new-instance v2, Lauc;

    const-string v3, "there should be an audio track"

    invoke-direct {v2, v3}, Lauc;-><init>(Ljava/lang/String;)V

    throw v2

    .line 588
    :cond_5
    const/4 v3, -0x1

    if-ne v10, v3, :cond_6

    .line 589
    new-instance v2, Lauc;

    const-string v3, "there should be a video track"

    invoke-direct {v2, v3}, Lauc;-><init>(Ljava/lang/String;)V

    throw v2

    .line 595
    :cond_6
    const-wide/16 v8, 0x0

    .line 596
    const-wide/16 v12, 0x0

    .line 597
    if-eqz p4, :cond_7

    .line 598
    invoke-interface {v11, v2}, Lbih;->b(I)V

    .line 600
    :cond_7
    invoke-interface {v11, v10}, Lbih;->b(I)V

    .line 602
    :goto_3
    invoke-interface {v11}, Lbih;->e()I

    move-result v3

    const/4 v14, -0x1

    if-eq v3, v14, :cond_a

    .line 604
    invoke-interface {v11}, Lbih;->d()J

    move-result-wide v14

    .line 605
    if-ne v3, v2, :cond_8

    move-wide v8, v12

    .line 621
    :goto_4
    invoke-interface {v11}, Lbih;->b()Z

    move-wide v12, v8

    move-wide v8, v14

    .line 622
    goto :goto_3

    .line 611
    :cond_8
    if-ne v3, v10, :cond_9

    move-wide/from16 v18, v14

    move-wide v14, v8

    move-wide/from16 v8, v18

    .line 616
    goto :goto_4

    .line 618
    :cond_9
    new-instance v2, Lauc;

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x29

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "invalid track from extractor: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lauc;-><init>(Ljava/lang/String;)V

    throw v2

    .line 625
    :cond_a
    const-wide/16 v14, 0x0

    const/4 v3, 0x0

    invoke-interface {v11, v14, v15, v3}, Lbih;->a(JI)V

    .line 626
    if-eqz p4, :cond_b

    .line 627
    invoke-interface {v11, v2}, Lbih;->b(I)V

    .line 629
    :cond_b
    invoke-interface {v11, v10}, Lbih;->b(I)V

    .line 631
    if-eqz p4, :cond_c

    :goto_5
    invoke-static {v8, v9, v12, v13}, Ljava/lang/Math;->min(JJ)J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-wide v8

    .line 634
    const-wide/16 v12, 0x0

    cmp-long v3, v8, v12

    if-nez v3, :cond_d

    .line 636
    invoke-static {v11}, Lcgl;->a(Lcgk;)V

    const/4 v3, 0x0

    .line 714
    :goto_6
    return-object v3

    .line 631
    :cond_c
    const-wide v8, 0x7fffffffffffffffL

    goto :goto_5

    .line 642
    :cond_d
    :try_start_4
    new-instance v12, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v12}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    .line 644
    invoke-static {}, Lath;->c()I

    move-result v3

    invoke-static {v3}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v13

    .line 645
    :goto_7
    invoke-interface {v11}, Lbih;->e()I

    move-result v3

    const/4 v14, -0x1

    if-eq v3, v14, :cond_13

    .line 647
    const/4 v3, 0x0

    invoke-interface {v11, v13, v3}, Lbih;->a(Ljava/nio/ByteBuffer;I)I

    move-result v3

    iput v3, v12, Landroid/media/MediaCodec$BufferInfo;->size:I

    .line 648
    iget v3, v12, Landroid/media/MediaCodec$BufferInfo;->size:I

    if-gez v3, :cond_e

    .line 649
    new-instance v2, Lauc;

    const-string v3, "failed to read sample"

    invoke-direct {v2, v3}, Lauc;-><init>(Ljava/lang/String;)V

    throw v2

    .line 651
    :cond_e
    invoke-interface {v11}, Lbih;->d()J

    move-result-wide v14

    iput-wide v14, v12, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    .line 652
    invoke-interface {v11}, Lbih;->c()I

    move-result v3

    iput v3, v12, Landroid/media/MediaCodec$BufferInfo;->flags:I

    .line 653
    const/4 v3, 0x0

    iput v3, v12, Landroid/media/MediaCodec$BufferInfo;->offset:I

    .line 655
    invoke-interface {v11}, Lbih;->e()I

    move-result v14

    .line 656
    const/4 v3, -0x1

    .line 657
    if-ne v14, v2, :cond_10

    .line 662
    iget-wide v0, v12, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    move-wide/from16 v16, v0

    cmp-long v15, v16, v8

    if-gez v15, :cond_f

    move v3, v4

    .line 691
    :cond_f
    :goto_8
    const/4 v15, -0x1

    if-eq v3, v15, :cond_12

    .line 694
    move-object/from16 v0, p2

    invoke-virtual {v0, v3, v13, v12}, Latm;->a(ILjava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;)V

    .line 696
    iget-wide v14, v12, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    move-object/from16 v0, p3

    invoke-interface {v0, v3, v14, v15}, Lats;->a(IJ)V

    .line 706
    :goto_9
    invoke-interface {v11}, Lbih;->b()Z

    goto :goto_7

    .line 672
    :cond_10
    if-ne v14, v10, :cond_11

    .line 677
    iget-wide v0, v12, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    move-wide/from16 v16, v0

    cmp-long v15, v16, v8

    if-gez v15, :cond_f

    move v3, v6

    .line 678
    goto :goto_8

    .line 688
    :cond_11
    new-instance v2, Lauc;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x29

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "invalid track from extractor: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lauc;-><init>(Ljava/lang/String;)V

    throw v2

    .line 704
    :cond_12
    invoke-interface {v11, v14}, Lbih;->c(I)V

    goto :goto_9

    .line 709
    :cond_13
    new-instance v3, Lauf;

    invoke-direct/range {v3 .. v9}, Lauf;-><init>(ILandroid/media/MediaFormat;ILandroid/media/MediaFormat;J)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 714
    invoke-static {v11}, Lcgl;->a(Lcgk;)V

    goto/16 :goto_6
.end method

.method static synthetic a(Latw;Lcdx;)Lcdx;
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Latw;->d:Lcdx;

    return-object p1
.end method

.method static synthetic a(Latw;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Latw;->y:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic a(Latw;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Latw;->z:Ljava/lang/Thread;

    return-object p1
.end method

.method static synthetic a(Latw;Laue;)V
    .locals 5

    .prologue
    .line 58
    iget-object v0, p1, Laue;->b:Latu;

    iget-boolean v1, p0, Latw;->x:Z

    invoke-static {v0, v1}, Lath;->a(Latu;Z)Landroid/media/MediaFormat;

    move-result-object v0

    iget-boolean v1, p1, Laue;->h:Z

    invoke-static {v1}, Lath;->b(Z)Landroid/media/MediaFormat;

    move-result-object v3

    iget-object v1, p1, Laue;->e:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->mkdirs()Z

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/FileWriter;

    iget-object v4, p1, Laue;->e:Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/FileWriter;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    const-string v2, "Storyboard: "

    invoke-virtual {v1, v2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    iget-object v2, p1, Laue;->a:Lboi;

    invoke-virtual {v2}, Lboi;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/io/FileWriter;->write(I)V

    const-string v2, "Resolution: "

    invoke-virtual {v1, v2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    iget-object v2, p1, Laue;->b:Latu;

    invoke-virtual {v2}, Latu;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Ljava/io/FileWriter;->write(I)V

    const-string v2, "VideoFormat: "

    invoke-virtual {v1, v2}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/media/MediaFormat;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Ljava/io/FileWriter;->write(I)V

    const-string v0, "AudioFormat: "

    invoke-virtual {v1, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/media/MediaFormat;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Ljava/io/FileWriter;->write(I)V

    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Ljava/io/FileWriter;->write(I)V

    const-string v0, "hasAudio: "

    invoke-virtual {v1, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    iget-object v0, p1, Laue;->a:Lboi;

    invoke-static {v0}, Latw;->a(Lboi;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "true"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    const-string v0, "AppInfo: "

    invoke-virtual {v1, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    iget-object v0, p0, Latw;->p:Lald;

    invoke-virtual {v0}, Lald;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Ljava/io/FileWriter;->write(I)V

    const-string v0, "DeviceInfo"

    invoke-virtual {v1, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    iget-object v0, p0, Latw;->q:Lamy;

    invoke-virtual {v0}, Lamy;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/FileWriter;->write(Ljava/lang/String;)V

    const/16 v0, 0xa

    invoke-virtual {v1, v0}, Ljava/io/FileWriter;->write(I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-static {v1}, Lcek;->a(Ljava/io/Closeable;)V

    :goto_1
    return-void

    :cond_0
    :try_start_2
    const-string v0, "false"
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v1, v2

    :goto_2
    :try_start_3
    sget-object v2, Latw;->a:Ljava/lang/String;

    const-string v3, "failed to write metadata"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    invoke-static {v1}, Lcek;->a(Ljava/io/Closeable;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    invoke-static {v1}, Lcek;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method static synthetic a(Latw;Z)Z
    .locals 0

    .prologue
    .line 58
    iput-boolean p1, p0, Latw;->c:Z

    return p1
.end method

.method private static a(Lboi;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 719
    move v0, v1

    :goto_0
    invoke-virtual {p0}, Lboi;->b()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 720
    invoke-virtual {p0, v0}, Lboi;->a(I)Lbma;

    move-result-object v2

    invoke-interface {v2}, Lbma;->a()I

    move-result v2

    if-lez v2, :cond_1

    .line 721
    const/4 v1, 0x1

    .line 724
    :cond_0
    return v1

    .line 719
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Latw;Laue;)J
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0, p1}, Latw;->a(Laue;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Latw;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Latw;)Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Latw;->z:Ljava/lang/Thread;

    return-object v0
.end method

.method static synthetic b(Latw;Z)Z
    .locals 0

    .prologue
    .line 58
    iput-boolean p1, p0, Latw;->e:Z

    return p1
.end method

.method static synthetic c(Latw;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Latw;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic d(Latw;)Lbgf;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Latw;->m:Lbgf;

    return-object v0
.end method

.method static synthetic e(Latw;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Latw;->h:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic f(Latw;)Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Latw;->t:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic g(Latw;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Latw;->i:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic h(Latw;)Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Latw;->c:Z

    return v0
.end method

.method static synthetic i(Latw;)Z
    .locals 1

    .prologue
    .line 58
    iget-boolean v0, p0, Latw;->e:Z

    return v0
.end method

.method static synthetic j(Latw;)Lcdx;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Latw;->d:Lcdx;

    return-object v0
.end method

.method static synthetic k(Latw;)Latx;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Latw;->w:Latx;

    return-object v0
.end method

.method static synthetic l(Latw;)J
    .locals 2

    .prologue
    .line 58
    iget-wide v0, p0, Latw;->f:J

    return-wide v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 235
    iget-object v1, p0, Latw;->y:Ljava/lang/Object;

    monitor-enter v1

    .line 236
    :try_start_0
    iget-object v0, p0, Latw;->z:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Latw;->z:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 238
    const/4 v0, 0x0

    iput-object v0, p0, Latw;->z:Ljava/lang/Thread;

    .line 240
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lboi;Latu;Ljava/io/File;Ljava/io/File;Ljava/io/File;Ljava/lang/String;Z)V
    .locals 10

    .prologue
    .line 218
    iget-object v9, p0, Latw;->y:Ljava/lang/Object;

    monitor-enter v9

    .line 219
    :try_start_0
    iget-object v0, p0, Latw;->z:Ljava/lang/Thread;

    const-string v1, "mEncodingThread"

    const-string v2, "already started"

    invoke-static {v0, v1, v2}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 220
    new-instance v0, Laue;

    .line 223
    invoke-virtual {p1}, Lboi;->o()I

    move-result v1

    invoke-virtual {p1}, Lboi;->p()I

    move-result v2

    if-le v1, v2, :cond_0

    const/4 v3, 0x0

    :goto_0
    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Laue;-><init>(Lboi;Latu;ILjava/io/File;Ljava/io/File;Ljava/io/File;Ljava/lang/String;Z)V

    .line 229
    iget-object v1, p0, Latw;->r:Lchl;

    new-instance v2, Latz;

    invoke-direct {v2, p0, v0}, Latz;-><init>(Latw;Laue;)V

    const-string v0, "encoding"

    invoke-interface {v1, v2, v0}, Lchl;->a(Ljava/lang/Runnable;Ljava/lang/String;)Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Latw;->z:Ljava/lang/Thread;

    .line 230
    iget-object v0, p0, Latw;->z:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 231
    monitor-exit v9

    return-void

    .line 223
    :cond_0
    const/16 v3, 0x5a

    goto :goto_0

    .line 231
    :catchall_0
    move-exception v0

    monitor-exit v9
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
