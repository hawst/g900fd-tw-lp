.class public Lbrc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbqz;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lbrd;

.field private final c:Ljava/io/File;

.field private final d:Lbri;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/Object;

.field private final g:Lcff;

.field private h:Lbqw;

.field private i:Lbre;

.field private j:Lbre;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lbrc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbrc;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lbri;Ljava/io/File;Ljava/util/Locale;Lcff;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    new-instance v0, Lbrd;

    invoke-direct {v0, p0}, Lbrd;-><init>(Lbrc;)V

    iput-object v0, p0, Lbrc;->b:Lbrd;

    .line 74
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbrc;->f:Ljava/lang/Object;

    .line 128
    const-string v0, "settings"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbri;

    iput-object v0, p0, Lbrc;->d:Lbri;

    .line 129
    const-string v0, "cacheFile"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, Lbrc;->c:Ljava/io/File;

    .line 130
    invoke-virtual {p3}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbrc;->e:Ljava/lang/String;

    .line 131
    const-string v0, "httpFetcher"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcff;

    iput-object v0, p0, Lbrc;->g:Lcff;

    .line 132
    return-void
.end method

.method public static a(Lbmk;J)I
    .locals 9

    .prologue
    .line 228
    invoke-virtual {p0}, Lbmk;->d()J

    move-result-wide v0

    const-string v2, "duration"

    invoke-static {v0, v1, v2}, Lcec;->a(JLjava/lang/CharSequence;)J

    .line 229
    const-string v3, "timeUs"

    const-wide/16 v4, 0x0

    invoke-virtual {p0}, Lbmk;->d()J

    move-result-wide v6

    move-wide v1, p1

    invoke-static/range {v1 .. v7}, Lcec;->a(JLjava/lang/CharSequence;JJ)J

    .line 230
    long-to-float v0, p1

    invoke-virtual {p0}, Lbmk;->d()J

    move-result-wide v2

    long-to-float v1, v2

    div-float/2addr v0, v1

    .line 231
    invoke-virtual {p0}, Lbmk;->e()I

    move-result v1

    .line 232
    invoke-virtual {p0}, Lbmk;->e()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v0, v2

    float-to-int v0, v0

    const v2, 0x186a0

    add-int/2addr v0, v2

    .line 231
    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 233
    invoke-virtual {p0}, Lbmk;->e()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method static synthetic a(Lbrc;Lboh;)Lbmk;
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lbrc;->b(Lboh;)Lbmk;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lbri;Lbrb;Ljava/io/File;Ljava/util/Locale;)Lbrc;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 105
    new-instance v1, Lbrc;

    new-instance v0, Lcer;

    .line 107
    invoke-interface {p0}, Lbri;->u()I

    move-result v2

    .line 108
    invoke-interface {p0}, Lbri;->u()I

    move-result v3

    invoke-direct {v0, v2, v3}, Lcer;-><init>(II)V

    invoke-direct {v1, p0, p2, p3, v0}, Lbrc;-><init>(Lbri;Ljava/io/File;Ljava/util/Locale;Lcff;)V

    .line 110
    new-instance v0, Lbre;

    invoke-direct {v0}, Lbre;-><init>()V

    invoke-direct {v1, v0}, Lbrc;->a(Lbre;)V

    .line 112
    :try_start_0
    invoke-direct {v1}, Lbrc;->g()Lbre;

    move-result-object v0

    invoke-direct {v1, v0}, Lbrc;->a(Lbre;)V
    :try_end_0
    .catch Lbrg; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lbrf; {:try_start_0 .. :try_end_0} :catch_2

    .line 122
    :goto_0
    invoke-virtual {p1, v1}, Lbrb;->a(Lbqz;)Lbqw;

    move-result-object v0

    iget-object v2, v1, Lbrc;->h:Lbqw;

    const-string v3, "mMusicCache"

    invoke-static {v2, v3, v4}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    const-string v2, "musicCache"

    invoke-static {v0, v2, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqw;

    iput-object v0, v1, Lbrc;->h:Lbqw;

    .line 123
    return-object v1

    .line 113
    :catch_0
    move-exception v0

    .line 114
    sget-object v2, Lbrc;->a:Ljava/lang/String;

    const-string v3, "Couldn\'t use cached track metadata - inconsistent protobuf"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 115
    :catch_1
    move-exception v0

    .line 116
    sget-object v2, Lbrc;->a:Ljava/lang/String;

    const-string v3, "Couldn\'t use cached track metadata - IO exception"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 117
    :catch_2
    move-exception v0

    .line 118
    sget-object v2, Lbrc;->a:Ljava/lang/String;

    const-string v3, "Couldn\'t use cached track metadata - encryption method not supported"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Ljava/net/HttpURLConnection;
    .locals 2

    .prologue
    .line 482
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 483
    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    .line 484
    iget-object v1, p0, Lbrc;->d:Lbri;

    invoke-interface {v1}, Lbri;->u()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 485
    iget-object v1, p0, Lbrc;->d:Lbri;

    invoke-interface {v1}, Lbri;->t()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 486
    return-object v0
.end method

.method private a(Lbre;)V
    .locals 2

    .prologue
    .line 371
    iget-object v1, p0, Lbrc;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 372
    :try_start_0
    iput-object p1, p0, Lbrc;->i:Lbre;

    .line 373
    iget-object v0, p0, Lbrc;->b:Lbrd;

    invoke-virtual {v0}, Lbrd;->evictAll()V

    .line 374
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private b(Lboh;)Lbmk;
    .locals 14

    .prologue
    const-wide/16 v8, -0x1

    const/4 v0, 0x0

    const/4 v10, -0x1

    .line 415
    invoke-direct {p0}, Lbrc;->h()Lbre;

    move-result-object v1

    invoke-virtual {v1}, Lbre;->a()Ljava/lang/String;

    move-result-object v1

    .line 416
    invoke-virtual {p1}, Lboh;->c()J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lbrc;->b(J)Ljava/lang/String;

    move-result-object v2

    const-string v3, ".metadata"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x0

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 418
    invoke-direct {p0, v1}, Lbrc;->a(Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v1

    .line 422
    :try_start_0
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v2}, Lcgs;->a(Ljava/io/InputStream;)[B

    move-result-object v2

    .line 421
    const/4 v3, 0x0

    array-length v4, v2

    invoke-static {v2, v3, v4}, Loxn;->a([BII)Loxn;

    move-result-object v2

    invoke-static {v2}, Lbui;->c(Loxn;)Lbui;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 424
    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 427
    iget v1, v2, Lbui;->e:I

    if-ne v1, v10, :cond_0

    .line 428
    const-string v0, "beatsPerBar"

    invoke-static {v0}, Lbrg;->a(Ljava/lang/String;)Lbrg;

    move-result-object v0

    throw v0

    .line 424
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v0

    .line 430
    :cond_0
    iget-wide v4, v2, Lbui;->b:J

    cmp-long v1, v4, v8

    if-nez v1, :cond_1

    .line 431
    const-string v0, "duration"

    invoke-static {v0}, Lbrg;->a(Ljava/lang/String;)Lbrg;

    move-result-object v0

    throw v0

    .line 433
    :cond_1
    iget-wide v4, v2, Lbui;->c:J

    cmp-long v1, v4, v8

    if-nez v1, :cond_2

    .line 434
    const-string v0, "firstBeatTimeUs"

    invoke-static {v0}, Lbrg;->a(Ljava/lang/String;)Lbrg;

    move-result-object v0

    throw v0

    .line 436
    :cond_2
    iget v1, v2, Lbui;->d:I

    if-ne v1, v10, :cond_3

    .line 437
    const-string v0, "sizeBytes"

    invoke-static {v0}, Lbrg;->a(Ljava/lang/String;)Lbrg;

    move-result-object v0

    throw v0

    .line 439
    :cond_3
    iget v1, v2, Lbui;->d:I

    if-gtz v1, :cond_4

    .line 440
    const-string v0, "sizeBytes"

    iget v1, v2, Lbui;->d:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0xb

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lbrg;->a(Ljava/lang/String;Ljava/lang/String;)Lbrg;

    move-result-object v0

    throw v0

    .line 445
    :cond_4
    iget-object v1, v2, Lbui;->a:[I

    array-length v1, v1

    new-array v3, v1, [J

    .line 446
    const-wide/16 v4, 0x0

    .line 448
    iget-object v6, v2, Lbui;->a:[I

    array-length v7, v6

    move v1, v0

    :goto_0
    if-ge v0, v7, :cond_5

    aget v8, v6, v0

    .line 449
    int-to-long v8, v8

    add-long/2addr v4, v8

    .line 450
    aput-wide v4, v3, v1

    .line 451
    add-int/lit8 v1, v1, 0x1

    .line 448
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 455
    :cond_5
    iget v0, v2, Lbui;->e:I

    if-ne v0, v10, :cond_6

    const/4 v10, 0x4

    .line 456
    :goto_1
    new-instance v1, Lbmk;

    iget-wide v4, v2, Lbui;->b:J

    iget-wide v6, v2, Lbui;->c:J

    iget v8, v2, Lbui;->d:I

    iget-object v0, p0, Lbrc;->h:Lbqw;

    .line 458
    invoke-virtual {p1}, Lboh;->c()J

    move-result-wide v12

    invoke-virtual {v0, v12, v13}, Lbqw;->a(J)Landroid/net/Uri;

    move-result-object v9

    move-object v2, p1

    invoke-direct/range {v1 .. v10}, Lbmk;-><init>(Lboh;[JJJILandroid/net/Uri;I)V

    return-object v1

    .line 455
    :cond_6
    iget v10, v2, Lbui;->e:I

    goto :goto_1
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lbrc;->a:Ljava/lang/String;

    return-object v0
.end method

.method private f()Lbre;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 335
    iget-object v0, p0, Lbrc;->d:Lbri;

    .line 336
    invoke-interface {v0}, Lbri;->v()[Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lbrc;->e:Ljava/lang/String;

    const-string v0, ""

    array-length v6, v4

    move v2, v3

    :goto_0
    if-ge v2, v6, :cond_2

    aget-object v1, v4, v2

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v8

    if-le v7, v8, :cond_1

    invoke-virtual {v5, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    if-eq v7, v8, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->charAt(I)C

    move-result v7

    const/16 v8, 0x5f

    if-ne v7, v8, :cond_1

    :cond_0
    move-object v0, v1

    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 337
    :cond_2
    iget-object v1, p0, Lbrc;->d:Lbri;

    invoke-interface {v1}, Lbri;->s()Ljava/lang/String;

    move-result-object v2

    .line 338
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 339
    sget-object v1, Lbrc;->a:Ljava/lang/String;

    .line 341
    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v1, "index"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v3, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 342
    :goto_1
    iget-object v3, p0, Lbrc;->g:Lcff;

    invoke-interface {v3, v1}, Lcff;->a(Ljava/lang/String;)[B

    move-result-object v1

    .line 344
    array-length v3, v1

    if-nez v3, :cond_6

    .line 345
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x40

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Zero length response when loading index for locale "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " from network"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 347
    sget-object v1, Lbrc;->a:Ljava/lang/String;

    .line 348
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 341
    :cond_4
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    const-string v1, "%s%s-%s"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v2, v4, v3

    const/4 v3, 0x1

    const-string v5, "index"

    aput-object v5, v4, v3

    const/4 v3, 0x2

    aput-object v0, v4, v3

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 352
    :cond_6
    invoke-static {v1}, Lbuk;->a([B)Lbuk;

    move-result-object v0

    .line 357
    new-instance v3, Ljava/io/FileOutputStream;

    iget-object v4, p0, Lbrc;->c:Ljava/io/File;

    invoke-direct {v3, v4}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 359
    :try_start_0
    invoke-virtual {v3, v1}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 361
    invoke-static {v3}, Lcek;->a(Ljava/io/Closeable;)V

    .line 364
    new-instance v1, Lbre;

    invoke-direct {v1, v0, v2}, Lbre;-><init>(Lbuk;Ljava/lang/String;)V

    return-object v1

    .line 361
    :catchall_0
    move-exception v0

    invoke-static {v3}, Lcek;->a(Ljava/io/Closeable;)V

    throw v0
.end method

.method private g()Lbre;
    .locals 4

    .prologue
    .line 383
    iget-object v0, p0, Lbrc;->c:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 387
    const/4 v2, 0x0

    .line 389
    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    iget-object v0, p0, Lbrc;->c:Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    :try_start_1
    invoke-static {v1}, Lcgs;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 390
    const/4 v2, 0x0

    array-length v3, v0

    invoke-static {v0, v2, v3}, Loxn;->a([BII)Loxn;

    move-result-object v0

    invoke-static {v0}, Lbuk;->c(Loxn;)Lbuk;

    move-result-object v2

    .line 392
    new-instance v0, Lbre;

    iget-object v3, p0, Lbrc;->d:Lbri;

    invoke-interface {v3}, Lbri;->s()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v2, v3}, Lbre;-><init>(Lbuk;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 394
    invoke-static {v1}, Lcek;->a(Ljava/io/Closeable;)V

    .line 401
    :goto_0
    return-object v0

    .line 394
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_1
    invoke-static {v1}, Lcek;->a(Ljava/io/Closeable;)V

    throw v0

    .line 401
    :cond_0
    new-instance v0, Lbre;

    invoke-direct {v0}, Lbre;-><init>()V

    goto :goto_0

    .line 394
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method private h()Lbre;
    .locals 2

    .prologue
    .line 467
    iget-object v1, p0, Lbrc;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 468
    :try_start_0
    iget-object v0, p0, Lbrc;->i:Lbre;

    monitor-exit v1

    return-object v0

    .line 469
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(Lboh;)Lbmk;
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lbrc;->b:Lbrd;

    invoke-virtual {v0, p1}, Lbrd;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmk;

    return-object v0
.end method

.method public a(J)Lboh;
    .locals 1

    .prologue
    .line 192
    invoke-direct {p0}, Lbrc;->h()Lbre;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lbre;->a(J)Lboh;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbmo;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbmo;",
            ")",
            "Ljava/util/List",
            "<",
            "Lboh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 196
    invoke-direct {p0}, Lbrc;->h()Lbre;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbre;->a(Lbmo;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbza;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbza;",
            ")",
            "Ljava/util/List",
            "<",
            "Lboh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 204
    invoke-direct {p0}, Lbrc;->h()Lbre;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbre;->a(Lbza;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(JLjava/io/File;II)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 251
    invoke-virtual {p3}, Ljava/io/File;->length()J

    move-result-wide v0

    long-to-int v0, v0

    .line 252
    if-eq v0, p4, :cond_0

    .line 253
    sget-object v1, Lbrc;->a:Ljava/lang/String;

    invoke-virtual {p3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x4a

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Music data Cache file "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " was "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " bytes long but expected "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move p4, v0

    .line 258
    :cond_0
    invoke-direct {p0}, Lbrc;->h()Lbre;

    move-result-object v0

    invoke-virtual {v0}, Lbre;->a()Ljava/lang/String;

    move-result-object v0

    .line 259
    invoke-virtual {p0, p1, p2}, Lbrc;->b(J)Ljava/lang/String;

    move-result-object v1

    const-string v3, ".mp3"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x0

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 258
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 260
    add-int/lit8 v1, p5, -0x1

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x1d

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "bytes="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 264
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lbrc;->a(Ljava/lang/String;)Ljava/net/HttpURLConnection;

    move-result-object v4

    .line 266
    :try_start_0
    const-string v0, "range"

    invoke-virtual {v4, v0, v1}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 269
    :try_start_1
    new-instance v1, Ljava/io/FileOutputStream;

    const/4 v0, 0x1

    invoke-direct {v1, p3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 270
    :try_start_2
    invoke-static {v3, v1}, Lcgs;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 272
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 273
    invoke-static {v3}, Lcek;->a(Ljava/io/Closeable;)V

    .line 274
    invoke-static {v1}, Lcek;->a(Ljava/io/Closeable;)V

    .line 278
    invoke-virtual {p3}, Ljava/io/File;->length()J

    move-result-wide v0

    int-to-long v2, p5

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 279
    new-instance v0, Ljava/io/IOException;

    .line 280
    invoke-virtual {p3}, Ljava/io/File;->length()J

    move-result-wide v2

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x6f

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Data from server was shorter than expected. Cache file is "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes, but should be "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 272
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_0
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 273
    invoke-static {v2}, Lcek;->a(Ljava/io/Closeable;)V

    .line 274
    invoke-static {v1}, Lcek;->a(Ljava/io/Closeable;)V

    throw v0

    .line 282
    :cond_1
    return-void

    .line 272
    :catchall_1
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_0

    :catchall_2
    move-exception v0

    move-object v2, v3

    goto :goto_0
.end method

.method public a()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 151
    :try_start_0
    invoke-direct {p0}, Lbrc;->f()Lbre;

    move-result-object v1

    .line 152
    const-string v2, "musicData"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 153
    iget-object v2, p0, Lbrc;->f:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lbrg; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lbrf; {:try_start_0 .. :try_end_0} :catch_2

    .line 154
    :try_start_1
    iput-object v1, p0, Lbrc;->j:Lbre;

    .line 155
    monitor-exit v2

    .line 156
    const/4 v0, 0x1

    .line 167
    :goto_0
    return v0

    .line 155
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lbrg; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lbrf; {:try_start_2 .. :try_end_2} :catch_2

    .line 161
    :catch_0
    move-exception v1

    goto :goto_0

    .line 163
    :catch_1
    move-exception v1

    sget-object v1, Lbrc;->a:Ljava/lang/String;

    goto :goto_0

    .line 166
    :catch_2
    move-exception v1

    sget-object v1, Lbrc;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Lbmk;I)Z
    .locals 4

    .prologue
    .line 291
    iget-object v0, p0, Lbrc;->h:Lbqw;

    .line 292
    invoke-virtual {p1}, Lbmk;->b()Lboh;

    move-result-object v1

    invoke-virtual {v1}, Lboh;->c()J

    move-result-wide v2

    .line 291
    invoke-virtual {v0, v2, v3, p2}, Lbqw;->a(JI)Z

    move-result v0

    .line 297
    return v0
.end method

.method public b(J)Ljava/lang/String;
    .locals 1

    .prologue
    .line 241
    invoke-static {p1, p2}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 179
    iget-object v1, p0, Lbrc;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 180
    :try_start_0
    iget-object v0, p0, Lbrc;->j:Lbre;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lbrc;->j:Lbre;

    invoke-direct {p0, v0}, Lbrc;->a(Lbre;)V

    .line 182
    const/4 v0, 0x0

    iput-object v0, p0, Lbrc;->j:Lbre;

    .line 184
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 188
    invoke-direct {p0}, Lbrc;->h()Lbre;

    move-result-object v0

    invoke-virtual {v0}, Lbre;->b()Z

    move-result v0

    return v0
.end method

.method public d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbmo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 200
    invoke-direct {p0}, Lbrc;->h()Lbre;

    move-result-object v0

    invoke-virtual {v0}, Lbre;->c()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
