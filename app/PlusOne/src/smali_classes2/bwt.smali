.class final Lbwt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lbwq;


# direct methods
.method constructor <init>(Lbwq;)V
    .locals 0

    .prologue
    .line 532
    iput-object p1, p0, Lbwt;->a:Lbwq;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 536
    iget-object v2, p0, Lbwt;->a:Lbwq;

    invoke-static {v2}, Lbwq;->a(Lbwq;)Lbvs;

    move-result-object v2

    invoke-interface {v2}, Lbvs;->c()Z

    move-result v3

    .line 537
    iget-object v2, p0, Lbwt;->a:Lbwq;

    invoke-static {v2}, Lbwq;->a(Lbwq;)Lbvs;

    move-result-object v2

    invoke-interface {v2}, Lbvs;->b()Z

    move-result v4

    .line 538
    iget-object v2, p0, Lbwt;->a:Lbwq;

    invoke-static {v2}, Lbwq;->a(Lbwq;)Lbvs;

    move-result-object v2

    invoke-interface {v2}, Lbvs;->a()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    .line 539
    :goto_0
    if-nez v3, :cond_3

    if-nez v4, :cond_3

    if-eqz v2, :cond_3

    .line 540
    :goto_1
    iget-object v1, p0, Lbwt;->a:Lbwq;

    invoke-static {v1}, Lbwq;->b(Lbwq;)Livc;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 541
    iget-object v1, p0, Lbwt;->a:Lbwq;

    invoke-static {v1}, Lbwq;->b(Lbwq;)Livc;

    move-result-object v1

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x67

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Updating analysis status to: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "(battery too low: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, ", screen on: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", power disconnected: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Livc;->a(Ljava/lang/String;)V

    .line 546
    :cond_0
    if-nez v0, :cond_1

    .line 547
    iget-object v0, p0, Lbwt;->a:Lbwq;

    invoke-static {v0}, Lbwq;->c(Lbwq;)Laim;

    move-result-object v0

    invoke-virtual {v0}, Laim;->a()V

    .line 549
    :cond_1
    return-void

    :cond_2
    move v2, v1

    .line 538
    goto :goto_0

    :cond_3
    move v0, v1

    .line 539
    goto :goto_1
.end method
