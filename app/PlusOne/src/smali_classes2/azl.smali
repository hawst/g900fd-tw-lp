.class public final Lazl;
.super Lazi;
.source "PG"


# instance fields
.field private a:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const v5, 0x7f0204b7

    const/4 v1, 0x3

    .line 22
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    new-array v1, v1, [Layj;

    const/4 v2, 0x0

    new-instance v3, Layr;

    const v4, 0x7f02042a

    invoke-direct {v3, v4}, Layr;-><init>(I)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Lays;

    invoke-direct {v3}, Lays;-><init>()V

    const v4, 0x7f02042b

    .line 25
    invoke-virtual {v3, v4}, Lays;->a(I)Lays;

    move-result-object v3

    .line 26
    invoke-virtual {v3, v5}, Lays;->d(I)Lays;

    move-result-object v3

    .line 27
    invoke-virtual {v3}, Lays;->a()Layr;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, Lays;

    invoke-direct {v3}, Lays;-><init>()V

    const v4, 0x7f02042c

    .line 29
    invoke-virtual {v3, v4}, Lays;->a(I)Lays;

    move-result-object v3

    .line 30
    invoke-virtual {v3, v5}, Lays;->d(I)Lays;

    move-result-object v3

    .line 31
    invoke-virtual {v3}, Lays;->a()Layr;

    move-result-object v3

    aput-object v3, v1, v2

    .line 22
    invoke-direct {p0, v0, v1}, Lazi;-><init>([I[Layj;)V

    .line 19
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lazl;->a:Landroid/graphics/Matrix;

    .line 32
    return-void

    .line 22
    nop

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
    .end array-data
.end method


# virtual methods
.method protected a(Layj;Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/high16 v3, 0x42700000    # 60.0f

    .line 58
    new-instance v0, Lazm;

    new-instance v1, Layu;

    invoke-direct {v1}, Layu;-><init>()V

    .line 59
    invoke-virtual {v1, p1}, Layu;->a(Layj;)Layu;

    move-result-object v1

    const/16 v2, 0x53

    .line 60
    invoke-virtual {v1, v2}, Layu;->b(I)Layu;

    move-result-object v1

    const v2, 0x7f0b00a8

    .line 61
    invoke-virtual {p3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Layu;->a(I)Layu;

    move-result-object v1

    .line 62
    invoke-virtual {v1, v3}, Layu;->a(F)Layu;

    move-result-object v1

    .line 63
    invoke-virtual {v1, v3}, Layu;->b(F)Layu;

    move-result-object v1

    const-string v2, "fonts/Roboto-Black.ttf"

    .line 64
    invoke-static {p2, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Layu;->a(Landroid/graphics/Typeface;)Layu;

    move-result-object v1

    .line 65
    invoke-virtual {v1, v4}, Layu;->a(Z)Layu;

    move-result-object v1

    const/16 v2, 0x32

    .line 66
    invoke-virtual {v1, v2}, Layu;->c(I)Layu;

    move-result-object v1

    .line 67
    invoke-virtual {v1, v4}, Layu;->b(Z)Layu;

    move-result-object v1

    const v2, 0x7f0b00b2

    .line 68
    invoke-virtual {p3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Layu;->g(I)Layu;

    move-result-object v1

    invoke-direct {v0, v1}, Lazm;-><init>(Layu;)V

    return-object v0
.end method

.method public f(Layl;)I
    .locals 1

    .prologue
    .line 36
    const v0, 0x7f0205d0

    return v0
.end method

.method public g(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lazl;->a:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public h(Layl;)F
    .locals 4

    .prologue
    .line 51
    const/high16 v0, 0x3f000000    # 0.5f

    const v1, 0x3f333333    # 0.7f

    iget-wide v2, p1, Layl;->a:J

    long-to-int v2, v2

    invoke-static {v0, v1, v2}, Lbag;->a(FFI)F

    move-result v0

    return v0
.end method

.method public r(Layl;)I
    .locals 1

    .prologue
    .line 46
    const/16 v0, 0x8

    return v0
.end method
