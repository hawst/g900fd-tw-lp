.class public final enum Lcgd;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcgd;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcgd;

.field private static final synthetic e:[Lcgd;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 16
    new-instance v0, Lcgd;

    const-string v1, "SHOW_ALL_THEMES"

    const-string v2, "debug.mm.show_all_themes"

    const-string v3, "false"

    invoke-direct {v0, v1, v4, v2, v3}, Lcgd;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcgd;->a:Lcgd;

    .line 10
    const/4 v0, 0x1

    new-array v0, v0, [Lcgd;

    sget-object v1, Lcgd;->a:Lcgd;

    aput-object v1, v0, v4

    sput-object v0, Lcgd;->e:[Lcgd;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 35
    const/4 v5, 0x2

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcgd;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    .line 36
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 39
    iput-object p3, p0, Lcgd;->b:Ljava/lang/String;

    .line 40
    iput-object p4, p0, Lcgd;->c:Ljava/lang/String;

    .line 41
    iput p5, p0, Lcgd;->d:I

    .line 42
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcgd;
    .locals 1

    .prologue
    .line 10
    const-class v0, Lcgd;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcgd;

    return-object v0
.end method

.method public static values()[Lcgd;
    .locals 1

    .prologue
    .line 10
    sget-object v0, Lcgd;->e:[Lcgd;

    invoke-virtual {v0}, [Lcgd;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcgd;

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 45
    iget-object v0, p0, Lcgd;->c:Ljava/lang/String;

    .line 46
    iget v1, p0, Lcgd;->d:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    .line 47
    iget-object v0, p0, Lcgd;->b:Ljava/lang/String;

    iget-object v1, p0, Lcgd;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcgu;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 49
    :cond_0
    return-object v0

    .line 46
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 53
    const-string v0, "TRUE"

    invoke-virtual {p0}, Lcgd;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
