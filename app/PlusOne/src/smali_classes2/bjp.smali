.class public final Lbjp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcgk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lbhj;",
        ">",
        "Ljava/lang/Object;",
        "Lcgk;"
    }
.end annotation


# instance fields
.field private final a:Lbjq;

.field private final b:Lbjl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjl",
            "<TT;>;"
        }
    .end annotation
.end field

.field private c:I

.field private d:Z

.field private e:Lbjl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjl",
            "<TT;>;"
        }
    .end annotation
.end field

.field private f:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lbji",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private g:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lbji",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lbjp;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbjq;Lbjl;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbjq;",
            "Lbjl",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    const-string v0, "settings"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjq;

    iput-object v0, p0, Lbjp;->a:Lbjq;

    .line 75
    const-string v0, "decoderFactory"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjl;

    iput-object v0, p0, Lbjp;->b:Lbjl;

    .line 76
    return-void
.end method


# virtual methods
.method public a(Lbiy;)Lbji;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbiy;",
            ")",
            "Lbji",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 84
    iget-object v0, p0, Lbjp;->b:Lbjl;

    invoke-virtual {p0, p1, v0}, Lbjp;->a(Lbiy;Lbjl;)Lbji;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbiy;Lbjl;)Lbji;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbiy;",
            "Lbjl",
            "<TT;>;)",
            "Lbji",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 96
    const-string v0, "source"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 97
    const-string v0, "decoderFactory"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 99
    iget-object v0, p0, Lbjp;->f:Ljava/util/Queue;

    if-nez v0, :cond_0

    iget-object v0, p0, Lbjp;->a:Lbjq;

    invoke-interface {v0}, Lbjq;->a()I

    move-result v0

    iput v0, p0, Lbjp;->c:I

    iget-object v0, p0, Lbjp;->a:Lbjq;

    invoke-interface {v0}, Lbjq;->b()Z

    move-result v0

    iput-boolean v0, p0, Lbjp;->d:Z

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbjp;->f:Ljava/util/Queue;

    new-instance v0, Ljava/util/HashSet;

    iget v2, p0, Lbjp;->c:I

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lbjp;->g:Ljava/util/Set;

    .line 101
    :cond_0
    iget-boolean v0, p0, Lbjp;->d:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lbjp;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_1
    const/4 v0, 0x1

    :goto_0
    const-string v2, "no decoders should be available if reusing decoders is not enabled"

    invoke-static {v0, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 105
    iget-object v0, p0, Lbjp;->e:Lbjl;

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 106
    iget-object v0, p0, Lbjp;->g:Ljava/util/Set;

    const-string v2, "mAvailableDecoders"

    invoke-static {v0, v2}, Lcgp;->a(Ljava/util/Collection;Ljava/lang/CharSequence;)Ljava/util/Collection;

    .line 107
    invoke-virtual {p0}, Lbjp;->a()V

    .line 108
    iput-object p2, p0, Lbjp;->e:Lbjl;

    .line 112
    :cond_2
    invoke-interface {p1}, Lbiy;->e()Landroid/media/MediaFormat;

    move-result-object v2

    .line 113
    iget-object v0, p0, Lbjp;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 114
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbji;

    .line 115
    invoke-virtual {v0, v2}, Lbji;->a(Landroid/media/MediaFormat;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 116
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    .line 117
    iget-object v1, p0, Lbjp;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 118
    invoke-virtual {v0, p1}, Lbji;->a(Lbiy;)V

    .line 134
    :goto_1
    return-object v0

    .line 101
    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    .line 125
    :cond_5
    iget-object v0, p0, Lbjp;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lbjp;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    iget v2, p0, Lbjp;->c:I

    if-ne v0, v2, :cond_6

    move-object v0, v1

    .line 126
    goto :goto_1

    .line 130
    :cond_6
    iget-object v0, p0, Lbjp;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    iget-object v1, p0, Lbjp;->g:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    add-int/2addr v0, v1

    iget v1, p0, Lbjp;->c:I

    if-ne v0, v1, :cond_7

    .line 131
    iget-object v0, p0, Lbjp;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgk;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 134
    :cond_7
    iget-object v0, p0, Lbjp;->e:Lbjl;

    invoke-interface {p1}, Lbiy;->e()Landroid/media/MediaFormat;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbjl;->a(Landroid/media/MediaFormat;)Lbji;

    move-result-object v0

    invoke-virtual {v0, p1}, Lbji;->a(Lbiy;)V

    iget-object v1, p0, Lbjp;->g:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public a()V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lbjp;->f:Ljava/util/Queue;

    if-nez v0, :cond_0

    .line 174
    :goto_0
    return-void

    .line 165
    :cond_0
    iget-object v0, p0, Lbjp;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbji;

    .line 166
    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    goto :goto_1

    .line 168
    :cond_1
    iget-object v0, p0, Lbjp;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 170
    iget-object v0, p0, Lbjp;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbji;

    .line 171
    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    goto :goto_2

    .line 173
    :cond_2
    iget-object v0, p0, Lbjp;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    goto :goto_0
.end method

.method public a(Lbji;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbji",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 143
    if-nez p1, :cond_0

    .line 156
    :goto_0
    return-void

    .line 147
    :cond_0
    iget-boolean v0, p0, Lbjp;->d:Z

    if-eqz v0, :cond_1

    .line 148
    iget-object v0, p0, Lbjp;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 149
    iget-object v0, p0, Lbjp;->f:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 150
    invoke-virtual {p1}, Lbji;->d()V

    goto :goto_0

    .line 152
    :cond_1
    iget-object v0, p0, Lbjp;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 153
    invoke-static {p1}, Lcgl;->a(Lcgk;)V

    goto :goto_0
.end method
