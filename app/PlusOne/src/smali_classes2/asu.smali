.class public final Lasu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lasv;

.field public final b:Landroid/net/Uri;

.field public final c:Lbmu;


# direct methods
.method private constructor <init>(Lasv;Landroid/net/Uri;Lbmu;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 84
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 85
    const-string v0, "outcome"

    invoke-static {p1, v0, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lasv;

    iput-object v0, p0, Lasu;->a:Lasv;

    .line 86
    const-string v0, "uri"

    invoke-static {p2, v0, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lasu;->b:Landroid/net/Uri;

    .line 87
    invoke-virtual {p1}, Lasv;->a()Z

    move-result v3

    if-eqz p3, :cond_0

    move v0, v1

    :goto_0
    if-ne v3, v0, :cond_1

    move v0, v1

    :goto_1
    const-string v1, "invalid metadata for outcome"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 89
    iput-object p3, p0, Lasu;->c:Lbmu;

    .line 90
    return-void

    :cond_0
    move v0, v2

    .line 87
    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1
.end method

.method public static a(Lasv;Landroid/net/Uri;)Lasu;
    .locals 2

    .prologue
    .line 70
    new-instance v0, Lasu;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lasu;-><init>(Lasv;Landroid/net/Uri;Lbmu;)V

    return-object v0
.end method

.method public static a(Lasv;Landroid/net/Uri;Lbmu;)Lasu;
    .locals 2

    .prologue
    .line 80
    const-string v0, "metadata"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 81
    new-instance v0, Lasu;

    invoke-direct {v0, p0, p1, p2}, Lasu;-><init>(Lasv;Landroid/net/Uri;Lbmu;)V

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lasu;->a:Lasv;

    invoke-virtual {v0}, Lasv;->a()Z

    move-result v0

    return v0
.end method
