.class public final Lccj;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final l:Ljava/lang/String;


# instance fields
.field public final a:Z

.field public final b:I

.field public final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lccg;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Landroid/graphics/Rect;

.field public final e:Landroid/graphics/Rect;

.field public final f:Z

.field public g:Lcch;

.field public h:Landroid/widget/Adapter;

.field public i:I

.field public j:Z

.field public k:Z

.field private m:Landroid/animation/Animator;

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lccj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lccj;->l:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ZIZ)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-boolean p1, p0, Lccj;->a:Z

    .line 67
    iput p2, p0, Lccj;->b:I

    .line 68
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lccj;->c:Landroid/util/SparseArray;

    .line 69
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lccj;->d:Landroid/graphics/Rect;

    .line 70
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lccj;->e:Landroid/graphics/Rect;

    .line 71
    iput-boolean p3, p0, Lccj;->f:Z

    .line 72
    iput-boolean v1, p0, Lccj;->j:Z

    .line 73
    iput-boolean v1, p0, Lccj;->k:Z

    .line 74
    return-void
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lccj;->l:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lccj;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v0

    goto :goto_0
.end method

.method public a(J)Lccg;
    .locals 7

    .prologue
    .line 110
    const/4 v0, 0x0

    iget-object v1, p0, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 111
    iget-object v0, p0, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    .line 112
    iget-wide v4, v0, Lccg;->b:J

    cmp-long v3, v4, p1

    if-nez v3, :cond_0

    .line 116
    :goto_1
    return-object v0

    .line 110
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 116
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Landroid/animation/Animator;I)V
    .locals 2

    .prologue
    .line 185
    invoke-virtual {p0, p2}, Lccj;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 186
    sget-object v0, Lccj;->l:Ljava/lang/String;

    .line 189
    :cond_0
    iget-boolean v0, p0, Lccj;->f:Z

    if-eqz v0, :cond_1

    .line 190
    sget-object v0, Lccj;->l:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x29

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Playing animation. Priority = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 192
    :cond_1
    invoke-virtual {p0}, Lccj;->c()V

    .line 193
    iput-object p1, p0, Lccj;->m:Landroid/animation/Animator;

    .line 194
    iput p2, p0, Lccj;->n:I

    .line 195
    invoke-virtual {p1}, Landroid/animation/Animator;->start()V

    .line 196
    return-void
.end method

.method public a(Landroid/animation/Animator;ILjava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 207
    if-eqz p3, :cond_0

    .line 208
    new-instance v0, Lcck;

    invoke-direct {v0, p0, p3}, Lcck;-><init>(Lccj;Ljava/lang/Runnable;)V

    invoke-virtual {p1, v0}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 244
    :cond_0
    invoke-virtual {p0, p1, p2}, Lccj;->a(Landroid/animation/Animator;I)V

    .line 245
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lccj;->m:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lccj;->m:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 155
    invoke-virtual {p0}, Lccj;->b()I

    move-result v0

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 146
    invoke-virtual {p0}, Lccj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lccj;->n:I

    :goto_0
    return v0

    :cond_0
    const/high16 v0, -0x80000000

    goto :goto_0
.end method

.method public b(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lccj;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    goto :goto_0
.end method

.method public c(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Lccj;->a:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 162
    invoke-virtual {p0}, Lccj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lccj;->m:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->end()V

    .line 165
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lccj;->m:Landroid/animation/Animator;

    .line 166
    return-void
.end method

.method public d(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 124
    const-string v0, "view"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 125
    const/4 v0, 0x0

    iget-object v1, p0, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 126
    iget-object v0, p0, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    iget-object v0, v0, Lccg;->a:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 127
    iget-object v0, p0, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    .line 130
    :goto_1
    return v0

    .line 125
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 130
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public d()V
    .locals 1

    .prologue
    .line 172
    invoke-virtual {p0}, Lccj;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Lccj;->m:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 175
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lccj;->m:Landroid/animation/Animator;

    .line 176
    return-void
.end method
