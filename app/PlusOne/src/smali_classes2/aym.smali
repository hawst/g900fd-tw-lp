.class public final Laym;
.super Lazi;
.source "PG"


# instance fields
.field private a:Lbaf;

.field private b:Landroid/animation/TimeInterpolator;


# direct methods
.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v1, 0x3

    const-wide/32 v6, 0xf4240

    .line 27
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    new-array v1, v1, [Layj;

    const/4 v2, 0x0

    new-instance v3, Layg;

    const v4, 0x7f020106

    const v5, 0x7f020107

    invoke-direct {v3, v4, v5, v6, v7}, Layg;-><init>(IIJ)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Layg;

    const v4, 0x7f020108

    const v5, 0x7f020109

    invoke-direct {v3, v4, v5, v6, v7}, Layg;-><init>(IIJ)V

    aput-object v3, v1, v2

    const/4 v2, 0x2

    new-instance v3, Layg;

    const v4, 0x7f02010a

    const v5, 0x7f02010b

    invoke-direct {v3, v4, v5, v6, v7}, Layg;-><init>(IIJ)V

    aput-object v3, v1, v2

    invoke-direct {p0, v0, v1}, Lazi;-><init>([I[Layj;)V

    .line 22
    new-instance v0, Lbak;

    const-wide/32 v2, 0xc3500

    invoke-direct {v0, v2, v3}, Lbak;-><init>(J)V

    iput-object v0, p0, Laym;->a:Lbaf;

    .line 24
    new-instance v0, Lbai;

    invoke-direct {v0}, Lbai;-><init>()V

    iput-object v0, p0, Laym;->b:Landroid/animation/TimeInterpolator;

    .line 34
    return-void

    .line 27
    :array_0
    .array-data 4
        0x1
        0x2
        0x3
    .end array-data
.end method


# virtual methods
.method public a(Lbmg;Lbmg;)J
    .locals 2

    .prologue
    .line 43
    sget-object v0, Lbmg;->c:Lbmg;

    if-ne p1, v0, :cond_0

    sget-object v0, Lbmg;->c:Lbmg;

    if-ne p2, v0, :cond_0

    .line 44
    const-wide/32 v0, 0x493e0

    .line 46
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method protected a(Layj;Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/high16 v3, 0x42100000    # 36.0f

    .line 63
    new-instance v0, Layn;

    new-instance v1, Layu;

    invoke-direct {v1}, Layu;-><init>()V

    .line 64
    invoke-virtual {v1, p1}, Layu;->a(Layj;)Layu;

    move-result-object v1

    const/16 v2, 0x53

    .line 65
    invoke-virtual {v1, v2}, Layu;->b(I)Layu;

    move-result-object v1

    const v2, 0x7f0b00a0

    .line 66
    invoke-virtual {p3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Layu;->a(I)Layu;

    move-result-object v1

    .line 67
    invoke-virtual {v1, v3}, Layu;->a(F)Layu;

    move-result-object v1

    .line 68
    invoke-virtual {v1, v3}, Layu;->b(F)Layu;

    move-result-object v1

    const-string v2, "fonts/Roboto-Black.ttf"

    .line 69
    invoke-static {p2, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Layu;->a(Landroid/graphics/Typeface;)Layu;

    move-result-object v1

    .line 70
    invoke-virtual {v1, v4}, Layu;->a(Z)Layu;

    move-result-object v1

    .line 71
    invoke-virtual {v1, v4}, Layu;->b(Z)Layu;

    move-result-object v1

    sget-object v2, Layc;->d:Layc;

    .line 72
    invoke-virtual {v1, v2}, Layu;->a(Layc;)Layu;

    move-result-object v1

    invoke-direct {v0, v1}, Layn;-><init>(Layu;)V

    return-object v0
.end method

.method public e(Layl;)I
    .locals 1

    .prologue
    .line 38
    const v0, 0x7f020423

    return v0
.end method

.method protected f()Layj;
    .locals 4

    .prologue
    .line 56
    new-instance v0, Lazt;

    const/4 v1, 0x1

    .line 57
    invoke-virtual {p0, v1}, Laym;->a(I)Layj;

    move-result-object v1

    sget-object v2, Lazv;->d:Lazv;

    const v3, 0x3f666666    # 0.9f

    invoke-direct {v0, v1, v2, v3}, Lazt;-><init>(Layj;Lazv;F)V

    return-object v0
.end method

.method public g_()Layc;
    .locals 1

    .prologue
    .line 51
    sget-object v0, Layc;->e:Layc;

    return-object v0
.end method

.method public v(Layl;)F
    .locals 2

    .prologue
    .line 101
    iget-boolean v0, p1, Layl;->w:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Laym;->a:Lbaf;

    invoke-virtual {v0, p1}, Lbaf;->a(Layl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Laym;->b:Landroid/animation/TimeInterpolator;

    iget-object v1, p0, Laym;->a:Lbaf;

    invoke-virtual {v1, p1}, Lbaf;->b(Layl;)F

    move-result v1

    invoke-interface {v0, v1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    .line 105
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lazi;->v(Layl;)F

    move-result v0

    goto :goto_0
.end method
