.class public final Lckj;
.super Lckg;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private synthetic d:Lcjy;


# direct methods
.method public constructor <init>(Lcjy;Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;ILandroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 879
    iput-object p1, p0, Lckj;->d:Lcjy;

    invoke-direct {p0}, Lckg;-><init>()V

    .line 880
    invoke-virtual {p2, p3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 881
    invoke-virtual {p2, p3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v0, v1, v0

    add-int/lit8 v0, v0, 0x1

    .line 883
    iput p3, p0, Lckj;->a:I

    .line 885
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lckj;->b:Ljava/util/List;

    .line 886
    :goto_0
    add-int/lit8 v1, v0, -0x1

    if-lez v0, :cond_0

    .line 887
    iget-object v0, p0, Lckj;->b:Ljava/util/List;

    invoke-interface {v0, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move v0, v1

    goto :goto_0

    .line 889
    :cond_0
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 902
    iget-object v0, p0, Lckj;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 892
    iput p1, p0, Lckj;->c:I

    .line 893
    return-void
.end method

.method public a(Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 916
    if-nez p1, :cond_0

    .line 917
    iget-object v0, p0, Lckj;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    .line 927
    :goto_0
    return v0

    .line 920
    :cond_0
    iget-object v0, p0, Lckj;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 921
    const/4 v0, 0x0

    goto :goto_0

    .line 924
    :cond_1
    iget-object v0, p0, Lckj;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 925
    iget-object v0, p0, Lckj;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 927
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Ljava/lang/Integer;)[Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 932
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lckj;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 933
    :cond_0
    const/4 v0, 0x0

    .line 947
    :goto_0
    return-object v0

    .line 936
    :cond_1
    iget-object v0, p0, Lckj;->d:Lcjy;

    .line 937
    invoke-virtual {v0}, Lcjy;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0048

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v6, v0

    .line 938
    iget-object v0, p0, Lckj;->d:Lcjy;

    invoke-virtual {v0}, Lcjy;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0038

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    .line 939
    iget-object v0, p0, Lckj;->b:Ljava/util/List;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 941
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    move v2, v1

    move v4, v3

    .line 942
    invoke-static/range {v0 .. v5}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->createCenterCropBitmap(Landroid/graphics/Bitmap;IIIII)Landroid/graphics/Bitmap;

    move-result-object v7

    move v2, v1

    move v4, v6

    .line 944
    invoke-static/range {v0 .. v5}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->createCenterCropBitmap(Landroid/graphics/Bitmap;IIIII)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 947
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/Bitmap;

    aput-object v7, v0, v3

    const/4 v2, 0x1

    aput-object v1, v0, v2

    goto :goto_0
.end method

.method public b(I)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 907
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Integer;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 912
    iget-object v0, p0, Lckj;->d:Lcjy;

    iget v1, p0, Lckj;->a:I

    invoke-virtual {v0, v1, p1}, Lcjy;->b(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/Integer;)Z
    .locals 1

    .prologue
    .line 897
    iget v0, p0, Lckj;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
