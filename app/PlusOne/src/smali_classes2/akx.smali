.class public final Lakx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lajw;


# static fields
.field private static final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 38
    const-class v0, Lakx;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lakx;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/util/LongSparseArray;J)Landroid/util/LongSparseArray;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;J)",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 185
    new-instance v3, Lbxd;

    invoke-direct {v3}, Lbxd;-><init>()V

    .line 186
    new-instance v4, Landroid/util/LongSparseArray;

    invoke-direct {v4}, Landroid/util/LongSparseArray;-><init>()V

    .line 188
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {p1}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 189
    invoke-virtual {p1, v2}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmn;

    .line 190
    invoke-virtual {p1, v2}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v6

    move v1, v2

    .line 193
    :goto_1
    invoke-virtual {p1}, Landroid/util/LongSparseArray;->size()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 194
    invoke-virtual {p1, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v8

    add-long v10, v6, p2

    cmp-long v5, v8, v10

    if-gez v5, :cond_0

    .line 195
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 197
    :cond_0
    invoke-virtual {p1}, Landroid/util/LongSparseArray;->size()I

    move-result v5

    if-eq v1, v5, :cond_1

    .line 198
    invoke-virtual {p1, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmn;

    .line 202
    invoke-virtual {v3, v0, v1}, Lbxd;->b(Lbmn;Lbmn;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 203
    invoke-virtual {v3, v0, v1}, Lbxd;->a(Lbmn;Lbmn;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 204
    invoke-virtual {v4, v6, v7, v0}, Landroid/util/LongSparseArray;->append(JLjava/lang/Object;)V

    .line 188
    :cond_1
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 206
    :cond_2
    sget-object v0, Lakx;->b:Ljava/lang/String;

    const-string v1, "ChromaHistogram dimensions are mismatched."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 209
    :cond_3
    return-object v4
.end method

.method private a(Ljava/util/ArrayList;)Lbmn;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lbmn;",
            ">;)",
            "Lbmn;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 148
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const-string v2, "matrices.size()"

    invoke-static {v0, v2}, Lcec;->a(ILjava/lang/CharSequence;)I

    .line 150
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmn;

    .line 151
    new-instance v3, Lbmn;

    .line 152
    invoke-virtual {v0}, Lbmn;->c()I

    move-result v2

    invoke-virtual {v0}, Lbmn;->d()I

    move-result v4

    invoke-direct {v3, v2, v4}, Lbmn;-><init>(II)V

    .line 153
    invoke-virtual {v0}, Lbmn;->b()I

    move-result v4

    .line 155
    new-array v5, v4, [F

    .line 156
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmn;

    move v2, v1

    .line 157
    :goto_0
    if-ge v2, v4, :cond_0

    .line 158
    invoke-virtual {v0, v2}, Lbmn;->a(I)F

    move-result v7

    .line 159
    aget v8, v5, v2

    add-float/2addr v7, v8

    aput v7, v5, v2

    .line 157
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 163
    :cond_1
    const/4 v0, 0x0

    move v2, v0

    move v0, v1

    .line 164
    :goto_1
    if-ge v0, v4, :cond_2

    .line 165
    aget v6, v5, v0

    add-float/2addr v2, v6

    .line 164
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 167
    :goto_2
    if-ge v0, v4, :cond_3

    .line 168
    aget v1, v5, v0

    div-float/2addr v1, v2

    invoke-virtual {v3, v0, v1}, Lbmn;->a(IF)V

    .line 167
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 171
    :cond_3
    return-object v3
.end method

.method private a(Landroid/util/LongSparseArray;JJJJ)Ljava/util/ArrayList;
    .locals 20
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Float;",
            ">;JJJJ)",
            "Ljava/util/ArrayList",
            "<",
            "Lbmr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 271
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 273
    invoke-virtual/range {p0 .. p5}, Lakx;->a(Landroid/util/LongSparseArray;JJ)Ljava/util/ArrayList;

    move-result-object v14

    .line 275
    invoke-virtual {v14}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_1

    .line 276
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    add-long v6, v4, p6

    .line 279
    invoke-virtual/range {p1 .. p1}, Landroid/util/LongSparseArray;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    sub-long v16, v4, p6

    .line 280
    const/4 v4, 0x0

    move v5, v4

    .line 282
    :goto_0
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v5, v4, :cond_2

    .line 283
    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    cmp-long v4, v8, v6

    if-gez v4, :cond_2

    .line 284
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    goto :goto_0

    .line 286
    :goto_1
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v12, v4, :cond_1

    .line 287
    invoke-virtual {v14, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    cmp-long v4, v4, v16

    if-gez v4, :cond_1

    .line 288
    invoke-virtual {v14, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    .line 289
    move-object/from16 v0, p1

    move-wide/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->indexOfKey(J)I

    move-result v6

    .line 291
    const/4 v7, 0x0

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-wide/from16 v8, p6

    move-wide/from16 v10, p8

    invoke-direct/range {v4 .. v11}, Lakx;->a(Landroid/util/LongSparseArray;IZJJ)Z

    move-result v15

    .line 294
    const/4 v7, 0x1

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-wide/from16 v8, p6

    move-wide/from16 v10, p8

    invoke-direct/range {v4 .. v11}, Lakx;->a(Landroid/util/LongSparseArray;IZJJ)Z

    move-result v4

    .line 300
    if-eqz v15, :cond_0

    if-eqz v4, :cond_0

    .line 302
    sub-long v4, v18, p4

    const-wide/16 v6, 0x2

    div-long v6, p6, v6

    add-long/2addr v4, v6

    .line 303
    add-long v6, v18, p4

    const-wide/16 v8, 0x2

    div-long v8, p6, v8

    add-long/2addr v6, v8

    .line 305
    new-instance v8, Lbmr;

    new-instance v9, Lbmp;

    invoke-direct {v9, v4, v5, v6, v7}, Lbmp;-><init>(JJ)V

    const/4 v4, 0x3

    invoke-direct {v8, v9, v4}, Lbmr;-><init>(Lbmp;I)V

    invoke-virtual {v13, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 308
    :cond_0
    add-int/lit8 v5, v12, 0x1

    move v12, v5

    .line 309
    goto :goto_1

    .line 312
    :cond_1
    return-object v13

    :cond_2
    move v12, v5

    goto :goto_1
.end method

.method private a(Landroid/util/LongSparseArray;IZJJ)Z
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Float;",
            ">;IZJJ)Z"
        }
    .end annotation

    .prologue
    .line 331
    invoke-virtual {p1, p2}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v5

    .line 332
    invoke-virtual {p1, p2}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v6

    .line 334
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v0

    add-long v0, v0, p6

    .line 336
    invoke-virtual {p1}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {p1, v2}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v2

    sub-long v2, v2, p6

    .line 338
    if-nez p3, :cond_0

    cmp-long v0, v6, v0

    if-gtz v0, :cond_1

    :cond_0
    if-eqz p3, :cond_5

    cmp-long v0, v6, v2

    if-gez v0, :cond_5

    .line 339
    :cond_1
    if-eqz p3, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 341
    :goto_0
    const/4 v3, 0x0

    .line 342
    const/4 v2, 0x0

    .line 343
    const/4 v0, 0x0

    move v4, v3

    move v3, v2

    move v2, v0

    .line 346
    :goto_1
    invoke-virtual {p1, p2}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v8

    sub-long/2addr v8, v6

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    .line 347
    cmp-long v0, v8, p6

    if-gtz v0, :cond_3

    .line 348
    cmp-long v0, v8, p4

    if-lez v0, :cond_6

    .line 354
    invoke-virtual {p1, p2}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 355
    add-float/2addr v4, v0

    .line 356
    mul-float/2addr v0, v0

    add-float/2addr v3, v0

    .line 357
    add-int/lit8 v0, v2, 0x1

    move v2, v3

    move v3, v4

    .line 359
    :goto_2
    add-int/2addr p2, v1

    move v4, v3

    move v3, v2

    move v2, v0

    .line 360
    goto :goto_1

    .line 339
    :cond_2
    const/4 v0, -0x1

    move v1, v0

    goto :goto_0

    .line 362
    :cond_3
    if-lez v2, :cond_5

    .line 363
    int-to-float v0, v2

    div-float v0, v4, v0

    .line 364
    int-to-float v1, v2

    div-float v1, v3, v1

    mul-float v2, v0, v0

    sub-float/2addr v1, v2

    .line 365
    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v1, v2

    .line 366
    sub-float v0, v5, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v2, 0x40400000    # 3.0f

    mul-float/2addr v1, v2

    cmpl-float v0, v0, v1

    if-lez v0, :cond_4

    const/4 v0, 0x1

    .line 369
    :goto_3
    return v0

    .line 366
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 369
    :cond_5
    const/4 v0, 0x0

    goto :goto_3

    :cond_6
    move v0, v2

    move v2, v3

    move v3, v4

    goto :goto_2
.end method

.method private b(Landroid/util/LongSparseArray;J)Landroid/util/LongSparseArray;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Float;",
            ">;J)",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 221
    new-instance v6, Landroid/util/LongSparseArray;

    invoke-direct {v6}, Landroid/util/LongSparseArray;-><init>()V

    move v1, v0

    move v2, v3

    move v4, v0

    .line 227
    :goto_0
    invoke-virtual {p1}, Landroid/util/LongSparseArray;->size()I

    move-result v5

    if-ge v1, v5, :cond_3

    .line 228
    invoke-virtual {p1, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v8

    move v5, v4

    move v4, v2

    move v2, v0

    .line 230
    :goto_1
    invoke-virtual {p1}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 231
    invoke-virtual {p1, v2}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v10

    sub-long/2addr v10, v8

    cmp-long v0, v10, p2

    if-gez v0, :cond_0

    .line 232
    invoke-virtual {p1, v2}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    add-float/2addr v0, v4

    .line 233
    add-int/lit8 v2, v2, 0x1

    .line 234
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move v4, v0

    goto :goto_1

    .line 236
    :cond_0
    if-lez v1, :cond_1

    .line 237
    add-int/lit8 v0, v1, -0x1

    invoke-virtual {p1, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sub-float/2addr v4, v0

    .line 238
    add-int/lit8 v5, v5, -0x1

    .line 241
    :cond_1
    invoke-virtual {p1, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v8

    .line 242
    if-lez v5, :cond_2

    int-to-float v0, v5

    div-float v0, v4, v0

    .line 243
    :goto_2
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v6, v8, v9, v0}, Landroid/util/LongSparseArray;->append(JLjava/lang/Object;)V

    .line 244
    invoke-virtual {p1}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-eq v2, v0, :cond_3

    .line 245
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    move v2, v4

    move v4, v5

    goto :goto_0

    :cond_2
    move v0, v3

    .line 242
    goto :goto_2

    .line 248
    :cond_3
    return-object v6
.end method


# virtual methods
.method a(Landroid/util/LongSparseArray;JJ)Ljava/util/ArrayList;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Float;",
            ">;JJ)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 384
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 385
    new-instance v6, Ljava/util/ArrayDeque;

    invoke-direct {v6}, Ljava/util/ArrayDeque;-><init>()V

    .line 388
    const/4 v2, 0x0

    move v4, v2

    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    if-ge v4, v2, :cond_3

    .line 389
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v8

    .line 390
    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 393
    :goto_1
    invoke-virtual {v6}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 394
    invoke-virtual {v6}, Ljava/util/ArrayDeque;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 395
    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    sub-long v12, v8, p2

    cmp-long v2, v10, v12

    if-gez v2, :cond_0

    .line 396
    invoke-virtual {v6}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    goto :goto_1

    .line 403
    :cond_0
    :goto_2
    invoke-virtual {v6}, Ljava/util/ArrayDeque;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 404
    invoke-virtual {v6}, Ljava/util/ArrayDeque;->getLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 405
    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    cmpg-float v2, v2, v3

    if-gez v2, :cond_1

    .line 406
    invoke-virtual {v6}, Ljava/util/ArrayDeque;->removeLast()Ljava/lang/Object;

    goto :goto_2

    .line 411
    :cond_1
    new-instance v2, Landroid/util/Pair;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-direct {v2, v7, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v6, v2}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    .line 415
    invoke-virtual {v6}, Ljava/util/ArrayDeque;->getFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    .line 416
    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 417
    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 418
    add-long v12, p2, p4

    const-wide/16 v14, 0x2

    div-long/2addr v12, v14

    sub-long v12, v8, v12

    .line 419
    sub-long v14, p2, p4

    const-wide/16 v16, 0x2

    div-long v14, v14, v16

    sub-long/2addr v8, v14

    .line 420
    cmp-long v3, v10, v12

    if-ltz v3, :cond_2

    cmp-long v3, v10, v8

    if-gez v3, :cond_2

    const v3, 0x3e19999a    # 0.15f

    cmpl-float v2, v2, v3

    if-lez v2, :cond_2

    .line 422
    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 388
    :cond_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_0

    .line 425
    :cond_3
    return-object v5
.end method

.method public a(Lbkn;Lbku;Lbkq;)V
    .locals 16

    .prologue
    .line 81
    sget-object v2, Lbmz;->m:Lbmz;

    .line 82
    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lbku;->b(Lbmz;)Landroid/util/LongSparseArray;

    move-result-object v8

    .line 83
    invoke-virtual {v8}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 103
    :goto_0
    return-void

    .line 88
    :cond_0
    new-instance v9, Landroid/util/LongSparseArray;

    invoke-direct {v9}, Landroid/util/LongSparseArray;-><init>()V

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    invoke-virtual {v8, v2}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    invoke-virtual {v8}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    if-ge v3, v2, :cond_3

    invoke-virtual {v8, v3}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v6

    sub-long v12, v6, v4

    const-wide/32 v14, 0x2e630

    cmp-long v2, v12, v14

    if-lez v2, :cond_1

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lakx;->a(Ljava/util/ArrayList;)Lbmn;

    move-result-object v2

    invoke-virtual {v9, v4, v5, v2}, Landroid/util/LongSparseArray;->append(JLjava/lang/Object;)V

    invoke-virtual {v10}, Ljava/util/ArrayList;->clear()V

    :goto_2
    move-wide v4, v6

    :cond_1
    invoke-virtual {v8, v3}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbmn;

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    :cond_2
    sget-object v2, Lakx;->b:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    const/16 v12, 0x4f

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v12, "averaged interval from "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " to "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " has no data"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 90
    :cond_3
    const-wide/32 v2, 0x10c8e0

    .line 91
    move-object/from16 v0, p0

    invoke-direct {v0, v9, v2, v3}, Lakx;->a(Landroid/util/LongSparseArray;J)Landroid/util/LongSparseArray;

    move-result-object v2

    .line 93
    const-wide/32 v4, 0x100590

    .line 94
    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4, v5}, Lakx;->b(Landroid/util/LongSparseArray;J)Landroid/util/LongSparseArray;

    move-result-object v3

    .line 96
    const-wide/32 v4, 0x1e8480

    const-wide/32 v6, 0x2e630

    const-wide/32 v8, 0x10c8e0

    const-wide/32 v10, 0x2dc6c0

    move-object/from16 v2, p0

    .line 97
    invoke-direct/range {v2 .. v11}, Lakx;->a(Landroid/util/LongSparseArray;JJJJ)Ljava/util/ArrayList;

    move-result-object v2

    .line 101
    const/4 v3, 0x3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3, v2}, Lbkq;->b(ILjava/util/List;)Lbkq;

    goto/16 :goto_0
.end method
