.class final Lanr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lane;


# instance fields
.field private synthetic a:Lanj;


# direct methods
.method constructor <init>(Lanj;)V
    .locals 0

    .prologue
    .line 345
    iput-object p1, p0, Lanr;->a:Lanj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 381
    iget-object v0, p0, Lanr;->a:Lanj;

    invoke-static {v0}, Lanj;->f(Lanj;)Lann;

    move-result-object v0

    sget-object v1, Lanm;->b:Lanm;

    invoke-interface {v0, v1}, Lann;->a(Lanm;)V

    .line 382
    return-void
.end method

.method public a(Lana;Lamz;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 349
    iget-object v0, p0, Lanr;->a:Lanj;

    invoke-virtual {v0}, Lanj;->e()V

    .line 354
    const-string v0, "task"

    iget-object v1, p0, Lanr;->a:Lanj;

    invoke-static {v1}, Lanj;->a(Lanj;)Landroid/os/AsyncTask;

    move-result-object v1

    invoke-static {p1, v0, v1, v5}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/lang/CharSequence;)V

    .line 355
    iget-object v0, p0, Lanr;->a:Lanj;

    invoke-static {v0, v5}, Lanj;->a(Lanj;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    .line 357
    iget-boolean v0, p2, Lamz;->a:Z

    if-eqz v0, :cond_1

    iget-object v0, p2, Lamz;->b:Ljava/util/Map;

    iget-object v1, p0, Lanr;->a:Lanj;

    .line 358
    invoke-static {v1}, Lanj;->b(Lanj;)Lavz;

    move-result-object v1

    iget-object v1, v1, Lavz;->a:Landroid/net/Uri;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 360
    iget-object v0, p2, Lamz;->b:Ljava/util/Map;

    iget-object v1, p0, Lanr;->a:Lanj;

    invoke-static {v1}, Lanj;->b(Lanj;)Lavz;

    move-result-object v1

    iget-object v1, v1, Lavz;->a:Landroid/net/Uri;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 362
    iget-object v1, p0, Lanr;->a:Lanj;

    iget-object v2, p0, Lanr;->a:Lanj;

    invoke-static {v2}, Lanj;->b(Lanj;)Lavz;

    move-result-object v2

    invoke-virtual {v2, v0}, Lavz;->c(Landroid/net/Uri;)Lavz;

    move-result-object v2

    invoke-static {v1, v2}, Lanj;->a(Lanj;Lavz;)Lavz;

    .line 363
    iget-object v1, p0, Lanr;->a:Lanj;

    invoke-static {v1, v0}, Lanj;->a(Lanj;Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lanr;->a:Lanj;

    invoke-static {v0}, Lanj;->c(Lanj;)V

    .line 365
    iget-object v0, p0, Lanr;->a:Lanj;

    invoke-static {v0}, Lanj;->d(Lanj;)Lawb;

    move-result-object v0

    invoke-interface {v0}, Lawb;->aM()V

    .line 366
    iget-object v0, p0, Lanr;->a:Lanj;

    invoke-static {v0, v5}, Lanj;->a(Lanj;Lavz;)Lavz;

    .line 377
    :goto_0
    return-void

    .line 369
    :cond_0
    iget-object v0, p0, Lanr;->a:Lanj;

    invoke-static {v0}, Lanj;->e(Lanj;)V

    goto :goto_0

    .line 371
    :cond_1
    invoke-static {}, Lanj;->d()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Failed to download %s. Result: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lanr;->a:Lanj;

    invoke-static {v4}, Lanj;->b(Lanj;)Lavz;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 372
    iget-object v0, p0, Lanr;->a:Lanj;

    invoke-static {v0}, Lanj;->d(Lanj;)Lawb;

    move-result-object v0

    invoke-interface {v0}, Lawb;->aM()V

    .line 373
    iget-object v0, p0, Lanr;->a:Lanj;

    invoke-static {v0, v5}, Lanj;->a(Lanj;Lavz;)Lavz;

    .line 374
    iget-object v0, p0, Lanr;->a:Lanj;

    invoke-static {v0}, Lanj;->c(Lanj;)V

    goto :goto_0
.end method
