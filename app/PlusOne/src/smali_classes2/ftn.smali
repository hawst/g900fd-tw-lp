.class public final Lftn;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lljh;


# instance fields
.field private a:Landroid/graphics/Path;

.field private b:Landroid/graphics/Path;

.field private c:Landroid/graphics/Paint;

.field private d:Landroid/graphics/Paint;

.field private e:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lftn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lftn;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lftn;->a:Landroid/graphics/Path;

    .line 27
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lftn;->b:Landroid/graphics/Path;

    .line 28
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lftn;->c:Landroid/graphics/Paint;

    .line 29
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lftn;->d:Landroid/graphics/Paint;

    .line 30
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lftn;->e:Landroid/graphics/Paint;

    .line 43
    iget-object v0, p0, Lftn;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 44
    iget-object v0, p0, Lftn;->c:Landroid/graphics/Paint;

    .line 45
    invoke-virtual {p0}, Lftn;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d00cf

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 44
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 46
    iget-object v0, p0, Lftn;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 47
    iget-object v0, p0, Lftn;->d:Landroid/graphics/Paint;

    .line 48
    invoke-virtual {p0}, Lftn;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d00d0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 47
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 49
    iget-object v0, p0, Lftn;->e:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 50
    return-void
.end method

.method private a(FFF)Landroid/graphics/PointF;
    .locals 10

    .prologue
    .line 84
    const/high16 v0, 0x43340000    # 180.0f

    add-float/2addr v0, p3

    .line 85
    new-instance v1, Landroid/graphics/PointF;

    float-to-double v2, p1

    neg-float v4, v0

    float-to-double v4, v4

    .line 86
    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    float-to-double v6, p2

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-float v2, v2

    float-to-double v4, p1

    neg-float v0, v0

    float-to-double v6, v0

    .line 87
    invoke-static {v6, v7}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->cos(D)D

    move-result-wide v6

    float-to-double v8, p2

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-float v0, v4

    invoke-direct {v1, v2, v0}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v1
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 99
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lftn;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 54
    iget-object v0, p0, Lftn;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 55
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lftn;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 59
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lftn;->a:Landroid/graphics/Path;

    iget-object v1, p0, Lftn;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 93
    iget-object v0, p0, Lftn;->a:Landroid/graphics/Path;

    iget-object v1, p0, Lftn;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 94
    iget-object v0, p0, Lftn;->b:Landroid/graphics/Path;

    iget-object v1, p0, Lftn;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 95
    return-void
.end method

.method public onMeasure(II)V
    .locals 5

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 63
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 65
    invoke-virtual {p0}, Lftn;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lftn;->getMeasuredHeight()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    .line 66
    iget-object v1, p0, Lftn;->c:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    div-float/2addr v1, v2

    sub-float v1, v0, v1

    .line 67
    iget-object v2, p0, Lftn;->a:Landroid/graphics/Path;

    sget-object v3, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v2, v0, v0, v1, v3}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 70
    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v2, v1

    const/high16 v3, 0x42b40000    # 90.0f

    invoke-direct {p0, v0, v2, v3}, Lftn;->a(FFF)Landroid/graphics/PointF;

    move-result-object v2

    .line 76
    const v3, 0x3f3d70a4    # 0.74f

    mul-float/2addr v1, v3

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v3}, Lftn;->a(FFF)Landroid/graphics/PointF;

    move-result-object v1

    .line 78
    iget-object v3, p0, Lftn;->b:Landroid/graphics/Path;

    iget v4, v1, Landroid/graphics/PointF;->x:F

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {v3, v4, v1}, Landroid/graphics/Path;->moveTo(FF)V

    .line 79
    iget-object v1, p0, Lftn;->b:Landroid/graphics/Path;

    invoke-virtual {v1, v0, v0}, Landroid/graphics/Path;->lineTo(FF)V

    .line 80
    iget-object v0, p0, Lftn;->b:Landroid/graphics/Path;

    iget v1, v2, Landroid/graphics/PointF;->x:F

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 81
    return-void
.end method
