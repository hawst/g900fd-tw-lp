.class public final Ladd;
.super Ladl;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/BlockingDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingDeque",
            "<",
            "Ladc;",
            ">;"
        }
    .end annotation
.end field

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(ILandroid/media/MediaFormat;Ladm;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0, p1, p2, p3}, Ladl;-><init>(ILandroid/media/MediaFormat;Ladm;)V

    .line 45
    invoke-static {p2}, Ladg;->a(Landroid/media/MediaFormat;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 46
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "AudioTrackDecoder can only be used with audio formats"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    new-instance v0, Ljava/util/concurrent/LinkedBlockingDeque;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingDeque;-><init>()V

    iput-object v0, p0, Ladd;->a:Ljava/util/concurrent/BlockingDeque;

    .line 51
    const-string v0, "sample-rate"

    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ladd;->b:I

    .line 52
    const-string v0, "channel-count"

    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ladd;->c:I

    .line 53
    return-void
.end method


# virtual methods
.method protected a(Landroid/media/MediaFormat;)Landroid/media/MediaCodec;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 57
    const-string v0, "mime"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/media/MediaCodec;->createDecoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v0

    .line 59
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v2, v2, v1}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 60
    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Ladd;->a:Ljava/util/concurrent/BlockingDeque;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingDeque;->clear()V

    .line 96
    return-void
.end method

.method public a(Labg;)V
    .locals 4

    .prologue
    .line 86
    iget-object v0, p0, Ladd;->a:Ljava/util/concurrent/BlockingDeque;

    invoke-interface {v0}, Ljava/util/concurrent/BlockingDeque;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladc;

    .line 87
    invoke-virtual {p1, v0}, Labg;->a(Ljava/lang/Object;)V

    .line 88
    iget-wide v0, v0, Ladc;->d:J

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    invoke-virtual {p1, v0, v1}, Labg;->a(J)V

    .line 89
    return-void
.end method

.method protected a(Landroid/media/MediaCodec;[Ljava/nio/ByteBuffer;ILandroid/media/MediaCodec$BufferInfo;Z)Z
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 67
    aget-object v6, p2, p3

    .line 68
    iget v0, p4, Landroid/media/MediaCodec$BufferInfo;->size:I

    new-array v3, v0, [B

    .line 69
    iget v0, p4, Landroid/media/MediaCodec$BufferInfo;->offset:I

    invoke-virtual {v6, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 70
    iget v0, p4, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-virtual {v6, v3, v8, v0}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 72
    iget-object v7, p0, Ladd;->a:Ljava/util/concurrent/BlockingDeque;

    new-instance v0, Ladc;

    iget v1, p0, Ladd;->b:I

    iget v2, p0, Ladd;->c:I

    iget-wide v4, p4, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    invoke-direct/range {v0 .. v5}, Ladc;-><init>(II[BJ)V

    invoke-interface {v7, v0}, Ljava/util/concurrent/BlockingDeque;->offerLast(Ljava/lang/Object;)Z

    .line 75
    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 76
    invoke-virtual {p1, p3, v8}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 77
    invoke-virtual {p0}, Ladd;->g()V

    .line 78
    const/4 v0, 0x1

    return v0
.end method

.method public bridge synthetic a(Landroid/media/MediaExtractor;)Z
    .locals 1

    .prologue
    .line 34
    invoke-super {p0, p1}, Ladl;->a(Landroid/media/MediaExtractor;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic b()Z
    .locals 1

    .prologue
    .line 34
    invoke-super {p0}, Ladl;->b()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic c()V
    .locals 0

    .prologue
    .line 34
    invoke-super {p0}, Ladl;->c()V

    return-void
.end method

.method public bridge synthetic d()V
    .locals 0

    .prologue
    .line 34
    invoke-super {p0}, Ladl;->d()V

    return-void
.end method

.method public bridge synthetic e()V
    .locals 0

    .prologue
    .line 34
    invoke-super {p0}, Ladl;->e()V

    return-void
.end method
