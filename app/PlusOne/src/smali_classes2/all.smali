.class public final Lall;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:I

.field f:I

.field g:I

.field final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmu;",
            ">;"
        }
    .end annotation
.end field

.field final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field final j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lasv;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lall;->h:Ljava/util/List;

    .line 62
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lall;->i:Ljava/util/List;

    .line 63
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lall;->j:Ljava/util/List;

    .line 108
    return-void
.end method


# virtual methods
.method public a()Lalk;
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lalk;

    invoke-direct {v0, p0}, Lalk;-><init>(Lall;)V

    return-object v0
.end method

.method public a(I)Lall;
    .locals 0

    .prologue
    .line 71
    iput p1, p0, Lall;->a:I

    .line 72
    return-object p0
.end method

.method public a(Lasu;)Lall;
    .locals 5

    .prologue
    .line 76
    iget-object v0, p0, Lall;->j:Ljava/util/List;

    iget-object v1, p1, Lasu;->a:Lasv;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 77
    invoke-virtual {p1}, Lasu;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 78
    iget-object v0, p0, Lall;->i:Ljava/util/List;

    iget-object v1, p1, Lasu;->b:Landroid/net/Uri;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    iget-object v0, p0, Lall;->h:Ljava/util/List;

    iget-object v1, p1, Lasu;->c:Lbmu;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    :goto_0
    iget-object v0, p1, Lasu;->a:Lasv;

    .line 86
    invoke-virtual {v0}, Lasv;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 87
    iget v1, p0, Lall;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lall;->c:I

    .line 88
    invoke-virtual {v0}, Lasv;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 89
    iget v1, p0, Lall;->e:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lall;->e:I

    .line 92
    :cond_0
    invoke-virtual {v0}, Lasv;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 93
    iget v1, p0, Lall;->d:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lall;->d:I

    .line 94
    invoke-virtual {v0}, Lasv;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 95
    iget v1, p0, Lall;->g:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lall;->g:I

    .line 98
    :cond_1
    sget-object v1, Lasv;->b:Lasv;

    if-ne v0, v1, :cond_2

    .line 99
    iget v1, p0, Lall;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lall;->b:I

    .line 101
    :cond_2
    sget-object v1, Lasv;->d:Lasv;

    if-ne v0, v1, :cond_3

    .line 102
    iget v0, p0, Lall;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lall;->f:I

    .line 104
    :cond_3
    return-object p0

    .line 81
    :cond_4
    invoke-static {}, Lalk;->b()Ljava/lang/String;

    iget-object v0, p1, Lasu;->a:Lasv;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lasu;->b:Landroid/net/Uri;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xf

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid Uri, "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0
.end method
