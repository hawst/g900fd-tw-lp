.class final Lcln;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private synthetic a:Lclm;


# direct methods
.method constructor <init>(Lclm;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lcln;->a:Lclm;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 186
    iget-object v0, p0, Lcln;->a:Lclm;

    invoke-static {v0}, Lclm;->a(Lclm;)Lhdy;

    move-result-object v0

    .line 187
    if-nez v0, :cond_0

    .line 188
    const-string v0, "StraightenFragment"

    const-string v1, "Rotate right failed: straighten view is not ready!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    :goto_0
    return-void

    .line 192
    :cond_0
    iget-object v1, p0, Lcln;->a:Lclm;

    invoke-virtual {v0}, Lhdy;->a()F

    move-result v0

    const/high16 v2, 0x42b40000    # 90.0f

    add-float/2addr v0, v2

    const/high16 v2, 0x43b40000    # 360.0f

    rem-float/2addr v0, v2

    invoke-static {v1, v0}, Lclm;->a(Lclm;F)V

    goto :goto_0
.end method
