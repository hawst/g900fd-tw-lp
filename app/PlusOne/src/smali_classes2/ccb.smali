.class public final Lccb;
.super Landroid/util/Property;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/Property",
        "<",
        "Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 94
    invoke-direct {p0, p1, p2}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;)Ljava/lang/Float;
    .locals 1

    .prologue
    .line 111
    invoke-static {p1}, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->a(Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;Ljava/lang/Float;)V
    .locals 4

    .prologue
    .line 101
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-static {p1, v0}, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->a(Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;F)F

    .line 103
    const/high16 v0, 0x437f0000    # 255.0f

    const v1, 0x3f333333    # 0.7f

    const/high16 v2, 0x3f800000    # 1.0f

    .line 104
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v3

    invoke-static {v1, v2, v3}, Lcfn;->c(FFF)F

    move-result v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p1, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->a:I

    .line 105
    invoke-virtual {p1}, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->e()V

    .line 106
    invoke-virtual {p1}, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->invalidate()V

    .line 107
    return-void
.end method

.method public synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 94
    check-cast p1, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;

    invoke-virtual {p0, p1}, Lccb;->a(Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 94
    check-cast p1, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p0, p1, p2}, Lccb;->a(Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;Ljava/lang/Float;)V

    return-void
.end method
