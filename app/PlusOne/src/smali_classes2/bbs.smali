.class public final Lbbs;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/graphics/Bitmap;

.field public final b:F


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;F)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const-string v0, "lookupTableBitmap"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lbbs;->a:Landroid/graphics/Bitmap;

    .line 25
    iput p2, p0, Lbbs;->b:F

    .line 26
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 30
    if-ne p0, p1, :cond_1

    .line 38
    :cond_0
    :goto_0
    return v0

    .line 33
    :cond_1
    instance-of v2, p1, Lbbs;

    if-nez v2, :cond_2

    move v0, v1

    .line 34
    goto :goto_0

    .line 37
    :cond_2
    check-cast p1, Lbbs;

    .line 38
    iget-object v2, p0, Lbbs;->a:Landroid/graphics/Bitmap;

    iget-object v3, p1, Lbbs;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Landroid/graphics/Bitmap;->sameAs(Landroid/graphics/Bitmap;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lbbs;->b:F

    iget v3, p1, Lbbs;->b:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 44
    iget v0, p0, Lbbs;->b:F

    iget-object v1, p0, Lbbs;->a:Landroid/graphics/Bitmap;

    const/16 v2, 0x11

    invoke-static {v1, v2}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v1

    invoke-static {v0, v1}, Lbqh;->a(FI)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 49
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lbbs;->a:Landroid/graphics/Bitmap;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lbbs;->b:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
