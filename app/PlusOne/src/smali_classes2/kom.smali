.class public final Lkom;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhlz;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lkot;

.field private final c:Lkos;

.field private final d:Lkon;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    new-instance v0, Lkos;

    invoke-direct {v0}, Lkos;-><init>()V

    iput-object v0, p0, Lkom;->c:Lkos;

    .line 27
    iput-object p1, p0, Lkom;->a:Landroid/content/Context;

    .line 28
    new-instance v0, Lkou;

    invoke-direct {v0}, Lkou;-><init>()V

    iput-object v0, p0, Lkom;->b:Lkot;

    .line 29
    const-class v0, Lkon;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkon;

    iput-object v0, p0, Lkom;->d:Lkon;

    .line 30
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lhlv;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 34
    const-string v0, "account_name"

    iget-object v1, p0, Lkom;->c:Lkos;

    .line 35
    invoke-interface {p2, p1, v1}, Lhlv;->a(Landroid/content/Context;Lhlx;)Ljava/lang/String;

    move-result-object v1

    .line 34
    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 36
    const-string v0, "application_id"

    invoke-static {p1}, Lhns;->a(Landroid/content/Context;)I

    move-result v1

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 37
    return-void
.end method

.method public a(Lhlv;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x3

    const/4 v2, 0x2

    .line 41
    const-string v0, "account_name"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 42
    const-string v0, "application_id"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 44
    new-instance v4, Lltz;

    invoke-direct {v4}, Lltz;-><init>()V

    .line 45
    new-instance v5, Lpec;

    invoke-direct {v5}, Lpec;-><init>()V

    .line 46
    iput-object v5, v4, Lltz;->a:Lpec;

    .line 47
    iget-object v5, v4, Lltz;->a:Lpec;

    iput v0, v5, Lpec;->b:I

    .line 48
    iget-object v5, v4, Lltz;->a:Lpec;

    iget-object v0, p0, Lkom;->a:Landroid/content/Context;

    invoke-static {v0}, Llsc;->a(Landroid/content/Context;)Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v0}, Llsc;->b(Landroid/util/DisplayMetrics;)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iput v0, v5, Lpec;->a:I

    .line 49
    iget-object v0, v4, Lltz;->a:Lpec;

    iput v2, v0, Lpec;->c:I

    .line 51
    iget-object v0, p0, Lkom;->b:Lkot;

    invoke-interface {v0, p1, v4, p2}, Lkot;->a(Lhlv;Lltz;Landroid/os/Bundle;)V

    .line 53
    iget-object v0, p0, Lkom;->d:Lkon;

    invoke-interface {v0, v3, v4, p2}, Lkon;->a(Ljava/lang/String;Lltz;Landroid/os/Bundle;)V

    .line 55
    const-string v0, "AnalyticsDispatcher"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x11

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "SocialExtension: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 58
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 48
    goto :goto_0
.end method
