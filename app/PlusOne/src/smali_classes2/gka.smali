.class public abstract Lgka;
.super Ljava/lang/Object;


# instance fields
.field public final a:Lgkq;

.field public final b:Ljava/lang/String;

.field public c:Lgkt;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lgkj;->a(Ljava/lang/String;)V

    iput-object p1, p0, Lgka;->b:Ljava/lang/String;

    new-instance v0, Lgkq;

    invoke-direct {v0, p2}, Lgkq;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lgka;->a:Lgkq;

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgka;->a:Lgkq;

    invoke-virtual {v0, p3}, Lgkq;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(JI)V
    .locals 0

    return-void
.end method

.method protected final a(Ljava/lang/String;JLjava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lgka;->a:Lgkq;

    const-string v1, "Sending text message: %s to: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p4, v2, v3

    invoke-virtual {v0, v1, v2}, Lgkq;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lgka;->c:Lgkt;

    iget-object v1, p0, Lgka;->b:Ljava/lang/String;

    invoke-interface {v0, v1, p1, p2, p3}, Lgkt;->a(Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method protected final c()J
    .locals 2

    iget-object v0, p0, Lgka;->c:Lgkt;

    invoke-interface {v0}, Lgkt;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public d()V
    .locals 0

    return-void
.end method
