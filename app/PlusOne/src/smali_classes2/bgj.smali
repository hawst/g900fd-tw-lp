.class final Lbgj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Ljava/util/concurrent/ExecutorService;

.field private final b:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/util/concurrent/FutureTask",
            "<*>;>;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field private synthetic d:Lbgf;


# direct methods
.method public constructor <init>(Lbgf;Ljava/util/concurrent/ExecutorService;Ljava/util/Queue;Landroid/content/Context;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/ExecutorService;",
            "Ljava/util/Queue",
            "<",
            "Ljava/util/concurrent/FutureTask",
            "<*>;>;",
            "Landroid/content/Context;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 1148
    iput-object p1, p0, Lbgj;->d:Lbgf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1149
    const-string v0, "executorService"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ExecutorService;

    iput-object v0, p0, Lbgj;->a:Ljava/util/concurrent/ExecutorService;

    .line 1150
    const-string v0, "pendingFutureTasks"

    .line 1151
    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Queue;

    iput-object v0, p0, Lbgj;->b:Ljava/util/Queue;

    .line 1152
    const-string v0, "context"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbgj;->c:Landroid/content/Context;

    .line 1153
    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1159
    :try_start_0
    iget-object v0, p0, Lbgj;->d:Lbgf;

    invoke-static {v0}, Lbgf;->b(Lbgf;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 1162
    iget-object v0, p0, Lbgj;->d:Lbgf;

    invoke-static {v0}, Lbgf;->j(Lbgf;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 1164
    :try_start_1
    iget-object v0, p0, Lbgj;->a:Ljava/util/concurrent/ExecutorService;

    iget-object v2, p0, Lbgj;->d:Lbgf;

    invoke-static {v2}, Lbgf;->k(Lbgf;)Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    if-eq v0, v2, :cond_2

    .line 1170
    iget-object v0, p0, Lbgj;->a:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 1174
    :cond_0
    :goto_0
    iget-object v0, p0, Lbgj;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/FutureTask;

    .line 1175
    if-eqz v0, :cond_1

    .line 1176
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1180
    invoke-static {}, Lbgf;->m()Ljava/lang/String;

    goto :goto_0

    .line 1188
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 1201
    :catch_0
    move-exception v0

    .line 1202
    invoke-static {}, Lbgf;->m()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Error initializing render context."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1204
    :goto_1
    return-void

    .line 1184
    :cond_1
    :try_start_3
    monitor-exit v1

    goto :goto_1

    .line 1187
    :cond_2
    iget-object v0, p0, Lbgj;->d:Lbgf;

    new-instance v2, Ljava/util/concurrent/CountDownLatch;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    invoke-static {v0, v2}, Lbgf;->a(Lbgf;Ljava/util/concurrent/CountDownLatch;)Ljava/util/concurrent/CountDownLatch;

    .line 1188
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1191
    :try_start_4
    iget-object v0, p0, Lbgj;->d:Lbgf;

    new-instance v1, Lbgr;

    iget-object v2, p0, Lbgj;->c:Landroid/content/Context;

    invoke-direct {v1, v2}, Lbgr;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lbgf;->a(Lbgf;Lbgr;)Lbgr;

    .line 1192
    iget-object v0, p0, Lbgj;->d:Lbgf;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbgf;->a(Lbgf;Z)V

    .line 1195
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 1196
    const/16 v1, 0xd33

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    .line 1197
    iget-object v1, p0, Lbgj;->d:Lbgf;

    const/4 v2, 0x0

    aget v0, v0, v2

    invoke-static {v1, v0}, Lbgf;->b(Lbgf;I)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1
.end method
