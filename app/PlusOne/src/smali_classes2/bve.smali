.class public final Lbve;
.super Loxu;
.source "PG"


# static fields
.field public static final a:[Lbve;


# instance fields
.field public b:J

.field public c:J

.field public d:I

.field public e:Lbuw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2711
    const/4 v0, 0x0

    new-array v0, v0, [Lbve;

    sput-object v0, Lbve;->a:[Lbve;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 2712
    invoke-direct {p0}, Loxu;-><init>()V

    .line 2726
    iput-wide v0, p0, Lbve;->b:J

    .line 2729
    iput-wide v0, p0, Lbve;->c:J

    .line 2732
    const/4 v0, 0x0

    iput v0, p0, Lbve;->d:I

    .line 2735
    const/4 v0, 0x0

    iput-object v0, p0, Lbve;->e:Lbuw;

    .line 2712
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 2759
    const/4 v0, 0x1

    iget-wide v2, p0, Lbve;->b:J

    .line 2761
    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2762
    const/4 v1, 0x2

    iget-wide v2, p0, Lbve;->c:J

    .line 2763
    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2764
    const/4 v1, 0x3

    iget v2, p0, Lbve;->d:I

    .line 2765
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2766
    iget-object v1, p0, Lbve;->e:Lbuw;

    if-eqz v1, :cond_0

    .line 2767
    const/4 v1, 0x4

    iget-object v2, p0, Lbve;->e:Lbuw;

    .line 2768
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2770
    :cond_0
    iput v0, p0, Lbve;->ai:I

    .line 2771
    return v0
.end method

.method public a(Loxn;)Lbve;
    .locals 2

    .prologue
    .line 2779
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2780
    sparse-switch v0, :sswitch_data_0

    .line 2784
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2785
    :sswitch_0
    return-object p0

    .line 2790
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    iput-wide v0, p0, Lbve;->b:J

    goto :goto_0

    .line 2794
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    iput-wide v0, p0, Lbve;->c:J

    goto :goto_0

    .line 2798
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2799
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-eq v0, v1, :cond_1

    const/4 v1, 0x6

    if-eq v0, v1, :cond_1

    const/4 v1, 0x7

    if-ne v0, v1, :cond_2

    .line 2807
    :cond_1
    iput v0, p0, Lbve;->d:I

    goto :goto_0

    .line 2809
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lbve;->d:I

    goto :goto_0

    .line 2814
    :sswitch_4
    iget-object v0, p0, Lbve;->e:Lbuw;

    if-nez v0, :cond_3

    .line 2815
    new-instance v0, Lbuw;

    invoke-direct {v0}, Lbuw;-><init>()V

    iput-object v0, p0, Lbve;->e:Lbuw;

    .line 2817
    :cond_3
    iget-object v0, p0, Lbve;->e:Lbuw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2780
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 2749
    const/4 v0, 0x1

    iget-wide v2, p0, Lbve;->b:J

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 2750
    const/4 v0, 0x2

    iget-wide v2, p0, Lbve;->c:J

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 2751
    const/4 v0, 0x3

    iget v1, p0, Lbve;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2752
    iget-object v0, p0, Lbve;->e:Lbuw;

    if-eqz v0, :cond_0

    .line 2753
    const/4 v0, 0x4

    iget-object v1, p0, Lbve;->e:Lbuw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2755
    :cond_0
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2708
    invoke-virtual {p0, p1}, Lbve;->a(Loxn;)Lbve;

    move-result-object v0

    return-object v0
.end method
