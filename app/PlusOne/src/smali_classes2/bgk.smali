.class final Lbgk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbge;
.implements Lcgk;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:Z

.field private h:Landroid/opengl/EGLSurface;

.field private i:J

.field private j:Z

.field private k:Z

.field private synthetic l:Lbgf;


# direct methods
.method constructor <init>(Lbgf;II)V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 867
    iput-object p1, p0, Lbgk;->l:Lbgf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 812
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lbgk;->i:J

    .line 869
    invoke-static {p1}, Lbgf;->c(Lbgf;)Landroid/opengl/EGLDisplay;

    move-result-object v2

    .line 870
    invoke-static {p1}, Lbgf;->c(Lbgf;)Landroid/opengl/EGLDisplay;

    move-result-object v3

    invoke-static {}, Lbgf;->i()[I

    move-result-object v4

    invoke-static {v3, v4}, Lbgf;->a(Landroid/opengl/EGLDisplay;[I)Landroid/opengl/EGLConfig;

    move-result-object v3

    const/4 v4, 0x5

    new-array v4, v4, [I

    const/16 v5, 0x3057

    aput v5, v4, v1

    aput p2, v4, v0

    const/4 v5, 0x2

    const/16 v6, 0x3056

    aput v6, v4, v5

    const/4 v5, 0x3

    aput p3, v4, v5

    const/4 v5, 0x4

    const/16 v6, 0x3038

    aput v6, v4, v5

    .line 868
    invoke-static {v2, v3, v4, v1}, Landroid/opengl/EGL14;->eglCreatePbufferSurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;[II)Landroid/opengl/EGLSurface;

    move-result-object v2

    iput-object v2, p0, Lbgk;->h:Landroid/opengl/EGLSurface;

    .line 873
    iget-object v2, p0, Lbgk;->h:Landroid/opengl/EGLSurface;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    if-eq v2, v3, :cond_0

    :goto_0
    const-string v2, "eglCreatePbufferSurface"

    invoke-static {v0, v2}, Lbgf;->a(ZLjava/lang/String;)V

    .line 875
    iput-boolean v1, p0, Lbgk;->g:Z

    .line 876
    const-string v0, "width"

    invoke-static {p2, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lbgk;->a:I

    .line 877
    const-string v0, "height"

    invoke-static {p3, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lbgk;->b:I

    .line 878
    iget v0, p0, Lbgk;->a:I

    iput v0, p0, Lbgk;->c:I

    .line 879
    iget v0, p0, Lbgk;->b:I

    iput v0, p0, Lbgk;->d:I

    .line 880
    iput v1, p0, Lbgk;->e:I

    .line 881
    iput v1, p0, Lbgk;->f:I

    .line 883
    iput-boolean v1, p0, Lbgk;->j:Z

    .line 885
    invoke-static {p1}, Lbgf;->d(Lbgf;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 886
    return-void

    :cond_0
    move v0, v1

    .line 873
    goto :goto_0
.end method

.method private constructor <init>(Lbgf;Ljava/lang/Object;ZIIIIZZ)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 828
    iput-object p1, p0, Lbgk;->l:Lbgf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 812
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lbgk;->i:J

    .line 830
    invoke-static {p1}, Lbgf;->c(Lbgf;)Landroid/opengl/EGLDisplay;

    move-result-object v3

    .line 831
    invoke-static {p1}, Lbgf;->c(Lbgf;)Landroid/opengl/EGLDisplay;

    move-result-object v4

    if-eqz p3, :cond_0

    .line 832
    invoke-static {}, Lbgf;->h()[I

    move-result-object v0

    .line 831
    :goto_0
    invoke-static {v4, v0}, Lbgf;->a(Landroid/opengl/EGLDisplay;[I)Landroid/opengl/EGLConfig;

    move-result-object v0

    const-string v4, "surface"

    .line 833
    const/4 v5, 0x0

    invoke-static {p2, v4, v5}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v4

    .line 834
    invoke-static {}, Lbgf;->j()[I

    move-result-object v5

    .line 829
    invoke-static {v3, v0, v4, v5, v2}, Landroid/opengl/EGL14;->eglCreateWindowSurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLConfig;Ljava/lang/Object;[II)Landroid/opengl/EGLSurface;

    move-result-object v0

    iput-object v0, p0, Lbgk;->h:Landroid/opengl/EGLSurface;

    .line 836
    iget-object v0, p0, Lbgk;->h:Landroid/opengl/EGLSurface;

    sget-object v3, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    if-eq v0, v3, :cond_1

    move v0, v1

    :goto_1
    const-string v3, "eglCreateWindowSurface"

    invoke-static {v0, v3}, Lbgf;->a(ZLjava/lang/String;)V

    .line 838
    const-string v0, "width"

    invoke-static {p4, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lbgk;->a:I

    .line 839
    const-string v0, "height"

    invoke-static {p5, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lbgk;->b:I

    .line 840
    if-eqz p9, :cond_2

    move v0, p5

    .line 841
    :goto_2
    if-eqz p9, :cond_3

    .line 842
    :goto_3
    const-string v3, "surfaceWidth"

    invoke-static {p6, v3}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v3

    iput v3, p0, Lbgk;->c:I

    .line 843
    const-string v3, "surfaceHeight"

    invoke-static {p7, v3}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v3

    iput v3, p0, Lbgk;->d:I

    .line 845
    iput-boolean p9, p0, Lbgk;->g:Z

    .line 849
    iget v3, p0, Lbgk;->c:I

    int-to-float v3, v3

    iget v4, p0, Lbgk;->d:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    int-to-float v4, v0

    int-to-float v5, p4

    div-float/2addr v4, v5

    cmpg-float v3, v3, v4

    if-gez v3, :cond_4

    .line 851
    :goto_4
    if-eqz v1, :cond_5

    .line 852
    iput v2, p0, Lbgk;->e:I

    .line 853
    iget v1, p0, Lbgk;->d:I

    iget v2, p0, Lbgk;->c:I

    mul-int/2addr v2, p4

    div-int v0, v2, v0

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lbgk;->f:I

    .line 861
    :goto_5
    iput-boolean p8, p0, Lbgk;->j:Z

    .line 863
    invoke-static {p1}, Lbgf;->d(Lbgf;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 864
    return-void

    .line 832
    :cond_0
    invoke-static {}, Lbgf;->i()[I

    move-result-object v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 836
    goto :goto_1

    :cond_2
    move v0, p4

    .line 840
    goto :goto_2

    :cond_3
    move p4, p5

    .line 841
    goto :goto_3

    :cond_4
    move v1, v2

    .line 849
    goto :goto_4

    .line 856
    :cond_5
    iget v1, p0, Lbgk;->c:I

    iget v3, p0, Lbgk;->d:I

    mul-int/2addr v0, v3

    div-int/2addr v0, p4

    sub-int v0, v1, v0

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lbgk;->e:I

    .line 858
    iput v2, p0, Lbgk;->f:I

    goto :goto_5
.end method

.method synthetic constructor <init>(Lbgf;Ljava/lang/Object;ZIIIIZZB)V
    .locals 0

    .prologue
    .line 785
    invoke-direct/range {p0 .. p9}, Lbgk;-><init>(Lbgf;Ljava/lang/Object;ZIIIIZZ)V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 906
    invoke-virtual {p0}, Lbgk;->g()V

    .line 907
    invoke-interface {p1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 971
    iget-object v0, p0, Lbgk;->h:Landroid/opengl/EGLSurface;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    if-ne v0, v1, :cond_1

    .line 981
    :cond_0
    :goto_0
    return-void

    .line 975
    :cond_1
    iget-object v0, p0, Lbgk;->l:Lbgf;

    invoke-static {v0}, Lbgf;->c(Lbgf;)Landroid/opengl/EGLDisplay;

    move-result-object v0

    iget-object v1, p0, Lbgk;->h:Landroid/opengl/EGLSurface;

    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglDestroySurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    move-result v0

    const-string v1, "eglDestroySurface"

    invoke-static {v0, v1}, Lbgf;->a(ZLjava/lang/String;)V

    .line 976
    sget-object v0, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    iput-object v0, p0, Lbgk;->h:Landroid/opengl/EGLSurface;

    .line 977
    iget-object v0, p0, Lbgk;->l:Lbgf;

    invoke-static {v0}, Lbgf;->d(Lbgf;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 978
    iget-object v0, p0, Lbgk;->l:Lbgf;

    invoke-static {v0}, Lbgf;->h(Lbgf;)Lbgk;

    move-result-object v0

    if-eq p0, v0, :cond_0

    .line 979
    iget-object v0, p0, Lbgk;->l:Lbgf;

    invoke-static {v0}, Lbgf;->h(Lbgf;)Lbgk;

    move-result-object v0

    invoke-virtual {v0}, Lbgk;->g()V

    goto :goto_0
.end method

.method a(I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 927
    iget-object v0, p0, Lbgk;->h:Landroid/opengl/EGLSurface;

    const-string v1, "mSurface"

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    invoke-static {v0, v2}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " must not be equal to "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    .line 929
    :cond_0
    iget-object v0, p0, Lbgk;->l:Lbgf;

    invoke-static {v0}, Lbgf;->e(Lbgf;)Lbgk;

    move-result-object v0

    if-eq v0, p0, :cond_1

    .line 930
    iget-object v0, p0, Lbgk;->l:Lbgf;

    invoke-static {v0}, Lbgf;->c(Lbgf;)Landroid/opengl/EGLDisplay;

    move-result-object v0

    iget-object v1, p0, Lbgk;->h:Landroid/opengl/EGLSurface;

    iget-object v2, p0, Lbgk;->h:Landroid/opengl/EGLSurface;

    iget-object v3, p0, Lbgk;->l:Lbgf;

    invoke-static {v3}, Lbgf;->f(Lbgf;)Landroid/opengl/EGLContext;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    move-result v0

    const-string v1, "eglMakeCurrent"

    invoke-static {v0, v1}, Lbgf;->a(ZLjava/lang/String;)V

    .line 932
    iget-object v0, p0, Lbgk;->l:Lbgf;

    invoke-static {v0, p0}, Lbgf;->a(Lbgf;Lbgk;)Lbgk;

    .line 935
    :cond_1
    const v0, 0x8ca6

    iget-object v1, p0, Lbgk;->l:Lbgf;

    invoke-static {v1}, Lbgf;->g(Lbgf;)[I

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    .line 936
    const-string v0, "glGetIntegerv"

    invoke-static {v0}, Lbgc;->a(Ljava/lang/String;)V

    .line 937
    iget-object v0, p0, Lbgk;->l:Lbgf;

    invoke-static {v0}, Lbgf;->g(Lbgf;)[I

    move-result-object v0

    aget v0, v0, v4

    if-eq v0, p1, :cond_2

    .line 938
    const v0, 0x8d40

    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 939
    const-string v0, "glBindFramebuffer"

    invoke-static {v0}, Lbgc;->a(Ljava/lang/String;)V

    .line 941
    :cond_2
    return-void
.end method

.method public a(J)V
    .locals 5

    .prologue
    .line 959
    iget-boolean v0, p0, Lbgk;->j:Z

    if-eqz v0, :cond_0

    .line 960
    iget-wide v0, p0, Lbgk;->i:J

    cmp-long v0, v0, p1

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "received out of order frames"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 962
    iput-wide p1, p0, Lbgk;->i:J

    .line 963
    iget-object v0, p0, Lbgk;->l:Lbgf;

    invoke-static {v0}, Lbgf;->c(Lbgf;)Landroid/opengl/EGLDisplay;

    move-result-object v0

    iget-object v1, p0, Lbgk;->h:Landroid/opengl/EGLSurface;

    const-wide/16 v2, 0x3e8

    mul-long/2addr v2, p1

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/EGLExt;->eglPresentationTimeANDROID(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;J)Z

    .line 966
    :cond_0
    invoke-virtual {p0}, Lbgk;->b()V

    .line 967
    return-void

    .line 960
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 950
    iget-boolean v0, p0, Lbgk;->k:Z

    if-nez v0, :cond_0

    .line 951
    iget-object v0, p0, Lbgk;->l:Lbgf;

    invoke-static {v0}, Lbgf;->c(Lbgf;)Landroid/opengl/EGLDisplay;

    move-result-object v0

    iget-object v1, p0, Lbgk;->h:Landroid/opengl/EGLSurface;

    invoke-static {v0, v1}, Landroid/opengl/EGL14;->eglSwapBuffers(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    move-result v0

    const-string v1, "eglSwapBuffers"

    invoke-static {v0, v1}, Lbgf;->a(ZLjava/lang/String;)V

    .line 955
    :goto_0
    return-void

    .line 953
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbgk;->k:Z

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 945
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbgk;->k:Z

    .line 946
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 891
    iget v0, p0, Lbgk;->a:I

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 896
    iget v0, p0, Lbgk;->b:I

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 901
    iget-boolean v0, p0, Lbgk;->g:Z

    return v0
.end method

.method g()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 911
    invoke-virtual {p0, v2}, Lbgk;->a(I)V

    .line 914
    iget v0, p0, Lbgk;->e:I

    if-gtz v0, :cond_0

    iget v0, p0, Lbgk;->f:I

    if-lez v0, :cond_1

    .line 915
    :cond_0
    iget v0, p0, Lbgk;->c:I

    iget v1, p0, Lbgk;->d:I

    invoke-static {v2, v2, v0, v1}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 916
    const-string v0, "glViewport"

    invoke-static {v0}, Lbgc;->a(Ljava/lang/String;)V

    .line 917
    const/16 v0, 0x4000

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    .line 918
    const-string v0, "glClear"

    invoke-static {v0}, Lbgc;->a(Ljava/lang/String;)V

    .line 921
    :cond_1
    iget v0, p0, Lbgk;->e:I

    iget v1, p0, Lbgk;->f:I

    iget v2, p0, Lbgk;->c:I

    iget v3, p0, Lbgk;->e:I

    shl-int/lit8 v3, v3, 0x1

    sub-int/2addr v2, v3

    iget v3, p0, Lbgk;->d:I

    iget v4, p0, Lbgk;->f:I

    shl-int/lit8 v4, v4, 0x1

    sub-int/2addr v3, v4

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 923
    const-string v0, "glViewport"

    invoke-static {v0}, Lbgc;->a(Ljava/lang/String;)V

    .line 924
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 985
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lbgk;->h:Landroid/opengl/EGLSurface;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lbgk;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lbgk;->d:I

    .line 986
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-boolean v3, p0, Lbgk;->g:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget v3, p0, Lbgk;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget v3, p0, Lbgk;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-boolean v3, p0, Lbgk;->j:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 985
    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 990
    return-object v0
.end method
