.class public final enum Lanm;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lanm;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lanm;

.field public static final enum b:Lanm;

.field private static final synthetic c:[Lanm;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 54
    new-instance v0, Lanm;

    const-string v1, "SCANNING"

    invoke-direct {v0, v1, v2}, Lanm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lanm;->a:Lanm;

    new-instance v0, Lanm;

    const-string v1, "DOWNLOADING"

    invoke-direct {v0, v1, v3}, Lanm;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lanm;->b:Lanm;

    .line 53
    const/4 v0, 0x2

    new-array v0, v0, [Lanm;

    sget-object v1, Lanm;->a:Lanm;

    aput-object v1, v0, v2

    sget-object v1, Lanm;->b:Lanm;

    aput-object v1, v0, v3

    sput-object v0, Lanm;->c:[Lanm;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lanm;
    .locals 1

    .prologue
    .line 53
    const-class v0, Lanm;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lanm;

    return-object v0
.end method

.method public static values()[Lanm;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lanm;->c:[Lanm;

    invoke-virtual {v0}, [Lanm;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lanm;

    return-object v0
.end method
