.class final Ljgx;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:I

.field b:J

.field final c:[Ljava/lang/String;

.field d:J

.field e:J

.field f:J

.field private final g:J

.field private final h:J


# direct methods
.method constructor <init>(I[Ljava/lang/String;JJJJJ)V
    .locals 3

    .prologue
    .line 466
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 467
    iput p1, p0, Ljgx;->a:I

    .line 469
    if-nez p2, :cond_0

    .line 470
    const/4 v0, 0x0

    new-array p2, v0, [Ljava/lang/String;

    .line 472
    :cond_0
    iput-object p2, p0, Ljgx;->c:[Ljava/lang/String;

    .line 473
    iput-wide p3, p0, Ljgx;->b:J

    .line 475
    iput-wide p5, p0, Ljgx;->g:J

    .line 476
    iput-wide p7, p0, Ljgx;->d:J

    .line 477
    iput-wide p9, p0, Ljgx;->e:J

    .line 478
    iput-wide p11, p0, Ljgx;->f:J

    .line 479
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Ljgx;->h:J

    .line 480
    return-void
.end method


# virtual methods
.method a()Z
    .locals 4

    .prologue
    .line 491
    iget-wide v0, p0, Ljgx;->h:J

    const-wide/16 v2, 0x1c20

    add-long/2addr v0, v2

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Z
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 495
    iget-wide v0, p0, Ljgx;->f:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-wide v0, p0, Ljgx;->e:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    :cond_0
    iget-wide v0, p0, Ljgx;->b:J

    iget-wide v2, p0, Ljgx;->d:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()J
    .locals 4

    .prologue
    .line 499
    iget-wide v0, p0, Ljgx;->d:J

    iget-wide v2, p0, Ljgx;->b:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public d()J
    .locals 4

    .prologue
    .line 503
    iget-wide v0, p0, Ljgx;->g:J

    iget-wide v2, p0, Ljgx;->b:J

    sub-long/2addr v0, v2

    return-wide v0
.end method
