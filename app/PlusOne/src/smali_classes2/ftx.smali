.class final Lftx;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/graphics/Bitmap;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lftv;",
            ">;"
        }
    .end annotation
.end field

.field private final b:I

.field private final c:I

.field private final d:F

.field private final e:Lfrg;

.field private final f:I

.field private final g:I

.field private final h:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lftv;IILfrg;F)V
    .locals 1

    .prologue
    .line 514
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 515
    iput-object p1, p0, Lftx;->h:Landroid/content/Context;

    .line 516
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lftx;->a:Ljava/lang/ref/WeakReference;

    .line 517
    iput p3, p0, Lftx;->b:I

    .line 518
    iput p4, p0, Lftx;->c:I

    .line 519
    iput p6, p0, Lftx;->d:F

    .line 520
    iput-object p5, p0, Lftx;->e:Lfrg;

    .line 521
    invoke-virtual {p2}, Lftv;->getMeasuredWidth()I

    move-result v0

    iput v0, p0, Lftx;->f:I

    .line 522
    invoke-virtual {p2}, Lftv;->getMeasuredHeight()I

    move-result v0

    iput v0, p0, Lftx;->g:I

    .line 523
    return-void
.end method


# virtual methods
.method protected varargs a([Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 12

    .prologue
    .line 527
    const/4 v0, 0x0

    aget-object v0, p1, v0

    .line 530
    invoke-static {}, Lftv;->f()Lkdv;

    move-result-object v1

    iget v2, p0, Lftx;->f:I

    iget v3, p0, Lftx;->g:I

    invoke-interface {v1, v2, v3}, Lkdv;->b(II)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 531
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xc

    if-lt v1, v2, :cond_0

    .line 532
    const/4 v1, 0x1

    invoke-virtual {v8, v1}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 534
    :cond_0
    new-instance v9, Landroid/graphics/Canvas;

    invoke-direct {v9, v8}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 536
    const/4 v1, 0x0

    .line 537
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_1

    iget v2, p0, Lftx;->d:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-lez v2, :cond_1

    .line 538
    iget v2, p0, Lftx;->d:F

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    div-int/lit8 v4, v1, 0x4

    div-int/lit8 v3, v3, 0x4

    invoke-static {}, Lftv;->f()Lkdv;

    move-result-object v1

    invoke-interface {v1, v4, v3}, Lkdv;->b(II)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v5, Landroid/graphics/Canvas;

    invoke-direct {v5, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v6, 0x0

    new-instance v7, Landroid/graphics/Rect;

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct {v7, v10, v11, v4, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    const/4 v3, 0x0

    invoke-virtual {v5, v0, v6, v7, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    iget-object v3, p0, Lftx;->h:Landroid/content/Context;

    invoke-static {v3}, Landroid/renderscript/RenderScript;->create(Landroid/content/Context;)Landroid/renderscript/RenderScript;

    move-result-object v3

    invoke-static {v3}, Landroid/renderscript/Element;->U8_4(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/renderscript/ScriptIntrinsicBlur;->create(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)Landroid/renderscript/ScriptIntrinsicBlur;

    move-result-object v4

    invoke-static {v3, v1}, Landroid/renderscript/Allocation;->createFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;)Landroid/renderscript/Allocation;

    move-result-object v5

    invoke-static {v3, v1}, Landroid/renderscript/Allocation;->createFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;)Landroid/renderscript/Allocation;

    move-result-object v3

    const/high16 v6, 0x41c80000    # 25.0f

    const/high16 v7, 0x40800000    # 4.0f

    div-float/2addr v2, v7

    invoke-static {v6, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-virtual {v4, v2}, Landroid/renderscript/ScriptIntrinsicBlur;->setRadius(F)V

    invoke-virtual {v4, v5}, Landroid/renderscript/ScriptIntrinsicBlur;->setInput(Landroid/renderscript/Allocation;)V

    invoke-virtual {v4, v3}, Landroid/renderscript/ScriptIntrinsicBlur;->forEach(Landroid/renderscript/Allocation;)V

    invoke-virtual {v3, v1}, Landroid/renderscript/Allocation;->copyTo(Landroid/graphics/Bitmap;)V

    .line 541
    :cond_1
    if-eqz v1, :cond_2

    move-object v0, v1

    :cond_2
    iget v3, p0, Lftx;->f:I

    iget v2, p0, Lftx;->g:I

    iget v4, p0, Lftx;->b:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_7

    const/4 v4, 0x1

    :goto_0
    if-eqz v4, :cond_3

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    move v3, v2

    :cond_3
    new-instance v4, Landroid/graphics/Rect;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6, v3, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    invoke-static {v0, v4, v2}, Lftv;->a(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    new-instance v3, Landroid/graphics/Paint;

    const/4 v5, 0x1

    invoke-direct {v3, v5}, Landroid/graphics/Paint;-><init>(I)V

    const/4 v5, 0x1

    invoke-virtual {v3, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    invoke-virtual {v9, v0, v2, v4, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 543
    if-eqz v1, :cond_4

    .line 544
    invoke-static {}, Lftv;->f()Lkdv;

    move-result-object v0

    invoke-interface {v0, v1}, Lkdv;->a(Landroid/graphics/Bitmap;)V

    .line 547
    :cond_4
    iget v0, p0, Lftx;->c:I

    if-eqz v0, :cond_5

    .line 548
    iget v0, p0, Lftx;->c:I

    invoke-virtual {v9, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 551
    :cond_5
    iget-object v0, p0, Lftx;->e:Lfrg;

    if-eqz v0, :cond_6

    .line 552
    new-instance v10, Landroid/graphics/Rect;

    const/4 v0, 0x0

    const/4 v1, 0x0

    iget v2, p0, Lftx;->f:I

    iget v3, p0, Lftx;->g:I

    invoke-direct {v10, v0, v1, v2, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 553
    new-instance v11, Landroid/graphics/Paint;

    const/4 v0, 0x1

    invoke-direct {v11, v0}, Landroid/graphics/Paint;-><init>(I)V

    .line 554
    const/4 v0, 0x1

    invoke-virtual {v11, v0}, Landroid/graphics/Paint;->setDither(Z)V

    .line 555
    new-instance v0, Landroid/graphics/LinearGradient;

    iget-object v1, p0, Lftx;->e:Lfrg;

    iget-object v1, v1, Lfrg;->a:Lfri;

    iget v1, v1, Lfri;->a:F

    iget v2, p0, Lftx;->f:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lftx;->e:Lfrg;

    iget-object v2, v2, Lfrg;->a:Lfri;

    iget v2, v2, Lfri;->b:F

    iget v3, p0, Lftx;->g:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lftx;->e:Lfrg;

    iget-object v3, v3, Lfrg;->a:Lfri;

    iget v3, v3, Lfri;->c:F

    iget v4, p0, Lftx;->f:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lftx;->e:Lfrg;

    iget-object v4, v4, Lfrg;->a:Lfri;

    iget v4, v4, Lfri;->d:F

    iget v5, p0, Lftx;->g:I

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget-object v5, p0, Lftx;->e:Lfrg;

    iget v5, v5, Lfrg;->b:I

    iget-object v6, p0, Lftx;->e:Lfrg;

    iget v6, v6, Lfrg;->c:I

    sget-object v7, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v0 .. v7}, Landroid/graphics/LinearGradient;-><init>(FFFFIILandroid/graphics/Shader$TileMode;)V

    invoke-virtual {v11, v0}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 563
    invoke-virtual {v9, v10, v11}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 566
    :cond_6
    iget v0, p0, Lftx;->b:I

    packed-switch v0, :pswitch_data_0

    .line 576
    iget v0, p0, Lftx;->b:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x24

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unrecognised mask shape: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 579
    :goto_1
    :pswitch_0
    return-object v8

    .line 541
    :cond_7
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 568
    :pswitch_1
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    new-instance v1, Landroid/graphics/Path;

    invoke-direct {v1}, Landroid/graphics/Path;-><init>()V

    invoke-virtual {v1}, Landroid/graphics/Path;->toggleInverseFillType()V

    iget v2, p0, Lftx;->f:I

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    iget v3, p0, Lftx;->g:I

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v4, p0, Lftx;->f:I

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    invoke-virtual {v9, v1, v0}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1

    .line 571
    :pswitch_2
    iget v0, p0, Lftx;->f:I

    iget v1, p0, Lftx;->g:I

    const-wide v2, 0x3ff4cccccccccccdL    # 1.3

    int-to-double v4, v0

    mul-double/2addr v2, v4

    double-to-int v2, v2

    new-instance v3, Landroid/graphics/Paint;

    const/4 v4, 0x1

    invoke-direct {v3, v4}, Landroid/graphics/Paint;-><init>(I)V

    const/high16 v4, -0x1000000

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v4, Landroid/graphics/PorterDuffXfermode;

    sget-object v5, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v4, v5}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    new-instance v4, Landroid/graphics/RectF;

    mul-int/lit8 v5, v2, 0x2

    sub-int v5, v0, v5

    int-to-float v5, v5

    div-int/lit8 v6, v1, 0x2

    sub-int/2addr v6, v2

    int-to-float v6, v6

    int-to-float v0, v0

    div-int/lit8 v7, v1, 0x2

    add-int/2addr v2, v7

    int-to-float v2, v2

    invoke-direct {v4, v5, v6, v0, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    invoke-virtual {v0}, Landroid/graphics/Path;->toggleInverseFillType()V

    const/high16 v2, 0x43480000    # 200.0f

    const/high16 v5, 0x43aa0000    # 340.0f

    invoke-virtual {v0, v4, v2, v5}, Landroid/graphics/Path;->arcTo(Landroid/graphics/RectF;FF)V

    const/4 v2, 0x0

    int-to-float v1, v1

    invoke-virtual {v0, v2, v1}, Landroid/graphics/Path;->moveTo(FF)V

    invoke-virtual {v0}, Landroid/graphics/Path;->close()V

    invoke-virtual {v9, v0, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 566
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 612
    invoke-virtual {p0}, Lftx;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 613
    const/4 p1, 0x0

    .line 616
    :cond_0
    iget-object v0, p0, Lftx;->a:Ljava/lang/ref/WeakReference;

    if-eqz p1, :cond_1

    .line 617
    iget-object v0, p0, Lftx;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lftv;

    .line 618
    if-eqz v0, :cond_1

    invoke-static {v0}, Lftv;->a(Lftv;)Landroid/os/AsyncTask;

    move-result-object v1

    if-ne v1, p0, :cond_1

    .line 619
    invoke-static {v0, p1}, Lftv;->a(Lftv;Landroid/graphics/Bitmap;)V

    .line 622
    :cond_1
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 497
    check-cast p1, [Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lftx;->a([Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 497
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lftx;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method
