.class final Layq;
.super Layt;
.source "PG"


# static fields
.field private static final b:Landroid/graphics/Typeface;


# instance fields
.field private final c:Landroid/graphics/Matrix;

.field private final d:J

.field private final e:[F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    const-string v0, "sans-serif"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Layq;->b:Landroid/graphics/Typeface;

    return-void
.end method

.method constructor <init>(Layj;Landroid/content/res/Resources;JJJ[F)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 67
    new-instance v0, Layu;

    invoke-direct {v0}, Layu;-><init>()V

    .line 68
    invoke-virtual {v0, p1}, Layu;->a(Layj;)Layu;

    move-result-object v0

    const/4 v1, -0x1

    .line 69
    invoke-virtual {v0, v1}, Layu;->a(I)Layu;

    move-result-object v0

    const/16 v1, 0x55

    .line 70
    invoke-virtual {v0, v1}, Layu;->b(I)Layu;

    move-result-object v0

    const v1, 0x428238d0

    .line 71
    invoke-virtual {v0, v1}, Layu;->a(F)Layu;

    move-result-object v0

    sget-object v1, Layq;->b:Landroid/graphics/Typeface;

    .line 72
    invoke-virtual {v0, v1}, Layu;->a(Landroid/graphics/Typeface;)Layu;

    move-result-object v0

    const/16 v1, 0x15

    .line 73
    invoke-virtual {v0, v1}, Layu;->d(I)Layu;

    move-result-object v0

    const v1, 0x42aceaea

    .line 74
    invoke-virtual {v0, v1}, Layu;->b(F)Layu;

    move-result-object v0

    .line 75
    invoke-virtual {v0, v2}, Layu;->a(Z)Layu;

    move-result-object v0

    .line 76
    invoke-virtual {v0, v2}, Layu;->e(I)Layu;

    move-result-object v0

    const/4 v1, 0x2

    .line 77
    invoke-virtual {v0, v1}, Layu;->f(I)Layu;

    move-result-object v0

    .line 78
    invoke-virtual {v0, p5, p6}, Layu;->a(J)Layu;

    move-result-object v0

    .line 79
    invoke-virtual {v0, p7, p8}, Layu;->b(J)Layu;

    move-result-object v0

    .line 80
    invoke-virtual {v0, v2}, Layu;->b(Z)Layu;

    move-result-object v0

    const v1, 0x3d4ccccd    # 0.05f

    .line 81
    invoke-virtual {v0, v1}, Layu;->c(F)Layu;

    move-result-object v0

    const v1, 0x3d27de6d

    .line 82
    invoke-virtual {v0, v1}, Layu;->e(F)Layu;

    move-result-object v0

    const v1, 0x7f0b009b

    .line 83
    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Layu;->g(I)Layu;

    move-result-object v0

    .line 67
    invoke-direct {p0, v0}, Layt;-><init>(Layu;)V

    .line 55
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Layq;->c:Landroid/graphics/Matrix;

    .line 84
    iput-wide p3, p0, Layq;->d:J

    .line 85
    iput-object p9, p0, Layq;->e:[F

    .line 86
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;IIIIF)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 187
    new-instance v0, Lbaj;

    invoke-direct {v0, p1}, Lbaj;-><init>(Landroid/content/Context;)V

    .line 189
    const/high16 v1, 0x41a80000    # 21.0f

    mul-float/2addr v1, p7

    const/high16 v2, 0x44440000    # 784.0f

    div-float/2addr v1, v2

    float-to-int v1, v1

    .line 190
    invoke-virtual {v0, v1, v1, v1, v1}, Lbaj;->setPadding(IIII)V

    .line 191
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbaj;->setVisibility(I)V

    .line 192
    invoke-virtual {v0, p4}, Lbaj;->setGravity(I)V

    .line 194
    invoke-virtual {v0, p2}, Lbaj;->setText(Ljava/lang/CharSequence;)V

    .line 195
    invoke-virtual {v0, p3}, Lbaj;->setTextColor(I)V

    .line 196
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbaj;->setAllCaps(Z)V

    .line 198
    sget-object v1, Layq;->b:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Lbaj;->setTypeface(Landroid/graphics/Typeface;)V

    .line 199
    invoke-virtual {v0, v3}, Lbaj;->a(F)V

    .line 200
    const v1, 0x3fa9f79b

    invoke-virtual {v0, v3, v1}, Lbaj;->setLineSpacing(FF)V

    .line 202
    invoke-virtual {p0, v0, p5, p6, p7}, Layq;->a(Lbaj;IIF)V

    .line 204
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x1

    invoke-direct {v1, p5, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Lbaj;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 207
    return-object v0
.end method


# virtual methods
.method protected a(Landroid/content/Context;Landroid/graphics/Bitmap;Layl;II)Landroid/graphics/Bitmap;
    .locals 10

    .prologue
    .line 150
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v0

    if-nez v0, :cond_1

    .line 151
    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p4, p5, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p2

    .line 155
    :cond_1
    new-instance v8, Landroid/graphics/Canvas;

    invoke-direct {v8, p2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 156
    const/4 v0, 0x0

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v8, v0, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 160
    new-instance v9, Landroid/widget/LinearLayout;

    invoke-direct {v9, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 161
    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->setOrientation(I)V

    .line 163
    int-to-float v0, p4

    int-to-float v1, p5

    invoke-static {v0, v1}, Lcfn;->a(FF)F

    move-result v7

    .line 164
    iget-object v2, p3, Layl;->p:Ljava/lang/String;

    const/4 v3, -0x1

    const/16 v4, 0x55

    int-to-float v0, p4

    const/high16 v1, 0x3f400000    # 0.75f

    mul-float/2addr v0, v1

    float-to-int v5, v0

    move-object v0, p0

    move-object v1, p1

    move v6, p5

    invoke-direct/range {v0 .. v7}, Layq;->a(Landroid/content/Context;Ljava/lang/String;IIIIF)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 168
    iget-object v2, p3, Layl;->q:Ljava/lang/String;

    const/high16 v3, -0x1000000

    const/16 v4, 0x53

    int-to-float v0, p4

    const/high16 v1, 0x3e800000    # 0.25f

    mul-float/2addr v0, v1

    float-to-int v5, v0

    move-object v0, p0

    move-object v1, p1

    move v6, p5

    invoke-direct/range {v0 .. v7}, Layq;->a(Landroid/content/Context;Ljava/lang/String;IIIIF)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 173
    invoke-virtual {v8}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 174
    invoke-virtual {v8}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 173
    invoke-virtual {v9, v0, v1}, Landroid/widget/LinearLayout;->measure(II)V

    .line 175
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {v8}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    invoke-virtual {v8}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    invoke-virtual {v9, v0, v1, v2, v3}, Landroid/widget/LinearLayout;->layout(IIII)V

    .line 176
    invoke-virtual {v9, v8}, Landroid/widget/LinearLayout;->draw(Landroid/graphics/Canvas;)V

    .line 178
    return-object p2
.end method

.method public o(Layl;)Landroid/graphics/Matrix;
    .locals 12

    .prologue
    .line 90
    iget-object v0, p0, Layq;->c:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 91
    iget v0, p1, Layl;->f:I

    if-nez v0, :cond_0

    .line 92
    iget-object v1, p0, Layq;->c:Landroid/graphics/Matrix;

    iget-wide v2, p1, Layl;->e:J

    const-wide/16 v4, 0x4

    div-long/2addr v2, v4

    iget-wide v4, p0, Layq;->d:J

    const-wide/16 v6, 0x2

    div-long/2addr v4, v6

    sub-long v4, v2, v4

    iget-wide v6, p1, Layl;->d:J

    cmp-long v0, v6, v4

    if-gez v0, :cond_1

    const/high16 v0, -0x40800000    # -1.0f

    :goto_0
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 94
    :cond_0
    iget-object v0, p0, Layq;->c:Landroid/graphics/Matrix;

    return-object v0

    .line 92
    :cond_1
    iget-wide v6, p0, Layq;->d:J

    const-wide/16 v8, 0x2

    div-long/2addr v6, v8

    add-long/2addr v6, v2

    iget-wide v8, p1, Layl;->d:J

    cmp-long v0, v8, v6

    if-gez v0, :cond_2

    const/high16 v0, -0x40800000    # -1.0f

    const/high16 v2, 0x3f400000    # 0.75f

    iget-wide v6, p1, Layl;->d:J

    sub-long v4, v6, v4

    long-to-float v3, v4

    iget-wide v4, p0, Layq;->d:J

    long-to-float v4, v4

    div-float/2addr v3, v4

    iget-object v4, p0, Layq;->e:[F

    invoke-static {v3, v4}, Lbag;->a(F[F)F

    move-result v3

    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    shl-long v4, v2, v0

    iget-wide v6, p0, Layq;->d:J

    const-wide/16 v8, 0x2

    div-long/2addr v6, v8

    sub-long/2addr v4, v6

    iget-wide v6, p1, Layl;->d:J

    cmp-long v0, v6, v4

    if-gez v0, :cond_3

    const/high16 v0, -0x41800000    # -0.25f

    goto :goto_0

    :cond_3
    const/4 v0, 0x1

    shl-long v6, v2, v0

    iget-wide v8, p0, Layq;->d:J

    const-wide/16 v10, 0x2

    div-long/2addr v8, v10

    add-long/2addr v6, v8

    iget-wide v8, p1, Layl;->d:J

    cmp-long v0, v8, v6

    if-gez v0, :cond_4

    const/high16 v0, -0x41800000    # -0.25f

    const/high16 v2, 0x3e800000    # 0.25f

    iget-wide v6, p1, Layl;->d:J

    sub-long v4, v6, v4

    long-to-float v3, v4

    iget-wide v4, p0, Layq;->d:J

    long-to-float v4, v4

    div-float/2addr v3, v4

    iget-object v4, p0, Layq;->e:[F

    invoke-static {v3, v4}, Lbag;->a(F[F)F

    move-result v3

    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    goto :goto_0

    :cond_4
    const/4 v0, 0x2

    shl-long/2addr v2, v0

    iget-wide v4, p1, Layl;->o:J

    sub-long/2addr v2, v4

    iget-wide v4, p1, Layl;->d:J

    cmp-long v0, v4, v2

    if-gez v0, :cond_5

    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    iget-wide v4, p1, Layl;->d:J

    sub-long v2, v4, v2

    long-to-float v0, v2

    iget-wide v2, p0, Layq;->d:J

    long-to-float v2, v2

    div-float/2addr v0, v2

    iget-object v2, p0, Layq;->e:[F

    invoke-static {v0, v2}, Lbag;->a(F[F)F

    move-result v0

    goto :goto_0
.end method
