.class public final Lcgn;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lcgo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcgo",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TT;>;"
        }
    .end annotation
.end field

.field private d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<TT;>;"
        }
    .end annotation
.end field

.field private e:Z


# direct methods
.method public constructor <init>(Lcgo;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcgo",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p1, p0, Lcgn;->a:Lcgo;

    .line 48
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcgn;->b:Ljava/util/Set;

    .line 49
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcgn;->c:Ljava/util/Set;

    .line 51
    iget-object v0, p0, Lcgn;->b:Ljava/util/Set;

    iput-object v0, p0, Lcgn;->d:Ljava/util/Set;

    .line 52
    return-void
.end method

.method private a(Ljava/util/Set;Ljava/util/Set;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<TT;>;",
            "Ljava/util/Set",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 129
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_0
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 130
    invoke-interface {p2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 131
    iget-object v2, p0, Lcgn;->a:Lcgo;

    invoke-interface {v2, v1}, Lcgo;->b(Ljava/lang/Object;)V

    goto :goto_0

    .line 134
    :cond_1
    invoke-interface {p2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :cond_2
    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 135
    invoke-interface {p1, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 136
    iget-object v2, p0, Lcgn;->a:Lcgo;

    invoke-interface {v2, v1}, Lcgo;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 139
    :cond_3
    return-void
.end method


# virtual methods
.method public a()Ljava/util/Set;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 59
    iget-boolean v0, p0, Lcgn;->e:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "beginEdit() called twice. Did you forget to call endEdit()?"

    invoke-static {v0, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 61
    iput-boolean v1, p0, Lcgn;->e:Z

    .line 62
    iget-object v0, p0, Lcgn;->d:Ljava/util/Set;

    iget-object v1, p0, Lcgn;->b:Ljava/util/Set;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcgn;->c:Ljava/util/Set;

    .line 63
    :goto_1
    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 64
    iget-object v1, p0, Lcgn;->d:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 65
    return-object v0

    .line 59
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 62
    :cond_1
    iget-object v0, p0, Lcgn;->b:Ljava/util/Set;

    goto :goto_1
.end method

.method public a(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 86
    iget-boolean v0, p0, Lcgn;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Do not call this method between beginEdit() and endEdit()"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 88
    iget-object v0, p0, Lcgn;->d:Ljava/util/Set;

    iget-object v1, p0, Lcgn;->b:Ljava/util/Set;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcgn;->c:Ljava/util/Set;

    .line 89
    :goto_1
    iget-object v1, p0, Lcgn;->d:Ljava/util/Set;

    .line 90
    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 91
    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 92
    invoke-direct {p0, v1, v0}, Lcgn;->a(Ljava/util/Set;Ljava/util/Set;)V

    .line 93
    iput-object v0, p0, Lcgn;->d:Ljava/util/Set;

    .line 94
    return-void

    .line 86
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 88
    :cond_1
    iget-object v0, p0, Lcgn;->b:Ljava/util/Set;

    goto :goto_1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 70
    iget-boolean v0, p0, Lcgn;->e:Z

    const-string v1, "endEdit() called without beginEdit()"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcgn;->e:Z

    .line 73
    iget-object v0, p0, Lcgn;->d:Ljava/util/Set;

    iget-object v1, p0, Lcgn;->b:Ljava/util/Set;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcgn;->c:Ljava/util/Set;

    .line 74
    :goto_0
    iget-object v1, p0, Lcgn;->d:Ljava/util/Set;

    .line 75
    invoke-direct {p0, v1, v0}, Lcgn;->a(Ljava/util/Set;Ljava/util/Set;)V

    .line 76
    iput-object v0, p0, Lcgn;->d:Ljava/util/Set;

    .line 77
    return-void

    .line 73
    :cond_0
    iget-object v0, p0, Lcgn;->b:Ljava/util/Set;

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 114
    iget-boolean v0, p0, Lcgn;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Do not call this method between beginEdit() and endEdit()"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 116
    iget-object v0, p0, Lcgn;->d:Ljava/util/Set;

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcgn;->a(Ljava/util/Set;Ljava/util/Set;)V

    .line 117
    iget-object v0, p0, Lcgn;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 118
    return-void

    .line 114
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 122
    const-string v0, "SetSnapshot[mIsEditing="

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcgn;->e:Z

    iget-object v2, p0, Lcgn;->d:Ljava/util/Set;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x15

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", LastSnapshot="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
