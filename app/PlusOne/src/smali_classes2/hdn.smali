.class public final Lhdn;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/app/Activity;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, -0x1

    .line 25
    if-nez p0, :cond_1

    .line 50
    :cond_0
    :goto_0
    return-void

    .line 29
    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    move-result v2

    .line 31
    invoke-virtual {p0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    packed-switch v3, :pswitch_data_0

    move v0, v1

    .line 47
    :cond_2
    :goto_1
    if-eq v0, v1, :cond_0

    .line 48
    invoke-virtual {p0, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0

    .line 33
    :pswitch_0
    if-eqz v2, :cond_2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_2

    const/16 v0, 0x9

    goto :goto_1

    .line 39
    :pswitch_1
    if-eqz v2, :cond_3

    if-ne v2, v0, :cond_4

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const/16 v0, 0x8

    goto :goto_1

    .line 31
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 60
    if-nez p0, :cond_0

    .line 65
    :goto_0
    return-void

    .line 64
    :cond_0
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_0
.end method
