.class public final Lbva;
.super Loxu;
.source "PG"


# static fields
.field public static final a:[Lbva;


# instance fields
.field public b:Lbvb;

.field public c:F

.field public d:F

.field public e:F

.field public f:F

.field public g:F

.field public h:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lbva;

    sput-object v0, Lbva;->a:[Lbva;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxu;-><init>()V

    .line 331
    const/4 v0, 0x0

    iput-object v0, p0, Lbva;->b:Lbvb;

    .line 334
    iput v1, p0, Lbva;->c:F

    .line 337
    iput v1, p0, Lbva;->d:F

    .line 340
    iput v1, p0, Lbva;->e:F

    .line 343
    iput v1, p0, Lbva;->f:F

    .line 346
    iput v1, p0, Lbva;->g:F

    .line 349
    iput v1, p0, Lbva;->h:F

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 391
    const/4 v0, 0x0

    .line 392
    iget-object v1, p0, Lbva;->b:Lbvb;

    if-eqz v1, :cond_0

    .line 393
    const/4 v0, 0x1

    iget-object v1, p0, Lbva;->b:Lbvb;

    .line 394
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 396
    :cond_0
    iget v1, p0, Lbva;->c:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_1

    .line 397
    const/4 v1, 0x2

    iget v2, p0, Lbva;->c:F

    .line 398
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 400
    :cond_1
    iget v1, p0, Lbva;->d:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_2

    .line 401
    const/4 v1, 0x3

    iget v2, p0, Lbva;->d:F

    .line 402
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 404
    :cond_2
    iget v1, p0, Lbva;->e:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_3

    .line 405
    const/4 v1, 0x4

    iget v2, p0, Lbva;->e:F

    .line 406
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 408
    :cond_3
    iget v1, p0, Lbva;->f:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_4

    .line 409
    const/4 v1, 0x5

    iget v2, p0, Lbva;->f:F

    .line 410
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 412
    :cond_4
    iget v1, p0, Lbva;->g:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_5

    .line 413
    const/4 v1, 0x6

    iget v2, p0, Lbva;->g:F

    .line 414
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 416
    :cond_5
    iget v1, p0, Lbva;->h:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_6

    .line 417
    const/4 v1, 0x7

    iget v2, p0, Lbva;->h:F

    .line 418
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 420
    :cond_6
    iput v0, p0, Lbva;->ai:I

    .line 421
    return v0
.end method

.method public a(Loxn;)Lbva;
    .locals 1

    .prologue
    .line 429
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 430
    sparse-switch v0, :sswitch_data_0

    .line 434
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 435
    :sswitch_0
    return-object p0

    .line 440
    :sswitch_1
    iget-object v0, p0, Lbva;->b:Lbvb;

    if-nez v0, :cond_1

    .line 441
    new-instance v0, Lbvb;

    invoke-direct {v0}, Lbvb;-><init>()V

    iput-object v0, p0, Lbva;->b:Lbvb;

    .line 443
    :cond_1
    iget-object v0, p0, Lbva;->b:Lbvb;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 447
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbva;->c:F

    goto :goto_0

    .line 451
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbva;->d:F

    goto :goto_0

    .line 455
    :sswitch_4
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbva;->e:F

    goto :goto_0

    .line 459
    :sswitch_5
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbva;->f:F

    goto :goto_0

    .line 463
    :sswitch_6
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbva;->g:F

    goto :goto_0

    .line 467
    :sswitch_7
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbva;->h:F

    goto :goto_0

    .line 430
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
        0x3d -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 366
    iget-object v0, p0, Lbva;->b:Lbvb;

    if-eqz v0, :cond_0

    .line 367
    const/4 v0, 0x1

    iget-object v1, p0, Lbva;->b:Lbvb;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 369
    :cond_0
    iget v0, p0, Lbva;->c:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_1

    .line 370
    const/4 v0, 0x2

    iget v1, p0, Lbva;->c:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 372
    :cond_1
    iget v0, p0, Lbva;->d:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_2

    .line 373
    const/4 v0, 0x3

    iget v1, p0, Lbva;->d:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 375
    :cond_2
    iget v0, p0, Lbva;->e:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_3

    .line 376
    const/4 v0, 0x4

    iget v1, p0, Lbva;->e:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 378
    :cond_3
    iget v0, p0, Lbva;->f:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_4

    .line 379
    const/4 v0, 0x5

    iget v1, p0, Lbva;->f:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 381
    :cond_4
    iget v0, p0, Lbva;->g:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_5

    .line 382
    const/4 v0, 0x6

    iget v1, p0, Lbva;->g:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 384
    :cond_5
    iget v0, p0, Lbva;->h:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_6

    .line 385
    const/4 v0, 0x7

    iget v1, p0, Lbva;->h:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 387
    :cond_6
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lbva;->a(Loxn;)Lbva;

    move-result-object v0

    return-object v0
.end method
