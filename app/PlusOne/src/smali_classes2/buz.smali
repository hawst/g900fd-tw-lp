.class public final Lbuz;
.super Loxu;
.source "PG"


# instance fields
.field public a:F

.field public b:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2841
    invoke-direct {p0}, Loxu;-><init>()V

    .line 2844
    const/4 v0, 0x0

    iput v0, p0, Lbuz;->a:F

    .line 2847
    sget-object v0, Loxx;->f:[B

    iput-object v0, p0, Lbuz;->b:[B

    .line 2841
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 2869
    const/4 v0, 0x0

    .line 2870
    iget v1, p0, Lbuz;->a:F

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_0

    .line 2871
    const/4 v0, 0x1

    iget v1, p0, Lbuz;->a:F

    .line 2872
    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 2874
    :cond_0
    iget-object v1, p0, Lbuz;->b:[B

    sget-object v2, Loxx;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2875
    const/4 v1, 0x2

    iget-object v2, p0, Lbuz;->b:[B

    .line 2876
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 2878
    :cond_1
    iput v0, p0, Lbuz;->ai:I

    .line 2879
    return v0
.end method

.method public a(Loxn;)Lbuz;
    .locals 1

    .prologue
    .line 2887
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2888
    sparse-switch v0, :sswitch_data_0

    .line 2892
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2893
    :sswitch_0
    return-object p0

    .line 2898
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbuz;->a:F

    goto :goto_0

    .line 2902
    :sswitch_2
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lbuz;->b:[B

    goto :goto_0

    .line 2888
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 2859
    iget v0, p0, Lbuz;->a:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 2860
    const/4 v0, 0x1

    iget v1, p0, Lbuz;->a:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 2862
    :cond_0
    iget-object v0, p0, Lbuz;->b:[B

    sget-object v1, Loxx;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2863
    const/4 v0, 0x2

    iget-object v1, p0, Lbuz;->b:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 2865
    :cond_1
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2837
    invoke-virtual {p0, p1}, Lbuz;->a(Loxn;)Lbuz;

    move-result-object v0

    return-object v0
.end method
