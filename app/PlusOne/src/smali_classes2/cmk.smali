.class final Lcmk;
.super Landroid/os/Handler;
.source "PG"


# static fields
.field private static final a:I

.field private static final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Landroid/app/Activity;

.field private final d:Lcmj;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcmd;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z

.field private g:Z

.field private h:J

.field private final i:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcmd;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private j:J

.field private k:Landroid/view/MotionEvent;

.field private l:J

.field private m:J

.field private n:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcmd;",
            ">;"
        }
    .end annotation
.end field

.field private o:Lcmd;

.field private p:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 89
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    .line 90
    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    long-to-int v0, v0

    div-int/lit8 v0, v0, 0x5a

    sput v0, Lcmk;->a:I

    .line 92
    new-array v0, v6, [Ljava/lang/Integer;

    const/4 v1, 0x0

    .line 94
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    const/4 v1, 0x2

    .line 95
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    .line 93
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 92
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcmk;->b:Ljava/util/List;

    return-void
.end method

.method constructor <init>(Landroid/app/Activity;Lcmj;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcmj;",
            "Ljava/util/List",
            "<",
            "Lcmd;",
            ">;)V"
        }
    .end annotation

    .prologue
    const-wide/16 v2, -0x1

    .line 118
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 102
    iput-wide v2, p0, Lcmk;->h:J

    .line 108
    iput-wide v2, p0, Lcmk;->j:J

    .line 110
    iput-wide v2, p0, Lcmk;->l:J

    .line 111
    iput-wide v2, p0, Lcmk;->m:J

    .line 119
    iput-object p1, p0, Lcmk;->c:Landroid/app/Activity;

    .line 120
    iput-object p2, p0, Lcmk;->d:Lcmj;

    .line 121
    iput-object p3, p0, Lcmk;->e:Ljava/util/List;

    .line 122
    new-instance v0, Ljava/util/HashMap;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcmk;->i:Ljava/util/Map;

    .line 123
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmd;

    .line 124
    iget-object v2, p0, Lcmk;->i:Ljava/util/Map;

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 126
    :cond_0
    return-void
.end method

.method private a()V
    .locals 6

    .prologue
    .line 182
    const/4 v1, 0x0

    .line 183
    iget-object v0, p0, Lcmk;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmd;

    .line 184
    invoke-direct {p0, v0}, Lcmk;->a(Lcmd;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 185
    const/4 v0, 0x1

    .line 190
    :goto_0
    iget-wide v2, p0, Lcmk;->m:J

    iget-wide v4, p0, Lcmk;->l:J

    cmp-long v1, v2, v4

    if-gtz v1, :cond_1

    if-eqz v0, :cond_2

    .line 191
    :cond_1
    const/4 v0, 0x2

    sget v1, Lcmk;->a:I

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcmk;->sendEmptyMessageDelayed(IJ)Z

    .line 195
    :goto_1
    return-void

    .line 193
    :cond_2
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcmk;->sendEmptyMessage(I)Z

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method private a(Lcmd;)Z
    .locals 2

    .prologue
    .line 239
    sget-object v0, Lcmk;->b:Ljava/util/List;

    iget-object v1, p0, Lcmk;->i:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 6

    .prologue
    .line 210
    iget-object v0, p0, Lcmk;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmd;

    .line 211
    invoke-virtual {v0}, Lcmd;->b()J

    move-result-wide v2

    iget-wide v4, p0, Lcmk;->l:J

    cmp-long v2, v2, v4

    if-gtz v2, :cond_2

    iget-wide v2, p0, Lcmk;->l:J

    invoke-virtual {v0}, Lcmd;->c()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gtz v2, :cond_2

    .line 212
    invoke-direct {p0, v0}, Lcmk;->a(Lcmd;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 213
    iget-object v2, p0, Lcmk;->n:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 214
    :cond_1
    iget-object v2, p0, Lcmk;->o:Lcmd;

    if-nez v2, :cond_0

    .line 215
    iput-object v0, p0, Lcmk;->o:Lcmd;

    .line 216
    const/4 v0, 0x0

    iput v0, p0, Lcmk;->p:I

    goto :goto_0

    .line 218
    :cond_2
    invoke-direct {p0, v0}, Lcmk;->a(Lcmd;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcmk;->o:Lcmd;

    if-nez v2, :cond_0

    .line 219
    iput-object v0, p0, Lcmk;->o:Lcmd;

    .line 220
    const/4 v0, 0x1

    iput v0, p0, Lcmk;->p:I

    goto :goto_0

    .line 223
    :cond_3
    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 226
    iget-object v0, p0, Lcmk;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmd;

    .line 227
    invoke-direct {p0, v0}, Lcmk;->a(Lcmd;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 228
    iget-object v2, p0, Lcmk;->o:Lcmd;

    if-nez v2, :cond_1

    .line 229
    iput-object v0, p0, Lcmk;->o:Lcmd;

    .line 230
    const/4 v0, 0x1

    iput v0, p0, Lcmk;->p:I

    goto :goto_0

    .line 232
    :cond_1
    iget-object v2, p0, Lcmk;->n:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 236
    :cond_2
    return-void
.end method

.method private d()V
    .locals 30

    .prologue
    .line 254
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcmk;->j:J

    const-wide/16 v8, -0x1

    cmp-long v6, v6, v8

    if-nez v6, :cond_0

    .line 255
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcmk;->l:J

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcmk;->j:J

    .line 259
    :cond_0
    move-object/from16 v0, p0

    iget-object v6, v0, Lcmk;->n:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    if-lez v6, :cond_1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcmk;->o:Lcmd;

    if-eqz v6, :cond_1

    .line 260
    move-object/from16 v0, p0

    iget v6, v0, Lcmk;->p:I

    if-nez v6, :cond_5

    const/4 v6, 0x5

    :goto_0
    move-object/from16 v0, p0

    iput v6, v0, Lcmk;->p:I

    .line 266
    :cond_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcmk;->o:Lcmd;

    if-eqz v6, :cond_6

    .line 267
    move-object/from16 v0, p0

    iget v10, v0, Lcmk;->p:I

    .line 268
    move-object/from16 v0, p0

    iget v6, v0, Lcmk;->p:I

    const/4 v7, 0x6

    if-eq v6, v7, :cond_2

    move-object/from16 v0, p0

    iget v6, v0, Lcmk;->p:I

    const/4 v7, 0x5

    if-ne v6, v7, :cond_3

    .line 271
    :cond_2
    move-object/from16 v0, p0

    iget-object v6, v0, Lcmk;->n:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    shl-int/lit8 v6, v6, 0x8

    or-int/2addr v10, v6

    .line 278
    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcmk;->n:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v11

    .line 279
    move-object/from16 v0, p0

    iget-object v6, v0, Lcmk;->o:Lcmd;

    if-eqz v6, :cond_4

    .line 280
    add-int/lit8 v11, v11, 0x1

    .line 282
    :cond_4
    new-array v12, v11, [Landroid/view/MotionEvent$PointerProperties;

    .line 284
    new-array v13, v11, [Landroid/view/MotionEvent$PointerCoords;

    .line 286
    const/4 v6, 0x0

    .line 287
    move-object/from16 v0, p0

    iget-object v7, v0, Lcmk;->n:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v7, v6

    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcmd;

    .line 288
    invoke-virtual {v6}, Lcmd;->d()Landroid/view/MotionEvent$PointerProperties;

    move-result-object v9

    aput-object v9, v12, v7

    .line 289
    move-object/from16 v0, p0

    iget-wide v14, v0, Lcmk;->l:J

    invoke-virtual {v6, v14, v15}, Lcmd;->a(J)Landroid/view/MotionEvent$PointerCoords;

    move-result-object v9

    aput-object v9, v13, v7

    .line 290
    move-object/from16 v0, p0

    iget-object v9, v0, Lcmk;->i:Ljava/util/Map;

    const/4 v14, 0x2

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    invoke-interface {v9, v6, v14}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 291
    add-int/lit8 v6, v7, 0x1

    move v7, v6

    .line 292
    goto :goto_2

    .line 260
    :cond_5
    const/4 v6, 0x6

    goto :goto_0

    .line 274
    :cond_6
    const/4 v10, 0x2

    goto :goto_1

    .line 293
    :cond_7
    move-object/from16 v0, p0

    iget-object v6, v0, Lcmk;->o:Lcmd;

    if-eqz v6, :cond_8

    .line 294
    move-object/from16 v0, p0

    iget-object v6, v0, Lcmk;->o:Lcmd;

    invoke-virtual {v6}, Lcmd;->d()Landroid/view/MotionEvent$PointerProperties;

    move-result-object v6

    aput-object v6, v12, v7

    .line 295
    move-object/from16 v0, p0

    iget-object v6, v0, Lcmk;->o:Lcmd;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcmk;->l:J

    invoke-virtual {v6, v8, v9}, Lcmd;->a(J)Landroid/view/MotionEvent$PointerCoords;

    move-result-object v6

    aput-object v6, v13, v7

    .line 296
    move-object/from16 v0, p0

    iget-object v6, v0, Lcmk;->i:Ljava/util/Map;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcmk;->o:Lcmd;

    move-object/from16 v0, p0

    iget v8, v0, Lcmk;->p:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v6, v7, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    :cond_8
    move-object/from16 v0, p0

    iget-wide v6, v0, Lcmk;->h:J

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcmk;->j:J

    add-long/2addr v6, v8

    .line 300
    move-object/from16 v0, p0

    iget-wide v8, v0, Lcmk;->h:J

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcmk;->l:J

    add-long/2addr v8, v14

    .line 302
    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    sget v18, Lcmi;->a:I

    .line 307
    const/16 v20, 0x0

    array-length v0, v13

    move/from16 v22, v0

    const/16 v19, 0x0

    move/from16 v21, v19

    move/from16 v19, v20

    :goto_3
    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_d

    aget-object v23, v13, v21

    const/16 v20, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcmk;->c:Landroid/app/Activity;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v24

    new-instance v25, Landroid/graphics/RectF;

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move/from16 v26, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/MotionEvent$PointerCoords;->touchMinor:F

    move/from16 v27, v0

    sub-float v26, v26, v27

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    move/from16 v27, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/MotionEvent$PointerCoords;->touchMinor:F

    move/from16 v28, v0

    sub-float v27, v27, v28

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/MotionEvent$PointerCoords;->x:F

    move/from16 v28, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/MotionEvent$PointerCoords;->touchMinor:F

    move/from16 v29, v0

    add-float v28, v28, v29

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/MotionEvent$PointerCoords;->y:F

    move/from16 v29, v0

    move-object/from16 v0, v23

    iget v0, v0, Landroid/view/MotionEvent$PointerCoords;->touchMinor:F

    move/from16 v23, v0

    add-float v23, v23, v29

    move-object/from16 v0, v25

    move/from16 v1, v26

    move/from16 v2, v27

    move/from16 v3, v28

    move/from16 v4, v23

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v23, v0

    const/16 v26, 0x0

    cmpg-float v23, v23, v26

    if-gez v23, :cond_b

    const/16 v23, 0x0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v26, v0

    cmpg-float v23, v23, v26

    if-gez v23, :cond_b

    const/16 v20, 0x4

    :cond_9
    :goto_4
    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v23, v0

    const/16 v26, 0x0

    cmpg-float v23, v23, v26

    if-gez v23, :cond_c

    const/16 v23, 0x0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v26, v0

    cmpg-float v23, v23, v26

    if-gez v23, :cond_c

    or-int/lit8 v20, v20, 0x1

    :cond_a
    :goto_5
    or-int v20, v20, v19

    add-int/lit8 v19, v21, 0x1

    move/from16 v21, v19

    move/from16 v19, v20

    goto/16 :goto_3

    :cond_b
    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/RectF;->left:F

    move/from16 v23, v0

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getRight()I

    move-result v26

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    cmpg-float v23, v23, v26

    if-gez v23, :cond_9

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getRight()I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/RectF;->right:F

    move/from16 v26, v0

    cmpg-float v23, v23, v26

    if-gez v23, :cond_9

    const/16 v20, 0x8

    goto :goto_4

    :cond_c
    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/RectF;->top:F

    move/from16 v23, v0

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getBottom()I

    move-result v26

    move/from16 v0, v26

    int-to-float v0, v0

    move/from16 v26, v0

    cmpg-float v23, v23, v26

    if-gez v23, :cond_a

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getBottom()I

    move-result v23

    move/from16 v0, v23

    int-to-float v0, v0

    move/from16 v23, v0

    move-object/from16 v0, v25

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    move/from16 v24, v0

    cmpg-float v23, v23, v24

    if-gez v23, :cond_a

    or-int/lit8 v20, v20, 0x2

    goto :goto_5

    :cond_d
    const/16 v20, 0x2

    const/16 v21, 0x0

    .line 302
    invoke-static/range {v6 .. v21}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v6

    move-object/from16 v0, p0

    iput-object v6, v0, Lcmk;->k:Landroid/view/MotionEvent;

    .line 310
    return-void
.end method


# virtual methods
.method a(Z)V
    .locals 2

    .prologue
    .line 129
    if-eqz p1, :cond_0

    .line 130
    iput-boolean p1, p0, Lcmk;->g:Z

    .line 132
    :cond_0
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcmk;->m:J

    .line 133
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcmk;->f:Z

    .line 134
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 139
    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 140
    iget-boolean v0, p0, Lcmk;->g:Z

    if-nez v0, :cond_0

    .line 141
    iget-object v0, p0, Lcmk;->d:Lcmj;

    iget-boolean v1, p0, Lcmk;->f:Z

    invoke-interface {v0}, Lcmj;->a()V

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    if-ne v0, v8, :cond_3

    iput-wide v2, p0, Lcmk;->h:J

    iget-object v0, p0, Lcmk;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmd;

    invoke-virtual {v0}, Lcmd;->c()J

    move-result-wide v4

    iget-wide v6, p0, Lcmk;->m:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    invoke-virtual {v0}, Lcmd;->c()J

    move-result-wide v4

    iput-wide v4, p0, Lcmk;->m:J

    goto :goto_1

    :cond_3
    iget-wide v0, p0, Lcmk;->h:J

    sub-long v0, v2, v0

    iput-wide v0, p0, Lcmk;->l:J

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcmk;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcmk;->n:Ljava/util/List;

    iput-object v9, p0, Lcmk;->o:Lcmd;

    const/4 v0, 0x4

    iput v0, p0, Lcmk;->p:I

    .line 147
    iget-boolean v0, p0, Lcmk;->f:Z

    if-eqz v0, :cond_6

    .line 148
    invoke-direct {p0}, Lcmk;->c()V

    .line 152
    :goto_2
    iget-object v0, p0, Lcmk;->n:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gtz v0, :cond_4

    iget-object v0, p0, Lcmk;->o:Lcmd;

    if-eqz v0, :cond_5

    .line 153
    :cond_4
    invoke-direct {p0}, Lcmk;->d()V

    iget-object v0, p0, Lcmk;->c:Landroid/app/Activity;

    iget-object v1, p0, Lcmk;->k:Landroid/view/MotionEvent;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    iget-object v0, p0, Lcmk;->k:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    iput-object v9, p0, Lcmk;->k:Landroid/view/MotionEvent;

    .line 154
    iget v0, p0, Lcmk;->p:I

    if-ne v0, v8, :cond_5

    .line 155
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcmk;->j:J

    .line 159
    :cond_5
    invoke-direct {p0}, Lcmk;->a()V

    goto :goto_0

    .line 150
    :cond_6
    invoke-direct {p0}, Lcmk;->b()V

    goto :goto_2
.end method
