.class public final Lzw;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static a:J

.field static b:Ljava/util/concurrent/locks/ReentrantLock;


# instance fields
.field private c:Laaf;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 66
    const-wide/16 v0, 0x5

    sput-wide v0, Lzw;->a:J

    .line 72
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lzw;->b:Ljava/util/concurrent/locks/ReentrantLock;

    return-void
.end method

.method constructor <init>(Lacs;)V
    .locals 1

    .prologue
    .line 1884
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lzw;->c:Laaf;

    .line 1885
    new-instance v0, Laaf;

    invoke-direct {v0, p0, p1}, Laaf;-><init>(Lzw;Lacs;)V

    iput-object v0, p0, Lzw;->c:Laaf;

    .line 1886
    return-void
.end method

.method public static b()I
    .locals 1

    .prologue
    .line 1881
    invoke-static {}, Landroid/hardware/Camera;->getNumberOfCameras()I

    move-result v0

    return v0
.end method

.method static c()Z
    .locals 2

    .prologue
    .line 1899
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 1737
    iget-object v0, p0, Lzw;->c:Laaf;

    const/4 v1, 0x3

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Laaf;->a(IZ)V

    .line 1738
    return-void
.end method

.method public a(Laai;)V
    .locals 1

    .prologue
    .line 1776
    iget-object v0, p0, Lzw;->c:Laaf;

    invoke-virtual {v0}, Laaf;->e()Lzx;

    move-result-object v0

    invoke-virtual {v0, p1}, Lzx;->a(Laai;)V

    .line 1777
    return-void
.end method

.method public a(Laas;)Z
    .locals 1

    .prologue
    .line 1798
    iget-object v0, p0, Lzw;->c:Laaf;

    invoke-virtual {v0, p1}, Laaf;->a(Laas;)Z

    move-result v0

    return v0
.end method

.method public b(Laai;)V
    .locals 1

    .prologue
    .line 1784
    iget-object v0, p0, Lzw;->c:Laaf;

    invoke-virtual {v0}, Laaf;->e()Lzx;

    move-result-object v0

    invoke-virtual {v0, p1}, Lzx;->b(Laai;)V

    .line 1785
    return-void
.end method

.method d()V
    .locals 3

    .prologue
    .line 1903
    iget-object v0, p0, Lzw;->c:Laaf;

    const/4 v1, 0x7

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Laaf;->a(IZ)V

    .line 1904
    return-void
.end method
