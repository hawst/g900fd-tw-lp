.class public final Lbxd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbxe;


# instance fields
.field private a:Lceb;

.field private b:Lceb;

.field private c:I

.field private d:I

.field private e:[F

.field private f:[F

.field private g:[F

.field private h:[F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    iput-object v0, p0, Lbxd;->a:Lceb;

    .line 20
    iput-object v0, p0, Lbxd;->b:Lceb;

    return-void
.end method

.method private a(Lbmn;[F)V
    .locals 3

    .prologue
    .line 113
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lbxd;->c:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lbxd;->d:I

    mul-int/2addr v1, v2

    if-ge v0, v1, :cond_0

    .line 114
    invoke-virtual {p1, v0}, Lbmn;->a(I)F

    move-result v1

    aput v1, p2, v0

    .line 113
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 116
    :cond_0
    return-void
.end method

.method private b(Lbmn;[F)V
    .locals 3

    .prologue
    .line 119
    iget v0, p0, Lbxd;->c:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lbxd;->d:I

    mul-int/2addr v1, v0

    .line 120
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lbxd;->d:I

    if-ge v0, v2, :cond_0

    .line 121
    add-int v2, v1, v0

    invoke-virtual {p1, v2}, Lbmn;->a(I)F

    move-result v2

    aput v2, p2, v0

    .line 120
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lbmn;Lbmn;)F
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 58
    iget-object v0, p0, Lbxd;->a:Lceb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbxd;->b:Lceb;

    if-nez v0, :cond_1

    .line 59
    :cond_0
    invoke-virtual {p1}, Lbmn;->c()I

    move-result v0

    iput v0, p0, Lbxd;->c:I

    .line 60
    invoke-virtual {p1}, Lbmn;->d()I

    move-result v0

    iput v0, p0, Lbxd;->d:I

    .line 62
    iget v0, p0, Lbxd;->c:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lbxd;->d:I

    mul-int/2addr v0, v1

    new-array v0, v0, [F

    iput-object v0, p0, Lbxd;->e:[F

    .line 63
    iget v0, p0, Lbxd;->c:I

    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lbxd;->d:I

    mul-int/2addr v0, v1

    new-array v0, v0, [F

    iput-object v0, p0, Lbxd;->f:[F

    .line 64
    iget v0, p0, Lbxd;->d:I

    new-array v0, v0, [F

    iput-object v0, p0, Lbxd;->g:[F

    .line 65
    iget v0, p0, Lbxd;->d:I

    new-array v0, v0, [F

    iput-object v0, p0, Lbxd;->h:[F

    .line 67
    new-instance v0, Lceb;

    iget v1, p0, Lbxd;->c:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lbxd;->d:I

    invoke-direct {v0, v1, v2, v3, v4}, Lceb;-><init>(IIZZ)V

    iput-object v0, p0, Lbxd;->a:Lceb;

    .line 69
    new-instance v0, Lceb;

    iget v1, p0, Lbxd;->d:I

    invoke-direct {v0, v4, v1, v3, v3}, Lceb;-><init>(IIZZ)V

    iput-object v0, p0, Lbxd;->b:Lceb;

    .line 76
    :goto_0
    iget-object v0, p0, Lbxd;->e:[F

    invoke-direct {p0, p1, v0}, Lbxd;->a(Lbmn;[F)V

    .line 77
    iget-object v0, p0, Lbxd;->f:[F

    invoke-direct {p0, p2, v0}, Lbxd;->a(Lbmn;[F)V

    .line 79
    iget-object v0, p0, Lbxd;->g:[F

    invoke-direct {p0, p1, v0}, Lbxd;->b(Lbmn;[F)V

    .line 80
    iget-object v0, p0, Lbxd;->h:[F

    invoke-direct {p0, p2, v0}, Lbxd;->b(Lbmn;[F)V

    .line 82
    iget-object v0, p0, Lbxd;->a:Lceb;

    iget-object v1, p0, Lbxd;->e:[F

    iget-object v2, p0, Lbxd;->f:[F

    .line 83
    invoke-virtual {v0, v1, v2}, Lceb;->a([F[F)F

    move-result v0

    .line 84
    iget-object v1, p0, Lbxd;->b:Lceb;

    iget-object v2, p0, Lbxd;->g:[F

    iget-object v3, p0, Lbxd;->h:[F

    invoke-virtual {v1, v2, v3}, Lceb;->a([F[F)F

    move-result v1

    .line 89
    add-float/2addr v0, v1

    return v0

    .line 71
    :cond_1
    iget v0, p0, Lbxd;->c:I

    const-string v1, "mMatrixRows"

    invoke-virtual {p1}, Lbmn;->c()I

    move-result v2

    const-string v3, "matrixA.getRows()"

    invoke-static {v0, v1, v2, v3}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 72
    iget v0, p0, Lbxd;->d:I

    const-string v1, "mMatrixColumns"

    .line 73
    invoke-virtual {p1}, Lbmn;->d()I

    move-result v2

    const-string v3, "matrixA.getColumns()"

    .line 72
    invoke-static {v0, v1, v2, v3}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(Lbok;Lbmr;Lbok;Lbmr;)F
    .locals 3

    .prologue
    .line 48
    invoke-virtual {p2}, Lbmr;->c()Lbky;

    move-result-object v0

    sget-object v1, Lbkz;->j:Lbkz;

    invoke-virtual {v0, v1}, Lbky;->a(Lbkz;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmn;

    .line 50
    invoke-virtual {p4}, Lbmr;->c()Lbky;

    move-result-object v1

    sget-object v2, Lbkz;->j:Lbkz;

    invoke-virtual {v1, v2}, Lbky;->a(Lbkz;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmn;

    .line 53
    invoke-virtual {p0, v0, v1}, Lbxd;->a(Lbmn;Lbmn;)F

    move-result v0

    return v0
.end method

.method public b(Lbmn;Lbmn;)Z
    .locals 2

    .prologue
    .line 105
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Lbmn;->c()I

    move-result v0

    invoke-virtual {p2}, Lbmn;->c()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 106
    invoke-virtual {p1}, Lbmn;->d()I

    move-result v0

    invoke-virtual {p2}, Lbmn;->d()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lbok;Lbmr;Lbok;Lbmr;)Z
    .locals 3

    .prologue
    .line 96
    invoke-virtual {p2}, Lbmr;->c()Lbky;

    move-result-object v0

    sget-object v1, Lbkz;->j:Lbkz;

    invoke-virtual {v0, v1}, Lbky;->a(Lbkz;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmn;

    .line 98
    invoke-virtual {p4}, Lbmr;->c()Lbky;

    move-result-object v1

    sget-object v2, Lbkz;->j:Lbkz;

    invoke-virtual {v1, v2}, Lbky;->a(Lbkz;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmn;

    .line 101
    invoke-virtual {p0, v0, v1}, Lbxd;->b(Lbmn;Lbmn;)Z

    move-result v0

    return v0
.end method
