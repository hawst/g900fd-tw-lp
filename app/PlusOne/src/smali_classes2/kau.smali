.class Lkau;
.super Lhny;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:I

.field private final d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lizu;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lkau;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lkau;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILizu;)V
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lkau;->a:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 27
    iput-object p1, p0, Lkau;->b:Landroid/content/Context;

    .line 28
    iput p2, p0, Lkau;->c:I

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkau;->d:Ljava/util/ArrayList;

    .line 30
    iget-object v0, p0, Lkau;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 31
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 4

    .prologue
    .line 35
    iget-object v0, p0, Lkau;->b:Landroid/content/Context;

    iget v1, p0, Lkau;->c:I

    iget-object v2, p0, Lkau;->d:Ljava/util/ArrayList;

    invoke-static {v0, v1, v2}, Ljvj;->b(Landroid/content/Context;ILjava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 36
    new-instance v1, Lhoz;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lhoz;-><init>(Z)V

    .line 37
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "media_refs_with_photo_ids"

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 39
    return-object v1
.end method
