.class public final Lbyq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lbmd;Lbmd;Lbmd;Lont;Lont;Lont;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lbyq;->a:Ljava/util/List;

    .line 113
    iget-object v2, p1, Lbmd;->d:Lbmg;

    sget-object v3, Lbmg;->a:Lbmg;

    if-ne v2, v3, :cond_1

    .line 114
    iget-object v0, p0, Lbyq;->a:Ljava/util/List;

    const-string v1, "V"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 117
    :cond_1
    invoke-direct {p0, p1, p2, p4, p5}, Lbyq;->a(Lbmd;Lbmd;Lont;Lont;)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-direct {p0, p1, p3, p4, p6}, Lbyq;->a(Lbmd;Lbmd;Lont;Lont;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p4, Lont;->c:Lonv;

    if-eqz v2, :cond_3

    iget-object v2, p4, Lont;->c:Lonv;

    iget v2, v2, Lonv;->a:I

    if-ne v2, v0, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_4

    .line 118
    iget-object v0, p0, Lbyq;->a:Ljava/util/List;

    const-string v1, "B"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move v2, v1

    .line 117
    goto :goto_1

    :cond_3
    move v2, v1

    goto :goto_1

    .line 121
    :cond_4
    invoke-direct {p0, p1, p2, p4, p5}, Lbyq;->a(Lbmd;Lbmd;Lont;Lont;)Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v2, p4, Lont;->c:Lonv;

    if-eqz v2, :cond_6

    iget-object v2, p4, Lont;->c:Lonv;

    iget v2, v2, Lonv;->a:I

    if-ne v2, v0, :cond_5

    move v2, v0

    :goto_2
    if-eqz v2, :cond_7

    .line 122
    iget-object v0, p0, Lbyq;->a:Ljava/util/List;

    const-string v1, "b"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    move v2, v1

    .line 121
    goto :goto_2

    :cond_6
    move v2, v1

    goto :goto_2

    .line 125
    :cond_7
    invoke-direct {p0, p1, p2, p4, p5}, Lbyq;->a(Lbmd;Lbmd;Lont;Lont;)Z

    move-result v2

    if-nez v2, :cond_9

    invoke-direct {p0, p1, p3, p4, p6}, Lbyq;->a(Lbmd;Lbmd;Lont;Lont;)Z

    move-result v2

    if-eqz v2, :cond_9

    iget-object v2, p4, Lont;->c:Lonv;

    if-eqz v2, :cond_9

    iget-object v2, p4, Lont;->c:Lonv;

    iget v2, v2, Lonv;->a:I

    if-ne v2, v4, :cond_8

    move v2, v0

    :goto_3
    if-eqz v2, :cond_a

    .line 126
    iget-object v0, p0, Lbyq;->a:Ljava/util/List;

    const-string v1, "F"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_8
    move v2, v1

    .line 125
    goto :goto_3

    :cond_9
    move v2, v1

    goto :goto_3

    .line 129
    :cond_a
    invoke-direct {p0, p1, p2, p4, p5}, Lbyq;->a(Lbmd;Lbmd;Lont;Lont;)Z

    move-result v2

    if-eqz v2, :cond_c

    iget-object v2, p4, Lont;->c:Lonv;

    if-eqz v2, :cond_c

    iget-object v2, p4, Lont;->c:Lonv;

    iget v2, v2, Lonv;->a:I

    if-ne v2, v4, :cond_b

    :goto_4
    if-eqz v0, :cond_d

    .line 130
    iget-object v0, p0, Lbyq;->a:Ljava/util/List;

    const-string v1, "f"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_b
    move v0, v1

    .line 129
    goto :goto_4

    :cond_c
    move v0, v1

    goto :goto_4

    .line 133
    :cond_d
    iget-object v0, p1, Lbmd;->d:Lbmg;

    sget-object v1, Lbmg;->c:Lbmg;

    if-ne v0, v1, :cond_0

    .line 136
    iget-object v0, p0, Lbyq;->a:Ljava/util/List;

    const-string v1, "P"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137
    iget-object v0, p0, Lbyq;->a:Ljava/util/List;

    const-string v1, "Z2"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    iget-object v0, p0, Lbyq;->a:Ljava/util/List;

    const-string v1, "Z3"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 139
    iget-object v0, p4, Lont;->d:[Loob;

    array-length v0, v0

    .line 140
    if-lez v0, :cond_e

    .line 141
    iget-object v1, p0, Lbyq;->a:Ljava/util/List;

    const-string v2, "K1"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 143
    :cond_e
    if-ne v0, v4, :cond_f

    .line 144
    iget-object v1, p0, Lbyq;->a:Ljava/util/List;

    const-string v2, "K2"

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    :cond_f
    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 147
    iget-object v0, p0, Lbyq;->a:Ljava/util/List;

    const-string v1, "K3"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private a(Lbmd;Lbmd;Lont;Lont;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 39
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 52
    :cond_0
    :goto_0
    return v0

    .line 43
    :cond_1
    iget-object v1, p3, Lont;->c:Lonv;

    if-eqz v1, :cond_0

    iget-object v1, p4, Lont;->c:Lonv;

    if-eqz v1, :cond_0

    .line 44
    iget-object v1, p3, Lont;->c:Lonv;

    iget v1, v1, Lonv;->a:I

    iget-object v2, p4, Lont;->c:Lonv;

    iget v2, v2, Lonv;->a:I

    if-ne v1, v2, :cond_0

    .line 47
    iget-object v1, p3, Lont;->c:Lonv;

    iget-object v1, v1, Lonv;->b:Ljava/lang/Integer;

    iget-object v2, p4, Lont;->c:Lonv;

    iget-object v2, v2, Lonv;->b:Ljava/lang/Integer;

    if-ne v1, v2, :cond_0

    .line 50
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    iget-object v0, p0, Lbyq;->a:Ljava/util/List;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lbyq;->a:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
