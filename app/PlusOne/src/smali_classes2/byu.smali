.class public final Lbyu;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lbyu;

.field public static final b:Lbyu;


# instance fields
.field public final c:J

.field public final d:J

.field public final e:J

.field public final f:J

.field public final g:J

.field public final h:F

.field public final i:F

.field public final j:I

.field public final k:J

.field public final l:Z

.field public final m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const-wide/32 v8, 0xf4240

    const/4 v7, 0x0

    const/high16 v6, 0x40400000    # 3.0f

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 12
    new-instance v0, Lbyv;

    invoke-direct {v0}, Lbyv;-><init>()V

    const-wide/32 v2, 0x2dc6c0

    .line 14
    invoke-virtual {v0, v2, v3}, Lbyv;->a(J)Lbyv;

    move-result-object v0

    const-wide/32 v2, 0x1e8480

    .line 15
    invoke-virtual {v0, v2, v3}, Lbyv;->b(J)Lbyv;

    move-result-object v0

    const-wide/32 v2, 0x4c4b40

    .line 16
    invoke-virtual {v0, v2, v3}, Lbyv;->c(J)Lbyv;

    move-result-object v0

    const-wide/32 v2, 0x16e360

    .line 17
    invoke-virtual {v0, v2, v3}, Lbyv;->d(J)Lbyv;

    move-result-object v0

    .line 18
    invoke-virtual {v0, v8, v9}, Lbyv;->e(J)Lbyv;

    move-result-object v0

    .line 19
    invoke-virtual {v0, v6}, Lbyv;->a(F)Lbyv;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 20
    invoke-virtual {v0, v1}, Lbyv;->b(F)Lbyv;

    move-result-object v0

    const/4 v1, 0x2

    .line 21
    invoke-virtual {v0, v1}, Lbyv;->a(I)Lbyv;

    move-result-object v0

    .line 23
    invoke-virtual {v0, v8, v9}, Lbyv;->f(J)Lbyv;

    move-result-object v0

    .line 24
    invoke-virtual {v0, v4}, Lbyv;->a(Z)Lbyv;

    move-result-object v0

    .line 25
    invoke-virtual {v0, v4}, Lbyv;->b(Z)Lbyv;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lbyv;->a()Lbyu;

    move-result-object v0

    sput-object v0, Lbyu;->a:Lbyu;

    .line 29
    new-instance v0, Lbyv;

    invoke-direct {v0}, Lbyv;-><init>()V

    const-wide/32 v2, 0x13af10

    .line 39
    invoke-virtual {v0, v2, v3}, Lbyv;->a(J)Lbyv;

    move-result-object v0

    const-wide/32 v2, 0x61a80

    .line 40
    invoke-virtual {v0, v2, v3}, Lbyv;->b(J)Lbyv;

    move-result-object v0

    const-wide/32 v2, 0x4c4b40

    .line 42
    invoke-virtual {v0, v2, v3}, Lbyv;->c(J)Lbyv;

    move-result-object v0

    const-wide/32 v2, 0x8f0d180

    .line 44
    invoke-virtual {v0, v2, v3}, Lbyv;->d(J)Lbyv;

    move-result-object v0

    const-wide/32 v2, 0x8f0d180

    .line 46
    invoke-virtual {v0, v2, v3}, Lbyv;->e(J)Lbyv;

    move-result-object v0

    .line 47
    invoke-virtual {v0, v6}, Lbyv;->a(F)Lbyv;

    move-result-object v0

    .line 48
    invoke-virtual {v0, v4}, Lbyv;->a(I)Lbyv;

    move-result-object v0

    const-wide/32 v2, 0x249f0

    .line 49
    invoke-virtual {v0, v2, v3}, Lbyv;->f(J)Lbyv;

    move-result-object v0

    .line 50
    invoke-virtual {v0, v7}, Lbyv;->a(Z)Lbyv;

    move-result-object v0

    .line 51
    invoke-virtual {v0, v7}, Lbyv;->b(Z)Lbyv;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lbyv;->a()Lbyu;

    move-result-object v0

    sput-object v0, Lbyu;->b:Lbyu;

    .line 58
    sget-object v0, Lbyu;->a:Lbyu;

    .line 59
    invoke-virtual {v0}, Lbyu;->a()Lbyv;

    move-result-object v0

    .line 60
    invoke-virtual {v0, v5}, Lbyv;->a(F)Lbyv;

    move-result-object v0

    .line 61
    invoke-virtual {v0, v5}, Lbyv;->b(F)Lbyv;

    move-result-object v0

    .line 62
    invoke-virtual {v0}, Lbyv;->a()Lbyu;

    .line 58
    return-void
.end method

.method constructor <init>(Lbyv;)V
    .locals 2

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iget-wide v0, p1, Lbyv;->a:J

    iput-wide v0, p0, Lbyu;->c:J

    .line 109
    iget-wide v0, p1, Lbyv;->b:J

    iput-wide v0, p0, Lbyu;->d:J

    .line 110
    iget-wide v0, p1, Lbyv;->c:J

    iput-wide v0, p0, Lbyu;->e:J

    .line 111
    iget-wide v0, p1, Lbyv;->d:J

    iput-wide v0, p0, Lbyu;->f:J

    .line 112
    iget-wide v0, p1, Lbyv;->e:J

    iput-wide v0, p0, Lbyu;->g:J

    .line 113
    iget v0, p1, Lbyv;->f:F

    iput v0, p0, Lbyu;->h:F

    .line 114
    iget v0, p1, Lbyv;->g:F

    iput v0, p0, Lbyu;->i:F

    .line 115
    iget v0, p1, Lbyv;->h:I

    iput v0, p0, Lbyu;->j:I

    .line 117
    iget-wide v0, p1, Lbyv;->i:J

    iput-wide v0, p0, Lbyu;->k:J

    .line 118
    iget-boolean v0, p1, Lbyv;->j:Z

    iput-boolean v0, p0, Lbyu;->l:Z

    .line 119
    iget-boolean v0, p1, Lbyv;->k:Z

    iput-boolean v0, p0, Lbyu;->m:Z

    .line 120
    return-void
.end method


# virtual methods
.method public a()Lbyv;
    .locals 1

    .prologue
    .line 123
    new-instance v0, Lbyv;

    invoke-direct {v0, p0}, Lbyv;-><init>(Lbyu;)V

    return-object v0
.end method
