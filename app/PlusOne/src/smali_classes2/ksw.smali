.class public final Lksw;
.super Lloj;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lbc;
.implements Lhob;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lloj;",
        "Landroid/widget/AdapterView$OnItemClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lhob;"
    }
.end annotation


# static fields
.field private static final Q:[Ljava/lang/String;


# instance fields
.field private R:Lhee;

.field private S:Lhoc;

.field private T:Lksv;

.field private U:Lkxr;

.field private V:[Lktg;

.field private W:Z

.field private X:Landroid/view/View;

.field private Y:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 54
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "square_streams"

    aput-object v2, v0, v1

    sput-object v0, Lksw;->Q:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Lloj;-><init>()V

    .line 60
    new-instance v0, Lhoc;

    iget-object v1, p0, Lksw;->P:Llqm;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    .line 61
    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    move-result-object v0

    iput-object v0, p0, Lksw;->S:Lhoc;

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lksw;->W:Z

    return-void
.end method

.method public static a(Lkxr;)Lt;
    .locals 2

    .prologue
    .line 79
    if-nez p0, :cond_0

    .line 80
    const/4 v0, 0x0

    .line 88
    :goto_0
    return-object v0

    .line 83
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 84
    const-string v0, "square_target"

    invoke-virtual {v1, v0, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 86
    new-instance v0, Lksw;

    invoke-direct {v0}, Lksw;-><init>()V

    .line 87
    invoke-virtual {v0, v1}, Lksw;->f(Landroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method public U()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 173
    iget-object v0, p0, Lksw;->V:[Lktg;

    if-nez v0, :cond_2

    .line 174
    iget-boolean v0, p0, Lksw;->W:Z

    if-nez v0, :cond_1

    .line 175
    iput-boolean v1, p0, Lksw;->W:Z

    .line 176
    new-instance v0, Lkti;

    iget-object v1, p0, Lksw;->N:Llnl;

    iget-object v2, p0, Lksw;->R:Lhee;

    .line 177
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lksw;->U:Lkxr;

    invoke-virtual {v3}, Lkxr;->a()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lkti;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    .line 178
    iget-object v1, p0, Lksw;->S:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    const-string v0, "SquareCategoryPickerDialog"

    const-string v1, "Unknown Square or Missing Streams"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    iget-object v0, p0, Lksw;->T:Lksv;

    invoke-interface {v0}, Lksv;->e()V

    .line 182
    invoke-virtual {p0}, Lksw;->a()V

    goto :goto_0

    .line 184
    :cond_2
    iget-object v0, p0, Lksw;->V:[Lktg;

    array-length v0, v0

    if-ne v0, v1, :cond_3

    .line 185
    iget-object v6, p0, Lksw;->T:Lksv;

    new-instance v0, Lkxr;

    iget-object v1, p0, Lksw;->U:Lkxr;

    .line 186
    invoke-virtual {v1}, Lkxr;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lksw;->U:Lkxr;

    .line 187
    invoke-virtual {v2}, Lkxr;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lksw;->V:[Lktg;

    aget-object v3, v3, v4

    .line 188
    invoke-virtual {v3}, Lktg;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lksw;->V:[Lktg;

    aget-object v4, v5, v4

    .line 189
    invoke-virtual {v4}, Lktg;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lksw;->U:Lkxr;

    .line 190
    invoke-virtual {v5}, Lkxr;->e()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lkxr;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 185
    invoke-interface {v6, v0}, Lksv;->a(Lkxr;)V

    .line 191
    invoke-virtual {p0}, Lksw;->a()V

    goto :goto_0

    .line 193
    :cond_3
    iget-object v0, p0, Lksw;->Y:Landroid/widget/ArrayAdapter;

    if-nez v0, :cond_4

    .line 194
    new-instance v1, Landroid/widget/ListView;

    iget-object v0, p0, Lksw;->N:Llnl;

    invoke-direct {v1, v0}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 195
    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lksw;->N:Llnl;

    const v3, 0x1090003

    const v5, 0x1020014

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-direct {v0, v2, v3, v5, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;IILjava/util/List;)V

    iput-object v0, p0, Lksw;->Y:Landroid/widget/ArrayAdapter;

    .line 197
    iget-object v0, p0, Lksw;->Y:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 198
    invoke-virtual {v1, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 200
    iget-object v0, p0, Lksw;->X:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 201
    iget-object v2, p0, Lksw;->X:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v2

    .line 202
    iget-object v3, p0, Lksw;->X:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 203
    iput-object v1, p0, Lksw;->X:Landroid/view/View;

    .line 204
    iget-object v1, p0, Lksw;->X:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 207
    :cond_4
    iget-object v0, p0, Lksw;->Y:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->clear()V

    .line 208
    iget-object v1, p0, Lksw;->V:[Lktg;

    array-length v2, v1

    move v0, v4

    :goto_1
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 209
    iget-object v4, p0, Lksw;->Y:Landroid/widget/ArrayAdapter;

    invoke-virtual {v3}, Lktg;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 208
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 141
    packed-switch p1, :pswitch_data_0

    .line 146
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid Loader Id"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143
    :pswitch_0
    new-instance v0, Lktn;

    iget-object v1, p0, Lksw;->N:Llnl;

    iget-object v2, p0, Lksw;->R:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lksw;->U:Lkxr;

    .line 144
    invoke-virtual {v3}, Lkxr;->a()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lksw;->Q:[Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3, v4}, Lktn;-><init>(Landroid/content/Context;ILjava/lang/String;[Ljava/lang/String;)V

    return-object v0

    .line 141
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 170
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 152
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 167
    :goto_0
    return-void

    .line 154
    :pswitch_0
    const/4 v0, 0x0

    iput-object v0, p0, Lksw;->V:[Lktg;

    .line 155
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    const/4 v0, 0x0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    .line 157
    invoke-static {v0}, Lktg;->a([B)[Lktg;

    move-result-object v0

    iput-object v0, p0, Lksw;->V:[Lktg;

    .line 159
    :cond_0
    new-instance v0, Lksx;

    invoke-direct {v0, p0}, Lksx;-><init>(Lksw;)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 152
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 44
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lksw;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 1

    .prologue
    .line 217
    const-string v0, "GetSquareTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    invoke-virtual {p0}, Lksw;->U()V

    .line 220
    :cond_0
    return-void
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 100
    invoke-virtual {p0}, Lksw;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 101
    const-string v1, "square_target"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lkxr;

    iput-object v0, p0, Lksw;->U:Lkxr;

    .line 103
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lksw;->N:Llnl;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 104
    iget-object v1, p0, Lksw;->N:Llnl;

    const v2, 0x7f0a016a

    invoke-virtual {v1, v2}, Llnl;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 106
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 107
    new-instance v1, Landroid/widget/ProgressBar;

    iget-object v2, p0, Lksw;->N:Llnl;

    invoke-direct {v1, v2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lksw;->X:Landroid/view/View;

    .line 108
    iget-object v1, p0, Lksw;->X:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 109
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public g()V
    .locals 3

    .prologue
    .line 114
    invoke-super {p0}, Lloj;->g()V

    .line 115
    invoke-virtual {p0}, Lksw;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 116
    return-void
.end method

.method protected k(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 93
    invoke-super {p0, p1}, Lloj;->k(Landroid/os/Bundle;)V

    .line 94
    iget-object v0, p0, Lksw;->O:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lksw;->R:Lhee;

    .line 95
    iget-object v0, p0, Lksw;->O:Llnh;

    const-class v1, Lksv;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lksv;

    iput-object v0, p0, Lksw;->T:Lksv;

    .line 96
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lksw;->T:Lksv;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lksw;->T:Lksv;

    invoke-interface {v0}, Lksv;->e()V

    .line 137
    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 120
    iget-object v0, p0, Lksw;->T:Lksv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lksw;->V:[Lktg;

    if-eqz v0, :cond_0

    if-ltz p3, :cond_0

    iget-object v0, p0, Lksw;->V:[Lktg;

    array-length v0, v0

    if-le p3, v0, :cond_1

    .line 130
    :cond_0
    :goto_0
    return-void

    .line 124
    :cond_1
    iget-object v6, p0, Lksw;->T:Lksv;

    new-instance v0, Lkxr;

    iget-object v1, p0, Lksw;->U:Lkxr;

    .line 125
    invoke-virtual {v1}, Lkxr;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lksw;->U:Lkxr;

    .line 126
    invoke-virtual {v2}, Lkxr;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lksw;->V:[Lktg;

    aget-object v3, v3, p3

    .line 127
    invoke-virtual {v3}, Lktg;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lksw;->V:[Lktg;

    aget-object v4, v4, p3

    .line 128
    invoke-virtual {v4}, Lktg;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lksw;->U:Lkxr;

    .line 129
    invoke-virtual {v5}, Lkxr;->e()Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lkxr;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 124
    invoke-interface {v6, v0}, Lksv;->a(Lkxr;)V

    goto :goto_0
.end method
