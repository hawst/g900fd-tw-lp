.class public final Lbkp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lbmr;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:[I

.field private final c:I


# direct methods
.method constructor <init>(Landroid/util/SparseArray;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lbmr;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lbkp;->a:Landroid/util/SparseArray;

    .line 34
    iget-object v0, p0, Lbkp;->a:Landroid/util/SparseArray;

    invoke-static {v0}, Lbkp;->b(Landroid/util/SparseArray;)[I

    move-result-object v0

    iput-object v0, p0, Lbkp;->b:[I

    .line 35
    iget-object v0, p0, Lbkp;->a:Landroid/util/SparseArray;

    invoke-static {v0}, Lbkp;->a(Landroid/util/SparseArray;)I

    move-result v0

    iput v0, p0, Lbkp;->c:I

    .line 36
    return-void
.end method

.method public constructor <init>(Lbvf;)V
    .locals 1

    .prologue
    .line 39
    invoke-static {p1}, Lbkp;->b(Lbvf;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-direct {p0, v0}, Lbkp;-><init>(Landroid/util/SparseArray;)V

    .line 40
    return-void
.end method

.method private static a(Landroid/util/SparseArray;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lbmr;",
            ">;>;)I"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 162
    move v1, v0

    move v2, v0

    .line 163
    :goto_0
    invoke-virtual {p0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 164
    invoke-virtual {p0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/2addr v2, v0

    .line 163
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 166
    :cond_0
    return v2
.end method

.method static a(Ljava/util/List;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmr;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lbmp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 155
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmr;

    .line 156
    invoke-virtual {v0}, Lbmr;->a()Lbmp;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 158
    :cond_0
    return-object v1
.end method

.method private static b(Lbvf;)Landroid/util/SparseArray;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbvf;",
            ")",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lbmr;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 190
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    .line 191
    iget-object v3, p0, Lbvf;->d:[Lbve;

    .line 192
    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget-object v0, v3, v1

    .line 193
    new-instance v5, Lbmr;

    new-instance v6, Lbmp;

    iget-wide v8, v0, Lbve;->b:J

    iget-wide v10, v0, Lbve;->c:J

    invoke-direct {v6, v8, v9, v10, v11}, Lbmp;-><init>(JJ)V

    iget-object v7, v0, Lbve;->e:Lbuw;

    invoke-static {v7}, Lbky;->a(Lbuw;)Lbky;

    move-result-object v7

    iget v8, v0, Lbve;->d:I

    invoke-direct {v5, v6, v7, v8}, Lbmr;-><init>(Lbmp;Lbky;I)V

    .line 194
    iget v6, v0, Lbve;->d:I

    .line 195
    invoke-virtual {v2, v6}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 196
    if-nez v0, :cond_0

    .line 197
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 198
    invoke-virtual {v2, v6, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 200
    :cond_0
    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 192
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 202
    :cond_1
    return-object v2
.end method

.method private static b(Landroid/util/SparseArray;)[I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lbmr;",
            ">;>;)[I"
        }
    .end annotation

    .prologue
    .line 170
    invoke-virtual {p0}, Landroid/util/SparseArray;->size()I

    move-result v0

    new-array v1, v0, [I

    .line 171
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Landroid/util/SparseArray;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 172
    invoke-virtual {p0, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    aput v2, v1, v0

    .line 171
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 174
    :cond_0
    return-object v1
.end method


# virtual methods
.method public a(I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lbmr;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    iget-object v0, p0, Lbkp;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 50
    if-nez v0, :cond_0

    .line 51
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 53
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lbvf;)V
    .locals 6

    .prologue
    .line 178
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 180
    invoke-virtual {p0}, Lbkp;->a()[I

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_1

    aget v0, v3, v1

    .line 181
    invoke-virtual {p0, v0}, Lbkp;->a(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmr;

    .line 182
    invoke-virtual {v0}, Lbmr;->d()Lbve;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 180
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 186
    :cond_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lbve;

    .line 185
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbve;

    iput-object v0, p1, Lbvf;->d:[Lbve;

    .line 187
    return-void
.end method

.method public a()[I
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lbkp;->b:[I

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Lbkp;->c:I

    return v0
.end method

.method public b(I)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lbmp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p0, p1}, Lbkp;->a(I)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lbkp;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
