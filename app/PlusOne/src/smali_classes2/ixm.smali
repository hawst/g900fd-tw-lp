.class final Lixm;
.super Llgr;
.source "PG"


# instance fields
.field private Q:Lixn;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Llgr;-><init>()V

    .line 24
    return-void
.end method

.method public static a(Landroid/content/Context;Lae;Lnql;Z)V
    .locals 5

    .prologue
    .line 43
    iget-object v0, p2, Lnql;->a:Lnqg;

    iget-object v0, v0, Lnqg;->b:[Lnqa;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Lnqa;->b:Lnqf;

    iget-object v0, v0, Lnqf;->a:Ljava/lang/String;

    .line 44
    iget-object v1, p2, Lnql;->a:Lnqg;

    iget-object v1, v1, Lnqg;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 46
    iget-object v1, p2, Lnql;->a:Lnqg;

    iget-object v1, v1, Lnqg;->d:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xe

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "<b>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</b><p></p>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 51
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 52
    const-string v2, "title"

    iget-object v3, p2, Lnql;->a:Lnqg;

    iget-object v3, v3, Lnqg;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string v2, "message"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    const-string v0, "positive"

    iget-object v2, p2, Lnql;->a:Lnqg;

    iget-object v2, v2, Lnqg;->b:[Lnqa;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    iget-object v2, v2, Lnqa;->e:Lnpx;

    iget-object v2, v2, Lnpx;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v0, "negative"

    const v2, 0x7f0a0597

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    const-string v0, "non_google_account"

    invoke-virtual {v1, v0, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 57
    const-string v0, "shown_page_tos"

    iget-object v2, p2, Lnql;->c:Ljava/lang/Boolean;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 59
    new-instance v0, Lixm;

    invoke-direct {v0}, Lixm;-><init>()V

    .line 60
    invoke-virtual {v0, v1}, Lixm;->f(Landroid/os/Bundle;)V

    .line 62
    const-string v1, "accept_tos"

    invoke-virtual {v0, p1, v1}, Lixm;->a(Lae;Ljava/lang/String;)V

    .line 63
    return-void
.end method

.method public static a(Lnql;)Z
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 31
    if-eqz p0, :cond_0

    iget-object v2, p0, Lnql;->a:Lnqg;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lnql;->a:Lnqg;

    iget-object v2, v2, Lnqg;->b:[Lnqa;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lnql;->a:Lnqg;

    iget-object v2, v2, Lnqg;->b:[Lnqa;

    array-length v2, v2

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lnql;->a:Lnqg;

    iget-object v2, v2, Lnqg;->b:[Lnqa;

    aget-object v2, v2, v1

    iget-object v2, v2, Lnqa;->b:Lnqf;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lnql;->a:Lnqg;

    iget-object v2, v2, Lnqg;->b:[Lnqa;

    aget-object v2, v2, v0

    iget-object v2, v2, Lnqa;->e:Lnpx;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lnql;->a:Lnqg;

    iget-object v2, v2, Lnqg;->b:[Lnqa;

    aget-object v2, v2, v0

    iget-object v2, v2, Lnqa;->e:Lnpx;

    iget v2, v2, Lnpx;->b:I

    if-ne v2, v3, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public W_()Landroid/content/Context;
    .locals 3

    .prologue
    .line 72
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lixm;->n()Lz;

    move-result-object v1

    const v2, 0x7f090065

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method protected k(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 67
    invoke-super {p0, p1}, Llgr;->k(Landroid/os/Bundle;)V

    .line 68
    iget-object v0, p0, Lixm;->O:Llnh;

    const-class v1, Lixn;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lixn;

    iput-object v0, p0, Lixm;->Q:Lixn;

    .line 69
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lixm;->Q:Lixn;

    invoke-interface {v0}, Lixn;->a()V

    .line 96
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 77
    packed-switch p2, :pswitch_data_0

    .line 91
    :goto_0
    return-void

    .line 79
    :pswitch_0
    iget-object v0, p0, Lixm;->Q:Lixn;

    .line 80
    invoke-virtual {p0}, Lixm;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "non_google_account"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 81
    invoke-virtual {p0}, Lixm;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "shown_page_tos"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 79
    invoke-interface {v0, v1, v2}, Lixn;->a(ZZ)V

    goto :goto_0

    .line 87
    :pswitch_1
    iget-object v0, p0, Lixm;->Q:Lixn;

    invoke-interface {v0}, Lixn;->a()V

    goto :goto_0

    .line 77
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
