.class public final enum Laxs;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Laxs;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Laxs;

.field public static final enum b:Laxs;

.field private static enum d:Laxs;

.field private static enum e:Laxs;

.field private static enum f:Laxs;

.field private static enum g:Laxs;

.field private static enum h:Laxs;

.field private static enum i:Laxs;

.field private static enum j:Laxs;

.field private static enum k:Laxs;

.field private static enum l:Laxs;

.field private static enum m:Laxs;

.field private static enum n:Laxs;

.field private static final synthetic o:[Laxs;


# instance fields
.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 16
    new-instance v0, Laxs;

    const-string v1, "SUGGEST_THEME_CHANGE"

    const v2, 0x7f0a015a

    invoke-direct {v0, v1, v3, v2}, Laxs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Laxs;->a:Laxs;

    .line 17
    new-instance v0, Laxs;

    const-string v1, "SUGGEST_SAVE"

    const v2, 0x7f0a0159

    invoke-direct {v0, v1, v4, v2}, Laxs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Laxs;->b:Laxs;

    .line 21
    new-instance v0, Laxs;

    const-string v1, "SUGGEST_ADD_SOUNDTRACK"

    invoke-direct {v0, v1, v5, v3}, Laxs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Laxs;->d:Laxs;

    .line 22
    new-instance v0, Laxs;

    const-string v1, "FIRST_PLAYBACK"

    invoke-direct {v0, v1, v6, v3}, Laxs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Laxs;->e:Laxs;

    .line 23
    new-instance v0, Laxs;

    const-string v1, "SUGGEST_EDIT_STORYBOARD"

    invoke-direct {v0, v1, v7, v3}, Laxs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Laxs;->f:Laxs;

    .line 24
    new-instance v0, Laxs;

    const-string v1, "SUGGEST_ADD_VIDEO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v3}, Laxs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Laxs;->g:Laxs;

    .line 25
    new-instance v0, Laxs;

    const-string v1, "SUGGEST_ADD_PHOTOS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2, v3}, Laxs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Laxs;->h:Laxs;

    .line 26
    new-instance v0, Laxs;

    const-string v1, "SUGGEST_LENGTH_CHANGE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2, v3}, Laxs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Laxs;->i:Laxs;

    .line 27
    new-instance v0, Laxs;

    const-string v1, "MILESTONE_1"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2, v3}, Laxs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Laxs;->j:Laxs;

    .line 28
    new-instance v0, Laxs;

    const-string v1, "MILESTONE_2"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2, v3}, Laxs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Laxs;->k:Laxs;

    .line 29
    new-instance v0, Laxs;

    const-string v1, "MILESTONE_3"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v3}, Laxs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Laxs;->l:Laxs;

    .line 30
    new-instance v0, Laxs;

    const-string v1, "MILESTONE_4"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2, v3}, Laxs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Laxs;->m:Laxs;

    .line 31
    new-instance v0, Laxs;

    const-string v1, "MILESTONE_5"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2, v3}, Laxs;-><init>(Ljava/lang/String;II)V

    sput-object v0, Laxs;->n:Laxs;

    .line 11
    const/16 v0, 0xd

    new-array v0, v0, [Laxs;

    sget-object v1, Laxs;->a:Laxs;

    aput-object v1, v0, v3

    sget-object v1, Laxs;->b:Laxs;

    aput-object v1, v0, v4

    sget-object v1, Laxs;->d:Laxs;

    aput-object v1, v0, v5

    sget-object v1, Laxs;->e:Laxs;

    aput-object v1, v0, v6

    sget-object v1, Laxs;->f:Laxs;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Laxs;->g:Laxs;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Laxs;->h:Laxs;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Laxs;->i:Laxs;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Laxs;->j:Laxs;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Laxs;->k:Laxs;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Laxs;->l:Laxs;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Laxs;->m:Laxs;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Laxs;->n:Laxs;

    aput-object v2, v0, v1

    sput-object v0, Laxs;->o:[Laxs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 38
    iput p3, p0, Laxs;->c:I

    .line 39
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Laxs;
    .locals 1

    .prologue
    .line 11
    const-class v0, Laxs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Laxs;

    return-object v0
.end method

.method public static values()[Laxs;
    .locals 1

    .prologue
    .line 11
    sget-object v0, Laxs;->o:[Laxs;

    invoke-virtual {v0}, [Laxs;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laxs;

    return-object v0
.end method
