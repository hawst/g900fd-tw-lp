.class public final Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;
.super Laak;
.source "PG"


# static fields
.field private static final CAMERA_FACE_COORDINATES_RANGE:I = 0x7d0

.field private static final TAG:Ljava/lang/String;

.field private static mLogVerbose:Z


# instance fields
.field private mEyesOpenModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

.field private mJoyModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

.field private mLandmarkModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    const-class v0, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 27
    sput-object v0, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->TAG:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->mLogVerbose:Z

    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method private static a(II)F
    .locals 2

    .prologue
    .line 161
    int-to-float v0, p0

    const/high16 v1, 0x447a0000    # 1000.0f

    add-float/2addr v0, v1

    const/high16 v1, 0x44fa0000    # 2000.0f

    div-float/2addr v0, v1

    int-to-float v1, p1

    mul-float/2addr v0, v1

    return v0
.end method

.method private a(Landroid/graphics/Point;[I)Landroid/graphics/PointF;
    .locals 4

    .prologue
    .line 166
    new-instance v0, Landroid/graphics/PointF;

    iget v1, p1, Landroid/graphics/Point;->x:I

    const/4 v2, 0x0

    aget v2, p2, v2

    invoke-static {v1, v2}, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->a(II)F

    move-result v1

    iget v2, p1, Landroid/graphics/Point;->y:I

    const/4 v3, 0x1

    aget v3, p2, v3

    invoke-static {v2, v3}, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->a(II)F

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    return-object v0
.end method

.method private a(Landroid/content/Context;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;
    .locals 3

    .prologue
    .line 141
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->b()Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 149
    :goto_0
    return-object v0

    .line 146
    :catch_0
    move-exception v0

    const-string v0, "LMspeed_508.emd"

    .line 147
    const-string v1, "frsdk_modules/"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146
    :goto_1
    invoke-static {p1, v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    move-result-object v0

    goto :goto_0

    .line 147
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private a(Landroid/graphics/Point;)Z
    .locals 3

    .prologue
    const/16 v2, 0x3e8

    const/16 v1, -0x3e8

    .line 192
    iget v0, p1, Landroid/graphics/Point;->x:I

    if-lt v0, v1, :cond_0

    iget v0, p1, Landroid/graphics/Point;->x:I

    if-ge v0, v2, :cond_0

    iget v0, p1, Landroid/graphics/Point;->y:I

    if-lt v0, v1, :cond_0

    iget v0, p1, Landroid/graphics/Point;->y:I

    if-ge v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 157
    const-string v1, "frsdk_expression_modules/"

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public c()Lacx;
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 43
    const/16 v0, 0x64

    invoke-static {v0}, Labf;->b(I)Labf;

    move-result-object v0

    .line 44
    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v1}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v1

    .line 46
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    .line 47
    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "faces"

    const-class v3, Landroid/hardware/Camera$Face;

    .line 48
    invoke-static {v3}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "smileScores"

    .line 49
    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "eyesOpenScores"

    .line 50
    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->d()Lacs;

    move-result-object v0

    invoke-virtual {v0}, Lacs;->b()Landroid/content/Context;

    move-result-object v0

    .line 62
    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->a(Landroid/content/Context;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->mLandmarkModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    .line 63
    const-string v1, "BCLjoy_100.emd"

    .line 64
    invoke-static {v1}, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 63
    invoke-static {v0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->mJoyModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    .line 65
    const-string v1, "BCLeyesclosed_100.emd"

    .line 66
    invoke-static {v1}, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 65
    invoke-static {v0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->mEyesOpenModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    .line 67
    return-void
.end method

.method protected i()V
    .locals 18

    .prologue
    .line 72
    const-string v2, "image"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v2

    invoke-virtual {v2}, Lacp;->c()Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->e()Laas;

    move-result-object v5

    .line 73
    const-string v2, "faces"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v2

    invoke-virtual {v2}, Lacp;->c()Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->b()Labh;

    move-result-object v2

    .line 74
    invoke-virtual {v2}, Labh;->k()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/hardware/Camera$Face;

    .line 75
    invoke-virtual {v5}, Laas;->i()[I

    move-result-object v6

    .line 77
    array-length v3, v2

    new-array v7, v3, [F

    .line 78
    array-length v3, v2

    new-array v8, v3, [F

    .line 80
    new-instance v9, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;

    invoke-direct {v9}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;-><init>()V

    .line 81
    const/4 v3, 0x1

    invoke-virtual {v5, v3}, Laas;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v5}, Laas;->j()I

    move-result v4

    .line 82
    invoke-virtual {v5}, Laas;->k()I

    move-result v10

    .line 81
    invoke-virtual {v9, v3, v4, v10}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a(Ljava/nio/ByteBuffer;II)V

    .line 84
    const/4 v3, 0x0

    :goto_0
    array-length v4, v2

    if-ge v3, v4, :cond_5

    .line 85
    aget-object v10, v2, v3

    .line 87
    iget-object v4, v10, Landroid/hardware/Camera$Face;->leftEye:Landroid/graphics/Point;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->a(Landroid/graphics/Point;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, v10, Landroid/hardware/Camera$Face;->rightEye:Landroid/graphics/Point;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->a(Landroid/graphics/Point;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, v10, Landroid/hardware/Camera$Face;->mouth:Landroid/graphics/Point;

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->a(Landroid/graphics/Point;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, v10, Landroid/hardware/Camera$Face;->rightEye:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    iget-object v11, v10, Landroid/hardware/Camera$Face;->leftEye:Landroid/graphics/Point;

    iget v11, v11, Landroid/graphics/Point;->y:I

    sub-int/2addr v4, v11

    int-to-double v12, v4

    iget-object v4, v10, Landroid/hardware/Camera$Face;->rightEye:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    iget-object v11, v10, Landroid/hardware/Camera$Face;->leftEye:Landroid/graphics/Point;

    iget v11, v11, Landroid/graphics/Point;->x:I

    sub-int/2addr v4, v11

    int-to-double v14, v4

    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v12

    new-instance v4, Landroid/graphics/Point;

    invoke-direct {v4}, Landroid/graphics/Point;-><init>()V

    iget-object v11, v10, Landroid/hardware/Camera$Face;->leftEye:Landroid/graphics/Point;

    iget v11, v11, Landroid/graphics/Point;->x:I

    iget-object v14, v10, Landroid/hardware/Camera$Face;->rightEye:Landroid/graphics/Point;

    iget v14, v14, Landroid/graphics/Point;->x:I

    add-int/2addr v11, v14

    div-int/lit8 v11, v11, 0x2

    iput v11, v4, Landroid/graphics/Point;->x:I

    iget-object v11, v10, Landroid/hardware/Camera$Face;->leftEye:Landroid/graphics/Point;

    iget v11, v11, Landroid/graphics/Point;->y:I

    iget-object v14, v10, Landroid/hardware/Camera$Face;->rightEye:Landroid/graphics/Point;

    iget v14, v14, Landroid/graphics/Point;->y:I

    add-int/2addr v11, v14

    div-int/lit8 v11, v11, 0x2

    iput v11, v4, Landroid/graphics/Point;->y:I

    iget v11, v4, Landroid/graphics/Point;->y:I

    iget-object v14, v10, Landroid/hardware/Camera$Face;->mouth:Landroid/graphics/Point;

    iget v14, v14, Landroid/graphics/Point;->y:I

    sub-int/2addr v11, v14

    int-to-double v14, v11

    iget v4, v4, Landroid/graphics/Point;->x:I

    iget-object v11, v10, Landroid/hardware/Camera$Face;->mouth:Landroid/graphics/Point;

    iget v11, v11, Landroid/graphics/Point;->x:I

    sub-int/2addr v4, v11

    int-to-double v0, v4

    move-wide/from16 v16, v0

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v14

    sub-double/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(D)D

    move-result-wide v12

    const-wide v14, 0x400921fb54442d18L    # Math.PI

    cmpg-double v4, v12, v14

    if-gez v4, :cond_2

    const/4 v4, 0x1

    :goto_1
    if-eqz v4, :cond_3

    const/4 v4, 0x1

    :goto_2
    if-eqz v4, :cond_4

    .line 89
    sget-boolean v4, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->mLogVerbose:Z

    if-eqz v4, :cond_0

    .line 90
    sget-object v4, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->TAG:Ljava/lang/String;

    iget-object v4, v10, Landroid/hardware/Camera$Face;->leftEye:Landroid/graphics/Point;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iget-object v11, v10, Landroid/hardware/Camera$Face;->rightEye:Landroid/graphics/Point;

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    iget-object v12, v10, Landroid/hardware/Camera$Face;->mouth:Landroid/graphics/Point;

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int/lit8 v14, v14, 0x1c

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    add-int/2addr v14, v15

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/String;->length()I

    move-result v15

    add-int/2addr v14, v15

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v14, "Sending Face Coords: "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v13, " : "

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v11, " : "

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v11, "."

    invoke-virtual {v4, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    :cond_0
    iget-object v4, v10, Landroid/hardware/Camera$Face;->leftEye:Landroid/graphics/Point;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v6}, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->a(Landroid/graphics/Point;[I)Landroid/graphics/PointF;

    move-result-object v4

    iget-object v11, v10, Landroid/hardware/Camera$Face;->rightEye:Landroid/graphics/Point;

    .line 99
    move-object/from16 v0, p0

    invoke-direct {v0, v11, v6}, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->a(Landroid/graphics/Point;[I)Landroid/graphics/PointF;

    move-result-object v11

    iget-object v10, v10, Landroid/hardware/Camera$Face;->mouth:Landroid/graphics/Point;

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v6}, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->a(Landroid/graphics/Point;[I)Landroid/graphics/PointF;

    move-result-object v10

    .line 98
    invoke-virtual {v9, v4, v11, v10}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)V

    .line 100
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->mLandmarkModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    invoke-virtual {v9, v4}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->c(Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;

    .line 103
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->mJoyModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    invoke-virtual {v9, v4}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->c(Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;

    .line 104
    invoke-virtual {v9}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->c()F

    move-result v4

    aput v4, v7, v3

    .line 107
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->mEyesOpenModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    invoke-virtual {v9, v4}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->c(Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;

    .line 108
    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v9}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->c()F

    move-result v10

    sub-float/2addr v4, v10

    aput v4, v8, v3

    .line 84
    :cond_1
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 87
    :cond_2
    const/4 v4, 0x0

    goto/16 :goto_1

    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 110
    :cond_4
    sget-boolean v4, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->mLogVerbose:Z

    if-eqz v4, :cond_1

    .line 111
    sget-object v4, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->TAG:Ljava/lang/String;

    goto :goto_3

    .line 115
    :cond_5
    invoke-virtual {v5}, Laas;->h()V

    .line 116
    invoke-virtual {v9}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a()V

    .line 117
    const-string v2, "smileScores"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v2

    .line 118
    const-string v3, "eyesOpenScores"

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v3

    .line 121
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lacv;->a([I)Laap;

    move-result-object v4

    invoke-virtual {v4}, Laap;->b()Labh;

    move-result-object v4

    .line 122
    invoke-virtual {v4, v7}, Labh;->b(Ljava/lang/Object;)V

    .line 123
    invoke-virtual {v2, v4}, Lacv;->a(Laap;)V

    .line 125
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->b()Labh;

    move-result-object v2

    .line 126
    invoke-virtual {v2, v8}, Labh;->b(Ljava/lang/Object;)V

    .line 127
    invoke-virtual {v3, v2}, Lacv;->a(Laap;)V

    .line 128
    return-void
.end method

.method protected k()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->mLandmarkModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->d()V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->mJoyModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->d()V

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->mEyesOpenModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->d()V

    .line 135
    iput-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->mLandmarkModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    .line 136
    iput-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->mJoyModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    .line 137
    iput-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FacialExpressionFilter;->mEyesOpenModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    .line 138
    return-void
.end method
