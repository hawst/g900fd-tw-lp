.class public Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;
.super Laak;
.source "PG"

# interfaces
.implements Lbbn;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# static fields
.field private static final FACE_THRESHOLD:F = 0.2f


# instance fields
.field private mDetectorModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->mDetectorModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    .line 32
    return-void
.end method

.method private static a(FI)I
    .locals 2

    .prologue
    .line 125
    const/high16 v0, 0x44fa0000    # 2000.0f

    int-to-float v1, p1

    div-float v1, p0, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    add-int/lit16 v0, v0, -0x3e8

    return v0
.end method

.method private static a(Landroid/graphics/PointF;[I)Landroid/graphics/Point;
    .locals 4

    .prologue
    .line 136
    new-instance v0, Landroid/graphics/Point;

    iget v1, p0, Landroid/graphics/PointF;->x:F

    const/4 v2, 0x0

    aget v2, p1, v2

    invoke-static {v1, v2}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->a(FI)I

    move-result v1

    iget v2, p0, Landroid/graphics/PointF;->y:F

    const/4 v3, 0x1

    aget v3, p1, v3

    invoke-static {v2, v3}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->a(FI)I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;)V
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call setCustomModule while tracker is running!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->mDetectorModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    if-eqz v0, :cond_1

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->mDetectorModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->d()V

    .line 95
    :cond_1
    if-eqz p1, :cond_2

    .line 97
    invoke-virtual {p1}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->c()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->a(Ljava/io/File;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->mDetectorModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    .line 101
    :goto_0
    return-void

    .line 99
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->mDetectorModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 36
    const/16 v0, 0x64

    invoke-static {v0}, Labf;->b(I)Labf;

    move-result-object v0

    .line 37
    new-instance v1, Lacx;

    invoke-direct {v1}, Lacx;-><init>()V

    const-string v2, "image"

    .line 38
    invoke-virtual {v1, v2, v3, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "faces"

    const-class v2, Landroid/hardware/Camera$Face;

    .line 39
    invoke-static {v2}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    .line 45
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->mDetectorModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    if-nez v0, :cond_0

    .line 46
    invoke-static {}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->a()Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->mDetectorModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    .line 48
    :cond_0
    const-string v0, "image"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->d()Laar;

    move-result-object v0

    .line 50
    invoke-virtual {v0}, Laar;->j()I

    move-result v1

    .line 51
    invoke-virtual {v0}, Laar;->k()I

    move-result v2

    .line 52
    const/4 v3, 0x2

    new-array v3, v3, [I

    aput v1, v3, v11

    aput v2, v3, v12

    .line 54
    new-instance v4, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;

    invoke-direct {v4}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;-><init>()V

    .line 55
    invoke-virtual {v0, v12}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 58
    invoke-virtual {v4, v5, v1, v2}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->b(Ljava/nio/ByteBuffer;II)V

    .line 59
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->mDetectorModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a(Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;

    .line 61
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 62
    :cond_1
    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->mDetectorModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    invoke-virtual {v4, v2}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->b(Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->b()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 63
    invoke-virtual {v4}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->c()F

    move-result v2

    const v5, 0x3e4ccccd    # 0.2f

    cmpl-float v2, v2, v5

    if-lez v2, :cond_1

    .line 64
    new-instance v2, Landroid/hardware/Camera$Face;

    invoke-direct {v2}, Landroid/hardware/Camera$Face;-><init>()V

    invoke-virtual {v4, v11}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a(I)Landroid/graphics/PointF;

    move-result-object v5

    invoke-static {v5, v3}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->a(Landroid/graphics/PointF;[I)Landroid/graphics/Point;

    move-result-object v5

    iput-object v5, v2, Landroid/hardware/Camera$Face;->leftEye:Landroid/graphics/Point;

    invoke-virtual {v4, v12}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a(I)Landroid/graphics/PointF;

    move-result-object v5

    invoke-static {v5, v3}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->a(Landroid/graphics/PointF;[I)Landroid/graphics/Point;

    move-result-object v5

    iput-object v5, v2, Landroid/hardware/Camera$Face;->rightEye:Landroid/graphics/Point;

    const/16 v5, 0x2d

    invoke-virtual {v4, v5}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a(I)Landroid/graphics/PointF;

    move-result-object v5

    invoke-static {v5, v3}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->a(Landroid/graphics/PointF;[I)Landroid/graphics/Point;

    move-result-object v5

    iput-object v5, v2, Landroid/hardware/Camera$Face;->mouth:Landroid/graphics/Point;

    invoke-virtual {v4}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->d()Landroid/graphics/RectF;

    move-result-object v5

    new-instance v6, Landroid/graphics/Rect;

    iget v7, v5, Landroid/graphics/RectF;->left:F

    aget v8, v3, v11

    invoke-static {v7, v8}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->a(FI)I

    move-result v7

    iget v8, v5, Landroid/graphics/RectF;->top:F

    aget v9, v3, v12

    invoke-static {v8, v9}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->a(FI)I

    move-result v8

    iget v9, v5, Landroid/graphics/RectF;->right:F

    aget v10, v3, v11

    invoke-static {v9, v10}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->a(FI)I

    move-result v9

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    aget v10, v3, v12

    invoke-static {v5, v10}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->a(FI)I

    move-result v5

    invoke-direct {v6, v7, v8, v9, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v6, v2, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    const/high16 v5, 0x42c80000    # 100.0f

    invoke-virtual {v4}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->c()F

    move-result v6

    mul-float/2addr v5, v6

    float-to-int v5, v5

    iput v5, v2, Landroid/hardware/Camera$Face;->score:I

    invoke-virtual {v1, v2}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 68
    :cond_2
    invoke-virtual {v4}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a()V

    .line 69
    invoke-virtual {v0}, Laar;->h()V

    .line 71
    const-string v0, "faces"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 72
    new-array v2, v12, [I

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v3

    aput v3, v2, v11

    invoke-virtual {v0, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    .line 73
    invoke-virtual {v2}, Laap;->b()Labh;

    move-result-object v3

    new-array v4, v11, [Landroid/hardware/Camera$Face;

    invoke-virtual {v1, v4}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v3, v1}, Labh;->b(Ljava/lang/Object;)V

    .line 74
    invoke-virtual {v0, v2}, Lacv;->a(Laap;)V

    .line 75
    return-void
.end method

.method protected k()V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->mDetectorModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->mDetectorModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->d()V

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetectorFilter;->mDetectorModule:Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    .line 109
    :cond_0
    return-void
.end method
