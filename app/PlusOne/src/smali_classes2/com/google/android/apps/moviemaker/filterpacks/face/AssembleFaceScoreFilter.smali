.class public Lcom/google/android/apps/moviemaker/filterpacks/face/AssembleFaceScoreFilter;
.super Laak;
.source "PG"


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 21
    return-void
.end method


# virtual methods
.method public c()Lacx;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 25
    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v0}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v0

    .line 26
    new-instance v1, Lacx;

    invoke-direct {v1}, Lacx;-><init>()V

    const-string v2, "faces"

    const-class v3, Landroid/hardware/Camera$Face;

    .line 27
    invoke-static {v3}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v1, v2, v5, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v1

    const-string v2, "histogramDistances"

    .line 28
    invoke-virtual {v1, v2, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v1

    const-string v2, "smileScores"

    .line 29
    invoke-virtual {v1, v2, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v1

    const-string v2, "eyesOpenScores"

    .line 30
    invoke-virtual {v1, v2, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v1

    const-string v2, "sharpnessScores"

    .line 31
    invoke-virtual {v1, v2, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "faceScores"

    const-class v2, Lbbo;

    .line 32
    invoke-static {v2}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v5, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 38
    const-string v0, "faces"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/AssembleFaceScoreFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->b()Labh;

    move-result-object v0

    .line 39
    invoke-virtual {v0}, Labh;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/hardware/Camera$Face;

    .line 45
    const-string v1, "histogramDistances"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/face/AssembleFaceScoreFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    .line 46
    if-eqz v1, :cond_8

    .line 47
    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->b()Labh;

    move-result-object v1

    invoke-virtual {v1}, Labh;->k()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [F

    move-object v2, v1

    .line 50
    :goto_0
    const-string v1, "smileScores"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/face/AssembleFaceScoreFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    .line 51
    if-eqz v1, :cond_7

    .line 53
    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->b()Labh;

    move-result-object v1

    invoke-virtual {v1}, Labh;->k()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [F

    move-object v4, v1

    .line 56
    :goto_1
    const-string v1, "eyesOpenScores"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/face/AssembleFaceScoreFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    .line 57
    if-eqz v1, :cond_6

    .line 59
    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->b()Labh;

    move-result-object v1

    invoke-virtual {v1}, Labh;->k()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [F

    move-object v5, v1

    .line 62
    :goto_2
    const-string v1, "sharpnessScores"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/face/AssembleFaceScoreFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    .line 63
    if-eqz v1, :cond_5

    .line 65
    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->b()Labh;

    move-result-object v1

    invoke-virtual {v1}, Labh;->k()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [F

    .line 68
    :goto_3
    array-length v6, v0

    new-array v7, v6, [Lbbo;

    .line 69
    const/4 v6, 0x0

    :goto_4
    array-length v8, v0

    if-ge v6, v8, :cond_4

    .line 70
    new-instance v8, Lbbo;

    invoke-direct {v8}, Lbbo;-><init>()V

    aput-object v8, v7, v6

    .line 71
    aget-object v8, v7, v6

    aget-object v9, v0, v6

    invoke-virtual {v8, v9}, Lbbo;->a(Landroid/hardware/Camera$Face;)V

    .line 73
    aget-object v8, v7, v6

    aget-object v9, v0, v6

    iget v9, v9, Landroid/hardware/Camera$Face;->score:I

    int-to-float v9, v9

    invoke-virtual {v8, v9}, Lbbo;->d(F)V

    .line 76
    if-eqz v2, :cond_0

    .line 77
    const/high16 v8, 0x3f800000    # 1.0f

    aget v9, v2, v6

    sub-float/2addr v8, v9

    .line 78
    aget-object v9, v7, v6

    invoke-virtual {v9, v8}, Lbbo;->f(F)V

    .line 82
    :cond_0
    if-eqz v4, :cond_1

    .line 83
    aget-object v8, v7, v6

    aget v9, v4, v6

    invoke-virtual {v8, v9}, Lbbo;->b(F)V

    .line 87
    :cond_1
    if-eqz v5, :cond_2

    .line 88
    aget-object v8, v7, v6

    aget v9, v5, v6

    invoke-virtual {v8, v9}, Lbbo;->c(F)V

    .line 92
    :cond_2
    if-eqz v1, :cond_3

    .line 93
    aget-object v8, v7, v6

    aget v9, v1, v6

    invoke-virtual {v8, v9}, Lbbo;->e(F)V

    .line 69
    :cond_3
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 98
    :cond_4
    const-string v0, "faceScores"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/AssembleFaceScoreFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 99
    invoke-virtual {v0, v3}, Lacv;->a([I)Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->b()Labh;

    move-result-object v1

    .line 100
    invoke-virtual {v1, v7}, Labh;->b(Ljava/lang/Object;)V

    .line 101
    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    .line 102
    return-void

    :cond_5
    move-object v1, v3

    goto :goto_3

    :cond_6
    move-object v5, v3

    goto :goto_2

    :cond_7
    move-object v4, v3

    goto/16 :goto_1

    :cond_8
    move-object v2, v3

    goto/16 :goto_0
.end method
