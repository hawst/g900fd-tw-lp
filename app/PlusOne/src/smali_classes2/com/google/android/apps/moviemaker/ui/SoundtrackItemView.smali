.class public Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field private a:Landroid/widget/TextView;

.field private b:Landroid/widget/ImageView;

.field private c:Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;

.field private final d:Lcbj;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    new-instance v0, Lcbj;

    invoke-direct {v0}, Lcbj;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->d:Lcbj;

    .line 42
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->d:Lcbj;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 43
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->d:Lcbj;

    invoke-virtual {v0, p1}, Lcbj;->a(I)V

    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->invalidate()V

    .line 85
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 46
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->c:Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 47
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->c:Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;->a()V

    .line 51
    :cond_0
    :goto_0
    return-void

    .line 48
    :cond_1
    if-nez p1, :cond_0

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->c:Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 89
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 90
    const v0, 0x7f10060c

    .line 91
    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const-string v1, "soundtrack_text"

    .line 90
    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->a:Landroid/widget/TextView;

    .line 92
    const v0, 0x7f10060e

    .line 93
    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const-string v1, "soundtrack_selected"

    .line 92
    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->b:Landroid/widget/ImageView;

    .line 94
    const v0, 0x7f10060d

    .line 95
    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;

    const-string v1, "soundtrack_spinner"

    .line 94
    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->c:Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;

    .line 97
    return-void
.end method

.method public setSelected(Z)V
    .locals 8

    .prologue
    const-wide/16 v6, 0xfa

    const/16 v1, 0x8

    const/high16 v4, 0x3f000000    # 0.5f

    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->isSelected()Z

    move-result v0

    if-eq v0, p1, :cond_0

    .line 58
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setAlpha(F)V

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->a:Landroid/widget/TextView;

    invoke-static {v0}, Lcad;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 60
    invoke-virtual {v0, v6, v7}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 62
    if-eqz p1, :cond_1

    .line 63
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->b:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 64
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 65
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setScaleX(F)V

    .line 66
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setScaleY(F)V

    .line 67
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->b:Landroid/widget/ImageView;

    invoke-static {v0}, Lcad;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 68
    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 69
    invoke-virtual {v0, v6, v7}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    .line 75
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setSelected(Z)V

    .line 76
    return-void

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->c:Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;->setVisibility(I)V

    goto :goto_0
.end method
