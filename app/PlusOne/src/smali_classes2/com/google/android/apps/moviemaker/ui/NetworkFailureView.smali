.class public final Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;
.super Landroid/widget/RelativeLayout;
.source "PG"

# interfaces
.implements Lapy;


# static fields
.field private static final a:Lapz;


# instance fields
.field private b:Landroid/widget/TextView;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Z

.field private f:Lapz;

.field private g:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lapz;

    .line 29
    invoke-static {v0}, Lcgf;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapz;

    sput-object v0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->a:Lapz;

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    sget-object v0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->a:Lapz;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->f:Lapz;

    .line 54
    return-void
.end method

.method private a(I)Landroid/view/View;
    .locals 4

    .prologue
    .line 151
    invoke-virtual {p0, p1}, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x13

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "View "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 151
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->g:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->g:Ljava/lang/Runnable;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;Z)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 19
    if-eqz p1, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->setVisibility(I)V

    invoke-static {p0}, Lcad;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->c:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->d:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setClickable(Z)V

    return-void

    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->setVisibility(I)V

    invoke-static {p0}, Lcad;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->withLayer()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lcat;

    invoke-direct {v1, p0}, Lcat;-><init>(Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->withEndAction(Ljava/lang/Runnable;)Landroid/view/ViewPropertyAnimator;

    goto :goto_0
.end method

.method public static synthetic b(Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;)Lapz;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->f:Lapz;

    return-object v0
.end method


# virtual methods
.method public a(Lapz;)V
    .locals 1

    .prologue
    .line 58
    if-nez p1, :cond_0

    .line 59
    sget-object v0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->a:Lapz;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->f:Lapz;

    .line 63
    :goto_0
    return-void

    .line 61
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->f:Lapz;

    goto :goto_0
.end method

.method public a(Z)V
    .locals 3

    .prologue
    .line 67
    if-eqz p1, :cond_0

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0106

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->d:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 74
    :goto_0
    return-void

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->b:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0105

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->d:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 4

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->e:Z

    if-ne v0, p1, :cond_0

    .line 104
    :goto_0
    return-void

    .line 81
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->e:Z

    .line 84
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->g:Ljava/lang/Runnable;

    if-eqz v0, :cond_1

    .line 85
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->g:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->g:Ljava/lang/Runnable;

    .line 88
    :cond_1
    new-instance v0, Lcaq;

    invoke-direct {v0, p0, p1}, Lcaq;-><init>(Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;Z)V

    .line 97
    if-eqz p1, :cond_2

    .line 98
    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->g:Ljava/lang/Runnable;

    .line 100
    const-wide/16 v2, 0x320

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 102
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 108
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 109
    const v0, 0x7f100399

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->a(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->b:Landroid/widget/TextView;

    .line 110
    const v0, 0x7f10039a

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->c:Landroid/view/View;

    .line 111
    const v0, 0x7f10039b

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->d:Landroid/view/View;

    .line 113
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->d:Landroid/view/View;

    new-instance v1, Lcar;

    invoke-direct {v1, p0}, Lcar;-><init>(Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 119
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;->c:Landroid/view/View;

    new-instance v1, Lcas;

    invoke-direct {v1, p0}, Lcas;-><init>(Lcom/google/android/apps/moviemaker/ui/NetworkFailureView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 125
    return-void
.end method
