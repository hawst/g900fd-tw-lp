.class public Lcom/google/android/apps/moviemaker/filterpacks/image/ExtractColorfulnessFilter;
.super Laak;
.source "PG"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-string v0, "moviemaker-jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 27
    return-void
.end method

.method private native extractColorfulness(Ljava/nio/ByteBuffer;)F
.end method


# virtual methods
.method public c()Lacx;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 31
    const/16 v0, 0x12d

    const/4 v1, 0x1

    invoke-static {v0, v1}, Labf;->a(II)Labf;

    move-result-object v0

    .line 33
    new-instance v1, Lacx;

    invoke-direct {v1}, Lacx;-><init>()V

    const-string v2, "image"

    .line 34
    invoke-virtual {v1, v2, v3, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "score"

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    .line 35
    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 36
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 4

    .prologue
    .line 41
    const-string v0, "image"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/image/ExtractColorfulnessFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v0

    .line 42
    const-string v1, "score"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/image/ExtractColorfulnessFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    .line 43
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->a()Labg;

    move-result-object v2

    .line 44
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Laas;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/apps/moviemaker/filterpacks/image/ExtractColorfulnessFilter;->extractColorfulness(Ljava/nio/ByteBuffer;)F

    move-result v3

    .line 45
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    invoke-virtual {v2, v3}, Labg;->a(Ljava/lang/Object;)V

    .line 46
    invoke-virtual {v0}, Laas;->h()V

    .line 47
    invoke-virtual {v1, v2}, Lacv;->a(Laap;)V

    .line 48
    return-void
.end method
