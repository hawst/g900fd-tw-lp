.class public Lcom/google/android/apps/photoeditor/views/ItemSelectorView;
.super Landroid/widget/RelativeLayout;
.source "PG"


# instance fields
.field private a:Landroid/widget/RelativeLayout;

.field private b:Lcmr;

.field private c:Lcmw;

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 44
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    .line 48
    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    .line 54
    const v0, 0x7f090009

    iput v0, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->d:I

    .line 59
    invoke-direct {p0, p1}, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->a(Landroid/content/Context;)V

    .line 60
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    .line 48
    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    .line 54
    const v0, 0x7f090009

    iput v0, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->d:I

    .line 65
    invoke-direct {p0, p1}, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->a(Landroid/content/Context;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    .line 48
    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    .line 54
    const v0, 0x7f090009

    iput v0, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->d:I

    .line 71
    invoke-direct {p0, p1}, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->a(Landroid/content/Context;)V

    .line 72
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/photoeditor/views/ItemSelectorView;Landroid/content/Context;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->c:Lcmw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->c:Lcmw;

    invoke-interface {v0}, Lcmw;->a()Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;-><init>(Landroid/content/Context;)V

    iget v1, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->d:I

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->a(I)V

    goto :goto_0
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 75
    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-direct {v0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->a:Landroid/widget/RelativeLayout;

    .line 76
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v2, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    .line 78
    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 79
    iget-object v1, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 80
    return-void
.end method


# virtual methods
.method public a(Lcmu;Lcmv;)V
    .locals 2

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->b:Lcmr;

    if-nez v0, :cond_0

    .line 99
    new-instance v0, Lcmr;

    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcmr;-><init>(Lcom/google/android/apps/photoeditor/views/ItemSelectorView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->b:Lcmr;

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->a:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->b:Lcmr;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->b:Lcmr;

    invoke-virtual {v0, p1}, Lcmr;->a(Lcmu;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->b:Lcmr;

    invoke-virtual {v0, p2}, Lcmr;->a(Lcmv;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->a:Landroid/widget/RelativeLayout;

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->requestLayout()V

    .line 107
    return-void
.end method

.method public a(Lcmw;)V
    .locals 0

    .prologue
    .line 365
    iput-object p1, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->c:Lcmw;

    .line 366
    return-void
.end method

.method public a(Lcmu;Z)Z
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->b:Lcmr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->b:Lcmr;

    .line 129
    invoke-virtual {v0, p1, p2}, Lcmr;->a(Lcmu;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 2

    .prologue
    .line 350
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->setEnabled(Z)V

    .line 352
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->b:Lcmr;

    if-eqz v0, :cond_1

    .line 353
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->b:Lcmr;

    invoke-virtual {v0}, Lcmr;->a()Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    move-result-object v0

    .line 354
    if-eqz v0, :cond_0

    .line 355
    invoke-virtual {v0, p1}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setEnabled(Z)V

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->b:Lcmr;

    iget-object v0, v0, Lcmr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 359
    invoke-virtual {v0, p1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 362
    :cond_1
    return-void
.end method
