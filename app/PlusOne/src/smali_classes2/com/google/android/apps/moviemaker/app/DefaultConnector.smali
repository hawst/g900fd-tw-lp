.class public Lcom/google/android/apps/moviemaker/app/DefaultConnector;
.super Ljem;
.source "PG"


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lanh;

.field private final c:Lajl;

.field private final d:Laro;

.field private final e:Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;

.field private final f:Lcdu;

.field private final g:Ljdw;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Ljem;-><init>()V

    .line 46
    const-string v0, "context"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->a:Landroid/content/Context;

    .line 48
    invoke-static {p1}, Lapm;->a(Landroid/content/Context;)Lapm;

    move-result-object v0

    invoke-virtual {v0}, Lapm;->a()Lamm;

    move-result-object v0

    .line 50
    invoke-interface {v0}, Lamm;->p()Lanh;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->b:Lanh;

    .line 51
    new-instance v1, Lapo;

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->b:Lanh;

    invoke-direct {v1, v2}, Lapo;-><init>(Lani;)V

    .line 52
    invoke-interface {v0}, Lamm;->L()Ljfb;

    .line 53
    invoke-interface {v0}, Lamm;->r()Lajl;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->c:Lajl;

    .line 54
    invoke-interface {v0}, Lamm;->v()Lbig;

    .line 55
    invoke-interface {v0}, Lamm;->C()Laro;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->d:Laro;

    .line 56
    invoke-interface {v0}, Lamm;->B()Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->e:Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;

    .line 57
    invoke-interface {v0}, Lamm;->k()Lcdu;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->f:Lcdu;

    .line 58
    invoke-interface {v0}, Lamm;->K()Ljdw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->g:Ljdw;

    .line 59
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->a:Landroid/content/Context;

    invoke-static {v1, p1}, Lbwq;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 100
    return-void
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0005

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->b:Lanh;

    .line 64
    invoke-virtual {v0}, Lanh;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->c:Lajl;

    invoke-interface {v0, p1}, Lajl;->f(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public a(Lood;)Z
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->b:Lanh;

    invoke-virtual {v0, p1}, Lanh;->a(Lood;)Z

    move-result v0

    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->b:Lanh;

    invoke-virtual {v0}, Lanh;->f()Z

    move-result v0

    return v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->d:Laro;

    invoke-interface {v0, p1}, Laro;->b(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->b:Lanh;

    invoke-virtual {v0}, Lanh;->R()Z

    move-result v0

    return v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->b:Lanh;

    invoke-virtual {v0}, Lanh;->S()J

    move-result-wide v0

    return-wide v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->b:Lanh;

    invoke-virtual {v0}, Lanh;->T()Z

    move-result v0

    return v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->b:Lanh;

    invoke-virtual {v0}, Lanh;->U()J

    move-result-wide v0

    return-wide v0
.end method

.method public g()Ljdw;
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->g:Ljdw;

    return-object v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->e:Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/app/ApplicationEnabler;->a()V

    .line 135
    return-void
.end method

.method public i()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->f:Lcdu;

    invoke-virtual {v0}, Lcdu;->l()V

    .line 140
    return-void
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/app/DefaultConnector;->b:Lanh;

    invoke-virtual {v0}, Lanh;->aj()Z

    move-result v0

    return v0
.end method
