.class public Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;
.super Landroid/view/ViewGroup;
.source "PG"


# instance fields
.field private final a:I

.field private b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:Z

.field private h:[Landroid/graphics/Point;

.field private i:Landroid/animation/ObjectAnimator;

.field private j:[Landroid/animation/ObjectAnimator;

.field private final k:Landroid/animation/AnimatorListenerAdapter;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 98
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 101
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v4, -0x1

    const/4 v8, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 105
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    iput v4, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->b:I

    .line 77
    iput-boolean v1, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->g:Z

    .line 88
    new-instance v0, Lcay;

    invoke-direct {v0}, Lcay;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->k:Landroid/animation/AnimatorListenerAdapter;

    .line 107
    sget-object v0, Lahj;->d:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 110
    invoke-virtual {v3, v1, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a:I

    .line 111
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a:I

    if-ne v0, v2, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    iget v4, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a:I

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x3c

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "orientation must be horizontal or vertical, not: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 113
    invoke-virtual {v3, v2, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c:I

    .line 114
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a:I

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c:I

    if-lt v0, v8, :cond_4

    :cond_1
    move v0, v2

    :goto_1
    const-string v4, "the number of columns must be at least 2 if the orientation is vertical"

    invoke-static {v0, v4}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 116
    invoke-virtual {v3, v8, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->d:I

    .line 117
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a:I

    if-eq v0, v2, :cond_2

    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->d:I

    if-lt v0, v8, :cond_5

    :cond_2
    move v0, v2

    :goto_2
    const-string v4, "the number of rows must be at least 2 if the orientation is horizontal"

    invoke-static {v0, v4}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 119
    invoke-virtual {v3, v9, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->f:I

    .line 120
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 124
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->f:I

    if-nez v0, :cond_6

    move v0, v2

    :goto_3
    iput-boolean v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->g:Z

    .line 126
    const-string v0, "left"

    new-array v3, v8, [I

    fill-array-data v3, :array_0

    invoke-static {v0, v3}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    .line 127
    const-string v3, "top"

    new-array v4, v8, [I

    fill-array-data v4, :array_1

    invoke-static {v3, v4}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v3

    .line 128
    const-string v4, "right"

    new-array v5, v8, [I

    fill-array-data v5, :array_2

    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    .line 129
    const-string v5, "bottom"

    new-array v6, v8, [I

    fill-array-data v6, :array_3

    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofInt(Ljava/lang/String;[I)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    .line 131
    const/4 v6, 0x0

    const/4 v7, 0x4

    new-array v7, v7, [Landroid/animation/PropertyValuesHolder;

    aput-object v0, v7, v1

    aput-object v3, v7, v2

    aput-object v4, v7, v8

    aput-object v5, v7, v9

    invoke-static {v6, v7}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->i:Landroid/animation/ObjectAnimator;

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->i:Landroid/animation/ObjectAnimator;

    sget-object v1, Lcak;->a:Lcak;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 135
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->e()V

    .line 138
    invoke-virtual {p0, v2}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->setChildrenDrawingOrderEnabled(Z)V

    .line 139
    return-void

    :cond_3
    move v0, v1

    .line 111
    goto/16 :goto_0

    :cond_4
    move v0, v1

    .line 114
    goto :goto_1

    :cond_5
    move v0, v1

    .line 117
    goto :goto_2

    :cond_6
    move v0, v1

    .line 124
    goto :goto_3

    .line 126
    nop

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data

    .line 127
    :array_1
    .array-data 4
        0x0
        0x1
    .end array-data

    .line 128
    :array_2
    .array-data 4
        0x0
        0x1
    .end array-data

    .line 129
    :array_3
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method public static synthetic a(Landroid/animation/Animator;)Landroid/view/View;
    .locals 2

    .prologue
    .line 45
    instance-of v0, p0, Landroid/animation/ObjectAnimator;

    const-string v1, "Wrong animator type."

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    check-cast p0, Landroid/animation/ObjectAnimator;

    invoke-virtual {p0}, Landroid/animation/ObjectAnimator;->getTarget()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;)[Landroid/animation/ObjectAnimator;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->j:[Landroid/animation/ObjectAnimator;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;)Landroid/animation/AnimatorListenerAdapter;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->k:Landroid/animation/AnimatorListenerAdapter;

    return-object v0
.end method

.method private e()V
    .locals 3

    .prologue
    .line 142
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c:I

    iget v1, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->d:I

    mul-int/2addr v0, v1

    new-array v0, v0, [Landroid/graphics/Point;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->h:[Landroid/graphics/Point;

    .line 143
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c:I

    iget v1, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->d:I

    mul-int/2addr v0, v1

    new-array v0, v0, [Landroid/animation/ObjectAnimator;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->j:[Landroid/animation/ObjectAnimator;

    .line 144
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->h:[Landroid/graphics/Point;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 145
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->h:[Landroid/graphics/Point;

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    aput-object v2, v1, v0

    .line 146
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->j:[Landroid/animation/ObjectAnimator;

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->i:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->clone()Landroid/animation/ObjectAnimator;

    move-result-object v2

    aput-object v2, v1, v0

    .line 144
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148
    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 169
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->d:I

    .line 170
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->e()V

    .line 171
    return-void
.end method

.method public a(IZ)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 208
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->b:I

    if-ne v0, p1, :cond_0

    .line 253
    :goto_0
    return-void

    .line 212
    :cond_0
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->b:I

    move v0, v1

    .line 214
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_3

    .line 215
    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-ne v0, p1, :cond_2

    const/4 v2, 0x1

    :goto_2
    invoke-virtual {v3, v2}, Landroid/view/View;->setSelected(Z)V

    .line 217
    if-eqz p2, :cond_1

    .line 220
    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->j:[Landroid/animation/ObjectAnimator;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 221
    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->j:[Landroid/animation/ObjectAnimator;

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->i:Landroid/animation/ObjectAnimator;

    invoke-virtual {v3}, Landroid/animation/ObjectAnimator;->clone()Landroid/animation/ObjectAnimator;

    move-result-object v3

    aput-object v3, v2, v0

    .line 222
    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->j:[Landroid/animation/ObjectAnimator;

    aget-object v2, v2, v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 223
    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->j:[Landroid/animation/ObjectAnimator;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->setupStartValues()V

    .line 214
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v2, v1

    .line 215
    goto :goto_2

    .line 227
    :cond_3
    if-eqz p2, :cond_4

    .line 228
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 229
    new-instance v1, Lcaz;

    invoke-direct {v1, p0, v0}, Lcaz;-><init>(Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;Landroid/view/ViewTreeObserver;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 251
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->invalidate()V

    .line 252
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->requestLayout()V

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 162
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->d:I

    return v0
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 184
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c:I

    .line 185
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->e()V

    .line 186
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c:I

    return v0
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 192
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->e:I

    .line 193
    return-void
.end method

.method public d()I
    .locals 1

    .prologue
    .line 259
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->b:I

    return v0
.end method

.method protected getChildDrawingOrder(II)I
    .locals 2

    .prologue
    .line 312
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    .line 324
    :cond_0
    :goto_0
    return p2

    .line 317
    :cond_1
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->b:I

    if-lt p2, v0, :cond_0

    .line 320
    add-int/lit8 v0, p1, -0x1

    if-ne p2, v0, :cond_2

    .line 321
    iget p2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->b:I

    goto :goto_0

    .line 324
    :cond_2
    add-int/lit8 p2, p2, 0x1

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 12

    .prologue
    .line 329
    iget v8, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->b:I

    const/4 v0, -0x1

    if-eq v8, v0, :cond_4

    const/4 v0, 0x1

    :goto_0
    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    iget v1, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->d:I

    div-int v1, v8, v1

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a()Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x0

    :goto_2
    const/4 v3, 0x0

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->getChildCount()I

    move-result v7

    if-ge v3, v7, :cond_f

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a()Z

    move-result v7

    if-eqz v7, :cond_8

    iget v7, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->d:I

    iget v9, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->d:I

    div-int v9, v8, v9

    mul-int/2addr v7, v9

    if-ge v3, v7, :cond_7

    const/4 v7, 0x1

    :goto_4
    if-eqz v0, :cond_0

    if-nez v7, :cond_0

    if-nez v6, :cond_0

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a()Z

    move-result v9

    if-eqz v9, :cond_a

    iget-object v9, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->h:[Landroid/graphics/Point;

    aget-object v9, v9, v8

    iget v10, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->f:I

    mul-int/2addr v10, v5

    const/4 v11, 0x0

    invoke-virtual {v9, v10, v11}, Landroid/graphics/Point;->set(II)V

    :cond_0
    :goto_5
    if-nez v7, :cond_1

    if-eq v8, v3, :cond_d

    :cond_1
    :goto_6
    if-eqz v6, :cond_c

    if-eq v5, v1, :cond_2

    add-int/lit8 v7, v1, 0x1

    if-ne v5, v7, :cond_c

    :cond_2
    if-eq v4, v2, :cond_3

    add-int/lit8 v7, v2, 0x1

    if-ne v4, v7, :cond_c

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a()Z

    move-result v7

    if-eqz v7, :cond_b

    add-int/lit8 v4, v4, 0x1

    iget v7, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->d:I

    rem-int/2addr v4, v7

    if-nez v4, :cond_1

    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    :cond_4
    const/4 v0, 0x0

    goto :goto_0

    :cond_5
    const/4 v1, 0x0

    goto :goto_1

    :cond_6
    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c:I

    div-int v2, v8, v2

    goto :goto_2

    :cond_7
    const/4 v7, 0x0

    goto :goto_4

    :cond_8
    iget v7, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c:I

    iget v9, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c:I

    div-int v9, v8, v9

    mul-int/2addr v7, v9

    if-ge v3, v7, :cond_9

    const/4 v7, 0x1

    goto :goto_4

    :cond_9
    const/4 v7, 0x0

    goto :goto_4

    :cond_a
    iget-object v9, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->h:[Landroid/graphics/Point;

    aget-object v9, v9, v8

    const/4 v10, 0x0

    iget v11, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->f:I

    mul-int/2addr v11, v4

    invoke-virtual {v9, v10, v11}, Landroid/graphics/Point;->set(II)V

    goto :goto_5

    :cond_b
    add-int/lit8 v5, v5, 0x1

    iget v7, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c:I

    rem-int/2addr v5, v7

    if-nez v5, :cond_1

    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_c
    iget-object v7, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->h:[Landroid/graphics/Point;

    aget-object v7, v7, v3

    iget v9, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->f:I

    mul-int/2addr v9, v5

    iget v10, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->f:I

    mul-int/2addr v10, v4

    invoke-virtual {v7, v9, v10}, Landroid/graphics/Point;->set(II)V

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a()Z

    move-result v7

    if-eqz v7, :cond_e

    add-int/lit8 v4, v4, 0x1

    iget v7, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->d:I

    rem-int/2addr v4, v7

    if-nez v4, :cond_d

    add-int/lit8 v5, v5, 0x1

    :cond_d
    :goto_7
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    :cond_e
    add-int/lit8 v5, v5, 0x1

    iget v7, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c:I

    rem-int/2addr v5, v7

    if-nez v5, :cond_d

    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    :cond_f
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->h:[Landroid/graphics/Point;

    .line 330
    const/4 v0, 0x0

    :goto_8
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_11

    .line 331
    aget-object v2, v1, v0

    .line 332
    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 334
    iget v4, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->b:I

    if-ne v0, v4, :cond_10

    .line 335
    iget v4, v2, Landroid/graphics/Point;->x:I

    iget v5, v2, Landroid/graphics/Point;->y:I

    iget v6, v2, Landroid/graphics/Point;->x:I

    iget v7, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->f:I

    mul-int/lit8 v7, v7, 0x2

    add-int/2addr v6, v7

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget v7, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->f:I

    mul-int/lit8 v7, v7, 0x2

    add-int/2addr v2, v7

    invoke-virtual {v3, v4, v5, v6, v2}, Landroid/view/View;->layout(IIII)V

    .line 330
    :goto_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 338
    :cond_10
    iget v4, v2, Landroid/graphics/Point;->x:I

    iget v5, v2, Landroid/graphics/Point;->y:I

    iget v6, v2, Landroid/graphics/Point;->x:I

    iget v7, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->f:I

    add-int/2addr v6, v7

    iget v2, v2, Landroid/graphics/Point;->y:I

    iget v7, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->f:I

    add-int/2addr v2, v7

    invoke-virtual {v3, v4, v5, v6, v2}, Landroid/view/View;->layout(IIII)V

    goto :goto_9

    .line 342
    :cond_11
    return-void
.end method

.method protected onMeasure(II)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    const/high16 v5, 0x40000000    # 2.0f

    .line 266
    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->g:Z

    if-eqz v0, :cond_0

    .line 267
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c:I

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->f:I

    .line 269
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 270
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->e:I

    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->d:I

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, -0x1

    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->d:I

    div-int/2addr v0, v2

    .line 271
    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->b:I

    if-eq v2, v3, :cond_2

    .line 273
    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->e:I

    add-int/lit8 v2, v2, 0x3

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->d:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->d:I

    div-int/2addr v2, v3

    .line 276
    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->b:I

    iget v4, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->d:I

    div-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_1

    .line 277
    add-int/lit8 v0, v0, 0x1

    .line 279
    :cond_1
    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 281
    :cond_2
    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->f:I

    mul-int/2addr v0, v2

    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->d:I

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->f:I

    mul-int/2addr v2, v3

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->setMeasuredDimension(II)V

    .line 297
    :goto_1
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->e:I

    if-ge v1, v0, :cond_8

    .line 298
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->b:I

    if-ne v0, v1, :cond_7

    .line 299
    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->f:I

    mul-int/lit8 v2, v2, 0x2

    .line 300
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->f:I

    mul-int/lit8 v3, v3, 0x2

    .line 301
    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 299
    invoke-virtual {v0, v2, v3}, Landroid/view/View;->measure(II)V

    .line 297
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 267
    :cond_3
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c:I

    div-int/2addr v0, v2

    goto :goto_0

    .line 283
    :cond_4
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->e:I

    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c:I

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, -0x1

    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c:I

    div-int/2addr v0, v2

    .line 284
    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->b:I

    if-eq v2, v3, :cond_6

    .line 286
    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->e:I

    add-int/lit8 v2, v2, 0x3

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c:I

    add-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c:I

    div-int/2addr v2, v3

    .line 289
    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->b:I

    iget v4, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c:I

    div-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x1

    if-ne v3, v0, :cond_5

    .line 290
    add-int/lit8 v0, v0, 0x1

    .line 292
    :cond_5
    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 294
    :cond_6
    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c:I

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->f:I

    mul-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->f:I

    mul-int/2addr v0, v3

    invoke-virtual {p0, v2, v0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->setMeasuredDimension(II)V

    goto :goto_1

    .line 303
    :cond_7
    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->f:I

    .line 304
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->f:I

    .line 305
    invoke-static {v3, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 303
    invoke-virtual {v0, v2, v3}, Landroid/view/View;->measure(II)V

    goto :goto_2

    .line 308
    :cond_8
    return-void
.end method
