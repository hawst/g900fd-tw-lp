.class public Lcom/google/android/apps/moviemaker/aggregator/Algorithms;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-string v0, "moviemaker-jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method public static a([FIII)F
    .locals 2

    .prologue
    .line 21
    if-ge p2, p3, :cond_0

    if-lt p2, p1, :cond_0

    array-length v0, p0

    if-ge v0, p3, :cond_1

    .line 22
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Out of bound"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_1
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/apps/moviemaker/aggregator/Algorithms;->nthElement([FIIII)F

    move-result v0

    return v0
.end method

.method private static native nthElement([FIIII)F
.end method
