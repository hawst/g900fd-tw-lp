.class public Lcom/google/android/apps/moviemaker/filterpacks/numeric/CompareBlockHistogramFilter;
.super Laak;
.source "PG"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    const-string v0, "moviemaker-jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 81
    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 24
    return-void
.end method

.method private native compareHistograms(Ljava/nio/IntBuffer;Ljava/nio/IntBuffer;ILjava/nio/IntBuffer;)F
.end method


# virtual methods
.method public c()Lacx;
    .locals 5

    .prologue
    const/16 v1, 0x66

    const/4 v4, 0x2

    .line 28
    invoke-static {v1}, Labf;->b(I)Labf;

    move-result-object v0

    .line 29
    invoke-static {v1}, Labf;->a(I)Labf;

    move-result-object v1

    .line 31
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "histograms1"

    .line 32
    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v2

    const-string v3, "histograms2"

    .line 33
    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "values"

    .line 34
    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 35
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 40
    const-string v0, "histograms1"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/CompareBlockHistogramFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    .line 41
    const-string v1, "histograms2"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/CompareBlockHistogramFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    .line 42
    invoke-virtual {v0}, Laap;->d()Laar;

    move-result-object v2

    .line 43
    invoke-virtual {v1}, Laap;->d()Laar;

    move-result-object v3

    .line 45
    if-ne v0, v1, :cond_0

    .line 74
    :goto_0
    return-void

    .line 50
    :cond_0
    invoke-virtual {v2}, Laar;->j()I

    move-result v0

    invoke-virtual {v3}, Laar;->j()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 51
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can only compare an equal number of histograms"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_1
    const-string v0, "values"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/CompareBlockHistogramFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    new-array v1, v6, [I

    const/4 v4, 0x0

    .line 55
    invoke-virtual {v2}, Laar;->j()I

    move-result v5

    aput v5, v1, v4

    invoke-virtual {v0, v1}, Lacv;->a([I)Laap;

    move-result-object v0

    .line 56
    invoke-virtual {v0}, Laap;->c()Laaq;

    move-result-object v0

    .line 58
    invoke-virtual {v2, v6}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v1

    .line 59
    invoke-virtual {v3, v6}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v4

    .line 61
    const/4 v5, 0x2

    invoke-virtual {v0, v5}, Laaq;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 62
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 63
    invoke-virtual {v5}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v5

    .line 65
    invoke-virtual {v2}, Laar;->j()I

    move-result v6

    invoke-direct {p0, v1, v4, v6, v5}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/CompareBlockHistogramFilter;->compareHistograms(Ljava/nio/IntBuffer;Ljava/nio/IntBuffer;ILjava/nio/IntBuffer;)F

    .line 68
    invoke-virtual {v2}, Laar;->h()V

    .line 69
    invoke-virtual {v3}, Laar;->h()V

    .line 71
    invoke-virtual {v0}, Laaq;->h()V

    .line 73
    const-string v1, "values"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/CompareBlockHistogramFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    invoke-virtual {v1, v0}, Lacv;->a(Laap;)V

    goto :goto_0
.end method
