.class public Lcom/google/android/apps/moviemaker/analyzer/postprocess/StabilizationPostProcessor;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lajj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private native getContextLength()I
.end method

.method private native stabilizationMotionFilteringProcess(Ljava/nio/ByteBuffer;[F[F[F)V
.end method

.method private native stabilizationMotionFilteringSetUp(Ljava/nio/ByteBuffer;I)V
.end method

.method private native stabilizationMotionFilteringTearDown(Ljava/nio/ByteBuffer;)V
.end method


# virtual methods
.method public a(Lbkv;)V
    .locals 13

    .prologue
    const/16 v12, 0x9

    const/4 v1, 0x0

    .line 22
    sget-object v0, Lbmz;->q:Lbmz;

    .line 23
    invoke-virtual {p1, v0}, Lbkv;->a(Lbmz;)Lbkw;

    move-result-object v4

    .line 26
    if-nez v4, :cond_1

    .line 77
    :cond_0
    return-void

    .line 30
    :cond_1
    invoke-virtual {v4}, Lbkw;->a()I

    move-result v5

    .line 33
    mul-int/lit8 v0, v5, 0x9

    new-array v6, v0, [F

    .line 34
    mul-int/lit8 v0, v5, 0x9

    new-array v7, v0, [F

    .line 35
    mul-int/lit8 v0, v5, 0x9

    new-array v8, v0, [F

    move v3, v1

    .line 38
    :goto_0
    if-ge v3, v5, :cond_3

    .line 39
    invoke-virtual {v4, v3}, Lbkw;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxi;

    move v2, v1

    .line 40
    :goto_1
    if-ge v2, v12, :cond_2

    .line 41
    mul-int/lit8 v9, v3, 0x9

    add-int/2addr v9, v2

    .line 42
    invoke-virtual {v0, v2}, Lbxi;->a(I)F

    move-result v10

    aput v10, v6, v9

    .line 40
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 38
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 49
    :cond_3
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/analyzer/postprocess/StabilizationPostProcessor;->getContextLength()I

    move-result v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 50
    invoke-direct {p0, v0, v5}, Lcom/google/android/apps/moviemaker/analyzer/postprocess/StabilizationPostProcessor;->stabilizationMotionFilteringSetUp(Ljava/nio/ByteBuffer;I)V

    .line 51
    invoke-direct {p0, v0, v6, v7, v8}, Lcom/google/android/apps/moviemaker/analyzer/postprocess/StabilizationPostProcessor;->stabilizationMotionFilteringProcess(Ljava/nio/ByteBuffer;[F[F[F)V

    .line 53
    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/analyzer/postprocess/StabilizationPostProcessor;->stabilizationMotionFilteringTearDown(Ljava/nio/ByteBuffer;)V

    move v2, v1

    .line 56
    :goto_2
    if-ge v2, v5, :cond_0

    .line 57
    invoke-virtual {v4, v2}, Lbkw;->a(I)J

    move-result-wide v10

    .line 58
    invoke-static {}, Lbxi;->a()[F

    move-result-object v3

    .line 59
    invoke-static {}, Lbxi;->a()[F

    move-result-object v6

    move v0, v1

    .line 61
    :goto_3
    if-ge v0, v12, :cond_4

    .line 62
    mul-int/lit8 v9, v2, 0x9

    add-int/2addr v9, v0

    aget v9, v8, v9

    aput v9, v3, v0

    .line 64
    mul-int/lit8 v9, v2, 0x9

    add-int/2addr v9, v0

    aget v9, v7, v9

    aput v9, v6, v0

    .line 61
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 67
    :cond_4
    sget-object v0, Lbmz;->b:Lbmz;

    .line 70
    invoke-static {v3}, Lbxi;->a([F)Lbxi;

    move-result-object v3

    .line 67
    invoke-virtual {p1, v0, v10, v11, v3}, Lbkv;->a(Lbmz;JLjava/lang/Object;)Lbkv;

    .line 72
    sget-object v0, Lbmz;->r:Lbmz;

    .line 74
    invoke-static {v6}, Lbxi;->a([F)Lbxi;

    move-result-object v3

    .line 72
    invoke-virtual {p1, v0, v10, v11, v3}, Lbkv;->a(Lbmz;JLjava/lang/Object;)Lbkv;

    .line 56
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2
.end method
