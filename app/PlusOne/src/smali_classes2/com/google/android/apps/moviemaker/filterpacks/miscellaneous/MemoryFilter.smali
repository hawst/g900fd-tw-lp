.class public Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;
.super Laak;
.source "PG"


# static fields
.field private static final PHASE_FILLING_BUFFER:I = 0x0

.field private static final PHASE_NORMAL:I = 0x1


# instance fields
.field private mNextSlot:I

.field private mPhase:I

.field private mPreviousFrames:[Laap;

.field private mSlots:I


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 31
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 25
    new-array v0, v1, [Laap;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mPreviousFrames:[Laap;

    .line 26
    iput v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mPhase:I

    .line 27
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mSlots:I

    .line 28
    iput v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mNextSlot:I

    .line 32
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 2

    .prologue
    .line 46
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "slots"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    const-string v0, "mSlots"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 48
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->a(Z)V

    .line 50
    :cond_0
    return-void
.end method

.method public c()Lacx;
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 36
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "current"

    .line 37
    invoke-static {}, Labf;->a()Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v4, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "current"

    .line 38
    invoke-static {}, Labf;->a()Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v4, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "slots"

    const/4 v2, 0x1

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    .line 39
    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "previous"

    .line 40
    invoke-static {}, Labf;->a()Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v4, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 54
    const-string v0, "previous"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 55
    const-string v1, "current"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    .line 56
    const-string v2, "current"

    invoke-virtual {p0, v2}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v2

    .line 58
    iget v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mSlots:I

    iget-object v4, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mPreviousFrames:[Laap;

    array-length v4, v4

    if-eq v3, v4, :cond_0

    .line 59
    iget v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mSlots:I

    new-array v3, v3, [Laap;

    iput-object v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mPreviousFrames:[Laap;

    .line 60
    iput v5, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mPhase:I

    .line 61
    iput v5, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mNextSlot:I

    .line 64
    :cond_0
    iget v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mPhase:I

    packed-switch v3, :pswitch_data_0

    .line 96
    :cond_1
    :goto_0
    return-void

    .line 69
    :pswitch_0
    iget-object v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mPreviousFrames:[Laap;

    iget v4, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mNextSlot:I

    invoke-virtual {v2}, Lacp;->c()Laap;

    move-result-object v5

    aput-object v5, v3, v4

    .line 70
    iget-object v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mPreviousFrames:[Laap;

    iget v4, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mNextSlot:I

    aget-object v3, v3, v4

    invoke-virtual {v3}, Laap;->g()Laap;

    .line 72
    iget-object v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mPreviousFrames:[Laap;

    iget v4, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mNextSlot:I

    aget-object v3, v3, v4

    invoke-virtual {v1, v3}, Lacv;->a(Laap;)V

    .line 73
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mPreviousFrames:[Laap;

    iget v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mNextSlot:I

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    .line 75
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mNextSlot:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mSlots:I

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mNextSlot:I

    .line 76
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mNextSlot:I

    if-nez v0, :cond_1

    .line 77
    iput v6, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mPhase:I

    .line 78
    invoke-virtual {v2, v6}, Lacp;->b(Z)V

    goto :goto_0

    .line 85
    :pswitch_1
    invoke-virtual {v2}, Lacp;->c()Laap;

    move-result-object v2

    .line 86
    invoke-virtual {v1, v2}, Lacv;->a(Laap;)V

    .line 87
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mPreviousFrames:[Laap;

    iget v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mNextSlot:I

    aget-object v1, v1, v3

    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mPreviousFrames:[Laap;

    iget v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mNextSlot:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Laap;->f()Laap;

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mPreviousFrames:[Laap;

    iget v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mNextSlot:I

    aput-object v2, v0, v1

    .line 91
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mPreviousFrames:[Laap;

    iget v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mNextSlot:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Laap;->g()Laap;

    .line 93
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mNextSlot:I

    add-int/lit8 v0, v0, 0x1

    iget v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mSlots:I

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/MemoryFilter;->mNextSlot:I

    goto :goto_0

    .line 64
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
