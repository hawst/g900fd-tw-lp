.class public Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;
.super Laak;
.source "PG"


# static fields
.field private static final BASE_FIX_EXPOSURE:I = 0x4b

.field private static final COLOR_DEPTH:I = 0x100

.field private static final FINE_TUNE_COLOR:I = 0x64

.field private static final LOOKUP_TABLE_MAX:I = 0x1000

.field private static final LOOKUP_TABLE_MAX_IDX:I = 0xfff

.field private static final LOOKUP_TABLE_MIN:I = -0x800

.field private static final LOOKUP_TABLE_ZERO:I = 0x800

.field private static final LOOKUP_TEXTURE_SIZE:I = 0x300

.field private static final MAX_VALUE:I = 0x7ff

.field private static final SCALE_FACTOR:I = 0x8


# instance fields
.field private mHistogramB:[J

.field private mHistogramG:[J

.field private mHistogramR:[J

.field private mListener:Lbbr;

.field private mTotalCount:J


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 54
    return-void
.end method

.method private static a(I)I
    .locals 3

    .prologue
    .line 344
    int-to-float v0, p0

    const/high16 v1, 0x41000000    # 8.0f

    div-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    float-to-int v0, v0

    .line 345
    const/4 v1, 0x0

    const/16 v2, 0xff

    invoke-static {v0, v1, v2}, Lcfn;->a(III)I

    move-result v0

    return v0
.end method

.method private static a(III)I
    .locals 1

    .prologue
    .line 154
    invoke-static {p0, p1}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v0, p2}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method private a([J)I
    .locals 8

    .prologue
    .line 161
    const-wide/16 v2, 0x0

    .line 162
    const/4 v0, 0x1

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 163
    int-to-long v4, v0

    aget-wide v6, p1, v0

    mul-long/2addr v4, v6

    add-long/2addr v2, v4

    .line 162
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 165
    :cond_0
    iget-wide v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mTotalCount:J

    div-long v0, v2, v0

    long-to-int v0, v0

    return v0
.end method

.method private a([JI)I
    .locals 8

    .prologue
    .line 175
    const-wide/16 v2, 0x0

    .line 176
    const/16 v0, 0xff

    .line 177
    :goto_0
    iget-wide v4, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mTotalCount:J

    int-to-long v6, p2

    mul-long/2addr v4, v6

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 178
    aget-wide v4, p1, v0

    add-long/2addr v2, v4

    .line 179
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 181
    :cond_0
    shl-int/lit8 v0, v0, 0x3

    return v0
.end method

.method private static a(IIIII)[I
    .locals 8

    .prologue
    .line 354
    const/16 v0, 0x1800

    new-array v3, v0, [I

    .line 355
    const/16 v0, -0x800

    :goto_0
    if-gez v0, :cond_0

    .line 356
    add-int/lit16 v1, v0, 0x800

    aput v0, v3, v1

    .line 355
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 358
    :cond_0
    const/4 v0, 0x0

    :goto_1
    const/16 v1, 0x800

    if-ge v0, v1, :cond_1

    .line 359
    add-int/lit16 v1, v0, 0x800

    sub-int v2, v0, p0

    shl-int/lit8 v2, v2, 0xb

    sub-int v4, p1, p0

    div-int/2addr v2, v4

    aput v2, v3, v1

    .line 358
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 361
    :cond_1
    const/16 v0, 0x800

    :goto_2
    const/16 v1, 0x1000

    if-ge v0, v1, :cond_2

    .line 362
    add-int/lit16 v1, v0, 0x800

    aput v0, v3, v1

    .line 361
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 364
    :cond_2
    const/16 v0, -0x800

    :goto_3
    const/16 v1, 0x1000

    if-ge v0, v1, :cond_5

    add-int/lit16 v1, v0, 0x800

    aget v1, v3, v1

    if-gez v1, :cond_3

    add-int/lit16 v1, v0, 0x800

    const/4 v2, 0x0

    aput v2, v3, v1

    :cond_3
    add-int/lit16 v1, v0, 0x800

    aget v1, v3, v1

    const/16 v2, 0x7ff

    if-le v1, v2, :cond_4

    add-int/lit16 v1, v0, 0x800

    const/16 v2, 0x7ff

    aput v2, v3, v1

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 365
    :cond_5
    const/16 v0, 0x1800

    new-array v4, v0, [I

    const/16 v0, -0x800

    move v2, v0

    :goto_4
    const/16 v0, 0x1000

    if-ge v2, v0, :cond_7

    sub-int v0, v2, p2

    const/16 v1, -0x800

    if-ge v0, v1, :cond_12

    const/16 v0, -0x800

    move v1, v0

    :goto_5
    add-int v0, v2, p2

    const/16 v5, 0xfff

    if-le v0, v5, :cond_6

    const/16 v0, 0xfff

    :cond_6
    add-int/lit16 v5, v2, 0x800

    add-int/lit16 v1, v1, 0x800

    aget v1, v3, v1

    add-int/lit16 v6, v2, 0x800

    aget v6, v3, v6

    add-int/2addr v1, v6

    add-int/lit16 v0, v0, 0x800

    aget v0, v3, v0

    add-int/2addr v0, v1

    aput v0, v4, v5

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    :cond_7
    const/16 v0, -0x800

    move v2, v0

    :goto_6
    const/16 v0, 0x1000

    if-ge v2, v0, :cond_a

    mul-int/lit8 v0, v2, 0x2

    sub-int/2addr v0, p2

    add-int/lit8 v0, v0, 0x1

    div-int/lit8 v0, v0, 0x2

    const/16 v1, -0x800

    if-ge v0, v1, :cond_8

    const/16 v0, -0x800

    :cond_8
    mul-int/lit8 v1, v2, 0x2

    add-int/2addr v1, p2

    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v1, v1, 0x2

    const/16 v5, 0xfff

    if-le v1, v5, :cond_9

    const/16 v1, 0xfff

    :cond_9
    add-int/lit16 v0, v0, 0x800

    aget v0, v4, v0

    add-int/lit16 v5, v2, 0x800

    aget v5, v4, v5

    add-int/2addr v0, v5

    add-int/lit16 v1, v1, 0x800

    aget v1, v4, v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x4

    div-int/lit8 v0, v0, 0x9

    add-int/lit16 v1, v2, 0x800

    add-int/lit16 v5, v2, 0x800

    aget v5, v3, v5

    invoke-static {v5, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    aput v0, v3, v1

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 366
    :cond_a
    const/16 v0, 0x1800

    new-array v4, v0, [I

    const/16 v0, -0x800

    move v2, v0

    :goto_7
    const/16 v0, 0x1000

    if-ge v2, v0, :cond_c

    sub-int v0, v2, p3

    const/16 v1, -0x800

    if-ge v0, v1, :cond_11

    const/16 v0, -0x800

    move v1, v0

    :goto_8
    add-int v0, v2, p3

    const/16 v5, 0xfff

    if-le v0, v5, :cond_b

    const/16 v0, 0xfff

    :cond_b
    add-int/lit16 v5, v2, 0x800

    add-int/lit16 v1, v1, 0x800

    aget v1, v3, v1

    add-int/lit16 v6, v2, 0x800

    aget v6, v3, v6

    add-int/2addr v1, v6

    add-int/lit16 v0, v0, 0x800

    aget v0, v3, v0

    add-int/2addr v0, v1

    aput v0, v4, v5

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_7

    :cond_c
    const/16 v0, -0x800

    move v2, v0

    :goto_9
    const/16 v0, 0x1000

    if-ge v2, v0, :cond_f

    mul-int/lit8 v0, v2, 0x2

    sub-int/2addr v0, p3

    add-int/lit8 v0, v0, 0x1

    div-int/lit8 v0, v0, 0x2

    const/16 v1, -0x800

    if-ge v0, v1, :cond_d

    const/16 v0, -0x800

    :cond_d
    mul-int/lit8 v1, v2, 0x2

    add-int/2addr v1, p3

    add-int/lit8 v1, v1, 0x1

    div-int/lit8 v1, v1, 0x2

    const/16 v5, 0xfff

    if-le v1, v5, :cond_e

    const/16 v1, 0xfff

    :cond_e
    add-int/lit16 v0, v0, 0x800

    aget v0, v4, v0

    add-int/lit16 v5, v2, 0x800

    aget v5, v4, v5

    add-int/2addr v0, v5

    add-int/lit16 v1, v1, 0x800

    aget v1, v4, v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x4

    div-int/lit8 v0, v0, 0x9

    add-int/lit16 v1, v2, 0x800

    add-int/lit16 v5, v2, 0x800

    aget v5, v3, v5

    invoke-static {v5, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    aput v0, v3, v1

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_9

    .line 367
    :cond_f
    const/16 v0, -0x800

    :goto_a
    const/16 v1, 0x1000

    if-ge v0, v1, :cond_10

    int-to-double v4, v0

    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    sub-double/2addr v4, v6

    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    div-double/2addr v4, v6

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    mul-double/2addr v4, v4

    sub-double v4, v6, v4

    add-int/lit16 v1, v0, 0x800

    add-int/lit16 v2, v0, 0x800

    aget v2, v3, v2

    int-to-double v6, p4

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    add-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v4, v4

    add-int/2addr v2, v4

    aput v2, v3, v1

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 368
    :cond_10
    return-object v3

    :cond_11
    move v1, v0

    goto/16 :goto_8

    :cond_12
    move v1, v0

    goto/16 :goto_5
.end method

.method private b([JI)I
    .locals 8

    .prologue
    .line 191
    const-wide/16 v2, 0x0

    .line 192
    const/4 v0, 0x0

    .line 193
    :goto_0
    iget-wide v4, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mTotalCount:J

    int-to-long v6, p2

    mul-long/2addr v4, v6

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 194
    aget-wide v4, p1, v0

    add-long/2addr v2, v4

    .line 195
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 197
    :cond_0
    shl-int/lit8 v0, v0, 0x3

    return v0
.end method


# virtual methods
.method public a(Lbbr;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mListener:Lbbr;

    .line 58
    return-void
.end method

.method public c()Lacx;
    .locals 5

    .prologue
    const/16 v2, 0x12d

    const/4 v4, 0x2

    .line 62
    invoke-static {v2, v4}, Labf;->a(II)Labf;

    move-result-object v0

    .line 63
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 64
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    const/4 v3, 0x1

    .line 65
    invoke-virtual {v0, v2, v3, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected h()V
    .locals 2

    .prologue
    const/16 v1, 0x100

    .line 71
    new-array v0, v1, [J

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramR:[J

    .line 72
    new-array v0, v1, [J

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramG:[J

    .line 73
    new-array v0, v1, [J

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramB:[J

    .line 74
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mTotalCount:J

    .line 75
    return-void
.end method

.method protected i()V
    .locals 14

    .prologue
    const/4 v1, 0x0

    const-wide/16 v12, 0x1

    .line 79
    const-string v0, "image"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    .line 80
    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v3

    .line 81
    invoke-virtual {v3}, Laas;->n()Landroid/graphics/Bitmap;

    move-result-object v4

    .line 82
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 83
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    move v2, v1

    .line 84
    :goto_0
    if-ge v2, v5, :cond_1

    move v0, v1

    .line 85
    :goto_1
    if-ge v0, v6, :cond_0

    .line 86
    invoke-virtual {v4, v2, v0}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v7

    .line 87
    iget-object v8, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramR:[J

    invoke-static {v7}, Landroid/graphics/Color;->red(I)I

    move-result v9

    aget-wide v10, v8, v9

    add-long/2addr v10, v12

    aput-wide v10, v8, v9

    .line 88
    iget-object v8, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramG:[J

    invoke-static {v7}, Landroid/graphics/Color;->green(I)I

    move-result v9

    aget-wide v10, v8, v9

    add-long/2addr v10, v12

    aput-wide v10, v8, v9

    .line 89
    iget-object v8, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramB:[J

    invoke-static {v7}, Landroid/graphics/Color;->blue(I)I

    move-result v7

    aget-wide v10, v8, v7

    add-long/2addr v10, v12

    aput-wide v10, v8, v7

    .line 90
    iget-wide v8, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mTotalCount:J

    add-long/2addr v8, v12

    iput-wide v8, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mTotalCount:J

    .line 85
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 84
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 93
    :cond_1
    const-string v0, "image"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 94
    if-eqz v0, :cond_2

    .line 95
    invoke-virtual {v0, v3}, Lacv;->a(Laap;)V

    .line 97
    :cond_2
    return-void
.end method

.method protected j()V
    .locals 26

    .prologue
    .line 101
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mListener:Lbbr;

    if-nez v2, :cond_0

    .line 151
    :goto_0
    return-void

    .line 106
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramR:[J

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a([J)I

    move-result v9

    .line 107
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramG:[J

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a([J)I

    move-result v10

    .line 108
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramB:[J

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a([J)I

    move-result v11

    .line 111
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramR:[J

    const/16 v3, 0xf

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->b([JI)I

    move-result v3

    .line 112
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramG:[J

    const/16 v4, 0xf

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v4}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->b([JI)I

    move-result v4

    .line 113
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramB:[J

    const/16 v5, 0xf

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v5}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->b([JI)I

    move-result v5

    .line 116
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramR:[J

    const/16 v6, 0x64

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v6}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->b([JI)I

    move-result v6

    .line 117
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramG:[J

    const/16 v7, 0x64

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v7}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->b([JI)I

    move-result v7

    .line 118
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramB:[J

    const/16 v8, 0x64

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v8}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->b([JI)I

    move-result v12

    .line 121
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramR:[J

    const/16 v8, 0xf

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v8}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a([JI)I

    move-result v13

    .line 122
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramG:[J

    const/16 v8, 0xf

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v8}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a([JI)I

    move-result v14

    .line 123
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramB:[J

    const/16 v8, 0xf

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v8}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a([JI)I

    move-result v15

    .line 126
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramR:[J

    const/16 v8, 0x64

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v8}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a([JI)I

    move-result v16

    .line 127
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramG:[J

    const/16 v8, 0x64

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v8}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a([JI)I

    move-result v17

    .line 128
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramB:[J

    const/16 v8, 0x64

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v8}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a([JI)I

    move-result v18

    .line 130
    invoke-static {v13, v14, v15}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a(III)I

    move-result v2

    div-int/lit8 v2, v2, 0x8

    invoke-static/range {v16 .. v18}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a(III)I

    move-result v8

    div-int/lit8 v8, v8, 0x8

    invoke-static {v9, v10, v11}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a(III)I

    move-result v19

    invoke-static {v3, v4, v5}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a(III)I

    move-result v20

    div-int/lit8 v20, v20, 0x8

    invoke-static {v6, v7, v12}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a(III)I

    move-result v21

    div-int/lit8 v21, v21, 0x8

    sub-int v22, v2, v20

    sub-int v23, v8, v21

    add-int v24, v21, v8

    add-int v24, v24, v19

    sub-int v8, v2, v8

    sub-int v20, v21, v20

    const/16 v2, 0x64

    const/16 v25, 0x8c

    move/from16 v0, v22

    move/from16 v1, v25

    if-ge v0, v1, :cond_1

    move/from16 v0, v22

    rsub-int v2, v0, 0x8c

    mul-int/lit8 v2, v2, 0x2d

    div-int/lit16 v2, v2, 0x8c

    rsub-int/lit8 v2, v2, 0x64

    :cond_1
    const/16 v22, 0x5a

    move/from16 v0, v23

    move/from16 v1, v22

    if-ge v0, v1, :cond_2

    rsub-int/lit8 v22, v23, 0x5a

    mul-int/lit8 v22, v22, 0x37

    div-int/lit8 v22, v22, 0x5a

    sub-int v2, v2, v22

    :cond_2
    const/16 v22, 0x96

    move/from16 v0, v24

    move/from16 v1, v22

    if-ge v0, v1, :cond_3

    move/from16 v0, v24

    rsub-int v0, v0, 0x96

    move/from16 v22, v0

    mul-int/lit8 v22, v22, 0x23

    move/from16 v0, v22

    div-int/lit16 v0, v0, 0x96

    move/from16 v22, v0

    sub-int v2, v2, v22

    :cond_3
    mul-int/lit8 v2, v2, 0x4b

    div-int/lit8 v2, v2, 0x64

    const/16 v22, 0x3c

    move/from16 v0, v19

    move/from16 v1, v22

    if-ge v0, v1, :cond_4

    const/16 v22, 0x16

    move/from16 v0, v22

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    :cond_4
    const/16 v22, 0x5

    move/from16 v0, v21

    move/from16 v1, v22

    if-ge v0, v1, :cond_5

    const/16 v22, 0x50

    move/from16 v0, v19

    move/from16 v1, v22

    if-ge v0, v1, :cond_5

    const/16 v19, 0x1e

    move/from16 v0, v19

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    :cond_5
    const/16 v19, 0x32

    move/from16 v0, v23

    move/from16 v1, v19

    if-ge v0, v1, :cond_6

    const/16 v19, 0x1b

    move/from16 v0, v19

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    :cond_6
    sub-int v19, v8, v20

    const/16 v22, 0x1e

    move/from16 v0, v19

    move/from16 v1, v22

    if-le v0, v1, :cond_7

    const/16 v19, 0x14

    move/from16 v0, v21

    move/from16 v1, v19

    if-ge v0, v1, :cond_7

    mul-int/lit8 v19, v20, 0x64

    div-int v8, v19, v8

    const/16 v19, 0x14

    move/from16 v0, v19

    if-ge v8, v0, :cond_7

    const/16 v8, 0x1e

    invoke-static {v8, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    :cond_7
    const/16 v8, 0xf

    if-ge v2, v8, :cond_8

    const/16 v2, 0xf

    .line 136
    :cond_8
    const/16 v8, 0x64

    const/16 v19, 0x0

    const-wide v20, 0x4050600000000000L    # 65.5

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->floor(D)D

    move-result-wide v20

    move-wide/from16 v0, v20

    double-to-int v0, v0

    move/from16 v20, v0

    add-int v20, v20, v2

    invoke-static/range {v19 .. v20}, Ljava/lang/Math;->max(II)I

    move-result v19

    move/from16 v0, v19

    invoke-static {v8, v0}, Ljava/lang/Math;->min(II)I

    move-result v19

    div-int/lit8 v20, v2, 0x2

    div-int/lit8 v21, v2, 0x4

    const/16 v8, 0x32

    if-ge v2, v8, :cond_c

    shl-int/lit8 v12, v2, 0x1

    mul-int/2addr v3, v12

    div-int/lit8 v8, v3, 0x64

    mul-int v3, v4, v12

    div-int/lit8 v7, v3, 0x64

    mul-int v3, v5, v12

    div-int/lit8 v6, v3, 0x64

    mul-int v3, v13, v12

    rsub-int/lit8 v4, v12, 0x64

    mul-int/lit16 v4, v4, 0x7ff

    add-int/2addr v3, v4

    div-int/lit8 v5, v3, 0x64

    mul-int v3, v14, v12

    rsub-int/lit8 v4, v12, 0x64

    mul-int/lit16 v4, v4, 0x7ff

    add-int/2addr v3, v4

    div-int/lit8 v4, v3, 0x64

    mul-int v3, v15, v12

    rsub-int/lit8 v12, v12, 0x64

    mul-int/lit16 v12, v12, 0x7ff

    add-int/2addr v3, v12

    div-int/lit8 v3, v3, 0x64

    :goto_1
    invoke-static {v8, v7, v6}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a(III)I

    move-result v12

    invoke-static {v4, v3}, Ljava/lang/Math;->max(II)I

    move-result v13

    invoke-static {v5, v13}, Ljava/lang/Math;->max(II)I

    move-result v13

    mul-int v8, v8, v19

    rsub-int/lit8 v14, v19, 0x64

    mul-int/2addr v14, v12

    add-int/2addr v8, v14

    div-int/lit8 v8, v8, 0x64

    mul-int v7, v7, v19

    rsub-int/lit8 v14, v19, 0x64

    mul-int/2addr v14, v12

    add-int/2addr v7, v14

    div-int/lit8 v7, v7, 0x64

    mul-int v6, v6, v19

    rsub-int/lit8 v14, v19, 0x64

    mul-int/2addr v12, v14

    add-int/2addr v6, v12

    div-int/lit8 v6, v6, 0x64

    mul-int v5, v5, v19

    rsub-int/lit8 v12, v19, 0x64

    mul-int/2addr v12, v13

    add-int/2addr v5, v12

    div-int/lit8 v5, v5, 0x64

    mul-int v4, v4, v19

    rsub-int/lit8 v12, v19, 0x64

    mul-int/2addr v12, v13

    add-int/2addr v4, v12

    div-int/lit8 v4, v4, 0x64

    mul-int v3, v3, v19

    rsub-int/lit8 v12, v19, 0x64

    mul-int/2addr v12, v13

    add-int/2addr v3, v12

    div-int/lit8 v3, v3, 0x64

    if-gt v5, v8, :cond_9

    add-int/lit8 v5, v8, 0x1

    :cond_9
    if-gt v4, v7, :cond_a

    add-int/lit8 v4, v7, 0x1

    :cond_a
    if-gt v3, v6, :cond_b

    add-int/lit8 v3, v6, 0x1

    :cond_b
    mul-int/lit8 v12, v21, 0x5

    mul-int/lit8 v13, v20, 0x5

    add-int/2addr v10, v9

    add-int/2addr v10, v11

    div-int/lit8 v10, v10, 0x3

    sub-int/2addr v9, v10

    mul-int/lit8 v9, v9, -0x1

    mul-int v9, v9, v19

    div-int/lit8 v9, v9, 0x64

    mul-int/lit8 v10, v9, -0x1

    mul-int v10, v10, v19

    div-int/lit8 v10, v10, 0x64

    invoke-static {v8, v5, v12, v13, v9}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a(IIIII)[I

    move-result-object v5

    invoke-static {v7, v4, v12, v13, v10}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a(IIIII)[I

    move-result-object v4

    invoke-static {v6, v3, v12, v13, v10}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a(IIIII)[I

    move-result-object v6

    const/16 v3, 0x300

    const/4 v7, 0x1

    sget-object v8, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v3, v7, v8}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    const/4 v3, 0x0

    :goto_2
    const/16 v8, 0x300

    if-ge v3, v8, :cond_d

    const/4 v8, 0x0

    shl-int/lit8 v9, v3, 0x3

    aget v9, v5, v9

    invoke-static {v9}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a(I)I

    move-result v9

    shl-int/lit8 v10, v3, 0x3

    aget v10, v4, v10

    invoke-static {v10}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a(I)I

    move-result v10

    shl-int/lit8 v11, v3, 0x3

    aget v11, v6, v11

    invoke-static {v11}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a(I)I

    move-result v11

    invoke-static {v9, v10, v11}, Landroid/graphics/Color;->rgb(III)I

    move-result v9

    invoke-virtual {v7, v3, v8, v9}, Landroid/graphics/Bitmap;->setPixel(III)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_c
    add-int/lit8 v8, v2, -0x32

    shl-int/lit8 v22, v8, 0x1

    mul-int v6, v6, v22

    rsub-int/lit8 v8, v22, 0x64

    mul-int/2addr v3, v8

    add-int/2addr v3, v6

    div-int/lit8 v8, v3, 0x64

    mul-int v3, v7, v22

    rsub-int/lit8 v6, v22, 0x64

    mul-int/2addr v4, v6

    add-int/2addr v3, v4

    div-int/lit8 v7, v3, 0x64

    mul-int v3, v12, v22

    rsub-int/lit8 v4, v22, 0x64

    mul-int/2addr v4, v5

    add-int/2addr v3, v4

    div-int/lit8 v6, v3, 0x64

    mul-int v3, v16, v22

    rsub-int/lit8 v4, v22, 0x64

    mul-int/2addr v4, v13

    add-int/2addr v3, v4

    div-int/lit8 v5, v3, 0x64

    mul-int v3, v17, v22

    rsub-int/lit8 v4, v22, 0x64

    mul-int/2addr v4, v14

    add-int/2addr v3, v4

    div-int/lit8 v4, v3, 0x64

    mul-int v3, v18, v22

    rsub-int/lit8 v12, v22, 0x64

    mul-int/2addr v12, v15

    add-int/2addr v3, v12

    div-int/lit8 v3, v3, 0x64

    goto/16 :goto_1

    .line 143
    :cond_d
    const/high16 v3, 0x42c80000    # 100.0f

    int-to-float v2, v2

    const/high16 v4, 0x41000000    # 8.0f

    div-float/2addr v2, v4

    sub-float v2, v3, v2

    const/high16 v3, 0x42c80000    # 100.0f

    div-float/2addr v2, v3

    .line 145
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mListener:Lbbr;

    new-instance v4, Lbbs;

    invoke-direct {v4, v7, v2}, Lbbs;-><init>(Landroid/graphics/Bitmap;F)V

    invoke-interface {v3, v4}, Lbbr;->a(Lbbs;)V

    .line 148
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramR:[J

    .line 149
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramG:[J

    .line 150
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->mHistogramB:[J

    goto/16 :goto_0
.end method
