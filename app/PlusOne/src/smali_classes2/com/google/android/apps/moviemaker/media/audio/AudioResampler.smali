.class public Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcgk;


# static fields
.field private static final a:F


# instance fields
.field private final b:Ljava/nio/ByteBuffer;

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:Z

.field private g:Z

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    .line 14
    const-class v0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 18
    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    div-double/2addr v0, v2

    double-to-float v0, v0

    sput v0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->a:F

    .line 275
    const-string v0, "moviemaker-jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 276
    return-void
.end method

.method constructor <init>(IIIZ)V
    .locals 7

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v2, 0x1

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->h:I

    .line 61
    if-eq p1, v2, :cond_0

    if-ne p1, v4, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "Only 8-bit and 16-bit PCM samples are supported"

    invoke-static {v0, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 63
    iput p1, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->c:I

    .line 65
    if-eq p2, v2, :cond_1

    if-ne p2, v4, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    const-string v0, "Only mono and stereo are supported"

    invoke-static {v1, v0}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 67
    iput p2, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->d:I

    .line 69
    const-string v0, "targetSampleRateHz"

    invoke-static {p3, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->e:I

    .line 70
    iput-boolean p4, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->f:Z

    .line 72
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->getNativeResamplerContextSize()I

    move-result v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->b:Ljava/nio/ByteBuffer;

    .line 73
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->b:Ljava/nio/ByteBuffer;

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->createNativeResampler(Ljava/nio/ByteBuffer;IIIZ)V

    .line 79
    invoke-virtual {p0, v6, v6}, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->a(FF)V

    .line 87
    return-void

    :cond_3
    move v0, v1

    .line 61
    goto :goto_0
.end method

.method public constructor <init>(IIZ)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;-><init>(IIIZ)V

    .line 46
    return-void
.end method

.method private native createNativeResampler(Ljava/nio/ByteBuffer;IIIZ)V
.end method

.method private d(I)I
    .locals 2

    .prologue
    .line 230
    iget v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->c:I

    iget v1, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->d:I

    mul-int/2addr v0, v1

    div-int v0, p1, v0

    return v0
.end method

.method private e(I)I
    .locals 2

    .prologue
    .line 234
    iget v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->e:I

    mul-int/2addr v0, p1

    iget v1, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->h:I

    div-int/2addr v0, v1

    return v0
.end method

.method private f(I)I
    .locals 2

    .prologue
    .line 238
    iget v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->c:I

    mul-int v1, p1, v0

    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    mul-int/2addr v0, v1

    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->d:I

    goto :goto_0
.end method

.method private native getNativeResamplerContextSize()I
.end method

.method private native releaseNativeResampler(Ljava/nio/ByteBuffer;)V
.end method

.method private native resampleFromByteBuffer(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;II[BIZ)V
.end method

.method private native setSourceSampleRate(Ljava/nio/ByteBuffer;I)V
.end method

.method private native setVolume(Ljava/nio/ByteBuffer;FF)V
.end method


# virtual methods
.method public a([BLjava/nio/ByteBuffer;IIZ)I
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 163
    if-nez p4, :cond_0

    .line 184
    :goto_0
    return v2

    .line 168
    :cond_0
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    .line 167
    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->g:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    const-string v4, "resampler must not be released"

    invoke-static {v0, v4}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    iget v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->h:I

    const-string v4, "mSourceSampleRateHz"

    const/4 v5, -0x1

    const-string v6, "source sample rate must be set before calling resample()"

    invoke-static {v0, v4, v5, v6}, Lcgp;->b(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)I

    const-string v0, "sourceSizeBytes"

    const/4 v4, 0x0

    invoke-static {p4, v0, v4}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    add-int v0, p3, p4

    if-gt v0, v3, :cond_2

    move v0, v1

    :goto_2
    const-string v3, "sourceOffsetBytes + sourceSizeBytes must be less or equal to source capacity"

    invoke-static {v0, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 170
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v0

    const-string v3, "sourceSamples must be a direct ByteBuffer"

    invoke-static {v0, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 172
    invoke-direct {p0, p4}, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->d(I)I

    move-result v0

    .line 174
    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->e(I)I

    move-result v6

    .line 176
    invoke-direct {p0, v6}, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->f(I)I

    move-result v8

    .line 178
    array-length v3, p1

    if-lt v3, v8, :cond_3

    :goto_3
    const-string v2, "the size of result must be at least getResampledByteCount()"

    invoke-static {v1, v2}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 181
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->b:Ljava/nio/ByteBuffer;

    iget v2, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->d:I

    mul-int v4, v0, v2

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v5, p1

    move v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->resampleFromByteBuffer(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;II[BIZ)V

    move v2, v8

    .line 184
    goto :goto_0

    :cond_1
    move v0, v2

    .line 167
    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v1, v2

    .line 178
    goto :goto_3
.end method

.method public a()V
    .locals 1

    .prologue
    .line 223
    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->g:Z

    if-nez v0, :cond_0

    .line 224
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->b:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->releaseNativeResampler(Ljava/nio/ByteBuffer;)V

    .line 225
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->g:Z

    .line 227
    :cond_0
    return-void
.end method

.method public a(FF)V
    .locals 2

    .prologue
    .line 98
    iget v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->f:Z

    if-eqz v0, :cond_0

    .line 99
    sget v0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->a:F

    mul-float/2addr p1, v0

    .line 100
    sget v0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->a:F

    mul-float/2addr p2, v0

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->b:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->setVolume(Ljava/nio/ByteBuffer;FF)V

    .line 103
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 106
    const-string v0, "sourceSampleRateHz"

    invoke-static {p1, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->h:I

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->b:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->setSourceSampleRate(Ljava/nio/ByteBuffer;I)V

    .line 111
    return-void
.end method

.method public b(I)I
    .locals 4

    .prologue
    .line 192
    if-nez p1, :cond_0

    .line 193
    const/4 v0, 0x0

    .line 198
    :goto_0
    return v0

    .line 196
    :cond_0
    iget v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->h:I

    const-string v1, "mSourceSampleRateHz"

    const/4 v2, -0x1

    const-string v3, "source sample rate must be set before calling getResampledByteCount()"

    invoke-static {v0, v1, v2, v3}, Lcgp;->b(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)I

    .line 200
    invoke-direct {p0, p1}, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->d(I)I

    move-result v0

    .line 199
    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->e(I)I

    move-result v0

    .line 198
    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->f(I)I

    move-result v0

    goto :goto_0
.end method

.method public c(I)I
    .locals 4

    .prologue
    .line 208
    if-nez p1, :cond_0

    .line 209
    const/4 v0, 0x0

    .line 218
    :goto_0
    return v0

    .line 212
    :cond_0
    iget v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->h:I

    const-string v1, "mSourceSampleRateHz"

    const/4 v2, -0x1

    const-string v3, "source sample rate must be set before calling getSourceByteCount()"

    invoke-static {v0, v1, v2, v3}, Lcgp;->b(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)I

    .line 214
    iget v1, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->c:I

    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->f:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    :goto_1
    mul-int/2addr v0, v1

    div-int v0, p1, v0

    .line 216
    int-to-double v0, v0

    iget v2, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->h:I

    int-to-double v2, v2

    mul-double/2addr v0, v2

    iget v2, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->e:I

    int-to-double v2, v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 218
    iget v1, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->c:I

    mul-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->d:I

    mul-int/2addr v0, v1

    goto :goto_0

    .line 214
    :cond_1
    iget v0, p0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->d:I

    goto :goto_1
.end method
