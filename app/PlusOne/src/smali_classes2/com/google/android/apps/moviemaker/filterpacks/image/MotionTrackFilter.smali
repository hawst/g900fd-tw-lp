.class public Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;
.super Laak;
.source "PG"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mExactMatchPercentage:F

.field private final mMotionEstimation:[I

.field private mPreviousFrame:Laar;

.field private timer:Lcgw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->TAG:Ljava/lang/String;

    .line 25
    const-string v0, "moviemaker-jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 28
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->mMotionEstimation:[I

    .line 31
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->mExactMatchPercentage:F

    .line 35
    return-void
.end method

.method private a(Ljava/lang/String;F)V
    .locals 3

    .prologue
    .line 105
    invoke-virtual {p0, p1}, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 106
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lacv;->a([I)Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->a()Labg;

    move-result-object v1

    .line 107
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Labg;->a(Ljava/lang/Object;)V

    .line 108
    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    .line 109
    return-void
.end method

.method private native trackGrossMotion(ILjava/nio/ByteBuffer;Ljava/nio/ByteBuffer;F[I)V
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 2

    .prologue
    .line 52
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "exactMatchPercentage"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    const-string v0, "mExactMatchPercentage"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 54
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->a(Z)V

    .line 55
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lacp;->b(Z)V

    .line 57
    :cond_0
    return-void
.end method

.method public c()Lacx;
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 39
    const/16 v0, 0x64

    invoke-static {v0}, Labf;->b(I)Labf;

    move-result-object v0

    .line 40
    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v1}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v1

    .line 42
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    .line 43
    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "exactMatchPercentage"

    const/4 v3, 0x1

    .line 44
    invoke-virtual {v0, v2, v3, v1}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "horizontalPanDirection"

    .line 45
    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "verticalPanDirection"

    .line 46
    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 61
    new-instance v0, Lcgw;

    sget-object v1, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->TAG:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcgw;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->timer:Lcgw;

    .line 62
    return-void
.end method

.method protected i()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 71
    const-string v0, "image"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->d()Laar;

    move-result-object v6

    .line 73
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->mPreviousFrame:Laar;

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->timer:Lcgw;

    const-string v1, "onProcess"

    invoke-virtual {v0, v1}, Lcgw;->a(Ljava/lang/String;)V

    .line 75
    invoke-virtual {v6, v7}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->timer:Lcgw;

    const-string v1, "lock current image"

    invoke-virtual {v0, v1}, Lcgw;->b(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->mPreviousFrame:Laar;

    invoke-virtual {v0, v7}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->timer:Lcgw;

    const-string v1, "lock previous image"

    invoke-virtual {v0, v1}, Lcgw;->b(Ljava/lang/String;)V

    .line 81
    invoke-virtual {v6}, Laar;->j()I

    move-result v1

    iget v4, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->mExactMatchPercentage:F

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->mMotionEstimation:[I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->trackGrossMotion(ILjava/nio/ByteBuffer;Ljava/nio/ByteBuffer;F[I)V

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->timer:Lcgw;

    const-string v1, "track motion in jni"

    invoke-virtual {v0, v1}, Lcgw;->b(Ljava/lang/String;)V

    .line 85
    invoke-virtual {v6}, Laar;->h()V

    .line 86
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->timer:Lcgw;

    const-string v1, "unlock current image"

    invoke-virtual {v0, v1}, Lcgw;->b(Ljava/lang/String;)V

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->mPreviousFrame:Laar;

    invoke-virtual {v0}, Laar;->h()V

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->timer:Lcgw;

    const-string v1, "unlock previous image"

    invoke-virtual {v0, v1}, Lcgw;->b(Ljava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->timer:Lcgw;

    invoke-virtual {v0}, Lcgw;->a()V

    .line 90
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->mPreviousFrame:Laar;

    invoke-virtual {v0}, Laar;->f()Laap;

    .line 92
    const-string v0, "horizontalPanDirection"

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->mMotionEstimation:[I

    const/4 v2, 0x0

    aget v1, v1, v2

    int-to-float v1, v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->a(Ljava/lang/String;F)V

    .line 93
    const-string v0, "verticalPanDirection"

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->mMotionEstimation:[I

    aget v1, v1, v7

    int-to-float v1, v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->a(Ljava/lang/String;F)V

    .line 96
    :cond_0
    iput-object v6, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->mPreviousFrame:Laar;

    .line 97
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->mPreviousFrame:Laar;

    invoke-virtual {v0}, Laar;->g()Laap;

    .line 98
    return-void
.end method

.method protected k()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionTrackFilter;->timer:Lcgw;

    invoke-virtual {v0}, Lcgw;->b()V

    .line 67
    return-void
.end method
