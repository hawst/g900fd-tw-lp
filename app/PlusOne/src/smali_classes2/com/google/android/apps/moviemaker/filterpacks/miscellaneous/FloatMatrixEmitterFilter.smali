.class public Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FloatMatrixEmitterFilter;
.super Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter",
        "<",
        "Lbmn;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter;-><init>(Lacs;Ljava/lang/String;)V

    .line 23
    return-void
.end method


# virtual methods
.method public c()Lacx;
    .locals 4

    .prologue
    .line 27
    const/16 v0, 0xc8

    invoke-static {v0}, Labf;->b(I)Labf;

    move-result-object v0

    .line 28
    new-instance v1, Lacx;

    invoke-direct {v1}, Lacx;-><init>()V

    const-string v2, "input"

    const/4 v3, 0x2

    .line 29
    invoke-virtual {v1, v2, v3, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 30
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 6

    .prologue
    .line 35
    const-string v0, "input"

    .line 36
    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FloatMatrixEmitterFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->d()Laar;

    move-result-object v0

    .line 37
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FloatMatrixEmitterFilter;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 38
    invoke-virtual {v0}, Laar;->j()I

    move-result v1

    .line 39
    invoke-virtual {v0}, Laar;->k()I

    move-result v2

    .line 40
    new-instance v3, Lbmn;

    invoke-direct {v3, v2, v1}, Lbmn;-><init>(II)V

    .line 42
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 43
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 44
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v1

    .line 45
    invoke-virtual {v1}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    .line 46
    invoke-virtual {v3}, Lbmn;->e()[F

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/FloatBuffer;->get([F)Ljava/nio/FloatBuffer;

    .line 47
    invoke-virtual {v0}, Laar;->h()V

    .line 49
    iget-object v0, v0, Laap;->a:Lzp;

    invoke-virtual {v0}, Lzp;->g()J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    invoke-virtual {p0, v0, v1, v3}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FloatMatrixEmitterFilter;->a(JLjava/lang/Object;)V

    .line 51
    :cond_0
    return-void
.end method
