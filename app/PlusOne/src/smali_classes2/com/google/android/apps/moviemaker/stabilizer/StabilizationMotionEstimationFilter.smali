.class public Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;
.super Laak;
.source "PG"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContextBuffer:Ljava/nio/ByteBuffer;

.field private mIsStabilizationMotionEstimationSetUp:Z

.field private mOriginalInputDimensions:[I

.field private mWorkingFrameHeight:I

.field private mWorkingFrameWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method private native getContextLength()I
.end method

.method private native stabilizationMotionEstimationProcessFrame(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;II[F)V
.end method

.method private native stabilizationMotionEstimationSetUp(Ljava/nio/ByteBuffer;II)V
.end method

.method private native stabilizationMotionEstimationTearDown(Ljava/nio/ByteBuffer;)V
.end method


# virtual methods
.method public c()Lacx;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 40
    const/16 v0, 0x64

    invoke-static {v0, v5}, Labf;->a(II)Labf;

    move-result-object v0

    .line 41
    const-class v1, Lbmn;

    invoke-static {v1}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v1

    .line 43
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    const/4 v4, 0x2

    .line 44
    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "interframetransform"

    .line 45
    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->getContextLength()I

    move-result v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->mContextBuffer:Ljava/nio/ByteBuffer;

    .line 53
    return-void
.end method

.method protected i()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    .line 68
    const-string v0, "image"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v6

    .line 70
    invoke-virtual {v6}, Laas;->i()[I

    move-result-object v0

    .line 77
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->mOriginalInputDimensions:[I

    if-nez v1, :cond_3

    .line 78
    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->mOriginalInputDimensions:[I

    .line 89
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->mIsStabilizationMotionEstimationSetUp:Z

    if-nez v1, :cond_1

    .line 90
    aget v1, v0, v5

    iput v1, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->mWorkingFrameWidth:I

    .line 91
    aget v0, v0, v7

    iput v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->mWorkingFrameHeight:I

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->mContextBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->mWorkingFrameWidth:I

    iget v2, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->mWorkingFrameHeight:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->stabilizationMotionEstimationSetUp(Ljava/nio/ByteBuffer;II)V

    .line 94
    iput-boolean v7, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->mIsStabilizationMotionEstimationSetUp:Z

    .line 98
    :cond_1
    invoke-virtual {v6, v7}, Laas;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 100
    invoke-static {}, Lbxi;->a()[F

    move-result-object v5

    .line 103
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->mContextBuffer:Ljava/nio/ByteBuffer;

    iget v3, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->mWorkingFrameWidth:I

    iget v4, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->mWorkingFrameHeight:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->stabilizationMotionEstimationProcessFrame(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;II[F)V

    .line 106
    invoke-virtual {v6}, Laas;->h()V

    .line 109
    const-string v0, "interframetransform"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 110
    if-eqz v0, :cond_2

    .line 111
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lacv;->a([I)Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->a()Labg;

    move-result-object v1

    .line 112
    iget-object v2, v6, Laap;->a:Lzp;

    invoke-virtual {v2}, Lzp;->g()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Labg;->a(J)V

    .line 113
    invoke-static {v5}, Lbxi;->a([F)Lbxi;

    move-result-object v2

    invoke-virtual {v1, v2}, Labg;->a(Ljava/lang/Object;)V

    .line 114
    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    .line 117
    :cond_2
    :goto_0
    return-void

    .line 79
    :cond_3
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->mOriginalInputDimensions:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 80
    sget-object v1, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->TAG:Ljava/lang/String;

    const-string v2, "Frame dimensions have changed during processing of a video, original dimensions were (%d, %d), current dimensions are (%d, %d). Ignoring current frame."

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->mOriginalInputDimensions:[I

    aget v4, v4, v5

    .line 83
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->mOriginalInputDimensions:[I

    aget v4, v4, v7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    const/4 v4, 0x2

    aget v5, v0, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    aget v0, v0, v7

    .line 84
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    .line 80
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected j()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->mOriginalInputDimensions:[I

    .line 58
    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->mIsStabilizationMotionEstimationSetUp:Z

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->mContextBuffer:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->stabilizationMotionEstimationTearDown(Ljava/nio/ByteBuffer;)V

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationMotionEstimationFilter;->mIsStabilizationMotionEstimationSetUp:Z

    .line 63
    :cond_0
    return-void
.end method
