.class public Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/KeyFrameEmitterFilter;
.super Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter",
        "<",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter;-><init>(Lacs;Ljava/lang/String;)V

    .line 16
    return-void
.end method


# virtual methods
.method public c()Lacx;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 20
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "videoInfo"

    const-class v2, Ladn;

    .line 22
    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    .line 21
    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "frameNumber"

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    .line 24
    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    .line 23
    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 25
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 6

    .prologue
    .line 30
    const-string v0, "videoInfo"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/KeyFrameEmitterFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->a()Labg;

    move-result-object v1

    .line 31
    const-string v0, "frameNumber"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/KeyFrameEmitterFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->a()Labg;

    move-result-object v2

    .line 32
    invoke-virtual {v1}, Labg;->j()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladn;

    .line 34
    iget-boolean v0, v0, Ladn;->a:Z

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, v1, Laap;->a:Lzp;

    invoke-virtual {v0}, Lzp;->g()J

    move-result-wide v0

    const-wide/16 v4, 0x3e8

    div-long v4, v0, v4

    invoke-virtual {v2}, Labg;->j()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {p0, v4, v5, v0}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/KeyFrameEmitterFilter;->a(JLjava/lang/Object;)V

    .line 37
    :cond_0
    return-void
.end method
