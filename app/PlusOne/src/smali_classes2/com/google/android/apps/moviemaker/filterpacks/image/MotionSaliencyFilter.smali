.class public Lcom/google/android/apps/moviemaker/filterpacks/image/MotionSaliencyFilter;
.super Laak;
.source "PG"


# static fields
.field private static final BLOCK_SIZE_PORT:Ljava/lang/String; = "blocksize"

.field private static final INPUT_PORT:Ljava/lang/String; = "image"

.field private static final MAP_PORT:Ljava/lang/String; = "map"

.field private static final SCORE_PORT:Ljava/lang/String; = "score"


# instance fields
.field private mBlockSize:I

.field private mPreviousFrame:Laap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    const-string v0, "moviemaker-jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 110
    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 29
    const/16 v0, 0xa

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionSaliencyFilter;->mBlockSize:I

    .line 33
    return-void
.end method

.method private native computeMotionSaliency(IIILjava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/FloatBuffer;)F
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 2

    .prologue
    .line 58
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "blocksize"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    const-string v0, "mBlockSize"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 60
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->a(Z)V

    .line 62
    :cond_0
    return-void
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 37
    const/16 v0, 0x64

    invoke-static {v0, v4}, Labf;->a(II)Labf;

    move-result-object v0

    .line 38
    const/16 v1, 0xc8

    invoke-static {v1}, Labf;->b(I)Labf;

    move-result-object v1

    .line 40
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    .line 41
    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "blocksize"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    .line 42
    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "score"

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    .line 43
    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v5, v3}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "map"

    .line 44
    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 13

    .prologue
    const/4 v6, 0x2

    const/4 v7, 0x0

    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 66
    const-string v0, "image"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionSaliencyFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    .line 67
    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v9

    .line 68
    invoke-virtual {v9}, Laap;->i()[I

    move-result-object v2

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionSaliencyFilter;->mPreviousFrame:Laap;

    if-eqz v0, :cond_1

    .line 71
    new-array v0, v6, [I

    aget v1, v2, v12

    iget v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionSaliencyFilter;->mBlockSize:I

    div-int/2addr v1, v3

    aput v1, v0, v12

    aget v1, v2, v11

    iget v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionSaliencyFilter;->mBlockSize:I

    div-int/2addr v1, v3

    aput v1, v0, v11

    .line 73
    invoke-virtual {v9}, Laap;->d()Laar;

    move-result-object v1

    invoke-virtual {v1, v11}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 74
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionSaliencyFilter;->mPreviousFrame:Laap;

    invoke-virtual {v1}, Laap;->d()Laar;

    move-result-object v1

    invoke-virtual {v1, v11}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 78
    const-string v1, "map"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionSaliencyFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v10

    .line 79
    if-eqz v10, :cond_2

    .line 80
    invoke-virtual {v10, v0}, Lacv;->a([I)Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->d()Laar;

    move-result-object v0

    .line 81
    invoke-virtual {v0, v6}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 82
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 83
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v6

    move-object v8, v0

    .line 85
    :goto_0
    aget v1, v2, v12

    aget v2, v2, v11

    iget v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionSaliencyFilter;->mBlockSize:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionSaliencyFilter;->computeMotionSaliency(IIILjava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/FloatBuffer;)F

    move-result v0

    .line 88
    if-eqz v10, :cond_0

    .line 89
    invoke-virtual {v8}, Laar;->h()V

    .line 90
    invoke-virtual {v10, v8}, Lacv;->a(Laap;)V

    .line 92
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionSaliencyFilter;->mPreviousFrame:Laap;

    invoke-virtual {v1}, Laap;->h()V

    .line 93
    invoke-virtual {v9}, Laap;->h()V

    .line 94
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionSaliencyFilter;->mPreviousFrame:Laap;

    invoke-virtual {v1}, Laap;->f()Laap;

    .line 96
    const-string v1, "score"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionSaliencyFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    .line 97
    invoke-virtual {v1, v7}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->a()Labg;

    move-result-object v2

    .line 98
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v2, v0}, Labg;->a(Ljava/lang/Object;)V

    .line 99
    invoke-virtual {v1, v2}, Lacv;->a(Laap;)V

    .line 101
    :cond_1
    invoke-virtual {v9}, Laap;->g()Laap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionSaliencyFilter;->mPreviousFrame:Laap;

    .line 102
    return-void

    :cond_2
    move-object v6, v7

    move-object v8, v7

    goto :goto_0
.end method

.method protected k()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionSaliencyFilter;->mPreviousFrame:Laap;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionSaliencyFilter;->mPreviousFrame:Laap;

    invoke-virtual {v0}, Laap;->f()Laap;

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/MotionSaliencyFilter;->mPreviousFrame:Laap;

    .line 54
    :cond_0
    return-void
.end method
