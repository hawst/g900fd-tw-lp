.class public Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;
.super Laak;
.source "PG"


# static fields
.field private static final BYTES_PER_SAMPLE:I = 0x2

.field private static final RMS_INTERVAL_SECONDS:F = 0.1f

.field private static final SECOND_IN_NS:F = 1.0E9f


# instance fields
.field private final mOutputTimestampQueue:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mPeakAmplitude:I

.field private final mPeakAmplitudeValueQueue:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final mRmsValueQueue:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private mSampleCount:I

.field private mSampleSquaredSum:F


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mRmsValueQueue:Ljava/util/List;

    .line 38
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mPeakAmplitudeValueQueue:Ljava/util/List;

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mOutputTimestampQueue:Ljava/util/List;

    .line 44
    iput v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mSampleCount:I

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mSampleSquaredSum:F

    .line 50
    iput v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mPeakAmplitude:I

    .line 56
    iput v1, p0, Laak;->mMinimumAvailableInputs:I

    .line 57
    return-void
.end method


# virtual methods
.method public c()Lacx;
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 61
    const-class v0, Ladc;

    invoke-static {v0}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v0

    .line 62
    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v1}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v1

    .line 63
    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    .line 65
    new-instance v3, Lacx;

    invoke-direct {v3}, Lacx;-><init>()V

    const-string v4, "audio"

    .line 66
    invoke-virtual {v3, v4, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v3, "rms"

    .line 67
    invoke-virtual {v0, v3, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "peakAmplitude"

    .line 68
    invoke-virtual {v0, v1, v5, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v2, 0x0

    .line 74
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v3

    .line 75
    invoke-virtual {v3}, Lacp;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 76
    invoke-virtual {v3}, Lacp;->c()Laap;

    move-result-object v4

    .line 77
    invoke-virtual {v4}, Laap;->a()Labg;

    move-result-object v0

    invoke-virtual {v0}, Labg;->j()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladc;

    .line 79
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mOutputTimestampQueue:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 81
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mOutputTimestampQueue:Ljava/util/List;

    iget-object v5, v4, Laap;->a:Lzp;

    invoke-virtual {v5}, Lzp;->g()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 84
    :cond_0
    iget v1, v0, Ladc;->a:I

    int-to-float v1, v1

    const v5, 0x3dcccccd    # 0.1f

    mul-float/2addr v1, v5

    iget v5, v0, Ladc;->b:I

    int-to-float v5, v5

    mul-float/2addr v1, v5

    float-to-int v5, v1

    move v1, v2

    .line 87
    :goto_0
    iget-object v6, v0, Ladc;->c:[B

    array-length v6, v6

    add-int/lit8 v6, v6, -0x2

    if-gt v1, v6, :cond_3

    .line 88
    iget-object v6, v0, Ladc;->c:[B

    const/4 v7, 0x2

    invoke-static {v6, v1, v7}, Lceg;->a([BII)S

    move-result v6

    .line 89
    iget v7, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mSampleSquaredSum:F

    mul-int v8, v6, v6

    int-to-float v8, v8

    add-float/2addr v7, v8

    iput v7, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mSampleSquaredSum:F

    iget v7, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mSampleCount:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mSampleCount:I

    .line 91
    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    .line 92
    iget v7, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mPeakAmplitude:I

    if-le v6, v7, :cond_1

    .line 93
    iput v6, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mPeakAmplitude:I

    .line 96
    :cond_1
    iget v6, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mSampleCount:I

    if-lt v6, v5, :cond_2

    .line 97
    iget-object v6, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mRmsValueQueue:Ljava/util/List;

    iget v7, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mSampleSquaredSum:F

    iget v8, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mSampleCount:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-float v7, v8

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 98
    iget-object v6, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mPeakAmplitudeValueQueue:Ljava/util/List;

    iget v7, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mPeakAmplitude:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    iput v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mSampleCount:I

    const/4 v6, 0x0

    iput v6, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mSampleSquaredSum:F

    iput v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mPeakAmplitude:I

    .line 100
    iget-object v6, v4, Laap;->a:Lzp;

    invoke-virtual {v6}, Lzp;->g()J

    move-result-wide v6

    const v8, 0x4e6e6b28    # 1.0E9f

    iget v9, v0, Ladc;->a:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    iget v9, v0, Ladc;->b:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    iget-object v9, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mOutputTimestampQueue:Ljava/util/List;

    add-int/lit8 v10, v1, 0x2

    int-to-float v10, v10

    mul-float/2addr v8, v10

    float-to-long v10, v8

    add-long/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v9, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 87
    :cond_2
    add-int/lit8 v1, v1, 0x2

    goto :goto_0

    .line 105
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mRmsValueQueue:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 108
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mOutputTimestampQueue:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 110
    const-string v4, "rms"

    invoke-virtual {p0, v4}, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v4

    .line 111
    invoke-virtual {v4, v12}, Lacv;->a([I)Laap;

    move-result-object v5

    invoke-virtual {v5}, Laap;->a()Labg;

    move-result-object v5

    .line 112
    iget-object v6, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mRmsValueQueue:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Labg;->a(Ljava/lang/Object;)V

    .line 113
    invoke-virtual {v5, v0, v1}, Labg;->a(J)V

    .line 114
    invoke-virtual {v4, v5}, Lacv;->a(Laap;)V

    .line 116
    const-string v4, "peakAmplitude"

    invoke-virtual {p0, v4}, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v4

    .line 118
    invoke-virtual {v4, v12}, Lacv;->a([I)Laap;

    move-result-object v5

    invoke-virtual {v5}, Laap;->a()Labg;

    move-result-object v5

    .line 119
    iget-object v6, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mPeakAmplitudeValueQueue:Ljava/util/List;

    invoke-interface {v6, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v5, v2}, Labg;->a(Ljava/lang/Object;)V

    .line 120
    invoke-virtual {v5, v0, v1}, Labg;->a(J)V

    .line 121
    invoke-virtual {v4, v5}, Lacv;->a(Laap;)V

    .line 125
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioRmsFilter;->mRmsValueQueue:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    invoke-virtual {v3, v0}, Lacp;->b(Z)V

    .line 126
    return-void
.end method
