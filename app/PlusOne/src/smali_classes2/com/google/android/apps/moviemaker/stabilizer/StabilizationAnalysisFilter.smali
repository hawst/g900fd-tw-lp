.class public Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;
.super Laak;
.source "PG"


# static fields
.field private static final BYTES_PER_FLOAT:I = 0x4

.field private static final DIMENSIONS_CHANGED_MESSAGE_TEMPLATE:Ljava/lang/String; = "Frame dimensions have changed during processing of a video, original dimensions were (%d, %d), current dimensions are (%d, %d). Ignoring current frame."

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mCameraMotions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lbxg;",
            ">;"
        }
    .end annotation
.end field

.field private mContextBuffer:Ljava/nio/ByteBuffer;

.field private final mDisplacement:[F

.field private final mFrameCompensations:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lbxh;",
            ">;"
        }
    .end annotation
.end field

.field private mFrameHeight:I

.field private mFrameWidth:I

.field private mInputDimensions:[I

.field private mIsStabilizerInited:Z

.field private mListener:Lbxj;

.field private mPreviousResults:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->TAG:Ljava/lang/String;

    .line 42
    const-string v0, "moviemaker-jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 132
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 58
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameCompensations:Ljava/util/ArrayList;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mCameraMotions:Ljava/util/ArrayList;

    .line 74
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mDisplacement:[F

    .line 133
    return-void
.end method

.method private a(J)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 232
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameCompensations:Ljava/util/ArrayList;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {p1, p2, v2, v2, v1}, Lbxh;->a(JFFF)Lbxh;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mCameraMotions:Ljava/util/ArrayList;

    new-instance v1, Lbxg;

    invoke-direct {v1, p1, p2}, Lbxg;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 234
    return-void
.end method

.method private native getContextLength()I
.end method

.method private native stabilizerAddPreviousDisplacementValues(Ljava/nio/ByteBuffer;Ljava/nio/FloatBuffer;)V
.end method

.method private native stabilizerGetCompensation(Ljava/nio/ByteBuffer;[F[F[F)V
.end method

.method private native stabilizerPrepare(Ljava/nio/ByteBuffer;II)V
.end method

.method private native stabilizerProcessFrame(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;II[F)V
.end method

.method private native stabilizerSetupSecondPass(Ljava/nio/ByteBuffer;)V
.end method

.method private native stabilizerTearDown(Ljava/nio/ByteBuffer;)V
.end method


# virtual methods
.method public a(Lbxj;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mListener:Lbxj;

    .line 142
    return-void
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 156
    const/16 v0, 0x12d

    invoke-static {v0, v5}, Labf;->a(II)Labf;

    move-result-object v0

    .line 157
    const-class v1, Lbmn;

    invoke-static {v1}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v1

    .line 159
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    const/4 v4, 0x2

    .line 160
    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "displacement"

    .line 161
    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 162
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 14

    .prologue
    const/4 v13, 0x0

    const/4 v12, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 180
    const-string v0, "image"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    .line 181
    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v8

    .line 182
    invoke-virtual {v8}, Laas;->i()[I

    move-result-object v1

    .line 188
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mInputDimensions:[I

    if-nez v0, :cond_3

    .line 189
    aget v0, v1, v7

    rem-int/lit8 v0, v0, 0x8

    if-nez v0, :cond_2

    move v0, v6

    :goto_0
    const-string v2, "input frame width must be a multiple of 8"

    invoke-static {v0, v2}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 190
    array-length v0, v1

    invoke-static {v1, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mInputDimensions:[I

    .line 198
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mIsStabilizerInited:Z

    if-nez v0, :cond_1

    .line 199
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->getContextLength()I

    move-result v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mContextBuffer:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mInputDimensions:[I

    aget v0, v0, v7

    iput v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameWidth:I

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mInputDimensions:[I

    aget v0, v0, v6

    iput v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameHeight:I

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mContextBuffer:Ljava/nio/ByteBuffer;

    iget v1, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameWidth:I

    iget v2, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameHeight:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->stabilizerPrepare(Ljava/nio/ByteBuffer;II)V

    .line 200
    iput-boolean v6, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mIsStabilizerInited:Z

    .line 203
    :cond_1
    iget-object v0, v8, Laap;->a:Lzp;

    invoke-virtual {v0}, Lzp;->g()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long v2, v0, v2

    .line 204
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mPreviousResults:Landroid/util/LongSparseArray;

    if-eqz v0, :cond_6

    .line 206
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mPreviousResults:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    shl-int/lit8 v0, v0, 0x1

    shl-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v4

    move v1, v7

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mPreviousResults:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mPreviousResults:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v10

    cmp-long v0, v10, v2

    if-gez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mPreviousResults:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v10, v11}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmn;

    invoke-virtual {v0, v7}, Lbmn;->a(I)F

    move-result v5

    iget v9, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameWidth:I

    int-to-float v9, v9

    mul-float/2addr v5, v9

    invoke-virtual {v4, v5}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v6}, Lbmn;->a(I)F

    move-result v0

    iget v5, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameHeight:I

    int-to-float v5, v5

    mul-float/2addr v0, v5

    invoke-virtual {v4, v0}, Ljava/nio/FloatBuffer;->put(F)Ljava/nio/FloatBuffer;

    invoke-direct {p0, v10, v11}, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->a(J)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move v0, v7

    .line 189
    goto/16 :goto_0

    .line 191
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mInputDimensions:[I

    invoke-static {v1, v0}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 192
    sget-object v0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->TAG:Ljava/lang/String;

    const-string v2, "Frame dimensions have changed during processing of a video, original dimensions were (%d, %d), current dimensions are (%d, %d). Ignoring current frame."

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mInputDimensions:[I

    aget v4, v4, v7

    .line 193
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mInputDimensions:[I

    aget v4, v4, v6

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    aget v4, v1, v7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v12

    const/4 v4, 0x3

    aget v1, v1, v6

    .line 194
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v4

    .line 192
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    :cond_4
    :goto_2
    return-void

    .line 206
    :cond_5
    invoke-virtual {v4}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mContextBuffer:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->stabilizerAddPreviousDisplacementValues(Ljava/nio/ByteBuffer;Ljava/nio/FloatBuffer;)V

    .line 207
    iput-object v13, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mPreviousResults:Landroid/util/LongSparseArray;

    .line 210
    :cond_6
    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->a(J)V

    .line 213
    invoke-virtual {v8, v6}, Laas;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 214
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mContextBuffer:Ljava/nio/ByteBuffer;

    iget v3, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameWidth:I

    iget v4, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameHeight:I

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mDisplacement:[F

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->stabilizerProcessFrame(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;II[F)V

    .line 216
    invoke-virtual {v8}, Laas;->h()V

    .line 218
    const-string v0, "displacement"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 219
    if-eqz v0, :cond_4

    .line 220
    invoke-virtual {v0, v13}, Lacv;->a([I)Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->a()Labg;

    move-result-object v1

    .line 221
    iget-object v2, v8, Laap;->a:Lzp;

    invoke-virtual {v2}, Lzp;->g()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Labg;->a(J)V

    .line 222
    new-instance v2, Lbmn;

    invoke-direct {v2, v6, v12}, Lbmn;-><init>(II)V

    .line 224
    iget-object v3, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mDisplacement:[F

    aget v3, v3, v7

    iget v4, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameWidth:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-virtual {v2, v7, v3}, Lbmn;->a(IF)V

    .line 225
    iget-object v3, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mDisplacement:[F

    aget v3, v3, v6

    iget v4, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameHeight:I

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-virtual {v2, v6, v3}, Lbmn;->a(IF)V

    .line 226
    invoke-virtual {v1, v2}, Labg;->a(Ljava/lang/Object;)V

    .line 227
    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    goto :goto_2
.end method

.method protected j()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    const/4 v1, 0x2

    const/4 v11, 0x1

    const/4 v3, 0x0

    .line 167
    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mIsStabilizerInited:Z

    if-nez v0, :cond_2

    new-array v0, v3, [Lbxh;

    new-array v1, v3, [Lbxg;

    :goto_0
    iget-object v2, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mListener:Lbxj;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mListener:Lbxj;

    invoke-interface {v2, v0, v1}, Lbxj;->a([Lbxh;[Lbxg;)V

    .line 168
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mIsStabilizerInited:Z

    if-eqz v0, :cond_1

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mContextBuffer:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->stabilizerTearDown(Ljava/nio/ByteBuffer;)V

    iput v3, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameHeight:I

    iput v3, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameWidth:I

    iput-object v12, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mContextBuffer:Ljava/nio/ByteBuffer;

    .line 170
    iput-boolean v3, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mIsStabilizerInited:Z

    .line 172
    :cond_1
    iput-object v12, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mInputDimensions:[I

    .line 173
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameCompensations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mCameraMotions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 175
    return-void

    .line 167
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mContextBuffer:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->stabilizerSetupSecondPass(Ljava/nio/ByteBuffer;)V

    new-array v4, v1, [F

    new-array v5, v11, [F

    new-array v6, v1, [F

    move v2, v3

    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameCompensations:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameCompensations:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxh;

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mContextBuffer:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v1, v4, v5, v6}, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->stabilizerGetCompensation(Ljava/nio/ByteBuffer;[F[F[F)V

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mCameraMotions:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbxg;

    aget v7, v6, v3

    iget v8, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameWidth:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    iput v7, v1, Lbxg;->b:F

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mCameraMotions:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbxg;

    aget v7, v6, v11

    iget v8, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameHeight:I

    int-to-float v8, v8

    div-float/2addr v7, v8

    iput v7, v1, Lbxg;->c:F

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameCompensations:Ljava/util/ArrayList;

    iget-wide v8, v0, Lbxh;->a:J

    aget v0, v4, v3

    iget v7, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameWidth:I

    int-to-float v7, v7

    div-float/2addr v0, v7

    aget v7, v4, v11

    iget v10, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameHeight:I

    int-to-float v10, v10

    div-float/2addr v7, v10

    aget v10, v5, v3

    invoke-static {v8, v9, v0, v7, v10}, Lbxh;->a(JFFF)Lbxh;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameCompensations:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mFrameCompensations:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lbxh;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbxh;

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mCameraMotions:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->mCameraMotions:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lbxg;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lbxg;

    goto/16 :goto_0
.end method
