.class public Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/TimestampEmitterFilter;
.super Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter;-><init>(Lacs;Ljava/lang/String;)V

    .line 19
    return-void
.end method


# virtual methods
.method protected b(Lacp;)V
    .locals 1

    .prologue
    .line 31
    const-string v0, "output"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/TimestampEmitterFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    invoke-virtual {p1, v0}, Lacp;->a(Lacv;)V

    .line 32
    return-void
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 23
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "input"

    .line 24
    invoke-static {}, Labf;->a()Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "output"

    .line 25
    invoke-static {}, Labf;->a()Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 6

    .prologue
    .line 36
    const-string v0, "input"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/TimestampEmitterFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    .line 37
    iget-object v1, v0, Laap;->a:Lzp;

    invoke-virtual {v1}, Lzp;->g()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    const/4 v1, 0x0

    invoke-virtual {p0, v2, v3, v1}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/TimestampEmitterFilter;->a(JLjava/lang/Object;)V

    .line 38
    const-string v1, "output"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/TimestampEmitterFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    invoke-virtual {v1, v0}, Lacv;->a(Laap;)V

    .line 39
    return-void
.end method
