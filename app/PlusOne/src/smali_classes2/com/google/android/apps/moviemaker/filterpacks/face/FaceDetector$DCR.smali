.class public Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 149
    const-string v0, "moviemaker-jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 150
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a:I

    .line 35
    invoke-static {}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->makeDCR()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a:I

    .line 36
    return-void
.end method

.method private static native makeDCR()I
.end method

.method private static native nativeApproved(I)Z
.end method

.method private static native nativeGetBoundingBox(I[F)Z
.end method

.method private static native nativeGetConfidence(I)F
.end method

.method private static native nativeGetFrom(II)Z
.end method

.method private static native nativeGetNodePosition(II[F)Z
.end method

.method private static native nativePutGrayImage(ILjava/nio/ByteBuffer;II)Z
.end method

.method private static native nativePutRgbImage(ILjava/nio/ByteBuffer;II)Z
.end method

.method private static native nativeRelease(I)Z
.end method

.method private static native nativeSendTo(II)Z
.end method

.method private static native nativeSetFaceCoordinates(IFFFFFF)Z
.end method


# virtual methods
.method public a(I)Landroid/graphics/PointF;
    .locals 4

    .prologue
    .line 87
    const/4 v0, 0x2

    new-array v1, v0, [F

    .line 88
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a:I

    invoke-static {v0, p1, v1}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->nativeGetNodePosition(II[F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    new-instance v0, Landroid/graphics/PointF;

    const/4 v2, 0x0

    aget v2, v1, v2

    const/4 v3, 0x1

    aget v1, v1, v3

    invoke-direct {v0, v2, v1}, Landroid/graphics/PointF;-><init>(FF)V

    .line 91
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;
    .locals 2

    .prologue
    .line 52
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a:I

    invoke-static {p1}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->a(Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->nativeSendTo(II)Z

    .line 53
    return-object p0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a:I

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->nativeRelease(I)Z

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a:I

    .line 41
    return-void
.end method

.method public a(Landroid/graphics/PointF;Landroid/graphics/PointF;Landroid/graphics/PointF;)V
    .locals 7

    .prologue
    .line 107
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a:I

    iget v1, p1, Landroid/graphics/PointF;->x:F

    iget v2, p1, Landroid/graphics/PointF;->y:F

    iget v3, p2, Landroid/graphics/PointF;->x:F

    iget v4, p2, Landroid/graphics/PointF;->y:F

    iget v5, p3, Landroid/graphics/PointF;->x:F

    iget v6, p3, Landroid/graphics/PointF;->y:F

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->nativeSetFaceCoordinates(IFFFFFF)Z

    .line 111
    return-void
.end method

.method public a(Ljava/nio/ByteBuffer;II)V
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a:I

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->nativePutGrayImage(ILjava/nio/ByteBuffer;II)Z

    .line 45
    return-void
.end method

.method public b(Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;
    .locals 2

    .prologue
    .line 57
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a:I

    invoke-static {p1}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->a(Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->nativeGetFrom(II)Z

    .line 58
    return-object p0
.end method

.method public b(Ljava/nio/ByteBuffer;II)V
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a:I

    invoke-static {v0, p1, p2, p3}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->nativePutRgbImage(ILjava/nio/ByteBuffer;II)Z

    .line 49
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a:I

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->nativeApproved(I)Z

    move-result v0

    return v0
.end method

.method public c()F
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a:I

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->nativeGetConfidence(I)F

    move-result v0

    return v0
.end method

.method public c(Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;
    .locals 1

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a(Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;

    .line 63
    invoke-virtual {p0, p1}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->b(Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;

    move-result-object v0

    return-object v0
.end method

.method public d()Landroid/graphics/RectF;
    .locals 6

    .prologue
    .line 95
    const/4 v0, 0x4

    new-array v1, v0, [F

    .line 96
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->a:I

    invoke-static {v0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$DCR;->nativeGetBoundingBox(I[F)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    new-instance v0, Landroid/graphics/RectF;

    const/4 v2, 0x0

    aget v2, v1, v2

    const/4 v3, 0x1

    aget v3, v1, v3

    const/4 v4, 0x2

    aget v4, v1, v4

    const/4 v5, 0x3

    aget v1, v1, v5

    invoke-direct {v0, v2, v3, v4, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 99
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
