.class public Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;
.super Lcom/google/android/libraries/social/media/ui/MediaView;
.source "PG"

# interfaces
.implements Lfwx;


# static fields
.field private static a:Z

.field private static b:Landroid/graphics/Paint;

.field private static c:Landroid/graphics/Paint;

.field private static d:Landroid/graphics/Bitmap;

.field private static e:Landroid/graphics/drawable/NinePatchDrawable;

.field private static f:Landroid/graphics/Rect;

.field private static g:Landroid/graphics/Bitmap;

.field private static h:I

.field private static i:Landroid/graphics/Bitmap;

.field private static j:I


# instance fields
.field private final k:Landroid/graphics/Rect;

.field private final l:Landroid/graphics/Rect;

.field private m:Z

.field private n:Z

.field private o:Ljava/lang/Integer;

.field private p:Lfww;

.field private q:Lfsu;

.field private r:Ljac;

.field private s:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 56
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->k:Landroid/graphics/Rect;

    .line 41
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->l:Landroid/graphics/Rect;

    .line 57
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget-boolean v0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->a:Z

    if-nez v0, :cond_0

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->b:Landroid/graphics/Paint;

    const v2, 0x7f0b00ce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->b:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->c:Landroid/graphics/Paint;

    const v2, 0x7f0b02e4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->c:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    const v0, 0x7f0204d6

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->d:Landroid/graphics/Bitmap;

    const v0, 0x7f020475

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/NinePatchDrawable;

    sput-object v0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->e:Landroid/graphics/drawable/NinePatchDrawable;

    const v0, 0x7f0d02ac

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    const v2, 0x7f020514

    invoke-static {v1, v2}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v2

    sput-object v2, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->g:Landroid/graphics/Bitmap;

    const v2, 0x7f0d02ab

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sput v2, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->h:I

    new-instance v2, Landroid/graphics/Rect;

    sget v3, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->h:I

    add-int/2addr v3, v0

    sget-object v4, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    sget v4, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->h:I

    add-int/2addr v0, v4

    sget-object v4, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    add-int/2addr v0, v4

    invoke-direct {v2, v5, v5, v3, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    sput-object v2, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->f:Landroid/graphics/Rect;

    const v0, 0x7f020470

    invoke-static {v1, v0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->i:Landroid/graphics/Bitmap;

    const v0, 0x7f0d02ad

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->j:I

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->a:Z

    .line 58
    :cond_0
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b02ea

    .line 59
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 58
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->b(Landroid/graphics/drawable/Drawable;)V

    .line 60
    return-void
.end method

.method public static a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 236
    :try_start_0
    invoke-static {p0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 239
    :goto_0
    return-object v0

    .line 237
    :catch_0
    move-exception v0

    .line 238
    const-string v1, "StoryElementPickerView"

    const-string v2, "decodeResource(Resources, int) threw an OOME"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 239
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 182
    invoke-super {p0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a()V

    .line 183
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->p:Lfww;

    .line 184
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 70
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->o:Ljava/lang/Integer;

    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->invalidate()V

    .line 72
    return-void
.end method

.method public a(Lfsu;)V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->p:Lfww;

    if-nez v0, :cond_0

    .line 81
    new-instance v0, Lfww;

    sget-object v1, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->f:Landroid/graphics/Rect;

    invoke-direct {v0, v1, p0}, Lfww;-><init>(Landroid/graphics/Rect;Lfwx;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->p:Lfww;

    .line 83
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->q:Lfsu;

    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->invalidate()V

    .line 85
    return-void
.end method

.method public a(Lfww;)V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->p:Lfww;

    if-ne p1, v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->q:Lfsu;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->q:Lfsu;

    invoke-interface {v0, p0}, Lfsu;->a(Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;)V

    .line 169
    :cond_0
    return-void
.end method

.method public a(Ljac;)V
    .locals 0

    .prologue
    .line 88
    iput-object p1, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->r:Ljac;

    .line 89
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->invalidate()V

    .line 90
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 64
    iput-boolean p1, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->n:Z

    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->invalidate()V

    .line 66
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 93
    iput-boolean p1, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->s:Z

    .line 94
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->invalidate()V

    .line 95
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->m:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->p:Lfww;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->p:Lfww;

    .line 174
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    .line 173
    invoke-virtual {v0, v1, v2, v3}, Lfww;->a(III)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    const/4 v0, 0x1

    .line 177
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->m:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->m:Z

    .line 105
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->invalidate()V

    .line 106
    return-void

    .line 104
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->r:Ljac;

    sget-object v1, Ljac;->b:Ljac;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->r:Ljac;

    sget-object v1, Ljac;->c:Ljac;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->r:Ljac;

    sget-object v1, Ljac;->d:Ljac;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 132
    invoke-super {p0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->onDraw(Landroid/graphics/Canvas;)V

    .line 133
    iget-boolean v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->m:Z

    if-eqz v0, :cond_4

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->k:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 135
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->o:Ljava/lang/Integer;

    if-nez v0, :cond_3

    .line 136
    sget-object v0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->d:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->getWidth()I

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    .line 137
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->getHeight()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    .line 136
    invoke-virtual {p1, v0, v1, v2, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 146
    :goto_0
    sget-object v0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->e:Landroid/graphics/drawable/NinePatchDrawable;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->l:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 147
    sget-object v0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->e:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 151
    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->p:Lfww;

    if-eqz v0, :cond_1

    .line 152
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->getWidth()I

    move-result v0

    sget-object v1, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    sget v1, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->h:I

    sub-int/2addr v0, v1

    .line 153
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->getHeight()I

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    sget v2, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->h:I

    sub-int/2addr v1, v2

    .line 154
    sget-object v2, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->g:Landroid/graphics/Bitmap;

    int-to-float v0, v0

    int-to-float v1, v1

    invoke-virtual {p1, v2, v0, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 156
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->s:Z

    if-eqz v0, :cond_2

    .line 157
    sget-object v0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->i:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->getWidth()I

    move-result v1

    sget-object v2, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    sget v2, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->j:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    sget v2, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->j:I

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 160
    :cond_2
    return-void

    .line 139
    :cond_3
    sget-object v0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->d:Landroid/graphics/Bitmap;

    .line 140
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->o:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    sget-object v2, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    int-to-float v1, v1

    .line 141
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->getHeight()I

    move-result v2

    sget-object v3, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    .line 139
    invoke-virtual {p1, v0, v1, v2, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 148
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->n:Z

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->k:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 4

    .prologue
    .line 121
    invoke-super/range {p0 .. p5}, Lcom/google/android/libraries/social/media/ui/MediaView;->onLayout(ZIIII)V

    .line 123
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->p:Lfww;

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->p:Lfww;

    invoke-virtual {v0}, Lfww;->a()Landroid/graphics/Rect;

    move-result-object v0

    .line 125
    sub-int v1, p4, p2

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v2

    sub-int/2addr v1, v2

    sub-int v2, p5, p3

    .line 126
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v3

    sub-int/2addr v2, v3

    .line 125
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 128
    :cond_0
    return-void
.end method

.method public onMeasure(II)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 110
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;->onMeasure(II)V

    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->getMeasuredWidth()I

    move-result v0

    .line 112
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->getMeasuredHeight()I

    move-result v1

    .line 113
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->k:Landroid/graphics/Rect;

    invoke-virtual {v2, v4, v4, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 114
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->l:Landroid/graphics/Rect;

    sget-object v3, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->e:Landroid/graphics/drawable/NinePatchDrawable;

    .line 115
    invoke-virtual {v3}, Landroid/graphics/drawable/NinePatchDrawable;->getIntrinsicHeight()I

    move-result v3

    sub-int v3, v1, v3

    .line 114
    invoke-virtual {v2, v4, v3, v0, v1}, Landroid/graphics/Rect;->set(IIII)V

    .line 117
    return-void
.end method

.method public setSelected(Z)V
    .locals 0

    .prologue
    .line 75
    iput-boolean p1, p0, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->m:Z

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryElementPickerView;->invalidate()V

    .line 77
    return-void
.end method
