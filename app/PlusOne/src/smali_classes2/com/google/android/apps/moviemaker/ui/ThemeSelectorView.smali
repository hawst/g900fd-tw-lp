.class public Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;
.super Landroid/support/v4/view/ViewPager;
.source "PG"


# instance fields
.field public a:I

.field private b:Landroid/view/ViewPropertyAnimator;

.field private c:Landroid/animation/Animator;

.field private d:I

.field private e:I

.field private f:I

.field private g:Lccs;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 34
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->d:I

    .line 67
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 68
    invoke-static {v0}, Lji;->a(Landroid/view/ViewConfiguration;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->a:I

    .line 70
    new-instance v0, Lccn;

    invoke-direct {v0, p0}, Lccn;-><init>(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->a(Lkc;)V

    .line 98
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;I)I
    .locals 0

    .prologue
    .line 27
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->e:I

    return p1
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;)Landroid/animation/Animator;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c:Landroid/animation/Animator;

    return-object v0
.end method

.method private a(Lcom/google/android/apps/moviemaker/ui/ThemeItemView;I)V
    .locals 4

    .prologue
    const v1, 0x3f333333    # 0.7f

    .line 205
    invoke-static {p1}, Lcad;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 206
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 207
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->scaleY(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/4 v1, 0x0

    .line 208
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 210
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c000e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    .line 209
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    int-to-long v2, p2

    .line 211
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v1, Lccq;

    invoke-direct {v1, p0, p1}, Lccq;-><init>(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;Lcom/google/android/apps/moviemaker/ui/ThemeItemView;)V

    .line 212
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->b:Landroid/view/ViewPropertyAnimator;

    .line 225
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;Lcom/google/android/apps/moviemaker/ui/ThemeItemView;I)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->a(Lcom/google/android/apps/moviemaker/ui/ThemeItemView;I)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;)Landroid/view/ViewPropertyAnimator;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->b:Landroid/view/ViewPropertyAnimator;

    return-object v0
.end method

.method public static synthetic b(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;I)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->f(I)V

    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;I)I
    .locals 0

    .prologue
    .line 27
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->f:I

    return p1
.end method

.method public static synthetic c(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;)Lccs;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->g:Lccs;

    return-object v0
.end method

.method public static synthetic d(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;)I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->f:I

    return v0
.end method

.method public static synthetic d(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;I)I
    .locals 0

    .prologue
    .line 27
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->d:I

    return p1
.end method

.method public static synthetic e(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;)V
    .locals 6

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->m()Lcom/google/android/apps/moviemaker/ui/ThemeItemView;

    move-result-object v1

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->b(F)V

    sget-object v2, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->a:Landroid/util/Property;

    const/4 v3, 0x2

    new-array v3, v3, [F

    fill-array-data v3, :array_0

    invoke-static {v1, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c:Landroid/animation/Animator;

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c:Landroid/animation/Animator;

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c000f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {v2, v4, v5}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c:Landroid/animation/Animator;

    sget-object v3, Lcak;->a:Lcak;

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c:Landroid/animation/Animator;

    new-instance v3, Lcco;

    invoke-direct {v3, p0, v1, v0}, Lcco;-><init>(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;Lcom/google/android/apps/moviemaker/ui/ThemeItemView;I)V

    invoke-virtual {v2, v3}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x43b40000    # 360.0f
    .end array-data
.end method

.method public static synthetic f(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;)I
    .locals 1

    .prologue
    .line 27
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->e:I

    return v0
.end method

.method private f(I)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 160
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->e:I

    if-eq v0, v5, :cond_2

    .line 161
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c()I

    move-result v0

    .line 164
    iget v1, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->d:I

    if-ne v0, v1, :cond_1

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c:Landroid/animation/Animator;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->b:Landroid/view/ViewPropertyAnimator;

    if-eqz v0, :cond_1

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->b:Landroid/view/ViewPropertyAnimator;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 172
    :cond_1
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->m()Lcom/google/android/apps/moviemaker/ui/ThemeItemView;

    move-result-object v0

    .line 173
    if-nez v0, :cond_3

    .line 202
    :cond_2
    :goto_0
    return-void

    .line 179
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->d:I

    .line 181
    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->b()F

    move-result v1

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_4

    .line 183
    sget-object v1, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->b:Landroid/util/Property;

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    .line 185
    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->b()F

    move-result v4

    aput v4, v2, v3

    const/4 v3, 0x0

    aput v3, v2, v5

    .line 183
    invoke-static {v0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c:Landroid/animation/Animator;

    .line 186
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c:Landroid/animation/Animator;

    .line 187
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0010

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    int-to-long v2, v2

    .line 186
    invoke-virtual {v1, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 188
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c:Landroid/animation/Animator;

    new-instance v2, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 189
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c:Landroid/animation/Animator;

    int-to-long v2, p1

    invoke-virtual {v1, v2, v3}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 190
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c:Landroid/animation/Animator;

    invoke-virtual {v1}, Landroid/animation/Animator;->removeAllListeners()V

    .line 191
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c:Landroid/animation/Animator;

    new-instance v2, Lccp;

    invoke-direct {v2, p0, v0}, Lccp;-><init>(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;Lcom/google/android/apps/moviemaker/ui/ThemeItemView;)V

    invoke-virtual {v1, v2}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c:Landroid/animation/Animator;

    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    goto :goto_0

    .line 199
    :cond_4
    invoke-direct {p0, v0, p1}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->a(Lcom/google/android/apps/moviemaker/ui/ThemeItemView;I)V

    goto :goto_0
.end method

.method private m()Lcom/google/android/apps/moviemaker/ui/ThemeItemView;
    .locals 1

    .prologue
    .line 228
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;

    return-object v0
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 285
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->a(I)V

    .line 286
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->f:I

    .line 287
    return-void
.end method

.method public a(IZ)V
    .locals 0

    .prologue
    .line 291
    invoke-super {p0, p1, p2}, Landroid/support/v4/view/ViewPager;->a(IZ)V

    .line 292
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->f:I

    .line 293
    return-void
.end method

.method public a(Lccs;)V
    .locals 0

    .prologue
    .line 116
    iput-object p1, p0, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->g:Lccs;

    .line 117
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbza;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->setAlpha(F)V

    .line 107
    new-instance v0, Lccr;

    invoke-direct {v0, p0, p1}, Lccr;-><init>(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;Ljava/util/List;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->a(Lip;)V

    .line 108
    return-void
.end method

.method public l()V
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->f(I)V

    .line 157
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    .line 273
    invoke-super {p0, p1}, Landroid/support/v4/view/ViewPager;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 277
    if-eqz v0, :cond_0

    .line 278
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 280
    :cond_0
    return v0
.end method
