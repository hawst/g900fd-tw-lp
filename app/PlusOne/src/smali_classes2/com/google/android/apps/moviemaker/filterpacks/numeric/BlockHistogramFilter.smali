.class public Lcom/google/android/apps/moviemaker/filterpacks/numeric/BlockHistogramFilter;
.super Laak;
.source "PG"


# static fields
.field private static final NUM_BINS:I = 0xff


# instance fields
.field private mNumColumns:I

.field private mNumRows:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 86
    const-string v0, "moviemaker-jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 25
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/BlockHistogramFilter;->mNumRows:I

    .line 26
    const/16 v0, 0xc

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/BlockHistogramFilter;->mNumColumns:I

    .line 30
    return-void
.end method

.method private native computeBlockHistogram(Ljava/nio/ByteBuffer;IIILjava/nio/IntBuffer;)F
.end method


# virtual methods
.method protected a(II)V
    .locals 0

    .prologue
    .line 33
    iput p1, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/BlockHistogramFilter;->mNumRows:I

    .line 34
    iput p2, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/BlockHistogramFilter;->mNumColumns:I

    .line 35
    return-void
.end method

.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 52
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "rows"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 53
    const-string v0, "mNumRows"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 54
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "cols"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    const-string v0, "mNumColumns"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 39
    const/16 v0, 0x64

    invoke-static {v0}, Labf;->b(I)Labf;

    move-result-object v0

    .line 40
    const/16 v1, 0x66

    invoke-static {v1}, Labf;->b(I)Labf;

    move-result-object v1

    .line 42
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "input"

    .line 43
    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "rows"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    .line 44
    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "cols"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    .line 45
    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "histograms"

    .line 46
    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v5, 0x1

    .line 63
    const-string v0, "input"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/BlockHistogramFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->d()Laar;

    move-result-object v6

    .line 64
    const-string v0, "histograms"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/BlockHistogramFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    new-array v1, v8, [I

    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/BlockHistogramFilter;->mNumRows:I

    iget v4, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/BlockHistogramFilter;->mNumColumns:I

    mul-int/2addr v3, v4

    aput v3, v1, v2

    const/16 v2, 0xff

    aput v2, v1, v5

    .line 65
    invoke-virtual {v0, v1}, Lacv;->a([I)Laap;

    move-result-object v0

    .line 66
    invoke-virtual {v0}, Laap;->d()Laar;

    move-result-object v7

    .line 68
    invoke-virtual {v6, v5}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 69
    invoke-virtual {v7, v8}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 70
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 71
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v5

    .line 73
    invoke-virtual {v6}, Laar;->j()I

    move-result v2

    iget v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/BlockHistogramFilter;->mNumRows:I

    iget v4, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/BlockHistogramFilter;->mNumColumns:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/BlockHistogramFilter;->computeBlockHistogram(Ljava/nio/ByteBuffer;IIILjava/nio/IntBuffer;)F

    .line 76
    invoke-virtual {v6}, Laar;->h()V

    .line 77
    invoke-virtual {v7}, Laar;->h()V

    .line 79
    const-string v0, "histograms"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/BlockHistogramFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    invoke-virtual {v0, v7}, Lacv;->a(Laap;)V

    .line 80
    return-void
.end method
