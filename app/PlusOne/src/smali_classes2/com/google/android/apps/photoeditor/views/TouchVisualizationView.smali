.class public Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lcmn;


# instance fields
.field private final b:Lcml;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcml",
            "<",
            "Ljava/lang/Integer;",
            "Lcmy;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Rect;

.field private final e:[I

.field private f:Ljava/lang/Integer;

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 39
    new-instance v0, Lcml;

    invoke-direct {v0}, Lcml;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->b:Lcml;

    .line 41
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->c:Landroid/graphics/Paint;

    .line 42
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->d:Landroid/graphics/Rect;

    .line 43
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->e:[I

    .line 46
    const/16 v0, 0x64

    iput v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->g:I

    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->a(Landroid/util/AttributeSet;)V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 39
    new-instance v0, Lcml;

    invoke-direct {v0}, Lcml;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->b:Lcml;

    .line 41
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->c:Landroid/graphics/Paint;

    .line 42
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->d:Landroid/graphics/Rect;

    .line 43
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->e:[I

    .line 46
    const/16 v0, 0x64

    iput v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->g:I

    .line 55
    invoke-direct {p0, p2}, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->a(Landroid/util/AttributeSet;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    new-instance v0, Lcml;

    invoke-direct {v0}, Lcml;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->b:Lcml;

    .line 41
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->c:Landroid/graphics/Paint;

    .line 42
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->d:Landroid/graphics/Rect;

    .line 43
    const/4 v0, 0x2

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->e:[I

    .line 46
    const/16 v0, 0x64

    iput v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->g:I

    .line 60
    invoke-direct {p0, p2}, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->a(Landroid/util/AttributeSet;)V

    .line 61
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->e:[I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->getLocationInWindow([I)V

    .line 83
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 84
    return-void
.end method

.method private a(Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/high16 v0, -0x10000

    .line 64
    .line 65
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcho;->a:[I

    invoke-virtual {v1, p1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 67
    if-eqz v1, :cond_0

    .line 68
    const/4 v2, 0x0

    invoke-virtual {v1, v2, v0}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    .line 71
    const/16 v2, 0x64

    invoke-virtual {v1, v3, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->g:I

    .line 74
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 76
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 77
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->c:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 79
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Integer;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->f:Ljava/lang/Integer;

    .line 162
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 174
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDeviceId()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v0, v2, :cond_2

    .line 175
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->a()V

    move v0, v1

    .line 177
    :goto_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 178
    new-instance v2, Lcmy;

    invoke-direct {v2, p1, v0}, Lcmy;-><init>(Landroid/view/MotionEvent;I)V

    .line 179
    iget-object v3, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->d:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->e:[I

    aget v4, v4, v1

    neg-int v4, v4

    iget-object v5, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->e:[I

    const/4 v6, 0x1

    aget v5, v5, v6

    neg-int v5, v5

    .line 180
    invoke-virtual {v2, v4, v5}, Lcmy;->a(II)Landroid/graphics/Rect;

    move-result-object v4

    .line 179
    invoke-virtual {v3, v4}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 181
    iget-object v3, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->b:Lcml;

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Lcml;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 177
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->d:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->invalidate(Landroid/graphics/Rect;)V

    .line 187
    :cond_2
    return v1
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 14

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->b:Lcml;

    invoke-virtual {v0}, Lcml;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    :goto_0
    return-void

    .line 91
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->a()V

    .line 93
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v4

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->e:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    neg-int v0, v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->e:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->b:Lcml;

    .line 97
    invoke-virtual {v0}, Lcml;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .line 98
    :cond_1
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 99
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Deque;

    .line 100
    invoke-interface {v0}, Ljava/util/Deque;->getLast()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcmy;

    iget v1, v1, Lcmy;->d:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    const/4 v1, 0x1

    :goto_2
    invoke-interface {v0}, Ljava/util/Deque;->descendingIterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v1

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcmy;

    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->getDrawingTime()J

    move-result-wide v8

    iget-wide v10, v1, Lcmy;->e:J

    iget v3, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->g:I

    int-to-long v12, v3

    add-long/2addr v10, v12

    cmp-long v3, v8, v10

    if-ltz v3, :cond_3

    const/4 v3, 0x0

    :goto_4
    if-eqz v3, :cond_5

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->d:Landroid/graphics/Rect;

    iget-object v7, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->e:[I

    const/4 v8, 0x0

    aget v7, v7, v8

    neg-int v7, v7

    iget-object v8, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->e:[I

    const/4 v9, 0x1

    aget v8, v8, v9

    neg-int v8, v8

    invoke-virtual {v1, v7, v8}, Lcmy;->a(II)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    goto :goto_3

    :cond_2
    const/4 v1, 0x0

    goto :goto_2

    :cond_3
    const/16 v3, 0xff

    if-eqz v2, :cond_4

    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->getDrawingTime()J

    move-result-wide v8

    iget-wide v10, v1, Lcmy;->e:J

    sub-long/2addr v8, v10

    long-to-float v3, v8

    const/high16 v7, 0x3f800000    # 1.0f

    iget v8, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->g:I

    int-to-float v8, v8

    div-float/2addr v3, v8

    sub-float v3, v7, v3

    mul-float/2addr v3, v3

    const/high16 v7, 0x437f0000    # 255.0f

    mul-float/2addr v3, v7

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    const/4 v7, 0x0

    const/16 v8, 0xff

    invoke-static {v3, v7, v8}, Lhcg;->a(III)I

    move-result v3

    :cond_4
    iget-object v7, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->c:Landroid/graphics/Paint;

    invoke-virtual {v7, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    invoke-virtual {v1}, Lcmy;->a()Landroid/graphics/Rect;

    move-result-object v3

    sget-object v7, Landroid/graphics/Region$Op;->UNION:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v3, v7}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;Landroid/graphics/Region$Op;)Z

    iget v3, v1, Lcmy;->a:F

    iget v7, v1, Lcmy;->b:F

    iget v8, v1, Lcmy;->c:F

    iget-object v9, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v7, v8, v9}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    const/4 v3, 0x1

    goto :goto_4

    :cond_5
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    goto :goto_3

    .line 102
    :cond_6
    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 103
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_1

    .line 108
    :cond_7
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 111
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->d:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/photoeditor/views/TouchVisualizationView;->invalidate(Landroid/graphics/Rect;)V

    goto/16 :goto_0
.end method
