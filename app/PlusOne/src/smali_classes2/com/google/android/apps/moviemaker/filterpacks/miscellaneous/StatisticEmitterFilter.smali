.class public Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/StatisticEmitterFilter;
.super Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter",
        "<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter;-><init>(Lacs;Ljava/lang/String;)V

    .line 16
    return-void
.end method


# virtual methods
.method public c()Lacx;
    .locals 4

    .prologue
    .line 20
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "input"

    const/4 v2, 0x2

    .line 21
    invoke-static {}, Labf;->b()Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 22
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 6

    .prologue
    .line 27
    const-string v0, "input"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/StatisticEmitterFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->a()Labg;

    move-result-object v0

    .line 29
    iget-object v1, v0, Laap;->a:Lzp;

    invoke-virtual {v1}, Lzp;->g()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-virtual {v0}, Labg;->j()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v2, v3, v0}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/StatisticEmitterFilter;->a(JLjava/lang/Object;)V

    .line 30
    return-void
.end method
