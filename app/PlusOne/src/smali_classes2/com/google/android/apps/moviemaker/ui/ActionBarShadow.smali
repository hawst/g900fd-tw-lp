.class public Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;
.super Landroid/view/View;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 20
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;->a()V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 25
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;->a()V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;->a()V

    .line 31
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 38
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 39
    const v1, 0x7f02057f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 40
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;->setAlpha(F)V

    .line 41
    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 1

    .prologue
    .line 34
    const/16 v0, 0x190

    invoke-static {p0, p1, v0}, Lcad;->a(Landroid/view/View;ZI)V

    .line 35
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/ActionBarShadow;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    .line 49
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v1

    .line 50
    sparse-switch v1, :sswitch_data_0

    .line 58
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 63
    :goto_0
    return-void

    .line 52
    :sswitch_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 61
    :sswitch_1
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 62
    invoke-super {p0, p1, v0}, Landroid/view/View;->onMeasure(II)V

    goto :goto_0

    .line 50
    nop

    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x0 -> :sswitch_1
    .end sparse-switch
.end method
