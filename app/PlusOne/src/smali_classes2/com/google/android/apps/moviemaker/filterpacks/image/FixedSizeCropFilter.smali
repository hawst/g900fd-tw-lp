.class public Lcom/google/android/apps/moviemaker/filterpacks/image/FixedSizeCropFilter;
.super Laak;
.source "PG"


# instance fields
.field private mMidPointX:F

.field private mMidPointY:F

.field private mOutputHeight:I

.field private mOutputWidth:I


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/16 v1, 0x64

    const/high16 v0, 0x3f000000    # 0.5f

    .line 29
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 23
    iput v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/FixedSizeCropFilter;->mOutputWidth:I

    .line 24
    iput v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/FixedSizeCropFilter;->mOutputHeight:I

    .line 25
    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/FixedSizeCropFilter;->mMidPointX:F

    .line 26
    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/FixedSizeCropFilter;->mMidPointY:F

    .line 30
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 48
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "outputWidth"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    const-string v0, "mOutputWidth"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 50
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "outputHeight"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 52
    const-string v0, "mOutputHeight"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 54
    :cond_2
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "midPointX"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 55
    const-string v0, "mMidPointX"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 56
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 57
    :cond_3
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "midPointY"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    const-string v0, "mMidPointY"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 59
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 34
    invoke-static {v2, v5}, Labf;->a(II)Labf;

    move-result-object v0

    .line 35
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 36
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    .line 37
    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "outputWidth"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    .line 38
    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "outputHeight"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    .line 39
    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "midPointX"

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    .line 40
    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "midPointY"

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    .line 41
    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    .line 42
    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 14

    .prologue
    const/high16 v13, 0x3f800000    # 1.0f

    const/high16 v12, 0x3f000000    # 0.5f

    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 65
    const-string v0, "image"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/image/FixedSizeCropFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 67
    const-string v1, "image"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/image/FixedSizeCropFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v1

    .line 68
    invoke-virtual {v1}, Laas;->i()[I

    move-result-object v2

    .line 69
    const/4 v3, 0x2

    new-array v3, v3, [I

    iget v4, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/FixedSizeCropFilter;->mOutputWidth:I

    aput v4, v3, v9

    iget v4, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/FixedSizeCropFilter;->mOutputHeight:I

    aput v4, v3, v10

    .line 70
    invoke-virtual {v0, v3}, Lacv;->a([I)Laap;

    move-result-object v4

    invoke-virtual {v4}, Laap;->e()Laas;

    move-result-object v4

    .line 72
    new-instance v5, Landroid/graphics/RectF;

    aget v6, v2, v9

    int-to-float v6, v6

    iget v7, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/FixedSizeCropFilter;->mMidPointX:F

    mul-float/2addr v6, v7

    aget v7, v3, v9

    int-to-float v7, v7

    mul-float/2addr v7, v12

    sub-float/2addr v6, v7

    aget v7, v2, v9

    int-to-float v7, v7

    div-float/2addr v6, v7

    aget v7, v2, v10

    int-to-float v7, v7

    iget v8, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/FixedSizeCropFilter;->mMidPointY:F

    mul-float/2addr v7, v8

    aget v3, v3, v10

    int-to-float v3, v3

    mul-float/2addr v3, v12

    sub-float v3, v7, v3

    aget v7, v2, v10

    int-to-float v7, v7

    div-float/2addr v3, v7

    iget v7, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/FixedSizeCropFilter;->mOutputWidth:I

    int-to-float v7, v7

    aget v8, v2, v9

    int-to-float v8, v8

    div-float/2addr v7, v8

    iget v8, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/FixedSizeCropFilter;->mOutputHeight:I

    int-to-float v8, v8

    aget v2, v2, v10

    int-to-float v2, v2

    div-float v2, v8, v2

    invoke-direct {v5, v6, v3, v7, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 76
    new-instance v2, Landroid/graphics/RectF;

    invoke-direct {v2, v11, v11, v13, v13}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 77
    invoke-virtual {v1, v4, v5, v2}, Laas;->a(Laas;Landroid/graphics/RectF;Landroid/graphics/RectF;)V

    .line 79
    invoke-virtual {v0, v4}, Lacv;->a(Laap;)V

    .line 80
    return-void
.end method
