.class public Lcom/google/android/apps/moviemaker/ui/VideoPosterView;
.super Landroid/view/View;
.source "PG"


# static fields
.field private static final b:Ljava/lang/String;

.field private static final x:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Lcom/google/android/apps/moviemaker/ui/VideoPosterView;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public a:I

.field private final c:Landroid/animation/ObjectAnimator;

.field private final d:Landroid/graphics/Paint;

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private i:Lbqo;

.field private j:Lccu;

.field private k:Landroid/graphics/drawable/Drawable;

.field private l:Landroid/graphics/drawable/Drawable;

.field private m:Ljava/lang/String;

.field private n:I

.field private o:F

.field private p:F

.field private q:Landroid/graphics/Matrix;

.field private r:Landroid/graphics/Matrix;

.field private s:Lbqt;

.field private t:Lbqt;

.field private u:I

.field private v:Z

.field private w:Landroid/graphics/drawable/Drawable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 42
    const-class v0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->b:Ljava/lang/String;

    .line 654
    new-instance v0, Lcct;

    const-class v1, Ljava/lang/Float;

    const-string v2, "posterAlpha"

    invoke-direct {v0, v1, v2}, Lcct;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->x:Landroid/util/Property;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 140
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 141
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 137
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 105
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 64
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v6}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->d:Landroid/graphics/Paint;

    .line 79
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->m:Ljava/lang/String;

    .line 80
    iput v5, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->n:I

    .line 82
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->o:F

    .line 83
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->p:F

    .line 85
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->q:Landroid/graphics/Matrix;

    .line 86
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->r:Landroid/graphics/Matrix;

    .line 93
    iput v5, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->u:I

    .line 95
    iput-boolean v5, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->v:Z

    .line 106
    sget-object v0, Lahj;->f:[I

    invoke-virtual {p1, p2, v0, p3, v5}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 108
    invoke-virtual {v0, v5, v5}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->u:I

    .line 109
    iget v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->u:I

    const-string v2, "anchorEdge"

    const/4 v3, 0x4

    const-string v4, "must be valid edge"

    invoke-static {v1, v2, v5, v3, v4}, Lcec;->a(ILjava/lang/CharSequence;IILjava/lang/CharSequence;)I

    .line 110
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 112
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 113
    const v1, 0x7f0b0052

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->e:I

    .line 115
    const v1, 0x7f020516

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->k:Landroid/graphics/drawable/Drawable;

    .line 116
    const v1, 0x7f0d005c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->f:I

    .line 117
    const v1, 0x7f0d005b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->g:I

    .line 118
    const v1, 0x7f0d005a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    .line 119
    const v1, 0x7f0205c5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    .line 120
    const v1, 0x7f020539

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->l:Landroid/graphics/drawable/Drawable;

    .line 121
    const v1, 0x7f0b0062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->h:I

    .line 123
    invoke-static {}, Lcad;->a()Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->c:Landroid/animation/ObjectAnimator;

    .line 124
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->c:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1, p0}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 125
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->c:Landroid/animation/ObjectAnimator;

    sget-object v2, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->x:Landroid/util/Property;

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setProperty(Landroid/util/Property;)V

    .line 126
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->c:Landroid/animation/ObjectAnimator;

    const/4 v2, 0x2

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 127
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->c:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 129
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->d:Landroid/graphics/Paint;

    const v2, 0x7f0d0065

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 130
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v6}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 132
    const/16 v0, 0xff

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a:I

    .line 133
    return-void

    .line 126
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;F)F
    .locals 0

    .prologue
    .line 34
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->o:F

    return p1
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;)Lbqo;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->i:Lbqo;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;Lccu;)Lccu;
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->j:Lccu;

    return-object p1
.end method

.method private a(Lbqt;)V
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->s:Lbqt;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->s:Lbqt;

    invoke-virtual {v0}, Lbqt;->b()V

    .line 315
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->s:Lbqt;

    .line 316
    return-void
.end method

.method private a(Lbqt;Z)V
    .locals 2

    .prologue
    .line 461
    invoke-direct {p0, p1}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Lbqt;)V

    .line 462
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->s:Lbqt;

    if-eqz v0, :cond_1

    .line 463
    if-eqz p2, :cond_0

    .line 464
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->c:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 466
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->s:Lbqt;

    invoke-virtual {v0}, Lbqt;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->q:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)V

    .line 467
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->invalidate()V

    .line 470
    :cond_1
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;Lbqt;Z)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Lbqt;Z)V

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;)Lccu;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->j:Lccu;

    return-object v0
.end method

.method private b(Lbqt;)V
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->t:Lbqt;

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->t:Lbqt;

    invoke-virtual {v0}, Lbqt;->b()V

    .line 322
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->t:Lbqt;

    .line 323
    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;)F
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->o:F

    return v0
.end method

.method private f()I
    .locals 2

    .prologue
    .line 647
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method private g()I
    .locals 2

    .prologue
    .line 651
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 251
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->m:Ljava/lang/String;

    .line 252
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->n:I

    .line 253
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->invalidate()V

    .line 254
    return-void
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 371
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->p:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 372
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->p:F

    .line 373
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->requestLayout()V

    .line 375
    :cond_0
    return-void
.end method

.method protected a(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/high16 v7, 0x3f000000    # 0.5f

    .line 486
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 487
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 489
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->f()I

    move-result v3

    .line 490
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->g()I

    move-result v1

    .line 492
    if-lez v1, :cond_0

    if-gtz v3, :cond_1

    .line 532
    :cond_0
    :goto_0
    return-void

    .line 497
    :cond_1
    if-lez v0, :cond_2

    if-gtz v2, :cond_3

    :cond_2
    move v0, v1

    move v2, v3

    .line 504
    :cond_3
    int-to-float v4, v3

    int-to-float v5, v2

    div-float/2addr v4, v5

    int-to-float v5, v1

    int-to-float v6, v0

    div-float/2addr v5, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 506
    int-to-float v2, v2

    mul-float/2addr v2, v4

    float-to-int v2, v2

    .line 507
    int-to-float v0, v0

    mul-float/2addr v0, v4

    float-to-int v0, v0

    .line 509
    invoke-virtual {p2}, Landroid/graphics/Matrix;->reset()V

    .line 512
    invoke-virtual {p2, v4, v4}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 515
    iget v4, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->u:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_4

    .line 516
    sub-int v0, v3, v2

    int-to-float v0, v0

    mul-float/2addr v0, v7

    add-float/2addr v0, v7

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p2, v0, v8}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    .line 517
    :cond_4
    iget v4, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->u:I

    const/4 v5, 0x4

    if-ne v4, v5, :cond_5

    .line 518
    sub-int v2, v3, v2

    int-to-float v2, v2

    mul-float/2addr v2, v7

    add-float/2addr v2, v7

    float-to-int v2, v2

    int-to-float v2, v2

    sub-int v0, v1, v0

    int-to-float v0, v0

    invoke-virtual {p2, v2, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    .line 521
    :cond_5
    iget v4, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->u:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_6

    .line 522
    sub-int v0, v1, v0

    int-to-float v0, v0

    mul-float/2addr v0, v7

    add-float/2addr v0, v7

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p2, v8, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    .line 524
    :cond_6
    iget v4, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->u:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_7

    .line 525
    sub-int v2, v3, v2

    int-to-float v2, v2

    sub-int v0, v1, v0

    int-to-float v0, v0

    mul-float/2addr v0, v7

    add-float/2addr v0, v7

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p2, v2, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    .line 529
    :cond_7
    sub-int v2, v3, v2

    int-to-float v2, v2

    mul-float/2addr v2, v7

    add-float/2addr v2, v7

    float-to-int v2, v2

    int-to-float v2, v2

    sub-int v0, v1, v0

    int-to-float v0, v0

    mul-float/2addr v0, v7

    add-float/2addr v0, v7

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p2, v2, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0
.end method

.method public a(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->w:Landroid/graphics/drawable/Drawable;

    if-eq p1, v0, :cond_0

    .line 345
    iput-object p1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->w:Landroid/graphics/drawable/Drawable;

    .line 346
    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->v:Z

    if-eqz v0, :cond_0

    .line 347
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->invalidate()V

    .line 350
    :cond_0
    return-void
.end method

.method public a(Lbqo;)V
    .locals 2

    .prologue
    .line 157
    const-string v0, "posterStore"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqo;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->i:Lbqo;

    .line 158
    return-void
.end method

.method public a(Lbqt;F)V
    .locals 1

    .prologue
    .line 230
    invoke-virtual {p0, p2}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(F)V

    .line 231
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Lbqt;Z)V

    .line 232
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 242
    const-string v0, "durationString"

    invoke-static {p1, v0}, Lcec;->a(Ljava/lang/String;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->m:Ljava/lang/String;

    .line 243
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->n:I

    .line 244
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->invalidate()V

    .line 245
    return-void
.end method

.method public a(Ljava/util/concurrent/Executor;Lbmm;II)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->i:Lbqo;

    const-string v2, "mPosterStore"

    const-string v3, "call setPosterStore before setDisplayableFrame"

    invoke-static {v0, v2, v3}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 192
    const-string v0, "executor"

    invoke-static {p1, v0, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 193
    const-string v0, "displayableFrame"

    invoke-static {p2, v0, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 194
    const-string v0, "targetWidth"

    invoke-static {p3, v0, v4}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    .line 195
    const-string v0, "targetHeight"

    invoke-static {p4, v0, v4}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    .line 196
    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v2, "one of target width/height must be unspecified"

    invoke-static {v0, v2}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 199
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->d()V

    .line 200
    iget-object v0, p2, Lbmm;->a:Lbml;

    invoke-interface {v0}, Lbml;->b()Lbmu;

    move-result-object v0

    invoke-virtual {v0}, Lbmu;->b()F

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(F)V

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->i:Lbqo;

    invoke-virtual {v0, p2, p3, p4}, Lbqo;->b(Lbmm;II)Lbqt;

    move-result-object v0

    .line 203
    if-eqz v0, :cond_2

    .line 204
    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Lbqt;Z)V

    .line 219
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 196
    goto :goto_0

    .line 207
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->i:Lbqo;

    invoke-virtual {v0, p2}, Lbqo;->a(Lbmm;)Lbqt;

    move-result-object v0

    .line 208
    if-eqz v0, :cond_3

    .line 211
    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->b(Lbqt;)V

    .line 212
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->t:Lbqt;

    invoke-virtual {v0}, Lbqt;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->r:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0, v2}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)V

    .line 213
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->invalidate()V

    .line 216
    :cond_3
    new-instance v0, Lccu;

    invoke-direct {v0, p0, p2, p3, p4}, Lccu;-><init>(Lcom/google/android/apps/moviemaker/ui/VideoPosterView;Lbmm;II)V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->j:Lccu;

    .line 217
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->j:Lccu;

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, p1, v1}, Lccu;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 331
    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->v:Z

    if-eq v0, p1, :cond_0

    .line 332
    iput-boolean p1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->v:Z

    .line 333
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->invalidate()V

    .line 335
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 260
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->m:Ljava/lang/String;

    .line 261
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->n:I

    .line 262
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->invalidate()V

    .line 263
    return-void
.end method

.method public c()Lbqt;
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->s:Lbqt;

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->s:Lbqt;

    invoke-virtual {v0}, Lbqt;->a()V

    .line 278
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->s:Lbqt;

    .line 283
    :goto_0
    return-object v0

    .line 279
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->t:Lbqt;

    if-eqz v0, :cond_1

    .line 280
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->t:Lbqt;

    invoke-virtual {v0}, Lbqt;->a()V

    .line 281
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->t:Lbqt;

    goto :goto_0

    .line 283
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 297
    invoke-direct {p0, v2}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Lbqt;)V

    .line 298
    invoke-direct {p0, v2}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->b(Lbqt;)V

    .line 299
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->q:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 300
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->r:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 301
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->b()V

    .line 302
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Z)V

    .line 303
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->c:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->end()V

    .line 304
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->o:F

    .line 305
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->j:Lccu;

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->j:Lccu;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lccu;->cancel(Z)Z

    .line 307
    iput-object v2, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->j:Lccu;

    .line 309
    :cond_0
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 476
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->s:Lbqt;

    if-eqz v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->s:Lbqt;

    invoke-virtual {v0}, Lbqt;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->q:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)V

    .line 479
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->t:Lbqt;

    if-eqz v0, :cond_1

    .line 480
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->t:Lbqt;

    invoke-virtual {v0}, Lbqt;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->r:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)V

    .line 482
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->invalidate()V

    .line 483
    return-void
.end method

.method public finalize()V
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->s:Lbqt;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->t:Lbqt;

    if-eqz v0, :cond_1

    .line 146
    :cond_0
    sget-object v0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->b:Ljava/lang/String;

    const-string v1, "ViewPosterView finalized before clear(). Poster bitmaps will be leaked!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :cond_1
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 149
    return-void
.end method

.method public onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 289
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->d()V

    .line 290
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 291
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, -0x1

    const/4 v1, 0x0

    .line 537
    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->v:Z

    if-eqz v0, :cond_0

    .line 538
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d005b

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingTop()I

    move-result v3

    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->f()I

    move-result v4

    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->g()I

    move-result v5

    iget-object v6, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, v2, v3, v4, v5}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v6, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->w:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v6, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    add-int/2addr v2, v0

    add-int/2addr v3, v0

    sub-int/2addr v4, v0

    sub-int v0, v5, v0

    invoke-virtual {p1, v2, v3, v4, v0}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 541
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getScrollX()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getScrollY()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingLeft()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingTop()I

    move-result v4

    add-int/2addr v4, v2

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getWidth()I

    move-result v5

    add-int/2addr v0, v5

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingRight()I

    move-result v5

    sub-int/2addr v0, v5

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getHeight()I

    move-result v5

    add-int/2addr v2, v5

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v2, v5

    invoke-virtual {p1, v3, v4, v0, v2}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingLeft()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingTop()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v2}, Landroid/graphics/Canvas;->translate(FF)V

    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->o:F

    const/high16 v2, 0x3f800000    # 1.0f

    cmpg-float v0, v0, v2

    if-ltz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->s:Lbqt;

    if-nez v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->t:Lbqt;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->t:Lbqt;

    invoke-virtual {v0}, Lbqt;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->r:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->s:Lbqt;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->d:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->o:F

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->s:Lbqt;

    invoke-virtual {v0}, Lbqt;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->q:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 544
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->n:I

    packed-switch v0, :pswitch_data_0

    .line 554
    :goto_1
    :pswitch_0
    return-void

    .line 541
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->d:Landroid/graphics/Paint;

    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->e:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->f()I

    move-result v0

    int-to-float v3, v0

    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->g()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->d:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 548
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getWidth()I

    move-result v2

    sub-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->f:I

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingTop()I

    move-result v3

    add-int/2addr v3, v1

    iget v4, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->f:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->k:Landroid/graphics/drawable/Drawable;

    iget v5, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a:I

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v4, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->k:Landroid/graphics/drawable/Drawable;

    sub-int v1, v3, v1

    add-int/2addr v0, v2

    invoke-virtual {v4, v2, v1, v0, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->k:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_1

    .line 551
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getWidth()I

    move-result v2

    sub-int/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->f:I

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingTop()I

    move-result v3

    add-int/2addr v3, v1

    iget v4, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->f:I

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->l:Landroid/graphics/drawable/Drawable;

    iget v5, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a:I

    invoke-virtual {v4, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    iget-object v4, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->l:Landroid/graphics/drawable/Drawable;

    sub-int v1, v3, v1

    add-int/2addr v0, v2

    invoke-virtual {v4, v2, v1, v0, v3}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->l:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->d:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->m:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->m:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-virtual {v1, v3, v8, v4, v2}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingTop()I

    move-result v3

    add-int/2addr v1, v3

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->g:I

    add-int v5, v1, v3

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getWidth()I

    move-result v1

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int/2addr v1, v3

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v1, v3

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->f:I

    sub-int/2addr v1, v3

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->g:I

    sub-int/2addr v1, v3

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v3

    sub-int/2addr v1, v3

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Rect;->offset(II)V

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->d:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->h:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->d:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->m:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    iget v0, v2, Landroid/graphics/Rect;->left:I

    int-to-float v4, v0

    int-to-float v5, v5

    iget-object v6, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->d:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v8

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 544
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 415
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 416
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->e()V

    .line 417
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    const/high16 v8, -0x80000000

    const/high16 v7, 0x40000000    # 2.0f

    .line 379
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 380
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 381
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->p:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-lez v0, :cond_0

    if-ne v2, v7, :cond_1

    if-ne v3, v7, :cond_1

    .line 383
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 411
    :goto_0
    return-void

    .line 387
    :cond_1
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 388
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 390
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingLeft()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingRight()I

    move-result v5

    add-int/2addr v4, v5

    .line 391
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingTop()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getPaddingBottom()I

    move-result v6

    add-int/2addr v5, v6

    .line 393
    if-eq v2, v7, :cond_4

    .line 394
    sub-int v1, v0, v4

    .line 395
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getMinimumHeight()I

    move-result v4

    .line 396
    int-to-float v1, v1

    iget v6, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->p:F

    div-float/2addr v1, v6

    float-to-int v1, v1

    add-int/2addr v1, v5

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 403
    :goto_1
    if-ne v2, v8, :cond_2

    .line 404
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 406
    :cond_2
    if-ne v3, v8, :cond_3

    .line 407
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 410
    :cond_3
    invoke-virtual {p0, v0, v1}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->setMeasuredDimension(II)V

    goto :goto_0

    .line 398
    :cond_4
    sub-int v0, v1, v5

    .line 399
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->getMinimumWidth()I

    move-result v5

    .line 400
    int-to-float v0, v0

    iget v6, p0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->p:F

    mul-float/2addr v0, v6

    float-to-int v0, v0

    add-int/2addr v0, v4

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    goto :goto_1
.end method
