.class public Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:I

.field private b:Ljava/io/File;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 285
    const-string v0, "moviemaker-jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 286
    return-void
.end method

.method private constructor <init>(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 251
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->a:I

    .line 252
    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->makeModule(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->a:I

    .line 253
    iput-object p1, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->b:Ljava/io/File;

    .line 254
    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;)I
    .locals 1

    .prologue
    .line 153
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->a:I

    return v0
.end method

.method public static a()Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;
    .locals 2

    .prologue
    .line 230
    new-instance v0, Ljava/io/File;

    const-string v1, "/system/vendor/media/PFFprec_600.emd"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->a(Ljava/io/File;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;
    .locals 4

    .prologue
    .line 206
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 207
    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    .line 208
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 209
    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    .line 210
    new-instance v3, Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 213
    const/4 v1, 0x2

    :try_start_0
    invoke-virtual {v0, p1, v1}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;I)Ljava/io/InputStream;

    move-result-object v0

    .line 215
    invoke-static {v0, v3}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->a(Ljava/io/InputStream;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 221
    :cond_0
    new-instance v0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    invoke-direct {v0, v3}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;-><init>(Ljava/io/File;)V

    :goto_0
    return-object v0

    .line 217
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Unable to unpack: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    const/4 v0, 0x0

    goto :goto_0

    .line 217
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static a(Ljava/io/File;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;
    .locals 4

    .prologue
    .line 165
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Module file \'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' does not exist!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 169
    :cond_0
    new-instance v0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    invoke-direct {v0, p0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;-><init>(Ljava/io/File;)V

    return-object v0
.end method

.method private static a(Ljava/io/InputStream;Ljava/io/File;)V
    .locals 4

    .prologue
    .line 258
    const/4 v2, 0x0

    .line 260
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_3

    .line 261
    const/16 v0, 0x3e8

    :try_start_1
    new-array v0, v0, [B

    .line 263
    :goto_0
    invoke-virtual {p0, v0}, Ljava/io/InputStream;->read([B)I

    move-result v2

    .line 264
    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    .line 265
    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, v2}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 277
    :catchall_0
    move-exception v0

    .line 271
    :goto_1
    if-eqz v1, :cond_0

    .line 272
    :try_start_2
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 275
    :cond_0
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    throw v0

    .line 271
    :cond_1
    :try_start_3
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 275
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    .line 276
    return-void

    .line 275
    :catchall_1
    move-exception v0

    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    throw v0

    :catchall_2
    move-exception v0

    invoke-virtual {p0}, Ljava/io/InputStream;->close()V

    throw v0

    .line 277
    :catchall_3
    move-exception v0

    move-object v1, v2

    goto :goto_1
.end method

.method public static b()Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;
    .locals 2

    .prologue
    .line 239
    new-instance v0, Ljava/io/File;

    const-string v1, "/system/vendor/media/LMspeed_508.emd"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->a(Ljava/io/File;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    move-result-object v0

    return-object v0
.end method

.method private static native makeModule(Ljava/lang/String;)I
.end method

.method private static native nativeRelease(I)Z
.end method


# virtual methods
.method public c()Ljava/io/File;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->b:Ljava/io/File;

    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 247
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->a:I

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->nativeRelease(I)Z

    .line 248
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->a:I

    .line 249
    return-void
.end method
