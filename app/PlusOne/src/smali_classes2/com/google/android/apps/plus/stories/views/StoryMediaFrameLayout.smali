.class public Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lljh;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "NewApi"
    }
.end annotation


# static fields
.field private static a:I


# instance fields
.field private b:Z

.field private c:Z

.field private d:Landroid/view/View$OnClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/4 v0, -0x1

    sput v0, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->b:Z

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->b:Z

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->b:Z

    .line 45
    return-void
.end method

.method public static a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 228
    sget v0, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->a:I

    sget v1, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->a:I

    invoke-virtual {p0, v0, v2, v1, v2}, Landroid/view/View;->setPadding(IIII)V

    .line 229
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 233
    const v0, 0x7f100366

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 234
    if-eqz v0, :cond_0

    .line 235
    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a()V

    .line 237
    :cond_0
    return-void
.end method

.method public a(Lfod;Ljava/lang/String;ZLandroid/view/View$OnClickListener;)V
    .locals 11

    .prologue
    const v2, 0x7f0205a5

    const/4 v10, 0x4

    const v3, 0x7f0205a6

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 76
    iput-boolean p3, p0, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->c:Z

    .line 77
    iput-object p4, p0, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->d:Landroid/view/View$OnClickListener;

    .line 78
    const v0, 0x7f100368

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 79
    const v0, 0x7f10036a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 80
    invoke-virtual {p1, p2}, Lfod;->d(Ljava/lang/String;)I

    move-result v5

    .line 81
    if-eqz v5, :cond_0

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    const v0, 0x7f100369

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v6, 0x7f0204f3

    invoke-virtual {v1, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 83
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 85
    const v0, 0x7f100367

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 86
    invoke-virtual {p1, p2}, Lfod;->c(Ljava/lang/String;)I

    move-result v6

    .line 87
    if-le v6, v9, :cond_1

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v7, 0xc

    invoke-direct {v1, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "+"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 88
    const v1, 0x7f100043

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v0, v1, v7}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    .line 90
    if-eqz p3, :cond_8

    .line 91
    if-lez v5, :cond_2

    .line 92
    invoke-virtual {v4, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 98
    :goto_2
    const/4 v1, 0x0

    invoke-virtual {v4, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    invoke-virtual {v4, v8}, Landroid/view/View;->setClickable(Z)V

    .line 100
    const v1, 0x7f100044

    .line 101
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 100
    invoke-static {v1}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v1

    .line 102
    if-lez v6, :cond_4

    .line 103
    if-eqz v1, :cond_3

    const v1, 0x7f0205a7

    :goto_3
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 111
    :goto_4
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 112
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setClickable(Z)V

    .line 114
    if-lez v5, :cond_6

    .line 115
    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 119
    :goto_5
    if-lez v6, :cond_7

    .line 120
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 140
    :goto_6
    invoke-static {v4}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->a(Landroid/view/View;)V

    .line 141
    invoke-static {v0}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->a(Landroid/view/View;)V

    .line 142
    iput-boolean v9, p0, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->b:Z

    .line 143
    return-void

    .line 81
    :cond_0
    const-string v1, ""

    goto/16 :goto_0

    .line 87
    :cond_1
    const-string v1, "+1"

    goto :goto_1

    .line 95
    :cond_2
    invoke-virtual {v4, v3}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_2

    :cond_3
    move v1, v2

    .line 103
    goto :goto_3

    .line 107
    :cond_4
    if-eqz v1, :cond_5

    const v1, 0x7f0205a8

    :goto_7
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_4

    :cond_5
    move v1, v3

    goto :goto_7

    .line 117
    :cond_6
    invoke-virtual {v4, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_5

    .line 122
    :cond_7
    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_6

    .line 125
    :cond_8
    invoke-virtual {v4, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 127
    invoke-virtual {v4, p4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    invoke-virtual {v4, v9}, Landroid/view/View;->setClickable(Z)V

    .line 129
    const v1, 0x7f100044

    .line 130
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    .line 129
    invoke-static {v1}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v1

    .line 131
    if-eqz v1, :cond_9

    const v3, 0x7f0205a8

    :cond_9
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 134
    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 135
    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setClickable(Z)V

    .line 137
    invoke-virtual {v4, v8}, Landroid/view/View;->setVisibility(I)V

    .line 138
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_6
.end method

.method public dispatchSystemUiVisibilityChanged(I)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    const v8, 0x7f02058f

    const/4 v7, 0x0

    const/4 v2, 0x0

    const/16 v6, 0xfa

    .line 58
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchSystemUiVisibilityChanged(I)V

    .line 59
    iget-boolean v0, p0, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->b:Z

    if-nez v0, :cond_0

    .line 72
    :goto_0
    return-void

    .line 63
    :cond_0
    and-int/lit8 v0, p1, 0x4

    if-lez v0, :cond_9

    .line 64
    const/4 v0, 0x1

    move v1, v0

    .line 66
    :goto_1
    if-eqz v1, :cond_5

    iget-boolean v0, p0, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->c:Z

    if-nez v0, :cond_5

    .line 67
    const v0, 0x7f100368

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f10036a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v3, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v3, v2}, Landroid/view/View;->setClickable(Z)V

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v3}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v4, 0xfa

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v4, Lfub;

    invoke-direct {v4, v3}, Lfub;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v4}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    :goto_2
    const v0, 0x7f100367

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v9}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v3, v2}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f100043

    invoke-virtual {v3, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v3}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v4, 0xfa

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    new-instance v2, Lfuc;

    invoke-direct {v2, v3}, Lfuc;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    .line 71
    :cond_1
    :goto_3
    iput-boolean v1, p0, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->c:Z

    goto :goto_0

    .line 67
    :cond_2
    invoke-virtual {v3, v8}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-static {v3}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->a(Landroid/view/View;)V

    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, v6}, Landroid/graphics/drawable/TransitionDrawable;->reverseTransition(I)V

    goto :goto_2

    :cond_3
    const v0, 0x7f100044

    invoke-virtual {v3, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f020590

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-static {v3}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->a(Landroid/view/View;)V

    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, v6}, Landroid/graphics/drawable/TransitionDrawable;->reverseTransition(I)V

    goto :goto_3

    :cond_4
    invoke-virtual {v3, v8}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-static {v3}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->a(Landroid/view/View;)V

    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, v6}, Landroid/graphics/drawable/TransitionDrawable;->reverseTransition(I)V

    goto :goto_3

    .line 68
    :cond_5
    if-nez v1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->c:Z

    if-eqz v0, :cond_1

    .line 69
    const v0, 0x7f100368

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f10036a

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3, v7}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {v3}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v0, v3}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v4, 0xfa

    invoke-virtual {v0, v4, v5}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    :goto_4
    const v0, 0x7f100367

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v3, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/view/View;->setClickable(Z)V

    const v0, 0x7f100043

    invoke-virtual {v3, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v3, v7}, Landroid/view/View;->setAlpha(F)V

    invoke-virtual {v3}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-virtual {v0, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    goto/16 :goto_3

    :cond_6
    invoke-virtual {v3, v8}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-static {v3}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->a(Landroid/view/View;)V

    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, v6}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    goto :goto_4

    :cond_7
    const v0, 0x7f100044

    invoke-virtual {v3, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    if-eqz v0, :cond_8

    const v0, 0x7f020590

    invoke-virtual {v3, v0}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-static {v3}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->a(Landroid/view/View;)V

    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, v6}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    goto/16 :goto_3

    :cond_8
    invoke-virtual {v3, v8}, Landroid/view/View;->setBackgroundResource(I)V

    invoke-static {v3}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->a(Landroid/view/View;)V

    invoke-virtual {v3}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, v6}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    goto/16 :goto_3

    :cond_9
    move v1, v2

    goto/16 :goto_1
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 49
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 50
    sget v0, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00f2

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->a:I

    .line 54
    :cond_0
    return-void
.end method
