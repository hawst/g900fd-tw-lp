.class public Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;
.super Landroid/view/View;
.source "PG"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:Landroid/graphics/Paint;

.field private g:I

.field private h:F

.field private i:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 51
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->g:I

    .line 39
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->h:F

    .line 40
    const/high16 v2, 0x3f800000    # 1.0f

    iput v2, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->i:F

    .line 53
    sget-object v2, Lahj;->b:[I

    invoke-virtual {p1, p2, v2, p3, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 56
    const/high16 v3, -0x1000000

    invoke-virtual {v2, v1, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->b:I

    .line 57
    const/4 v3, 0x2

    const v4, -0x777778

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->c:I

    .line 59
    const/4 v3, 0x3

    const v4, -0xffff01

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->d:I

    .line 60
    const/4 v3, 0x4

    const v4, -0xff0100

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->e:I

    .line 63
    const/4 v3, -0x1

    invoke-virtual {v2, v0, v3}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->a:I

    .line 64
    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->a:I

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->a:I

    if-ne v3, v1, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->a:I

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x3c

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "orientation must be horizontal or vertical, not: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 66
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 68
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->f:Landroid/graphics/Paint;

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->f:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->f:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 71
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->g:I

    if-eq v0, p1, :cond_0

    .line 110
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->g:I

    .line 111
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->invalidate()V

    .line 113
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 91
    const v0, 0xbeef

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->a(I)V

    .line 92
    return-void
.end method

.method public a(FF)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 80
    cmpg-float v0, p1, p2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x45

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "in point should be before out. in:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " out:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 82
    const-string v0, "in"

    invoke-static {p1, v0, v3, v4}, Lcec;->a(FLjava/lang/CharSequence;FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->h:F

    .line 83
    const-string v0, "out"

    invoke-static {p2, v0, v3, v4}, Lcec;->a(FLjava/lang/CharSequence;FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->i:F

    .line 84
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->invalidate()V

    .line 85
    return-void

    .line 80
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 98
    const v0, 0xc0de

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->a(I)V

    .line 99
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->a(I)V

    .line 106
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const v10, 0xc0de

    const v9, 0xbeef

    .line 117
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 118
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->a:I

    if-nez v0, :cond_2

    .line 119
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->f:Landroid/graphics/Paint;

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->b:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    div-int/lit8 v6, v1, 0x4

    sub-int v7, v0, v1

    div-int/lit8 v8, v1, 0x2

    int-to-float v1, v8

    int-to-float v2, v6

    add-int v0, v8, v7

    int-to-float v3, v0

    mul-int/lit8 v0, v6, 0x3

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->f:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->f:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v0, v8

    iget v1, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->h:F

    int-to-float v2, v7

    mul-float/2addr v1, v2

    add-float/2addr v1, v0

    int-to-float v2, v6

    int-to-float v0, v8

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->i:F

    int-to-float v4, v7

    mul-float/2addr v3, v4

    add-float/2addr v3, v0

    mul-int/lit8 v0, v6, 0x3

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->f:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->f:Landroid/graphics/Paint;

    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->g:I

    if-ne v0, v10, :cond_0

    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->e:I

    :goto_0
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v0, v8

    iget v1, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->h:F

    int-to-float v2, v7

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    int-to-float v1, v8

    int-to-float v2, v8

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->f:Landroid/graphics/Paint;

    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->g:I

    if-ne v0, v9, :cond_1

    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->e:I

    :goto_1
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v0, v8

    iget v1, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->i:F

    int-to-float v2, v7

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    int-to-float v1, v8

    int-to-float v2, v8

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 123
    :goto_2
    return-void

    .line 119
    :cond_0
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->d:I

    goto :goto_0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->d:I

    goto :goto_1

    .line 121
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->f:Landroid/graphics/Paint;

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->b:I

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    div-int/lit8 v6, v0, 0x2

    sub-int v7, v1, v0

    div-int/lit8 v8, v0, 0x4

    int-to-float v1, v8

    int-to-float v2, v6

    mul-int/lit8 v0, v8, 0x3

    int-to-float v3, v0

    add-int v0, v6, v7

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->f:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->f:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->c:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v1, v8

    int-to-float v0, v6

    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->h:F

    int-to-float v3, v7

    mul-float/2addr v2, v3

    add-float/2addr v2, v0

    mul-int/lit8 v0, v8, 0x3

    int-to-float v3, v0

    int-to-float v0, v6

    iget v4, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->i:F

    int-to-float v5, v7

    mul-float/2addr v4, v5

    add-float/2addr v4, v0

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->f:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->f:Landroid/graphics/Paint;

    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->g:I

    if-ne v0, v10, :cond_3

    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->e:I

    :goto_3
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v0, v6

    int-to-float v1, v6

    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->h:F

    int-to-float v3, v7

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    int-to-float v2, v6

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->f:Landroid/graphics/Paint;

    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->g:I

    if-ne v0, v9, :cond_4

    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->e:I

    :goto_4
    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v0, v6

    int-to-float v1, v6

    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->i:F

    int-to-float v3, v7

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    int-to-float v2, v6

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_2

    :cond_3
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->d:I

    goto :goto_3

    :cond_4
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->d:I

    goto :goto_4
.end method
