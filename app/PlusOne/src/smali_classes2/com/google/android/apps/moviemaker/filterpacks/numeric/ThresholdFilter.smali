.class public Lcom/google/android/apps/moviemaker/filterpacks/numeric/ThresholdFilter;
.super Laak;
.source "PG"


# instance fields
.field private mThreshold:I


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 21
    const/16 v0, 0x1e0

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/ThresholdFilter;->mThreshold:I

    .line 25
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 2

    .prologue
    .line 42
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "threshold"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    const-string v0, "mThreshold"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 44
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->a(Z)V

    .line 46
    :cond_0
    return-void
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 29
    const/16 v0, 0x66

    invoke-static {v0}, Labf;->a(I)Labf;

    move-result-object v0

    .line 30
    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v1

    .line 31
    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    .line 33
    new-instance v3, Lacx;

    invoke-direct {v3}, Lacx;-><init>()V

    const-string v4, "values"

    .line 34
    invoke-virtual {v3, v4, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v3, "threshold"

    const/4 v4, 0x1

    .line 35
    invoke-virtual {v0, v3, v4, v1}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "ratio"

    .line 36
    invoke-virtual {v0, v1, v5, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 50
    const-string v1, "values"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/ThresholdFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->c()Laaq;

    move-result-object v2

    .line 51
    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Laaq;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v3

    move v1, v0

    .line 55
    :goto_0
    invoke-virtual {v3}, Ljava/nio/IntBuffer;->hasRemaining()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 56
    invoke-virtual {v3}, Ljava/nio/IntBuffer;->get()I

    move-result v4

    iget v5, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/ThresholdFilter;->mThreshold:I

    if-le v4, v5, :cond_0

    .line 57
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 59
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 63
    :cond_1
    invoke-virtual {v2}, Laaq;->h()V

    .line 65
    const-string v2, "ratio"

    invoke-virtual {p0, v2}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/ThresholdFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v2

    .line 66
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lacv;->a([I)Laap;

    move-result-object v3

    invoke-virtual {v3}, Laap;->a()Labg;

    move-result-object v3

    .line 67
    int-to-float v4, v1

    add-int/2addr v0, v1

    int-to-float v0, v0

    div-float v0, v4, v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v3, v0}, Labg;->a(Ljava/lang/Object;)V

    .line 68
    invoke-virtual {v2, v3}, Lacv;->a(Laap;)V

    .line 69
    return-void
.end method
