.class public Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;
.super Lcom/google/android/apps/moviemaker/ui/VideoPosterView;
.source "PG"


# static fields
.field private static final f:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final b:Landroid/graphics/Matrix;

.field private final c:Landroid/graphics/Matrix;

.field private d:Landroid/graphics/RectF;

.field private e:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 93
    new-instance v0, Lccb;

    const-class v1, Ljava/lang/Float;

    const-string v2, "transition"

    invoke-direct {v0, v1, v2}, Lccb;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->f:Landroid/util/Property;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;-><init>(Landroid/content/Context;)V

    .line 20
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->b:Landroid/graphics/Matrix;

    .line 21
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->c:Landroid/graphics/Matrix;

    .line 23
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->e:F

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->b:Landroid/graphics/Matrix;

    .line 21
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->c:Landroid/graphics/Matrix;

    .line 23
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->e:F

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 20
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->b:Landroid/graphics/Matrix;

    .line 21
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->c:Landroid/graphics/Matrix;

    .line 23
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->e:F

    .line 27
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;)F
    .locals 1

    .prologue
    .line 18
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->e:F

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;F)F
    .locals 0

    .prologue
    .line 18
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->e:F

    return p1
.end method


# virtual methods
.method protected a(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)V
    .locals 8

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->d:Landroid/graphics/RectF;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->e:F

    const/high16 v1, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    .line 66
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)V

    .line 72
    :goto_0
    return-void

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->c:Landroid/graphics/Matrix;

    invoke-super {p0, p1, v0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)V

    .line 69
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->b:Landroid/graphics/Matrix;

    new-instance v1, Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->d:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->d:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->d:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v4, v5

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->d:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v5, v6

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->getPaddingLeft()I

    move-result v2

    neg-int v2, v2

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->getPaddingTop()I

    move-result v3

    neg-int v3, v3

    new-instance v4, Landroid/graphics/RectF;

    int-to-float v5, v2

    int-to-float v6, v3

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->getWidth()I

    move-result v7

    add-int/2addr v2, v7

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->getHeight()I

    move-result v7

    add-int/2addr v3, v7

    int-to-float v3, v3

    invoke-direct {v4, v5, v6, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    sget-object v2, Landroid/graphics/Matrix$ScaleToFit;->FILL:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v0, v1, v4, v2}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    .line 70
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->b:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->c:Landroid/graphics/Matrix;

    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->e:F

    invoke-static {v0, v1, v2, p2}, Lcfn;->a(Landroid/graphics/Matrix;Landroid/graphics/Matrix;FLandroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method public a(Landroid/graphics/RectF;II)V
    .locals 4

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->d:Landroid/graphics/RectF;

    .line 50
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->e()V

    .line 52
    invoke-static {}, Lcad;->a()Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 53
    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 54
    sget-object v1, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->f:Landroid/util/Property;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setProperty(Landroid/util/Property;)V

    .line 55
    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 56
    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 57
    int-to-long v2, p3

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 59
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->a:I

    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->e:F

    .line 61
    return-void

    .line 55
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method
