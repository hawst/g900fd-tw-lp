.class public Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BitmapEmitterFilter;
.super Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private mEmitIntervalMillis:I

.field private mLastEmittedTimestampNs:J


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter;-><init>(Lacs;Ljava/lang/String;)V

    .line 14
    const/16 v0, 0x1388

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BitmapEmitterFilter;->mEmitIntervalMillis:I

    .line 15
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BitmapEmitterFilter;->mLastEmittedTimestampNs:J

    .line 19
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 2

    .prologue
    .line 38
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "emitIntervalMillis"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 39
    const-string v0, "mEmitIntervalMillis"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 40
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->a(Z)V

    .line 42
    :cond_0
    return-void
.end method

.method public c()Lacx;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 28
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "input"

    const/4 v2, 0x2

    const/16 v3, 0x12d

    .line 30
    invoke-static {v3, v4}, Labf;->a(II)Labf;

    move-result-object v3

    .line 29
    invoke-virtual {v0, v1, v2, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "emitIntervalMillis"

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    .line 32
    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    .line 31
    invoke-virtual {v0, v1, v4, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 23
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BitmapEmitterFilter;->mLastEmittedTimestampNs:J

    .line 24
    return-void
.end method

.method protected i()V
    .locals 10

    .prologue
    const-wide/16 v8, 0x3e8

    .line 46
    const-string v0, "input"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BitmapEmitterFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    .line 47
    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    .line 48
    iget-object v1, v0, Laap;->a:Lzp;

    invoke-virtual {v1}, Lzp;->g()J

    move-result-wide v2

    .line 49
    iget v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BitmapEmitterFilter;->mEmitIntervalMillis:I

    int-to-long v4, v1

    mul-long/2addr v4, v8

    mul-long/2addr v4, v8

    sub-long v4, v2, v4

    iget-wide v6, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BitmapEmitterFilter;->mLastEmittedTimestampNs:J

    cmp-long v1, v4, v6

    if-ltz v1, :cond_0

    .line 50
    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v1

    .line 51
    iget-object v0, v0, Laap;->a:Lzp;

    invoke-virtual {v0}, Lzp;->g()J

    move-result-wide v4

    div-long/2addr v4, v8

    invoke-virtual {v1}, Laas;->n()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v4, v5, v0}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BitmapEmitterFilter;->a(JLjava/lang/Object;)V

    .line 52
    iput-wide v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BitmapEmitterFilter;->mLastEmittedTimestampNs:J

    .line 54
    :cond_0
    return-void
.end method
