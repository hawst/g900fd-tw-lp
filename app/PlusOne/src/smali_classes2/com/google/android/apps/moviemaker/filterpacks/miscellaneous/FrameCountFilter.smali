.class public Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FrameCountFilter;
.super Laak;
.source "PG"


# static fields
.field private static final INPUT_TYPE:Labf;

.field private static final OUTPUT_TYPE:Labf;


# instance fields
.field private mFrameNumber:I

.field private mFrameNumberOffset:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    invoke-static {}, Labf;->a()Labf;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FrameCountFilter;->INPUT_TYPE:Labf;

    .line 17
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v0}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FrameCountFilter;->OUTPUT_TYPE:Labf;

    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 19
    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FrameCountFilter;->mFrameNumber:I

    .line 20
    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FrameCountFilter;->mFrameNumberOffset:I

    .line 24
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    .prologue
    .line 39
    iput p1, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FrameCountFilter;->mFrameNumberOffset:I

    .line 40
    return-void
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 28
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "input"

    sget-object v2, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FrameCountFilter;->INPUT_TYPE:Labf;

    .line 29
    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "frameNumber"

    sget-object v2, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FrameCountFilter;->OUTPUT_TYPE:Labf;

    .line 30
    invoke-virtual {v0, v1, v3, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0}, Laak;->g()V

    .line 46
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FrameCountFilter;->mFrameNumber:I

    .line 47
    return-void
.end method

.method protected i()V
    .locals 4

    .prologue
    .line 52
    const-string v0, "input"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FrameCountFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    .line 54
    const-string v0, "frameNumber"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FrameCountFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 55
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lacv;->a([I)Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->a()Labg;

    move-result-object v1

    .line 56
    iget v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FrameCountFilter;->mFrameNumberOffset:I

    iget v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FrameCountFilter;->mFrameNumber:I

    add-int/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Labg;->a(Ljava/lang/Object;)V

    .line 57
    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    .line 59
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FrameCountFilter;->mFrameNumber:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FrameCountFilter;->mFrameNumber:I

    .line 60
    return-void
.end method
