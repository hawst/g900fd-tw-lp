.class public Lcom/google/android/apps/moviemaker/picker/PickerTileView;
.super Landroid/widget/ImageView;
.source "PG"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private final a:Landroid/graphics/Point;

.field private final b:Landroid/graphics/Rect;

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Paint;

.field private final e:Ljava/lang/Object;

.field private final f:I

.field private g:Landroid/graphics/Bitmap;

.field private final h:Landroid/graphics/ColorFilter;

.field private i:Landroid/graphics/Bitmap;

.field private j:Z

.field private k:J

.field private l:Z

.field private final m:Landroid/animation/ObjectAnimator;

.field private final n:Ljava/lang/Runnable;

.field private final o:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 116
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 120
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    .line 123
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 31
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->a:Landroid/graphics/Point;

    .line 32
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->b:Landroid/graphics/Rect;

    .line 33
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->c:Landroid/graphics/Paint;

    .line 34
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->d:Landroid/graphics/Paint;

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->e:Ljava/lang/Object;

    .line 39
    const/16 v0, 0x33

    const/16 v1, 0xb6

    const/16 v2, 0x79

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->f:I

    .line 59
    new-instance v0, Lbru;

    invoke-direct {v0, p0}, Lbru;-><init>(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->n:Ljava/lang/Runnable;

    .line 96
    new-instance v0, Lbrv;

    invoke-direct {v0, p0}, Lbrv;-><init>(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->o:Ljava/lang/Runnable;

    .line 126
    new-instance v0, Landroid/graphics/ColorMatrix;

    invoke-direct {v0}, Landroid/graphics/ColorMatrix;-><init>()V

    .line 127
    const v1, 0x3e4ccccd    # 0.2f

    invoke-virtual {v0, v1}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    .line 128
    new-instance v1, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v1, v0}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    iput-object v1, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->h:Landroid/graphics/ColorFilter;

    .line 130
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->setBackgroundColor(I)V

    .line 132
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->d:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->f:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 133
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 135
    const-string v0, "imageAlpha"

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->m:Landroid/animation/ObjectAnimator;

    .line 136
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->m:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x2ee

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 137
    return-void

    .line 135
    :array_0
    .array-data 4
        0x0
        0xff
    .end array-data
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/picker/PickerTileView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->i:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->e:Ljava/lang/Object;

    return-object v0
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/picker/PickerTileView;Z)V
    .locals 0

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->l:Z

    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)Z
    .locals 1

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->j:Z

    return v0
.end method

.method public static synthetic c(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)J
    .locals 2

    .prologue
    .line 29
    iget-wide v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->k:J

    return-wide v0
.end method

.method public static synthetic d(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->o:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static synthetic e(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->i:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public static synthetic f(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)Landroid/animation/ObjectAnimator;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->m:Landroid/animation/ObjectAnimator;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 209
    iput-object p1, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->g:Landroid/graphics/Bitmap;

    .line 210
    return-void
.end method

.method public a(Landroid/os/Handler;ZJ)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 221
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 222
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->j:Z

    if-ne v0, p2, :cond_0

    iget-wide v2, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->k:J

    cmp-long v0, v2, p3

    if-nez v0, :cond_0

    .line 223
    monitor-exit v1

    .line 241
    :goto_0
    return-void

    .line 226
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->i:Landroid/graphics/Bitmap;

    .line 227
    iput-boolean p2, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->j:Z

    .line 228
    iput-wide p3, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->k:J

    .line 229
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 230
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    invoke-virtual {p0, v4}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->setImageAlpha(I)V

    .line 235
    iput-boolean v4, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->l:Z

    .line 237
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->m:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->n:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->n:Ljava/lang/Runnable;

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 230
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 182
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->isSelected()Z

    move-result v0

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 164
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 166
    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->l:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->c:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->getImageAlpha()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->g:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->a:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->a:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 174
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 175
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->d:Landroid/graphics/Paint;

    const/16 v1, 0xcc

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->getImageAlpha()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 176
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->b:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 178
    :cond_1
    return-void
.end method

.method protected onMeasure(II)V
    .locals 0

    .prologue
    .line 142
    invoke-super {p0, p1, p1}, Landroid/widget/ImageView;->onMeasure(II)V

    .line 143
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 147
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    .line 149
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->g:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    sub-int v0, p1, v0

    div-int/lit8 v0, v0, 0x2

    .line 151
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    sub-int v1, p2, v1

    div-int/lit8 v1, v1, 0x2

    .line 152
    iget-object v2, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->a:Landroid/graphics/Point;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v3, v3, p1, p2}, Landroid/graphics/Rect;->set(IIII)V

    .line 156
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->b:Landroid/graphics/Rect;

    div-int/lit8 v1, p1, 0x14

    div-int/lit8 v2, p2, 0x14

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->inset(II)V

    .line 158
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->d:Landroid/graphics/Paint;

    div-int/lit8 v1, p1, 0xa

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 159
    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0, p1}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->setSelected(Z)V

    .line 188
    if-eqz p1, :cond_0

    .line 189
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->h:Landroid/graphics/ColorFilter;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 193
    :goto_0
    return-void

    .line 191
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->clearColorFilter()V

    goto :goto_0
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 198
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->setSelected(Z)V

    .line 202
    :goto_0
    return-void

    .line 200
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->setSelected(Z)V

    goto :goto_0
.end method
