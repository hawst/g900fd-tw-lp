.class public Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorLinearCombinationFilter;
.super Laak;
.source "PG"


# static fields
.field private static final COEFFICIENT_A_INPUT_PORT:Ljava/lang/String; = "coefficientA"

.field private static final COEFFICIENT_B_INPUT_PORT:Ljava/lang/String; = "coefficientB"

.field private static final TAG:Ljava/lang/String;

.field private static final VECTOR_A_INPUT_PORT:Ljava/lang/String; = "vectorA"

.field private static final VECTOR_B_INPUT_PORT:Ljava/lang/String; = "vectorB"

.field private static final VECTOR_OUTPUT_PORT:Ljava/lang/String; = "vector"


# instance fields
.field private mCoefficientA:I

.field private mCoefficientB:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorLinearCombinationFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorLinearCombinationFilter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 31
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 27
    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorLinearCombinationFilter;->mCoefficientA:I

    .line 28
    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorLinearCombinationFilter;->mCoefficientB:I

    .line 32
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 50
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "coefficientA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    const-string v0, "mCoefficientA"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p1, v3}, Lacp;->a(Z)V

    .line 53
    invoke-virtual {p1, v2}, Lacp;->b(Z)V

    .line 55
    :cond_0
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "coefficientB"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    const-string v0, "mCoefficientB"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p1, v3}, Lacp;->a(Z)V

    .line 58
    invoke-virtual {p1, v2}, Lacp;->b(Z)V

    .line 60
    :cond_1
    return-void
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 36
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v0}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v0

    .line 37
    const-class v1, Ljava/lang/Long;

    invoke-static {v1}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v1

    .line 39
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "vectorA"

    .line 40
    invoke-virtual {v2, v3, v4, v1}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v2

    const-string v3, "vectorB"

    .line 41
    invoke-virtual {v2, v3, v4, v1}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v2

    const-string v3, "coefficientA"

    .line 42
    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v2

    const-string v3, "coefficientB"

    .line 43
    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "vector"

    .line 44
    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 12

    .prologue
    .line 64
    const-string v0, "vectorA"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorLinearCombinationFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    .line 65
    const-string v1, "vectorB"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorLinearCombinationFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    .line 66
    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v3

    .line 67
    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    .line 68
    invoke-virtual {v3}, Laap;->a()Labg;

    move-result-object v0

    invoke-virtual {v0}, Labg;->j()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    .line 69
    invoke-virtual {v1}, Laap;->a()Labg;

    move-result-object v1

    invoke-virtual {v1}, Labg;->j()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [J

    .line 71
    array-length v2, v0

    array-length v4, v1

    if-eq v2, v4, :cond_0

    .line 72
    sget-object v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorLinearCombinationFilter;->TAG:Ljava/lang/String;

    .line 86
    :goto_0
    return-void

    .line 76
    :cond_0
    array-length v2, v0

    new-array v4, v2, [J

    .line 77
    const/4 v2, 0x0

    :goto_1
    array-length v5, v4

    if-ge v2, v5, :cond_1

    .line 78
    iget v5, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorLinearCombinationFilter;->mCoefficientA:I

    int-to-long v6, v5

    aget-wide v8, v0, v2

    mul-long/2addr v6, v8

    iget v5, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorLinearCombinationFilter;->mCoefficientB:I

    int-to-long v8, v5

    aget-wide v10, v1, v2

    mul-long/2addr v8, v10

    add-long/2addr v6, v8

    aput-wide v6, v4, v2

    .line 77
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 81
    :cond_1
    const-string v0, "vector"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorLinearCombinationFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 82
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lacv;->a([I)Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->a()Labg;

    move-result-object v1

    .line 83
    invoke-virtual {v1, v4}, Labg;->a(Ljava/lang/Object;)V

    .line 84
    iget-object v2, v3, Laap;->a:Lzp;

    invoke-virtual {v2}, Lzp;->g()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Labg;->a(J)V

    .line 85
    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    goto :goto_0
.end method
