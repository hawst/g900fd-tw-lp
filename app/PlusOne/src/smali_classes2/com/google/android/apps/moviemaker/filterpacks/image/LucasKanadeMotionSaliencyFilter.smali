.class public Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;
.super Laak;
.source "PG"


# static fields
.field private static final DISPLACEMENT:Ljava/lang/String; = "displacement"

.field private static final DROP_RATE:Ljava/lang/String; = "dropRate"

.field private static final FRAME:Ljava/lang/String; = "image"

.field private static final FRAME_NUMBER:Ljava/lang/String; = "frameNumber"

.field private static final INTERFRAMETRANSFORM:Ljava/lang/String; = "interframetransform"

.field private static final MAP:Ljava/lang/String; = "map"

.field private static final MAP_DROP_RATE:Ljava/lang/String; = "mapDropRate"

.field private static final SALIENCY_MAP_HEIGHT:I = 0x8

.field private static final SALIENCY_MAP_WIDTH:I = 0x8

.field private static final SCORE:Ljava/lang/String; = "score"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mContextBuffer:Ljava/nio/ByteBuffer;

.field private mDropRate:F

.field private mImageDims:[I

.field private mIsAnalyzerPrepared:Z

.field private mMapDropRate:F

.field private final mPreviousDisplacement:[F

.field private mPreviousFrame:Laap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->TAG:Ljava/lang/String;

    .line 233
    const-string v0, "moviemaker-jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 234
    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    const v1, 0x3f733333    # 0.95f

    .line 61
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 43
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mPreviousDisplacement:[F

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mPreviousFrame:Laap;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mIsAnalyzerPrepared:Z

    .line 53
    iput v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mDropRate:F

    .line 58
    iput v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mMapDropRate:F

    .line 62
    return-void
.end method

.method private native computeMotionSaliency(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;FFIILjava/nio/ByteBuffer;)F
.end method

.method private native getMotionSaliencyContextLength()I
.end method

.method private native prepareMotionAnalyzer(Ljava/nio/ByteBuffer;II)Z
.end method

.method private native releaseMotionAnalyzer(Ljava/nio/ByteBuffer;)V
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 87
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "dropRate"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    const-string v0, "mDropRate"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 89
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 91
    :cond_0
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mapDropRate"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    const-string v0, "mMapDropRate"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 93
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 95
    :cond_1
    return-void
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v1, 0x64

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 66
    invoke-static {v1, v4}, Labf;->a(II)Labf;

    move-result-object v0

    .line 67
    invoke-static {v1}, Labf;->b(I)Labf;

    move-result-object v1

    .line 69
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "frameNumber"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    .line 70
    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v5, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "displacement"

    .line 71
    invoke-static {}, Labf;->b()Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "interframetransform"

    .line 72
    invoke-static {}, Labf;->b()Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "dropRate"

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    .line 73
    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "mapDropRate"

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    .line 74
    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "score"

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    .line 75
    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v5, v3}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "map"

    .line 76
    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->getMotionSaliencyContextLength()I

    move-result v0

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mContextBuffer:Ljava/nio/ByteBuffer;

    .line 83
    return-void
.end method

.method protected i()V
    .locals 14

    .prologue
    const/16 v6, 0x8

    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v4, 0x0

    const/4 v7, 0x1

    .line 111
    const-string v0, "frameNumber"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    .line 112
    invoke-virtual {v0}, Laap;->a()Labg;

    move-result-object v0

    invoke-virtual {v0}, Labg;->j()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 111
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v11

    .line 117
    const-string v0, "interframetransform"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    .line 118
    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->a()Labg;

    move-result-object v0

    invoke-virtual {v0}, Labg;->j()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxi;

    .line 124
    const-string v1, "image"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    .line 125
    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v12

    .line 126
    invoke-virtual {v12}, Laap;->i()[I

    move-result-object v1

    .line 128
    iget-boolean v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mIsAnalyzerPrepared:Z

    if-nez v2, :cond_4

    .line 129
    array-length v2, v1

    invoke-static {v1, v2}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mImageDims:[I

    .line 130
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mContextBuffer:Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mImageDims:[I

    aget v2, v2, v4

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mImageDims:[I

    aget v3, v3, v7

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->prepareMotionAnalyzer(Ljava/nio/ByteBuffer;II)Z

    move-result v1

    if-nez v1, :cond_0

    .line 131
    sget-object v0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->TAG:Ljava/lang/String;

    const-string v1, "Failed to initialize motion saliency analyzer"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 210
    :goto_0
    return-void

    .line 134
    :cond_0
    iput-boolean v7, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mIsAnalyzerPrepared:Z

    .line 144
    :cond_1
    const-string v1, "map"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v13

    .line 146
    iget v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mDropRate:F

    invoke-static {v11, v1}, Lbbq;->a(IF)Z

    move-result v1

    if-nez v1, :cond_2

    .line 147
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mPreviousFrame:Laap;

    if-eqz v1, :cond_5

    .line 153
    invoke-virtual {v0}, Lbxi;->d()F

    move-result v1

    neg-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mImageDims:[I

    aget v2, v2, v4

    int-to-float v2, v2

    mul-float v4, v1, v2

    .line 154
    invoke-virtual {v0}, Lbxi;->e()F

    move-result v0

    neg-float v0, v0

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mImageDims:[I

    aget v1, v1, v7

    int-to-float v1, v1

    mul-float v5, v0, v1

    .line 162
    invoke-virtual {v12}, Laap;->d()Laar;

    move-result-object v0

    invoke-virtual {v0, v7}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 163
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mPreviousFrame:Laap;

    .line 164
    invoke-virtual {v0}, Laap;->d()Laar;

    move-result-object v0

    invoke-virtual {v0, v7}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 168
    if-eqz v13, :cond_6

    .line 169
    new-array v0, v8, [I

    fill-array-data v0, :array_0

    invoke-virtual {v13, v0}, Lacv;->a([I)Laap;

    move-result-object v0

    .line 170
    invoke-virtual {v0}, Laap;->d()Laar;

    move-result-object v0

    .line 171
    invoke-virtual {v0, v8}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v8

    move-object v10, v0

    .line 174
    :goto_1
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mContextBuffer:Ljava/nio/ByteBuffer;

    move-object v0, p0

    move v7, v6

    invoke-direct/range {v0 .. v8}, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->computeMotionSaliency(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;FFIILjava/nio/ByteBuffer;)F

    move-result v0

    .line 178
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mPreviousFrame:Laap;

    invoke-virtual {v1}, Laap;->h()V

    .line 179
    invoke-virtual {v12}, Laap;->h()V

    .line 181
    const-string v1, "score"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    .line 182
    invoke-virtual {v1, v9}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->a()Labg;

    move-result-object v2

    .line 183
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v2, v0}, Labg;->a(Ljava/lang/Object;)V

    .line 184
    invoke-virtual {v1, v2}, Lacv;->a(Laap;)V

    .line 186
    if-eqz v13, :cond_2

    .line 187
    invoke-virtual {v10}, Laar;->h()V

    .line 188
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mMapDropRate:F

    invoke-static {v11, v0}, Lbbq;->a(IF)Z

    move-result v0

    if-nez v0, :cond_2

    .line 189
    invoke-virtual {v13, v10}, Lacv;->a(Laap;)V

    .line 201
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mPreviousFrame:Laap;

    if-eqz v0, :cond_3

    .line 202
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mPreviousFrame:Laap;

    invoke-virtual {v0}, Laap;->f()Laap;

    .line 204
    :cond_3
    invoke-virtual {v12}, Laap;->g()Laap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mPreviousFrame:Laap;

    goto/16 :goto_0

    .line 136
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mImageDims:[I

    invoke-static {v2, v1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 137
    sget-object v0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->TAG:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mImageDims:[I

    aget v2, v2, v4

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mImageDims:[I

    aget v3, v3, v7

    aget v4, v1, v4

    aget v1, v1, v7

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x65

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "input frame dimensions have changed from: ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ", "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") to ( "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " )."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 192
    :cond_5
    if-eqz v11, :cond_2

    if-eqz v13, :cond_2

    .line 196
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mMapDropRate:F

    invoke-static {v11, v0}, Lbbq;->a(IF)Z

    move-result v0

    if-nez v0, :cond_2

    .line 197
    new-array v0, v8, [I

    fill-array-data v0, :array_1

    invoke-virtual {v13, v0}, Lacv;->a([I)Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->d()Laar;

    move-result-object v0

    invoke-virtual {v0, v8}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-static {v1, v4}, Ljava/util/Arrays;->fill([BB)V

    invoke-virtual {v0}, Laar;->h()V

    invoke-virtual {v13, v0}, Lacv;->a(Laap;)V

    goto/16 :goto_2

    :cond_6
    move-object v8, v9

    move-object v10, v9

    goto/16 :goto_1

    .line 169
    :array_0
    .array-data 4
        0x8
        0x8
    .end array-data

    .line 197
    :array_1
    .array-data 4
        0x8
        0x8
    .end array-data
.end method

.method protected j()V
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mIsAnalyzerPrepared:Z

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mContextBuffer:Ljava/nio/ByteBuffer;

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->releaseMotionAnalyzer(Ljava/nio/ByteBuffer;)V

    .line 101
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mIsAnalyzerPrepared:Z

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mPreviousFrame:Laap;

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mPreviousFrame:Laap;

    invoke-virtual {v0}, Laap;->f()Laap;

    .line 105
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/LucasKanadeMotionSaliencyFilter;->mPreviousFrame:Laap;

    .line 107
    :cond_1
    return-void
.end method
