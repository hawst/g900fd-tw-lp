.class public Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorAccumulationFilter;
.super Laak;
.source "PG"


# static fields
.field private static final VECTOR_INPUT_PORT:Ljava/lang/String; = "input"

.field private static final VECTOR_OUTPUT_PORT:Ljava/lang/String; = "output"


# instance fields
.field private mCumulativeVector:[J


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 35
    return-void
.end method


# virtual methods
.method public a([J)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorAccumulationFilter;->mCumulativeVector:[J

    .line 50
    return-void
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 39
    const-class v0, Ljava/lang/Long;

    invoke-static {v0}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v0

    .line 41
    new-instance v1, Lacx;

    invoke-direct {v1}, Lacx;-><init>()V

    const-string v2, "input"

    .line 42
    invoke-virtual {v1, v2, v3, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v1

    const-string v2, "output"

    .line 43
    invoke-virtual {v1, v2, v3, v0}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 54
    const-string v0, "input"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorAccumulationFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v3

    .line 56
    invoke-virtual {v3}, Laap;->a()Labg;

    move-result-object v0

    invoke-virtual {v0}, Labg;->j()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    .line 59
    iget-object v4, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorAccumulationFilter;->mCumulativeVector:[J

    if-nez v4, :cond_0

    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorAccumulationFilter;->mCumulativeVector:[J

    .line 61
    const-string v0, "output"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorAccumulationFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 62
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lacv;->a([I)Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->a()Labg;

    move-result-object v1

    .line 63
    iget-object v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorAccumulationFilter;->mCumulativeVector:[J

    invoke-virtual {v1, v2}, Labg;->a(Ljava/lang/Object;)V

    .line 64
    iget-object v2, v3, Laap;->a:Lzp;

    invoke-virtual {v2}, Lzp;->g()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Labg;->a(J)V

    .line 65
    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    .line 66
    return-void

    .line 59
    :cond_0
    array-length v2, v0

    array-length v5, v4

    if-eq v2, v5, :cond_1

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Unexpected vector length - the current vector\'s length is %d while the previous cumulative vector\'s length is %d."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v1

    const/4 v0, 0x1

    array-length v1, v4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v0

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_1
    array-length v2, v0

    new-array v2, v2, [J

    :goto_1
    array-length v5, v2

    if-ge v1, v5, :cond_2

    aget-wide v6, v0, v1

    aget-wide v8, v4, v1

    add-long/2addr v6, v8

    aput-wide v6, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v2

    goto :goto_0
.end method

.method protected j()V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorAccumulationFilter;->mCumulativeVector:[J

    .line 71
    return-void
.end method
