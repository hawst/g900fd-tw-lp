.class public Lcom/google/android/apps/moviemaker/filterpacks/numeric/HistogramFilter;
.super Lcom/google/android/apps/moviemaker/filterpacks/numeric/BlockHistogramFilter;
.source "PG"


# static fields
.field private static final INPUT_TYPE:Labf;

.field private static final OUTPUT_TYPE:Labf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const/16 v0, 0x64

    invoke-static {v0}, Labf;->b(I)Labf;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/HistogramFilter;->INPUT_TYPE:Labf;

    .line 14
    const/16 v0, 0x66

    invoke-static {v0}, Labf;->a(I)Labf;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/HistogramFilter;->OUTPUT_TYPE:Labf;

    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 17
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/BlockHistogramFilter;-><init>(Lacs;Ljava/lang/String;)V

    .line 18
    invoke-virtual {p0, v0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/HistogramFilter;->a(II)V

    .line 19
    return-void
.end method


# virtual methods
.method public c()Lacx;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 23
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "input"

    sget-object v2, Lcom/google/android/apps/moviemaker/filterpacks/numeric/HistogramFilter;->INPUT_TYPE:Labf;

    .line 24
    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "histograms"

    sget-object v2, Lcom/google/android/apps/moviemaker/filterpacks/numeric/HistogramFilter;->OUTPUT_TYPE:Labf;

    .line 25
    invoke-virtual {v0, v1, v3, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method
