.class public Lcom/google/android/apps/moviemaker/ui/ThemeItemView;
.super Landroid/widget/TextView;
.source "PG"


# static fields
.field public static final a:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Lcom/google/android/apps/moviemaker/ui/ThemeItemView;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Lcom/google/android/apps/moviemaker/ui/ThemeItemView;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Paint;

.field private e:I

.field private f:F

.field private g:Landroid/graphics/RectF;

.field private h:F


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 23
    new-instance v0, Lccl;

    const-class v1, Ljava/lang/Float;

    const-string v2, "lineAngle"

    invoke-direct {v0, v1, v2}, Lccl;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->a:Landroid/util/Property;

    .line 40
    new-instance v0, Lccm;

    const-class v1, Ljava/lang/Float;

    const-string v2, "lineSeparation"

    invoke-direct {v0, v1, v2}, Lccm;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->b:Landroid/util/Property;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 66
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 73
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 57
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->c:Landroid/graphics/Paint;

    .line 58
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->d:Landroid/graphics/Paint;

    .line 62
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->h:F

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 76
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 78
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 79
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0076

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->e:I

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->d:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->e:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 81
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 82
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 164
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->h:F

    sub-float v0, v2, v0

    const v1, 0x3fe66666    # 1.8f

    mul-float/2addr v0, v1

    add-float/2addr v0, v2

    iget v1, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->e:I

    int-to-float v1, v1

    mul-float/2addr v0, v1

    .line 165
    new-instance v1, Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    sub-float/2addr v2, v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    sub-float/2addr v3, v0

    invoke-direct {v1, v0, v0, v2, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v1, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->g:Landroid/graphics/RectF;

    .line 166
    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    .prologue
    .line 124
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->f:F

    return v0
.end method

.method public a(F)V
    .locals 0

    .prologue
    .line 133
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->f:F

    .line 134
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->invalidate()V

    .line 135
    return-void
.end method

.method public b()F
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->h:F

    return v0
.end method

.method public b(F)V
    .locals 0

    .prologue
    .line 152
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->h:F

    .line 153
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->c()V

    .line 154
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->invalidate()V

    .line 155
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 95
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    .line 96
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v2

    .line 100
    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->e:I

    int-to-float v2, v2

    const v3, 0x40133333    # 2.3f

    mul-float/2addr v2, v3

    sub-float v2, v0, v2

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 104
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->f:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 105
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->g:Landroid/graphics/RectF;

    const/high16 v2, -0x3d4c0000    # -90.0f

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->f:F

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->d:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 109
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/TextView;->onDraw(Landroid/graphics/Canvas;)V

    .line 110
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .prologue
    .line 114
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/TextView;->onSizeChanged(IIII)V

    .line 115
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->c()V

    .line 116
    return-void
.end method

.method public setBackgroundColor(I)V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 87
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->c:Landroid/graphics/Paint;

    const/16 v1, 0xe6

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 88
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->invalidate()V

    .line 90
    return-void
.end method
