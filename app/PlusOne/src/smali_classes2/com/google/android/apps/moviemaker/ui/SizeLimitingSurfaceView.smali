.class public Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;
.super Landroid/view/SurfaceView;
.source "PG"


# instance fields
.field private a:I

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 14
    const v0, 0xe1000

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->a:I

    .line 30
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->c()V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 14
    const v0, 0xe1000

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->a:I

    .line 25
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->c()V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 14
    const v0, 0xe1000

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->a:I

    .line 20
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->c()V

    .line 21
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;I)I
    .locals 0

    .prologue
    .line 11
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->b:I

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;I)I
    .locals 0

    .prologue
    .line 11
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->c:I

    return p1
.end method

.method private c()V
    .locals 2

    .prologue
    .line 34
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    new-instance v1, Lcbi;

    invoke-direct {v1, p0}, Lcbi;-><init>(Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;)V

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 49
    return-void
.end method

.method private d()V
    .locals 8

    .prologue
    .line 71
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->getWidth()I

    move-result v0

    .line 72
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->getHeight()I

    move-result v1

    .line 73
    mul-int v2, v0, v1

    .line 74
    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->a:I

    if-le v2, v3, :cond_0

    .line 75
    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->a:I

    int-to-double v4, v3

    int-to-double v2, v2

    div-double v2, v4, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v4

    int-to-double v6, v0

    mul-double/2addr v6, v2

    double-to-int v0, v6

    int-to-double v6, v1

    mul-double/2addr v2, v6

    double-to-int v1, v2

    invoke-interface {v4, v0, v1}, Landroid/view/SurfaceHolder;->setFixedSize(II)V

    .line 80
    :goto_0
    return-void

    .line 78
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->setSizeFromLayout()V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->b:I

    return v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 52
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->a:I

    .line 53
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->d()V

    .line 54
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 61
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->c:I

    return v0
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->d()V

    .line 67
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/SurfaceView;->onSizeChanged(IIII)V

    .line 68
    return-void
.end method
