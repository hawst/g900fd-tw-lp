.class public Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;
.super Laak;
.source "PG"


# static fields
.field private static final BYTES_PER_SAMPLE:I = 0x2

.field private static final FFT_SIZE:I = 0x200

.field private static final FFT_STEP_SIZE:I = 0x100

.field private static final SAMPLE_BUFFER_SIZE:I = 0xfa0

.field private static final SECOND_IN_NS:F = 1.0E9f

.field private static final TARGET_SAMPLE_RATE_HZ:I = 0x3e80


# instance fields
.field private mDownsampleFactor:I

.field private mNanosPerFrame:I

.field private final mOutputTimestampQueue:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final mOutputValueQueue:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[F>;"
        }
    .end annotation
.end field

.field private mSampleBuffer:[S

.field private mSampleRate:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 177
    const-string v0, "moviemaker-jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 178
    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mOutputValueQueue:Ljava/util/List;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mOutputTimestampQueue:Ljava/util/List;

    .line 60
    const/16 v0, 0xfa0

    new-array v0, v0, [S

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mSampleBuffer:[S

    .line 65
    const/4 v0, 0x0

    iput v0, p0, Laak;->mMinimumAvailableInputs:I

    .line 66
    return-void
.end method

.method private native close()V
.end method

.method private native init(I)V
.end method

.method private native process([SI)[[F
.end method


# virtual methods
.method public c()Lacx;
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 70
    const-class v0, Ladc;

    invoke-static {v0}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v0

    .line 71
    const/16 v1, 0xc8

    invoke-static {v1}, Labf;->b(I)Labf;

    move-result-object v1

    .line 73
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "audio"

    .line 74
    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "mfcc"

    .line 75
    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 76
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 14

    .prologue
    .line 89
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v6

    .line 90
    invoke-virtual {v6}, Lacp;->d()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 91
    invoke-virtual {v6}, Lacp;->c()Laap;

    move-result-object v7

    .line 92
    invoke-virtual {v7}, Laap;->a()Labg;

    move-result-object v0

    invoke-virtual {v0}, Labg;->j()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ladc;

    .line 94
    iget v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mSampleRate:I

    iget v2, v0, Ladc;->a:I

    if-eq v1, v2, :cond_1

    .line 95
    iget v2, v0, Ladc;->a:I

    if-lez v2, :cond_3

    const/4 v1, 0x1

    :goto_0
    const-string v3, "sampleRate must be >0"

    invoke-static {v1, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    iget v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mSampleRate:I

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->close()V

    :cond_0
    iput v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mSampleRate:I

    const/4 v1, 0x1

    div-int/lit16 v3, v2, 0x3e80

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mDownsampleFactor:I

    iget v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mDownsampleFactor:I

    div-int v1, v2, v1

    invoke-direct {p0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->init(I)V

    .line 98
    :cond_1
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mOutputTimestampQueue:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 100
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mOutputTimestampQueue:Ljava/util/List;

    iget-object v2, v7, Laap;->a:Lzp;

    invoke-virtual {v2}, Lzp;->g()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    :cond_2
    const/4 v3, 0x0

    .line 105
    iget-object v1, v0, Ladc;->c:[B

    array-length v1, v1

    div-int/lit8 v1, v1, 0x2

    iget v2, v0, Ladc;->b:I

    div-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mDownsampleFactor:I

    div-int/2addr v1, v2

    .line 107
    :goto_1
    if-lez v1, :cond_7

    .line 108
    iget-object v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mSampleBuffer:[S

    array-length v2, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 109
    sub-int v5, v1, v8

    .line 112
    iget v1, v0, Ladc;->b:I

    iget v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mDownsampleFactor:I

    mul-int v9, v1, v2

    .line 113
    const/4 v1, 0x0

    move v4, v1

    :goto_2
    if-ge v4, v8, :cond_5

    .line 114
    const/4 v2, 0x0

    .line 115
    const/4 v1, 0x0

    :goto_3
    if-ge v1, v9, :cond_4

    .line 116
    iget-object v10, v0, Ladc;->c:[B

    const/4 v11, 0x2

    invoke-static {v10, v3, v11}, Lceg;->a([BII)S

    move-result v10

    add-int/2addr v2, v10

    .line 117
    add-int/lit8 v3, v3, 0x2

    .line 115
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 95
    :cond_3
    const/4 v1, 0x0

    goto :goto_0

    .line 119
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mSampleBuffer:[S

    div-int/2addr v2, v9

    int-to-short v2, v2

    aput-short v2, v1, v4

    .line 113
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_2

    .line 121
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mSampleBuffer:[S

    invoke-direct {p0, v1, v8}, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->process([SI)[[F

    move-result-object v2

    .line 122
    if-eqz v2, :cond_6

    .line 123
    const/4 v1, 0x0

    :goto_4
    array-length v4, v2

    if-ge v1, v4, :cond_6

    .line 124
    iget-object v4, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mOutputValueQueue:Ljava/util/List;

    aget-object v8, v2, v1

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 125
    iget-object v4, v7, Laap;->a:Lzp;

    invoke-virtual {v4}, Lzp;->g()J

    move-result-wide v8

    add-int/lit8 v4, v1, 0x1

    const v10, 0x4e6e6b28    # 1.0E9f

    iget v11, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mSampleRate:I

    int-to-float v11, v11

    div-float/2addr v10, v11

    iget v11, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mDownsampleFactor:I

    int-to-float v11, v11

    mul-float/2addr v10, v11

    const/high16 v11, 0x44000000    # 512.0f

    mul-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    iget-object v11, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mOutputTimestampQueue:Ljava/util/List;

    int-to-float v4, v4

    mul-float/2addr v4, v10

    float-to-long v12, v4

    add-long/2addr v8, v12

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v11, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_6
    move v1, v5

    .line 128
    goto :goto_1

    .line 131
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mOutputValueQueue:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 134
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mOutputValueQueue:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    .line 135
    const-string v1, "mfcc"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    .line 136
    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    const/4 v4, 0x1

    aput v4, v2, v3

    const/4 v3, 0x1

    array-length v4, v0

    aput v4, v2, v3

    invoke-virtual {v1, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    .line 137
    invoke-virtual {v2}, Laap;->d()Laar;

    move-result-object v2

    .line 139
    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 140
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 141
    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    .line 142
    invoke-virtual {v2}, Laar;->h()V

    .line 144
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mOutputTimestampQueue:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Laar;->a(J)V

    .line 145
    invoke-virtual {v1, v2}, Lacv;->a(Laap;)V

    .line 149
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mOutputValueQueue:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    invoke-virtual {v6, v0}, Lacp;->b(Z)V

    .line 150
    return-void
.end method

.method protected k()V
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mSampleRate:I

    if-eqz v0, :cond_0

    .line 82
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->mSampleRate:I

    .line 83
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/filterpacks/audio/AudioMfccFilter;->close()V

    .line 85
    :cond_0
    return-void
.end method
