.class public Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;
.super Laak;
.source "PG"

# interfaces
.implements Lbbn;


# static fields
.field private static final TAG:Ljava/lang/String;

.field private static mLogVerbose:Z


# instance fields
.field private mMinEyeDistance:I

.field private mModuleFile:Ljava/io/File;

.field public mNativeTrackerHandle:I

.field private mNumSkipFrames:I

.field private mTimingTracking:Laen;

.field private mWaitForDetector:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    const-class v0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    .line 28
    sput-object v0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->TAG:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mLogVerbose:Z

    .line 185
    const-string v0, "moviemaker-jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 186
    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 31
    iput v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mNativeTrackerHandle:I

    .line 35
    const/16 v0, 0xf

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mMinEyeDistance:I

    .line 37
    iput v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mNumSkipFrames:I

    .line 39
    iput-boolean v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mWaitForDetector:Z

    .line 46
    return-void
.end method

.method private native getFace(II[F)I
.end method

.method private native getNumOfFaces(I)I
.end method

.method private native newFaceTracker()I
.end method

.method private native reset(I)I
.end method

.method private native setParameter(ILjava/lang/String;Ljava/lang/String;)V
.end method

.method private native tearDown(I)I
.end method

.method private native track(ILjava/nio/ByteBuffer;IIJ)I
.end method


# virtual methods
.method public a(Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;)V
    .locals 2

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call setCustomModule while tracker is running!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_0
    if-eqz p1, :cond_1

    .line 91
    invoke-virtual {p1}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->c()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mModuleFile:Ljava/io/File;

    .line 95
    :goto_0
    return-void

    .line 93
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mModuleFile:Ljava/io/File;

    goto :goto_0
.end method

.method protected a([FLandroid/hardware/Camera$Face;)V
    .locals 7

    .prologue
    const/high16 v6, 0x44fa0000    # 2000.0f

    const/high16 v5, 0x3f000000    # 0.5f

    .line 99
    new-instance v0, Landroid/graphics/Rect;

    const/4 v1, 0x0

    aget v1, p1, v1

    sub-float/2addr v1, v5

    mul-float/2addr v1, v6

    float-to-int v1, v1

    const/4 v2, 0x1

    aget v2, p1, v2

    sub-float/2addr v2, v5

    mul-float/2addr v2, v6

    float-to-int v2, v2

    const/4 v3, 0x2

    aget v3, p1, v3

    sub-float/2addr v3, v5

    mul-float/2addr v3, v6

    float-to-int v3, v3

    const/4 v4, 0x3

    aget v4, p1, v4

    sub-float/2addr v4, v5

    mul-float/2addr v4, v6

    float-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p2, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    .line 105
    new-instance v0, Landroid/graphics/Point;

    const/4 v1, 0x4

    aget v1, p1, v1

    sub-float/2addr v1, v5

    mul-float/2addr v1, v6

    float-to-int v1, v1

    const/4 v2, 0x5

    aget v2, p1, v2

    sub-float/2addr v2, v5

    mul-float/2addr v2, v6

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p2, Landroid/hardware/Camera$Face;->leftEye:Landroid/graphics/Point;

    .line 108
    new-instance v0, Landroid/graphics/Point;

    const/4 v1, 0x6

    aget v1, p1, v1

    sub-float/2addr v1, v5

    mul-float/2addr v1, v6

    float-to-int v1, v1

    const/4 v2, 0x7

    aget v2, p1, v2

    sub-float/2addr v2, v5

    mul-float/2addr v2, v6

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p2, Landroid/hardware/Camera$Face;->rightEye:Landroid/graphics/Point;

    .line 111
    new-instance v0, Landroid/graphics/Point;

    const/16 v1, 0x8

    aget v1, p1, v1

    sub-float/2addr v1, v5

    mul-float/2addr v1, v6

    float-to-int v1, v1

    const/16 v2, 0x9

    aget v2, p1, v2

    sub-float/2addr v2, v5

    mul-float/2addr v2, v6

    float-to-int v2, v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p2, Landroid/hardware/Camera$Face;->mouth:Landroid/graphics/Point;

    .line 114
    const/16 v0, 0xa

    aget v0, p1, v0

    const/high16 v1, 0x42c60000    # 99.0f

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p2, Landroid/hardware/Camera$Face;->score:I

    .line 115
    return-void
.end method

.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 65
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "minEyeDist"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 66
    const-string v0, "mMinEyeDistance"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 68
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "numSkipFrames"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 69
    const-string v0, "mNumSkipFrames"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 70
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 71
    :cond_2
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "waitForDetector"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 72
    const-string v0, "mWaitForDetector"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 73
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 50
    const/16 v0, 0x64

    invoke-static {v0}, Labf;->b(I)Labf;

    move-result-object v0

    .line 51
    const-class v1, Landroid/hardware/Camera$Face;

    invoke-static {v1}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v1

    .line 52
    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    .line 53
    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    .line 54
    new-instance v4, Lacx;

    invoke-direct {v4}, Lacx;-><init>()V

    const-string v5, "image"

    .line 55
    invoke-virtual {v4, v5, v7, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v4, "minEyeDist"

    .line 56
    invoke-virtual {v0, v4, v6, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v4, "numSkipFrames"

    .line 57
    invoke-virtual {v0, v4, v6, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "waitForDetector"

    .line 58
    invoke-virtual {v0, v2, v6, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "faces"

    .line 59
    invoke-virtual {v0, v2, v7, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 60
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v8, 0x0

    .line 119
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mNativeTrackerHandle:I

    if-nez v0, :cond_1

    .line 120
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mModuleFile:Ljava/io/File;

    if-nez v0, :cond_0

    .line 121
    invoke-static {}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->a()Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    move-result-object v0

    .line 122
    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->c()Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mModuleFile:Ljava/io/File;

    .line 124
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->newFaceTracker()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mNativeTrackerHandle:I

    .line 125
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mNativeTrackerHandle:I

    const-string v1, "minEyeDist"

    iget v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mMinEyeDistance:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->setParameter(ILjava/lang/String;Ljava/lang/String;)V

    .line 126
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mNativeTrackerHandle:I

    const-string v1, "numSkipFrames"

    iget v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mNumSkipFrames:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->setParameter(ILjava/lang/String;Ljava/lang/String;)V

    .line 127
    iget v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mNativeTrackerHandle:I

    const-string v2, "waitForDetector"

    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mWaitForDetector:Z

    if-eqz v0, :cond_5

    const-string v0, "1"

    :goto_0
    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->setParameter(ILjava/lang/String;Ljava/lang/String;)V

    .line 128
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mNativeTrackerHandle:I

    const-string v1, "modulePath"

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mModuleFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->setParameter(ILjava/lang/String;Ljava/lang/String;)V

    .line 129
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mNativeTrackerHandle:I

    const-string v1, "ffModule"

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mModuleFile:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->setParameter(ILjava/lang/String;Ljava/lang/String;)V

    .line 130
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mNativeTrackerHandle:I

    const-string v1, "trackingError"

    const-string v2, "1.0"

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->setParameter(ILjava/lang/String;Ljava/lang/String;)V

    .line 133
    :cond_1
    sget-boolean v0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mLogVerbose:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mTimingTracking:Laen;

    if-nez v0, :cond_2

    .line 134
    new-instance v0, Laen;

    const-string v1, "FaceTracking"

    invoke-direct {v0, v1}, Laen;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mTimingTracking:Laen;

    .line 136
    :cond_2
    sget-boolean v0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mLogVerbose:Z

    if-eqz v0, :cond_3

    .line 137
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mTimingTracking:Laen;

    invoke-virtual {v0}, Laen;->a()V

    .line 140
    :cond_3
    const-string v0, "faces"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v9

    .line 141
    const-string v0, "image"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v0

    .line 143
    iget v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mNativeTrackerHandle:I

    invoke-virtual {v0, v10}, Laas;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 144
    invoke-virtual {v0}, Laas;->j()I

    move-result v4

    invoke-virtual {v0}, Laas;->k()I

    move-result v5

    iget-object v1, v0, Laap;->a:Lzp;

    invoke-virtual {v1}, Lzp;->g()J

    move-result-wide v6

    move-object v1, p0

    .line 143
    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->track(ILjava/nio/ByteBuffer;IIJ)I

    .line 145
    invoke-virtual {v0}, Laas;->h()V

    .line 147
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mNativeTrackerHandle:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->getNumOfFaces(I)I

    move-result v1

    .line 148
    sget-boolean v0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mLogVerbose:Z

    if-eqz v0, :cond_4

    .line 149
    sget-object v0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->TAG:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0x1d

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Number of faces = "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 152
    :cond_4
    new-array v2, v1, [Landroid/hardware/Camera$Face;

    .line 153
    const/16 v0, 0xb

    new-array v3, v0, [F

    move v0, v8

    .line 154
    :goto_1
    if-ge v0, v1, :cond_6

    .line 155
    new-instance v4, Landroid/hardware/Camera$Face;

    invoke-direct {v4}, Landroid/hardware/Camera$Face;-><init>()V

    aput-object v4, v2, v0

    .line 156
    aget-object v4, v2, v0

    iget v5, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mNativeTrackerHandle:I

    invoke-direct {p0, v5, v0, v3}, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->getFace(II[F)I

    move-result v5

    iput v5, v4, Landroid/hardware/Camera$Face;->id:I

    .line 157
    aget-object v4, v2, v0

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->a([FLandroid/hardware/Camera$Face;)V

    .line 154
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 127
    :cond_5
    const-string v0, "0"

    goto/16 :goto_0

    .line 159
    :cond_6
    new-array v0, v10, [I

    aput v1, v0, v8

    .line 160
    invoke-virtual {v9, v0}, Lacv;->a([I)Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->b()Labh;

    move-result-object v0

    .line 161
    invoke-virtual {v0, v2}, Labh;->b(Ljava/lang/Object;)V

    .line 162
    invoke-virtual {v9, v0}, Lacv;->a(Laap;)V

    .line 164
    sget-boolean v0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mLogVerbose:Z

    if-eqz v0, :cond_7

    .line 165
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mTimingTracking:Laen;

    invoke-virtual {v0}, Laen;->b()V

    .line 166
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mTimingTracking:Laen;

    invoke-virtual {v0}, Laen;->c()V

    .line 168
    :cond_7
    return-void
.end method

.method protected j()V
    .locals 1

    .prologue
    .line 172
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mNativeTrackerHandle:I

    if-eqz v0, :cond_0

    .line 173
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mNativeTrackerHandle:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->reset(I)I

    .line 175
    :cond_0
    return-void
.end method

.method protected k()V
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mNativeTrackerHandle:I

    if-eqz v0, :cond_0

    .line 180
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->mNativeTrackerHandle:I

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/face/AsyncFaceTrackerFilter;->tearDown(I)I

    .line 182
    :cond_0
    return-void
.end method
