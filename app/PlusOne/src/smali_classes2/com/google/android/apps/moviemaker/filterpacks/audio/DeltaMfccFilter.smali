.class public Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;
.super Laak;
.source "PG"


# static fields
.field private static final FEATURE_BUFFER_SIZE:I = 0x5

.field private static final FRAME_BUFFER_SIZE:I = 0x2

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mFeatureBuffer:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<[F>;"
        }
    .end annotation
.end field

.field private mMfccDim:I

.field private final mTimestampBuffer:Ljava/util/ArrayDeque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayDeque",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mFeatureBuffer:Ljava/util/ArrayList;

    .line 30
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mTimestampBuffer:Ljava/util/ArrayDeque;

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mMfccDim:I

    .line 36
    return-void
.end method

.method private a(Laar;)Laar;
    .locals 12

    .prologue
    const/4 v6, 0x5

    const/4 v11, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 80
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mFeatureBuffer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v0, v6, :cond_0

    .line 81
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mMfccDim:I

    new-array v0, v0, [F

    .line 86
    :goto_0
    invoke-virtual {p1, v5}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 87
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 88
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v3

    .line 89
    invoke-virtual {v3}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    move v1, v2

    .line 90
    :goto_1
    iget v4, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mMfccDim:I

    if-ge v1, v4, :cond_1

    .line 91
    invoke-virtual {v3, v1}, Ljava/nio/FloatBuffer;->get(I)F

    move-result v4

    aput v4, v0, v1

    .line 90
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mFeatureBuffer:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    goto :goto_0

    .line 93
    :cond_1
    invoke-virtual {p1}, Laar;->h()V

    .line 94
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mFeatureBuffer:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 96
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mFeatureBuffer:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v6, :cond_5

    .line 97
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mMfccDim:I

    new-array v7, v0, [F

    move v4, v5

    move v1, v2

    :goto_2
    if-gt v4, v11, :cond_3

    mul-int/lit8 v0, v4, 0x2

    mul-int/2addr v0, v4

    add-int v6, v1, v0

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mFeatureBuffer:Ljava/util/ArrayList;

    rsub-int/lit8 v1, v4, 0x2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mFeatureBuffer:Ljava/util/ArrayList;

    add-int/lit8 v3, v4, 0x2

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [F

    move v3, v2

    :goto_3
    iget v8, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mMfccDim:I

    if-ge v3, v8, :cond_2

    aget v8, v7, v3

    aget v9, v1, v3

    aget v10, v0, v3

    sub-float/2addr v9, v10

    int-to-float v10, v4

    mul-float/2addr v9, v10

    add-float/2addr v8, v9

    aput v8, v7, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v1, v6

    goto :goto_2

    :cond_3
    move v0, v2

    :goto_4
    iget v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mMfccDim:I

    if-ge v0, v3, :cond_4

    aget v3, v7, v0

    int-to-float v4, v1

    div-float/2addr v3, v4

    aput v3, v7, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_4
    const-string v0, "delta"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    new-array v1, v11, [I

    aput v5, v1, v2

    iget v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mMfccDim:I

    aput v2, v1, v5

    invoke-virtual {v0, v1}, Lacv;->a([I)Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->d()Laar;

    move-result-object v0

    invoke-virtual {v0, v11}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    invoke-virtual {v0}, Laar;->h()V

    :goto_5
    return-object v0

    :cond_5
    const/4 v0, 0x0

    goto :goto_5
.end method


# virtual methods
.method public c()Lacx;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 40
    const/16 v0, 0xc8

    invoke-static {v0}, Labf;->b(I)Labf;

    move-result-object v0

    .line 42
    new-instance v1, Lacx;

    invoke-direct {v1}, Lacx;-><init>()V

    const-string v2, "mfcc"

    .line 43
    invoke-virtual {v1, v2, v3, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v1

    const-string v2, "delta"

    .line 44
    invoke-virtual {v1, v2, v3, v0}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 45
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 6

    .prologue
    .line 50
    const-string v0, "mfcc"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    .line 51
    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->d()Laar;

    move-result-object v2

    .line 53
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mMfccDim:I

    if-nez v0, :cond_3

    .line 54
    iget-object v0, v2, Laap;->a:Lzp;

    invoke-virtual {v0}, Lzp;->f()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mMfccDim:I

    .line 60
    :cond_0
    const-wide/16 v0, 0x0

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mTimestampBuffer:Ljava/util/ArrayDeque;

    invoke-virtual {v3}, Ljava/util/ArrayDeque;->size()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mTimestampBuffer:Ljava/util/ArrayDeque;

    invoke-virtual {v0}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :cond_1
    iget-object v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mTimestampBuffer:Ljava/util/ArrayDeque;

    iget-object v4, v2, Laap;->a:Lzp;

    invoke-virtual {v4}, Lzp;->g()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayDeque;->add(Ljava/lang/Object;)Z

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 61
    invoke-direct {p0, v2}, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->a(Laar;)Laar;

    move-result-object v1

    .line 62
    if-eqz v1, :cond_2

    .line 63
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Laar;->a(J)V

    .line 64
    const-string v0, "delta"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    .line 66
    :cond_2
    :goto_0
    return-void

    .line 55
    :cond_3
    iget v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mMfccDim:I

    iget-object v1, v2, Laap;->a:Lzp;

    invoke-virtual {v1}, Lzp;->f()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 56
    sget-object v0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->TAG:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/audio/DeltaMfccFilter;->mMfccDim:I

    .line 57
    iget-object v2, v2, Laap;->a:Lzp;

    invoke-virtual {v2}, Lzp;->f()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x3e

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "mfcc feature dimension changes from "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " to "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 56
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
