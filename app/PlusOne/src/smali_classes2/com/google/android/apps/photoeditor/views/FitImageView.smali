.class public Lcom/google/android/apps/photoeditor/views/FitImageView;
.super Landroid/view/View;
.source "PG"


# instance fields
.field private a:Landroid/graphics/Bitmap;

.field private final b:Landroid/graphics/Matrix;

.field private c:Z

.field private d:F

.field private final e:Landroid/graphics/Paint;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 21
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->b:Landroid/graphics/Matrix;

    .line 26
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->e:Landroid/graphics/Paint;

    .line 31
    invoke-direct {p0}, Lcom/google/android/apps/photoeditor/views/FitImageView;->a()V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 21
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->b:Landroid/graphics/Matrix;

    .line 26
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->e:Landroid/graphics/Paint;

    .line 37
    invoke-direct {p0}, Lcom/google/android/apps/photoeditor/views/FitImageView;->a()V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->b:Landroid/graphics/Matrix;

    .line 26
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->e:Landroid/graphics/Paint;

    .line 43
    invoke-direct {p0}, Lcom/google/android/apps/photoeditor/views/FitImageView;->a()V

    .line 44
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 47
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/photoeditor/views/FitImageView;->setBackgroundColor(I)V

    .line 49
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 50
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 51
    return-void
.end method


# virtual methods
.method public a(F)V
    .locals 2

    .prologue
    .line 62
    iget v0, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->d:F

    cmpl-float v0, v0, p1

    if-eqz v0, :cond_0

    .line 63
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v1, 0x42b40000    # 90.0f

    rem-float/2addr v0, v1

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    .line 71
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    iput p1, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->d:F

    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->c:Z

    .line 70
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/views/FitImageView;->invalidate()V

    goto :goto_0
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 54
    iput-object p1, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->a:Landroid/graphics/Bitmap;

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->c:Z

    .line 57
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/views/FitImageView;->invalidate()V

    .line 58
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    .line 107
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 116
    :goto_0
    return-void

    .line 111
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->c:Z

    if-eqz v0, :cond_2

    .line 112
    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/views/FitImageView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/apps/photoeditor/views/FitImageView;->getHeight()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iget-object v4, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->b:Landroid/graphics/Matrix;

    neg-int v5, v3

    int-to-float v5, v5

    div-float/2addr v5, v7

    neg-int v6, v0

    int-to-float v6, v6

    div-float/2addr v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->setTranslate(FF)V

    iget-object v4, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->b:Landroid/graphics/Matrix;

    iget v5, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->d:F

    invoke-virtual {v4, v5}, Landroid/graphics/Matrix;->postRotate(F)Z

    iget-object v4, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->b:Landroid/graphics/Matrix;

    int-to-float v5, v1

    div-float/2addr v5, v7

    int-to-float v6, v2

    div-float/2addr v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iget v4, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->d:F

    const/high16 v5, 0x42b40000    # 90.0f

    cmpl-float v4, v4, v5

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->d:F

    const/high16 v5, 0x43870000    # 270.0f

    cmpl-float v4, v4, v5

    if-nez v4, :cond_3

    :cond_1
    invoke-static {v3, v0, v2, v1}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->a(IIII)Landroid/graphics/Point;

    move-result-object v0

    :goto_1
    iget v0, v0, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    int-to-float v3, v3

    div-float/2addr v0, v3

    iget-object v3, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->b:Landroid/graphics/Matrix;

    int-to-float v1, v1

    div-float/2addr v1, v7

    int-to-float v2, v2

    div-float/2addr v2, v7

    invoke-virtual {v3, v0, v0, v1, v2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->c:Z

    .line 115
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->a:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->b:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto :goto_0

    .line 112
    :cond_3
    invoke-static {v3, v0, v1, v2}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->a(IIII)Landroid/graphics/Point;

    move-result-object v0

    goto :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 101
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 102
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/photoeditor/views/FitImageView;->c:Z

    .line 103
    return-void
.end method
