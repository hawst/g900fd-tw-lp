.class public Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbxr;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lbvr;

.field private final c:Lbxe;

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final e:F

.field private f:Lbxn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->a:Ljava/lang/String;

    .line 719
    const-string v0, "moviemaker-jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 720
    return-void
.end method

.method public constructor <init>(Lbvr;Lbxe;FLjava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbvr;",
            "Lbxe;",
            "F",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 116
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 117
    const-string v0, "qualityModel"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvr;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->b:Lbvr;

    .line 118
    const-string v0, "similarityMetric"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxe;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->c:Lbxe;

    .line 119
    invoke-static {p4}, Lbya;->a(Ljava/util/Collection;)Lbmg;

    move-result-object v0

    sget-object v1, Lbmg;->c:Lbmg;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    cmpl-float v0, p3, v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "the minQuality should only be used for photos"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 121
    iput-object p4, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->d:Ljava/util/Set;

    .line 122
    iput p3, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->e:F

    .line 123
    return-void

    .line 119
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/util/List;[F[[FLbxl;J)Ljava/util/List;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbxk;",
            ">;[F[[F",
            "Lbxl;",
            "J)",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 329
    invoke-virtual/range {p3 .. p3}, Lbxl;->a()Ljava/util/List;

    move-result-object v6

    .line 331
    invoke-static/range {p0 .. p0}, Lbya;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 330
    invoke-static {v2, v6}, Lbya;->a(Ljava/util/List;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v8

    .line 335
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v2

    const-string v3, "owners.size()"

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v4

    const-string v5, "ownerClips.size()"

    invoke-static {v2, v3, v4, v5}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 336
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v7

    .line 337
    new-array v9, v7, [F

    .line 338
    filled-new-array {v7, v7}, [I

    move-result-object v2

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v3, v2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [[F

    .line 341
    const/4 v3, 0x0

    move v4, v3

    :goto_0
    if-ge v4, v7, :cond_0

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget v3, p1, v3

    aput v3, v9, v4

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    :cond_0
    const/4 v3, 0x0

    move v5, v3

    :goto_1
    if-ge v5, v7, :cond_2

    const/4 v3, 0x0

    move v4, v3

    :goto_2
    if-ge v4, v5, :cond_1

    aget-object v10, v2, v5

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget-object v11, p2, v3

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget v3, v11, v3

    aput v3, v10, v4

    aget-object v3, v2, v4

    aget-object v10, v2, v5

    aget v10, v10, v4

    aput v10, v3, v5

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    :cond_1
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    goto :goto_1

    .line 344
    :cond_2
    new-array v5, v7, [I

    .line 345
    const/4 v3, 0x0

    move v4, v3

    :goto_3
    if-ge v4, v7, :cond_3

    .line 346
    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    move-object/from16 v0, p3

    invoke-virtual {v0, v3}, Lbxl;->a(I)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    aput v3, v5, v4

    .line 345
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_3

    .line 350
    :cond_3
    const/4 v3, 0x0

    :goto_4
    if-ge v3, v7, :cond_4

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v14, 0x4000000000000000L    # 2.0

    aget v4, v5, v3

    rsub-int/lit8 v4, v4, 0x1

    int-to-double v0, v4

    move-wide/from16 v16, v0

    invoke-static/range {v14 .. v17}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v14

    add-double/2addr v12, v14

    div-double/2addr v10, v12

    double-to-float v4, v10

    const/high16 v6, 0x40000000    # 2.0f

    add-float/2addr v4, v6

    const/high16 v6, 0x40400000    # 3.0f

    div-float/2addr v4, v6

    aget v6, v9, v3

    mul-float/2addr v4, v6

    aput v4, v9, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 352
    :cond_4
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    new-array v11, v3, [Z

    const/4 v3, 0x0

    :goto_5
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_5

    const/4 v4, 0x1

    aput-boolean v4, v11, v3

    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_5
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    move-wide v6, v4

    move v4, v3

    :goto_6
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v3

    if-ge v4, v3, :cond_7

    long-to-float v3, v6

    move-wide/from16 v0, p4

    long-to-float v5, v0

    const v12, 0x3f8ccccd    # 1.1f

    mul-float/2addr v5, v12

    cmpl-float v3, v3, v5

    if-ltz v3, :cond_6

    aget v3, v9, v4

    const v5, 0x3dcccccd    # 0.1f

    cmpg-float v3, v3, v5

    if-gez v3, :cond_6

    const/4 v3, 0x0

    aput-boolean v3, v11, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v10, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_6
    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbmd;

    invoke-virtual {v3}, Lbmd;->b()J

    move-result-wide v12

    add-long/2addr v6, v12

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_6

    :cond_7
    const v3, 0x3ecccccd    # 0.4f

    sget-object v4, Lbxo;->a:Lbxo;

    invoke-static {v2, v9, v3, v4, v11}, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->a([[F[FFLbxo;[Z)Ljava/util/List;

    move-result-object v2

    invoke-interface {v10, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move v3, v2

    :goto_7
    if-ltz v3, :cond_8

    invoke-interface {v10, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v8, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v3, -0x1

    move v3, v2

    goto :goto_7

    :cond_8
    return-object v4
.end method

.method private a([FLbyd;J)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([F",
            "Lbyd;",
            "J)",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v9, 0x19

    .line 485
    const v2, 0x3f19999a    # 0.6f

    .line 486
    const/4 v1, 0x0

    .line 487
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    move v6, v1

    move v7, v2

    .line 489
    :goto_0
    if-ge v6, v9, :cond_3

    .line 490
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {p2}, Lbyd;->a()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 491
    invoke-static {v0}, Lbya;->b(Ljava/util/List;)J

    move-result-wide v2

    cmp-long v1, v2, p3

    if-gez v1, :cond_3

    .line 498
    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Lbyd;->a(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-static {v0}, Lbya;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 506
    :goto_1
    const v1, 0x3f99999a    # 1.2f

    div-float v2, v7, v1

    .line 507
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    move v7, v2

    goto :goto_0

    .line 498
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->f:Lbxn;

    invoke-virtual {v1}, Lbxn;->a()[[F

    move-result-object v1

    invoke-static {p1, v1, v7}, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->getClusterMemberships([F[[FF)[I

    move-result-object v1

    new-instance v3, Lbxl;

    invoke-direct {v3, v1}, Lbxl;-><init>([I)V

    const/4 v1, 0x6

    invoke-virtual {p2, v1}, Lbyd;->a(I)Ljava/util/List;

    move-result-object v8

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->f:Lbxn;

    invoke-virtual {v1}, Lbxn;->a()[[F

    move-result-object v2

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbxk;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_1
    invoke-virtual {v3, v4, v2}, Lbxl;->a(Ljava/util/List;[[F)Lbxl;

    move-result-object v3

    :cond_2
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->f:Lbxn;

    invoke-virtual {v1}, Lbxn;->a()[[F

    move-result-object v2

    move-object v1, p1

    move-wide v4, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->a(Ljava/util/List;[F[[FLbxl;J)Ljava/util/List;

    move-result-object v1

    invoke-static {v0}, Lbya;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v8}, Lbya;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->a([FLjava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    invoke-static {v2, v0}, Lcfi;->a(Ljava/util/Collection;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 510
    :cond_3
    if-ne v6, v9, :cond_4

    .line 511
    sget-object v1, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x52

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Clustering reached maximum "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " iterations without meeting target duration."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    :cond_4
    return-object v0
.end method

.method private a([FLjava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([F",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 573
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 574
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 575
    invoke-interface {p2, v0}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v3

    aget v3, p1, v3

    iget v4, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->e:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_0

    .line 576
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 579
    :cond_1
    return-object v1
.end method

.method public static a([[F[FFLbxo;[Z)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([[F[FF",
            "Lbxo;",
            "[Z)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 442
    const-string v0, "distMatrix"

    invoke-static {p0, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 443
    const-string v0, "qualityVec"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 444
    const-string v0, "selected"

    invoke-static {p4, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 446
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 448
    :goto_0
    const v0, 0x7f7fffff    # Float.MAX_VALUE

    move v2, v3

    move v4, v0

    move v0, v1

    .line 450
    :goto_1
    array-length v5, p4

    if-ge v0, v5, :cond_1

    .line 451
    aget-boolean v5, p4, v0

    if-eqz v5, :cond_0

    .line 452
    aget-object v5, p0, v0

    invoke-interface {p3, v0, v5, p4}, Lbxo;->a(I[F[Z)F

    move-result v5

    .line 457
    aget v7, p1, v0

    mul-float/2addr v7, p2

    add-float/2addr v5, v7

    .line 458
    cmpg-float v7, v5, v4

    if-gez v7, :cond_0

    move v2, v0

    move v4, v5

    .line 450
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 463
    :cond_1
    if-eq v2, v3, :cond_2

    .line 464
    aput-boolean v1, p4, v2

    .line 470
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 472
    :cond_2
    return-object v6
.end method

.method private static native getClusterMemberships([F[[FF)[I
.end method


# virtual methods
.method public a(Lbxs;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxs;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x2

    const/4 v8, 0x4

    .line 147
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->d:Ljava/util/Set;

    .line 148
    invoke-static {p1, v0}, Lbya;->a(Lbxs;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 154
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->b:Lbvr;

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->d:Ljava/util/Set;

    .line 155
    invoke-static {v0, v1, p1, v2}, Lbya;->a(Ljava/util/List;Lbvr;Lbxs;Ljava/util/Collection;)Lbyd;

    move-result-object v3

    .line 161
    const/16 v0, 0xa

    invoke-virtual {v3, v0}, Lbyd;->a(I)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lbya;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 162
    iget-wide v0, p1, Lbxs;->c:J

    .line 163
    invoke-static {v2}, Lbya;->b(Ljava/util/List;)J

    move-result-wide v4

    sub-long v4, v0, v4

    .line 164
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-gtz v0, :cond_0

    move-object v0, v2

    .line 171
    :goto_0
    return-object v0

    .line 168
    :cond_0
    invoke-virtual {v3, v8}, Lbyd;->a(I)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lbya;->a(Ljava/util/List;)J

    move-result-wide v6

    cmp-long v1, v6, v4

    if-gtz v1, :cond_4

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->d:Ljava/util/Set;

    invoke-static {v0}, Lbya;->a(Ljava/util/Collection;)Lbmg;

    move-result-object v0

    sget-object v1, Lbmg;->c:Lbmg;

    if-ne v0, v1, :cond_3

    iget-object v4, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->b:Lbvr;

    iget v5, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->e:F

    const-string v0, "clipMetrics"

    invoke-static {v3, v0, v10}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/4 v0, 0x6

    invoke-virtual {v3, v0, v1}, Lbyd;->a(ILjava/util/List;)V

    invoke-virtual {v3, v8, v9}, Lbyd;->a(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxk;

    iget-object v6, v0, Lbxk;->b:Lbmr;

    invoke-interface {v4, v6}, Lbvr;->a(Lbmr;)F

    move-result v6

    cmpl-float v6, v6, v5

    if-ltz v6, :cond_1

    iget-object v0, v0, Lbxk;->a:Lbmd;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 170
    :goto_2
    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 171
    iget-wide v0, p1, Lbxs;->c:J

    invoke-static {v2, v0, v1}, Lbya;->a(Ljava/util/List;J)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 168
    :cond_3
    const-string v0, "summarizerClipMetrics"

    invoke-static {v3, v0, v10}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v3, v8, v0}, Lbyd;->a(ILjava/util/List;)V

    const/16 v1, 0x8

    invoke-virtual {v3, v1, v9}, Lbyd;->a(II)Ljava/util/List;

    move-result-object v1

    invoke-static {v1, v0}, Lbya;->a(Ljava/util/List;Ljava/util/List;)V

    goto :goto_2

    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->f:Lbxn;

    iget-object v6, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->b:Lbvr;

    iget-object v7, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->c:Lbxe;

    invoke-static {v0, v1, v6, v7}, Lbya;->a(Ljava/util/List;Lbxn;Lbvr;Lbxe;)Landroid/util/Pair;

    move-result-object v1

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lbxn;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->f:Lbxn;

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, [F

    invoke-direct {p0, v0, v3, v4, v5}, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->a([FLbyd;J)Ljava/util/List;

    move-result-object v0

    goto :goto_2
.end method

.method public b(Lbxs;)J
    .locals 7

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->d:Ljava/util/Set;

    .line 186
    invoke-static {p1, v0}, Lbya;->a(Lbxs;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 187
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->b:Lbvr;

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->d:Ljava/util/Set;

    .line 188
    invoke-static {v0, v1, p1, v2}, Lbya;->a(Ljava/util/List;Lbvr;Lbxs;Ljava/util/Collection;)Lbyd;

    move-result-object v0

    .line 190
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->d:Ljava/util/Set;

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 192
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lbyd;->a(I)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->b:Lbvr;

    iget v3, p0, Lcom/google/android/apps/moviemaker/summarizer/ClusteringSummarizer;->e:F

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v5, 0x4

    invoke-virtual {v0, v5}, Lbyd;->a(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxk;

    iget-object v6, v0, Lbxk;->b:Lbmr;

    invoke-interface {v2, v6}, Lbvr;->a(Lbmr;)F

    move-result v6

    cmpl-float v6, v6, v3

    if-ltz v6, :cond_0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-static {v1}, Lbya;->a(Ljava/util/List;)J

    move-result-wide v0

    invoke-static {v4}, Lbya;->a(Ljava/util/List;)J

    move-result-wide v2

    add-long/2addr v0, v2

    .line 195
    :goto_1
    return-wide v0

    :cond_2
    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lbyd;->b(I)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lbya;->a(Ljava/util/List;)J

    move-result-wide v0

    goto :goto_1
.end method
