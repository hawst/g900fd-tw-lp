.class public Lcom/google/android/apps/moviemaker/filterpacks/base/ToggleableBranchFilter;
.super Laak;
.source "PG"


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 20
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 5

    .prologue
    .line 31
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "input"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 32
    iget-object v1, p0, Laak;->mConnectedOutputPortArray:[Lacv;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 33
    invoke-virtual {v3}, Lacv;->d()Lacp;

    move-result-object v4

    invoke-virtual {v4}, Lacp;->g()Laak;

    move-result-object v4

    invoke-virtual {v4}, Laak;->e()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 34
    invoke-virtual {p1, v3}, Lacp;->a(Lacv;)V

    .line 32
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 38
    :cond_1
    return-void
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    .line 24
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "input"

    const/4 v2, 0x2

    .line 25
    invoke-static {}, Labf;->a()Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Lacx;->a()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected h()V
    .locals 5

    .prologue
    .line 42
    iget-object v1, p0, Laak;->mConnectedOutputPortArray:[Lacv;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 44
    invoke-virtual {v3}, Lacv;->d()Lacp;

    move-result-object v4

    invoke-virtual {v4}, Lacp;->g()Laak;

    move-result-object v4

    invoke-virtual {v4}, Laak;->e()Z

    move-result v4

    invoke-virtual {v3, v4}, Lacv;->a(Z)V

    .line 42
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 46
    :cond_0
    return-void
.end method

.method protected i()V
    .locals 6

    .prologue
    .line 50
    const-string v0, "input"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/base/ToggleableBranchFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v1

    .line 51
    iget-object v2, p0, Laak;->mConnectedOutputPortArray:[Lacv;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 52
    invoke-virtual {v4}, Lacv;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 53
    invoke-virtual {v4, v1}, Lacv;->a(Laap;)V

    .line 51
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 56
    :cond_1
    return-void
.end method
