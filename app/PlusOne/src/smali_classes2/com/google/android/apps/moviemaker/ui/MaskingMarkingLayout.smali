.class public Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;
.super Landroid/widget/FrameLayout;
.source "PG"


# instance fields
.field private A:Lalx;

.field private B:Lcap;

.field private C:Z

.field private D:Landroid/graphics/Paint;

.field private E:Landroid/graphics/Paint;

.field private a:Z

.field private b:Z

.field private c:F

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:Landroid/graphics/Rect;

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:Landroid/graphics/Paint;

.field private final l:Landroid/graphics/Paint;

.field private final m:Landroid/graphics/Paint;

.field private final n:I

.field private final o:I

.field private final p:I

.field private final q:I

.field private r:Landroid/view/View;

.field private s:Landroid/view/View;

.field private t:Landroid/view/View;

.field private final u:Landroid/graphics/Bitmap;

.field private final v:Landroid/graphics/Bitmap;

.field private final w:Landroid/graphics/Bitmap;

.field private final x:Landroid/graphics/Bitmap;

.field private final y:Lalw;

.field private final z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcao;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 142
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 146
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 149
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 106
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->k:Landroid/graphics/Paint;

    .line 107
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->l:Landroid/graphics/Paint;

    .line 108
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->m:Landroid/graphics/Paint;

    .line 151
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 153
    invoke-virtual {p0, v2}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->setWillNotDraw(Z)V

    .line 154
    invoke-virtual {p0, v2}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->setClipToPadding(Z)V

    .line 156
    sget-object v0, Lahj;->c:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v4

    .line 158
    invoke-virtual {v4, v1, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->h:I

    .line 160
    const/4 v0, 0x2

    const v5, -0xff0001

    invoke-virtual {v4, v0, v5}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->i:I

    .line 163
    const/4 v0, 0x4

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->o:I

    .line 164
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->o:I

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    const-string v5, "inMarkerViewId is a required xml attribute"

    invoke-static {v0, v5}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 166
    const/4 v0, 0x5

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->p:I

    .line 167
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->p:I

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    const-string v5, "outMarkerViewId is a required xml attribute"

    invoke-static {v0, v5}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 169
    const/4 v0, 0x3

    invoke-virtual {v4, v0, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->n:I

    .line 170
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->n:I

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    const-string v5, "maskedViewId is a required xml attribute"

    invoke-static {v0, v5}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 172
    invoke-virtual {v4, v2, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->q:I

    .line 173
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->q:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->q:I

    if-ne v0, v1, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->q:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v5, 0x3c

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "orientation must be horizontal or vertical, not: "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 176
    invoke-virtual {v4}, Landroid/content/res/TypedArray;->recycle()V

    .line 178
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->l:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->h:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 179
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->l:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->m:Landroid/graphics/Paint;

    iget v1, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->i:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 182
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->m:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 184
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->l:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->D:Landroid/graphics/Paint;

    .line 185
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->l:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->E:Landroid/graphics/Paint;

    .line 187
    const v0, 0x7f02056e

    .line 188
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 189
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->u:Landroid/graphics/Bitmap;

    .line 190
    const v0, 0x7f02056f

    .line 191
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 192
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->v:Landroid/graphics/Bitmap;

    .line 194
    const v0, 0x7f020574

    .line 195
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 196
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->w:Landroid/graphics/Bitmap;

    .line 197
    const v0, 0x7f020575

    .line 198
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    .line 199
    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->x:Landroid/graphics/Bitmap;

    .line 202
    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->j:I

    .line 204
    const v0, 0x7f0d0089

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->e:I

    .line 206
    const v0, 0x7f0d008a

    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->f:I

    .line 209
    new-instance v0, Landroid/graphics/Rect;

    const v1, 0x7f0d0084

    .line 210
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v2, 0x7f0d0085

    .line 211
    invoke-virtual {v3, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    const v4, 0x7f0d0086

    .line 212
    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    const v5, 0x7f0d0087

    .line 213
    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    invoke-direct {v0, v1, v2, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->g:Landroid/graphics/Rect;

    .line 215
    const v0, 0x7f0d0088

    .line 216
    invoke-virtual {v3, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->d:I

    .line 218
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->k:Landroid/graphics/Paint;

    const v1, 0x7f0b003b

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 219
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->k:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 220
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->k:Landroid/graphics/Paint;

    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 221
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->k:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SCREEN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 223
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->q:I

    if-nez v0, :cond_5

    .line 225
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingLeft()I

    move-result v0

    .line 226
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingTop()I

    move-result v1

    .line 227
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingRight()I

    move-result v2

    .line 228
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingBottom()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->u:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    .line 224
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->setPadding(IIII)V

    .line 237
    :goto_3
    new-instance v0, Lalw;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Lalw;-><init>(FF)V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->y:Lalw;

    .line 238
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->z:Ljava/util/List;

    .line 239
    return-void

    :cond_2
    move v0, v2

    .line 164
    goto/16 :goto_0

    :cond_3
    move v0, v2

    .line 167
    goto/16 :goto_1

    :cond_4
    move v0, v2

    .line 170
    goto/16 :goto_2

    .line 231
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingLeft()I

    move-result v0

    .line 232
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingTop()I

    move-result v1

    .line 233
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingRight()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->u:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    .line 234
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingBottom()I

    move-result v3

    .line 230
    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->setPadding(IIII)V

    goto :goto_3
.end method

.method private a(Z)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/high16 v7, 0x40000000    # 2.0f

    .line 683
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a()I

    move-result v0

    int-to-float v0, v0

    move v3, v0

    .line 684
    :goto_0
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->s:Landroid/view/View;

    .line 685
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->g:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->g:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    move v2, v0

    .line 687
    :goto_1
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->g:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    .line 688
    :goto_2
    neg-float v4, v0

    .line 689
    sub-float v1, v3, v2

    sub-float v5, v1, v0

    .line 690
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->y:Lalw;

    iget v1, v1, Lalw;->a:F

    mul-float/2addr v1, v3

    div-float v6, v2, v7

    sub-float/2addr v1, v6

    sub-float/2addr v1, v0

    float-to-int v1, v1

    int-to-float v1, v1

    invoke-static {v1, v4, v5}, Lcfn;->a(FFF)F

    move-result v1

    .line 693
    iget-object v6, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->y:Lalw;

    iget v6, v6, Lalw;->b:F

    mul-float/2addr v3, v6

    div-float v6, v2, v7

    sub-float/2addr v3, v6

    sub-float v0, v3, v0

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-static {v0, v4, v5}, Lcfn;->a(FFF)F

    move-result v0

    .line 698
    add-float v3, v1, v2

    sub-float/2addr v3, v0

    .line 699
    cmpl-float v6, v3, v8

    if-lez v6, :cond_0

    .line 700
    sub-float v4, v1, v4

    invoke-static {v4, v2}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 701
    sub-float/2addr v5, v0

    invoke-static {v5, v2}, Ljava/lang/Math;->min(FF)F

    move-result v5

    .line 702
    add-float/2addr v5, v4

    .line 703
    cmpl-float v6, v5, v8

    if-lez v6, :cond_0

    .line 705
    div-float/2addr v4, v5

    .line 706
    div-float/2addr v2, v7

    invoke-static {v3, v2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    .line 707
    mul-float v3, v2, v4

    sub-float/2addr v1, v3

    .line 708
    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v0, v2

    .line 713
    :cond_0
    if-eqz p1, :cond_4

    .line 714
    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->s:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 715
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->t:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 720
    :goto_3
    return-void

    .line 683
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->b()I

    move-result v0

    int-to-float v0, v0

    move v3, v0

    goto :goto_0

    .line 685
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->s:Landroid/view/View;

    .line 686
    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->g:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->g:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    move v2, v0

    goto :goto_1

    .line 687
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->g:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    goto :goto_2

    .line 717
    :cond_4
    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->s:Landroid/view/View;

    invoke-virtual {v2, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 718
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->t:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_3
.end method

.method private a(IIZ)Z
    .locals 5

    .prologue
    .line 572
    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->s:Landroid/view/View;

    move-object v2, v0

    .line 573
    :goto_0
    if-eqz p3, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->y:Lalw;

    iget v0, v0, Lalw;->a:F

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->d(F)I

    move-result v3

    .line 580
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->q:I

    if-nez v0, :cond_2

    .line 581
    invoke-virtual {v2}, Landroid/view/View;->getTranslationX()F

    move-result v0

    float-to-int v1, v0

    .line 582
    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v4

    add-int/2addr v0, v4

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    add-int/2addr v0, v2

    .line 590
    :goto_2
    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->d:I

    div-int/lit8 v2, v2, 0x2

    sub-int v2, v3, v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 591
    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->d:I

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v3

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 592
    if-le p1, v1, :cond_3

    if-ge p1, v0, :cond_3

    const/4 v0, 0x1

    :goto_3
    return v0

    .line 572
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->t:Landroid/view/View;

    move-object v2, v0

    goto :goto_0

    .line 573
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->y:Lalw;

    iget v0, v0, Lalw;->b:F

    goto :goto_1

    .line 585
    :cond_2
    invoke-virtual {v2}, Landroid/view/View;->getTranslationY()F

    move-result v0

    float-to-int v1, v0

    .line 586
    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    add-int/2addr v0, v4

    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    add-int/2addr v0, v2

    move p1, p2

    .line 587
    goto :goto_2

    .line 592
    :cond_3
    const/4 v0, 0x0

    goto :goto_3
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 730
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->A:Lalx;

    if-eqz v0, :cond_0

    .line 731
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->A:Lalx;

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->y:Lalw;

    invoke-interface {v0, v1, p1}, Lalx;->a(Lalw;Z)Lalw;

    .line 733
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->d()V

    .line 734
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->invalidate()V

    .line 735
    return-void
.end method

.method private d(F)I
    .locals 2

    .prologue
    .line 534
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->q:I

    if-nez v0, :cond_0

    .line 535
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingLeft()I

    move-result v1

    add-int/2addr v0, v1

    .line 537
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->b()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingTop()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 666
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->q:I

    if-nez v0, :cond_0

    .line 667
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a(Z)V

    .line 671
    :goto_0
    return-void

    .line 669
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a(Z)V

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 513
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public a(F)Lalw;
    .locals 4

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->y:Lalw;

    const-string v1, "position"

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {p1, v1, v2, v3}, Lcec;->a(FLjava/lang/CharSequence;FF)F

    move-result v1

    iput v1, v0, Lalw;->a:F

    .line 261
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->b(Z)V

    .line 262
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->y:Lalw;

    return-object v0
.end method

.method public a(Lalx;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->A:Lalx;

    .line 316
    return-void
.end method

.method public a(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V
    .locals 5

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->s:Landroid/view/View;

    .line 286
    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    float-to-int v0, v0

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->s:Landroid/view/View;

    .line 287
    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->s:Landroid/view/View;

    .line 288
    invoke-virtual {v2}, Landroid/view/View;->getTranslationX()F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->s:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->s:Landroid/view/View;

    .line 289
    invoke-virtual {v3}, Landroid/view/View;->getTranslationY()F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->s:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    .line 285
    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 290
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->s:Landroid/view/View;

    invoke-virtual {p1, v0, p2}, Landroid/view/ViewGroup;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 291
    return-void
.end method

.method public a(Lcao;)V
    .locals 3

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->z:Ljava/util/List;

    const-string v1, "listener"

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 323
    return-void
.end method

.method public a(Lcap;)V
    .locals 2

    .prologue
    .line 329
    const-string v0, "touchListener"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcap;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->B:Lcap;

    .line 330
    return-void
.end method

.method public a(II)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 631
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->q:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->q:I

    if-ne v0, v1, :cond_2

    .line 632
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->b()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move v0, v2

    .line 658
    :goto_0
    return v0

    .line 636
    :cond_2
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->q:I

    if-nez v0, :cond_3

    .line 637
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingLeft()I

    move-result v0

    sub-int v0, p1, v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v0, v3

    .line 640
    :goto_1
    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->c:F

    sub-float/2addr v0, v3

    invoke-static {v0}, Lcfn;->a(F)F

    move-result v0

    .line 642
    iget-boolean v3, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a:Z

    if-eqz v3, :cond_5

    .line 643
    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a(F)Lalw;

    .line 644
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    :goto_2
    if-ge v2, v3, :cond_4

    .line 645
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->z:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcao;

    iget-object v4, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->y:Lalw;

    invoke-interface {v0, v4}, Lcao;->a(Lalw;)V

    .line 644
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 638
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingTop()I

    move-result v0

    sub-int v0, p2, v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->b()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v0, v3

    goto :goto_1

    :cond_4
    move v0, v1

    .line 647
    goto :goto_0

    .line 650
    :cond_5
    iget-boolean v3, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->b:Z

    if-eqz v3, :cond_7

    .line 651
    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->b(F)Lalw;

    .line 652
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    :goto_3
    if-ge v2, v3, :cond_6

    .line 653
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->z:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcao;

    iget-object v4, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->y:Lalw;

    invoke-interface {v0, v4}, Lcao;->b(Lalw;)V

    .line 652
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_6
    move v0, v1

    .line 655
    goto :goto_0

    :cond_7
    move v0, v2

    .line 658
    goto :goto_0
.end method

.method public b()I
    .locals 2

    .prologue
    .line 520
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingBottom()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingTop()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public b(F)Lalw;
    .locals 4

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->y:Lalw;

    const-string v1, "position"

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {p1, v1, v2, v3}, Lcec;->a(FLjava/lang/CharSequence;FF)F

    move-result v1

    iput v1, v0, Lalw;->b:F

    .line 273
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->b(Z)V

    .line 274
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->y:Lalw;

    return-object v0
.end method

.method public b(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V
    .locals 5

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->t:Landroid/view/View;

    .line 302
    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v0

    float-to-int v0, v0

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->t:Landroid/view/View;

    .line 303
    invoke-virtual {v1}, Landroid/view/View;->getTranslationY()F

    move-result v1

    float-to-int v1, v1

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->t:Landroid/view/View;

    .line 304
    invoke-virtual {v2}, Landroid/view/View;->getTranslationX()F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->t:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->t:Landroid/view/View;

    .line 305
    invoke-virtual {v3}, Landroid/view/View;->getTranslationY()F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->t:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v3, v4

    .line 301
    invoke-virtual {p2, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 306
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->t:Landroid/view/View;

    invoke-virtual {p1, v0, p2}, Landroid/view/ViewGroup;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 307
    return-void
.end method

.method public c(F)I
    .locals 1

    .prologue
    .line 616
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->q:I

    if-nez v0, :cond_0

    .line 617
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getWidth()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    .line 619
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getHeight()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-int v0, v0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 527
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->q:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    .line 334
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 336
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->y:Lalw;

    iget v0, v0, Lalw;->a:F

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->d(F)I

    move-result v6

    .line 337
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->y:Lalw;

    iget v0, v0, Lalw;->b:F

    invoke-direct {p0, v0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->d(F)I

    move-result v7

    .line 340
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->q:I

    if-nez v0, :cond_0

    .line 342
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingLeft()I

    move-result v0

    int-to-float v1, v0

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->r:Landroid/view/View;

    .line 343
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    int-to-float v2, v0

    int-to-float v3, v6

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->r:Landroid/view/View;

    .line 345
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->k:Landroid/graphics/Paint;

    move-object v0, p1

    .line 341
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 347
    int-to-float v1, v7

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->r:Landroid/view/View;

    .line 349
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    int-to-float v2, v0

    .line 350
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingLeft()I

    move-result v3

    add-int/2addr v0, v3

    int-to-float v3, v0

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->r:Landroid/view/View;

    .line 351
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->k:Landroid/graphics/Paint;

    move-object v0, p1

    .line 347
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 369
    :goto_0
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->q:I

    if-nez v0, :cond_1

    .line 370
    int-to-float v1, v6

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->s:Landroid/view/View;

    .line 372
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->g:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v2

    int-to-float v2, v0

    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->j:I

    add-int/2addr v0, v6

    int-to-float v3, v0

    .line 374
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getHeight()I

    move-result v0

    iget-object v4, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->w:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->D:Landroid/graphics/Paint;

    move-object v0, p1

    .line 370
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 376
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->j:I

    sub-int v0, v7, v0

    int-to-float v1, v0

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->t:Landroid/view/View;

    .line 378
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->g:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v0, v2

    int-to-float v2, v0

    int-to-float v3, v7

    .line 380
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getHeight()I

    move-result v0

    iget-object v4, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->w:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    sub-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->E:Landroid/graphics/Paint;

    move-object v0, p1

    .line 376
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 398
    :goto_1
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->q:I

    if-nez v0, :cond_4

    .line 399
    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->v:Landroid/graphics/Bitmap;

    :goto_2
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->u:Landroid/graphics/Bitmap;

    .line 400
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int v1, v6, v1

    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->f:I

    add-int/2addr v1, v2

    int-to-float v1, v1

    .line 401
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->u:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->e:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->D:Landroid/graphics/Paint;

    .line 399
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 404
    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->b:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->x:Landroid/graphics/Bitmap;

    :goto_3
    iget v1, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->f:I

    sub-int v1, v7, v1

    int-to-float v1, v1

    .line 406
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getHeight()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->w:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    sub-int/2addr v2, v3

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->e:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->E:Landroid/graphics/Paint;

    .line 404
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 419
    :goto_4
    return-void

    .line 354
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->r:Landroid/view/View;

    .line 355
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v1, v0

    .line 356
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingTop()I

    move-result v0

    int-to-float v2, v0

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->r:Landroid/view/View;

    .line 357
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    int-to-float v3, v0

    int-to-float v4, v6

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->k:Landroid/graphics/Paint;

    move-object v0, p1

    .line 354
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 360
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->r:Landroid/view/View;

    .line 361
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    int-to-float v1, v0

    int-to-float v2, v7

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->r:Landroid/view/View;

    .line 363
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    int-to-float v3, v0

    .line 364
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->b()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingTop()I

    move-result v4

    add-int/2addr v0, v4

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->k:Landroid/graphics/Paint;

    move-object v0, p1

    .line 360
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 383
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->s:Landroid/view/View;

    .line 384
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->g:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    int-to-float v1, v0

    int-to-float v2, v6

    .line 386
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getWidth()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->w:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int/2addr v0, v3

    int-to-float v3, v0

    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->j:I

    add-int/2addr v0, v6

    int-to-float v4, v0

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->D:Landroid/graphics/Paint;

    move-object v0, p1

    .line 383
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 389
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->t:Landroid/view/View;

    .line 390
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->g:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    sub-int/2addr v0, v1

    int-to-float v1, v0

    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->j:I

    sub-int v0, v7, v0

    int-to-float v2, v0

    .line 392
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getWidth()I

    move-result v0

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->w:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    sub-int/2addr v0, v3

    int-to-float v3, v0

    int-to-float v4, v7

    iget-object v5, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->E:Landroid/graphics/Paint;

    move-object v0, p1

    .line 389
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_1

    .line 399
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->u:Landroid/graphics/Bitmap;

    goto/16 :goto_2

    .line 404
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->w:Landroid/graphics/Bitmap;

    goto/16 :goto_3

    .line 409
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->v:Landroid/graphics/Bitmap;

    .line 410
    :goto_5
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->u:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->f:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->u:Landroid/graphics/Bitmap;

    .line 411
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int v2, v6, v2

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->e:I

    add-int/2addr v2, v3

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->D:Landroid/graphics/Paint;

    .line 409
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 414
    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->b:Z

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->x:Landroid/graphics/Bitmap;

    .line 415
    :goto_6
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->w:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    sub-int/2addr v1, v2

    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->f:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->e:I

    sub-int v2, v7, v2

    int-to-float v2, v2

    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->E:Landroid/graphics/Paint;

    .line 414
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 409
    :cond_5
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->u:Landroid/graphics/Bitmap;

    goto :goto_5

    .line 414
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->w:Landroid/graphics/Bitmap;

    goto :goto_6
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 243
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->n:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->r:Landroid/view/View;

    .line 244
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->o:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->s:Landroid/view/View;

    .line 245
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->p:I

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->t:Landroid/view/View;

    .line 247
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->s:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 248
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->C:Z

    .line 250
    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    .line 251
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 597
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 598
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->d()V

    .line 599
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .prologue
    .line 603
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 604
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->d()V

    .line 605
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 423
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v3, v0

    .line 424
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v4, v0

    .line 426
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->q:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a()I

    move-result v0

    if-eqz v0, :cond_1

    .line 427
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->b()I

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    move v0, v2

    .line 506
    :goto_0
    return v0

    .line 431
    :cond_2
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->q:I

    if-nez v0, :cond_4

    .line 432
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingLeft()I

    move-result v0

    sub-int v0, v3, v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v0, v5

    .line 435
    :goto_1
    invoke-static {v0}, Lcfn;->a(F)F

    move-result v5

    .line 437
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_3
    move v0, v2

    .line 506
    goto :goto_0

    .line 433
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getPaddingTop()I

    move-result v0

    sub-int v0, v4, v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->b()I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v0, v5

    goto :goto_1

    .line 439
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->B:Lcap;

    if-eqz v0, :cond_5

    .line 440
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->B:Lcap;

    invoke-interface {v0, v3, v4}, Lcap;->a(II)V

    .line 442
    :cond_5
    invoke-direct {p0, v3, v4, v1}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a(IIZ)Z

    move-result v0

    .line 443
    invoke-direct {p0, v3, v4, v2}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a(IIZ)Z

    move-result v3

    .line 444
    if-nez v0, :cond_6

    if-eqz v3, :cond_3

    .line 445
    :cond_6
    if-eqz v0, :cond_9

    if-eqz v3, :cond_7

    iget-boolean v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->C:Z

    if-eqz v0, :cond_9

    .line 446
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 447
    iput-boolean v1, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a:Z

    .line 448
    iput-boolean v2, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->b:Z

    .line 449
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    :goto_2
    if-ge v3, v4, :cond_8

    .line 450
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->z:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcao;

    invoke-interface {v0}, Lcao;->a()V

    .line 449
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 452
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->m:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->D:Landroid/graphics/Paint;

    .line 453
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->l:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->E:Landroid/graphics/Paint;

    .line 454
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->s:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 455
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->t:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setPressed(Z)V

    .line 456
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->s:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 457
    iput-boolean v1, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->C:Z

    .line 458
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->y:Lalw;

    iget v0, v0, Lalw;->a:F

    sub-float v0, v5, v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->c:F

    move v0, v1

    .line 459
    goto/16 :goto_0

    .line 461
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 462
    iput-boolean v1, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->b:Z

    .line 463
    iput-boolean v2, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a:Z

    .line 464
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    :goto_3
    if-ge v3, v4, :cond_a

    .line 465
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->z:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcao;

    invoke-interface {v0}, Lcao;->b()V

    .line 464
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_3

    .line 467
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->l:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->D:Landroid/graphics/Paint;

    .line 468
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->m:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->E:Landroid/graphics/Paint;

    .line 469
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->s:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setPressed(Z)V

    .line 470
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->t:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    .line 471
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->t:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 472
    iput-boolean v2, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->C:Z

    .line 473
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->y:Lalw;

    iget v0, v0, Lalw;->b:F

    sub-float v0, v5, v0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->c:F

    move v0, v1

    .line 474
    goto/16 :goto_0

    .line 479
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->B:Lcap;

    if-eqz v0, :cond_b

    .line 480
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->B:Lcap;

    invoke-interface {v0, v3, v4}, Lcap;->b(II)V

    .line 482
    :cond_b
    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a(II)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    .line 483
    goto/16 :goto_0

    .line 487
    :pswitch_2
    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a(II)Z

    move-result v4

    .line 488
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->B:Lcap;

    if-eqz v0, :cond_c

    .line 489
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->B:Lcap;

    invoke-interface {v0}, Lcap;->a()V

    .line 491
    :cond_c
    iput-boolean v2, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a:Z

    .line 492
    iput-boolean v2, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->b:Z

    .line 493
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    move v3, v2

    :goto_4
    if-ge v3, v5, :cond_d

    .line 494
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->z:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcao;

    invoke-interface {v0}, Lcao;->c()V

    .line 493
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 496
    :cond_d
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->c:F

    .line 497
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->l:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->D:Landroid/graphics/Paint;

    .line 498
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->l:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->E:Landroid/graphics/Paint;

    .line 499
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->s:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setPressed(Z)V

    .line 500
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->t:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setPressed(Z)V

    .line 501
    if-eqz v4, :cond_3

    move v0, v1

    .line 502
    goto/16 :goto_0

    .line 437
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
