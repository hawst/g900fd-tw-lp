.class public Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;
.super Landroid/view/View;
.source "PG"


# static fields
.field public static final a:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field public static final b:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Paint;

.field private final e:Landroid/graphics/Matrix;

.field private final f:Landroid/graphics/Matrix;

.field private final g:Landroid/graphics/Matrix;

.field private final h:Landroid/graphics/Path;

.field private i:Landroid/graphics/Bitmap;

.field private j:Landroid/graphics/BitmapShader;

.field private k:F

.field private l:F

.field private m:F

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    new-instance v0, Lcaf;

    const-class v1, Ljava/lang/Float;

    const-string v2, "radius"

    invoke-direct {v0, v1, v2}, Lcaf;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->a:Landroid/util/Property;

    .line 54
    new-instance v0, Lcag;

    const-class v1, Ljava/lang/Float;

    const-string v2, "scale"

    invoke-direct {v0, v1, v2}, Lcag;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->b:Landroid/util/Property;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 84
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 89
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 92
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->c:Landroid/graphics/Paint;

    .line 68
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->d:Landroid/graphics/Paint;

    .line 69
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->e:Landroid/graphics/Matrix;

    .line 70
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->f:Landroid/graphics/Matrix;

    .line 71
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->g:Landroid/graphics/Matrix;

    .line 72
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->h:Landroid/graphics/Path;

    .line 78
    iput v1, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->k:F

    .line 80
    iput v1, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->m:F

    .line 93
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 94
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 95
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 97
    sget-object v0, Lahj;->a:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 100
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->a(I)V

    .line 102
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 103
    return-void
.end method

.method private a(II)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/high16 v5, 0x3f000000    # 0.5f

    .line 174
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->i:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    .line 194
    :goto_0
    return-void

    .line 177
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 178
    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->i:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    .line 184
    mul-int v2, v1, p2

    mul-int v4, p1, v3

    if-le v2, v4, :cond_1

    .line 185
    int-to-float v2, p2

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 186
    int-to-float v3, p1

    int-to-float v1, v1

    mul-float/2addr v1, v2

    sub-float v1, v3, v1

    mul-float/2addr v1, v5

    .line 191
    :goto_1
    iget-object v3, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->e:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 192
    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->e:Landroid/graphics/Matrix;

    add-float/2addr v1, v5

    float-to-int v1, v1

    int-to-float v1, v1

    add-float/2addr v0, v5

    float-to-int v0, v0

    int-to-float v0, v0

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 193
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->e()V

    goto :goto_0

    .line 188
    :cond_1
    int-to-float v2, p1

    int-to-float v1, v1

    div-float/2addr v2, v1

    .line 189
    int-to-float v1, p2

    int-to-float v3, v3

    mul-float/2addr v3, v2

    sub-float/2addr v1, v3

    mul-float/2addr v1, v5

    move v6, v1

    move v1, v0

    move v0, v6

    goto :goto_1
.end method

.method private e()V
    .locals 6

    .prologue
    const/high16 v5, 0x40000000    # 2.0f

    .line 197
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->f:Landroid/graphics/Matrix;

    iget v1, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->m:F

    iget v2, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->m:F

    .line 198
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v5

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v5

    .line 197
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 199
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->g:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->f:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->e:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setConcat(Landroid/graphics/Matrix;Landroid/graphics/Matrix;)Z

    .line 200
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->j:Landroid/graphics/BitmapShader;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->j:Landroid/graphics/BitmapShader;

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->g:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/BitmapShader;->setLocalMatrix(Landroid/graphics/Matrix;)V

    .line 203
    :cond_0
    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->m:F

    return v0
.end method

.method public a(F)V
    .locals 0

    .prologue
    .line 148
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->m:F

    .line 149
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->e()V

    .line 150
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->invalidate()V

    .line 151
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 134
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 120
    iput-object p1, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->i:Landroid/graphics/Bitmap;

    .line 121
    if-eqz p1, :cond_0

    .line 122
    new-instance v0, Landroid/graphics/BitmapShader;

    sget-object v1, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    sget-object v2, Landroid/graphics/Shader$TileMode;->CLAMP:Landroid/graphics/Shader$TileMode;

    invoke-direct {v0, p1, v1, v2}, Landroid/graphics/BitmapShader;-><init>(Landroid/graphics/Bitmap;Landroid/graphics/Shader$TileMode;Landroid/graphics/Shader$TileMode;)V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->j:Landroid/graphics/BitmapShader;

    .line 123
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->a(II)V

    .line 124
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->c:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->j:Landroid/graphics/BitmapShader;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    .line 126
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 113
    iput-boolean p1, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->n:Z

    .line 114
    return-void
.end method

.method public b()F
    .locals 1

    .prologue
    .line 221
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->k:F

    return v0
.end method

.method public b(F)F
    .locals 1

    .prologue
    .line 166
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->getWidth()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->getHeight()I

    move-result v0

    if-nez v0, :cond_1

    .line 167
    :cond_0
    const/4 v0, 0x0

    .line 170
    :goto_0
    return v0

    :cond_1
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->l:F

    div-float v0, p1, v0

    goto :goto_0
.end method

.method public c()F
    .locals 1

    .prologue
    .line 228
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->l:F

    return v0
.end method

.method public c(F)V
    .locals 1

    .prologue
    .line 212
    const-string v0, "radius"

    invoke-static {p1, v0}, Lcec;->b(FLjava/lang/CharSequence;)F

    .line 213
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->k:F

    .line 214
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->invalidate()V

    .line 215
    return-void
.end method

.method public d()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->i:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->i:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->d:Landroid/graphics/Paint;

    .line 244
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->n:Z

    if-eqz v1, :cond_1

    .line 245
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->h:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->reset()V

    .line 246
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->h:Landroid/graphics/Path;

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v4

    iget v4, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->l:F

    iget v5, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->k:F

    mul-float/2addr v4, v5

    sget-object v5, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Path;->addCircle(FFFLandroid/graphics/Path$Direction;)V

    .line 248
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->h:Landroid/graphics/Path;

    sget-object v2, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    .line 249
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawPaint(Landroid/graphics/Paint;)V

    .line 254
    :goto_1
    return-void

    .line 242
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->c:Landroid/graphics/Paint;

    goto :goto_0

    .line 251
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    iget v3, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->l:F

    iget v4, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->k:F

    mul-float/2addr v3, v4

    invoke-virtual {p1, v1, v2, v3, v0}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    goto :goto_1
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    .prologue
    .line 138
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/View;->onSizeChanged(IIII)V

    .line 139
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->a(II)V

    .line 140
    mul-int v0, p1, p1

    mul-int v1, p2, p2

    add-int/2addr v0, v1

    int-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->l:F

    .line 141
    return-void
.end method
