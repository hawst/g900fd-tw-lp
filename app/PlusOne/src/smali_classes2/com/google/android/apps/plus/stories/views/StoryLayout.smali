.class public Lcom/google/android/apps/plus/stories/views/StoryLayout;
.super Landroid/widget/AdapterView;
.source "PG"

# interfaces
.implements Lfrs;
.implements Lfsf;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Lfqu;",
        ">;",
        "Lfrs;",
        "Lfsf;"
    }
.end annotation


# instance fields
.field private A:Lfrw;

.field private B:Lfte;

.field private C:Lftd;

.field private D:Lftc;

.field private E:Z

.field private F:Z

.field private G:Lftk;

.field private final H:Lftb;

.field private I:Lftf;

.field private J:J

.field private final K:Ljava/lang/Runnable;

.field private final L:Ljava/lang/Runnable;

.field private a:[[Landroid/graphics/Point;

.field private final b:Lfta;

.field private c:Lfqu;

.field private d:Lfrj;

.field private e:Lfrb;

.field private f:Lfrq;

.field private g:I

.field private h:[I

.field private final i:Landroid/graphics/Point;

.field private j:I

.field private k:I

.field private l:I

.field private final m:I

.field private final n:I

.field private o:Landroid/view/VelocityTracker;

.field private p:[Landroid/graphics/Point;

.field private final q:Lnh;

.field private final r:Lnh;

.field private s:I

.field private t:I

.field private u:Z

.field private final v:Ljava/lang/Runnable;

.field private final w:Ljava/lang/Runnable;

.field private x:Landroid/graphics/Path;

.field private y:Landroid/graphics/Paint;

.field private z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lfqz;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 152
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 153
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 157
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 160
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 79
    new-instance v0, Lfta;

    invoke-direct {v0, p0}, Lfta;-><init>(Lcom/google/android/apps/plus/stories/views/StoryLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->b:Lfta;

    .line 87
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->g:I

    .line 92
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->i:Landroid/graphics/Point;

    .line 115
    new-instance v0, Lfsv;

    invoke-direct {v0, p0}, Lfsv;-><init>(Lcom/google/android/apps/plus/stories/views/StoryLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->v:Ljava/lang/Runnable;

    .line 120
    new-instance v0, Lfsw;

    invoke-direct {v0, p0}, Lfsw;-><init>(Lcom/google/android/apps/plus/stories/views/StoryLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->w:Ljava/lang/Runnable;

    .line 137
    iput-boolean v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->E:Z

    .line 138
    iput-boolean v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->F:Z

    .line 140
    new-instance v0, Lftb;

    invoke-direct {v0}, Lftb;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->H:Lftb;

    .line 142
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->J:J

    .line 144
    new-instance v0, Lfsx;

    invoke-direct {v0, p0}, Lfsx;-><init>(Lcom/google/android/apps/plus/stories/views/StoryLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->K:Ljava/lang/Runnable;

    .line 908
    new-instance v0, Lfsz;

    invoke-direct {v0, p0}, Lfsz;-><init>(Lcom/google/android/apps/plus/stories/views/StoryLayout;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->L:Ljava/lang/Runnable;

    .line 161
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 162
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->m:I

    .line 163
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->n:I

    .line 164
    new-instance v0, Lnh;

    invoke-direct {v0, p1}, Lnh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->q:Lnh;

    .line 165
    new-instance v0, Lnh;

    invoke-direct {v0, p1}, Lnh;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->r:Lnh;

    .line 166
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->x:Landroid/graphics/Path;

    .line 167
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->y:Landroid/graphics/Paint;

    .line 168
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->y:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 169
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->y:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 170
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    .line 171
    new-instance v0, Lfrw;

    invoke-direct {v0, p0}, Lfrw;-><init>(Lfsf;)V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->A:Lfrw;

    .line 173
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->setWillNotDraw(Z)V

    .line 174
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->setChildrenDrawingOrderEnabled(Z)V

    .line 175
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/stories/views/StoryLayout;I)I
    .locals 0

    .prologue
    .line 69
    iput p1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->g:I

    return p1
.end method

.method private a(II)Landroid/view/View;
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v4, -0x2

    const/4 v1, 0x0

    .line 368
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->d:Lfrj;

    iget-object v3, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    .line 369
    invoke-virtual {v3, p1}, Lfqu;->getItemViewType(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lfrj;->a(I)Landroid/view/View;

    move-result-object v2

    .line 368
    invoke-virtual {v0, p1, v2, p0}, Lfqu;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 370
    const v0, 0x7f10003f

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 371
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 372
    if-nez v0, :cond_0

    .line 373
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v4, v4}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 375
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {p0, v2, p2, v0, v3}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    .line 377
    invoke-static {v2}, Lfrc;->a(Landroid/view/View;)Lfrc;

    move-result-object v3

    .line 378
    invoke-virtual {v3}, Lfrc;->W()Lfrh;

    move-result-object v4

    .line 379
    invoke-virtual {v3}, Lfrc;->S()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 380
    invoke-virtual {v3}, Lfrc;->T()Lfrd;

    move-result-object v0

    iget v0, v0, Lfrd;->a:I

    :goto_0
    mul-int/lit8 v0, v0, 0x2

    .line 381
    invoke-virtual {v3}, Lfrc;->l()I

    move-result v5

    iget v6, v4, Lfrh;->b:I

    add-int/2addr v5, v6

    iget v6, v4, Lfrh;->d:I

    add-int/2addr v5, v6

    add-int/2addr v5, v0

    .line 383
    invoke-virtual {v3}, Lfrc;->n()I

    move-result v3

    iget v6, v4, Lfrh;->c:I

    add-int/2addr v3, v6

    iget v4, v4, Lfrh;->e:I

    add-int/2addr v3, v4

    add-int/2addr v0, v3

    .line 386
    invoke-static {v5, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 387
    invoke-static {v0, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 385
    invoke-virtual {v2, v3, v0}, Landroid/view/View;->measure(II)V

    .line 388
    invoke-static {v2, v1, v1}, Lfua;->a(Landroid/view/View;II)V

    .line 389
    return-object v2

    :cond_1
    move v0, v1

    .line 380
    goto :goto_0
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/stories/views/StoryLayout;)Lftb;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->H:Lftb;

    return-object v0
.end method

.method private a(Landroid/graphics/Canvas;Lnh;FFF)V
    .locals 3

    .prologue
    .line 554
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lnh;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 555
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    .line 556
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getScrollX()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getScrollY()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 557
    invoke-virtual {p1, p3}, Landroid/graphics/Canvas;->rotate(F)V

    .line 558
    invoke-virtual {p1, p4, p5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 559
    invoke-virtual {p2, p1}, Lnh;->a(Landroid/graphics/Canvas;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 560
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->invalidate()V

    .line 562
    :cond_0
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 564
    :cond_1
    return-void
.end method

.method private a(Landroid/view/View;I)V
    .locals 4

    .prologue
    .line 393
    .line 394
    invoke-static {p1}, Lfrc;->a(Landroid/view/View;)Lfrc;

    move-result-object v0

    .line 396
    invoke-virtual {v0}, Lfrc;->i()I

    move-result v1

    .line 397
    invoke-virtual {v0}, Lfrc;->j()I

    move-result v0

    .line 399
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v1

    .line 400
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v0

    invoke-virtual {p1, v1, v0, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 402
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->b(Landroid/view/View;I)V

    .line 403
    return-void
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 768
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->C:Lftd;

    if-eqz v0, :cond_0

    .line 770
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->L:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 771
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->C:Lftd;

    invoke-interface {v0, p1}, Lftd;->i(Z)V

    .line 773
    :cond_0
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/plus/stories/views/StoryLayout;Z)Z
    .locals 0

    .prologue
    .line 69
    iput-boolean p1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->u:Z

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/stories/views/StoryLayout;)Landroid/graphics/Path;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->x:Landroid/graphics/Path;

    return-object v0
.end method

.method private b(Landroid/view/View;I)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 407
    .line 408
    invoke-static {p1}, Lfrc;->a(Landroid/view/View;)Lfrc;

    move-result-object v0

    .line 410
    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v1}, Lfrq;->j()I

    move-result v1

    .line 411
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v2}, Lfrq;->k()I

    move-result v2

    .line 414
    invoke-virtual {v0}, Lfrc;->t()I

    move-result v0

    .line 415
    iget-object v3, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v3, v0}, Lfrq;->b(I)I

    move-result v3

    sub-int v1, v3, v1

    .line 418
    iget-object v3, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v3, v0}, Lfrq;->a(I)I

    move-result v0

    sub-int/2addr v0, v2

    .line 421
    neg-int v1, v1

    int-to-float v1, v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 422
    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 424
    invoke-direct {p0, p1, p2}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c(Landroid/view/View;I)V

    .line 425
    return-void
.end method

.method public static synthetic b(Lcom/google/android/apps/plus/stories/views/StoryLayout;Z)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(Z)V

    return-void
.end method

.method public static synthetic c(Lcom/google/android/apps/plus/stories/views/StoryLayout;)Lfrq;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    return-object v0
.end method

.method private c(Landroid/view/View;I)V
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 429
    invoke-virtual {p1}, Landroid/view/View;->getX()F

    move-result v0

    float-to-int v1, v0

    .line 430
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int v5, v1, v0

    .line 431
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v0}, Lfrq;->j()I

    move-result v6

    .line 433
    instance-of v0, p1, Lfua;

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Lfua;

    .line 434
    invoke-virtual {v0}, Lfua;->b()Z

    move-result v0

    if-nez v0, :cond_1

    move v4, v2

    .line 435
    :goto_0
    if-lt v5, v6, :cond_2

    add-int v0, v6, p2

    if-gt v1, v0, :cond_2

    move v1, v2

    .line 436
    :goto_1
    const v0, 0x7f100040

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 437
    if-eqz v1, :cond_3

    if-nez v4, :cond_3

    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Boolean;

    .line 438
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    .line 439
    :cond_0
    :goto_2
    if-eqz v2, :cond_4

    :goto_3
    invoke-virtual {p1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 440
    return-void

    :cond_1
    move v4, v3

    .line 434
    goto :goto_0

    :cond_2
    move v1, v3

    .line 435
    goto :goto_1

    :cond_3
    move v2, v3

    .line 438
    goto :goto_2

    .line 439
    :cond_4
    const/4 v3, 0x4

    goto :goto_3
.end method

.method public static synthetic d(Lcom/google/android/apps/plus/stories/views/StoryLayout;)Landroid/graphics/Paint;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->y:Landroid/graphics/Paint;

    return-object v0
.end method

.method private d(Landroid/view/View;I)V
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 452
    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->removeViewInLayout(Landroid/view/View;)V

    .line 454
    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 455
    invoke-virtual {p1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 456
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 457
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->d:Lfrj;

    invoke-virtual {v0, p1, p2}, Lfrj;->a(Landroid/view/View;I)V

    .line 458
    return-void
.end method

.method private h()V
    .locals 3

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->A:Lfrw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a:[[Landroid/graphics/Point;

    if-eqz v0, :cond_0

    .line 294
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->A:Lfrw;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a:[[Landroid/graphics/Point;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lfrw;->a([[Landroid/graphics/Point;I)V

    .line 295
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(Lfrt;)V

    .line 297
    :cond_0
    return-void
.end method

.method private i()V
    .locals 1

    .prologue
    .line 599
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->u:Z

    .line 600
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->v:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 601
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 444
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getWidth()I

    move-result v1

    .line 445
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 446
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2, v1}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c(Landroid/view/View;I)V

    .line 445
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 448
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 4

    .prologue
    .line 917
    const/4 v0, 0x0

    .line 918
    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->A:Lfrw;

    if-eqz v1, :cond_0

    .line 919
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->A:Lfrw;

    invoke-virtual {v0, p1}, Lfrw;->a(I)Z

    move-result v0

    .line 921
    :cond_0
    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->p:[Landroid/graphics/Point;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ne v1, p1, :cond_1

    .line 922
    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->D:Lftc;

    invoke-interface {v1, v0}, Lftc;->c(Z)V

    .line 924
    :cond_1
    if-eqz p1, :cond_2

    .line 927
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->L:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 928
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->L:Ljava/lang/Runnable;

    const-wide/16 v2, 0xbb8

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 930
    :cond_2
    return-void
.end method

.method public a(Lfqu;)V
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    if-eqz v0, :cond_0

    .line 225
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->b:Lfta;

    invoke-virtual {v0, v1}, Lfqu;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 227
    :cond_0
    iput-object p1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    .line 228
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    if-eqz v0, :cond_1

    .line 229
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->b:Lfta;

    invoke-virtual {v0, v1}, Lfqu;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 230
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    invoke-virtual {v0}, Lfqu;->a()Lfrj;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->d:Lfrj;

    .line 231
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->A:Lfrw;

    invoke-virtual {v0, v1}, Lfqu;->a(Lfrw;)V

    .line 233
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->removeAllViewsInLayout()V

    .line 234
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->requestLayout()V

    .line 235
    return-void
.end method

.method public a(Lfrb;)V
    .locals 8

    .prologue
    const/4 v7, -0x1

    .line 238
    iput-object p1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->e:Lfrb;

    .line 239
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->e:Lfrb;

    invoke-virtual {v0}, Lfrb;->k()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->setBackgroundColor(I)V

    .line 241
    invoke-virtual {p1}, Lfrb;->c()[[Landroid/graphics/Point;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a:[[Landroid/graphics/Point;

    .line 242
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a:[[Landroid/graphics/Point;

    .line 243
    invoke-virtual {p1}, Lfrb;->e()I

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->p:[Landroid/graphics/Point;

    .line 245
    invoke-virtual {p1}, Lfrb;->d()[[F

    move-result-object v2

    .line 246
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->e:Lfrb;

    invoke-virtual {v0}, Lfrb;->f()[[Lhng;

    move-result-object v4

    .line 248
    new-instance v0, Lfrq;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a:[[Landroid/graphics/Point;

    .line 249
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getHeight()I

    move-result v6

    invoke-direct/range {v0 .. v6}, Lfrq;-><init>(Landroid/content/Context;[[F[[Landroid/graphics/Point;[[Lhng;II)V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    .line 250
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v0, p0}, Lfrq;->a(Lfrs;)V

    .line 253
    new-instance v0, Lftf;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getWidth()I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40a00000    # 5.0f

    div-float/2addr v2, v3

    float-to-int v2, v2

    invoke-direct {v0, p0, v1, v2}, Lftf;-><init>(Lcom/google/android/apps/plus/stories/views/StoryLayout;Lfrq;I)V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->I:Lftf;

    .line 254
    iget-wide v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->J:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->I:Lftf;

    iget-wide v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->J:J

    invoke-virtual {v0, v2, v3}, Lftf;->a(J)V

    .line 260
    :cond_0
    invoke-direct {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->h()V

    .line 262
    invoke-virtual {p1}, Lfrb;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 263
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->y:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->e:Lfrb;

    invoke-virtual {v1}, Lfrb;->h()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 267
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->z:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->z:Ljava/util/List;

    .line 268
    invoke-virtual {v0, v1}, Lfqu;->a(Ljava/util/List;)I

    move-result v0

    move v1, v0

    .line 269
    :goto_0
    if-eq v1, v7, :cond_4

    .line 270
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v0, v1}, Lfrq;->d(I)V

    .line 274
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 275
    const/4 v0, 0x0

    :goto_1
    if-gt v0, v1, :cond_3

    .line 276
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 275
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move v1, v7

    .line 268
    goto :goto_0

    .line 278
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->A:Lfrw;

    invoke-virtual {v0, v2}, Lfrw;->a(Ljava/util/List;)V

    .line 281
    :cond_4
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->e:Lfrb;

    invoke-virtual {v0}, Lfrb;->g()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 282
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->y:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->e:Lfrb;

    invoke-virtual {v1}, Lfrb;->i()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 285
    :cond_5
    new-instance v0, Lftk;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    .line 286
    invoke-virtual {v1}, Lfrq;->j()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getHeight()I

    move-result v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lftk;-><init>(Lcom/google/android/apps/plus/stories/views/StoryLayout;Landroid/content/Context;III)V

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->G:Lftk;

    .line 287
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->G:Lftk;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->e:Lfrb;

    invoke-virtual {v1}, Lfrb;->j()[Lhng;

    move-result-object v1

    invoke-virtual {v0, v1}, Lftk;->a([Lhng;)V

    .line 289
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->computeScroll()V

    .line 290
    return-void
.end method

.method public a(Lfrt;)V
    .locals 1

    .prologue
    .line 941
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->A:Lfrw;

    if-eqz v0, :cond_0

    .line 942
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->A:Lfrw;

    invoke-virtual {v0, p1}, Lfrw;->a(Lfrt;)V

    .line 944
    :cond_0
    return-void
.end method

.method public a(Lftc;)V
    .locals 0

    .prologue
    .line 308
    iput-object p1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->D:Lftc;

    .line 309
    return-void
.end method

.method public a(Lftd;)V
    .locals 0

    .prologue
    .line 304
    iput-object p1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->C:Lftd;

    .line 305
    return-void
.end method

.method public a(Lfte;)V
    .locals 0

    .prologue
    .line 300
    iput-object p1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->B:Lfte;

    .line 301
    return-void
.end method

.method protected addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z
    .locals 1

    .prologue
    .line 357
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->h:[I

    .line 358
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/AdapterView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->G:Lftk;

    if-eqz v0, :cond_0

    .line 213
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->G:Lftk;

    invoke-virtual {v0}, Lftk;->a()V

    .line 215
    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 934
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->p:[Landroid/graphics/Point;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ne v0, p1, :cond_0

    .line 935
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->D:Lftc;

    invoke-interface {v0}, Lftc;->d()V

    .line 937
    :cond_0
    return-void
.end method

.method public c()Lfqu;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    return-object v0
.end method

.method public computeScroll()V
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 786
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    if-nez v0, :cond_1

    .line 836
    :cond_0
    return-void

    .line 790
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v0}, Lfrq;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 793
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->postInvalidate()V

    .line 796
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getChildCount()I

    move-result v3

    .line 797
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    if-nez v0, :cond_6

    move v0, v2

    .line 798
    :goto_0
    if-eqz v0, :cond_0

    if-eqz v3, :cond_0

    .line 802
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getMeasuredWidth()I

    move-result v6

    .line 803
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v0}, Lfrq;->j()I

    move-result v3

    .line 804
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getScrollX()I

    move-result v0

    if-le v3, v0, :cond_7

    move v0, v1

    .line 806
    :goto_1
    iget-object v4, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v4}, Lfrq;->k()I

    move-result v4

    invoke-virtual {p0, v3, v4}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->scrollTo(II)V

    .line 808
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getScrollX()I

    move-result v4

    .line 821
    iget-object v3, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v3}, Lfrq;->b()Z

    move-result v3

    if-nez v3, :cond_3

    if-eqz v0, :cond_d

    iget v3, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->s:I

    add-int v5, v4, v6

    sub-int/2addr v3, v5

    if-gt v3, v6, :cond_d

    .line 823
    :cond_3
    mul-int/lit8 v3, v6, 0x3

    add-int/2addr v3, v4

    iput v3, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->s:I

    move v3, v1

    .line 826
    :goto_2
    iget-object v5, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v5}, Lfrq;->b()Z

    move-result v5

    if-nez v5, :cond_4

    if-nez v0, :cond_5

    iget v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->t:I

    sub-int v0, v4, v0

    if-gt v0, v6, :cond_5

    .line 828
    :cond_4
    mul-int/lit8 v0, v6, 0x2

    sub-int v0, v4, v0

    iput v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->t:I

    move v3, v1

    .line 832
    :cond_5
    if-eqz v3, :cond_b

    .line 833
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getChildCount()I

    move-result v4

    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    if-nez v0, :cond_8

    move v0, v2

    :goto_3
    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v7

    add-int/2addr v3, v7

    iget v7, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->t:I

    if-gt v3, v7, :cond_9

    move-object v2, v5

    :goto_4
    if-le v4, v1, :cond_a

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getX()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v7

    add-int/2addr v5, v7

    iget v7, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->t:I

    if-gt v5, v7, :cond_a

    iget-object v5, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    iget v7, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->g:I

    add-int/lit8 v8, v7, 0x1

    iput v8, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->g:I

    invoke-virtual {v5, v7}, Lfqu;->getItemViewType(I)I

    move-result v5

    invoke-direct {p0, v2, v5}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->d(Landroid/view/View;I)V

    add-int/lit8 v4, v4, -0x1

    move-object v2, v3

    goto :goto_4

    .line 797
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    invoke-virtual {v0}, Lfqu;->getCount()I

    move-result v0

    goto/16 :goto_0

    :cond_7
    move v0, v2

    .line 804
    goto/16 :goto_1

    .line 833
    :cond_8
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    invoke-virtual {v0}, Lfqu;->getCount()I

    move-result v0

    goto :goto_3

    :cond_9
    :goto_5
    iget v5, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->g:I

    if-lez v5, :cond_a

    iget v5, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->t:I

    if-le v3, v5, :cond_a

    iget v3, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->g:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->g:I

    invoke-direct {p0, v3, v2}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(II)Landroid/view/View;

    move-result-object v3

    invoke-direct {p0, v3, v6}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(Landroid/view/View;I)V

    invoke-virtual {v3}, Landroid/view/View;->getX()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v3, v5

    add-int/lit8 v4, v4, 0x1

    goto :goto_5

    :cond_a
    add-int/lit8 v2, v4, -0x1

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getX()F

    move-result v2

    float-to-int v2, v2

    iget v5, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->s:I

    if-lt v2, v5, :cond_c

    move-object v0, v3

    :goto_6
    if-le v4, v1, :cond_b

    add-int/lit8 v2, v4, -0x2

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->getX()F

    move-result v3

    float-to-int v3, v3

    iget v5, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->s:I

    if-lt v3, v5, :cond_b

    iget-object v3, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    iget v5, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->g:I

    add-int/2addr v5, v4

    add-int/lit8 v5, v5, -0x1

    invoke-virtual {v3, v5}, Lfqu;->getItemViewType(I)I

    move-result v3

    invoke-direct {p0, v0, v3}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->d(Landroid/view/View;I)V

    add-int/lit8 v4, v4, -0x1

    move-object v0, v2

    goto :goto_6

    :goto_7
    iget v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->g:I

    add-int/2addr v2, v4

    if-ge v2, v0, :cond_b

    iget v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->s:I

    if-ge v1, v2, :cond_b

    iget v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->g:I

    add-int/2addr v1, v4

    invoke-direct {p0, v1, v4}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(II)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1, v6}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(Landroid/view/View;I)V

    invoke-virtual {v1}, Landroid/view/View;->getX()F

    move-result v1

    float-to-int v1, v1

    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 835
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_8
    if-ltz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1, v6}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->b(Landroid/view/View;I)V

    add-int/lit8 v0, v0, -0x1

    goto :goto_8

    :cond_c
    move v1, v2

    goto :goto_7

    :cond_d
    move v3, v2

    goto/16 :goto_2
.end method

.method public d()Lftb;
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->H:Lftb;

    return-object v0
.end method

.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 609
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 610
    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->H:Lftb;

    iget-boolean v1, v1, Lftb;->a:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->H:Lftb;

    iget-boolean v1, v1, Lftb;->b:Z

    if-eqz v1, :cond_0

    .line 611
    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->w:Ljava/lang/Runnable;

    invoke-virtual {p0, v1}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->post(Ljava/lang/Runnable;)Z

    .line 613
    :cond_0
    return v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 546
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->draw(Landroid/graphics/Canvas;)V

    .line 548
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->q:Lnh;

    const/high16 v3, 0x43870000    # 270.0f

    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getHeight()I

    move-result v0

    neg-int v0, v0

    int-to-float v4, v0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(Landroid/graphics/Canvas;Lnh;FFF)V

    .line 549
    iget-object v3, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->r:Lnh;

    const/high16 v4, 0x42b40000    # 90.0f

    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getWidth()I

    move-result v0

    neg-int v0, v0

    int-to-float v6, v0

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(Landroid/graphics/Canvas;Lnh;FFF)V

    .line 550
    return-void
.end method

.method public e()V
    .locals 2

    .prologue
    .line 779
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lfrq;->a(Z)V

    .line 780
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->I:Lftf;

    invoke-virtual {v0}, Lftf;->b()V

    .line 781
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->invalidate()V

    .line 782
    return-void
.end method

.method public f()D
    .locals 4

    .prologue
    .line 948
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v0}, Lfrq;->l()I

    move-result v0

    int-to-double v0, v0

    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v2}, Lfrq;->o()I

    move-result v2

    int-to-double v2, v2

    div-double/2addr v0, v2

    return-wide v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 1461
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v0}, Lfrq;->p()I

    move-result v0

    return v0
.end method

.method public synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c()Lfqu;

    move-result-object v0

    return-object v0
.end method

.method protected getChildDrawingOrder(II)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 508
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->h:[I

    if-nez v0, :cond_1

    .line 509
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getChildCount()I

    move-result v3

    new-instance v4, Ljava/util/TreeSet;

    new-instance v0, Lfsy;

    invoke-direct {v0}, Lfsy;-><init>()V

    invoke-direct {v4, v0}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    new-instance v5, Ljava/util/TreeMap;

    invoke-direct {v5}, Ljava/util/TreeMap;-><init>()V

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lfrc;->a(Landroid/view/View;)Lfrc;

    move-result-object v0

    invoke-virtual {v0}, Lfrc;->t()I

    move-result v6

    if-ltz v6, :cond_2

    invoke-virtual {v0}, Lfrc;->t()I

    move-result v0

    :goto_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/SortedSet;->addAll(Ljava/util/Collection;)Z

    new-array v0, v3, [I

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->h:[I

    invoke-interface {v4}, Ljava/util/SortedSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget-object v4, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->h:[I

    add-int/lit8 v2, v1, 0x1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v4, v1

    move v1, v2

    goto :goto_2

    .line 511
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->h:[I

    aget v0, v0, p2

    return v0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 1

    .prologue
    .line 313
    const/4 v0, 0x0

    return-object v0
.end method

.method public onAttachedToWindow()V
    .locals 2

    .prologue
    .line 179
    invoke-super {p0}, Landroid/widget/AdapterView;->onAttachedToWindow()V

    .line 180
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->A:Lfrw;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->A:Lfrw;

    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v0, v1}, Lfrw;->a(Landroid/os/Handler;)V

    .line 183
    :cond_0
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 568
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->G:Lftk;

    if-eqz v0, :cond_0

    .line 569
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->G:Lftk;

    invoke-virtual {v0, p1}, Lftk;->a(Landroid/graphics/Canvas;)V

    .line 572
    :cond_0
    sget-object v0, Lfqm;->a:Lloz;

    .line 573
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 618
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getChildCount()I

    move-result v2

    if-nez v2, :cond_1

    .line 670
    :cond_0
    :goto_0
    return v0

    .line 622
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 624
    :pswitch_0
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->i:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Point;->set(II)V

    .line 625
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->i:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iput v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->j:I

    .line 626
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v2}, Lfrq;->l()I

    move-result v2

    iput v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->k:I

    .line 627
    iput v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->l:I

    .line 628
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->H:Lftb;

    iput-boolean v0, v2, Lftb;->a:Z

    iput-boolean v0, v2, Lftb;->b:Z

    .line 629
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->v:Ljava/lang/Runnable;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v3

    int-to-long v4, v3

    invoke-virtual {p0, v2, v4, v5}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 630
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->o:Landroid/view/VelocityTracker;

    if-nez v2, :cond_2

    .line 631
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->o:Landroid/view/VelocityTracker;

    .line 635
    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->o:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 636
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v2}, Lfrq;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 637
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v0}, Lfrq;->m()V

    .line 638
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v0}, Lfrq;->e()V

    move v0, v1

    .line 639
    goto :goto_0

    .line 633
    :cond_2
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->o:Landroid/view/VelocityTracker;

    invoke-virtual {v2}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_1

    .line 643
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->o:Landroid/view/VelocityTracker;

    invoke-virtual {v2, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 644
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->i:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Point;->set(II)V

    .line 645
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v2}, Lfrq;->f()Z

    move-result v2

    if-nez v2, :cond_0

    .line 646
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->j:I

    int-to-float v3, v3

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v3, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->m:I

    int-to-float v3, v3

    cmpl-float v2, v2, v3

    if-lez v2, :cond_0

    .line 650
    iput-object v5, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->z:Ljava/util/List;

    .line 651
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v0}, Lfrq;->e()V

    .line 652
    invoke-direct {p0, v1}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(Z)V

    .line 653
    invoke-direct {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->i()V

    move v0, v1

    .line 654
    goto/16 :goto_0

    .line 658
    :pswitch_2
    iget-boolean v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->u:Z

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v2}, Lfrq;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 659
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->H:Lftb;

    iput-boolean v1, v2, Lftb;->b:Z

    .line 663
    :cond_3
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->i()V

    .line 664
    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->o:Landroid/view/VelocityTracker;

    if-eqz v1, :cond_0

    .line 665
    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->o:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->recycle()V

    .line 666
    iput-object v5, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->o:Landroid/view/VelocityTracker;

    goto/16 :goto_0

    .line 622
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    .prologue
    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 462
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    if-nez v0, :cond_1

    move v0, v1

    .line 463
    :goto_0
    if-nez v0, :cond_2

    .line 504
    :cond_0
    :goto_1
    return-void

    .line 462
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    invoke-virtual {v0}, Lfqu;->getCount()I

    move-result v0

    goto :goto_0

    .line 467
    :cond_2
    sub-int v3, p5, p3

    .line 468
    sub-int v4, p4, p2

    .line 470
    iget v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->g:I

    if-ne v0, v2, :cond_4

    .line 471
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->z:Ljava/util/List;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->z:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    iget-object v5, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->z:Ljava/util/List;

    .line 472
    invoke-virtual {v0, v5}, Lfqu;->b(Ljava/util/List;)I

    move-result v0

    .line 473
    :goto_2
    if-ne v0, v2, :cond_3

    move v0, v1

    :cond_3
    iput v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->g:I

    .line 474
    iget v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->g:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(II)Landroid/view/View;

    move-result-object v0

    .line 475
    invoke-direct {p0, v0, v4}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(Landroid/view/View;I)V

    .line 476
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->computeScroll()V

    .line 479
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_3
    if-ltz v0, :cond_6

    .line 480
    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2, v4}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(Landroid/view/View;I)V

    .line 479
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    :cond_5
    move v0, v2

    .line 472
    goto :goto_2

    .line 483
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->q:Lnh;

    invoke-virtual {v0, v3, v4}, Lnh;->a(II)V

    .line 484
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->r:Lnh;

    invoke-virtual {v0, v3, v4}, Lnh;->a(II)V

    .line 486
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v0}, Lfrq;->p()I

    move-result v0

    if-nez v0, :cond_7

    .line 489
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->I:Lftf;

    invoke-virtual {v0}, Lftf;->a()V

    .line 492
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->E:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v0}, Lfrq;->p()I

    move-result v0

    if-nez v0, :cond_8

    .line 494
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->K:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->post(Ljava/lang/Runnable;)Z

    .line 496
    :cond_8
    iput-boolean v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->E:Z

    .line 498
    iget-boolean v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->F:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 501
    new-instance v0, Lkoe;

    const/4 v2, 0x6

    invoke-direct {v0, v2}, Lkoe;-><init>(I)V

    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Lkoe;->a(Landroid/content/Context;)V

    .line 502
    iput-boolean v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->F:Z

    goto/16 :goto_1
.end method

.method protected onMeasure(II)V
    .locals 5

    .prologue
    .line 323
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 324
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 325
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->e:Lfrb;

    if-eqz v2, :cond_0

    .line 326
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->e:Lfrb;

    invoke-virtual {v2}, Lfrb;->b()F

    move-result v2

    .line 329
    const/4 v3, 0x0

    cmpl-float v3, v2, v3

    if-lez v3, :cond_0

    .line 330
    int-to-float v3, v0

    mul-float/2addr v3, v2

    int-to-float v4, v1

    cmpg-float v3, v3, v4

    if-gez v3, :cond_2

    .line 331
    int-to-float v1, v0

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 337
    :cond_0
    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->setMeasuredDimension(II)V

    .line 339
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    if-eqz v2, :cond_1

    .line 340
    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v2, v1, v0}, Lfrq;->a(II)V

    .line 342
    :cond_1
    return-void

    .line 332
    :cond_2
    int-to-float v3, v1

    div-float/2addr v3, v2

    int-to-float v4, v0

    cmpg-float v3, v3, v4

    if-gez v3, :cond_0

    .line 333
    int-to-float v0, v1

    div-float/2addr v0, v2

    float-to-int v0, v0

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 204
    check-cast p1, Lfti;

    .line 205
    invoke-virtual {p1}, Lfti;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/AdapterView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 206
    iget-object v0, p1, Lfti;->b:Ljava/util/List;

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->z:Ljava/util/List;

    .line 207
    iget-boolean v0, p1, Lfti;->a:Z

    iput-boolean v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->E:Z

    .line 208
    iget-wide v0, p1, Lfti;->c:J

    iput-wide v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->J:J

    .line 209
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 187
    invoke-super {p0}, Landroid/widget/AdapterView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 188
    new-instance v2, Lfti;

    invoke-direct {v2, v0}, Lfti;-><init>(Landroid/os/Parcelable;)V

    .line 189
    iget-boolean v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->E:Z

    iput-boolean v0, v2, Lfti;->a:Z

    .line 192
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->z:Ljava/util/List;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->z:Ljava/util/List;

    .line 194
    :goto_0
    iput-object v0, v2, Lfti;->b:Ljava/util/List;

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->I:Lftf;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->I:Lftf;

    .line 198
    invoke-virtual {v0}, Lftf;->c()J

    move-result-wide v0

    :goto_1
    iput-wide v0, v2, Lfti;->c:J

    .line 199
    return-object v2

    .line 193
    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c:Lfqu;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    .line 195
    invoke-virtual {v1}, Lfrq;->p()I

    move-result v1

    .line 194
    invoke-virtual {v0, v1}, Lfqu;->b(I)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 198
    :cond_2
    const-wide/16 v0, -0x1

    goto :goto_1
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    .prologue
    .line 348
    invoke-direct {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->h()V

    .line 349
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->B:Lfte;

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->B:Lfte;

    invoke-interface {v0, p0}, Lfte;->c(Landroid/view/View;)V

    .line 352
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 675
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getChildCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 758
    :goto_0
    return v2

    .line 678
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_1
    move v2, v9

    .line 758
    goto :goto_0

    .line 680
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->i:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v0, v1, v3}, Landroid/graphics/Point;->set(II)V

    .line 681
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->i:Landroid/graphics/Point;

    iget v0, v0, Landroid/graphics/Point;->x:I

    iput v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->j:I

    .line 682
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v0}, Lfrq;->l()I

    move-result v0

    iput v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->k:I

    .line 683
    iput v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->l:I

    .line 684
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->v:Ljava/lang/Runnable;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 685
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v0}, Lfrq;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 686
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v0}, Lfrq;->m()V

    .line 687
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v0}, Lfrq;->e()V

    .line 689
    :cond_2
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->o:Landroid/view/VelocityTracker;

    if-nez v0, :cond_3

    .line 690
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->o:Landroid/view/VelocityTracker;

    .line 694
    :goto_2
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->o:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    goto :goto_1

    .line 692
    :cond_3
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->o:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto :goto_2

    .line 697
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->o:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 698
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    iget v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->j:I

    sub-int/2addr v0, v1

    .line 699
    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v1}, Lfrq;->f()Z

    move-result v1

    if-nez v1, :cond_4

    .line 700
    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v1

    iget v3, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->m:I

    if-le v1, v3, :cond_4

    .line 704
    iput-object v10, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->z:Ljava/util/List;

    .line 705
    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v1}, Lfrq;->e()V

    .line 706
    invoke-direct {p0, v9}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(Z)V

    .line 707
    invoke-direct {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->i()V

    .line 709
    :cond_4
    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v1}, Lfrq;->f()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 710
    iget v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->k:I

    sub-int v0, v1, v0

    .line 711
    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v1}, Lfrq;->o()I

    move-result v1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 712
    iget-object v3, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v3}, Lfrq;->l()I

    move-result v3

    if-eq v1, v3, :cond_5

    .line 713
    iget-object v3, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v3, v1}, Lfrq;->c(I)V

    .line 714
    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->I:Lftf;

    invoke-virtual {v1}, Lftf;->b()V

    .line 715
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->computeScroll()V

    .line 717
    :cond_5
    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v1}, Lfrq;->l()I

    move-result v1

    if-eq v1, v0, :cond_8

    .line 718
    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v1}, Lfrq;->l()I

    move-result v1

    sub-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->l:I

    .line 719
    iget v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->l:I

    if-gez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->q:Lnh;

    .line 720
    :goto_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->i:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    sub-float/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {v0, v1}, Lnh;->a(F)Z

    .line 724
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->invalidate()V

    .line 726
    :cond_6
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->i:Landroid/graphics/Point;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto/16 :goto_1

    .line 719
    :cond_7
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->r:Lnh;

    goto :goto_3

    .line 722
    :cond_8
    iput v2, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->l:I

    goto :goto_4

    .line 729
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->u:Z

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v0}, Lfrq;->b()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 730
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->performClick()Z

    .line 750
    :cond_9
    :goto_5
    :pswitch_3
    invoke-direct {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->i()V

    .line 751
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->o:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    .line 752
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->o:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 753
    iput-object v10, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->o:Landroid/view/VelocityTracker;

    goto/16 :goto_1

    .line 731
    :cond_a
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v0}, Lfrq;->f()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 732
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->o:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    iget v3, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->n:I

    int-to-float v3, v3

    invoke-virtual {v0, v1, v3}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 733
    iget v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->l:I

    if-eqz v0, :cond_d

    .line 734
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    iget v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->l:I

    if-gez v1, :cond_b

    move v2, v9

    :cond_b
    invoke-virtual {v0, v2}, Lfrq;->b(Z)V

    .line 735
    iget v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->l:I

    if-gez v0, :cond_c

    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->q:Lnh;

    .line 736
    :goto_6
    invoke-virtual {v0}, Lnh;->c()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 737
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->invalidate()V

    goto :goto_5

    .line 735
    :cond_c
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->r:Lnh;

    goto :goto_6

    .line 739
    :cond_d
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->o:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_e

    .line 740
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    iget-object v1, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v1}, Lfrq;->l()I

    move-result v1

    iget-object v3, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->o:Landroid/view/VelocityTracker;

    .line 741
    invoke-virtual {v3}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v3

    neg-float v3, v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    .line 742
    invoke-virtual {v4}, Lfrq;->o()I

    move-result v6

    move v4, v2

    move v5, v2

    move v7, v2

    move v8, v2

    .line 740
    invoke-virtual/range {v0 .. v8}, Lfrq;->a(IIIIIIII)V

    .line 743
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->invalidate()V

    goto :goto_5

    .line 745
    :cond_e
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->f:Lfrq;

    invoke-virtual {v0}, Lfrq;->a()V

    goto :goto_5

    .line 678
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public performClick()Z
    .locals 1

    .prologue
    .line 763
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(Z)V

    .line 764
    invoke-super {p0}, Landroid/widget/AdapterView;->performClick()Z

    move-result v0

    return v0
.end method

.method public removeViewInLayout(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 363
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryLayout;->h:[I

    .line 364
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->removeViewInLayout(Landroid/view/View;)V

    .line 365
    return-void
.end method

.method public synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0

    .prologue
    .line 69
    check-cast p1, Lfqu;

    invoke-virtual {p0, p1}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->a(Lfqu;)V

    return-void
.end method

.method public setSelection(I)V
    .locals 2

    .prologue
    .line 318
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
