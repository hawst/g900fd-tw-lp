.class public Lcom/google/android/apps/moviemaker/filterpacks/image/SaliencyFilter;
.super Laak;
.source "PG"


# static fields
.field private static final INPUT_PORT:Ljava/lang/String; = "input"

.field private static final MAP_OUTPUT_PORT:Ljava/lang/String; = "saliencyMap"

.field private static final OUTPUT_PORT:Ljava/lang/String; = "saliency"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    const-string v0, "moviemaker-jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 82
    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 26
    return-void
.end method

.method private static native getSaliencyScore(Ljava/nio/ByteBuffer;IILjava/nio/ByteBuffer;)F
.end method


# virtual methods
.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v1, 0x64

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 30
    invoke-static {v1, v4}, Labf;->a(II)Labf;

    move-result-object v0

    .line 31
    invoke-static {v1}, Labf;->b(I)Labf;

    move-result-object v1

    .line 33
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "input"

    .line 34
    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "saliency"

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    .line 35
    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v5, v3}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "saliencyMap"

    .line 36
    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 37
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 9

    .prologue
    const/4 v2, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 42
    const-string v0, "input"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/image/SaliencyFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    .line 43
    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v3

    .line 44
    invoke-virtual {v3}, Laap;->d()Laar;

    move-result-object v0

    invoke-virtual {v0, v8}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 45
    invoke-virtual {v3}, Laap;->i()[I

    move-result-object v5

    .line 46
    const-string v0, "saliencyMap"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/image/SaliencyFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v6

    .line 49
    if-eqz v6, :cond_2

    .line 50
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    .line 51
    invoke-virtual {v6, v0}, Lacv;->a([I)Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->d()Laar;

    move-result-object v0

    .line 52
    invoke-virtual {v0, v2}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 54
    :goto_0
    const/4 v7, 0x0

    aget v7, v5, v7

    aget v5, v5, v8

    invoke-static {v4, v7, v5, v2}, Lcom/google/android/apps/moviemaker/filterpacks/image/SaliencyFilter;->getSaliencyScore(Ljava/nio/ByteBuffer;IILjava/nio/ByteBuffer;)F

    move-result v2

    .line 55
    invoke-virtual {v3}, Laap;->h()V

    .line 56
    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {v0}, Laar;->h()V

    .line 59
    :cond_0
    const-string v3, "saliency"

    invoke-virtual {p0, v3}, Lcom/google/android/apps/moviemaker/filterpacks/image/SaliencyFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v3

    .line 60
    invoke-virtual {v3, v1}, Lacv;->a([I)Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->a()Labg;

    move-result-object v1

    .line 61
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Labg;->a(Ljava/lang/Object;)V

    .line 62
    invoke-virtual {v3, v1}, Lacv;->a(Laap;)V

    .line 63
    if-eqz v6, :cond_1

    .line 64
    invoke-virtual {v6, v0}, Lacv;->a(Laap;)V

    .line 66
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    move-object v2, v1

    goto :goto_0

    .line 50
    :array_0
    .array-data 4
        0x40
        0x40
    .end array-data
.end method
