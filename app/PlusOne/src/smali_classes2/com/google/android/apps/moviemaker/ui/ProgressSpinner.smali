.class public Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;
.super Landroid/widget/ProgressBar;
.source "PG"


# static fields
.field private static a:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private b:F

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    new-instance v0, Lcav;

    const-class v1, Ljava/lang/Float;

    const-string v2, "showingness"

    invoke-direct {v0, v1, v2}, Lcav;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->a:Landroid/util/Property;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;)V

    .line 50
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->c()V

    .line 51
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->c()V

    .line 56
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ProgressBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 60
    invoke-direct {p0}, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->c()V

    .line 61
    return-void
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;)F
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->b:F

    return v0
.end method

.method public static synthetic a(Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;I)I
    .locals 0

    .prologue
    .line 26
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->c:I

    return p1
.end method

.method public static synthetic b(Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;)I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->c:I

    return v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 64
    new-instance v0, Lcax;

    invoke-direct {v0, p0}, Lcax;-><init>(Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;)V

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->setIndeterminateDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 65
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 74
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->setVisibility(I)V

    .line 75
    sget-object v0, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->a:Landroid/util/Property;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 76
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c000d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 77
    new-instance v1, Lcaw;

    invoke-direct {v1, p0}, Lcaw;-><init>(Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 83
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 84
    return-void

    .line 75
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public a(F)V
    .locals 0

    .prologue
    .line 101
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->b:F

    .line 102
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->invalidate()V

    .line 103
    return-void
.end method

.method public b()F
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->b:F

    return v0
.end method
