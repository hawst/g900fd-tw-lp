.class public Lcom/google/android/apps/moviemaker/filterpacks/image/ExtractHueFilter;
.super Laak;
.source "PG"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-string v0, "moviemaker-jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 56
    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 21
    return-void
.end method

.method private native extractHue(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)F
.end method


# virtual methods
.method public c()Lacx;
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 25
    const/16 v0, 0x12d

    const/4 v1, 0x1

    invoke-static {v0, v1}, Labf;->a(II)Labf;

    move-result-object v0

    .line 26
    const/16 v1, 0x64

    invoke-static {v1}, Labf;->b(I)Labf;

    move-result-object v1

    .line 28
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    .line 29
    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "hues"

    .line 30
    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 31
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 36
    const-string v0, "image"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/image/ExtractHueFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v0

    .line 37
    const-string v1, "hues"

    invoke-virtual {p0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/image/ExtractHueFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    new-array v2, v6, [I

    const/4 v3, 0x0

    .line 38
    invoke-virtual {v0}, Laas;->j()I

    move-result v4

    aput v4, v2, v3

    invoke-virtual {v0}, Laas;->k()I

    move-result v3

    aput v3, v2, v5

    invoke-virtual {v1, v2}, Lacv;->a([I)Laap;

    move-result-object v1

    .line 39
    invoke-virtual {v1}, Laap;->d()Laar;

    move-result-object v1

    .line 41
    invoke-virtual {v0, v5}, Laas;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 42
    invoke-virtual {v1, v6}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 44
    invoke-direct {p0, v2, v3}, Lcom/google/android/apps/moviemaker/filterpacks/image/ExtractHueFilter;->extractHue(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)F

    .line 46
    invoke-virtual {v0}, Laas;->h()V

    .line 47
    invoke-virtual {v1}, Laar;->h()V

    .line 49
    const-string v0, "hues"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/image/ExtractHueFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    .line 50
    return-void
.end method
