.class public Lcom/google/android/apps/moviemaker/filterpacks/numeric/MapProjectionFilter;
.super Laak;
.source "PG"


# static fields
.field private static final BATCH_SIZE_INPUT_PORT:Ljava/lang/String; = "batchSize"

.field private static final DROP_RATE_INPUT_PORT:Ljava/lang/String; = "dropRate"

.field private static final FRAME_NUMBER_INPUT_PORT:Ljava/lang/String; = "frameNumber"

.field private static final HORIZONTAL_PROJECTION_OUTPUT_PORT:Ljava/lang/String; = "horizontalProjection"

.field private static final MAP_INPUT_PORT:Ljava/lang/String; = "map"

.field private static final TAG:Ljava/lang/String;

.field private static final VERTICAL_PROJECTION_OUTPUT_PORT:Ljava/lang/String; = "verticalProjection"


# instance fields
.field private mBatchSize:I

.field private mDropRate:F

.field private mFrameNumber:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/MapProjectionFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/MapProjectionFilter;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 44
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/MapProjectionFilter;->mBatchSize:I

    .line 49
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/MapProjectionFilter;->mDropRate:F

    .line 54
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/MapProjectionFilter;->mFrameNumber:I

    .line 58
    return-void
.end method

.method private a([JJ)V
    .locals 4

    .prologue
    .line 177
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 178
    aget-wide v2, p1, v0

    div-long/2addr v2, p2

    aput-wide v2, p1, v0

    .line 177
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 180
    :cond_0
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 82
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "batchSize"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const-string v0, "mBatchSize"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p1, v3}, Lacp;->a(Z)V

    .line 85
    invoke-virtual {p1, v2}, Lacp;->b(Z)V

    .line 87
    :cond_0
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "dropRate"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 88
    const-string v0, "mDropRate"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 89
    invoke-virtual {p1, v3}, Lacp;->a(Z)V

    .line 90
    invoke-virtual {p1, v2}, Lacp;->b(Z)V

    .line 92
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "frameNumber"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 93
    const-string v0, "mFrameNumber"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p1, v3}, Lacp;->a(Z)V

    .line 95
    invoke-virtual {p1, v2}, Lacp;->b(Z)V

    .line 97
    :cond_2
    return-void
.end method

.method public c()Lacx;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 62
    const/16 v0, 0x64

    invoke-static {v0}, Labf;->b(I)Labf;

    move-result-object v0

    .line 63
    const-class v1, Ljava/lang/Integer;

    invoke-static {v1}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v1

    .line 64
    const-class v2, Ljava/lang/Long;

    invoke-static {v2}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v2

    .line 66
    new-instance v3, Lacx;

    invoke-direct {v3}, Lacx;-><init>()V

    const-string v4, "map"

    const/4 v5, 0x2

    .line 67
    invoke-virtual {v3, v4, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v3, "batchSize"

    .line 68
    invoke-virtual {v0, v3, v6, v1}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "dropRate"

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    .line 70
    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    .line 69
    invoke-virtual {v0, v1, v6, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "frameNumber"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    .line 72
    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    .line 71
    invoke-virtual {v0, v1, v6, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "verticalProjection"

    .line 73
    invoke-virtual {v0, v1, v6, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "horizontalProjection"

    .line 75
    invoke-virtual {v0, v1, v6, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 77
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 22

    .prologue
    .line 101
    const-string v2, "map"

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/MapProjectionFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v2

    .line 102
    invoke-virtual {v2}, Lacp;->c()Laap;

    move-result-object v11

    .line 103
    move-object/from16 v0, p0

    iget v2, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/MapProjectionFilter;->mFrameNumber:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/MapProjectionFilter;->mDropRate:F

    invoke-static {v2, v3}, Lbbq;->a(IF)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    invoke-virtual {v11}, Laap;->d()Laar;

    move-result-object v2

    .line 108
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v12

    .line 109
    invoke-virtual {v2}, Laar;->i()[I

    move-result-object v13

    .line 111
    const-string v2, "verticalProjection"

    .line 112
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/MapProjectionFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v14

    .line 113
    const-string v2, "horizontalProjection"

    .line 114
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/MapProjectionFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v15

    .line 116
    if-eqz v14, :cond_6

    const/4 v2, 0x1

    move v10, v2

    .line 117
    :goto_1
    if-eqz v15, :cond_7

    const/4 v2, 0x1

    .line 118
    :goto_2
    const/4 v6, 0x0

    .line 119
    const/4 v4, 0x0

    .line 121
    const/4 v5, 0x0

    .line 123
    const/4 v3, 0x0

    .line 125
    move-object/from16 v0, p0

    iget v7, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/MapProjectionFilter;->mBatchSize:I

    .line 126
    const/4 v8, 0x0

    aget v8, v13, v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/MapProjectionFilter;->mBatchSize:I

    rem-int/2addr v8, v9

    if-nez v8, :cond_2

    const/4 v8, 0x1

    aget v8, v13, v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/MapProjectionFilter;->mBatchSize:I

    rem-int/2addr v8, v9

    if-eqz v8, :cond_d

    .line 129
    :cond_2
    sget-object v7, Lcom/google/android/apps/moviemaker/filterpacks/numeric/MapProjectionFilter;->TAG:Ljava/lang/String;

    .line 131
    const/4 v7, 0x1

    move v9, v7

    .line 134
    :goto_3
    if-eqz v10, :cond_c

    .line 135
    const/4 v5, 0x0

    .line 136
    invoke-virtual {v14, v5}, Lacv;->a([I)Laap;

    move-result-object v5

    invoke-virtual {v5}, Laap;->a()Labg;

    move-result-object v6

    .line 137
    const/4 v5, 0x0

    aget v5, v13, v5

    div-int/2addr v5, v9

    new-array v5, v5, [J

    move-object v7, v5

    move-object v8, v6

    .line 139
    :goto_4
    if-eqz v2, :cond_b

    .line 140
    const/4 v3, 0x0

    .line 141
    invoke-virtual {v15, v3}, Lacv;->a([I)Laap;

    move-result-object v3

    invoke-virtual {v3}, Laap;->a()Labg;

    move-result-object v4

    .line 142
    const/4 v3, 0x1

    aget v3, v13, v3

    div-int/2addr v3, v9

    new-array v3, v3, [J

    move-object v5, v3

    move-object v6, v4

    .line 144
    :goto_5
    if-nez v10, :cond_3

    if-eqz v2, :cond_9

    .line 145
    :cond_3
    const/4 v3, 0x0

    :goto_6
    const/4 v4, 0x1

    aget v4, v13, v4

    if-ge v3, v4, :cond_9

    .line 146
    const/4 v4, 0x0

    :goto_7
    const/16 v16, 0x0

    aget v16, v13, v16

    move/from16 v0, v16

    if-ge v4, v0, :cond_8

    .line 149
    const/16 v16, 0x0

    aget v16, v13, v16

    mul-int v16, v16, v3

    add-int v16, v16, v4

    move/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/nio/ByteBuffer;->get(I)B

    move-result v16

    move/from16 v0, v16

    and-int/lit16 v0, v0, 0xff

    move/from16 v16, v0

    .line 150
    if-eqz v10, :cond_4

    .line 151
    div-int v17, v4, v9

    aget-wide v18, v7, v17

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v20, v0

    add-long v18, v18, v20

    aput-wide v18, v7, v17

    .line 153
    :cond_4
    if-eqz v2, :cond_5

    .line 154
    div-int v17, v3, v9

    aget-wide v18, v5, v17

    move/from16 v0, v16

    int-to-long v0, v0

    move-wide/from16 v20, v0

    add-long v18, v18, v20

    aput-wide v18, v5, v17

    .line 146
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 116
    :cond_6
    const/4 v2, 0x0

    move v10, v2

    goto/16 :goto_1

    .line 117
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 145
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 161
    :cond_9
    mul-int v3, v9, v9

    .line 162
    if-eqz v10, :cond_a

    .line 163
    int-to-long v12, v3

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v12, v13}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/MapProjectionFilter;->a([JJ)V

    .line 164
    iget-object v4, v11, Laap;->a:Lzp;

    invoke-virtual {v4}, Lzp;->g()J

    move-result-wide v12

    invoke-virtual {v8, v12, v13}, Labg;->a(J)V

    .line 165
    invoke-virtual {v8, v7}, Labg;->a(Ljava/lang/Object;)V

    .line 166
    invoke-virtual {v14, v8}, Lacv;->a(Laap;)V

    .line 168
    :cond_a
    if-eqz v2, :cond_0

    .line 169
    int-to-long v2, v3

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v2, v3}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/MapProjectionFilter;->a([JJ)V

    .line 170
    iget-object v2, v11, Laap;->a:Lzp;

    invoke-virtual {v2}, Lzp;->g()J

    move-result-wide v2

    invoke-virtual {v6, v2, v3}, Labg;->a(J)V

    .line 171
    invoke-virtual {v6, v5}, Labg;->a(Ljava/lang/Object;)V

    .line 172
    invoke-virtual {v15, v6}, Lacv;->a(Laap;)V

    goto/16 :goto_0

    :cond_b
    move-object v5, v3

    move-object v6, v4

    goto/16 :goto_5

    :cond_c
    move-object v7, v5

    move-object v8, v6

    goto/16 :goto_4

    :cond_d
    move v9, v7

    goto/16 :goto_3
.end method
