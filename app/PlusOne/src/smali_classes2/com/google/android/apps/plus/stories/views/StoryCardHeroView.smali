.class public Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lljh;


# instance fields
.field private a:Landroid/view/View;

.field private b:Lcom/google/android/libraries/social/media/ui/MediaView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040204

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->a:Landroid/view/View;

    .line 43
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->addView(Landroid/view/View;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->a:Landroid/view/View;

    const v1, 0x7f1005d6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    iput-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 45
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->c()V

    .line 92
    return-void
.end method

.method public a(Lfnz;)V
    .locals 4

    .prologue
    .line 52
    invoke-virtual {p1}, Lfnz;->b()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1}, Lfnz;->b()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Ljac;->a:Ljac;

    invoke-static {v0, v1, v2}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    .line 54
    :goto_0
    invoke-virtual {p1}, Lfnz;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lfnz;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lfnz;->f()I

    move-result v3

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->a(Lizu;Ljava/lang/String;Ljava/lang/String;I)V

    .line 55
    return-void

    .line 53
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lizu;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 60
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->i(I)V

    .line 61
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->h(Z)V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0028

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 65
    if-eqz p4, :cond_0

    .line 67
    :goto_0
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    shl-int/lit8 v0, v0, 0x18

    add-int/2addr v0, p4

    invoke-direct {v1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 68
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->b:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->f(Landroid/graphics/drawable/Drawable;)V

    .line 71
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->a:Landroid/view/View;

    const v1, 0x7f1005d7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 72
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->a:Landroid/view/View;

    const v1, 0x7f1005d8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 76
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 77
    return-void

    .line 66
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/plus/stories/views/StoryCardHeroView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b00c9

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result p4

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 81
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 82
    if-nez p2, :cond_0

    int-to-float v0, v1

    const v2, 0x3f2aaaab

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 84
    :goto_0
    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 85
    invoke-static {v0, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 86
    invoke-super {p0, v1, v0}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 87
    return-void

    .line 83
    :cond_0
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    goto :goto_0
.end method
