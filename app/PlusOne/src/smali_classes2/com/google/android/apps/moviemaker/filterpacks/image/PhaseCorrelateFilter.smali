.class public Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;
.super Laak;
.source "PG"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mMotionEstimation:[F

.field private mPreviousFrame:Laar;

.field private mTimer:Lcgw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->TAG:Ljava/lang/String;

    .line 28
    const-string v0, "moviemaker-jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 31
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->mMotionEstimation:[F

    .line 37
    return-void
.end method

.method private a(Ljava/lang/String;F)V
    .locals 3

    .prologue
    .line 100
    invoke-virtual {p0, p1}, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 101
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lacv;->a([I)Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->a()Labg;

    move-result-object v1

    .line 102
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Labg;->a(Ljava/lang/Object;)V

    .line 103
    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    .line 104
    return-void
.end method

.method private native phaseCorrelate(ILjava/nio/ByteBuffer;Ljava/nio/ByteBuffer;[F)V
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 0

    .prologue
    .line 53
    return-void
.end method

.method public c()Lacx;
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 41
    const/16 v0, 0x64

    invoke-static {v0}, Labf;->b(I)Labf;

    move-result-object v0

    .line 42
    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v1}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v1

    .line 44
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    .line 45
    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "horizontalPanDirection"

    .line 46
    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "verticalPanDirection"

    .line 47
    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    .line 48
    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 57
    new-instance v0, Lcgw;

    sget-object v1, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->TAG:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcgw;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->mTimer:Lcgw;

    .line 58
    return-void
.end method

.method protected i()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 67
    const-string v0, "image"

    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->d()Laar;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->mPreviousFrame:Laar;

    if-eqz v1, :cond_0

    .line 70
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->mTimer:Lcgw;

    const-string v2, "onProcess"

    invoke-virtual {v1, v2}, Lcgw;->a(Ljava/lang/String;)V

    .line 71
    invoke-virtual {v0, v5}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 72
    iget-object v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->mTimer:Lcgw;

    const-string v3, "lock current image"

    invoke-virtual {v2, v3}, Lcgw;->b(Ljava/lang/String;)V

    .line 73
    iget-object v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->mPreviousFrame:Laar;

    invoke-virtual {v2, v5}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 74
    iget-object v3, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->mTimer:Lcgw;

    const-string v4, "lock previous image"

    invoke-virtual {v3, v4}, Lcgw;->b(Ljava/lang/String;)V

    .line 76
    invoke-virtual {v0}, Laar;->j()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->mMotionEstimation:[F

    invoke-direct {p0, v3, v2, v1, v4}, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->phaseCorrelate(ILjava/nio/ByteBuffer;Ljava/nio/ByteBuffer;[F)V

    .line 78
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->mTimer:Lcgw;

    const-string v2, "phase correlate in jni"

    invoke-virtual {v1, v2}, Lcgw;->b(Ljava/lang/String;)V

    .line 80
    invoke-virtual {v0}, Laar;->h()V

    .line 81
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->mTimer:Lcgw;

    const-string v2, "unlock current image"

    invoke-virtual {v1, v2}, Lcgw;->b(Ljava/lang/String;)V

    .line 82
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->mPreviousFrame:Laar;

    invoke-virtual {v1}, Laar;->h()V

    .line 83
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->mTimer:Lcgw;

    const-string v2, "unlock previous image"

    invoke-virtual {v1, v2}, Lcgw;->b(Ljava/lang/String;)V

    .line 84
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->mTimer:Lcgw;

    invoke-virtual {v1, v5}, Lcgw;->a(Z)V

    .line 85
    iget-object v1, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->mPreviousFrame:Laar;

    invoke-virtual {v1}, Laar;->f()Laap;

    .line 87
    const-string v1, "horizontalPanDirection"

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->mMotionEstimation:[F

    const/4 v3, 0x0

    aget v2, v2, v3

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->a(Ljava/lang/String;F)V

    .line 88
    const-string v1, "verticalPanDirection"

    iget-object v2, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->mMotionEstimation:[F

    aget v2, v2, v5

    invoke-direct {p0, v1, v2}, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->a(Ljava/lang/String;F)V

    .line 91
    :cond_0
    iput-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->mPreviousFrame:Laar;

    .line 92
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->mPreviousFrame:Laar;

    invoke-virtual {v0}, Laar;->g()Laap;

    .line 93
    return-void
.end method

.method protected k()V
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/image/PhaseCorrelateFilter;->mTimer:Lcgw;

    invoke-virtual {v0}, Lcgw;->b()V

    .line 63
    return-void
.end method
