.class public Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;
.super Landroid/widget/RelativeLayout;
.source "PG"


# instance fields
.field private a:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 17
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 21
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->a:F

    return v0
.end method

.method public a(F)V
    .locals 0

    .prologue
    .line 35
    iput p1, p0, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->a:F

    .line 36
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->requestLayout()V

    .line 37
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    .line 50
    iget v0, p0, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->a:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 51
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 90
    :goto_0
    return-void

    .line 55
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->getPaddingRight()I

    move-result v1

    add-int v2, v0, v1

    .line 56
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->getPaddingTop()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->getPaddingBottom()I

    move-result v1

    add-int v3, v0, v1

    .line 58
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 59
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 60
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v4

    .line 61
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    .line 62
    if-nez v4, :cond_2

    .line 63
    iget v1, p0, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->a:F

    int-to-float v4, v0

    mul-float/2addr v1, v4

    float-to-int v1, v1

    .line 68
    :cond_1
    :goto_1
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    sub-int/2addr v4, v2

    .line 69
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    sub-int/2addr v5, v3

    .line 71
    int-to-float v6, v4

    int-to-float v7, v5

    div-float/2addr v6, v7

    .line 73
    iget v7, p0, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->a:F

    cmpl-float v6, v7, v6

    if-lez v6, :cond_3

    .line 74
    int-to-float v0, v5

    int-to-float v2, v4

    iget v4, p0, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->a:F

    div-float/2addr v2, v4

    sub-float/2addr v0, v2

    float-to-int v0, v0

    .line 77
    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    sub-int v0, v5, v0

    add-int/2addr v0, v3

    .line 78
    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 76
    invoke-super {p0, v1, v0}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    goto :goto_0

    .line 64
    :cond_2
    if-nez v5, :cond_1

    .line 65
    int-to-float v0, v1

    iget v4, p0, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->a:F

    div-float/2addr v0, v4

    float-to-int v0, v0

    goto :goto_1

    .line 83
    :cond_3
    int-to-float v1, v4

    int-to-float v3, v5

    iget v5, p0, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->a:F

    mul-float/2addr v3, v5

    sub-float/2addr v1, v3

    float-to-int v1, v1

    .line 85
    sub-int v1, v4, v1

    add-int/2addr v1, v2

    .line 86
    invoke-static {v1, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 88
    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 85
    invoke-super {p0, v1, v0}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    goto :goto_0
.end method
