.class public abstract Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter;
.super Laak;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Laak;"
    }
.end annotation


# instance fields
.field private mListener:Lbbp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbbp",
            "<TT;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 17
    return-void
.end method


# virtual methods
.method protected a(JLjava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JTT;)V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-virtual {p0}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter;->mListener:Lbbp;

    invoke-interface {v0, p1, p2, p3}, Lbbp;->a(JLjava/lang/Object;)V

    .line 27
    :cond_0
    return-void
.end method

.method public a(Lbbp;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbbp",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 20
    iput-object p1, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter;->mListener:Lbbp;

    .line 21
    return-void
.end method

.method protected a()Z
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter;->mListener:Lbbp;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
