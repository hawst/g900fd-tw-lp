.class public final Lcom/google/android/gms/location/places/PlaceType;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lgrv;

.field public static final a:Lcom/google/android/gms/location/places/PlaceType;

.field public static final b:Lcom/google/android/gms/location/places/PlaceType;

.field public static final c:Lcom/google/android/gms/location/places/PlaceType;

.field public static final d:Lcom/google/android/gms/location/places/PlaceType;

.field public static final e:Lcom/google/android/gms/location/places/PlaceType;

.field public static final f:Lcom/google/android/gms/location/places/PlaceType;

.field public static final g:Lcom/google/android/gms/location/places/PlaceType;

.field public static final h:Lcom/google/android/gms/location/places/PlaceType;

.field public static final i:Lcom/google/android/gms/location/places/PlaceType;

.field public static final j:Lcom/google/android/gms/location/places/PlaceType;

.field public static final k:Lcom/google/android/gms/location/places/PlaceType;

.field public static final l:Lcom/google/android/gms/location/places/PlaceType;

.field public static final m:Lcom/google/android/gms/location/places/PlaceType;

.field public static final n:Lcom/google/android/gms/location/places/PlaceType;

.field public static final o:Lcom/google/android/gms/location/places/PlaceType;

.field public static final p:Lcom/google/android/gms/location/places/PlaceType;

.field public static final q:Lcom/google/android/gms/location/places/PlaceType;

.field public static final r:Lcom/google/android/gms/location/places/PlaceType;


# instance fields
.field public final s:I

.field public final t:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "accounting"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "airport"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "amusement_park"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "aquarium"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "art_gallery"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "atm"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "bakery"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "bank"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "bar"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "beauty_salon"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "bicycle_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "book_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "bowling_alley"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "bus_station"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "cafe"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "campground"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "car_dealer"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "car_rental"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "car_repair"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "car_wash"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "casino"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "cemetery"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "church"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "city_hall"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "clothing_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "convenience_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "courthouse"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "dentist"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "department_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "doctor"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "electrician"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "electronics_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "embassy"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "establishment"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "finance"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "fire_station"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "florist"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "food"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "funeral_home"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "furniture_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "gas_station"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "general_contractor"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "grocery_or_supermarket"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "gym"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "hair_care"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "hardware_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "health"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "hindu_temple"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "home_goods_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "hospital"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "insurance_agency"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "jewelry_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "laundry"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "lawyer"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "library"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "liquor_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "local_government_office"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "locksmith"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "lodging"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "meal_delivery"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "meal_takeaway"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "mosque"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "movie_rental"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "movie_theater"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "moving_company"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "museum"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "night_club"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "painter"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "park"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "parking"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "pet_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "pharmacy"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "physiotherapist"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "place_of_worship"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "plumber"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "police"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "post_office"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "real_estate_agency"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "restaurant"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "roofing_contractor"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "rv_park"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "school"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "shoe_store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "shopping_mall"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "spa"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "stadium"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "storage"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "store"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "subway_station"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "synagogue"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "taxi_stand"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "train_station"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "travel_agency"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "university"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "veterinary_care"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "zoo"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "administrative_area_level_1"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->a:Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "administrative_area_level_2"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->b:Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "administrative_area_level_3"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->c:Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "colloquial_area"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->d:Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "country"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "floor"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "geocode"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->e:Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "intersection"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "locality"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->f:Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "natural_feature"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "neighborhood"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->g:Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "political"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "point_of_interest"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "post_box"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "postal_code"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->h:Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "postal_code_prefix"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->i:Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "postal_town"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "premise"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->j:Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "room"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "route"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "street_address"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->k:Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "sublocality"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->l:Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "sublocality_level_1"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->m:Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "sublocality_level_2"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->n:Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "sublocality_level_3"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->o:Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "sublocality_level_4"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->p:Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "sublocality_level_5"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->q:Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "subpremise"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "transit_station"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    const-string v0, "other"

    invoke-static {v0}, Lcom/google/android/gms/location/places/PlaceType;->a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->r:Lcom/google/android/gms/location/places/PlaceType;

    new-instance v0, Lgrv;

    invoke-direct {v0}, Lgrv;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/places/PlaceType;->CREATOR:Lgrv;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lgih;->a(Ljava/lang/String;)Ljava/lang/String;

    iput p1, p0, Lcom/google/android/gms/location/places/PlaceType;->s:I

    iput-object p2, p0, Lcom/google/android/gms/location/places/PlaceType;->t:Ljava/lang/String;

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/location/places/PlaceType;
    .locals 2

    new-instance v0, Lcom/google/android/gms/location/places/PlaceType;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/location/places/PlaceType;-><init>(ILjava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/PlaceType;->t:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/location/places/PlaceType;->CREATOR:Lgrv;

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    instance-of v0, p1, Lcom/google/android/gms/location/places/PlaceType;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/location/places/PlaceType;->t:Ljava/lang/String;

    check-cast p1, Lcom/google/android/gms/location/places/PlaceType;

    iget-object v1, p1, Lcom/google/android/gms/location/places/PlaceType;->t:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/PlaceType;->t:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/places/PlaceType;->t:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/location/places/PlaceType;->CREATOR:Lgrv;

    invoke-static {p0, p1}, Lgrv;->a(Lcom/google/android/gms/location/places/PlaceType;Landroid/os/Parcel;)V

    return-void
.end method
