.class public Lcom/google/android/libraries/photoeditor/filterparameters/AutoEnhanceFilterParameter;
.super Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
.source "PG"


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# static fields
.field private static g:[I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/AutoEnhanceFilterParameter;->g:[I

    .line 26
    const/16 v0, 0x12

    const-class v1, Lcom/google/android/libraries/photoeditor/filterparameters/AutoEnhanceFilterParameter;

    invoke-static {v0, v1}, Lham;->a(ILjava/lang/Class;)V

    .line 27
    return-void

    .line 16
    :array_0
    .array-data 4
        0xc
        0xc9
        0xca
        0x1e0
        0x1e1
        0x1e2
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 54
    sget-object v0, Lhan;->a:Lhao;

    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/AutoEnhanceFilterParameter;->getFilterType()I

    move-result v1

    invoke-interface {v0, v1, p1}, Lhao;->a(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    .line 43
    :sswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 47
    :sswitch_1
    new-array v0, v0, [B

    goto :goto_0

    .line 51
    :sswitch_2
    const-string v0, ""

    goto :goto_0

    .line 41
    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_0
        0xc9 -> :sswitch_1
        0xca -> :sswitch_1
        0x1e0 -> :sswitch_2
        0x1e1 -> :sswitch_2
    .end sparse-switch
.end method

.method public a()[I
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/AutoEnhanceFilterParameter;->g:[I

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 98
    const/16 v0, 0xc

    return v0
.end method

.method public b(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 60
    sparse-switch p1, :sswitch_data_0

    .line 73
    sget-object v0, Lhan;->a:Lhao;

    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/AutoEnhanceFilterParameter;->getFilterType()I

    move-result v1

    invoke-interface {v0, v1, p1}, Lhao;->b(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    .line 62
    :sswitch_0
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 66
    :sswitch_1
    const/4 v0, 0x0

    new-array v0, v0, [B

    goto :goto_0

    .line 70
    :sswitch_2
    const-string v0, ""

    goto :goto_0

    .line 60
    nop

    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_0
        0xc9 -> :sswitch_1
        0xca -> :sswitch_1
        0x1e0 -> :sswitch_2
        0x1e1 -> :sswitch_2
    .end sparse-switch
.end method

.method public c(I)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 79
    sparse-switch p1, :sswitch_data_0

    .line 92
    sget-object v0, Lhan;->a:Lhao;

    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/AutoEnhanceFilterParameter;->getFilterType()I

    move-result v1

    invoke-interface {v0, v1, p1}, Lhao;->c(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    .line 81
    :sswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 85
    :sswitch_1
    new-array v0, v0, [B

    goto :goto_0

    .line 89
    :sswitch_2
    const-string v0, ""

    goto :goto_0

    .line 79
    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_0
        0xc9 -> :sswitch_1
        0xca -> :sswitch_1
        0x1e0 -> :sswitch_2
        0x1e1 -> :sswitch_2
    .end sparse-switch
.end method

.method public getFilterType()I
    .locals 1

    .prologue
    .line 31
    const/16 v0, 0x12

    return v0
.end method
