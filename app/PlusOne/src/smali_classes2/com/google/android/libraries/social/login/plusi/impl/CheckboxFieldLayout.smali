.class public Lcom/google/android/libraries/social/login/plusi/impl/CheckboxFieldLayout;
.super Lixo;
.source "PG"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private c:Landroid/widget/CheckBox;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lixo;-><init>(Landroid/content/Context;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lixo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lixo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method


# virtual methods
.method public a(Lnqa;ILiyf;)V
    .locals 3

    .prologue
    .line 59
    invoke-super {p0, p1, p2, p3}, Lixo;->a(Lnqa;ILiyf;)V

    .line 60
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/CheckboxFieldLayout;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/CheckboxFieldLayout;->c:Landroid/widget/CheckBox;

    .line 62
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/CheckboxFieldLayout;->c()Lnqa;

    move-result-object v0

    iget-object v0, v0, Lnqa;->c:Lnqe;

    .line 63
    iget-object v1, p0, Lcom/google/android/libraries/social/login/plusi/impl/CheckboxFieldLayout;->c:Landroid/widget/CheckBox;

    iget-object v2, v0, Lnqe;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 64
    iget-object v1, p0, Lcom/google/android/libraries/social/login/plusi/impl/CheckboxFieldLayout;->c:Landroid/widget/CheckBox;

    iget-object v0, v0, Lnqe;->e:Lnqh;

    invoke-static {v1, v0}, Liyc;->a(Landroid/widget/TextView;Lnqh;)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/CheckboxFieldLayout;->g()Ljava/lang/Boolean;

    move-result-object v0

    .line 67
    if-eqz v0, :cond_0

    .line 68
    iget-object v1, p0, Lcom/google/android/libraries/social/login/plusi/impl/CheckboxFieldLayout;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/CheckboxFieldLayout;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 72
    return-void
.end method

.method public k()Lnqe;
    .locals 3

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/CheckboxFieldLayout;->c()Lnqa;

    move-result-object v0

    iget-object v0, v0, Lnqa;->c:Lnqe;

    invoke-static {v0}, Liyi;->a(Lnqe;)Lnqe;

    move-result-object v0

    .line 90
    new-instance v1, Lnqc;

    invoke-direct {v1}, Lnqc;-><init>()V

    iput-object v1, v0, Lnqe;->f:Lnqc;

    .line 91
    iget-object v1, v0, Lnqe;->f:Lnqc;

    iget-object v2, p0, Lcom/google/android/libraries/social/login/plusi/impl/CheckboxFieldLayout;->c:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lnqc;->b:Ljava/lang/Boolean;

    .line 92
    return-object v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/CheckboxFieldLayout;->a:Lnqa;

    iget-object v0, v0, Lnqa;->c:Lnqe;

    iget-object v0, v0, Lnqe;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/CheckboxFieldLayout;->a:Lnqa;

    iget-object v0, v0, Lnqa;->c:Lnqe;

    iget-object v0, v0, Lnqe;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/CheckboxFieldLayout;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/CheckboxFieldLayout;->b:Liyf;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/CheckboxFieldLayout;->b:Liyf;

    invoke-virtual {v0}, Liyf;->V()V

    .line 82
    :cond_0
    return-void
.end method
