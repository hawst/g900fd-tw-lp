.class public Lcom/google/android/libraries/social/login/plusi/impl/TextInputFieldLayout;
.super Lixo;
.source "PG"


# instance fields
.field private c:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lixo;-><init>(Landroid/content/Context;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lixo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Lixo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    return-void
.end method


# virtual methods
.method public a(Lnqa;ILiyf;)V
    .locals 4

    .prologue
    .line 57
    invoke-super {p0, p1, p2, p3}, Lixo;->a(Lnqa;ILiyf;)V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/TextInputFieldLayout;->c()Lnqa;

    move-result-object v0

    iget-object v1, v0, Lnqa;->c:Lnqe;

    .line 61
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/TextInputFieldLayout;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/TextInputFieldLayout;->c:Landroid/widget/TextView;

    .line 62
    iget-object v0, v1, Lnqe;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/TextInputFieldLayout;->c:Landroid/widget/TextView;

    iget-object v2, v1, Lnqe;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/TextInputFieldLayout;->c:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/TextInputFieldLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0148

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setHintTextColor(I)V

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/TextInputFieldLayout;->c:Landroid/widget/TextView;

    iget-object v1, v1, Lnqe;->e:Lnqh;

    invoke-static {v0, v1}, Liyc;->a(Landroid/widget/TextView;Lnqh;)V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/TextInputFieldLayout;->e()Ljava/lang/String;

    move-result-object v0

    .line 69
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 70
    iget-object v1, p0, Lcom/google/android/libraries/social/login/plusi/impl/TextInputFieldLayout;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/TextInputFieldLayout;->c:Landroid/widget/TextView;

    new-instance v1, Lizc;

    invoke-direct {v1, p0}, Lizc;-><init>(Lcom/google/android/libraries/social/login/plusi/impl/TextInputFieldLayout;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 98
    return-void
.end method

.method public k()Lnqe;
    .locals 3

    .prologue
    .line 113
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/TextInputFieldLayout;->c()Lnqa;

    move-result-object v0

    iget-object v0, v0, Lnqa;->c:Lnqe;

    invoke-static {v0}, Liyi;->a(Lnqe;)Lnqe;

    move-result-object v0

    .line 114
    new-instance v1, Lnqc;

    invoke-direct {v1}, Lnqc;-><init>()V

    iput-object v1, v0, Lnqe;->f:Lnqc;

    .line 115
    iget-object v1, v0, Lnqe;->f:Lnqc;

    iget-object v2, p0, Lcom/google/android/libraries/social/login/plusi/impl/TextInputFieldLayout;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lnqc;->a:Ljava/lang/String;

    .line 116
    return-object v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/TextInputFieldLayout;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/TextInputFieldLayout;->c:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
