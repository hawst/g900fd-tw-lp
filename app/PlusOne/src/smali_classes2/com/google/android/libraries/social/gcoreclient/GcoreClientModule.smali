.class public Lcom/google/android/libraries/social/gcoreclient/GcoreClientModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 108
    const-class v0, Lihf;

    if-ne p2, v0, :cond_1

    .line 109
    const-class v0, Lihf;

    new-instance v1, Lihh;

    invoke-direct {v1}, Lihh;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 110
    :cond_1
    const-class v0, Lilb;

    if-ne p2, v0, :cond_2

    .line 111
    const-class v0, Lilb;

    .line 112
    invoke-static {p1}, Lilc;->a(Landroid/content/Context;)Lilb;

    move-result-object v1

    .line 111
    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 113
    :cond_2
    const-class v0, Lija;

    if-ne p2, v0, :cond_3

    .line 114
    const-class v0, Lija;

    new-instance v1, Lijb;

    invoke-direct {v1}, Lijb;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 115
    :cond_3
    const-class v0, Lijc;

    if-ne p2, v0, :cond_4

    .line 116
    const-class v0, Lijc;

    new-instance v1, Lijd;

    invoke-direct {v1}, Lijd;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 117
    :cond_4
    const-class v0, Lije;

    if-ne p2, v0, :cond_5

    .line 118
    const-class v0, Lije;

    new-instance v1, Lijf;

    invoke-direct {v1}, Lijf;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 119
    :cond_5
    const-class v0, Lino;

    if-ne p2, v0, :cond_6

    .line 120
    const-class v0, Lino;

    new-instance v1, Linp;

    invoke-direct {v1, p1}, Linp;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 121
    :cond_6
    const-class v0, Lihk;

    if-ne p2, v0, :cond_7

    .line 122
    const-class v0, Lihk;

    new-instance v1, Lihi;

    invoke-direct {v1}, Lihi;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 123
    :cond_7
    const-class v0, Liof;

    if-ne p2, v0, :cond_8

    .line 124
    const-class v0, Liof;

    new-instance v1, Liog;

    invoke-direct {v1, p1}, Liog;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 125
    :cond_8
    const-class v0, Linw;

    if-ne p2, v0, :cond_9

    .line 126
    const-class v0, Linw;

    new-instance v1, Linx;

    invoke-direct {v1, p1}, Linx;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 127
    :cond_9
    const-class v0, Linz;

    if-ne p2, v0, :cond_a

    .line 128
    const-class v0, Linz;

    new-instance v1, Lioa;

    invoke-direct {v1}, Lioa;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 129
    :cond_a
    const-class v0, Liok;

    if-ne p2, v0, :cond_b

    .line 130
    const-class v0, Liok;

    new-instance v1, Liol;

    invoke-direct {v1}, Liol;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 131
    :cond_b
    const-class v0, Lipl;

    if-ne p2, v0, :cond_c

    .line 132
    const-class v0, Lipl;

    new-instance v1, Lipm;

    invoke-direct {v1}, Lipm;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 133
    :cond_c
    const-class v0, Lioz;

    if-ne p2, v0, :cond_d

    .line 134
    const-class v0, Lioz;

    new-instance v1, Lipa;

    invoke-direct {v1}, Lipa;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 135
    :cond_d
    const-class v0, Lipg;

    if-ne p2, v0, :cond_e

    .line 136
    const-class v0, Lipg;

    new-instance v1, Liph;

    invoke-direct {v1}, Liph;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 137
    :cond_e
    const-class v0, Liqv;

    if-ne p2, v0, :cond_f

    .line 138
    const-class v0, Liqv;

    new-instance v1, Liqw;

    invoke-direct {v1}, Liqw;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 139
    :cond_f
    const-class v0, Lird;

    if-ne p2, v0, :cond_10

    .line 140
    const-class v0, Lird;

    new-instance v1, Lirf;

    invoke-direct {v1, p1}, Lirf;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 141
    :cond_10
    const-class v0, Lire;

    if-ne p2, v0, :cond_11

    .line 142
    const-class v0, Lire;

    new-instance v1, Lirg;

    invoke-direct {v1}, Lirg;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 144
    :cond_11
    const-class v0, Lirh;

    if-ne p2, v0, :cond_12

    .line 145
    const-class v0, Lirh;

    new-instance v1, Liri;

    invoke-direct {v1}, Liri;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 147
    :cond_12
    const-class v0, Limn;

    if-ne p2, v0, :cond_13

    .line 148
    const-class v0, Limn;

    new-instance v1, Limo;

    invoke-direct {v1}, Limo;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 149
    :cond_13
    const-class v0, Limk;

    if-ne p2, v0, :cond_14

    .line 150
    const-class v0, Limk;

    new-instance v1, Liml;

    invoke-direct {v1}, Liml;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 151
    :cond_14
    const-class v0, Lilw;

    if-ne p2, v0, :cond_15

    .line 152
    const-class v0, Lilw;

    new-instance v1, Lilx;

    invoke-direct {v1}, Lilx;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 153
    :cond_15
    const-class v0, Lilu;

    if-ne p2, v0, :cond_16

    .line 154
    const-class v0, Lilu;

    new-instance v1, Lilv;

    invoke-direct {v1}, Lilv;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 155
    :cond_16
    const-class v0, Lipz;

    if-ne p2, v0, :cond_17

    .line 156
    const-class v0, Lipz;

    new-instance v1, Liqa;

    invoke-direct {v1}, Liqa;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 157
    :cond_17
    const-class v0, Lipx;

    if-ne p2, v0, :cond_18

    .line 158
    const-class v0, Lipx;

    new-instance v1, Lipy;

    invoke-direct {v1}, Lipy;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 159
    :cond_18
    const-class v0, Lihu;

    if-ne p2, v0, :cond_19

    .line 160
    const-class v0, Lihu;

    new-instance v1, Lihv;

    invoke-direct {v1}, Lihv;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 161
    :cond_19
    const-class v0, Liid;

    if-ne p2, v0, :cond_1a

    .line 162
    const-class v0, Liid;

    new-instance v1, Liie;

    invoke-direct {v1}, Liie;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 163
    :cond_1a
    const-class v0, Liqz;

    if-ne p2, v0, :cond_1b

    .line 164
    const-class v0, Liqz;

    new-instance v1, Lira;

    invoke-direct {v1}, Lira;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 166
    :cond_1b
    const-class v0, Lini;

    if-ne p2, v0, :cond_1c

    .line 167
    const-class v0, Lini;

    new-instance v1, Linj;

    invoke-direct {v1}, Linj;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 168
    :cond_1c
    const-class v0, Limx;

    if-ne p2, v0, :cond_1d

    .line 169
    const-class v0, Limx;

    new-instance v1, Limy;

    invoke-direct {v1}, Limy;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 170
    :cond_1d
    const-class v0, Liig;

    if-ne p2, v0, :cond_1e

    .line 171
    const-class v0, Liig;

    new-instance v1, Liih;

    invoke-direct {v1}, Liih;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 172
    :cond_1e
    const-class v0, Liik;

    if-ne p2, v0, :cond_1f

    .line 173
    const-class v0, Liik;

    new-instance v1, Liil;

    invoke-direct {v1}, Liil;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 174
    :cond_1f
    const-class v0, Liqo;

    if-ne p2, v0, :cond_20

    .line 175
    const-class v0, Liqo;

    new-instance v1, Liqp;

    invoke-direct {v1}, Liqp;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 176
    :cond_20
    const-class v0, Lihm;

    if-ne p2, v0, :cond_21

    .line 177
    const-class v0, Lihm;

    new-instance v1, Lihn;

    invoke-direct {v1, p1}, Lihn;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 178
    :cond_21
    const-class v0, Lilk;

    if-ne p2, v0, :cond_22

    .line 179
    const-class v0, Lilk;

    new-instance v1, Lill;

    invoke-direct {v1}, Lill;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 180
    :cond_22
    const-class v0, Lilq;

    if-ne p2, v0, :cond_23

    .line 181
    const-class v0, Lilq;

    new-instance v1, Lilr;

    invoke-direct {v1}, Lilr;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 182
    :cond_23
    const-class v0, Likn;

    if-ne p2, v0, :cond_24

    .line 183
    const-class v0, Likn;

    new-instance v1, Likt;

    invoke-direct {v1}, Likt;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 184
    :cond_24
    const-class v0, Likv;

    if-ne p2, v0, :cond_25

    .line 185
    const-class v0, Likv;

    new-instance v1, Liky;

    invoke-direct {v1}, Liky;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 187
    :cond_25
    const-class v0, Likw;

    if-ne p2, v0, :cond_26

    .line 188
    const-class v0, Likw;

    new-instance v1, Likz;

    invoke-direct {v1}, Likz;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 190
    :cond_26
    const-class v0, Likp;

    if-ne p2, v0, :cond_27

    .line 191
    const-class v0, Likp;

    new-instance v1, Likr;

    invoke-direct {v1}, Likr;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 193
    :cond_27
    const-class v0, Lijl;

    if-ne p2, v0, :cond_28

    .line 194
    const-class v0, Lijl;

    new-instance v1, Lijp;

    invoke-direct {v1, p1}, Lijp;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 196
    :cond_28
    const-class v0, Lijx;

    if-ne p2, v0, :cond_29

    .line 197
    const-class v0, Lijx;

    new-instance v1, Lijz;

    invoke-direct {v1}, Lijz;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 198
    :cond_29
    const-class v0, Lile;

    if-ne p2, v0, :cond_2a

    .line 199
    const-class v0, Lile;

    new-instance v1, Lilf;

    invoke-direct {v1}, Lilf;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 200
    :cond_2a
    const-class v0, Lilh;

    if-ne p2, v0, :cond_2b

    .line 201
    const-class v0, Lilh;

    new-instance v1, Lili;

    invoke-direct {v1}, Lili;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 202
    :cond_2b
    const-class v0, Lirj;

    if-ne p2, v0, :cond_2c

    .line 203
    const-class v0, Lirj;

    new-instance v1, Lirk;

    invoke-direct {v1}, Lirk;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 204
    :cond_2c
    const-class v0, Lilz;

    if-ne p2, v0, :cond_2d

    .line 205
    const-class v0, Lilz;

    new-instance v1, Lima;

    invoke-direct {v1}, Lima;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 206
    :cond_2d
    const-class v0, Lily;

    if-ne p2, v0, :cond_2e

    .line 207
    const-class v0, Lily;

    new-instance v1, Limb;

    invoke-direct {v1}, Limb;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0

    .line 208
    :cond_2e
    const-class v0, Liis;

    if-ne p2, v0, :cond_0

    .line 209
    const-class v0, Liis;

    new-instance v1, Liit;

    invoke-direct {v1, p1}, Liit;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0
.end method
