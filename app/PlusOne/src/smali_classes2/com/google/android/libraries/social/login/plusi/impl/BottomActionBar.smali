.class public Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field private a:Landroid/widget/Button;

.field private b:Landroid/widget/Button;

.field private c:Landroid/widget/Button;

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->d:I

    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->a(Landroid/content/Context;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->d:I

    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->a(Landroid/content/Context;)V

    .line 36
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->d:I

    .line 40
    invoke-direct {p0, p1}, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->a(Landroid/content/Context;)V

    .line 41
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 44
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 46
    const v1, 0x7f04014c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 47
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->addView(Landroid/view/View;)V

    .line 49
    const v0, 0x7f10046b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->a:Landroid/widget/Button;

    .line 50
    const v0, 0x7f10046c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->b:Landroid/widget/Button;

    .line 51
    const v0, 0x7f100219

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->c:Landroid/widget/Button;

    .line 53
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->a()V

    .line 54
    return-void
.end method

.method private a(Landroid/widget/Button;ILjava/lang/String;Ljava/lang/Object;Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 93
    if-eqz p1, :cond_0

    .line 94
    invoke-virtual {p1, p2}, Landroid/widget/Button;->setId(I)V

    .line 95
    invoke-virtual {p1, p3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 96
    invoke-virtual {p1, p5}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    if-eqz p4, :cond_0

    .line 98
    invoke-virtual {p1, p4}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 101
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 60
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->d:I

    .line 61
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->a:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 62
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->b:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 63
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->c:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 64
    return-void
.end method

.method public a(ILjava/lang/String;Ljava/lang/Object;Landroid/view/View$OnClickListener;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 77
    iget v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->d:I

    if-nez v0, :cond_1

    .line 78
    iget-object v1, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->a:Landroid/widget/Button;

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->a(Landroid/widget/Button;ILjava/lang/String;Ljava/lang/Object;Landroid/view/View$OnClickListener;)V

    .line 79
    iget-object v1, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->b:Landroid/widget/Button;

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->a(Landroid/widget/Button;ILjava/lang/String;Ljava/lang/Object;Landroid/view/View$OnClickListener;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->a:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 81
    iget v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->d:I

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    iget v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->d:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->c:Landroid/widget/Button;

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->a(Landroid/widget/Button;ILjava/lang/String;Ljava/lang/Object;Landroid/view/View$OnClickListener;)V

    .line 84
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->a:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 85
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->b:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 86
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->c:Landroid/widget/Button;

    invoke-virtual {v0, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 87
    iget v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->d:I

    goto :goto_0
.end method

.method public b()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/widget/Button;",
            ">;"
        }
    .end annotation

    .prologue
    .line 107
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 108
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 109
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 110
    instance-of v3, v0, Landroid/widget/Button;

    if-eqz v3, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v3

    if-nez v3, :cond_0

    .line 111
    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 114
    :cond_1
    return-object v2
.end method
