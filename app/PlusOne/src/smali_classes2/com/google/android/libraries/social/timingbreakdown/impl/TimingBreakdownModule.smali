.class public Lcom/google/android/libraries/social/timingbreakdown/impl/TimingBreakdownModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 21
    const-class v0, Llfm;

    if-ne p2, v0, :cond_1

    .line 22
    const-class v0, Llfm;

    new-instance v1, Llfr;

    invoke-direct {v1, p1}, Llfr;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 33
    :cond_0
    :goto_0
    return-void

    .line 24
    :cond_1
    const-class v0, Lkfe;

    if-ne p2, v0, :cond_2

    .line 25
    const-class v0, Lkfe;

    new-instance v1, Llfq;

    invoke-direct {v1}, Llfq;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 26
    :cond_2
    const-class v0, Llfp;

    if-ne p2, v0, :cond_3

    .line 27
    const-class v0, Llfp;

    new-instance v1, Llfv;

    invoke-direct {v1}, Llfv;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 28
    :cond_3
    const-class v0, Lhzs;

    if-ne p2, v0, :cond_4

    .line 29
    const-class v0, Lhzs;

    new-instance v1, Llfs;

    invoke-direct {v1}, Llfs;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 30
    :cond_4
    const-class v0, Llfn;

    if-ne p2, v0, :cond_0

    .line 31
    const-class v0, Llfn;

    new-instance v1, Llfu;

    invoke-direct {v1, p1}, Llfu;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0
.end method
