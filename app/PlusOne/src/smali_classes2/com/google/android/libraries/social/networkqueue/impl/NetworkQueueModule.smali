.class public Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 22
    const-class v0, Ljhi;

    if-ne p2, v0, :cond_1

    .line 23
    const-class v0, Ljhi;

    const-class v1, Ljhs;

    invoke-virtual {p3, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 39
    :cond_0
    :goto_0
    return-void

    .line 24
    :cond_1
    const-class v0, Ljhs;

    if-ne p2, v0, :cond_2

    .line 25
    const-class v0, Ljhs;

    new-instance v1, Ljhs;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Ljhs;-><init>(Landroid/content/Context;Z)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 27
    :cond_2
    const-class v0, Lhzs;

    if-ne p2, v0, :cond_3

    .line 28
    const-class v0, Lhzs;

    new-instance v1, Ljhu;

    invoke-direct {v1}, Ljhu;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 29
    :cond_3
    const-class v0, Ljib;

    if-ne p2, v0, :cond_4

    .line 30
    const-class v0, Ljib;

    new-instance v1, Ljic;

    invoke-direct {v1, p1}, Ljic;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 31
    :cond_4
    const-class v0, Liwq;

    if-ne p2, v0, :cond_5

    .line 32
    const-class v0, Liwq;

    new-instance v1, Ljhr;

    invoke-direct {v1, p1}, Ljhr;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 33
    :cond_5
    const-class v0, Lkyz;

    if-ne p2, v0, :cond_6

    .line 34
    const-class v0, Lkyz;

    new-instance v1, Ljht;

    invoke-direct {v1, p1}, Ljht;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 36
    :cond_6
    const-class v0, Lief;

    if-ne p2, v0, :cond_0

    .line 37
    const-class v0, Lief;

    sget-object v1, Ljhj;->a:Lief;

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0
.end method
