.class public Lcom/google/android/libraries/social/networkcapability/impl/NetworkCapabilityModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 22
    const-class v0, Ljgn;

    if-ne p2, v0, :cond_1

    .line 23
    const-class v0, Ljgn;

    new-instance v1, Ljgt;

    invoke-direct {v1, p1}, Ljgt;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 34
    :cond_0
    :goto_0
    return-void

    .line 24
    :cond_1
    const-class v0, Ljgw;

    if-ne p2, v0, :cond_2

    .line 25
    const-class v0, Ljgw;

    new-instance v1, Ljgw;

    invoke-direct {v1, p1}, Ljgw;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 26
    :cond_2
    const-class v0, Ljhd;

    if-ne p2, v0, :cond_3

    .line 27
    const-class v0, Ljhd;

    const-class v1, Ljgw;

    invoke-virtual {p3, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 28
    :cond_3
    const-class v0, Ljgp;

    if-ne p2, v0, :cond_4

    .line 29
    const-class v0, Ljgp;

    new-instance v1, Ljgu;

    invoke-direct {v1, p1}, Ljgu;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 30
    :cond_4
    const-class v0, Lief;

    if-ne p2, v0, :cond_0

    .line 31
    const-class v0, Lief;

    sget-object v1, Ljgo;->a:Lief;

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0
.end method
