.class public final Lcom/google/android/libraries/snapseed/ui/views/CropImageView;
.super Landroid/view/View;
.source "PG"


# instance fields
.field private a:Landroid/graphics/Bitmap;

.field private b:Landroid/graphics/Paint;

.field private c:Landroid/graphics/Paint;

.field private d:Lhci;

.field private e:I

.field private f:I

.field private final g:Landroid/graphics/RectF;

.field private final h:Landroid/graphics/RectF;

.field private i:Z

.field private j:Z

.field private final k:Landroid/graphics/Matrix;

.field private final l:Landroid/graphics/Rect;

.field private m:F

.field private n:I

.field private final o:Landroid/graphics/RectF;

.field private final p:[Landroid/graphics/PointF;

.field private q:[I

.field private final r:Landroid/graphics/RectF;

.field private final s:Landroid/graphics/RectF;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 91
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 64
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->g:Landroid/graphics/RectF;

    .line 65
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    .line 77
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->k:Landroid/graphics/Matrix;

    .line 78
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->l:Landroid/graphics/Rect;

    .line 80
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->m:F

    .line 82
    iput v2, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->n:I

    .line 83
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->o:Landroid/graphics/RectF;

    .line 85
    new-array v0, v3, [Landroid/graphics/PointF;

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    aput-object v1, v0, v2

    const/4 v1, 0x1

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->p:[Landroid/graphics/PointF;

    .line 86
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->q:[I

    .line 88
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->r:Landroid/graphics/RectF;

    .line 415
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->s:Landroid/graphics/RectF;

    .line 93
    invoke-direct {p0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->c()V

    .line 94
    return-void

    .line 86
    nop

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 97
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 64
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->g:Landroid/graphics/RectF;

    .line 65
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    .line 77
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->k:Landroid/graphics/Matrix;

    .line 78
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->l:Landroid/graphics/Rect;

    .line 80
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->m:F

    .line 82
    iput v2, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->n:I

    .line 83
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->o:Landroid/graphics/RectF;

    .line 85
    new-array v0, v3, [Landroid/graphics/PointF;

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    aput-object v1, v0, v2

    const/4 v1, 0x1

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->p:[Landroid/graphics/PointF;

    .line 86
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->q:[I

    .line 88
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->r:Landroid/graphics/RectF;

    .line 415
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->s:Landroid/graphics/RectF;

    .line 99
    invoke-direct {p0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->c()V

    .line 100
    return-void

    .line 86
    nop

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x0

    .line 103
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 64
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->g:Landroid/graphics/RectF;

    .line 65
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    .line 77
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->k:Landroid/graphics/Matrix;

    .line 78
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->l:Landroid/graphics/Rect;

    .line 80
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->m:F

    .line 82
    iput v2, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->n:I

    .line 83
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->o:Landroid/graphics/RectF;

    .line 85
    new-array v0, v3, [Landroid/graphics/PointF;

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    aput-object v1, v0, v2

    const/4 v1, 0x1

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->p:[Landroid/graphics/PointF;

    .line 86
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->q:[I

    .line 88
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->r:Landroid/graphics/RectF;

    .line 415
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->s:Landroid/graphics/RectF;

    .line 105
    invoke-direct {p0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->c()V

    .line 106
    return-void

    .line 86
    nop

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data
.end method

.method private a(FFFLandroid/graphics/RectF;)V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 645
    div-float v0, p2, p3

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_1

    .line 646
    cmpl-float v0, p1, v1

    if-lez v0, :cond_0

    .line 647
    iget v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->f:I

    int-to-float v0, v0

    mul-float/2addr v0, p1

    invoke-static {v0, p2}, Ljava/lang/Math;->max(FF)F

    move-result p2

    .line 650
    :cond_0
    iget v0, p4, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, p2

    iput v0, p4, Landroid/graphics/RectF;->right:F

    .line 651
    iget v0, p4, Landroid/graphics/RectF;->top:F

    div-float v1, p2, p1

    add-float/2addr v0, v1

    iput v0, p4, Landroid/graphics/RectF;->bottom:F

    .line 660
    :goto_0
    return-void

    .line 653
    :cond_1
    cmpg-float v0, p1, v1

    if-gez v0, :cond_2

    .line 654
    iget v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->f:I

    int-to-float v0, v0

    div-float/2addr v0, p1

    invoke-static {v0, p3}, Ljava/lang/Math;->max(FF)F

    move-result p3

    .line 657
    :cond_2
    iget v0, p4, Landroid/graphics/RectF;->left:F

    mul-float v1, p3, p1

    add-float/2addr v0, v1

    iput v0, p4, Landroid/graphics/RectF;->right:F

    .line 658
    iget v0, p4, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, p3

    iput v0, p4, Landroid/graphics/RectF;->bottom:F

    goto :goto_0
.end method

.method private b(FZ)V
    .locals 9

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x0

    .line 227
    iput p1, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->m:F

    .line 229
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 240
    :goto_0
    return-void

    .line 233
    :cond_0
    if-eqz p2, :cond_3

    .line 234
    iget v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->m:F

    cmpl-float v0, v0, v6

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->r:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v1, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->r:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    iget v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->m:F

    float-to-double v2, v0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v2, v4

    if-ltz v0, :cond_4

    iget v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->m:F

    div-float v0, v1, v0

    :goto_1
    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v1, v2

    if-lez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    :cond_1
    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    cmpl-float v2, v0, v2

    if-lez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    div-float/2addr v1, v0

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    int-to-float v0, v0

    :cond_2
    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->r:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    iget-object v3, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->r:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    sub-float/2addr v3, v1

    div-float/2addr v3, v7

    add-float/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->r:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->r:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    sub-float/2addr v4, v0

    div-float/2addr v4, v7

    add-float/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->g:Landroid/graphics/RectF;

    invoke-virtual {v4, v6, v6, v1, v0}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v4, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->g:Landroid/graphics/RectF;

    iget-object v5, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    sub-float v1, v5, v1

    invoke-static {v2, v6, v1}, Lhcg;->a(FFF)F

    move-result v1

    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    sub-float v0, v2, v0

    invoke-static {v3, v6, v0}, Lhcg;->a(FFF)F

    move-result v0

    invoke-virtual {v4, v1, v0}, Landroid/graphics/RectF;->offsetTo(FF)V

    .line 237
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->j:Z

    .line 239
    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->invalidate()V

    goto/16 :goto_0

    .line 234
    :cond_4
    iget v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->m:F

    mul-float/2addr v0, v1

    move v8, v1

    move v1, v0

    move v0, v8

    goto/16 :goto_1
.end method

.method private c()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 109
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->setBackgroundColor(I)V

    .line 111
    new-instance v0, Lhci;

    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lhci;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->d:Lhci;

    .line 113
    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 115
    const/high16 v1, 0x41a00000    # 20.0f

    invoke-static {v2, v1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->e:I

    .line 118
    const/high16 v1, 0x41f00000    # 30.0f

    invoke-static {v2, v1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->f:I

    .line 121
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->b:Landroid/graphics/Paint;

    .line 122
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 123
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->b:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 125
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->c:Landroid/graphics/Paint;

    .line 126
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->c:Landroid/graphics/Paint;

    const/high16 v1, 0x7f000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 127
    return-void
.end method

.method private d()V
    .locals 4

    .prologue
    .line 339
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->g:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 350
    :cond_0
    :goto_0
    return-void

    .line 343
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->k:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->g:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 344
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/RectF;->left:F

    .line 345
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/RectF;->top:F

    .line 346
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/RectF;->right:F

    .line 347
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    .line 349
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->j:Z

    goto :goto_0
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 514
    iget v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->n:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/graphics/RectF;
    .locals 7

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 187
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 188
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 194
    :goto_0
    return-object v0

    .line 190
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->g:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0, v5, v6}, Lhcg;->a(FFF)F

    move-result v1

    .line 191
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->g:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v0, v2

    invoke-static {v0, v5, v6}, Lhcg;->a(FFF)F

    move-result v2

    .line 192
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->g:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v0, v3

    invoke-static {v0, v5, v6}, Lhcg;->a(FFF)F

    move-result v3

    .line 193
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->g:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v4, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v0, v4

    invoke-static {v0, v5, v6}, Lhcg;->a(FFF)F

    move-result v4

    .line 194
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    goto :goto_0
.end method

.method public a(FFFF)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 161
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    .line 162
    iget-object v1, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    .line 163
    invoke-static {p1, v5, v0}, Lhcg;->a(FFF)F

    move-result v2

    .line 164
    invoke-static {p2, v5, v1}, Lhcg;->a(FFF)F

    move-result v3

    .line 165
    add-float v4, p1, p3

    invoke-static {v4, v5, v0}, Lhcg;->a(FFF)F

    move-result v0

    .line 166
    add-float v4, p2, p4

    invoke-static {v4, v5, v1}, Lhcg;->a(FFF)F

    move-result v1

    .line 168
    iget-object v4, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->g:Landroid/graphics/RectF;

    invoke-virtual {v4, v2, v3, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    .line 169
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->j:Z

    .line 171
    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->invalidate()V

    .line 172
    return-void
.end method

.method public a(FZ)V
    .locals 2

    .prologue
    .line 216
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gtz v0, :cond_0

    .line 217
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid aspect ratio value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 220
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->b(FZ)V

    .line 221
    return-void
.end method

.method public a(IIII)V
    .locals 4

    .prologue
    .line 150
    int-to-float v0, p1

    int-to-float v1, p2

    int-to-float v2, p3

    int-to-float v3, p4

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a(FFFF)V

    .line 151
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 134
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    if-ne v0, p1, :cond_1

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 138
    :cond_1
    iput-object p1, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    .line 140
    if-eqz p1, :cond_0

    .line 141
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->r:Landroid/graphics/RectF;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 142
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-virtual {p0, v4, v4, v0, v1}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a(IIII)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 284
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->b(FZ)V

    .line 285
    return-void
.end method

.method public b()F
    .locals 2

    .prologue
    .line 204
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x0

    .line 391
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 410
    :goto_0
    return-void

    .line 395
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->i:Z

    if-eqz v0, :cond_3

    .line 396
    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    if-nez v1, :cond_2

    .line 401
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->k:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 402
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->l:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 404
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 405
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 406
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->k:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 407
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 409
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->d:Lhci;

    iget-object v1, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    invoke-virtual {v0, p1, v1, v7, v7}, Lhci;->a(Landroid/graphics/Canvas;Landroid/graphics/RectF;II)V

    goto :goto_0

    .line 396
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->getPaddingLeft()I

    move-result v2

    sub-int v2, v0, v2

    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->d:Lhci;

    invoke-virtual {v3}, Lhci;->a()I

    move-result v3

    shl-int/lit8 v3, v3, 0x1

    iget v4, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->e:I

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->getPaddingTop()I

    move-result v3

    sub-int v3, v1, v3

    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->getPaddingBottom()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->d:Lhci;

    invoke-virtual {v4}, Lhci;->a()I

    move-result v4

    shl-int/lit8 v4, v4, 0x1

    iget v5, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->e:I

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-static {v4, v5, v2, v3}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->a(IIII)Landroid/graphics/Point;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->l:Landroid/graphics/Rect;

    iget v4, v2, Landroid/graphics/Point;->x:I

    iget v5, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v3, v6, v6, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v3, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->l:Landroid/graphics/Rect;

    iget v4, v2, Landroid/graphics/Point;->x:I

    sub-int/2addr v0, v4

    div-int/lit8 v0, v0, 0x2

    iget v4, v2, Landroid/graphics/Point;->y:I

    sub-int/2addr v1, v4

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Rect;->offsetTo(II)V

    iget v0, v2, Landroid/graphics/Point;->x:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->k:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0, v0}, Landroid/graphics/Matrix;->setScale(FF)V

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->k:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->l:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->l:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    iput-boolean v6, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->i:Z

    invoke-direct {p0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->d()V

    goto/16 :goto_1

    .line 397
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->j:Z

    if-eqz v0, :cond_1

    .line 398
    invoke-direct {p0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->d()V

    goto/16 :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 354
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 356
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->i:Z

    .line 357
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13

    .prologue
    const/16 v3, 0xf

    const/4 v12, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 361
    iget-boolean v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->j:Z

    if-eqz v0, :cond_0

    .line 362
    invoke-direct {p0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->d()V

    .line 365
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_1
    move v1, v2

    .line 386
    :cond_2
    :goto_0
    return v1

    .line 367
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->s:Landroid/graphics/RectF;

    iget-object v6, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    invoke-virtual {v0, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->s:Landroid/graphics/RectF;

    iget v6, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->e:I

    neg-int v6, v6

    int-to-float v6, v6

    iget v7, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->e:I

    neg-int v7, v7

    int-to-float v7, v7

    invoke-virtual {v0, v6, v7}, Landroid/graphics/RectF;->inset(FF)V

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->s:Landroid/graphics/RectF;

    invoke-virtual {v0, v4, v5}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v2

    .line 368
    :cond_3
    :goto_1
    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    if-nez v0, :cond_9

    move v1, v2

    goto :goto_0

    .line 367
    :cond_4
    new-instance v0, Lhdp;

    iget-object v6, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->left:F

    iget-object v7, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->top:F

    iget-object v8, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->left:F

    iget-object v9, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v0, v6, v7, v8, v9}, Lhdp;-><init>(FFFF)V

    new-instance v6, Lhdp;

    iget-object v7, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->left:F

    iget-object v8, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->top:F

    iget-object v9, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->right:F

    iget-object v10, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->top:F

    invoke-direct {v6, v7, v8, v9, v10}, Lhdp;-><init>(FFFF)V

    new-instance v7, Lhdp;

    iget-object v8, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v8, v8, Landroid/graphics/RectF;->right:F

    iget-object v9, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->top:F

    iget-object v10, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->right:F

    iget-object v11, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v7, v8, v9, v10, v11}, Lhdp;-><init>(FFFF)V

    new-instance v8, Lhdp;

    iget-object v9, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v9, v9, Landroid/graphics/RectF;->left:F

    iget-object v10, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v10, v10, Landroid/graphics/RectF;->bottom:F

    iget-object v11, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v11, v11, Landroid/graphics/RectF;->right:F

    iget-object v12, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v12, v12, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v8, v9, v10, v11, v12}, Lhdp;-><init>(FFFF)V

    invoke-static {v0, v4, v5}, Lhdo;->a(Lhdp;FF)F

    move-result v0

    iget v9, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->e:I

    int-to-float v9, v9

    cmpg-float v0, v0, v9

    if-gez v0, :cond_2e

    move v0, v1

    :goto_2
    invoke-static {v6, v4, v5}, Lhdo;->a(Lhdp;FF)F

    move-result v6

    iget v9, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->e:I

    int-to-float v9, v9

    cmpg-float v6, v6, v9

    if-gez v6, :cond_5

    or-int/lit8 v0, v0, 0x2

    :cond_5
    invoke-static {v7, v4, v5}, Lhdo;->a(Lhdp;FF)F

    move-result v6

    iget v7, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->e:I

    int-to-float v7, v7

    cmpg-float v6, v6, v7

    if-gez v6, :cond_6

    or-int/lit8 v0, v0, 0x4

    :cond_6
    invoke-static {v8, v4, v5}, Lhdo;->a(Lhdp;FF)F

    move-result v6

    iget v7, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->e:I

    int-to-float v7, v7

    cmpg-float v6, v6, v7

    if-gez v6, :cond_7

    or-int/lit8 v0, v0, 0x8

    :cond_7
    and-int/lit8 v6, v0, 0x5

    const/4 v7, 0x5

    if-eq v6, v7, :cond_8

    and-int/lit8 v6, v0, 0xa

    const/16 v7, 0xa

    if-eq v6, v7, :cond_8

    if-nez v0, :cond_3

    iget-object v6, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    invoke-virtual {v6, v4, v5}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_8
    move v0, v3

    goto/16 :goto_1

    .line 368
    :cond_9
    iput v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->n:I

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->p:[Landroid/graphics/PointF;

    aget-object v0, v0, v2

    invoke-virtual {v0, v4, v5}, Landroid/graphics/PointF;->set(FF)V

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->q:[I

    aput v3, v0, v2

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->o:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto/16 :goto_0

    .line 371
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->q:[I

    aget v0, v0, v2

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v4

    if-ne v0, v4, :cond_1

    .line 372
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    iget v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->n:I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->p:[Landroid/graphics/PointF;

    aget-object v0, v0, v2

    iget v0, v0, Landroid/graphics/PointF;->x:F

    sub-float v4, v7, v0

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->p:[Landroid/graphics/PointF;

    aget-object v0, v0, v2

    iget v0, v0, Landroid/graphics/PointF;->y:F

    sub-float v0, v8, v0

    iget v5, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->n:I

    iget-object v9, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->o:Landroid/graphics/RectF;

    and-int/lit8 v6, v5, 0x1

    if-eqz v6, :cond_12

    move v6, v1

    :goto_3
    and-int/lit8 v5, v5, 0x4

    if-eqz v5, :cond_13

    move v5, v1

    :goto_4
    if-eqz v6, :cond_17

    if-eqz v5, :cond_15

    cmpg-float v10, v4, v12

    if-gez v10, :cond_14

    iget-object v10, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->l:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    int-to-float v10, v10

    iget v11, v9, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    invoke-static {v4, v10}, Ljava/lang/Math;->max(FF)F

    move-result v4

    :cond_a
    :goto_5
    if-eqz v6, :cond_b

    iget v6, v9, Landroid/graphics/RectF;->left:F

    add-float/2addr v6, v4

    iput v6, v9, Landroid/graphics/RectF;->left:F

    :cond_b
    if-eqz v5, :cond_c

    iget v5, v9, Landroid/graphics/RectF;->right:F

    add-float/2addr v4, v5

    iput v4, v9, Landroid/graphics/RectF;->right:F

    :cond_c
    iget v4, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->n:I

    iget-object v6, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->o:Landroid/graphics/RectF;

    and-int/lit8 v5, v4, 0x2

    if-eqz v5, :cond_19

    move v5, v1

    :goto_6
    and-int/lit8 v4, v4, 0x8

    if-eqz v4, :cond_1a

    move v4, v1

    :goto_7
    if-eqz v5, :cond_1e

    if-eqz v4, :cond_1c

    cmpg-float v9, v0, v12

    if-gez v9, :cond_1b

    iget-object v9, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->l:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    int-to-float v9, v9

    iget v10, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v9, v10

    invoke-static {v0, v9}, Ljava/lang/Math;->max(FF)F

    move-result v0

    :cond_d
    :goto_8
    if-eqz v5, :cond_e

    iget v5, v6, Landroid/graphics/RectF;->top:F

    add-float/2addr v5, v0

    iput v5, v6, Landroid/graphics/RectF;->top:F

    :cond_e
    if-eqz v4, :cond_f

    iget v4, v6, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, v4

    iput v0, v6, Landroid/graphics/RectF;->bottom:F

    :cond_f
    iget v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->n:I

    iget-object v6, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->o:Landroid/graphics/RectF;

    iget-object v9, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    iget v4, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->m:F

    cmpl-float v4, v4, v12

    if-eqz v4, :cond_10

    if-ne v0, v3, :cond_20

    :cond_10
    invoke-virtual {v9, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    :cond_11
    :goto_9
    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->invalidate()V

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->p:[Landroid/graphics/PointF;

    aget-object v0, v0, v2

    invoke-virtual {v0, v7, v8}, Landroid/graphics/PointF;->set(FF)V

    goto/16 :goto_0

    :cond_12
    move v6, v2

    goto :goto_3

    :cond_13
    move v5, v2

    goto :goto_4

    :cond_14
    iget-object v10, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->l:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    int-to-float v10, v10

    iget v11, v9, Landroid/graphics/RectF;->right:F

    sub-float/2addr v10, v11

    invoke-static {v4, v10}, Ljava/lang/Math;->min(FF)F

    move-result v4

    goto :goto_5

    :cond_15
    cmpg-float v10, v4, v12

    if-gez v10, :cond_16

    iget-object v10, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->l:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    int-to-float v10, v10

    iget v11, v9, Landroid/graphics/RectF;->left:F

    sub-float/2addr v10, v11

    invoke-static {v4, v10}, Ljava/lang/Math;->max(FF)F

    move-result v4

    goto :goto_5

    :cond_16
    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v10

    iget v11, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->f:I

    int-to-float v11, v11

    sub-float/2addr v10, v11

    invoke-static {v4, v10}, Ljava/lang/Math;->min(FF)F

    move-result v4

    goto/16 :goto_5

    :cond_17
    if-eqz v5, :cond_a

    cmpg-float v10, v4, v12

    if-gez v10, :cond_18

    iget v10, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->f:I

    int-to-float v10, v10

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v11

    sub-float/2addr v10, v11

    invoke-static {v4, v10}, Ljava/lang/Math;->max(FF)F

    move-result v4

    goto/16 :goto_5

    :cond_18
    iget-object v10, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->l:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    int-to-float v10, v10

    iget v11, v9, Landroid/graphics/RectF;->right:F

    sub-float/2addr v10, v11

    invoke-static {v4, v10}, Ljava/lang/Math;->min(FF)F

    move-result v4

    goto/16 :goto_5

    :cond_19
    move v5, v2

    goto/16 :goto_6

    :cond_1a
    move v4, v2

    goto/16 :goto_7

    :cond_1b
    iget-object v9, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->l:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    int-to-float v9, v9

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v9, v10

    invoke-static {v0, v9}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto/16 :goto_8

    :cond_1c
    cmpg-float v9, v0, v12

    if-gez v9, :cond_1d

    iget-object v9, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->l:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    int-to-float v9, v9

    iget v10, v6, Landroid/graphics/RectF;->top:F

    sub-float/2addr v9, v10

    invoke-static {v0, v9}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto/16 :goto_8

    :cond_1d
    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v9

    iget v10, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->f:I

    int-to-float v10, v10

    sub-float/2addr v9, v10

    invoke-static {v0, v9}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto/16 :goto_8

    :cond_1e
    if-eqz v4, :cond_d

    cmpg-float v9, v0, v12

    if-gez v9, :cond_1f

    iget v9, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->f:I

    int-to-float v9, v9

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v10

    sub-float/2addr v9, v10

    invoke-static {v0, v9}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto/16 :goto_8

    :cond_1f
    iget-object v9, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->l:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->bottom:I

    int-to-float v9, v9

    iget v10, v6, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v9, v10

    invoke-static {v0, v9}, Ljava/lang/Math;->min(FF)F

    move-result v0

    goto/16 :goto_8

    :cond_20
    and-int/lit8 v3, v0, 0x1

    if-eqz v3, :cond_22

    move v5, v1

    :goto_a
    and-int/lit8 v3, v0, 0x4

    if-eqz v3, :cond_23

    move v3, v1

    :goto_b
    and-int/lit8 v4, v0, 0x2

    if-eqz v4, :cond_24

    move v4, v1

    :goto_c
    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_25

    move v0, v1

    :goto_d
    invoke-virtual {v9, v6}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    if-nez v5, :cond_21

    if-eqz v4, :cond_28

    :cond_21
    if-nez v3, :cond_28

    if-nez v0, :cond_28

    iget v10, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->m:F

    if-eqz v5, :cond_26

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v0

    move v3, v0

    :goto_e
    if-eqz v4, :cond_27

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v0

    :goto_f
    invoke-direct {p0, v10, v3, v0, v9}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a(FFFLandroid/graphics/RectF;)V

    iget v0, v6, Landroid/graphics/RectF;->right:F

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v3

    sub-float/2addr v0, v3

    iget v3, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-virtual {v9, v0, v3}, Landroid/graphics/RectF;->offsetTo(FF)V

    goto/16 :goto_9

    :cond_22
    move v5, v2

    goto :goto_a

    :cond_23
    move v3, v2

    goto :goto_b

    :cond_24
    move v4, v2

    goto :goto_c

    :cond_25
    move v0, v2

    goto :goto_d

    :cond_26
    iget v0, v9, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->l:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    sub-float/2addr v0, v3

    move v3, v0

    goto :goto_e

    :cond_27
    iget v0, v9, Landroid/graphics/RectF;->bottom:F

    iget-object v4, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->l:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    sub-float/2addr v0, v4

    goto :goto_f

    :cond_28
    if-nez v3, :cond_29

    if-eqz v0, :cond_2c

    :cond_29
    if-nez v5, :cond_2c

    if-nez v4, :cond_2c

    iget v4, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->m:F

    if-eqz v3, :cond_2a

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v3

    :goto_10
    if-eqz v0, :cond_2b

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v0

    :goto_11
    invoke-direct {p0, v4, v3, v0, v9}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a(FFFLandroid/graphics/RectF;)V

    goto/16 :goto_9

    :cond_2a
    iget-object v3, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->l:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    iget v5, v9, Landroid/graphics/RectF;->left:F

    sub-float/2addr v3, v5

    goto :goto_10

    :cond_2b
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->l:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v0, v0

    iget v5, v9, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v5

    goto :goto_11

    :cond_2c
    if-eqz v3, :cond_2d

    if-eqz v4, :cond_2d

    iget v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->m:F

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-direct {p0, v0, v3, v4, v9}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a(FFFLandroid/graphics/RectF;)V

    iget v0, v6, Landroid/graphics/RectF;->left:F

    iget v3, v6, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-virtual {v9, v0, v3}, Landroid/graphics/RectF;->offsetTo(FF)V

    goto/16 :goto_9

    :cond_2d
    if-eqz v5, :cond_11

    if-eqz v0, :cond_11

    iget v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->m:F

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-virtual {v9}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-direct {p0, v0, v3, v4, v9}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a(FFFLandroid/graphics/RectF;)V

    iget v0, v6, Landroid/graphics/RectF;->right:F

    invoke-virtual {v9}, Landroid/graphics/RectF;->width()F

    move-result v3

    sub-float/2addr v0, v3

    iget v3, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v9, v0, v3}, Landroid/graphics/RectF;->offsetTo(FF)V

    goto/16 :goto_9

    .line 379
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 380
    invoke-direct {p0}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    iput v2, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->n:I

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->o:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->setEmpty()V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->k:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->g:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->h:Landroid/graphics/RectF;

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->r:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->g:Landroid/graphics/RectF;

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto/16 :goto_0

    :cond_2e
    move v0, v2

    goto/16 :goto_2

    .line 365
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
