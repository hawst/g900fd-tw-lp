.class public Lcom/google/android/libraries/photoeditor/filterparameters/BrushFilterParameter;
.super Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
.source "PG"


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# static fields
.field private static final g:Ljava/lang/Float;

.field private static final h:Ljava/lang/Float;

.field private static final i:Ljava/lang/Float;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/BrushFilterParameter;->g:Ljava/lang/Float;

    .line 24
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/BrushFilterParameter;->h:Ljava/lang/Float;

    .line 26
    const v0, 0x3ecccccd    # 0.4f

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/BrushFilterParameter;->i:Ljava/lang/Float;

    .line 29
    const/16 v0, 0x16

    const-class v1, Lcom/google/android/libraries/photoeditor/filterparameters/BrushFilterParameter;

    invoke-static {v0, v1}, Lham;->a(ILjava/lang/Class;)V

    .line 30
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 71
    sparse-switch p1, :sswitch_data_0

    .line 80
    invoke-super {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    .line 74
    :sswitch_0
    sget-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/BrushFilterParameter;->g:Ljava/lang/Float;

    goto :goto_0

    .line 77
    :sswitch_1
    sget-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/BrushFilterParameter;->h:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    neg-float v0, v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    .line 71
    nop

    :sswitch_data_0
    .sparse-switch
        0x390 -> :sswitch_0
        0x393 -> :sswitch_1
        0x399 -> :sswitch_0
    .end sparse-switch
.end method

.method public a()[I
    .locals 1

    .prologue
    .line 38
    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    return-object v0

    :array_0
    .array-data 4
        0x320
        0x3a3
        0x385
        0x386
        0x38f
        0x390
        0x393
        0x394
        0x399
    .end array-data
.end method

.method public b()I
    .locals 1

    .prologue
    .line 53
    const/16 v0, 0x393

    return v0
.end method

.method public b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 58
    sparse-switch p1, :sswitch_data_0

    .line 65
    invoke-super {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    .line 62
    :sswitch_0
    sget-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/BrushFilterParameter;->h:Ljava/lang/Float;

    goto :goto_0

    .line 58
    nop

    :sswitch_data_0
    .sparse-switch
        0x390 -> :sswitch_0
        0x393 -> :sswitch_0
        0x399 -> :sswitch_0
    .end sparse-switch
.end method

.method public c(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 86
    sparse-switch p1, :sswitch_data_0

    .line 95
    invoke-super {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->c(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    .line 88
    :sswitch_0
    sget-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/BrushFilterParameter;->i:Ljava/lang/Float;

    goto :goto_0

    .line 92
    :sswitch_1
    sget-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/BrushFilterParameter;->g:Ljava/lang/Float;

    goto :goto_0

    .line 86
    :sswitch_data_0
    .sparse-switch
        0x390 -> :sswitch_0
        0x393 -> :sswitch_1
        0x399 -> :sswitch_1
    .end sparse-switch
.end method

.method public getFilterType()I
    .locals 1

    .prologue
    .line 33
    const/16 v0, 0x16

    return v0
.end method
