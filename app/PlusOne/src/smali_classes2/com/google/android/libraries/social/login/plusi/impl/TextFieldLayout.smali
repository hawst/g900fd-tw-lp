.class public Lcom/google/android/libraries/social/login/plusi/impl/TextFieldLayout;
.super Lixo;
.source "PG"

# interfaces
.implements Liyb;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lixo;-><init>(Landroid/content/Context;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lixo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0, p1, p2, p3}, Lixo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/TextFieldLayout;->b:Liyf;

    if-eqz v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/TextFieldLayout;->b:Liyf;

    invoke-virtual {v0, p1}, Liyf;->a(Ljava/lang/String;)V

    .line 96
    :cond_0
    return-void
.end method

.method public a(Lnqa;ILiyf;)V
    .locals 7

    .prologue
    const/16 v6, 0xa

    .line 57
    invoke-super {p0, p1, p2, p3}, Lixo;->a(Lnqa;ILiyf;)V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/TextFieldLayout;->a()Landroid/widget/TextView;

    move-result-object v2

    .line 60
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/TextFieldLayout;->c()Lnqa;

    move-result-object v0

    iget-object v0, v0, Lnqa;->b:Lnqf;

    iget-object v0, v0, Lnqf;->a:Ljava/lang/String;

    .line 61
    invoke-static {v0}, Lixy;->a(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 62
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/TextFieldLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b00d6

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    .line 64
    const/4 v4, 0x0

    new-instance v5, Lixy;

    invoke-direct {v5, v1, v3, p0}, Lixy;-><init>(Ljava/util/List;ILiyb;)V

    invoke-static {v0, v4, v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;Landroid/text/Html$ImageGetter;Landroid/text/Html$TagHandler;)Landroid/text/Spanned;

    move-result-object v1

    instance-of v0, v1, Landroid/text/SpannableStringBuilder;

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Landroid/text/SpannableStringBuilder;

    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/4 v4, 0x2

    if-lt v3, v4, :cond_0

    add-int/lit8 v4, v3, -0x1

    invoke-virtual {v0, v4}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v4

    if-ne v4, v6, :cond_0

    add-int/lit8 v4, v3, -0x2

    invoke-virtual {v0, v4}, Landroid/text/SpannableStringBuilder;->charAt(I)C

    move-result v4

    if-ne v4, v6, :cond_0

    add-int/lit8 v4, v3, -0x2

    invoke-virtual {v0, v4, v3}, Landroid/text/SpannableStringBuilder;->delete(II)Landroid/text/SpannableStringBuilder;

    :cond_0
    sget-object v0, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    .line 63
    invoke-virtual {v2, v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    .line 66
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 67
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 69
    iget-object v0, p1, Lnqa;->b:Lnqf;

    iget-object v0, v0, Lnqf;->b:Lnqh;

    invoke-static {v2, v0}, Liyc;->a(Landroid/widget/TextView;Lnqh;)V

    .line 70
    return-void
.end method

.method public k()Lnqe;
    .locals 1

    .prologue
    .line 85
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 77
    const/4 v0, 0x0

    return v0
.end method
