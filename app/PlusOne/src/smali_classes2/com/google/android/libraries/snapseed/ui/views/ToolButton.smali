.class public Lcom/google/android/libraries/snapseed/ui/views/ToolButton;
.super Landroid/widget/TextView;
.source "PG"


# instance fields
.field private a:I

.field private b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 46
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->a:I

    .line 53
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->a(Landroid/content/Context;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 57
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 46
    iput v3, p0, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->a:I

    .line 59
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->a(Landroid/content/Context;)V

    .line 61
    invoke-interface {p2}, Landroid/util/AttributeSet;->getStyleAttribute()I

    move-result v0

    .line 62
    if-eqz v0, :cond_0

    .line 63
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->a(I)V

    .line 66
    :cond_0
    const-string v0, "http://schemas.android.com/apk/res/android"

    const-string v1, "src"

    const/4 v2, 0x0

    invoke-interface {p2, v0, v1, v2}, Landroid/util/AttributeSet;->getAttributeResourceValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    .line 67
    if-eqz v0, :cond_1

    .line 68
    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 69
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->a(Landroid/graphics/drawable/Drawable;)V

    .line 72
    :cond_1
    const-string v0, "http://schemas.android.com/apk/res/android"

    const-string v1, "orientation"

    invoke-interface {p2, v0, v1, v3}, Landroid/util/AttributeSet;->getAttributeIntValue(Ljava/lang/String;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->a:I

    .line 74
    return-void
.end method


# virtual methods
.method protected final a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/BitmapDrawable;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x0

    .line 233
    if-nez p1, :cond_0

    .line 248
    :goto_0
    return-object v0

    .line 237
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v1

    .line 238
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v2

    .line 240
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 241
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 242
    invoke-virtual {p1, v5, v5, v1, v2}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 243
    new-instance v1, Landroid/graphics/LightingColorFilter;

    const/4 v2, 0x1

    invoke-direct {v1, p2, v2}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    .line 244
    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 245
    invoke-virtual {p1, v4}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 246
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 248
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 195
    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setTextAppearance(Landroid/content/Context;I)V

    .line 196
    return-void
.end method

.method public a(III)V
    .locals 7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    const/4 v6, 0x2

    .line 110
    if-nez p1, :cond_0

    .line 133
    :goto_0
    return-void

    .line 114
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 116
    new-instance v1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 117
    if-eqz p2, :cond_1

    .line 118
    invoke-virtual {v0, p2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 119
    new-array v3, v6, [I

    fill-array-data v3, :array_0

    invoke-virtual {v1, v3, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 123
    :cond_1
    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 124
    if-nez p3, :cond_2

    .line 125
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    .line 126
    :goto_1
    const/4 v3, 0x1

    new-array v3, v3, [I

    const/4 v4, 0x0

    const v5, -0x101009e

    aput v5, v3, v4

    invoke-virtual {v1, v3, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 127
    new-array v3, v6, [I

    fill-array-data v3, :array_1

    invoke-virtual {v1, v3, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 130
    sget-object v0, Landroid/util/StateSet;->WILD_CARD:[I

    invoke-virtual {v1, v0, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 132
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 125
    :cond_2
    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    goto :goto_1

    .line 119
    nop

    :array_0
    .array-data 4
        0x101009e
        0x10100a1
    .end array-data

    .line 127
    :array_1
    .array-data 4
        -0x101009e
        0x10100a1
    .end array-data
.end method

.method protected a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 77
    invoke-static {p1}, Lhea;->b(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->b:Z

    .line 79
    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 80
    const/high16 v1, 0x40c00000    # 6.0f

    invoke-static {v2, v1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setCompoundDrawablePadding(I)V

    .line 83
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setClickable(Z)V

    .line 84
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;II)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 172
    if-nez p1, :cond_0

    .line 192
    :goto_0
    return-void

    .line 176
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 178
    new-instance v2, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 179
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v1, v0, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 180
    if-eqz p2, :cond_1

    .line 181
    invoke-virtual {p0, v1, p2}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    .line 182
    new-array v3, v6, [I

    const v4, 0x10100a1

    aput v4, v3, v5

    invoke-virtual {v2, v3, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 183
    new-array v3, v6, [I

    const v4, 0x10100a7

    aput v4, v3, v5

    invoke-virtual {v2, v3, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 186
    :cond_1
    if-nez p3, :cond_2

    move-object v0, v1

    .line 188
    :goto_1
    new-array v3, v6, [I

    const v4, -0x101009e

    aput v4, v3, v5

    invoke-virtual {v2, v3, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 189
    sget-object v0, Landroid/util/StateSet;->WILD_CARD:[I

    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 191
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 187
    :cond_2
    invoke-virtual {p0, v1, p3}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->a(Landroid/graphics/drawable/Drawable;I)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    goto :goto_1
.end method

.method public a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    .line 136
    if-nez p1, :cond_0

    .line 160
    :goto_0
    return-void

    .line 140
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 142
    new-instance v2, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 143
    if-eqz p2, :cond_1

    .line 144
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, v1, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 145
    new-array v3, v6, [I

    fill-array-data v3, :array_0

    invoke-virtual {v2, v3, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 149
    :cond_1
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v3, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 150
    if-nez p3, :cond_2

    .line 151
    invoke-virtual {p0, v3}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    .line 153
    :goto_1
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v4, 0x0

    const v5, -0x101009e

    aput v5, v1, v4

    invoke-virtual {v2, v1, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 154
    new-array v1, v6, [I

    fill-array-data v1, :array_1

    invoke-virtual {v2, v1, v0}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 157
    sget-object v0, Landroid/util/StateSet;->WILD_CARD:[I

    invoke-virtual {v2, v0, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 159
    invoke-virtual {p0, v2}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 151
    :cond_2
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-direct {v0, v1, p3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 145
    :array_0
    .array-data 4
        0x101009e
        0x10100a1
    .end array-data

    .line 154
    :array_1
    .array-data 4
        -0x101009e
        0x10100a1
    .end array-data
.end method

.method protected final a(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 199
    iget v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->a:I

    if-nez v0, :cond_1

    .line 200
    iget-boolean v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->b:Z

    if-eqz v0, :cond_0

    .line 201
    invoke-virtual {p0, v1, v1, p1, v1}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 206
    :goto_0
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setGravity(I)V

    .line 212
    :goto_1
    return-void

    .line 203
    :cond_0
    invoke-virtual {p0, p1, v1, v1, v1}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 208
    :cond_1
    invoke-virtual {p0, v1, p1, v1, v1}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 210
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setGravity(I)V

    goto :goto_1
.end method

.method protected final b(Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 215
    if-nez p1, :cond_0

    .line 216
    const/4 v0, 0x0

    .line 229
    :goto_0
    return-object v0

    .line 219
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 220
    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 222
    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 223
    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 224
    invoke-virtual {p1, v4, v4, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 225
    const/16 v0, 0x6e

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 226
    invoke-virtual {p1, v3}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 227
    const/16 v0, 0xff

    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 229
    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public b(III)V
    .locals 1

    .prologue
    .line 164
    if-eqz p1, :cond_0

    .line 165
    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->a(Landroid/graphics/Bitmap;II)V

    .line 168
    :cond_0
    return-void
.end method
