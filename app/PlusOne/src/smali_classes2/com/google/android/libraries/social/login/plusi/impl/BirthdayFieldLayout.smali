.class public Lcom/google/android/libraries/social/login/plusi/impl/BirthdayFieldLayout;
.super Lixo;
.source "PG"


# instance fields
.field private c:Landroid/widget/DatePicker;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lixo;-><init>(Landroid/content/Context;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lixo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lixo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method


# virtual methods
.method public a(Lnqa;ILiyf;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 64
    invoke-super {p0, p1, p2, p3}, Lixo;->a(Lnqa;ILiyf;)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/BirthdayFieldLayout;->c()Lnqa;

    move-result-object v0

    iget-object v0, v0, Lnqa;->c:Lnqe;

    .line 67
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/BirthdayFieldLayout;->a()Landroid/widget/TextView;

    move-result-object v2

    .line 68
    iget-object v3, v0, Lnqe;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, v0, Lnqe;->e:Lnqh;

    invoke-static {v2, v0}, Liyc;->a(Landroid/widget/TextView;Lnqh;)V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/BirthdayFieldLayout;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/DatePicker;

    iput-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BirthdayFieldLayout;->c:Landroid/widget/DatePicker;

    .line 73
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/BirthdayFieldLayout;->f()Lnpv;

    move-result-object v3

    .line 74
    if-eqz v3, :cond_3

    .line 75
    iget-object v0, v3, Lnpv;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    iget-object v0, v3, Lnpv;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 76
    :goto_0
    iget-object v2, v3, Lnpv;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_2

    iget-object v2, v3, Lnpv;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    .line 77
    :goto_1
    iget-object v4, v3, Lnpv;->a:Ljava/lang/Integer;

    if-eqz v4, :cond_0

    iget-object v1, v3, Lnpv;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 78
    :cond_0
    iget-object v3, p0, Lcom/google/android/libraries/social/login/plusi/impl/BirthdayFieldLayout;->c:Landroid/widget/DatePicker;

    invoke-virtual {v3, v0, v2, v1}, Landroid/widget/DatePicker;->updateDate(III)V

    .line 82
    :goto_2
    return-void

    :cond_1
    move v0, v1

    .line 75
    goto :goto_0

    :cond_2
    move v2, v1

    .line 76
    goto :goto_1

    .line 80
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BirthdayFieldLayout;->c:Landroid/widget/DatePicker;

    const/16 v2, 0x7b2

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v1, v3}, Landroid/widget/DatePicker;->updateDate(III)V

    goto :goto_2
.end method

.method public k()Lnqe;
    .locals 4

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/BirthdayFieldLayout;->c()Lnqa;

    move-result-object v0

    iget-object v0, v0, Lnqa;->c:Lnqe;

    invoke-static {v0}, Liyi;->a(Lnqe;)Lnqe;

    move-result-object v0

    .line 90
    new-instance v1, Lnqc;

    invoke-direct {v1}, Lnqc;-><init>()V

    iput-object v1, v0, Lnqe;->f:Lnqc;

    .line 91
    iget-object v1, v0, Lnqe;->f:Lnqc;

    new-instance v2, Lnpv;

    invoke-direct {v2}, Lnpv;-><init>()V

    iget-object v3, p0, Lcom/google/android/libraries/social/login/plusi/impl/BirthdayFieldLayout;->c:Landroid/widget/DatePicker;

    invoke-virtual {v3}, Landroid/widget/DatePicker;->getYear()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lnpv;->c:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/google/android/libraries/social/login/plusi/impl/BirthdayFieldLayout;->c:Landroid/widget/DatePicker;

    invoke-virtual {v3}, Landroid/widget/DatePicker;->getMonth()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lnpv;->b:Ljava/lang/Integer;

    iget-object v3, p0, Lcom/google/android/libraries/social/login/plusi/impl/BirthdayFieldLayout;->c:Landroid/widget/DatePicker;

    invoke-virtual {v3}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lnpv;->a:Ljava/lang/Integer;

    iput-object v2, v1, Lnqc;->c:Lnpv;

    .line 92
    return-object v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    return v0
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 4

    .prologue
    .line 128
    check-cast p1, Lixp;

    .line 129
    invoke-virtual {p1}, Lixp;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Lixo;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 130
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/BirthdayFieldLayout;->c:Landroid/widget/DatePicker;

    iget v1, p1, Lixp;->a:I

    iget v2, p1, Lixp;->b:I

    iget v3, p1, Lixp;->c:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/DatePicker;->updateDate(III)V

    .line 131
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 5

    .prologue
    .line 116
    invoke-super {p0}, Lixo;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    .line 117
    iget-object v1, p0, Lcom/google/android/libraries/social/login/plusi/impl/BirthdayFieldLayout;->c:Landroid/widget/DatePicker;

    invoke-virtual {v1}, Landroid/widget/DatePicker;->getYear()I

    move-result v1

    .line 118
    iget-object v2, p0, Lcom/google/android/libraries/social/login/plusi/impl/BirthdayFieldLayout;->c:Landroid/widget/DatePicker;

    invoke-virtual {v2}, Landroid/widget/DatePicker;->getMonth()I

    move-result v2

    .line 119
    iget-object v3, p0, Lcom/google/android/libraries/social/login/plusi/impl/BirthdayFieldLayout;->c:Landroid/widget/DatePicker;

    invoke-virtual {v3}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v3

    .line 120
    new-instance v4, Lixp;

    invoke-direct {v4, v0, v1, v2, v3}, Lixp;-><init>(Landroid/os/Parcelable;III)V

    return-object v4
.end method
