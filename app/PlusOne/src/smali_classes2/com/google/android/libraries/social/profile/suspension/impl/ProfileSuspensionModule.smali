.class public final Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 20
    const-class v0, Lixl;

    if-ne p2, v0, :cond_1

    .line 21
    const-class v0, Lixl;

    new-instance v1, Lkcg;

    invoke-direct {v1, p1}, Lkcg;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 30
    :cond_0
    :goto_0
    return-void

    .line 23
    :cond_1
    const-class v0, Lkby;

    if-ne p2, v0, :cond_2

    .line 24
    const-class v0, Lkby;

    new-instance v1, Lkcd;

    invoke-direct {v1}, Lkcd;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 25
    :cond_2
    const-class v0, Lkbz;

    if-ne p2, v0, :cond_3

    .line 26
    const-class v0, Lkbz;

    new-instance v1, Lkbz;

    invoke-direct {v1}, Lkbz;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 27
    :cond_3
    const-class v0, Lheo;

    if-ne p2, v0, :cond_0

    .line 28
    const-class v0, Lheo;

    const-class v1, Lkbz;

    invoke-virtual {p3, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0
.end method
