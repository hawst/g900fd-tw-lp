.class public Lcom/google/android/libraries/social/login/impl/LoginModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 21
    const-class v0, Lixd;

    if-ne p2, v0, :cond_1

    .line 22
    const-class v0, Lixd;

    new-instance v1, Lixd;

    invoke-direct {v1, p1}, Lixd;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 34
    :cond_0
    :goto_0
    return-void

    .line 23
    :cond_1
    const-class v0, Liwc;

    if-ne p2, v0, :cond_2

    .line 24
    const-class v0, Liwc;

    const-class v1, Lixd;

    invoke-virtual {p3, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 25
    :cond_2
    const-class v0, Lheh;

    if-ne p2, v0, :cond_3

    .line 26
    const-class v0, Lheh;

    const-class v1, Lixd;

    invoke-virtual {p3, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 27
    :cond_3
    const-class v0, Liwl;

    if-ne p2, v0, :cond_4

    .line 28
    const-class v0, Liwl;

    new-instance v1, Lixh;

    invoke-direct {v1}, Lixh;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 29
    :cond_4
    const-class v0, Livu;

    if-ne p2, v0, :cond_5

    .line 30
    const-class v0, Livu;

    new-instance v1, Liwu;

    invoke-direct {v1}, Liwu;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 31
    :cond_5
    const-class v0, Liww;

    if-ne p2, v0, :cond_0

    .line 32
    const-class v0, Liww;

    new-instance v1, Liww;

    invoke-direct {v1, p1}, Liww;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0
.end method
