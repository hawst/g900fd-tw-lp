.class public Lcom/google/android/libraries/social/experiments/impl/ExperimentServiceModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 18
    const-class v0, Lieh;

    if-ne p2, v0, :cond_1

    .line 19
    const-class v0, Lien;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lien;

    .line 20
    new-instance v1, Lies;

    invoke-direct {v1}, Lies;-><init>()V

    .line 22
    new-instance v2, Liep;

    invoke-direct {v2, p1, v0, v1}, Liep;-><init>(Landroid/content/Context;Lien;Lier;)V

    .line 24
    const-class v0, Lieh;

    invoke-virtual {p3, v0, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 30
    :cond_0
    :goto_0
    return-void

    .line 25
    :cond_1
    const-class v0, Liwo;

    if-ne p2, v0, :cond_2

    .line 26
    const-class v0, Liwo;

    new-instance v1, Liel;

    invoke-direct {v1, p1}, Liel;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 27
    :cond_2
    const-class v0, Lien;

    if-ne p2, v0, :cond_0

    .line 28
    const-class v0, Lien;

    new-instance v1, Lieo;

    invoke-direct {v1, p1}, Lieo;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0
.end method
