.class public Lcom/google/android/libraries/social/silentfeedback/impl/SilentFeedbackModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 24
    const-class v0, Lknq;

    if-ne p2, v0, :cond_1

    .line 25
    const-class v0, Lknq;

    new-instance v1, Lknr;

    invoke-direct {v1, p1}, Lknr;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 30
    :cond_0
    :goto_0
    return-void

    .line 26
    :cond_1
    const-class v0, Ljhb;

    if-ne p2, v0, :cond_0

    .line 27
    const-class v0, Ljhb;

    new-instance v1, Ljhb;

    const-string v2, "enable_silent_feedback_android_k"

    const/4 v3, 0x0

    invoke-direct {v1, p1, v2, v3}, Ljhb;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0
.end method
