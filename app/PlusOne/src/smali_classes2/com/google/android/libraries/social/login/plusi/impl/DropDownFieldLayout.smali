.class public Lcom/google/android/libraries/social/login/plusi/impl/DropDownFieldLayout;
.super Lixo;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field private c:Landroid/widget/Spinner;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lixo;-><init>(Landroid/content/Context;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Lixo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Lixo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method

.method private m()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/DropDownFieldLayout;->b:Liyf;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/DropDownFieldLayout;->b:Liyf;

    invoke-virtual {v0}, Liyf;->V()V

    .line 93
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lnqa;ILiyf;)V
    .locals 4

    .prologue
    const/4 v1, -0x1

    .line 64
    invoke-super {p0, p1, p2, p3}, Lixo;->a(Lnqa;ILiyf;)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/DropDownFieldLayout;->c()Lnqa;

    move-result-object v0

    iget-object v2, v0, Lnqa;->c:Lnqe;

    .line 67
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/DropDownFieldLayout;->a()Landroid/widget/TextView;

    move-result-object v0

    .line 68
    iget-object v3, v2, Lnqe;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v3, v2, Lnqe;->e:Lnqh;

    invoke-static {v0, v3}, Liyc;->a(Landroid/widget/TextView;Lnqh;)V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/DropDownFieldLayout;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/DropDownFieldLayout;->c:Landroid/widget/Spinner;

    .line 74
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/DropDownFieldLayout;->c:Landroid/widget/Spinner;

    iget-object v3, v2, Lnqe;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setPrompt(Ljava/lang/CharSequence;)V

    .line 76
    iget-object v0, v2, Lnqe;->i:[Lnqb;

    .line 77
    new-instance v2, Lixr;

    invoke-direct {v2, v0}, Lixr;-><init>([Lnqb;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/DropDownFieldLayout;->c:Landroid/widget/Spinner;

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/DropDownFieldLayout;->h()Lnqc;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_1

    invoke-virtual {v2, v0}, Lixr;->a(Lnqc;)I

    move-result v0

    .line 82
    :goto_0
    if-eq v0, v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/google/android/libraries/social/login/plusi/impl/DropDownFieldLayout;->c:Landroid/widget/Spinner;

    invoke-virtual {v1, v0}, Landroid/widget/Spinner;->setSelection(I)V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/DropDownFieldLayout;->c:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 87
    return-void

    :cond_1
    move v0, v1

    .line 81
    goto :goto_0
.end method

.method public k()Lnqe;
    .locals 2

    .prologue
    .line 116
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/DropDownFieldLayout;->c()Lnqa;

    move-result-object v0

    iget-object v0, v0, Lnqa;->c:Lnqe;

    invoke-static {v0}, Liyi;->a(Lnqe;)Lnqe;

    move-result-object v1

    .line 117
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/DropDownFieldLayout;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnqb;

    .line 118
    if-eqz v0, :cond_0

    .line 119
    iget-object v0, v0, Lnqb;->b:Lnqc;

    iput-object v0, v1, Lnqe;->f:Lnqc;

    .line 121
    :cond_0
    return-object v1
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/DropDownFieldLayout;->c:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/android/libraries/social/login/plusi/impl/DropDownFieldLayout;->m()V

    .line 101
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/google/android/libraries/social/login/plusi/impl/DropDownFieldLayout;->m()V

    .line 109
    return-void
.end method
