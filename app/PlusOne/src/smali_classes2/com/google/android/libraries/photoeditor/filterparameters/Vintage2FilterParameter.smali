.class public Lcom/google/android/libraries/photoeditor/filterparameters/Vintage2FilterParameter;
.super Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
.source "PG"


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# static fields
.field private static final g:[I

.field private static final h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x7

    .line 18
    new-array v0, v3, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/Vintage2FilterParameter;->g:[I

    .line 30
    const/16 v0, 0xc

    new-array v0, v0, [Ljava/lang/Integer;

    .line 31
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const/16 v2, 0xb

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v3

    const/16 v1, 0x8

    const/16 v2, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0xc

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const/16 v2, 0x8

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/Vintage2FilterParameter;->h:Ljava/util/List;

    .line 34
    const/16 v0, 0xca

    const-class v1, Lcom/google/android/libraries/photoeditor/filterparameters/Vintage2FilterParameter;

    invoke-static {v0, v1}, Lham;->a(ILjava/lang/Class;)V

    .line 35
    return-void

    .line 18
    nop

    :array_0
    .array-data 4
        0x9
        0x0
        0x2
        0x13
        0x6
        0x3
        0x320
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;-><init>()V

    return-void
.end method

.method private static e(I)I
    .locals 2

    .prologue
    .line 57
    sget-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/Vintage2FilterParameter;->h:Ljava/util/List;

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public a(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 91
    const/16 v0, 0x320

    if-ne p1, v0, :cond_0

    .line 92
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 94
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lhan;->a:Lhao;

    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/Vintage2FilterParameter;->getFilterType()I

    move-result v1

    invoke-interface {v0, v1, p1}, Lhao;->a(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public declared-synchronized a(ILjava/lang/Object;)Z
    .locals 3

    .prologue
    .line 72
    monitor-enter p0

    const/16 v0, 0x320

    if-ne p1, v0, :cond_0

    .line 73
    :try_start_0
    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result v0

    .line 74
    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/libraries/photoeditor/filterparameters/Vintage2FilterParameter;->h:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-super {p0, v1, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(ILjava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 76
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-super {p0, p1, p2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(ILjava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a()[I
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/Vintage2FilterParameter;->g:[I

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    return v0
.end method

.method public b(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 82
    const/16 v0, 0x320

    if-ne p1, v0, :cond_0

    .line 83
    sget-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/Vintage2FilterParameter;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 85
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lhan;->a:Lhao;

    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/Vintage2FilterParameter;->getFilterType()I

    move-result v1

    invoke-interface {v0, v1, p1}, Lhao;->b(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public c(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 100
    const/16 v0, 0x320

    if-ne p1, v0, :cond_0

    .line 101
    const/4 v0, 0x3

    invoke-super {p0, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->c(I)Ljava/lang/Object;

    move-result-object v0

    .line 102
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/Vintage2FilterParameter;->e(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 104
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->c(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getFilterType()I
    .locals 1

    .prologue
    .line 39
    const/16 v0, 0xca

    return v0
.end method

.method public declared-synchronized getParameterValue(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 62
    monitor-enter p0

    const/16 v0, 0x320

    if-ne p1, v0, :cond_0

    .line 63
    const/4 v0, 0x3

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/Vintage2FilterParameter;->getParameterInteger(I)I

    move-result v0

    .line 64
    invoke-static {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/Vintage2FilterParameter;->e(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 66
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-super {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterValue(I)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 62
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
