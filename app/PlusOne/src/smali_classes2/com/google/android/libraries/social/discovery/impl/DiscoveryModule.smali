.class public Lcom/google/android/libraries/social/discovery/impl/DiscoveryModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    .line 23
    const-class v0, Liaw;

    if-ne p2, v0, :cond_1

    .line 24
    const-class v0, Liaw;

    new-instance v1, Libi;

    invoke-direct {v1, p1}, Libi;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 38
    :cond_0
    :goto_0
    return-void

    .line 25
    :cond_1
    const-class v0, Libp;

    if-ne p2, v0, :cond_2

    .line 26
    const-class v0, Libp;

    new-instance v1, Libp;

    invoke-direct {v1}, Libp;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 27
    :cond_2
    const-class v0, Liax;

    if-ne p2, v0, :cond_3

    .line 28
    const-class v0, Liax;

    new-instance v1, Libj;

    invoke-direct {v1}, Libj;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 29
    :cond_3
    const-class v0, Lhzs;

    if-ne p2, v0, :cond_4

    .line 30
    const-class v0, Lhzs;

    new-instance v1, Libo;

    invoke-direct {v1}, Libo;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 31
    :cond_4
    const-class v0, Lief;

    if-ne p2, v0, :cond_5

    .line 32
    const-class v0, Lief;

    sget-object v1, Libh;->a:Lief;

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    .line 33
    const-class v0, Lief;

    sget-object v1, Libh;->b:Lief;

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 35
    :cond_5
    const-class v0, Lheo;

    if-ne p2, v0, :cond_0

    .line 36
    const-class v0, Lheo;

    new-instance v1, Libf;

    invoke-direct {v1}, Libf;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0
.end method
