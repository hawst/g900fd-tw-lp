.class public Lcom/google/android/libraries/photoeditor/filterparameters/UPointFilterParameter;
.super Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
.source "PG"


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 17
    const/4 v0, 0x3

    const-class v1, Lcom/google/android/libraries/photoeditor/filterparameters/UPointFilterParameter;

    invoke-static {v0, v1}, Lham;->a(ILjava/lang/Class;)V

    .line 18
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/UPointFilterParameter;->c:Ljava/lang/Integer;

    return-object v0
.end method

.method public a()[I
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    new-array v0, v0, [I

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 47
    const/16 v0, 0x3e8

    return v0
.end method

.method public b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/UPointFilterParameter;->b:Ljava/lang/Integer;

    return-object v0
.end method

.method public declared-synchronized c()Lcom/google/android/libraries/photoeditor/filterparameters/UPointFilterParameter;
    .locals 1

    .prologue
    .line 52
    monitor-enter p0

    :try_start_0
    invoke-super {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->e()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/photoeditor/filterparameters/UPointFilterParameter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/UPointFilterParameter;->a:Ljava/lang/Integer;

    return-object v0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/UPointFilterParameter;->c()Lcom/google/android/libraries/photoeditor/filterparameters/UPointFilterParameter;

    move-result-object v0

    return-object v0
.end method

.method public synthetic e()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/UPointFilterParameter;->c()Lcom/google/android/libraries/photoeditor/filterparameters/UPointFilterParameter;

    move-result-object v0

    return-object v0
.end method

.method public getFilterType()I
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x3

    return v0
.end method
