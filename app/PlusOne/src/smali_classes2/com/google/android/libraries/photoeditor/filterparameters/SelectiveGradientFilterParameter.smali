.class public Lcom/google/android/libraries/photoeditor/filterparameters/SelectiveGradientFilterParameter;
.super Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
.source "PG"


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# instance fields
.field private final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 19
    const/16 v0, 0x15

    const-class v1, Lcom/google/android/libraries/photoeditor/filterparameters/SelectiveGradientFilterParameter;

    invoke-static {v0, v1}, Lham;->a(ILjava/lang/Class;)V

    .line 21
    const/16 v0, 0x13

    const-class v1, Lcom/google/android/libraries/photoeditor/filterparameters/SelectiveGradientFilterParameter;

    invoke-static {v0, v1}, Lham;->a(ILjava/lang/Class;)V

    .line 23
    return-void
.end method


# virtual methods
.method public a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 78
    sparse-switch p1, :sswitch_data_0

    .line 86
    invoke-super {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    .line 80
    :sswitch_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 83
    :sswitch_1
    const-string v0, ""

    goto :goto_0

    .line 78
    nop

    :sswitch_data_0
    .sparse-switch
        0xc9 -> :sswitch_0
        0x352 -> :sswitch_1
    .end sparse-switch
.end method

.method public a()[I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    return-object v0

    nop

    :array_0
    .array-data 4
        0xc9
        0x352
    .end array-data
.end method

.method public affectsPanorama()Z
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    return v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 45
    const/16 v0, 0xc9

    return v0
.end method

.method public b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 64
    sparse-switch p1, :sswitch_data_0

    .line 72
    invoke-super {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    .line 66
    :sswitch_0
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 69
    :sswitch_1
    const-string v0, ""

    goto :goto_0

    .line 64
    nop

    :sswitch_data_0
    .sparse-switch
        0xc9 -> :sswitch_0
        0x352 -> :sswitch_1
    .end sparse-switch
.end method

.method public c(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 50
    sparse-switch p1, :sswitch_data_0

    .line 58
    invoke-super {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->c(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    .line 52
    :sswitch_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 55
    :sswitch_1
    const-string v0, ""

    goto :goto_0

    .line 50
    nop

    :sswitch_data_0
    .sparse-switch
        0xc9 -> :sswitch_0
        0x352 -> :sswitch_1
    .end sparse-switch
.end method

.method public getFilterType()I
    .locals 2

    .prologue
    .line 31
    iget v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/SelectiveGradientFilterParameter;->g:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const/16 v0, 0x15

    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x13

    goto :goto_0
.end method
