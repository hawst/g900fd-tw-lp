.class public Lcom/google/android/libraries/social/login/plusi/impl/ButtonFieldLayout;
.super Lixo;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private c:Landroid/widget/Button;

.field private d:Lnpx;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0, p1}, Lixo;-><init>(Landroid/content/Context;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Lixo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Lixo;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    return-void
.end method


# virtual methods
.method public a(Lnqa;ILiyf;)V
    .locals 2

    .prologue
    .line 59
    invoke-super {p0, p1, p2, p3}, Lixo;->a(Lnqa;ILiyf;)V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/ButtonFieldLayout;->c()Lnqa;

    move-result-object v0

    iget-object v0, v0, Lnqa;->e:Lnpx;

    iput-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/ButtonFieldLayout;->d:Lnpx;

    .line 63
    invoke-virtual {p0}, Lcom/google/android/libraries/social/login/plusi/impl/ButtonFieldLayout;->b()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/ButtonFieldLayout;->c:Landroid/widget/Button;

    .line 64
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/ButtonFieldLayout;->d:Lnpx;

    iget-object v0, v0, Lnpx;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/ButtonFieldLayout;->c:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/libraries/social/login/plusi/impl/ButtonFieldLayout;->d:Lnpx;

    iget-object v1, v1, Lnpx;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 68
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/ButtonFieldLayout;->c:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 69
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/ButtonFieldLayout;->c:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 103
    return-void
.end method

.method public k()Lnqe;
    .locals 1

    .prologue
    .line 94
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/ButtonFieldLayout;->b:Liyf;

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lcom/google/android/libraries/social/login/plusi/impl/ButtonFieldLayout;->b:Liyf;

    iget-object v1, p0, Lcom/google/android/libraries/social/login/plusi/impl/ButtonFieldLayout;->d:Lnpx;

    invoke-virtual {v0, v1}, Liyf;->a(Lnpx;)V

    .line 79
    :cond_0
    return-void
.end method
