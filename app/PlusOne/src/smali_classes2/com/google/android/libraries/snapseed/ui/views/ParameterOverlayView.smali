.class public final Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;
.super Landroid/view/View;
.source "PG"


# instance fields
.field private final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lhdx;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Landroid/graphics/Rect;

.field private final c:Landroid/graphics/RectF;

.field private d:Lhdq;

.field private e:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 24
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a:Landroid/util/SparseArray;

    .line 26
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b:Landroid/graphics/Rect;

    .line 27
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->c:Landroid/graphics/RectF;

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a:Landroid/util/SparseArray;

    .line 26
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b:Landroid/graphics/Rect;

    .line 27
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->c:Landroid/graphics/RectF;

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a:Landroid/util/SparseArray;

    .line 26
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b:Landroid/graphics/Rect;

    .line 27
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->c:Landroid/graphics/RectF;

    .line 49
    return-void
.end method


# virtual methods
.method public a(Lhdx;)I
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 201
    move v0, v1

    move v2, v3

    .line 202
    :goto_0
    iget-object v4, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 203
    iget-object v4, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a:Landroid/util/SparseArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 202
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 206
    :cond_0
    if-ne v2, v3, :cond_1

    .line 207
    :goto_1
    invoke-virtual {p0, v1, p1}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a(ILhdx;)V

    .line 209
    return v1

    .line 206
    :cond_1
    add-int/lit8 v1, v2, 0x1

    goto :goto_1
.end method

.method public a()Landroid/graphics/Rect;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 148
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->e:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getRootView()Landroid/view/View;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b:Landroid/graphics/Rect;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 150
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b:Landroid/graphics/Rect;

    .line 156
    :goto_0
    return-object v0

    .line 154
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->e:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->e:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v2

    iget-object v4, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->e:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->e:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    :goto_1
    instance-of v1, v0, Landroid/view/View;

    if-eqz v1, :cond_2

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v1, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    :goto_2
    instance-of v4, v1, Landroid/view/View;

    if-eqz v4, :cond_4

    if-ne v0, v1, :cond_3

    const/4 v1, 0x1

    :goto_3
    if-eqz v1, :cond_5

    move-object v2, v0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    :goto_4
    instance-of v0, v1, Landroid/view/View;

    if-eqz v0, :cond_6

    move-object v0, v1

    check-cast v0, Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/view/View;->getScrollX()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {v0}, Landroid/view/View;->getScrollY()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Rect;->offset(II)V

    if-eq v1, v2, :cond_6

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_4

    :cond_3
    invoke-interface {v1}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    goto :goto_2

    :cond_4
    move v1, v3

    goto :goto_3

    :cond_5
    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->e:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->e:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->e:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b:Landroid/graphics/Rect;

    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v2, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->e:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 156
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b:Landroid/graphics/Rect;

    goto/16 :goto_0
.end method

.method public a(ILhdx;)V
    .locals 2

    .prologue
    .line 180
    if-nez p2, :cond_0

    .line 181
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Handler cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 184
    :cond_0
    if-gez p1, :cond_1

    .line 185
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Handler id should be a positive number or 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 189
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->d:Lhdq;

    if-nez v0, :cond_2

    .line 190
    new-instance v0, Lhdq;

    invoke-direct {v0}, Lhdq;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->d:Lhdq;

    .line 192
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->d:Lhdq;

    invoke-virtual {v0, p2}, Lhdq;->a(Lhdu;)V

    .line 193
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 78
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getRootView()Landroid/view/View;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 79
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The overlay target and the overlay have to be in the same view hierarchy."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_1
    iput-object p1, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->e:Landroid/view/View;

    .line 83
    return-void
.end method

.method public b()Landroid/graphics/RectF;
    .locals 2

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a()Landroid/graphics/Rect;

    move-result-object v0

    .line 167
    iget-object v1, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->c:Landroid/graphics/RectF;

    invoke-virtual {v1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 175
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->c:Landroid/graphics/RectF;

    return-object v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 218
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 69
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 70
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdx;

    .line 71
    invoke-virtual {v0}, Lhdx;->o()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 72
    invoke-virtual {v0, p1}, Lhdx;->a(Landroid/graphics/Canvas;)V

    .line 69
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 75
    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->d:Lhdq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->d:Lhdq;

    invoke-virtual {v0, p1}, Lhdq;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
