.class public Lcom/google/android/libraries/photoeditor/filterparameters/StrokeParameter;
.super Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
.source "PG"


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# static fields
.field private static final g:Ljava/lang/Float;

.field private static final h:Ljava/lang/Float;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 16
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/StrokeParameter;->g:Ljava/lang/Float;

    .line 17
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/StrokeParameter;->h:Ljava/lang/Float;

    .line 21
    const/16 v0, 0x192

    const-class v1, Lcom/google/android/libraries/photoeditor/filterparameters/StrokeParameter;

    invoke-static {v0, v1}, Lham;->a(ILjava/lang/Class;)V

    .line 22
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 80
    sparse-switch p1, :sswitch_data_0

    .line 89
    invoke-super {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    .line 83
    :sswitch_0
    sget-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/StrokeParameter;->g:Ljava/lang/Float;

    goto :goto_0

    .line 86
    :sswitch_1
    sget-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/StrokeParameter;->h:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    neg-float v0, v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    .line 80
    nop

    :sswitch_data_0
    .sparse-switch
        0x390 -> :sswitch_0
        0x393 -> :sswitch_1
        0x399 -> :sswitch_0
    .end sparse-switch
.end method

.method protected a([I)V
    .locals 3

    .prologue
    .line 31
    invoke-virtual {p1}, [I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    iput-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/StrokeParameter;->d:[I

    .line 32
    array-length v0, p1

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/StrokeParameter;->e:[Ljava/lang/Object;

    .line 34
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 35
    iget-object v1, p0, Lcom/google/android/libraries/photoeditor/filterparameters/StrokeParameter;->e:[Ljava/lang/Object;

    aget v2, p1, v0

    invoke-virtual {p0, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/StrokeParameter;->c(I)Ljava/lang/Object;

    move-result-object v2

    aput-object v2, v1, v0

    .line 34
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 38
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/StrokeParameter;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/libraries/photoeditor/filterparameters/StrokeParameter;->f:I

    .line 39
    return-void
.end method

.method protected a()[I
    .locals 1

    .prologue
    .line 48
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    return-object v0

    :array_0
    .array-data 4
        0x385
        0x38f
        0x390
        0x391
        0x392
        0x393
        0x394
        0x399
    .end array-data
.end method

.method public b()I
    .locals 1

    .prologue
    .line 62
    const/16 v0, 0x393

    return v0
.end method

.method public b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 67
    sparse-switch p1, :sswitch_data_0

    .line 74
    invoke-super {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    .line 71
    :sswitch_0
    sget-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/StrokeParameter;->h:Ljava/lang/Float;

    goto :goto_0

    .line 67
    nop

    :sswitch_data_0
    .sparse-switch
        0x390 -> :sswitch_0
        0x393 -> :sswitch_0
        0x399 -> :sswitch_0
    .end sparse-switch
.end method

.method public c(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 95
    sparse-switch p1, :sswitch_data_0

    .line 102
    invoke-super {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->c(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    .line 99
    :sswitch_0
    sget-object v0, Lcom/google/android/libraries/photoeditor/filterparameters/StrokeParameter;->g:Ljava/lang/Float;

    goto :goto_0

    .line 95
    nop

    :sswitch_data_0
    .sparse-switch
        0x390 -> :sswitch_0
        0x393 -> :sswitch_0
        0x399 -> :sswitch_0
    .end sparse-switch
.end method

.method public getFilterType()I
    .locals 1

    .prologue
    .line 43
    const/16 v0, 0x192

    return v0
.end method
