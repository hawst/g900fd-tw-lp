.class public Lcom/google/android/libraries/social/poll/impl/PollModule;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llnq;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/Class;Llnh;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;",
            "Llnh;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 27
    const-class v0, Lloc;

    if-ne p2, v0, :cond_1

    .line 28
    const-class v0, Lloc;

    new-array v1, v1, [Lloc;

    new-instance v2, Ljzw;

    invoke-direct {v2}, Ljzw;-><init>()V

    aput-object v2, v1, v3

    new-instance v2, Lkar;

    invoke-direct {v2}, Lkar;-><init>()V

    aput-object v2, v1, v4

    new-instance v2, Lkao;

    invoke-direct {v2}, Lkao;-><init>()V

    aput-object v2, v1, v5

    new-instance v2, Ljzu;

    invoke-direct {v2}, Ljzu;-><init>()V

    aput-object v2, v1, v6

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 32
    :cond_1
    const-class v0, Llop;

    if-ne p2, v0, :cond_2

    .line 33
    const-class v0, Llop;

    new-array v1, v1, [Llop;

    new-instance v2, Ljzw;

    invoke-direct {v2}, Ljzw;-><init>()V

    aput-object v2, v1, v3

    new-instance v2, Lkar;

    invoke-direct {v2}, Lkar;-><init>()V

    aput-object v2, v1, v4

    new-instance v2, Lkao;

    invoke-direct {v2}, Lkao;-><init>()V

    aput-object v2, v1, v5

    new-instance v2, Ljzu;

    invoke-direct {v2}, Ljzu;-><init>()V

    aput-object v2, v1, v6

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;[Ljava/lang/Object;)V

    goto :goto_0

    .line 37
    :cond_2
    const-class v0, Ljxn;

    if-ne p2, v0, :cond_3

    .line 38
    const-class v0, Ljxn;

    new-instance v1, Ljyz;

    invoke-direct {v1, p1}, Ljyz;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 39
    :cond_3
    const-class v0, Lief;

    if-ne p2, v0, :cond_4

    .line 40
    const-class v0, Lief;

    sget-object v1, Lkat;->a:Lief;

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 41
    :cond_4
    const-class v0, Ljxv;

    if-ne p2, v0, :cond_5

    .line 42
    const-class v0, Ljxv;

    new-instance v1, Lkac;

    invoke-direct {v1, p1}, Lkac;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto :goto_0

    .line 43
    :cond_5
    const-class v0, Lheo;

    if-ne p2, v0, :cond_6

    .line 44
    const-class v0, Lheo;

    const-class v1, Ljxv;

    invoke-virtual {p3, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 45
    :cond_6
    const-class v0, Lkjl;

    if-ne p2, v0, :cond_7

    .line 46
    const-class v0, Lkjl;

    new-instance v1, Ljze;

    invoke-direct {v1}, Ljze;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 52
    :cond_7
    const-class v0, Lkzf;

    if-ne p2, v0, :cond_8

    .line 53
    const-class v0, Lkzf;

    new-instance v1, Ljzf;

    invoke-direct {v1}, Ljzf;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto :goto_0

    .line 54
    :cond_8
    const-class v0, Ljxq;

    if-ne p2, v0, :cond_0

    .line 55
    const-class v0, Ljxq;

    new-instance v1, Ljzq;

    invoke-direct {v1}, Ljzq;-><init>()V

    invoke-virtual {p3, v0, v1}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    goto/16 :goto_0
.end method
