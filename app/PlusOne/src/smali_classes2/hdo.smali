.class public final Lhdo;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(FFFF)F
    .locals 2

    .prologue
    .line 17
    invoke-static {p0, p1, p2, p3}, Lhdo;->b(FFFF)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public static a(Lhdp;FF)F
    .locals 4

    .prologue
    .line 31
    iget v0, p0, Lhdp;->a:F

    mul-float/2addr v0, p1

    iget v1, p0, Lhdp;->b:F

    mul-float/2addr v1, p2

    add-float/2addr v0, v1

    iget v1, p0, Lhdp;->c:F

    add-float/2addr v0, v1

    mul-float/2addr v0, v0

    iget v1, p0, Lhdp;->a:F

    iget v2, p0, Lhdp;->a:F

    mul-float/2addr v1, v2

    iget v2, p0, Lhdp;->b:F

    iget v3, p0, Lhdp;->b:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public static b(FFFF)F
    .locals 3

    .prologue
    .line 24
    sub-float v0, p2, p0

    sub-float v1, p2, p0

    mul-float/2addr v0, v1

    sub-float v1, p3, p1

    sub-float v2, p3, p1

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method
