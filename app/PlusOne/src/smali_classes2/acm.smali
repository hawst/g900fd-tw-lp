.class public final Lacm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:I

.field private b:Z

.field private c:[F

.field private d:Z

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:[F

.field private l:[F

.field private m:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lacn;",
            ">;"
        }
    .end annotation
.end field

.field private n:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Laco;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 224
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput v1, p0, Lacm;->a:I

    .line 43
    iput-boolean v1, p0, Lacm;->b:Z

    .line 44
    new-array v0, v2, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lacm;->c:[F

    .line 45
    iput-boolean v1, p0, Lacm;->d:Z

    .line 46
    const/16 v0, 0x302

    iput v0, p0, Lacm;->e:I

    .line 47
    const/16 v0, 0x303

    iput v0, p0, Lacm;->f:I

    .line 48
    const/4 v0, 0x5

    iput v0, p0, Lacm;->g:I

    .line 49
    iput v2, p0, Lacm;->h:I

    .line 50
    const v0, 0x84c0

    iput v0, p0, Lacm;->i:I

    .line 51
    const/16 v0, 0x4000

    iput v0, p0, Lacm;->j:I

    .line 52
    new-array v0, v3, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lacm;->k:[F

    .line 53
    new-array v0, v3, [F

    fill-array-data v0, :array_2

    iput-object v0, p0, Lacm;->l:[F

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lacm;->n:Ljava/util/HashMap;

    .line 225
    const-string v0, "attribute vec4 a_position;\nattribute vec2 a_texcoord;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_Position = a_position;\n  v_texcoord = a_texcoord;\n}\n"

    invoke-static {v0, p1}, Lacm;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lacm;->a:I

    .line 226
    invoke-direct {p0}, Lacm;->g()V

    .line 227
    return-void

    .line 44
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 52
    :array_1
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 53
    :array_2
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput v1, p0, Lacm;->a:I

    .line 43
    iput-boolean v1, p0, Lacm;->b:Z

    .line 44
    new-array v0, v2, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lacm;->c:[F

    .line 45
    iput-boolean v1, p0, Lacm;->d:Z

    .line 46
    const/16 v0, 0x302

    iput v0, p0, Lacm;->e:I

    .line 47
    const/16 v0, 0x303

    iput v0, p0, Lacm;->f:I

    .line 48
    const/4 v0, 0x5

    iput v0, p0, Lacm;->g:I

    .line 49
    iput v2, p0, Lacm;->h:I

    .line 50
    const v0, 0x84c0

    iput v0, p0, Lacm;->i:I

    .line 51
    const/16 v0, 0x4000

    iput v0, p0, Lacm;->j:I

    .line 52
    new-array v0, v3, [F

    fill-array-data v0, :array_1

    iput-object v0, p0, Lacm;->k:[F

    .line 53
    new-array v0, v3, [F

    fill-array-data v0, :array_2

    iput-object v0, p0, Lacm;->l:[F

    .line 56
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lacm;->n:Ljava/util/HashMap;

    .line 230
    invoke-static {p1, p2}, Lacm;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lacm;->a:I

    .line 231
    invoke-direct {p0}, Lacm;->g()V

    .line 232
    return-void

    .line 44
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x0
    .end array-data

    .line 52
    :array_1
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 53
    :array_2
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private static a(ILjava/lang/String;)I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 690
    invoke-static {p0}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v0

    .line 691
    if-eqz v0, :cond_0

    .line 692
    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    .line 693
    invoke-static {v0}, Landroid/opengl/GLES20;->glCompileShader(I)V

    .line 694
    const/4 v1, 0x1

    new-array v1, v1, [I

    .line 695
    const v2, 0x8b81

    invoke-static {v0, v2, v1, v3}, Landroid/opengl/GLES20;->glGetShaderiv(II[II)V

    .line 696
    aget v1, v1, v3

    if-nez v1, :cond_0

    .line 697
    invoke-static {v0}, Landroid/opengl/GLES20;->glGetShaderInfoLog(I)Ljava/lang/String;

    move-result-object v1

    .line 698
    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 699
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not compile shader "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 703
    :cond_0
    return v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 707
    const v0, 0x8b31

    invoke-static {v0, p0}, Lacm;->a(ILjava/lang/String;)I

    move-result v0

    .line 708
    if-nez v0, :cond_0

    .line 709
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not create shader-program as vertex shader could not be compiled!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 712
    :cond_0
    const v1, 0x8b30

    invoke-static {v1, p1}, Lacm;->a(ILjava/lang/String;)I

    move-result v1

    .line 713
    if-nez v1, :cond_1

    .line 714
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not create shader-program as fragment shader could not be compiled!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 718
    :cond_1
    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v2

    .line 719
    if-eqz v2, :cond_2

    .line 720
    invoke-static {v2, v0}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 721
    const-string v3, "glAttachShader"

    invoke-static {v3}, Labi;->a(Ljava/lang/String;)V

    .line 722
    invoke-static {v2, v1}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 723
    const-string v3, "glAttachShader"

    invoke-static {v3}, Labi;->a(Ljava/lang/String;)V

    .line 724
    invoke-static {v2}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    .line 725
    new-array v3, v6, [I

    .line 726
    const v4, 0x8b82

    invoke-static {v2, v4, v3, v5}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    .line 727
    aget v3, v3, v5

    if-eq v3, v6, :cond_2

    .line 728
    invoke-static {v2}, Landroid/opengl/GLES20;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object v0

    .line 729
    invoke-static {v2}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    .line 730
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not link program: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 735
    :cond_2
    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 736
    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 738
    return v2
.end method

.method static synthetic a([B)I
    .locals 2

    .prologue
    .line 40
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    aget-byte v1, p0, v0

    if-nez v1, :cond_0

    :goto_1
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    array-length v0, p0

    goto :goto_1
.end method

.method public static a()Lacm;
    .locals 2

    .prologue
    .line 235
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private a(Ljava/lang/String;Z)Lacn;
    .locals 3

    .prologue
    .line 754
    iget-object v0, p0, Lacm;->m:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacn;

    .line 755
    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 756
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown uniform \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 758
    :cond_0
    return-object v0
.end method

.method private a(Lacn;II)V
    .locals 3

    .prologue
    .line 776
    rem-int v0, p2, p3

    if-eqz v0, :cond_0

    .line 777
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Size mismatch: Attempting to assign values of size "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to uniform \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lacn;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' (must be multiple of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 780
    :cond_0
    invoke-virtual {p1}, Lacn;->d()I

    move-result v0

    div-int v1, p2, p3

    if-eq v0, v1, :cond_1

    .line 781
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Size mismatch: Cannot assign "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " values to uniform \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lacn;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 784
    :cond_1
    return-void
.end method

.method private a([Lada;)V
    .locals 4

    .prologue
    .line 611
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_1

    .line 613
    invoke-virtual {p0}, Lacm;->b()I

    move-result v1

    add-int/2addr v1, v0

    invoke-static {v1}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 616
    aget-object v1, p1, v0

    invoke-virtual {v1}, Lada;->e()V

    .line 619
    iget v1, p0, Lacm;->a:I

    invoke-virtual {p0, v0}, Lacm;->c(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v1

    .line 620
    if-ltz v1, :cond_0

    .line 621
    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 627
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Binding input texture "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Labi;->a(Ljava/lang/String;)V

    .line 611
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 623
    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Shader does not seem to support "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " number of input textures! Missing uniform "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0, v0}, Lacm;->c(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 629
    :cond_1
    return-void
.end method

.method private b(Ljava/lang/String;Z)Laco;
    .locals 3

    .prologue
    .line 762
    iget-object v0, p0, Lacm;->n:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laco;

    .line 763
    if-nez v0, :cond_0

    .line 764
    iget v1, p0, Lacm;->a:I

    invoke-static {v1, p1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v1

    .line 765
    if-ltz v1, :cond_1

    .line 766
    new-instance v0, Laco;

    invoke-direct {v0, p1, v1}, Laco;-><init>(Ljava/lang/String;I)V

    .line 767
    iget-object v1, p0, Lacm;->n:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 772
    :cond_0
    return-object v0

    .line 768
    :cond_1
    if-eqz p2, :cond_0

    .line 769
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown attribute \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private f()V
    .locals 1

    .prologue
    .line 678
    iget v0, p0, Lacm;->a:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 679
    const-string v0, "glUseProgram"

    invoke-static {v0}, Labi;->a(Ljava/lang/String;)V

    .line 680
    return-void
.end method

.method private g()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 742
    const/4 v0, 0x1

    new-array v2, v0, [I

    .line 743
    iget v0, p0, Lacm;->a:I

    const v3, 0x8b86

    invoke-static {v0, v3, v2, v1}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    .line 744
    aget v0, v2, v1

    if-lez v0, :cond_0

    .line 745
    new-instance v0, Ljava/util/HashMap;

    aget v3, v2, v1

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lacm;->m:Ljava/util/HashMap;

    move v0, v1

    .line 746
    :goto_0
    aget v3, v2, v1

    if-ge v0, v3, :cond_0

    .line 747
    new-instance v3, Lacn;

    iget v4, p0, Lacm;->a:I

    invoke-direct {v3, v4, v0}, Lacn;-><init>(II)V

    .line 748
    iget-object v4, p0, Lacm;->m:Ljava/util/HashMap;

    invoke-virtual {v3}, Lacn;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 746
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 751
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 302
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lacm;->a(Ljava/lang/String;Z)Lacn;

    move-result-object v0

    invoke-virtual {v0}, Lacn;->c()I

    move-result v0

    return v0
.end method

.method public a(FFFF)V
    .locals 3

    .prologue
    .line 412
    const/16 v0, 0x8

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    const/4 v1, 0x2

    add-float v2, p1, p3

    aput v2, v0, v1

    const/4 v1, 0x3

    aput p2, v0, v1

    const/4 v1, 0x4

    aput p1, v0, v1

    const/4 v1, 0x5

    add-float v2, p2, p4

    aput v2, v0, v1

    const/4 v1, 0x6

    add-float v2, p1, p3

    aput v2, v0, v1

    const/4 v1, 0x7

    add-float v2, p2, p4

    aput v2, v0, v1

    invoke-virtual {p0, v0}, Lacm;->a([F)V

    .line 413
    return-void
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 542
    iput p1, p0, Lacm;->g:I

    .line 543
    return-void
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 537
    iput p1, p0, Lacm;->e:I

    .line 538
    iput p2, p0, Lacm;->f:I

    .line 539
    return-void
.end method

.method public a(Laas;)V
    .locals 3

    .prologue
    .line 292
    invoke-virtual {p1}, Laas;->m()Lacw;

    move-result-object v0

    .line 293
    invoke-virtual {p1}, Laas;->j()I

    move-result v1

    invoke-virtual {p1}, Laas;->k()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lacm;->a(Lacw;II)V

    .line 294
    invoke-virtual {p1}, Laas;->h()V

    .line 295
    return-void
.end method

.method public a(Laas;Laas;)V
    .locals 4

    .prologue
    .line 251
    invoke-virtual {p1}, Laas;->l()Lada;

    move-result-object v0

    .line 252
    invoke-virtual {p2}, Laas;->m()Lacw;

    move-result-object v1

    .line 253
    const/4 v2, 0x1

    new-array v2, v2, [Lada;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p2}, Laas;->j()I

    move-result v0

    invoke-virtual {p2}, Laas;->k()I

    move-result v3

    invoke-virtual {p0, v2, v1, v0, v3}, Lacm;->a([Lada;Lacw;II)V

    .line 257
    invoke-virtual {p1}, Laas;->h()V

    .line 258
    invoke-virtual {p2}, Laas;->h()V

    .line 259
    return-void
.end method

.method public a(Lacw;II)V
    .locals 1

    .prologue
    .line 298
    const/4 v0, 0x0

    new-array v0, v0, [Lada;

    invoke-virtual {p0, v0, p1, p2, p3}, Lacm;->a([Lada;Lacw;II)V

    .line 299
    return-void
.end method

.method public a(Lada;Lacw;II)V
    .locals 2

    .prologue
    .line 278
    const/4 v0, 0x1

    new-array v0, v0, [Lada;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, v0, p2, p3, p4}, Lacm;->a([Lada;Lacw;II)V

    .line 279
    return-void
.end method

.method public a(Ladp;)V
    .locals 3

    .prologue
    .line 420
    const/16 v0, 0x8

    new-array v0, v0, [F

    const/4 v1, 0x0

    invoke-virtual {p1}, Ladp;->a()Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->x:F

    aput v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p1}, Ladp;->a()Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->y:F

    aput v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p1}, Ladp;->b()Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->x:F

    aput v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p1}, Ladp;->b()Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->y:F

    aput v2, v0, v1

    const/4 v1, 0x4

    invoke-virtual {p1}, Ladp;->c()Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->x:F

    aput v2, v0, v1

    const/4 v1, 0x5

    invoke-virtual {p1}, Ladp;->c()Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->y:F

    aput v2, v0, v1

    const/4 v1, 0x6

    invoke-virtual {p1}, Ladp;->d()Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->x:F

    aput v2, v0, v1

    const/4 v1, 0x7

    invoke-virtual {p1}, Ladp;->d()Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->y:F

    aput v2, v0, v1

    invoke-virtual {p0, v0}, Lacm;->a([F)V

    .line 424
    return-void
.end method

.method public a(Landroid/graphics/RectF;)V
    .locals 5

    .prologue
    .line 416
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->top:F

    iget v2, p1, Landroid/graphics/RectF;->right:F

    iget v3, p1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v2, v3

    iget v3, p1, Landroid/graphics/RectF;->bottom:F

    iget v4, p1, Landroid/graphics/RectF;->top:F

    sub-float/2addr v3, v4

    invoke-virtual {p0, v0, v1, v2, v3}, Lacm;->a(FFFF)V

    .line 417
    return-void
.end method

.method public a(Ljava/lang/String;F)V
    .locals 2

    .prologue
    .line 325
    invoke-direct {p0}, Lacm;->f()V

    .line 326
    invoke-virtual {p0, p1}, Lacm;->a(Ljava/lang/String;)I

    move-result v0

    .line 327
    invoke-static {v0, p2}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 328
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Set uniform value ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Labi;->a(Ljava/lang/String;)V

    .line 329
    return-void
.end method

.method public a(Ljava/lang/String;[F)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 361
    invoke-direct {p0, p1, v4}, Lacm;->a(Ljava/lang/String;Z)Lacn;

    move-result-object v0

    .line 362
    invoke-direct {p0}, Lacm;->f()V

    .line 363
    array-length v1, p2

    .line 364
    invoke-virtual {v0}, Lacn;->b()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 394
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot assign float-array to incompatible uniform type for uniform \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 366
    :sswitch_0
    invoke-direct {p0, v0, v1, v4}, Lacm;->a(Lacn;II)V

    .line 367
    invoke-virtual {v0}, Lacn;->c()I

    move-result v0

    invoke-static {v0, v1, p2, v3}, Landroid/opengl/GLES20;->glUniform1fv(II[FI)V

    .line 397
    :goto_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Set uniform value ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Labi;->a(Ljava/lang/String;)V

    .line 398
    return-void

    .line 370
    :sswitch_1
    const/4 v2, 0x2

    invoke-direct {p0, v0, v1, v2}, Lacm;->a(Lacn;II)V

    .line 371
    invoke-virtual {v0}, Lacn;->c()I

    move-result v0

    div-int/lit8 v1, v1, 0x2

    invoke-static {v0, v1, p2, v3}, Landroid/opengl/GLES20;->glUniform2fv(II[FI)V

    goto :goto_0

    .line 374
    :sswitch_2
    const/4 v2, 0x3

    invoke-direct {p0, v0, v1, v2}, Lacm;->a(Lacn;II)V

    .line 375
    invoke-virtual {v0}, Lacn;->c()I

    move-result v0

    div-int/lit8 v1, v1, 0x3

    invoke-static {v0, v1, p2, v3}, Landroid/opengl/GLES20;->glUniform3fv(II[FI)V

    goto :goto_0

    .line 378
    :sswitch_3
    invoke-direct {p0, v0, v1, v5}, Lacm;->a(Lacn;II)V

    .line 379
    invoke-virtual {v0}, Lacn;->c()I

    move-result v0

    div-int/lit8 v1, v1, 0x4

    invoke-static {v0, v1, p2, v3}, Landroid/opengl/GLES20;->glUniform4fv(II[FI)V

    goto :goto_0

    .line 382
    :sswitch_4
    invoke-direct {p0, v0, v1, v5}, Lacm;->a(Lacn;II)V

    .line 383
    invoke-virtual {v0}, Lacn;->c()I

    move-result v0

    div-int/lit8 v1, v1, 0x4

    invoke-static {v0, v1, v3, p2, v3}, Landroid/opengl/GLES20;->glUniformMatrix2fv(IIZ[FI)V

    goto :goto_0

    .line 386
    :sswitch_5
    const/16 v2, 0x9

    invoke-direct {p0, v0, v1, v2}, Lacm;->a(Lacn;II)V

    .line 387
    invoke-virtual {v0}, Lacn;->c()I

    move-result v0

    div-int/lit8 v1, v1, 0x9

    invoke-static {v0, v1, v3, p2, v3}, Landroid/opengl/GLES20;->glUniformMatrix3fv(IIZ[FI)V

    goto :goto_0

    .line 390
    :sswitch_6
    const/16 v2, 0x10

    invoke-direct {p0, v0, v1, v2}, Lacm;->a(Lacn;II)V

    .line 391
    invoke-virtual {v0}, Lacn;->c()I

    move-result v0

    div-int/lit8 v1, v1, 0x10

    invoke-static {v0, v1, v3, p2, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    goto :goto_0

    .line 364
    :sswitch_data_0
    .sparse-switch
        0x1406 -> :sswitch_0
        0x8b50 -> :sswitch_1
        0x8b51 -> :sswitch_2
        0x8b52 -> :sswitch_3
        0x8b5a -> :sswitch_4
        0x8b5b -> :sswitch_5
        0x8b5c -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Ljava/lang/String;[FI)V
    .locals 6

    .prologue
    .line 401
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lacm;->b(Ljava/lang/String;Z)Laco;

    move-result-object v0

    .line 402
    const/4 v1, 0x0

    mul-int/lit8 v2, p3, 0x4

    const/16 v4, 0x1406

    move v3, p3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Laco;->a(ZIII[F)V

    .line 403
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 505
    iput-boolean p1, p0, Lacm;->b:Z

    .line 506
    return-void
.end method

.method public a([F)V
    .locals 3

    .prologue
    const/16 v1, 0x8

    .line 427
    array-length v0, p1

    if-eq v0, v1, :cond_0

    .line 428
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected 8 coordinates as source coordinates but got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " coordinates!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 431
    :cond_0
    invoke-static {p1, v1}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v0

    iput-object v0, p0, Lacm;->k:[F

    .line 432
    return-void
.end method

.method public a([Laas;Laas;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 262
    array-length v0, p1

    new-array v2, v0, [Lada;

    move v0, v1

    .line 263
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_0

    .line 264
    aget-object v3, p1, v0

    invoke-virtual {v3}, Laas;->l()Lada;

    move-result-object v3

    aput-object v3, v2, v0

    .line 263
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 266
    :cond_0
    invoke-virtual {p2}, Laas;->m()Lacw;

    move-result-object v0

    .line 267
    invoke-virtual {p2}, Laas;->j()I

    move-result v3

    invoke-virtual {p2}, Laas;->k()I

    move-result v4

    invoke-virtual {p0, v2, v0, v3, v4}, Lacm;->a([Lada;Lacw;II)V

    .line 271
    array-length v0, p1

    :goto_1
    if-ge v1, v0, :cond_1

    aget-object v2, p1, v1

    .line 272
    invoke-virtual {v2}, Laas;->h()V

    .line 271
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 274
    :cond_1
    invoke-virtual {p2}, Laas;->h()V

    .line 275
    return-void
.end method

.method public a([Lada;Lacw;II)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 282
    const-string v0, "Unknown Operation"

    invoke-static {v0}, Labi;->a(Ljava/lang/String;)V

    .line 283
    iget v0, p0, Lacm;->a:I

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempting to execute invalid shader-program!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 284
    :cond_0
    array-length v0, p1

    const v1, 0x8b4d

    if-le v0, v1, :cond_1

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Number of textures passed ("

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") exceeds the maximum number of allowed texture units (35661"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ")!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 285
    :cond_1
    invoke-virtual {p2}, Lacw;->d()V

    invoke-static {v2, v2, p3, p4}, Landroid/opengl/GLES20;->glViewport(IIII)V

    const-string v0, "glViewport"

    invoke-static {v0}, Labi;->a(Ljava/lang/String;)V

    .line 286
    invoke-virtual {p0}, Lacm;->e()V

    .line 287
    invoke-direct {p0, p1}, Lacm;->a([Lada;)V

    .line 288
    const-string v0, "glDrawArrays"

    invoke-static {v0}, Laep;->a(Ljava/lang/String;)V

    iget v0, p0, Lacm;->g:I

    iget v1, p0, Lacm;->h:I

    invoke-static {v0, v2, v1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    const-string v0, "glDrawArrays"

    invoke-static {v0}, Labi;->a(Ljava/lang/String;)V

    invoke-static {}, Laep;->a()V

    .line 289
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 562
    iget v0, p0, Lacm;->i:I

    return v0
.end method

.method public b(FFFF)V
    .locals 3

    .prologue
    .line 454
    const/16 v0, 0x8

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput p1, v0, v1

    const/4 v1, 0x1

    aput p2, v0, v1

    const/4 v1, 0x2

    add-float v2, p1, p3

    aput v2, v0, v1

    const/4 v1, 0x3

    aput p2, v0, v1

    const/4 v1, 0x4

    aput p1, v0, v1

    const/4 v1, 0x5

    add-float v2, p2, p4

    aput v2, v0, v1

    const/4 v1, 0x6

    add-float v2, p1, p3

    aput v2, v0, v1

    const/4 v1, 0x7

    add-float v2, p2, p4

    aput v2, v0, v1

    invoke-virtual {p0, v0}, Lacm;->c([F)V

    .line 458
    return-void
.end method

.method public b(I)V
    .locals 0

    .prologue
    .line 550
    iput p1, p0, Lacm;->h:I

    .line 551
    return-void
.end method

.method public b(Ladp;)V
    .locals 3

    .prologue
    .line 468
    const/16 v0, 0x8

    new-array v0, v0, [F

    const/4 v1, 0x0

    invoke-virtual {p1}, Ladp;->a()Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->x:F

    aput v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p1}, Ladp;->a()Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->y:F

    aput v2, v0, v1

    const/4 v1, 0x2

    invoke-virtual {p1}, Ladp;->b()Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->x:F

    aput v2, v0, v1

    const/4 v1, 0x3

    invoke-virtual {p1}, Ladp;->b()Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->y:F

    aput v2, v0, v1

    const/4 v1, 0x4

    invoke-virtual {p1}, Ladp;->c()Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->x:F

    aput v2, v0, v1

    const/4 v1, 0x5

    invoke-virtual {p1}, Ladp;->c()Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->y:F

    aput v2, v0, v1

    const/4 v1, 0x6

    invoke-virtual {p1}, Ladp;->d()Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->x:F

    aput v2, v0, v1

    const/4 v1, 0x7

    invoke-virtual {p1}, Ladp;->d()Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->y:F

    aput v2, v0, v1

    invoke-virtual {p0, v0}, Lacm;->c([F)V

    .line 472
    return-void
.end method

.method public b(Landroid/graphics/RectF;)V
    .locals 3

    .prologue
    .line 461
    const/16 v0, 0x8

    new-array v0, v0, [F

    const/4 v1, 0x0

    iget v2, p1, Landroid/graphics/RectF;->left:F

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p1, Landroid/graphics/RectF;->top:F

    aput v2, v0, v1

    const/4 v1, 0x2

    iget v2, p1, Landroid/graphics/RectF;->right:F

    aput v2, v0, v1

    const/4 v1, 0x3

    iget v2, p1, Landroid/graphics/RectF;->top:F

    aput v2, v0, v1

    const/4 v1, 0x4

    iget v2, p1, Landroid/graphics/RectF;->left:F

    aput v2, v0, v1

    const/4 v1, 0x5

    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    aput v2, v0, v1

    const/4 v1, 0x6

    iget v2, p1, Landroid/graphics/RectF;->right:F

    aput v2, v0, v1

    const/4 v1, 0x7

    iget v2, p1, Landroid/graphics/RectF;->bottom:F

    aput v2, v0, v1

    invoke-virtual {p0, v0}, Lacm;->c([F)V

    .line 465
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 529
    iput-boolean p1, p0, Lacm;->d:Z

    .line 530
    return-void
.end method

.method public b([F)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/16 v5, 0xd

    const/16 v4, 0xc

    .line 435
    array-length v0, p1

    const/16 v1, 0x10

    if-eq v0, v1, :cond_0

    .line 436
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Expected 4x4 matrix for source transform!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 438
    :cond_0
    const/16 v0, 0x8

    new-array v0, v0, [F

    aget v1, p1, v4

    aput v1, v0, v6

    aget v1, p1, v5

    aput v1, v0, v7

    const/4 v1, 0x2

    aget v2, p1, v6

    aget v3, p1, v4

    add-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x3

    aget v2, p1, v7

    aget v3, p1, v5

    add-float/2addr v2, v3

    aput v2, v0, v1

    aget v1, p1, v8

    aget v2, p1, v4

    add-float/2addr v1, v2

    aput v1, v0, v8

    const/4 v1, 0x5

    const/4 v2, 0x5

    aget v2, p1, v2

    aget v3, p1, v5

    add-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x6

    aget v2, p1, v6

    aget v3, p1, v8

    add-float/2addr v2, v3

    aget v3, p1, v4

    add-float/2addr v2, v3

    aput v2, v0, v1

    const/4 v1, 0x7

    aget v2, p1, v7

    const/4 v3, 0x5

    aget v3, p1, v3

    add-float/2addr v2, v3

    aget v3, p1, v5

    add-float/2addr v2, v3

    aput v2, v0, v1

    invoke-virtual {p0, v0}, Lacm;->a([F)V

    .line 451
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 566
    const-string v0, "a_texcoord"

    return-object v0
.end method

.method public c(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 574
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "tex_sampler_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c([F)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 475
    array-length v0, p1

    if-eq v0, v4, :cond_0

    .line 476
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Expected 8 coordinates as target coordinates but got "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, p1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " coordinates!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 479
    :cond_0
    new-array v0, v4, [F

    iput-object v0, p0, Lacm;->l:[F

    .line 480
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_1

    .line 481
    iget-object v1, p0, Lacm;->l:[F

    aget v2, p1, v0

    const/high16 v3, 0x40000000    # 2.0f

    mul-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    aput v2, v1, v0

    .line 480
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 483
    :cond_1
    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 570
    const-string v0, "a_position"

    return-object v0
.end method

.method public d([F)V
    .locals 0

    .prologue
    .line 513
    iput-object p1, p0, Lacm;->c:[F

    .line 514
    return-void
.end method

.method protected e()V
    .locals 7

    .prologue
    const/16 v4, 0x1406

    const/16 v6, 0xbe2

    const/16 v2, 0x8

    const/4 v3, 0x2

    const/4 v1, 0x0

    .line 587
    invoke-direct {p0}, Lacm;->f()V

    .line 588
    invoke-virtual {p0}, Lacm;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lacm;->b(Ljava/lang/String;Z)Laco;

    move-result-object v0

    iget-object v5, p0, Lacm;->k:[F

    if-eqz v5, :cond_0

    if-eqz v0, :cond_0

    iget-object v5, p0, Lacm;->k:[F

    invoke-virtual/range {v0 .. v5}, Laco;->a(ZIII[F)V

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lacm;->k:[F

    .line 589
    invoke-virtual {p0}, Lacm;->d()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v1}, Lacm;->b(Ljava/lang/String;Z)Laco;

    move-result-object v0

    iget-object v5, p0, Lacm;->l:[F

    if-eqz v5, :cond_1

    if-eqz v0, :cond_1

    iget-object v5, p0, Lacm;->l:[F

    invoke-virtual/range {v0 .. v5}, Laco;->a(ZIII[F)V

    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lacm;->l:[F

    .line 590
    iget-object v0, p0, Lacm;->n:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laco;

    invoke-virtual {v0}, Laco;->a()Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to assign attribute value \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_3
    const-string v0, "Push Attributes"

    invoke-static {v0}, Labi;->a(Ljava/lang/String;)V

    .line 591
    iget-boolean v0, p0, Lacm;->b:Z

    if-eqz v0, :cond_4

    .line 592
    iget-object v0, p0, Lacm;->c:[F

    aget v0, v0, v1

    iget-object v1, p0, Lacm;->c:[F

    const/4 v2, 0x1

    aget v1, v1, v2

    iget-object v2, p0, Lacm;->c:[F

    aget v2, v2, v3

    iget-object v3, p0, Lacm;->c:[F

    const/4 v4, 0x3

    aget v3, v3, v4

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 593
    iget v0, p0, Lacm;->j:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    .line 595
    :cond_4
    iget-boolean v0, p0, Lacm;->d:Z

    if-eqz v0, :cond_5

    .line 596
    invoke-static {v6}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 597
    iget v0, p0, Lacm;->e:I

    iget v1, p0, Lacm;->f:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 601
    :goto_0
    const-string v0, "Set render variables"

    invoke-static {v0}, Labi;->a(Ljava/lang/String;)V

    .line 602
    return-void

    .line 599
    :cond_5
    invoke-static {v6}, Landroid/opengl/GLES20;->glDisable(I)V

    goto :goto_0
.end method

.method protected finalize()V
    .locals 1

    .prologue
    .line 583
    iget v0, p0, Lacm;->a:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    .line 584
    return-void
.end method
