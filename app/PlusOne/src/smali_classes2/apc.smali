.class final Lapc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lanz;


# instance fields
.field private a:Landroid/graphics/Bitmap;

.field private synthetic b:Laol;


# direct methods
.method constructor <init>(Laol;)V
    .locals 0

    .prologue
    .line 1380
    iput-object p1, p0, Lapc;->b:Laol;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lanu;Laob;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 1396
    iget-object v1, p0, Lapc;->b:Laol;

    invoke-static {v1, p1}, Laol;->b(Laol;Landroid/os/AsyncTask;)V

    .line 1398
    iget-object v1, p2, Laob;->d:Lche;

    if-nez v1, :cond_3

    iget-object v1, p2, Laob;->c:Ljfd;

    iget-object v1, v1, Ljfd;->a:Ljff;

    sget-object v2, Ljff;->a:Ljff;

    if-ne v1, v2, :cond_3

    .line 1400
    iget-object v1, p0, Lapc;->b:Laol;

    iget-object v2, p2, Laob;->c:Ljfd;

    iget-object v2, v2, Ljfd;->b:Lood;

    invoke-static {v1, v2}, Laol;->a(Laol;Lood;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1401
    iget-object v1, p0, Lapc;->b:Laol;

    invoke-static {v1}, Laol;->a(Laol;)Lapp;

    move-result-object v1

    invoke-interface {v1}, Lapp;->f()V

    .line 1402
    invoke-static {}, Laol;->q()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Storyboard version too high for client renderer."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1403
    iget-object v1, p0, Lapc;->b:Laol;

    invoke-static {v1}, Laol;->g(Laol;)Laqc;

    move-result-object v1

    invoke-virtual {v1, v0}, Laqc;->a(Z)V

    .line 1439
    :goto_0
    return-void

    .line 1406
    :cond_0
    iget-object v1, p0, Lapc;->b:Laol;

    invoke-static {v1}, Laol;->g(Laol;)Laqc;

    move-result-object v1

    invoke-virtual {v1, v4}, Laqc;->a(Z)V

    .line 1407
    iget-object v1, p2, Laob;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    new-array v2, v1, [Lavz;

    .line 1409
    iget-object v1, p2, Laob;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljej;

    .line 1410
    iget-object v0, v0, Ljej;->a:Landroid/net/Uri;

    invoke-static {v0}, Lavz;->a(Landroid/net/Uri;)Lavz;

    move-result-object v0

    aput-object v0, v2, v1

    .line 1411
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 1412
    goto :goto_1

    .line 1413
    :cond_1
    iget-object v0, p0, Lapc;->b:Laol;

    invoke-static {v0}, Laol;->d(Laol;)Lawc;

    move-result-object v0

    invoke-interface {v0, v2}, Lawc;->a([Lavz;)V

    .line 1414
    iget-object v0, p0, Lapc;->b:Laol;

    invoke-static {v0}, Laol;->d(Laol;)Lawc;

    move-result-object v0

    iget-object v1, p2, Laob;->c:Ljfd;

    invoke-interface {v0, v1}, Lawc;->a(Ljfd;)V

    .line 1415
    iget-object v0, p0, Lapc;->b:Laol;

    invoke-static {v0}, Laol;->d(Laol;)Lawc;

    move-result-object v0

    iget-object v1, p2, Laob;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Lawc;->b(Ljava/util/Map;)V

    .line 1416
    iget-object v0, p0, Lapc;->b:Laol;

    iget-object v1, p2, Laob;->b:Ljava/util/Map;

    invoke-static {v0, v1}, Laol;->a(Laol;Ljava/util/Map;)V

    .line 1417
    iget-object v0, p0, Lapc;->b:Laol;

    invoke-static {v0}, Laol;->m(Laol;)V

    .line 1418
    iget-object v0, p0, Lapc;->b:Laol;

    invoke-static {v0}, Laol;->d(Laol;)Lawc;

    move-result-object v0

    invoke-interface {v0}, Lawc;->be()Ljfd;

    move-result-object v0

    .line 1419
    iget-object v1, p0, Lapc;->b:Laol;

    iget-object v2, v0, Ljfd;->b:Lood;

    invoke-static {v1, v2}, Laol;->b(Laol;Lood;)V

    .line 1420
    iget-object v1, p0, Lapc;->b:Laol;

    iget-object v2, v0, Ljfd;->b:Lood;

    invoke-static {v1, v2}, Laol;->c(Laol;Lood;)V

    .line 1421
    iget-object v1, p0, Lapc;->b:Laol;

    iget-object v2, v0, Ljfd;->b:Lood;

    invoke-static {v1, v2}, Laol;->d(Laol;Lood;)V

    .line 1422
    iget-object v1, v0, Ljfd;->b:Lood;

    iget-object v1, v1, Lood;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1423
    iget-object v1, p0, Lapc;->b:Laol;

    invoke-static {v1}, Laol;->d(Laol;)Lawc;

    move-result-object v1

    iget-object v0, v0, Ljfd;->b:Lood;

    iget-object v0, v0, Lood;->f:Ljava/lang/String;

    invoke-interface {v1, v0}, Lawc;->a(Ljava/lang/String;)V

    .line 1426
    :cond_2
    iget-object v0, p0, Lapc;->b:Laol;

    invoke-static {v0}, Laol;->d(Laol;)Lawc;

    move-result-object v0

    invoke-interface {v0, v4}, Lawc;->f(Z)V

    goto/16 :goto_0

    .line 1427
    :cond_3
    iget-object v1, p2, Laob;->d:Lche;

    if-eqz v1, :cond_5

    .line 1428
    iget-object v1, p0, Lapc;->b:Laol;

    invoke-static {v1}, Laol;->g(Laol;)Laqc;

    move-result-object v1

    invoke-virtual {v1, v0}, Laqc;->a(Z)V

    .line 1429
    iget-object v0, p2, Laob;->d:Lche;

    instance-of v0, v0, Lcga;

    if-eqz v0, :cond_4

    .line 1430
    iget-object v0, p0, Lapc;->b:Laol;

    invoke-static {v0}, Laol;->g(Laol;)Laqc;

    move-result-object v0

    invoke-virtual {v0}, Laqc;->d()V

    goto/16 :goto_0

    .line 1432
    :cond_4
    iget-object v0, p0, Lapc;->b:Laol;

    invoke-static {v0}, Laol;->g(Laol;)Laqc;

    move-result-object v0

    invoke-virtual {v0}, Laqc;->c()V

    goto/16 :goto_0

    .line 1435
    :cond_5
    iget-object v1, p0, Lapc;->b:Laol;

    invoke-static {v1}, Laol;->g(Laol;)Laqc;

    move-result-object v1

    invoke-virtual {v1, v0}, Laqc;->a(Z)V

    .line 1436
    iget-object v0, p0, Lapc;->b:Laol;

    invoke-static {v0}, Laol;->a(Laol;)Lapp;

    move-result-object v0

    invoke-interface {v0}, Lapp;->g()V

    goto/16 :goto_0
.end method

.method public a(Laoa;)V
    .locals 2

    .prologue
    .line 1387
    iget-object v0, p0, Lapc;->a:Landroid/graphics/Bitmap;

    iget-object v1, p1, Laoa;->a:Landroid/graphics/Bitmap;

    if-eq v0, v1, :cond_0

    .line 1388
    iget-object v0, p1, Laoa;->a:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lapc;->a:Landroid/graphics/Bitmap;

    .line 1389
    iget-object v0, p0, Lapc;->b:Laol;

    invoke-static {v0}, Laol;->g(Laol;)Laqc;

    move-result-object v0

    iget-object v1, p0, Lapc;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Laqc;->b(Landroid/graphics/Bitmap;)V

    .line 1391
    :cond_0
    iget-object v0, p0, Lapc;->b:Laol;

    invoke-static {v0}, Laol;->g(Laol;)Laqc;

    move-result-object v0

    iget v1, p1, Laoa;->b:I

    invoke-virtual {v0, v1}, Laqc;->c(I)V

    .line 1392
    return-void
.end method
