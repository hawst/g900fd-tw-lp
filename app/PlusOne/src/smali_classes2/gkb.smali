.class public final Lgkb;
.super Lghh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lghh",
        "<",
        "Lgkl;",
        ">;"
    }
.end annotation


# static fields
.field private static final A:Ljava/lang/Object;

.field private static final B:Ljava/lang/Object;

.field private static final d:Lgkq;


# instance fields
.field private final e:Lcom/google/android/gms/cast/CastDevice;

.field private final f:Lgdt;

.field private final g:Landroid/os/Handler;

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lgdu;",
            ">;"
        }
    .end annotation
.end field

.field private final i:J

.field private j:Lgke;

.field private k:Ljava/lang/String;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:D

.field private q:I

.field private r:I

.field private final s:Ljava/util/concurrent/atomic/AtomicLong;

.field private t:Ljava/lang/String;

.field private u:Ljava/lang/String;

.field private v:Landroid/os/Bundle;

.field private w:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Lgfy",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;>;"
        }
    .end annotation
.end field

.field private x:Lgkd;

.field private y:Lgfy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgfy",
            "<",
            "Lgdm;",
            ">;"
        }
    .end annotation
.end field

.field private z:Lgfy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgfy",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lgkq;

    const-string v1, "CastClientImpl"

    invoke-direct {v0, v1}, Lgkq;-><init>(Ljava/lang/String;)V

    sput-object v0, Lgkb;->d:Lgkq;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lgkb;->A:Ljava/lang/Object;

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lgkb;->B:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/cast/CastDevice;JLgdt;Lggb;Lggc;)V
    .locals 4

    invoke-direct {p0, p1, p2, p7, p8}, Lghh;-><init>(Landroid/content/Context;Landroid/os/Looper;Lggb;Lggc;)V

    iput-object p3, p0, Lgkb;->e:Lcom/google/android/gms/cast/CastDevice;

    iput-object p6, p0, Lgkb;->f:Lgdt;

    iput-wide p4, p0, Lgkb;->i:J

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lgkb;->g:Landroid/os/Handler;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lgkb;->h:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v2, 0x0

    invoke-direct {v0, v2, v3}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lgkb;->s:Ljava/util/concurrent/atomic/AtomicLong;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lgkb;->w:Ljava/util/Map;

    invoke-direct {p0}, Lgkb;->n()V

    new-instance v0, Lgkd;

    invoke-direct {v0, p0}, Lgkd;-><init>(Lgkb;)V

    iput-object v0, p0, Lgkb;->x:Lgkd;

    iget-object v0, p0, Lgkb;->x:Lgkd;

    invoke-virtual {p0, v0}, Lgkb;->a(Lggc;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/cast/ApplicationMetadata;)Lcom/google/android/gms/cast/ApplicationMetadata;
    .locals 0

    return-object p0
.end method

.method static synthetic a(Lgkb;Lgfy;)Lgfy;
    .locals 0

    iput-object p1, p0, Lgkb;->y:Lgfy;

    return-object p1
.end method

.method static synthetic a(Lgkb;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lgkb;->t:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lgkb;)V
    .locals 0

    invoke-direct {p0}, Lgkb;->o()V

    return-void
.end method

.method static synthetic a(Lgkb;Lcom/google/android/gms/internal/ig;)V
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/internal/ig;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lgkb;->k:Ljava/lang/String;

    invoke-static {v0, v3}, Lgkj;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    iput-object v0, p0, Lgkb;->k:Ljava/lang/String;

    move v0, v1

    :goto_0
    sget-object v3, Lgkb;->d:Lgkq;

    const-string v4, "hasChanged=%b, mFirstApplicationStatusUpdate=%b"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-boolean v6, p0, Lgkb;->m:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Lgkq;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lgkb;->f:Lgdt;

    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lgkb;->m:Z

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lgkb;->f:Lgdt;

    :cond_1
    iput-boolean v2, p0, Lgkb;->m:Z

    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method static synthetic a(Lgkb;Lcom/google/android/gms/internal/il;)V
    .locals 9

    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/internal/il;->f()Lcom/google/android/gms/cast/ApplicationMetadata;

    invoke-virtual {p1}, Lcom/google/android/gms/internal/il;->b()D

    move-result-wide v4

    const-wide/high16 v6, 0x7ff8000000000000L    # NaN

    cmpl-double v0, v4, v6

    if-eqz v0, :cond_9

    iget-wide v6, p0, Lgkb;->p:D

    cmpl-double v0, v4, v6

    if-eqz v0, :cond_9

    iput-wide v4, p0, Lgkb;->p:D

    move v0, v1

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/internal/il;->c()Z

    move-result v3

    iget-boolean v4, p0, Lgkb;->l:Z

    if-eq v3, v4, :cond_0

    iput-boolean v3, p0, Lgkb;->l:Z

    move v0, v1

    :cond_0
    sget-object v3, Lgkb;->d:Lgkq;

    const-string v4, "hasVolumeChanged=%b, mFirstDeviceStatusUpdate=%b"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-boolean v6, p0, Lgkb;->n:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Lgkq;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v3, p0, Lgkb;->f:Lgdt;

    if-eqz v3, :cond_2

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lgkb;->n:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lgkb;->f:Lgdt;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/internal/il;->d()I

    move-result v0

    iget v3, p0, Lgkb;->q:I

    if-eq v0, v3, :cond_8

    iput v0, p0, Lgkb;->q:I

    move v0, v1

    :goto_1
    sget-object v3, Lgkb;->d:Lgkq;

    const-string v4, "hasActiveInputChanged=%b, mFirstDeviceStatusUpdate=%b"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-boolean v6, p0, Lgkb;->n:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Lgkq;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v3, p0, Lgkb;->f:Lgdt;

    if-eqz v3, :cond_4

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lgkb;->n:Z

    if-eqz v0, :cond_4

    :cond_3
    iget-object v0, p0, Lgkb;->f:Lgdt;

    iget v0, p0, Lgkb;->q:I

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/internal/il;->e()I

    move-result v0

    iget v3, p0, Lgkb;->r:I

    if-eq v0, v3, :cond_7

    iput v0, p0, Lgkb;->r:I

    move v0, v1

    :goto_2
    sget-object v3, Lgkb;->d:Lgkq;

    const-string v4, "hasStandbyStateChanged=%b, mFirstDeviceStatusUpdate=%b"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v2

    iget-boolean v6, p0, Lgkb;->n:Z

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v3, v4, v5}, Lgkq;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lgkb;->f:Lgdt;

    if-eqz v1, :cond_6

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lgkb;->n:Z

    if-eqz v0, :cond_6

    :cond_5
    iget-object v0, p0, Lgkb;->f:Lgdt;

    iget v0, p0, Lgkb;->r:I

    :cond_6
    iput-boolean v2, p0, Lgkb;->n:Z

    return-void

    :cond_7
    move v0, v2

    goto :goto_2

    :cond_8
    move v0, v2

    goto :goto_1

    :cond_9
    move v0, v2

    goto/16 :goto_0
.end method

.method static synthetic b(Lgkb;Lgfy;)Lgfy;
    .locals 0

    iput-object p1, p0, Lgkb;->z:Lgfy;

    return-object p1
.end method

.method static synthetic b(Lgkb;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lgkb;->u:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lgkb;)V
    .locals 0

    invoke-direct {p0}, Lgkb;->n()V

    return-void
.end method

.method static synthetic c(Lgkb;)Lgfy;
    .locals 1

    iget-object v0, p0, Lgkb;->y:Lgfy;

    return-object v0
.end method

.method static synthetic d(Lgkb;)Lgdt;
    .locals 1

    iget-object v0, p0, Lgkb;->f:Lgdt;

    return-object v0
.end method

.method static synthetic e(Lgkb;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lgkb;->g:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic f(Lgkb;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lgkb;->h:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic g(Lgkb;)Lcom/google/android/gms/cast/CastDevice;
    .locals 1

    iget-object v0, p0, Lgkb;->e:Lcom/google/android/gms/cast/CastDevice;

    return-object v0
.end method

.method static synthetic h(Lgkb;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lgkb;->w:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic i(Lgkb;)Lgfy;
    .locals 1

    iget-object v0, p0, Lgkb;->z:Lgfy;

    return-object v0
.end method

.method static synthetic k()Lgkq;
    .locals 1

    sget-object v0, Lgkb;->d:Lgkq;

    return-object v0
.end method

.method static synthetic l()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lgkb;->A:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic m()Ljava/lang/Object;
    .locals 1

    sget-object v0, Lgkb;->B:Ljava/lang/Object;

    return-object v0
.end method

.method private n()V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, -0x1

    iput-boolean v2, p0, Lgkb;->o:Z

    iput v0, p0, Lgkb;->q:I

    iput v0, p0, Lgkb;->r:I

    const/4 v0, 0x0

    iput-object v0, p0, Lgkb;->k:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lgkb;->p:D

    iput-boolean v2, p0, Lgkb;->l:Z

    return-void
.end method

.method private o()V
    .locals 3

    sget-object v0, Lgkb;->d:Lgkq;

    const-string v1, "removing all MessageReceivedCallbacks"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lgkq;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v1, p0, Lgkb;->h:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lgkb;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private p()V
    .locals 2

    iget-boolean v0, p0, Lgkb;->o:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgkb;->j:Lgke;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgkb;->j:Lgke;

    invoke-virtual {v0}, Lgke;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected to a device"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method


# virtual methods
.method protected synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, Lgkb;->b(Landroid/os/IBinder;)Lgkl;

    move-result-object v0

    return-object v0
.end method

.method public a(D)V
    .locals 7

    invoke-static {p1, p2}, Ljava/lang/Double;->isInfinite(D)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1, p2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Volume cannot be "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {p0}, Lgkb;->i()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lgkl;

    iget-wide v4, p0, Lgkb;->p:D

    iget-boolean v6, p0, Lgkb;->l:Z

    move-wide v2, p1

    invoke-interface/range {v1 .. v6}, Lgkl;->a(DDZ)V

    return-void
.end method

.method protected a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 7

    const/16 v6, 0x3e9

    const/4 v0, 0x0

    const/4 v5, 0x1

    sget-object v1, Lgkb;->d:Lgkq;

    const-string v2, "in onPostInitHandler; statusCode=%d"

    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, Lgkq;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    if-ne p1, v6, :cond_2

    :cond_0
    iput-boolean v5, p0, Lgkb;->o:Z

    iput-boolean v5, p0, Lgkb;->m:Z

    iput-boolean v5, p0, Lgkb;->n:Z

    :goto_0
    if-ne p1, v6, :cond_1

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    iput-object v1, p0, Lgkb;->v:Landroid/os/Bundle;

    iget-object v1, p0, Lgkb;->v:Landroid/os/Bundle;

    const-string v2, "com.google.android.gms.cast.EXTRA_APP_NO_LONGER_RUNNING"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move p1, v0

    :cond_1
    invoke-super {p0, p1, p2, p3}, Lghh;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    return-void

    :cond_2
    iput-boolean v0, p0, Lgkb;->o:Z

    goto :goto_0
.end method

.method protected a(Lgic;Lghl;)V
    .locals 6

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    sget-object v0, Lgkb;->d:Lgkq;

    const-string v1, "getServiceFromBroker(): mLastApplicationId=%s, mLastSessionId=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lgkb;->t:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lgkb;->u:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lgkq;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lgkb;->e:Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/cast/CastDevice;->a(Landroid/os/Bundle;)V

    const-string v0, "com.google.android.gms.cast.EXTRA_CAST_FLAGS"

    iget-wide v2, p0, Lgkb;->i:J

    invoke-virtual {v5, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-object v0, p0, Lgkb;->t:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "last_application_id"

    iget-object v1, p0, Lgkb;->t:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lgkb;->u:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "last_session_id"

    iget-object v1, p0, Lgkb;->u:Ljava/lang/String;

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    new-instance v0, Lgke;

    invoke-direct {v0, p0}, Lgke;-><init>(Lgkb;)V

    iput-object v0, p0, Lgkb;->j:Lgke;

    const v2, 0x5e3530

    iget-object v0, p0, Lghh;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lgkb;->j:Lgke;

    invoke-virtual {v0}, Lgke;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Lgic;->a(Lghz;ILjava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 5

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Channel namespace cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Lgkb;->h:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lgkb;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgdu;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :try_start_1
    invoke-virtual {p0}, Lgkb;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lgkl;

    invoke-interface {v0, p1}, Lgkl;->c(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_1
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_0
    move-exception v0

    sget-object v1, Lgkb;->d:Lgkq;

    const-string v2, "Error unregistering namespace (%s): %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lgkq;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;Lgfy;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/cast/LaunchOptions;",
            "Lgfy",
            "<",
            "Lgdm;",
            ">;)V"
        }
    .end annotation

    sget-object v1, Lgkb;->A:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lgkb;->y:Lgfy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgkb;->y:Lgfy;

    new-instance v2, Lgkc;

    new-instance v3, Lcom/google/android/gms/common/api/Status;

    const/16 v4, 0x7d2

    invoke-direct {v3, v4}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lgkc;-><init>(Lcom/google/android/gms/common/api/Status;B)V

    invoke-interface {v0, v2}, Lgfy;->a(Ljava/lang/Object;)V

    :cond_0
    iput-object p3, p0, Lgkb;->y:Lgfy;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lgkb;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lgkl;

    invoke-interface {v0, p1, p2}, Lgkl;->a(Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;Lgdu;)V
    .locals 2

    invoke-static {p1}, Lgkj;->a(Ljava/lang/String;)V

    invoke-virtual {p0, p1}, Lgkb;->a(Ljava/lang/String;)V

    if-eqz p2, :cond_0

    iget-object v1, p0, Lgkb;->h:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lgkb;->h:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {p0}, Lgkb;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lgkl;

    invoke-interface {v0, p1}, Lgkl;->b(Ljava/lang/String;)V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lgfy;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lgfy",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;)V"
        }
    .end annotation

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The message payload cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    const/high16 v1, 0x10000

    if-le v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Message exceeds maximum size"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p1}, Lgkj;->a(Ljava/lang/String;)V

    invoke-direct {p0}, Lgkb;->p()V

    iget-object v0, p0, Lgkb;->s:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->incrementAndGet()J

    move-result-wide v2

    :try_start_0
    iget-object v0, p0, Lgkb;->w:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p0}, Lgkb;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lgkl;

    invoke-interface {v0, p1, p2, v2, v3}, Lgkl;->a(Ljava/lang/String;Ljava/lang/String;J)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lgkb;->w:Ljava/util/Map;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    throw v0
.end method

.method public aH_()Landroid/os/Bundle;
    .locals 2

    iget-object v0, p0, Lgkb;->v:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgkb;->v:Landroid/os/Bundle;

    const/4 v1, 0x0

    iput-object v1, p0, Lgkb;->v:Landroid/os/Bundle;

    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lghh;->aH_()Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method protected b(Landroid/os/IBinder;)Lgkl;
    .locals 1

    invoke-static {p1}, Lgkm;->a(Landroid/os/IBinder;)Lgkl;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    sget-object v0, Lgkb;->d:Lgkq;

    const-string v1, "disconnect(); ServiceListener=%s, isConnected=%b"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lgkb;->j:Lgke;

    aput-object v3, v2, v4

    invoke-virtual {p0}, Lgkb;->c()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Lgkq;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lgkb;->j:Lgke;

    const/4 v1, 0x0

    iput-object v1, p0, Lgkb;->j:Lgke;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lgke;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lgkb;->d:Lgkq;

    const-string v1, "already disposed, so short-circuiting"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lgkq;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lgkb;->o()V

    :try_start_0
    invoke-virtual {p0}, Lgkb;->c()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lgkb;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    invoke-virtual {p0}, Lgkb;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lgkl;

    invoke-interface {v0}, Lgkl;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_3
    invoke-super {p0}, Lghh;->b()V

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    sget-object v1, Lgkb;->d:Lgkq;

    const-string v2, "Error while disconnecting the controller interface: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lgkq;->c(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-super {p0}, Lghh;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-super {p0}, Lghh;->b()V

    throw v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.cast.service.BIND_CAST_DEVICE_CONTROLLER_SERVICE"

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.cast.internal.ICastDeviceController"

    return-object v0
.end method

.method public j()D
    .locals 2

    invoke-direct {p0}, Lgkb;->p()V

    iget-wide v0, p0, Lgkb;->p:D

    return-wide v0
.end method
