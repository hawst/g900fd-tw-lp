.class final Ljzi;
.super Landroid/widget/ArrayAdapter;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lkav;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lkav;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 64
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 68
    check-cast p2, Lcom/google/android/libraries/social/poll/views/PollOptionVoterListItemView;

    .line 69
    if-nez p2, :cond_0

    .line 70
    invoke-static {p3}, Lcom/google/android/libraries/social/poll/views/PollOptionVoterListItemView;->a(Landroid/view/ViewGroup;)Lcom/google/android/libraries/social/poll/views/PollOptionVoterListItemView;

    move-result-object p2

    .line 72
    :cond_0
    invoke-virtual {p0, p1}, Ljzi;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkav;

    .line 73
    invoke-virtual {v0}, Lkav;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lkav;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Lcom/google/android/libraries/social/poll/views/PollOptionVoterListItemView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 74
    invoke-virtual {v0}, Lkav;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/poll/views/PollOptionVoterListItemView;->a(Ljava/lang/String;)V

    .line 75
    return-object p2
.end method
