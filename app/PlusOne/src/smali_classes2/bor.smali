.class public final Lbor;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lbor;",
            ">;"
        }
    .end annotation
.end field

.field private static final a:[Lbov;

.field private static final b:[Lbot;


# instance fields
.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbov;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbot;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lbox;

.field private f:Z

.field private g:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    new-array v0, v1, [Lbov;

    sput-object v0, Lbor;->a:[Lbov;

    .line 30
    new-array v0, v1, [Lbot;

    sput-object v0, Lbor;->b:[Lbot;

    .line 339
    new-instance v0, Lbos;

    invoke-direct {v0}, Lbos;-><init>()V

    sput-object v0, Lbor;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbor;->c:Ljava/util/List;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbor;->d:Ljava/util/List;

    .line 52
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbor;->g:J

    .line 53
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v3

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbor;->c:Ljava/util/List;

    .line 58
    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v0, v3, v2

    .line 59
    iget-object v5, p0, Lbor;->c:Ljava/util/List;

    check-cast v0, Lbov;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 58
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 61
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v3

    .line 62
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbor;->d:Ljava/util/List;

    .line 63
    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v0, v3, v2

    .line 64
    iget-object v5, p0, Lbor;->d:Ljava/util/List;

    check-cast v0, Lbot;

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 66
    :cond_1
    const-class v0, Lbox;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lbox;

    iput-object v0, p0, Lbor;->e:Lbox;

    .line 67
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Lbor;->b(J)J

    move-result-wide v2

    iput-wide v2, p0, Lbor;->g:J

    .line 68
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    iput-boolean v0, p0, Lbor;->f:Z

    .line 69
    return-void

    :cond_2
    move v0, v1

    .line 68
    goto :goto_2
.end method

.method public constructor <init>(Lbua;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iget-wide v2, p1, Lbua;->a:J

    invoke-static {v2, v3}, Lbor;->b(J)J

    move-result-wide v2

    iput-wide v2, p0, Lbor;->g:J

    .line 78
    iget-boolean v1, p1, Lbua;->e:Z

    iput-boolean v1, p0, Lbor;->f:Z

    .line 80
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p1, Lbua;->b:[Lbub;

    array-length v2, v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lbor;->d:Ljava/util/List;

    .line 81
    iget-object v2, p1, Lbua;->b:[Lbub;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 82
    iget-object v5, p0, Lbor;->d:Ljava/util/List;

    invoke-static {v4}, Lbot;->a(Lbub;)Lbot;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 85
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, p1, Lbua;->c:[Lbuc;

    array-length v2, v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lbor;->c:Ljava/util/List;

    .line 86
    iget-object v1, p1, Lbua;->c:[Lbuc;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 87
    iget-object v4, p0, Lbor;->c:Ljava/util/List;

    invoke-static {v3}, Lbov;->a(Lbuc;)Lbov;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 90
    :cond_1
    iget-object v0, p1, Lbua;->d:Lbud;

    if-eqz v0, :cond_2

    .line 91
    iget-object v0, p1, Lbua;->d:Lbud;

    invoke-static {v0}, Lbox;->a(Lbud;)Lbox;

    move-result-object v0

    iput-object v0, p0, Lbor;->e:Lbox;

    .line 95
    :goto_2
    return-void

    .line 93
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lbor;->e:Lbox;

    goto :goto_2
.end method

.method private static b(J)J
    .locals 4

    .prologue
    .line 271
    const-wide/16 v0, 0x0

    cmp-long v0, p0, v0

    if-gez v0, :cond_0

    const-wide/16 v0, -0x1

    cmp-long v0, p0, v0

    if-eqz v0, :cond_0

    .line 272
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x2c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid target duration "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0, p1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 274
    :cond_0
    return-wide p0
.end method


# virtual methods
.method public a(Ljava/util/List;Ljava/util/List;)I
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbon;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmw;",
            ">;)I"
        }
    .end annotation

    .prologue
    const-wide/16 v4, 0x0

    .line 176
    const/16 v6, 0x19

    .line 177
    iget-boolean v0, p0, Lbor;->f:Z

    if-eqz v0, :cond_1

    .line 178
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0x1e8480

    mul-long v8, v0, v2

    .line 179
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-wide v2, v4

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbon;

    invoke-virtual {v0}, Lbon;->e()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    goto :goto_0

    .line 180
    :cond_0
    add-long v0, v8, v2

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 181
    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    long-to-double v4, v8

    mul-double/2addr v0, v4

    add-long/2addr v2, v8

    long-to-double v2, v2

    div-double/2addr v0, v2

    double-to-int v0, v0

    .line 185
    :goto_1
    return v0

    :cond_1
    move v0, v6

    goto :goto_1
.end method

.method public a()Lbua;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 99
    new-instance v3, Lbua;

    invoke-direct {v3}, Lbua;-><init>()V

    .line 100
    iget-wide v4, p0, Lbor;->g:J

    iput-wide v4, v3, Lbua;->a:J

    .line 101
    iget-object v0, p0, Lbor;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    .line 102
    new-array v0, v4, [Lbub;

    iput-object v0, v3, Lbua;->b:[Lbub;

    move v2, v1

    .line 103
    :goto_0
    if-ge v2, v4, :cond_0

    .line 104
    iget-object v5, v3, Lbua;->b:[Lbub;

    iget-object v0, p0, Lbor;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbot;

    invoke-virtual {v0}, Lbot;->a()Lbub;

    move-result-object v0

    aput-object v0, v5, v2

    .line 103
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 106
    :cond_0
    iget-object v0, p0, Lbor;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    .line 107
    new-array v0, v2, [Lbuc;

    iput-object v0, v3, Lbua;->c:[Lbuc;

    .line 108
    :goto_1
    if-ge v1, v2, :cond_1

    .line 109
    iget-object v4, v3, Lbua;->c:[Lbuc;

    iget-object v0, p0, Lbor;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbov;

    invoke-virtual {v0}, Lbov;->a()Lbuc;

    move-result-object v0

    aput-object v0, v4, v1

    .line 108
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 112
    :cond_1
    iget-object v0, p0, Lbor;->e:Lbox;

    if-eqz v0, :cond_2

    .line 113
    iget-object v0, p0, Lbor;->e:Lbox;

    invoke-virtual {v0}, Lbox;->a()Lbud;

    move-result-object v0

    iput-object v0, v3, Lbua;->d:Lbud;

    .line 115
    :cond_2
    iget-boolean v0, p0, Lbor;->f:Z

    iput-boolean v0, v3, Lbua;->e:Z

    .line 116
    return-object v3
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 209
    invoke-static {p1, p2}, Lbor;->b(J)J

    move-result-wide v0

    iput-wide v0, p0, Lbor;->g:J

    .line 210
    return-void
.end method

.method public a(Lbot;)V
    .locals 3

    .prologue
    .line 195
    iget-object v0, p0, Lbor;->d:Ljava/util/List;

    const-string v1, "newExclusion"

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 196
    return-void
.end method

.method public a(Lbov;)V
    .locals 3

    .prologue
    .line 190
    iget-object v0, p0, Lbor;->c:Ljava/util/List;

    const-string v1, "newInclusion"

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 191
    return-void
.end method

.method public a(Lbox;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lbor;->e:Lbox;

    .line 222
    return-void
.end method

.method public a(Ljava/util/Collection;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 254
    iget-object v0, p0, Lbor;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lbor;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbov;

    iget-object v0, v0, Lbov;->a:Lbmd;

    iget-object v0, v0, Lbmd;->e:Ljeg;

    iget-object v0, v0, Ljeg;->b:Landroid/net/Uri;

    invoke-interface {p1, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbor;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 255
    :cond_1
    iget-object v0, p0, Lbor;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_1
    if-ltz v1, :cond_3

    iget-object v0, p0, Lbor;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbot;

    iget-object v0, v0, Lbot;->a:Lbmd;

    iget-object v0, v0, Lbmd;->e:Ljeg;

    iget-object v0, v0, Ljeg;->b:Landroid/net/Uri;

    invoke-interface {p1, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lbor;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    .line 256
    :cond_3
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 217
    iput-boolean p1, p0, Lbor;->f:Z

    .line 218
    return-void
.end method

.method public a(Lbmd;)Z
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Lbor;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbot;

    .line 263
    invoke-virtual {v0, p1}, Lbot;->a(Lbmd;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    const/4 v0, 0x1

    .line 267
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lbor;)Z
    .locals 4

    .prologue
    .line 230
    invoke-virtual {p0, p1}, Lbor;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 232
    :goto_0
    iget-object v1, p0, Lbor;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 233
    iget-object v1, p0, Lbor;->c:Ljava/util/List;

    invoke-virtual {p1}, Lbor;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 234
    iget-object v1, p0, Lbor;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 235
    iget-object v1, p0, Lbor;->d:Ljava/util/List;

    invoke-virtual {p1}, Lbor;->c()Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 236
    invoke-virtual {p1}, Lbor;->e()J

    move-result-wide v2

    iput-wide v2, p0, Lbor;->g:J

    .line 237
    invoke-virtual {p1}, Lbor;->d()Lbox;

    move-result-object v1

    iput-object v1, p0, Lbor;->e:Lbox;

    .line 238
    invoke-virtual {p1}, Lbor;->f()Z

    move-result v1

    iput-boolean v1, p0, Lbor;->f:Z

    .line 240
    return v0

    .line 230
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbov;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    iget-object v0, p0, Lbor;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbot;",
            ">;"
        }
    .end annotation

    .prologue
    .line 130
    iget-object v0, p0, Lbor;->d:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public d()Lbox;
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lbor;->e:Lbox;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 311
    const/4 v0, 0x0

    return v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 145
    iget-wide v0, p0, Lbor;->g:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 279
    if-ne p0, p1, :cond_1

    .line 290
    :cond_0
    :goto_0
    return v0

    .line 282
    :cond_1
    instance-of v2, p1, Lbor;

    if-nez v2, :cond_2

    move v0, v1

    .line 283
    goto :goto_0

    .line 285
    :cond_2
    check-cast p1, Lbor;

    .line 286
    iget-object v2, p0, Lbor;->c:Ljava/util/List;

    iget-object v3, p1, Lbor;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lbor;->d:Ljava/util/List;

    iget-object v3, p1, Lbor;->d:Ljava/util/List;

    .line 287
    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lbor;->f:Z

    iget-boolean v3, p1, Lbor;->f:Z

    if-ne v2, v3, :cond_3

    iget-wide v2, p0, Lbor;->g:J

    iget-wide v4, p1, Lbor;->g:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lbor;->e:Lbox;

    iget-object v3, p1, Lbor;->e:Lbox;

    .line 290
    invoke-static {v2, v3}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 153
    iget-boolean v0, p0, Lbor;->f:Z

    return v0
.end method

.method public g()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 160
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lbor;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 161
    iget-object v0, p0, Lbor;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbov;

    .line 162
    iget-object v0, v0, Lbov;->a:Lbmd;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 164
    :cond_0
    return-object v1
.end method

.method public h()V
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lbor;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 201
    return-void
.end method

.method public hashCode()I
    .locals 7

    .prologue
    .line 295
    iget-object v0, p0, Lbor;->c:Ljava/util/List;

    iget-object v1, p0, Lbor;->d:Ljava/util/List;

    iget-boolean v2, p0, Lbor;->f:Z

    iget-wide v4, p0, Lbor;->g:J

    iget-object v3, p0, Lbor;->e:Lbox;

    const/16 v6, 0x11

    .line 299
    invoke-static {v3, v6}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v3

    .line 298
    invoke-static {v4, v5, v3}, Lbqh;->a(JI)I

    move-result v3

    .line 297
    invoke-static {v2, v3}, Lbqh;->a(ZI)I

    move-result v2

    .line 296
    invoke-static {v1, v2}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v1

    .line 295
    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public i()V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lbor;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 206
    return-void
.end method

.method public j()V
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Lbor;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 246
    iget-object v0, p0, Lbor;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Lbor;->e:Lbox;

    .line 248
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbor;->f:Z

    .line 249
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbor;->g:J

    .line 250
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 304
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lbor;->c:Ljava/util/List;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lbor;->d:Ljava/util/List;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lbor;->e:Lbox;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-wide v4, p0, Lbor;->g:J

    .line 306
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-boolean v3, p0, Lbor;->f:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 304
    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 316
    iget-object v0, p0, Lbor;->c:Ljava/util/List;

    sget-object v2, Lbor;->a:[Lbov;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 317
    iget-object v0, p0, Lbor;->d:Ljava/util/List;

    sget-object v2, Lbor;->b:[Lbot;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 318
    iget-object v0, p0, Lbor;->e:Lbox;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 319
    iget-wide v2, p0, Lbor;->g:J

    invoke-virtual {p1, v2, v3}, Landroid/os/Parcel;->writeLong(J)V

    .line 320
    iget-boolean v0, p0, Lbor;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 321
    return-void

    :cond_0
    move v0, v1

    .line 320
    goto :goto_0
.end method
