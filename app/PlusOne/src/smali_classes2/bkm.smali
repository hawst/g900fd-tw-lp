.class public final Lbkm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:[B

.field private final b:Lbko;

.field private final c:Lbkv;

.field private final d:Lbkq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    new-instance v0, Lbko;

    invoke-direct {v0}, Lbko;-><init>()V

    iput-object v0, p0, Lbkm;->b:Lbko;

    .line 104
    new-instance v0, Lbkv;

    invoke-direct {v0}, Lbkv;-><init>()V

    iput-object v0, p0, Lbkm;->c:Lbkv;

    .line 105
    new-instance v0, Lbkq;

    invoke-direct {v0}, Lbkq;-><init>()V

    iput-object v0, p0, Lbkm;->d:Lbkq;

    .line 106
    return-void
.end method

.method constructor <init>(Lbkr;)V
    .locals 2

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    invoke-interface {p1}, Lbkr;->c()Lbkn;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 110
    new-instance v0, Lbko;

    .line 111
    invoke-interface {p1}, Lbkr;->c()Lbkn;

    move-result-object v1

    invoke-direct {v0, v1}, Lbko;-><init>(Lbkn;)V

    iput-object v0, p0, Lbkm;->b:Lbko;

    .line 115
    :goto_0
    invoke-interface {p1}, Lbkr;->d()Lbku;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 116
    new-instance v0, Lbkv;

    .line 117
    invoke-interface {p1}, Lbkr;->d()Lbku;

    move-result-object v1

    invoke-direct {v0, v1}, Lbkv;-><init>(Lbku;)V

    iput-object v0, p0, Lbkm;->c:Lbkv;

    .line 121
    :goto_1
    invoke-interface {p1}, Lbkr;->e()Lbkp;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 122
    new-instance v0, Lbkq;

    .line 123
    invoke-interface {p1}, Lbkr;->e()Lbkp;

    move-result-object v1

    invoke-direct {v0, v1}, Lbkq;-><init>(Lbkp;)V

    iput-object v0, p0, Lbkm;->d:Lbkq;

    .line 127
    :goto_2
    return-void

    .line 113
    :cond_0
    new-instance v0, Lbko;

    invoke-direct {v0}, Lbko;-><init>()V

    iput-object v0, p0, Lbkm;->b:Lbko;

    goto :goto_0

    .line 119
    :cond_1
    new-instance v0, Lbkv;

    invoke-direct {v0}, Lbkv;-><init>()V

    iput-object v0, p0, Lbkm;->c:Lbkv;

    goto :goto_1

    .line 125
    :cond_2
    new-instance v0, Lbkq;

    invoke-direct {v0}, Lbkq;-><init>()V

    iput-object v0, p0, Lbkm;->d:Lbkq;

    goto :goto_2
.end method


# virtual methods
.method public a([B)Lbkm;
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lbkm;->a:[B

    .line 131
    return-object p0
.end method

.method public a()Lbko;
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lbkm;->b:Lbko;

    return-object v0
.end method

.method public a([Lajj;[Lajw;)Lbkr;
    .locals 10

    .prologue
    const-wide/16 v0, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 167
    const-string v3, "postProcessors"

    invoke-static {p1, v3, v5}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 168
    const-string v3, "segmenters"

    invoke-static {p2, v3, v5}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 170
    array-length v5, p1

    move v3, v2

    :goto_0
    if-ge v3, v5, :cond_0

    aget-object v6, p1, v3

    .line 171
    iget-object v7, p0, Lbkm;->c:Lbkv;

    invoke-interface {v6, v7}, Lajj;->a(Lbkv;)V

    .line 170
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 173
    :cond_0
    iget-object v3, p0, Lbkm;->c:Lbkv;

    invoke-virtual {v3}, Lbkv;->a()Lbku;

    move-result-object v5

    .line 174
    iget-object v3, p0, Lbkm;->b:Lbko;

    invoke-virtual {v3}, Lbko;->a()Z

    move-result v3

    if-nez v3, :cond_3

    .line 175
    iget-object v6, p0, Lbkm;->b:Lbko;

    .line 176
    sget-object v3, Lbmz;->a:Lbmz;

    invoke-virtual {v5, v3}, Lbku;->a(Lbmz;)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Lbmz;->b:Lbmz;

    invoke-virtual {v5, v3}, Lbku;->a(Lbmz;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_1
    move v3, v4

    :goto_1
    if-nez v3, :cond_5

    .line 175
    :cond_2
    :goto_2
    invoke-virtual {v6, v0, v1}, Lbko;->a(J)Lbko;

    .line 178
    :cond_3
    iget-object v0, p0, Lbkm;->b:Lbko;

    invoke-virtual {v0}, Lbko;->b()Lbkn;

    move-result-object v1

    .line 179
    array-length v3, p2

    move v0, v2

    :goto_3
    if-ge v0, v3, :cond_7

    aget-object v2, p2, v0

    .line 180
    iget-object v4, p0, Lbkm;->d:Lbkq;

    invoke-interface {v2, v1, v5, v4}, Lajw;->a(Lbkn;Lbku;Lbkq;)V

    .line 179
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    move v3, v2

    .line 176
    goto :goto_1

    :cond_5
    invoke-static {v5}, Lbkx;->a(Lbku;)Lbmz;

    move-result-object v3

    invoke-virtual {v5, v3}, Lbku;->b(Lbmz;)Landroid/util/LongSparseArray;

    move-result-object v3

    invoke-virtual {v3}, Landroid/util/LongSparseArray;->size()I

    move-result v7

    if-eqz v7, :cond_2

    if-ne v7, v4, :cond_6

    add-int/lit8 v0, v7, -0x1

    invoke-virtual {v3, v0}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v0

    goto :goto_2

    :cond_6
    add-int/lit8 v0, v7, -0x1

    invoke-virtual {v3, v0}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v0

    add-int/lit8 v4, v7, -0x2

    invoke-virtual {v3, v4}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v8

    sub-long v8, v0, v8

    add-long/2addr v0, v8

    goto :goto_2

    .line 182
    :cond_7
    iget-object v0, p0, Lbkm;->d:Lbkq;

    invoke-virtual {v0}, Lbkq;->b()Lbkp;

    move-result-object v0

    .line 183
    new-instance v2, Lbkl;

    iget-object v3, p0, Lbkm;->a:[B

    invoke-direct {v2, v3, v1, v5, v0}, Lbkl;-><init>([BLbkn;Lbku;Lbkp;)V

    return-object v2
.end method

.method public b()Lbkv;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lbkm;->c:Lbkv;

    return-object v0
.end method

.method public c()Lbkr;
    .locals 2

    .prologue
    .line 193
    sget-object v0, Lajj;->a:[Lajj;

    sget-object v1, Lajw;->a:[Lajw;

    invoke-virtual {p0, v0, v1}, Lbkm;->a([Lajj;[Lajw;)Lbkr;

    move-result-object v0

    return-object v0
.end method
