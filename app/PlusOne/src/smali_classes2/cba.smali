.class public Lcba;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Lcam;

.field b:F

.field c:I


# direct methods
.method public constructor <init>(Lcam;)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcba;->b:F

    .line 24
    const/4 v0, -0x1

    iput v0, p0, Lcba;->c:I

    .line 28
    iput-object p1, p0, Lcba;->a:Lcam;

    .line 31
    new-instance v0, Lcbb;

    invoke-direct {v0, p0}, Lcbb;-><init>(Lcba;)V

    invoke-interface {p1, v0}, Lcam;->a(Lcan;)V

    .line 41
    new-instance v0, Lcbc;

    invoke-direct {v0, p0}, Lcbc;-><init>(Lcba;)V

    invoke-interface {p1, v0}, Lcam;->a(Landroid/view/View$OnLayoutChangeListener;)V

    .line 56
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 67
    iget v0, p0, Lcba;->c:I

    return v0
.end method

.method public a(F)V
    .locals 3

    .prologue
    .line 60
    const-string v0, "position"

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {p1, v0, v1, v2}, Lcgp;->a(FLjava/lang/CharSequence;FF)F

    .line 61
    iput p1, p0, Lcba;->b:F

    .line 62
    invoke-virtual {p0}, Lcba;->d()V

    .line 63
    return-void
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 71
    iget v0, p0, Lcba;->b:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()F
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcba;->b:F

    return v0
.end method

.method protected d()V
    .locals 3

    .prologue
    .line 80
    iget v0, p0, Lcba;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcba;->b:F

    const/high16 v1, -0x40800000    # -1.0f

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcba;->a:Lcam;

    iget v1, p0, Lcba;->b:F

    iget v2, p0, Lcba;->c:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    invoke-interface {v0, v1}, Lcam;->b(I)V

    .line 84
    :cond_0
    return-void
.end method
