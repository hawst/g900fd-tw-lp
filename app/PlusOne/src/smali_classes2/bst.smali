.class final enum Lbst;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbst;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lbst;

.field public static final enum b:Lbst;

.field public static final enum c:Lbst;

.field private static final synthetic d:[Lbst;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 12
    new-instance v0, Lbst;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v2}, Lbst;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbst;->a:Lbst;

    new-instance v0, Lbst;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v3}, Lbst;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbst;->b:Lbst;

    new-instance v0, Lbst;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v4}, Lbst;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbst;->c:Lbst;

    const/4 v0, 0x3

    new-array v0, v0, [Lbst;

    sget-object v1, Lbst;->a:Lbst;

    aput-object v1, v0, v2

    sget-object v1, Lbst;->b:Lbst;

    aput-object v1, v0, v3

    sget-object v1, Lbst;->c:Lbst;

    aput-object v1, v0, v4

    sput-object v0, Lbst;->d:[Lbst;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 12
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbst;
    .locals 1

    .prologue
    .line 12
    const-class v0, Lbst;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbst;

    return-object v0
.end method

.method public static values()[Lbst;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lbst;->d:[Lbst;

    invoke-virtual {v0}, [Lbst;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbst;

    return-object v0
.end method
