.class public final Lhad;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lgqx;

.field private b:Lcom/google/android/gms/internal/qu;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILhae;)V
    .locals 6

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, v3

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lhad;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lhae;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lhae;)V
    .locals 7

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lhad;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lhae;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lhae;Z)V
    .locals 7

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    iget v2, v0, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Lcom/google/android/gms/internal/qu;

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/qu;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lhad;->b:Lcom/google/android/gms/internal/qu;

    new-instance v0, Lgqx;

    new-instance v1, Lgqv;

    invoke-direct {v1, p5}, Lgqv;-><init>(Lhae;)V

    invoke-direct {v0, p1, v1}, Lgqx;-><init>(Landroid/content/Context;Lgqv;)V

    iput-object v0, p0, Lhad;->a:Lgqx;

    return-void

    :catch_0
    move-exception v0

    const-string v0, "PlayLogger"

    const-string v3, "This can\'t happen."

    invoke-static {v0, v3}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lhad;
    .locals 7

    iget-object v0, p0, Lhad;->b:Lcom/google/android/gms/internal/qu;

    iget-object v0, v0, Lcom/google/android/gms/internal/qu;->e:Ljava/lang/String;

    invoke-static {p1, v0}, Lgif;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/qu;

    iget-object v1, p0, Lhad;->b:Lcom/google/android/gms/internal/qu;

    iget-object v1, v1, Lcom/google/android/gms/internal/qu;->b:Ljava/lang/String;

    iget-object v2, p0, Lhad;->b:Lcom/google/android/gms/internal/qu;

    iget v2, v2, Lcom/google/android/gms/internal/qu;->c:I

    iget-object v3, p0, Lhad;->b:Lcom/google/android/gms/internal/qu;

    iget v3, v3, Lcom/google/android/gms/internal/qu;->d:I

    iget-object v4, p0, Lhad;->b:Lcom/google/android/gms/internal/qu;

    iget-object v5, v4, Lcom/google/android/gms/internal/qu;->f:Ljava/lang/String;

    iget-object v4, p0, Lhad;->b:Lcom/google/android/gms/internal/qu;

    iget-boolean v6, v4, Lcom/google/android/gms/internal/qu;->g:Z

    move-object v4, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/qu;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lhad;->b:Lcom/google/android/gms/internal/qu;

    :cond_0
    return-object p0
.end method

.method public a(Z)Lhad;
    .locals 7

    iget-object v0, p0, Lhad;->b:Lcom/google/android/gms/internal/qu;

    iget-boolean v0, v0, Lcom/google/android/gms/internal/qu;->g:Z

    if-eq p1, v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/qu;

    iget-object v1, p0, Lhad;->b:Lcom/google/android/gms/internal/qu;

    iget-object v1, v1, Lcom/google/android/gms/internal/qu;->b:Ljava/lang/String;

    iget-object v2, p0, Lhad;->b:Lcom/google/android/gms/internal/qu;

    iget v2, v2, Lcom/google/android/gms/internal/qu;->c:I

    iget-object v3, p0, Lhad;->b:Lcom/google/android/gms/internal/qu;

    iget v3, v3, Lcom/google/android/gms/internal/qu;->d:I

    iget-object v4, p0, Lhad;->b:Lcom/google/android/gms/internal/qu;

    iget-object v4, v4, Lcom/google/android/gms/internal/qu;->e:Ljava/lang/String;

    iget-object v5, p0, Lhad;->b:Lcom/google/android/gms/internal/qu;

    iget-object v5, v5, Lcom/google/android/gms/internal/qu;->f:Ljava/lang/String;

    move v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/internal/qu;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V

    iput-object v0, p0, Lhad;->b:Lcom/google/android/gms/internal/qu;

    :cond_0
    return-object p0
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lhad;->a:Lgqx;

    invoke-virtual {v0}, Lgqx;->j()V

    return-void
.end method

.method public varargs a(JLjava/lang/String;[B[Ljava/lang/String;)V
    .locals 9

    iget-object v0, p0, Lhad;->a:Lgqx;

    iget-object v7, p0, Lhad;->b:Lcom/google/android/gms/internal/qu;

    new-instance v1, Lcom/google/android/gms/internal/qq;

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/internal/qq;-><init>(JLjava/lang/String;[B[Ljava/lang/String;)V

    invoke-virtual {v0, v7, v1}, Lgqx;->a(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V

    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 2

    instance-of v0, p1, Lpyo;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must be a ClientAnalytics.LogEvent"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lhad;->a:Lgqx;

    iget-object v1, p0, Lhad;->b:Lcom/google/android/gms/internal/qu;

    check-cast p1, Lpyo;

    invoke-virtual {v0, v1, p1}, Lgqx;->a(Lcom/google/android/gms/internal/qu;Lpyo;)V

    return-void
.end method

.method public varargs a(Ljava/lang/String;[B[Ljava/lang/String;)V
    .locals 7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-virtual/range {v1 .. v6}, Lhad;->a(JLjava/lang/String;[B[Ljava/lang/String;)V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lhad;->a:Lgqx;

    invoke-virtual {v0}, Lgqx;->k()V

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lhad;->b:Lcom/google/android/gms/internal/qu;

    iget-object v0, v0, Lcom/google/android/gms/internal/qu;->e:Ljava/lang/String;

    return-object v0
.end method
