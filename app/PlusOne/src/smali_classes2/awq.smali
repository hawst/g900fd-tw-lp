.class public final Lawq;
.super Lamn;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lavu;

.field private final c:Lcfg;

.field private final d:Lalg;

.field private final e:Lcfj;

.field private final f:Lcfj;

.field private final g:Lalf;

.field private final h:Laws;

.field private final j:Lawk;

.field private final k:Ljava/lang/Object;

.field private l:Lawr;

.field private m:Lcff;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lawq;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lawq;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lavu;Lalg;Ljava/io/File;Lcfg;Lalf;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 67
    invoke-direct {p0}, Lamn;-><init>()V

    .line 41
    new-instance v0, Lcfj;

    invoke-direct {v0}, Lcfj;-><init>()V

    iput-object v0, p0, Lawq;->e:Lcfj;

    .line 43
    new-instance v0, Lcfj;

    invoke-direct {v0}, Lcfj;-><init>()V

    iput-object v0, p0, Lawq;->f:Lcfj;

    .line 46
    new-instance v0, Lawt;

    invoke-direct {v0, p0}, Lawt;-><init>(Lawq;)V

    iput-object v0, p0, Lawq;->j:Lawk;

    .line 48
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lawq;->k:Ljava/lang/Object;

    .line 68
    const-string v0, "state"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavu;

    iput-object v0, p0, Lawq;->b:Lavu;

    .line 69
    const-string v0, "asyncTaskRunner"

    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lalg;

    iput-object v0, p0, Lawq;->d:Lalg;

    .line 70
    const-string v0, "httpFetcherFactory"

    invoke-static {p4, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfg;

    iput-object v0, p0, Lawq;->c:Lcfg;

    .line 71
    const-string v0, "assetSettings"

    invoke-static {p5, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lalf;

    iput-object v0, p0, Lawq;->g:Lalf;

    .line 72
    new-instance v1, Laws;

    const-string v0, "cacheDir"

    invoke-static {p3, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-direct {v1, v0}, Laws;-><init>(Ljava/io/File;)V

    iput-object v1, p0, Lawq;->h:Laws;

    .line 73
    return-void
.end method

.method static synthetic a(Lawq;J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lawq;->g:Lalf;

    invoke-interface {v0}, Lalf;->a()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lawq;)V
    .locals 0

    .prologue
    .line 34
    invoke-virtual {p0}, Lawq;->e()V

    return-void
.end method

.method static synthetic a(Lawq;Lboi;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 34
    iget-object v0, p0, Lawq;->f:Lcfj;

    invoke-virtual {v0}, Lcfj;->c()V

    iget-object v0, p0, Lawq;->l:Lawr;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lawq;->l:Lawr;

    invoke-virtual {v0, v6}, Lawr;->cancel(Z)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lawq;->l:Lawr;

    :cond_0
    if-eqz p1, :cond_5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lboi;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-object v3, v0, Lbmd;->e:Ljeg;

    iget-object v3, v3, Ljeg;->a:Ljdx;

    if-eqz v3, :cond_1

    iget-object v3, v0, Lbmd;->e:Ljeg;

    iget-object v3, v3, Ljeg;->a:Ljdx;

    iget-object v3, v3, Ljdx;->b:Ljava/lang/Long;

    if-eqz v3, :cond_1

    iget-object v0, v0, Lbmd;->e:Ljeg;

    iget-object v0, v0, Ljeg;->a:Ljdx;

    iget-object v0, v0, Ljdx;->b:Ljava/lang/Long;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    iget-object v2, p0, Lawq;->e:Lcfj;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcfj;->b(J)Z

    move-result v2

    if-nez v2, :cond_3

    iget-object v2, p0, Lawq;->f:Lcfj;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcfj;->a(J)V

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lawq;->f:Lcfj;

    invoke-virtual {v0}, Lcfj;->b()Z

    move-result v0

    if-nez v0, :cond_6

    new-instance v0, Lawr;

    invoke-direct {v0, p0}, Lawr;-><init>(Lawq;)V

    iput-object v0, p0, Lawq;->l:Lawr;

    iget-object v0, p0, Lawq;->d:Lalg;

    const-class v1, Lawr;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    iget-object v1, p0, Lawq;->l:Lawr;

    new-array v2, v6, [[J

    const/4 v3, 0x0

    iget-object v4, p0, Lawq;->f:Lcfj;

    invoke-virtual {v4}, Lcfj;->d()[J

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lalg;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    :cond_5
    :goto_2
    return-void

    :cond_6
    iget-object v0, p0, Lawq;->b:Lavu;

    invoke-interface {v0, v6}, Lavu;->t(Z)V

    goto :goto_2
.end method

.method static synthetic b(Lawq;)Lavu;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lawq;->b:Lavu;

    return-object v0
.end method

.method static synthetic c(Lawq;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lawq;->k:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lawq;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lawq;)V
    .locals 0

    .prologue
    .line 34
    invoke-virtual {p0}, Lawq;->e()V

    return-void
.end method

.method static synthetic e(Lawq;)Lcfj;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lawq;->f:Lcfj;

    return-object v0
.end method

.method static synthetic f(Lawq;)Lcfj;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lawq;->e:Lcfj;

    return-object v0
.end method

.method static synthetic g(Lawq;)Lcff;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lawq;->m:Lcff;

    return-object v0
.end method

.method static synthetic h(Lawq;)Laws;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lawq;->h:Laws;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 77
    invoke-super {p0}, Lamn;->a()V

    .line 80
    iget-object v0, p0, Lawq;->m:Lcff;

    if-nez v0, :cond_0

    .line 81
    iget-object v0, p0, Lawq;->c:Lcfg;

    iget-object v1, p0, Lawq;->g:Lalf;

    .line 82
    invoke-interface {v1}, Lalf;->c()I

    move-result v1

    iget-object v2, p0, Lawq;->g:Lalf;

    .line 83
    invoke-interface {v2}, Lalf;->b()I

    move-result v2

    .line 81
    invoke-interface {v0, v1, v2}, Lcfg;->a(II)Lcff;

    move-result-object v0

    iput-object v0, p0, Lawq;->m:Lcff;

    .line 84
    iget-object v0, p0, Lawq;->m:Lcff;

    const-string v1, "httpFetcher"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 86
    :cond_0
    iget-object v0, p0, Lawq;->b:Lavu;

    iget-object v1, p0, Lawq;->j:Lawk;

    invoke-interface {v0, v1}, Lavu;->L(Lawk;)V

    .line 87
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lawq;->b:Lavu;

    iget-object v1, p0, Lawq;->j:Lawk;

    invoke-interface {v0, v1}, Lavu;->M(Lawk;)V

    .line 92
    iget-object v0, p0, Lawq;->l:Lawr;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lawq;->l:Lawr;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lawr;->cancel(Z)Z

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lawq;->l:Lawr;

    .line 96
    :cond_0
    invoke-super {p0}, Lamn;->b()V

    .line 97
    return-void
.end method

.method public c()Lanw;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lawq;->h:Laws;

    return-object v0
.end method
