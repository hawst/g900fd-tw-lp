.class public Lbhx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbhr;
.implements Lbhs;
.implements Lcgk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbhr",
        "<",
        "Lbia;",
        ">;",
        "Lbhs;",
        "Lcgk;"
    }
.end annotation


# instance fields
.field private final a:[Lbhy;

.field private final b:Lbia;

.field private final c:Lbhz;

.field private final d:I

.field private final e:I

.field private final f:F

.field private g:Z

.field private h:J

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lbhx;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/List;II)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbib;",
            ">;II)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const-string v0, "mixerSources"

    invoke-static {p1, v0}, Lcec;->a(Ljava/util/Collection;Ljava/lang/CharSequence;)Ljava/util/Collection;

    .line 65
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    .line 66
    new-array v0, v4, [Lbhy;

    iput-object v0, p0, Lbhx;->a:[Lbhy;

    move v3, v1

    .line 67
    :goto_0
    if-ge v3, v4, :cond_0

    .line 68
    iget-object v5, p0, Lbhx;->a:[Lbhy;

    new-instance v6, Lbhy;

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbib;

    invoke-direct {v6, v0}, Lbhy;-><init>(Lbib;)V

    aput-object v6, v5, v3

    .line 67
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 71
    :cond_0
    const-string v0, "targetSampleRateHz"

    invoke-static {p2, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lbhx;->e:I

    .line 73
    if-eq p3, v2, :cond_1

    const/4 v0, 0x2

    if-ne p3, v0, :cond_2

    :cond_1
    move v0, v2

    :goto_1
    const-string v1, "Only mono and stereo are supported"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 75
    iput p3, p0, Lbhx;->d:I

    .line 77
    new-instance v0, Lbhz;

    invoke-direct {v0}, Lbhz;-><init>()V

    iput-object v0, p0, Lbhx;->c:Lbhz;

    .line 78
    new-instance v0, Lbia;

    invoke-direct {v0}, Lbia;-><init>()V

    iput-object v0, p0, Lbhx;->b:Lbia;

    .line 79
    iget-object v0, p0, Lbhx;->b:Lbia;

    iput p3, v0, Lbia;->e:I

    .line 80
    iget-object v0, p0, Lbhx;->b:Lbia;

    iput p2, v0, Lbia;->d:I

    .line 81
    iput-boolean v2, p0, Lbhx;->g:Z

    .line 83
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    int-to-double v2, v4

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    iput v0, p0, Lbhx;->f:F

    .line 86
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lbhx;->a(J)Z

    .line 87
    return-void

    :cond_2
    move v0, v1

    .line 73
    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 363
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lbhx;->a:[Lbhy;

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 364
    iget-object v0, p0, Lbhx;->a:[Lbhy;

    aget-object v0, v0, v1

    iget-object v0, v0, Lbhy;->a:Lbht;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 365
    iget-object v0, p0, Lbhx;->a:[Lbhy;

    aget-object v0, v0, v1

    iget-object v0, v0, Lbhy;->c:Lbhh;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 366
    iget-object v0, p0, Lbhx;->a:[Lbhy;

    aget-object v0, v0, v1

    iget-object v0, v0, Lbhy;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;

    .line 367
    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    goto :goto_1

    .line 363
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 370
    :cond_1
    return-void
.end method

.method public a(Lbia;)V
    .locals 3

    .prologue
    .line 315
    const-string v0, "polled"

    iget-object v1, p0, Lbhx;->b:Lbia;

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    .line 316
    iget-boolean v0, p0, Lbhx;->g:Z

    const-string v1, "mOwnsAudioSamples"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->a(ZLjava/lang/CharSequence;Z)V

    .line 317
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbhx;->g:Z

    .line 318
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 25
    check-cast p1, Lbia;

    invoke-virtual {p0, p1}, Lbhx;->a(Lbia;)V

    return-void
.end method

.method public a(J)Z
    .locals 11

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 322
    iget-object v5, p0, Lbhx;->a:[Lbhy;

    array-length v6, v5

    move v4, v2

    :goto_0
    if-ge v4, v6, :cond_5

    aget-object v7, v5, v4

    .line 323
    iget-object v0, v7, Lbhy;->e:Lbhk;

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, v7, Lbhy;->a:Lbht;

    iget-object v1, v7, Lbhy;->e:Lbhk;

    invoke-virtual {v0, v1}, Lbht;->a(Lbhj;)V

    .line 325
    const/4 v0, 0x0

    iput-object v0, v7, Lbhy;->e:Lbhk;

    .line 327
    :cond_0
    iput-boolean v2, v7, Lbhy;->i:Z

    .line 329
    iget-object v0, v7, Lbhy;->c:Lbhh;

    .line 330
    invoke-virtual {v0, p1, p2}, Lbhh;->a(J)I

    move-result v0

    .line 331
    iget-object v1, v7, Lbhy;->d:Lbma;

    invoke-interface {v1}, Lbma;->b()J

    move-result-wide v8

    cmp-long v1, p1, v8

    if-ltz v1, :cond_1

    move v0, v2

    .line 358
    :goto_1
    return v0

    .line 334
    :cond_1
    iput v0, v7, Lbhy;->h:I

    .line 335
    iget-object v1, v7, Lbhy;->a:Lbht;

    invoke-virtual {v1, v0}, Lbht;->a(I)V

    .line 337
    iget-object v1, v7, Lbhy;->d:Lbma;

    invoke-interface {v1}, Lbma;->a()I

    move-result v1

    if-eq v0, v1, :cond_2

    iget-object v1, v7, Lbhy;->d:Lbma;

    .line 338
    invoke-interface {v1, v0}, Lbma;->b(I)J

    move-result-wide v8

    cmp-long v1, p1, v8

    if-gez v1, :cond_4

    .line 339
    :cond_2
    iput-boolean v3, v7, Lbhy;->j:Z

    .line 341
    iget-object v1, v7, Lbhy;->d:Lbma;

    invoke-interface {v1}, Lbma;->a()I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 342
    iget-object v0, v7, Lbhy;->d:Lbma;

    invoke-interface {v0}, Lbma;->b()J

    move-result-wide v0

    sub-long/2addr v0, p1

    .line 347
    :goto_2
    const/4 v8, 0x2

    iget v9, p0, Lbhx;->d:I

    iget v10, p0, Lbhx;->e:I

    invoke-static {v0, v1, v8, v9, v10}, Lceg;->a(JIII)I

    move-result v0

    iput v0, v7, Lbhy;->k:I

    .line 322
    :goto_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 344
    :cond_3
    iget-object v1, v7, Lbhy;->d:Lbma;

    .line 345
    invoke-interface {v1, v0}, Lbma;->b(I)J

    move-result-wide v0

    sub-long/2addr v0, p1

    goto :goto_2

    .line 353
    :cond_4
    iput-boolean v2, v7, Lbhy;->j:Z

    .line 354
    const/4 v0, -0x1

    iput v0, v7, Lbhy;->k:I

    goto :goto_3

    .line 357
    :cond_5
    iput-wide p1, p0, Lbhx;->h:J

    move v0, v3

    .line 358
    goto :goto_1
.end method

.method public b()Lbia;
    .locals 18

    .prologue
    .line 91
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lbhx;->g:Z

    if-nez v2, :cond_0

    .line 92
    const/4 v2, 0x0

    .line 123
    :goto_0
    return-object v2

    .line 95
    :cond_0
    move-object/from16 v0, p0

    iget-object v5, v0, Lbhx;->c:Lbhz;

    const/4 v2, -0x1

    iput v2, v5, Lbhz;->a:I

    const/4 v2, -0x1

    iput v2, v5, Lbhz;->b:I

    const/4 v2, 0x0

    move v4, v2

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lbhx;->a:[Lbhy;

    array-length v2, v2

    if-ge v4, v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lbhx;->a:[Lbhy;

    aget-object v6, v2, v4

    iget-boolean v2, v6, Lbhy;->j:Z

    if-eqz v2, :cond_4

    iget v2, v5, Lbhz;->a:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    iget v2, v5, Lbhz;->a:I

    iget v3, v6, Lbhy;->k:I

    if-le v2, v3, :cond_2

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lbhx;->b:Lbia;

    iget-object v2, v2, Lbia;->f:[B

    if-nez v2, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Lbhx;->d:I

    mul-int/lit16 v2, v2, 0x800

    :goto_2
    iget v3, v6, Lbhy;->k:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, v5, Lbhz;->a:I

    iput v4, v5, Lbhz;->b:I

    :cond_2
    :goto_3
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lbhx;->b:Lbia;

    iget-object v2, v2, Lbia;->f:[B

    array-length v2, v2

    goto :goto_2

    :cond_4
    iget-object v2, v6, Lbhy;->e:Lbhk;

    if-nez v2, :cond_7

    iget-object v2, v6, Lbhy;->a:Lbht;

    invoke-virtual {v2}, Lbht;->b()Lbhj;

    move-result-object v2

    check-cast v2, Lbhk;

    if-nez v2, :cond_5

    const/4 v2, 0x0

    .line 96
    :goto_4
    if-nez v2, :cond_b

    .line 97
    const/4 v2, 0x0

    goto :goto_0

    .line 95
    :cond_5
    iput-object v2, v6, Lbhy;->e:Lbhk;

    iget v3, v2, Lbhk;->g:I

    iput v3, v6, Lbhy;->f:I

    iget v3, v2, Lbhk;->h:I

    const-string v7, "polledSamples.size"

    const/4 v8, 0x0

    invoke-static {v3, v7, v8}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v3

    iput v3, v6, Lbhy;->g:I

    iget-boolean v3, v2, Lbhk;->b:Z

    if-nez v3, :cond_6

    iget v3, v2, Lbhk;->h:I

    if-lez v3, :cond_9

    :cond_6
    const/4 v3, 0x1

    :goto_5
    const-string v7, "polled samples may only have size zero at the end of the stream"

    invoke-static {v3, v7}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    iget-boolean v3, v6, Lbhy;->i:Z

    if-nez v3, :cond_7

    iget-object v3, v6, Lbhy;->b:Ljava/util/List;

    iget v7, v6, Lbhy;->h:I

    invoke-interface {v3, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;

    iget v2, v2, Lbhk;->d:I

    invoke-virtual {v3, v2}, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->a(I)V

    const/4 v2, 0x1

    iput-boolean v2, v6, Lbhy;->i:Z

    :cond_7
    iget-object v2, v6, Lbhy;->b:Ljava/util/List;

    iget v3, v6, Lbhy;->h:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;

    iget v3, v6, Lbhy;->g:I

    invoke-virtual {v2, v3}, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->b(I)I

    move-result v2

    iget v3, v5, Lbhz;->a:I

    const/4 v6, -0x1

    if-eq v3, v6, :cond_8

    iget v3, v5, Lbhz;->a:I

    if-le v3, v2, :cond_2

    :cond_8
    iput v2, v5, Lbhz;->a:I

    iput v4, v5, Lbhz;->b:I

    goto :goto_3

    :cond_9
    const/4 v3, 0x0

    goto :goto_5

    :cond_a
    const/4 v2, 0x1

    goto :goto_4

    .line 100
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lbhx;->c:Lbhz;

    iget v10, v2, Lbhz;->a:I

    .line 101
    move-object/from16 v0, p0

    iget-object v2, v0, Lbhx;->c:Lbhz;

    iget v2, v2, Lbhz;->b:I

    .line 103
    move-object/from16 v0, p0

    iget-object v3, v0, Lbhx;->a:[Lbhy;

    aget-object v3, v3, v2

    iget-boolean v3, v3, Lbhy;->j:Z

    if-eqz v3, :cond_e

    .line 104
    move-object/from16 v0, p0

    iget-wide v2, v0, Lbhx;->h:J

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget v5, v0, Lbhx;->d:I

    move-object/from16 v0, p0

    iget v6, v0, Lbhx;->e:I

    invoke-static {v10, v4, v5, v6}, Lceg;->a(IIII)J

    move-result-wide v4

    add-long/2addr v2, v4

    move-object/from16 v0, p0

    iput-wide v2, v0, Lbhx;->h:J

    .line 113
    :goto_6
    move-object/from16 v0, p0

    iget-object v11, v0, Lbhx;->b:Lbia;

    move-object/from16 v0, p0

    iget-object v12, v0, Lbhx;->a:[Lbhy;

    move-object/from16 v0, p0

    iget-object v2, v0, Lbhx;->c:Lbhz;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lbhx;->h:J

    iget v13, v2, Lbhz;->a:I

    iget v0, v2, Lbhz;->b:I

    move/from16 v16, v0

    iget-object v2, v11, Lbia;->f:[B

    if-eqz v2, :cond_c

    iget-object v2, v11, Lbia;->f:[B

    array-length v2, v2

    if-ge v2, v13, :cond_d

    :cond_c
    new-array v2, v13, [B

    iput-object v2, v11, Lbia;->f:[B

    move-object/from16 v0, p0

    iput v13, v0, Lbhx;->i:I

    :cond_d
    const/4 v3, 0x0

    const/4 v7, 0x0

    const/4 v2, 0x0

    move v8, v2

    move v9, v3

    :goto_7
    array-length v2, v12

    if-ge v8, v2, :cond_1a

    aget-object v17, v12, v8

    move-object/from16 v0, v17

    iget-boolean v2, v0, Lbhy;->j:Z

    if-eqz v2, :cond_f

    move-object/from16 v0, v17

    iget v2, v0, Lbhy;->k:I

    sub-int/2addr v2, v13

    move-object/from16 v0, v17

    iput v2, v0, Lbhy;->k:I

    move-object/from16 v0, v17

    iget v2, v0, Lbhy;->k:I

    if-nez v2, :cond_13

    const/4 v2, 0x0

    move-object/from16 v0, v17

    iput-boolean v2, v0, Lbhy;->j:Z

    const/4 v2, -0x1

    move-object/from16 v0, v17

    iput v2, v0, Lbhy;->k:I

    move-object/from16 v0, v17

    iget v2, v0, Lbhy;->h:I

    move-object/from16 v0, v17

    iget-object v3, v0, Lbhy;->d:Lbma;

    invoke-interface {v3}, Lbma;->a()I

    move-result v3

    if-ne v2, v3, :cond_13

    const/4 v9, 0x1

    move v3, v9

    :goto_8
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    move v9, v3

    goto :goto_7

    .line 109
    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lbhx;->a:[Lbhy;

    aget-object v2, v3, v2

    iget-object v2, v2, Lbhy;->e:Lbhk;

    iget-wide v2, v2, Lbhk;->a:J

    move-object/from16 v0, p0

    iput-wide v2, v0, Lbhx;->h:J

    goto :goto_6

    .line 113
    :cond_f
    move-object/from16 v0, v17

    iget-object v2, v0, Lbhy;->b:Ljava/util/List;

    move-object/from16 v0, v17

    iget v3, v0, Lbhy;->h:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;

    move/from16 v0, v16

    if-ne v8, v0, :cond_14

    move-object/from16 v0, v17

    iget v6, v0, Lbhy;->g:I

    :goto_9
    move-object/from16 v0, v17

    iget-object v3, v0, Lbhy;->e:Lbhk;

    iget v3, v3, Lbhk;->h:I

    if-lez v3, :cond_10

    move-object/from16 v0, v17

    iget-object v3, v0, Lbhy;->d:Lbma;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lbhx;->h:J

    invoke-interface {v3, v4, v5}, Lbma;->a(J)F

    move-result v3

    move-object/from16 v0, p0

    iget v4, v0, Lbhx;->f:F

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3, v3}, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->a(FF)V

    :cond_10
    iget-object v3, v11, Lbia;->f:[B

    move-object/from16 v0, v17

    iget-object v4, v0, Lbhy;->e:Lbhk;

    iget-object v4, v4, Lbhk;->f:Ljava/nio/ByteBuffer;

    move-object/from16 v0, v17

    iget v5, v0, Lbhy;->f:I

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->a([BLjava/nio/ByteBuffer;IIZ)I

    const/4 v7, 0x1

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput v2, v0, Lbhx;->i:I

    move-object/from16 v0, v17

    iget v2, v0, Lbhy;->f:I

    add-int/2addr v2, v6

    move-object/from16 v0, v17

    iput v2, v0, Lbhy;->f:I

    move-object/from16 v0, v17

    iget v2, v0, Lbhy;->g:I

    sub-int/2addr v2, v6

    move-object/from16 v0, v17

    iput v2, v0, Lbhy;->g:I

    move-object/from16 v0, v17

    iget v2, v0, Lbhy;->g:I

    if-gtz v2, :cond_13

    move-object/from16 v0, v17

    iget-object v2, v0, Lbhy;->e:Lbhk;

    iget-boolean v2, v2, Lbhk;->b:Z

    if-eqz v2, :cond_12

    move-object/from16 v0, v17

    iget v2, v0, Lbhy;->h:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, v17

    iput v2, v0, Lbhy;->h:I

    const/4 v2, 0x0

    move-object/from16 v0, v17

    iput-boolean v2, v0, Lbhy;->i:Z

    move-object/from16 v0, v17

    iget v2, v0, Lbhy;->h:I

    move-object/from16 v0, v17

    iget-object v3, v0, Lbhy;->d:Lbma;

    invoke-interface {v3}, Lbma;->a()I

    move-result v3

    if-ge v2, v3, :cond_16

    move-object/from16 v0, v17

    iget-object v2, v0, Lbhy;->d:Lbma;

    move-object/from16 v0, v17

    iget v3, v0, Lbhy;->h:I

    invoke-interface {v2, v3}, Lbma;->b(I)J

    move-result-wide v4

    cmp-long v2, v4, v14

    if-lez v2, :cond_15

    const/4 v2, 0x1

    :goto_a
    move-object/from16 v0, v17

    iput-boolean v2, v0, Lbhy;->j:Z

    move-object/from16 v0, v17

    iget-boolean v2, v0, Lbhy;->j:Z

    if-eqz v2, :cond_11

    sub-long v2, v4, v14

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget v5, v0, Lbhx;->d:I

    move-object/from16 v0, p0

    iget v6, v0, Lbhx;->e:I

    invoke-static {v2, v3, v4, v5, v6}, Lceg;->a(JIII)I

    move-result v2

    move-object/from16 v0, v17

    iput v2, v0, Lbhy;->k:I

    :cond_11
    :goto_b
    const/4 v2, 0x1

    :goto_c
    if-nez v2, :cond_19

    const/4 v2, 0x1

    :goto_d
    or-int/2addr v9, v2

    :cond_12
    move-object/from16 v0, v17

    iget-object v2, v0, Lbhy;->a:Lbht;

    move-object/from16 v0, v17

    iget-object v3, v0, Lbhy;->e:Lbhk;

    invoke-virtual {v2, v3}, Lbht;->a(Lbhj;)V

    const/4 v2, 0x0

    move-object/from16 v0, v17

    iput-object v2, v0, Lbhy;->e:Lbhk;

    :cond_13
    move v3, v9

    goto/16 :goto_8

    :cond_14
    invoke-virtual {v2, v13}, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;->c(I)I

    move-result v6

    goto/16 :goto_9

    :cond_15
    const/4 v2, 0x0

    goto :goto_a

    :cond_16
    move-object/from16 v0, v17

    iget-object v2, v0, Lbhy;->d:Lbma;

    invoke-interface {v2}, Lbma;->b()J

    move-result-wide v4

    cmp-long v2, v4, v14

    if-lez v2, :cond_17

    const/4 v2, 0x1

    :goto_e
    move-object/from16 v0, v17

    iput-boolean v2, v0, Lbhy;->j:Z

    move-object/from16 v0, v17

    iget-boolean v2, v0, Lbhy;->j:Z

    if-eqz v2, :cond_18

    sub-long v2, v4, v14

    const/4 v4, 0x2

    move-object/from16 v0, p0

    iget v5, v0, Lbhx;->d:I

    move-object/from16 v0, p0

    iget v6, v0, Lbhx;->e:I

    invoke-static {v2, v3, v4, v5, v6}, Lceg;->a(JIII)I

    move-result v2

    move-object/from16 v0, v17

    iput v2, v0, Lbhy;->k:I

    goto :goto_b

    :cond_17
    const/4 v2, 0x0

    goto :goto_e

    :cond_18
    const/4 v2, 0x0

    goto :goto_c

    :cond_19
    const/4 v2, 0x0

    goto :goto_d

    :cond_1a
    if-nez v7, :cond_1c

    move-object/from16 v0, p0

    iget v2, v0, Lbhx;->i:I

    if-le v13, v2, :cond_1c

    move-object/from16 v0, p0

    iget v2, v0, Lbhx;->i:I

    :goto_f
    if-ge v2, v13, :cond_1b

    iget-object v3, v11, Lbia;->f:[B

    const/4 v4, 0x0

    aput-byte v4, v3, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_f

    :cond_1b
    move-object/from16 v0, p0

    iput v13, v0, Lbhx;->i:I

    .line 116
    :cond_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lbhx;->b:Lbia;

    .line 117
    move-object/from16 v0, p0

    iget-object v2, v0, Lbhx;->b:Lbia;

    iput v10, v2, Lbia;->g:I

    .line 118
    move-object/from16 v0, p0

    iget-object v2, v0, Lbhx;->b:Lbia;

    const/4 v3, 0x2

    iput v3, v2, Lbia;->c:I

    .line 119
    move-object/from16 v0, p0

    iget-object v2, v0, Lbhx;->b:Lbia;

    iput-boolean v9, v2, Lbia;->b:Z

    .line 120
    move-object/from16 v0, p0

    iget-object v2, v0, Lbhx;->b:Lbia;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lbhx;->h:J

    iput-wide v4, v2, Lbia;->a:J

    .line 122
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lbhx;->g:Z

    .line 123
    move-object/from16 v0, p0

    iget-object v2, v0, Lbhx;->b:Lbia;

    goto/16 :goto_0
.end method

.method public synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    invoke-virtual {p0}, Lbhx;->b()Lbia;

    move-result-object v0

    return-object v0
.end method
