.class final Lhlj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Lhle;

.field final b:Lhkv;


# direct methods
.method public constructor <init>(Lhle;Lhkv;)V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 101
    iput-object p2, p0, Lhlj;->b:Lhkv;

    .line 102
    iput-object p1, p0, Lhlj;->a:Lhle;

    .line 103
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 107
    if-ne p0, p1, :cond_1

    .line 123
    :cond_0
    :goto_0
    return v0

    .line 110
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 111
    goto :goto_0

    .line 114
    :cond_3
    check-cast p1, Lhlj;

    .line 116
    iget-object v2, p0, Lhlj;->b:Lhkv;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lhlj;->b:Lhkv;

    iget-object v3, p1, Lhlj;->b:Lhkv;

    invoke-virtual {v2, v3}, Lhkv;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    .line 117
    goto :goto_0

    .line 116
    :cond_5
    iget-object v2, p1, Lhlj;->b:Lhkv;

    if-nez v2, :cond_4

    .line 119
    :cond_6
    iget-object v2, p0, Lhlj;->a:Lhle;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lhlj;->a:Lhle;

    iget-object v3, p1, Lhlj;->a:Lhle;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    .line 120
    goto :goto_0

    .line 119
    :cond_7
    iget-object v2, p1, Lhlj;->a:Lhle;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 128
    iget-object v0, p0, Lhlj;->a:Lhle;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lhlj;->a:Lhle;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 129
    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lhlj;->b:Lhkv;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lhlj;->b:Lhkv;

    invoke-virtual {v1}, Lhkv;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    .line 130
    return v0

    :cond_1
    move v0, v1

    .line 128
    goto :goto_0
.end method
