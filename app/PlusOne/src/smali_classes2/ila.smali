.class public Lila;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lilb;


# static fields
.field private static a:Lgjp;

.field private static b:Lilb;


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    return-void
.end method

.method public static a(Landroid/content/Context;)Lilb;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lila;->b:Lilb;

    if-nez v0, :cond_0

    .line 29
    new-instance v0, Lila;

    invoke-direct {v0}, Lila;-><init>()V

    sput-object v0, Lila;->b:Lilb;

    .line 31
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lgjp;->a(Landroid/content/Context;)Lgjp;

    move-result-object v0

    sput-object v0, Lila;->a:Lgjp;

    .line 32
    sget-object v0, Lila;->b:Lilb;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 47
    sget-object v0, Lila;->a:Lgjp;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lgjp;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lila;->a:Lgjp;

    invoke-virtual {v0}, Lgjp;->a()V

    .line 38
    return-void
.end method

.method public a(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 52
    const-string v0, "gcm"

    sget-object v1, Lila;->a:Lgjp;

    invoke-virtual {v1, p1}, Lgjp;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 42
    sget-object v0, Lila;->a:Lgjp;

    invoke-virtual {v0}, Lgjp;->b()V

    .line 43
    return-void
.end method

.method public b(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 57
    const-string v0, "send_error"

    sget-object v1, Lila;->a:Lgjp;

    invoke-virtual {v1, p1}, Lgjp;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public c(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 62
    const-string v0, "deleted_messages"

    sget-object v1, Lila;->a:Lgjp;

    invoke-virtual {v1, p1}, Lgjp;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public d(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 67
    sget-object v0, Lila;->a:Lgjp;

    invoke-virtual {v0, p1}, Lgjp;->a(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method
