.class public final Lcft;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcfp;


# instance fields
.field private final a:Landroid/content/ContentResolver;

.field private b:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcft;->a:Landroid/content/ContentResolver;

    .line 27
    return-void
.end method

.method static synthetic a(Lcft;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Lcft;->b:Landroid/database/Cursor;

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcft;->b:Landroid/database/Cursor;

    invoke-static {v0}, Lcek;->a(Ljava/io/Closeable;)V

    .line 65
    const/4 v0, 0x0

    iput-object v0, p0, Lcft;->b:Landroid/database/Cursor;

    .line 66
    return-void
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljej;",
            ">;"
        }
    .end annotation

    .prologue
    .line 31
    invoke-virtual {p0}, Lcft;->close()V

    .line 32
    iget-object v0, p0, Lcft;->a:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/MediaStore$Video$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Landroid/provider/MediaStore$Video;->query(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcft;->b:Landroid/database/Cursor;

    .line 36
    new-instance v0, Lcfu;

    invoke-direct {v0, p0}, Lcfu;-><init>(Lcft;)V

    return-object v0
.end method
