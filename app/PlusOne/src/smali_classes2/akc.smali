.class public final Lakc;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lacs;

.field private final b:Lbjf;


# direct methods
.method public constructor <init>(Lacs;Lbjf;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const-string v0, "mffContext"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacs;

    iput-object v0, p0, Lakc;->a:Lacs;

    .line 41
    const-string v0, "bitmapFactory"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjf;

    iput-object v0, p0, Lakc;->b:Lbjf;

    .line 42
    return-void
.end method

.method private a()Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;
    .locals 2

    .prologue
    .line 128
    :try_start_0
    invoke-static {}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->a()Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 137
    :goto_0
    return-object v0

    .line 134
    :catch_0
    move-exception v0

    iget-object v0, p0, Lakc;->a:Lacs;

    .line 135
    invoke-virtual {v0}, Lacs;->b()Landroid/content/Context;

    move-result-object v0

    const-string v1, "frsdk_modules/PFFprec_600.emd"

    .line 134
    invoke-static {v0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Laan;)V
    .locals 9

    .prologue
    const v8, 0x7f080021

    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    .line 78
    const-string v0, "faceTracker"

    invoke-virtual {p1, v0}, Laan;->a(Ljava/lang/String;)Laak;

    move-result-object v0

    check-cast v0, Lbbn;

    .line 79
    invoke-direct {p0}, Lakc;->a()Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;

    move-result-object v1

    .line 81
    :try_start_0
    invoke-interface {v0, v1}, Lbbn;->a(Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    invoke-virtual {v1}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->d()V

    .line 88
    const-string v2, "sharpnessScorer"

    const v3, 0x7f08002a

    const-string v4, "score"

    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    .line 89
    invoke-static {v0}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    .line 88
    invoke-direct/range {v0 .. v5}, Lakc;->a(Laan;Ljava/lang/String;ILjava/lang/String;Labf;)V

    .line 90
    const-string v2, "faceIlluminationScorer"

    const v3, 0x7f08001b

    const-string v4, "score"

    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    .line 91
    invoke-static {v0}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    .line 90
    invoke-direct/range {v0 .. v5}, Lakc;->a(Laan;Ljava/lang/String;ILjava/lang/String;Labf;)V

    .line 92
    const-string v2, "newChromaHistogram"

    const-string v4, "newChromaHistogramOut"

    const/16 v0, 0xc8

    .line 93
    invoke-static {v0}, Labf;->b(I)Labf;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move v3, v8

    .line 92
    invoke-direct/range {v0 .. v5}, Lakc;->a(Laan;Ljava/lang/String;ILjava/lang/String;Labf;)V

    .line 94
    const-string v2, "newChromaHistogram"

    const-string v4, "newColorfulnessOut"

    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    .line 95
    invoke-static {v0}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move v3, v8

    .line 94
    invoke-direct/range {v0 .. v5}, Lakc;->a(Laan;Ljava/lang/String;ILjava/lang/String;Labf;)V

    .line 98
    const-string v0, "defaultImageQuad"

    invoke-virtual {p1, v0}, Laan;->b(Ljava/lang/String;)Landroidx/media/filterpacks/base/VariableSource;

    move-result-object v0

    invoke-static {v6, v6, v7, v7}, Ladp;->a(FFFF)Ladp;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroidx/media/filterpacks/base/VariableSource;->a(Ljava/lang/Object;)V

    .line 99
    return-void

    .line 84
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/apps/moviemaker/filterpacks/face/FaceDetector$Module;->d()V

    throw v0
.end method

.method private a(Laan;Ljava/lang/String;ILjava/lang/String;Labf;)V
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lakc;->a:Lacs;

    invoke-static {v0, p3, p1}, Labj;->a(Lacs;ILaan;)Laan;

    move-result-object v1

    .line 122
    invoke-virtual {v1, p4}, Laan;->c(Ljava/lang/String;)Landroidx/media/filterpacks/base/GraphOutputTarget;

    move-result-object v0

    invoke-virtual {v0, p5}, Landroidx/media/filterpacks/base/GraphOutputTarget;->a(Labf;)V

    .line 123
    invoke-virtual {p1, p2}, Laan;->a(Ljava/lang/String;)Laak;

    move-result-object v0

    check-cast v0, Landroidx/media/filterpacks/base/MetaFilter;

    .line 124
    invoke-virtual {v0, v1}, Landroidx/media/filterpacks/base/MetaFilter;->a(Laan;)V

    .line 125
    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)Laan;
    .locals 4

    .prologue
    .line 49
    iget-object v0, p0, Lakc;->a:Lacs;

    const v1, 0x7f080001

    invoke-static {v0, v1}, Labj;->a(Lacs;I)Laan;

    move-result-object v0

    .line 51
    invoke-direct {p0, v0}, Lakc;->a(Laan;)V

    .line 52
    const-string v1, "path"

    invoke-virtual {v0, v1}, Laan;->b(Ljava/lang/String;)Landroidx/media/filterpacks/base/VariableSource;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroidx/media/filterpacks/base/VariableSource;->a(Ljava/lang/Object;)V

    .line 53
    const-string v1, "decoderStartTime"

    invoke-virtual {v0, v1}, Laan;->b(Ljava/lang/String;)Landroidx/media/filterpacks/base/VariableSource;

    move-result-object v1

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroidx/media/filterpacks/base/VariableSource;->a(Ljava/lang/Object;)V

    .line 55
    return-object v0
.end method

.method public b(Landroid/net/Uri;)Laan;
    .locals 6

    .prologue
    .line 63
    iget-object v0, p0, Lakc;->a:Lacs;

    const v1, 0x7f080002

    invoke-static {v0, v1}, Labj;->a(Lacs;I)Laan;

    move-result-object v1

    .line 66
    invoke-direct {p0, v1}, Lakc;->a(Laan;)V

    .line 67
    const-string v2, "faceSharpnessScorer"

    const v3, 0x7f08002a

    const-string v4, "score"

    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v0}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lakc;->a(Laan;Ljava/lang/String;ILjava/lang/String;Labf;)V

    .line 68
    const-string v0, "bitmapImage"

    invoke-virtual {v1, v0}, Laan;->b(Ljava/lang/String;)Landroidx/media/filterpacks/base/VariableSource;

    move-result-object v0

    iget-object v2, p0, Lakc;->b:Lbjf;

    iget-object v3, p0, Lakc;->a:Lacs;

    invoke-virtual {v3}, Lacs;->b()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const v4, 0x12c00

    invoke-virtual {v2, p1, v3, v4}, Lbjf;->a(Landroid/net/Uri;Landroid/content/ContentResolver;I)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroidx/media/filterpacks/base/VariableSource;->a(Ljava/lang/Object;)V

    .line 70
    return-object v1
.end method
