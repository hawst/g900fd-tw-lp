.class final Lbqn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lbgf;

.field private final b:Lbon;

.field private final c:[J

.field private final d:I

.field private final e:I

.field private final f:Lbqm;

.field private synthetic g:Lbqk;


# direct methods
.method public constructor <init>(Lbqk;Lbgf;Lbon;[JIILbqm;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 230
    iput-object p1, p0, Lbqn;->g:Lbqk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 231
    const-string v0, "renderContext"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgf;

    iput-object v0, p0, Lbqn;->a:Lbgf;

    .line 232
    const-string v0, "video"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbon;

    iput-object v0, p0, Lbqn;->b:Lbon;

    .line 233
    const-string v0, "timestampsUs"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    iput-object v0, p0, Lbqn;->c:[J

    .line 234
    const-string v0, "displayWidth"

    invoke-static {p5, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lbqn;->d:I

    .line 235
    const-string v0, "displayHeight"

    invoke-static {p6, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lbqn;->e:I

    .line 236
    const-string v0, "callback"

    invoke-static {p7, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqm;

    iput-object v0, p0, Lbqn;->f:Lbqm;

    .line 237
    return-void
.end method

.method private a(Lbji;Lbhl;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbji",
            "<",
            "Lbhl;",
            ">;",
            "Lbhl;",
            ")V"
        }
    .end annotation

    .prologue
    .line 344
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 345
    :try_start_0
    invoke-virtual {p1, p2}, Lbji;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 350
    :cond_0
    :goto_0
    return-void

    .line 347
    :catch_0
    move-exception v0

    .line 348
    invoke-static {}, Lbqk;->a()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Exception thrown while releasing the polled video frame"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method public a()Landroid/graphics/Bitmap;
    .locals 14

    .prologue
    const/4 v2, 0x0

    .line 241
    .line 246
    const-string v0, "decodeFrames()"

    invoke-static {v0}, Laep;->a(Ljava/lang/String;)V

    .line 255
    :try_start_0
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 256
    new-instance v9, Landroid/graphics/Matrix;

    invoke-direct {v9}, Landroid/graphics/Matrix;-><init>()V

    .line 257
    new-instance v10, Landroid/graphics/Matrix;

    invoke-direct {v10}, Landroid/graphics/Matrix;-><init>()V

    .line 258
    iget-object v0, p0, Lbqn;->b:Lbon;

    invoke-virtual {v0}, Lbon;->g()Lboo;

    move-result-object v0

    iget v0, v0, Lboo;->a:I

    invoke-static {v10, v0}, Lbqk;->a(Landroid/graphics/Matrix;I)V

    .line 260
    const-string v0, "init codec"

    invoke-static {v0}, Laep;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_6

    .line 262
    :try_start_1
    new-instance v1, Lbja;

    iget-object v0, p0, Lbqn;->g:Lbqk;

    .line 263
    invoke-static {v0}, Lbqk;->a(Lbqk;)Lbig;

    move-result-object v0

    iget-object v3, p0, Lbqn;->b:Lbon;

    invoke-virtual {v3}, Lbon;->a()Ljeg;

    move-result-object v3

    invoke-direct {v1, v0, v3}, Lbja;-><init>(Lbig;Ljeg;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    .line 264
    :try_start_2
    invoke-virtual {v1}, Lbja;->c()V

    .line 265
    iget-object v0, p0, Lbqn;->g:Lbqk;

    invoke-static {v0}, Lbqk;->b(Lbqk;)Lbjp;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbjp;->a(Lbiy;)Lbji;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_b

    move-result-object v4

    .line 267
    :try_start_3
    invoke-static {}, Laep;->a()V

    .line 270
    iget-object v0, p0, Lbqn;->a:Lbgf;

    .line 271
    invoke-virtual {v0}, Lbgf;->f()Lbgr;

    move-result-object v0

    const v3, 0x7f08002e

    const v5, 0x7f08000f

    invoke-virtual {v0, v3, v5}, Lbgr;->a(II)Lbgx;

    move-result-object v11

    .line 274
    iget-object v0, p0, Lbqn;->a:Lbgf;

    iget v3, p0, Lbqn;->d:I

    iget v5, p0, Lbqn;->e:I

    invoke-virtual {v0, v3, v5}, Lbgf;->a(II)Lbge;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_7

    move-result-object v5

    .line 276
    const/4 v0, 0x0

    move v6, v0

    move-object v3, v4

    move-object v0, v2

    :goto_0
    :try_start_4
    iget-object v4, p0, Lbqn;->c:[J

    array-length v4, v4

    if-ge v6, v4, :cond_3

    .line 277
    iget-object v0, p0, Lbqn;->c:[J

    aget-wide v12, v0, v6

    .line 279
    const-string v0, "seek and poll"

    invoke-static {v0}, Laep;->a(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_9

    .line 281
    :try_start_5
    invoke-virtual {v1, v12, v13}, Lbja;->a(J)Z

    .line 287
    iget-object v0, p0, Lbqn;->g:Lbqk;

    invoke-static {v0}, Lbqk;->b(Lbqk;)Lbjp;

    move-result-object v0

    invoke-virtual {v0, v3}, Lbjp;->a(Lbji;)V

    .line 288
    iget-object v0, p0, Lbqn;->g:Lbqk;

    invoke-static {v0}, Lbqk;->b(Lbqk;)Lbjp;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbjp;->a(Lbiy;)Lbji;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_a

    move-result-object v4

    move-object v3, v2

    .line 290
    :goto_1
    if-nez v3, :cond_1

    .line 291
    :try_start_6
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 292
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 297
    :catchall_0
    move-exception v0

    move-object v2, v3

    move-object v3, v4

    :goto_2
    :try_start_7
    invoke-static {}, Laep;->a()V

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 332
    :catchall_1
    move-exception v0

    move-object v4, v3

    move-object v3, v2

    move-object v2, v5

    :goto_3
    invoke-direct {p0, v4, v3}, Lbqn;->a(Lbji;Lbhl;)V

    .line 333
    invoke-static {v2}, Lcgl;->a(Lcgk;)V

    .line 334
    iget-object v2, p0, Lbqn;->g:Lbqk;

    invoke-static {v2}, Lbqk;->b(Lbqk;)Lbjp;

    move-result-object v2

    invoke-virtual {v2, v4}, Lbjp;->a(Lbji;)V

    .line 335
    invoke-static {v1}, Lcgl;->a(Lcgk;)V

    .line 336
    invoke-static {}, Laep;->a()V

    throw v0

    .line 267
    :catchall_2
    move-exception v0

    move-object v1, v2

    :goto_4
    :try_start_8
    invoke-static {}, Laep;->a()V

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 332
    :catchall_3
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    goto :goto_3

    .line 294
    :cond_0
    :try_start_9
    invoke-virtual {v4}, Lbji;->b()Lbhj;

    move-result-object v0

    check-cast v0, Lbhl;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-object v3, v0

    goto :goto_1

    .line 297
    :cond_1
    :try_start_a
    invoke-static {}, Laep;->a()V

    .line 301
    iget-object v0, v3, Lbhl;->d:[F

    invoke-static {v9, v0}, Lcfn;->a(Landroid/graphics/Matrix;[F)V

    .line 302
    invoke-virtual {v8, v10}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 303
    invoke-virtual {v8, v9}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 305
    iget-object v0, v3, Lbhl;->c:Lbhb;

    invoke-static {v5, v0, v8, v11}, Lbfu;->a(Lbgn;Lbhb;Landroid/graphics/Matrix;Lbgx;)V

    .line 312
    iget v0, p0, Lbqn;->d:I

    iget v7, p0, Lbqn;->e:I

    sget-object v12, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v7, v12}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v7

    .line 314
    const-string v0, "decodeFrames()"

    invoke-static {v0}, Laep;->a(Ljava/lang/String;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_4

    .line 316
    :try_start_b
    iget-object v0, p0, Lbqn;->a:Lbgf;

    invoke-virtual {v0, v5, v7}, Lbgf;->a(Lbgn;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_5

    .line 318
    :try_start_c
    invoke-static {}, Laep;->a()V

    .line 321
    iget-boolean v0, v3, Lbhl;->b:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbqn;->c:[J

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-eq v6, v0, :cond_2

    .line 322
    const-string v0, "Unexpected end-of-stream while extracting key-frame poster"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 332
    :catchall_4
    move-exception v0

    move-object v2, v5

    goto :goto_3

    .line 318
    :catchall_5
    move-exception v0

    invoke-static {}, Laep;->a()V

    throw v0

    .line 326
    :cond_2
    invoke-direct {p0, v4, v3}, Lbqn;->a(Lbji;Lbhl;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 329
    :try_start_d
    iget-object v0, p0, Lbqn;->f:Lbqm;

    invoke-interface {v0, v7, v6}, Lbqm;->a(Landroid/graphics/Bitmap;I)V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_8

    .line 276
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move-object v3, v4

    move-object v0, v7

    goto/16 :goto_0

    .line 332
    :cond_3
    invoke-direct {p0, v3, v2}, Lbqn;->a(Lbji;Lbhl;)V

    .line 333
    invoke-static {v5}, Lcgl;->a(Lcgk;)V

    .line 334
    iget-object v2, p0, Lbqn;->g:Lbqk;

    invoke-static {v2}, Lbqk;->b(Lbqk;)Lbjp;

    move-result-object v2

    invoke-virtual {v2, v3}, Lbjp;->a(Lbji;)V

    .line 335
    invoke-static {v1}, Lcgl;->a(Lcgk;)V

    .line 336
    invoke-static {}, Laep;->a()V

    .line 338
    return-object v0

    .line 332
    :catchall_6
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    move-object v1, v2

    goto/16 :goto_3

    :catchall_7
    move-exception v0

    move-object v3, v2

    goto/16 :goto_3

    :catchall_8
    move-exception v0

    move-object v3, v2

    move-object v2, v5

    goto/16 :goto_3

    :catchall_9
    move-exception v0

    move-object v4, v3

    move-object v3, v2

    move-object v2, v5

    goto/16 :goto_3

    .line 297
    :catchall_a
    move-exception v0

    goto/16 :goto_2

    .line 267
    :catchall_b
    move-exception v0

    goto/16 :goto_4
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 215
    invoke-virtual {p0}, Lbqn;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
