.class public final Lfrw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Landroid/animation/ObjectAnimator;",
            ">;>;"
        }
    .end annotation
.end field

.field final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lfsk;",
            ">;>;"
        }
    .end annotation
.end field

.field c:Landroid/os/Handler;

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Landroid/animation/ObjectAnimator;",
            ">;>;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lfsi;

.field private final g:Lfsf;

.field private final h:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation
.end field

.field private i:I


# direct methods
.method public constructor <init>(Lfsf;)V
    .locals 1

    .prologue
    .line 382
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfrw;->h:Ljava/util/Map;

    .line 61
    const/4 v0, -0x1

    iput v0, p0, Lfrw;->i:I

    .line 383
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfrw;->d:Ljava/util/Map;

    .line 384
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfrw;->b:Ljava/util/Map;

    .line 385
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfrw;->a:Ljava/util/Map;

    .line 386
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfrw;->e:Ljava/util/Set;

    .line 387
    iput-object p1, p0, Lfrw;->g:Lfsf;

    .line 388
    return-void
.end method

.method static a(II)Ljava/lang/String;
    .locals 2

    .prologue
    .line 281
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x17

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/view/View;Ljava/lang/String;)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 649
    if-eqz p2, :cond_0

    const-string v0, "verticalFold"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 650
    :cond_0
    instance-of v0, p1, Lftv;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 651
    check-cast v0, Lftv;

    invoke-virtual {v0, v1}, Lftv;->setVerticalFold(F)V

    .line 669
    :cond_1
    :goto_0
    invoke-virtual {p0, p1, p2}, Lfrw;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 673
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 674
    return-void

    .line 653
    :cond_2
    if-eqz p2, :cond_3

    const-string v0, "linePercentDrawn"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 654
    :cond_3
    instance-of v0, p1, Lftz;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 655
    check-cast v0, Lftz;

    invoke-virtual {v0, v1}, Lftz;->setLinePercentDrawn(F)V

    goto :goto_0

    .line 657
    :cond_4
    if-eqz p2, :cond_5

    const-string v0, "alpha"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 658
    :cond_5
    invoke-virtual {p1, v1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0

    .line 659
    :cond_6
    if-eqz p2, :cond_7

    const-string v0, "translationX"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 660
    :cond_7
    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_0

    .line 661
    :cond_8
    if-eqz p2, :cond_9

    const-string v0, "translationY"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 662
    :cond_9
    invoke-virtual {p1, v2}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0

    .line 663
    :cond_a
    if-eqz p2, :cond_b

    const-string v0, "scaleX"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 664
    :cond_b
    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleX(F)V

    goto :goto_0

    .line 665
    :cond_c
    if-eqz p2, :cond_d

    const-string v0, "scaleY"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 666
    :cond_d
    invoke-virtual {p1, v1}, Landroid/view/View;->setScaleY(F)V

    goto :goto_0
.end method


# virtual methods
.method public a(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 826
    iget-object v0, p0, Lfrw;->h:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 827
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 839
    :cond_0
    :goto_0
    return-void

    .line 831
    :cond_1
    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 832
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 833
    iget-object v0, p0, Lfrw;->h:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 834
    iget v0, p0, Lfrw;->i:I

    if-ne v0, p1, :cond_0

    .line 835
    invoke-virtual {p0, p1}, Lfrw;->a(I)Z

    .line 836
    const/4 v0, -0x1

    iput v0, p0, Lfrw;->i:I

    goto :goto_0
.end method

.method public a(ILjava/lang/String;I)V
    .locals 4

    .prologue
    .line 808
    iget-object v0, p0, Lfrw;->h:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 809
    iget-object v0, p0, Lfrw;->h:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 811
    :cond_0
    iget-object v0, p0, Lfrw;->h:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 813
    iget-object v0, p0, Lfrw;->c:Landroid/os/Handler;

    new-instance v1, Lfsa;

    invoke-direct {v1, p0, p1, p2}, Lfsa;-><init>(Lfrw;ILjava/lang/String;)V

    int-to-long v2, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 819
    return-void
.end method

.method public a(Landroid/os/Handler;)V
    .locals 0

    .prologue
    .line 391
    iput-object p1, p0, Lfrw;->c:Landroid/os/Handler;

    .line 392
    return-void
.end method

.method public a(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 546
    invoke-static {p1}, Lfrc;->a(Landroid/view/View;)Lfrc;

    move-result-object v0

    invoke-virtual {v0}, Lfrc;->U()I

    move-result v2

    iget-object v0, p0, Lfrw;->d:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lfrw;->d:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->getTarget()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-nez v1, :cond_1

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    invoke-direct {p0, p1, v5}, Lfrw;->b(Landroid/view/View;Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->getPropertyName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lfrw;->b(Landroid/view/View;Ljava/lang/String;)V

    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lfrw;->d:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lfrw;->d:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_3
    iget-object v0, p0, Lfrw;->a:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lfrw;->a:Ljava/util/Map;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->getTarget()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-nez v1, :cond_5

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    invoke-direct {p0, p1, v5}, Lfrw;->b(Landroid/view/View;Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :cond_5
    invoke-virtual {v1, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->getPropertyName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lfrw;->b(Landroid/view/View;Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 547
    :cond_6
    invoke-static {p1}, Lfrc;->a(Landroid/view/View;)Lfrc;

    move-result-object v0

    invoke-virtual {v0}, Lfrc;->U()I

    move-result v0

    invoke-static {p1}, Lfrc;->a(Landroid/view/View;)Lfrc;

    move-result-object v1

    invoke-virtual {v1}, Lfrc;->t()I

    move-result v1

    invoke-static {v1, v0}, Lfrw;->a(II)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lfrw;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lfrw;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfsk;

    invoke-virtual {v0}, Lfsk;->a()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {v0}, Lfsk;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v3, v0}, Lfrw;->b(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_3

    :cond_8
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_2

    :cond_9
    iget-object v0, p0, Lfrw;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_a

    iget-object v0, p0, Lfrw;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 548
    :cond_a
    return-void
.end method

.method a(Landroid/view/View;Ljava/lang/String;)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    const v3, 0x7f100040

    .line 843
    invoke-virtual {p1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 844
    check-cast v0, Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    .line 846
    const-string v0, "alpha"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 847
    invoke-virtual {p1}, Landroid/view/View;->getAlpha()F

    move-result v0

    cmpl-float v0, v0, v4

    if-nez v0, :cond_2

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v3, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 858
    :cond_0
    :goto_1
    invoke-virtual {p1, v3}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 859
    check-cast v0, Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    .line 861
    if-eq v2, v0, :cond_1

    .line 862
    iget-object v0, p0, Lfrw;->g:Lfsf;

    invoke-interface {v0}, Lfsf;->a()V

    .line 864
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    .line 847
    goto :goto_0

    .line 848
    :cond_3
    const-string v0, "scaleX"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 849
    invoke-virtual {p1}, Landroid/view/View;->getScaleX()F

    move-result v0

    cmpl-float v0, v0, v4

    if-nez v0, :cond_4

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :cond_4
    invoke-virtual {p1, v3, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_1

    .line 850
    :cond_5
    const-string v0, "scaleY"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 851
    invoke-virtual {p1}, Landroid/view/View;->getScaleY()F

    move-result v0

    cmpl-float v0, v0, v4

    if-nez v0, :cond_6

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    :cond_6
    invoke-virtual {p1, v3, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_1

    .line 852
    :cond_7
    const-string v0, "verticalFold"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 853
    instance-of v0, p1, Lftv;

    if-eqz v0, :cond_8

    move-object v0, p1

    check-cast v0, Lftv;

    .line 855
    invoke-virtual {v0}, Lftv;->e()F

    move-result v0

    cmpl-float v0, v0, v4

    if-nez v0, :cond_8

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 853
    :cond_8
    invoke-virtual {p1, v3, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_1
.end method

.method public a(Lfrt;)V
    .locals 1

    .prologue
    .line 872
    iget-object v0, p0, Lfrw;->f:Lfsi;

    if-eqz v0, :cond_0

    .line 873
    iget-object v0, p0, Lfrw;->f:Lfsi;

    invoke-virtual {v0, p1}, Lfsi;->a(Lfrt;)V

    .line 875
    :cond_0
    return-void
.end method

.method public a(Lfsc;JILandroid/view/View;)V
    .locals 10
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x2

    .line 441
    iget-object v0, p0, Lfrw;->e:Ljava/util/Set;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 539
    :cond_0
    :goto_0
    return-void

    .line 446
    :cond_1
    iget-object v0, p0, Lfrw;->d:Ljava/util/Map;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 447
    iget-object v0, p0, Lfrw;->d:Ljava/util/Map;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 450
    :cond_2
    iget-object v0, p0, Lfrw;->d:Ljava/util/Map;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 451
    const/4 v1, 0x0

    .line 452
    instance-of v2, p1, Lfso;

    if-eqz v2, :cond_3

    .line 453
    const-string v1, "verticalFold"

    new-array v2, v6, [F

    fill-array-data v2, :array_0

    invoke-static {p5, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 454
    iget-wide v2, p1, Lfsc;->c:J

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 455
    invoke-virtual {v1, p2, p3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 456
    iget-object v2, p1, Lfsc;->d:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 457
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 459
    iget-boolean v0, p1, Lfsc;->e:Z

    if-eqz v0, :cond_a

    .line 460
    instance-of v0, p5, Lftv;

    if-eqz v0, :cond_a

    move-object v0, p5

    .line 461
    check-cast v0, Lftv;

    invoke-virtual {v0, v4}, Lftv;->setVerticalFold(F)V

    move-object v0, v1

    .line 536
    :goto_1
    if-eqz v0, :cond_0

    .line 537
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->getPropertyName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p5, v0}, Lfrw;->a(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_0

    .line 464
    :cond_3
    instance-of v2, p1, Lfsb;

    if-eqz v2, :cond_5

    move-object v1, p1

    .line 465
    check-cast v1, Lfsb;

    .line 466
    const-string v2, "alpha"

    new-array v3, v6, [F

    iget v4, v1, Lfsb;->a:F

    aput v4, v3, v7

    iget v4, v1, Lfsb;->b:F

    aput v4, v3, v8

    invoke-static {p5, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 468
    iget-wide v4, p1, Lfsc;->c:J

    invoke-virtual {v2, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 469
    invoke-virtual {v2, p2, p3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 470
    iget-object v3, p1, Lfsc;->d:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 471
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 473
    iget-boolean v0, p1, Lfsc;->e:Z

    if-eqz v0, :cond_4

    .line 474
    iget v0, v1, Lfsb;->a:F

    invoke-virtual {p5, v0}, Landroid/view/View;->setAlpha(F)V

    :cond_4
    move-object v0, v2

    .line 476
    goto :goto_1

    :cond_5
    instance-of v2, p1, Lfsn;

    if-eqz v2, :cond_7

    move-object v1, p1

    .line 477
    check-cast v1, Lfsn;

    .line 479
    const-string v2, "translationX"

    new-array v3, v6, [F

    iget v4, v1, Lfsn;->a:I

    int-to-float v4, v4

    aput v4, v3, v7

    iget v4, v1, Lfsn;->f:I

    int-to-float v4, v4

    aput v4, v3, v8

    invoke-static {p5, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 482
    iget-wide v4, p1, Lfsc;->c:J

    invoke-virtual {v2, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 483
    invoke-virtual {v2, p2, p3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 484
    iget-object v3, p1, Lfsc;->d:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 485
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v2, p1

    .line 487
    check-cast v2, Lfsn;

    .line 489
    const-string v3, "translationY"

    new-array v4, v6, [F

    iget v5, v2, Lfsn;->b:I

    int-to-float v5, v5

    aput v5, v4, v7

    iget v2, v2, Lfsn;->g:I

    int-to-float v2, v2

    aput v2, v4, v8

    invoke-static {p5, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 492
    iget-wide v4, p1, Lfsc;->c:J

    invoke-virtual {v2, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 493
    invoke-virtual {v2, p2, p3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 494
    iget-object v3, p1, Lfsc;->d:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 495
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 497
    iget-boolean v0, p1, Lfsc;->e:Z

    if-eqz v0, :cond_6

    .line 498
    iget v0, v1, Lfsn;->a:I

    int-to-float v0, v0

    invoke-virtual {p5, v0}, Landroid/view/View;->setTranslationX(F)V

    .line 499
    iget v0, v1, Lfsn;->b:I

    int-to-float v0, v0

    invoke-virtual {p5, v0}, Landroid/view/View;->setTranslationY(F)V

    :cond_6
    move-object v0, v2

    .line 501
    goto/16 :goto_1

    :cond_7
    instance-of v2, p1, Lfsh;

    if-eqz v2, :cond_9

    move-object v1, p1

    .line 502
    check-cast v1, Lfsh;

    .line 504
    const-string v2, "scaleX"

    new-array v3, v6, [F

    iget v4, v1, Lfsh;->a:F

    aput v4, v3, v7

    iget v4, v1, Lfsh;->b:F

    aput v4, v3, v8

    invoke-static {p5, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 507
    iget-wide v4, p1, Lfsc;->c:J

    invoke-virtual {v2, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 508
    invoke-virtual {v2, p2, p3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 509
    iget-object v3, p1, Lfsc;->d:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 510
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 512
    const-string v2, "scaleY"

    new-array v3, v6, [F

    iget v4, v1, Lfsh;->f:F

    aput v4, v3, v7

    iget v4, v1, Lfsh;->g:F

    aput v4, v3, v8

    invoke-static {p5, v2, v3}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 515
    iget-wide v4, p1, Lfsc;->c:J

    invoke-virtual {v2, v4, v5}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 516
    invoke-virtual {v2, p2, p3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 517
    iget-object v3, p1, Lfsc;->d:Landroid/view/animation/Interpolator;

    invoke-virtual {v2, v3}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 518
    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 520
    iget-boolean v0, p1, Lfsc;->e:Z

    if-eqz v0, :cond_8

    .line 521
    iget v0, v1, Lfsh;->a:F

    invoke-virtual {p5, v0}, Landroid/view/View;->setScaleX(F)V

    .line 522
    iget v0, v1, Lfsh;->f:F

    invoke-virtual {p5, v0}, Landroid/view/View;->setScaleY(F)V

    :cond_8
    move-object v0, v2

    .line 524
    goto/16 :goto_1

    :cond_9
    instance-of v2, p1, Lfse;

    if-eqz v2, :cond_b

    .line 525
    const-string v1, "linePercentDrawn"

    new-array v2, v6, [F

    fill-array-data v2, :array_1

    invoke-static {p5, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 526
    iget-wide v2, p1, Lfsc;->c:J

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 527
    invoke-virtual {v1, p2, p3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 528
    iget-object v2, p1, Lfsc;->d:Landroid/view/animation/Interpolator;

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 529
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 531
    iget-boolean v0, p1, Lfsc;->e:Z

    if-eqz v0, :cond_a

    instance-of v0, p5, Lftz;

    if-eqz v0, :cond_a

    move-object v0, p5

    .line 532
    check-cast v0, Lftz;

    invoke-virtual {v0, v4}, Lftz;->setLinePercentDrawn(F)V

    :cond_a
    move-object v0, v1

    goto/16 :goto_1

    :cond_b
    move-object v0, v1

    goto/16 :goto_1

    .line 453
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 525
    :array_1
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public a(Lfsc;ZIILandroid/view/View;)V
    .locals 3

    .prologue
    .line 410
    invoke-static {p3, p4}, Lfrw;->a(II)Ljava/lang/String;

    move-result-object v0

    .line 411
    iget-object v1, p0, Lfrw;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 412
    iget-object v1, p0, Lfrw;->b:Ljava/util/Map;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    :cond_0
    iget-object v1, p0, Lfrw;->b:Ljava/util/Map;

    .line 416
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 418
    new-instance v1, Lfsk;

    invoke-direct {v1, p0, p1, p2, p5}, Lfsk;-><init>(Lfrw;Lfsc;ZLandroid/view/View;)V

    .line 419
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 422
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lfsk;->a(F)V

    .line 426
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p5, v0, v1}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 427
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 399
    iget-object v0, p0, Lfrw;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 400
    return-void
.end method

.method public a([[Landroid/graphics/Point;I)V
    .locals 2

    .prologue
    .line 285
    new-instance v0, Lfsi;

    new-instance v1, Lfrx;

    invoke-direct {v1, p0}, Lfrx;-><init>(Lfrw;)V

    invoke-direct {v0, p1, p2, v1}, Lfsi;-><init>([[Landroid/graphics/Point;ILfsj;)V

    iput-object v0, p0, Lfrw;->f:Lfsi;

    .line 293
    return-void
.end method

.method public a(I)Z
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v7, 0x0

    const v6, 0x7f100040

    const/4 v3, 0x1

    .line 695
    iget-object v0, p0, Lfrw;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 696
    const/4 v0, 0x0

    .line 786
    :goto_0
    return v0

    .line 699
    :cond_0
    iget-object v0, p0, Lfrw;->h:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfrw;->h:Ljava/util/Map;

    .line 700
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 701
    iput p1, p0, Lfrw;->i:I

    move v0, v3

    .line 702
    goto :goto_0

    .line 705
    :cond_1
    iget-object v0, p0, Lfrw;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 707
    new-instance v4, Lfsm;

    invoke-direct {v4, v0}, Lfsm;-><init>(Ljava/util/List;)V

    .line 711
    iget-object v1, p0, Lfrw;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 713
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 714
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/animation/ObjectAnimator;

    .line 715
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->getTarget()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 717
    if-nez v1, :cond_2

    .line 721
    invoke-interface {v5}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    .line 727
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {v1, v2, v7}, Landroid/view/View;->setLayerType(ILandroid/graphics/Paint;)V

    .line 729
    new-instance v2, Lfry;

    invoke-direct {v2, p0, v1, v4, p1}, Lfry;-><init>(Lfrw;Landroid/view/View;Lfsm;I)V

    invoke-virtual {v0, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 775
    invoke-virtual {v1, v6}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    .line 776
    check-cast v2, Ljava/lang/Boolean;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 777
    invoke-virtual {v1, v6, v7}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 778
    iget-object v1, p0, Lfrw;->g:Lfsf;

    invoke-interface {v1}, Lfsf;->a()V

    .line 781
    :cond_3
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_1

    .line 784
    :cond_4
    iget-object v0, p0, Lfrw;->d:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 785
    iget-object v0, p0, Lfrw;->e:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move v0, v3

    .line 786
    goto/16 :goto_0
.end method
