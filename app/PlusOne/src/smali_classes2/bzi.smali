.class public final Lbzi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbzf;


# static fields
.field public static final a:Lbzi;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lbzi;

    invoke-direct {v0}, Lbzi;-><init>()V

    sput-object v0, Lbzi;->a:Lbzi;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;Ljava/util/List;)J
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbon;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmw;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 79
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public a(Ljava/util/List;Ljava/util/List;Ljava/util/Map;Lbor;Lbzg;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbon;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmw;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;",
            "Lbor;",
            "Lbzg;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/32 v8, 0x16e360

    .line 41
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 42
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbon;

    .line 43
    new-instance v3, Lbmp;

    const-wide/16 v4, 0x0

    .line 44
    invoke-virtual {v0}, Lbon;->e()J

    move-result-wide v6

    invoke-direct {v3, v4, v5, v6, v7}, Lbmp;-><init>(JJ)V

    .line 46
    invoke-virtual {v0}, Lbon;->c()Lbol;

    move-result-object v4

    invoke-virtual {v4}, Lbol;->a()Lbkp;

    move-result-object v4

    .line 47
    if-eqz v4, :cond_2

    .line 48
    const/4 v5, 0x3

    .line 49
    invoke-virtual {v4, v5}, Lbkp;->b(I)Ljava/util/List;

    move-result-object v4

    .line 55
    invoke-static {v3, v4}, Lbpx;->a(Lbmp;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v3

    .line 58
    invoke-static {v0}, Lbok;->a(Lbml;)Lbok;

    move-result-object v0

    .line 57
    invoke-static {v0, v3}, Lbpm;->a(Lbok;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 59
    invoke-virtual {v0}, Lbmd;->b()J

    move-result-wide v4

    cmp-long v4, v4, v8

    if-ltz v4, :cond_1

    .line 60
    invoke-virtual {p4, v0}, Lbor;->a(Lbmd;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 61
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 64
    :cond_2
    invoke-virtual {v3}, Lbmp;->a()J

    move-result-wide v4

    cmp-long v4, v4, v8

    if-ltz v4, :cond_0

    .line 67
    invoke-static {v0}, Lbok;->a(Lbml;)Lbok;

    move-result-object v0

    .line 66
    invoke-static {v0, v3}, Lbpm;->a(Lbok;Lbmp;)Lbmd;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 71
    :cond_3
    new-instance v0, Lbpo;

    invoke-direct {v0, p3}, Lbpo;-><init>(Ljava/util/Map;)V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 72
    return-object v1
.end method
