.class public Lcdq;
.super Lccy;
.source "PG"


# instance fields
.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/Runnable;

.field private e:Lccg;

.field private f:Z

.field private g:I

.field private h:I

.field private i:Lcds;


# direct methods
.method public constructor <init>(Lcci;)V
    .locals 2

    .prologue
    .line 102
    const/4 v0, 0x0

    const/high16 v1, 0x40200000    # 2.5f

    invoke-direct {p0, p1, v0, v1}, Lccy;-><init>(Lcci;ZF)V

    .line 83
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcdq;->c:Ljava/util/List;

    .line 84
    new-instance v0, Lcdr;

    invoke-direct {v0, p0}, Lcdr;-><init>(Lcdq;)V

    iput-object v0, p0, Lcdq;->d:Ljava/lang/Runnable;

    .line 103
    return-void
.end method

.method static synthetic a(Lcdq;)I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcdq;->h:I

    return v0
.end method

.method static synthetic b(Lcdq;)I
    .locals 1

    .prologue
    .line 21
    iget v0, p0, Lcdq;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcdq;->h:I

    return v0
.end method

.method private b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 180
    iput-boolean v0, p0, Lcdq;->f:Z

    .line 181
    iput v0, p0, Lcdq;->g:I

    .line 182
    const/4 v0, 0x0

    iput-object v0, p0, Lcdq;->e:Lccg;

    .line 183
    return-void
.end method

.method private b(Lccj;)V
    .locals 10

    .prologue
    const/4 v4, 0x2

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 189
    const/16 v0, 0x4b

    invoke-virtual {p1, v0}, Lccj;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 190
    iget-boolean v0, p1, Lccj;->a:Z

    if-eqz v0, :cond_0

    .line 191
    iget-object v0, p0, Lcdq;->e:Lccg;

    iget-object v0, v0, Lccg;->a:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setTranslationY(F)V

    .line 214
    :goto_0
    return-void

    .line 193
    :cond_0
    iget-object v0, p0, Lcdq;->e:Lccg;

    iget-object v0, v0, Lccg;->a:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_0

    .line 198
    :cond_1
    const/high16 v0, 0x43fa0000    # 500.0f

    invoke-direct {p0, p1}, Lcdq;->c(Lccj;)F

    move-result v1

    mul-float/2addr v0, v1

    float-to-long v2, v0

    .line 202
    iget-boolean v0, p1, Lccj;->a:Z

    if-eqz v0, :cond_2

    .line 203
    iget-object v0, p0, Lcdq;->e:Lccg;

    iget-object v0, v0, Lccg;->a:Landroid/view/View;

    new-array v1, v4, [Landroid/animation/PropertyValuesHolder;

    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v5, v7, [F

    aput v9, v5, v6

    .line 204
    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    aput-object v4, v1, v6

    sget-object v4, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v5, v7, [F

    aput v8, v5, v6

    .line 205
    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    aput-object v4, v1, v7

    .line 203
    invoke-static {v0, v1}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 212
    :goto_1
    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 213
    const/16 v1, 0x4b

    invoke-virtual {p1, v0, v1}, Lccj;->a(Landroid/animation/Animator;I)V

    goto :goto_0

    .line 207
    :cond_2
    iget-object v0, p0, Lcdq;->e:Lccg;

    iget-object v0, v0, Lccg;->a:Landroid/view/View;

    new-array v1, v4, [Landroid/animation/PropertyValuesHolder;

    sget-object v4, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v5, v7, [F

    aput v9, v5, v6

    .line 208
    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    aput-object v4, v1, v6

    sget-object v4, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v5, v7, [F

    aput v8, v5, v6

    .line 209
    invoke-static {v4, v5}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v4

    aput-object v4, v1, v7

    .line 207
    invoke-static {v0, v1}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    goto :goto_1
.end method

.method private c(Lccj;)F
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, Lcdq;->e:Lccg;

    iget-object v0, v0, Lccg;->a:Landroid/view/View;

    invoke-virtual {p1, v0}, Lccj;->c(Landroid/view/View;)I

    move-result v0

    .line 273
    iget v1, p0, Lcdq;->g:I

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    return v0
.end method

.method static synthetic c(Lcdq;)V
    .locals 6

    .prologue
    .line 21
    iget-object v0, p0, Lcdq;->i:Lcds;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcdq;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    new-array v3, v2, [J

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lcdq;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcdq;->i:Lcds;

    invoke-interface {v0, v3}, Lcds;->a([J)V

    iget-object v0, p0, Lcdq;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_1
    return-void
.end method

.method private d(Lccj;I)V
    .locals 11

    .prologue
    const/16 v10, 0x4b

    const/4 v5, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 224
    iget-object v0, p0, Lcdq;->e:Lccg;

    iget-object v0, v0, Lccg;->a:Landroid/view/View;

    invoke-virtual {p1, v0}, Lccj;->c(Landroid/view/View;)I

    move-result v0

    .line 227
    int-to-float v0, v0

    const/high16 v1, 0x3fc00000    # 1.5f

    mul-float/2addr v0, v1

    iget v1, p0, Lcdq;->g:I

    .line 229
    invoke-static {v1}, Ljava/lang/Integer;->signum(I)I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    .line 231
    iget v1, p0, Lcdq;->h:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcdq;->h:I

    .line 232
    iget-object v1, p0, Lcdq;->c:Ljava/util/List;

    iget-object v2, p0, Lcdq;->e:Lccg;

    iget-wide v2, v2, Lccg;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 233
    iget-object v1, p0, Lcdq;->i:Lcds;

    iget-object v2, p0, Lcdq;->e:Lccg;

    iget-wide v2, v2, Lccg;->b:J

    iget-object v2, p0, Lcdq;->e:Lccg;

    iget-object v2, v2, Lccg;->a:Landroid/view/View;

    invoke-interface {v1, v2}, Lcds;->a(Landroid/view/View;)V

    .line 235
    invoke-virtual {p1, v10}, Lccj;->a(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 236
    iget-boolean v1, p1, Lccj;->a:Z

    if-eqz v1, :cond_0

    .line 237
    iget-object v1, p0, Lcdq;->e:Lccg;

    iget-object v1, v1, Lccg;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 241
    :goto_0
    iget-object v0, p0, Lcdq;->d:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 266
    :goto_1
    return-void

    .line 239
    :cond_0
    iget-object v1, p0, Lcdq;->e:Lccg;

    iget-object v1, v1, Lccg;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_0

    .line 246
    :cond_1
    iget v1, p0, Lcdq;->g:I

    int-to-float v1, v1

    sub-float v1, v0, v1

    .line 249
    const/high16 v2, 0x447a0000    # 1000.0f

    int-to-float v3, p2

    div-float/2addr v1, v3

    const v3, 0x3dcccccd    # 0.1f

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-static {v1, v3, v4}, Lcfn;->a(FFF)F

    move-result v1

    mul-float/2addr v1, v2

    float-to-long v2, v1

    .line 254
    iget-boolean v1, p1, Lccj;->a:Z

    if-eqz v1, :cond_2

    .line 255
    iget-object v1, p0, Lcdq;->e:Lccg;

    iget-object v1, v1, Lccg;->a:Landroid/view/View;

    new-array v4, v5, [Landroid/animation/PropertyValuesHolder;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v8, [F

    aput v9, v6, v7

    .line 256
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v7

    sget-object v5, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v6, v8, [F

    aput v0, v6, v7

    .line 257
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    aput-object v0, v4, v8

    .line 255
    invoke-static {v1, v4}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 264
    :goto_2
    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 265
    iget-object v1, p0, Lcdq;->d:Ljava/lang/Runnable;

    invoke-virtual {p1, v0, v10, v1}, Lccj;->a(Landroid/animation/Animator;ILjava/lang/Runnable;)V

    goto :goto_1

    .line 259
    :cond_2
    iget-object v1, p0, Lcdq;->e:Lccg;

    iget-object v1, v1, Lccg;->a:Landroid/view/View;

    new-array v4, v5, [Landroid/animation/PropertyValuesHolder;

    sget-object v5, Landroid/view/View;->ALPHA:Landroid/util/Property;

    new-array v6, v8, [F

    aput v9, v6, v7

    .line 260
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v5

    aput-object v5, v4, v7

    sget-object v5, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v6, v8, [F

    aput v0, v6, v7

    .line 261
    invoke-static {v5, v6}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    aput-object v0, v4, v8

    .line 259
    invoke-static {v1, v4}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    goto :goto_2
.end method


# virtual methods
.method protected a()V
    .locals 0

    .prologue
    .line 153
    return-void
.end method

.method protected a(Lccj;IZ)V
    .locals 2

    .prologue
    .line 157
    iget-boolean v0, p0, Lcdq;->f:Z

    if-eqz v0, :cond_0

    if-nez p3, :cond_0

    invoke-direct {p0, p1}, Lcdq;->c(Lccj;)F

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 159
    :cond_0
    invoke-direct {p0, p1}, Lcdq;->b(Lccj;)V

    .line 163
    :goto_0
    invoke-direct {p0}, Lcdq;->b()V

    .line 164
    return-void

    .line 161
    :cond_1
    invoke-direct {p0, p1, p2}, Lcdq;->d(Lccj;I)V

    goto :goto_0
.end method

.method public a(Lcds;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcdq;->i:Lcds;

    .line 109
    return-void
.end method

.method protected b(Lccj;I)V
    .locals 2

    .prologue
    .line 168
    iget-boolean v0, p0, Lcdq;->f:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcdq;->g:I

    invoke-static {v0}, Ljava/lang/Integer;->signum(I)I

    move-result v0

    mul-int/2addr v0, p2

    int-to-float v0, v0

    const/high16 v1, 0x43480000    # 200.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 170
    :cond_0
    invoke-direct {p0, p1}, Lcdq;->b(Lccj;)V

    .line 174
    :goto_0
    invoke-direct {p0}, Lcdq;->b()V

    .line 175
    return-void

    .line 172
    :cond_1
    invoke-direct {p0, p1, p2}, Lcdq;->d(Lccj;I)V

    goto :goto_0
.end method

.method protected b(Lccj;II)Z
    .locals 4

    .prologue
    .line 115
    invoke-virtual {p0, p1, p2, p3}, Lcdq;->a(Lccj;II)I

    move-result v0

    .line 116
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 117
    const/4 v0, 0x0

    .line 122
    :goto_0
    return v0

    .line 120
    :cond_0
    iget-object v1, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    iput-object v0, p0, Lcdq;->e:Lccg;

    .line 121
    iget-object v0, p0, Lcdq;->i:Lcds;

    iget-object v1, p0, Lcdq;->e:Lccg;

    iget-wide v2, v1, Lccg;->b:J

    iget-object v1, p0, Lcdq;->e:Lccg;

    iget-object v1, v1, Lccg;->a:Landroid/view/View;

    invoke-interface {v0, v2, v3, v1}, Lcds;->a(JLandroid/view/View;)Z

    move-result v0

    iput-boolean v0, p0, Lcdq;->f:Z

    .line 122
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected c(Lccj;I)V
    .locals 4

    .prologue
    .line 127
    iget-object v0, p0, Lcdq;->e:Lccg;

    iget-object v0, v0, Lccg;->a:Landroid/view/View;

    invoke-virtual {p1, v0}, Lccj;->c(Landroid/view/View;)I

    move-result v0

    .line 129
    iget-boolean v1, p0, Lcdq;->f:Z

    if-eqz v1, :cond_0

    .line 130
    iget v1, p0, Lcdq;->g:I

    add-int/2addr v1, p2

    iput v1, p0, Lcdq;->g:I

    .line 138
    :goto_0
    iget v1, p0, Lcdq;->g:I

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    .line 139
    const/high16 v1, 0x3f800000    # 1.0f

    const v2, 0x3e99999a    # 0.3f

    const v3, 0x3f4ccccd    # 0.8f

    .line 140
    invoke-static {v2, v3, v0}, Lcfn;->c(FFF)F

    move-result v0

    sub-float v0, v1, v0

    .line 142
    iget-object v1, p0, Lcdq;->e:Lccg;

    iget-object v1, v1, Lccg;->a:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 144
    iget-boolean v0, p1, Lccj;->a:Z

    if-eqz v0, :cond_1

    .line 145
    iget-object v0, p0, Lcdq;->e:Lccg;

    iget-object v0, v0, Lccg;->a:Landroid/view/View;

    iget v1, p0, Lcdq;->g:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    .line 149
    :goto_1
    return-void

    .line 133
    :cond_0
    iget v1, p0, Lcdq;->g:I

    int-to-float v1, v1

    int-to-float v2, p2

    const v3, 0x3ecccccd    # 0.4f

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    float-to-int v1, v1

    iput v1, p0, Lcdq;->g:I

    .line 134
    int-to-float v1, v0

    const v2, 0x3e4ccccd    # 0.2f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 135
    iget v2, p0, Lcdq;->g:I

    neg-int v3, v1

    invoke-static {v2, v3, v1}, Lcfn;->a(III)I

    move-result v1

    iput v1, p0, Lcdq;->g:I

    goto :goto_0

    .line 147
    :cond_1
    iget-object v0, p0, Lcdq;->e:Lccg;

    iget-object v0, v0, Lccg;->a:Landroid/view/View;

    iget v1, p0, Lcdq;->g:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    goto :goto_1
.end method
