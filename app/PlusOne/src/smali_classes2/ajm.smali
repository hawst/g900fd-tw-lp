.class public final Lajm;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lajn;",
        "Ljava/lang/Float;",
        "Lajp;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lbit;

.field private final b:Lajl;

.field private final c:Lajo;

.field private final d:Lbqg;


# direct methods
.method public constructor <init>(Lbit;Lajl;Lajo;Lbqg;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 85
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 86
    const-string v0, "videoIdentifierGenerator"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbit;

    iput-object v0, p0, Lajm;->a:Lbit;

    .line 88
    const-string v0, "metricsStore"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajl;

    iput-object v0, p0, Lajm;->b:Lajl;

    .line 89
    const-string v0, "listener"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajo;

    iput-object v0, p0, Lajm;->c:Lajo;

    .line 90
    const-string v0, "uriMimeTypeRetriever"

    .line 91
    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqg;

    iput-object v0, p0, Lajm;->d:Lbqg;

    .line 92
    return-void
.end method


# virtual methods
.method public varargs a([Lajn;)Lajp;
    .locals 10

    .prologue
    .line 96
    new-instance v1, Lajp;

    invoke-direct {v1}, Lajp;-><init>()V

    .line 97
    new-instance v2, Lbks;

    invoke-direct {v2}, Lbks;-><init>()V

    .line 99
    array-length v3, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_4

    aget-object v4, p1, v0

    .line 100
    iget-object v5, p0, Lajm;->d:Lbqg;

    iget-object v6, v4, Lajn;->a:Landroid/net/Uri;

    invoke-interface {v5, v6}, Lbqg;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    .line 103
    iget-object v6, v4, Lajn;->c:Lbkr;

    if-nez v6, :cond_2

    .line 104
    const-string v6, "image/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 105
    iget-object v5, v1, Lajp;->b:Ljava/util/List;

    iget-object v4, v4, Lajn;->a:Landroid/net/Uri;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 99
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 106
    :cond_1
    const-string v6, "video/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 107
    iget-object v5, v1, Lajp;->a:Ljava/util/List;

    iget-object v6, v4, Lajn;->a:Landroid/net/Uri;

    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 108
    iget-object v5, v1, Lajp;->d:Ljava/util/Map;

    iget-object v6, v4, Lajn;->a:Landroid/net/Uri;

    iget-object v7, v4, Lajn;->b:Lbmu;

    .line 109
    invoke-virtual {v7}, Lbmu;->a()Lboo;

    move-result-object v7

    iget-wide v8, v7, Lboo;->f:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    .line 108
    invoke-interface {v5, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    iget-object v5, p0, Lajm;->a:Lbit;

    iget-object v6, v4, Lajn;->a:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Lbit;->a(Landroid/net/Uri;)[B

    move-result-object v5

    .line 113
    iget-object v6, v4, Lajn;->a:Landroid/net/Uri;

    iget-object v7, p0, Lajm;->b:Lajl;

    .line 114
    invoke-virtual {v2, v6, v7}, Lbks;->a(Landroid/net/Uri;Lajl;)Lbkr;

    move-result-object v6

    .line 115
    invoke-static {v6, v5}, Lajd;->a(Lbkr;[B)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 116
    invoke-static {v6}, Lajd;->a(Lbkr;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 117
    iget-object v5, v1, Lajp;->c:Ljava/util/List;

    iget-object v4, v4, Lajn;->a:Landroid/net/Uri;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 121
    :cond_2
    const-string v6, "image/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 122
    iget v4, v1, Lajp;->f:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v1, Lajp;->f:I

    goto :goto_1

    .line 123
    :cond_3
    const-string v6, "video/"

    invoke-virtual {v5, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 124
    iget-wide v6, v1, Lajp;->e:J

    iget-object v4, v4, Lajn;->c:Lbkr;

    .line 125
    invoke-interface {v4}, Lbkr;->c()Lbkn;

    move-result-object v4

    invoke-virtual {v4}, Lbkn;->c()J

    move-result-wide v4

    add-long/2addr v4, v6

    iput-wide v4, v1, Lajp;->e:J

    goto :goto_1

    .line 130
    :cond_4
    return-object v1
.end method

.method protected a(Lajp;)V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lajm;->c:Lajo;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lajm;->c:Lajo;

    invoke-interface {v0, p0, p1}, Lajo;->a(Lajm;Lajp;)V

    .line 138
    :cond_0
    return-void
.end method

.method public synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 26
    check-cast p1, [Lajn;

    invoke-virtual {p0, p1}, Lajm;->a([Lajn;)Lajp;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 26
    check-cast p1, Lajp;

    invoke-virtual {p0, p1}, Lajm;->a(Lajp;)V

    return-void
.end method
