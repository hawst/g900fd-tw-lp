.class public final Lixg;
.super Lt;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Lt;-><init>()V

    return-void
.end method

.method public static a(Lae;)V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lixg;

    invoke-direct {v0}, Lixg;-><init>()V

    .line 20
    const-string v1, "connection_error"

    invoke-virtual {v0, p0, v1}, Lixg;->a(Lae;Ljava/lang/String;)V

    .line 21
    return-void
.end method


# virtual methods
.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 25
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lixg;->n()Lz;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 26
    const v1, 0x7f0a01ab

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 27
    const v1, 0x7f0a01ac

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 28
    const v1, 0x7f0a01ad

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 29
    const/high16 v1, 0x1040000

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 30
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lixg;->p()Lae;

    move-result-object v0

    invoke-static {v0}, Liwy;->a(Lae;)Liwy;

    move-result-object v0

    invoke-virtual {v0}, Liwy;->ad()V

    .line 47
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    .prologue
    .line 35
    .line 36
    invoke-virtual {p0}, Lixg;->p()Lae;

    move-result-object v0

    invoke-static {v0}, Liwy;->a(Lae;)Liwy;

    move-result-object v0

    .line 37
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 38
    invoke-virtual {v0}, Liwy;->ac()V

    .line 42
    :goto_0
    return-void

    .line 40
    :cond_0
    invoke-virtual {v0}, Liwy;->ad()V

    goto :goto_0
.end method
