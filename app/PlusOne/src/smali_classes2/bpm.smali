.class public Lbpm;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lbpm;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbpm;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method

.method public static a(Lbok;)Lbmd;
    .locals 6

    .prologue
    .line 136
    new-instance v0, Lbmp;

    const-wide/16 v2, 0x0

    invoke-virtual {p0}, Lbok;->d()J

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lbmp;-><init>(JJ)V

    invoke-static {p0, v0}, Lbpm;->a(Lbok;Lbmp;)Lbmd;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lbok;J)Lbmd;
    .locals 7

    .prologue
    .line 140
    new-instance v0, Lbmd;

    sget-object v1, Lbmg;->c:Lbmg;

    invoke-virtual {p0}, Lbok;->a()Ljeg;

    move-result-object v2

    new-instance v3, Lbmp;

    const-wide/16 v4, 0x0

    invoke-direct {v3, v4, v5, p1, p2}, Lbmp;-><init>(JJ)V

    invoke-direct {v0, v1, v2, v3}, Lbmd;-><init>(Lbmg;Ljeg;Lbmp;)V

    return-object v0
.end method

.method public static a(Lbok;Lbmp;)Lbmd;
    .locals 6

    .prologue
    .line 122
    iget-wide v0, p1, Lbmp;->c:J

    invoke-virtual {p0}, Lbok;->d()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 123
    sget-object v0, Lbpm;->a:Ljava/lang/String;

    iget-wide v0, p1, Lbmp;->c:J

    .line 124
    invoke-virtual {p0}, Lbok;->d()J

    move-result-wide v2

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x50

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Clip end point "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " exceeds video end point "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 123
    :cond_0
    new-instance v0, Lbmd;

    sget-object v1, Lbmg;->a:Lbmg;

    invoke-virtual {p0}, Lbok;->a()Ljeg;

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Lbmd;-><init>(Lbmg;Ljeg;Lbmp;)V

    return-object v0
.end method

.method public static a(Lbty;)Lbmd;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 46
    iget-object v0, p0, Lbty;->f:Lbug;

    const-string v1, "clipProto.interval"

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 47
    iget-object v0, p0, Lbty;->e:Ljava/lang/String;

    const-string v1, "clipProto.uri"

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 48
    iget-object v0, p0, Lbty;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 49
    iget v0, p0, Lbty;->d:I

    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1e

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid clip type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    sget-object v0, Lbmg;->a:Lbmg;

    .line 50
    :goto_0
    new-instance v2, Lbmf;

    invoke-direct {v2}, Lbmf;-><init>()V

    iget v3, p0, Lbty;->b:I

    .line 51
    invoke-virtual {v2, v3}, Lbmf;->a(I)Lbmf;

    move-result-object v2

    iget v3, p0, Lbty;->c:I

    .line 52
    invoke-virtual {v2, v3}, Lbmf;->b(I)Lbmf;

    move-result-object v2

    .line 53
    invoke-virtual {v2, v0}, Lbmf;->a(Lbmg;)Lbmf;

    move-result-object v2

    .line 55
    invoke-static {v0}, Lbpm;->a(Lbmg;)Ljei;

    move-result-object v0

    .line 54
    invoke-static {v1, v0}, Ljeg;->a(Landroid/net/Uri;Ljei;)Ljeg;

    move-result-object v0

    invoke-virtual {v2, v0}, Lbmf;->a(Ljeg;)Lbmf;

    move-result-object v0

    iget-object v1, p0, Lbty;->f:Lbug;

    .line 56
    new-instance v2, Lbmp;

    iget-wide v4, v1, Lbug;->a:J

    iget-wide v6, v1, Lbug;->b:J

    invoke-direct {v2, v4, v5, v6, v7}, Lbmp;-><init>(JJ)V

    invoke-virtual {v0, v2}, Lbmf;->a(Lbmp;)Lbmf;

    move-result-object v0

    iget-boolean v1, p0, Lbty;->g:Z

    .line 57
    invoke-virtual {v0, v1}, Lbmf;->a(Z)Lbmf;

    move-result-object v0

    iget-boolean v1, p0, Lbty;->h:Z

    .line 58
    invoke-virtual {v0, v1}, Lbmf;->b(Z)Lbmf;

    move-result-object v0

    .line 59
    invoke-virtual {v0}, Lbmf;->a()Lbmd;

    move-result-object v0

    return-object v0

    .line 49
    :pswitch_1
    sget-object v0, Lbmg;->b:Lbmg;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lbmg;->c:Lbmg;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lbmg;->d:Lbmg;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lbmd;)Lbty;
    .locals 6

    .prologue
    .line 64
    new-instance v1, Lbty;

    invoke-direct {v1}, Lbty;-><init>()V

    .line 65
    iget v0, p0, Lbmd;->b:I

    iput v0, v1, Lbty;->b:I

    .line 66
    iget v0, p0, Lbmd;->c:I

    iput v0, v1, Lbty;->c:I

    .line 67
    iget-object v0, p0, Lbmd;->d:Lbmg;

    sget-object v2, Lbpn;->a:[I

    invoke-virtual {v0}, Lbmg;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    const/4 v0, 0x1

    :goto_0
    iput v0, v1, Lbty;->d:I

    .line 68
    iget-object v0, p0, Lbmd;->e:Ljeg;

    iget-object v0, v0, Ljeg;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lbty;->e:Ljava/lang/String;

    .line 69
    iget-object v0, p0, Lbmd;->f:Lbmp;

    new-instance v2, Lbug;

    invoke-direct {v2}, Lbug;-><init>()V

    iget-wide v4, v0, Lbmp;->b:J

    iput-wide v4, v2, Lbug;->a:J

    iget-wide v4, v0, Lbmp;->c:J

    iput-wide v4, v2, Lbug;->b:J

    iput-object v2, v1, Lbty;->f:Lbug;

    .line 70
    iget-boolean v0, p0, Lbmd;->g:Z

    iput-boolean v0, v1, Lbty;->g:Z

    .line 71
    iget-boolean v0, p0, Lbmd;->h:Z

    iput-boolean v0, v1, Lbty;->h:Z

    .line 72
    return-object v1

    .line 67
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lbok;Ljava/util/Collection;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbok;",
            "Ljava/util/Collection",
            "<",
            "Lbmp;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 92
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 93
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmp;

    .line 94
    invoke-static {p0, v0}, Lbpm;->a(Lbok;Lbmp;)Lbmd;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 96
    :cond_0
    return-object v1
.end method

.method public static a(Ljava/util/List;J)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;J)",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166
    const-string v0, "mergeThresholdUs"

    invoke-static {p1, p2, v0}, Lcec;->b(JLjava/lang/CharSequence;)J

    .line 167
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 169
    const/4 v0, 0x0

    .line 170
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v0

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 171
    if-eqz v0, :cond_0

    .line 172
    if-nez v1, :cond_1

    move-object v1, v0

    .line 179
    goto :goto_0

    .line 183
    :cond_1
    iget-object v2, v1, Lbmd;->d:Lbmg;

    sget-object v3, Lbmg;->a:Lbmg;

    if-ne v2, v3, :cond_2

    iget-object v2, v0, Lbmd;->d:Lbmg;

    sget-object v3, Lbmg;->a:Lbmg;

    if-ne v2, v3, :cond_2

    iget-object v2, v0, Lbmd;->e:Ljeg;

    iget-object v3, v1, Lbmd;->e:Ljeg;

    .line 184
    invoke-virtual {v2, v3}, Ljeg;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v0, Lbmd;->f:Lbmp;

    iget-wide v2, v2, Lbmp;->b:J

    iget-object v6, v1, Lbmd;->f:Lbmp;

    iget-wide v6, v6, Lbmp;->b:J

    cmp-long v2, v2, v6

    if-ltz v2, :cond_2

    .line 187
    iget-object v2, v0, Lbmd;->f:Lbmp;

    iget-wide v2, v2, Lbmp;->b:J

    iget-object v6, v1, Lbmd;->f:Lbmp;

    iget-wide v6, v6, Lbmp;->c:J

    sub-long/2addr v2, v6

    .line 192
    :goto_1
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-ltz v6, :cond_3

    cmp-long v6, v2, p1

    if-gez v6, :cond_3

    .line 193
    iget-object v1, v1, Lbmd;->f:Lbmp;

    iget-wide v6, v1, Lbmp;->b:J

    const-wide/16 v8, 0x2

    div-long v8, v2, v8

    add-long/2addr v6, v8

    .line 194
    iget-object v1, v0, Lbmd;->f:Lbmp;

    iget-wide v8, v1, Lbmp;->c:J

    const-wide/16 v10, 0x2

    div-long/2addr v2, v10

    sub-long v2, v8, v2

    .line 196
    invoke-virtual {v0}, Lbmd;->a()Lbmf;

    move-result-object v0

    new-instance v1, Lbmp;

    invoke-direct {v1, v6, v7, v2, v3}, Lbmp;-><init>(JJ)V

    .line 197
    invoke-virtual {v0, v1}, Lbmf;->a(Lbmp;)Lbmf;

    move-result-object v0

    .line 198
    invoke-virtual {v0}, Lbmf;->a()Lbmd;

    move-result-object v0

    move-object v1, v0

    .line 199
    goto :goto_0

    .line 189
    :cond_2
    const-wide v2, 0x7fffffffffffffffL

    goto :goto_1

    .line 199
    :cond_3
    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-gez v2, :cond_4

    .line 204
    iget-object v2, v0, Lbmd;->f:Lbmp;

    iget-wide v2, v2, Lbmp;->c:J

    iget-object v6, v1, Lbmd;->f:Lbmp;

    iget-wide v6, v6, Lbmp;->c:J

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 207
    invoke-virtual {v0}, Lbmd;->a()Lbmf;

    move-result-object v0

    new-instance v6, Lbmp;

    iget-object v1, v1, Lbmd;->f:Lbmp;

    iget-wide v8, v1, Lbmp;->b:J

    invoke-direct {v6, v8, v9, v2, v3}, Lbmp;-><init>(JJ)V

    .line 208
    invoke-virtual {v0, v6}, Lbmf;->a(Lbmp;)Lbmf;

    move-result-object v0

    .line 209
    invoke-virtual {v0}, Lbmf;->a()Lbmd;

    move-result-object v0

    move-object v1, v0

    .line 210
    goto/16 :goto_0

    .line 212
    :cond_4
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    .line 215
    goto/16 :goto_0

    .line 216
    :cond_5
    if-eqz v1, :cond_6

    .line 217
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    :cond_6
    return-object v4
.end method

.method public static a(Ljava/util/List;Lama;)Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Lama;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 314
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 315
    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 316
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    invoke-virtual {v0}, Lbmd;->a()Lbmf;

    move-result-object v0

    .line 317
    invoke-interface {p1}, Lama;->a()I

    move-result v4

    invoke-virtual {v0, v4}, Lbmf;->a(I)Lbmf;

    move-result-object v0

    .line 318
    invoke-virtual {v0}, Lbmf;->a()Lbmd;

    move-result-object v0

    .line 316
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 315
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 320
    :cond_0
    return-object v2
.end method

.method public static a(Ljava/util/List;Lbmg;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Lbmg;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 149
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 150
    iget-object v3, v0, Lbmd;->d:Lbmg;

    if-ne v3, p1, :cond_0

    .line 151
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 154
    :cond_1
    return-object v1
.end method

.method public static a(Ljava/util/List;Ljava/util/Map;Lbzg;)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;",
            "Lbzg;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 233
    const-string v2, "limits"

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 234
    const-wide/32 v2, 0x16e360

    move-object/from16 v0, p0

    invoke-static {v0, v2, v3}, Lbpm;->b(Ljava/util/List;J)V

    .line 235
    new-instance v12, Ljava/util/ArrayList;

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v12, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 236
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbmd;

    .line 237
    iget-object v3, v2, Lbmd;->e:Ljeg;

    .line 238
    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbml;

    .line 237
    iget-object v4, v2, Lbmd;->d:Lbmg;

    sget-object v5, Lbmg;->a:Lbmg;

    if-ne v4, v5, :cond_0

    if-nez v3, :cond_1

    :cond_0
    :goto_1
    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    check-cast v3, Lbon;

    invoke-virtual {v3}, Lbon;->g()Lboo;

    move-result-object v4

    if-eqz v4, :cond_0

    iget v5, v4, Lbmu;->c:I

    iget v4, v4, Lbmu;->d:I

    mul-int/2addr v4, v5

    invoke-interface/range {p2 .. p2}, Lbzg;->m()I

    move-result v5

    if-le v4, v5, :cond_0

    iget-object v4, v2, Lbmd;->f:Lbmp;

    iget-wide v8, v4, Lbmp;->b:J

    iget-object v4, v2, Lbmd;->f:Lbmp;

    iget-wide v6, v4, Lbmp;->c:J

    invoke-virtual {v3}, Lbon;->h()Lbms;

    move-result-object v4

    invoke-virtual {v4, v8, v9}, Lbms;->a(J)Landroid/util/Pair;

    move-result-object v4

    iget-object v5, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-nez v5, :cond_3

    iget-object v5, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v5, :cond_2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x20

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "There are no keyframes for clip "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v2

    throw v2

    :cond_2
    sget-object v5, Lbpm;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x3b

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v11, "First key-frame for "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v10, " before "

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " was unset."

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lbon;->h()Lbms;

    move-result-object v5

    iget-object v4, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v5, v8, v9}, Lbms;->a(J)Landroid/util/Pair;

    move-result-object v5

    iget-object v4, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v3}, Lbon;->i()J

    move-result-wide v10

    const-wide/32 v14, 0x16e360

    add-long/2addr v14, v8

    invoke-static {v14, v15, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v6

    invoke-static {v10, v11, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    move-object v4, v5

    :cond_3
    iget-object v3, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v3, v10, v8

    if-lez v3, :cond_4

    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "invalid earlier keyframe from getNearestKeyFrameTimestampsUs: %s %s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_4
    iget-object v3, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v3, :cond_7

    const-wide v4, 0x7fffffffffffffffL

    :goto_2
    sub-long v14, v8, v10

    sub-long v8, v4, v8

    cmp-long v3, v14, v8

    if-ltz v3, :cond_5

    sub-long v8, v6, v4

    const-wide/32 v14, 0x16e360

    cmp-long v3, v8, v14

    if-gez v3, :cond_6

    :cond_5
    move-wide v4, v10

    :cond_6
    invoke-virtual {v2}, Lbmd;->a()Lbmf;

    move-result-object v2

    new-instance v3, Lbmp;

    invoke-direct {v3, v4, v5, v6, v7}, Lbmp;-><init>(JJ)V

    invoke-virtual {v2, v3}, Lbmf;->a(Lbmp;)Lbmf;

    move-result-object v2

    invoke-virtual {v2}, Lbmf;->a()Lbmd;

    move-result-object v2

    goto/16 :goto_1

    :cond_7
    iget-object v3, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    goto :goto_2

    .line 240
    :cond_8
    return-object v12
.end method

.method public static a([Lbty;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lbty;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 77
    new-instance v1, Ljava/util/ArrayList;

    array-length v0, p0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 78
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    .line 79
    invoke-static {v3}, Lbpm;->a(Lbty;)Lbmd;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 78
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 81
    :cond_0
    return-object v1
.end method

.method public static a(Lbmg;)Ljei;
    .locals 2

    .prologue
    .line 372
    sget-object v0, Lbpn;->a:[I

    invoke-virtual {p0}, Lbmg;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 381
    const-string v0, "Unsupported clip type"

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 375
    :pswitch_0
    sget-object v0, Ljei;->b:Ljei;

    .line 379
    :goto_0
    return-object v0

    .line 377
    :pswitch_1
    sget-object v0, Ljei;->a:Ljei;

    goto :goto_0

    .line 379
    :pswitch_2
    sget-object v0, Ljei;->c:Ljei;

    goto :goto_0

    .line 372
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static b(Ljava/util/List;J)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 358
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 359
    invoke-virtual {v0}, Lbmd;->b()J

    move-result-wide v2

    .line 360
    cmp-long v0, v2, p1

    if-gez v0, :cond_0

    .line 361
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x50

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "clip duration "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " was less than minimum "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " us"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 365
    :cond_1
    return-void
.end method
