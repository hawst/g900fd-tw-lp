.class public final Lbpd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbph;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lbpd;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I

.field private final b:Lbmd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    new-instance v0, Lbpe;

    invoke-direct {v0}, Lbpe;-><init>()V

    sput-object v0, Lbpd;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILbmd;)V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p1, p0, Lbpd;->a:I

    .line 26
    const-string v0, "clip"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iput-object v0, p0, Lbpd;->b:Lbmd;

    .line 27
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lbpd;->a:I

    .line 31
    new-instance v0, Lbmd;

    invoke-direct {v0, p1}, Lbmd;-><init>(Landroid/os/Parcel;)V

    iput-object v0, p0, Lbpd;->b:Lbmd;

    .line 32
    return-void
.end method


# virtual methods
.method public a()Lbut;
    .locals 3

    .prologue
    .line 41
    new-instance v0, Lbut;

    invoke-direct {v0}, Lbut;-><init>()V

    .line 42
    const/4 v1, 0x3

    iput v1, v0, Lbut;->b:I

    .line 43
    new-instance v1, Lbur;

    invoke-direct {v1}, Lbur;-><init>()V

    .line 44
    iget v2, p0, Lbpd;->a:I

    iput v2, v1, Lbur;->a:I

    .line 45
    iget-object v2, p0, Lbpd;->b:Lbmd;

    invoke-static {v2}, Lbpm;->a(Lbmd;)Lbty;

    move-result-object v2

    iput-object v2, v1, Lbur;->b:Lbty;

    .line 46
    iput-object v1, v0, Lbut;->e:Lbur;

    .line 47
    return-object v0
.end method

.method public a(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 36
    iget v0, p0, Lbpd;->a:I

    invoke-interface {p1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 37
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lbpd;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 58
    iget-object v0, p0, Lbpd;->b:Lbmd;

    invoke-virtual {v0, p1, p2}, Lbmd;->writeToParcel(Landroid/os/Parcel;I)V

    .line 59
    return-void
.end method
