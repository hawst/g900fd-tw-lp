.class public final Lacw;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Z

.field private static b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljavax/microedition/khronos/egl/EGLSurface;",
            ">;"
        }
    .end annotation
.end field

.field private static c:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Object;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static d:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Lacw;",
            ">;"
        }
    .end annotation
.end field

.field private static f:Ljavax/microedition/khronos/egl/EGLConfig;

.field private static g:Ljavax/microedition/khronos/egl/EGLDisplay;

.field private static o:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljavax/microedition/khronos/egl/EGLContext;",
            "Lacm;",
            ">;"
        }
    .end annotation
.end field

.field private static p:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljavax/microedition/khronos/egl/EGLContext;",
            "Ljavax/microedition/khronos/egl/EGLSurface;",
            ">;"
        }
    .end annotation
.end field

.field private static q:I

.field private static r:I

.field private static s:I

.field private static t:I


# instance fields
.field private e:Ljava/lang/Object;

.field private h:Ljavax/microedition/khronos/egl/EGL10;

.field private i:Ljavax/microedition/khronos/egl/EGLDisplay;

.field private j:Ljavax/microedition/khronos/egl/EGLContext;

.field private k:Ljavax/microedition/khronos/egl/EGLSurface;

.field private l:I

.field private m:Z

.field private n:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    .line 44
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lacw;->a:Z

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lacw;->b:Ljava/util/HashMap;

    .line 50
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lacw;->c:Ljava/util/HashMap;

    .line 53
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Lacw;->d:Ljava/lang/ThreadLocal;

    .line 59
    const/4 v0, 0x0

    sput-object v0, Lacw;->f:Ljavax/microedition/khronos/egl/EGLConfig;

    .line 73
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lacw;->o:Ljava/util/HashMap;

    .line 76
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lacw;->p:Ljava/util/HashMap;

    .line 79
    sput v2, Lacw;->q:I

    .line 80
    sput v2, Lacw;->r:I

    .line 81
    sput v2, Lacw;->s:I

    .line 82
    sput v2, Lacw;->t:I

    .line 83
    return-void

    .line 44
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;Ljavax/microedition/khronos/egl/EGLSurface;IZZ)V
    .locals 1

    .prologue
    .line 349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lacw;->e:Ljava/lang/Object;

    .line 350
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    iput-object v0, p0, Lacw;->h:Ljavax/microedition/khronos/egl/EGL10;

    .line 351
    iput-object p1, p0, Lacw;->i:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 352
    iput-object p2, p0, Lacw;->j:Ljavax/microedition/khronos/egl/EGLContext;

    .line 353
    iput-object p3, p0, Lacw;->k:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 354
    iput p4, p0, Lacw;->l:I

    .line 355
    iput-boolean p5, p0, Lacw;->m:Z

    .line 356
    iput-boolean p6, p0, Lacw;->n:Z

    .line 357
    return-void
.end method

.method public static a()Lacw;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lacw;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacw;

    return-object v0
.end method

.method public static a(II)Lacw;
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v4, 0x0

    const/4 v5, 0x1

    .line 87
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    .line 88
    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v1

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-ne v1, v2, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "EGL Error: Bad display: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lacw;->a(Ljavax/microedition/khronos/egl/EGL10;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-array v2, v8, [I

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "EGL Error: eglInitialize failed "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lacw;->a(Ljavax/microedition/khronos/egl/EGL10;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 89
    :cond_1
    invoke-static {v0, v1}, Lacw;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v2

    .line 90
    new-array v3, v9, [I

    fill-array-data v3, :array_0

    sget-object v6, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v6, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljavax/microedition/khronos/egl/EGLContext;[I)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v2

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    if-ne v2, v3, :cond_2

    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "EGL Error: Bad context: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lacw;->a(Ljavax/microedition/khronos/egl/EGL10;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 91
    :cond_2
    invoke-static {v0, v1}, Lacw;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v3

    const/4 v6, 0x5

    new-array v6, v6, [I

    const/16 v7, 0x3057

    aput v7, v6, v4

    aput p0, v6, v5

    const/16 v7, 0x3056

    aput v7, v6, v8

    aput p1, v6, v9

    const/4 v7, 0x4

    const/16 v8, 0x3038

    aput v8, v6, v7

    invoke-interface {v0, v1, v3, v6}, Ljavax/microedition/khronos/egl/EGL10;->eglCreatePbufferSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;[I)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v3

    .line 92
    new-instance v0, Lacw;

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lacw;-><init>(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;Ljavax/microedition/khronos/egl/EGLSurface;IZZ)V

    .line 93
    invoke-direct {v0, v3}, Lacw;->a(Ljava/lang/Object;)V

    .line 94
    return-object v0

    .line 90
    :array_0
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data
.end method

.method private static a(Ljavax/microedition/khronos/egl/EGL10;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 428
    invoke-interface {p0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    .line 429
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    .line 430
    invoke-static {v0}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v0

    .line 432
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "EGL Error 0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 320
    sget-object v0, Lacw;->f:Ljavax/microedition/khronos/egl/EGLConfig;

    if-eqz v0, :cond_0

    sget-object v0, Lacw;->g:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 321
    :cond_0
    new-array v5, v4, [I

    .line 322
    new-array v3, v4, [Ljavax/microedition/khronos/egl/EGLConfig;

    .line 323
    const/16 v0, 0xf

    new-array v2, v0, [I

    const/16 v0, 0x3040

    aput v0, v2, v6

    aput v7, v2, v4

    const/4 v0, 0x2

    const/16 v1, 0x3024

    aput v1, v2, v0

    const/4 v0, 0x3

    sget v1, Lacw;->q:I

    aput v1, v2, v0

    const/16 v0, 0x3023

    aput v0, v2, v7

    const/4 v0, 0x5

    sget v1, Lacw;->r:I

    aput v1, v2, v0

    const/4 v0, 0x6

    const/16 v1, 0x3022

    aput v1, v2, v0

    const/4 v0, 0x7

    sget v1, Lacw;->s:I

    aput v1, v2, v0

    const/16 v0, 0x8

    const/16 v1, 0x3021

    aput v1, v2, v0

    const/16 v0, 0x9

    sget v1, Lacw;->t:I

    aput v1, v2, v0

    const/16 v0, 0xa

    const/16 v1, 0x3025

    aput v1, v2, v0

    const/16 v0, 0xb

    aput v6, v2, v0

    const/16 v0, 0xc

    const/16 v1, 0x3026

    aput v1, v2, v0

    const/16 v0, 0xd

    aput v6, v2, v0

    const/16 v0, 0xe

    const/16 v1, 0x3038

    aput v1, v2, v0

    move-object v0, p0

    move-object v1, p1

    .line 324
    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 325
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "EGL Error: eglChooseConfig failed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lacw;->a(Ljavax/microedition/khronos/egl/EGL10;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 327
    :cond_1
    aget v0, v5, v6

    if-lez v0, :cond_2

    .line 328
    aget-object v0, v3, v6

    sput-object v0, Lacw;->f:Ljavax/microedition/khronos/egl/EGLConfig;

    .line 329
    sput-object p1, Lacw;->g:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 332
    :cond_2
    sget-object v0, Lacw;->f:Ljavax/microedition/khronos/egl/EGLConfig;

    return-object v0
.end method

.method private a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 299
    sget-object v0, Lacw;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 300
    if-eqz v0, :cond_0

    .line 301
    sget-object v1, Lacw;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 305
    :goto_0
    return-void

    .line 303
    :cond_0
    sget-object v0, Lacw;->c:Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static a(Ljavax/microedition/khronos/egl/EGL10;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 420
    invoke-interface {p0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    .line 421
    const/16 v1, 0x3000

    if-eq v0, v1, :cond_0

    .line 422
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error executing "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "! EGL error = 0x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 425
    :cond_0
    return-void
.end method

.method private static a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLSurface;)V
    .locals 3

    .prologue
    .line 414
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-ne p1, v0, :cond_0

    .line 415
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "EGL Error: Bad surface: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lacw;->a(Ljavax/microedition/khronos/egl/EGL10;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 417
    :cond_0
    return-void
.end method

.method public static e()V
    .locals 5

    .prologue
    .line 224
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    .line 225
    invoke-interface {v0}, Ljavax/microedition/khronos/egl/EGL10;->eglGetCurrentDisplay()Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v1

    sget-object v2, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 229
    sget-object v1, Lacw;->d:Ljava/lang/ThreadLocal;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 230
    const-string v1, "eglMakeCurrent"

    invoke-static {v0, v1}, Lacw;->a(Ljavax/microedition/khronos/egl/EGL10;Ljava/lang/String;)V

    .line 231
    return-void
.end method

.method public static h()Ljavax/microedition/khronos/egl/EGLContext;
    .locals 1

    .prologue
    .line 242
    invoke-static {}, Lacw;->a()Lacw;

    move-result-object v0

    .line 243
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lacw;->g()Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    goto :goto_0
.end method

.method private k()Ljavax/microedition/khronos/egl/EGLSurface;
    .locals 2

    .prologue
    .line 360
    sget-boolean v0, Lacw;->a:Z

    if-eqz v0, :cond_1

    .line 361
    iget-object v0, p0, Lacw;->k:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 364
    :cond_0
    :goto_0
    return-object v0

    .line 363
    :cond_1
    sget-object v0, Lacw;->p:Ljava/util/HashMap;

    iget-object v1, p0, Lacw;->j:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGLSurface;

    .line 364
    if-nez v0, :cond_0

    iget-object v0, p0, Lacw;->k:Ljavax/microedition/khronos/egl/EGLSurface;

    goto :goto_0
.end method


# virtual methods
.method public a(Lada;)Lacw;
    .locals 7

    .prologue
    const v3, 0x8d40

    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 106
    new-array v0, v1, [I

    invoke-static {v1, v0, v5}, Landroid/opengl/GLES20;->glGenFramebuffers(I[II)V

    const-string v1, "glGenFramebuffers"

    invoke-static {v1}, Labi;->a(Ljava/lang/String;)V

    aget v4, v0, v5

    .line 107
    invoke-static {v3, v4}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 108
    const-string v0, "glBindFramebuffer"

    invoke-static {v0}, Labi;->a(Ljava/lang/String;)V

    .line 109
    const v0, 0x8ce0

    invoke-virtual {p1}, Lada;->d()I

    move-result v1

    invoke-virtual {p1}, Lada;->c()I

    move-result v2

    invoke-static {v3, v0, v1, v2, v5}, Landroid/opengl/GLES20;->glFramebufferTexture2D(IIIII)V

    .line 114
    const-string v0, "glFramebufferTexture2D"

    invoke-static {v0}, Labi;->a(Ljava/lang/String;)V

    .line 115
    new-instance v0, Lacw;

    iget-object v1, p0, Lacw;->i:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, Lacw;->j:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-direct {p0}, Lacw;->k()Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v3

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lacw;-><init>(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;Ljavax/microedition/khronos/egl/EGLSurface;IZZ)V

    return-object v0
.end method

.method public a(Landroid/graphics/SurfaceTexture;)Lacw;
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 138
    iget-object v0, p0, Lacw;->h:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lacw;->i:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-static {v0, v1}, Lacw;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v1

    .line 139
    sget-object v2, Lacw;->b:Ljava/util/HashMap;

    monitor-enter v2

    .line 141
    :try_start_0
    sget-object v0, Lacw;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGLSurface;

    .line 142
    if-nez v0, :cond_0

    .line 143
    iget-object v0, p0, Lacw;->h:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, p0, Lacw;->i:Ljavax/microedition/khronos/egl/EGLDisplay;

    const/4 v5, 0x0

    invoke-interface {v0, v3, v1, p1, v5}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateWindowSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v3

    .line 144
    sget-object v0, Lacw;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 146
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    iget-object v0, p0, Lacw;->h:Ljavax/microedition/khronos/egl/EGL10;

    const-string v1, "eglCreateWindowSurface"

    invoke-static {v0, v1}, Lacw;->a(Ljavax/microedition/khronos/egl/EGL10;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lacw;->h:Ljavax/microedition/khronos/egl/EGL10;

    invoke-static {v0, v3}, Lacw;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLSurface;)V

    .line 149
    new-instance v0, Lacw;

    iget-object v1, p0, Lacw;->i:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, Lacw;->j:Ljavax/microedition/khronos/egl/EGLContext;

    const/4 v6, 0x1

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lacw;-><init>(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;Ljavax/microedition/khronos/egl/EGLSurface;IZZ)V

    .line 150
    iput-object p1, v0, Lacw;->e:Ljava/lang/Object;

    .line 151
    invoke-direct {v0, v3}, Lacw;->a(Ljava/lang/Object;)V

    .line 152
    return-object v0

    .line 146
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object v3, v0

    goto :goto_0
.end method

.method public a(Landroid/view/SurfaceHolder;)Lacw;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 119
    iget-object v0, p0, Lacw;->h:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lacw;->i:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-static {v0, v1}, Lacw;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;)Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v1

    .line 120
    sget-object v2, Lacw;->b:Ljava/util/HashMap;

    monitor-enter v2

    .line 122
    :try_start_0
    sget-object v0, Lacw;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGLSurface;

    .line 123
    if-nez v0, :cond_0

    .line 124
    iget-object v0, p0, Lacw;->h:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, p0, Lacw;->i:Ljavax/microedition/khronos/egl/EGLDisplay;

    const/4 v5, 0x0

    invoke-interface {v0, v3, v1, p1, v5}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateWindowSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v3

    .line 125
    sget-object v0, Lacw;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    iget-object v0, p0, Lacw;->h:Ljavax/microedition/khronos/egl/EGL10;

    const-string v1, "eglCreateWindowSurface"

    invoke-static {v0, v1}, Lacw;->a(Ljavax/microedition/khronos/egl/EGL10;Ljava/lang/String;)V

    .line 129
    iget-object v0, p0, Lacw;->h:Ljavax/microedition/khronos/egl/EGL10;

    invoke-static {v0, v3}, Lacw;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLSurface;)V

    .line 130
    new-instance v0, Lacw;

    iget-object v1, p0, Lacw;->i:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, Lacw;->j:Ljavax/microedition/khronos/egl/EGLContext;

    const/4 v6, 0x1

    move v5, v4

    invoke-direct/range {v0 .. v6}, Lacw;-><init>(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;Ljavax/microedition/khronos/egl/EGLSurface;IZZ)V

    .line 131
    invoke-direct {v0, v3}, Lacw;->a(Ljava/lang/Object;)V

    .line 132
    iput-object p1, v0, Lacw;->e:Ljava/lang/Object;

    .line 133
    return-object v0

    .line 127
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    move-object v3, v0

    goto :goto_0
.end method

.method public a(Ljava/nio/ByteBuffer;II)V
    .locals 0

    .prologue
    .line 266
    invoke-static {p0, p1, p2, p3}, Labi;->a(Lacw;Ljava/nio/ByteBuffer;II)V

    .line 267
    return-void
.end method

.method public b(II)Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 270
    mul-int v0, p1, p2

    shl-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 271
    invoke-static {p0, v0, p1, p2}, Labi;->a(Lacw;Ljava/nio/ByteBuffer;II)V

    .line 272
    return-object v0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 189
    sget-boolean v0, Lacw;->a:Z

    if-nez v0, :cond_1

    .line 194
    sget-object v0, Lacw;->p:Ljava/util/HashMap;

    iget-object v1, p0, Lacw;->j:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGLSurface;

    .line 195
    if-eqz v0, :cond_0

    iget-object v1, p0, Lacw;->k:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 196
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "This device supports only a single display surface!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 198
    :cond_0
    sget-object v0, Lacw;->p:Ljava/util/HashMap;

    iget-object v1, p0, Lacw;->j:Ljavax/microedition/khronos/egl/EGLContext;

    iget-object v2, p0, Lacw;->k:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    :cond_1
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 204
    sget-boolean v0, Lacw;->a:Z

    if-nez v0, :cond_0

    .line 205
    sget-object v0, Lacw;->p:Ljava/util/HashMap;

    iget-object v1, p0, Lacw;->j:Ljavax/microedition/khronos/egl/EGLContext;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    :cond_0
    return-void
.end method

.method public d()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 210
    sget-object v0, Lacw;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacw;

    .line 213
    if-eq v0, p0, :cond_0

    .line 214
    iget-object v0, p0, Lacw;->h:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lacw;->i:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-direct {p0}, Lacw;->k()Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v2

    invoke-direct {p0}, Lacw;->k()Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v3

    iget-object v4, p0, Lacw;->j:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 215
    sget-object v0, Lacw;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0, p0}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 217
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x8ca6

    invoke-static {v1, v0, v5}, Landroid/opengl/GLES20;->glGetIntegerv(I[II)V

    aget v0, v0, v5

    iget v1, p0, Lacw;->l:I

    if-eq v0, v1, :cond_1

    .line 218
    const v0, 0x8d40

    iget v1, p0, Lacw;->l:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 219
    const-string v0, "glBindFramebuffer"

    invoke-static {v0}, Labi;->a(Ljava/lang/String;)V

    .line 221
    :cond_1
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 234
    iget-object v0, p0, Lacw;->h:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lacw;->i:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-direct {p0}, Lacw;->k()Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglSwapBuffers(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    .line 235
    return-void
.end method

.method public g()Ljavax/microedition/khronos/egl/EGLContext;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lacw;->j:Ljavax/microedition/khronos/egl/EGLContext;

    return-object v0
.end method

.method public i()V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 247
    iget-boolean v0, p0, Lacw;->m:Z

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lacw;->h:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v3, p0, Lacw;->i:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v4, p0, Lacw;->j:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroyContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 249
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    iput-object v0, p0, Lacw;->j:Ljavax/microedition/khronos/egl/EGLContext;

    .line 251
    :cond_0
    iget-boolean v0, p0, Lacw;->n:Z

    if-eqz v0, :cond_2

    .line 252
    sget-object v3, Lacw;->b:Ljava/util/HashMap;

    monitor-enter v3

    .line 253
    :try_start_0
    iget-object v4, p0, Lacw;->k:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v0, Lacw;->c:Ljava/util/HashMap;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-lez v5, :cond_5

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v5, Lacw;->c:Ljava/util/HashMap;

    invoke-virtual {v5, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 254
    iget-object v0, p0, Lacw;->h:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v4, p0, Lacw;->i:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v5, p0, Lacw;->k:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v0, v4, v5}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    .line 255
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    iput-object v0, p0, Lacw;->k:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 256
    sget-object v0, Lacw;->b:Ljava/util/HashMap;

    iget-object v4, p0, Lacw;->e:Ljava/lang/Object;

    invoke-virtual {v0, v4}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 260
    :cond_2
    iget v0, p0, Lacw;->l:I

    if-eqz v0, :cond_3

    .line 261
    iget v0, p0, Lacw;->l:I

    new-array v3, v1, [I

    aput v0, v3, v2

    const-string v0, "glDeleteFramebuffers"

    invoke-static {v0}, Labi;->b(Ljava/lang/String;)V

    invoke-static {v1, v3, v2}, Landroid/opengl/GLES20;->glDeleteFramebuffers(I[II)V

    const-string v0, "glDeleteFramebuffers"

    invoke-static {v0}, Labi;->a(Ljava/lang/String;)V

    .line 263
    :cond_3
    return-void

    :cond_4
    move v0, v2

    .line 253
    goto :goto_0

    :cond_5
    :try_start_1
    const-string v0, "RenderTarget"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Removing reference of already released: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    goto :goto_0

    .line 258
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public j()Lacm;
    .locals 3

    .prologue
    .line 281
    sget-object v0, Lacw;->o:Ljava/util/HashMap;

    iget-object v1, p0, Lacw;->j:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacm;

    .line 282
    if-nez v0, :cond_0

    .line 283
    invoke-static {}, Lacm;->a()Lacm;

    move-result-object v0

    .line 284
    sget-object v1, Lacw;->o:Ljava/util/HashMap;

    iget-object v2, p0, Lacw;->j:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 286
    :cond_0
    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 291
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "RenderTarget("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lacw;->i:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lacw;->j:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lacw;->k:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lacw;->l:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
