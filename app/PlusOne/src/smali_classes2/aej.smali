.class public final Laej;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# instance fields
.field private synthetic a:Landroidx/media/filterpacks/video/MediaPlayerSource;


# direct methods
.method public constructor <init>(Landroidx/media/filterpacks/video/MediaPlayerSource;)V
    .locals 0

    .prologue
    .line 349
    iput-object p1, p0, Laej;->a:Landroidx/media/filterpacks/video/MediaPlayerSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 2

    .prologue
    .line 352
    iget-object v0, p0, Laej;->a:Landroidx/media/filterpacks/video/MediaPlayerSource;

    const-string v1, "MediaPlayer is prepared"

    invoke-static {v0, v1}, Landroidx/media/filterpacks/video/MediaPlayerSource;->a(Landroidx/media/filterpacks/video/MediaPlayerSource;Ljava/lang/String;)V

    .line 353
    iget-object v1, p0, Laej;->a:Landroidx/media/filterpacks/video/MediaPlayerSource;

    monitor-enter v1

    .line 354
    :try_start_0
    iget-object v0, p0, Laej;->a:Landroidx/media/filterpacks/video/MediaPlayerSource;

    invoke-static {v0}, Landroidx/media/filterpacks/video/MediaPlayerSource;->a(Landroidx/media/filterpacks/video/MediaPlayerSource;)Landroid/media/MediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 355
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
