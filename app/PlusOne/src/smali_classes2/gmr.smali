.class final Lgmr;
.super Ljava/lang/Object;

# interfaces
.implements Lgsc;


# instance fields
.field private final a:Lcom/google/android/gms/common/api/Status;

.field private final b:Lcom/google/android/gms/location/reporting/ReportingState;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/location/reporting/ReportingState;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgmr;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Lgih;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iput-object p2, p0, Lgmr;->b:Lcom/google/android/gms/location/reporting/ReportingState;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, Lgmr;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lgmr;->b:Lcom/google/android/gms/location/reporting/ReportingState;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/ReportingState;->a()I

    move-result v0

    return v0
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lgmr;->b:Lcom/google/android/gms/location/reporting/ReportingState;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/ReportingState;->b()I

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lgmr;->b:Lcom/google/android/gms/location/reporting/ReportingState;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/ReportingState;->c()Z

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Lgmr;->b:Lcom/google/android/gms/location/reporting/ReportingState;

    invoke-virtual {v0}, Lcom/google/android/gms/location/reporting/ReportingState;->e()Z

    move-result v0

    return v0
.end method
