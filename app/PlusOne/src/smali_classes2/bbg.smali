.class public final enum Lbbg;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbbg;",
        ">;"
    }
.end annotation


# static fields
.field private static enum b:Lbbg;

.field private static enum c:Lbbg;

.field private static enum d:Lbbg;

.field private static enum e:Lbbg;

.field private static enum f:Lbbg;

.field private static enum g:Lbbg;

.field private static enum h:Lbbg;

.field private static enum i:Lbbg;

.field private static enum j:Lbbg;

.field private static enum k:Lbbg;

.field private static enum l:Lbbg;

.field private static enum m:Lbbg;

.field private static enum n:Lbbg;

.field private static enum o:Lbbg;

.field private static enum p:Lbbg;

.field private static enum q:Lbbg;

.field private static final synthetic t:[Lbbg;


# instance fields
.field public final a:Ljava/lang/String;

.field private r:Lbkz;

.field private s:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 34
    new-instance v0, Lbbg;

    const-string v1, "METRIC_FACE_SCORE"

    const/4 v2, 0x0

    const-string v3, "face_score"

    sget-object v4, Lbkz;->b:Lbkz;

    const/4 v5, 0x4

    invoke-direct/range {v0 .. v5}, Lbbg;-><init>(Ljava/lang/String;ILjava/lang/String;Lbkz;I)V

    sput-object v0, Lbbg;->b:Lbbg;

    .line 37
    new-instance v0, Lbbg;

    const-string v1, "METRIC_AVERAGE_SHARPNESS"

    const/4 v2, 0x1

    const-string v3, "sharpness"

    sget-object v4, Lbkz;->a:Lbkz;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lbbg;-><init>(Ljava/lang/String;ILjava/lang/String;Lbkz;I)V

    sput-object v0, Lbbg;->c:Lbbg;

    .line 40
    new-instance v0, Lbbg;

    const-string v1, "METRIC_SALIENCY_SCORE"

    const/4 v2, 0x2

    const-string v3, "saliency"

    sget-object v4, Lbkz;->e:Lbkz;

    const/4 v5, 0x2

    invoke-direct/range {v0 .. v5}, Lbbg;-><init>(Ljava/lang/String;ILjava/lang/String;Lbkz;I)V

    sput-object v0, Lbbg;->d:Lbbg;

    .line 43
    new-instance v0, Lbbg;

    const-string v1, "METRIC_HISTOGRAM_SHARPNESS"

    const/4 v2, 0x3

    const-string v3, "sharpness_hist"

    sget-object v4, Lbkz;->c:Lbkz;

    const/4 v5, 0x2

    invoke-direct/range {v0 .. v5}, Lbbg;-><init>(Ljava/lang/String;ILjava/lang/String;Lbkz;I)V

    sput-object v0, Lbbg;->e:Lbbg;

    .line 46
    new-instance v0, Lbbg;

    const-string v1, "METRIC_NEW_HISTOGRAM_AVERAGE_CHROMA"

    const/4 v2, 0x4

    const-string v3, "new_chroma_hist"

    sget-object v4, Lbkz;->j:Lbkz;

    const/4 v5, 0x3

    invoke-direct/range {v0 .. v5}, Lbbg;-><init>(Ljava/lang/String;ILjava/lang/String;Lbkz;I)V

    sput-object v0, Lbbg;->f:Lbbg;

    .line 49
    new-instance v0, Lbbg;

    const-string v1, "METRIC_HISTOGRAM_SMILESCORE"

    const/4 v2, 0x5

    const-string v3, "smiliness_hist"

    sget-object v4, Lbkz;->d:Lbkz;

    const/4 v5, 0x2

    invoke-direct/range {v0 .. v5}, Lbbg;-><init>(Ljava/lang/String;ILjava/lang/String;Lbkz;I)V

    sput-object v0, Lbbg;->g:Lbbg;

    .line 52
    new-instance v0, Lbbg;

    const-string v1, "METRIC_MOTION_SALIENCY_SCORE"

    const/4 v2, 0x6

    const-string v3, "motion_saliency"

    sget-object v4, Lbkz;->f:Lbkz;

    const/4 v5, 0x2

    invoke-direct/range {v0 .. v5}, Lbbg;-><init>(Ljava/lang/String;ILjava/lang/String;Lbkz;I)V

    sput-object v0, Lbbg;->h:Lbbg;

    .line 55
    new-instance v0, Lbbg;

    const-string v1, "METRIC_NEW_MOTION_SALIENCY_SCORE"

    const/4 v2, 0x7

    const-string v3, "new_motion_saliency"

    sget-object v4, Lbkz;->p:Lbkz;

    const/4 v5, 0x2

    invoke-direct/range {v0 .. v5}, Lbbg;-><init>(Ljava/lang/String;ILjava/lang/String;Lbkz;I)V

    sput-object v0, Lbbg;->i:Lbbg;

    .line 58
    new-instance v0, Lbbg;

    const-string v1, "METRIC_JITTER_CAMERA_MOTION"

    const/16 v2, 0x8

    const-string v3, "jitter_camera_motion_stabilizer"

    sget-object v4, Lbkz;->o:Lbkz;

    const/4 v5, 0x2

    invoke-direct/range {v0 .. v5}, Lbbg;-><init>(Ljava/lang/String;ILjava/lang/String;Lbkz;I)V

    sput-object v0, Lbbg;->j:Lbbg;

    .line 62
    new-instance v0, Lbbg;

    const-string v1, "METRIC_AVERAGE_FACE_AREA"

    const/16 v2, 0x9

    const-string v3, "face_area"

    sget-object v4, Lbkz;->g:Lbkz;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lbbg;-><init>(Ljava/lang/String;ILjava/lang/String;Lbkz;I)V

    sput-object v0, Lbbg;->k:Lbbg;

    .line 65
    new-instance v0, Lbbg;

    const-string v1, "METRIC_AVERAGE_NEW_COLORFULNESS"

    const/16 v2, 0xa

    const-string v3, "new_colorfulness"

    sget-object v4, Lbkz;->i:Lbkz;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lbbg;-><init>(Ljava/lang/String;ILjava/lang/String;Lbkz;I)V

    sput-object v0, Lbbg;->l:Lbbg;

    .line 68
    new-instance v0, Lbbg;

    const-string v1, "METRIC_AUDIO_RMS"

    const/16 v2, 0xb

    const-string v3, "audio_rms"

    sget-object v4, Lbkz;->h:Lbkz;

    const/4 v5, 0x2

    invoke-direct/range {v0 .. v5}, Lbbg;-><init>(Ljava/lang/String;ILjava/lang/String;Lbkz;I)V

    sput-object v0, Lbbg;->m:Lbbg;

    .line 71
    new-instance v0, Lbbg;

    const-string v1, "METRIC_AUDIO_MFCC_MEAN"

    const/16 v2, 0xc

    const-string v3, "audio_mfcc_mean"

    sget-object v4, Lbkz;->k:Lbkz;

    const/4 v5, 0x3

    invoke-direct/range {v0 .. v5}, Lbbg;-><init>(Ljava/lang/String;ILjava/lang/String;Lbkz;I)V

    sput-object v0, Lbbg;->n:Lbbg;

    .line 74
    new-instance v0, Lbbg;

    const-string v1, "METRIC_AUDIO_MFCC_STDDEV"

    const/16 v2, 0xd

    const-string v3, "audio_mfcc_stddev"

    sget-object v4, Lbkz;->l:Lbkz;

    const/4 v5, 0x3

    invoke-direct/range {v0 .. v5}, Lbbg;-><init>(Ljava/lang/String;ILjava/lang/String;Lbkz;I)V

    sput-object v0, Lbbg;->o:Lbbg;

    .line 77
    new-instance v0, Lbbg;

    const-string v1, "METRIC_DELTA_MFCC_MEAN"

    const/16 v2, 0xe

    const-string v3, "delta_mfcc_mean"

    sget-object v4, Lbkz;->m:Lbkz;

    const/4 v5, 0x3

    invoke-direct/range {v0 .. v5}, Lbbg;-><init>(Ljava/lang/String;ILjava/lang/String;Lbkz;I)V

    sput-object v0, Lbbg;->p:Lbbg;

    .line 80
    new-instance v0, Lbbg;

    const-string v1, "METRIC_DELTA_MFCC_STDDEV"

    const/16 v2, 0xf

    const-string v3, "delta_mfcc_stddev"

    sget-object v4, Lbkz;->n:Lbkz;

    const/4 v5, 0x3

    invoke-direct/range {v0 .. v5}, Lbbg;-><init>(Ljava/lang/String;ILjava/lang/String;Lbkz;I)V

    sput-object v0, Lbbg;->q:Lbbg;

    .line 33
    const/16 v0, 0x10

    new-array v0, v0, [Lbbg;

    const/4 v1, 0x0

    sget-object v2, Lbbg;->b:Lbbg;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lbbg;->c:Lbbg;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lbbg;->d:Lbbg;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lbbg;->e:Lbbg;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lbbg;->f:Lbbg;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lbbg;->g:Lbbg;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lbbg;->h:Lbbg;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lbbg;->i:Lbbg;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lbbg;->j:Lbbg;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lbbg;->k:Lbbg;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lbbg;->l:Lbbg;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lbbg;->m:Lbbg;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lbbg;->n:Lbbg;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lbbg;->o:Lbbg;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lbbg;->p:Lbbg;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lbbg;->q:Lbbg;

    aput-object v2, v0, v1

    sput-object v0, Lbbg;->t:[Lbbg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Lbkz;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lbkz;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 89
    iput-object p3, p0, Lbbg;->a:Ljava/lang/String;

    .line 90
    iput-object p4, p0, Lbbg;->r:Lbkz;

    .line 91
    iput p5, p0, Lbbg;->s:I

    .line 92
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbbg;
    .locals 1

    .prologue
    .line 33
    const-class v0, Lbbg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbbg;

    return-object v0
.end method

.method public static values()[Lbbg;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lbbg;->t:[Lbbg;

    invoke-virtual {v0}, [Lbbg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbbg;

    return-object v0
.end method


# virtual methods
.method public a(Lbbf;Lorg/w3c/dom/Element;)Lbbd;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 97
    iget v0, p0, Lbbg;->s:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 98
    iget-object v1, p0, Lbbg;->r:Lbkz;

    invoke-static {p2}, Lbbe;->a(Lorg/w3c/dom/Element;)Lbaz;

    move-result-object v2

    new-instance v0, Lbbl;

    invoke-virtual {p1}, Lbbf;->b()F

    move-result v3

    invoke-virtual {p1}, Lbbf;->a()F

    move-result v4

    invoke-direct {v0, v1, v3, v4, v2}, Lbbl;-><init>(Lbkz;FFLbaz;)V

    .line 107
    :goto_0
    return-object v0

    .line 100
    :cond_0
    iget v0, p0, Lbbg;->s:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 101
    iget-object v1, p0, Lbbg;->r:Lbkz;

    invoke-virtual {p1}, Lbbf;->c()I

    move-result v0

    if-ltz v0, :cond_1

    new-instance v4, Lbaw;

    invoke-virtual {p1}, Lbbf;->c()I

    move-result v0

    invoke-direct {v4, v0}, Lbaw;-><init>(I)V

    invoke-static {p2}, Lbbe;->a(Lorg/w3c/dom/Element;)Lbaz;

    move-result-object v5

    :goto_1
    new-instance v0, Lbbk;

    invoke-virtual {p1}, Lbbf;->b()F

    move-result v2

    invoke-virtual {p1}, Lbbf;->a()F

    move-result v3

    invoke-direct/range {v0 .. v5}, Lbbk;-><init>(Lbkz;FFLbav;Lbaz;)V

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_e

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "No functions supported for float array"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_2
    iget v0, p0, Lbbg;->s:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_7

    .line 104
    iget-object v1, p0, Lbbg;->r:Lbkz;

    invoke-virtual {p1}, Lbbf;->c()I

    move-result v0

    if-ltz v0, :cond_3

    new-instance v4, Lbay;

    invoke-virtual {p1}, Lbbf;->c()I

    move-result v0

    invoke-direct {v4, v0}, Lbay;-><init>(I)V

    invoke-static {p2}, Lbbe;->a(Lorg/w3c/dom/Element;)Lbaz;

    move-result-object v5

    :goto_2
    new-instance v0, Lbbm;

    invoke-virtual {p1}, Lbbf;->b()F

    move-result v2

    invoke-virtual {p1}, Lbbf;->a()F

    move-result v3

    invoke-direct/range {v0 .. v5}, Lbbm;-><init>(Lbkz;FFLbax;Lbaz;)V

    goto :goto_0

    :cond_3
    if-eqz p2, :cond_d

    const-string v0, "name"

    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "entropyHSV"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v4, Lbas;

    invoke-direct {v4}, Lbas;-><init>()V

    goto :goto_2

    :cond_4
    const-string v2, "saturationHSV"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    new-instance v4, Lbbb;

    invoke-direct {v4}, Lbbb;-><init>()V

    goto :goto_2

    :cond_5
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unsupported feature function: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_3
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 106
    :cond_7
    iget v0, p0, Lbbg;->s:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_b

    .line 107
    iget-object v1, p0, Lbbg;->r:Lbkz;

    invoke-virtual {p1}, Lbbf;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_8

    new-instance v4, Lbat;

    invoke-virtual {p1}, Lbbf;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Lbat;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lbbe;->a(Lorg/w3c/dom/Element;)Lbaz;

    move-result-object v5

    :goto_4
    new-instance v0, Lbbj;

    invoke-virtual {p1}, Lbbf;->b()F

    move-result v2

    invoke-virtual {p1}, Lbbf;->a()F

    move-result v3

    invoke-direct/range {v0 .. v5}, Lbbj;-><init>(Lbkz;FFLbau;Lbaz;)V

    goto/16 :goto_0

    :cond_8
    if-eqz p2, :cond_c

    const-string v0, "name"

    invoke-interface {p2, v0}, Lorg/w3c/dom/Element;->getAttribute(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "has_face"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    new-instance v4, Lbba;

    invoke-direct {v4}, Lbba;-><init>()V

    goto :goto_4

    :cond_9
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unsupported feature function: "

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_a

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_5
    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_a
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_5

    .line 109
    :cond_b
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported feature type."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_c
    move-object v4, v5

    goto :goto_4

    :cond_d
    move-object v4, v5

    goto/16 :goto_2

    :cond_e
    move-object v4, v5

    goto/16 :goto_1
.end method
