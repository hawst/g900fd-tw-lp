.class public final Lgyy;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lgyy;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private d:I

.field private e:Z

.field private f:J

.field private g:Ljava/lang/String;

.field private h:I

.field private i:I

.field private j:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lgyy;

    invoke-direct {v0}, Lgyy;-><init>()V

    sput-object v0, Lgyy;->a:Lgyy;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x1fffff

    iput v0, p0, Lgyy;->d:I

    const/4 v0, 0x7

    iput v0, p0, Lgyy;->h:I

    const/4 v0, 0x0

    iput v0, p0, Lgyy;->i:I

    return-void
.end method


# virtual methods
.method public a(I)Lgyy;
    .locals 0

    iput p1, p0, Lgyy;->d:I

    return-object p0
.end method

.method public a(J)Lgyy;
    .locals 1

    iput-wide p1, p0, Lgyy;->f:J

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lgyy;
    .locals 0

    iput-object p1, p0, Lgyy;->b:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/util/Collection;)Lgyy;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lgyy;"
        }
    .end annotation

    iput-object p1, p0, Lgyy;->c:Ljava/util/Collection;

    return-object p0
.end method

.method public a(Z)Lgyy;
    .locals 0

    iput-boolean p1, p0, Lgyy;->e:Z

    return-object p0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgyy;->b:Ljava/lang/String;

    return-object v0
.end method

.method public b(I)Lgyy;
    .locals 0

    iput p1, p0, Lgyy;->h:I

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lgyy;
    .locals 0

    iput-object p1, p0, Lgyy;->g:Ljava/lang/String;

    return-object p0
.end method

.method public b()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lgyy;->c:Ljava/util/Collection;

    return-object v0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lgyy;->d:I

    return v0
.end method

.method public c(I)Lgyy;
    .locals 0

    iput p1, p0, Lgyy;->i:I

    return-object p0
.end method

.method public d(I)Lgyy;
    .locals 0

    iput p1, p0, Lgyy;->j:I

    return-object p0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lgyy;->e:Z

    return v0
.end method

.method public e()J
    .locals 2

    iget-wide v0, p0, Lgyy;->f:J

    return-wide v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgyy;->g:Ljava/lang/String;

    return-object v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, Lgyy;->h:I

    return v0
.end method

.method public h()I
    .locals 1

    iget v0, p0, Lgyy;->i:I

    return v0
.end method

.method public i()I
    .locals 1

    iget v0, p0, Lgyy;->j:I

    return v0
.end method
