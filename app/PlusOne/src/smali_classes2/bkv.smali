.class public final Lbkv;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lbmz;",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/TreeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 202
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbkv;->a:Ljava/util/Map;

    .line 204
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lbkv;->b:Ljava/util/TreeSet;

    .line 205
    return-void
.end method

.method public constructor <init>(Lbku;)V
    .locals 6

    .prologue
    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208
    new-instance v0, Ljava/util/HashMap;

    .line 209
    iget-object v1, p1, Lbku;->a:Ljava/util/Map;

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Lbkv;->a:Ljava/util/Map;

    .line 210
    new-instance v0, Ljava/util/TreeSet;

    invoke-direct {v0}, Ljava/util/TreeSet;-><init>()V

    iput-object v0, p0, Lbkv;->b:Ljava/util/TreeSet;

    .line 211
    invoke-virtual {p1}, Lbku;->a()[J

    move-result-object v1

    .line 212
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-wide v4, v1, v0

    .line 213
    iget-object v3, p0, Lbkv;->b:Ljava/util/TreeSet;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 212
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 215
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lbku;
    .locals 3

    .prologue
    .line 261
    new-instance v0, Lbku;

    iget-object v1, p0, Lbkv;->a:Ljava/util/Map;

    iget-object v2, p0, Lbkv;->b:Ljava/util/TreeSet;

    invoke-direct {v0, v1, v2}, Lbku;-><init>(Ljava/util/Map;Ljava/util/TreeSet;)V

    return-object v0
.end method

.method public a(J)Lbkv;
    .locals 7

    .prologue
    .line 244
    iget-object v0, p0, Lbkv;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/LongSparseArray;

    .line 245
    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_0
    if-ltz v1, :cond_0

    .line 246
    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    cmp-long v3, v4, p1

    if-ltz v3, :cond_0

    .line 247
    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->removeAt(I)V

    .line 245
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 252
    :cond_1
    return-object p0
.end method

.method public a(Lbmz;JLjava/lang/Object;)Lbkv;
    .locals 2

    .prologue
    .line 227
    iget-object v0, p0, Lbkv;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/LongSparseArray;

    .line 228
    if-nez v0, :cond_0

    .line 229
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    .line 230
    iget-object v1, p0, Lbkv;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 232
    :cond_0
    invoke-virtual {v0, p2, p3, p4}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 233
    iget-object v0, p0, Lbkv;->b:Ljava/util/TreeSet;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeSet;->add(Ljava/lang/Object;)Z

    .line 234
    return-object p0
.end method

.method public a(Lbmz;)Lbkw;
    .locals 1

    .prologue
    .line 271
    iget-object v0, p0, Lbkv;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 272
    const/4 v0, 0x0

    .line 274
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lbkw;

    invoke-direct {v0, p0, p1}, Lbkw;-><init>(Lbkv;Lbmz;)V

    goto :goto_0
.end method
