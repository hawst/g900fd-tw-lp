.class public final Lbkj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lajl;


# instance fields
.field private final a:Lajl;

.field private b:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Landroid/net/Uri;",
            "Lbvf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lajl;)V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Lbkk;

    const/high16 v1, 0x1800000

    invoke-direct {v0, v1}, Lbkk;-><init>(I)V

    iput-object v0, p0, Lbkj;->b:Landroid/util/LruCache;

    .line 59
    iput-object p1, p0, Lbkj;->a:Lajl;

    .line 60
    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;)Lbvf;
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lbkj;->b:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvf;

    .line 66
    if-nez v0, :cond_0

    .line 67
    iget-object v0, p0, Lbkj;->a:Lajl;

    invoke-interface {v0, p1}, Lajl;->a(Landroid/net/Uri;)Lbvf;

    move-result-object v0

    .line 68
    if-eqz v0, :cond_0

    .line 69
    iget-object v1, p0, Lbkj;->b:Landroid/util/LruCache;

    invoke-virtual {v1, p1, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    :cond_0
    return-object v0
.end method

.method public a(Landroid/net/Uri;Lbvf;)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lbkj;->a:Lajl;

    invoke-interface {v0, p1, p2}, Lajl;->a(Landroid/net/Uri;Lbvf;)V

    .line 79
    iget-object v0, p0, Lbkj;->b:Landroid/util/LruCache;

    invoke-virtual {v0, p1, p2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    return-void
.end method

.method public f(Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lbkj;->b:Landroid/util/LruCache;

    invoke-virtual {v0, p1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvf;

    .line 86
    if-eqz v0, :cond_0

    .line 87
    iget-boolean v0, v0, Lbvf;->f:Z

    .line 89
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lbkj;->a:Lajl;

    invoke-interface {v0, p1}, Lajl;->f(Landroid/net/Uri;)Z

    move-result v0

    goto :goto_0
.end method
