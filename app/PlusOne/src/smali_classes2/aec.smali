.class public final Laec;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# instance fields
.field private synthetic a:Landroidx/media/filterpacks/image/SurfaceTextureTarget;


# direct methods
.method public constructor <init>(Landroidx/media/filterpacks/image/SurfaceTextureTarget;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Laec;->a:Landroidx/media/filterpacks/image/SurfaceTextureTarget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Laec;->a:Landroidx/media/filterpacks/image/SurfaceTextureTarget;

    invoke-virtual {v0, p1, p2, p3}, Landroidx/media/filterpacks/image/SurfaceTextureTarget;->a(Landroid/graphics/SurfaceTexture;II)V

    .line 55
    iget-object v0, p0, Laec;->a:Landroidx/media/filterpacks/image/SurfaceTextureTarget;

    invoke-static {v0}, Landroidx/media/filterpacks/image/SurfaceTextureTarget;->a(Landroidx/media/filterpacks/image/SurfaceTextureTarget;)Landroid/os/ConditionVariable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 56
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Laec;->a:Landroidx/media/filterpacks/image/SurfaceTextureTarget;

    invoke-static {v0}, Landroidx/media/filterpacks/image/SurfaceTextureTarget;->b(Landroidx/media/filterpacks/image/SurfaceTextureTarget;)V

    .line 66
    const/4 v0, 0x1

    return v0
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Laec;->a:Landroidx/media/filterpacks/image/SurfaceTextureTarget;

    invoke-virtual {v0, p2, p3}, Landroidx/media/filterpacks/image/SurfaceTextureTarget;->a(II)V

    .line 61
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0

    .prologue
    .line 71
    return-void
.end method
