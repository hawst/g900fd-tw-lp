.class public Lakp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lald;

.field private final c:Lamy;

.field private final d:Lasy;

.field private final e:Lakq;

.field private f:Laks;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lakp;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lald;Lamy;Lasy;Lakq;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    const-string v0, "context"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lakp;->a:Landroid/content/Context;

    .line 61
    const-string v0, "appInfo"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lald;

    iput-object v0, p0, Lakp;->b:Lald;

    .line 62
    const-string v0, "deviceInfo"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamy;

    iput-object v0, p0, Lakp;->c:Lamy;

    .line 63
    const-string v0, "stateTracker"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lasy;

    iput-object v0, p0, Lakp;->d:Lasy;

    .line 64
    const-string v0, "analysisPerformanceLogs"

    .line 65
    invoke-static {p5, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakq;

    iput-object v0, p0, Lakp;->e:Lakq;

    .line 66
    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;Laic;)V
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lakp;->a(Landroid/net/Uri;Laic;Lbkn;)V

    .line 91
    return-void
.end method

.method public a(Landroid/net/Uri;Laic;Lbkn;)V
    .locals 4

    .prologue
    .line 73
    new-instance v0, Laks;

    iget-object v1, p0, Lakp;->b:Lald;

    iget-object v2, p0, Lakp;->c:Lamy;

    invoke-direct {v0, p1, p2, v1, v2}, Laks;-><init>(Landroid/net/Uri;Laic;Lald;Lamy;)V

    iput-object v0, p0, Lakp;->f:Laks;

    .line 74
    iget-object v0, p0, Lakp;->f:Laks;

    iget-object v1, p0, Lakp;->a:Landroid/content/Context;

    invoke-static {v1}, Lakt;->a(Landroid/content/Context;)Lakt;

    move-result-object v1

    invoke-virtual {v0, v1}, Laks;->a(Lakt;)V

    .line 75
    if-eqz p3, :cond_0

    .line 76
    iget-object v0, p0, Lakp;->f:Laks;

    .line 77
    invoke-virtual {p3}, Lbkn;->b()J

    move-result-wide v2

    .line 76
    invoke-virtual {v0, v2, v3}, Laks;->b(J)V

    .line 83
    :cond_0
    iget-object v0, p0, Lakp;->d:Lasy;

    iget-object v1, p0, Lakp;->f:Laks;

    invoke-virtual {v0, v1}, Lasy;->a(Livo;)V

    .line 84
    return-void
.end method

.method public a(Lbkn;)V
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lakp;->a(Lbkn;Lcdu;)V

    .line 98
    return-void
.end method

.method public a(Lbkn;Lcdu;)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 105
    iget-object v0, p0, Lakp;->f:Laks;

    iget-object v1, p0, Lakp;->a:Landroid/content/Context;

    invoke-static {v1}, Lakt;->a(Landroid/content/Context;)Lakt;

    move-result-object v1

    invoke-virtual {v0, v1}, Laks;->b(Lakt;)V

    .line 106
    iget-object v1, p0, Lakp;->f:Laks;

    if-nez p1, :cond_3

    new-instance v0, Lakv;

    const-wide/16 v2, -0x1

    invoke-direct {v0, v4, v4, v2, v3}, Lakv;-><init>(IIJ)V

    :goto_0
    invoke-virtual {v1, v0}, Laks;->a(Lakv;)V

    .line 107
    if-eqz p1, :cond_0

    .line 108
    iget-object v0, p0, Lakp;->f:Laks;

    invoke-virtual {p1}, Lbkn;->e()Z

    move-result v1

    invoke-virtual {v0, v1}, Laks;->a(Z)V

    .line 109
    iget-object v0, p0, Lakp;->f:Laks;

    invoke-virtual {p1}, Lbkn;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Laks;->a(J)V

    .line 111
    :cond_0
    iget-object v0, p0, Lakp;->e:Lakq;

    if-eqz v0, :cond_1

    .line 112
    iget-object v0, p0, Lakp;->f:Laks;

    iget-object v1, p0, Lakp;->e:Lakq;

    invoke-virtual {v0, v1}, Laks;->a(Lakq;)V

    .line 118
    :cond_1
    if-eqz p2, :cond_2

    .line 119
    iget-object v0, p0, Lakp;->f:Laks;

    invoke-virtual {p2, v0}, Lcdu;->a(Laks;)V

    .line 121
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lakp;->f:Laks;

    .line 122
    return-void

    .line 106
    :cond_3
    new-instance v0, Lakv;

    invoke-virtual {p1}, Lbkn;->a()J

    move-result-wide v2

    invoke-direct {v0, v4, v4, v2, v3}, Lakv;-><init>(IIJ)V

    goto :goto_0
.end method
