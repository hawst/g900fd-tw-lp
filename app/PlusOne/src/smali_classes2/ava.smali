.class public Lava;
.super Lamn;
.source "PG"

# interfaces
.implements Laur;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lbrh;

.field private final c:Lalg;

.field private final d:Lawi;

.field private final e:Lavd;

.field private final f:Lave;

.field private final g:Lavi;

.field private final h:Lavg;

.field private final j:Lavf;

.field private k:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<***>;"
        }
    .end annotation
.end field

.field private l:Lavm;

.field private m:Lavh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lava;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lava;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lalg;Lawi;Lbrh;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 91
    invoke-direct {p0}, Lamn;-><init>()V

    .line 42
    new-instance v0, Lavd;

    invoke-direct {v0, p0}, Lavd;-><init>(Lava;)V

    iput-object v0, p0, Lava;->e:Lavd;

    .line 44
    new-instance v0, Lave;

    invoke-direct {v0, p0}, Lave;-><init>(Lava;)V

    iput-object v0, p0, Lava;->f:Lave;

    .line 46
    new-instance v0, Lavi;

    invoke-direct {v0, p0}, Lavi;-><init>(Lava;)V

    iput-object v0, p0, Lava;->g:Lavi;

    .line 48
    new-instance v0, Lavg;

    invoke-direct {v0, p0}, Lavg;-><init>(Lava;)V

    iput-object v0, p0, Lava;->h:Lavg;

    .line 49
    new-instance v0, Lavf;

    invoke-direct {v0, p0}, Lavf;-><init>(Lava;)V

    iput-object v0, p0, Lava;->j:Lavf;

    .line 86
    sget-object v0, Lavh;->a:Lavh;

    iput-object v0, p0, Lava;->m:Lavh;

    .line 92
    const-string v0, "musicLibraryFactory"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrh;

    iput-object v0, p0, Lava;->b:Lbrh;

    .line 93
    const-string v0, "networkAsyncTaskRunner"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lalg;

    iput-object v0, p0, Lava;->c:Lalg;

    .line 95
    const-string v0, "state"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawi;

    iput-object v0, p0, Lava;->d:Lawi;

    .line 96
    return-void
.end method

.method static synthetic a(Lava;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lava;->k:Landroid/os/AsyncTask;

    return-object p1
.end method

.method static synthetic a(Lava;Lavh;)Lavh;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lava;->m:Lavh;

    return-object p1
.end method

.method static synthetic a(Lava;Lavm;)Lavm;
    .locals 0

    .prologue
    .line 31
    iput-object p1, p0, Lava;->l:Lavm;

    return-object p1
.end method

.method static synthetic a(Lava;)V
    .locals 0

    .prologue
    .line 31
    invoke-virtual {p0}, Lava;->e()V

    return-void
.end method

.method private a(Lavh;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 111
    iget-object v0, p0, Lava;->k:Landroid/os/AsyncTask;

    const-string v1, "mCurrentTask"

    invoke-static {v0, v1, v3}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 112
    iget-object v0, p0, Lava;->m:Lavh;

    const-string v1, "mTaskState"

    sget-object v2, Lavh;->a:Lavh;

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    .line 114
    sget-object v0, Lavc;->a:[I

    invoke-virtual {p1}, Lavh;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 128
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid task value: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 116
    :pswitch_0
    invoke-virtual {p0}, Lava;->e()V

    iget-object v0, p0, Lava;->m:Lavh;

    sget-object v1, Lavh;->b:Lavh;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lava;->k:Landroid/os/AsyncTask;

    const-string v1, "current task"

    invoke-static {v0, v1, v3}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lava;->m:Lavh;

    const-string v1, "busy state should be NOT_BUSY"

    sget-object v2, Lavh;->a:Lavh;

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    iget-object v0, p0, Lava;->d:Lawi;

    invoke-interface {v0}, Lawi;->u()Lbrc;

    move-result-object v0

    const-string v1, "music library"

    invoke-static {v0, v1, v3}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lava;->d:Lawi;

    invoke-interface {v0}, Lawi;->v()Lbmk;

    move-result-object v0

    const-string v1, "detailed soundtrack"

    invoke-static {v0, v1, v3}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    new-instance v0, Lauk;

    iget-object v1, p0, Lava;->f:Lave;

    invoke-direct {v0, v1}, Lauk;-><init>(Laul;)V

    sget-object v1, Lavh;->b:Lavh;

    iput-object v1, p0, Lava;->m:Lavh;

    iput-object v0, p0, Lava;->k:Landroid/os/AsyncTask;

    iget-object v1, p0, Lava;->c:Lalg;

    const-class v2, Lauk;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    new-array v2, v5, [Lbrh;

    iget-object v3, p0, Lava;->b:Lbrh;

    aput-object v3, v2, v4

    invoke-interface {v1, v0, v2}, Lalg;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 126
    :cond_0
    :goto_0
    return-void

    .line 119
    :pswitch_1
    invoke-virtual {p0}, Lava;->e()V

    iget-object v0, p0, Lava;->k:Landroid/os/AsyncTask;

    const-string v1, "current task"

    invoke-static {v0, v1, v3}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lava;->m:Lavh;

    const-string v1, "busy state should be NOT_BUSY"

    sget-object v2, Lavh;->a:Lavh;

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    iget-object v0, p0, Lava;->d:Lawi;

    invoke-interface {v0}, Lawi;->u()Lbrc;

    move-result-object v0

    const-string v1, "music library"

    invoke-static {v0, v1, v3}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrc;

    iget-object v1, p0, Lava;->d:Lawi;

    invoke-interface {v1}, Lawi;->w()Lboh;

    move-result-object v1

    const-string v2, "soundtrack"

    invoke-static {v1, v2, v3}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lboh;

    iget-object v2, p0, Lava;->d:Lawi;

    invoke-interface {v2, v4}, Lawi;->e(Z)V

    new-instance v2, Laui;

    iget-object v3, p0, Lava;->e:Lavd;

    invoke-direct {v2, v0, v3}, Laui;-><init>(Lbrc;Lauj;)V

    sget-object v0, Lavh;->c:Lavh;

    iput-object v0, p0, Lava;->m:Lavh;

    iput-object v2, p0, Lava;->k:Landroid/os/AsyncTask;

    iget-object v0, p0, Lava;->c:Lalg;

    const-class v3, Laui;

    invoke-virtual {v3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    new-array v3, v5, [Lboh;

    aput-object v1, v3, v4

    invoke-interface {v0, v2, v3}, Lalg;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    goto :goto_0

    .line 122
    :pswitch_2
    invoke-direct {p0, v5}, Lava;->a(Z)V

    goto :goto_0

    .line 125
    :pswitch_3
    invoke-direct {p0, v4}, Lava;->a(Z)V

    goto :goto_0

    .line 114
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private a(Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 232
    invoke-virtual {p0}, Lava;->e()V

    .line 233
    iget-object v0, p0, Lava;->d:Lawi;

    invoke-interface {v0}, Lawi;->v()Lbmk;

    move-result-object v0

    const-string v1, "detailed soundtrack"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 234
    iget-object v0, p0, Lava;->d:Lawi;

    invoke-interface {v0}, Lawi;->F()Lboi;

    move-result-object v0

    .line 236
    if-eqz v0, :cond_0

    iget-object v1, p0, Lava;->d:Lawi;

    invoke-interface {v1}, Lawi;->G()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    if-nez p1, :cond_1

    .line 273
    :goto_0
    return-void

    .line 241
    :cond_1
    if-eqz p1, :cond_2

    const-wide/32 v2, 0x989680

    .line 243
    :goto_1
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-ltz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    const-string v1, "endpoint should be non-negative"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 244
    iget-object v0, p0, Lava;->d:Lawi;

    invoke-interface {v0, v6}, Lawi;->e(Z)V

    .line 245
    new-instance v0, Laug;

    iget-object v1, p0, Lava;->d:Lawi;

    invoke-interface {v1}, Lawi;->u()Lbrc;

    move-result-object v1

    iget-object v4, p0, Lava;->d:Lawi;

    .line 246
    invoke-interface {v4}, Lawi;->v()Lbmk;

    move-result-object v4

    new-instance v5, Lavb;

    invoke-direct {v5, p0, p1}, Lavb;-><init>(Lava;Z)V

    invoke-direct/range {v0 .. v5}, Laug;-><init>(Lbrc;JLbmk;Lauh;)V

    .line 266
    if-eqz p1, :cond_4

    .line 267
    sget-object v1, Lavh;->d:Lavh;

    iput-object v1, p0, Lava;->m:Lavh;

    .line 271
    :goto_3
    iput-object v0, p0, Lava;->k:Landroid/os/AsyncTask;

    .line 272
    iget-object v1, p0, Lava;->c:Lalg;

    const-class v2, Laug;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    new-array v2, v6, [Ljava/lang/Void;

    invoke-interface {v1, v0, v2}, Lalg;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    goto :goto_0

    .line 242
    :cond_2
    invoke-virtual {v0}, Lboi;->d()J

    move-result-wide v2

    goto :goto_1

    :cond_3
    move v0, v6

    .line 243
    goto :goto_2

    .line 269
    :cond_4
    sget-object v1, Lavh;->e:Lavh;

    iput-object v1, p0, Lava;->m:Lavh;

    goto :goto_3
.end method

.method static synthetic b(Lava;)Lawi;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lava;->d:Lawi;

    return-object v0
.end method

.method static synthetic c(Lava;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lava;->h()V

    return-void
.end method

.method static synthetic d(Lava;)V
    .locals 0

    .prologue
    .line 31
    invoke-virtual {p0}, Lava;->e()V

    return-void
.end method

.method static synthetic e(Lava;)Lavh;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lava;->m:Lavh;

    return-object v0
.end method

.method static synthetic f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lava;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lava;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lava;->g()V

    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lava;->k:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lava;->k:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lava;->k:Landroid/os/AsyncTask;

    .line 104
    :cond_0
    sget-object v0, Lavh;->a:Lavh;

    iput-object v0, p0, Lava;->m:Lavh;

    .line 105
    return-void
.end method

.method static synthetic g(Lava;)V
    .locals 0

    .prologue
    .line 31
    invoke-virtual {p0}, Lava;->e()V

    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lava;->m:Lavh;

    sget-object v1, Lavh;->a:Lavh;

    if-eq v0, v1, :cond_1

    .line 211
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    iget-object v0, p0, Lava;->d:Lawi;

    invoke-interface {v0}, Lawi;->u()Lbrc;

    move-result-object v0

    if-nez v0, :cond_2

    .line 185
    sget-object v0, Lavh;->b:Lavh;

    invoke-direct {p0, v0}, Lava;->a(Lavh;)V

    goto :goto_0

    .line 190
    :cond_2
    iget-object v0, p0, Lava;->d:Lawi;

    invoke-interface {v0}, Lawi;->w()Lboh;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lava;->d:Lawi;

    invoke-interface {v0}, Lawi;->v()Lbmk;

    move-result-object v0

    if-nez v0, :cond_3

    .line 196
    sget-object v0, Lavh;->c:Lavh;

    invoke-direct {p0, v0}, Lava;->a(Lavh;)V

    goto :goto_0

    .line 201
    :cond_3
    iget-object v0, p0, Lava;->d:Lawi;

    invoke-interface {v0}, Lawi;->ae()Z

    move-result v0

    if-nez v0, :cond_4

    .line 202
    sget-object v0, Lavh;->d:Lavh;

    invoke-direct {p0, v0}, Lava;->a(Lavh;)V

    goto :goto_0

    .line 208
    :cond_4
    iget-object v0, p0, Lava;->d:Lawi;

    invoke-interface {v0}, Lawi;->F()Lboi;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lava;->d:Lawi;

    invoke-interface {v0}, Lawi;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 209
    sget-object v0, Lavh;->e:Lavh;

    invoke-direct {p0, v0}, Lava;->a(Lavh;)V

    goto :goto_0
.end method

.method static synthetic h(Lava;)V
    .locals 0

    .prologue
    .line 31
    invoke-virtual {p0}, Lava;->e()V

    return-void
.end method

.method static synthetic i(Lava;)V
    .locals 0

    .prologue
    .line 31
    invoke-virtual {p0}, Lava;->e()V

    return-void
.end method

.method static synthetic j(Lava;)V
    .locals 0

    .prologue
    .line 31
    invoke-virtual {p0}, Lava;->e()V

    return-void
.end method

.method static synthetic k(Lava;)Lavm;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lava;->l:Lavm;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 169
    invoke-super {p0}, Lamn;->a()V

    .line 170
    iget-object v0, p0, Lava;->d:Lawi;

    iget-object v1, p0, Lava;->h:Lavg;

    invoke-interface {v0, v1}, Lawi;->i(Lawk;)V

    .line 171
    iget-object v0, p0, Lava;->d:Lawi;

    iget-object v1, p0, Lava;->j:Lavf;

    invoke-interface {v0, v1}, Lawi;->n(Lawk;)V

    .line 172
    invoke-direct {p0}, Lava;->h()V

    .line 173
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 215
    iget-object v0, p0, Lava;->d:Lawi;

    iget-object v1, p0, Lava;->h:Lavg;

    invoke-interface {v0, v1}, Lawi;->j(Lawk;)V

    .line 216
    iget-object v0, p0, Lava;->d:Lawi;

    iget-object v1, p0, Lava;->j:Lavf;

    invoke-interface {v0, v1}, Lawi;->o(Lawk;)V

    .line 217
    invoke-direct {p0}, Lava;->g()V

    .line 218
    iget-object v0, p0, Lava;->l:Lavm;

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lava;->l:Lavm;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lavm;->cancel(Z)Z

    .line 220
    const/4 v0, 0x0

    iput-object v0, p0, Lava;->l:Lavm;

    .line 222
    :cond_0
    invoke-super {p0}, Lamn;->b()V

    .line 223
    return-void
.end method

.method public c()V
    .locals 3

    .prologue
    .line 138
    invoke-virtual {p0}, Lava;->e()V

    .line 139
    iget-object v0, p0, Lava;->d:Lawi;

    invoke-interface {v0}, Lawi;->u()Lbrc;

    move-result-object v0

    .line 141
    if-nez v0, :cond_1

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    iget-object v1, p0, Lava;->l:Lavm;

    if-nez v1, :cond_0

    .line 152
    new-instance v1, Lavm;

    iget-object v2, p0, Lava;->g:Lavi;

    invoke-direct {v1, v0, v2}, Lavm;-><init>(Lbrc;Lavn;)V

    iput-object v1, p0, Lava;->l:Lavm;

    .line 154
    iget-object v0, p0, Lava;->c:Lalg;

    const-class v1, Lavm;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    iget-object v1, p0, Lava;->l:Lavm;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-interface {v0, v1, v2}, Lalg;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 163
    invoke-virtual {p0}, Lava;->e()V

    .line 164
    iget-object v0, p0, Lava;->m:Lavh;

    sget-object v1, Lavh;->b:Lavh;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lava;->l:Lavm;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
