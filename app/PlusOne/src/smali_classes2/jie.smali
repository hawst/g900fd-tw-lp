.class public final Ljie;
.super Landroid/widget/BaseAdapter;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljig;

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljhw;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljig;)V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 58
    iput-object p1, p0, Ljie;->a:Landroid/content/Context;

    .line 59
    iput-object p2, p0, Ljie;->b:Ljig;

    .line 60
    return-void
.end method

.method static synthetic a(Ljie;)Ljig;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Ljie;->b:Ljig;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljhw;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 67
    iput-object p1, p0, Ljie;->c:Ljava/util/List;

    .line 68
    invoke-virtual {p0}, Ljie;->notifyDataSetChanged()V

    .line 69
    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Ljie;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljie;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Ljie;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 83
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 20

    .prologue
    .line 100
    move-object/from16 v0, p0

    iget-object v2, v0, Ljie;->c:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljhw;

    .line 103
    if-nez p2, :cond_0

    .line 104
    move-object/from16 v0, p0

    iget-object v3, v0, Ljie;->a:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f04011a

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v3, v4, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 107
    :cond_0
    invoke-virtual {v2}, Ljhw;->a()J

    move-result-wide v8

    .line 108
    invoke-virtual {v2}, Ljhw;->c()Ljava/lang/String;

    .line 109
    invoke-virtual {v2}, Ljhw;->d()Ljava/lang/String;

    move-result-object v10

    .line 110
    invoke-virtual {v2}, Ljhw;->b()I

    .line 111
    invoke-virtual {v2}, Ljhw;->e()I

    move-result v11

    .line 112
    invoke-virtual {v2}, Ljhw;->g()Ljava/lang/String;

    move-result-object v12

    .line 113
    invoke-virtual {v2}, Ljhw;->h()Z

    move-result v13

    .line 116
    const v2, 0x7f1001b1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 119
    const v3, 0x7f100395

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 120
    const v4, 0x7f100178

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    .line 121
    const v5, 0x7f100285

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 122
    const v5, 0x7f100397

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 123
    const v6, 0x7f100134

    move-object/from16 v0, p2

    invoke-virtual {v0, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 124
    const v7, 0x7f100396

    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ImageView;

    .line 126
    new-instance v14, Lhmk;

    sget-object v15, Lonb;->a:Lhmn;

    invoke-direct {v14, v15}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v4, v14}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 129
    move-object/from16 v0, p0

    iget-object v14, v0, Ljie;->a:Landroid/content/Context;

    const/16 v15, 0x1a

    invoke-static {v14, v3, v15}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 130
    move-object/from16 v0, p0

    iget-object v14, v0, Ljie;->a:Landroid/content/Context;

    const/16 v15, 0x13

    invoke-static {v14, v5, v15}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 132
    move-object/from16 v0, p0

    iget-object v14, v0, Ljie;->a:Landroid/content/Context;

    const/4 v15, 0x7

    invoke-static {v14, v6, v15}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 134
    const v14, 0x7f020513

    invoke-virtual {v2, v14}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 139
    const/16 v14, 0x8a

    invoke-virtual {v2, v14}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 144
    new-instance v14, Ljava/util/Date;

    invoke-direct {v14, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 145
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    invoke-virtual {v14}, Ljava/util/Date;->getTime()J

    move-result-wide v18

    sub-long v16, v16, v18

    const-wide/32 v18, 0x5265c00

    cmp-long v2, v16, v18

    if-gez v2, :cond_2

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Ljie;->a:Landroid/content/Context;

    invoke-static {v2}, Landroid/text/format/DateFormat;->getTimeFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    :goto_1
    invoke-virtual {v2, v14}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, " - "

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-static {v8}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v2

    .line 144
    invoke-virtual {v5, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 146
    invoke-virtual {v6, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 149
    packed-switch v11, :pswitch_data_0

    .line 169
    :goto_2
    :pswitch_0
    const/4 v2, 0x4

    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 176
    :goto_3
    if-eqz v13, :cond_4

    .line 177
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 178
    move-object/from16 v0, p0

    iget-object v2, v0, Ljie;->c:Ljava/util/List;

    move/from16 v0, p1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljhw;

    invoke-virtual {v2}, Ljhw;->f()J

    move-result-wide v2

    .line 179
    new-instance v5, Lhmi;

    new-instance v6, Ljif;

    move-object/from16 v0, p0

    invoke-direct {v6, v0, v2, v3}, Ljif;-><init>(Ljie;J)V

    invoke-direct {v5, v6}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    .line 180
    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    :goto_4
    return-object p2

    .line 145
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Ljie;->a:Landroid/content/Context;

    invoke-static {v2}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v2

    goto :goto_1

    .line 152
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Ljie;->a:Landroid/content/Context;

    const v5, 0x7f0a01a3

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 157
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Ljie;->a:Landroid/content/Context;

    const v5, 0x7f0a01a2

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 162
    :pswitch_3
    move-object/from16 v0, p0

    iget-object v2, v0, Ljie;->a:Landroid/content/Context;

    const v5, 0x7f0a01a4

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 163
    const v2, 0x7f020509

    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 164
    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 168
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v2, v0, Ljie;->a:Landroid/content/Context;

    const v5, 0x7f0a01a5

    invoke-virtual {v2, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 182
    :cond_4
    const/16 v2, 0x8

    invoke-virtual {v4, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4

    .line 149
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return v0
.end method
