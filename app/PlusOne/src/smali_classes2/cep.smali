.class final Lcep;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Iterator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Iterator",
        "<",
        "Ljej;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lceo;


# direct methods
.method constructor <init>(Lceo;)V
    .locals 0

    .prologue
    .line 34
    iput-object p1, p0, Lcep;->a:Lceo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljej;
    .locals 3

    .prologue
    .line 59
    iget-object v0, p0, Lcep;->a:Lceo;

    invoke-static {v0}, Lceo;->a(Lceo;)I

    move-result v0

    iget-object v1, p0, Lcep;->a:Lceo;

    invoke-static {v1}, Lceo;->b(Lceo;)[Lcfp;

    move-result-object v1

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 61
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 63
    :cond_0
    iget-object v0, p0, Lcep;->a:Lceo;

    iget-object v0, v0, Lceo;->a:Ljava/util/Iterator;

    if-nez v0, :cond_1

    .line 65
    iget-object v0, p0, Lcep;->a:Lceo;

    iget-object v1, p0, Lcep;->a:Lceo;

    invoke-static {v1}, Lceo;->b(Lceo;)[Lcfp;

    move-result-object v1

    iget-object v2, p0, Lcep;->a:Lceo;

    invoke-static {v2}, Lceo;->a(Lceo;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-interface {v1}, Lcfp;->iterator()Ljava/util/Iterator;

    move-result-object v1

    iput-object v1, v0, Lceo;->a:Ljava/util/Iterator;

    .line 67
    :cond_1
    iget-object v0, p0, Lcep;->a:Lceo;

    iget-object v0, v0, Lceo;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljej;

    .line 68
    iget-object v1, p0, Lcep;->a:Lceo;

    iget-object v1, v1, Lceo;->a:Ljava/util/Iterator;

    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    .line 69
    iget-object v1, p0, Lcep;->a:Lceo;

    invoke-static {v1}, Lceo;->b(Lceo;)[Lcfp;

    move-result-object v1

    iget-object v2, p0, Lcep;->a:Lceo;

    invoke-static {v2}, Lceo;->a(Lceo;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-interface {v1}, Lcfp;->close()V

    .line 70
    iget-object v1, p0, Lcep;->a:Lceo;

    const/4 v2, 0x0

    iput-object v2, v1, Lceo;->a:Ljava/util/Iterator;

    .line 71
    iget-object v1, p0, Lcep;->a:Lceo;

    invoke-static {v1}, Lceo;->c(Lceo;)I

    .line 73
    :cond_2
    return-object v0
.end method

.method public hasNext()Z
    .locals 3

    .prologue
    .line 38
    iget-object v0, p0, Lcep;->a:Lceo;

    invoke-static {v0}, Lceo;->a(Lceo;)I

    move-result v0

    iget-object v1, p0, Lcep;->a:Lceo;

    invoke-static {v1}, Lceo;->b(Lceo;)[Lcfp;

    move-result-object v1

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 40
    const/4 v0, 0x0

    .line 54
    :goto_0
    return v0

    .line 42
    :cond_0
    iget-object v0, p0, Lcep;->a:Lceo;

    iget-object v0, v0, Lceo;->a:Ljava/util/Iterator;

    if-nez v0, :cond_1

    .line 44
    iget-object v0, p0, Lcep;->a:Lceo;

    iget-object v1, p0, Lcep;->a:Lceo;

    invoke-static {v1}, Lceo;->b(Lceo;)[Lcfp;

    move-result-object v1

    iget-object v2, p0, Lcep;->a:Lceo;

    invoke-static {v2}, Lceo;->a(Lceo;)I

    move-result v2

    aget-object v1, v1, v2

    invoke-interface {v1}, Lcfp;->iterator()Ljava/util/Iterator;

    move-result-object v1

    iput-object v1, v0, Lceo;->a:Ljava/util/Iterator;

    .line 46
    :cond_1
    iget-object v0, p0, Lcep;->a:Lceo;

    iget-object v0, v0, Lceo;->a:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 48
    const/4 v0, 0x1

    goto :goto_0

    .line 51
    :cond_2
    iget-object v0, p0, Lcep;->a:Lceo;

    invoke-static {v0}, Lceo;->b(Lceo;)[Lcfp;

    move-result-object v0

    iget-object v1, p0, Lcep;->a:Lceo;

    invoke-static {v1}, Lceo;->a(Lceo;)I

    move-result v1

    aget-object v0, v0, v1

    invoke-interface {v0}, Lcfp;->close()V

    .line 52
    iget-object v0, p0, Lcep;->a:Lceo;

    const/4 v1, 0x0

    iput-object v1, v0, Lceo;->a:Ljava/util/Iterator;

    .line 53
    iget-object v0, p0, Lcep;->a:Lceo;

    invoke-static {v0}, Lceo;->c(Lceo;)I

    .line 54
    invoke-virtual {p0}, Lcep;->hasNext()Z

    move-result v0

    goto :goto_0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lcep;->a()Ljej;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
