.class public Lahr;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lahr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    return-void
.end method

.method private static a(Landroid/util/LongSparseArray;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Lahs;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v10, 0x0

    .line 106
    invoke-virtual {p0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 124
    :goto_0
    return-void

    .line 109
    :cond_0
    invoke-virtual {p0, v10}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v2

    .line 110
    const/4 v0, 0x1

    move v1, v0

    :goto_1
    invoke-virtual {p0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 111
    invoke-virtual {p0, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    .line 112
    invoke-virtual {p0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahs;

    .line 114
    sub-long v6, v4, v2

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_1

    .line 115
    const v6, 0x4700e800    # 33000.0f

    sub-long v2, v4, v2

    long-to-float v2, v2

    div-float v2, v6, v2

    .line 117
    iget v3, v0, Lahs;->a:F

    mul-float/2addr v3, v2

    iput v3, v0, Lahs;->a:F

    .line 118
    iget v3, v0, Lahs;->b:F

    mul-float/2addr v2, v3

    iput v2, v0, Lahs;->b:F

    .line 110
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move-wide v2, v4

    goto :goto_1

    .line 123
    :cond_2
    invoke-virtual {p0, v10}, Landroid/util/LongSparseArray;->removeAt(I)V

    goto :goto_0
.end method

.method public static a(Lbku;[J)[F
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 31
    .line 32
    new-instance v3, Landroid/util/LongSparseArray;

    invoke-direct {v3}, Landroid/util/LongSparseArray;-><init>()V

    array-length v4, p1

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_3

    aget-wide v6, p1, v1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {p0}, Lbkx;->b(Lbku;)Lbmz;

    move-result-object v0

    sget-object v6, Lbmz;->a:Lbmz;

    if-eq v0, v6, :cond_0

    sget-object v6, Lbmz;->r:Lbmz;

    if-ne v0, v6, :cond_2

    :cond_0
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {p0, v0, v6, v7}, Lbku;->a(Lbmz;J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxi;

    if-eqz v0, :cond_1

    new-instance v6, Lahs;

    invoke-virtual {v0}, Lbxi;->d()F

    move-result v7

    invoke-virtual {v0}, Lbxi;->e()F

    move-result v0

    invoke-direct {v6, v7, v0}, Lahs;-><init>(FF)V

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v3, v8, v9, v6}, Landroid/util/LongSparseArray;->append(JLjava/lang/Object;)V

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_1

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x14

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid field type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 33
    :cond_3
    invoke-virtual {v3}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x0

    :goto_1
    return-object v0

    :cond_4
    invoke-static {v3}, Lahr;->a(Landroid/util/LongSparseArray;)V

    invoke-static {v3}, Lahr;->b(Landroid/util/LongSparseArray;)Landroid/util/LongSparseArray;

    move-result-object v0

    invoke-static {v0}, Lahr;->a(Landroid/util/LongSparseArray;)V

    invoke-static {v3}, Lahr;->c(Landroid/util/LongSparseArray;)[F

    move-result-object v1

    invoke-static {v0}, Lahr;->c(Landroid/util/LongSparseArray;)[F

    move-result-object v3

    array-length v0, v1

    array-length v4, v3

    add-int/2addr v0, v4

    new-array v0, v0, [F

    array-length v4, v1

    invoke-static {v1, v2, v0, v2, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v1, v1

    array-length v4, v3

    invoke-static {v3, v2, v0, v1, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_1
.end method

.method private static b(Landroid/util/LongSparseArray;)Landroid/util/LongSparseArray;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Lahs;",
            ">;)",
            "Landroid/util/LongSparseArray",
            "<",
            "Lahs;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 128
    new-instance v3, Landroid/util/LongSparseArray;

    invoke-direct {v3}, Landroid/util/LongSparseArray;-><init>()V

    .line 130
    invoke-virtual {p0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 131
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahs;

    move-object v2, v0

    .line 132
    :goto_0
    invoke-virtual {p0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 133
    invoke-virtual {p0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahs;

    .line 134
    invoke-virtual {p0, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    .line 135
    new-instance v6, Lahs;

    iget v7, v0, Lahs;->a:F

    iget v8, v2, Lahs;->a:F

    sub-float/2addr v7, v8

    iget v8, v0, Lahs;->b:F

    iget v2, v2, Lahs;->b:F

    sub-float v2, v8, v2

    invoke-direct {v6, v7, v2}, Lahs;-><init>(FF)V

    .line 137
    invoke-virtual {v3, v4, v5, v6}, Landroid/util/LongSparseArray;->append(JLjava/lang/Object;)V

    .line 132
    add-int/lit8 v1, v1, 0x1

    move-object v2, v0

    goto :goto_0

    .line 141
    :cond_0
    return-object v3
.end method

.method private static c(Landroid/util/LongSparseArray;)[F
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Lahs;",
            ">;)[F"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 162
    .line 168
    invoke-virtual {p0}, Landroid/util/LongSparseArray;->size()I

    move-result v8

    .line 169
    if-lez v8, :cond_1

    move v1, v2

    move v3, v0

    move v4, v0

    move v5, v0

    move v6, v0

    move v7, v0

    .line 170
    :goto_0
    invoke-virtual {p0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 171
    invoke-virtual {p0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahs;

    .line 172
    iget v9, v0, Lahs;->a:F

    add-float/2addr v7, v9

    .line 173
    iget v9, v0, Lahs;->b:F

    add-float/2addr v6, v9

    .line 174
    iget v9, v0, Lahs;->a:F

    iget v10, v0, Lahs;->a:F

    mul-float/2addr v9, v10

    add-float/2addr v5, v9

    .line 175
    iget v9, v0, Lahs;->b:F

    iget v10, v0, Lahs;->b:F

    mul-float/2addr v9, v10

    add-float/2addr v4, v9

    .line 176
    iget v9, v0, Lahs;->a:F

    iget v0, v0, Lahs;->b:F

    mul-float/2addr v0, v9

    add-float/2addr v3, v0

    .line 170
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 178
    :cond_0
    int-to-float v0, v8

    div-float/2addr v7, v0

    .line 179
    int-to-float v0, v8

    div-float/2addr v6, v0

    .line 180
    int-to-float v0, v8

    div-float v0, v5, v0

    mul-float v1, v7, v7

    sub-float v5, v0, v1

    .line 181
    int-to-float v0, v8

    div-float v0, v4, v0

    mul-float v1, v6, v6

    sub-float v1, v0, v1

    .line 182
    int-to-float v0, v8

    div-float v0, v3, v0

    mul-float v3, v7, v6

    sub-float/2addr v0, v3

    move v3, v5

    move v4, v6

    move v5, v7

    .line 185
    :goto_1
    const/4 v6, 0x6

    new-array v6, v6, [F

    .line 186
    aput v5, v6, v2

    .line 187
    const/4 v2, 0x1

    aput v4, v6, v2

    .line 188
    const/4 v2, 0x2

    aput v3, v6, v2

    .line 189
    const/4 v2, 0x3

    aput v1, v6, v2

    .line 190
    const/4 v1, 0x4

    aput v0, v6, v1

    .line 191
    const/4 v0, 0x5

    int-to-float v1, v8

    aput v1, v6, v0

    .line 192
    return-object v6

    :cond_1
    move v1, v0

    move v3, v0

    move v4, v0

    move v5, v0

    goto :goto_1
.end method
