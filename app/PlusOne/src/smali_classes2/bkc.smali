.class public final Lbkc;
.super Lbji;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbji",
        "<",
        "Lbhl;",
        ">;"
    }
.end annotation


# instance fields
.field private final e:Lbhl;

.field private final f:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<[B>;"
        }
    .end annotation
.end field

.field private final g:Landroid/graphics/SurfaceTexture;

.field private volatile h:Z

.field private i:Z


# direct methods
.method private constructor <init>(Lbjs;Lbhb;Ljava/util/concurrent/Executor;)V
    .locals 2

    .prologue
    .line 90
    invoke-direct {p0, p1, p3}, Lbji;-><init>(Lbjs;Ljava/util/concurrent/Executor;)V

    .line 92
    const-string v0, "outputTexture"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 93
    new-instance v0, Lbhl;

    invoke-direct {v0, p2}, Lbhl;-><init>(Lbhb;)V

    iput-object v0, p0, Lbkc;->e:Lbhl;

    .line 94
    new-instance v0, Landroid/graphics/SurfaceTexture;

    invoke-interface {p2}, Lbhb;->c()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, Lbkc;->g:Landroid/graphics/SurfaceTexture;

    .line 95
    iget-object v0, p0, Lbkc;->g:Landroid/graphics/SurfaceTexture;

    new-instance v1, Lbkd;

    invoke-direct {v1, p0}, Lbkd;-><init>(Lbkc;)V

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 103
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbkc;->f:Ljava/util/Queue;

    .line 104
    return-void
.end method

.method public static a(Lbjs;Lbhb;Landroid/media/MediaFormat;Ljava/util/concurrent/Executor;)Lbkc;
    .locals 4

    .prologue
    const/4 v3, 0x6

    const/4 v2, 0x0

    .line 44
    const-string v0, "codec"

    invoke-static {p0, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 45
    const-string v0, "inputFormat"

    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 46
    invoke-interface {p0}, Lbjs;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "adaptive codec requires SDK >= 19"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 48
    invoke-static {v3}, Landroid/media/CamcorderProfile;->hasProfile(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v3}, Landroid/media/CamcorderProfile;->get(I)Landroid/media/CamcorderProfile;

    move-result-object v0

    iget v1, v0, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    iget v0, v0, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    :goto_1
    const-string v1, "max-width"

    invoke-virtual {p2, v1, v0}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    const-string v1, "max-height"

    invoke-virtual {p2, v1, v0}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 54
    :cond_0
    :try_start_0
    new-instance v3, Lbkc;

    invoke-direct {v3, p0, p1, p3}, Lbkc;-><init>(Lbjs;Lbhb;Ljava/util/concurrent/Executor;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 55
    :try_start_1
    new-instance v1, Landroid/view/Surface;

    iget-object v0, v3, Lbkc;->g:Landroid/graphics/SurfaceTexture;

    invoke-direct {v1, v0}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 56
    :try_start_2
    invoke-interface {p0, p2, v1}, Lbjs;->a(Landroid/media/MediaFormat;Landroid/view/Surface;)V
    :try_end_2
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 62
    invoke-static {v1}, Lcgl;->a(Landroid/view/Surface;)V

    return-object v3

    .line 47
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 48
    :cond_2
    const/16 v0, 0x780

    goto :goto_1

    .line 58
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 59
    :goto_2
    :try_start_3
    invoke-static {v2}, Lcgl;->a(Lcgk;)V

    .line 60
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 62
    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_3
    invoke-static {v2}, Lcgl;->a(Landroid/view/Surface;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    .line 58
    :catch_1
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v2, v3

    goto :goto_2
.end method

.method static synthetic a(Lbkc;Z)Z
    .locals 0

    .prologue
    .line 36
    iput-boolean p1, p0, Lbkc;->h:Z

    return p1
.end method


# virtual methods
.method protected bridge synthetic a(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;Z)Lbhj;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0, p2, p3}, Lbkc;->a(Landroid/media/MediaCodec$BufferInfo;Z)Lbhl;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/media/MediaCodec$BufferInfo;Z)Lbhl;
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 143
    iget-boolean v1, p0, Lbkc;->i:Z

    if-eqz v1, :cond_2

    .line 144
    iget-boolean v1, p0, Lbkc;->h:Z

    if-nez v1, :cond_1

    .line 167
    :cond_0
    :goto_0
    return-object v0

    .line 148
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbkc;->i:Z

    .line 149
    iget-object v0, p0, Lbkc;->g:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 150
    iget-object v0, p0, Lbkc;->e:Lbhl;

    iget-wide v2, p1, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iput-wide v2, v0, Lbhl;->a:J

    .line 151
    iget-object v0, p0, Lbkc;->e:Lbhl;

    iput-boolean p2, v0, Lbhl;->b:Z

    .line 156
    iget-object v0, p0, Lbkc;->e:Lbhl;

    iget-object v0, v0, Lbhl;->d:[F

    invoke-static {v0}, Lbqu;->b([F)V

    .line 157
    const/16 v0, -0xff

    iput v0, p0, Lbji;->d:I

    .line 158
    iget-object v0, p0, Lbkc;->e:Lbhl;

    goto :goto_0

    .line 161
    :cond_2
    iget-boolean v1, p0, Lbkc;->h:Z

    if-nez v1, :cond_0

    .line 163
    invoke-virtual {p0, v2}, Lbkc;->a(Z)V

    .line 164
    iput-boolean v2, p0, Lbkc;->i:Z

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 191
    invoke-super {p0}, Lbji;->a()V

    .line 193
    iget-object v0, p0, Lbkc;->g:Landroid/graphics/SurfaceTexture;

    invoke-static {v0}, Lcgl;->a(Landroid/graphics/SurfaceTexture;)V

    .line 194
    iget-object v0, p0, Lbkc;->e:Lbhl;

    iget-object v0, v0, Lbhl;->c:Lbhb;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 195
    return-void
.end method

.method public a(Lbhl;)V
    .locals 3

    .prologue
    .line 172
    const-string v0, "videoFrame"

    iget-object v1, p0, Lbkc;->e:Lbhl;

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/lang/CharSequence;)V

    .line 174
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbkc;->h:Z

    .line 175
    return-void
.end method

.method public a(Lbiy;)V
    .locals 6

    .prologue
    const/16 v5, 0xf

    const/4 v1, 0x0

    .line 108
    const-string v0, "source"

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 109
    iget-object v0, p0, Lbkc;->b:Landroid/media/MediaFormat;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbkc;->b:Landroid/media/MediaFormat;

    .line 110
    invoke-interface {p1}, Lbiy;->e()Landroid/media/MediaFormat;

    move-result-object v2

    invoke-static {v0, v2}, Lbkf;->a(Landroid/media/MediaFormat;Landroid/media/MediaFormat;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 111
    :goto_0
    invoke-super {p0, p1}, Lbji;->a(Lbiy;)V

    .line 114
    if-eqz v0, :cond_3

    .line 119
    :cond_1
    return-void

    :cond_2
    move v0, v1

    .line 110
    goto :goto_0

    .line 117
    :cond_3
    iget-object v0, p0, Lbkc;->a:Lbjs;

    invoke-interface {v0}, Lbjs;->b()Z

    move-result v0

    const-string v2, "non-adaptive format change"

    invoke-static {v0, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 118
    invoke-interface {p1}, Lbiy;->e()Landroid/media/MediaFormat;

    move-result-object v2

    iget-object v0, p0, Lbkc;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    move v0, v1

    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "csd-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "csd-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/media/MediaFormat;->getByteBuffer(Ljava/lang/String;)Ljava/nio/ByteBuffer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->limit()I

    move-result v4

    new-array v4, v4, [B

    invoke-virtual {v3, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    invoke-virtual {v3, v4}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    iget-object v3, p0, Lbkc;->f:Ljava/util/Queue;

    invoke-interface {v3, v4}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 35
    check-cast p1, Lbhl;

    invoke-virtual {p0, p1}, Lbkc;->a(Lbhl;)V

    return-void
.end method

.method public synthetic b()Lbhj;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lbkc;->e()Lbhl;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 35
    invoke-virtual {p0}, Lbkc;->e()Lbhl;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 179
    invoke-super {p0}, Lbji;->d()V

    .line 182
    iget-boolean v0, p0, Lbkc;->h:Z

    if-eqz v0, :cond_0

    .line 183
    iput-boolean v1, p0, Lbkc;->i:Z

    .line 184
    iput-boolean v1, p0, Lbkc;->h:Z

    .line 185
    iget-object v0, p0, Lbkc;->g:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 187
    :cond_0
    return-void
.end method

.method public e()Lbhl;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 124
    :goto_0
    iget-object v0, p0, Lbkc;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 125
    iget-object v0, p0, Lbkc;->a:Lbjs;

    invoke-interface {v0}, Lbjs;->f()I

    move-result v1

    .line 126
    const/4 v0, -0x1

    if-eq v1, v0, :cond_0

    .line 127
    iget-object v0, p0, Lbkc;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, [B

    .line 128
    iget-object v0, p0, Lbkc;->a:Lbjs;

    invoke-interface {v0}, Lbjs;->g()[Ljava/nio/ByteBuffer;

    move-result-object v0

    aget-object v0, v0, v1

    .line 129
    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 130
    iget-object v0, p0, Lbkc;->a:Lbjs;

    array-length v3, v3

    const-wide/16 v4, 0x0

    move v6, v2

    invoke-interface/range {v0 .. v6}, Lbjs;->a(IIIJI)V

    goto :goto_0

    .line 132
    :cond_0
    const/4 v0, 0x0

    .line 134
    :goto_1
    return-object v0

    :cond_1
    invoke-super {p0}, Lbji;->b()Lbhj;

    move-result-object v0

    check-cast v0, Lbhl;

    goto :goto_1
.end method
