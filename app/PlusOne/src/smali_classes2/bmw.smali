.class public final Lbmw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbml;


# instance fields
.field private final a:Ljeg;

.field private final b:Lbmx;

.field private final c:Lbol;

.field private final d:Lbmy;

.field private final e:Z


# direct methods
.method public constructor <init>(Ljeg;Lbmx;Lbkr;Z)V
    .locals 7

    .prologue
    const-wide/16 v3, 0x0

    const/4 v5, 0x0

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-string v0, "mediaIdentifier"

    invoke-static {p1, v0, v5}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljeg;

    iput-object v0, p0, Lbmw;->a:Ljeg;

    .line 29
    const-string v0, "metadata"

    invoke-static {p2, v0, v5}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmx;

    iput-object v0, p0, Lbmw;->b:Lbmx;

    .line 30
    invoke-static {p3}, Lbol;->a(Lbkr;)Lbol;

    move-result-object v0

    iput-object v0, p0, Lbmw;->c:Lbol;

    .line 31
    invoke-static {p3}, Lbmy;->a(Lbkr;)Lbmy;

    move-result-object v0

    iput-object v0, p0, Lbmw;->d:Lbmy;

    .line 34
    invoke-interface {p3}, Lbkr;->c()Lbkn;

    move-result-object v0

    const-string v1, "generalMetrics"

    invoke-static {v0, v1, v5}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lbkn;

    .line 36
    invoke-interface {p3}, Lbkr;->d()Lbku;

    move-result-object v0

    const-string v1, "pointMetrics"

    invoke-static {v0, v1, v5}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbku;

    .line 38
    invoke-virtual {v0}, Lbku;->d()Lbms;

    move-result-object v0

    invoke-virtual {v0}, Lbms;->a()Z

    move-result v0

    const-string v1, "pointMetrics.getKeyFrameMetrics().isEmpty()"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 40
    invoke-virtual {v6}, Lbkn;->c()J

    move-result-wide v0

    const-string v2, "durationUs"

    invoke-static/range {v0 .. v5}, Lcec;->a(JLjava/lang/CharSequence;JLjava/lang/CharSequence;)V

    .line 43
    invoke-virtual {v6}, Lbkn;->a()J

    move-result-wide v0

    const-string v2, "finalFrameTimestampUs"

    invoke-static/range {v0 .. v5}, Lcec;->a(JLjava/lang/CharSequence;JLjava/lang/CharSequence;)V

    .line 46
    iput-boolean p4, p0, Lbmw;->e:Z

    .line 47
    return-void
.end method


# virtual methods
.method public a()Ljeg;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lbmw;->a:Ljeg;

    return-object v0
.end method

.method public synthetic b()Lbmu;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Lbmw;->g()Lbmx;

    move-result-object v0

    return-object v0
.end method

.method public c()Lbol;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lbmw;->c:Lbol;

    return-object v0
.end method

.method public d()Lbmy;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lbmw;->d:Lbmy;

    return-object v0
.end method

.method public e()J
    .locals 2

    .prologue
    .line 76
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lbmw;->e:Z

    return v0
.end method

.method public g()Lbmx;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lbmw;->b:Lbmx;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 92
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lbmw;->a:Ljeg;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lbmw;->b:Lbmx;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-boolean v3, p0, Lbmw;->e:Z

    .line 95
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    .line 92
    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
