.class public final Lazs;
.super Laye;
.source "PG"


# instance fields
.field private a:Landroid/graphics/Matrix;

.field private b:Landroid/graphics/Matrix;

.field private c:Landroid/graphics/Matrix;

.field private d:Landroid/graphics/Matrix;

.field private e:Landroid/graphics/Matrix;

.field private f:Landroid/animation/TimeInterpolator;


# direct methods
.method public constructor <init>(Layj;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0, p1}, Laye;-><init>(Layj;)V

    .line 29
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lazs;->a:Landroid/graphics/Matrix;

    .line 30
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lazs;->b:Landroid/graphics/Matrix;

    .line 31
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lazs;->c:Landroid/graphics/Matrix;

    .line 32
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lazs;->d:Landroid/graphics/Matrix;

    .line 33
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lazs;->e:Landroid/graphics/Matrix;

    .line 34
    new-instance v0, Landroid/view/animation/AnticipateOvershootInterpolator;

    const v1, 0x3f99999a    # 1.2f

    invoke-direct {v0, v1}, Landroid/view/animation/AnticipateOvershootInterpolator;-><init>(F)V

    iput-object v0, p0, Lazs;->f:Landroid/animation/TimeInterpolator;

    .line 38
    return-void
.end method

.method static a(Lbmj;FF)Lbmj;
    .locals 12

    .prologue
    const/high16 v10, 0x3f000000    # 0.5f

    const/4 v5, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    const/high16 v8, 0x3f800000    # 1.0f

    .line 121
    iget v6, p0, Lbmj;->a:F

    .line 122
    iget v2, p0, Lbmj;->b:F

    .line 123
    iget v1, p0, Lbmj;->c:F

    .line 124
    iget v0, p0, Lbmj;->d:F

    .line 125
    cmpl-float v3, p1, p2

    if-lez v3, :cond_2

    .line 128
    sub-float v3, v8, v6

    sub-float/2addr v3, v2

    .line 129
    div-float v4, p1, p2

    sub-float/2addr v4, v8

    mul-float v7, v4, v3

    .line 130
    cmpg-float v3, v6, v2

    if-gez v3, :cond_1

    .line 131
    div-float v3, v7, v9

    invoke-static {v3, v6}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 132
    sub-float v4, v6, v3

    .line 133
    sub-float v6, v7, v3

    .line 134
    invoke-static {v6, v2}, Ljava/lang/Math;->min(FF)F

    move-result v7

    .line 135
    sub-float v3, v2, v7

    .line 136
    sub-float v2, v6, v7

    move v11, v2

    move v2, v3

    move v3, v4

    move v4, v11

    .line 145
    :goto_0
    cmpl-float v6, v4, v5

    if-lez v6, :cond_0

    .line 148
    sub-float v6, v8, v1

    sub-float/2addr v6, v0

    .line 150
    mul-float/2addr v6, v10

    mul-float/2addr v6, v4

    add-float/2addr v4, v8

    div-float v4, v6, v4

    .line 152
    add-float/2addr v1, v4

    .line 153
    add-float/2addr v0, v4

    :cond_0
    move v4, v0

    move v11, v1

    move v1, v3

    move v3, v11

    .line 186
    :goto_1
    new-instance v0, Lbmj;

    invoke-direct/range {v0 .. v5}, Lbmj;-><init>(FFFFF)V

    return-object v0

    .line 138
    :cond_1
    div-float v3, v7, v9

    invoke-static {v3, v2}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 139
    sub-float v3, v2, v4

    .line 140
    sub-float v2, v7, v4

    .line 141
    invoke-static {v2, v6}, Ljava/lang/Math;->min(FF)F

    move-result v7

    .line 142
    sub-float v4, v6, v7

    .line 143
    sub-float/2addr v2, v7

    move v11, v2

    move v2, v3

    move v3, v4

    move v4, v11

    goto :goto_0

    .line 158
    :cond_2
    sub-float v3, v8, v1

    sub-float/2addr v3, v0

    .line 159
    div-float v4, p2, p1

    sub-float/2addr v4, v8

    mul-float v7, v4, v3

    .line 160
    cmpg-float v3, v1, v0

    if-gez v3, :cond_3

    .line 161
    div-float v3, v7, v9

    invoke-static {v1, v3}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 162
    sub-float v3, v1, v4

    .line 163
    sub-float v1, v7, v4

    .line 164
    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v7

    .line 165
    sub-float v4, v0, v7

    .line 166
    sub-float v0, v1, v7

    .line 175
    :goto_2
    cmpl-float v1, v0, v5

    if-lez v1, :cond_4

    .line 178
    sub-float v1, v8, v6

    sub-float/2addr v1, v2

    .line 180
    mul-float/2addr v1, v10

    mul-float/2addr v1, v0

    add-float/2addr v0, v8

    div-float v0, v1, v0

    .line 182
    add-float v1, v6, v0

    .line 183
    add-float/2addr v2, v0

    goto :goto_1

    .line 168
    :cond_3
    div-float v3, v7, v9

    invoke-static {v0, v3}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 169
    sub-float v4, v0, v3

    .line 170
    sub-float v0, v7, v3

    .line 171
    invoke-static {v1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v7

    .line 172
    sub-float v3, v1, v7

    .line 173
    sub-float/2addr v0, v7

    goto :goto_2

    :cond_4
    move v1, v6

    goto :goto_1
.end method

.method private a(Landroid/graphics/Matrix;Lbmj;)V
    .locals 7

    .prologue
    const v6, 0x3f705dcd

    const v5, 0x3f343058

    const/high16 v4, 0x3f800000    # 1.0f

    .line 193
    invoke-static {p2, v5, v6}, Lazs;->a(Lbmj;FF)Lbmj;

    move-result-object v0

    .line 203
    iget v1, v0, Lbmj;->a:F

    sub-float v1, v4, v1

    iget v2, v0, Lbmj;->b:F

    sub-float/2addr v1, v2

    div-float v1, v4, v1

    .line 204
    iget v2, v0, Lbmj;->c:F

    sub-float v2, v4, v2

    iget v3, v0, Lbmj;->d:F

    sub-float/2addr v2, v3

    div-float v2, v4, v2

    .line 206
    iget v3, v0, Lbmj;->a:F

    .line 207
    iget v0, v0, Lbmj;->d:F

    .line 212
    neg-float v3, v3

    neg-float v0, v0

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 215
    mul-float v0, v5, v1

    mul-float v1, v6, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 217
    const v0, 0x3e156cc6

    const v1, 0x3ce11fa8

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 220
    invoke-virtual {p1, p1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 221
    return-void
.end method

.method private b(Landroid/graphics/Matrix;Lbmj;)V
    .locals 2

    .prologue
    .line 224
    const v0, 0x3f343058

    const v1, 0x3f705dcd

    invoke-static {p2, v0, v1}, Lazs;->a(Lbmj;FF)Lbmj;

    move-result-object v0

    .line 225
    invoke-virtual {p1}, Landroid/graphics/Matrix;->reset()V

    .line 226
    invoke-virtual {v0, p1}, Lbmj;->a(Landroid/graphics/Matrix;)V

    .line 227
    return-void
.end method


# virtual methods
.method public a(Layl;Lbof;)Landroid/graphics/Matrix;
    .locals 4

    .prologue
    .line 70
    iget-object v0, p0, Lazs;->b:Landroid/graphics/Matrix;

    iget-object v1, p1, Layl;->s:Lbmj;

    invoke-direct {p0, v0, v1}, Lazs;->a(Landroid/graphics/Matrix;Lbmj;)V

    .line 73
    invoke-static {p1}, Lbag;->a(Layl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lazs;->b:Landroid/graphics/Matrix;

    const/4 v1, 0x0

    iget-object v2, p0, Lazs;->f:Landroid/animation/TimeInterpolator;

    iget v3, p1, Layl;->m:F

    .line 75
    invoke-interface {v2, v3}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v2

    .line 74
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    .line 78
    :cond_0
    iget-object v0, p0, Lazs;->b:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public b(Layl;)Landroid/graphics/Matrix;
    .locals 4

    .prologue
    .line 89
    iget-object v0, p0, Lazs;->c:Landroid/graphics/Matrix;

    iget-object v1, p1, Layl;->t:Lbmj;

    invoke-direct {p0, v0, v1}, Lazs;->a(Landroid/graphics/Matrix;Lbmj;)V

    .line 92
    invoke-static {p1}, Lbag;->a(Layl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lazs;->c:Landroid/graphics/Matrix;

    const/4 v1, 0x0

    iget-object v2, p0, Lazs;->f:Landroid/animation/TimeInterpolator;

    iget v3, p1, Layl;->m:F

    .line 94
    invoke-interface {v2, v3}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float/2addr v2, v3

    .line 93
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    .line 97
    :cond_0
    iget-object v0, p0, Lazs;->c:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public f(Layl;)I
    .locals 1

    .prologue
    .line 42
    const v0, 0x7f020115

    return v0
.end method

.method public g(Layl;)Landroid/graphics/Matrix;
    .locals 4

    .prologue
    .line 58
    iget-object v0, p0, Lazs;->a:Landroid/graphics/Matrix;

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 60
    invoke-static {p1}, Lbag;->a(Layl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lazs;->a:Landroid/graphics/Matrix;

    const/4 v1, 0x0

    iget-object v2, p0, Lazs;->f:Landroid/animation/TimeInterpolator;

    iget v3, p1, Layl;->m:F

    .line 63
    invoke-interface {v2, v3}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    .line 62
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 65
    :cond_0
    iget-object v0, p0, Lazs;->a:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public h(Layl;)F
    .locals 1

    .prologue
    .line 52
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public i(Layl;)I
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x4

    return v0
.end method

.method public w(Layl;)F
    .locals 1

    .prologue
    .line 108
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public x(Layl;)Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lazs;->d:Landroid/graphics/Matrix;

    iget-object v1, p1, Layl;->s:Lbmj;

    invoke-direct {p0, v0, v1}, Lazs;->b(Landroid/graphics/Matrix;Lbmj;)V

    .line 84
    iget-object v0, p0, Lazs;->d:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public y(Layl;)Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lazs;->e:Landroid/graphics/Matrix;

    iget-object v1, p1, Layl;->t:Lbmj;

    invoke-direct {p0, v0, v1}, Lazs;->b(Landroid/graphics/Matrix;Lbmj;)V

    .line 103
    iget-object v0, p0, Lazs;->e:Landroid/graphics/Matrix;

    return-object v0
.end method
