.class public final Lbjd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbhr;
.implements Lbhw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lbhj;",
        ">",
        "Ljava/lang/Object;",
        "Lbhr",
        "<TT;>;",
        "Lbhw",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lbjp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjp",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:Lbiy;

.field private c:Lbji;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbji",
            "<TT;>;"
        }
    .end annotation
.end field

.field private d:J


# direct methods
.method public constructor <init>(Lbjp;Lbiy;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbjp",
            "<TT;>;",
            "Lbiy;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbjd;->d:J

    .line 29
    const-string v0, "decoderPool"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjp;

    iput-object v0, p0, Lbjd;->a:Lbjp;

    .line 30
    const-string v0, "source"

    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbiy;

    iput-object v0, p0, Lbjd;->b:Lbiy;

    .line 31
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lbjd;->a:Lbjp;

    iget-object v1, p0, Lbjd;->c:Lbji;

    invoke-virtual {v0, v1}, Lbjp;->a(Lbji;)V

    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lbjd;->c:Lbji;

    .line 65
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbjd;->d:J

    .line 66
    return-void
.end method

.method public a(Lbhj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 54
    iget-boolean v0, p1, Lbhj;->b:Z

    .line 55
    iget-object v1, p0, Lbjd;->c:Lbji;

    invoke-virtual {v1, p1}, Lbji;->a(Ljava/lang/Object;)V

    .line 56
    if-eqz v0, :cond_0

    .line 57
    invoke-virtual {p0}, Lbjd;->a()V

    .line 59
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lbhj;

    invoke-virtual {p0, p1}, Lbjd;->a(Lbhj;)V

    return-void
.end method

.method public b()J
    .locals 2

    .prologue
    .line 72
    iget-wide v0, p0, Lbjd;->d:J

    return-wide v0
.end method

.method public synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    invoke-virtual {p0}, Lbjd;->d()Lbhj;

    move-result-object v0

    return-object v0
.end method

.method public d()Lbhj;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 36
    iget-object v0, p0, Lbjd;->c:Lbji;

    if-nez v0, :cond_1

    .line 37
    iget-object v0, p0, Lbjd;->a:Lbjp;

    iget-object v1, p0, Lbjd;->b:Lbiy;

    invoke-virtual {v0, v1}, Lbjp;->a(Lbiy;)Lbji;

    move-result-object v0

    iput-object v0, p0, Lbjd;->c:Lbji;

    .line 40
    iget-object v0, p0, Lbjd;->c:Lbji;

    if-nez v0, :cond_1

    .line 41
    const/4 v0, 0x0

    .line 49
    :cond_0
    :goto_0
    return-object v0

    .line 45
    :cond_1
    iget-object v0, p0, Lbjd;->c:Lbji;

    invoke-virtual {v0}, Lbji;->b()Lbhj;

    move-result-object v0

    .line 46
    if-eqz v0, :cond_0

    .line 47
    iget-wide v2, v0, Lbhj;->a:J

    iput-wide v2, p0, Lbjd;->d:J

    goto :goto_0
.end method
