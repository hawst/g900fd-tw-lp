.class public final Ljyw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Ljyw;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Lizu;

.field private b:Ljava/lang/String;

.field private c:Ljyy;

.field private d:Lizu;

.field private final e:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 205
    new-instance v0, Ljyx;

    invoke-direct {v0}, Ljyx;-><init>()V

    sput-object v0, Ljyw;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ljyw;->e:Ljava/lang/Object;

    .line 47
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 191
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ljyw;->e:Ljava/lang/Object;

    .line 192
    const-class v0, Lizu;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lizu;

    iput-object v0, p0, Ljyw;->a:Lizu;

    .line 193
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljyw;->b:Ljava/lang/String;

    .line 194
    const-class v0, Lizu;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lizu;

    iput-object v0, p0, Ljyw;->d:Lizu;

    .line 195
    return-void
.end method


# virtual methods
.method public a()Lizu;
    .locals 2

    .prologue
    .line 58
    iget-object v1, p0, Ljyw;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 59
    :try_start_0
    iget-object v0, p0, Ljyw;->a:Lizu;

    monitor-exit v1

    return-object v0

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Landroid/content/Context;Lizu;)V
    .locals 2

    .prologue
    .line 78
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0, p1, p2, v0, v1}, Ljyw;->a(Landroid/content/Context;Lizu;Lizu;Z)V

    .line 79
    return-void
.end method

.method public a(Landroid/content/Context;Lizu;Lizu;Z)V
    .locals 6

    .prologue
    .line 88
    iget-object v1, p0, Ljyw;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 89
    :try_start_0
    iget-object v0, p0, Ljyw;->a:Lizu;

    invoke-static {p2, v0}, Llsh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 90
    monitor-exit v1

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 92
    :cond_1
    iput-object p2, p0, Ljyw;->a:Lizu;

    .line 93
    iput-object p3, p0, Ljyw;->d:Lizu;

    .line 94
    if-eqz p2, :cond_2

    invoke-virtual {p2}, Lizu;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    .line 95
    const-class v0, Lkjj;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkjj;

    const-class v2, Ljzz;

    .line 96
    invoke-interface {v0, v2}, Lkjj;->a(Ljava/lang/Class;)Lkji;

    move-result-object v0

    check-cast v0, Ljzz;

    invoke-virtual {v0}, Ljzz;->k()Ljye;

    move-result-object v0

    .line 97
    invoke-virtual {v0, p2}, Ljye;->a(Lizu;)V

    .line 99
    :cond_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 100
    iget-object v0, p0, Ljyw;->c:Ljyy;

    if-eqz v0, :cond_0

    if-eqz p4, :cond_0

    .line 101
    iget-object v0, p0, Ljyw;->c:Ljyy;

    invoke-interface {v0}, Ljyy;->m()V

    goto :goto_0

    .line 99
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Ljyw;->b:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    iput-object p1, p0, Ljyw;->b:Ljava/lang/String;

    .line 120
    iget-object v0, p0, Ljyw;->c:Ljyy;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Ljyw;->c:Ljyy;

    invoke-interface {v0}, Ljyy;->n()V

    goto :goto_0
.end method

.method public a(Ljyy;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Ljyw;->c:Ljyy;

    .line 51
    return-void
.end method

.method a(Z)Z
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Ljyw;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Ljyw;->a()Lizu;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lizu;
    .locals 2

    .prologue
    .line 68
    iget-object v1, p0, Ljyw;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 69
    :try_start_0
    iget-object v0, p0, Ljyw;->d:Lizu;

    if-nez v0, :cond_0

    iget-object v0, p0, Ljyw;->a:Lizu;

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    iget-object v0, p0, Ljyw;->d:Lizu;

    goto :goto_0

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Ljyw;->b:Ljava/lang/String;

    return-object v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Ljyw;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljyw;->a()Lizu;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 199
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 163
    instance-of v2, p1, Ljyw;

    if-nez v2, :cond_0

    .line 173
    :goto_0
    return v0

    .line 166
    :cond_0
    if-ne p1, p0, :cond_1

    move v0, v1

    .line 167
    goto :goto_0

    .line 169
    :cond_1
    iget-object v2, p0, Ljyw;->e:Ljava/lang/Object;

    monitor-enter v2

    .line 170
    :try_start_0
    check-cast p1, Ljyw;

    .line 171
    iget-object v3, p0, Ljyw;->b:Ljava/lang/String;

    iget-object v4, p1, Ljyw;->b:Ljava/lang/String;

    invoke-static {v3, v4}, Llsh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Ljyw;->a:Lizu;

    iget-object v4, p1, Ljyw;->a:Lizu;

    .line 172
    invoke-static {v3, v4}, Llsh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Ljyw;->d:Lizu;

    iget-object v4, p1, Ljyw;->d:Lizu;

    .line 173
    invoke-static {v3, v4}, Llsh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    :cond_2
    monitor-exit v2

    goto :goto_0

    .line 174
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 148
    iget-object v2, p0, Ljyw;->e:Ljava/lang/Object;

    monitor-enter v2

    .line 149
    :try_start_0
    iget-object v1, p0, Ljyw;->a:Lizu;

    if-nez v1, :cond_0

    move v1, v0

    :goto_0
    add-int/lit8 v1, v1, 0x1f

    .line 151
    mul-int/lit8 v3, v1, 0x1f

    iget-object v1, p0, Ljyw;->b:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v0

    :goto_1
    add-int/2addr v1, v3

    .line 152
    mul-int/lit8 v1, v1, 0x1f

    iget-object v3, p0, Ljyw;->d:Lizu;

    if-nez v3, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 153
    monitor-exit v2

    return v0

    .line 149
    :cond_0
    iget-object v1, p0, Ljyw;->a:Lizu;

    invoke-virtual {v1}, Lizu;->hashCode()I

    move-result v1

    goto :goto_0

    .line 151
    :cond_1
    iget-object v1, p0, Ljyw;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 152
    :cond_2
    iget-object v0, p0, Ljyw;->d:Lizu;

    invoke-virtual {v0}, Lizu;->hashCode()I

    move-result v0

    goto :goto_2

    .line 154
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Ljyw;->b:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    .line 184
    iget-object v1, p0, Ljyw;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 185
    :try_start_0
    iget-object v0, p0, Ljyw;->a:Lizu;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 186
    iget-object v0, p0, Ljyw;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 187
    iget-object v0, p0, Ljyw;->d:Lizu;

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 188
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
