.class public final Lbvf;
.super Loxu;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:J

.field public c:[Lbvg;

.field public d:[Lbve;

.field public e:Lbuz;

.field public f:Z

.field public g:J

.field public h:[B


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 2926
    invoke-direct {p0}, Loxu;-><init>()V

    .line 2929
    const-string v0, ""

    iput-object v0, p0, Lbvf;->a:Ljava/lang/String;

    .line 2932
    iput-wide v2, p0, Lbvf;->b:J

    .line 2935
    sget-object v0, Lbvg;->a:[Lbvg;

    iput-object v0, p0, Lbvf;->c:[Lbvg;

    .line 2938
    sget-object v0, Lbve;->a:[Lbve;

    iput-object v0, p0, Lbvf;->d:[Lbve;

    .line 2941
    const/4 v0, 0x0

    iput-object v0, p0, Lbvf;->e:Lbuz;

    .line 2944
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbvf;->f:Z

    .line 2947
    iput-wide v2, p0, Lbvf;->g:J

    .line 2950
    sget-object v0, Loxx;->f:[B

    iput-object v0, p0, Lbvf;->h:[B

    .line 2926
    return-void
.end method


# virtual methods
.method public a()I
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 3004
    .line 3005
    iget-object v0, p0, Lbvf;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 3006
    iget-object v0, p0, Lbvf;->a:Ljava/lang/String;

    .line 3007
    invoke-static {v7, v0}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3009
    :goto_0
    iget-wide v2, p0, Lbvf;->b:J

    cmp-long v2, v2, v8

    if-eqz v2, :cond_0

    .line 3010
    const/4 v2, 0x2

    iget-wide v4, p0, Lbvf;->b:J

    .line 3011
    invoke-static {v2, v4, v5}, Loxo;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 3013
    :cond_0
    iget-object v2, p0, Lbvf;->c:[Lbvg;

    if-eqz v2, :cond_2

    .line 3014
    iget-object v3, p0, Lbvf;->c:[Lbvg;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 3015
    if-eqz v5, :cond_1

    .line 3016
    const/4 v6, 0x3

    .line 3017
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 3014
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3021
    :cond_2
    iget-object v2, p0, Lbvf;->d:[Lbve;

    if-eqz v2, :cond_4

    .line 3022
    iget-object v2, p0, Lbvf;->d:[Lbve;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 3023
    if-eqz v4, :cond_3

    .line 3024
    const/4 v5, 0x4

    .line 3025
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 3022
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3029
    :cond_4
    iget-object v1, p0, Lbvf;->e:Lbuz;

    if-eqz v1, :cond_5

    .line 3030
    const/4 v1, 0x5

    iget-object v2, p0, Lbvf;->e:Lbuz;

    .line 3031
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3033
    :cond_5
    iget-boolean v1, p0, Lbvf;->f:Z

    if-eq v1, v7, :cond_6

    .line 3034
    const/4 v1, 0x6

    iget-boolean v2, p0, Lbvf;->f:Z

    .line 3035
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3037
    :cond_6
    iget-wide v2, p0, Lbvf;->g:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_7

    .line 3038
    const/4 v1, 0x7

    iget-wide v2, p0, Lbvf;->g:J

    .line 3039
    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3041
    :cond_7
    iget-object v1, p0, Lbvf;->h:[B

    sget-object v2, Loxx;->f:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_8

    .line 3042
    const/16 v1, 0x8

    iget-object v2, p0, Lbvf;->h:[B

    .line 3043
    invoke-static {v1, v2}, Loxo;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 3045
    :cond_8
    iput v0, p0, Lbvf;->ai:I

    .line 3046
    return v0

    :cond_9
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lbvf;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3054
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 3055
    sparse-switch v0, :sswitch_data_0

    .line 3059
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3060
    :sswitch_0
    return-object p0

    .line 3065
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbvf;->a:Ljava/lang/String;

    goto :goto_0

    .line 3069
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    iput-wide v2, p0, Lbvf;->b:J

    goto :goto_0

    .line 3073
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 3074
    iget-object v0, p0, Lbvf;->c:[Lbvg;

    if-nez v0, :cond_2

    move v0, v1

    .line 3075
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lbvg;

    .line 3076
    iget-object v3, p0, Lbvf;->c:[Lbvg;

    if-eqz v3, :cond_1

    .line 3077
    iget-object v3, p0, Lbvf;->c:[Lbvg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3079
    :cond_1
    iput-object v2, p0, Lbvf;->c:[Lbvg;

    .line 3080
    :goto_2
    iget-object v2, p0, Lbvf;->c:[Lbvg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_3

    .line 3081
    iget-object v2, p0, Lbvf;->c:[Lbvg;

    new-instance v3, Lbvg;

    invoke-direct {v3}, Lbvg;-><init>()V

    aput-object v3, v2, v0

    .line 3082
    iget-object v2, p0, Lbvf;->c:[Lbvg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 3083
    invoke-virtual {p1}, Loxn;->a()I

    .line 3080
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 3074
    :cond_2
    iget-object v0, p0, Lbvf;->c:[Lbvg;

    array-length v0, v0

    goto :goto_1

    .line 3086
    :cond_3
    iget-object v2, p0, Lbvf;->c:[Lbvg;

    new-instance v3, Lbvg;

    invoke-direct {v3}, Lbvg;-><init>()V

    aput-object v3, v2, v0

    .line 3087
    iget-object v2, p0, Lbvf;->c:[Lbvg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 3091
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 3092
    iget-object v0, p0, Lbvf;->d:[Lbve;

    if-nez v0, :cond_5

    move v0, v1

    .line 3093
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lbve;

    .line 3094
    iget-object v3, p0, Lbvf;->d:[Lbve;

    if-eqz v3, :cond_4

    .line 3095
    iget-object v3, p0, Lbvf;->d:[Lbve;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3097
    :cond_4
    iput-object v2, p0, Lbvf;->d:[Lbve;

    .line 3098
    :goto_4
    iget-object v2, p0, Lbvf;->d:[Lbve;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 3099
    iget-object v2, p0, Lbvf;->d:[Lbve;

    new-instance v3, Lbve;

    invoke-direct {v3}, Lbve;-><init>()V

    aput-object v3, v2, v0

    .line 3100
    iget-object v2, p0, Lbvf;->d:[Lbve;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 3101
    invoke-virtual {p1}, Loxn;->a()I

    .line 3098
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 3092
    :cond_5
    iget-object v0, p0, Lbvf;->d:[Lbve;

    array-length v0, v0

    goto :goto_3

    .line 3104
    :cond_6
    iget-object v2, p0, Lbvf;->d:[Lbve;

    new-instance v3, Lbve;

    invoke-direct {v3}, Lbve;-><init>()V

    aput-object v3, v2, v0

    .line 3105
    iget-object v2, p0, Lbvf;->d:[Lbve;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 3109
    :sswitch_5
    iget-object v0, p0, Lbvf;->e:Lbuz;

    if-nez v0, :cond_7

    .line 3110
    new-instance v0, Lbuz;

    invoke-direct {v0}, Lbuz;-><init>()V

    iput-object v0, p0, Lbvf;->e:Lbuz;

    .line 3112
    :cond_7
    iget-object v0, p0, Lbvf;->e:Lbuz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 3116
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    iput-boolean v0, p0, Lbvf;->f:Z

    goto/16 :goto_0

    .line 3120
    :sswitch_7
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    iput-wide v2, p0, Lbvf;->g:J

    goto/16 :goto_0

    .line 3124
    :sswitch_8
    invoke-virtual {p1}, Loxn;->l()[B

    move-result-object v0

    iput-object v0, p0, Lbvf;->h:[B

    goto/16 :goto_0

    .line 3055
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 2968
    iget-object v1, p0, Lbvf;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2969
    iget-object v1, p0, Lbvf;->a:Ljava/lang/String;

    invoke-virtual {p1, v6, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2971
    :cond_0
    iget-wide v2, p0, Lbvf;->b:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_1

    .line 2972
    const/4 v1, 0x2

    iget-wide v2, p0, Lbvf;->b:J

    invoke-virtual {p1, v1, v2, v3}, Loxo;->b(IJ)V

    .line 2974
    :cond_1
    iget-object v1, p0, Lbvf;->c:[Lbvg;

    if-eqz v1, :cond_3

    .line 2975
    iget-object v2, p0, Lbvf;->c:[Lbvg;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 2976
    if-eqz v4, :cond_2

    .line 2977
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 2975
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2981
    :cond_3
    iget-object v1, p0, Lbvf;->d:[Lbve;

    if-eqz v1, :cond_5

    .line 2982
    iget-object v1, p0, Lbvf;->d:[Lbve;

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 2983
    if-eqz v3, :cond_4

    .line 2984
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2982
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2988
    :cond_5
    iget-object v0, p0, Lbvf;->e:Lbuz;

    if-eqz v0, :cond_6

    .line 2989
    const/4 v0, 0x5

    iget-object v1, p0, Lbvf;->e:Lbuz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2991
    :cond_6
    iget-boolean v0, p0, Lbvf;->f:Z

    if-eq v0, v6, :cond_7

    .line 2992
    const/4 v0, 0x6

    iget-boolean v1, p0, Lbvf;->f:Z

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 2994
    :cond_7
    iget-wide v0, p0, Lbvf;->g:J

    cmp-long v0, v0, v8

    if-eqz v0, :cond_8

    .line 2995
    const/4 v0, 0x7

    iget-wide v2, p0, Lbvf;->g:J

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 2997
    :cond_8
    iget-object v0, p0, Lbvf;->h:[B

    sget-object v1, Loxx;->f:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_9

    .line 2998
    const/16 v0, 0x8

    iget-object v1, p0, Lbvf;->h:[B

    invoke-virtual {p1, v0, v1}, Loxo;->a(I[B)V

    .line 3000
    :cond_9
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2922
    invoke-virtual {p0, p1}, Lbvf;->a(Loxn;)Lbvf;

    move-result-object v0

    return-object v0
.end method
