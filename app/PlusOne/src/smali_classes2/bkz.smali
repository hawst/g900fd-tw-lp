.class public final enum Lbkz;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbkz;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lbkz;

.field public static final enum b:Lbkz;

.field public static final enum c:Lbkz;

.field public static final enum d:Lbkz;

.field public static final enum e:Lbkz;

.field public static final enum f:Lbkz;

.field public static final enum g:Lbkz;

.field public static final enum h:Lbkz;

.field public static final enum i:Lbkz;

.field public static final enum j:Lbkz;

.field public static final enum k:Lbkz;

.field public static final enum l:Lbkz;

.field public static final enum m:Lbkz;

.field public static final enum n:Lbkz;

.field public static final enum o:Lbkz;

.field public static final enum p:Lbkz;

.field private static enum q:Lbkz;

.field private static enum r:Lbkz;

.field private static final synthetic t:[Lbkz;


# instance fields
.field private final s:Lblk;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 34
    new-instance v0, Lbkz;

    const-string v1, "AVERAGE_SHARPNESS"

    new-instance v2, Lblj;

    invoke-direct {v2}, Lblj;-><init>()V

    invoke-direct {v0, v1, v4, v2}, Lbkz;-><init>(Ljava/lang/String;ILblk;)V

    sput-object v0, Lbkz;->a:Lbkz;

    .line 35
    new-instance v0, Lbkz;

    const-string v1, "AVERAGE_FACE_SCORES"

    new-instance v2, Lblg;

    invoke-direct {v2}, Lblg;-><init>()V

    invoke-direct {v0, v1, v5, v2}, Lbkz;-><init>(Ljava/lang/String;ILblk;)V

    sput-object v0, Lbkz;->b:Lbkz;

    .line 36
    new-instance v0, Lbkz;

    const-string v1, "AVERAGE_CHROMA_HISTOGRAM"

    new-instance v2, Lblc;

    invoke-direct {v2}, Lblc;-><init>()V

    invoke-direct {v0, v1, v6, v2}, Lbkz;-><init>(Ljava/lang/String;ILblk;)V

    sput-object v0, Lbkz;->q:Lbkz;

    .line 37
    new-instance v0, Lbkz;

    const-string v1, "HISTOGRAM_SHARPNESS"

    new-instance v2, Lbln;

    invoke-direct {v2}, Lbln;-><init>()V

    invoke-direct {v0, v1, v7, v2}, Lbkz;-><init>(Ljava/lang/String;ILblk;)V

    sput-object v0, Lbkz;->c:Lbkz;

    .line 38
    new-instance v0, Lbkz;

    const-string v1, "HISTOGRAM_SMILESCORE"

    new-instance v2, Lblo;

    invoke-direct {v2}, Lblo;-><init>()V

    invoke-direct {v0, v1, v8, v2}, Lbkz;-><init>(Ljava/lang/String;ILblk;)V

    sput-object v0, Lbkz;->d:Lbkz;

    .line 39
    new-instance v0, Lbkz;

    const-string v1, "SALIENCY_FEATURE"

    const/4 v2, 0x5

    new-instance v3, Lblt;

    invoke-direct {v3}, Lblt;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbkz;-><init>(Ljava/lang/String;ILblk;)V

    sput-object v0, Lbkz;->e:Lbkz;

    .line 40
    new-instance v0, Lbkz;

    const-string v1, "MOTION_SALIENCY_FEATURE"

    const/4 v2, 0x6

    new-instance v3, Lblp;

    invoke-direct {v3}, Lblp;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbkz;-><init>(Ljava/lang/String;ILblk;)V

    sput-object v0, Lbkz;->f:Lbkz;

    .line 41
    new-instance v0, Lbkz;

    const-string v1, "AVERAGE_COLORFULNESS"

    const/4 v2, 0x7

    new-instance v3, Lbld;

    invoke-direct {v3}, Lbld;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbkz;-><init>(Ljava/lang/String;ILblk;)V

    sput-object v0, Lbkz;->r:Lbkz;

    .line 42
    new-instance v0, Lbkz;

    const-string v1, "AVERAGE_FACE_AREA"

    const/16 v2, 0x8

    new-instance v3, Lblf;

    invoke-direct {v3}, Lblf;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbkz;-><init>(Ljava/lang/String;ILblk;)V

    sput-object v0, Lbkz;->g:Lbkz;

    .line 43
    new-instance v0, Lbkz;

    const-string v1, "AUDIO_RMS_FEATURE"

    const/16 v2, 0x9

    new-instance v3, Lbla;

    invoke-direct {v3}, Lbla;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbkz;-><init>(Ljava/lang/String;ILblk;)V

    sput-object v0, Lbkz;->h:Lbkz;

    .line 44
    new-instance v0, Lbkz;

    const-string v1, "NEW_AVERAGE_COLORFULNESS"

    const/16 v2, 0xa

    new-instance v3, Lblr;

    invoke-direct {v3}, Lblr;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbkz;-><init>(Ljava/lang/String;ILblk;)V

    sput-object v0, Lbkz;->i:Lbkz;

    .line 45
    new-instance v0, Lbkz;

    const-string v1, "NEW_AVERAGE_CHROMA_HISTOGRAM"

    const/16 v2, 0xb

    new-instance v3, Lblq;

    invoke-direct {v3}, Lblq;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbkz;-><init>(Ljava/lang/String;ILblk;)V

    sput-object v0, Lbkz;->j:Lbkz;

    .line 46
    new-instance v0, Lbkz;

    const-string v1, "AVERAGE_AUDIO_MFCC"

    const/16 v2, 0xc

    new-instance v3, Lblb;

    invoke-direct {v3}, Lblb;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbkz;-><init>(Ljava/lang/String;ILblk;)V

    sput-object v0, Lbkz;->k:Lbkz;

    .line 47
    new-instance v0, Lbkz;

    const-string v1, "STDDEV_AUDIO_MFCC"

    const/16 v2, 0xd

    new-instance v3, Lblv;

    invoke-direct {v3}, Lblv;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbkz;-><init>(Ljava/lang/String;ILblk;)V

    sput-object v0, Lbkz;->l:Lbkz;

    .line 48
    new-instance v0, Lbkz;

    const-string v1, "AVERAGE_DELTA_MFCC"

    const/16 v2, 0xe

    new-instance v3, Lble;

    invoke-direct {v3}, Lble;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbkz;-><init>(Ljava/lang/String;ILblk;)V

    sput-object v0, Lbkz;->m:Lbkz;

    .line 49
    new-instance v0, Lbkz;

    const-string v1, "VARIANCE_DELTA_MFCC"

    const/16 v2, 0xf

    new-instance v3, Lblw;

    invoke-direct {v3}, Lblw;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbkz;-><init>(Ljava/lang/String;ILblk;)V

    sput-object v0, Lbkz;->n:Lbkz;

    .line 50
    new-instance v0, Lbkz;

    const-string v1, "STABILIZER_CAMERA_COMPENSATION_FEATURE"

    const/16 v2, 0x10

    new-instance v3, Lblu;

    invoke-direct {v3}, Lblu;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbkz;-><init>(Ljava/lang/String;ILblk;)V

    sput-object v0, Lbkz;->o:Lbkz;

    .line 51
    new-instance v0, Lbkz;

    const-string v1, "NEW_MOTION_SALIENCY_FEATURE"

    const/16 v2, 0x11

    new-instance v3, Lbls;

    invoke-direct {v3}, Lbls;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbkz;-><init>(Ljava/lang/String;ILblk;)V

    sput-object v0, Lbkz;->p:Lbkz;

    .line 31
    const/16 v0, 0x12

    new-array v0, v0, [Lbkz;

    sget-object v1, Lbkz;->a:Lbkz;

    aput-object v1, v0, v4

    sget-object v1, Lbkz;->b:Lbkz;

    aput-object v1, v0, v5

    sget-object v1, Lbkz;->q:Lbkz;

    aput-object v1, v0, v6

    sget-object v1, Lbkz;->c:Lbkz;

    aput-object v1, v0, v7

    sget-object v1, Lbkz;->d:Lbkz;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lbkz;->e:Lbkz;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lbkz;->f:Lbkz;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lbkz;->r:Lbkz;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lbkz;->g:Lbkz;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lbkz;->h:Lbkz;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lbkz;->i:Lbkz;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lbkz;->j:Lbkz;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lbkz;->k:Lbkz;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lbkz;->l:Lbkz;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lbkz;->m:Lbkz;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lbkz;->n:Lbkz;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lbkz;->o:Lbkz;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lbkz;->p:Lbkz;

    aput-object v2, v0, v1

    sput-object v0, Lbkz;->t:[Lbkz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILblk;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lblk;",
            ")V"
        }
    .end annotation

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 56
    const-string v0, "field"

    const/4 v1, 0x0

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lblk;

    iput-object v0, p0, Lbkz;->s:Lblk;

    .line 57
    return-void
.end method

.method static synthetic a(Lbvj;)Lbmn;
    .locals 1

    .prologue
    .line 32
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lbmn;->a(Lbvj;)Lbmn;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(F)Ljava/lang/Float;
    .locals 1

    .prologue
    .line 32
    invoke-static {p0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lbkz;
    .locals 1

    .prologue
    .line 31
    const-class v0, Lbkz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbkz;

    return-object v0
.end method

.method public static values()[Lbkz;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lbkz;->t:[Lbkz;

    invoke-virtual {v0}, [Lbkz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbkz;

    return-object v0
.end method


# virtual methods
.method public a(Lbku;[J)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lbkz;->s:Lblk;

    invoke-interface {v0, p1, p2}, Lblk;->a(Lbku;[J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbuw;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lbkz;->s:Lblk;

    invoke-interface {v0, p1}, Lblk;->a(Lbuw;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Lbuw;)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lbkz;->s:Lblk;

    invoke-interface {v0, p1, p2}, Lblk;->a(Ljava/lang/Object;Lbuw;)V

    .line 67
    return-void
.end method
