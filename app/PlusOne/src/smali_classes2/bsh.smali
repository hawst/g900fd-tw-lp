.class public Lbsh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbtu;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/Runnable;

.field private final c:Ljava/lang/Object;

.field private final d:Lbjp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjp",
            "<",
            "Lbhl;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lbjl;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjl",
            "<",
            "Lbhl;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lbig;

.field private final g:Lbgf;

.field private final h:Lasy;

.field private final i:Ljava/util/concurrent/Executor;

.field private j:Ljava/util/concurrent/FutureTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/FutureTask",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private k:Lbtv;

.field private l:Lbji;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbji",
            "<",
            "Lbhl;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lbja;

.field private n:Lbge;

.field private o:J

.field private volatile p:Z

.field private volatile q:Z

.field private r:J

.field private s:Lbsp;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lbsh;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbsh;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbjp;Lbjl;Lbig;Lchk;Lbgf;Lasy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbjp",
            "<",
            "Lbhl;",
            ">;",
            "Lbjl",
            "<",
            "Lbhl;",
            ">;",
            "Lbig;",
            "Lchk;",
            "Lbgf;",
            "Lasy;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 190
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Lbsi;

    invoke-direct {v0, p0}, Lbsi;-><init>(Lbsh;)V

    iput-object v0, p0, Lbsh;->b:Ljava/lang/Runnable;

    .line 109
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbsh;->c:Ljava/lang/Object;

    .line 191
    const-string v0, "videoDecoderPool"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjp;

    iput-object v0, p0, Lbsh;->d:Lbjp;

    .line 192
    const-string v0, "videoDecoderFactory"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjl;

    iput-object v0, p0, Lbsh;->e:Lbjl;

    .line 193
    const-string v0, "mediaExtractorFactory"

    .line 194
    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbig;

    iput-object v0, p0, Lbsh;->f:Lbig;

    .line 195
    const-string v0, "renderContext"

    invoke-static {p5, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgf;

    iput-object v0, p0, Lbsh;->g:Lbgf;

    .line 196
    const-string v0, "stateTracker"

    invoke-static {p6, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lasy;

    iput-object v0, p0, Lbsh;->h:Lasy;

    .line 197
    const-string v0, "executorFactory"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 198
    const-class v0, Lbsh;

    const-string v1, "decoding"

    .line 199
    invoke-interface {p4, v0, v1}, Lchk;->a(Ljava/lang/Class;Ljava/lang/String;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lbsh;->i:Ljava/util/concurrent/Executor;

    .line 200
    new-instance v0, Lbsp;

    invoke-direct {v0}, Lbsp;-><init>()V

    iput-object v0, p0, Lbsh;->s:Lbsp;

    .line 202
    const-wide/16 v0, -0x1

    invoke-virtual {p0, v0, v1}, Lbsh;->a(J)V

    .line 203
    return-void
.end method

.method private a(JLandroid/graphics/Matrix;Landroid/graphics/Matrix;Lbgx;)J
    .locals 9

    .prologue
    .line 449
    iget-object v0, p0, Lbsh;->l:Lbji;

    invoke-virtual {v0}, Lbji;->b()Lbhj;

    move-result-object v0

    check-cast v0, Lbhl;

    .line 452
    if-nez v0, :cond_0

    .line 453
    const-wide/16 v0, -0x1

    .line 493
    :goto_0
    return-wide v0

    .line 457
    :cond_0
    :try_start_0
    iget-wide v2, v0, Lbhl;->a:J

    sub-long/2addr v2, p1

    .line 458
    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v4

    .line 461
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-gtz v1, :cond_3

    const/4 v1, 0x1

    .line 464
    :goto_1
    if-nez v1, :cond_1

    iget-wide v4, p0, Lbsh;->r:J

    cmp-long v1, v4, p1

    if-gez v1, :cond_4

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_4

    :cond_1
    const/4 v1, 0x1

    .line 468
    :goto_2
    if-nez v1, :cond_2

    iget-boolean v1, v0, Lbhl;->b:Z

    if-eqz v1, :cond_5

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_5

    :cond_2
    const/4 v1, 0x1

    .line 470
    :goto_3
    iget-wide v2, v0, Lbhl;->a:J

    iput-wide v2, p0, Lbsh;->r:J

    .line 471
    iget-object v2, p0, Lbsh;->s:Lbsp;

    iget-wide v4, p0, Lbsh;->r:J

    iput-wide v4, v2, Lbsp;->a:J

    .line 473
    if-eqz v1, :cond_6

    .line 478
    iget-object v1, v0, Lbhl;->c:Lbhb;

    .line 479
    iget-object v2, v0, Lbhl;->d:[F

    invoke-static {p3, v2}, Lcfn;->a(Landroid/graphics/Matrix;[F)V

    .line 480
    invoke-virtual {p3, p4}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 481
    iget-object v2, p0, Lbsh;->n:Lbge;

    invoke-static {v2, v1, p3, p5}, Lbfu;->a(Lbgn;Lbhb;Landroid/graphics/Matrix;Lbgx;)V

    .line 482
    iget-object v1, p0, Lbsh;->n:Lbge;

    invoke-interface {v1}, Lbge;->b()V

    .line 483
    iget-wide v2, v0, Lbhl;->a:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 493
    iget-object v1, p0, Lbsh;->l:Lbji;

    invoke-virtual {v1, v0}, Lbji;->a(Ljava/lang/Object;)V

    move-wide v0, v2

    goto :goto_0

    .line 461
    :cond_3
    const/4 v1, 0x0

    goto :goto_1

    .line 464
    :cond_4
    const/4 v1, 0x0

    goto :goto_2

    .line 468
    :cond_5
    const/4 v1, 0x0

    goto :goto_3

    .line 491
    :cond_6
    iget-object v1, p0, Lbsh;->l:Lbji;

    invoke-virtual {v1, v0}, Lbji;->a(Ljava/lang/Object;)V

    const-wide/16 v0, -0x1

    goto :goto_0

    :catchall_0
    move-exception v1

    iget-object v2, p0, Lbsh;->l:Lbji;

    invoke-virtual {v2, v0}, Lbji;->a(Ljava/lang/Object;)V

    throw v1
.end method

.method private static a(Lbms;J)J
    .locals 7

    .prologue
    .line 503
    invoke-virtual {p0, p1, p2}, Lbms;->b(J)J

    move-result-wide v0

    .line 504
    sub-long v2, v0, p1

    .line 505
    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    const-wide/32 v4, 0x186a0

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    move-wide p1, v0

    .line 508
    :cond_0
    return-wide p1
.end method

.method static synthetic a(Lbsh;JLandroid/graphics/Matrix;Landroid/graphics/Matrix;Lbgx;)J
    .locals 3

    .prologue
    .line 50
    invoke-direct/range {p0 .. p5}, Lbsh;->a(JLandroid/graphics/Matrix;Landroid/graphics/Matrix;Lbgx;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(Lbsh;Lbge;)Lbge;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lbsh;->n:Lbge;

    return-object p1
.end method

.method static synthetic a(Lbsh;Lbja;)Lbja;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lbsh;->m:Lbja;

    return-object p1
.end method

.method static synthetic a(Lbsh;)Lbji;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lbsh;->l:Lbji;

    return-object v0
.end method

.method static synthetic a(Lbsh;Lbji;)Lbji;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lbsh;->l:Lbji;

    return-object p1
.end method

.method static synthetic a(Lbsh;Lbtv;)Lbtv;
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lbsh;->k:Lbtv;

    return-object p1
.end method

.method static synthetic a(Lbsh;Lbon;)V
    .locals 14

    .prologue
    .line 50
    new-instance v7, Lcgw;

    sget-object v0, Lbsh;->a:Ljava/lang/String;

    invoke-direct {v7, v0}, Lcgw;-><init>(Ljava/lang/String;)V

    :try_start_0
    new-instance v0, Lbja;

    iget-object v1, p0, Lbsh;->f:Lbig;

    invoke-virtual {p1}, Lbon;->a()Ljeg;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lbja;-><init>(Lbig;Ljeg;)V

    iput-object v0, p0, Lbsh;->m:Lbja;

    iget-object v0, p0, Lbsh;->m:Lbja;

    invoke-virtual {v0}, Lbja;->c()V

    iget-object v0, p0, Lbsh;->g:Lbgf;

    new-instance v1, Lbsk;

    invoke-direct {v1, p0}, Lbsk;-><init>(Lbsh;)V

    invoke-virtual {v0, v1}, Lbgf;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbji;

    iput-object v0, p0, Lbsh;->l:Lbji;

    iget-object v0, p0, Lbsh;->l:Lbji;

    if-nez v0, :cond_0

    const-string v0, "no decoder for looping video frame viewer"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    :catch_0
    move-exception v0

    iget-object v0, p0, Lbsh;->h:Lasy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lasy;->b(Livo;)V

    :goto_0
    invoke-virtual {v7}, Lcgw;->b()V

    return-void

    :cond_0
    const-wide/16 v0, -0x1

    :try_start_1
    iput-wide v0, p0, Lbsh;->r:J

    invoke-virtual {p1}, Lbon;->h()Lbms;

    move-result-object v8

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    new-instance v4, Landroid/graphics/Matrix;

    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {p1}, Lbon;->g()Lboo;

    move-result-object v0

    invoke-static {v0}, Lbqu;->a(Lbmu;)[F

    move-result-object v0

    invoke-static {v5, v0}, Lcfn;->a(Landroid/graphics/Matrix;[F)V

    iget-object v0, p0, Lbsh;->h:Lasy;

    iget-object v1, p0, Lbsh;->s:Lbsp;

    invoke-virtual {v0, v1}, Lasy;->b(Livo;)V

    iget-object v0, p0, Lbsh;->g:Lbgf;

    new-instance v1, Lbsl;

    invoke-direct {v1, p0}, Lbsl;-><init>(Lbsh;)V

    invoke-virtual {v0, v1}, Lbgf;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lbgx;

    const/4 v0, 0x0

    :goto_1
    iget-boolean v1, p0, Lbsh;->q:Z

    if-nez v1, :cond_1

    if-nez v0, :cond_1

    iget-object v0, p0, Lbsh;->g:Lbgf;

    new-instance v1, Lbsm;

    invoke-direct {v1, p0}, Lbsm;-><init>(Lbsh;)V

    invoke-virtual {v0, v1}, Lbgf;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_1

    :cond_1
    const-wide/16 v2, 0x0

    iget-object v1, p0, Lbsh;->c:Ljava/lang/Object;

    monitor-enter v1
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    :cond_2
    :goto_2
    :try_start_2
    iget-boolean v0, p0, Lbsh;->q:Z

    if-nez v0, :cond_3

    iget-wide v10, p0, Lbsh;->o:J

    const-wide/16 v12, -0x1

    cmp-long v0, v10, v12

    if-nez v0, :cond_3

    iget-object v0, p0, Lbsh;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V

    iget-boolean v0, p0, Lbsh;->q:Z

    if-nez v0, :cond_2

    iget-wide v10, p0, Lbsh;->o:J

    invoke-static {v8, v10, v11}, Lbsh;->a(Lbms;J)J

    move-result-wide v10

    iput-wide v10, p0, Lbsh;->o:J

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :catch_1
    move-exception v0

    :try_start_4
    sget-object v1, Lbsh;->a:Ljava/lang/String;

    const-string v2, "Error on seeking preview thread: "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lbsh;->h:Lasy;

    invoke-virtual {v0}, Lasy;->b()Livo;

    move-result-object v0

    sget-object v1, Lbsh;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Livl;->a(Livo;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    iget-object v0, p0, Lbsh;->h:Lasy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lasy;->b(Livo;)V

    goto/16 :goto_0

    :cond_3
    :try_start_5
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_4
    :goto_3
    :try_start_6
    iget-boolean v0, p0, Lbsh;->q:Z

    if-nez v0, :cond_c

    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-nez v0, :cond_c

    const/4 v0, 0x0

    iget-object v1, p0, Lbsh;->c:Ljava/lang/Object;

    monitor-enter v1
    :try_end_6
    .catch Ljava/lang/InterruptedException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    iget-boolean v9, p0, Lbsh;->q:Z

    if-nez v9, :cond_6

    iget-wide v10, p0, Lbsh;->o:J

    cmp-long v9, v2, v10

    if-eqz v9, :cond_6

    iget-wide v2, p0, Lbsh;->o:J

    iget-wide v10, p0, Lbsh;->r:J

    const-wide/16 v12, -0x1

    cmp-long v0, v10, v12

    if-eqz v0, :cond_5

    iget-wide v10, p0, Lbsh;->r:J

    cmp-long v0, v2, v10

    if-lez v0, :cond_5

    iget-wide v10, p0, Lbsh;->r:J

    invoke-virtual {v8, v2, v3}, Lbms;->c(J)J

    move-result-wide v12

    cmp-long v0, v10, v12

    if-gez v0, :cond_a

    :cond_5
    const/4 v0, 0x1

    :cond_6
    :goto_4
    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    if-eqz v0, :cond_7

    :try_start_8
    iget-object v0, p0, Lbsh;->m:Lbja;

    invoke-virtual {v0, v2, v3}, Lbja;->a(J)Z

    iget-object v0, p0, Lbsh;->g:Lbgf;

    new-instance v1, Lbsn;

    invoke-direct {v1, p0}, Lbsn;-><init>(Lbsh;)V

    invoke-virtual {v0, v1}, Lbgf;->a(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lbsh;->s:Lbsp;

    iput-wide v2, v0, Lbsp;->b:J

    :cond_7
    iget-object v9, p0, Lbsh;->g:Lbgf;

    new-instance v0, Lbso;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lbso;-><init>(Lbsh;JLandroid/graphics/Matrix;Landroid/graphics/Matrix;Lbgx;)V

    invoke-virtual {v9, v0}, Lbgf;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v10, -0x1

    cmp-long v0, v0, v10

    if-eqz v0, :cond_4

    iget-object v0, p0, Lbsh;->k:Lbtv;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lbsh;->k:Lbtv;

    invoke-interface {v0}, Lbtv;->a()V

    :cond_8
    iget-object v1, p0, Lbsh;->c:Ljava/lang/Object;

    monitor-enter v1
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :cond_9
    :goto_5
    :try_start_9
    iget-boolean v0, p0, Lbsh;->q:Z

    if-nez v0, :cond_b

    iget-wide v10, p0, Lbsh;->o:J
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    cmp-long v0, v10, v2

    if-nez v0, :cond_b

    :try_start_a
    iget-object v0, p0, Lbsh;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_a
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :try_start_b
    iget-boolean v0, p0, Lbsh;->q:Z

    if-nez v0, :cond_9

    iget-wide v10, p0, Lbsh;->o:J

    invoke-static {v8, v10, v11}, Lbsh;->a(Lbms;J)J

    move-result-wide v10

    iput-wide v10, p0, Lbsh;->o:J

    goto :goto_5

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :try_start_c
    throw v0
    :try_end_c
    .catch Ljava/lang/InterruptedException; {:try_start_c .. :try_end_c} :catch_0
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_1
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    :catchall_2
    move-exception v0

    iget-object v1, p0, Lbsh;->h:Lasy;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lasy;->b(Livo;)V

    throw v0

    :cond_a
    const/4 v0, 0x0

    goto :goto_4

    :catchall_3
    move-exception v0

    :try_start_d
    monitor-exit v1
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    :try_start_e
    throw v0
    :try_end_e
    .catch Ljava/lang/InterruptedException; {:try_start_e .. :try_end_e} :catch_0
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_1
    .catchall {:try_start_e .. :try_end_e} :catchall_2

    :catch_2
    move-exception v0

    :try_start_f
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    :cond_b
    monitor-exit v1
    :try_end_f
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    const-wide/16 v2, -0x1

    goto/16 :goto_3

    :cond_c
    iget-object v0, p0, Lbsh;->h:Lasy;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lasy;->b(Livo;)V

    goto/16 :goto_0
.end method

.method static synthetic a(Lbsh;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lbsh;->p:Z

    return p1
.end method

.method static synthetic b(Lbsh;)Lbjp;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lbsh;->d:Lbjp;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lbsh;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lbsh;Z)Z
    .locals 0

    .prologue
    .line 50
    iput-boolean p1, p0, Lbsh;->q:Z

    return p1
.end method

.method static synthetic c(Lbsh;)Lbja;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lbsh;->m:Lbja;

    return-object v0
.end method

.method static synthetic d(Lbsh;)Lbge;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lbsh;->n:Lbge;

    return-object v0
.end method

.method static synthetic e(Lbsh;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lbsh;->c:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic f(Lbsh;)Z
    .locals 1

    .prologue
    .line 50
    iget-boolean v0, p0, Lbsh;->p:Z

    return v0
.end method

.method static synthetic g(Lbsh;)Lbjl;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lbsh;->e:Lbjl;

    return-object v0
.end method

.method static synthetic h(Lbsh;)Lbgf;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lbsh;->g:Lbgf;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 254
    iget-object v1, p0, Lbsh;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 255
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lbsh;->q:Z

    .line 256
    iget-object v0, p0, Lbsh;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 257
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    iget-object v0, p0, Lbsh;->j:Ljava/util/concurrent/FutureTask;

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lbsh;->j:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    .line 266
    :try_start_1
    iget-object v0, p0, Lbsh;->g:Lbgf;

    iget-object v1, p0, Lbsh;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lbgf;->b(Ljava/lang/Runnable;)V
    :try_end_1
    .catch Lbgm; {:try_start_1 .. :try_end_1} :catch_0

    .line 271
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lbsh;->j:Ljava/util/concurrent/FutureTask;

    .line 273
    :cond_0
    return-void

    .line 257
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 268
    :catch_0
    move-exception v0

    sget-object v0, Lbsh;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 246
    iget-object v1, p0, Lbsh;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 247
    :try_start_0
    iput-wide p1, p0, Lbsh;->o:J

    .line 248
    iget-object v0, p0, Lbsh;->c:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    .line 249
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lbon;Lbge;Lbtv;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 208
    const-string v0, "video"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 209
    const-string v0, "renderSink"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 212
    iget-boolean v0, p0, Lbsh;->p:Z

    if-eqz v0, :cond_0

    .line 213
    invoke-virtual {p0}, Lbsh;->a()V

    .line 216
    :cond_0
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, Lbsj;

    invoke-direct {v1, p0, p2, p3, p1}, Lbsj;-><init>(Lbsh;Lbge;Lbtv;Lbon;)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    iput-object v0, p0, Lbsh;->j:Ljava/util/concurrent/FutureTask;

    .line 241
    iget-object v0, p0, Lbsh;->i:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lbsh;->j:Ljava/util/concurrent/FutureTask;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 242
    return-void
.end method
