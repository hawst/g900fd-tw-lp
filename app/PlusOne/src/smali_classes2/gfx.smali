.class public abstract Lgfx;
.super Lgfv;

# interfaces
.implements Lggq;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R::",
        "Lggf;",
        "A::",
        "Lgfr;",
        ">",
        "Lgfv",
        "<TR;>;",
        "Lggq",
        "<TA;>;"
    }
.end annotation


# instance fields
.field private final a:Lgft;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgft",
            "<TA;>;"
        }
    .end annotation
.end field

.field private e:Lggo;


# direct methods
.method public constructor <init>(Lgft;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgft",
            "<TA;>;)V"
        }
    .end annotation

    invoke-direct {p0}, Lgfv;-><init>()V

    invoke-static {p1}, Lgih;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgft;

    iput-object v0, p0, Lgfx;->a:Lgft;

    return-void
.end method

.method private a(Landroid/os/RemoteException;)V
    .locals 4

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x8

    invoke-virtual {p1}, Landroid/os/RemoteException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-virtual {p0, v0}, Lgfx;->c(Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method


# virtual methods
.method public abstract a(Lgfr;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)V"
        }
    .end annotation
.end method

.method public a(Lggo;)V
    .locals 0

    iput-object p1, p0, Lgfx;->e:Lggo;

    return-void
.end method

.method public final b(Lgfr;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TA;)V"
        }
    .end annotation

    iget-object v0, p0, Lgfx;->c:Lgfw;

    if-nez v0, :cond_0

    new-instance v0, Lgfw;

    invoke-interface {p1}, Lgfr;->d()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Lgfw;-><init>(Landroid/os/Looper;)V

    invoke-virtual {p0, v0}, Lgfx;->a(Lgfw;)V

    :cond_0
    :try_start_0
    invoke-virtual {p0, p1}, Lgfx;->a(Lgfr;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-direct {p0, v0}, Lgfx;->a(Landroid/os/RemoteException;)V

    throw v0

    :catch_1
    move-exception v0

    invoke-direct {p0, v0}, Lgfx;->a(Landroid/os/RemoteException;)V

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Failed result must not be success"

    invoke-static {v0, v1}, Lgih;->b(ZLjava/lang/Object;)V

    invoke-virtual {p0, p1}, Lgfx;->b(Lcom/google/android/gms/common/api/Status;)Lggf;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgfx;->a(Lggf;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected f()V
    .locals 1

    invoke-super {p0}, Lgfv;->f()V

    iget-object v0, p0, Lgfx;->e:Lggo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgfx;->e:Lggo;

    invoke-interface {v0, p0}, Lggo;->a(Lggq;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lgfx;->e:Lggo;

    :cond_0
    return-void
.end method

.method public final g()Lgft;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lgft",
            "<TA;>;"
        }
    .end annotation

    iget-object v0, p0, Lgfx;->a:Lgft;

    return-object v0
.end method

.method public h()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
