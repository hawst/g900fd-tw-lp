.class final Linq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lggg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lggg",
        "<",
        "Lgyq;",
        ">;"
    }
.end annotation


# instance fields
.field final a:Landroid/net/Uri;

.field private final b:Linr;

.field private synthetic c:Linp;


# direct methods
.method public constructor <init>(Linp;Linr;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Linq;->c:Linp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p2, p0, Linq;->b:Linr;

    .line 52
    iput-object p3, p0, Linq;->a:Landroid/net/Uri;

    .line 53
    return-void
.end method


# virtual methods
.method public a()Linr;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Linq;->b:Linr;

    return-object v0
.end method

.method public bridge synthetic a(Lggf;)V
    .locals 0

    .prologue
    .line 46
    check-cast p1, Lgyq;

    invoke-virtual {p0, p1}, Linq;->a(Lgyq;)V

    return-void
.end method

.method public a(Lgyq;)V
    .locals 6

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x1

    .line 58
    invoke-interface {p1}, Lgyq;->a()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    invoke-interface {p1}, Lgyq;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 72
    :cond_0
    const/4 v0, 0x0

    .line 77
    :goto_0
    iget-object v3, p0, Linq;->b:Linr;

    invoke-interface {p1}, Lgyq;->c()Landroid/content/Intent;

    move-result-object v4

    invoke-interface {v3, v0, v4}, Linr;->a(ILandroid/content/Intent;)V

    .line 80
    iget-object v3, p0, Linq;->c:Linp;

    invoke-static {v3}, Linp;->a(Linp;)Landroid/os/Handler;

    move-result-object v3

    const-wide/16 v4, 0xbb8

    invoke-virtual {v3, v1, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 82
    const-string v1, "PanoramaClient"

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 83
    const-string v1, "onPanoramaTypeDetected: "

    invoke-static {v0}, Linp;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 85
    :cond_1
    :goto_1
    return-void

    .line 63
    :pswitch_0
    const/4 v0, 0x2

    .line 64
    goto :goto_0

    :pswitch_1
    move v0, v1

    .line 67
    goto :goto_0

    :pswitch_2
    move v0, v2

    .line 70
    goto :goto_0

    .line 83
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 59
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
