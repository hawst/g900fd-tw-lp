.class public abstract Lccv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcch;


# instance fields
.field public final a:Lcci;


# direct methods
.method protected constructor <init>(Lcci;)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-string v0, "owner"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcci;

    iput-object v0, p0, Lccv;->a:Lcci;

    .line 23
    return-void
.end method


# virtual methods
.method protected final a(Lccj;II)I
    .locals 6

    .prologue
    .line 60
    iget-boolean v0, p1, Lccj;->a:Z

    if-eqz v0, :cond_0

    .line 61
    iget v0, p1, Lccj;->i:I

    add-int/2addr p2, v0

    .line 65
    :goto_0
    const/4 v0, 0x0

    iget-object v1, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_2

    .line 66
    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    iget-object v0, v0, Lccg;->a:Landroid/view/View;

    .line 67
    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v3

    float-to-int v3, v3

    .line 68
    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v4

    float-to-int v4, v4

    .line 69
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    add-int/2addr v5, v3

    .line 70
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v4

    .line 71
    if-lt p2, v3, :cond_1

    if-lt p3, v4, :cond_1

    if-ge p2, v5, :cond_1

    if-ge p3, v0, :cond_1

    .line 72
    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    .line 75
    :goto_2
    return v0

    .line 63
    :cond_0
    iget v0, p1, Lccj;->i:I

    add-int/2addr p3, v0

    goto :goto_0

    .line 65
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 75
    :cond_2
    const/4 v0, -0x1

    goto :goto_2
.end method

.method public a(Lccj;)V
    .locals 0

    .prologue
    .line 30
    return-void
.end method

.method public a(Lccj;Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 35
    return-void
.end method

.method public a(Lccj;I)Z
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lccj;Landroid/view/DragEvent;)Z
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x0

    return v0
.end method

.method public a(Lccj;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    return v0
.end method
