.class public Lbyf;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lbyg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lbyf;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbyf;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbyg;)V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-string v0, "autoTitler"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbyg;

    iput-object v0, p0, Lbyf;->b:Lbyg;

    .line 29
    return-void
.end method


# virtual methods
.method public a(Lawc;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 32
    const-string v0, "state"

    const/4 v3, 0x0

    invoke-static {p1, v0, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 33
    invoke-interface {p1}, Lawc;->B()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Auto attributes already applied"

    invoke-static {v0, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 35
    iget-object v0, p0, Lbyf;->b:Lbyg;

    invoke-interface {p1}, Lawc;->b()Ljava/util/List;

    invoke-interface {p1}, Lawc;->c()Ljava/util/List;

    invoke-interface {v0}, Lbyg;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lawc;->a(Ljava/lang/String;)V

    .line 39
    const/16 v0, 0x11

    .line 40
    invoke-interface {p1}, Lawc;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v0

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbon;

    .line 41
    invoke-virtual {v0}, Lbon;->a()Ljeg;

    move-result-object v0

    invoke-static {v0, v3}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v0

    move v3, v0

    .line 42
    goto :goto_1

    :cond_0
    move v0, v2

    .line 33
    goto :goto_0

    .line 43
    :cond_1
    invoke-interface {p1}, Lawc;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmw;

    .line 44
    invoke-virtual {v0}, Lbmw;->a()Ljeg;

    move-result-object v0

    invoke-static {v0, v3}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v3

    goto :goto_2

    .line 46
    :cond_2
    new-instance v0, Ljava/util/Random;

    int-to-long v4, v3

    invoke-direct {v0, v4, v5}, Ljava/util/Random;-><init>(J)V

    .line 47
    sget-object v3, Lbzb;->a:[Lbza;

    const/16 v3, 0xa

    invoke-virtual {v0, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    .line 48
    sget-object v4, Lbzb;->a:[Lbza;

    aget-object v3, v4, v3

    .line 49
    invoke-interface {p1, v3}, Lawc;->a(Lbza;)V

    .line 54
    invoke-interface {p1}, Lawc;->u()Lbrc;

    move-result-object v4

    .line 55
    if-eqz v4, :cond_4

    .line 57
    invoke-virtual {v4, v3}, Lbrc;->a(Lbza;)Ljava/util/List;

    move-result-object v3

    .line 58
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    .line 60
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboh;

    .line 61
    invoke-interface {p1, v0}, Lawc;->a(Lboh;)V

    .line 62
    invoke-interface {p1, v2}, Lawc;->j(Z)V

    .line 67
    :cond_3
    :goto_3
    invoke-interface {p1, v1}, Lawc;->f(Z)V

    .line 68
    return-void

    .line 65
    :cond_4
    sget-object v0, Lbyf;->a:Ljava/lang/String;

    goto :goto_3
.end method
