.class public Lbyb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbxr;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lbvr;

.field private final c:Lbxe;

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final e:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lbyb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbyb;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbvr;Lbxe;FLjava/util/Set;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbvr;",
            "Lbxe;",
            "F",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    const-string v0, "qualityModel"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvr;

    iput-object v0, p0, Lbyb;->b:Lbvr;

    .line 58
    const-string v0, "similarityMetric"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxe;

    iput-object v0, p0, Lbyb;->c:Lbxe;

    .line 59
    iput p3, p0, Lbyb;->e:F

    .line 60
    iput-object p4, p0, Lbyb;->d:Ljava/util/Set;

    .line 61
    return-void
.end method

.method private a(Ljava/util/Set;Lbyc;I)Lbyc;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Lbyc;",
            ">;",
            "Lbyc;",
            "I)",
            "Lbyc;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 164
    .line 166
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v2, v1

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbyc;

    .line 167
    invoke-virtual {v0}, Lbyc;->b()F

    move-result v6

    .line 168
    if-lez p3, :cond_3

    iget-object v3, p0, Lbyb;->c:Lbxe;

    .line 169
    if-eqz v3, :cond_0

    if-nez p2, :cond_2

    :cond_0
    move v3, v4

    :goto_1
    int-to-float v7, p3

    div-float/2addr v3, v7

    .line 170
    :goto_2
    iget v7, p0, Lbyb;->e:F

    mul-float/2addr v3, v7

    add-float/2addr v3, v6

    .line 171
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v6

    cmpl-float v6, v3, v6

    if-lez v6, :cond_5

    .line 172
    :cond_1
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    :goto_3
    move-object v2, v1

    move-object v1, v0

    .line 175
    goto :goto_0

    .line 169
    :cond_2
    invoke-virtual {v0}, Lbyc;->d()Lbok;

    move-result-object v7

    invoke-virtual {v0}, Lbyc;->c()Lbmr;

    move-result-object v8

    invoke-virtual {p2}, Lbyc;->d()Lbok;

    move-result-object v9

    invoke-virtual {p2}, Lbyc;->c()Lbmr;

    move-result-object v10

    invoke-interface {v3, v7, v8, v9, v10}, Lbxe;->a(Lbok;Lbmr;Lbok;Lbmr;)F

    move-result v3

    invoke-virtual {v0, v3}, Lbyc;->a(F)V

    invoke-virtual {v0}, Lbyc;->e()F

    move-result v3

    goto :goto_1

    :cond_3
    move v3, v4

    goto :goto_2

    .line 176
    :cond_4
    return-object v1

    :cond_5
    move-object v0, v1

    move-object v1, v2

    goto :goto_3
.end method

.method private static a(Lbxs;Lbvr;Ljava/util/Collection;)Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxs;",
            "Lbvr;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/Set",
            "<",
            "Lbyc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    .line 109
    invoke-static {p0, p2}, Lbya;->a(Lbxs;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    .line 111
    invoke-static {v0, p1, p0, p2}, Lbya;->a(Ljava/util/List;Lbvr;Lbxs;Ljava/util/Collection;)Lbyd;

    move-result-object v0

    .line 116
    const/4 v1, 0x4

    .line 117
    invoke-virtual {v0, v1}, Lbyd;->a(I)Ljava/util/List;

    move-result-object v0

    .line 119
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 124
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxk;

    .line 125
    iget-object v3, v0, Lbxk;->b:Lbmr;

    invoke-interface {p1}, Lbvr;->a()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 126
    iget-object v3, v0, Lbxk;->b:Lbmr;

    invoke-interface {p1, v3}, Lbvr;->a(Lbmr;)F

    move-result v3

    .line 127
    new-instance v4, Lbyc;

    invoke-direct {v4, v0, v3}, Lbyc;-><init>(Lbxk;F)V

    invoke-interface {v1, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 130
    :cond_1
    return-object v1
.end method


# virtual methods
.method public a(Lbxs;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbxs;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    iget-object v0, p0, Lbyb;->b:Lbvr;

    iget-object v1, p0, Lbyb;->d:Ljava/util/Set;

    invoke-static {p1, v0, v1}, Lbyb;->a(Lbxs;Lbvr;Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v3

    .line 68
    const/4 v2, 0x0

    .line 71
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 73
    const-wide/16 v0, 0x0

    .line 74
    :goto_0
    iget-wide v6, p1, Lbxs;->c:J

    cmp-long v5, v0, v6

    if-gtz v5, :cond_0

    .line 75
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    invoke-direct {p0, v3, v2, v5}, Lbyb;->a(Ljava/util/Set;Lbyc;I)Lbyc;

    move-result-object v2

    .line 76
    if-nez v2, :cond_1

    .line 77
    sget-object v0, Lbyb;->a:Ljava/lang/String;

    .line 89
    :cond_0
    return-object v4

    .line 80
    :cond_1
    invoke-virtual {v2}, Lbyc;->a()Lbmd;

    move-result-object v5

    invoke-virtual {v5}, Lbmd;->b()J

    move-result-wide v6

    .line 81
    invoke-virtual {v2}, Lbyc;->a()Lbmd;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    invoke-interface {v3, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 87
    add-long/2addr v0, v6

    .line 88
    goto :goto_0
.end method

.method public b(Lbxs;)J
    .locals 5

    .prologue
    .line 94
    iget-object v0, p0, Lbyb;->b:Lbvr;

    iget-object v1, p0, Lbyb;->d:Ljava/util/Set;

    invoke-static {p1, v0, v1}, Lbyb;->a(Lbxs;Lbvr;Ljava/util/Collection;)Ljava/util/Set;

    move-result-object v2

    .line 95
    const-wide/16 v0, 0x0

    .line 96
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbyc;

    .line 97
    invoke-virtual {v0}, Lbyc;->a()Lbmd;

    move-result-object v0

    invoke-virtual {v0}, Lbmd;->b()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 98
    goto :goto_0

    .line 99
    :cond_0
    return-wide v2
.end method
