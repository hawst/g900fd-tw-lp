.class final Lbwy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lahy;


# instance fields
.field private synthetic a:Lbwx;


# direct methods
.method constructor <init>(Lbwx;)V
    .locals 0

    .prologue
    .line 315
    iput-object p1, p0, Lbwy;->a:Lbwx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 345
    return-void
.end method

.method public a(Landroid/net/Uri;J)V
    .locals 2

    .prologue
    .line 334
    iget-object v0, p0, Lbwy;->a:Lbwx;

    iget-boolean v0, v0, Lbwx;->b:Z

    if-eqz v0, :cond_0

    .line 335
    iget-object v0, p0, Lbwy;->a:Lbwx;

    iget-object v0, v0, Lbwx;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 337
    :cond_0
    return-void
.end method

.method public a(Landroid/net/Uri;Laic;Lbkr;)V
    .locals 4

    .prologue
    .line 319
    iget-object v0, p0, Lbwy;->a:Lbwx;

    iget-object v1, v0, Lbwx;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v2, v0, Lbwx;->d:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    monitor-exit v1

    .line 320
    :goto_0
    return-void

    .line 319
    :cond_0
    iput-object p3, v0, Lbwx;->e:Lbkr;

    const/4 v2, 0x1

    iput v2, v0, Lbwx;->d:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Lbwx;->g()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Landroid/net/Uri;Laic;Ljava/lang/Exception;)V
    .locals 4

    .prologue
    .line 324
    iget-object v0, p0, Lbwy;->a:Lbwx;

    iget-object v1, v0, Lbwx;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v2, v0, Lbwx;->d:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    monitor-exit v1

    .line 325
    :goto_0
    return-void

    .line 324
    :cond_0
    iput-object p3, v0, Lbwx;->f:Ljava/lang/Exception;

    const/4 v2, 0x2

    iput v2, v0, Lbwx;->d:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Lbwx;->g()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Landroid/net/Uri;Lbkr;)V
    .locals 0

    .prologue
    .line 341
    return-void
.end method

.method public b(Landroid/net/Uri;Laic;Lbkr;)V
    .locals 4

    .prologue
    .line 329
    iget-object v0, p0, Lbwy;->a:Lbwx;

    iget-object v1, v0, Lbwx;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v2, v0, Lbwx;->d:I

    const/4 v3, 0x4

    if-ne v2, v3, :cond_0

    monitor-exit v1

    .line 330
    :goto_0
    return-void

    .line 329
    :cond_0
    iput-object p3, v0, Lbwx;->e:Lbkr;

    const/4 v2, 0x3

    iput v2, v0, Lbwx;->d:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v0}, Lbwx;->g()V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
