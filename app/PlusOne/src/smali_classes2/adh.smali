.class public final Ladh;
.super Lado;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final c:[F

.field private final d:I

.field private final e:I

.field private f:Lacm;

.field private g:Landroid/graphics/SurfaceTexture;

.field private h:Lada;

.field private i:Z

.field private j:J


# direct methods
.method public constructor <init>(ILandroid/media/MediaFormat;Ladm;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0, p1, p2, p3}, Lado;-><init>(ILandroid/media/MediaFormat;Ladm;)V

    .line 71
    const-string v0, "width"

    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ladh;->d:I

    .line 72
    const-string v0, "height"

    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ladh;->e:I

    .line 74
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Ladh;->c:[F

    .line 75
    return-void
.end method

.method private i()Z
    .locals 2

    .prologue
    .line 126
    iget-object v1, p0, Ladh;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 128
    :goto_0
    :try_start_0
    iget-boolean v0, p0, Ladh;->b:Z

    if-nez v0, :cond_0

    .line 129
    iget-object v0, p0, Ladh;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 133
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    :try_start_1
    monitor-exit v1

    :goto_1
    return v0

    .line 131
    :cond_0
    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_1

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method protected a(Landroid/media/MediaFormat;)Landroid/media/MediaCodec;
    .locals 4

    .prologue
    .line 80
    invoke-static {}, Lada;->b()Lada;

    move-result-object v0

    iput-object v0, p0, Ladh;->h:Lada;

    .line 81
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Ladh;->h:Lada;

    invoke-virtual {v1}, Lada;->c()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, Ladh;->g:Landroid/graphics/SurfaceTexture;

    .line 82
    iget-object v0, p0, Ladh;->g:Landroid/graphics/SurfaceTexture;

    new-instance v1, Ladi;

    invoke-direct {v1, p0}, Ladi;-><init>(Ladh;)V

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 89
    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, Ladh;->g:Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    .line 90
    const-string v1, "mime"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/media/MediaCodec;->createDecoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v1

    .line 92
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v0, v2, v3}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 93
    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 94
    return-object v1
.end method

.method protected a(Laas;Labg;I)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x3e8

    const/high16 v3, 0x3f800000    # 1.0f

    const/16 v4, 0x8

    .line 142
    iget-object v0, p0, Ladh;->g:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 143
    iget-object v0, p0, Ladh;->g:Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Ladh;->c:[F

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    .line 145
    iget-object v0, p0, Ladh;->f:Lacm;

    if-nez v0, :cond_0

    new-instance v0, Lacm;

    const-string v1, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Ladh;->f:Lacm;

    iget-object v0, p0, Ladh;->f:Lacm;

    const/4 v1, 0x0

    const/high16 v2, -0x40800000    # -1.0f

    invoke-virtual {v0, v1, v3, v3, v2}, Lacm;->b(FFFF)V

    :cond_0
    iget-object v3, p0, Ladh;->f:Lacm;

    .line 146
    iget-object v0, p0, Ladh;->c:[F

    invoke-virtual {v3, v0}, Lacm;->b([F)V

    .line 148
    iget v2, p0, Ladh;->d:I

    .line 149
    iget v1, p0, Ladh;->e:I

    .line 150
    if-eqz p3, :cond_2

    .line 151
    sparse-switch p3, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported rotation angle."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    new-array v0, v4, [F

    fill-array-data v0, :array_0

    .line 152
    :goto_0
    invoke-virtual {v3, v0}, Lacm;->c([F)V

    .line 153
    invoke-static {p3}, Ladh;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 154
    iget v1, p0, Ladh;->e:I

    .line 155
    iget v0, p0, Ladh;->d:I

    .line 158
    :goto_1
    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v4, 0x0

    aput v1, v2, v4

    const/4 v4, 0x1

    aput v0, v2, v4

    invoke-virtual {p1, v2}, Laas;->a([I)V

    .line 159
    iget-object v2, p0, Ladh;->h:Lada;

    invoke-virtual {p1}, Laas;->m()Lacw;

    move-result-object v4

    invoke-virtual {v3, v2, v4, v1, v0}, Lacm;->a(Lada;Lacw;II)V

    .line 164
    iget-wide v0, p0, Ladh;->j:J

    mul-long/2addr v0, v6

    invoke-virtual {p1, v0, v1}, Laas;->a(J)V

    .line 165
    invoke-virtual {p1}, Laas;->h()V

    .line 167
    if-eqz p2, :cond_1

    .line 168
    new-instance v0, Ladn;

    iget-boolean v1, p0, Ladh;->i:Z

    invoke-direct {v0, v1}, Ladn;-><init>(Z)V

    invoke-virtual {p2, v0}, Labg;->a(Ljava/lang/Object;)V

    .line 169
    iget-wide v0, p0, Ladh;->j:J

    mul-long/2addr v0, v6

    invoke-virtual {p2, v0, v1}, Labg;->a(J)V

    .line 171
    :cond_1
    return-void

    .line 151
    :sswitch_1
    new-array v0, v4, [F

    fill-array-data v0, :array_1

    goto :goto_0

    :sswitch_2
    new-array v0, v4, [F

    fill-array-data v0, :array_2

    goto :goto_0

    :sswitch_3
    new-array v0, v4, [F

    fill-array-data v0, :array_3

    goto :goto_0

    :cond_2
    move v0, v1

    move v1, v2

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_3
        0x5a -> :sswitch_0
        0xb4 -> :sswitch_1
        0x10e -> :sswitch_2
    .end sparse-switch

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
    .end array-data

    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
    .end array-data

    :array_3
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method protected a(Landroid/media/MediaCodec;[Ljava/nio/ByteBuffer;ILandroid/media/MediaCodec$BufferInfo;Z)Z
    .locals 4

    .prologue
    .line 103
    invoke-virtual {p0}, Ladh;->a()Z

    move-result v0

    .line 105
    iget-wide v2, p4, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iput-wide v2, p0, Ladh;->j:J

    .line 106
    iput-boolean p5, p0, Ladh;->i:Z

    .line 110
    invoke-virtual {p1, p3, v0}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 112
    if-eqz v0, :cond_0

    .line 113
    invoke-direct {p0}, Ladh;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    invoke-virtual {p0}, Ladh;->g()V

    .line 118
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 175
    invoke-super {p0}, Lado;->c()V

    .line 176
    iget-object v1, p0, Ladh;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 177
    :try_start_0
    iget-object v0, p0, Ladh;->h:Lada;

    invoke-virtual {v0}, Lada;->g()V

    .line 178
    iget-object v0, p0, Ladh;->g:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 179
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
