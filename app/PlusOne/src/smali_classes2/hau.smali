.class final Lhau;
.super Ljava/util/TimerTask;
.source "PG"


# instance fields
.field private a:I

.field private b:J

.field private c:Ljava/util/Timer;

.field private d:Lhaw;

.field private synthetic e:Lhap;


# direct methods
.method public constructor <init>(Lhap;Lhaw;I)V
    .locals 4

    .prologue
    .line 936
    iput-object p1, p0, Lhau;->e:Lhap;

    invoke-direct {p0}, Ljava/util/TimerTask;-><init>()V

    .line 937
    if-nez p2, :cond_0

    .line 938
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 941
    :cond_0
    new-instance v0, Ljava/util/Timer;

    invoke-direct {v0}, Ljava/util/Timer;-><init>()V

    iput-object v0, p0, Lhau;->c:Ljava/util/Timer;

    .line 942
    iget-object v0, p0, Lhau;->c:Ljava/util/Timer;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    int-to-long v2, p3

    invoke-virtual {v0, p0, v1, v2, v3}, Ljava/util/Timer;->scheduleAtFixedRate(Ljava/util/TimerTask;Ljava/util/Date;J)V

    .line 944
    iput-object p2, p0, Lhau;->d:Lhaw;

    .line 945
    return-void
.end method

.method static synthetic a(Lhau;I)I
    .locals 0

    .prologue
    .line 929
    iput p1, p0, Lhau;->a:I

    return p1
.end method

.method static synthetic a(Lhau;J)J
    .locals 1

    .prologue
    .line 929
    iput-wide p1, p0, Lhau;->b:J

    return-wide p1
.end method

.method static synthetic a(Lhau;)V
    .locals 1

    .prologue
    .line 929
    iget-object v0, p0, Lhau;->c:Ljava/util/Timer;

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    const/4 v0, 0x0

    iput-object v0, p0, Lhau;->d:Lhaw;

    return-void
.end method

.method static synthetic b(Lhau;)I
    .locals 1

    .prologue
    .line 929
    iget v0, p0, Lhau;->a:I

    return v0
.end method

.method static synthetic c(Lhau;)J
    .locals 2

    .prologue
    .line 929
    iget-wide v0, p0, Lhau;->b:J

    return-wide v0
.end method

.method static synthetic d(Lhau;)Lhaw;
    .locals 1

    .prologue
    .line 929
    iget-object v0, p0, Lhau;->d:Lhaw;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 948
    iget v0, p0, Lhau;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhau;->a:I

    .line 949
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 952
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lhau;->b:J

    .line 953
    const/4 v0, 0x0

    iput v0, p0, Lhau;->a:I

    .line 954
    return-void
.end method

.method public run()V
    .locals 2

    .prologue
    .line 963
    iget-object v0, p0, Lhau;->e:Lhap;

    new-instance v1, Lhav;

    invoke-direct {v1, p0}, Lhav;-><init>(Lhau;)V

    invoke-virtual {v0, v1}, Lhap;->post(Ljava/lang/Runnable;)Z

    .line 974
    return-void
.end method
