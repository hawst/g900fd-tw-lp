.class public abstract Liqj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lggb;
.implements Lggc;
.implements Liql;


# instance fields
.field private a:Lgfz;

.field private b:Liqm;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Liqm;)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p3, p0, Liqj;->b:Liqm;

    .line 28
    new-instance v0, Lgga;

    invoke-direct {v0, p1}, Lgga;-><init>(Landroid/content/Context;)V

    .line 29
    invoke-virtual {v0, p0}, Lgga;->a(Lggb;)Lgga;

    move-result-object v0

    .line 30
    invoke-virtual {v0, p0}, Lgga;->a(Lggc;)Lgga;

    move-result-object v0

    sget-object v1, Lgzz;->b:Lgfo;

    .line 31
    invoke-virtual {v0, v1}, Lgga;->a(Lgfo;)Lgga;

    move-result-object v0

    .line 32
    invoke-virtual {v0, p2}, Lgga;->a(Ljava/lang/String;)Lgga;

    move-result-object v0

    .line 33
    invoke-virtual {v0}, Lgga;->b()Lgfz;

    move-result-object v0

    iput-object v0, p0, Liqj;->a:Lgfz;

    .line 34
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Liqj;->a:Lgfz;

    invoke-interface {v0}, Lgfz;->a()V

    .line 39
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Liqj;->b:Liqm;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Liqj;->b:Liqm;

    invoke-interface {v0}, Liqm;->b()V

    .line 74
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Liqj;->b:Liqm;

    if-eqz v0, :cond_0

    .line 65
    iget-object v0, p0, Liqj;->b:Liqm;

    invoke-interface {v0}, Liqm;->a()V

    .line 67
    :cond_0
    return-void
.end method

.method public a(Lgfd;)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Liqj;->b:Liqm;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Liqj;->b:Liqm;

    invoke-interface {v0}, Liqm;->b()V

    .line 81
    :cond_0
    return-void
.end method

.method public a(Liqn;)V
    .locals 2

    .prologue
    .line 53
    sget-object v0, Lgzz;->c:Lhab;

    iget-object v1, p0, Liqj;->a:Lgfz;

    invoke-interface {v0, v1}, Lhab;->a(Lgfz;)Lggd;

    move-result-object v0

    new-instance v1, Liqk;

    invoke-direct {v1, p1}, Liqk;-><init>(Liqn;)V

    invoke-interface {v0, v1}, Lggd;->a(Lggg;)V

    .line 60
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Liqj;->a:Lgfz;

    invoke-interface {v0}, Lgfz;->c()V

    .line 44
    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Liqj;->a:Lgfz;

    invoke-interface {v0}, Lgfz;->d()Z

    move-result v0

    return v0
.end method
