.class public final Lhcr;
.super Lhdf;
.source "PG"


# instance fields
.field private g:F

.field private final h:Lhcq;

.field private final i:F


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lhdf;-><init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V

    .line 16
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Lhcr;->g:F

    .line 25
    invoke-virtual {p1}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 27
    new-instance v1, Lhcq;

    invoke-direct {v1, v0}, Lhcq;-><init>(Landroid/content/res/Resources;)V

    iput-object v1, p0, Lhcr;->h:Lhcq;

    .line 29
    iget-object v0, p0, Lhdf;->c:Lhcn;

    invoke-virtual {v0}, Lhcn;->a()I

    move-result v0

    iget-object v1, p0, Lhdf;->c:Lhcn;

    invoke-virtual {v1}, Lhcn;->b()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lhcr;->i:F

    .line 30
    return-void
.end method

.method private static a(Landroid/graphics/RectF;F)F
    .locals 4

    .prologue
    const v3, 0x3f19999a    # 0.6f

    .line 73
    .line 74
    invoke-virtual {p0}, Landroid/graphics/RectF;->width()F

    move-result v0

    mul-float/2addr v0, v3

    float-to-double v0, v0

    .line 75
    invoke-virtual {p0}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v2, v3

    float-to-double v2, v2

    .line 73
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 76
    mul-float/2addr v0, p1

    return v0
.end method


# virtual methods
.method public a(F)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    iget-boolean v0, p0, Lhcr;->d:Z

    if-eqz v0, :cond_0

    .line 34
    invoke-virtual {p0, v1}, Lhcr;->b(Z)V

    .line 37
    :cond_0
    iput p1, p0, Lhcr;->g:F

    .line 39
    iget-boolean v0, p0, Lhcr;->d:Z

    if-eqz v0, :cond_1

    .line 40
    invoke-virtual {p0, v1}, Lhcr;->b(Z)V

    .line 42
    :cond_1
    return-void
.end method

.method protected a(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V
    .locals 4

    .prologue
    .line 59
    iget v0, p0, Lhcr;->g:F

    invoke-static {p2, v0}, Lhcr;->a(Landroid/graphics/RectF;F)F

    move-result v0

    .line 60
    iget v1, p0, Lhcr;->i:F

    cmpg-float v1, v0, v1

    if-gez v1, :cond_0

    .line 70
    :goto_0
    return-void

    .line 64
    :cond_0
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->save(I)I

    .line 65
    invoke-virtual {p1, p2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 67
    iget-object v1, p0, Lhcr;->h:Lhcq;

    iget-object v2, p0, Lhcr;->b:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lhcr;->b:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, p1, v2, v3, v0}, Lhcq;->a(Landroid/graphics/Canvas;FFF)V

    .line 69
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto :goto_0
.end method

.method protected a(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 46
    iget v0, p0, Lhcr;->g:F

    invoke-static {p1, v0}, Lhcr;->a(Landroid/graphics/RectF;F)F

    move-result v3

    .line 47
    iget v0, p0, Lhcr;->i:F

    cmpg-float v0, v3, v0

    if-gez v0, :cond_0

    .line 48
    invoke-virtual {p2}, Landroid/graphics/RectF;->setEmpty()V

    .line 54
    :goto_0
    return v4

    .line 50
    :cond_0
    iget-object v0, p0, Lhcr;->h:Lhcq;

    iget-object v1, p0, Lhcr;->b:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lhcr;->b:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lhcq;->a(FFFZLandroid/graphics/RectF;)V

    goto :goto_0
.end method
