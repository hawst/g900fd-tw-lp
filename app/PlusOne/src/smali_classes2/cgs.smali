.class public final Lcgs;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcgs;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcgs;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    return-void
.end method

.method public static a(Ljava/io/InputStream;Ljava/io/OutputStream;)J
    .locals 8

    .prologue
    const/16 v7, 0x400

    const/4 v6, 0x0

    .line 32
    const-wide/16 v0, 0x0

    .line 34
    new-array v2, v7, [B

    .line 35
    :goto_0
    invoke-virtual {p0, v2, v6, v7}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_0

    .line 36
    invoke-virtual {p1, v2, v6, v3}, Ljava/io/OutputStream;->write([BII)V

    .line 37
    int-to-long v4, v3

    add-long/2addr v0, v4

    goto :goto_0

    .line 39
    :cond_0
    invoke-virtual {p1}, Ljava/io/OutputStream;->flush()V

    .line 40
    return-wide v0
.end method

.method public static a(Ljava/io/InputStream;[BI)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 71
    array-length v0, p1

    add-int/lit8 v2, p2, 0x0

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Buffer is too small for the length/offset requested"

    invoke-static {v0, v2}, Lcec;->a(ZLjava/lang/CharSequence;)V

    move v0, v1

    :goto_1
    if-ltz v1, :cond_1

    if-ge v0, p2, :cond_1

    add-int/lit8 v1, v0, 0x0

    sub-int v2, p2, v0

    invoke-virtual {p0, p1, v1, v2}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    if-ge v0, p2, :cond_2

    sget-object v0, Lcgs;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x68

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Reached end of stream before desired length reached. Wanted "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bytes, but only read 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Unable to read sufficient data"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :cond_2
    return-void
.end method

.method public static a(Ljava/io/InputStream;)[B
    .locals 2

    .prologue
    .line 47
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v1}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 49
    :try_start_0
    invoke-static {p0, v1}, Lcgs;->a(Ljava/io/InputStream;Ljava/io/OutputStream;)J

    .line 50
    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 52
    invoke-static {v1}, Lcek;->a(Ljava/io/Closeable;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcek;->a(Ljava/io/Closeable;)V

    throw v0
.end method

.method public static a(Ljava/io/InputStream;I)[B
    .locals 1

    .prologue
    .line 61
    new-array v0, p1, [B

    .line 62
    invoke-static {p0, v0, p1}, Lcgs;->a(Ljava/io/InputStream;[BI)V

    .line 63
    return-object v0
.end method
