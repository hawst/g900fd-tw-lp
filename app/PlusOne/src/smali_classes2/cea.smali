.class public final enum Lcea;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcea;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lcea;

.field public static final enum b:Lcea;

.field public static final enum c:Lcea;

.field private static final synthetic e:[Lcea;


# instance fields
.field final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 73
    new-instance v0, Lcea;

    const-string v1, "CLOUD"

    const-string v2, "Cloud"

    invoke-direct {v0, v1, v3, v2}, Lcea;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcea;->a:Lcea;

    .line 75
    new-instance v0, Lcea;

    const-string v1, "CLUSTER"

    const-string v2, "Cluster"

    invoke-direct {v0, v1, v4, v2}, Lcea;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcea;->b:Lcea;

    .line 77
    new-instance v0, Lcea;

    const-string v1, "MANUAL"

    const-string v2, "Manual"

    invoke-direct {v0, v1, v5, v2}, Lcea;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    sput-object v0, Lcea;->c:Lcea;

    .line 71
    const/4 v0, 0x3

    new-array v0, v0, [Lcea;

    sget-object v1, Lcea;->a:Lcea;

    aput-object v1, v0, v3

    sget-object v1, Lcea;->b:Lcea;

    aput-object v1, v0, v4

    sget-object v1, Lcea;->c:Lcea;

    aput-object v1, v0, v5

    sput-object v0, Lcea;->e:[Lcea;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 81
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 82
    iput-object p3, p0, Lcea;->d:Ljava/lang/String;

    .line 83
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcea;
    .locals 1

    .prologue
    .line 71
    const-class v0, Lcea;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcea;

    return-object v0
.end method

.method public static values()[Lcea;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcea;->e:[Lcea;

    invoke-virtual {v0}, [Lcea;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcea;

    return-object v0
.end method
