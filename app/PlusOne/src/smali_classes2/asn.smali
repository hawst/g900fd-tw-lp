.class public Lasn;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final b:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation
.end field

.field private d:Z


# direct methods
.method public constructor <init>(Ljava/lang/Class;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {p1}, Lcgf;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lasn;->b:Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Lasn;->b:Ljava/lang/Object;

    iput-object v0, p0, Lasn;->a:Ljava/lang/Object;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lasn;->c:Ljava/util/List;

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lasn;->d:Z

    .line 56
    return-void
.end method

.method private e(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lasn;->a:Ljava/lang/Object;

    if-eq v0, p1, :cond_1

    .line 159
    iget-object v0, p0, Lasn;->a:Ljava/lang/Object;

    iget-object v1, p0, Lasn;->b:Ljava/lang/Object;

    if-eq v0, v1, :cond_0

    .line 160
    iget-object v0, p0, Lasn;->a:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lasn;->a(Ljava/lang/Object;)V

    .line 162
    :cond_0
    iput-object p1, p0, Lasn;->a:Ljava/lang/Object;

    .line 163
    iget-object v0, p0, Lasn;->a:Ljava/lang/Object;

    iget-object v1, p0, Lasn;->b:Ljava/lang/Object;

    if-eq v0, v1, :cond_1

    .line 164
    iget-object v0, p0, Lasn;->a:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lasn;->b(Ljava/lang/Object;)V

    .line 167
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 68
    iget-boolean v0, p0, Lasn;->d:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v2, "Already initialized"

    invoke-static {v0, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Lasn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 70
    if-lez v0, :cond_0

    .line 71
    iget-object v2, p0, Lasn;->c:Ljava/util/List;

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lasn;->e(Ljava/lang/Object;)V

    .line 73
    :cond_0
    iput-boolean v1, p0, Lasn;->d:Z

    .line 74
    return-void

    .line 68
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 155
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 85
    iget-boolean v0, p0, Lasn;->d:Z

    const-string v1, "Already suspended"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 86
    iget-object v0, p0, Lasn;->b:Ljava/lang/Object;

    invoke-direct {p0, v0}, Lasn;->e(Ljava/lang/Object;)V

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Lasn;->d:Z

    .line 88
    return-void
.end method

.method public b(Ljava/lang/Object;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 146
    return-void
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 98
    const-string v0, "ui"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 99
    iget-object v0, p0, Lasn;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "UI already attached"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 100
    iget-object v0, p0, Lasn;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    iget-boolean v0, p0, Lasn;->d:Z

    if-eqz v0, :cond_0

    .line 102
    invoke-direct {p0, p1}, Lasn;->e(Ljava/lang/Object;)V

    .line 104
    :cond_0
    return-void

    .line 99
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Lasn;->a:Ljava/lang/Object;

    iget-object v1, p0, Lasn;->b:Ljava/lang/Object;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 115
    const-string v0, "ui"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 116
    iget-object v0, p0, Lasn;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "UI not attached"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 117
    iget-boolean v0, p0, Lasn;->d:Z

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lasn;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 119
    if-lez v0, :cond_1

    iget-object v1, p0, Lasn;->c:Ljava/util/List;

    add-int/lit8 v0, v0, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    invoke-direct {p0, v0}, Lasn;->e(Ljava/lang/Object;)V

    .line 121
    :cond_0
    return-void

    .line 119
    :cond_1
    iget-object v0, p0, Lasn;->b:Ljava/lang/Object;

    goto :goto_0
.end method
