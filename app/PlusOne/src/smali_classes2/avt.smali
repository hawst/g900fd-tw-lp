.class final Lavt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public A:Z

.field public B:Z

.field public C:Z

.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmu;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbkr;",
            ">;"
        }
    .end annotation
.end field

.field public c:I

.field d:Z

.field public e:Lboi;

.field public f:Lbyx;

.field public g:J

.field public h:Z

.field public i:Z

.field public final j:Landroid/graphics/RectF;

.field public k:Lboh;

.field public l:Lbmk;

.field public m:Z

.field public n:Z

.field public o:Z

.field public p:Lood;

.field q:Z

.field r:Lboh;

.field public s:Lbrc;

.field public t:I

.field public u:Z

.field public v:J

.field public w:J

.field public x:Landroid/graphics/Bitmap;

.field y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbph;",
            ">;"
        }
    .end annotation
.end field

.field z:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 694
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 603
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lavt;->a:Ljava/util/List;

    .line 605
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lavt;->b:Ljava/util/List;

    .line 622
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Lavt;->j:Landroid/graphics/RectF;

    .line 695
    invoke-virtual {p0}, Lavt;->a()V

    .line 696
    return-void
.end method


# virtual methods
.method a()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 701
    iget-object v0, p0, Lavt;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 702
    iget-object v0, p0, Lavt;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 703
    iput v2, p0, Lavt;->c:I

    .line 704
    iput-object v3, p0, Lavt;->l:Lbmk;

    .line 705
    iput-object v3, p0, Lavt;->k:Lboh;

    .line 706
    iput-boolean v2, p0, Lavt;->o:Z

    .line 707
    iput-boolean v1, p0, Lavt;->m:Z

    .line 708
    iput-boolean v1, p0, Lavt;->n:Z

    .line 709
    iput-boolean v2, p0, Lavt;->q:Z

    .line 710
    iput-object v3, p0, Lavt;->e:Lboi;

    .line 711
    iput-boolean v2, p0, Lavt;->d:Z

    .line 712
    iget-object v0, p0, Lavt;->x:Landroid/graphics/Bitmap;

    .line 714
    iput-object v3, p0, Lavt;->x:Landroid/graphics/Bitmap;

    .line 715
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lavt;->g:J

    .line 716
    iput-boolean v2, p0, Lavt;->A:Z

    .line 717
    iput-object v3, p0, Lavt;->p:Lood;

    .line 718
    iput-boolean v2, p0, Lavt;->B:Z

    .line 719
    iput-boolean v2, p0, Lavt;->C:Z

    .line 720
    invoke-virtual {p0}, Lavt;->b()V

    .line 721
    return-void
.end method

.method b()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 724
    iput-boolean v0, p0, Lavt;->i:Z

    .line 725
    iput-boolean v0, p0, Lavt;->h:Z

    .line 726
    return-void
.end method
