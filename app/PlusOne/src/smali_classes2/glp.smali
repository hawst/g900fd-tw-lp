.class public interface abstract Lglp;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a()Landroid/location/Location;
.end method

.method public abstract a(Ljava/lang/String;)Landroid/location/Location;
.end method

.method public abstract a(JZLandroid/app/PendingIntent;)V
.end method

.method public abstract a(Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Landroid/app/PendingIntent;Lglm;Ljava/lang/String;)V
.end method

.method public abstract a(Landroid/location/Location;)V
.end method

.method public abstract a(Landroid/location/Location;I)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/nk;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/nk;Lgrk;)V
.end method

.method public abstract a(Lcom/google/android/gms/internal/nw;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;Lgrk;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/LocationRequest;Lgrk;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/NearbyAlertRequest;Lcom/google/android/gms/internal/nw;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/internal/nw;Lgma;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/PlaceReport;Lcom/google/android/gms/internal/nw;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/PlaceRequest;Lcom/google/android/gms/internal/nw;Landroid/app/PendingIntent;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/UserAddedPlace;Lcom/google/android/gms/internal/nw;Lgma;)V
.end method

.method public abstract a(Lcom/google/android/gms/location/places/UserDataType;Lcom/google/android/gms/maps/model/LatLngBounds;Ljava/util/List;Lcom/google/android/gms/internal/nw;Lgma;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/location/places/UserDataType;",
            "Lcom/google/android/gms/maps/model/LatLngBounds;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/gms/internal/nw;",
            "Lgma;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/internal/nw;Lgma;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLngBounds;ILcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/internal/nw;Lgma;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/LatLngBounds;ILjava/lang/String;Lcom/google/android/gms/location/places/PlaceFilter;Lcom/google/android/gms/internal/nw;Lgma;)V
.end method

.method public abstract a(Lglm;Ljava/lang/String;)V
.end method

.method public abstract a(Lgrk;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/gms/internal/nw;Lgma;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/gms/maps/model/LatLngBounds;Lcom/google/android/gms/location/places/AutocompleteFilter;Lcom/google/android/gms/internal/nw;Lgma;)V
.end method

.method public abstract a(Ljava/util/List;Landroid/app/PendingIntent;Lglm;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/internal/nm;",
            ">;",
            "Landroid/app/PendingIntent;",
            "Lglm;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Z)V
.end method

.method public abstract a([Ljava/lang/String;Lglm;Ljava/lang/String;)V
.end method

.method public abstract b()Landroid/os/IBinder;
.end method

.method public abstract b(Ljava/lang/String;)Lcom/google/android/gms/location/LocationStatus;
.end method

.method public abstract b(Landroid/app/PendingIntent;)V
.end method

.method public abstract b(Lcom/google/android/gms/internal/nw;Landroid/app/PendingIntent;)V
.end method

.method public abstract b(Ljava/lang/String;Lcom/google/android/gms/internal/nw;Lgma;)V
.end method

.method public abstract c()Landroid/os/IBinder;
.end method
