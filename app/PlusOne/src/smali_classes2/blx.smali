.class public final Lblx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbmi;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:[F


# instance fields
.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbly;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lblx;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lblx;->a:Ljava/lang/String;

    .line 24
    const/16 v0, 0xd

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lblx;->b:[F

    return-void

    :array_0
    .array-data 4
        0x0
        0x3cf5c28f    # 0.03f
        0x3da9fbe7    # 0.083f
        0x3e45a1cb    # 0.193f
        0x3f00c49c    # 0.503f
        0x3f3ef9db    # 0.746f
        0x3f591687    # 0.848f
        0x3f683127    # 0.907f
        0x3f71eb85    # 0.945f
        0x3f7851ec    # 0.97f
        0x3f7c6a7f    # 0.986f
        0x3f7eb852    # 0.995f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private constructor <init>(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbly;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 72
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-object p1, p0, Lblx;->c:Ljava/util/List;

    .line 74
    return-void
.end method

.method public static a(Look;)Lbmi;
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 51
    if-eqz p0, :cond_0

    iget-object v2, p0, Look;->g:Looh;

    if-nez v2, :cond_1

    .line 68
    :cond_0
    :goto_0
    return-object v0

    .line 55
    :cond_1
    iget-object v2, p0, Look;->g:Looh;

    iget-object v5, v2, Looh;->d:[Looj;

    .line 56
    array-length v2, v5

    if-eqz v2, :cond_0

    .line 59
    new-instance v6, Ljava/util/ArrayList;

    array-length v0, v5

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    .line 60
    :goto_1
    array-length v2, v5

    if-ge v0, v2, :cond_5

    .line 61
    aget-object v4, v5, v0

    .line 62
    new-instance v7, Lbly;

    iget-object v8, v4, Looj;->b:Loog;

    iget-object v2, v4, Looj;->d:Ljava/lang/Float;

    if-nez v2, :cond_2

    move v2, v3

    :goto_2
    invoke-static {v8, v2}, Lbmj;->a(Loog;F)Lbmj;

    move-result-object v8

    iget-object v2, v4, Looj;->c:Ljava/lang/Float;

    if-nez v2, :cond_3

    move v2, v3

    .line 64
    :goto_3
    iget-object v4, p0, Look;->g:Looh;

    iget v4, v4, Looh;->a:I

    const/4 v9, 0x5

    if-ne v4, v9, :cond_4

    const/4 v4, 0x1

    :goto_4
    invoke-direct {v7, v8, v2, v4}, Lbly;-><init>(Lbmj;FZ)V

    .line 62
    invoke-virtual {v6, v0, v7}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 62
    :cond_2
    iget-object v2, v4, Looj;->d:Ljava/lang/Float;

    .line 63
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    goto :goto_2

    .line 62
    :cond_3
    iget-object v2, v4, Looj;->c:Ljava/lang/Float;

    .line 64
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    goto :goto_3

    :cond_4
    move v4, v1

    goto :goto_4

    .line 68
    :cond_5
    new-instance v0, Lblx;

    invoke-direct {v0, v6}, Lblx;-><init>(Ljava/util/List;)V

    goto :goto_0
.end method


# virtual methods
.method public a(JJ)Lbmj;
    .locals 11

    .prologue
    .line 78
    iget-object v0, p0, Lblx;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const-string v1, "crops.size()"

    const-string v2, "AnimatedCropSequence should have at least one crop."

    if-gtz v0, :cond_0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " must be positive: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {v0, v2}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    .line 80
    :cond_0
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    .line 82
    iget-object v0, p0, Lblx;->c:Ljava/util/List;

    iget-object v1, p0, Lblx;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbly;

    iget-object v0, v0, Lbly;->a:Lbmj;

    .line 113
    :goto_0
    return-object v0

    .line 84
    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_2

    .line 86
    iget-object v0, p0, Lblx;->c:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbly;

    iget-object v0, v0, Lbly;->a:Lbmj;

    goto :goto_0

    .line 89
    :cond_2
    const-wide/16 v2, 0x0

    .line 90
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lblx;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 91
    iget-object v0, p0, Lblx;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbly;

    .line 92
    iget v4, v0, Lbly;->b:F

    long-to-float v5, p3

    mul-float/2addr v4, v5

    float-to-long v6, v4

    .line 93
    iget-boolean v4, v0, Lbly;->c:Z

    if-eqz v4, :cond_3

    const-wide/32 v4, 0x61a80

    :goto_2
    const-wide/16 v8, 0x2

    div-long v8, v4, v8

    .line 94
    add-long v4, v2, v6

    sub-long/2addr v4, v8

    cmp-long v4, p1, v4

    if-gez v4, :cond_4

    .line 95
    iget-object v5, v0, Lbly;->a:Lbmj;

    .line 96
    iget-boolean v0, v0, Lbly;->c:Z

    if-eqz v0, :cond_6

    sub-long v6, p1, v2

    cmp-long v0, v6, v8

    if-gez v0, :cond_6

    if-lez v1, :cond_6

    .line 100
    iget-object v0, p0, Lblx;->c:Ljava/util/List;

    add-int/lit8 v1, v1, -0x1

    .line 101
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbly;

    iget-object v6, v0, Lbly;->a:Lbmj;

    long-to-float v0, p1

    long-to-float v1, v2

    sub-float/2addr v0, v1

    long-to-float v1, v8

    add-float/2addr v0, v1

    const v1, 0x48c35000    # 400000.0f

    div-float/2addr v0, v1

    sget-object v1, Lblx;->b:[F

    .line 102
    invoke-static {v0, v1}, Lbag;->a(F[F)F

    move-result v7

    .line 100
    const/high16 v0, 0x3f800000    # 1.0f

    sub-float v8, v0, v7

    new-instance v0, Lbmj;

    iget v1, v6, Lbmj;->a:F

    mul-float/2addr v1, v8

    iget v2, v5, Lbmj;->a:F

    mul-float/2addr v2, v7

    add-float/2addr v1, v2

    iget v2, v6, Lbmj;->b:F

    mul-float/2addr v2, v8

    iget v3, v5, Lbmj;->b:F

    mul-float/2addr v3, v7

    add-float/2addr v2, v3

    iget v3, v6, Lbmj;->c:F

    mul-float/2addr v3, v8

    iget v4, v5, Lbmj;->c:F

    mul-float/2addr v4, v7

    add-float/2addr v3, v4

    iget v4, v6, Lbmj;->d:F

    mul-float/2addr v4, v8

    iget v9, v5, Lbmj;->d:F

    mul-float/2addr v9, v7

    add-float/2addr v4, v9

    iget v6, v6, Lbmj;->e:F

    mul-float/2addr v6, v8

    iget v5, v5, Lbmj;->e:F

    mul-float/2addr v5, v7

    add-float/2addr v5, v6

    invoke-direct/range {v0 .. v5}, Lbmj;-><init>(FFFFF)V

    goto/16 :goto_0

    .line 93
    :cond_3
    const-wide/16 v4, 0x0

    goto :goto_2

    .line 108
    :cond_4
    add-long/2addr v2, v6

    .line 90
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    .line 111
    :cond_5
    sget-object v0, Lblx;->a:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x58

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Not enough subclips: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " : "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " in "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 113
    iget-object v0, p0, Lblx;->c:Ljava/util/List;

    iget-object v1, p0, Lblx;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbly;

    iget-object v0, v0, Lbly;->a:Lbmj;

    goto/16 :goto_0

    :cond_6
    move-object v0, v5

    goto/16 :goto_0
.end method
