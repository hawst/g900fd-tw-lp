.class public final Lkad;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljxx;


# instance fields
.field final a:Landroid/content/Context;

.field b:Lkzw;

.field c:Ljava/lang/String;

.field d:Ljxo;

.field final e:Lhee;

.field f:Z

.field private g:Ljxy;

.field private h:Lkbf;

.field private i:Lkbb;

.field private j:Lkbe;

.field private k:Lkbd;

.field private l:Lkbt;

.field private m:Lkbc;

.field private n:Landroid/widget/TextView;

.field private o:Lkak;

.field private p:Lkah;

.field private q:Lkaf;

.field private r:Lkag;

.field private s:Lhob;

.field private t:Z

.field private u:Ljxz;

.field private v:Ljya;

.field private w:Ljxp;

.field private x:Ljxs;

.field private y:Ljxr;

.field private z:Liwk;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    iput-object p1, p0, Lkad;->a:Landroid/content/Context;

    .line 94
    const-class v0, Lhee;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lkad;->e:Lhee;

    .line 95
    return-void
.end method

.method private s()Lkbf;
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lkad;->h:Lkbf;

    if-nez v0, :cond_0

    .line 131
    new-instance v0, Lkbf;

    iget-object v1, p0, Lkad;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lkbf;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lkad;->h:Lkbf;

    .line 133
    :cond_0
    iget-object v0, p0, Lkad;->h:Lkbf;

    return-object v0
.end method

.method private t()Lkbc;
    .locals 2

    .prologue
    .line 137
    iget-object v0, p0, Lkad;->m:Lkbc;

    if-nez v0, :cond_0

    .line 138
    new-instance v0, Lkbc;

    iget-object v1, p0, Lkad;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lkbc;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lkad;->m:Lkbc;

    .line 140
    iget-object v0, p0, Lkad;->m:Lkbc;

    .line 141
    invoke-virtual {p0}, Lkad;->m()Lkah;

    move-result-object v1

    .line 140
    invoke-virtual {v0, v1}, Lkbc;->b(Landroid/view/View$OnClickListener;)V

    .line 142
    iget-object v0, p0, Lkad;->m:Lkbc;

    .line 143
    invoke-virtual {p0}, Lkad;->l()Lkaf;

    move-result-object v1

    .line 142
    invoke-virtual {v0, v1}, Lkbc;->a(Landroid/view/View$OnClickListener;)V

    .line 144
    iget-object v0, p0, Lkad;->m:Lkbc;

    .line 145
    invoke-virtual {p0}, Lkad;->k()Lkag;

    move-result-object v1

    .line 144
    invoke-virtual {v0, v1}, Lkbc;->a(Landroid/view/View$OnLongClickListener;)V

    .line 146
    iget-object v0, p0, Lkad;->m:Lkbc;

    invoke-virtual {p0}, Lkad;->p()Ljya;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkbc;->a(Ljya;)V

    .line 148
    :cond_0
    iget-object v0, p0, Lkad;->m:Lkbc;

    return-object v0
.end method

.method private u()Lkbt;
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lkad;->l:Lkbt;

    if-nez v0, :cond_0

    .line 153
    new-instance v0, Lkbt;

    iget-object v1, p0, Lkad;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lkbt;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lkad;->l:Lkbt;

    .line 154
    iget-object v0, p0, Lkad;->l:Lkbt;

    .line 155
    invoke-virtual {p0}, Lkad;->m()Lkah;

    move-result-object v1

    .line 154
    invoke-virtual {v0, v1}, Lkbt;->b(Landroid/view/View$OnClickListener;)V

    .line 156
    iget-object v0, p0, Lkad;->l:Lkbt;

    .line 157
    invoke-virtual {p0}, Lkad;->l()Lkaf;

    move-result-object v1

    .line 156
    invoke-virtual {v0, v1}, Lkbt;->a(Landroid/view/View$OnClickListener;)V

    .line 158
    iget-object v0, p0, Lkad;->l:Lkbt;

    .line 159
    invoke-virtual {p0}, Lkad;->k()Lkag;

    move-result-object v1

    .line 158
    invoke-virtual {v0, v1}, Lkbt;->a(Landroid/view/View$OnLongClickListener;)V

    .line 160
    iget-object v0, p0, Lkad;->l:Lkbt;

    invoke-virtual {p0}, Lkad;->p()Ljya;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkbt;->a(Ljya;)V

    .line 162
    :cond_0
    iget-object v0, p0, Lkad;->l:Lkbt;

    return-object v0
.end method

.method private v()Lkbd;
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Lkad;->k:Lkbd;

    if-nez v0, :cond_0

    .line 167
    new-instance v0, Lkbd;

    iget-object v1, p0, Lkad;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lkbd;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lkad;->k:Lkbd;

    .line 168
    iget-object v0, p0, Lkad;->k:Lkbd;

    .line 169
    invoke-virtual {p0}, Lkad;->m()Lkah;

    move-result-object v1

    .line 168
    invoke-virtual {v0, v1}, Lkbd;->b(Landroid/view/View$OnClickListener;)V

    .line 170
    iget-object v0, p0, Lkad;->k:Lkbd;

    .line 171
    invoke-virtual {p0}, Lkad;->l()Lkaf;

    move-result-object v1

    .line 170
    invoke-virtual {v0, v1}, Lkbd;->a(Landroid/view/View$OnClickListener;)V

    .line 172
    iget-object v0, p0, Lkad;->k:Lkbd;

    .line 173
    invoke-virtual {p0}, Lkad;->k()Lkag;

    move-result-object v1

    .line 172
    invoke-virtual {v0, v1}, Lkbd;->a(Landroid/view/View$OnLongClickListener;)V

    .line 174
    iget-object v0, p0, Lkad;->k:Lkbd;

    invoke-virtual {p0}, Lkad;->p()Ljya;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkbd;->a(Ljya;)V

    .line 176
    :cond_0
    iget-object v0, p0, Lkad;->k:Lkbd;

    return-object v0
.end method

.method private w()Lkbe;
    .locals 2

    .prologue
    .line 180
    iget-object v0, p0, Lkad;->j:Lkbe;

    if-nez v0, :cond_0

    .line 181
    new-instance v0, Lkbe;

    iget-object v1, p0, Lkad;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lkbe;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lkad;->j:Lkbe;

    .line 182
    iget-object v0, p0, Lkad;->j:Lkbe;

    .line 183
    invoke-virtual {p0}, Lkad;->m()Lkah;

    move-result-object v1

    .line 182
    invoke-virtual {v0, v1}, Lkbe;->b(Landroid/view/View$OnClickListener;)V

    .line 184
    iget-object v0, p0, Lkad;->j:Lkbe;

    .line 185
    invoke-virtual {p0}, Lkad;->l()Lkaf;

    move-result-object v1

    .line 184
    invoke-virtual {v0, v1}, Lkbe;->a(Landroid/view/View$OnClickListener;)V

    .line 186
    iget-object v0, p0, Lkad;->j:Lkbe;

    .line 187
    invoke-virtual {p0}, Lkad;->k()Lkag;

    move-result-object v1

    .line 186
    invoke-virtual {v0, v1}, Lkbe;->a(Landroid/view/View$OnLongClickListener;)V

    .line 188
    iget-object v0, p0, Lkad;->j:Lkbe;

    invoke-virtual {p0}, Lkad;->p()Ljya;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkbe;->a(Ljya;)V

    .line 190
    :cond_0
    iget-object v0, p0, Lkad;->j:Lkbe;

    return-object v0
.end method

.method private x()Lkbb;
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lkad;->i:Lkbb;

    if-nez v0, :cond_0

    .line 195
    new-instance v0, Lkbb;

    iget-object v1, p0, Lkad;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lkbb;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lkad;->i:Lkbb;

    .line 196
    iget-object v0, p0, Lkad;->i:Lkbb;

    .line 197
    invoke-virtual {p0}, Lkad;->m()Lkah;

    move-result-object v1

    .line 196
    invoke-virtual {v0, v1}, Lkbb;->b(Landroid/view/View$OnClickListener;)V

    .line 198
    iget-object v0, p0, Lkad;->i:Lkbb;

    invoke-virtual {p0}, Lkad;->p()Ljya;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkbb;->a(Ljya;)V

    .line 200
    :cond_0
    iget-object v0, p0, Lkad;->i:Lkbb;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 318
    invoke-direct {p0}, Lkad;->s()Lkbf;

    move-result-object v0

    invoke-virtual {v0}, Lkbf;->a()V

    .line 319
    iget-object v0, p0, Lkad;->b:Lkzw;

    invoke-virtual {v0}, Lkzw;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 320
    invoke-direct {p0}, Lkad;->x()Lkbb;

    move-result-object v0

    invoke-virtual {v0}, Lkbb;->a()V

    .line 330
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lkad;->n()Ljxz;

    move-result-object v0

    invoke-virtual {p0}, Lkad;->o()Lhob;

    move-result-object v1

    invoke-interface {v0, v1}, Ljxz;->b(Lhob;)V

    .line 331
    return-void

    .line 321
    :cond_1
    iget-object v0, p0, Lkad;->b:Lkzw;

    invoke-virtual {v0}, Lkzw;->n()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 322
    invoke-direct {p0}, Lkad;->w()Lkbe;

    move-result-object v0

    invoke-virtual {v0}, Lkbe;->a()V

    goto :goto_0

    .line 323
    :cond_2
    iget-object v0, p0, Lkad;->b:Lkzw;

    invoke-virtual {v0}, Lkzw;->m()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 324
    invoke-direct {p0}, Lkad;->v()Lkbd;

    move-result-object v0

    invoke-virtual {v0}, Lkbd;->a()V

    goto :goto_0

    .line 325
    :cond_3
    iget-object v0, p0, Lkad;->b:Lkzw;

    invoke-virtual {v0}, Lkzw;->l()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 326
    invoke-direct {p0}, Lkad;->u()Lkbt;

    move-result-object v0

    invoke-virtual {v0}, Lkbt;->a()V

    goto :goto_0

    .line 327
    :cond_4
    iget-object v0, p0, Lkad;->b:Lkzw;

    invoke-virtual {v0}, Lkzw;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    invoke-direct {p0}, Lkad;->t()Lkbc;

    move-result-object v0

    invoke-virtual {v0}, Lkbc;->a()V

    goto :goto_0
.end method

.method a(I)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 378
    iget-object v0, p0, Lkad;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Llsa;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 379
    iget-object v0, p0, Lkad;->a:Landroid/content/Context;

    invoke-static {v0}, Lkbr;->a(Landroid/content/Context;)Lkbr;

    move-result-object v0

    iget-object v1, p0, Lkad;->a:Landroid/content/Context;

    const v2, 0x7f0a016e

    .line 380
    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 379
    invoke-virtual {v0, v1}, Lkbr;->a(Ljava/lang/String;)V

    .line 400
    :cond_0
    :goto_0
    return-void

    .line 383
    :cond_1
    iget-object v0, p0, Lkad;->z:Liwk;

    if-nez v0, :cond_2

    new-instance v0, Liwk;

    iget-object v1, p0, Lkad;->a:Landroid/content/Context;

    iget-object v2, p0, Lkad;->e:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {v0, v1, v2}, Liwk;-><init>(Landroid/content/Context;I)V

    const-class v1, Lixj;

    invoke-virtual {v0, v1}, Liwk;->a(Ljava/lang/Class;)Liwk;

    move-result-object v0

    iput-object v0, p0, Lkad;->z:Liwk;

    :cond_2
    iget-object v0, p0, Lkad;->z:Liwk;

    invoke-virtual {v0}, Liwk;->a()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v1, p0, Lkad;->a:Landroid/content/Context;

    iget-object v2, p0, Lkad;->z:Liwk;

    invoke-virtual {v2}, Liwk;->b()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    :cond_3
    if-eqz v0, :cond_0

    .line 386
    invoke-virtual {p0}, Lkad;->c()Ljxw;

    move-result-object v0

    check-cast v0, Lkbl;

    .line 387
    invoke-virtual {v0}, Lkbl;->l()Z

    move-result v1

    if-nez v1, :cond_0

    .line 390
    iget-object v1, p0, Lkad;->b:Lkzw;

    invoke-virtual {v1}, Lkzw;->f()I

    move-result v1

    .line 391
    invoke-virtual {p0, p1}, Lkad;->b(I)Z

    move-result v2

    .line 392
    iput-boolean v3, p0, Lkad;->f:Z

    .line 393
    invoke-virtual {v0, v3}, Lkbl;->a(Z)V

    .line 394
    if-eqz v2, :cond_4

    .line 395
    invoke-virtual {p0}, Lkad;->n()Ljxz;

    move-result-object v0

    iget-object v2, p0, Lkad;->c:Ljava/lang/String;

    iget-object v3, p0, Lkad;->b:Lkzw;

    iget-boolean v4, p0, Lkad;->t:Z

    invoke-interface {v0, v2, v3, v1, v4}, Ljxz;->b(Ljava/lang/String;Lkzw;IZ)V

    goto :goto_0

    .line 398
    :cond_4
    invoke-virtual {p0}, Lkad;->n()Ljxz;

    move-result-object v0

    iget-object v2, p0, Lkad;->c:Ljava/lang/String;

    iget-object v3, p0, Lkad;->b:Lkzw;

    iget-boolean v4, p0, Lkad;->t:Z

    invoke-interface {v0, v2, v3, v1, v4}, Ljxz;->a(Ljava/lang/String;Lkzw;IZ)V

    goto :goto_0
.end method

.method public a(Ljxu;)V
    .locals 1

    .prologue
    .line 313
    invoke-virtual {p0}, Lkad;->c()Ljxw;

    move-result-object v0

    check-cast v0, Lkbl;

    invoke-virtual {v0, p1}, Lkbl;->a(Ljxu;)V

    .line 314
    return-void
.end method

.method public a(Lkzw;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 205
    if-nez p1, :cond_1

    .line 237
    :cond_0
    :goto_0
    return-void

    .line 208
    :cond_1
    iput-object p1, p0, Lkad;->b:Lkzw;

    .line 209
    iput-object p2, p0, Lkad;->c:Ljava/lang/String;

    .line 210
    iput-boolean p3, p0, Lkad;->t:Z

    .line 212
    invoke-direct {p0}, Lkad;->s()Lkbf;

    move-result-object v1

    .line 213
    invoke-virtual {v1, p1}, Lkbf;->a(Lkzw;)Ljxw;

    .line 214
    if-eqz p3, :cond_4

    move-object v0, v2

    :goto_1
    invoke-virtual {v1, v0}, Lkbf;->a(Landroid/view/View$OnClickListener;)V

    .line 216
    if-nez p3, :cond_5

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v1, v0}, Lkbf;->a(Z)V

    .line 219
    invoke-virtual {p1}, Lkzw;->o()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 220
    invoke-direct {p0}, Lkad;->x()Lkbb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkbb;->a(Lkzw;)Ljxw;

    move-result-object v1

    .line 232
    :goto_3
    iget-object v0, p0, Lkad;->g:Ljxy;

    if-nez v0, :cond_2

    iget-object v0, p0, Lkad;->a:Landroid/content/Context;

    const-class v3, Ljxy;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxy;

    iput-object v0, p0, Lkad;->g:Ljxy;

    :cond_2
    iget-object v0, p0, Lkad;->g:Ljxy;

    .line 233
    invoke-interface {v0, p2}, Ljxy;->b(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lkad;->f:Z

    move-object v0, v1

    .line 234
    check-cast v0, Lkbl;

    iget-boolean v1, p0, Lkad;->f:Z

    invoke-virtual {v0, v1}, Lkbl;->a(Z)V

    .line 235
    invoke-virtual {p0}, Lkad;->n()Ljxz;

    move-result-object v0

    invoke-virtual {p0}, Lkad;->o()Lhob;

    move-result-object v1

    invoke-interface {v0, v1}, Ljxz;->a(Lhob;)V

    .line 236
    iget-object v0, p0, Lkad;->x:Ljxs;

    if-nez v0, :cond_3

    iget-object v0, p0, Lkad;->a:Landroid/content/Context;

    const-class v1, Ljxs;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxs;

    iput-object v0, p0, Lkad;->x:Ljxs;

    :cond_3
    iget-object v0, p0, Lkad;->x:Ljxs;

    if-nez p3, :cond_a

    :goto_4
    invoke-interface {v0, p0}, Ljxs;->a(Ljxx;)V

    goto :goto_0

    .line 215
    :cond_4
    invoke-virtual {p0}, Lkad;->j()Lkak;

    move-result-object v0

    goto :goto_1

    .line 216
    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 221
    :cond_6
    invoke-virtual {p1}, Lkzw;->n()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 222
    invoke-direct {p0}, Lkad;->w()Lkbe;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkbe;->a(Lkzw;)Ljxw;

    move-result-object v1

    goto :goto_3

    .line 223
    :cond_7
    invoke-virtual {p1}, Lkzw;->m()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 224
    invoke-direct {p0}, Lkad;->v()Lkbd;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkbd;->a(Lkzw;)Ljxw;

    move-result-object v1

    goto :goto_3

    .line 225
    :cond_8
    invoke-virtual {p1}, Lkzw;->l()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 226
    invoke-direct {p0}, Lkad;->u()Lkbt;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkbt;->a(Lkzw;)Ljxw;

    move-result-object v1

    goto :goto_3

    .line 227
    :cond_9
    invoke-virtual {p1}, Lkzw;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    invoke-direct {p0}, Lkad;->t()Lkbc;

    move-result-object v0

    invoke-virtual {v0, p1}, Lkbc;->a(Lkzw;)Ljxw;

    move-result-object v1

    goto/16 :goto_3

    :cond_a
    move-object p0, v2

    .line 236
    goto :goto_4
.end method

.method public b()Ljxw;
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lkad;->b:Lkzw;

    if-nez v0, :cond_0

    .line 242
    const/4 v0, 0x0

    .line 244
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0}, Lkad;->s()Lkbf;

    move-result-object v0

    goto :goto_0
.end method

.method b(I)Z
    .locals 3

    .prologue
    .line 403
    invoke-virtual {p0}, Lkad;->c()Ljxw;

    move-result-object v0

    check-cast v0, Lkbl;

    .line 404
    invoke-virtual {v0, p1}, Lkbl;->n(I)Z

    move-result v1

    .line 405
    invoke-virtual {v0}, Lkbl;->k()Z

    move-result v2

    .line 406
    invoke-virtual {v0, p1}, Lkbl;->m(I)V

    .line 408
    if-nez v1, :cond_0

    if-nez v2, :cond_1

    .line 409
    :cond_0
    invoke-direct {p0}, Lkad;->s()Lkbf;

    move-result-object v0

    iget-object v2, p0, Lkad;->a:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lkbf;->b(Landroid/content/Context;)V

    .line 411
    :cond_1
    return v1
.end method

.method public c()Ljxw;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 249
    iget-object v1, p0, Lkad;->b:Lkzw;

    if-nez v1, :cond_1

    .line 267
    :cond_0
    :goto_0
    return-object v0

    .line 252
    :cond_1
    iget-object v1, p0, Lkad;->b:Lkzw;

    invoke-virtual {v1}, Lkzw;->o()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 253
    invoke-direct {p0}, Lkad;->x()Lkbb;

    move-result-object v0

    goto :goto_0

    .line 255
    :cond_2
    iget-object v1, p0, Lkad;->b:Lkzw;

    invoke-virtual {v1}, Lkzw;->n()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 256
    invoke-direct {p0}, Lkad;->w()Lkbe;

    move-result-object v0

    goto :goto_0

    .line 258
    :cond_3
    iget-object v1, p0, Lkad;->b:Lkzw;

    invoke-virtual {v1}, Lkzw;->m()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 259
    invoke-direct {p0}, Lkad;->v()Lkbd;

    move-result-object v0

    goto :goto_0

    .line 261
    :cond_4
    iget-object v1, p0, Lkad;->b:Lkzw;

    invoke-virtual {v1}, Lkzw;->l()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 262
    invoke-direct {p0}, Lkad;->u()Lkbt;

    move-result-object v0

    goto :goto_0

    .line 264
    :cond_5
    iget-object v1, p0, Lkad;->b:Lkzw;

    invoke-virtual {v1}, Lkzw;->k()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 265
    invoke-direct {p0}, Lkad;->t()Lkbc;

    move-result-object v0

    goto :goto_0
.end method

.method public d()Landroid/view/View;
    .locals 2

    .prologue
    .line 272
    iget-object v0, p0, Lkad;->n:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 273
    new-instance v0, Landroid/widget/TextView;

    iget-object v1, p0, Lkad;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lkad;->n:Landroid/widget/TextView;

    .line 274
    iget-object v0, p0, Lkad;->n:Landroid/widget/TextView;

    const v1, 0x7f0a018b

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 276
    :cond_0
    iget-object v0, p0, Lkad;->n:Landroid/widget/TextView;

    return-object v0
.end method

.method public e()Lkzw;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lkad;->b:Lkzw;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lkad;->c:Ljava/lang/String;

    return-object v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 291
    iget-boolean v0, p0, Lkad;->t:Z

    return v0
.end method

.method public h()Ljava/lang/String;
    .locals 6

    .prologue
    .line 301
    iget-object v0, p0, Lkad;->b:Lkzw;

    if-nez v0, :cond_0

    .line 302
    const/4 v0, 0x0

    .line 307
    :goto_0
    return-object v0

    .line 304
    :cond_0
    iget-object v0, p0, Lkad;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 305
    iget-object v1, p0, Lkad;->b:Lkzw;

    invoke-virtual {v1}, Lkzw;->r()I

    move-result v1

    const/16 v2, 0x9

    if-ne v1, v2, :cond_1

    const v1, 0x7f0a018c

    .line 306
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const v1, 0x7f0a018d

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lkad;->e:Lhee;

    .line 308
    invoke-interface {v4}, Lhee;->g()Lhej;

    move-result-object v4

    const-string v5, "domain_name"

    invoke-interface {v4, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 307
    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 296
    iget-boolean v0, p0, Lkad;->f:Z

    return v0
.end method

.method j()Lkak;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lkad;->o:Lkak;

    if-nez v0, :cond_0

    .line 100
    new-instance v0, Lkak;

    invoke-direct {v0, p0}, Lkak;-><init>(Lkad;)V

    iput-object v0, p0, Lkad;->o:Lkak;

    .line 102
    :cond_0
    iget-object v0, p0, Lkad;->o:Lkak;

    return-object v0
.end method

.method k()Lkag;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lkad;->r:Lkag;

    if-nez v0, :cond_0

    .line 108
    new-instance v0, Lkag;

    invoke-direct {v0, p0}, Lkag;-><init>(Lkad;)V

    iput-object v0, p0, Lkad;->r:Lkag;

    .line 110
    :cond_0
    iget-object v0, p0, Lkad;->r:Lkag;

    return-object v0
.end method

.method l()Lkaf;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lkad;->q:Lkaf;

    if-nez v0, :cond_0

    .line 116
    new-instance v0, Lkaf;

    invoke-direct {v0, p0}, Lkaf;-><init>(Lkad;)V

    iput-object v0, p0, Lkad;->q:Lkaf;

    .line 118
    :cond_0
    iget-object v0, p0, Lkad;->q:Lkaf;

    return-object v0
.end method

.method m()Lkah;
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lkad;->p:Lkah;

    if-nez v0, :cond_0

    .line 124
    new-instance v0, Lkah;

    invoke-direct {v0, p0}, Lkah;-><init>(Lkad;)V

    iput-object v0, p0, Lkad;->p:Lkah;

    .line 126
    :cond_0
    iget-object v0, p0, Lkad;->p:Lkah;

    return-object v0
.end method

.method n()Ljxz;
    .locals 2

    .prologue
    .line 341
    iget-object v0, p0, Lkad;->u:Ljxz;

    if-nez v0, :cond_0

    .line 342
    iget-object v0, p0, Lkad;->a:Landroid/content/Context;

    const-class v1, Ljxz;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxz;

    iput-object v0, p0, Lkad;->u:Ljxz;

    .line 344
    :cond_0
    iget-object v0, p0, Lkad;->u:Ljxz;

    return-object v0
.end method

.method o()Lhob;
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lkad;->s:Lhob;

    if-nez v0, :cond_0

    .line 350
    new-instance v0, Lkal;

    invoke-direct {v0, p0}, Lkal;-><init>(Lkad;)V

    iput-object v0, p0, Lkad;->s:Lhob;

    .line 352
    :cond_0
    iget-object v0, p0, Lkad;->s:Lhob;

    return-object v0
.end method

.method p()Ljya;
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Lkad;->v:Ljya;

    if-nez v0, :cond_0

    .line 358
    new-instance v0, Lkam;

    invoke-direct {v0, p0}, Lkam;-><init>(Lkad;)V

    iput-object v0, p0, Lkad;->v:Ljya;

    .line 360
    :cond_0
    iget-object v0, p0, Lkad;->v:Ljya;

    return-object v0
.end method

.method public q()Ljxr;
    .locals 1

    .prologue
    .line 433
    iget-object v0, p0, Lkad;->y:Ljxr;

    if-nez v0, :cond_0

    .line 434
    new-instance v0, Lkai;

    invoke-direct {v0, p0}, Lkai;-><init>(Lkad;)V

    iput-object v0, p0, Lkad;->y:Ljxr;

    .line 436
    :cond_0
    iget-object v0, p0, Lkad;->y:Ljxr;

    return-object v0
.end method

.method public r()Ljxp;
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lkad;->w:Ljxp;

    if-nez v0, :cond_0

    .line 441
    new-instance v0, Lkae;

    invoke-direct {v0, p0}, Lkae;-><init>(Lkad;)V

    iput-object v0, p0, Lkad;->w:Ljxp;

    .line 443
    :cond_0
    iget-object v0, p0, Lkad;->w:Ljxp;

    return-object v0
.end method
