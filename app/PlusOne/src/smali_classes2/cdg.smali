.class public Lcdg;
.super Lccv;
.source "PG"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/view/View;",
            "Lcdi;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/Runnable;

.field private e:I

.field private f:Landroid/graphics/Rect;

.field private g:I

.field private h:I

.field private i:I

.field private j:Landroid/animation/AnimatorSet;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcdg;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcdg;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcci;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lccv;-><init>(Lcci;)V

    .line 34
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcdg;->c:Ljava/util/Map;

    .line 37
    new-instance v0, Lcdh;

    invoke-direct {v0, p0}, Lcdh;-><init>(Lcdg;)V

    iput-object v0, p0, Lcdg;->d:Ljava/lang/Runnable;

    .line 55
    return-void
.end method

.method static synthetic a(Lcdg;I)I
    .locals 0

    .prologue
    .line 24
    iput p1, p0, Lcdg;->e:I

    return p1
.end method

.method static synthetic a(Lcdg;Landroid/animation/AnimatorSet;)Landroid/animation/AnimatorSet;
    .locals 0

    .prologue
    .line 24
    iput-object p1, p0, Lcdg;->j:Landroid/animation/AnimatorSet;

    return-object p1
.end method

.method static synthetic a(Lcdg;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcdg;->c:Ljava/util/Map;

    return-object v0
.end method

.method private b(Lccj;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 154
    iget-object v0, p0, Lcdg;->j:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isStarted()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    move v1, v0

    .line 155
    :goto_0
    iget-boolean v0, p1, Lccj;->f:Z

    if-eqz v0, :cond_0

    .line 156
    sget-object v0, Lcdg;->b:Ljava/lang/String;

    iget v0, p0, Lcdg;->i:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x3f

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "updateAnimations() KeyPosition = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " resetViews = "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 160
    :cond_0
    new-instance v3, Landroid/graphics/Rect;

    iget-object v0, p0, Lcdg;->f:Landroid/graphics/Rect;

    invoke-direct {v3, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 161
    iget-object v0, p1, Lccj;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    neg-int v0, v0

    iget-object v4, p1, Lccj;->e:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    neg-int v4, v4

    invoke-virtual {v3, v0, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 163
    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v4

    :goto_1
    if-ge v2, v4, :cond_3

    .line 164
    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    iget-object v5, v0, Lccg;->a:Landroid/view/View;

    .line 165
    iget-object v0, p0, Lcdg;->c:Ljava/util/Map;

    invoke-interface {v0, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdi;

    .line 166
    if-eqz v0, :cond_1

    .line 167
    invoke-virtual {v0, p1, v5, v3, v1}, Lcdi;->a(Lccj;Landroid/view/View;Landroid/graphics/Rect;Z)V

    .line 163
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v1, v2

    .line 154
    goto :goto_0

    .line 170
    :cond_3
    return-void
.end method


# virtual methods
.method public a(IIILandroid/graphics/Rect;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 69
    const-string v0, "startDelayMs"

    invoke-static {p1, v0, v1}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lcdg;->g:I

    .line 70
    const-string v0, "durationMs"

    invoke-static {p2, v0, v1}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lcdg;->h:I

    .line 71
    const-string v0, "keyPosition"

    invoke-static {p3, v0, v1}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lcdg;->i:I

    .line 72
    const-string v0, "startRect"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Rect;

    iput-object v0, p0, Lcdg;->f:Landroid/graphics/Rect;

    .line 73
    const/4 v0, 0x1

    iput v0, p0, Lcdg;->e:I

    .line 74
    iget-object v0, p0, Lcdg;->a:Lcci;

    invoke-interface {v0}, Lcci;->d()V

    .line 75
    return-void
.end method

.method public a(Lccj;)V
    .locals 12

    .prologue
    const/16 v11, 0x4b

    const/4 v10, 0x2

    const/4 v2, 0x0

    .line 81
    iget v0, p0, Lcdg;->e:I

    packed-switch v0, :pswitch_data_0

    .line 99
    :goto_0
    :pswitch_0
    return-void

    .line 86
    :pswitch_1
    invoke-virtual {p1, v11}, Lccj;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 87
    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    move v1, v2

    :goto_1
    if-ge v1, v3, :cond_0

    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    iget v5, p0, Lcdg;->i:I

    sub-int v4, v5, v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    rsub-int v4, v4, 0x7fff

    int-to-short v4, v4

    iput-short v4, v0, Lccg;->d:S

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 88
    :cond_0
    iget-boolean v0, p1, Lccj;->f:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcdg;->b:Ljava/lang/String;

    iget v0, p0, Lcdg;->i:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x2b

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "startAnimations() KeyPosition = "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_1
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    iput-object v0, p0, Lcdg;->j:Landroid/animation/AnimatorSet;

    const/4 v0, 0x0

    new-instance v4, Landroid/graphics/Rect;

    iget-object v1, p0, Lcdg;->f:Landroid/graphics/Rect;

    invoke-direct {v4, v1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iget-object v1, p1, Lccj;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    iget-object v3, p1, Lccj;->e:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    neg-int v3, v3

    invoke-virtual {v4, v1, v3}, Landroid/graphics/Rect;->offset(II)V

    iget-object v1, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v5

    move v1, v2

    move-object v3, v0

    :goto_2
    if-ge v1, v5, :cond_2

    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    iget-object v0, v0, Lccg;->a:Landroid/view/View;

    new-instance v6, Lcdi;

    invoke-direct {v6, p1, v0, v4}, Lcdi;-><init>(Lccj;Landroid/view/View;Landroid/graphics/Rect;)V

    iget-object v7, p0, Lcdg;->c:Ljava/util/Map;

    invoke-interface {v7, v0, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const/4 v7, 0x4

    new-array v7, v7, [Landroid/animation/PropertyValuesHolder;

    iget-object v8, v6, Lcdi;->a:Landroid/animation/PropertyValuesHolder;

    aput-object v8, v7, v2

    const/4 v8, 0x1

    iget-object v9, v6, Lcdi;->b:Landroid/animation/PropertyValuesHolder;

    aput-object v9, v7, v8

    iget-object v8, v6, Lcdi;->c:Landroid/animation/PropertyValuesHolder;

    aput-object v8, v7, v10

    const/4 v8, 0x3

    iget-object v6, v6, Lcdi;->d:Landroid/animation/PropertyValuesHolder;

    aput-object v6, v7, v8

    invoke-static {v0, v7}, Landroid/animation/ObjectAnimator;->ofPropertyValuesHolder(Ljava/lang/Object;[Landroid/animation/PropertyValuesHolder;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iget-object v6, p0, Lcdg;->j:Landroid/animation/AnimatorSet;

    iget v7, p0, Lcdg;->h:I

    int-to-long v8, v7

    invoke-virtual {v0, v8, v9}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-static {v6, v3, v0}, Lcad;->a(Landroid/animation/AnimatorSet;Landroid/animation/AnimatorSet$Builder;Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcdg;->j:Landroid/animation/AnimatorSet;

    iget v1, p0, Lcdg;->g:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    iget-object v0, p0, Lcdg;->j:Landroid/animation/AnimatorSet;

    sget-object v1, Lcak;->a:Lcak;

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    iget-object v0, p0, Lcdg;->j:Landroid/animation/AnimatorSet;

    iget-object v1, p0, Lcdg;->d:Ljava/lang/Runnable;

    invoke-virtual {p1, v0, v11, v1}, Lccj;->a(Landroid/animation/Animator;ILjava/lang/Runnable;)V

    .line 89
    iput v10, p0, Lcdg;->e:I

    goto/16 :goto_0

    .line 91
    :cond_3
    iput v2, p0, Lcdg;->e:I

    goto/16 :goto_0

    .line 96
    :pswitch_2
    invoke-direct {p0, p1}, Lcdg;->b(Lccj;)V

    goto/16 :goto_0

    .line 81
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
