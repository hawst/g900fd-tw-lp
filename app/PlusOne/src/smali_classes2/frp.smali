.class public final Lfrp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Llds;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lfrp;->a:Landroid/content/Context;

    .line 28
    return-void
.end method


# virtual methods
.method public a(Loya;)Lgs;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Loya;",
            ")",
            "Lgs",
            "<[B",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 47
    sget-object v0, Lpda;->a:Loxr;

    invoke-virtual {p1, v0}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpda;

    .line 48
    if-eqz v0, :cond_0

    .line 49
    new-instance v1, Lgs;

    iget-object v2, p0, Lfrp;->a:Landroid/content/Context;

    iget-object v3, p1, Loya;->c:Ljava/lang/String;

    new-instance v4, Lfnz;

    invoke-direct {v4, v2, v0, v3}, Lfnz;-><init>(Landroid/content/Context;Lpda;Ljava/lang/String;)V

    new-instance v0, Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0x100

    invoke-direct {v0, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v0}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v3, v4, Lfnz;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lfnz;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, v4, Lfnz;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lfnz;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, v4, Lfnz;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lfnz;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, v4, Lfnz;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lfnz;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, v4, Lfnz;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lfnz;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget v3, v4, Lfnz;->f:I

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    const-wide/32 v2, 0x100000

    .line 50
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lgs;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    .line 52
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 37
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/16 v3, 0x17c

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public a(Landroid/content/Context;)Lldq;
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lfst;

    invoke-direct {v0, p1}, Lfst;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public a(J)Z
    .locals 5

    .prologue
    .line 32
    const-wide/32 v0, 0x100000

    and-long/2addr v0, p1

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
