.class public Layr;
.super Layb;
.source "PG"


# static fields
.field private static a:Landroid/animation/TimeInterpolator;


# instance fields
.field private b:I

.field private c:I

.field private d:Landroid/graphics/Matrix;

.field private e:F

.field private f:I

.field private g:I

.field private h:Landroid/graphics/Matrix;

.field private i:F

.field private j:I

.field private k:I

.field private l:Landroid/graphics/Matrix;

.field private m:F

.field private n:I

.field private o:I

.field private p:F

.field private q:Layc;

.field private r:Landroid/graphics/Matrix;

.field private s:Landroid/graphics/Matrix;

.field private t:Landroid/graphics/Matrix;

.field private u:Landroid/graphics/Matrix;

.field private v:J

.field private w:J

.field private x:J

.field private y:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, Layr;->a:Landroid/animation/TimeInterpolator;

    return-void
.end method

.method protected constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/high16 v0, 0x3f800000    # 1.0f

    .line 69
    invoke-direct {p0}, Layb;-><init>()V

    .line 39
    iput v0, p0, Layr;->e:F

    .line 40
    iput v1, p0, Layr;->f:I

    .line 44
    iput v0, p0, Layr;->i:F

    .line 45
    iput v1, p0, Layr;->j:I

    .line 49
    iput v0, p0, Layr;->m:F

    .line 50
    iput v1, p0, Layr;->n:I

    .line 54
    sget-object v0, Layc;->a:Layc;

    iput-object v0, p0, Layr;->q:Layc;

    .line 70
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Layr;->r:Landroid/graphics/Matrix;

    .line 71
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Layr;->s:Landroid/graphics/Matrix;

    .line 72
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Layr;->t:Landroid/graphics/Matrix;

    .line 73
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Layr;->u:Landroid/graphics/Matrix;

    .line 74
    return-void
.end method

.method protected constructor <init>(I)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Layr;-><init>()V

    .line 82
    iput p1, p0, Layr;->b:I

    .line 83
    return-void
.end method

.method protected constructor <init>(Lays;)V
    .locals 2

    .prologue
    .line 90
    invoke-direct {p0}, Layr;-><init>()V

    .line 91
    iget v0, p1, Lays;->a:I

    iput v0, p0, Layr;->b:I

    .line 92
    iget v0, p1, Lays;->b:I

    iput v0, p0, Layr;->c:I

    .line 93
    iget v0, p1, Lays;->c:F

    iput v0, p0, Layr;->e:F

    .line 94
    iget v0, p1, Lays;->d:I

    iput v0, p0, Layr;->f:I

    .line 96
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Layr;->d:Landroid/graphics/Matrix;

    .line 99
    iget v0, p1, Lays;->e:I

    iput v0, p0, Layr;->g:I

    .line 100
    iget v0, p1, Lays;->f:F

    iput v0, p0, Layr;->i:F

    .line 101
    iget v0, p1, Lays;->g:I

    iput v0, p0, Layr;->j:I

    .line 103
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Layr;->h:Landroid/graphics/Matrix;

    .line 106
    const/4 v0, 0x0

    iput v0, p0, Layr;->k:I

    .line 107
    iget v0, p1, Lays;->h:F

    iput v0, p0, Layr;->m:F

    .line 108
    iget v0, p1, Lays;->i:I

    iput v0, p0, Layr;->n:I

    .line 110
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Layr;->l:Landroid/graphics/Matrix;

    .line 113
    iget v0, p1, Lays;->k:I

    iput v0, p0, Layr;->o:I

    .line 114
    const/4 v0, 0x0

    iput v0, p0, Layr;->p:F

    .line 115
    iget-object v0, p1, Lays;->j:Layc;

    iput-object v0, p0, Layr;->q:Layc;

    .line 117
    iget-wide v0, p1, Lays;->l:J

    iput-wide v0, p0, Layr;->v:J

    .line 118
    iget-wide v0, p1, Lays;->m:J

    iput-wide v0, p0, Layr;->w:J

    .line 119
    iget-wide v0, p1, Lays;->n:J

    iput-wide v0, p0, Layr;->x:J

    .line 120
    iget-wide v0, p1, Lays;->o:J

    iput-wide v0, p0, Layr;->y:J

    .line 122
    return-void
.end method


# virtual methods
.method public a(Lbmg;Lbmg;)J
    .locals 3

    .prologue
    .line 133
    const-wide/16 v0, 0x0

    .line 134
    sget-object v2, Lbmg;->a:Lbmg;

    if-ne p1, v2, :cond_2

    .line 135
    sget-object v0, Lbmg;->c:Lbmg;

    if-ne p2, v0, :cond_1

    .line 136
    iget-wide v0, p0, Layr;->x:J

    .line 147
    :cond_0
    :goto_0
    return-wide v0

    .line 138
    :cond_1
    iget-wide v0, p0, Layr;->v:J

    goto :goto_0

    .line 140
    :cond_2
    sget-object v2, Lbmg;->c:Lbmg;

    if-ne p1, v2, :cond_0

    .line 141
    sget-object v0, Lbmg;->a:Lbmg;

    if-ne p2, v0, :cond_3

    .line 142
    iget-wide v0, p0, Layr;->y:J

    goto :goto_0

    .line 144
    :cond_3
    iget-wide v0, p0, Layr;->w:J

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Layl;II)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Layl;Lbof;)Landroid/graphics/Matrix;
    .locals 6

    .prologue
    .line 300
    iget-object v0, p0, Layr;->r:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 301
    iget-object v0, p1, Layl;->s:Lbmj;

    .line 302
    if-eqz p2, :cond_1

    .line 303
    iget-wide v0, p1, Layl;->d:J

    .line 304
    iget-object v2, p2, Lbof;->b:Lbog;

    invoke-virtual {v2}, Lbog;->a()Z

    move-result v2

    if-nez v2, :cond_2

    .line 306
    const-wide/16 v0, 0x0

    .line 312
    :cond_0
    :goto_0
    invoke-virtual {p2}, Lbof;->d()Lbmi;

    move-result-object v2

    iget-wide v4, p1, Layl;->e:J

    invoke-interface {v2, v0, v1, v4, v5}, Lbmi;->a(JJ)Lbmj;

    move-result-object v0

    .line 314
    :cond_1
    iget-object v1, p0, Layr;->r:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lbmj;->a(Landroid/graphics/Matrix;)V

    .line 315
    iget-object v0, p0, Layr;->r:Landroid/graphics/Matrix;

    return-object v0

    .line 307
    :cond_2
    iget-object v2, p2, Lbof;->b:Lbog;

    invoke-virtual {v2}, Lbog;->b()Z

    move-result v2

    if-nez v2, :cond_0

    .line 310
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public a(Lbmd;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbmd;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Layl;)Z
    .locals 1

    .prologue
    .line 265
    const/4 v0, 0x0

    return v0
.end method

.method public b(Layl;)Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Layr;->s:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 321
    iget-object v0, p1, Layl;->t:Lbmj;

    iget-object v1, p0, Layr;->s:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lbmj;->a(Landroid/graphics/Matrix;)V

    .line 322
    iget-object v0, p0, Layr;->s:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public b(Lbmd;)Lool;
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 270
    const/4 v0, 0x0

    return-object v0
.end method

.method public c(Lbmd;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbmd;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public d(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 275
    const/4 v0, 0x0

    return-object v0
.end method

.method public e(Layl;)I
    .locals 1

    .prologue
    .line 168
    iget v0, p0, Layr;->b:I

    return v0
.end method

.method public f(Layl;)I
    .locals 1

    .prologue
    .line 173
    iget v0, p0, Layr;->c:I

    return v0
.end method

.method public f_()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 280
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 281
    iget v1, p0, Layr;->c:I

    if-eqz v1, :cond_0

    .line 282
    iget v1, p0, Layr;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 284
    :cond_0
    iget v1, p0, Layr;->g:I

    if-eqz v1, :cond_1

    .line 285
    iget v1, p0, Layr;->g:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 287
    :cond_1
    return-object v0
.end method

.method public g(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Layr;->d:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public g_()Layc;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Layr;->q:Layc;

    return-object v0
.end method

.method public h(Layl;)F
    .locals 1

    .prologue
    .line 183
    iget v0, p0, Layr;->e:F

    return v0
.end method

.method public i(Layl;)I
    .locals 1

    .prologue
    .line 188
    iget v0, p0, Layr;->f:I

    return v0
.end method

.method public j(Layl;)I
    .locals 1

    .prologue
    .line 193
    iget v0, p0, Layr;->g:I

    return v0
.end method

.method public k(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Layr;->h:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public l(Layl;)F
    .locals 1

    .prologue
    .line 203
    iget v0, p0, Layr;->i:F

    return v0
.end method

.method public m(Layl;)I
    .locals 1

    .prologue
    .line 208
    iget v0, p0, Layr;->j:I

    return v0
.end method

.method public n(Layl;)I
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x0

    return v0
.end method

.method public o(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Layr;->l:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public p(Layl;)F
    .locals 1

    .prologue
    .line 230
    iget v0, p0, Layr;->m:F

    return v0
.end method

.method public q(Layl;)I
    .locals 1

    .prologue
    .line 235
    iget v0, p0, Layr;->n:I

    return v0
.end method

.method public r(Layl;)I
    .locals 1

    .prologue
    .line 240
    iget v0, p0, Layr;->o:I

    return v0
.end method

.method public s(Layl;)J
    .locals 2

    .prologue
    .line 245
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public t(Layl;)F
    .locals 1

    .prologue
    .line 250
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 347
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u(Layl;)F
    .locals 1

    .prologue
    .line 260
    invoke-virtual {p0}, Layr;->g_()Layc;

    move-result-object v0

    invoke-virtual {v0}, Layc;->b()F

    move-result v0

    return v0
.end method

.method public v(Layl;)F
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 292
    iget-boolean v2, p1, Layl;->w:Z

    if-eqz v2, :cond_0

    .line 295
    :goto_0
    return v0

    :cond_0
    sget-object v2, Layr;->a:Landroid/animation/TimeInterpolator;

    iget-object v3, p1, Layl;->i:Lbmg;

    sget-object v4, Lbmg;->d:Lbmg;

    if-ne v3, v4, :cond_1

    invoke-static {p1}, Lbag;->e(Layl;)Z

    move-result v3

    if-nez v3, :cond_1

    :goto_1
    invoke-interface {v2, v0}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    sub-float/2addr v0, v1

    goto :goto_0

    :cond_1
    iget v0, p1, Layl;->f:I

    iget v3, p1, Layl;->h:I

    add-int/lit8 v3, v3, -0x1

    if-ne v0, v3, :cond_4

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_2

    iget-object v3, p1, Layl;->i:Lbmg;

    sget-object v4, Lbmg;->e:Lbmg;

    if-ne v3, v4, :cond_5

    :cond_2
    if-nez v0, :cond_3

    iget-object v0, p1, Layl;->k:Lbmg;

    sget-object v3, Lbmg;->d:Lbmg;

    if-eq v0, v3, :cond_5

    :cond_3
    move v0, v1

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :cond_5
    const-wide/32 v4, 0x16e360

    iget-wide v6, p1, Layl;->e:J

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    iget-wide v6, p1, Layl;->e:J

    iget-wide v8, p1, Layl;->d:J

    sub-long/2addr v6, v8

    cmp-long v0, v6, v4

    if-lez v0, :cond_6

    move v0, v1

    goto :goto_1

    :cond_6
    long-to-float v0, v6

    long-to-float v3, v4

    div-float/2addr v0, v3

    goto :goto_1
.end method

.method public w(Layl;)F
    .locals 1

    .prologue
    .line 337
    iget v0, p1, Layl;->m:F

    return v0
.end method

.method public x(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 327
    iget-object v0, p0, Layr;->t:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public y(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 332
    iget-object v0, p0, Layr;->u:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public z(Layl;)V
    .locals 0

    .prologue
    .line 342
    return-void
.end method
