.class final Lbjc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lbjb;


# direct methods
.method constructor <init>(Lbjb;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lbjc;->a:Lbjb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 98
    :try_start_0
    iget-object v0, p0, Lbjc;->a:Lbjb;

    invoke-static {v0}, Lbjb;->b(Lbjb;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lbjc;->a:Lbjb;

    invoke-static {v1}, Lbjb;->a(Lbjb;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-object v0, v0, Lbmd;->e:Ljeg;

    iget-object v1, v0, Ljeg;->b:Landroid/net/Uri;

    .line 99
    iget-object v0, p0, Lbjc;->a:Lbjb;

    invoke-static {v0}, Lbjb;->c(Lbjb;)Ljava/util/List;

    move-result-object v0

    iget-object v2, p0, Lbjc;->a:Lbjb;

    invoke-static {v2}, Lbjb;->a(Lbjb;)I

    move-result v2

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelFileDescriptor;

    .line 100
    iget-object v2, p0, Lbjc;->a:Lbjb;

    invoke-static {v2}, Lbjb;->d(Lbjb;)Landroid/net/Uri;

    move-result-object v2

    if-eq v2, v1, :cond_0

    .line 101
    iget-object v2, p0, Lbjc;->a:Lbjb;

    invoke-static {v2, v0}, Lbjb;->a(Lbjb;Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 104
    if-eqz v0, :cond_0

    .line 105
    iget-object v2, p0, Lbjc;->a:Lbjb;

    invoke-static {v2, v0}, Lbjb;->a(Lbjb;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 106
    iget-object v0, p0, Lbjc;->a:Lbjb;

    invoke-static {v0, v1}, Lbjb;->a(Lbjb;Landroid/net/Uri;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    :cond_0
    :goto_0
    iget-object v0, p0, Lbjc;->a:Lbjb;

    invoke-static {v0}, Lbjb;->e(Lbjb;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    invoke-static {}, Lbjb;->d()Ljava/lang/String;

    iget-object v0, p0, Lbjc;->a:Lbjb;

    .line 111
    invoke-static {v0}, Lbjb;->b(Lbjb;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lbjc;->a:Lbjb;

    invoke-static {v1}, Lbjb;->a(Lbjb;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-object v0, v0, Lbmd;->e:Ljeg;

    iget-object v0, v0, Ljeg;->b:Landroid/net/Uri;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1c

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Error loading photo for Uri "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 94
    invoke-virtual {p0}, Lbjc;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
