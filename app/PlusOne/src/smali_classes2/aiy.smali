.class final Laiy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lahy;


# instance fields
.field private synthetic a:Laiw;


# direct methods
.method constructor <init>(Laiw;)V
    .locals 0

    .prologue
    .line 268
    iput-object p1, p0, Laiy;->a:Laiw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/net/Uri;Laic;)Z
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Laiy;->a:Laiw;

    iget-object v0, v0, Laiw;->a:Laix;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laiy;->a:Laiw;

    iget-object v0, v0, Laiw;->a:Laix;

    iget-object v0, v0, Laix;->j:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laiy;->a:Laiw;

    .line 321
    iget-object v0, v0, Laiw;->a:Laix;

    iget-object v0, v0, Laix;->a:Laic;

    invoke-virtual {p2, v0}, Laic;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 307
    iget-object v0, p0, Laiy;->a:Laiw;

    iget-object v0, v0, Laiw;->a:Laix;

    if-eqz v0, :cond_0

    .line 308
    iget-object v0, p0, Laiy;->a:Laiw;

    iget-object v0, v0, Laiw;->a:Laix;

    iget-object v0, v0, Laix;->g:Lajb;

    invoke-interface {v0, p1}, Lajb;->a(Landroid/graphics/Bitmap;)V

    .line 310
    :cond_0
    return-void
.end method

.method public a(Landroid/net/Uri;J)V
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Laiy;->a:Laiw;

    iget-object v0, v0, Laiw;->a:Laix;

    if-eqz v0, :cond_0

    .line 301
    iget-object v0, p0, Laiy;->a:Laiw;

    iget-object v0, v0, Laiw;->a:Laix;

    iget-object v0, v0, Laix;->g:Lajb;

    invoke-interface {v0, p1, p2, p3}, Lajb;->a(Landroid/net/Uri;J)V

    .line 303
    :cond_0
    return-void
.end method

.method public a(Landroid/net/Uri;Laic;Lbkr;)V
    .locals 3

    .prologue
    .line 273
    invoke-direct {p0, p1, p2}, Laiy;->a(Landroid/net/Uri;Laic;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Laiy;->a:Laiw;

    iget-object v0, v0, Laiw;->a:Laix;

    iget-object v0, v0, Laix;->i:Ljava/util/List;

    new-instance v1, Lajc;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p3, v2}, Lajc;-><init>(Landroid/net/Uri;Lbkr;Ljava/lang/Exception;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 275
    iget-object v0, p0, Laiy;->a:Laiw;

    invoke-virtual {v0}, Laiw;->b()V

    .line 277
    :cond_0
    return-void
.end method

.method public a(Landroid/net/Uri;Laic;Ljava/lang/Exception;)V
    .locals 3

    .prologue
    .line 292
    invoke-direct {p0, p1, p2}, Laiy;->a(Landroid/net/Uri;Laic;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    iget-object v0, p0, Laiy;->a:Laiw;

    iget-object v0, v0, Laiw;->a:Laix;

    iget-object v0, v0, Laix;->i:Ljava/util/List;

    new-instance v1, Lajc;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2, p3}, Lajc;-><init>(Landroid/net/Uri;Lbkr;Ljava/lang/Exception;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 294
    iget-object v0, p0, Laiy;->a:Laiw;

    invoke-virtual {v0}, Laiw;->b()V

    .line 296
    :cond_0
    return-void
.end method

.method public a(Landroid/net/Uri;Lbkr;)V
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Laiy;->a:Laiw;

    .line 315
    return-void
.end method

.method public b(Landroid/net/Uri;Laic;Lbkr;)V
    .locals 3

    .prologue
    .line 282
    invoke-direct {p0, p1, p2}, Laiy;->a(Landroid/net/Uri;Laic;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Laiy;->a:Laiw;

    iget-object v0, v0, Laiw;->a:Laix;

    iget-object v0, v0, Laix;->i:Ljava/util/List;

    new-instance v1, Lajc;

    new-instance v2, Laja;

    invoke-direct {v2}, Laja;-><init>()V

    invoke-direct {v1, p1, p3, v2}, Lajc;-><init>(Landroid/net/Uri;Lbkr;Ljava/lang/Exception;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285
    iget-object v0, p0, Laiy;->a:Laiw;

    invoke-virtual {v0}, Laiw;->b()V

    .line 287
    :cond_0
    return-void
.end method
