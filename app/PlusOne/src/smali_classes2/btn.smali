.class public Lbtn;
.super Lbtf;
.source "PG"


# instance fields
.field private final a:Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;

.field private final b:Landroid/view/SurfaceHolder$Callback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lbtn;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbgf;Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;)V
    .locals 3

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lbtf;-><init>(Lbgf;)V

    .line 27
    const-string v0, "surfaceView"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;

    iput-object v0, p0, Lbtn;->a:Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;

    .line 28
    new-instance v0, Lbto;

    invoke-direct {v0, p0}, Lbto;-><init>(Lbtn;)V

    iput-object v0, p0, Lbtn;->b:Landroid/view/SurfaceHolder$Callback;

    .line 63
    iget-object v0, p0, Lbtn;->a:Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lbtn;->b:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 66
    iget-object v0, p0, Lbtn;->a:Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lbtn;->a:Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lbtn;->a:Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;

    .line 68
    invoke-virtual {v1}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->a()I

    move-result v1

    iget-object v2, p0, Lbtn;->a:Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;

    invoke-virtual {v2}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->b()I

    move-result v2

    .line 67
    invoke-virtual {p0, v0, v1, v2}, Lbtn;->a(Ljava/lang/Object;II)V

    .line 70
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lbtn;->a:Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/SizeLimitingSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lbtn;->b:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 75
    invoke-super {p0}, Lbtf;->a()V

    .line 76
    return-void
.end method
