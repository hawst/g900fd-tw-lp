.class public final Lcaf;
.super Landroid/util/Property;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/util/Property",
        "<",
        "Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;",
        "Ljava/lang/Float;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Ljava/lang/Class;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0, p1, p2}, Landroid/util/Property;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;)Ljava/lang/Float;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->b()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;Ljava/lang/Float;)V
    .locals 1

    .prologue
    .line 41
    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->c(F)V

    .line 42
    return-void
.end method

.method public synthetic get(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    check-cast p1, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    invoke-virtual {p0, p1}, Lcaf;->a(Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public synthetic set(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 38
    check-cast p1, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p0, p1, p2}, Lcaf;->a(Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;Ljava/lang/Float;)V

    return-void
.end method
