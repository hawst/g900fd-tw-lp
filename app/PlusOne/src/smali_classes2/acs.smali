.class public Lacs;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Landroid/content/Context;

.field b:Z

.field c:Landroid/renderscript/RenderScript;

.field private d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Laan;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Labx;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lzw;

.field private g:Lacu;

.field private h:Landroid/view/SurfaceView;

.field private i:Landroid/os/Handler;

.field private j:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lact;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 90
    iput-object v3, p0, Lacs;->a:Landroid/content/Context;

    .line 93
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lacs;->d:Ljava/util/Set;

    .line 96
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lacs;->e:Ljava/util/Set;

    .line 99
    iput-object v3, p0, Lacs;->f:Lzw;

    .line 105
    new-instance v0, Lacu;

    invoke-direct {v0}, Lacu;-><init>()V

    iput-object v0, p0, Lacs;->g:Lacu;

    .line 108
    iput-object v3, p0, Lacs;->h:Landroid/view/SurfaceView;

    .line 111
    iput-object v3, p0, Lacs;->i:Landroid/os/Handler;

    .line 155
    const-string v0, "activity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getDeviceConfigurationInfo()Landroid/content/pm/ConfigurationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ConfigurationInfo;->reqGlEsVersion:I

    const/high16 v4, 0x20000

    if-lt v0, v4, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lacs;->b:Z

    iget-boolean v0, p2, Lact;->b:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lacs;->b:Z

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot create context that requires GL support on this platform!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-static {}, Lzw;->b()I

    move-result v0

    if-lez v0, :cond_2

    move v2, v1

    :cond_2
    iput-boolean v2, p0, Lacs;->j:Z

    iget-boolean v0, p2, Lact;->a:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lacs;->j:Z

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot create context that requires a camera on this platform!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "MffContext must be created in a thread with a Looper!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lacs;->i:Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lacs;->a:Landroid/content/Context;

    iget-boolean v0, p2, Lact;->a:Z

    if-eqz v0, :cond_6

    invoke-static {}, Lzw;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Landroid/view/SurfaceView;

    invoke-direct {v0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    const/4 v4, 0x3

    invoke-interface {v2, v4}, Landroid/view/SurfaceHolder;->setType(I)V

    instance-of v2, p1, Landroid/app/Activity;

    if-eqz v2, :cond_7

    check-cast p1, Landroid/app/Activity;

    :goto_1
    if-eqz p1, :cond_5

    new-instance v2, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v2, v1, v1}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p1, v0, v2}, Landroid/app/Activity;->addContentView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :cond_5
    iput-object v0, p0, Lacs;->h:Landroid/view/SurfaceView;

    .line 156
    :cond_6
    return-void

    :cond_7
    move-object p1, v3

    .line 155
    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    const/4 v2, 0x3

    .line 197
    iget-object v1, p0, Lacs;->g:Lacu;

    monitor-enter v1

    .line 198
    :try_start_0
    iget-object v0, p0, Lacs;->g:Lacu;

    iget v0, v0, Lacu;->a:I

    if-eq v0, v2, :cond_9

    .line 199
    iget-object v0, p0, Lacs;->f:Lzw;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lacs;->f:Lzw;

    invoke-virtual {v0}, Lzw;->a()V

    .line 201
    iget-object v0, p0, Lacs;->f:Lzw;

    invoke-virtual {v0}, Lzw;->d()V

    .line 203
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v0, v2, :cond_1

    .line 204
    iget-object v0, p0, Lacs;->c:Landroid/renderscript/RenderScript;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lacs;->c:Landroid/renderscript/RenderScript;

    invoke-virtual {v0}, Landroid/renderscript/RenderScript;->destroy()V

    const/4 v0, 0x0

    iput-object v0, p0, Lacs;->c:Landroid/renderscript/RenderScript;

    .line 206
    :cond_1
    iget-object v2, p0, Lacs;->e:Ljava/util/Set;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-object v0, p0, Lacs;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labx;

    invoke-virtual {v0}, Labx;->d()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v0

    .line 211
    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 206
    :cond_2
    :try_start_3
    iget-object v0, p0, Lacs;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labx;

    invoke-virtual {v0}, Labx;->o()V

    goto :goto_1

    :cond_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 207
    :try_start_4
    iget-object v0, p0, Lacs;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labx;

    invoke-virtual {v0}, Labx;->c()V

    goto :goto_2

    .line 208
    :cond_4
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iget-object v3, p0, Lacs;->d:Ljava/util/Set;

    monitor-enter v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    iget-object v0, p0, Lacs;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laan;

    invoke-virtual {v0}, Laan;->b()Z

    move-result v5

    if-nez v5, :cond_5

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :catchall_2
    move-exception v0

    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :cond_6
    :try_start_7
    monitor-exit v3
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    :try_start_8
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laan;

    invoke-virtual {v0}, Laan;->a()V

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lacs;->e:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labx;

    invoke-virtual {v0}, Labx;->l()V

    goto :goto_5

    .line 209
    :cond_8
    iget-object v0, p0, Lacs;->g:Lacu;

    const/4 v2, 0x3

    iput v2, v0, Lacu;->a:I

    .line 211
    :cond_9
    monitor-exit v1
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    return-void
.end method

.method a(Laan;)V
    .locals 2

    .prologue
    .line 320
    iget-object v1, p0, Lacs;->d:Ljava/util/Set;

    monitor-enter v1

    .line 321
    :try_start_0
    iget-object v0, p0, Lacs;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 322
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(Labx;)V
    .locals 2

    .prologue
    .line 332
    iget-object v1, p0, Lacs;->e:Ljava/util/Set;

    monitor-enter v1

    .line 333
    :try_start_0
    iget-object v0, p0, Lacs;->e:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 334
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lacs;->i:Landroid/os/Handler;

    invoke-virtual {v0, p1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 343
    return-void
.end method

.method public b()Landroid/content/Context;
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lacs;->a:Landroid/content/Context;

    return-object v0
.end method

.method b(Laan;)V
    .locals 2

    .prologue
    .line 326
    iget-object v1, p0, Lacs;->d:Ljava/util/Set;

    monitor-enter v1

    .line 327
    :try_start_0
    iget-object v0, p0, Lacs;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 328
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()Lzw;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lacs;->f:Lzw;

    if-nez v0, :cond_0

    .line 256
    new-instance v0, Lzw;

    invoke-direct {v0, p0}, Lzw;-><init>(Lacs;)V

    iput-object v0, p0, Lacs;->f:Lzw;

    .line 258
    :cond_0
    iget-object v0, p0, Lacs;->f:Lzw;

    return-object v0
.end method

.method d()Landroid/view/SurfaceView;
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lacs;->h:Landroid/view/SurfaceView;

    return-object v0
.end method
