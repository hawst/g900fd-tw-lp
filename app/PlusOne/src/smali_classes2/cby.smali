.class public Lcby;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcau;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/database/DataSetObserver;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lcca;

.field private final e:Landroid/os/Handler;

.field private final f:Ljava/lang/Runnable;

.field private g:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/lang/String;

.field private i:Lcbx;

.field private j:I

.field private k:Z

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    const-class v0, Lcby;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcca;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcby;->b:Ljava/util/List;

    .line 104
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcby;->c:Ljava/util/List;

    .line 107
    new-instance v0, Lcbz;

    invoke-direct {v0, p0}, Lcbz;-><init>(Lcby;)V

    iput-object v0, p0, Lcby;->f:Ljava/lang/Runnable;

    .line 115
    const-string v0, ""

    iput-object v0, p0, Lcby;->h:Ljava/lang/String;

    .line 117
    const/4 v0, -0x1

    iput v0, p0, Lcby;->j:I

    .line 118
    iput-boolean v1, p0, Lcby;->k:Z

    .line 119
    iput-boolean v1, p0, Lcby;->l:Z

    .line 124
    const-string v0, "context"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcby;->a:Landroid/content/Context;

    .line 125
    const-string v0, "viewConfigurer"

    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcca;

    iput-object v0, p0, Lcby;->d:Lcca;

    .line 126
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcby;->e:Landroid/os/Handler;

    .line 127
    return-void
.end method

.method static synthetic a(Lcby;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcby;->i()V

    return-void
.end method

.method private g()I
    .locals 2

    .prologue
    .line 469
    iget-object v0, p0, Lcby;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0}, Lcby;->j()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method private h()I
    .locals 2

    .prologue
    .line 481
    iget-boolean v0, p0, Lcby;->k:Z

    if-eqz v0, :cond_0

    .line 482
    invoke-direct {p0}, Lcby;->g()I

    move-result v0

    invoke-direct {p0}, Lcby;->k()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private i()V
    .locals 2

    .prologue
    .line 551
    iget-object v0, p0, Lcby;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcby;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 552
    iget-object v0, p0, Lcby;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/DataSetObserver;

    .line 553
    invoke-virtual {v0}, Landroid/database/DataSetObserver;->onChanged()V

    goto :goto_0

    .line 555
    :cond_0
    return-void
.end method

.method private i(I)V
    .locals 4

    .prologue
    .line 487
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 488
    const-string v0, "newPos"

    const/4 v1, 0x1

    iget-object v2, p0, Lcby;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {p1, v0, v1, v2}, Lcec;->a(ILjava/lang/CharSequence;II)I

    .line 490
    :cond_0
    iget v0, p0, Lcby;->j:I

    if-eq p1, v0, :cond_1

    .line 491
    iput p1, p0, Lcby;->j:I

    .line 492
    iget-object v0, p0, Lcby;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcby;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 493
    iget-object v0, p0, Lcby;->e:Landroid/os/Handler;

    iget-object v1, p0, Lcby;->f:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 495
    :cond_1
    return-void
.end method

.method private j()I
    .locals 2

    .prologue
    .line 562
    iget v0, p0, Lcby;->j:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private k()I
    .locals 1

    .prologue
    .line 566
    iget-boolean v0, p0, Lcby;->l:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(J)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 423
    const-wide v2, 0x80000002L

    cmp-long v1, p1, v2

    if-nez v1, :cond_1

    .line 424
    iget v0, p0, Lcby;->j:I

    .line 441
    :cond_0
    :goto_0
    return v0

    .line 425
    :cond_1
    const-wide v2, 0x80000001L

    cmp-long v1, p1, v2

    if-eqz v1, :cond_0

    .line 427
    const-wide v2, 0x80000003L

    cmp-long v1, p1, v2

    if-nez v1, :cond_2

    .line 428
    invoke-virtual {p0}, Lcby;->f()I

    move-result v0

    goto :goto_0

    .line 429
    :cond_2
    const-wide v2, 0x80000000L

    cmp-long v1, p1, v2

    if-nez v1, :cond_3

    .line 430
    invoke-direct {p0}, Lcby;->h()I

    move-result v0

    goto :goto_0

    .line 434
    :cond_3
    iget-object v1, p0, Lcby;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_5

    .line 435
    iget-object v0, p0, Lcby;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget v0, v0, Lbmd;->b:I

    int-to-long v4, v0

    cmp-long v0, v4, p1

    if-nez v0, :cond_4

    .line 436
    invoke-virtual {p0, v1}, Lcby;->d(I)I

    move-result v0

    goto :goto_0

    .line 434
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 441
    :cond_5
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a(Lbmd;)I
    .locals 3

    .prologue
    .line 247
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcby;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 248
    iget-object v0, p0, Lcby;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget v0, v0, Lbmd;->b:I

    iget v2, p1, Lbmd;->b:I

    if-ne v0, v2, :cond_0

    .line 252
    :goto_1
    return v1

    .line 247
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 252
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public a()V
    .locals 1

    .prologue
    .line 171
    iget-boolean v0, p0, Lcby;->k:Z

    if-nez v0, :cond_0

    .line 172
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcby;->k:Z

    .line 173
    invoke-direct {p0}, Lcby;->i()V

    .line 175
    :cond_0
    return-void
.end method

.method public a(Landroid/util/SparseArray;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lbmd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 322
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 330
    :goto_0
    return-void

    .line 326
    :cond_0
    invoke-virtual {p1}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    if-ltz v0, :cond_1

    .line 327
    iget-object v1, p0, Lcby;->b:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 326
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 329
    :cond_1
    invoke-direct {p0}, Lcby;->i()V

    goto :goto_0
.end method

.method public a(Lbph;)V
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcby;->b:Ljava/util/List;

    invoke-interface {p1, v0}, Lbph;->a(Ljava/util/List;)V

    .line 313
    invoke-direct {p0}, Lcby;->i()V

    .line 314
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 137
    const-string v0, "title"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 138
    iget-object v0, p0, Lcby;->h:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 139
    iput-object p1, p0, Lcby;->h:Ljava/lang/String;

    .line 142
    iget-object v0, p0, Lcby;->i:Lcbx;

    if-eqz v0, :cond_0

    .line 143
    iget-object v0, p0, Lcby;->d:Lcca;

    iget-object v1, p0, Lcby;->i:Lcbx;

    iget-object v2, p0, Lcby;->h:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcca;->a(Lcbx;Ljava/lang/String;)V

    .line 146
    :cond_0
    invoke-direct {p0}, Lcby;->i()V

    .line 148
    :cond_1
    return-void
.end method

.method public a(Ljava/util/List;Ljava/util/Map;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 155
    const-string v0, "displayables"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lcby;->g:Ljava/util/Map;

    .line 156
    iget-object v0, p0, Lcby;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 157
    const/4 v0, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 158
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 159
    iget-object v3, v0, Lbmd;->d:Lbmg;

    sget-object v4, Lbmg;->d:Lbmg;

    if-eq v3, v4, :cond_0

    .line 160
    iget-object v3, p0, Lcby;->b:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 167
    :cond_1
    invoke-direct {p0}, Lcby;->i()V

    .line 168
    return-void
.end method

.method public a(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 202
    invoke-virtual {p0, p1}, Lcby;->getItemViewType(I)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    iget-object v2, p0, Lcby;->h:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public b(Lbmd;)I
    .locals 1

    .prologue
    .line 277
    invoke-virtual {p0, p1}, Lcby;->a(Lbmd;)I

    move-result v0

    .line 278
    if-gez v0, :cond_0

    .line 281
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, v0}, Lcby;->d(I)I

    move-result v0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    .prologue
    .line 178
    iget-boolean v0, p0, Lcby;->k:Z

    if-eqz v0, :cond_0

    .line 179
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcby;->k:Z

    .line 180
    invoke-direct {p0}, Lcby;->i()V

    .line 182
    :cond_0
    return-void
.end method

.method public b(I)Z
    .locals 1

    .prologue
    .line 209
    if-lez p1, :cond_0

    .line 210
    iget v0, p0, Lcby;->j:I

    if-eq p1, v0, :cond_0

    .line 211
    invoke-direct {p0}, Lcby;->g()I

    move-result v0

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(J)Z
    .locals 3

    .prologue
    .line 232
    const-wide v0, 0x80000002L

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lcby;->l:Z

    if-nez v0, :cond_0

    .line 186
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcby;->l:Z

    .line 187
    invoke-direct {p0}, Lcby;->i()V

    .line 189
    :cond_0
    return-void
.end method

.method public c(I)Z
    .locals 1

    .prologue
    .line 225
    invoke-virtual {p0}, Lcby;->f()I

    move-result v0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c(J)Z
    .locals 3

    .prologue
    .line 239
    const-wide v0, 0x80000003L

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(I)I
    .locals 3

    .prologue
    .line 259
    const-string v0, "clipIndex"

    const/4 v1, 0x0

    iget-object v2, p0, Lcby;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {p1, v0, v1, v2}, Lcec;->a(ILjava/lang/CharSequence;II)I

    .line 261
    if-gez p1, :cond_0

    .line 270
    :goto_0
    return p1

    .line 265
    :cond_0
    add-int/lit8 v0, p1, 0x1

    .line 267
    iget v1, p0, Lcby;->j:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    iget v1, p0, Lcby;->j:I

    if-lt v0, v1, :cond_1

    .line 268
    add-int/lit8 v0, v0, 0x1

    :cond_1
    move p1, v0

    .line 270
    goto :goto_0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 192
    iget-boolean v0, p0, Lcby;->l:Z

    if-eqz v0, :cond_0

    .line 193
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcby;->l:Z

    .line 194
    invoke-direct {p0}, Lcby;->i()V

    .line 196
    :cond_0
    return-void
.end method

.method public e(I)I
    .locals 3

    .prologue
    .line 291
    const-string v0, "position"

    const/4 v1, 0x1

    invoke-direct {p0}, Lcby;->g()I

    move-result v2

    invoke-static {p1, v0, v1, v2}, Lcec;->a(ILjava/lang/CharSequence;II)I

    .line 292
    const-string v0, "position"

    iget v1, p0, Lcby;->j:I

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcec;->b(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)I

    .line 293
    iget v0, p0, Lcby;->j:I

    if-le p1, v0, :cond_0

    .line 294
    add-int/lit8 v0, p1, -0x1

    invoke-direct {p0}, Lcby;->j()I

    move-result v1

    sub-int/2addr v0, v1

    .line 296
    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, p1, -0x1

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 449
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcby;->i(I)V

    .line 450
    return-void
.end method

.method public f()I
    .locals 1

    .prologue
    .line 477
    iget-boolean v0, p0, Lcby;->l:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcby;->g()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public f(I)Lbmd;
    .locals 2

    .prologue
    .line 303
    invoke-virtual {p0, p1}, Lcby;->b(I)Z

    move-result v0

    const-string v1, "getClipFromViewPosition called with non-clip position"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 304
    iget-object v0, p0, Lcby;->b:Ljava/util/List;

    invoke-virtual {p0, p1}, Lcby;->e(I)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    return-object v0
.end method

.method public g(I)Z
    .locals 1

    .prologue
    .line 333
    invoke-virtual {p0, p1}, Lcby;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcby;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcby;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 374
    iget-object v0, p0, Lcby;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 375
    invoke-direct {p0}, Lcby;->j()I

    move-result v1

    add-int/2addr v0, v1

    invoke-direct {p0}, Lcby;->k()I

    move-result v1

    add-int/2addr v1, v0

    .line 376
    iget-boolean v0, p0, Lcby;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 354
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 3

    .prologue
    .line 381
    const-string v0, "position"

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcby;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {p1, v0, v1, v2}, Lcec;->a(ILjava/lang/CharSequence;II)I

    .line 383
    iget v0, p0, Lcby;->j:I

    if-ne p1, v0, :cond_0

    .line 384
    const-wide v0, 0x80000002L

    .line 392
    :goto_0
    return-wide v0

    .line 385
    :cond_0
    if-nez p1, :cond_1

    .line 386
    const-wide v0, 0x80000001L

    goto :goto_0

    .line 387
    :cond_1
    invoke-virtual {p0}, Lcby;->f()I

    move-result v0

    if-ne p1, v0, :cond_2

    .line 388
    const-wide v0, 0x80000003L

    goto :goto_0

    .line 389
    :cond_2
    invoke-direct {p0}, Lcby;->h()I

    move-result v0

    if-ne p1, v0, :cond_3

    .line 390
    const-wide v0, 0x80000000L

    goto :goto_0

    .line 392
    :cond_3
    iget-object v0, p0, Lcby;->b:Ljava/util/List;

    invoke-virtual {p0, p1}, Lcby;->e(I)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget v0, v0, Lbmd;->b:I

    int-to-long v0, v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 339
    iget v0, p0, Lcby;->j:I

    if-ne p1, v0, :cond_0

    .line 340
    const/4 v0, 0x3

    .line 348
    :goto_0
    return v0

    .line 341
    :cond_0
    if-nez p1, :cond_1

    .line 342
    const/4 v0, 0x2

    goto :goto_0

    .line 343
    :cond_1
    invoke-virtual {p0}, Lcby;->f()I

    move-result v0

    if-ne p1, v0, :cond_2

    .line 344
    const/4 v0, 0x4

    goto :goto_0

    .line 345
    :cond_2
    invoke-direct {p0}, Lcby;->h()I

    move-result v0

    if-ne p1, v0, :cond_3

    .line 346
    const/4 v0, 0x1

    goto :goto_0

    .line 348
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    .prologue
    const-wide v10, 0x80000003L

    const-wide v8, 0x80000002L

    const-wide v6, 0x80000001L

    const-wide v4, 0x80000000L

    .line 398
    const-string v0, "invalid position"

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcby;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {p1, v0, v1, v2}, Lcec;->a(ILjava/lang/CharSequence;II)I

    .line 400
    invoke-virtual {p0, p1}, Lcby;->getItemId(I)J

    move-result-wide v0

    .line 401
    cmp-long v2, v0, v8

    if-eqz v2, :cond_0

    cmp-long v2, v0, v6

    if-eqz v2, :cond_0

    cmp-long v2, v0, v4

    if-eqz v2, :cond_0

    cmp-long v2, v0, v10

    if-nez v2, :cond_6

    .line 403
    :cond_0
    instance-of v2, p2, Lcbx;

    if-eqz v2, :cond_2

    check-cast p2, Lcbx;

    :goto_0
    cmp-long v2, v0, v4

    if-nez v2, :cond_3

    iget-object v0, p0, Lcby;->d:Lcca;

    invoke-interface {v0, p2}, Lcca;->a(Lcbx;)V

    .line 405
    :cond_1
    :goto_1
    return-object p2

    .line 403
    :cond_2
    new-instance p2, Lcbx;

    iget-object v2, p0, Lcby;->a:Landroid/content/Context;

    invoke-direct {p2, v2}, Lcbx;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_3
    cmp-long v2, v0, v6

    if-nez v2, :cond_4

    iget-object v0, p0, Lcby;->d:Lcca;

    iget-object v1, p0, Lcby;->h:Ljava/lang/String;

    invoke-interface {v0, p2, v1}, Lcca;->a(Lcbx;Ljava/lang/String;)V

    iput-object p2, p0, Lcby;->i:Lcbx;

    goto :goto_1

    :cond_4
    cmp-long v2, v0, v8

    if-nez v2, :cond_5

    iget-object v0, p0, Lcby;->d:Lcca;

    invoke-interface {v0, p2}, Lcca;->b(Lcbx;)V

    goto :goto_1

    :cond_5
    cmp-long v0, v0, v10

    if-nez v0, :cond_1

    iget-object v0, p0, Lcby;->d:Lcca;

    invoke-interface {v0, p2}, Lcca;->c(Lcbx;)V

    goto :goto_1

    .line 405
    :cond_6
    invoke-virtual {p0, p1}, Lcby;->e(I)I

    move-result v1

    if-eqz p2, :cond_7

    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v0

    const v2, 0x7f1005e5

    if-ne v0, v2, :cond_7

    :goto_2
    iget-object v0, p0, Lcby;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-object v2, p0, Lcby;->d:Lcca;

    iget-object v1, p0, Lcby;->g:Ljava/util/Map;

    iget-object v3, v0, Lbmd;->e:Ljeg;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbml;

    invoke-interface {v2, p1, p2, v1, v0}, Lcca;->a(ILandroid/view/View;Lbml;Lbmd;)V

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcby;->a:Landroid/content/Context;

    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v2, 0x7f040210

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v0, 0x7f1005e6

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    iget-object v2, p0, Lcby;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f020598

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 369
    const/4 v0, 0x5

    return v0
.end method

.method public h(I)V
    .locals 1

    .prologue
    .line 445
    add-int/lit8 v0, p1, 0x1

    invoke-direct {p0, v0}, Lcby;->i(I)V

    .line 446
    return-void
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 364
    const/4 v0, 0x1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 359
    const/4 v0, 0x0

    return v0
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcby;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 412
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Lcby;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 417
    return-void
.end method
