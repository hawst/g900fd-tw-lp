.class public final Lbol;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lbkp;

.field private final b:Lbmb;


# direct methods
.method public constructor <init>(Lbkp;Lbmb;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string v0, "intervalMetrics"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbkp;

    iput-object v0, p0, Lbol;->a:Lbkp;

    .line 26
    const-string v0, "audioMetrics"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmb;

    iput-object v0, p0, Lbol;->b:Lbmb;

    .line 27
    return-void
.end method

.method public static a(Lbkr;)Lbol;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 16
    new-instance v2, Lbol;

    .line 17
    invoke-interface {p0}, Lbkr;->e()Lbkp;

    move-result-object v0

    const-string v1, "intervalMetrics"

    invoke-static {v0, v1, v5}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbkp;

    .line 18
    new-instance v3, Lbmb;

    invoke-interface {p0}, Lbkr;->d()Lbku;

    move-result-object v1

    const-string v4, "pointMetrics"

    invoke-static {v1, v4, v5}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbku;

    invoke-direct {v3, v1}, Lbmb;-><init>(Lbku;)V

    invoke-direct {v2, v0, v3}, Lbol;-><init>(Lbkp;Lbmb;)V

    return-object v2
.end method


# virtual methods
.method public a()Lbkp;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lbol;->a:Lbkp;

    return-object v0
.end method

.method public b()Lbmb;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lbol;->b:Lbmb;

    return-object v0
.end method
