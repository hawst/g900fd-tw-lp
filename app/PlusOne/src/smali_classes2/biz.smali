.class public Lbiz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbhs;
.implements Lbiy;


# instance fields
.field private a:Lbih;

.field private b:Lbix;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/nio/ByteBuffer;)Lbix;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 62
    iget-object v4, p0, Lbiz;->b:Lbix;

    const-string v0, "byteBuffer"

    invoke-static {p1, v0, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    iput-object v0, v4, Lbix;->b:Ljava/nio/ByteBuffer;

    .line 63
    invoke-virtual {p0}, Lbiz;->d()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v3

    .line 86
    :goto_0
    return-object v0

    .line 68
    :cond_0
    :try_start_0
    iget-object v0, p0, Lbiz;->a:Lbih;

    invoke-interface {v0}, Lbih;->g()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v3

    .line 69
    goto :goto_0

    .line 71
    :catch_0
    move-exception v0

    .line 72
    const-string v1, "Error checking read status."

    invoke-static {v1, v0}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/Throwable;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 76
    :cond_1
    :try_start_1
    iget-object v0, p0, Lbiz;->b:Lbix;

    iget-object v3, p0, Lbiz;->a:Lbih;

    iget-object v4, p0, Lbiz;->b:Lbix;

    iget-object v4, v4, Lbix;->b:Ljava/nio/ByteBuffer;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Lbih;->a(Ljava/nio/ByteBuffer;I)I

    move-result v3

    iput v3, v0, Lbix;->c:I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 80
    iget-object v0, p0, Lbiz;->b:Lbix;

    iget-object v3, p0, Lbiz;->a:Lbih;

    invoke-interface {v3}, Lbih;->d()J

    move-result-wide v4

    iput-wide v4, v0, Lbix;->d:J

    .line 81
    iget-object v3, p0, Lbiz;->b:Lbix;

    iget-object v0, p0, Lbiz;->a:Lbih;

    .line 82
    invoke-interface {v0}, Lbih;->c()I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, v3, Lbix;->e:Z

    .line 83
    iget-object v0, p0, Lbiz;->a:Lbih;

    invoke-interface {v0}, Lbih;->b()Z

    .line 84
    iget-object v0, p0, Lbiz;->b:Lbix;

    invoke-virtual {p0}, Lbiz;->d()Z

    move-result v3

    if-nez v3, :cond_3

    :goto_2
    iput-boolean v1, v0, Lbix;->f:Z

    .line 86
    iget-object v0, p0, Lbiz;->b:Lbix;

    goto :goto_0

    .line 77
    :catch_1
    move-exception v0

    .line 78
    const-string v1, "Error reading sample data."

    invoke-static {v1, v0}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/Throwable;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    :cond_2
    move v0, v2

    .line 82
    goto :goto_1

    :cond_3
    move v1, v2

    .line 84
    goto :goto_2
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    check-cast p1, Ljava/nio/ByteBuffer;

    invoke-virtual {p0, p1}, Lbiz;->a(Ljava/nio/ByteBuffer;)Lbix;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lbiz;->a:Lbih;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 102
    return-void
.end method

.method public a(Lbih;)V
    .locals 3

    .prologue
    .line 42
    iget-object v0, p0, Lbiz;->a:Lbih;

    const-string v1, "mMediaExtractor"

    const-string v2, "can only be initialized once."

    invoke-static {v0, v1, v2}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 43
    const-string v0, "mediaExtractor"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbih;

    iput-object v0, p0, Lbiz;->a:Lbih;

    .line 44
    new-instance v0, Lbix;

    invoke-direct {v0}, Lbix;-><init>()V

    iput-object v0, p0, Lbiz;->b:Lbix;

    .line 45
    iget-object v0, p0, Lbiz;->b:Lbix;

    iget-object v1, p0, Lbiz;->a:Lbih;

    iget-object v2, p0, Lbiz;->a:Lbih;

    .line 46
    invoke-interface {v2}, Lbih;->e()I

    move-result v2

    invoke-interface {v1, v2}, Lbih;->a(I)Landroid/media/MediaFormat;

    move-result-object v1

    iput-object v1, v0, Lbix;->a:Landroid/media/MediaFormat;

    .line 47
    return-void
.end method

.method public a(J)Z
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lbiz;->a:Lbih;

    invoke-static {v0, p1, p2}, Lbke;->a(Lbih;J)V

    .line 52
    const/4 v0, 0x1

    return v0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lbiz;->a:Lbih;

    invoke-interface {v0}, Lbih;->e()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Landroid/media/MediaFormat;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lbiz;->b:Lbix;

    iget-object v0, v0, Lbix;->a:Landroid/media/MediaFormat;

    return-object v0
.end method

.method protected f()J
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Lbiz;->a:Lbih;

    invoke-interface {v0}, Lbih;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lbiz;->a:Lbih;

    invoke-interface {v0}, Lbih;->h()V

    .line 119
    return-void
.end method
