.class public Ljhs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljhi;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljhy;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljho;

.field private final d:Z

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljhk;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Ljhs;->b:Landroid/util/SparseArray;

    .line 45
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ljhs;->a:Landroid/content/Context;

    .line 46
    iput-boolean p2, p0, Ljhs;->d:Z

    .line 48
    new-instance v0, Ljho;

    invoke-direct {v0, p1}, Ljho;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ljhs;->c:Ljho;

    .line 49
    invoke-direct {p0}, Ljhs;->a()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Ljhs;->e:Ljava/util/Map;

    .line 50
    return-void
.end method

.method private a()Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljhk;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 59
    iget-object v0, p0, Ljhs;->a:Landroid/content/Context;

    const-class v2, Ljhk;

    .line 60
    invoke-static {v0, v2}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    .line 61
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhk;

    .line 62
    invoke-interface {v0}, Ljhk;->a()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 64
    :cond_0
    return-object v1
.end method

.method static synthetic a(Ljhs;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 22
    iget-object v0, p0, Ljhs;->a:Landroid/content/Context;

    const-class v2, Lhei;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "logged_in"

    aput-object v3, v2, v1

    invoke-interface {v0, v2}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Ljhs;->b(I)Ljhy;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljhy;->h()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Ljhy;->c()V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method


# virtual methods
.method public synthetic a(I)Ljhh;
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Ljhs;->b(I)Ljhy;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized b(I)Ljhy;
    .locals 4

    .prologue
    .line 69
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ljhs;->c:Ljho;

    invoke-virtual {v0, p1}, Ljho;->a(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 70
    const/4 v0, 0x0

    .line 83
    :cond_0
    :goto_0
    monitor-exit p0

    return-object v0

    .line 77
    :cond_1
    :try_start_1
    iget-object v0, p0, Ljhs;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhy;

    .line 78
    if-nez v0, :cond_0

    .line 79
    new-instance v0, Ljhy;

    iget-object v1, p0, Ljhs;->a:Landroid/content/Context;

    iget-object v2, p0, Ljhs;->e:Ljava/util/Map;

    iget-boolean v3, p0, Ljhs;->d:Z

    invoke-direct {v0, v1, p1, v2, v3}, Ljhy;-><init>(Landroid/content/Context;ILjava/util/Map;Z)V

    .line 81
    iget-object v1, p0, Ljhs;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method c(I)Z
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Ljhs;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhy;

    .line 93
    if-eqz v0, :cond_0

    .line 94
    invoke-virtual {v0}, Ljhy;->d()V

    .line 95
    const/4 v0, 0x1

    .line 97
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
