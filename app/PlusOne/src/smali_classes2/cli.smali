.class public final Lcli;
.super Lcjy;
.source "PG"


# static fields
.field private static final W:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final X:[I


# instance fields
.field private final Y:Lckf;

.field private final Z:Lcmv;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 24
    const/16 v0, 0x1a

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcli;->X:[I

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 42
    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Integer;

    .line 44
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    .line 45
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    .line 46
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/4 v2, 0x3

    const/16 v3, 0x9

    .line 47
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const/16 v3, 0xe9

    .line 48
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const/16 v3, 0xe8

    .line 49
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 42
    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 51
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcli;->W:Ljava/util/List;

    .line 52
    return-void

    .line 24
    nop

    :array_0
    .array-data 4
        0x7f02021d
        0x7f02021c
        0x7f02021f
        0x7f02021e
        0x7f020221
        0x7f020220
        0x7f020223
        0x7f020222
        0x7f020225
        0x7f020224
        0x7f020227
        0x7f020226
        0x7f020229
        0x7f020228
        0x7f02022b
        0x7f02022a
        0x7f02022d
        0x7f02022c
        0x7f020215
        0x7f020214
        0x7f020217
        0x7f020216
        0x7f020219
        0x7f020218
        0x7f02021b
        0x7f02021a
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 21
    invoke-direct {p0}, Lcjy;-><init>()V

    .line 54
    new-instance v0, Lckf;

    const/4 v1, 0x3

    sget-object v2, Lcli;->X:[I

    invoke-direct {v0, p0, v1, v2}, Lckf;-><init>(Lcjy;I[I)V

    iput-object v0, p0, Lcli;->Y:Lckf;

    .line 56
    new-instance v0, Lcll;

    invoke-direct {v0, p0}, Lcll;-><init>(Lcli;)V

    iput-object v0, p0, Lcli;->Z:Lcmv;

    .line 129
    return-void
.end method

.method static synthetic a(Lcli;)Lckf;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcli;->Y:Lckf;

    return-object v0
.end method

.method private a(IZ)Ljava/lang/String;
    .locals 4

    .prologue
    .line 121
    if-eqz p2, :cond_0

    const-string v0, "%s %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const v3, 0x7f0a009b

    .line 122
    invoke-virtual {p0, v3}, Lcli;->e_(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    add-int/lit8 v3, p1, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 123
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1}, Lcli;->g(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lcli;)Lcmv;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcli;->Z:Lcmv;

    return-object v0
.end method


# virtual methods
.method protected X()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    sget-object v0, Lcli;->W:Ljava/util/List;

    return-object v0
.end method

.method public a(ILjava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 113
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 114
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcli;->a(IZ)Ljava/lang/String;

    move-result-object v0

    .line 116
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcjy;->b(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Lckp;)V
    .locals 3

    .prologue
    .line 76
    const v0, 0x7f0203a3

    const v1, 0x7f0a009b

    .line 78
    invoke-virtual {p0, v1}, Lcli;->e_(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lclj;

    invoke-direct {v2, p0}, Lclj;-><init>(Lcli;)V

    .line 76
    invoke-virtual {p1, v0, v1, v2}, Lckp;->a(ILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 89
    const v0, 0x7f02039c

    const v1, 0x7f0a0031

    .line 91
    invoke-virtual {p0, v1}, Lcli;->e_(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lclk;

    invoke-direct {v2, p0}, Lclk;-><init>(Lcli;)V

    .line 89
    invoke-virtual {p1, v0, v1, v2}, Lckp;->a(ILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 100
    return-void
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 61
    new-instance v0, Lhmk;

    sget-object v1, Lonc;->t:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method public b(ILjava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 104
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 105
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcli;->a(IZ)Ljava/lang/String;

    move-result-object v0

    .line 107
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcjy;->b(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 66
    const/16 v0, 0x10

    return v0
.end method
