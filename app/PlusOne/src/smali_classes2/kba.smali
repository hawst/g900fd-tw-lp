.class abstract Lkba;
.super Lkaz;
.source "PG"


# static fields
.field private static u:Z

.field private static v:I


# instance fields
.field g:I

.field h:Landroid/view/View;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lkaz;-><init>(Landroid/content/Context;)V

    .line 34
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 68
    invoke-super {p0, p1}, Lkaz;->a(I)V

    .line 70
    invoke-virtual {p0}, Lkba;->c()I

    move-result v0

    int-to-float v0, v0

    const v1, 0x3f19999a    # 0.6f

    mul-float/2addr v0, v1

    float-to-int v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    .line 69
    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Lkba;->g:I

    .line 71
    return-void
.end method

.method protected a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 60
    invoke-super {p0, p1}, Lkaz;->a(Landroid/content/Context;)V

    .line 61
    sget-boolean v0, Lkba;->u:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00af

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lkba;->v:I

    const/4 v0, 0x1

    sput-boolean v0, Lkba;->u:Z

    .line 62
    :cond_0
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lkba;->h:Landroid/view/View;

    .line 63
    iget-object v0, p0, Lkba;->h:Landroid/view/View;

    const v1, 0x7f02049f

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 64
    return-void
.end method

.method public b()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-virtual {p0}, Lkba;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 76
    iget-object v0, p0, Lkba;->b:Lkzw;

    invoke-virtual {v0}, Lkzw;->j()[Lkzx;

    move-result-object v3

    move v0, v1

    .line 77
    :goto_0
    iget v4, p0, Lkba;->r:I

    if-ge v0, v4, :cond_1

    .line 78
    aget-object v4, v3, v0

    invoke-virtual {v4}, Lkzx;->d()Lkzv;

    move-result-object v4

    .line 79
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lkzv;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 80
    invoke-virtual {p0, v2, v0}, Lkba;->a(Landroid/content/Context;I)Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 77
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_1
    iget-object v0, p0, Lkba;->h:Landroid/view/View;

    invoke-virtual {p0, v0}, Lkba;->addView(Landroid/view/View;)V

    .line 84
    :goto_1
    iget v0, p0, Lkba;->r:I

    if-ge v1, v0, :cond_2

    .line 85
    invoke-virtual {p0, v2, v1}, Lkba;->d(Landroid/content/Context;I)Landroid/widget/TextView;

    .line 86
    invoke-virtual {p0, v2, v1}, Lkba;->f(Landroid/content/Context;I)Landroid/view/View;

    .line 87
    invoke-virtual {p0, v2, v1}, Lkba;->e(Landroid/content/Context;I)Landroid/view/View;

    .line 88
    invoke-virtual {p0, v2, v1}, Lkba;->c(Landroid/content/Context;I)Landroid/widget/TextView;

    move-result-object v0

    aget-object v4, v3, v1

    invoke-virtual {v4}, Lkzx;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 90
    :cond_2
    iget-object v0, p0, Lkba;->p:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lkba;->addView(Landroid/view/View;)V

    .line 91
    return-void
.end method

.method c()I
    .locals 4

    .prologue
    .line 162
    iget v0, p0, Lkba;->c:I

    int-to-double v0, v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    div-double/2addr v0, v2

    const-wide/high16 v2, 0x3ff4000000000000L    # 1.25

    mul-double/2addr v0, v2

    double-to-int v0, v0

    return v0
.end method

.method c(Landroid/content/Context;I)Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 95
    invoke-super {p0, p1, p2}, Lkaz;->c(Landroid/content/Context;I)Landroid/widget/TextView;

    move-result-object v0

    .line 96
    const/16 v1, 0x24

    invoke-static {p1, v0, v1}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 98
    return-object v0
.end method

.method c(I)V
    .locals 5

    .prologue
    const/4 v1, -0x2

    .line 111
    invoke-super {p0, p1}, Lkaz;->c(I)V

    .line 112
    iget-object v0, p0, Lkba;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 113
    invoke-virtual {v0, v1, v1}, Landroid/widget/TextView;->measure(II)V

    .line 115
    iget-object v1, p0, Lkba;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 116
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 117
    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 118
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    .line 121
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v3

    sget v4, Lkba;->v:I

    sub-int/2addr v3, v4

    .line 122
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v3, v4

    .line 123
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v4, v2

    .line 124
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    .line 119
    invoke-virtual {v0, v2, v3, v4, v1}, Landroid/widget/TextView;->layout(IIII)V

    .line 125
    return-void
.end method

.method d(Landroid/content/Context;I)Landroid/widget/TextView;
    .locals 2

    .prologue
    .line 103
    invoke-super {p0, p1, p2}, Lkaz;->d(Landroid/content/Context;I)Landroid/widget/TextView;

    move-result-object v0

    .line 104
    const/16 v1, 0x28

    invoke-static {p1, v0, v1}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 106
    return-object v0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 142
    invoke-super {p0}, Lkaz;->d()V

    .line 143
    iget-object v1, p0, Lkba;->h:Landroid/view/View;

    invoke-virtual {p0}, Lkba;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 144
    return-void

    .line 143
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method d(I)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 129
    iget-object v0, p0, Lkba;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 130
    iget-object v1, p0, Lkba;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 131
    iget v2, p0, Lkba;->q:I

    if-ne p1, v2, :cond_0

    iget-object v2, p0, Lkba;->p:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v2

    .line 132
    :goto_0
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v4, v2

    .line 133
    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 135
    invoke-virtual {p0, p1, v4}, Lkba;->a(II)V

    .line 136
    invoke-virtual {p0, v0, v1, v2, v3}, Lkba;->a(Landroid/view/View;Landroid/view/View;II)V

    .line 138
    return-void

    :cond_0
    move v2, v3

    .line 131
    goto :goto_0
.end method

.method e()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation

    .prologue
    .line 148
    invoke-super {p0}, Lkaz;->e()Ljava/util/ArrayList;

    move-result-object v0

    .line 149
    iget-object v1, p0, Lkba;->s:Lkbg;

    iget-object v2, p0, Lkba;->h:Landroid/view/View;

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2, v3, v4}, Lkbg;->b(Landroid/view/View;FF)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 150
    return-object v0
.end method

.method f()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation

    .prologue
    .line 155
    invoke-super {p0}, Lkaz;->f()Ljava/util/ArrayList;

    move-result-object v0

    .line 156
    iget-object v1, p0, Lkba;->s:Lkbg;

    iget-object v2, p0, Lkba;->h:Landroid/view/View;

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lkbg;->b(Landroid/view/View;FF)Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 157
    return-object v0
.end method
