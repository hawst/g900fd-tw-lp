.class final Lbjx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbjr;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 70
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/media/MediaFormat;)Lbjs;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 75
    const-string v0, "mediaFormat"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 76
    const-string v0, "mime"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lbjt;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 78
    :try_start_0
    new-instance v0, Lbkb;

    .line 79
    invoke-static {v3}, Landroid/media/MediaCodec;->createDecoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v2

    invoke-direct {v0, v2, v3}, Lbkb;-><init>(Landroid/media/MediaCodec;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 83
    :goto_0
    return-object v0

    .line 81
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 82
    invoke-static {}, Lbjt;->e()Ljava/lang/String;

    move-result-object v4

    const-string v5, "Problem creating codec by type: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v5, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v4, v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 83
    goto :goto_0

    .line 82
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method
