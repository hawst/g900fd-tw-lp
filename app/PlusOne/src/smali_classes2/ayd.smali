.class public Layd;
.super Laye;
.source "PG"


# instance fields
.field private final a:I

.field private final b:F

.field private final c:F

.field private final d:Landroid/animation/TimeInterpolator;

.field private final e:Landroid/graphics/Matrix;

.field private final f:Landroid/graphics/Matrix;

.field private final g:Landroid/graphics/Matrix;

.field private final h:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Layj;FFLandroid/animation/TimeInterpolator;I)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0, p1}, Laye;-><init>(Layj;)V

    .line 31
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Layd;->e:Landroid/graphics/Matrix;

    .line 32
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Layd;->f:Landroid/graphics/Matrix;

    .line 33
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Layd;->g:Landroid/graphics/Matrix;

    .line 34
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Layd;->h:Landroid/graphics/Matrix;

    .line 51
    const/4 v0, -0x1

    if-eq p5, v0, :cond_0

    if-ltz p5, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "rotationStyledValueIndex should be NO_ROTATION or >= 0"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 54
    iput p5, p0, Layd;->a:I

    .line 55
    iput p2, p0, Layd;->b:F

    .line 56
    iput p3, p0, Layd;->c:F

    .line 57
    iput-object p4, p0, Layd;->d:Landroid/animation/TimeInterpolator;

    .line 58
    return-void

    .line 51
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Layl;Z)F
    .locals 2

    .prologue
    .line 136
    iget v0, p0, Layd;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 137
    const/4 v0, 0x0

    .line 141
    :goto_0
    return v0

    .line 139
    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p1, Layl;->x:Look;

    .line 140
    invoke-direct {p0, v0}, Layd;->a(Look;)F

    move-result v0

    goto :goto_0

    :cond_1
    iget-object v0, p1, Layl;->y:Look;

    .line 141
    invoke-direct {p0, v0}, Layd;->a(Look;)F

    move-result v0

    goto :goto_0
.end method

.method private a(Look;)F
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 145
    const-string v0, "clipParams"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 146
    iget-object v0, p1, Look;->f:[Ljava/lang/Double;

    const-string v1, "clipParams.styledValue"

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 147
    iget v0, p0, Layd;->a:I

    const-string v1, "mRotationStyledValueIndex"

    iget-object v2, p1, Look;->f:[Ljava/lang/Double;

    invoke-static {v0, v1, v2}, Lcec;->a(ILjava/lang/CharSequence;[Ljava/lang/Object;)I

    .line 149
    iget-object v0, p1, Look;->f:[Ljava/lang/Double;

    iget v1, p0, Layd;->a:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Double;->floatValue()F

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Layl;Lbof;)Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Layd;->e:Landroid/graphics/Matrix;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p1, v1}, Layd;->a(Landroid/graphics/Matrix;Layl;Z)V

    .line 82
    iget-object v0, p0, Layd;->e:Landroid/graphics/Matrix;

    return-object v0
.end method

.method protected a(Landroid/graphics/Matrix;Layl;Z)V
    .locals 6

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v4, 0x3f000000    # 0.5f

    .line 106
    invoke-virtual {p1}, Landroid/graphics/Matrix;->reset()V

    .line 108
    invoke-direct {p0, p2, p3}, Layd;->a(Layl;Z)F

    move-result v0

    .line 109
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v2, 0x38d1b717    # 1.0E-4f

    cmpl-float v0, v0, v2

    if-ltz v0, :cond_0

    .line 111
    invoke-direct {p0, p2, p3}, Layd;->a(Layl;Z)F

    move-result v0

    iget v2, p2, Layl;->u:F

    .line 110
    invoke-static {p1, v0, v2}, Lbag;->a(Landroid/graphics/Matrix;FF)V

    .line 114
    :cond_0
    if-eqz p3, :cond_3

    invoke-static {p2}, Lbag;->b(Layl;)F

    move-result v0

    .line 116
    :goto_0
    iget-object v2, p0, Layd;->d:Landroid/animation/TimeInterpolator;

    if-eqz v2, :cond_1

    .line 117
    iget-object v2, p0, Layd;->d:Landroid/animation/TimeInterpolator;

    invoke-interface {v2, v0}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    .line 119
    :cond_1
    iget v2, p0, Layd;->b:F

    iget v3, p0, Layd;->c:F

    sub-float/2addr v3, v2

    mul-float/2addr v0, v3

    add-float/2addr v0, v2

    .line 120
    invoke-virtual {p1, v0, v0, v4, v4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 122
    if-eqz p3, :cond_4

    iget-object v0, p2, Layl;->s:Lbmj;

    .line 123
    invoke-static {v0}, Lbag;->a(Lbmj;)F

    move-result v0

    .line 125
    :goto_1
    cmpg-float v2, v0, v1

    if-gez v2, :cond_2

    div-float v0, v1, v0

    move v5, v1

    move v1, v0

    move v0, v5

    :cond_2
    invoke-virtual {p1, v1, v0, v4, v4}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 126
    return-void

    .line 115
    :cond_3
    invoke-static {p2}, Lbag;->c(Layl;)F

    move-result v0

    goto :goto_0

    .line 123
    :cond_4
    iget-object v0, p2, Layl;->t:Lbmj;

    .line 124
    invoke-static {v0}, Lbag;->a(Lbmj;)F

    move-result v0

    goto :goto_1
.end method

.method public a(Layl;)Z
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x1

    return v0
.end method

.method public b(Layl;)Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Layd;->f:Landroid/graphics/Matrix;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Layd;->a(Landroid/graphics/Matrix;Layl;Z)V

    .line 88
    iget-object v0, p0, Layd;->f:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public c(Layl;)Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Layd;->g:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 94
    iget-object v0, p1, Layl;->s:Lbmj;

    iget-object v1, p0, Layd;->g:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lbmj;->a(Landroid/graphics/Matrix;)V

    .line 95
    iget-object v0, p0, Layd;->g:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public d(Layl;)Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Layd;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 101
    iget-object v0, p1, Layl;->t:Lbmj;

    iget-object v1, p0, Layd;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lbmj;->a(Landroid/graphics/Matrix;)V

    .line 102
    iget-object v0, p0, Layd;->h:Landroid/graphics/Matrix;

    return-object v0
.end method
