.class public Lazc;
.super Laye;
.source "PG"


# instance fields
.field private final a:Landroid/graphics/Matrix;

.field private final b:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Layj;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p1}, Laye;-><init>(Layj;)V

    .line 15
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lazc;->a:Landroid/graphics/Matrix;

    .line 16
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lazc;->b:Landroid/graphics/Matrix;

    .line 20
    return-void
.end method


# virtual methods
.method public a(Layl;Lbof;)Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 24
    iget-object v0, p0, Lazc;->a:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 25
    iget-object v0, p1, Layl;->s:Lbmj;

    iget-object v1, p0, Lazc;->a:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lbmj;->a(Landroid/graphics/Matrix;)V

    .line 26
    iget-object v0, p0, Lazc;->a:Landroid/graphics/Matrix;

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Lbah;->a(Landroid/graphics/Matrix;Layl;Z)V

    .line 27
    iget-object v0, p0, Lazc;->a:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public b(Layl;)Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 32
    iget-object v0, p1, Layl;->k:Lbmg;

    sget-object v1, Lbmg;->c:Lbmg;

    if-eq v0, v1, :cond_0

    .line 33
    invoke-virtual {p0}, Lazc;->h()Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->b(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 39
    :goto_0
    return-object v0

    .line 36
    :cond_0
    iget-object v0, p0, Lazc;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 37
    iget-object v0, p1, Layl;->t:Lbmj;

    iget-object v1, p0, Lazc;->b:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Lbmj;->a(Landroid/graphics/Matrix;)V

    .line 38
    iget-object v0, p0, Lazc;->b:Landroid/graphics/Matrix;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lbah;->a(Landroid/graphics/Matrix;Layl;Z)V

    .line 39
    iget-object v0, p0, Lazc;->b:Landroid/graphics/Matrix;

    goto :goto_0
.end method
