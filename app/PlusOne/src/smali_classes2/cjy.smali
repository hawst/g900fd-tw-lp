.class public abstract Lcjy;
.super Lcje;
.source "PG"

# interfaces
.implements Lckq;
.implements Lhdb;


# instance fields
.field public O:Landroid/widget/FrameLayout;

.field public P:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

.field public Q:Landroid/widget/TextView;

.field public R:Lhcs;

.field public S:Lckp;

.field public T:Lhdd;

.field public U:Lhcz;

.field public V:Lhbf;

.field private W:Landroid/view/View;

.field private X:Landroid/view/View;

.field private Y:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

.field private Z:I

.field private aa:I

.field private final ab:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Lcje;-><init>()V

    .line 88
    new-instance v0, Lcjz;

    invoke-direct {v0, p0}, Lcjz;-><init>(Lcjy;)V

    iput-object v0, p0, Lcjy;->ab:Ljava/lang/Runnable;

    .line 955
    return-void
.end method

.method static synthetic a(Lcjy;)Landroid/view/View;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcjy;->X:Landroid/view/View;

    return-object v0
.end method

.method private at()V
    .locals 3

    .prologue
    .line 273
    invoke-virtual {p0}, Lcjy;->ah()Lchp;

    move-result-object v0

    invoke-virtual {v0}, Lchp;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 274
    iget-object v1, p0, Lcjy;->T:Lhdd;

    if-eqz v1, :cond_0

    .line 276
    invoke-virtual {p0}, Lcjy;->ah()Lchp;

    move-result-object v1

    invoke-virtual {v1}, Lchp;->h()Lcom/google/android/libraries/photoeditor/core/FilterChain;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getPostRotation()I

    move-result v1

    invoke-static {v1}, Lhbg;->a(I)F

    move-result v1

    .line 277
    iget-object v2, p0, Lcjy;->T:Lhdd;

    invoke-virtual {v2, v1}, Lhdd;->a(F)V

    .line 278
    iget-object v1, p0, Lcjy;->T:Lhdd;

    invoke-virtual {v1, v0}, Lhdd;->a(Landroid/graphics/Bitmap;)V

    .line 280
    :cond_0
    return-void
.end method

.method private au()Ljava/lang/String;
    .locals 4

    .prologue
    .line 382
    invoke-virtual {p0}, Lcjy;->aq()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 384
    invoke-virtual {p0}, Lcjy;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    .line 385
    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getActiveParameterKey()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v1

    .line 386
    if-eqz v1, :cond_0

    const-string v0, "%+d"

    :goto_0
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 388
    :goto_1
    return-object v0

    .line 386
    :cond_0
    const-string v0, "%d"

    goto :goto_0

    .line 388
    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method private av()V
    .locals 3

    .prologue
    .line 472
    invoke-virtual {p0}, Lcjy;->ah()Lchp;

    move-result-object v0

    invoke-virtual {v0}, Lchp;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 473
    if-nez v0, :cond_0

    .line 475
    invoke-virtual {p0}, Lcje;->n()Lz;

    move-result-object v0

    check-cast v0, Lcky;

    new-instance v1, Lckd;

    invoke-direct {v1, p0}, Lckd;-><init>(Lcjy;)V

    const/16 v2, 0x64

    invoke-interface {v0, v1, v2}, Lcky;->a(Ljava/lang/Runnable;I)V

    .line 520
    :goto_0
    return-void

    .line 488
    :cond_0
    iget-object v1, p0, Lcjy;->T:Lhdd;

    if-eqz v1, :cond_1

    .line 489
    iget-object v1, p0, Lcjy;->T:Lhdd;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lhdd;->a(Z)V

    .line 492
    :cond_1
    iget-object v1, p0, Lcjy;->X:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 494
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iput v1, p0, Lcjy;->Z:I

    .line 495
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lcjy;->aa:I

    .line 497
    invoke-virtual {p0}, Lcjy;->y_()V

    .line 499
    invoke-virtual {p0}, Lcjy;->ag()V

    .line 505
    invoke-virtual {p0}, Lcje;->n()Lz;

    move-result-object v0

    check-cast v0, Lcky;

    new-instance v1, Lcke;

    invoke-direct {v1, p0}, Lcke;-><init>(Lcjy;)V

    const/16 v2, 0xa

    invoke-interface {v0, v1, v2}, Lcky;->a(Ljava/lang/Runnable;I)V

    goto :goto_0
.end method

.method static synthetic b(Lcjy;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcjy;->av()V

    return-void
.end method

.method static synthetic c(Lcjy;)Z
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcjy;->Z:I

    if-lez v0, :cond_0

    iget v0, p0, Lcjy;->aa:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected V()V
    .locals 1

    .prologue
    .line 765
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcjy;->k(Z)V

    .line 766
    invoke-super {p0}, Lcje;->V()V

    .line 767
    return-void
.end method

.method protected W()Lhbi;
    .locals 3

    .prologue
    .line 325
    invoke-virtual {p0}, Lcjy;->ah()Lchp;

    move-result-object v0

    .line 326
    if-nez v0, :cond_0

    .line 327
    const/4 v0, 0x0

    .line 335
    :goto_0
    return-object v0

    .line 330
    :cond_0
    iget-object v1, p0, Lcjy;->V:Lhbf;

    if-nez v1, :cond_1

    .line 331
    new-instance v1, Lhbf;

    invoke-direct {v1}, Lhbf;-><init>()V

    iput-object v1, p0, Lcjy;->V:Lhbf;

    .line 333
    :cond_1
    iget-object v1, p0, Lcjy;->V:Lhbf;

    invoke-virtual {v0}, Lchp;->g()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0}, Lchp;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lhbf;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 335
    iget-object v0, p0, Lcjy;->V:Lhbf;

    goto :goto_0
.end method

.method protected X()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 210
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method protected final a(ILandroid/graphics/Bitmap;)Landroid/graphics/Rect;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 344
    int-to-float v0, p1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 345
    int-to-float v1, p1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 346
    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 347
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 349
    new-instance v1, Landroid/graphics/Rect;

    .line 350
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-direct {v1, v4, v4, v2, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v1
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 100
    iget-object v0, p0, Lcjy;->at:Llnl;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 102
    const v1, 0x7f0400bb

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcjy;->W:Landroid/view/View;

    .line 104
    iget-object v0, p0, Lcjy;->W:Landroid/view/View;

    if-nez v0, :cond_0

    .line 105
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to inflate the filter fragment"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_0
    iget-object v0, p0, Lcjy;->W:Landroid/view/View;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClickable(Z)V

    .line 112
    iget-object v0, p0, Lcjy;->W:Landroid/view/View;

    const v1, 0x7f1002d1

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcjy;->O:Landroid/widget/FrameLayout;

    .line 113
    iget-object v0, p0, Lcjy;->W:Landroid/view/View;

    const v1, 0x7f1002d2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    iput-object v0, p0, Lcjy;->P:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    .line 114
    iget-object v0, p0, Lcjy;->W:Landroid/view/View;

    const v1, 0x7f1002d4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcjy;->Q:Landroid/widget/TextView;

    .line 116
    iget-object v0, p0, Lcjy;->O:Landroid/widget/FrameLayout;

    new-instance v1, Lcka;

    invoke-direct {v1, p0}, Lcka;-><init>(Lcjy;)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 127
    invoke-virtual {p0}, Lcjy;->p()Lae;

    move-result-object v0

    .line 128
    if-nez v0, :cond_1

    .line 129
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to get fragment manager"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 132
    :cond_1
    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v0

    .line 133
    new-instance v1, Lckp;

    invoke-direct {v1}, Lckp;-><init>()V

    iput-object v1, p0, Lcjy;->S:Lckp;

    .line 134
    const v1, 0x7f1002d3

    iget-object v2, p0, Lcjy;->S:Lckp;

    const-string v3, "ParameterPanelFragment"

    invoke-virtual {v0, v1, v2, v3}, Lat;->b(ILu;Ljava/lang/String;)Lat;

    .line 136
    invoke-virtual {v0}, Lat;->b()I

    .line 138
    iget-object v0, p0, Lcjy;->S:Lckp;

    invoke-virtual {v0, p0}, Lckp;->a(Lckq;)V

    .line 140
    iget-object v0, p0, Lcjy;->W:Landroid/view/View;

    return-object v0
.end method

.method protected final a(Lckg;Z)V
    .locals 1

    .prologue
    .line 437
    iget-object v0, p0, Lcjy;->S:Lckp;

    if-nez v0, :cond_1

    .line 445
    :cond_0
    :goto_0
    return-void

    .line 441
    :cond_1
    iget-object v0, p0, Lcjy;->S:Lckp;

    invoke-virtual {v0}, Lckp;->b()Lcom/google/android/apps/photoeditor/views/ItemSelectorView;

    move-result-object v0

    .line 442
    if-eqz v0, :cond_0

    .line 443
    invoke-virtual {v0, p1, p2}, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->a(Lcmu;Z)Z

    goto :goto_0
.end method

.method protected a(Lckp;)V
    .locals 0

    .prologue
    .line 233
    return-void
.end method

.method protected final a(Lcmu;Lcmv;)V
    .locals 3

    .prologue
    .line 414
    invoke-virtual {p0}, Lcjy;->n()Lz;

    move-result-object v0

    .line 415
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcjy;->S:Lckp;

    if-nez v1, :cond_1

    .line 433
    :cond_0
    :goto_0
    return-void

    .line 419
    :cond_1
    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 420
    iget-object v1, p0, Lcjy;->S:Lckp;

    invoke-virtual {v1}, Lckp;->b()Lcom/google/android/apps/photoeditor/views/ItemSelectorView;

    move-result-object v1

    .line 421
    if-eqz v1, :cond_0

    .line 425
    new-instance v2, Lckc;

    invoke-direct {v2, v0}, Lckc;-><init>(Landroid/view/LayoutInflater;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->a(Lcmw;)V

    .line 431
    invoke-virtual {v1, p1, p2}, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->a(Lcmu;Lcmv;)V

    .line 432
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->a(Lcmw;)V

    goto :goto_0
.end method

.method protected a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 771
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcjy;->k(Z)V

    .line 772
    invoke-super {p0, p1, p2}, Lcje;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Landroid/graphics/Bitmap;)V

    .line 773
    return-void
.end method

.method protected a(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V
    .locals 3

    .prologue
    .line 214
    new-instance v0, Lcki;

    invoke-direct {v0, p0}, Lcki;-><init>(Lcjy;)V

    iput-object v0, p0, Lcjy;->R:Lhcs;

    .line 215
    iget-object v0, p0, Lcjy;->R:Lhcs;

    invoke-virtual {p0}, Lcjy;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    invoke-virtual {p0}, Lcjy;->X()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lhcs;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Ljava/util/List;)V

    .line 216
    new-instance v0, Lhcz;

    invoke-direct {v0, p1}, Lhcz;-><init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V

    iput-object v0, p0, Lcjy;->U:Lhcz;

    .line 217
    iget-object v0, p0, Lcjy;->U:Lhcz;

    iget-object v1, p0, Lcjy;->R:Lhcs;

    invoke-virtual {v0, v1}, Lhcz;->a(Lhdc;)V

    .line 218
    iget-object v0, p0, Lcjy;->U:Lhcz;

    invoke-virtual {v0, p0}, Lhcz;->a(Lhdb;)V

    .line 220
    iget-object v0, p0, Lcjy;->U:Lhcz;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a(Lhdx;)I

    .line 222
    new-instance v0, Lhdd;

    invoke-direct {v0, p1}, Lhdd;-><init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V

    iput-object v0, p0, Lcjy;->T:Lhdd;

    .line 223
    iget-object v0, p0, Lcjy;->T:Lhdd;

    invoke-virtual {p0}, Lcjy;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0024

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lhdd;->a(I)V

    .line 225
    iget-object v0, p0, Lcjy;->T:Lhdd;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a(Lhdx;)I

    .line 226
    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lcjy;->S:Lckp;

    if-eqz v0, :cond_0

    .line 367
    iget-object v0, p0, Lcjy;->S:Lckp;

    invoke-virtual {v0, p1, p2}, Lckp;->a(Ljava/lang/String;Ljava/lang/String;)Z

    .line 369
    :cond_0
    return-void
.end method

.method protected a(Z)V
    .locals 3

    .prologue
    .line 613
    invoke-virtual {p0}, Lcjy;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    .line 614
    invoke-virtual {v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->e()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    iput-object v0, p0, Lcjy;->Y:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 615
    invoke-virtual {p0}, Lcjy;->X()Ljava/util/List;

    move-result-object v0

    .line 616
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 617
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setActiveParameterKey(I)V

    .line 618
    iget-object v0, p0, Lcjy;->S:Lckp;

    if-eqz v0, :cond_0

    .line 619
    iget-object v0, p0, Lcjy;->S:Lckp;

    invoke-virtual {v0}, Lckp;->c()Z

    .line 622
    :cond_0
    return-void
.end method

.method protected a(ILjava/lang/Object;Z)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 579
    invoke-super {p0, p1, p2, p3}, Lcje;->a(ILjava/lang/Object;Z)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 602
    :goto_0
    return v0

    .line 584
    :cond_0
    invoke-virtual {p0}, Lcjy;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    .line 585
    invoke-virtual {p0}, Lcjy;->aq()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getActiveParameterKey()I

    move-result v0

    if-ne p1, v0, :cond_4

    .line 586
    invoke-direct {p0}, Lcjy;->au()Ljava/lang/String;

    move-result-object v0

    .line 587
    const/4 v3, 0x0

    invoke-virtual {p0, v3, v0}, Lcjy;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    invoke-virtual {p0}, Lcjy;->aq()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcjy;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getActiveParameterKey()I

    move-result v3

    invoke-virtual {p0, v3}, Lcjy;->f(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v3

    if-eqz v3, :cond_2

    const-string v0, "%s %+d"

    :goto_1
    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v4, v5, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v2

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 596
    :goto_2
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 597
    iget-object v1, p0, Lcjy;->Q:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 599
    const/16 v0, 0x14d

    invoke-virtual {p0, v0}, Lcjy;->h(I)V

    :cond_1
    move v0, v2

    .line 602
    goto :goto_0

    .line 591
    :cond_2
    const-string v0, "%s %d"

    goto :goto_1

    :cond_3
    const-string v0, ""

    goto :goto_2

    .line 593
    :cond_4
    invoke-virtual {p0, p1, p2}, Lcjy;->a(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method public aO_()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 153
    invoke-super {p0}, Lcje;->aO_()V

    .line 155
    invoke-virtual {p0}, Lcjy;->aa()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcjy;->X:Landroid/view/View;

    .line 156
    iget-object v0, p0, Lcjy;->X:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 157
    iget-object v0, p0, Lcjy;->O:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcjy;->X:Landroid/view/View;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 160
    invoke-virtual {p0}, Lcjy;->ag()V

    .line 162
    invoke-virtual {p0}, Lcjy;->ab()V

    .line 164
    iget-object v0, p0, Lcjy;->P:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcjy;->P:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    iget-object v1, p0, Lcjy;->X:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a(Landroid/view/View;)V

    .line 166
    iget-object v0, p0, Lcjy;->P:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->c()V

    .line 168
    iget-object v0, p0, Lcjy;->P:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {p0, v0}, Lcjy;->a(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V

    .line 171
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcjy;->k(Z)V

    .line 174
    invoke-virtual {p0}, Lcjy;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Lhdn;->a(Landroid/app/Activity;)V

    .line 176
    invoke-virtual {p0}, Lcjy;->ap()V

    .line 178
    invoke-direct {p0}, Lcjy;->av()V

    .line 179
    return-void
.end method

.method protected aa()Landroid/view/View;
    .locals 2

    .prologue
    .line 241
    new-instance v0, Lhap;

    invoke-virtual {p0}, Lcjy;->n()Lz;

    move-result-object v1

    invoke-direct {v0, v1}, Lhap;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected ab()V
    .locals 2

    .prologue
    .line 292
    invoke-virtual {p0}, Lcjy;->ad()Lhbn;

    move-result-object v0

    invoke-super {p0}, Lcje;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    invoke-interface {v0, v1}, Lhbn;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    .line 293
    return-void
.end method

.method protected ac()V
    .locals 1

    .prologue
    .line 305
    iget-object v0, p0, Lcjy;->X:Landroid/view/View;

    check-cast v0, Lhap;

    invoke-virtual {v0}, Lhap;->b()V

    .line 306
    return-void
.end method

.method protected ad()Lhbn;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcjy;->X:Landroid/view/View;

    check-cast v0, Lhap;

    return-object v0
.end method

.method protected ae()Lhbp;
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lcjy;->X:Landroid/view/View;

    check-cast v0, Lhap;

    return-object v0
.end method

.method public ae_()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 145
    invoke-super {p0}, Lcje;->ae_()V

    .line 147
    iget-object v0, p0, Lcjy;->S:Lckp;

    invoke-virtual {v0, v1}, Lckp;->a(Lckq;)V

    .line 148
    iput-object v1, p0, Lcjy;->S:Lckp;

    .line 149
    return-void
.end method

.method protected af()Lhbj;
    .locals 1

    .prologue
    .line 320
    iget-object v0, p0, Lcjy;->X:Landroid/view/View;

    check-cast v0, Lhap;

    return-object v0
.end method

.method protected ag()V
    .locals 8

    .prologue
    .line 534
    invoke-virtual {p0}, Lcjy;->ah()Lchp;

    move-result-object v0

    .line 535
    if-nez v0, :cond_0

    .line 566
    :goto_0
    return-void

    .line 539
    :cond_0
    invoke-virtual {p0}, Lcjy;->o()Landroid/content/res/Resources;

    move-result-object v1

    .line 540
    const v2, 0x7f0d0026

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 541
    const v3, 0x7f0d0023

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    .line 542
    const v4, 0x7f0d0024

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 544
    iget-object v4, p0, Lcjy;->O:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v4

    shl-int/lit8 v5, v2, 0x1

    sub-int/2addr v4, v5

    .line 545
    iget-object v5, p0, Lcjy;->O:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v5

    sub-int/2addr v5, v3

    sub-int/2addr v5, v1

    .line 547
    invoke-virtual {v0}, Lchp;->h()Lcom/google/android/libraries/photoeditor/core/FilterChain;

    move-result-object v0

    .line 548
    if-eqz v0, :cond_2

    .line 549
    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getPostRotation()I

    move-result v0

    .line 552
    :goto_1
    if-eqz v0, :cond_1

    const/4 v6, 0x2

    if-ne v0, v6, :cond_3

    .line 553
    :cond_1
    iget v0, p0, Lcjy;->Z:I

    iget v6, p0, Lcjy;->aa:I

    invoke-static {v0, v6, v4, v5}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->a(IIII)Landroid/graphics/Point;

    move-result-object v0

    .line 558
    :goto_2
    iget v6, v0, Landroid/graphics/Point;->x:I

    sub-int/2addr v4, v6

    div-int/lit8 v4, v4, 0x2

    .line 559
    iget v6, v0, Landroid/graphics/Point;->y:I

    sub-int/2addr v5, v6

    div-int/lit8 v5, v5, 0x2

    .line 560
    new-instance v6, Landroid/widget/FrameLayout$LayoutParams;

    iget v7, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v6, v7, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 561
    add-int v0, v2, v4

    iput v0, v6, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 562
    add-int v0, v3, v5

    iput v0, v6, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 563
    add-int v0, v2, v4

    iput v0, v6, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    .line 564
    add-int v0, v1, v5

    iput v0, v6, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 565
    iget-object v0, p0, Lcjy;->X:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 549
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 555
    :cond_3
    iget v0, p0, Lcjy;->aa:I

    iget v6, p0, Lcjy;->Z:I

    invoke-static {v0, v6, v4, v5}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->a(IIII)Landroid/graphics/Point;

    move-result-object v0

    goto :goto_2
.end method

.method protected ai()Lhbi;
    .locals 1

    .prologue
    .line 340
    invoke-virtual {p0}, Lcjy;->W()Lhbi;

    move-result-object v0

    return-object v0
.end method

.method protected an()Landroid/view/View;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lcjy;->X:Landroid/view/View;

    return-object v0
.end method

.method public ao()V
    .locals 1

    .prologue
    .line 362
    iget-object v0, p0, Lcjy;->S:Lckp;

    invoke-virtual {v0}, Lckp;->a()V

    iget-object v0, p0, Lcjy;->S:Lckp;

    invoke-virtual {p0, v0}, Lcjy;->a(Lckp;)V

    invoke-virtual {p0}, Lcjy;->ap()V

    .line 363
    return-void
.end method

.method protected ap()V
    .locals 2

    .prologue
    .line 372
    invoke-virtual {p0}, Lcjy;->aq()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    invoke-virtual {p0}, Lcjy;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getActiveParameterKey()I

    move-result v0

    invoke-virtual {p0, v0}, Lcjy;->f(I)Ljava/lang/String;

    move-result-object v0

    .line 375
    invoke-direct {p0}, Lcjy;->au()Ljava/lang/String;

    move-result-object v1

    .line 373
    invoke-virtual {p0, v0, v1}, Lcjy;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    :goto_0
    return-void

    .line 377
    :cond_0
    const-string v0, ""

    const-string v1, ""

    invoke-virtual {p0, v0, v1}, Lcjy;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected aq()Z
    .locals 2

    .prologue
    .line 408
    invoke-virtual {p0}, Lcjy;->X()Ljava/util/List;

    move-result-object v0

    .line 409
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 410
    invoke-virtual {p0}, Lcjy;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getActiveParameterKey()I

    move-result v0

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final ar()V
    .locals 2

    .prologue
    .line 448
    iget-object v0, p0, Lcjy;->S:Lckp;

    if-nez v0, :cond_1

    .line 459
    :cond_0
    :goto_0
    return-void

    .line 452
    :cond_1
    iget-object v0, p0, Lcjy;->S:Lckp;

    invoke-virtual {v0}, Lckp;->b()Lcom/google/android/apps/photoeditor/views/ItemSelectorView;

    move-result-object v0

    .line 453
    if-eqz v0, :cond_0

    .line 454
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected as()V
    .locals 2

    .prologue
    .line 743
    iget-object v0, p0, Lcjy;->S:Lckp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcjy;->S:Lckp;

    invoke-virtual {v0}, Lckp;->b()Lcom/google/android/apps/photoeditor/views/ItemSelectorView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcjy;->S:Lckp;

    invoke-virtual {v0}, Lckp;->b()Lcom/google/android/apps/photoeditor/views/ItemSelectorView;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->setVisibility(I)V

    .line 744
    :cond_0
    return-void
.end method

.method protected b(Z)V
    .locals 2

    .prologue
    .line 626
    invoke-virtual {p0}, Lcjy;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    iget-object v1, p0, Lcjy;->Y:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    .line 627
    iget-object v0, p0, Lcjy;->S:Lckp;

    if-eqz v0, :cond_0

    .line 628
    iget-object v0, p0, Lcjy;->S:Lckp;

    invoke-virtual {v0}, Lckp;->c()Z

    .line 630
    :cond_0
    invoke-virtual {p0}, Lcjy;->ak()V

    .line 631
    return-void
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 724
    invoke-virtual {p0}, Lcjy;->as()V

    .line 725
    return-void
.end method

.method public c(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 732
    iget-object v0, p0, Lcjy;->R:Lhcs;

    invoke-virtual {v0, p1}, Lhcs;->f(I)I

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p2, v1}, Lcjy;->a(ILjava/lang/Object;Z)Z

    .line 734
    return-void
.end method

.method protected c(Z)V
    .locals 1

    .prologue
    .line 607
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a:Lcom/google/android/libraries/photoeditor/core/NativeCore;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->setCompare(Z)V

    .line 608
    invoke-virtual {p0}, Lcjy;->ak()V

    .line 609
    return-void
.end method

.method public d(I)V
    .locals 0

    .prologue
    .line 728
    return-void
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 635
    const/4 v0, 0x1

    return v0
.end method

.method protected e()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 640
    iget-object v0, p0, Lcjy;->P:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcjy;->U:Lhcz;

    if-nez v0, :cond_1

    .line 641
    :cond_0
    const/4 v0, 0x0

    .line 711
    :goto_0
    return-object v0

    .line 644
    :cond_1
    invoke-virtual {p0}, Lcjy;->n()Lz;

    move-result-object v0

    .line 645
    if-nez v0, :cond_2

    .line 646
    const/4 v0, 0x0

    goto :goto_0

    .line 649
    :cond_2
    invoke-virtual {p0}, Lcjy;->o()Landroid/content/res/Resources;

    move-result-object v1

    .line 650
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0, v1}, Lcmh;->a(Landroid/view/Window;Landroid/content/res/Resources;)Landroid/graphics/Rect;

    move-result-object v0

    .line 651
    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 652
    const/4 v0, 0x0

    goto :goto_0

    .line 656
    :cond_3
    new-instance v2, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 657
    new-instance v3, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 660
    const v4, 0x7f0d004b

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 661
    const v5, 0x7f0d0049

    invoke-virtual {v1, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 662
    div-int/lit8 v6, v5, 0x2

    .line 665
    iget-object v7, p0, Lcjy;->P:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-static {v7}, Lcmp;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v7

    .line 666
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    .line 667
    iget-object v9, p0, Lcjy;->U:Lhcz;

    invoke-virtual {v9, v8}, Lhcz;->a(Landroid/graphics/Rect;)V

    .line 668
    iget v9, v7, Landroid/graphics/Rect;->left:I

    iget v7, v7, Landroid/graphics/Rect;->top:I

    invoke-virtual {v8, v9, v7}, Landroid/graphics/Rect;->offset(II)V

    .line 669
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v7

    if-le v7, v4, :cond_4

    .line 670
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v7

    sub-int/2addr v7, v4

    div-int/lit8 v7, v7, 0x2

    const/4 v9, 0x0

    invoke-virtual {v0, v7, v9}, Landroid/graphics/Rect;->inset(II)V

    .line 672
    :cond_4
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v7

    if-le v7, v4, :cond_5

    .line 673
    const/4 v7, 0x0

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v9

    sub-int v4, v9, v4

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {v0, v7, v4}, Landroid/graphics/Rect;->inset(II)V

    .line 676
    :cond_5
    const v4, 0x7f0c0002

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v10, v1

    .line 677
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v4, v8, Landroid/graphics/Rect;->right:I

    iget v7, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v4, v7

    div-int/lit8 v4, v4, 0x2

    .line 678
    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 680
    const/16 v4, 0x3e8

    int-to-float v7, v6

    invoke-static {v4, v7}, Lcmd;->a(IF)Lcme;

    move-result-object v4

    .line 681
    invoke-virtual {v4, v10, v11}, Lcme;->a(J)Lcme;

    move-result-object v7

    int-to-float v8, v6

    const v9, 0x3e99999a    # 0.3f

    mul-float/2addr v8, v9

    invoke-virtual {v7, v8}, Lcme;->a(F)Lcme;

    move-result-object v7

    int-to-float v8, v1

    iget v9, v0, Landroid/graphics/Rect;->bottom:I

    int-to-float v9, v9

    .line 682
    invoke-virtual {v7, v8, v9}, Lcme;->a(FF)Lcme;

    .line 683
    const-wide/16 v8, 0x96

    invoke-virtual {v4, v8, v9}, Lcme;->b(J)Lcmg;

    move-result-object v7

    int-to-float v8, v6

    invoke-virtual {v7, v8}, Lcmg;->a(F)Lcmg;

    move-result-object v7

    invoke-virtual {v7}, Lcmg;->a()Lcme;

    .line 684
    int-to-float v1, v1

    iget v7, v0, Landroid/graphics/Rect;->top:I

    int-to-float v7, v7

    const-wide/16 v8, 0x44c

    invoke-virtual {v4, v1, v7, v8, v9}, Lcme;->b(FFJ)Lcmg;

    move-result-object v1

    .line 685
    invoke-virtual {v1, v2}, Lcmg;->a(Landroid/animation/TimeInterpolator;)Lcmg;

    move-result-object v1

    invoke-virtual {v1}, Lcmg;->a()Lcme;

    .line 686
    const-wide/16 v8, 0x12c

    invoke-virtual {v4, v8, v9}, Lcme;->b(J)Lcmg;

    move-result-object v1

    int-to-float v7, v6

    const v8, 0x3f666666    # 0.9f

    mul-float/2addr v7, v8

    invoke-virtual {v1, v7}, Lcmg;->a(F)Lcmg;

    move-result-object v1

    invoke-virtual {v1}, Lcmg;->a()Lcme;

    .line 687
    const-wide/16 v8, 0x64

    invoke-virtual {v4, v8, v9}, Lcme;->b(J)Lcmg;

    move-result-object v1

    invoke-virtual {v1}, Lcmg;->a()Lcme;

    .line 688
    const-wide/16 v8, 0x32

    invoke-virtual {v4, v8, v9}, Lcme;->b(J)Lcmg;

    move-result-object v1

    int-to-float v7, v6

    const v8, 0x3f6b851f    # 0.92f

    mul-float/2addr v7, v8

    invoke-virtual {v1, v7}, Lcmg;->a(F)Lcmg;

    move-result-object v1

    invoke-virtual {v1}, Lcmg;->a()Lcme;

    .line 689
    const/4 v1, 0x0

    const/high16 v7, 0x3fc00000    # 1.5f

    int-to-float v8, v5

    mul-float/2addr v7, v8

    const-wide/16 v8, 0x30c

    invoke-virtual {v4, v1, v7, v8, v9}, Lcme;->a(FFJ)Lcmg;

    move-result-object v1

    .line 690
    invoke-virtual {v1, v2}, Lcmg;->a(Landroid/animation/TimeInterpolator;)Lcmg;

    move-result-object v1

    int-to-float v7, v6

    invoke-virtual {v1, v7}, Lcmg;->a(F)Lcmg;

    move-result-object v1

    invoke-virtual {v1}, Lcmg;->a()Lcme;

    .line 691
    const-wide/16 v8, 0x96

    invoke-virtual {v4, v8, v9}, Lcme;->b(J)Lcmg;

    move-result-object v1

    int-to-float v7, v6

    const v8, 0x3e99999a    # 0.3f

    mul-float/2addr v7, v8

    invoke-virtual {v1, v7}, Lcmg;->a(F)Lcmg;

    move-result-object v1

    invoke-virtual {v1}, Lcmg;->a()Lcme;

    .line 692
    invoke-virtual {v4}, Lcme;->a()Lcmd;

    move-result-object v1

    .line 694
    invoke-virtual {v1}, Lcmd;->c()J

    move-result-wide v8

    const-wide/16 v10, 0x514

    add-long/2addr v8, v10

    .line 695
    iget v4, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v7

    add-int/2addr v4, v7

    div-int/lit8 v4, v4, 0x2

    .line 697
    const/16 v7, 0x3e8

    int-to-float v10, v6

    invoke-static {v7, v10}, Lcmd;->a(IF)Lcme;

    move-result-object v7

    .line 698
    invoke-virtual {v7, v8, v9}, Lcme;->a(J)Lcme;

    move-result-object v8

    int-to-float v9, v6

    const v10, 0x3e99999a    # 0.3f

    mul-float/2addr v9, v10

    invoke-virtual {v8, v9}, Lcme;->a(F)Lcme;

    move-result-object v8

    iget v9, v0, Landroid/graphics/Rect;->left:I

    int-to-float v9, v9

    int-to-float v10, v4

    .line 699
    invoke-virtual {v8, v9, v10}, Lcme;->a(FF)Lcme;

    .line 700
    const-wide/16 v8, 0x96

    invoke-virtual {v7, v8, v9}, Lcme;->b(J)Lcmg;

    move-result-object v8

    int-to-float v9, v6

    invoke-virtual {v8, v9}, Lcmg;->a(F)Lcmg;

    move-result-object v8

    invoke-virtual {v8}, Lcmg;->a()Lcme;

    .line 701
    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v0, v0

    int-to-float v4, v4

    const-wide/16 v8, 0x4b0

    invoke-virtual {v7, v0, v4, v8, v9}, Lcme;->b(FFJ)Lcmg;

    move-result-object v0

    .line 702
    invoke-virtual {v0, v2}, Lcmg;->a(Landroid/animation/TimeInterpolator;)Lcmg;

    move-result-object v0

    invoke-virtual {v0}, Lcmg;->a()Lcme;

    .line 703
    const-wide/16 v8, 0x12c

    invoke-virtual {v7, v8, v9}, Lcme;->b(J)Lcmg;

    move-result-object v0

    int-to-float v2, v6

    const v4, 0x3f666666    # 0.9f

    mul-float/2addr v2, v4

    invoke-virtual {v0, v2}, Lcmg;->a(F)Lcmg;

    move-result-object v0

    invoke-virtual {v0}, Lcmg;->a()Lcme;

    .line 704
    const-wide/16 v8, 0x64

    invoke-virtual {v7, v8, v9}, Lcme;->b(J)Lcmg;

    move-result-object v0

    invoke-virtual {v0}, Lcmg;->a()Lcme;

    .line 705
    const-wide/16 v8, 0x32

    invoke-virtual {v7, v8, v9}, Lcme;->b(J)Lcmg;

    move-result-object v0

    int-to-float v2, v6

    const v4, 0x3f6b851f    # 0.92f

    mul-float/2addr v2, v4

    invoke-virtual {v0, v2}, Lcmg;->a(F)Lcmg;

    move-result-object v0

    invoke-virtual {v0}, Lcmg;->a()Lcme;

    .line 706
    const/high16 v0, -0x40400000    # -1.5f

    int-to-float v2, v5

    mul-float/2addr v0, v2

    const/4 v2, 0x0

    const-wide/16 v4, 0x2d0

    invoke-virtual {v7, v0, v2, v4, v5}, Lcme;->a(FFJ)Lcmg;

    move-result-object v0

    .line 707
    invoke-virtual {v0, v3}, Lcmg;->a(Landroid/animation/TimeInterpolator;)Lcmg;

    move-result-object v0

    int-to-float v2, v6

    invoke-virtual {v0, v2}, Lcmg;->a(F)Lcmg;

    move-result-object v0

    invoke-virtual {v0}, Lcmg;->a()Lcme;

    .line 708
    const-wide/16 v2, 0x96

    invoke-virtual {v7, v2, v3}, Lcme;->b(J)Lcmg;

    move-result-object v0

    int-to-float v2, v6

    const v3, 0x3e99999a    # 0.3f

    mul-float/2addr v2, v3

    invoke-virtual {v0, v2}, Lcmg;->a(F)Lcmg;

    move-result-object v0

    invoke-virtual {v0}, Lcmg;->a()Lcme;

    .line 709
    invoke-virtual {v7}, Lcme;->a()Lcmd;

    move-result-object v0

    .line 711
    const/4 v2, 0x2

    new-array v2, v2, [Lcmd;

    const/4 v3, 0x0

    aput-object v1, v2, v3

    const/4 v1, 0x1

    aput-object v0, v2, v1

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public e(I)V
    .locals 2

    .prologue
    .line 716
    invoke-virtual {p0}, Lcjy;->as()V

    .line 717
    invoke-virtual {p0}, Lcjy;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    iget-object v1, p0, Lcjy;->R:Lhcs;

    .line 718
    invoke-virtual {v1, p1}, Lhcs;->f(I)I

    move-result v1

    .line 717
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setActiveParameterKey(I)V

    .line 719
    invoke-virtual {p0}, Lcjy;->ap()V

    .line 720
    return-void
.end method

.method protected h(I)V
    .locals 3

    .prologue
    .line 569
    invoke-virtual {p0}, Lcje;->n()Lz;

    move-result-object v0

    check-cast v0, Lcky;

    .line 570
    iget-object v1, p0, Lcjy;->ab:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Lcky;->a(Ljava/lang/Runnable;)V

    .line 572
    iget-object v1, p0, Lcjy;->Q:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 573
    iget-object v1, p0, Lcjy;->ab:Ljava/lang/Runnable;

    invoke-interface {v0, v1, p1}, Lcky;->a(Ljava/lang/Runnable;I)V

    .line 574
    return-void
.end method

.method protected final k(Z)V
    .locals 2

    .prologue
    .line 757
    iget-object v0, p0, Lcjy;->W:Landroid/view/View;

    const v1, 0x7f1002d6

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 758
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 761
    return-void

    .line 758
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method protected y_()V
    .locals 3

    .prologue
    .line 249
    invoke-direct {p0}, Lcjy;->at()V

    .line 251
    iget-object v0, p0, Lcjy;->X:Landroid/view/View;

    check-cast v0, Lhap;

    .line 253
    new-instance v1, Lckb;

    invoke-direct {v1, p0}, Lckb;-><init>(Lcjy;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lhap;->a(Lhbl;Z)V

    .line 269
    invoke-virtual {p0}, Lcjy;->W()Lhbi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhap;->a(Lhbi;)V

    .line 270
    return-void
.end method

.method public z()V
    .locals 2

    .prologue
    .line 183
    invoke-super {p0}, Lcje;->z()V

    .line 185
    invoke-direct {p0}, Lcjy;->at()V

    .line 196
    invoke-virtual {p0}, Lcjy;->ac()V

    .line 198
    iget-object v0, p0, Lcjy;->O:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcjy;->X:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 199
    const/4 v0, 0x0

    iput-object v0, p0, Lcjy;->X:Landroid/view/View;

    .line 200
    return-void
.end method
