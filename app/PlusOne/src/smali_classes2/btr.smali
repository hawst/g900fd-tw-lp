.class public Lbtr;
.super Lbtf;
.source "PG"


# instance fields
.field private final a:Landroid/view/TextureView;

.field private final b:Landroid/view/TextureView$SurfaceTextureListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lbtr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbgf;Landroid/view/TextureView;)V
    .locals 3

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lbtf;-><init>(Lbgf;)V

    .line 26
    const-string v0, "textureView"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    iput-object v0, p0, Lbtr;->a:Landroid/view/TextureView;

    .line 27
    new-instance v0, Lbts;

    invoke-direct {v0, p0}, Lbts;-><init>(Lbtr;)V

    iput-object v0, p0, Lbtr;->b:Landroid/view/TextureView$SurfaceTextureListener;

    .line 74
    iget-object v0, p0, Lbtr;->a:Landroid/view/TextureView;

    iget-object v1, p0, Lbtr;->b:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 77
    iget-object v0, p0, Lbtr;->a:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lbtr;->a:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    iget-object v1, p0, Lbtr;->a:Landroid/view/TextureView;

    invoke-virtual {v1}, Landroid/view/TextureView;->getWidth()I

    move-result v1

    iget-object v2, p0, Lbtr;->a:Landroid/view/TextureView;

    .line 79
    invoke-virtual {v2}, Landroid/view/TextureView;->getHeight()I

    move-result v2

    .line 78
    invoke-virtual {p0, v0, v1, v2}, Lbtr;->a(Ljava/lang/Object;II)V

    .line 81
    :cond_0
    return-void
.end method

.method static synthetic a(Lbtr;)Landroid/view/TextureView;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lbtr;->a:Landroid/view/TextureView;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lbtr;->a:Landroid/view/TextureView;

    invoke-virtual {v0}, Landroid/view/TextureView;->getSurfaceTextureListener()Landroid/view/TextureView$SurfaceTextureListener;

    move-result-object v0

    iget-object v1, p0, Lbtr;->b:Landroid/view/TextureView$SurfaceTextureListener;

    if-ne v0, v1, :cond_0

    .line 86
    iget-object v0, p0, Lbtr;->a:Landroid/view/TextureView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/TextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 88
    :cond_0
    invoke-super {p0}, Lbtf;->a()V

    .line 89
    return-void
.end method
