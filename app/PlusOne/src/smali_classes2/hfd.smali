.class final Lhfd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhek;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lhfg;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;"
        }
    .end annotation
.end field

.field private e:Z

.field private synthetic f:Lhev;


# direct methods
.method constructor <init>(Lhev;I)V
    .locals 2

    .prologue
    .line 551
    const-string v0, ""

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, p1, p2, v0, v1}, Lhfd;-><init>(Lhev;ILjava/lang/String;Ljava/util/Map;)V

    .line 552
    return-void
.end method

.method private constructor <init>(Lhev;ILjava/lang/String;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lhfg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 554
    iput-object p1, p0, Lhfd;->f:Lhev;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 547
    iget-object v0, p0, Lhfd;->f:Lhev;

    invoke-static {v0}, Lhev;->b(Lhev;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lhfd;->d:Ljava/util/Map;

    .line 555
    iput p2, p0, Lhfd;->b:I

    .line 556
    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lhfd;->a:Ljava/lang/String;

    .line 557
    iput-object p4, p0, Lhfd;->c:Ljava/util/Map;

    .line 558
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 655
    invoke-direct {p0, p1}, Lhfd;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 657
    iget-object v0, p0, Lhfd;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhfg;

    .line 658
    if-eqz v0, :cond_1

    .line 659
    invoke-interface {v0, p2}, Lhfg;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    .line 665
    :cond_0
    :goto_0
    return-object p2

    .line 661
    :cond_1
    iget v0, p0, Lhfd;->b:I

    invoke-direct {p0, v0, v1}, Lhfd;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 662
    iget-object v1, p0, Lhfd;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 663
    iget-object v1, p0, Lhfd;->d:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    goto :goto_0
.end method

.method private a(ILjava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 673
    invoke-static {p1}, Lhev;->g(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lhfg;)V
    .locals 2

    .prologue
    .line 651
    iget-object v0, p0, Lhfd;->c:Ljava/util/Map;

    invoke-direct {p0, p1}, Lhfd;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 652
    return-void
.end method

.method private i(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 669
    iget-object v0, p0, Lhfd;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 627
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lhfd;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;J)J
    .locals 2

    .prologue
    .line 632
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lhfd;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 607
    invoke-direct {p0, p1, p2}, Lhfd;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method a(ILandroid/content/SharedPreferences$Editor;)V
    .locals 3

    .prologue
    .line 728
    iget-object v0, p0, Lhfd;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 729
    iget-object v1, p0, Lhfd;->c:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhfg;

    .line 730
    invoke-direct {p0, p1, v0}, Lhfd;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 731
    invoke-interface {v1, p2, v0}, Lhfg;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    goto :goto_0

    .line 733
    :cond_0
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 642
    const-string v0, "logged_in"

    invoke-virtual {p0, v0}, Lhfd;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 598
    invoke-direct {p0, p1}, Lhfd;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 599
    iget-object v0, p0, Lhfd;->c:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhfg;

    .line 600
    if-eqz v0, :cond_1

    sget-object v1, Lhev;->a:Lhfj;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    .line 602
    :goto_0
    return v0

    .line 600
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhfd;->f:Lhev;

    .line 602
    invoke-static {v0}, Lhev;->b(Lhev;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget v2, p0, Lhfd;->b:I

    invoke-direct {p0, v2, v1}, Lhfd;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 617
    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lhfd;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public b()Lhek;
    .locals 1

    .prologue
    .line 678
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhfd;->e:Z

    .line 679
    return-object p0
.end method

.method public b(Ljava/lang/String;I)Lhek;
    .locals 1

    .prologue
    .line 574
    new-instance v0, Lhfh;

    invoke-direct {v0, p2}, Lhfh;-><init>(I)V

    invoke-direct {p0, p1, v0}, Lhfd;->a(Ljava/lang/String;Lhfg;)V

    .line 575
    return-object p0
.end method

.method public b(Ljava/lang/String;J)Lhek;
    .locals 2

    .prologue
    .line 580
    new-instance v0, Lhfi;

    invoke-direct {v0, p2, p3}, Lhfi;-><init>(J)V

    invoke-direct {p0, p1, v0}, Lhfd;->a(Ljava/lang/String;Lhfg;)V

    .line 581
    return-object p0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lhek;
    .locals 1

    .prologue
    .line 562
    new-instance v0, Lhfk;

    invoke-direct {v0, p2}, Lhfk;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, p1, v0}, Lhfd;->a(Ljava/lang/String;Lhfg;)V

    .line 563
    return-object p0
.end method

.method public b(Ljava/lang/String;Z)Lhek;
    .locals 1

    .prologue
    .line 568
    new-instance v0, Lhff;

    invoke-direct {v0, p2}, Lhff;-><init>(Z)V

    invoke-direct {p0, p1, v0}, Lhfd;->a(Ljava/lang/String;Lhfg;)V

    .line 569
    return-object p0
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 612
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lhfd;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v8, -0x1

    .line 684
    iget v1, p0, Lhfd;->b:I

    .line 685
    iget-boolean v0, p0, Lhfd;->e:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lhfd;->b:I

    if-eq v0, v8, :cond_0

    move v4, v3

    .line 686
    :goto_0
    if-eqz v4, :cond_1

    .line 687
    iget-object v0, p0, Lhfd;->f:Lhev;

    invoke-static {v0}, Lhev;->c(Lhev;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lheh;

    .line 688
    iget v6, p0, Lhfd;->b:I

    invoke-interface {v0, v6}, Lheh;->a(I)V

    goto :goto_1

    :cond_0
    move v4, v2

    .line 685
    goto :goto_0

    .line 692
    :cond_1
    iget-object v5, p0, Lhfd;->f:Lhev;

    monitor-enter v5

    .line 693
    :try_start_0
    iget-object v0, p0, Lhfd;->f:Lhev;

    const-string v6, "account_name"

    invoke-virtual {p0, v6}, Lhfd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "effective_gaia_id"

    .line 694
    invoke-virtual {p0, v7}, Lhfd;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 693
    invoke-virtual {v0, v6, v7}, Lhev;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 695
    iget v6, p0, Lhfd;->b:I

    if-eq v0, v6, :cond_2

    if-ne v0, v8, :cond_3

    :cond_2
    move v0, v3

    .line 697
    :goto_2
    if-nez v0, :cond_4

    .line 698
    new-instance v0, Lhen;

    const-string v1, "Duplicate account."

    invoke-direct {v0, v1}, Lhen;-><init>(Ljava/lang/String;)V

    throw v0

    .line 715
    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    move v0, v2

    .line 695
    goto :goto_2

    .line 701
    :cond_4
    :try_start_1
    iget-object v0, p0, Lhfd;->f:Lhev;

    invoke-static {v0}, Lhev;->b(Lhev;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 702
    if-eq v1, v8, :cond_5

    if-eqz v4, :cond_8

    .line 703
    :cond_5
    iget-object v0, p0, Lhfd;->f:Lhev;

    invoke-static {v0}, Lhev;->d(Lhev;)I

    move-result v0

    .line 706
    :goto_3
    if-eqz v4, :cond_6

    .line 707
    iget-object v1, p0, Lhfd;->f:Lhev;

    iget v3, p0, Lhfd;->b:I

    invoke-static {v1, v3, v0, v2}, Lhev;->a(Lhev;IILandroid/content/SharedPreferences$Editor;)V

    .line 708
    iget-object v1, p0, Lhfd;->f:Lhev;

    iget v3, p0, Lhfd;->b:I

    invoke-static {v1, v3}, Lhev;->a(Lhev;I)V

    .line 711
    :cond_6
    invoke-virtual {p0, v0, v2}, Lhfd;->a(ILandroid/content/SharedPreferences$Editor;)V

    .line 713
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 714
    iget-object v1, p0, Lhfd;->f:Lhev;

    invoke-static {v1}, Lhev;->e(Lhev;)V

    .line 715
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 717
    iget v1, p0, Lhfd;->b:I

    if-eq v0, v1, :cond_7

    .line 718
    iget-object v1, p0, Lhfd;->f:Lhev;

    invoke-static {v1}, Lhev;->c(Lhev;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_4

    .line 722
    :cond_7
    iget-object v1, p0, Lhfd;->f:Lhev;

    invoke-static {v1}, Lhev;->a(Lhev;)V

    .line 724
    return v0

    :cond_8
    move v0, v1

    goto :goto_3
.end method

.method public synthetic c(Ljava/lang/String;I)Lhem;
    .locals 1

    .prologue
    .line 543
    invoke-virtual {p0, p1, p2}, Lhfd;->b(Ljava/lang/String;I)Lhek;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Ljava/lang/String;J)Lhem;
    .locals 2

    .prologue
    .line 543
    invoke-virtual {p0, p1, p2, p3}, Lhfd;->b(Ljava/lang/String;J)Lhek;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Ljava/lang/String;Ljava/lang/String;)Lhem;
    .locals 1

    .prologue
    .line 543
    invoke-virtual {p0, p1, p2}, Lhfd;->b(Ljava/lang/String;Ljava/lang/String;)Lhek;

    move-result-object v0

    return-object v0
.end method

.method public synthetic c(Ljava/lang/String;Z)Lhem;
    .locals 1

    .prologue
    .line 543
    invoke-virtual {p0, p1, p2}, Lhfd;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 622
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lhfd;->a(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public synthetic d(Ljava/lang/String;)Lhej;
    .locals 1

    .prologue
    .line 543
    invoke-virtual {p0, p1}, Lhfd;->f(Ljava/lang/String;)Lhek;

    move-result-object v0

    return-object v0
.end method

.method public e(Ljava/lang/String;)Lhek;
    .locals 1

    .prologue
    .line 592
    sget-object v0, Lhev;->a:Lhfj;

    invoke-direct {p0, p1, v0}, Lhfd;->a(Ljava/lang/String;Lhfg;)V

    .line 593
    return-object p0
.end method

.method public f(Ljava/lang/String;)Lhek;
    .locals 6

    .prologue
    .line 647
    new-instance v1, Lhfd;

    iget-object v2, p0, Lhfd;->f:Lhev;

    iget v3, p0, Lhfd;->b:I

    iget-object v0, p0, Lhfd;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v4, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v4, p0, Lhfd;->c:Ljava/util/Map;

    invoke-direct {v1, v2, v3, v0, v4}, Lhfd;-><init>(Lhev;ILjava/lang/String;Ljava/util/Map;)V

    return-object v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public synthetic g(Ljava/lang/String;)Lhem;
    .locals 1

    .prologue
    .line 543
    invoke-virtual {p0, p1}, Lhfd;->e(Ljava/lang/String;)Lhek;

    move-result-object v0

    return-object v0
.end method

.method public synthetic h(Ljava/lang/String;)Lhem;
    .locals 1

    .prologue
    .line 543
    invoke-virtual {p0, p1}, Lhfd;->f(Ljava/lang/String;)Lhek;

    move-result-object v0

    return-object v0
.end method
