.class final Lbqf;
.super Ljava/io/FilterInputStream;
.source "PG"


# instance fields
.field private final a:Ljava/net/HttpURLConnection;


# direct methods
.method private constructor <init>(Ljava/io/InputStream;Ljava/net/HttpURLConnection;)V
    .locals 0

    .prologue
    .line 204
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 205
    iput-object p2, p0, Lbqf;->a:Ljava/net/HttpURLConnection;

    .line 206
    return-void
.end method

.method public static a(Ljava/net/URL;)Lbqf;
    .locals 4

    .prologue
    .line 193
    invoke-virtual {p0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    new-instance v2, Lbqf;

    new-instance v3, Ljava/io/BufferedInputStream;

    invoke-direct {v3, v1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v3, v0}, Lbqf;-><init>(Ljava/io/InputStream;Ljava/net/HttpURLConnection;)V

    return-object v2
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 210
    invoke-super {p0}, Ljava/io/FilterInputStream;->close()V

    .line 211
    iget-object v0, p0, Lbqf;->a:Ljava/net/HttpURLConnection;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lbqf;->a:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 214
    :cond_0
    return-void
.end method
