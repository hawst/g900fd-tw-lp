.class public Lcdj;
.super Lccv;
.source "PG"

# interfaces
.implements Lcaj;


# instance fields
.field private final b:Lcah;

.field private final c:[Landroid/view/View;

.field private d:Lcdl;

.field private e:J

.field private f:J

.field private g:Z

.field private h:I

.field private i:I

.field private j:I

.field private k:Lcdk;


# direct methods
.method public constructor <init>(Lcci;I)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v3, 0x0

    .line 93
    invoke-direct {p0, p1}, Lccv;-><init>(Lcci;)V

    .line 71
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/view/View;

    iput-object v0, p0, Lcdj;->c:[Landroid/view/View;

    .line 76
    iput-wide v4, p0, Lcdj;->e:J

    .line 77
    iput-wide v4, p0, Lcdj;->f:J

    .line 94
    new-instance v0, Lcah;

    invoke-direct {v0, p0}, Lcah;-><init>(Lcaj;)V

    iput-object v0, p0, Lcdj;->b:Lcah;

    .line 95
    iget-object v0, p0, Lcdj;->c:[Landroid/view/View;

    const/4 v1, 0x0

    invoke-interface {p1}, Lcci;->f()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 96
    invoke-virtual {v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    aput-object v2, v0, v1

    .line 97
    iget-object v0, p0, Lcdj;->c:[Landroid/view/View;

    const/4 v1, 0x1

    invoke-interface {p1}, Lcci;->f()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 98
    invoke-virtual {v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    aput-object v2, v0, v1

    .line 99
    return-void
.end method

.method static synthetic a(II)I
    .locals 1

    .prologue
    .line 28
    if-le p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sub-int v0, p0, v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcdj;)Lcdk;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcdj;->k:Lcdk;

    return-object v0
.end method

.method static synthetic b(II)I
    .locals 1

    .prologue
    .line 28
    if-lt p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lccj;)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 214
    iget-wide v0, p0, Lcdj;->e:J

    invoke-virtual {p1, v0, v1}, Lccj;->a(J)Lccg;

    move-result-object v0

    .line 215
    if-eqz v0, :cond_0

    .line 216
    iget-object v0, v0, Lccg;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    .line 217
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 218
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 219
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 222
    :cond_0
    iget-object v0, p0, Lcdj;->b:Lcah;

    invoke-virtual {v0}, Lcah;->a()V

    .line 223
    iput-wide v4, p0, Lcdj;->e:J

    .line 224
    iput-wide v4, p0, Lcdj;->f:J

    .line 225
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcdj;->g:Z

    .line 226
    iget-object v0, p0, Lcdj;->a:Lcci;

    invoke-interface {v0}, Lcci;->d()V

    .line 227
    return-void
.end method

.method static synthetic b(Lcdj;)[Landroid/view/View;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcdj;->c:[Landroid/view/View;

    return-object v0
.end method

.method private c(Lccj;)V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v6, 0x0

    .line 235
    iget-object v0, p0, Lcdj;->d:Lcdl;

    if-nez v0, :cond_1

    .line 265
    :cond_0
    :goto_0
    return-void

    .line 239
    :cond_1
    iget-object v0, p0, Lcdj;->d:Lcdl;

    invoke-virtual {v0}, Lcdl;->b()J

    move-result-wide v8

    .line 240
    iget-boolean v0, p1, Lccj;->a:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcdj;->h:I

    :goto_1
    iget v1, p1, Lccj;->i:I

    add-int v10, v0, v1

    .line 241
    const-wide/16 v4, 0x0

    .line 242
    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v11

    move v7, v6

    :goto_2
    if-ge v7, v11, :cond_4

    .line 243
    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v7}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    .line 244
    iget-wide v2, v0, Lccg;->b:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_3

    .line 245
    iget-wide v4, v0, Lccg;->b:J

    .line 248
    iget-object v1, v0, Lccg;->a:Landroid/view/View;

    invoke-virtual {p1, v1}, Lccj;->a(Landroid/view/View;)I

    move-result v1

    iget-object v0, v0, Lccg;->a:Landroid/view/View;

    invoke-virtual {p1, v0}, Lccj;->b(Landroid/view/View;)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v1

    .line 249
    if-ge v10, v0, :cond_3

    .line 250
    iget-object v1, p0, Lcdj;->d:Lcdl;

    iget-wide v2, p0, Lcdj;->e:J

    invoke-virtual/range {v1 .. v6}, Lcdl;->a(JJZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 251
    iput-wide v4, p0, Lcdj;->f:J

    .line 252
    iput-boolean v6, p0, Lcdj;->g:Z

    .line 253
    iget-object v0, p0, Lcdj;->d:Lcdl;

    invoke-virtual {v0, v4, v5, v6}, Lcdl;->a(JZ)V

    goto :goto_0

    .line 240
    :cond_2
    iget v0, p0, Lcdj;->i:I

    goto :goto_1

    .line 242
    :cond_3
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_2

    .line 260
    :cond_4
    iget-object v1, p0, Lcdj;->d:Lcdl;

    iget-wide v2, p0, Lcdj;->e:J

    move v6, v12

    invoke-virtual/range {v1 .. v6}, Lcdl;->a(JJZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    iput-wide v4, p0, Lcdj;->f:J

    .line 262
    iput-boolean v12, p0, Lcdj;->g:Z

    .line 263
    iget-object v0, p0, Lcdj;->d:Lcdl;

    invoke-virtual {v0, v4, v5, v12}, Lcdl;->a(JZ)V

    goto :goto_0
.end method


# virtual methods
.method public a(I)V
    .locals 1

    .prologue
    .line 206
    iget v0, p0, Lcdj;->j:I

    add-int/2addr v0, p1

    iput v0, p0, Lcdj;->j:I

    .line 207
    iget-object v0, p0, Lcdj;->a:Lcci;

    invoke-interface {v0}, Lcci;->d()V

    .line 208
    return-void
.end method

.method public a(Lccj;)V
    .locals 4

    .prologue
    .line 138
    iget-object v0, p0, Lcdj;->k:Lcdk;

    const-string v1, "mListener"

    const-string v2, "setListener() was not called"

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 140
    iget-wide v0, p0, Lcdj;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 157
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v0, p0, Lcdj;->d:Lcdl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcdj;->d:Lcdl;

    invoke-virtual {v0}, Lcdl;->a()Landroid/widget/Adapter;

    move-result-object v0

    iget-object v1, p1, Lccj;->h:Landroid/widget/Adapter;

    if-eq v0, v1, :cond_2

    .line 146
    :cond_1
    iget-object v0, p1, Lccj;->h:Landroid/widget/Adapter;

    check-cast v0, Lcau;

    .line 147
    new-instance v1, Lcdl;

    invoke-direct {v1, p0, v0}, Lcdl;-><init>(Lcdj;Lcau;)V

    iput-object v1, p0, Lcdj;->d:Lcdl;

    .line 149
    :cond_2
    iget-object v0, p0, Lcdj;->d:Lcdl;

    iput-object v0, p1, Lccj;->h:Landroid/widget/Adapter;

    .line 152
    invoke-direct {p0, p1}, Lcdj;->c(Lccj;)V

    .line 155
    iget v0, p1, Lccj;->i:I

    iget v1, p0, Lcdj;->j:I

    add-int/2addr v0, v1

    iput v0, p1, Lccj;->i:I

    .line 156
    const/4 v0, 0x0

    iput v0, p0, Lcdj;->j:I

    goto :goto_0
.end method

.method public a(Lcdk;)V
    .locals 2

    .prologue
    .line 104
    const-string v0, "listener"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdk;

    iput-object v0, p0, Lcdj;->k:Lcdk;

    .line 105
    return-void
.end method

.method public a(Lccj;I)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 111
    iget-object v0, p0, Lcdj;->k:Lcdk;

    const-string v1, "mListener"

    const-string v2, "setListener() was not called"

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 112
    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    iget-object v1, v0, Lccg;->a:Landroid/view/View;

    .line 113
    iget-object v0, p1, Lccj;->h:Landroid/widget/Adapter;

    invoke-interface {v0, p2}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcdj;->e:J

    .line 114
    iget-wide v2, p0, Lcdj;->e:J

    iput-wide v2, p0, Lcdj;->f:J

    .line 115
    iput-boolean v4, p0, Lcdj;->g:Z

    .line 116
    iput v4, p0, Lcdj;->j:I

    .line 118
    iget-object v0, p0, Lcdj;->k:Lcdk;

    invoke-interface {v0, v1}, Lcdk;->a(Landroid/view/View;)Landroid/view/View$DragShadowBuilder;

    move-result-object v0

    .line 119
    if-nez v0, :cond_0

    .line 120
    new-instance v0, Landroid/view/View$DragShadowBuilder;

    invoke-direct {v0, v1}, Landroid/view/View$DragShadowBuilder;-><init>(Landroid/view/View;)V

    .line 123
    :cond_0
    iget-object v2, p0, Lcdj;->a:Lcci;

    const/4 v3, 0x0

    invoke-interface {v2, v0, v3}, Lcci;->a(Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;)V

    .line 126
    invoke-virtual {v1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    .line 127
    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f000000    # 0.5f

    .line 128
    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    .line 129
    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    .line 131
    iget-object v0, p0, Lcdj;->a:Lcci;

    invoke-interface {v0}, Lcci;->d()V

    .line 133
    const/4 v0, 0x1

    return v0
.end method

.method public a(Lccj;Landroid/view/DragEvent;)Z
    .locals 7

    .prologue
    .line 161
    iget-object v0, p0, Lcdj;->k:Lcdk;

    const-string v1, "mListener"

    const-string v2, "setListener() was not called"

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 163
    iget-wide v0, p0, Lcdj;->e:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 164
    const/4 v0, 0x0

    .line 199
    :goto_0
    return v0

    .line 168
    :cond_0
    invoke-virtual {p2}, Landroid/view/DragEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    .line 169
    invoke-virtual {p2}, Landroid/view/DragEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    .line 170
    iput v0, p0, Lcdj;->h:I

    .line 171
    iput v1, p0, Lcdj;->i:I

    .line 173
    invoke-direct {p0, p1}, Lcdj;->c(Lccj;)V

    .line 175
    invoke-virtual {p2}, Landroid/view/DragEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 199
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 177
    :pswitch_0
    iget-object v2, p1, Lccj;->d:Landroid/graphics/Rect;

    .line 178
    iget-boolean v3, p1, Lccj;->a:Z

    if-eqz v3, :cond_1

    iget v1, v2, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget v2, v2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    int-to-float v0, v0

    .line 179
    invoke-static {v1, v2, v0}, Lcfn;->b(FFF)F

    move-result v0

    .line 181
    :goto_2
    iget-object v1, p0, Lcdj;->b:Lcah;

    invoke-virtual {v1, v0}, Lcah;->a(F)V

    goto :goto_1

    .line 179
    :cond_1
    iget v0, v2, Landroid/graphics/Rect;->top:I

    int-to-float v0, v0

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    int-to-float v1, v1

    .line 180
    invoke-static {v0, v2, v1}, Lcfn;->b(FFF)F

    move-result v0

    goto :goto_2

    .line 186
    :pswitch_1
    iget-object v0, p0, Lcdj;->d:Lcdl;

    if-eqz v0, :cond_2

    .line 187
    iget-object v1, p0, Lcdj;->d:Lcdl;

    iget-wide v2, p0, Lcdj;->e:J

    iget-wide v4, p0, Lcdj;->f:J

    iget-boolean v6, p0, Lcdj;->g:Z

    invoke-virtual/range {v1 .. v6}, Lcdl;->b(JJZ)V

    .line 190
    :cond_2
    invoke-direct {p0, p1}, Lcdj;->b(Lccj;)V

    goto :goto_1

    .line 195
    :pswitch_2
    invoke-direct {p0, p1}, Lcdj;->b(Lccj;)V

    goto :goto_1

    .line 175
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
