.class public final Lhct;
.super Lhcz;
.source "PG"


# instance fields
.field private c:I

.field private d:Lhcv;

.field private e:Lhcw;

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lhcx;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lhcx;

.field private i:Lhcq;

.field private j:Lhcn;

.field private final k:F

.field private l:I

.field private m:Z

.field private n:Z

.field private o:F

.field private p:Z

.field private q:Z

.field private final r:Landroid/graphics/PointF;

.field private final s:Landroid/graphics/PointF;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V
    .locals 2

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lhcz;-><init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V

    .line 52
    const/4 v0, 0x1

    iput v0, p0, Lhct;->c:I

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lhct;->g:Ljava/util/List;

    .line 66
    const/4 v0, -0x1

    iput v0, p0, Lhct;->l:I

    .line 252
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lhct;->r:Landroid/graphics/PointF;

    .line 253
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lhct;->s:Landroid/graphics/PointF;

    .line 88
    invoke-virtual {p1}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 90
    new-instance v1, Lhcq;

    invoke-direct {v1, v0}, Lhcq;-><init>(Landroid/content/res/Resources;)V

    iput-object v1, p0, Lhct;->i:Lhcq;

    .line 91
    new-instance v1, Lhcn;

    invoke-direct {v1, v0}, Lhcn;-><init>(Landroid/content/res/Resources;)V

    iput-object v1, p0, Lhct;->j:Lhcn;

    .line 92
    new-instance v1, Lhch;

    invoke-direct {v1, v0}, Lhch;-><init>(Landroid/content/res/Resources;)V

    .line 94
    iget-object v0, p0, Lhdx;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhea;->a(Landroid/content/Context;)Lhea;

    move-result-object v0

    invoke-virtual {v0}, Lhea;->b()F

    move-result v0

    iput v0, p0, Lhct;->o:F

    .line 95
    iget v0, p0, Lhct;->o:F

    iget v1, p0, Lhct;->o:F

    mul-float/2addr v0, v1

    iput v0, p0, Lhct;->o:F

    .line 97
    iget-object v0, p0, Lhct;->j:Lhcn;

    .line 98
    invoke-virtual {v0}, Lhcn;->a()I

    move-result v0

    iget-object v1, p0, Lhct;->j:Lhcn;

    invoke-virtual {v1}, Lhcn;->b()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    iput v0, p0, Lhct;->k:F

    .line 99
    return-void
.end method

.method private static a(FFF)F
    .locals 4

    .prologue
    .line 632
    float-to-double v0, p0

    float-to-double v2, p1

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    float-to-double v2, p2

    mul-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method private a(Landroid/graphics/RectF;FF)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 410
    iget-object v1, p0, Lhct;->e:Lhcw;

    if-nez v1, :cond_1

    .line 437
    :cond_0
    :goto_0
    return v0

    .line 414
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    div-float v1, p2, v1

    .line 415
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v2

    div-float v2, p3, v2

    .line 417
    invoke-static {v1}, Lhct;->b(F)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v2}, Lhct;->b(F)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 421
    iget-object v0, p0, Lhct;->e:Lhcw;

    invoke-interface {v0, v1, v2}, Lhcw;->a(FF)Ljava/lang/Object;

    move-result-object v0

    .line 423
    iget-object v1, p0, Lhct;->h:Lhcx;

    .line 425
    new-instance v2, Lhcx;

    invoke-direct {v2, v0}, Lhcx;-><init>(Ljava/lang/Object;)V

    iput-object v2, p0, Lhct;->h:Lhcx;

    .line 426
    iget-object v0, p0, Lhct;->h:Lhcx;

    iget-object v0, v0, Lhcx;->b:Landroid/graphics/PointF;

    iput p2, v0, Landroid/graphics/PointF;->x:F

    .line 427
    iget-object v0, p0, Lhct;->h:Lhcx;

    iget-object v0, v0, Lhcx;->b:Landroid/graphics/PointF;

    iput p3, v0, Landroid/graphics/PointF;->y:F

    .line 428
    iget-object v0, p0, Lhct;->g:Ljava/util/List;

    iget-object v2, p0, Lhct;->h:Lhcx;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 430
    iget-object v0, p0, Lhct;->e:Lhcw;

    .line 431
    invoke-static {v1}, Lhct;->b(Lhcx;)Ljava/lang/Object;

    move-result-object v1

    iget-object v2, p0, Lhct;->h:Lhcx;

    invoke-static {v2}, Lhct;->b(Lhcx;)Ljava/lang/Object;

    move-result-object v2

    .line 430
    invoke-interface {v0, v1, v2}, Lhcw;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 433
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lhct;->a(I)Z

    .line 435
    iget-object v0, p0, Lhct;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    .line 437
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Lhcx;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 441
    iget-object v0, p0, Lhct;->h:Lhcx;

    if-ne v0, p1, :cond_0

    .line 442
    const/4 v1, 0x0

    .line 455
    :goto_0
    return v1

    .line 445
    :cond_0
    iget-object v0, p0, Lhct;->e:Lhcw;

    if-eqz v0, :cond_1

    .line 446
    iget-object v0, p0, Lhct;->e:Lhcw;

    iget-object v2, p0, Lhct;->h:Lhcx;

    invoke-static {v2}, Lhct;->b(Lhcx;)Ljava/lang/Object;

    move-result-object v2

    invoke-static {p1}, Lhct;->b(Lhcx;)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lhcw;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 449
    :cond_1
    iput-object p1, p0, Lhct;->h:Lhcx;

    .line 451
    iget-object v0, p0, Lhct;->h:Lhcx;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    invoke-virtual {p0, v0}, Lhct;->a(I)Z

    .line 453
    iget-object v0, p0, Lhct;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    goto :goto_0

    .line 451
    :cond_2
    const/4 v0, 0x2

    goto :goto_1
.end method

.method private static b(Lhcx;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 636
    if-eqz p0, :cond_0

    iget-object v0, p0, Lhcx;->a:Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(F)Z
    .locals 1

    .prologue
    .line 640
    const/4 v0, 0x0

    cmpl-float v0, p0, v0

    if-ltz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p0, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g(FF)Z
    .locals 6

    .prologue
    .line 237
    iget-object v0, p0, Lhct;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b()Landroid/graphics/RectF;

    move-result-object v0

    .line 239
    iget v1, v0, Landroid/graphics/RectF;->left:F

    sub-float v1, p1, v1

    .line 240
    iget v2, v0, Landroid/graphics/RectF;->top:F

    sub-float v2, p2, v2

    .line 242
    invoke-direct {p0, v1, v2}, Lhct;->h(FF)Lhcx;

    move-result-object v3

    .line 244
    iget v4, p0, Lhct;->c:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    if-nez v3, :cond_0

    .line 245
    invoke-direct {p0, v0, v1, v2}, Lhct;->a(Landroid/graphics/RectF;FF)Z

    move-result v0

    .line 247
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, v3}, Lhct;->a(Lhcx;)Z

    move-result v0

    goto :goto_0
.end method

.method private h(FF)Lhcx;
    .locals 7

    .prologue
    .line 604
    const/4 v3, 0x0

    .line 606
    const v2, 0x7f7fffff    # Float.MAX_VALUE

    .line 607
    iget-object v0, p0, Lhct;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhcx;

    .line 608
    iget-object v1, v0, Lhcx;->b:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v5, v0, Lhcx;->b:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    invoke-static {p1, p2, v1, v5}, Lhdo;->b(FFFF)F

    move-result v1

    .line 609
    iget v5, p0, Lhct;->o:F

    cmpg-float v5, v1, v5

    if-gez v5, :cond_1

    cmpg-float v5, v1, v2

    if-gez v5, :cond_1

    move v6, v1

    move-object v1, v0

    move v0, v6

    :goto_1
    move v2, v0

    move-object v3, v1

    .line 613
    goto :goto_0

    .line 615
    :cond_0
    return-object v3

    :cond_1
    move v0, v2

    move-object v1, v3

    goto :goto_1
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Lhck;
    .locals 1

    .prologue
    .line 213
    new-instance v0, Lhco;

    invoke-direct {v0, p1}, Lhco;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhct;->p:Z

    .line 117
    iget-object v0, p0, Lhct;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    .line 118
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 460
    iget-object v0, p0, Lhct;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b()Landroid/graphics/RectF;

    move-result-object v2

    .line 461
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 494
    :cond_0
    :goto_0
    return-void

    .line 465
    :cond_1
    iget-boolean v0, p0, Lhct;->p:Z

    if-eqz v0, :cond_4

    .line 466
    iget-object v0, p0, Lhct;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iput-object v11, p0, Lhct;->h:Lhcx;

    iget-object v0, p0, Lhct;->d:Lhcv;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhct;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b()Landroid/graphics/RectF;

    move-result-object v4

    move v0, v1

    :goto_1
    iget-object v5, p0, Lhct;->d:Lhcv;

    invoke-interface {v5}, Lhcv;->at()I

    move-result v5

    if-ge v0, v5, :cond_3

    iget-object v5, p0, Lhct;->d:Lhcv;

    invoke-interface {v5, v0}, Lhcv;->j(I)Ljava/lang/Object;

    move-result-object v5

    iget-object v6, p0, Lhct;->d:Lhcv;

    invoke-interface {v6, v5}, Lhcv;->b(Ljava/lang/Object;)Landroid/graphics/PointF;

    move-result-object v6

    new-instance v7, Lhcx;

    invoke-direct {v7, v5}, Lhcx;-><init>(Ljava/lang/Object;)V

    iget-object v8, v7, Lhcx;->b:Landroid/graphics/PointF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v9

    iget v10, v6, Landroid/graphics/PointF;->x:F

    mul-float/2addr v9, v10

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v10

    iget v6, v6, Landroid/graphics/PointF;->y:F

    mul-float/2addr v6, v10

    invoke-virtual {v8, v9, v6}, Landroid/graphics/PointF;->set(FF)V

    iget-object v6, p0, Lhct;->g:Ljava/util/List;

    invoke-interface {v6, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v6, p0, Lhct;->d:Lhcv;

    invoke-interface {v6, v5}, Lhcv;->d(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    iput-object v7, p0, Lhct;->h:Lhcx;

    iget-object v5, p0, Lhct;->e:Lhcw;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lhct;->e:Lhcw;

    iget-object v6, p0, Lhct;->h:Lhcx;

    invoke-static {v6}, Lhct;->b(Lhcx;)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v5, v11, v6}, Lhcw;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 467
    :cond_3
    iput-boolean v1, p0, Lhct;->p:Z

    .line 470
    :cond_4
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->save(I)I

    .line 472
    iget v0, v2, Landroid/graphics/RectF;->left:F

    iget v1, v2, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 474
    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v5, v0

    .line 475
    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v6, v0

    .line 477
    invoke-virtual {p0, p1, v5, v6}, Lhct;->a(Landroid/graphics/Canvas;II)V

    .line 483
    iget-object v0, p0, Lhct;->h:Lhcx;

    if-eqz v0, :cond_5

    .line 484
    invoke-virtual {p0}, Lhct;->k()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 485
    iget-object v0, p0, Lhcz;->a:Lhck;

    iget-object v1, p0, Lhcz;->b:Lhcm;

    iget-object v2, p0, Lhct;->h:Lhcx;

    iget-object v2, v2, Lhcx;->b:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    float-to-int v2, v2

    iget-object v3, p0, Lhct;->h:Lhcx;

    iget-object v3, v3, Lhcx;->b:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    float-to-int v3, v3

    invoke-virtual {v0, p1, v1, v2, v3}, Lhck;->a(Landroid/graphics/Canvas;Lhcm;II)V

    .line 493
    :cond_5
    :goto_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0

    .line 487
    :cond_6
    iget-boolean v0, p0, Lhct;->q:Z

    if-nez v0, :cond_5

    .line 489
    iget-object v2, p0, Lhct;->h:Lhcx;

    iget-boolean v4, p0, Lhct;->m:Z

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v6}, Lhct;->a(Landroid/graphics/Canvas;Lhcx;ZZII)V

    goto :goto_2
.end method

.method protected a(Landroid/graphics/Canvas;II)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 525
    iget-object v0, p0, Lhct;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhcx;

    .line 527
    iget-object v0, p0, Lhct;->h:Lhcx;

    if-eq v2, v0, :cond_0

    move-object v0, p0

    move-object v1, p1

    move v4, v3

    move v5, p2

    move v6, p3

    .line 528
    invoke-virtual/range {v0 .. v6}, Lhct;->a(Landroid/graphics/Canvas;Lhcx;ZZII)V

    goto :goto_0

    .line 531
    :cond_1
    return-void
.end method

.method protected a(Landroid/graphics/Canvas;Lhcx;ZZII)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 535
    iget-object v0, p0, Lhct;->d:Lhcv;

    iget-object v1, p2, Lhcx;->a:Ljava/lang/Object;

    invoke-interface {v0}, Lhcv;->av()F

    move-result v5

    .line 536
    iget-object v0, p0, Lhct;->d:Lhcv;

    iget-object v1, p2, Lhcx;->a:Ljava/lang/Object;

    invoke-interface {v0}, Lhcv;->au()Ljava/lang/CharSequence;

    move-result-object v4

    .line 538
    if-eqz p3, :cond_1

    .line 539
    iget-object v0, p0, Lhct;->j:Lhcn;

    iget-object v1, p2, Lhcx;->b:Landroid/graphics/PointF;

    iget v2, v1, Landroid/graphics/PointF;->x:F

    iget-object v1, p2, Lhcx;->b:Landroid/graphics/PointF;

    iget v3, v1, Landroid/graphics/PointF;->y:F

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lhcn;->b(Landroid/graphics/Canvas;FFLjava/lang/CharSequence;F)V

    .line 553
    :goto_0
    if-eqz p3, :cond_0

    if-eqz p4, :cond_0

    iget-object v0, p0, Lhct;->d:Lhcv;

    if-eqz v0, :cond_0

    .line 554
    iget-object v0, p0, Lhct;->d:Lhcv;

    iget-object v1, p2, Lhcx;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lhcv;->c(Ljava/lang/Object;)F

    move-result v0

    .line 555
    int-to-float v1, p5

    int-to-float v2, p6

    invoke-static {v1, v2, v0}, Lhct;->a(FFF)F

    move-result v0

    .line 557
    iget v1, p0, Lhct;->k:F

    cmpl-float v1, v0, v1

    if-ltz v1, :cond_0

    .line 558
    const/4 v1, 0x2

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->save(I)I

    .line 560
    invoke-virtual {p1, v6, v6, p5, p6}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    .line 561
    iget-object v1, p0, Lhct;->i:Lhcq;

    iget-object v2, p2, Lhcx;->b:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p2, Lhcx;->b:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v0, v4

    invoke-virtual {v1, p1, v2, v3, v0}, Lhcq;->a(Landroid/graphics/Canvas;FFF)V

    .line 564
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 567
    :cond_0
    return-void

    .line 549
    :cond_1
    iget-object v0, p0, Lhct;->j:Lhcn;

    iget-object v1, p2, Lhcx;->b:Landroid/graphics/PointF;

    iget v2, v1, Landroid/graphics/PointF;->x:F

    iget-object v1, p2, Lhcx;->b:Landroid/graphics/PointF;

    iget v3, v1, Landroid/graphics/PointF;->y:F

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lhcn;->a(Landroid/graphics/Canvas;FFLjava/lang/CharSequence;F)V

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Rect;Landroid/graphics/Point;)V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lhct;->h:Lhcx;

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lhct;->h:Lhcx;

    iget-object v0, v0, Lhcx;->b:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    iget-object v1, p0, Lhct;->h:Lhcx;

    iget-object v1, v1, Lhcx;->b:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 220
    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Point;->offset(II)V

    .line 224
    :goto_0
    return-void

    .line 222
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Point;->set(II)V

    goto :goto_0
.end method

.method public a(Lhcv;)V
    .locals 0

    .prologue
    .line 102
    iput-object p1, p0, Lhct;->d:Lhcv;

    .line 104
    invoke-virtual {p0}, Lhct;->a()V

    .line 105
    return-void
.end method

.method public a(Lhcw;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lhct;->e:Lhcw;

    .line 109
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lhct;->m:Z

    if-ne v0, p1, :cond_0

    .line 131
    :goto_0
    return-void

    .line 129
    :cond_0
    iput-boolean p1, p0, Lhct;->m:Z

    .line 130
    iget-object v0, p0, Lhct;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    goto :goto_0
.end method

.method public a(FF)Z
    .locals 1

    .prologue
    .line 228
    invoke-direct {p0, p1, p2}, Lhct;->g(FF)Z

    move-result v0

    return v0
.end method

.method public a(I)Z
    .locals 2

    .prologue
    .line 178
    iget v0, p0, Lhct;->c:I

    if-ne v0, p1, :cond_0

    .line 179
    const/4 v0, 0x0

    .line 187
    :goto_0
    return v0

    .line 182
    :cond_0
    iput p1, p0, Lhct;->c:I

    .line 183
    iget-object v0, p0, Lhct;->e:Lhcw;

    if-eqz v0, :cond_1

    .line 184
    iget-object v0, p0, Lhct;->e:Lhcw;

    iget v1, p0, Lhct;->c:I

    invoke-interface {v0, v1}, Lhcw;->i(I)V

    .line 187
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b()Z
    .locals 4

    .prologue
    const/4 v0, 0x3

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 134
    invoke-virtual {p0}, Lhct;->d()Z

    move-result v3

    if-eqz v3, :cond_2

    iget v3, p0, Lhct;->c:I

    if-eq v3, v0, :cond_0

    :goto_0
    invoke-virtual {p0, v0}, Lhct;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    :goto_2
    return v1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method public b(FF)Z
    .locals 1

    .prologue
    .line 233
    invoke-direct {p0, p1, p2}, Lhct;->g(FF)Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 141
    iget-object v0, p0, Lhct;->h:Lhcx;

    if-nez v0, :cond_0

    .line 155
    :goto_0
    return-void

    .line 145
    :cond_0
    iget-object v0, p0, Lhct;->g:Ljava/util/List;

    iget-object v1, p0, Lhct;->h:Lhcx;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 147
    iget-object v0, p0, Lhct;->e:Lhcw;

    if-eqz v0, :cond_1

    .line 148
    iget-object v0, p0, Lhct;->e:Lhcw;

    iget-object v1, p0, Lhct;->h:Lhcx;

    iget-object v1, v1, Lhcx;->a:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lhcw;->a(Ljava/lang/Object;)V

    .line 149
    iget-object v0, p0, Lhct;->e:Lhcw;

    iget-object v1, p0, Lhct;->h:Lhcx;

    iget-object v1, v1, Lhcx;->a:Ljava/lang/Object;

    invoke-interface {v0, v1, v2}, Lhcw;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 152
    :cond_1
    iput-object v2, p0, Lhct;->h:Lhcx;

    .line 154
    iget-object v0, p0, Lhct;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    goto :goto_0
.end method

.method public c(FF)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 257
    invoke-super {p0, p1, p2}, Lhcz;->c(FF)Z

    .line 259
    iget-object v0, p0, Lhct;->e:Lhcw;

    if-nez v0, :cond_0

    .line 260
    const/4 v0, 0x0

    .line 301
    :goto_0
    return v0

    .line 263
    :cond_0
    iget-object v0, p0, Lhct;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b()Landroid/graphics/RectF;

    move-result-object v0

    .line 265
    iget v1, v0, Landroid/graphics/RectF;->left:F

    sub-float v1, p1, v1

    .line 266
    iget v2, v0, Landroid/graphics/RectF;->top:F

    sub-float v2, p2, v2

    .line 268
    invoke-direct {p0, v1, v2}, Lhct;->h(FF)Lhcx;

    move-result-object v3

    .line 270
    sget-object v4, Lhcu;->a:[I

    iget v5, p0, Lhct;->c:I

    add-int/lit8 v5, v5, -0x1

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 290
    :cond_1
    :goto_1
    iget-boolean v0, p0, Lhct;->q:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lhct;->h:Lhcx;

    if-eqz v0, :cond_3

    .line 291
    iget-object v0, p0, Lhct;->s:Landroid/graphics/PointF;

    iget-object v1, p0, Lhct;->h:Lhcx;

    iget-object v1, v1, Lhcx;->b:Landroid/graphics/PointF;

    invoke-virtual {v0, v1}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    .line 292
    iget-object v0, p0, Lhct;->r:Landroid/graphics/PointF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 294
    iget-object v0, p0, Lhct;->e:Lhcw;

    if-eqz v0, :cond_2

    .line 295
    iget-object v0, p0, Lhct;->e:Lhcw;

    iget-object v0, p0, Lhct;->h:Lhcx;

    .line 298
    :cond_2
    iget-object v0, p0, Lhct;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    .line 301
    :cond_3
    iget-boolean v0, p0, Lhct;->q:Z

    goto :goto_0

    .line 272
    :pswitch_0
    if-nez v3, :cond_1

    .line 273
    invoke-direct {p0, v0, v1, v2}, Lhct;->a(Landroid/graphics/RectF;FF)Z

    .line 274
    iput-boolean v6, p0, Lhct;->q:Z

    goto :goto_1

    .line 280
    :pswitch_1
    if-eqz v3, :cond_1

    .line 281
    invoke-direct {p0, v3}, Lhct;->a(Lhcx;)Z

    .line 282
    iput-boolean v6, p0, Lhct;->q:Z

    goto :goto_1

    .line 270
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 158
    iget-object v0, p0, Lhct;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(FF)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 306
    iget-boolean v0, p0, Lhct;->q:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhct;->h:Lhcx;

    if-nez v0, :cond_1

    .line 307
    :cond_0
    invoke-super {p0, p1, p2}, Lhcz;->d(FF)Z

    move-result v0

    .line 327
    :goto_0
    return v0

    .line 310
    :cond_1
    iget-object v0, p0, Lhct;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b()Landroid/graphics/RectF;

    move-result-object v0

    .line 312
    iget-object v1, p0, Lhct;->h:Lhcx;

    iget-object v1, v1, Lhcx;->b:Landroid/graphics/PointF;

    iget-object v2, p0, Lhct;->s:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    add-float/2addr v2, p1

    iget-object v3, p0, Lhct;->r:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v3

    .line 313
    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v3

    .line 312
    invoke-static {v2, v4, v3}, Lhcg;->a(FFF)F

    move-result v2

    iput v2, v1, Landroid/graphics/PointF;->x:F

    .line 314
    iget-object v1, p0, Lhct;->h:Lhcx;

    iget-object v1, v1, Lhcx;->b:Landroid/graphics/PointF;

    iget-object v2, p0, Lhct;->s:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    add-float/2addr v2, p2

    iget-object v3, p0, Lhct;->r:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    sub-float/2addr v2, v3

    .line 315
    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v3

    .line 314
    invoke-static {v2, v4, v3}, Lhcg;->a(FFF)F

    move-result v2

    iput v2, v1, Landroid/graphics/PointF;->y:F

    .line 317
    iget-object v1, p0, Lhct;->e:Lhcw;

    if-eqz v1, :cond_2

    .line 318
    iget-object v1, p0, Lhct;->h:Lhcx;

    iget-object v1, v1, Lhcx;->b:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v1, v2

    .line 319
    iget-object v2, p0, Lhct;->h:Lhcx;

    iget-object v2, v2, Lhcx;->b:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    div-float v0, v2, v0

    .line 320
    iget-object v2, p0, Lhct;->e:Lhcw;

    iget-object v3, p0, Lhct;->h:Lhcx;

    iget-object v3, v3, Lhcx;->a:Ljava/lang/Object;

    invoke-interface {v2, v3, v1, v0}, Lhcw;->a(Ljava/lang/Object;FF)V

    .line 323
    :cond_2
    invoke-virtual {p0}, Lhct;->i()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lhct;->j()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 324
    :cond_3
    iget-object v0, p0, Lhct;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    .line 327
    :cond_4
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lhct;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    .line 164
    return-void
.end method

.method public e(FF)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 349
    iget v1, p0, Lhct;->l:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lhct;->h:Lhcx;

    if-nez v1, :cond_1

    .line 350
    :cond_0
    const/4 v0, 0x0

    .line 370
    :goto_0
    return v0

    .line 353
    :cond_1
    iput-boolean v0, p0, Lhct;->n:Z

    .line 359
    iget-object v1, p0, Lhct;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v1}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b()Landroid/graphics/RectF;

    move-result-object v1

    .line 360
    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/RectF;->height()F

    move-result v1

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v2, v1, v3}, Lhct;->a(FFF)F

    .line 365
    iget-object v1, p0, Lhct;->d:Lhcv;

    iget-object v2, p0, Lhct;->h:Lhcx;

    iget-object v2, v2, Lhcx;->a:Ljava/lang/Object;

    invoke-interface {v1, v2}, Lhcv;->c(Ljava/lang/Object;)F

    .line 366
    iget-object v1, p0, Lhct;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v1}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 174
    iget v0, p0, Lhct;->c:I

    return v0
.end method

.method public f(FF)Z
    .locals 1

    .prologue
    .line 375
    iget-boolean v0, p0, Lhct;->n:Z

    if-nez v0, :cond_0

    .line 376
    const/4 v0, 0x0

    .line 388
    :goto_0
    return v0

    .line 379
    :cond_0
    iget-object v0, p0, Lhct;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    .line 388
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 332
    iget-boolean v0, p0, Lhct;->q:Z

    if-nez v0, :cond_0

    .line 333
    invoke-super {p0}, Lhcz;->g()Z

    move-result v0

    .line 344
    :goto_0
    return v0

    .line 336
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhct;->q:Z

    .line 338
    iget-object v0, p0, Lhct;->e:Lhcw;

    if-eqz v0, :cond_1

    .line 339
    iget-object v0, p0, Lhct;->e:Lhcw;

    iget-object v0, p0, Lhct;->h:Lhcx;

    .line 342
    :cond_1
    iget-object v0, p0, Lhct;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    .line 344
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public h()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 393
    iget-boolean v1, p0, Lhct;->n:Z

    if-nez v1, :cond_0

    .line 401
    :goto_0
    return v0

    .line 397
    :cond_0
    iput-boolean v0, p0, Lhct;->n:Z

    .line 399
    iget-object v0, p0, Lhct;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    .line 401
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected i()Z
    .locals 1

    .prologue
    .line 504
    const/4 v0, 0x0

    return v0
.end method

.method protected j()Z
    .locals 1

    .prologue
    .line 521
    const/4 v0, 0x0

    return v0
.end method
