.class final Lcdi;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/animation/PropertyValuesHolder;

.field public final b:Landroid/animation/PropertyValuesHolder;

.field public final c:Landroid/animation/PropertyValuesHolder;

.field public final d:Landroid/animation/PropertyValuesHolder;


# direct methods
.method public constructor <init>(Lccj;Landroid/view/View;Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    sget-object v0, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    new-array v1, v2, [F

    fill-array-data v1, :array_0

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    iput-object v0, p0, Lcdi;->a:Landroid/animation/PropertyValuesHolder;

    .line 181
    sget-object v0, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    new-array v1, v2, [F

    fill-array-data v1, :array_1

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    iput-object v0, p0, Lcdi;->b:Landroid/animation/PropertyValuesHolder;

    .line 182
    sget-object v0, Landroid/view/View;->SCALE_X:Landroid/util/Property;

    new-array v1, v2, [F

    fill-array-data v1, :array_2

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    iput-object v0, p0, Lcdi;->c:Landroid/animation/PropertyValuesHolder;

    .line 183
    sget-object v0, Landroid/view/View;->SCALE_Y:Landroid/util/Property;

    new-array v1, v2, [F

    fill-array-data v1, :array_3

    invoke-static {v0, v1}, Landroid/animation/PropertyValuesHolder;->ofFloat(Landroid/util/Property;[F)Landroid/animation/PropertyValuesHolder;

    move-result-object v0

    iput-object v0, p0, Lcdi;->d:Landroid/animation/PropertyValuesHolder;

    .line 184
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Lcdi;->a(Lccj;Landroid/view/View;Landroid/graphics/Rect;Z)V

    .line 185
    return-void

    .line 180
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 181
    :array_1
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 182
    :array_2
    .array-data 4
        0x0
        0x0
    .end array-data

    .line 183
    :array_3
    .array-data 4
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method a(Lccj;Landroid/view/View;Landroid/graphics/Rect;Z)V
    .locals 8

    .prologue
    .line 198
    invoke-virtual {p3}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    int-to-float v1, v1

    div-float v1, v0, v1

    .line 199
    invoke-virtual {p3}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v2, v0, v2

    .line 201
    invoke-virtual {p3}, Landroid/graphics/Rect;->centerX()I

    move-result v3

    iget-boolean v0, p1, Lccj;->a:Z

    if-eqz v0, :cond_1

    iget v0, p1, Lccj;->i:I

    :goto_0
    add-int/2addr v0, v3

    .line 202
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v3

    sub-int/2addr v0, v3

    .line 203
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int v3, v0, v3

    .line 205
    invoke-virtual {p3}, Landroid/graphics/Rect;->centerY()I

    move-result v4

    iget-boolean v0, p1, Lccj;->a:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_1
    add-int/2addr v0, v4

    .line 206
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v4

    sub-int/2addr v0, v4

    .line 207
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    sub-int/2addr v0, v4

    .line 209
    iget-object v4, p0, Lcdi;->a:Landroid/animation/PropertyValuesHolder;

    const/4 v5, 0x2

    new-array v5, v5, [F

    const/4 v6, 0x0

    int-to-float v7, v3

    aput v7, v5, v6

    const/4 v6, 0x1

    const/4 v7, 0x0

    aput v7, v5, v6

    invoke-virtual {v4, v5}, Landroid/animation/PropertyValuesHolder;->setFloatValues([F)V

    .line 210
    iget-object v4, p0, Lcdi;->b:Landroid/animation/PropertyValuesHolder;

    const/4 v5, 0x2

    new-array v5, v5, [F

    const/4 v6, 0x0

    int-to-float v7, v0

    aput v7, v5, v6

    const/4 v6, 0x1

    const/4 v7, 0x0

    aput v7, v5, v6

    invoke-virtual {v4, v5}, Landroid/animation/PropertyValuesHolder;->setFloatValues([F)V

    .line 211
    iget-object v4, p0, Lcdi;->c:Landroid/animation/PropertyValuesHolder;

    const/4 v5, 0x2

    new-array v5, v5, [F

    const/4 v6, 0x0

    aput v1, v5, v6

    const/4 v6, 0x1

    const/high16 v7, 0x3f800000    # 1.0f

    aput v7, v5, v6

    invoke-virtual {v4, v5}, Landroid/animation/PropertyValuesHolder;->setFloatValues([F)V

    .line 212
    iget-object v4, p0, Lcdi;->d:Landroid/animation/PropertyValuesHolder;

    const/4 v5, 0x2

    new-array v5, v5, [F

    const/4 v6, 0x0

    aput v2, v5, v6

    const/4 v6, 0x1

    const/high16 v7, 0x3f800000    # 1.0f

    aput v7, v5, v6

    invoke-virtual {v4, v5}, Landroid/animation/PropertyValuesHolder;->setFloatValues([F)V

    .line 214
    if-eqz p4, :cond_0

    .line 215
    int-to-float v3, v3

    invoke-virtual {p2, v3}, Landroid/view/View;->setTranslationX(F)V

    .line 216
    int-to-float v0, v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTranslationY(F)V

    .line 217
    invoke-virtual {p2, v1}, Landroid/view/View;->setScaleX(F)V

    .line 218
    invoke-virtual {p2, v2}, Landroid/view/View;->setScaleY(F)V

    .line 220
    :cond_0
    return-void

    .line 201
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 205
    :cond_2
    iget v0, p1, Lccj;->i:I

    goto :goto_1
.end method
