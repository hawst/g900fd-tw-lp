.class public final Lahq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lbku;

.field private final b:[J

.field private final c:Lbmz;


# direct methods
.method public constructor <init>(Lbku;[JLbmz;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lahq;->a:Lbku;

    .line 18
    iput-object p2, p0, Lahq;->b:[J

    .line 19
    iput-object p3, p0, Lahq;->c:Lbmz;

    .line 20
    return-void
.end method


# virtual methods
.method public a()[F
    .locals 24

    .prologue
    .line 26
    const-wide v10, 0x47efffffe0000000L    # 3.4028234663852886E38

    const-wide/16 v8, 0x0

    const-wide/16 v18, 0x0

    const-wide/16 v16, 0x0

    const-wide/16 v14, 0x0

    .line 27
    const/4 v3, 0x0

    .line 28
    const-wide/16 v6, 0x0

    .line 29
    const-wide/16 v4, 0x0

    .line 30
    move-object/from16 v0, p0

    iget-object v2, v0, Lahq;->b:[J

    array-length v2, v2

    new-array v0, v2, [F

    move-object/from16 v20, v0

    .line 31
    const/4 v2, 0x0

    move-wide v12, v10

    move-wide v10, v8

    move v8, v3

    move v3, v2

    :goto_0
    move-object/from16 v0, v20

    array-length v2, v0

    if-ge v3, v2, :cond_3

    .line 32
    move-object/from16 v0, p0

    iget-object v2, v0, Lahq;->a:Lbku;

    move-object/from16 v0, p0

    iget-object v9, v0, Lahq;->c:Lbmz;

    move-object/from16 v0, p0

    iget-object v0, v0, Lahq;->b:[J

    move-object/from16 v21, v0

    aget-wide v22, v21, v3

    move-wide/from16 v0, v22

    invoke-virtual {v2, v9, v0, v1}, Lbku;->a(Lbmz;J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    .line 33
    if-eqz v2, :cond_2

    .line 34
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v9

    aput v9, v20, v8

    .line 35
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v9

    float-to-double v0, v9

    move-wide/from16 v22, v0

    add-double v6, v6, v22

    .line 36
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v9

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v21

    mul-float v9, v9, v21

    float-to-double v0, v9

    move-wide/from16 v22, v0

    add-double v4, v4, v22

    .line 37
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v9

    float-to-double v0, v9

    move-wide/from16 v22, v0

    cmpl-double v9, v22, v10

    if-lez v9, :cond_0

    .line 38
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v9

    float-to-double v10, v9

    .line 40
    :cond_0
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v9

    float-to-double v0, v9

    move-wide/from16 v22, v0

    cmpg-double v9, v22, v12

    if-gez v9, :cond_1

    .line 41
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    float-to-double v12, v2

    .line 43
    :cond_1
    add-int/lit8 v8, v8, 0x1

    .line 31
    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 46
    :cond_3
    if-lez v8, :cond_5

    .line 47
    const/4 v2, 0x0

    div-int/lit8 v3, v8, 0x2

    move-object/from16 v0, v20

    invoke-static {v0, v2, v3, v8}, Lcom/google/android/apps/moviemaker/aggregator/Algorithms;->a([FIII)F

    move-result v2

    float-to-double v0, v2

    move-wide/from16 v16, v0

    .line 48
    int-to-double v2, v8

    div-double/2addr v6, v2

    .line 49
    int-to-double v2, v8

    mul-double/2addr v2, v6

    mul-double/2addr v2, v6

    sub-double v2, v4, v2

    .line 52
    const/4 v4, 0x1

    if-le v8, v4, :cond_4

    const-wide/16 v4, 0x0

    cmpl-double v4, v2, v4

    if-lez v4, :cond_4

    .line 53
    add-int/lit8 v4, v8, -0x1

    int-to-double v4, v4

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    move-wide/from16 v4, v16

    .line 56
    :goto_1
    const/4 v9, 0x6

    new-array v9, v9, [F

    const/4 v14, 0x0

    double-to-float v6, v6

    aput v6, v9, v14

    const/4 v6, 0x1

    double-to-float v2, v2

    aput v2, v9, v6

    const/4 v2, 0x2

    double-to-float v3, v4

    aput v3, v9, v2

    const/4 v2, 0x3

    double-to-float v3, v12

    aput v3, v9, v2

    const/4 v2, 0x4

    double-to-float v3, v10

    aput v3, v9, v2

    const/4 v2, 0x5

    int-to-float v3, v8

    aput v3, v9, v2

    return-object v9

    :cond_4
    move-wide v2, v14

    move-wide/from16 v4, v16

    goto :goto_1

    :cond_5
    move-wide v2, v14

    move-wide/from16 v4, v16

    move-wide/from16 v6, v18

    goto :goto_1
.end method
