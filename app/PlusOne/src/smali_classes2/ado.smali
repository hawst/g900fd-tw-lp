.class public abstract Lado;
.super Ladl;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field public final a:Ljava/lang/Object;

.field public volatile b:Z


# direct methods
.method protected constructor <init>(ILandroid/media/MediaFormat;Ladm;)V
    .locals 2

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3}, Ladl;-><init>(ILandroid/media/MediaFormat;Ladm;)V

    .line 18
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lado;->a:Ljava/lang/Object;

    .line 23
    invoke-static {p2}, Ladg;->b(Landroid/media/MediaFormat;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 24
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "VideoTrackDecoder can only be used with supported video formats"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 27
    :cond_0
    return-void
.end method

.method protected static a(I)Z
    .locals 2

    .prologue
    .line 74
    sparse-switch p0, :sswitch_data_0

    .line 82
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsupported rotation angle."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 77
    :sswitch_0
    const/4 v0, 0x1

    .line 80
    :goto_0
    return v0

    :sswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 74
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x5a -> :sswitch_0
        0xb4 -> :sswitch_1
        0x10e -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method protected abstract a(Laas;Labg;I)V
.end method

.method public a()Z
    .locals 2

    .prologue
    .line 50
    iget-object v1, p0, Lado;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 52
    :goto_0
    :try_start_0
    iget-boolean v0, p0, Lado;->b:Z

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lado;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 57
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    :try_start_1
    monitor-exit v1

    :goto_1
    return v0

    .line 55
    :cond_0
    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_1

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public bridge synthetic a(Landroid/media/MediaExtractor;)Z
    .locals 1

    .prologue
    .line 13
    invoke-super {p0, p1}, Ladl;->a(Landroid/media/MediaExtractor;)Z

    move-result v0

    return v0
.end method

.method public b(Laas;Labg;I)V
    .locals 2

    .prologue
    .line 30
    iget-object v1, p0, Lado;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 31
    :try_start_0
    iget-boolean v0, p0, Lado;->b:Z

    if-nez v0, :cond_0

    .line 32
    monitor-exit v1

    .line 41
    :goto_0
    return-void

    .line 37
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lado;->a(Laas;Labg;I)V

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lado;->b:Z

    .line 40
    iget-object v0, p0, Lado;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 41
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public bridge synthetic b()Z
    .locals 1

    .prologue
    .line 13
    invoke-super {p0}, Ladl;->b()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic c()V
    .locals 0

    .prologue
    .line 13
    invoke-super {p0}, Ladl;->c()V

    return-void
.end method

.method public bridge synthetic d()V
    .locals 0

    .prologue
    .line 13
    invoke-super {p0}, Ladl;->d()V

    return-void
.end method

.method public bridge synthetic e()V
    .locals 0

    .prologue
    .line 13
    invoke-super {p0}, Ladl;->e()V

    return-void
.end method

.method protected final h()V
    .locals 2

    .prologue
    .line 63
    iget-object v1, p0, Lado;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 64
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lado;->b:Z

    .line 65
    iget-object v0, p0, Lado;->a:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 66
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
