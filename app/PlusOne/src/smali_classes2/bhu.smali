.class public final Lbhu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbhr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lbhj;",
        ">",
        "Ljava/lang/Object;",
        "Lbhr",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Lbhr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbhr",
            "<TT;>;"
        }
    .end annotation
.end field

.field private final b:J


# direct methods
.method public constructor <init>(Lbhr;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbhr",
            "<TT;>;J)V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-string v0, "source"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    iput-object v0, p0, Lbhu;->a:Lbhr;

    .line 21
    iput-wide p2, p0, Lbhu;->b:J

    .line 22
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lbhu;->a:Lbhr;

    invoke-interface {v0}, Lbhr;->a()V

    .line 43
    return-void
.end method

.method public a(Lbhj;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 37
    iget-object v0, p0, Lbhu;->a:Lbhr;

    invoke-interface {v0, p1}, Lbhr;->a(Ljava/lang/Object;)V

    .line 38
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 9
    check-cast p1, Lbhj;

    invoke-virtual {p0, p1}, Lbhu;->a(Lbhj;)V

    return-void
.end method

.method public b()Lbhj;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 26
    iget-object v0, p0, Lbhu;->a:Lbhr;

    invoke-interface {v0}, Lbhr;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhj;

    .line 27
    if-nez v0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 32
    :goto_0
    return-object v0

    .line 31
    :cond_0
    iget-wide v2, v0, Lbhj;->a:J

    iget-wide v4, p0, Lbhu;->b:J

    add-long/2addr v2, v4

    iput-wide v2, v0, Lbhj;->a:J

    goto :goto_0
.end method

.method public synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 9
    invoke-virtual {p0}, Lbhu;->b()Lbhj;

    move-result-object v0

    return-object v0
.end method
