.class public final Lcmh;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/view/Window;Landroid/content/res/Resources;)Landroid/graphics/Rect;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 37
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/view/Window;->peekDecorView()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 39
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 58
    :goto_0
    return-object v0

    .line 43
    :cond_1
    const v0, 0x7f0d004e

    .line 44
    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 45
    const v1, 0x7f0d004f

    .line 46
    invoke-virtual {p1, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 47
    const v2, 0x7f0d0049

    .line 48
    invoke-virtual {p1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 49
    div-int/lit8 v2, v2, 0x2

    .line 50
    add-int v3, v2, v0

    .line 51
    add-int/2addr v1, v2

    .line 54
    invoke-virtual {p0}, Landroid/view/Window;->peekDecorView()Landroid/view/View;

    move-result-object v2

    .line 55
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v4

    invoke-virtual {v2}, Landroid/view/View;->getHeight()I

    move-result v2

    invoke-direct {v0, v5, v5, v4, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 56
    invoke-virtual {v0, v3, v1}, Landroid/graphics/Rect;->inset(II)V

    goto :goto_0
.end method
