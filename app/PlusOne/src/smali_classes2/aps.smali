.class final Laps;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lchb;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lchb",
        "<",
        "Landroid/content/Context;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    const/4 v9, 0x5

    .line 25
    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    .line 27
    new-array v4, v9, [Ljava/lang/String;

    const-string v2, "AamEventsLog"

    aput-object v2, v4, v0

    const-string v2, "ClusteringLog"

    aput-object v2, v4, v1

    const/4 v2, 0x2

    const-string v5, "PluggedInLog"

    aput-object v5, v4, v2

    const/4 v2, 0x3

    const-string v5, "PostSyncLog"

    aput-object v5, v4, v2

    const/4 v2, 0x4

    const-string v5, "PostCaptureLog"

    aput-object v5, v4, v2

    move v2, v0

    .line 30
    :goto_0
    if-ge v2, v9, :cond_1

    aget-object v5, v4, v2

    move v0, v1

    .line 31
    :goto_1
    if-gt v0, v9, :cond_0

    .line 32
    new-instance v6, Ljava/io/File;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0xc

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v3, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 33
    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    .line 31
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 35
    :cond_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v3, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 36
    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    .line 30
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 38
    :cond_1
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 22
    check-cast p1, Landroid/content/Context;

    invoke-virtual {p0, p1}, Laps;->a(Landroid/content/Context;)V

    return-void
.end method
