.class public final Lceo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcfp;


# instance fields
.field public a:Ljava/util/Iterator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Iterator",
            "<",
            "Ljej;",
            ">;"
        }
    .end annotation
.end field

.field private final b:[Lcfp;

.field private c:I


# direct methods
.method public varargs constructor <init>([Lcfp;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lceo;->b:[Lcfp;

    .line 30
    return-void
.end method

.method static synthetic a(Lceo;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lceo;->c:I

    return v0
.end method

.method static synthetic b(Lceo;)[Lcfp;
    .locals 1

    .prologue
    .line 13
    iget-object v0, p0, Lceo;->b:[Lcfp;

    return-object v0
.end method

.method static synthetic c(Lceo;)I
    .locals 1

    .prologue
    .line 13
    iget v0, p0, Lceo;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lceo;->c:I

    return v0
.end method


# virtual methods
.method public close()V
    .locals 2

    .prologue
    .line 86
    iget v0, p0, Lceo;->c:I

    iget-object v1, p0, Lceo;->b:[Lcfp;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 87
    iget-object v0, p0, Lceo;->b:[Lcfp;

    iget v1, p0, Lceo;->c:I

    aget-object v0, v0, v1

    invoke-interface {v0}, Lcfp;->close()V

    .line 88
    const/4 v0, 0x0

    iput-object v0, p0, Lceo;->a:Ljava/util/Iterator;

    .line 89
    iget-object v0, p0, Lceo;->b:[Lcfp;

    array-length v0, v0

    iput v0, p0, Lceo;->c:I

    .line 91
    :cond_0
    return-void
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<",
            "Ljej;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    new-instance v0, Lcep;

    invoke-direct {v0, p0}, Lcep;-><init>(Lceo;)V

    return-object v0
.end method
