.class public final Lbpb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbph;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lbpb;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I

.field private final b:Lbmd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92
    new-instance v0, Lbpc;

    invoke-direct {v0}, Lbpc;-><init>()V

    sput-object v0, Lbpb;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILbmd;)V
    .locals 4

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput p1, p0, Lbpb;->a:I

    .line 30
    invoke-virtual {p2}, Lbmd;->b()J

    move-result-wide v0

    const-wide/32 v2, 0x16e360

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "duration >= min length"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 32
    const-string v0, "clipAfterResizing"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iput-object v0, p0, Lbpb;->b:Lbmd;

    .line 33
    return-void

    .line 30
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lbpb;->a:I

    .line 37
    new-instance v0, Lbmd;

    invoke-direct {v0, p1}, Lbmd;-><init>(Landroid/os/Parcel;)V

    iput-object v0, p0, Lbpb;->b:Lbmd;

    .line 38
    return-void
.end method


# virtual methods
.method public a()Lbut;
    .locals 3

    .prologue
    .line 57
    new-instance v0, Lbut;

    invoke-direct {v0}, Lbut;-><init>()V

    .line 58
    const/4 v1, 0x2

    iput v1, v0, Lbut;->b:I

    .line 59
    new-instance v1, Lbuq;

    invoke-direct {v1}, Lbuq;-><init>()V

    .line 60
    iget v2, p0, Lbpb;->a:I

    iput v2, v1, Lbuq;->a:I

    .line 61
    iget-object v2, p0, Lbpb;->b:Lbmd;

    invoke-static {v2}, Lbpm;->a(Lbmd;)Lbty;

    move-result-object v2

    iput-object v2, v1, Lbuq;->b:Lbty;

    .line 62
    iput-object v1, v0, Lbut;->d:Lbuq;

    .line 63
    return-object v0
.end method

.method public a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    iget v0, p0, Lbpb;->a:I

    const-string v1, "mIndex"

    invoke-static {v0, v1, p1}, Lcgp;->a(ILjava/lang/CharSequence;Ljava/util/Collection;)I

    .line 43
    iget v0, p0, Lbpb;->a:I

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 50
    iget v1, p0, Lbpb;->a:I

    iget-object v2, p0, Lbpb;->b:Lbmd;

    invoke-virtual {v2}, Lbmd;->a()Lbmf;

    move-result-object v2

    iget v0, v0, Lbmd;->b:I

    .line 51
    invoke-virtual {v2, v0}, Lbmf;->b(I)Lbmf;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Lbmf;->a()Lbmd;

    move-result-object v0

    .line 50
    invoke-interface {p1, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 53
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 89
    const-class v0, Lbpb;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lbpb;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lbpb;->b:Lbmd;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lbpb;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 74
    iget-object v0, p0, Lbpb;->b:Lbmd;

    invoke-virtual {v0, p1, p2}, Lbmd;->writeToParcel(Landroid/os/Parcel;I)V

    .line 75
    return-void
.end method
