.class public final Lgqs;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lgqt;",
            ">;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/16 v0, 0x64

    invoke-direct {p0, v0}, Lgqs;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgqs;->a:Ljava/util/ArrayList;

    iput p1, p0, Lgqs;->b:I

    return-void
.end method

.method private f()V
    .locals 2

    :goto_0
    invoke-virtual {p0}, Lgqs;->c()I

    move-result v0

    invoke-virtual {p0}, Lgqs;->d()I

    move-result v1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lgqs;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lgqt;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lgqs;->a:Ljava/util/ArrayList;

    return-object v0
.end method

.method public a(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V
    .locals 2

    iget-object v0, p0, Lgqs;->a:Ljava/util/ArrayList;

    new-instance v1, Lgqt;

    invoke-direct {v1, p1, p2}, Lgqt;-><init>(Lcom/google/android/gms/internal/qu;Lcom/google/android/gms/internal/qq;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lgqs;->f()V

    return-void
.end method

.method public a(Lcom/google/android/gms/internal/qu;Lpyo;)V
    .locals 2

    iget-object v0, p0, Lgqs;->a:Ljava/util/ArrayList;

    new-instance v1, Lgqt;

    invoke-direct {v1, p1, p2}, Lgqt;-><init>(Lcom/google/android/gms/internal/qu;Lpyo;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Lgqs;->f()V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lgqs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lgqs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lgqs;->b:I

    return v0
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Lgqs;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    return v0
.end method
