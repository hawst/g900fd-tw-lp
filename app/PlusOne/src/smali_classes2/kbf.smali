.class public final Lkbf;
.super Ljxw;
.source "PG"


# static fields
.field private static d:Z

.field private static e:I

.field private static f:Ljava/text/NumberFormat;


# instance fields
.field private g:Landroid/widget/TextView;

.field private h:Landroid/view/View$OnClickListener;

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Ljxw;-><init>(Landroid/content/Context;)V

    .line 37
    return-void
.end method

.method private c()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 130
    invoke-virtual {p0}, Lkbf;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 131
    iget-object v0, p0, Lkbf;->b:Lkzw;

    invoke-virtual {v0}, Lkzw;->h()J

    move-result-wide v0

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    iget-boolean v0, p0, Lkbf;->i:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    .line 133
    :goto_0
    iget-object v4, p0, Lkbf;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    const v1, 0x7f0b00d6

    .line 134
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 133
    :goto_1
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 135
    iget-object v4, p0, Lkbf;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_3

    const v1, 0x7f0204a3

    .line 136
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 135
    :goto_2
    invoke-virtual {v4, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 138
    iget-object v1, p0, Lkbf;->g:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lkbf;->h:Landroid/view/View$OnClickListener;

    :cond_0
    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 140
    iget-object v1, p0, Lkbf;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setClickable(Z)V

    .line 141
    return-void

    .line 131
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 134
    :cond_2
    const v1, 0x7f0b013d

    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_1

    :cond_3
    move-object v1, v2

    .line 136
    goto :goto_2
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0}, Lkbf;->removeAllViews()V

    .line 108
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkbf;->i:Z

    .line 109
    return-void
.end method

.method protected a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 66
    sget-boolean v0, Lkbf;->d:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00a7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lkbf;->e:I

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getIntegerInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    sput-object v0, Lkbf;->f:Ljava/text/NumberFormat;

    const/4 v0, 0x1

    sput-boolean v0, Lkbf;->d:Z

    .line 67
    :cond_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/16 v2, 0xa

    invoke-static {p1, v0, v1, v2}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lkbf;->g:Landroid/widget/TextView;

    .line 69
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lkbf;->h:Landroid/view/View$OnClickListener;

    .line 113
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 144
    iput-boolean p1, p0, Lkbf;->i:Z

    .line 145
    return-void
.end method

.method public b()V
    .locals 9

    .prologue
    const/4 v6, 0x1

    .line 73
    invoke-virtual {p0}, Lkbf;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lkbf;->b:Lkzw;

    invoke-virtual {v1}, Lkzw;->h()J

    move-result-wide v2

    .line 75
    invoke-direct {p0}, Lkbf;->c()V

    .line 76
    iget-object v1, p0, Lkbf;->g:Landroid/widget/TextView;

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setFocusable(Z)V

    .line 77
    iget-object v1, p0, Lkbf;->g:Landroid/widget/TextView;

    invoke-static {v1, v6}, Liu;->c(Landroid/view/View;I)V

    .line 79
    iget-object v1, p0, Lkbf;->g:Landroid/widget/TextView;

    const-wide/16 v4, 0x0

    cmp-long v4, v2, v4

    if-lez v4, :cond_0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v4, 0x7f110003

    long-to-int v5, v2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    sget-object v8, Lkbf;->f:Ljava/text/NumberFormat;

    .line 80
    invoke-virtual {v8, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v7

    .line 79
    invoke-virtual {v0, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 82
    iget-object v0, p0, Lkbf;->g:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lkbf;->addView(Landroid/view/View;)V

    .line 83
    return-void

    .line 81
    :cond_0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0a0181

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Landroid/content/Context;)V
    .locals 10

    .prologue
    const/4 v9, -0x2

    .line 116
    iget-object v0, p0, Lkbf;->b:Lkzw;

    invoke-virtual {v0}, Lkzw;->h()J

    move-result-wide v0

    .line 117
    invoke-direct {p0}, Lkbf;->c()V

    .line 118
    iget-object v2, p0, Lkbf;->g:Landroid/widget/TextView;

    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-lez v3, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f110003

    long-to-int v5, v0

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    sget-object v8, Lkbf;->f:Ljava/text/NumberFormat;

    .line 119
    invoke-virtual {v8, v0, v1}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v7

    .line 118
    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 121
    iget-object v0, p0, Lkbf;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v9, v9}, Landroid/widget/TextView;->measure(II)V

    .line 122
    iget-object v0, p0, Lkbf;->g:Landroid/widget/TextView;

    iget-object v1, p0, Lkbf;->g:Landroid/widget/TextView;

    .line 123
    invoke-virtual {v1}, Landroid/widget/TextView;->getLeft()I

    move-result v1

    iget-object v2, p0, Lkbf;->g:Landroid/widget/TextView;

    .line 124
    invoke-virtual {v2}, Landroid/widget/TextView;->getTop()I

    move-result v2

    iget-object v3, p0, Lkbf;->g:Landroid/widget/TextView;

    .line 125
    invoke-virtual {v3}, Landroid/widget/TextView;->getLeft()I

    move-result v3

    iget-object v4, p0, Lkbf;->g:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lkbf;->g:Landroid/widget/TextView;

    .line 126
    invoke-virtual {v4}, Landroid/widget/TextView;->getTop()I

    move-result v4

    iget-object v5, p0, Lkbf;->g:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    .line 122
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 127
    return-void

    .line 120
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a0181

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 95
    sget v0, Lkbf;->e:I

    .line 96
    sget-object v1, Lkbf;->a:Llct;

    iget v1, v1, Llct;->m:I

    .line 98
    iget-object v2, p0, Lkbf;->g:Landroid/widget/TextView;

    iget-object v3, p0, Lkbf;->g:Landroid/widget/TextView;

    .line 101
    invoke-virtual {v3}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v3

    add-int/2addr v3, v1

    iget-object v4, p0, Lkbf;->g:Landroid/widget/TextView;

    .line 102
    invoke-virtual {v4}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v0

    .line 98
    invoke-virtual {v2, v1, v0, v3, v4}, Landroid/widget/TextView;->layout(IIII)V

    .line 103
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    const/4 v1, -0x2

    .line 88
    iget-object v0, p0, Lkbf;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1}, Landroid/widget/TextView;->measure(II)V

    .line 89
    iget v0, p0, Lkbf;->c:I

    iget-object v1, p0, Lkbf;->g:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    sget v2, Lkbf;->e:I

    add-int/2addr v1, v2

    invoke-virtual {p0, v0, v1}, Lkbf;->setMeasuredDimension(II)V

    .line 91
    return-void
.end method
