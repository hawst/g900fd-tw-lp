.class public final Lbzb;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:[Lbza;


# instance fields
.field private final b:Lama;

.field private final c:Lbzd;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 36
    const/16 v0, 0xa

    new-array v0, v0, [Lbza;

    const/4 v1, 0x0

    sget-object v2, Lbza;->b:Lbza;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lbza;->c:Lbza;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lbza;->d:Lbza;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lbza;->e:Lbza;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lbza;->f:Lbza;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lbza;->g:Lbza;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lbza;->h:Lbza;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lbza;->i:Lbza;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lbza;->j:Lbza;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lbza;->k:Lbza;

    aput-object v2, v0, v1

    sput-object v0, Lbzb;->a:[Lbza;

    return-void
.end method

.method public constructor <init>(Landroid/content/res/Resources;Lama;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    new-instance v0, Lbzd;

    .line 30
    invoke-virtual {p1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-direct {v0, v1, p1}, Lbzd;-><init>(Ljava/util/Locale;Landroid/content/res/Resources;)V

    iput-object v0, p0, Lbzb;->c:Lbzd;

    .line 31
    const-string v0, "clipIdentifierGenerator"

    .line 32
    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lama;

    iput-object v0, p0, Lbzb;->b:Lama;

    .line 33
    return-void
.end method


# virtual methods
.method public a(Lbza;)Lbzp;
    .locals 2

    .prologue
    .line 97
    iget-object v0, p1, Lbza;->s:Lbzq;

    iget-object v1, p0, Lbzb;->b:Lama;

    invoke-interface {v0, v1, p1}, Lbzq;->a(Lama;Lbza;)Lbzp;

    move-result-object v0

    return-object v0
.end method

.method public a()Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbza;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 84
    invoke-static {}, Lbza;->values()[Lbza;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 85
    iget-boolean v5, v4, Lbza;->v:Z

    if-nez v5, :cond_0

    sget-object v5, Lcgd;->a:Lcgd;

    invoke-virtual {v5}, Lcgd;->b()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 86
    :cond_0
    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 84
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 89
    :cond_2
    iget-object v0, p0, Lbzb;->c:Lbzd;

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 90
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
