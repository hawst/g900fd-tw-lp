.class public final Lbkq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lbmr;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lbkq;->a:Landroid/util/SparseArray;

    .line 92
    return-void
.end method

.method public constructor <init>(Lbkp;)V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    iget-object v0, p1, Lbkp;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clone()Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p0, Lbkq;->a:Landroid/util/SparseArray;

    .line 96
    return-void
.end method


# virtual methods
.method public a(ILjava/util/List;)Lbkq;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lbmr;",
            ">;)",
            "Lbkq;"
        }
    .end annotation

    .prologue
    .line 107
    iget-object v0, p0, Lbkq;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 108
    return-object p0
.end method

.method public a()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbmp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 136
    iget-object v0, p0, Lbkq;->a:Landroid/util/SparseArray;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 138
    if-eqz v0, :cond_0

    invoke-static {v0}, Lbkp;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_0
.end method

.method public b()Lbkp;
    .locals 2

    .prologue
    .line 148
    new-instance v0, Lbkp;

    iget-object v1, p0, Lbkq;->a:Landroid/util/SparseArray;

    invoke-direct {v0, v1}, Lbkp;-><init>(Landroid/util/SparseArray;)V

    return-object v0
.end method

.method public b(ILjava/util/List;)Lbkq;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lbmr;",
            ">;)",
            "Lbkq;"
        }
    .end annotation

    .prologue
    .line 119
    iget-object v0, p0, Lbkq;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 120
    invoke-virtual {p0, p1, p2}, Lbkq;->a(ILjava/util/List;)Lbkq;

    .line 125
    :goto_0
    return-object p0

    .line 122
    :cond_0
    iget-object v0, p0, Lbkq;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 123
    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
