.class public final Lcij;
.super Lcjy;
.source "PG"

# interfaces
.implements Lhdg;


# static fields
.field private static final W:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final X:[I


# instance fields
.field private Y:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

.field private Z:Lhcr;

.field private aa:I

.field private ab:Lckj;

.field private final ac:Lcmv;

.field private final ad:Lcin;

.field private ae:I

.field private final af:Landroid/graphics/PointF;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x4

    .line 39
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcij;->X:[I

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 62
    new-array v1, v4, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/16 v3, 0x13

    .line 64
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const/16 v3, 0x17

    .line 65
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const/16 v3, 0x16

    .line 66
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    .line 67
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 62
    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 69
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcij;->W:Ljava/util/List;

    .line 70
    return-void

    .line 39
    :array_0
    .array-data 4
        0x7f0a0063
        0x7f0a0064
        0x7f0a0065
        0x7f0a0066
        0x7f0a0067
        0x7f0a0068
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Lcjy;-><init>()V

    .line 52
    new-instance v0, Lcio;

    invoke-direct {v0, p0}, Lcio;-><init>(Lcij;)V

    iput-object v0, p0, Lcij;->ac:Lcmv;

    .line 54
    new-instance v0, Lcin;

    invoke-direct {v0, p0}, Lcin;-><init>(Lcij;)V

    iput-object v0, p0, Lcij;->ad:Lcin;

    .line 58
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcij;->af:Landroid/graphics/PointF;

    .line 317
    return-void
.end method

.method static synthetic a(Lcij;)Lcin;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcij;->ad:Lcin;

    return-object v0
.end method

.method private at()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 282
    invoke-virtual {p0}, Lcij;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    .line 283
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v1

    .line 285
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    .line 286
    div-float v0, v1, v0

    .line 288
    iget-object v1, p0, Lcij;->Z:Lhcr;

    invoke-virtual {v1, v0}, Lhcr;->a(F)V

    .line 289
    return-void
.end method

.method static synthetic b(Lcij;)Lckj;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcij;->ab:Lckj;

    return-object v0
.end method

.method static synthetic c(Lcij;)Lcmv;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcij;->ac:Lcmv;

    return-object v0
.end method

.method static synthetic d(Lcij;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcij;->Y:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    return-object v0
.end method

.method static synthetic e(Lcij;)I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lcij;->aa:I

    return v0
.end method

.method static synthetic f(Lcij;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Lcij;->at()V

    return-void
.end method


# virtual methods
.method protected X()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    sget-object v0, Lcij;->W:Ljava/util/List;

    return-object v0
.end method

.method public a(FF)V
    .locals 6

    .prologue
    const/16 v5, 0x19

    const/16 v4, 0x18

    const/high16 v2, 0x3f800000    # 1.0f

    .line 248
    iget-object v0, p0, Lcij;->af:Landroid/graphics/PointF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 250
    iget-object v0, p0, Lcij;->af:Landroid/graphics/PointF;

    iget v1, p0, Lcij;->ae:I

    invoke-static {v0, v1, v2, v2}, Lhbg;->a(Landroid/graphics/PointF;IFF)V

    .line 252
    invoke-virtual {p0}, Lcij;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    .line 253
    invoke-virtual {v1, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v2

    .line 254
    invoke-virtual {v1, v5}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    .line 255
    iget-object v3, p0, Lcij;->af:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    int-to-float v2, v2

    mul-float/2addr v2, v3

    .line 256
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .line 255
    invoke-virtual {v1, v4, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(ILjava/lang/Object;)Z

    move-result v2

    .line 257
    iget-object v3, p0, Lcij;->af:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    int-to-float v0, v0

    mul-float/2addr v0, v3

    .line 258
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 257
    invoke-virtual {v1, v5, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(ILjava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz v2, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 259
    :goto_0
    if-eqz v0, :cond_1

    .line 260
    invoke-virtual {p0}, Lcij;->ak()V

    .line 262
    :cond_1
    return-void

    .line 257
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lckp;)V
    .locals 8

    .prologue
    const/16 v7, 0xc

    const/4 v3, 0x0

    .line 190
    invoke-virtual {p0}, Lcij;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v6

    .line 191
    const v0, 0x7f0203a3

    const v1, 0x7f0a009b

    .line 193
    invoke-virtual {p0, v1}, Lcij;->e_(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcil;

    invoke-direct {v2, p0}, Lcil;-><init>(Lcij;)V

    .line 191
    invoke-virtual {p1, v0, v1, v2}, Lckp;->a(ILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 205
    invoke-virtual {v6, v7}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcij;->aa:I

    .line 206
    const v1, 0x7f0203a1

    const v2, 0x7f0203a0

    const v0, 0x7f0a0089

    .line 210
    invoke-virtual {p0, v0}, Lcij;->e_(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcim;

    invoke-direct {v5, p0, v6}, Lcim;-><init>(Lcij;Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    move-object v0, p1

    .line 206
    invoke-virtual/range {v0 .. v5}, Lckp;->a(IIILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    move-result-object v0

    iput-object v0, p0, Lcij;->Y:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 223
    iget-object v0, p0, Lcij;->Y:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 224
    invoke-virtual {v6, v7}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v1

    iget v2, p0, Lcij;->aa:I

    if-ne v1, v2, :cond_0

    const/4 v3, 0x1

    .line 223
    :cond_0
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setSelected(Z)V

    .line 226
    return-void
.end method

.method protected a(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V
    .locals 7

    .prologue
    const/16 v6, 0x19

    const/16 v5, 0x18

    const/high16 v4, 0x3f800000    # 1.0f

    .line 89
    invoke-super {p0, p1}, Lcjy;->a(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V

    .line 91
    new-instance v0, Lhcr;

    invoke-direct {v0, p1}, Lhcr;-><init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V

    iput-object v0, p0, Lcij;->Z:Lhcr;

    .line 92
    iget-object v0, p0, Lcij;->Z:Lhcr;

    iput-object p0, v0, Lhdf;->a:Lhdg;

    .line 94
    invoke-virtual {p0}, Lcij;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    .line 95
    iget-object v2, p0, Lcij;->af:Landroid/graphics/PointF;

    invoke-virtual {v1, v5}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v3

    .line 96
    invoke-virtual {v1, v5}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v3, v0

    iput v0, v2, Landroid/graphics/PointF;->x:F

    .line 97
    iget-object v2, p0, Lcij;->af:Landroid/graphics/PointF;

    invoke-virtual {v1, v6}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v3

    .line 98
    invoke-virtual {v1, v6}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v3, v0

    iput v0, v2, Landroid/graphics/PointF;->y:F

    .line 100
    invoke-virtual {p0}, Lcij;->ah()Lchp;

    move-result-object v0

    invoke-virtual {v0}, Lchp;->h()Lcom/google/android/libraries/photoeditor/core/FilterChain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getPostRotation()I

    move-result v0

    iput v0, p0, Lcij;->ae:I

    .line 101
    iget-object v0, p0, Lcij;->af:Landroid/graphics/PointF;

    iget v1, p0, Lcij;->ae:I

    invoke-static {v0, v1, v4, v4}, Lhbg;->b(Landroid/graphics/PointF;IFF)V

    .line 103
    iget-object v0, p0, Lcij;->Z:Lhcr;

    iget-object v1, p0, Lcij;->af:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcij;->af:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Lhcr;->g(FF)Z

    .line 104
    invoke-direct {p0}, Lcij;->at()V

    .line 106
    iget-object v0, p0, Lcij;->Z:Lhcr;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a(Lhdx;)I

    .line 107
    return-void
.end method

.method protected a(Z)V
    .locals 2

    .prologue
    .line 266
    invoke-super {p0, p1}, Lcjy;->a(Z)V

    .line 268
    iget-object v0, p0, Lcij;->Z:Lhcr;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhcr;->c(Z)V

    .line 269
    return-void
.end method

.method protected a(ILjava/lang/Object;Z)Z
    .locals 1

    .prologue
    .line 177
    invoke-super {p0, p1, p2, p3}, Lcjy;->a(ILjava/lang/Object;Z)Z

    move-result v0

    if-nez v0, :cond_0

    .line 178
    const/4 v0, 0x0

    .line 185
    :goto_0
    return v0

    .line 181
    :cond_0
    const/4 v0, 0x4

    if-ne p1, v0, :cond_1

    .line 182
    invoke-direct {p0}, Lcij;->at()V

    .line 185
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 74
    new-instance v0, Lhmk;

    sget-object v1, Lonc;->e:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method public b(ILjava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 230
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 231
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_0

    sget-object v1, Lcij;->X:[I

    const/4 v1, 0x6

    if-ge v0, v1, :cond_0

    sget-object v1, Lcij;->X:[I

    aget v0, v1, v0

    invoke-virtual {p0, v0}, Lcij;->e_(I)Ljava/lang/String;

    move-result-object v0

    .line 237
    :goto_0
    return-object v0

    .line 231
    :cond_0
    const-string v0, "*UNKNOWN*"

    goto :goto_0

    .line 232
    :cond_1
    const/16 v0, 0xc

    if-ne p1, v0, :cond_3

    .line 233
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 234
    iget v1, p0, Lcij;->aa:I

    if-ne v0, v1, :cond_2

    const v0, 0x7f0a0062

    .line 235
    invoke-virtual {p0, v0}, Lcij;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const v0, 0x7f0a0061

    invoke-virtual {p0, v0}, Lcij;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 237
    :cond_3
    invoke-super {p0, p1, p2}, Lcjy;->b(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected b(Z)V
    .locals 2

    .prologue
    .line 273
    invoke-super {p0, p1}, Lcjy;->b(Z)V

    .line 275
    iget-object v0, p0, Lcij;->Z:Lhcr;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhcr;->c(Z)V

    .line 276
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 79
    const/16 v0, 0xb

    return v0
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 154
    invoke-super {p0, p1}, Lcjy;->c(I)V

    .line 155
    iget-object v0, p0, Lcij;->P:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    .line 156
    return-void
.end method

.method public d(I)V
    .locals 1

    .prologue
    .line 160
    invoke-super {p0, p1}, Lcjy;->d(I)V

    .line 161
    iget-object v0, p0, Lcij;->P:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    .line 162
    return-void
.end method

.method public e(I)V
    .locals 3

    .prologue
    .line 166
    invoke-super {p0, p1}, Lcjy;->e(I)V

    .line 168
    iget-object v1, p0, Lcij;->Z:Lhcr;

    .line 169
    invoke-virtual {p0}, Lcij;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getActiveParameterKey()I

    move-result v0

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    .line 168
    :goto_0
    invoke-virtual {v1, v0}, Lhcr;->a(Z)V

    .line 171
    iget-object v0, p0, Lcij;->P:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    .line 172
    return-void

    .line 169
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected y_()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 111
    invoke-super {p0}, Lcjy;->y_()V

    .line 113
    iget-object v0, p0, Lcij;->ab:Lckj;

    if-eqz v0, :cond_0

    .line 150
    :goto_0
    return-void

    .line 117
    :cond_0
    invoke-virtual {p0}, Lcij;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 118
    const v1, 0x7f0d0047

    .line 119
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 120
    invoke-virtual {p0}, Lcij;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v6

    .line 123
    invoke-virtual {p0}, Lcij;->ah()Lchp;

    move-result-object v0

    invoke-virtual {v0}, Lchp;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    move v2, v1

    move v4, v3

    move v5, v3

    .line 122
    invoke-static/range {v0 .. v5}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->createCenterCropBitmap(Landroid/graphics/Bitmap;IIIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 124
    new-instance v2, Lckj;

    const/4 v3, 0x3

    invoke-direct {v2, p0, v6, v3, v0}, Lckj;-><init>(Lcjy;Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;ILandroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcij;->ab:Lckj;

    .line 128
    invoke-virtual {p0}, Lcij;->ah()Lchp;

    move-result-object v0

    invoke-virtual {v0}, Lchp;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcij;->a(ILandroid/graphics/Bitmap;)Landroid/graphics/Rect;

    move-result-object v1

    .line 130
    invoke-virtual {p0}, Lcij;->an()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lhap;

    .line 132
    new-instance v2, Lcik;

    invoke-direct {v2, p0, v1, v6}, Lcik;-><init>(Lcij;Landroid/graphics/Rect;Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lhap;->a(Lhbl;Z)V

    goto :goto_0
.end method
