.class public final enum Lbyn;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Lbys;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbyn;",
        ">;",
        "Lbys;"
    }
.end annotation


# static fields
.field public static final enum A:Lbyn;

.field public static final enum B:Lbyn;

.field public static final enum C:Lbyn;

.field public static final enum D:Lbyn;

.field public static final enum E:Lbyn;

.field public static final enum F:Lbyn;

.field public static final enum G:Lbyn;

.field public static final enum H:Lbyn;

.field public static final enum I:Lbyn;

.field public static final enum J:Lbyn;

.field public static final enum K:Lbyn;

.field public static final enum L:Lbyn;

.field public static final enum M:Lbyn;

.field public static final enum N:Lbyn;

.field public static final enum O:Lbyn;

.field public static final enum P:Lbyn;

.field public static final enum Q:Lbyn;

.field public static final enum R:Lbyn;

.field private static final synthetic Y:[Lbyn;

.field public static final enum a:Lbyn;

.field public static final enum b:Lbyn;

.field public static final enum c:Lbyn;

.field public static final enum d:Lbyn;

.field public static final enum e:Lbyn;

.field public static final enum f:Lbyn;

.field public static final enum g:Lbyn;

.field public static final enum h:Lbyn;

.field public static final enum i:Lbyn;

.field public static final enum j:Lbyn;

.field public static final enum k:Lbyn;

.field public static final enum l:Lbyn;

.field public static final enum m:Lbyn;

.field public static final enum n:Lbyn;

.field public static final enum o:Lbyn;

.field public static final enum p:Lbyn;

.field public static final enum q:Lbyn;

.field public static final enum r:Lbyn;

.field public static final enum s:Lbyn;

.field public static final enum t:Lbyn;

.field public static final enum u:Lbyn;

.field public static final enum v:Lbyn;

.field public static final enum w:Lbyn;

.field public static final enum x:Lbyn;

.field public static final enum y:Lbyn;

.field public static final enum z:Lbyn;


# instance fields
.field private final S:I

.field private final T:Ljava/lang/String;

.field private final U:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final V:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final W:I

.field private X:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    .line 53
    new-instance v0, Lbyn;

    const-string v1, "PT"

    const/4 v2, 0x0

    const/4 v3, -0x1

    const-string v4, "P"

    const-string v5, "P"

    const-string v6, ""

    const/4 v7, 0x4

    const/4 v8, 0x1

    const/4 v9, 0x1

    new-array v9, v9, [I

    const/4 v10, 0x0

    const/4 v11, 0x1

    aput v11, v9, v10

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->a:Lbyn;

    .line 54
    new-instance v0, Lbyn;

    const-string v1, "VT"

    const/4 v2, 0x1

    const/4 v3, -0x1

    const-string v4, "V"

    const-string v5, "V"

    const-string v6, ""

    const/4 v7, 0x4

    const/4 v8, 0x1

    const/4 v9, 0x1

    new-array v9, v9, [I

    const/4 v10, 0x0

    const/4 v11, 0x1

    aput v11, v9, v10

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->b:Lbyn;

    .line 59
    new-instance v0, Lbyn;

    const-string v1, "B8"

    const/4 v2, 0x2

    const/4 v3, 0x0

    const-string v4, "Bbbbbbbb"

    const-string v5, "B,b,b,b,b,b,b,b"

    const-string v6, "B"

    const/4 v7, 0x4

    const/16 v8, 0x8

    const/16 v9, 0x8

    new-array v9, v9, [I

    fill-array-data v9, :array_0

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->c:Lbyn;

    .line 60
    new-instance v0, Lbyn;

    const-string v1, "B7"

    const/4 v2, 0x3

    const/4 v3, 0x0

    const-string v4, "Bbbbbbb"

    const-string v5, "B,b,b,b,b,b,b"

    const-string v6, "B"

    const/4 v7, 0x4

    const/16 v8, 0x8

    const/4 v9, 0x7

    new-array v9, v9, [I

    fill-array-data v9, :array_1

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->d:Lbyn;

    .line 61
    new-instance v0, Lbyn;

    const-string v1, "B6"

    const/4 v2, 0x4

    const/4 v3, 0x0

    const-string v4, "Bbbbbb"

    const-string v5, "B,b,b,b,b,b,"

    const-string v6, "B"

    const/4 v7, 0x4

    const/16 v8, 0x8

    const/4 v9, 0x6

    new-array v9, v9, [I

    fill-array-data v9, :array_2

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->e:Lbyn;

    .line 62
    new-instance v0, Lbyn;

    const-string v1, "F8"

    const/4 v2, 0x5

    const/4 v3, 0x0

    const-string v4, "Ffffffff"

    const-string v5, "F,f,f,f,f,f,f,f,"

    const-string v6, "F"

    const/4 v7, 0x4

    const/16 v8, 0x8

    const/16 v9, 0x8

    new-array v9, v9, [I

    fill-array-data v9, :array_3

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->f:Lbyn;

    .line 63
    new-instance v0, Lbyn;

    const-string v1, "F7"

    const/4 v2, 0x6

    const/4 v3, 0x0

    const-string v4, "Fffffff"

    const-string v5, "F,f,f,f,f,f,f"

    const-string v6, "F"

    const/4 v7, 0x4

    const/16 v8, 0x8

    const/4 v9, 0x7

    new-array v9, v9, [I

    fill-array-data v9, :array_4

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->g:Lbyn;

    .line 64
    new-instance v0, Lbyn;

    const-string v1, "F6"

    const/4 v2, 0x7

    const/4 v3, 0x0

    const-string v4, "Ffffff"

    const-string v5, "F,f,f,f,f,f"

    const-string v6, "F"

    const/4 v7, 0x4

    const/16 v8, 0x8

    const/4 v9, 0x6

    new-array v9, v9, [I

    fill-array-data v9, :array_5

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->h:Lbyn;

    .line 67
    new-instance v0, Lbyn;

    const-string v1, "PPPP"

    const/16 v2, 0x8

    const/4 v3, 0x1

    const-string v4, "PPPP"

    const-string v5, "P,P,P,P"

    const-string v6, "P,S"

    const/4 v7, 0x4

    const/4 v8, 0x4

    const/4 v9, 0x4

    new-array v9, v9, [I

    fill-array-data v9, :array_6

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->i:Lbyn;

    .line 68
    new-instance v0, Lbyn;

    const-string v1, "PPPZ2"

    const/16 v2, 0x9

    const/4 v3, 0x1

    const-string v4, "PPPP"

    const-string v5, "P,P,P,Z2"

    const-string v6, "P,S,Z"

    const/4 v7, 0x4

    const/4 v8, 0x4

    const/4 v9, 0x4

    new-array v9, v9, [I

    fill-array-data v9, :array_7

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->j:Lbyn;

    .line 69
    new-instance v0, Lbyn;

    const-string v1, "PPK1"

    const/16 v2, 0xa

    const/4 v3, 0x1

    const-string v4, "PPP"

    const-string v5, "P,P,K1"

    const-string v6, "P,S,K"

    const/4 v7, 0x4

    const/4 v8, 0x4

    const/4 v9, 0x3

    new-array v9, v9, [I

    fill-array-data v9, :array_8

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->k:Lbyn;

    .line 70
    new-instance v0, Lbyn;

    const-string v1, "PPZ3"

    const/16 v2, 0xb

    const/4 v3, 0x1

    const-string v4, "PPP"

    const-string v5, "P,P,Z3"

    const-string v6, "P,S,Z"

    const/4 v7, 0x4

    const/4 v8, 0x4

    const/4 v9, 0x3

    new-array v9, v9, [I

    fill-array-data v9, :array_9

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->l:Lbyn;

    .line 71
    new-instance v0, Lbyn;

    const-string v1, "PK2"

    const/16 v2, 0xc

    const/4 v3, 0x1

    const-string v4, "PP"

    const-string v5, "P,K2"

    const-string v6, "P,K"

    const/4 v7, 0x4

    const/4 v8, 0x4

    const/4 v9, 0x2

    new-array v9, v9, [I

    fill-array-data v9, :array_a

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->m:Lbyn;

    .line 72
    new-instance v0, Lbyn;

    const-string v1, "K3"

    const/16 v2, 0xd

    const/4 v3, 0x1

    const-string v4, "P"

    const-string v5, "K3"

    const-string v6, "K"

    const/4 v7, 0x4

    const/4 v8, 0x1

    const/4 v9, 0x1

    new-array v9, v9, [I

    const/4 v10, 0x0

    const/4 v11, 0x1

    aput v11, v9, v10

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->n:Lbyn;

    .line 73
    new-instance v0, Lbyn;

    const-string v1, "PPB4"

    const/16 v2, 0xe

    const/4 v3, 0x1

    const-string v4, "PPBbbb"

    const-string v5, "P,P,B,b,b,b"

    const-string v6, "P,S,B"

    const/4 v7, 0x4

    const/16 v8, 0x8

    const/4 v9, 0x6

    new-array v9, v9, [I

    fill-array-data v9, :array_b

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->o:Lbyn;

    .line 74
    new-instance v0, Lbyn;

    const-string v1, "PPB5"

    const/16 v2, 0xf

    const/4 v3, 0x1

    const-string v4, "PPBbbbb"

    const-string v5, "P,P,B,b,b,b,b"

    const-string v6, "P,S,B"

    const/4 v7, 0x4

    const/16 v8, 0x14

    const/4 v9, 0x7

    new-array v9, v9, [I

    fill-array-data v9, :array_c

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->p:Lbyn;

    .line 75
    new-instance v0, Lbyn;

    const-string v1, "PPF4"

    const/16 v2, 0x10

    const/4 v3, 0x1

    const-string v4, "PPFfff"

    const-string v5, "P,P,F,f,f,f"

    const-string v6, "P,S,F"

    const/4 v7, 0x4

    const/16 v8, 0x8

    const/4 v9, 0x6

    new-array v9, v9, [I

    fill-array-data v9, :array_d

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->q:Lbyn;

    .line 76
    new-instance v0, Lbyn;

    const-string v1, "PPF5"

    const/16 v2, 0x11

    const/4 v3, 0x1

    const-string v4, "PPFffff"

    const-string v5, "P,P,F,f,f,f,f"

    const-string v6, "P,S,F"

    const/4 v7, 0x4

    const/16 v8, 0x14

    const/4 v9, 0x7

    new-array v9, v9, [I

    fill-array-data v9, :array_e

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->r:Lbyn;

    .line 77
    new-instance v0, Lbyn;

    const-string v1, "PPV"

    const/16 v2, 0x12

    const/4 v3, 0x1

    const-string v4, "PPV"

    const-string v5, "P,P,V"

    const-string v6, "P,S,V"

    const/4 v7, 0x4

    const/4 v8, 0x4

    const/4 v9, 0x3

    new-array v9, v9, [I

    fill-array-data v9, :array_f

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->s:Lbyn;

    .line 78
    new-instance v0, Lbyn;

    const-string v1, "K1V"

    const/16 v2, 0x13

    const/4 v3, 0x1

    const-string v4, "PV"

    const-string v5, "K1,V"

    const-string v6, "K,V"

    const/4 v7, 0x4

    const/4 v8, 0x2

    const/4 v9, 0x2

    new-array v9, v9, [I

    fill-array-data v9, :array_10

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->t:Lbyn;

    .line 81
    new-instance v0, Lbyn;

    const-string v1, "PPZ2"

    const/16 v2, 0x14

    const/4 v3, 0x2

    const-string v4, "PPP"

    const-string v5, "P,S,Z"

    const-string v6, "P,S,Z"

    const/4 v7, 0x4

    const/4 v8, 0x4

    const/4 v9, 0x3

    new-array v9, v9, [I

    fill-array-data v9, :array_11

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->u:Lbyn;

    .line 82
    new-instance v0, Lbyn;

    const-string v1, "PPB2"

    const/16 v2, 0x15

    const/4 v3, 0x2

    const-string v4, "PPBb"

    const-string v5, "P,P,B,b"

    const-string v6, "P,S,B"

    const/4 v7, 0x4

    const/4 v8, 0x4

    const/4 v9, 0x4

    new-array v9, v9, [I

    fill-array-data v9, :array_12

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->v:Lbyn;

    .line 83
    new-instance v0, Lbyn;

    const-string v1, "PPB3"

    const/16 v2, 0x16

    const/4 v3, 0x2

    const-string v4, "PPBbb"

    const-string v5, "P,P,B,b,b,"

    const-string v6, "P,S,B"

    const/4 v7, 0x4

    const/16 v8, 0xc

    const/4 v9, 0x5

    new-array v9, v9, [I

    fill-array-data v9, :array_13

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->w:Lbyn;

    .line 84
    new-instance v0, Lbyn;

    const-string v1, "PB4"

    const/16 v2, 0x17

    const/4 v3, 0x2

    const-string v4, "PBbbb"

    const-string v5, "P,B,b,b,b"

    const-string v6, "P,B"

    const/4 v7, 0x4

    const/16 v8, 0x8

    const/4 v9, 0x5

    new-array v9, v9, [I

    fill-array-data v9, :array_14

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->x:Lbyn;

    .line 85
    new-instance v0, Lbyn;

    const-string v1, "PB5"

    const/16 v2, 0x18

    const/4 v3, 0x2

    const-string v4, "PBbbbb"

    const-string v5, "P,B,b,b,b,b"

    const-string v6, "P,B"

    const/4 v7, 0x4

    const/16 v8, 0x8

    const/4 v9, 0x6

    new-array v9, v9, [I

    fill-array-data v9, :array_15

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->y:Lbyn;

    .line 86
    new-instance v0, Lbyn;

    const-string v1, "PPF2"

    const/16 v2, 0x19

    const/4 v3, 0x2

    const-string v4, "PPFf"

    const-string v5, "P,P,F,f"

    const-string v6, "P,S,F"

    const/4 v7, 0x4

    const/4 v8, 0x4

    const/4 v9, 0x4

    new-array v9, v9, [I

    fill-array-data v9, :array_16

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->z:Lbyn;

    .line 87
    new-instance v0, Lbyn;

    const-string v1, "PPF3"

    const/16 v2, 0x1a

    const/4 v3, 0x2

    const-string v4, "PPFff"

    const-string v5, "P,P,F,f,f,"

    const-string v6, "P,S,F"

    const/4 v7, 0x4

    const/16 v8, 0xc

    const/4 v9, 0x5

    new-array v9, v9, [I

    fill-array-data v9, :array_17

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->A:Lbyn;

    .line 88
    new-instance v0, Lbyn;

    const-string v1, "PF4"

    const/16 v2, 0x1b

    const/4 v3, 0x2

    const-string v4, "PFfff"

    const-string v5, "P,F,f,f,f"

    const-string v6, "P,F"

    const/4 v7, 0x4

    const/16 v8, 0x8

    const/4 v9, 0x5

    new-array v9, v9, [I

    fill-array-data v9, :array_18

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->B:Lbyn;

    .line 89
    new-instance v0, Lbyn;

    const-string v1, "PF5"

    const/16 v2, 0x1c

    const/4 v3, 0x2

    const-string v4, "PFffff"

    const-string v5, "P,F,f,f,f,f"

    const-string v6, "P,F"

    const/4 v7, 0x4

    const/16 v8, 0x8

    const/4 v9, 0x6

    new-array v9, v9, [I

    fill-array-data v9, :array_19

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->C:Lbyn;

    .line 90
    new-instance v0, Lbyn;

    const-string v1, "Z2K1"

    const/16 v2, 0x1d

    const/4 v3, 0x2

    const-string v4, "PP"

    const-string v5, "Z2,K1"

    const-string v6, "Z,K"

    const/4 v7, 0x4

    const/4 v8, 0x2

    const/4 v9, 0x2

    new-array v9, v9, [I

    fill-array-data v9, :array_1a

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->D:Lbyn;

    .line 91
    new-instance v0, Lbyn;

    const-string v1, "Z2Z2"

    const/16 v2, 0x1e

    const/4 v3, 0x2

    const-string v4, "PP"

    const-string v5, "Z2,Z2"

    const-string v6, "Z"

    const/4 v7, 0x4

    const/4 v8, 0x2

    const/4 v9, 0x2

    new-array v9, v9, [I

    fill-array-data v9, :array_1b

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->E:Lbyn;

    .line 92
    new-instance v0, Lbyn;

    const-string v1, "Z2V"

    const/16 v2, 0x1f

    const/4 v3, 0x2

    const-string v4, "PV"

    const-string v5, "Z2,V"

    const-string v6, "Z,V"

    const/4 v7, 0x4

    const/4 v8, 0x2

    const/4 v9, 0x2

    new-array v9, v9, [I

    fill-array-data v9, :array_1c

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->F:Lbyn;

    .line 96
    new-instance v0, Lbyn;

    const-string v1, "FB_P"

    const/16 v2, 0x20

    const/4 v3, 0x3

    const-string v4, "P"

    const-string v5, "P"

    const-string v6, "P"

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x1

    new-array v9, v9, [I

    const/4 v10, 0x0

    const/4 v11, 0x1

    aput v11, v9, v10

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->G:Lbyn;

    .line 97
    new-instance v0, Lbyn;

    const-string v1, "FB_V"

    const/16 v2, 0x21

    const/4 v3, 0x3

    const-string v4, "V"

    const-string v5, "V"

    const-string v6, "V"

    const/4 v7, 0x2

    const/4 v8, 0x1

    const/4 v9, 0x1

    new-array v9, v9, [I

    const/4 v10, 0x0

    const/4 v11, 0x1

    aput v11, v9, v10

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->H:Lbyn;

    .line 98
    new-instance v0, Lbyn;

    const-string v1, "FB_B2"

    const/16 v2, 0x22

    const/4 v3, 0x3

    const-string v4, "Bb"

    const-string v5, "B,b"

    const-string v6, "B"

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x2

    new-array v9, v9, [I

    fill-array-data v9, :array_1d

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->I:Lbyn;

    .line 99
    new-instance v0, Lbyn;

    const-string v1, "FB_B3"

    const/16 v2, 0x23

    const/4 v3, 0x3

    const-string v4, "Bbb"

    const-string v5, "B,b,b"

    const-string v6, "B"

    const/4 v7, 0x2

    const/4 v8, 0x4

    const/4 v9, 0x3

    new-array v9, v9, [I

    fill-array-data v9, :array_1e

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->J:Lbyn;

    .line 100
    new-instance v0, Lbyn;

    const-string v1, "FB_B4"

    const/16 v2, 0x24

    const/4 v3, 0x3

    const-string v4, "Bbbb"

    const-string v5, "B,b,b,b"

    const-string v6, "B"

    const/4 v7, 0x2

    const/4 v8, 0x4

    const/4 v9, 0x4

    new-array v9, v9, [I

    fill-array-data v9, :array_1f

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->K:Lbyn;

    .line 101
    new-instance v0, Lbyn;

    const-string v1, "FB_B5"

    const/16 v2, 0x25

    const/4 v3, 0x3

    const-string v4, "Bbbbb"

    const-string v5, "B,b,b,b,b"

    const-string v6, "B"

    const/4 v7, 0x3

    const/4 v8, 0x6

    const/4 v9, 0x5

    new-array v9, v9, [I

    fill-array-data v9, :array_20

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->L:Lbyn;

    .line 102
    new-instance v0, Lbyn;

    const-string v1, "FB_F2"

    const/16 v2, 0x26

    const/4 v3, 0x3

    const-string v4, "Ff"

    const-string v5, "F,f"

    const-string v6, "F"

    const/4 v7, 0x1

    const/4 v8, 0x2

    const/4 v9, 0x2

    new-array v9, v9, [I

    fill-array-data v9, :array_21

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->M:Lbyn;

    .line 103
    new-instance v0, Lbyn;

    const-string v1, "FB_F3"

    const/16 v2, 0x27

    const/4 v3, 0x3

    const-string v4, "Fff"

    const-string v5, "F,f,f"

    const-string v6, "F"

    const/4 v7, 0x2

    const/4 v8, 0x4

    const/4 v9, 0x3

    new-array v9, v9, [I

    fill-array-data v9, :array_22

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->N:Lbyn;

    .line 104
    new-instance v0, Lbyn;

    const-string v1, "FB_F4"

    const/16 v2, 0x28

    const/4 v3, 0x3

    const-string v4, "Ffff"

    const-string v5, "F,f,f,f"

    const-string v6, "F"

    const/4 v7, 0x2

    const/4 v8, 0x4

    const/4 v9, 0x4

    new-array v9, v9, [I

    fill-array-data v9, :array_23

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->O:Lbyn;

    .line 105
    new-instance v0, Lbyn;

    const-string v1, "FB_F5"

    const/16 v2, 0x29

    const/4 v3, 0x3

    const-string v4, "Fffff"

    const-string v5, "F,f,f,f,f"

    const-string v6, "F"

    const/4 v7, 0x3

    const/4 v8, 0x6

    const/4 v9, 0x5

    new-array v9, v9, [I

    fill-array-data v9, :array_24

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->P:Lbyn;

    .line 108
    new-instance v0, Lbyn;

    const-string v1, "EMPTY_VIDEO"

    const/16 v2, 0x2a

    const v3, 0x7fffffff

    const-string v4, "O"

    const-string v5, "EV"

    const-string v6, ""

    const/4 v7, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x1

    new-array v9, v9, [I

    const/4 v10, 0x0

    const/4 v11, 0x1

    aput v11, v9, v10

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->Q:Lbyn;

    .line 109
    new-instance v0, Lbyn;

    const-string v1, "END_CARD"

    const/16 v2, 0x2b

    const v3, 0x7fffffff

    const-string v4, "D"

    const-string v5, "EC"

    const-string v6, ""

    const/4 v7, 0x4

    const/4 v8, 0x1

    const/4 v9, 0x1

    new-array v9, v9, [I

    const/4 v10, 0x0

    const/4 v11, 0x1

    aput v11, v9, v10

    invoke-direct/range {v0 .. v9}, Lbyn;-><init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V

    sput-object v0, Lbyn;->R:Lbyn;

    .line 48
    const/16 v0, 0x2c

    new-array v0, v0, [Lbyn;

    const/4 v1, 0x0

    sget-object v2, Lbyn;->a:Lbyn;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lbyn;->b:Lbyn;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lbyn;->c:Lbyn;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lbyn;->d:Lbyn;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lbyn;->e:Lbyn;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lbyn;->f:Lbyn;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lbyn;->g:Lbyn;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lbyn;->h:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lbyn;->i:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lbyn;->j:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lbyn;->k:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lbyn;->l:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lbyn;->m:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lbyn;->n:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lbyn;->o:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lbyn;->p:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lbyn;->q:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lbyn;->r:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lbyn;->s:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lbyn;->t:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lbyn;->u:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lbyn;->v:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lbyn;->w:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lbyn;->x:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lbyn;->y:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lbyn;->z:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lbyn;->A:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lbyn;->B:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lbyn;->C:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lbyn;->D:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lbyn;->E:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lbyn;->F:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lbyn;->G:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lbyn;->H:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lbyn;->I:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lbyn;->J:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lbyn;->K:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lbyn;->L:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lbyn;->M:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lbyn;->N:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lbyn;->O:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lbyn;->P:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lbyn;->Q:Lbyn;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lbyn;->R:Lbyn;

    aput-object v2, v0, v1

    sput-object v0, Lbyn;->Y:[Lbyn;

    return-void

    .line 59
    :array_0
    .array-data 4
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
    .end array-data

    .line 60
    :array_1
    .array-data 4
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x2
    .end array-data

    .line 61
    :array_2
    .array-data 4
        0x1
        0x1
        0x1
        0x1
        0x1
        0x3
    .end array-data

    .line 62
    :array_3
    .array-data 4
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
    .end array-data

    .line 63
    :array_4
    .array-data 4
        0x1
        0x1
        0x1
        0x1
        0x1
        0x1
        0x2
    .end array-data

    .line 64
    :array_5
    .array-data 4
        0x1
        0x1
        0x1
        0x1
        0x1
        0x3
    .end array-data

    .line 67
    :array_6
    .array-data 4
        0x1
        0x1
        0x1
        0x1
    .end array-data

    .line 68
    :array_7
    .array-data 4
        0x1
        0x1
        0x1
        0x1
    .end array-data

    .line 69
    :array_8
    .array-data 4
        0x1
        0x1
        0x2
    .end array-data

    .line 70
    :array_9
    .array-data 4
        0x1
        0x1
        0x2
    .end array-data

    .line 71
    :array_a
    .array-data 4
        0x1
        0x3
    .end array-data

    .line 73
    :array_b
    .array-data 4
        0x2
        0x2
        0x1
        0x1
        0x1
        0x1
    .end array-data

    .line 74
    :array_c
    .array-data 4
        0x5
        0x5
        0x2
        0x2
        0x2
        0x2
        0x2
    .end array-data

    .line 75
    :array_d
    .array-data 4
        0x2
        0x2
        0x1
        0x1
        0x1
        0x1
    .end array-data

    .line 76
    :array_e
    .array-data 4
        0x5
        0x5
        0x2
        0x2
        0x2
        0x2
        0x2
    .end array-data

    .line 77
    :array_f
    .array-data 4
        0x1
        0x1
        0x2
    .end array-data

    .line 78
    :array_10
    .array-data 4
        0x1
        0x1
    .end array-data

    .line 81
    :array_11
    .array-data 4
        0x1
        0x1
        0x2
    .end array-data

    .line 82
    :array_12
    .array-data 4
        0x1
        0x1
        0x1
        0x1
    .end array-data

    .line 83
    :array_13
    .array-data 4
        0x3
        0x3
        0x2
        0x2
        0x2
    .end array-data

    .line 84
    :array_14
    .array-data 4
        0x2
        0x2
        0x2
        0x1
        0x1
    .end array-data

    .line 85
    :array_15
    .array-data 4
        0x2
        0x2
        0x1
        0x1
        0x1
        0x1
    .end array-data

    .line 86
    :array_16
    .array-data 4
        0x1
        0x1
        0x1
        0x1
    .end array-data

    .line 87
    :array_17
    .array-data 4
        0x3
        0x3
        0x2
        0x2
        0x2
    .end array-data

    .line 88
    :array_18
    .array-data 4
        0x2
        0x2
        0x2
        0x1
        0x1
    .end array-data

    .line 89
    :array_19
    .array-data 4
        0x2
        0x2
        0x1
        0x1
        0x1
        0x1
    .end array-data

    .line 90
    :array_1a
    .array-data 4
        0x1
        0x1
    .end array-data

    .line 91
    :array_1b
    .array-data 4
        0x1
        0x1
    .end array-data

    .line 92
    :array_1c
    .array-data 4
        0x1
        0x1
    .end array-data

    .line 98
    :array_1d
    .array-data 4
        0x1
        0x1
    .end array-data

    .line 99
    :array_1e
    .array-data 4
        0x1
        0x1
        0x2
    .end array-data

    .line 100
    :array_1f
    .array-data 4
        0x1
        0x1
        0x1
        0x1
    .end array-data

    .line 101
    :array_20
    .array-data 4
        0x1
        0x1
        0x1
        0x1
        0x2
    .end array-data

    .line 102
    :array_21
    .array-data 4
        0x1
        0x1
    .end array-data

    .line 103
    :array_22
    .array-data 4
        0x1
        0x1
        0x2
    .end array-data

    .line 104
    :array_23
    .array-data 4
        0x1
        0x1
        0x1
        0x1
    .end array-data

    .line 105
    :array_24
    .array-data 4
        0x1
        0x1
        0x1
        0x1
        0x2
    .end array-data
.end method

.method private varargs constructor <init>(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;II[I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "II[I)V"
        }
    .end annotation

    .prologue
    .line 130
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 126
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbyn;->X:Ljava/util/List;

    .line 131
    iput p3, p0, Lbyn;->S:I

    .line 132
    iput-object p4, p0, Lbyn;->T:Ljava/lang/String;

    .line 133
    const-string v0, ","

    invoke-virtual {p6, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbyn;->U:Ljava/util/List;

    .line 134
    iput p7, p0, Lbyn;->W:I

    .line 135
    const-string v0, ","

    invoke-virtual {p5, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbyn;->V:Ljava/util/List;

    .line 136
    array-length v1, p9

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget v2, p9, v0

    .line 137
    iget-object v3, p0, Lbyn;->X:Ljava/util/List;

    int-to-float v2, v2

    int-to-float v4, p8

    div-float/2addr v2, v4

    .line 138
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    .line 137
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 140
    :cond_0
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbyn;
    .locals 1

    .prologue
    .line 48
    const-class v0, Lbyn;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbyn;

    return-object v0
.end method

.method public static values()[Lbyn;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lbyn;->Y:[Lbyn;

    invoke-virtual {v0}, [Lbyn;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbyn;

    return-object v0
.end method


# virtual methods
.method public a(I)D
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lbyn;->X:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    float-to-double v0, v0

    return-wide v0
.end method

.method public a()I
    .locals 1

    .prologue
    .line 144
    iget v0, p0, Lbyn;->S:I

    return v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lbyn;->T:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lbyn;->U:Ljava/util/List;

    return-object v0
.end method

.method public d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 159
    iget-object v0, p0, Lbyn;->V:Ljava/util/List;

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lbyn;->X:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 169
    iget v0, p0, Lbyn;->W:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 184
    iget-object v0, p0, Lbyn;->T:Ljava/lang/String;

    iget-object v1, p0, Lbyn;->V:Ljava/util/List;

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
