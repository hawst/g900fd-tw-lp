.class public Lbjb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbhr;
.implements Lbhs;
.implements Lcgk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbhr",
        "<",
        "Lbhl;",
        ">;",
        "Lbhs;",
        "Lcgk;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lbgf;

.field private final c:Landroid/content/Context;

.field private final d:Ljava/util/concurrent/Executor;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/os/ParcelFileDescriptor;",
            ">;"
        }
    .end annotation
.end field

.field private h:Lbhl;

.field private i:Ljava/util/concurrent/FutureTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/FutureTask",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private j:I

.field private k:J

.field private l:Z

.field private m:Landroid/graphics/Bitmap;

.field private n:Landroid/net/Uri;

.field private final o:Lbjf;

.field private final p:Ljava/util/concurrent/Callable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Callable",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const-class v0, Lbjb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbjb;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbgf;Landroid/content/Context;Ljava/util/concurrent/Executor;Lbjf;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbjb;->e:Ljava/util/List;

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbjb;->f:Ljava/util/List;

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbjb;->g:Ljava/util/List;

    .line 94
    new-instance v0, Lbjc;

    invoke-direct {v0, p0}, Lbjc;-><init>(Lbjb;)V

    iput-object v0, p0, Lbjb;->p:Ljava/util/concurrent/Callable;

    .line 128
    const-string v0, "renderContext"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgf;

    iput-object v0, p0, Lbjb;->b:Lbgf;

    .line 129
    const-string v0, "context"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbjb;->c:Landroid/content/Context;

    .line 130
    const-string v0, "singleThreadedExecutor"

    .line 131
    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lbjb;->d:Ljava/util/concurrent/Executor;

    .line 132
    const-string v0, "bitmapFactory"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjf;

    iput-object v0, p0, Lbjb;->o:Lbjf;

    .line 133
    return-void
.end method

.method static synthetic a(Lbjb;)I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lbjb;->j:I

    return v0
.end method

.method private a(Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/16 v1, 0x780

    .line 328
    iget-object v0, p0, Lbjb;->m:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 329
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v1, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lbjb;->m:Landroid/graphics/Bitmap;

    .line 331
    :cond_0
    iget-object v0, p0, Lbjb;->o:Lbjf;

    .line 332
    invoke-virtual {p1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    const v2, 0xe1000

    iget-object v3, p0, Lbjb;->m:Landroid/graphics/Bitmap;

    .line 331
    invoke-virtual {v0, v1, v2, v3}, Lbjf;->a(Ljava/io/FileDescriptor;ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbjb;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lbjb;->m:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic a(Lbjb;Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lbjb;->a(Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbjb;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    .prologue
    .line 38
    iput-object p1, p0, Lbjb;->n:Landroid/net/Uri;

    return-object p1
.end method

.method static synthetic b(Lbjb;)Ljava/util/List;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lbjb;->e:Ljava/util/List;

    return-object v0
.end method

.method static synthetic c(Lbjb;)Ljava/util/List;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lbjb;->g:Ljava/util/List;

    return-object v0
.end method

.method static synthetic d(Lbjb;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lbjb;->n:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lbjb;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lbjb;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lbjb;->m:Landroid/graphics/Bitmap;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 203
    iget-object v0, p0, Lbjb;->h:Lbhl;

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lbjb;->h:Lbhl;

    invoke-virtual {v0}, Lbhl;->b()V

    .line 205
    iget-object v0, p0, Lbjb;->h:Lbhl;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 206
    iput-object v1, p0, Lbjb;->h:Lbhl;

    .line 208
    :cond_0
    iget-object v0, p0, Lbjb;->m:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 209
    iget-object v0, p0, Lbjb;->m:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 210
    iput-object v1, p0, Lbjb;->m:Landroid/graphics/Bitmap;

    .line 212
    :cond_1
    iget-object v0, p0, Lbjb;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelFileDescriptor;

    .line 213
    invoke-static {v0}, Lcek;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 215
    :cond_2
    iget-object v0, p0, Lbjb;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 216
    iget-object v0, p0, Lbjb;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 217
    iget-object v0, p0, Lbjb;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 218
    return-void
.end method

.method public a(Lbhl;)V
    .locals 0

    .prologue
    .line 222
    invoke-static {p1}, Lcgl;->a(Lcgk;)V

    .line 223
    return-void
.end method

.method public a(Lbmd;J)V
    .locals 6

    .prologue
    .line 257
    iget-object v0, p1, Lbmd;->d:Lbmg;

    sget-object v1, Lbmg;->c:Lbmg;

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v1, "clip type must be PHOTO"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 260
    :try_start_0
    iget-object v0, p1, Lbmd;->e:Ljeg;

    iget-object v0, v0, Ljeg;->b:Landroid/net/Uri;

    invoke-static {}, Lceh;->a()Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lbjb;->c:Landroid/content/Context;

    invoke-static {v0, v1}, Lbje;->a(Landroid/net/Uri;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    invoke-static {v0, v1}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 266
    :goto_1
    iget-object v1, p0, Lbjb;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 267
    iget-object v1, p1, Lbmd;->f:Lbmp;

    iget-wide v2, v1, Lbmp;->b:J

    iput-wide v2, p0, Lbjb;->k:J

    .line 269
    :cond_0
    iget-object v1, p0, Lbjb;->e:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 270
    iget-object v1, p0, Lbjb;->f:Ljava/util/List;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 274
    iget-object v1, p0, Lbjb;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 277
    iget-object v1, p0, Lbjb;->m:Landroid/graphics/Bitmap;

    if-nez v1, :cond_1

    .line 279
    :try_start_1
    invoke-direct {p0, v0}, Lbjb;->a(Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lbjb;->m:Landroid/graphics/Bitmap;

    .line 280
    iget-object v0, p1, Lbmd;->e:Ljeg;

    iget-object v0, v0, Ljeg;->b:Landroid/net/Uri;

    iput-object v0, p0, Lbjb;->n:Landroid/net/Uri;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 285
    :cond_1
    :goto_2
    return-void

    .line 257
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 260
    :cond_3
    :try_start_2
    iget-object v1, p0, Lbjb;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "r"

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    goto :goto_1

    .line 261
    :catch_0
    move-exception v0

    .line 262
    new-instance v1, Ljava/lang/IllegalStateException;

    iget-object v2, p1, Lbmd;->e:Ljeg;

    iget-object v2, v2, Ljeg;->b:Landroid/net/Uri;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x19

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Failed to open photo URI "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 281
    :catch_1
    move-exception v0

    .line 282
    sget-object v1, Lbjb;->a:Ljava/lang/String;

    const-string v2, "preloading photo bitmap"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method

.method public a(Lbop;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 296
    const-string v1, "timeline"

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move v1, v0

    move v2, v0

    .line 301
    :goto_0
    invoke-interface {p1}, Lbop;->e()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 302
    invoke-interface {p1, v1}, Lbop;->b(I)Lbmd;

    move-result-object v3

    .line 303
    iget-object v0, v3, Lbmd;->d:Lbmg;

    sget-object v4, Lbmg;->c:Lbmg;

    if-ne v0, v4, :cond_0

    .line 304
    iget-object v0, p0, Lbjb;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    iget-object v0, v3, Lbmd;->e:Ljeg;

    iget-object v4, v0, Ljeg;->b:Landroid/net/Uri;

    iget-object v0, p0, Lbjb;->e:Ljava/util/List;

    .line 309
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-object v0, v0, Lbmd;->e:Ljeg;

    iget-object v0, v0, Ljeg;->b:Landroid/net/Uri;

    .line 308
    invoke-virtual {v4, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lbjb;->e:Ljava/util/List;

    invoke-interface {v0, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 315
    iget-object v0, p0, Lbjb;->f:Ljava/util/List;

    .line 316
    invoke-interface {p1, v1}, Lbop;->f(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 315
    invoke-interface {v0, v2, v3}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 317
    add-int/lit8 v2, v2, 0x1

    .line 318
    iget-object v0, p0, Lbjb;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v2, v0, :cond_0

    .line 319
    const-string v0, "new timeline is incompatible with existing clips"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 301
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 323
    :cond_1
    const-string v0, "oldClipIndex"

    iget-object v1, p0, Lbjb;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    const-string v3, "new timeline is incompatible with existing clips"

    invoke-static {v2, v0, v1, v3}, Lcgp;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 325
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 38
    check-cast p1, Lbhl;

    invoke-virtual {p0, p1}, Lbjb;->a(Lbhl;)V

    return-void
.end method

.method public a(J)Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 227
    const-string v0, "timestampUs"

    invoke-static {p1, p2, v0}, Lcec;->b(JLjava/lang/CharSequence;)J

    .line 229
    iput-boolean v3, p0, Lbjb;->l:Z

    .line 230
    iget-object v0, p0, Lbjb;->i:Ljava/util/concurrent/FutureTask;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lbjb;->i:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    .line 233
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lbjb;->i:Ljava/util/concurrent/FutureTask;

    .line 234
    iput v3, p0, Lbjb;->j:I

    :goto_0
    iget v0, p0, Lbjb;->j:I

    iget-object v1, p0, Lbjb;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 235
    iget-object v0, p0, Lbjb;->e:Ljava/util/List;

    iget v1, p0, Lbjb;->j:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 236
    iget-object v1, p0, Lbjb;->f:Ljava/util/List;

    iget v4, p0, Lbjb;->j:I

    .line 237
    invoke-interface {v1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v4, p1, v4

    .line 238
    invoke-virtual {v0}, Lbmd;->b()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-gez v1, :cond_1

    .line 240
    const-wide/16 v6, 0x0

    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iget-object v0, v0, Lbmd;->f:Lbmp;

    iget-wide v0, v0, Lbmp;->b:J

    add-long/2addr v0, v4

    iput-wide v0, p0, Lbjb;->k:J

    move v0, v2

    .line 245
    :goto_1
    return v0

    .line 234
    :cond_1
    iget v0, p0, Lbjb;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbjb;->j:I

    goto :goto_0

    :cond_2
    move v0, v3

    .line 245
    goto :goto_1
.end method

.method public b()Lbhl;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 137
    iget-boolean v0, p0, Lbjb;->l:Z

    if-nez v0, :cond_3

    .line 138
    iget-object v0, p0, Lbjb;->i:Ljava/util/concurrent/FutureTask;

    if-nez v0, :cond_1

    .line 140
    iget v0, p0, Lbjb;->j:I

    iget-object v2, p0, Lbjb;->e:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 141
    new-instance v0, Ljava/util/concurrent/FutureTask;

    iget-object v2, p0, Lbjb;->p:Ljava/util/concurrent/Callable;

    invoke-direct {v0, v2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    iput-object v0, p0, Lbjb;->i:Ljava/util/concurrent/FutureTask;

    .line 142
    iget-object v0, p0, Lbjb;->d:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lbjb;->i:Ljava/util/concurrent/FutureTask;

    invoke-interface {v0, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    :cond_0
    move-object v0, v1

    .line 198
    :goto_0
    return-object v0

    .line 145
    :cond_1
    iget-object v0, p0, Lbjb;->i:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->isDone()Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v1

    .line 147
    goto :goto_0

    .line 152
    :cond_2
    :try_start_0
    iget-object v0, p0, Lbjb;->i:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 164
    iget-object v2, p0, Lbjb;->h:Lbhl;

    invoke-static {v2}, Lcgl;->a(Lcgk;)V

    .line 166
    if-eqz v0, :cond_5

    .line 167
    new-instance v2, Lbhl;

    iget-object v3, p0, Lbjb;->b:Lbgf;

    invoke-virtual {v3, v0}, Lbgf;->a(Landroid/graphics/Bitmap;)Lbfw;

    move-result-object v0

    invoke-direct {v2, v0}, Lbhl;-><init>(Lbhb;)V

    iput-object v2, p0, Lbjb;->h:Lbhl;

    .line 168
    iget-object v0, p0, Lbjb;->h:Lbhl;

    iget-object v0, v0, Lbhl;->d:[F

    invoke-static {v0}, Lbqu;->b([F)V

    .line 176
    :goto_1
    iput-boolean v5, p0, Lbjb;->l:Z

    .line 177
    iput-object v1, p0, Lbjb;->i:Ljava/util/concurrent/FutureTask;

    .line 181
    :cond_3
    iget-object v0, p0, Lbjb;->h:Lbhl;

    iget-wide v2, p0, Lbjb;->k:J

    iput-wide v2, v0, Lbhl;->a:J

    .line 182
    iget-wide v0, p0, Lbjb;->k:J

    const-wide/32 v2, 0x80e8

    add-long/2addr v0, v2

    iput-wide v0, p0, Lbjb;->k:J

    .line 184
    iget-wide v2, p0, Lbjb;->k:J

    iget-object v0, p0, Lbjb;->e:Ljava/util/List;

    iget v1, p0, Lbjb;->j:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-object v0, v0, Lbmd;->f:Lbmp;

    iget-wide v0, v0, Lbmp;->c:J

    cmp-long v0, v2, v0

    if-ltz v0, :cond_6

    .line 185
    iget-object v0, p0, Lbjb;->h:Lbhl;

    iput-boolean v5, v0, Lbhl;->b:Z

    .line 186
    iput-boolean v4, p0, Lbjb;->l:Z

    .line 187
    iget v0, p0, Lbjb;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbjb;->j:I

    .line 188
    iget v0, p0, Lbjb;->j:I

    iget-object v1, p0, Lbjb;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 189
    iget-object v0, p0, Lbjb;->e:Ljava/util/List;

    iget v1, p0, Lbjb;->j:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-object v0, v0, Lbmd;->f:Lbmp;

    iget-wide v0, v0, Lbmp;->b:J

    iput-wide v0, p0, Lbjb;->k:J

    .line 197
    :cond_4
    :goto_2
    iget-object v0, p0, Lbjb;->h:Lbhl;

    invoke-virtual {v0}, Lbhl;->c()V

    .line 198
    iget-object v0, p0, Lbjb;->h:Lbhl;

    goto/16 :goto_0

    .line 154
    :catch_0
    move-exception v0

    sget-object v0, Lbjb;->a:Ljava/lang/String;

    move-object v0, v1

    .line 155
    goto/16 :goto_0

    .line 157
    :catch_1
    move-exception v0

    sget-object v0, Lbjb;->a:Ljava/lang/String;

    move-object v0, v1

    .line 158
    goto/16 :goto_0

    .line 171
    :cond_5
    new-instance v0, Lbhl;

    iget-object v2, p0, Lbjb;->b:Lbgf;

    invoke-virtual {v2}, Lbgf;->d()Lbfw;

    move-result-object v2

    invoke-direct {v0, v2}, Lbhl;-><init>(Lbhb;)V

    iput-object v0, p0, Lbjb;->h:Lbhl;

    .line 172
    iget-object v0, p0, Lbjb;->h:Lbhl;

    iget-object v0, v0, Lbhl;->d:[F

    invoke-static {v0}, Lbqu;->b([F)V

    goto :goto_1

    .line 192
    :cond_6
    iget-object v0, p0, Lbjb;->h:Lbhl;

    iput-boolean v4, v0, Lbhl;->b:Z

    goto :goto_2
.end method

.method public synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-virtual {p0}, Lbjb;->b()Lbhl;

    move-result-object v0

    return-object v0
.end method
