.class public final Layu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Z

.field b:I

.field c:I

.field d:Layj;

.field e:I

.field f:I

.field g:F

.field h:F

.field i:I

.field j:I

.field k:Landroid/graphics/Typeface;

.field l:F

.field m:Z

.field n:F

.field o:F

.field p:F

.field q:I

.field r:I

.field s:Layc;

.field t:I

.field u:J

.field v:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    const v1, 0x3c23d70a    # 0.01f

    .line 370
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 372
    iput v0, p0, Layu;->b:I

    .line 373
    iput v0, p0, Layu;->c:I

    .line 376
    const/16 v0, 0x11

    iput v0, p0, Layu;->f:I

    .line 380
    const/16 v0, 0x60

    iput v0, p0, Layu;->j:I

    .line 382
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Layu;->l:F

    .line 384
    const v0, 0x3d4ccccd    # 0.05f

    iput v0, p0, Layu;->n:F

    .line 385
    iput v1, p0, Layu;->o:F

    .line 386
    iput v1, p0, Layu;->p:F

    .line 387
    iput v2, p0, Layu;->q:I

    .line 388
    iput v2, p0, Layu;->r:I

    .line 390
    const/4 v0, 0x4

    iput v0, p0, Layu;->t:I

    return-void
.end method


# virtual methods
.method public a()Layt;
    .locals 1

    .prologue
    .line 505
    new-instance v0, Layt;

    invoke-direct {v0, p0}, Layt;-><init>(Layu;)V

    return-object v0
.end method

.method public a(F)Layu;
    .locals 0

    .prologue
    .line 410
    iput p1, p0, Layu;->g:F

    .line 411
    return-object p0
.end method

.method public a(I)Layu;
    .locals 0

    .prologue
    .line 400
    iput p1, p0, Layu;->e:I

    .line 401
    return-object p0
.end method

.method public a(J)Layu;
    .locals 1

    .prologue
    .line 495
    iput-wide p1, p0, Layu;->u:J

    .line 496
    return-object p0
.end method

.method public a(Landroid/graphics/Typeface;)Layu;
    .locals 0

    .prologue
    .line 430
    iput-object p1, p0, Layu;->k:Landroid/graphics/Typeface;

    .line 431
    return-object p0
.end method

.method public a(Layc;)Layu;
    .locals 0

    .prologue
    .line 485
    iput-object p1, p0, Layu;->s:Layc;

    .line 486
    return-object p0
.end method

.method public a(Layj;)Layu;
    .locals 0

    .prologue
    .line 395
    iput-object p1, p0, Layu;->d:Layj;

    .line 396
    return-object p0
.end method

.method public a(Z)Layu;
    .locals 0

    .prologue
    .line 440
    iput-boolean p1, p0, Layu;->a:Z

    .line 441
    return-object p0
.end method

.method public b(F)Layu;
    .locals 0

    .prologue
    .line 415
    iput p1, p0, Layu;->h:F

    .line 416
    return-object p0
.end method

.method public b(I)Layu;
    .locals 0

    .prologue
    .line 405
    iput p1, p0, Layu;->f:I

    .line 406
    return-object p0
.end method

.method public b(J)Layu;
    .locals 1

    .prologue
    .line 500
    iput-wide p1, p0, Layu;->v:J

    .line 501
    return-object p0
.end method

.method public b(Z)Layu;
    .locals 0

    .prologue
    .line 455
    iput-boolean p1, p0, Layu;->m:Z

    .line 456
    return-object p0
.end method

.method public c(F)Layu;
    .locals 0

    .prologue
    .line 460
    iput p1, p0, Layu;->n:F

    .line 461
    return-object p0
.end method

.method public c(I)Layu;
    .locals 0

    .prologue
    .line 420
    iput p1, p0, Layu;->i:I

    .line 421
    return-object p0
.end method

.method public d(F)Layu;
    .locals 0

    .prologue
    .line 470
    iput p1, p0, Layu;->o:F

    .line 471
    return-object p0
.end method

.method public d(I)Layu;
    .locals 0

    .prologue
    .line 425
    iput p1, p0, Layu;->j:I

    .line 426
    return-object p0
.end method

.method public e(F)Layu;
    .locals 0

    .prologue
    .line 475
    iput p1, p0, Layu;->p:F

    .line 476
    return-object p0
.end method

.method public e(I)Layu;
    .locals 0

    .prologue
    .line 445
    iput p1, p0, Layu;->c:I

    .line 446
    return-object p0
.end method

.method public f(I)Layu;
    .locals 0

    .prologue
    .line 450
    iput p1, p0, Layu;->b:I

    .line 451
    return-object p0
.end method

.method public g(I)Layu;
    .locals 0

    .prologue
    .line 465
    iput p1, p0, Layu;->q:I

    .line 466
    return-object p0
.end method

.method public h(I)Layu;
    .locals 0

    .prologue
    .line 480
    iput p1, p0, Layu;->r:I

    .line 481
    return-object p0
.end method
