.class public final Lccn;
.super Lkh;
.source "PG"


# instance fields
.field private synthetic a:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lccn;->a:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-direct {p0}, Lkh;-><init>()V

    return-void
.end method


# virtual methods
.method public b(I)V
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lccn;->a:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-static {v0, p1}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->a(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;I)I

    .line 75
    const/4 v0, 0x1

    if-ne p1, v0, :cond_3

    .line 76
    iget-object v0, p0, Lccn;->a:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->a(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;)Landroid/animation/Animator;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 77
    iget-object v0, p0, Lccn;->a:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->a(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;)Landroid/animation/Animator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/Animator;->cancel()V

    .line 79
    :cond_0
    iget-object v0, p0, Lccn;->a:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->b(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lccn;->a:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->b(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 82
    :cond_1
    iget-object v0, p0, Lccn;->a:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-static {v0}, Lcad;->a(Landroid/view/View;)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    .line 83
    iget-object v0, p0, Lccn;->a:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;)Lccs;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 84
    iget-object v0, p0, Lccn;->a:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;)Lccs;

    move-result-object v0

    invoke-interface {v0}, Lccs;->a()V

    .line 96
    :cond_2
    :goto_0
    return-void

    .line 86
    :cond_3
    if-nez p1, :cond_2

    .line 87
    iget-object v0, p0, Lccn;->a:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->c()I

    move-result v0

    iget-object v1, p0, Lccn;->a:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-static {v1}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->d(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;)I

    move-result v1

    if-eq v0, v1, :cond_4

    .line 90
    iget-object v0, p0, Lccn;->a:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->e(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;)V

    goto :goto_0

    .line 93
    :cond_4
    iget-object v0, p0, Lccn;->a:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->l()V

    goto :goto_0
.end method
