.class final Lbrk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Landroid/util/LongSparseArray;

.field private synthetic b:J


# direct methods
.method constructor <init>(Landroid/util/LongSparseArray;J)V
    .locals 0

    .prologue
    .line 289
    iput-object p1, p0, Lbrk;->a:Landroid/util/LongSparseArray;

    iput-wide p2, p0, Lbrk;->b:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/Long;Ljava/lang/Long;)I
    .locals 6

    .prologue
    .line 292
    iget-object v0, p0, Lbrk;->a:Landroid/util/LongSparseArray;

    .line 293
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrl;

    iget v0, v0, Lbrl;->a:F

    .line 294
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-wide v4, p0, Lbrk;->b:J

    invoke-static {v2, v3, v4, v5}, Lbrj;->a(JJ)F

    move-result v1

    sub-float v1, v0, v1

    iget-object v0, p0, Lbrk;->a:Landroid/util/LongSparseArray;

    .line 295
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrl;

    iget v0, v0, Lbrl;->a:F

    .line 296
    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-wide v4, p0, Lbrk;->b:J

    invoke-static {v2, v3, v4, v5}, Lbrj;->a(JJ)F

    move-result v2

    sub-float/2addr v0, v2

    .line 292
    invoke-static {v1, v0}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    return v0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 289
    check-cast p1, Ljava/lang/Long;

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p0, p1, p2}, Lbrk;->a(Ljava/lang/Long;Ljava/lang/Long;)I

    move-result v0

    return v0
.end method
