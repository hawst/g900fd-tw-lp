.class public final Ljym;
.super Llol;
.source "PG"

# interfaces
.implements Ljyv;
.implements Llgs;


# static fields
.field private static P:Landroid/graphics/drawable/Drawable;

.field private static Q:Z

.field private static an:[Ljava/lang/String;

.field private static ao:I


# instance fields
.field private N:Landroid/view/View;

.field private O:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

.field private R:Lcom/google/android/libraries/social/poll/views/PollTooltipView;

.field private S:Landroid/view/View;

.field private T:Landroid/view/View;

.field private U:Landroid/view/View;

.field private V:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private W:Landroid/widget/ImageView;

.field private X:Landroid/view/View;

.field private Y:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private Z:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private aa:Landroid/widget/EditText;

.field private ab:Landroid/widget/EditText;

.field private ac:Lcom/google/android/libraries/social/media/ui/MediaView;

.field private ad:Landroid/view/View;

.field private ae:Landroid/view/View;

.field private af:Landroid/widget/LinearLayout;

.field private ag:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private ah:Ljyu;

.field private ai:I

.field private aj:Z

.field private ak:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkjq;",
            ">;"
        }
    .end annotation
.end field

.field private al:Ljys;

.field private am:Lhoc;

.field private ap:Llgr;

.field private aq:I

.field private ar:Ljxv;

.field private as:Lhee;

.field private final aw:Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Llol;-><init>()V

    .line 106
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljym;->ag:Ljava/util/ArrayList;

    .line 113
    const/4 v0, -0x1

    iput v0, p0, Ljym;->ai:I

    .line 118
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljym;->aj:Z

    .line 120
    new-instance v0, Ljys;

    invoke-direct {v0, p0}, Ljys;-><init>(Ljym;)V

    iput-object v0, p0, Ljym;->al:Ljys;

    .line 388
    new-instance v0, Ljyo;

    invoke-direct {v0, p0}, Ljyo;-><init>(Ljym;)V

    iput-object v0, p0, Ljym;->aw:Landroid/view/View$OnClickListener;

    .line 799
    return-void
.end method

.method static synthetic a(Ljym;)Ljyu;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Ljym;->ah:Ljyu;

    return-object v0
.end method

.method private a(Lcom/google/android/libraries/social/media/ui/MediaView;Lizu;)V
    .locals 1

    .prologue
    .line 602
    invoke-virtual {p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->l()Lizu;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/libraries/social/media/ui/MediaView;->l()Lizu;

    move-result-object v0

    invoke-virtual {v0, p2}, Lizu;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 606
    :goto_0
    return-void

    .line 605
    :cond_0
    invoke-virtual {p1, p2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    goto :goto_0
.end method

.method static synthetic a(Ljym;I)V
    .locals 6

    .prologue
    .line 69
    int-to-double v0, p1

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    div-double/2addr v0, v2

    double-to-int v0, v0

    int-to-double v2, v0

    const-wide/high16 v4, 0x3ff4000000000000L    # 1.25

    mul-double/2addr v2, v4

    double-to-int v1, v2

    iget-object v2, p0, Ljym;->Y:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v2, p0, Ljym;->Y:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v2, p0, Ljym;->Y:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(II)V

    iget-object v2, p0, Ljym;->Z:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v0, v2, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v2, p0, Ljym;->Z:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iput v1, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v2, p0, Ljym;->Z:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(II)V

    int-to-double v0, p1

    const-wide/high16 v2, 0x3fe2000000000000L    # 0.5625

    mul-double/2addr v0, v2

    double-to-int v0, v0

    iput v0, p0, Ljym;->aq:I

    iget-object v0, p0, Ljym;->V:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v0, p0, Ljym;->V:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Ljym;->aq:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Ljym;->V:Lcom/google/android/libraries/social/media/ui/MediaView;

    iget v1, p0, Ljym;->aq:I

    invoke-virtual {v0, p1, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(II)V

    iget-object v0, p0, Ljym;->W:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Ljym;->aq:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Ljym;->ac:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v0, p0, Ljym;->ac:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v1, p0, Ljym;->aq:I

    iput v1, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Ljym;->ac:Lcom/google/android/libraries/social/media/ui/MediaView;

    iget v1, p0, Ljym;->aq:I

    invoke-virtual {v0, p1, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(II)V

    iget-object v0, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v0}, Ljyu;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Ljym;->ad:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget-object v0, p0, Ljym;->ah:Ljyu;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljyu;->a(I)Ljyw;

    move-result-object v0

    invoke-virtual {v0}, Ljyw;->a()Lizu;

    move-result-object v0

    if-nez v0, :cond_1

    sget v0, Ljym;->ao:I

    :goto_0
    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_0
    invoke-direct {p0}, Ljym;->d()V

    return-void

    :cond_1
    iget v0, p0, Ljym;->aq:I

    goto :goto_0
.end method

.method static synthetic a(Ljym;Lizu;Lizu;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 69
    const/4 v0, -0x2

    if-ne p3, v0, :cond_1

    iget-object v0, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v0, v2}, Ljyu;->b(Z)V

    iget-object v0, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v0, v1}, Ljyu;->a(I)Ljyw;

    move-result-object v0

    iget-object v1, p0, Ljym;->at:Llnl;

    invoke-virtual {v0, v1, p1, p2, v2}, Ljyw;->a(Landroid/content/Context;Lizu;Lizu;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ltz p3, :cond_0

    iget-object v0, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v0}, Ljyu;->a()I

    move-result v0

    if-ge p3, v0, :cond_0

    iget-object v0, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v0, v1}, Ljyu;->b(Z)V

    iget-object v0, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v0, p3}, Ljyu;->a(I)Ljyw;

    move-result-object v0

    iget-object v1, p0, Ljym;->at:Llnl;

    invoke-virtual {v0, v1, p1, p2, v2}, Ljyw;->a(Landroid/content/Context;Lizu;Lizu;Z)V

    goto :goto_0
.end method

.method static synthetic a(Ljym;Z)Z
    .locals 0

    .prologue
    .line 69
    iput-boolean p1, p0, Ljym;->aj:Z

    return p1
.end method

.method static synthetic b(Ljym;)I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Ljym;->ai:I

    return v0
.end method

.method private b(Landroid/view/View;ILhmn;I)Lcom/google/android/libraries/social/media/ui/MediaView;
    .locals 2

    .prologue
    .line 419
    invoke-virtual {p0, p1, p2, p3, p4}, Ljym;->a(Landroid/view/View;ILhmn;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 420
    sget-object v1, Ljym;->P:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->d(Landroid/graphics/drawable/Drawable;)V

    .line 421
    return-object v0
.end method

.method static synthetic b(Ljym;I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-direct {p0, p1}, Ljym;->d(I)Ljyw;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Ljym;->c(I)V

    :goto_1
    return-void

    :cond_0
    invoke-virtual {v0}, Ljyw;->a()Lizu;

    move-result-object v0

    goto :goto_0

    :cond_1
    iput p1, p0, Ljym;->ai:I

    sget-object v0, Ljym;->an:[Ljava/lang/String;

    invoke-static {v1, v0}, Llgr;->a(Ljava/lang/String;[Ljava/lang/String;)Llgr;

    move-result-object v0

    iput-object v0, p0, Ljym;->ap:Llgr;

    iget-object v0, p0, Ljym;->ap:Llgr;

    const/4 v1, 0x2

    invoke-virtual {v0, p0, v1}, Llgr;->a(Lu;I)V

    iget-object v0, p0, Ljym;->ap:Llgr;

    invoke-virtual {p0}, Ljym;->p()Lae;

    move-result-object v1

    const-string v2, "EditPhotoOptions"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic c(Ljym;I)I
    .locals 0

    .prologue
    .line 69
    iput p1, p0, Ljym;->ai:I

    return p1
.end method

.method private c(I)V
    .locals 4

    .prologue
    .line 642
    iput p1, p0, Ljym;->ai:I

    .line 643
    iget-object v0, p0, Ljym;->O:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljym;->O:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    .line 644
    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 645
    :goto_0
    new-instance v2, Ljca;

    iget-object v0, p0, Ljym;->at:Llnl;

    invoke-direct {v2, v0}, Ljca;-><init>(Landroid/content/Context;)V

    iget-object v0, p0, Ljym;->au:Llnh;

    const-class v3, Lhee;

    .line 646
    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    invoke-virtual {v2, v0}, Ljca;->a(I)Ljca;

    move-result-object v0

    .line 647
    invoke-virtual {v0, v1}, Ljca;->a(Ljava/lang/String;)Ljca;

    move-result-object v0

    .line 648
    invoke-virtual {v0}, Ljca;->a()Ljca;

    move-result-object v0

    .line 649
    invoke-virtual {v0}, Ljca;->b()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Ljym;->a(Landroid/content/Intent;I)V

    .line 650
    return-void

    .line 644
    :cond_0
    iget-object v0, p0, Ljym;->O:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_0
.end method

.method static synthetic c(Ljym;)Z
    .locals 1

    .prologue
    .line 69
    iget-boolean v0, p0, Ljym;->aj:Z

    return v0
.end method

.method static synthetic d(Ljym;)Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Ljym;->O:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    return-object v0
.end method

.method private d(I)Ljyw;
    .locals 2

    .prologue
    .line 712
    const/4 v0, -0x2

    if-ne p1, v0, :cond_0

    .line 713
    iget-object v0, p0, Ljym;->ah:Ljyu;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljyu;->a(I)Ljyw;

    move-result-object v0

    .line 717
    :goto_0
    return-object v0

    .line 714
    :cond_0
    if-ltz p1, :cond_1

    iget-object v0, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v0}, Ljyu;->a()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 715
    iget-object v0, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v0, p1}, Ljyu;->a(I)Ljyw;

    move-result-object v0

    goto :goto_0

    .line 717
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 510
    iget-object v0, p0, Ljym;->N:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 511
    iget-object v0, p0, Ljym;->S:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 512
    iget-object v0, p0, Ljym;->af:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 513
    iget-object v0, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v0}, Ljyu;->b()Z

    move-result v0

    if-nez v0, :cond_2

    .line 514
    iget-object v0, p0, Ljym;->O:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a()Ljava/util/List;

    move-result-object v1

    .line 515
    if-eqz v1, :cond_0

    iget-object v0, p0, Ljym;->ak:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 516
    iget-object v0, p0, Ljym;->au:Llnh;

    const-class v2, Lkjm;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkjm;

    iget-object v2, p0, Ljym;->ak:Ljava/util/List;

    invoke-interface {v0, v1, v2}, Lkjm;->a(Ljava/util/List;Ljava/util/List;)V

    .line 518
    :cond_0
    iget-object v0, p0, Ljym;->N:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 524
    :cond_1
    :goto_0
    return-void

    .line 519
    :cond_2
    iget-object v0, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v0}, Ljyu;->a()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 520
    iget-object v0, p0, Ljym;->S:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v0, v4}, Ljyu;->a(I)Ljyw;

    move-result-object v0

    invoke-virtual {v0}, Ljyw;->a()Lizu;

    move-result-object v0

    iget-object v1, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v1, v3}, Ljyu;->a(I)Ljyw;

    move-result-object v1

    invoke-virtual {v1}, Ljyw;->a()Lizu;

    move-result-object v1

    iget-object v2, p0, Ljym;->T:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Ljym;->U:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Ljym;->X:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Ljym;->aa:Landroid/widget/EditText;

    iget-object v5, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v5, v4}, Ljyu;->a(I)Ljyw;

    move-result-object v5

    invoke-virtual {v5}, Ljyw;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Ljym;->ab:Landroid/widget/EditText;

    iget-object v5, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v5, v3}, Ljyu;->a(I)Ljyw;

    move-result-object v3

    invoke-virtual {v3}, Ljyw;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    iget-object v2, p0, Ljym;->X:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v2, p0, Ljym;->Y:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-direct {p0, v2, v0}, Ljym;->a(Lcom/google/android/libraries/social/media/ui/MediaView;Lizu;)V

    iget-object v0, p0, Ljym;->Z:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-direct {p0, v0, v1}, Ljym;->a(Lcom/google/android/libraries/social/media/ui/MediaView;Lizu;)V

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_4

    iget-object v1, p0, Ljym;->U:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Ljym;->V:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-direct {p0, v1, v0}, Ljym;->a(Lcom/google/android/libraries/social/media/ui/MediaView;Lizu;)V

    iget-object v0, p0, Ljym;->ar:Ljxv;

    iget-object v1, p0, Ljym;->as:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-interface {v0, v1}, Ljxv;->c(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Ljym;->R:Lcom/google/android/libraries/social/poll/views/PollTooltipView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/poll/views/PollTooltipView;->a()V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Ljym;->T:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 522
    :cond_5
    iget-object v0, p0, Ljym;->af:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v0}, Ljyu;->j()Z

    move-result v0

    if-nez v0, :cond_9

    move v0, v3

    :goto_1
    iget-object v1, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v1, v4}, Ljyu;->a(I)Ljyw;

    move-result-object v1

    invoke-virtual {v1}, Ljyw;->a()Lizu;

    move-result-object v2

    iget-object v1, p0, Ljym;->ac:Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    iget-object v6, p0, Ljym;->ac:Lcom/google/android/libraries/social/media/ui/MediaView;

    if-eqz v0, :cond_a

    move v1, v4

    :goto_2
    invoke-virtual {v6, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    if-eqz v0, :cond_6

    iget-object v1, p0, Ljym;->ad:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v6

    if-nez v2, :cond_b

    sget v1, Ljym;->ao:I

    :goto_3
    iput v1, v6, Landroid/view/ViewGroup$LayoutParams;->height:I

    :cond_6
    iget-object v6, p0, Ljym;->ad:Landroid/view/View;

    if-eqz v0, :cond_c

    move v1, v4

    :goto_4
    invoke-virtual {v6, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Ljym;->ae:Landroid/view/View;

    if-eqz v0, :cond_d

    if-nez v2, :cond_d

    move v0, v4

    :goto_5
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    move v6, v4

    :goto_6
    iget-object v0, p0, Ljym;->ag:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v6, v0, :cond_1

    iget-object v0, p0, Ljym;->ag:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v1, v6}, Ljyu;->a(I)Ljyw;

    move-result-object v8

    if-nez v8, :cond_e

    move v1, v5

    :goto_7
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    if-eqz v8, :cond_8

    const v1, 0x7f1004eb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    invoke-virtual {v8}, Ljyw;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f1004e8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    const v2, 0x7f1004e9

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/media/ui/MediaView;

    const v7, 0x7f1004ea

    invoke-virtual {v0, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iget-object v7, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v7}, Ljyu;->j()Z

    move-result v7

    if-nez v7, :cond_7

    iget-object v7, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v7}, Ljyu;->i()Z

    move-result v7

    if-nez v7, :cond_f

    :cond_7
    move v7, v3

    :goto_8
    if-eqz v7, :cond_11

    invoke-virtual {v8}, Ljyw;->a()Lizu;

    move-result-object v7

    if-eqz v7, :cond_10

    invoke-virtual {v8}, Ljyw;->a()Lizu;

    move-result-object v7

    invoke-virtual {v2, v7}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    invoke-virtual {v2, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_9
    invoke-virtual {v1, v4}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_8
    :goto_a
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_6

    :cond_9
    move v0, v4

    goto/16 :goto_1

    :cond_a
    move v1, v5

    goto/16 :goto_2

    :cond_b
    iget v1, p0, Ljym;->aq:I

    goto/16 :goto_3

    :cond_c
    move v1, v5

    goto/16 :goto_4

    :cond_d
    move v0, v5

    goto/16 :goto_5

    :cond_e
    move v1, v4

    goto :goto_7

    :cond_f
    move v7, v4

    goto :goto_8

    :cond_10
    invoke-virtual {v2, v5}, Lcom/google/android/libraries/social/media/ui/MediaView;->setVisibility(I)V

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_9

    :cond_11
    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_a
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 15

    .prologue
    .line 165
    iget-object v2, p0, Ljym;->at:Llnl;

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 167
    invoke-virtual {p0}, Ljym;->n()Lz;

    move-result-object v2

    sget-boolean v3, Ljym;->Q:Z

    if-nez v3, :cond_0

    invoke-virtual {v2}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    sput-object v3, Ljym;->an:[Ljava/lang/String;

    const/4 v5, 0x0

    const v6, 0x7f0a059b

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    sget-object v3, Ljym;->an:[Ljava/lang/String;

    const/4 v5, 0x1

    const v6, 0x7f0a059a

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    const v3, 0x7f0d00a6

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    float-to-int v3, v3

    sput v3, Ljym;->ao:I

    const v3, 0x7f02049c

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    sput-object v2, Ljym;->P:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x1

    sput-boolean v2, Ljym;->Q:Z

    .line 168
    :cond_0
    const v2, 0x7f04019c

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v4, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 171
    const v2, 0x7f10036b

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Ljym;->N:Landroid/view/View;

    .line 172
    iget-object v2, p0, Ljym;->N:Landroid/view/View;

    new-instance v3, Lhmk;

    sget-object v6, Lonf;->g:Lhmn;

    invoke-direct {v3, v6}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v2, v3}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 175
    const v2, 0x7f1004e2

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    iput-object v2, p0, Ljym;->O:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    .line 176
    iget-object v2, p0, Ljym;->O:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    new-instance v3, Lhmk;

    sget-object v6, Lonf;->i:Lhmn;

    invoke-direct {v3, v6}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v2, v3}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 178
    iget-object v2, p0, Ljym;->O:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    new-instance v3, Ljyr;

    invoke-direct {v3}, Ljyr;-><init>()V

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 180
    iget-object v2, p0, Ljym;->O:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    new-instance v3, Ljyt;

    invoke-direct {v3, p0}, Ljyt;-><init>(Ljym;)V

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 181
    iget-object v3, p0, Ljym;->O:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    iget-object v2, p0, Ljym;->au:Llnh;

    const-class v6, Lhee;

    invoke-virtual {v2, v6}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v6

    const/4 v7, 0x0

    iget-object v2, p0, Ljym;->au:Llnh;

    const-class v8, Lkjm;

    .line 182
    invoke-virtual {v2, v8}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkjm;

    .line 181
    invoke-virtual {v3, p0, v6, v7, v2}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a(Lu;ILjava/lang/String;Lkjm;)V

    .line 183
    if-nez p3, :cond_4

    .line 184
    invoke-virtual {p0}, Ljym;->k()Landroid/os/Bundle;

    move-result-object v2

    .line 185
    if-eqz v2, :cond_2

    .line 186
    const-string v3, "editable_post_text"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 187
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 188
    new-instance v6, Landroid/text/SpannableStringBuilder;

    invoke-direct {v6, v3}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 189
    const-string v3, "editable_post_text_url_spans"

    .line 190
    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v7

    .line 191
    if-eqz v7, :cond_1

    .line 192
    const/4 v2, 0x0

    move v3, v2

    :goto_0
    array-length v2, v7

    if-ge v3, v2, :cond_1

    .line 193
    aget-object v2, v7, v3

    check-cast v2, Lkiz;

    .line 196
    invoke-virtual {v2}, Lkiz;->a()Landroid/text/style/URLSpan;

    move-result-object v8

    invoke-virtual {v2}, Lkiz;->b()I

    move-result v9

    .line 197
    invoke-virtual {v2}, Lkiz;->c()I

    move-result v2

    const/16 v10, 0x21

    .line 196
    invoke-virtual {v6, v8, v9, v2, v10}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 192
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 200
    :cond_1
    iget-object v2, p0, Ljym;->O:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v2, v6}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->setText(Ljava/lang/CharSequence;)V

    .line 201
    iget-object v2, p0, Ljym;->ah:Ljyu;

    iget-object v3, p0, Ljym;->O:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v3}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->d()Ljava/lang/String;

    move-result-object v3

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v6}, Ljyu;->a(Ljava/lang/String;Z)V

    .line 202
    iget-object v2, p0, Ljym;->O:Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    invoke-virtual {v2}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->a()Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Ljym;->ak:Ljava/util/List;

    .line 210
    :cond_2
    :goto_1
    const v2, 0x7f1004f4

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Ljym;->S:Landroid/view/View;

    .line 213
    const v2, 0x7f1004f5

    sget-object v3, Lonf;->a:Lhmn;

    invoke-virtual {p0, v5, v2, v3}, Ljym;->a(Landroid/view/View;ILhmn;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Ljym;->T:Landroid/view/View;

    .line 216
    const v2, 0x7f1004f6

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Ljym;->U:Landroid/view/View;

    .line 220
    sget-object v2, Lonf;->e:Lhmn;

    const v3, 0x7f1004f7

    const/4 v6, -0x1

    invoke-direct {p0, v5, v3, v2, v6}, Ljym;->b(Landroid/view/View;ILhmn;I)Lcom/google/android/libraries/social/media/ui/MediaView;

    move-result-object v2

    iput-object v2, p0, Ljym;->V:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 223
    const v2, 0x7f1004f8

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Ljym;->W:Landroid/widget/ImageView;

    .line 227
    const v2, 0x7f1004fa

    sget-object v3, Lonf;->a:Lhmn;

    invoke-virtual {p0, v5, v2, v3}, Ljym;->a(Landroid/view/View;ILhmn;)Landroid/view/View;

    .line 231
    const v2, 0x7f1004fb

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Ljym;->X:Landroid/view/View;

    .line 234
    const v2, 0x7f1004fc

    sget-object v3, Lonf;->f:Lhmn;

    const/4 v6, 0x0

    invoke-direct {p0, v5, v2, v3, v6}, Ljym;->b(Landroid/view/View;ILhmn;I)Lcom/google/android/libraries/social/media/ui/MediaView;

    move-result-object v2

    iput-object v2, p0, Ljym;->Y:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 236
    const v2, 0x7f1004fd

    sget-object v3, Lonf;->f:Lhmn;

    const/4 v6, 0x1

    invoke-direct {p0, v5, v2, v3, v6}, Ljym;->b(Landroid/view/View;ILhmn;I)Lcom/google/android/libraries/social/media/ui/MediaView;

    move-result-object v2

    iput-object v2, p0, Ljym;->Z:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 240
    const v2, 0x7f1004fe

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Ljym;->aa:Landroid/widget/EditText;

    .line 241
    iget-object v2, p0, Ljym;->aa:Landroid/widget/EditText;

    new-instance v3, Lkqz;

    sget-object v6, Lonf;->h:Lhmn;

    const/4 v7, 0x0

    invoke-direct {v3, v6, v7}, Lkqz;-><init>(Lhmn;I)V

    invoke-static {v2, v3}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 243
    iget-object v2, p0, Ljym;->aa:Landroid/widget/EditText;

    new-instance v3, Ljyt;

    const/4 v6, 0x0

    invoke-direct {v3, p0, v6}, Ljyt;-><init>(Ljym;I)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 244
    iget-object v2, p0, Ljym;->aa:Landroid/widget/EditText;

    new-instance v3, Ljyr;

    invoke-direct {v3}, Ljyr;-><init>()V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 246
    const v2, 0x7f1004ff

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Ljym;->ab:Landroid/widget/EditText;

    .line 247
    iget-object v2, p0, Ljym;->ab:Landroid/widget/EditText;

    new-instance v3, Lkqz;

    sget-object v6, Lonf;->h:Lhmn;

    const/4 v7, 0x1

    invoke-direct {v3, v6, v7}, Lkqz;-><init>(Lhmn;I)V

    invoke-static {v2, v3}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 249
    iget-object v2, p0, Ljym;->ab:Landroid/widget/EditText;

    new-instance v3, Ljyt;

    const/4 v6, 0x1

    invoke-direct {v3, p0, v6}, Ljyt;-><init>(Ljym;I)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 250
    iget-object v2, p0, Ljym;->ab:Landroid/widget/EditText;

    new-instance v3, Ljyr;

    invoke-direct {v3}, Ljyr;-><init>()V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 252
    const v2, 0x7f1004e3

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Ljym;->af:Landroid/widget/LinearLayout;

    .line 256
    const v2, 0x7f1004e5

    sget-object v3, Lonf;->f:Lhmn;

    invoke-virtual {p0, v5, v2, v3}, Ljym;->a(Landroid/view/View;ILhmn;)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/media/ui/MediaView;

    iput-object v2, p0, Ljym;->ac:Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 259
    const v2, 0x7f1004e4

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Ljym;->ad:Landroid/view/View;

    .line 260
    const v2, 0x7f1004e6

    sget-object v3, Lonf;->f:Lhmn;

    invoke-virtual {p0, v5, v2, v3}, Ljym;->a(Landroid/view/View;ILhmn;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Ljym;->ae:Landroid/view/View;

    .line 264
    iget-object v2, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v2}, Ljyu;->h()J

    move-result-wide v6

    .line 265
    const/4 v2, 0x0

    move v3, v2

    :goto_2
    int-to-long v8, v3

    cmp-long v2, v8, v6

    if-gez v2, :cond_5

    .line 268
    const v2, 0x7f04019e

    iget-object v8, p0, Ljym;->af:Landroid/widget/LinearLayout;

    const/4 v9, 0x0

    invoke-virtual {v4, v2, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v8

    .line 270
    iget-object v2, p0, Ljym;->ag:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v9

    const v2, 0x7f1004ec

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    new-instance v10, Lkqz;

    sget-object v11, Lonf;->k:Lhmn;

    invoke-direct {v10, v11, v9}, Lkqz;-><init>(Lhmn;I)V

    invoke-static {v2, v10}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    new-instance v10, Lhmi;

    new-instance v11, Ljyp;

    invoke-direct {v11, p0, v9}, Ljyp;-><init>(Ljym;I)V

    invoke-direct {v10, v11}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v2, v10}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f1004eb

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    new-instance v10, Lkqz;

    sget-object v11, Lonf;->h:Lhmn;

    invoke-direct {v10, v11, v9}, Lkqz;-><init>(Lhmn;I)V

    invoke-static {v2, v10}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    new-instance v10, Ljyt;

    invoke-direct {v10, p0, v9}, Ljyt;-><init>(Ljym;I)V

    invoke-virtual {v2, v10}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {p0}, Ljym;->o()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f0a0175

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    add-int/lit8 v14, v9, 0x1

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    aput-object v14, v12, v13

    invoke-virtual {v10, v11, v12}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    new-instance v10, Ljyr;

    invoke-direct {v10}, Ljyr;-><init>()V

    invoke-virtual {v2, v10}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    new-instance v10, Lhmi;

    new-instance v2, Ljyq;

    invoke-direct {v2, p0, v9}, Ljyq;-><init>(Ljym;I)V

    invoke-direct {v10, v2}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f1004e9

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/media/ui/MediaView;

    new-instance v11, Lkqz;

    sget-object v12, Lonf;->f:Lhmn;

    invoke-direct {v11, v12, v9}, Lkqz;-><init>(Lhmn;I)V

    invoke-static {v2, v11}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    invoke-virtual {v2, v10}, Lcom/google/android/libraries/social/media/ui/MediaView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f1004e8

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v11, Lkqz;

    sget-object v12, Lonf;->c:Lhmn;

    invoke-direct {v11, v12, v9}, Lkqz;-><init>(Lhmn;I)V

    invoke-static {v2, v11}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    invoke-virtual {v2, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 271
    int-to-long v10, v3

    const-wide/16 v12, 0x1

    sub-long v12, v6, v12

    cmp-long v2, v10, v12

    if-nez v2, :cond_3

    .line 272
    const v2, 0x7f1004eb

    invoke-virtual {v8, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    .line 273
    const/4 v9, 0x6

    invoke-virtual {v2, v9}, Landroid/widget/EditText;->setImeOptions(I)V

    .line 275
    :cond_3
    iget-object v2, p0, Ljym;->ag:Ljava/util/ArrayList;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 276
    iget-object v2, p0, Ljym;->af:Landroid/widget/LinearLayout;

    invoke-virtual {v2, v8}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 265
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_2

    .line 206
    :cond_4
    const-string v2, "ORIGINAL_MENTIONS"

    move-object/from16 v0, p3

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    iput-object v2, p0, Ljym;->ak:Ljava/util/List;

    goto/16 :goto_1

    .line 278
    :cond_5
    iget-object v2, p0, Ljym;->ar:Ljxv;

    iget-object v3, p0, Ljym;->as:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-interface {v2, v3}, Ljxv;->c(I)Z

    move-result v2

    if-nez v2, :cond_6

    .line 279
    const v2, 0x7f1004f9

    invoke-virtual {v5, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/poll/views/PollTooltipView;

    iput-object v2, p0, Ljym;->R:Lcom/google/android/libraries/social/poll/views/PollTooltipView;

    .line 280
    iget-object v2, p0, Ljym;->R:Lcom/google/android/libraries/social/poll/views/PollTooltipView;

    const v3, 0x7f0a018f

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/social/poll/views/PollTooltipView;->a(I)V

    .line 284
    :cond_6
    iget-object v2, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v2, p0}, Ljyu;->a(Ljyv;)V

    .line 286
    return-object v5
.end method

.method a(Landroid/view/View;ILhmn;)Landroid/view/View;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            "I",
            "Lhmn;",
            ")TT;"
        }
    .end annotation

    .prologue
    .line 434
    const/4 v0, -0x1

    invoke-virtual {p0, p1, p2, p3, v0}, Ljym;->a(Landroid/view/View;ILhmn;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method a(Landroid/view/View;ILhmn;I)Landroid/view/View;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Landroid/view/View;",
            ">(",
            "Landroid/view/View;",
            "I",
            "Lhmn;",
            "I)TT;"
        }
    .end annotation

    .prologue
    .line 449
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 451
    const/4 v0, -0x1

    if-ne p4, v0, :cond_0

    new-instance v0, Lhmk;

    invoke-direct {v0, p3}, Lhmk;-><init>(Lhmn;)V

    :goto_0
    invoke-static {v1, v0}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 454
    new-instance v0, Lhmi;

    iget-object v2, p0, Ljym;->aw:Landroid/view/View$OnClickListener;

    invoke-direct {v0, v2}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 455
    return-object v1

    .line 451
    :cond_0
    new-instance v0, Lkqz;

    invoke-direct {v0, p3, p4}, Lkqz;-><init>(Lhmn;I)V

    goto :goto_0
.end method

.method public a()V
    .locals 0

    .prologue
    .line 844
    invoke-direct {p0}, Ljym;->d()V

    .line 845
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 835
    invoke-direct {p0}, Ljym;->d()V

    .line 836
    iget-object v0, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v0}, Ljyu;->a()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 837
    iget-object v0, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v0}, Ljyu;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    .line 838
    iget-object v1, p0, Ljym;->ag:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    const v1, 0x7f1004eb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    invoke-static {v0}, Llsn;->a(Landroid/view/View;)V

    .line 840
    :cond_0
    return-void
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 786
    invoke-super {p0, p1, p2, p3}, Llol;->a(IILandroid/content/Intent;)V

    .line 787
    iget-object v0, p0, Ljym;->ap:Llgr;

    if-eqz v0, :cond_0

    .line 788
    iget-object v0, p0, Ljym;->ap:Llgr;

    invoke-virtual {v0}, Llgr;->a()V

    .line 790
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 791
    if-eq p2, v1, :cond_2

    iput v1, p0, Ljym;->ai:I

    .line 793
    :cond_1
    :goto_0
    return-void

    .line 791
    :cond_2
    const-string v0, "shareables"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizr;

    invoke-interface {v0}, Lizr;->f()Lizu;

    move-result-object v0

    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_1

    iget-object v2, p0, Ljym;->am:Lhoc;

    new-instance v3, Lkau;

    iget-object v4, p0, Ljym;->at:Llnl;

    iget-object v0, p0, Ljym;->au:Llnh;

    const-class v5, Lhee;

    invoke-virtual {v0, v5}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    invoke-direct {v3, v4, v0, v1}, Lkau;-><init>(Landroid/content/Context;ILizu;)V

    invoke-virtual {v2, v3}, Lhoc;->b(Lhny;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Ljym;->au:Llnh;

    const-class v1, Ljbt;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljbt;

    invoke-interface {v0, p3}, Ljbt;->a(Landroid/content/Intent;)Lizu;

    move-result-object v0

    move-object v1, v0

    goto :goto_1
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 753
    if-nez p1, :cond_2

    .line 754
    sget-object v0, Lonf;->d:Lhmn;

    .line 755
    iget v1, p0, Ljym;->ai:I

    invoke-direct {p0, v1}, Ljym;->c(I)V

    .line 776
    :cond_0
    :goto_0
    iget-object v1, p0, Ljym;->at:Llnl;

    const/4 v2, 0x4

    new-instance v3, Lhml;

    invoke-direct {v3}, Lhml;-><init>()V

    new-instance v4, Lhmk;

    invoke-direct {v4, v0}, Lhmk;-><init>(Lhmn;)V

    .line 777
    invoke-virtual {v3, v4}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v0

    iget-object v3, p0, Ljym;->at:Llnl;

    invoke-virtual {v0, v3}, Lhml;->a(Landroid/content/Context;)Lhml;

    move-result-object v0

    .line 776
    invoke-static {v1, v2, v0}, Lhly;->a(Landroid/content/Context;ILhml;)V

    .line 778
    :cond_1
    return-void

    .line 757
    :cond_2
    sget-object v0, Lonf;->j:Lhmn;

    .line 758
    iget v1, p0, Ljym;->ai:I

    invoke-direct {p0, v1}, Ljym;->d(I)Ljyw;

    move-result-object v1

    .line 759
    if-eqz v1, :cond_1

    .line 762
    iget-object v2, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v2}, Ljyu;->a()I

    move-result v2

    if-ne v2, v4, :cond_3

    .line 763
    iget-object v2, p0, Ljym;->ah:Ljyu;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljyu;->b(Z)V

    .line 765
    :cond_3
    iget-object v2, p0, Ljym;->at:Llnl;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Ljyw;->a(Landroid/content/Context;Lizu;)V

    .line 768
    iget v1, p0, Ljym;->ai:I

    if-nez v1, :cond_0

    iget-object v1, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v1}, Ljyu;->a()I

    move-result v1

    if-ne v1, v4, :cond_0

    .line 769
    iget-object v1, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v1}, Ljyu;->k()V

    .line 770
    iget-object v1, p0, Ljym;->ah:Ljyu;

    invoke-virtual {v1}, Ljyu;->m()V

    goto :goto_0
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 781
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 742
    return-void
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 331
    invoke-super {p0}, Llol;->aO_()V

    .line 340
    invoke-virtual {p0}, Ljym;->x()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljym;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    if-lez v0, :cond_0

    .line 341
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljym;->aj:Z

    .line 343
    :cond_0
    return-void
.end method

.method public b()V
    .locals 0

    .prologue
    .line 850
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 745
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 854
    invoke-direct {p0}, Ljym;->d()V

    .line 855
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 148
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 150
    iget-object v0, p0, Ljym;->au:Llnh;

    const-class v1, Lkjj;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkjj;

    const-class v1, Ljzz;

    invoke-interface {v0, v1}, Lkjj;->a(Ljava/lang/Class;)Lkji;

    move-result-object v0

    check-cast v0, Ljzz;

    invoke-virtual {v0}, Ljzz;->j()Ljyu;

    move-result-object v0

    iput-object v0, p0, Ljym;->ah:Ljyu;

    .line 153
    if-eqz p1, :cond_0

    .line 154
    const-string v0, "INDEX_PENDING_PHOTO"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ljym;->ai:I

    .line 156
    :cond_0
    iget-object v0, p0, Ljym;->at:Llnl;

    const-class v1, Lhoc;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Ljym;->am:Lhoc;

    .line 157
    iget-object v0, p0, Ljym;->am:Lhoc;

    iget-object v1, p0, Ljym;->al:Ljys;

    invoke-virtual {v0, v1}, Lhoc;->a(Lhob;)Lhoc;

    .line 158
    iget-object v0, p0, Ljym;->au:Llnh;

    const-class v1, Ljxv;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxv;

    iput-object v0, p0, Ljym;->ar:Ljxv;

    .line 159
    iget-object v0, p0, Ljym;->au:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Ljym;->as:Lhee;

    .line 160
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 748
    return-void
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 291
    invoke-super {p0, p1}, Llol;->d(Landroid/os/Bundle;)V

    .line 292
    invoke-virtual {p0}, Ljym;->x()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 319
    :cond_0
    :goto_0
    return-void

    .line 295
    :cond_1
    invoke-virtual {p0}, Ljym;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 296
    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 303
    new-instance v1, Ljyn;

    invoke-direct {v1, p0}, Ljyn;-><init>(Ljym;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 323
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 324
    const-string v0, "INDEX_PENDING_PHOTO"

    iget v1, p0, Ljym;->ai:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 325
    const-string v1, "ORIGINAL_MENTIONS"

    iget-object v0, p0, Ljym;->ak:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 327
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 347
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljym;->aj:Z

    .line 348
    invoke-super {p0}, Llol;->z()V

    .line 349
    return-void
.end method
