.class final Lgeq;
.super Ljava/lang/Object;

# interfaces
.implements Lggg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lggg",
        "<",
        "Lcom/google/android/gms/common/api/Status;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:J

.field private synthetic b:Lgep;


# direct methods
.method constructor <init>(Lgep;J)V
    .locals 0

    iput-object p1, p0, Lgeq;->b:Lgep;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p2, p0, Lgeq;->a:J

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/common/api/Status;)V
    .locals 4

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lgeq;->b:Lgep;

    iget-object v0, v0, Lgep;->a:Lgeh;

    iget-object v0, v0, Lgeh;->b:Lgkr;

    iget-wide v2, p0, Lgeq;->a:J

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()I

    move-result v1

    invoke-virtual {v0, v2, v3, v1}, Lgkr;->a(JI)V

    :cond_0
    return-void
.end method

.method public synthetic a(Lggf;)V
    .locals 0

    check-cast p1, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, p1}, Lgeq;->a(Lcom/google/android/gms/common/api/Status;)V

    return-void
.end method
