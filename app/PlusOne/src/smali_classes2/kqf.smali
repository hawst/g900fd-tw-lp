.class public Lkqf;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Lhmk;

.field private static final b:Lhmk;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lhei;

.field private e:Ljava/lang/String;

.field private f:I

.field private g:Z

.field private h:Z

.field private i:I

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lhmk;

    sget-object v1, Lonn;->b:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    sput-object v0, Lkqf;->a:Lhmk;

    .line 28
    new-instance v0, Lhmk;

    sget-object v1, Lonn;->a:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    sput-object v0, Lkqf;->b:Lhmk;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkqf;->j:Ljava/util/List;

    .line 61
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lkqf;->c:Landroid/content/Context;

    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lkqf;->i:I

    .line 63
    iget-object v0, p0, Lkqf;->c:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lkqf;->d:Lhei;

    .line 64
    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 154
    const-string v0, "ActivityGround"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lkqf;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 156
    invoke-direct {p0, p1}, Lkqf;->b(Ljava/lang/String;)V

    .line 158
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 5

    .prologue
    .line 124
    iget-boolean v0, p0, Lkqf;->g:Z

    if-eq p1, v0, :cond_0

    .line 125
    if-eqz p1, :cond_1

    const-string v0, "foreground"

    :goto_0
    invoke-direct {p0, v0}, Lkqf;->b(Ljava/lang/String;)V

    .line 127
    iget-object v1, p0, Lkqf;->e:Ljava/lang/String;

    if-eqz p1, :cond_2

    sget-object v0, Lkqf;->a:Lhmk;

    :goto_1
    new-instance v2, Lhly;

    const/16 v3, 0x19

    new-instance v4, Lhml;

    invoke-direct {v4}, Lhml;-><init>()V

    invoke-virtual {v4, v0}, Lhml;->a(Lhmk;)Lhml;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Lhly;-><init>(ILhml;)V

    invoke-virtual {v2, v1}, Lhly;->a(Ljava/lang/String;)Lhly;

    move-result-object v0

    iget-object v1, p0, Lkqf;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lhly;->a(Landroid/content/Context;)V

    .line 130
    iput-boolean p1, p0, Lkqf;->g:Z

    .line 132
    :cond_0
    return-void

    .line 125
    :cond_1
    const-string v0, "background"

    goto :goto_0

    .line 127
    :cond_2
    sget-object v0, Lkqf;->b:Lhmk;

    goto :goto_1
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 161
    const-string v0, "ActivityGround"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lkqf;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    :cond_0
    return-void
.end method

.method private c()V
    .locals 1

    .prologue
    .line 120
    iget v0, p0, Lkqf;->f:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lkqf;->a(Z)V

    .line 121
    return-void

    .line 120
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Z
    .locals 3

    .prologue
    .line 142
    const/4 v0, 0x0

    .line 143
    iget-object v1, p0, Lkqf;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    .line 144
    iget v2, p0, Lkqf;->i:I

    if-eq v2, v1, :cond_0

    .line 145
    const-string v0, "rotation"

    invoke-direct {p0, v0}, Lkqf;->b(Ljava/lang/String;)V

    .line 147
    const/4 v0, 0x1

    .line 148
    iput v1, p0, Lkqf;->i:I

    .line 150
    :cond_0
    return v0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 167
    const-string v0, "ActivityGround"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v0, p0, Lkqf;->j:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 169
    const/4 v0, 0x1

    move v1, v0

    :goto_0
    iget-object v0, p0, Lkqf;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 170
    iget-object v0, p0, Lkqf;->j:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 171
    const-string v3, " -> "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 169
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 174
    :cond_0
    return-void
.end method


# virtual methods
.method a()V
    .locals 3

    .prologue
    .line 68
    const-string v0, "visible"

    invoke-direct {p0, v0}, Lkqf;->a(Ljava/lang/String;)V

    .line 70
    iget-boolean v0, p0, Lkqf;->h:Z

    if-nez v0, :cond_0

    .line 71
    iget v0, p0, Lkqf;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lkqf;->f:I

    .line 72
    iget v0, p0, Lkqf;->f:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x11

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "count="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lkqf;->b(Ljava/lang/String;)V

    .line 73
    invoke-direct {p0}, Lkqf;->c()V

    .line 76
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkqf;->h:Z

    .line 77
    invoke-direct {p0}, Lkqf;->d()Z

    .line 79
    invoke-direct {p0}, Lkqf;->e()V

    .line 80
    return-void
.end method

.method a(I)V
    .locals 2

    .prologue
    .line 98
    const-string v0, "notifyAccountChanged"

    invoke-direct {p0, v0}, Lkqf;->a(Ljava/lang/String;)V

    .line 99
    iget-object v0, p0, Lkqf;->d:Lhei;

    invoke-interface {v0, p1}, Lhei;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lkqf;->d:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lkqf;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "account changed"

    invoke-direct {p0, v1}, Lkqf;->b(Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lkqf;->a(Z)V

    iput-object v0, p0, Lkqf;->e:Ljava/lang/String;

    invoke-direct {p0}, Lkqf;->c()V

    .line 100
    :cond_0
    invoke-direct {p0}, Lkqf;->e()V

    .line 101
    return-void

    .line 99
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()V
    .locals 3

    .prologue
    .line 84
    const-string v0, "invisible"

    invoke-direct {p0, v0}, Lkqf;->a(Ljava/lang/String;)V

    .line 86
    invoke-direct {p0}, Lkqf;->d()Z

    move-result v0

    iput-boolean v0, p0, Lkqf;->h:Z

    .line 87
    iget-boolean v0, p0, Lkqf;->h:Z

    if-nez v0, :cond_0

    .line 88
    iget v0, p0, Lkqf;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lkqf;->f:I

    .line 89
    iget v0, p0, Lkqf;->f:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x11

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "count="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lkqf;->b(Ljava/lang/String;)V

    .line 90
    invoke-direct {p0}, Lkqf;->c()V

    .line 93
    :cond_0
    invoke-direct {p0}, Lkqf;->e()V

    .line 94
    return-void
.end method
