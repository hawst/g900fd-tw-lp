.class final Lgzg;
.super Ljava/lang/Object;

# interfaces
.implements Lgfs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lgfs",
        "<",
        "Lgny;",
        "Lgzh;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const v0, 0x7fffffff

    return v0
.end method

.method public bridge synthetic a(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/lang/Object;Lggb;Lggc;)Lgfr;
    .locals 7

    move-object v4, p4

    check-cast v4, Lgzh;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lgzg;->a(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Lgzh;Lggb;Lggc;)Lgny;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Lgzh;Lggb;Lggc;)Lgny;
    .locals 7

    const-string v0, "Must provide valid PeopleOptions!"

    invoke-static {p4, v0}, Lgih;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lgny;

    iget v1, p4, Lgzh;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p3}, Lcom/google/android/gms/common/internal/ClientSettings;->c()Ljava/lang/String;

    move-result-object v6

    move-object v1, p1

    move-object v2, p2

    move-object v3, p5

    move-object v4, p6

    invoke-direct/range {v0 .. v6}, Lgny;-><init>(Landroid/content/Context;Landroid/os/Looper;Lggb;Lggc;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
