.class public final Lbzv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbzx;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:[F

.field private static final c:[F

.field private static final d:[F


# instance fields
.field private final e:Lama;

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lont;",
            ">;"
        }
    .end annotation
.end field

.field private g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Look;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 42
    const-class v0, Lbzv;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbzv;->a:Ljava/lang/String;

    .line 46
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lbzv;->b:[F

    .line 48
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    sput-object v0, Lbzv;->c:[F

    .line 49
    new-array v0, v1, [F

    fill-array-data v0, :array_2

    sput-object v0, Lbzv;->d:[F

    return-void

    .line 46
    :array_0
    .array-data 4
        0x3f200000    # 0.625f
        0x3f800000    # 1.0f
    .end array-data

    .line 48
    :array_1
    .array-data 4
        0x3f44dd2f    # 0.769f
        0x3f200000    # 0.625f
        0x3f800000    # 1.0f
    .end array-data

    .line 49
    :array_2
    .array-data 4
        0x3e800000    # 0.25f
        0x3e800000    # 0.25f
        0x3f000000    # 0.5f
    .end array-data
.end method

.method public constructor <init>(Lama;)V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbzv;->f:Ljava/util/List;

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbzv;->g:Ljava/util/List;

    .line 58
    const-string v0, "clipIdentifierGenerator"

    .line 59
    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lama;

    iput-object v0, p0, Lbzv;->e:Lama;

    .line 60
    return-void
.end method

.method private a(Lonr;Look;)Lbmd;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 249
    iget v0, p1, Lonr;->b:I

    const-string v1, "cloud clip type"

    const/4 v2, 0x4

    invoke-static {v0, v1, v2, v3}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 251
    new-instance v0, Lbmf;

    invoke-direct {v0}, Lbmf;-><init>()V

    iget-object v1, p0, Lbzv;->e:Lama;

    .line 252
    invoke-interface {v1}, Lama;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lbmf;->a(I)Lbmf;

    move-result-object v0

    iget-object v1, p1, Lonr;->d:Lons;

    .line 253
    invoke-static {v1}, Lbpx;->a(Lons;)Lbmp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbmf;->a(Lbmp;)Lbmf;

    move-result-object v0

    sget-object v1, Lbmg;->d:Lbmg;

    .line 254
    invoke-virtual {v0, v1}, Lbmf;->a(Lbmg;)Lbmf;

    move-result-object v0

    sget-object v1, Lbmd;->a:Ljeg;

    .line 255
    invoke-virtual {v0, v1}, Lbmf;->a(Ljeg;)Lbmf;

    move-result-object v0

    const/4 v1, 0x1

    .line 256
    invoke-virtual {v0, v1}, Lbmf;->a(Z)Lbmf;

    move-result-object v1

    const-string v0, "themeParams"

    .line 257
    invoke-static {p2, v0, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    invoke-virtual {v1, v0}, Lbmf;->a(Look;)Lbmf;

    move-result-object v0

    .line 258
    invoke-virtual {v0}, Lbmf;->a()Lbmd;

    move-result-object v0

    return-object v0
.end method

.method private static a(Loog;)Loog;
    .locals 9

    .prologue
    const v8, 0x3fe38e39

    const/high16 v7, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f000000    # 0.5f

    .line 979
    new-instance v0, Loog;

    invoke-direct {v0}, Loog;-><init>()V

    .line 982
    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Loog;->b:Ljava/lang/Float;

    .line 983
    iget-object v1, p0, Loog;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    sub-float v1, v7, v1

    iget-object v2, p0, Loog;->d:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    sub-float/2addr v1, v2

    .line 984
    iget-object v2, p0, Loog;->c:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    mul-float v3, v6, v1

    add-float/2addr v2, v3

    .line 985
    iget-object v3, p0, Loog;->e:Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    sub-float v3, v7, v3

    iget-object v4, p0, Loog;->f:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    sub-float/2addr v3, v4

    .line 986
    iget-object v4, p0, Loog;->e:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    mul-float v5, v6, v3

    add-float/2addr v4, v5

    .line 987
    iget-object v5, p0, Loog;->b:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    cmpg-float v5, v8, v5

    if-gez v5, :cond_0

    .line 989
    mul-float/2addr v1, v8

    iget-object v3, p0, Loog;->b:Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    div-float/2addr v1, v3

    .line 990
    mul-float v3, v6, v1

    sub-float v3, v2, v3

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    iput-object v3, v0, Loog;->c:Ljava/lang/Float;

    .line 991
    mul-float/2addr v1, v6

    sub-float v1, v2, v1

    sub-float v1, v7, v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Loog;->d:Ljava/lang/Float;

    .line 992
    iget-object v1, p0, Loog;->e:Ljava/lang/Float;

    iput-object v1, v0, Loog;->e:Ljava/lang/Float;

    .line 993
    iget-object v1, p0, Loog;->f:Ljava/lang/Float;

    iput-object v1, v0, Loog;->f:Ljava/lang/Float;

    .line 1003
    :goto_0
    return-object v0

    .line 997
    :cond_0
    div-float v1, v3, v8

    iget-object v2, p0, Loog;->b:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    mul-float/2addr v1, v2

    .line 998
    iget-object v2, p0, Loog;->c:Ljava/lang/Float;

    iput-object v2, v0, Loog;->c:Ljava/lang/Float;

    .line 999
    iget-object v2, p0, Loog;->d:Ljava/lang/Float;

    iput-object v2, v0, Loog;->d:Ljava/lang/Float;

    .line 1000
    mul-float v2, v6, v1

    sub-float v2, v4, v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v0, Loog;->e:Ljava/lang/Float;

    .line 1001
    mul-float/2addr v1, v6

    add-float/2addr v1, v4

    sub-float v1, v7, v1

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Loog;->f:Ljava/lang/Float;

    goto :goto_0
.end method

.method private static a(I)Looi;
    .locals 2

    .prologue
    .line 858
    new-instance v0, Looi;

    invoke-direct {v0}, Looi;-><init>()V

    .line 859
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Looi;->b:Ljava/lang/Integer;

    .line 860
    return-object v0
.end method

.method private a(Landroid/util/SparseArray;Ljava/util/List;Lood;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lbmh;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Lood;",
            ")V"
        }
    .end annotation

    .prologue
    .line 265
    iget-object v0, p3, Lood;->j:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/16 v0, 0x2d0

    move v3, v0

    .line 267
    :goto_0
    iget-object v0, p3, Lood;->i:Ljava/lang/Integer;

    if-nez v0, :cond_1

    const/16 v0, 0x500

    move v4, v0

    .line 270
    :goto_1
    const/4 v0, 0x0

    move v5, v0

    :goto_2
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v5, v0, :cond_2

    .line 271
    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 272
    iget-object v1, p0, Lbzv;->f:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lont;

    .line 273
    iget-object v2, p0, Lbzv;->g:Ljava/util/List;

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Look;

    .line 274
    iget v0, v0, Lbmd;->b:I

    .line 275
    invoke-static {v2, v1, v4, v3}, Lbmh;->a(Look;Lont;II)Lbmh;

    move-result-object v1

    .line 274
    invoke-virtual {p1, v0, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 270
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_2

    .line 265
    :cond_0
    iget-object v0, p3, Lood;->j:Ljava/lang/Integer;

    .line 266
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move v3, v0

    goto :goto_0

    .line 267
    :cond_1
    iget-object v0, p3, Lood;->i:Ljava/lang/Integer;

    .line 268
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move v4, v0

    goto :goto_1

    .line 278
    :cond_2
    return-void
.end method

.method private static a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbys;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 848
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 849
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    sget-object v2, Lbyn;->R:Lbyn;

    if-eq v1, v2, :cond_1

    .line 850
    add-int/lit8 v0, v0, 0x1

    sget-object v1, Lbyn;->Q:Lbyn;

    invoke-interface {p0, v0, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 854
    :cond_0
    return-void

    .line 848
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private static a(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbyp;",
            ">;",
            "Ljava/util/List",
            "<",
            "Look;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lont;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbys;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 286
    const/4 v2, 0x0

    .line 287
    const/4 v1, 0x0

    .line 288
    const/4 v0, 0x0

    move v3, v2

    move v2, v1

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_11

    .line 289
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbyp;

    .line 290
    invoke-virtual {v0}, Lbyp;->b()Lbyn;

    move-result-object v4

    .line 291
    sget-object v0, Lbzv;->a:Ljava/lang/String;

    invoke-virtual {v4}, Lbyn;->toString()Ljava/lang/String;

    .line 292
    invoke-interface {p3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 294
    sget-object v0, Lbzw;->a:[I

    invoke-virtual {v4}, Lbyn;->ordinal()I

    move-result v5

    aget v0, v0, v5

    packed-switch v0, :pswitch_data_0

    .line 827
    invoke-virtual {v4}, Lbyn;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    .line 828
    const/4 v0, 0x0

    move v4, v0

    :goto_1
    if-ge v4, v6, :cond_0

    .line 830
    new-instance v7, Looh;

    invoke-direct {v7}, Looh;-><init>()V

    .line 831
    const/4 v0, 0x1

    iput v0, v7, Looh;->a:I

    .line 832
    add-int/lit8 v5, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Looh;->b:Ljava/lang/Integer;

    .line 833
    add-int/lit8 v3, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v7, v0, Look;->g:Looh;

    .line 828
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v2, v3

    move v3, v5

    goto :goto_1

    .line 297
    :pswitch_0
    invoke-virtual {v4}, Lbyn;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    .line 298
    add-int/2addr v2, v0

    .line 836
    :cond_0
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 305
    :pswitch_1
    invoke-virtual {v4}, Lbyn;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    .line 306
    const/4 v0, 0x0

    move v4, v0

    :goto_3
    if-ge v4, v6, :cond_2

    .line 307
    new-instance v7, Looh;

    invoke-direct {v7}, Looh;-><init>()V

    .line 308
    if-nez v4, :cond_1

    .line 310
    const/4 v0, 0x2

    iput v0, v7, Looh;->a:I

    .line 312
    :cond_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Looh;->b:Ljava/lang/Integer;

    .line 313
    add-int/lit8 v5, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v7, v0, Look;->g:Looh;

    .line 306
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v2, v5

    goto :goto_3

    .line 315
    :cond_2
    add-int/lit8 v3, v3, 0x1

    .line 316
    goto :goto_2

    .line 323
    :pswitch_2
    invoke-virtual {v4}, Lbyn;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    .line 324
    const/4 v0, 0x0

    move v4, v0

    :goto_4
    if-ge v4, v6, :cond_4

    .line 325
    new-instance v7, Looh;

    invoke-direct {v7}, Looh;-><init>()V

    .line 326
    if-nez v4, :cond_3

    .line 328
    const/4 v0, 0x3

    iput v0, v7, Looh;->a:I

    .line 330
    :cond_3
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Looh;->b:Ljava/lang/Integer;

    .line 331
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lont;

    invoke-static {v0}, Lbzv;->a(Lont;)[Looj;

    move-result-object v0

    iput-object v0, v7, Looh;->d:[Looj;

    .line 332
    add-int/lit8 v5, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v7, v0, Look;->g:Looh;

    .line 324
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v2, v5

    goto :goto_4

    .line 334
    :cond_4
    add-int/lit8 v3, v3, 0x1

    .line 336
    goto :goto_2

    .line 343
    :pswitch_3
    new-instance v4, Looh;

    invoke-direct {v4}, Looh;-><init>()V

    .line 344
    const/4 v0, 0x1

    iput v0, v4, Looh;->a:I

    .line 345
    add-int/lit8 v5, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Looh;->b:Ljava/lang/Integer;

    .line 346
    add-int/lit8 v3, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v4, v0, Look;->g:Looh;

    .line 349
    new-instance v2, Looh;

    invoke-direct {v2}, Looh;-><init>()V

    .line 350
    const/4 v0, 0x1

    iput v0, v2, Looh;->a:I

    .line 351
    add-int/lit8 v4, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Looh;->b:Ljava/lang/Integer;

    .line 352
    const/4 v0, 0x1

    new-array v0, v0, [Looi;

    .line 353
    const/4 v5, 0x0

    add-int/lit8 v6, v3, -0x1

    invoke-static {v6}, Lbzv;->a(I)Looi;

    move-result-object v6

    aput-object v6, v0, v5

    .line 354
    iput-object v0, v2, Looh;->c:[Looi;

    .line 355
    add-int/lit8 v5, v3, 0x1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v2, v0, Look;->g:Looh;

    .line 358
    new-instance v2, Looh;

    invoke-direct {v2}, Looh;-><init>()V

    .line 359
    const/4 v0, 0x1

    iput v0, v2, Looh;->a:I

    .line 360
    add-int/lit8 v6, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Looh;->b:Ljava/lang/Integer;

    .line 361
    const/4 v0, 0x2

    new-array v0, v0, [Looi;

    .line 362
    const/4 v3, 0x0

    add-int/lit8 v4, v5, -0x2

    invoke-static {v4}, Lbzv;->a(I)Looi;

    move-result-object v4

    aput-object v4, v0, v3

    .line 363
    const/4 v3, 0x1

    add-int/lit8 v4, v5, -0x1

    invoke-static {v4}, Lbzv;->a(I)Looi;

    move-result-object v4

    aput-object v4, v0, v3

    .line 364
    iput-object v0, v2, Looh;->c:[Looi;

    .line 365
    add-int/lit8 v4, v5, 0x1

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v2, v0, Look;->g:Looh;

    .line 368
    new-instance v5, Looh;

    invoke-direct {v5}, Looh;-><init>()V

    .line 369
    const/4 v0, 0x1

    iput v0, v5, Looh;->a:I

    .line 370
    add-int/lit8 v3, v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v5, Looh;->b:Ljava/lang/Integer;

    .line 371
    const/4 v0, 0x1

    new-array v0, v0, [Looi;

    .line 372
    const/4 v2, 0x0

    add-int/lit8 v6, v4, -0x1

    invoke-static {v6}, Lbzv;->a(I)Looi;

    move-result-object v6

    aput-object v6, v0, v2

    .line 373
    iput-object v0, v5, Looh;->c:[Looi;

    .line 374
    add-int/lit8 v2, v4, 0x1

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v5, v0, Look;->g:Looh;

    goto/16 :goto_2

    .line 382
    :pswitch_4
    new-instance v4, Looh;

    invoke-direct {v4}, Looh;-><init>()V

    .line 383
    const/4 v0, 0x1

    iput v0, v4, Looh;->a:I

    .line 384
    add-int/lit8 v5, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Looh;->b:Ljava/lang/Integer;

    .line 385
    add-int/lit8 v3, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v4, v0, Look;->g:Looh;

    .line 388
    new-instance v2, Looh;

    invoke-direct {v2}, Looh;-><init>()V

    .line 389
    const/4 v0, 0x1

    iput v0, v2, Looh;->a:I

    .line 390
    add-int/lit8 v4, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Looh;->b:Ljava/lang/Integer;

    .line 391
    const/4 v0, 0x1

    new-array v0, v0, [Looi;

    .line 392
    const/4 v5, 0x0

    add-int/lit8 v6, v3, -0x1

    invoke-static {v6}, Lbzv;->a(I)Looi;

    move-result-object v6

    aput-object v6, v0, v5

    .line 393
    iput-object v0, v2, Looh;->c:[Looi;

    .line 394
    add-int/lit8 v5, v3, 0x1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v2, v0, Look;->g:Looh;

    .line 397
    new-instance v2, Looh;

    invoke-direct {v2}, Looh;-><init>()V

    .line 398
    const/4 v0, 0x1

    iput v0, v2, Looh;->a:I

    .line 399
    add-int/lit8 v6, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Looh;->b:Ljava/lang/Integer;

    .line 400
    const/4 v0, 0x2

    new-array v0, v0, [Looi;

    .line 401
    const/4 v3, 0x0

    add-int/lit8 v4, v5, -0x2

    invoke-static {v4}, Lbzv;->a(I)Looi;

    move-result-object v4

    aput-object v4, v0, v3

    .line 402
    const/4 v3, 0x1

    add-int/lit8 v4, v5, -0x1

    invoke-static {v4}, Lbzv;->a(I)Looi;

    move-result-object v4

    aput-object v4, v0, v3

    .line 403
    iput-object v0, v2, Looh;->c:[Looi;

    .line 404
    add-int/lit8 v4, v5, 0x1

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v2, v0, Look;->g:Looh;

    .line 407
    new-instance v5, Looh;

    invoke-direct {v5}, Looh;-><init>()V

    .line 408
    const/4 v0, 0x4

    iput v0, v5, Looh;->a:I

    .line 409
    add-int/lit8 v3, v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v5, Looh;->b:Ljava/lang/Integer;

    .line 411
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lont;

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lbzv;->a(Lont;I)[Looj;

    move-result-object v0

    iput-object v0, v5, Looh;->d:[Looj;

    .line 412
    const/4 v0, 0x1

    new-array v0, v0, [Looi;

    .line 413
    const/4 v2, 0x0

    add-int/lit8 v6, v4, -0x1

    invoke-static {v6}, Lbzv;->a(I)Looi;

    move-result-object v6

    aput-object v6, v0, v2

    .line 414
    iput-object v0, v5, Looh;->c:[Looi;

    .line 415
    add-int/lit8 v2, v4, 0x1

    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v5, v0, Look;->g:Looh;

    goto/16 :goto_2

    .line 421
    :pswitch_5
    new-instance v4, Looh;

    invoke-direct {v4}, Looh;-><init>()V

    .line 422
    const/4 v0, 0x1

    iput v0, v4, Looh;->a:I

    .line 423
    add-int/lit8 v5, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Looh;->b:Ljava/lang/Integer;

    .line 424
    add-int/lit8 v3, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v4, v0, Look;->g:Looh;

    .line 427
    new-instance v2, Looh;

    invoke-direct {v2}, Looh;-><init>()V

    .line 428
    const/4 v0, 0x1

    iput v0, v2, Looh;->a:I

    .line 429
    add-int/lit8 v4, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Looh;->b:Ljava/lang/Integer;

    .line 430
    const/4 v0, 0x1

    new-array v0, v0, [Looi;

    .line 431
    const/4 v5, 0x0

    add-int/lit8 v6, v3, -0x1

    invoke-static {v6}, Lbzv;->a(I)Looi;

    move-result-object v6

    aput-object v6, v0, v5

    .line 432
    iput-object v0, v2, Looh;->c:[Looi;

    .line 433
    add-int/lit8 v5, v3, 0x1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v2, v0, Look;->g:Looh;

    .line 436
    new-instance v6, Looh;

    invoke-direct {v6}, Looh;-><init>()V

    .line 437
    const/4 v0, 0x4

    iput v0, v6, Looh;->a:I

    .line 438
    add-int/lit8 v3, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v6, Looh;->b:Ljava/lang/Integer;

    .line 440
    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lont;

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lbzv;->a(Lont;I)[Looj;

    move-result-object v0

    iput-object v0, v6, Looh;->d:[Looj;

    .line 441
    const/4 v0, 0x1

    new-array v0, v0, [Looi;

    .line 442
    const/4 v2, 0x0

    add-int/lit8 v4, v5, -0x1

    invoke-static {v4}, Lbzv;->a(I)Looi;

    move-result-object v4

    aput-object v4, v0, v2

    .line 443
    add-int/lit8 v2, v5, 0x1

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v6, v0, Look;->g:Looh;

    goto/16 :goto_2

    .line 451
    :pswitch_6
    new-instance v4, Looh;

    invoke-direct {v4}, Looh;-><init>()V

    .line 452
    const/4 v0, 0x1

    iput v0, v4, Looh;->a:I

    .line 453
    add-int/lit8 v5, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Looh;->b:Ljava/lang/Integer;

    .line 454
    add-int/lit8 v3, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v4, v0, Look;->g:Looh;

    .line 457
    new-instance v2, Looh;

    invoke-direct {v2}, Looh;-><init>()V

    .line 458
    const/4 v0, 0x1

    iput v0, v2, Looh;->a:I

    .line 459
    add-int/lit8 v4, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Looh;->b:Ljava/lang/Integer;

    .line 460
    const/4 v0, 0x1

    new-array v0, v0, [Looi;

    .line 461
    const/4 v5, 0x0

    add-int/lit8 v6, v3, -0x1

    invoke-static {v6}, Lbzv;->a(I)Looi;

    move-result-object v6

    aput-object v6, v0, v5

    .line 462
    iput-object v0, v2, Looh;->c:[Looi;

    .line 463
    add-int/lit8 v5, v3, 0x1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v2, v0, Look;->g:Looh;

    .line 466
    new-instance v6, Looh;

    invoke-direct {v6}, Looh;-><init>()V

    .line 467
    const/4 v0, 0x4

    iput v0, v6, Looh;->a:I

    .line 468
    add-int/lit8 v3, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v6, Looh;->b:Ljava/lang/Integer;

    .line 470
    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lont;

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lbzv;->a(Lont;I)[Looj;

    move-result-object v0

    iput-object v0, v6, Looh;->d:[Looj;

    .line 471
    const/4 v0, 0x1

    new-array v0, v0, [Looi;

    .line 472
    const/4 v2, 0x0

    add-int/lit8 v4, v5, -0x1

    invoke-static {v4}, Lbzv;->a(I)Looi;

    move-result-object v4

    aput-object v4, v0, v2

    .line 473
    add-int/lit8 v2, v5, 0x1

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v6, v0, Look;->g:Looh;

    goto/16 :goto_2

    .line 480
    :pswitch_7
    new-instance v4, Looh;

    invoke-direct {v4}, Looh;-><init>()V

    .line 481
    const/4 v0, 0x1

    iput v0, v4, Looh;->a:I

    .line 482
    add-int/lit8 v5, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Looh;->b:Ljava/lang/Integer;

    .line 483
    add-int/lit8 v3, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v4, v0, Look;->g:Looh;

    .line 486
    new-instance v2, Looh;

    invoke-direct {v2}, Looh;-><init>()V

    .line 487
    const/4 v0, 0x1

    iput v0, v2, Looh;->a:I

    .line 488
    add-int/lit8 v4, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Looh;->b:Ljava/lang/Integer;

    .line 489
    const/4 v0, 0x1

    new-array v0, v0, [Looi;

    .line 490
    const/4 v5, 0x0

    add-int/lit8 v6, v3, -0x1

    invoke-static {v6}, Lbzv;->a(I)Looi;

    move-result-object v6

    aput-object v6, v0, v5

    .line 491
    iput-object v0, v2, Looh;->c:[Looi;

    .line 492
    add-int/lit8 v5, v3, 0x1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v2, v0, Look;->g:Looh;

    .line 495
    new-instance v6, Looh;

    invoke-direct {v6}, Looh;-><init>()V

    .line 496
    const/4 v0, 0x5

    iput v0, v6, Looh;->a:I

    .line 497
    add-int/lit8 v3, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v6, Looh;->b:Ljava/lang/Integer;

    .line 499
    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lont;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lbzv;->b(Lont;I)[Looj;

    move-result-object v0

    iput-object v0, v6, Looh;->d:[Looj;

    .line 500
    const/4 v0, 0x1

    new-array v0, v0, [Looi;

    .line 501
    const/4 v2, 0x0

    add-int/lit8 v4, v5, -0x1

    invoke-static {v4}, Lbzv;->a(I)Looi;

    move-result-object v4

    aput-object v4, v0, v2

    .line 502
    add-int/lit8 v2, v5, 0x1

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v6, v0, Look;->g:Looh;

    goto/16 :goto_2

    .line 509
    :pswitch_8
    new-instance v4, Looh;

    invoke-direct {v4}, Looh;-><init>()V

    .line 510
    const/4 v0, 0x1

    iput v0, v4, Looh;->a:I

    .line 511
    add-int/lit8 v5, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Looh;->b:Ljava/lang/Integer;

    .line 512
    add-int/lit8 v6, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v4, v0, Look;->g:Looh;

    .line 515
    new-instance v4, Looh;

    invoke-direct {v4}, Looh;-><init>()V

    .line 516
    const/4 v0, 0x5

    iput v0, v4, Looh;->a:I

    .line 517
    add-int/lit8 v3, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Looh;->b:Ljava/lang/Integer;

    .line 519
    invoke-interface {p2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lont;

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lbzv;->b(Lont;I)[Looj;

    move-result-object v0

    iput-object v0, v4, Looh;->d:[Looj;

    .line 520
    add-int/lit8 v2, v6, 0x1

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v4, v0, Look;->g:Looh;

    goto/16 :goto_2

    .line 527
    :pswitch_9
    new-instance v5, Looh;

    invoke-direct {v5}, Looh;-><init>()V

    .line 528
    const/4 v0, 0x5

    iput v0, v5, Looh;->a:I

    .line 529
    add-int/lit8 v4, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v5, Looh;->b:Ljava/lang/Integer;

    .line 531
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lont;

    const/4 v3, 0x3

    invoke-static {v0, v3}, Lbzv;->b(Lont;I)[Looj;

    move-result-object v0

    iput-object v0, v5, Looh;->d:[Looj;

    .line 532
    add-int/lit8 v3, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v5, v0, Look;->g:Looh;

    move v2, v3

    move v3, v4

    .line 534
    goto/16 :goto_2

    .line 546
    :pswitch_a
    new-instance v5, Looh;

    invoke-direct {v5}, Looh;-><init>()V

    .line 547
    const/4 v0, 0x1

    iput v0, v5, Looh;->a:I

    .line 548
    add-int/lit8 v6, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v5, Looh;->b:Ljava/lang/Integer;

    .line 549
    add-int/lit8 v3, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v5, v0, Look;->g:Looh;

    .line 552
    new-instance v5, Looh;

    invoke-direct {v5}, Looh;-><init>()V

    .line 553
    const/4 v0, 0x1

    iput v0, v5, Looh;->a:I

    .line 554
    add-int/lit8 v7, v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v5, Looh;->b:Ljava/lang/Integer;

    .line 555
    const/4 v0, 0x1

    new-array v0, v0, [Looi;

    .line 556
    const/4 v2, 0x0

    add-int/lit8 v6, v3, -0x1

    invoke-static {v6}, Lbzv;->a(I)Looi;

    move-result-object v6

    aput-object v6, v0, v2

    .line 557
    iput-object v0, v5, Looh;->c:[Looi;

    .line 558
    add-int/lit8 v2, v3, 0x1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v5, v0, Look;->g:Looh;

    .line 561
    add-int/lit8 v5, v2, -0x1

    .line 562
    invoke-virtual {v4}, Lbyn;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v6, v0, -0x2

    .line 563
    const/4 v0, 0x0

    move v3, v0

    :goto_5
    if-ge v3, v6, :cond_6

    .line 564
    new-instance v8, Looh;

    invoke-direct {v8}, Looh;-><init>()V

    .line 565
    if-nez v3, :cond_5

    .line 567
    const/4 v0, 0x2

    iput v0, v8, Looh;->a:I

    .line 569
    :cond_5
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v8, Looh;->b:Ljava/lang/Integer;

    .line 570
    const/4 v0, 0x1

    new-array v0, v0, [Looi;

    .line 571
    const/4 v4, 0x0

    invoke-static {v5}, Lbzv;->a(I)Looi;

    move-result-object v9

    aput-object v9, v0, v4

    .line 572
    add-int/lit8 v4, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v8, v0, Look;->g:Looh;

    .line 563
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v2, v4

    goto :goto_5

    .line 574
    :cond_6
    add-int/lit8 v3, v7, 0x1

    .line 576
    goto/16 :goto_2

    .line 588
    :pswitch_b
    new-instance v5, Looh;

    invoke-direct {v5}, Looh;-><init>()V

    .line 589
    const/4 v0, 0x1

    iput v0, v5, Looh;->a:I

    .line 590
    add-int/lit8 v6, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v5, Looh;->b:Ljava/lang/Integer;

    .line 591
    add-int/lit8 v3, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v5, v0, Look;->g:Looh;

    .line 594
    new-instance v5, Looh;

    invoke-direct {v5}, Looh;-><init>()V

    .line 595
    const/4 v0, 0x1

    iput v0, v5, Looh;->a:I

    .line 596
    add-int/lit8 v7, v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v5, Looh;->b:Ljava/lang/Integer;

    .line 597
    const/4 v0, 0x1

    new-array v0, v0, [Looi;

    .line 598
    const/4 v2, 0x0

    add-int/lit8 v6, v3, -0x1

    invoke-static {v6}, Lbzv;->a(I)Looi;

    move-result-object v6

    aput-object v6, v0, v2

    .line 599
    iput-object v0, v5, Looh;->c:[Looi;

    .line 600
    add-int/lit8 v2, v3, 0x1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v5, v0, Look;->g:Looh;

    .line 603
    add-int/lit8 v5, v2, -0x1

    .line 604
    invoke-virtual {v4}, Lbyn;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v6, v0, -0x2

    .line 605
    const/4 v0, 0x0

    move v3, v0

    :goto_6
    if-ge v3, v6, :cond_8

    .line 606
    new-instance v8, Looh;

    invoke-direct {v8}, Looh;-><init>()V

    .line 607
    if-nez v3, :cond_7

    .line 609
    const/4 v0, 0x3

    iput v0, v8, Looh;->a:I

    .line 611
    :cond_7
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v8, Looh;->b:Ljava/lang/Integer;

    .line 612
    const/4 v0, 0x1

    new-array v0, v0, [Looi;

    .line 613
    const/4 v4, 0x0

    invoke-static {v5}, Lbzv;->a(I)Looi;

    move-result-object v9

    aput-object v9, v0, v4

    .line 614
    add-int/lit8 v4, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v8, v0, Look;->g:Looh;

    .line 605
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v2, v4

    goto :goto_6

    .line 616
    :cond_8
    add-int/lit8 v3, v7, 0x1

    .line 618
    goto/16 :goto_2

    .line 624
    :pswitch_c
    new-instance v4, Looh;

    invoke-direct {v4}, Looh;-><init>()V

    .line 625
    const/4 v0, 0x1

    iput v0, v4, Looh;->a:I

    .line 626
    add-int/lit8 v5, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Looh;->b:Ljava/lang/Integer;

    .line 627
    add-int/lit8 v3, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v4, v0, Look;->g:Looh;

    .line 630
    new-instance v2, Looh;

    invoke-direct {v2}, Looh;-><init>()V

    .line 631
    const/4 v0, 0x1

    iput v0, v2, Looh;->a:I

    .line 632
    add-int/lit8 v4, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Looh;->b:Ljava/lang/Integer;

    .line 633
    const/4 v0, 0x1

    new-array v0, v0, [Looi;

    .line 634
    const/4 v5, 0x0

    add-int/lit8 v6, v3, -0x1

    invoke-static {v6}, Lbzv;->a(I)Looi;

    move-result-object v6

    aput-object v6, v0, v5

    .line 635
    iput-object v0, v2, Looh;->c:[Looi;

    .line 636
    add-int/lit8 v5, v3, 0x1

    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v2, v0, Look;->g:Looh;

    .line 639
    new-instance v6, Looh;

    invoke-direct {v6}, Looh;-><init>()V

    .line 640
    const/4 v0, 0x1

    iput v0, v6, Looh;->a:I

    .line 641
    add-int/lit8 v3, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v6, Looh;->b:Ljava/lang/Integer;

    .line 642
    const/4 v0, 0x1

    new-array v0, v0, [Looi;

    .line 643
    const/4 v4, 0x0

    add-int/lit8 v7, v5, -0x1

    invoke-static {v7}, Lbzv;->a(I)Looi;

    move-result-object v7

    aput-object v7, v0, v4

    .line 644
    iput-object v0, v2, Looh;->c:[Looi;

    .line 645
    add-int/lit8 v2, v5, 0x1

    invoke-interface {p1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v6, v0, Look;->g:Looh;

    goto/16 :goto_2

    .line 653
    :pswitch_d
    new-instance v4, Looh;

    invoke-direct {v4}, Looh;-><init>()V

    .line 654
    const/4 v0, 0x5

    iput v0, v4, Looh;->a:I

    .line 655
    add-int/lit8 v5, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Looh;->b:Ljava/lang/Integer;

    .line 657
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lont;

    const/4 v3, 0x1

    invoke-static {v0, v3}, Lbzv;->b(Lont;I)[Looj;

    move-result-object v0

    iput-object v0, v4, Looh;->d:[Looj;

    .line 658
    add-int/lit8 v6, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v4, v0, Look;->g:Looh;

    .line 661
    new-instance v4, Looh;

    invoke-direct {v4}, Looh;-><init>()V

    .line 662
    const/4 v0, 0x1

    iput v0, v4, Looh;->a:I

    .line 663
    add-int/lit8 v3, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Looh;->b:Ljava/lang/Integer;

    .line 664
    add-int/lit8 v2, v6, 0x1

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v4, v0, Look;->g:Looh;

    goto/16 :goto_2

    .line 674
    :pswitch_e
    new-instance v5, Looh;

    invoke-direct {v5}, Looh;-><init>()V

    .line 675
    const/4 v0, 0x1

    iput v0, v5, Looh;->a:I

    .line 676
    add-int/lit8 v6, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v5, Looh;->b:Ljava/lang/Integer;

    .line 677
    add-int/lit8 v3, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v5, v0, Look;->g:Looh;

    .line 680
    invoke-virtual {v4}, Lbyn;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v5, v0, -0x1

    .line 681
    const/4 v0, 0x0

    move v2, v3

    move v3, v0

    :goto_7
    if-ge v3, v5, :cond_a

    .line 682
    new-instance v7, Looh;

    invoke-direct {v7}, Looh;-><init>()V

    .line 683
    if-nez v3, :cond_9

    .line 685
    const/4 v0, 0x2

    iput v0, v7, Looh;->a:I

    .line 687
    :cond_9
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Looh;->b:Ljava/lang/Integer;

    .line 688
    add-int/lit8 v4, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v7, v0, Look;->g:Looh;

    .line 681
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v2, v4

    goto :goto_7

    .line 690
    :cond_a
    add-int/lit8 v3, v6, 0x1

    .line 692
    goto/16 :goto_2

    .line 700
    :pswitch_f
    new-instance v5, Looh;

    invoke-direct {v5}, Looh;-><init>()V

    .line 701
    const/4 v0, 0x1

    iput v0, v5, Looh;->a:I

    .line 702
    add-int/lit8 v6, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v5, Looh;->b:Ljava/lang/Integer;

    .line 703
    add-int/lit8 v3, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v5, v0, Look;->g:Looh;

    .line 706
    invoke-virtual {v4}, Lbyn;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v5, v0, -0x1

    .line 707
    const/4 v0, 0x0

    move v2, v3

    move v3, v0

    :goto_8
    if-ge v3, v5, :cond_c

    .line 708
    new-instance v7, Looh;

    invoke-direct {v7}, Looh;-><init>()V

    .line 709
    if-nez v3, :cond_b

    .line 711
    const/4 v0, 0x3

    iput v0, v7, Looh;->a:I

    .line 713
    :cond_b
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Looh;->b:Ljava/lang/Integer;

    .line 714
    add-int/lit8 v4, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v7, v0, Look;->g:Looh;

    .line 707
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v2, v4

    goto :goto_8

    .line 716
    :cond_c
    add-int/lit8 v3, v6, 0x1

    .line 718
    goto/16 :goto_2

    .line 724
    :pswitch_10
    new-instance v4, Looh;

    invoke-direct {v4}, Looh;-><init>()V

    .line 725
    const/4 v0, 0x4

    iput v0, v4, Looh;->a:I

    .line 726
    add-int/lit8 v5, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Looh;->b:Ljava/lang/Integer;

    .line 728
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lont;

    const/4 v3, 0x2

    invoke-static {v0, v3}, Lbzv;->a(Lont;I)[Looj;

    move-result-object v0

    iput-object v0, v4, Looh;->d:[Looj;

    .line 729
    add-int/lit8 v6, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v4, v0, Look;->g:Looh;

    .line 732
    new-instance v4, Looh;

    invoke-direct {v4}, Looh;-><init>()V

    .line 733
    const/4 v0, 0x5

    iput v0, v4, Looh;->a:I

    .line 734
    add-int/lit8 v3, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Looh;->b:Ljava/lang/Integer;

    .line 736
    invoke-interface {p2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lont;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lbzv;->b(Lont;I)[Looj;

    move-result-object v0

    iput-object v0, v4, Looh;->d:[Looj;

    .line 737
    add-int/lit8 v2, v6, 0x1

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v4, v0, Look;->g:Looh;

    goto/16 :goto_2

    .line 745
    :pswitch_11
    new-instance v4, Looh;

    invoke-direct {v4}, Looh;-><init>()V

    .line 746
    const/4 v0, 0x4

    iput v0, v4, Looh;->a:I

    .line 747
    add-int/lit8 v5, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Looh;->b:Ljava/lang/Integer;

    .line 749
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lont;

    const/4 v3, 0x2

    invoke-static {v0, v3}, Lbzv;->a(Lont;I)[Looj;

    move-result-object v0

    iput-object v0, v4, Looh;->d:[Looj;

    .line 750
    add-int/lit8 v6, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v4, v0, Look;->g:Looh;

    .line 753
    new-instance v4, Looh;

    invoke-direct {v4}, Looh;-><init>()V

    .line 754
    const/4 v0, 0x4

    iput v0, v4, Looh;->a:I

    .line 755
    add-int/lit8 v3, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Looh;->b:Ljava/lang/Integer;

    .line 757
    invoke-interface {p2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lont;

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lbzv;->a(Lont;I)[Looj;

    move-result-object v0

    iput-object v0, v4, Looh;->d:[Looj;

    .line 758
    add-int/lit8 v2, v6, 0x1

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v4, v0, Look;->g:Looh;

    goto/16 :goto_2

    .line 766
    :pswitch_12
    new-instance v4, Looh;

    invoke-direct {v4}, Looh;-><init>()V

    .line 767
    const/4 v0, 0x4

    iput v0, v4, Looh;->a:I

    .line 768
    add-int/lit8 v5, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Looh;->b:Ljava/lang/Integer;

    .line 770
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lont;

    const/4 v3, 0x2

    invoke-static {v0, v3}, Lbzv;->a(Lont;I)[Looj;

    move-result-object v0

    iput-object v0, v4, Looh;->d:[Looj;

    .line 771
    add-int/lit8 v6, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v4, v0, Look;->g:Looh;

    .line 774
    new-instance v4, Looh;

    invoke-direct {v4}, Looh;-><init>()V

    .line 775
    const/4 v0, 0x1

    iput v0, v4, Looh;->a:I

    .line 776
    add-int/lit8 v3, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v4, Looh;->b:Ljava/lang/Integer;

    .line 777
    add-int/lit8 v2, v6, 0x1

    invoke-interface {p1, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v4, v0, Look;->g:Looh;

    goto/16 :goto_2

    .line 785
    :pswitch_13
    new-instance v5, Looh;

    invoke-direct {v5}, Looh;-><init>()V

    .line 786
    const/4 v0, 0x1

    iput v0, v5, Looh;->a:I

    .line 787
    add-int/lit8 v4, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v5, Looh;->b:Ljava/lang/Integer;

    .line 788
    add-int/lit8 v3, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v5, v0, Look;->g:Looh;

    move v2, v3

    move v3, v4

    .line 789
    goto/16 :goto_2

    .line 795
    :pswitch_14
    invoke-virtual {v4}, Lbyn;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    .line 796
    const/4 v0, 0x0

    move v4, v0

    :goto_9
    if-ge v4, v6, :cond_e

    .line 797
    new-instance v7, Looh;

    invoke-direct {v7}, Looh;-><init>()V

    .line 798
    if-nez v4, :cond_d

    .line 800
    const/4 v0, 0x2

    iput v0, v7, Looh;->a:I

    .line 802
    :cond_d
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Looh;->b:Ljava/lang/Integer;

    .line 803
    add-int/lit8 v5, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v7, v0, Look;->g:Looh;

    .line 796
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v2, v5

    goto :goto_9

    .line 805
    :cond_e
    add-int/lit8 v3, v3, 0x1

    .line 806
    goto/16 :goto_2

    .line 812
    :pswitch_15
    invoke-virtual {v4}, Lbyn;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    .line 813
    const/4 v0, 0x0

    move v4, v0

    :goto_a
    if-ge v4, v6, :cond_10

    .line 814
    new-instance v7, Looh;

    invoke-direct {v7}, Looh;-><init>()V

    .line 815
    if-nez v4, :cond_f

    .line 817
    const/4 v0, 0x3

    iput v0, v7, Looh;->a:I

    .line 819
    :cond_f
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v7, Looh;->b:Ljava/lang/Integer;

    .line 820
    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lont;

    invoke-static {v0}, Lbzv;->a(Lont;)[Looj;

    move-result-object v0

    iput-object v0, v7, Looh;->d:[Looj;

    .line 821
    add-int/lit8 v5, v2, 0x1

    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    iput-object v7, v0, Look;->g:Looh;

    .line 813
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v2, v5

    goto :goto_a

    .line 823
    :cond_10
    add-int/lit8 v3, v3, 0x1

    .line 824
    goto/16 :goto_2

    .line 840
    :cond_11
    return-void

    .line 294
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_a
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_e
        :pswitch_f
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_13
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_14
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_15
    .end packed-switch
.end method

.method private a(Lood;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lood;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 958
    iget-object v0, p0, Lbzv;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    move v1, v2

    .line 959
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 960
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-object v0, v0, Lbmd;->e:Ljeg;

    iget-object v3, v0, Ljeg;->a:Ljdx;

    move v0, v2

    .line 962
    :goto_1
    iget-object v4, p1, Lood;->b:[Lonr;

    array-length v4, v4

    if-ge v0, v4, :cond_0

    .line 963
    iget-object v4, p1, Lood;->d:[Ljava/lang/Boolean;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_1

    .line 964
    iget-object v4, p1, Lood;->b:[Lonr;

    aget-object v4, v4, v0

    .line 965
    iget-object v5, v4, Lonr;->c:Lonw;

    invoke-static {v5}, Ljdx;->a(Lonw;)Ljdx;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljdx;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 966
    iget-object v3, p0, Lbzv;->f:Ljava/util/List;

    iget-object v4, v4, Lonr;->e:Lont;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 967
    iget-object v3, p0, Lbzv;->g:Ljava/util/List;

    iget-object v4, p1, Lood;->h:Looe;

    iget-object v4, v4, Looe;->b:Looo;

    iget-object v4, v4, Looo;->b:[Look;

    aget-object v0, v4, v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 959
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 962
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 974
    :cond_2
    return-void
.end method

.method private static a(Lont;)[Looj;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 866
    iget-object v0, p0, Lont;->d:[Loob;

    array-length v0, v0

    if-nez v0, :cond_0

    .line 867
    new-array v0, v2, [Looj;

    .line 877
    :goto_0
    return-object v0

    .line 871
    :cond_0
    new-instance v1, Looj;

    invoke-direct {v1}, Looj;-><init>()V

    .line 872
    iget-object v0, p0, Lont;->d:[Loob;

    aget-object v0, v0, v2

    iget-object v0, v0, Loob;->b:Loog;

    invoke-static {v0}, Lbzv;->a(Loog;)Loog;

    move-result-object v0

    iput-object v0, v1, Looj;->b:Loog;

    .line 873
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v1, Looj;->d:Ljava/lang/Float;

    .line 874
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v1, Looj;->c:Ljava/lang/Float;

    .line 875
    const/4 v0, 0x1

    new-array v0, v0, [Looj;

    .line 876
    aput-object v1, v0, v2

    goto :goto_0
.end method

.method private static a(Lont;I)[Looj;
    .locals 12

    .prologue
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v11, 0x3f000000    # 0.5f

    .line 883
    const/16 v2, 0x500

    const/16 v3, 0x2d0

    invoke-static {p0, v2, v3}, Lbpp;->b(Lont;II)Loog;

    move-result-object v2

    .line 885
    if-nez v2, :cond_0

    .line 886
    new-array v0, v0, [Looj;

    .line 915
    :goto_0
    return-object v0

    .line 888
    :cond_0
    iget-object v3, v2, Loog;->c:Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    sub-float v3, v1, v3

    iget-object v4, v2, Loog;->d:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    sub-float v5, v3, v4

    .line 889
    iget-object v3, v2, Loog;->c:Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    mul-float v4, v11, v5

    add-float v6, v3, v4

    .line 890
    iget-object v3, v2, Loog;->e:Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    sub-float v3, v1, v3

    iget-object v4, v2, Loog;->f:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    sub-float v7, v3, v4

    .line 891
    iget-object v2, v2, Loog;->e:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    mul-float v3, v11, v7

    add-float v8, v2, v3

    .line 893
    new-array v3, p1, [Looj;

    .line 894
    int-to-float v2, p1

    div-float v2, v1, v2

    move v4, v0

    .line 895
    :goto_1
    if-ge v4, p1, :cond_2

    .line 897
    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    .line 898
    sget-object v0, Lbzv;->b:[F

    aget v0, v0, v4

    .line 903
    :goto_2
    new-instance v9, Loog;

    invoke-direct {v9}, Loog;-><init>()V

    .line 904
    mul-float v10, v11, v5

    mul-float/2addr v10, v0

    sub-float v10, v6, v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    iput-object v10, v9, Loog;->c:Ljava/lang/Float;

    .line 905
    mul-float v10, v11, v5

    mul-float/2addr v10, v0

    add-float/2addr v10, v6

    sub-float v10, v1, v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    iput-object v10, v9, Loog;->d:Ljava/lang/Float;

    .line 906
    mul-float v10, v11, v7

    mul-float/2addr v10, v0

    sub-float v10, v8, v10

    invoke-static {v10}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v10

    iput-object v10, v9, Loog;->e:Ljava/lang/Float;

    .line 907
    mul-float v10, v11, v7

    mul-float/2addr v0, v10

    add-float/2addr v0, v8

    sub-float v0, v1, v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, v9, Loog;->f:Ljava/lang/Float;

    .line 908
    new-instance v0, Looj;

    invoke-direct {v0}, Looj;-><init>()V

    .line 909
    iput-object v9, v0, Looj;->b:Loog;

    .line 911
    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    iput-object v9, v0, Looj;->d:Ljava/lang/Float;

    .line 912
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    iput-object v9, v0, Looj;->c:Ljava/lang/Float;

    .line 913
    aput-object v0, v3, v4

    .line 895
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    .line 899
    :cond_1
    const/4 v0, 0x3

    if-ne p1, v0, :cond_3

    .line 900
    sget-object v0, Lbzv;->c:[F

    aget v0, v0, v4

    .line 901
    sget-object v2, Lbzv;->d:[F

    aget v2, v2, v4

    goto :goto_2

    :cond_2
    move-object v0, v3

    .line 915
    goto/16 :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method private static b(Lont;I)[Looj;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 921
    iget-object v0, p0, Lont;->d:[Loob;

    array-length v0, v0

    if-ge v0, p1, :cond_0

    iget-object v0, p0, Lont;->d:[Loob;

    array-length v0, v0

    .line 923
    :goto_0
    add-int/lit8 v1, p1, 0x1

    new-array v2, v1, [Looj;

    .line 924
    const/high16 v1, 0x3f800000    # 1.0f

    add-int/lit8 v3, p1, 0x1

    int-to-float v3, v3

    div-float v3, v1, v3

    .line 925
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_1

    .line 926
    iget-object v4, p0, Lont;->d:[Loob;

    aget-object v4, v4, v1

    .line 927
    new-instance v5, Looj;

    invoke-direct {v5}, Looj;-><init>()V

    .line 928
    iget-object v4, v4, Loob;->b:Loog;

    invoke-static {v4}, Lbzv;->a(Loog;)Loog;

    move-result-object v4

    iput-object v4, v5, Looj;->b:Loog;

    .line 929
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    iput-object v4, v5, Looj;->c:Ljava/lang/Float;

    .line 930
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    iput-object v4, v5, Looj;->d:Ljava/lang/Float;

    .line 931
    aput-object v5, v2, v1

    .line 925
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, p1

    .line 921
    goto :goto_0

    .line 935
    :cond_1
    const/16 v1, 0x500

    const/16 v4, 0x2d0

    invoke-static {p0, v1, v4}, Lbpp;->b(Lont;II)Loog;

    move-result-object v1

    .line 937
    if-nez v1, :cond_2

    .line 938
    new-instance v1, Loog;

    invoke-direct {v1}, Loog;-><init>()V

    .line 939
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    iput-object v4, v1, Loog;->c:Ljava/lang/Float;

    .line 940
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    iput-object v4, v1, Loog;->d:Ljava/lang/Float;

    .line 941
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    iput-object v4, v1, Loog;->e:Ljava/lang/Float;

    .line 942
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    iput-object v4, v1, Loog;->f:Ljava/lang/Float;

    .line 944
    :cond_2
    :goto_2
    add-int/lit8 v4, p1, 0x1

    if-ge v0, v4, :cond_3

    .line 945
    new-instance v4, Looj;

    invoke-direct {v4}, Looj;-><init>()V

    .line 946
    iput-object v1, v4, Looj;->b:Loog;

    .line 947
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    iput-object v5, v4, Looj;->c:Ljava/lang/Float;

    .line 948
    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    iput-object v5, v4, Looj;->d:Ljava/lang/Float;

    .line 949
    aput-object v4, v2, v0

    .line 944
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 952
    :cond_3
    return-object v2
.end method


# virtual methods
.method public a(Lbmd;Lbza;)Lbmd;
    .locals 1

    .prologue
    .line 1009
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Lood;Ljava/util/List;Lbza;Ljava/util/List;Landroid/util/SparseArray;Z)Lbzy;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lood;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Lbza;",
            "Ljava/util/List",
            "<",
            "Lbys;",
            ">;",
            "Landroid/util/SparseArray",
            "<",
            "Lbmh;",
            ">;Z)",
            "Lbzy;"
        }
    .end annotation

    .prologue
    .line 70
    const-string v0, "cloud storyboard should not be null for FunFactory movies!"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 73
    if-eqz p1, :cond_13

    if-nez p6, :cond_13

    iget-object v0, p1, Lood;->h:Looe;

    iget v0, v0, Looe;->a:I

    .line 75
    invoke-static {v0}, Lbza;->a(I)Lbza;

    move-result-object v0

    if-ne v0, p3, :cond_13

    .line 76
    iget-object v0, p1, Lood;->h:Looe;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lood;->h:Looe;

    iget-object v0, v0, Looe;->b:Looo;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    move v2, v0

    .line 77
    :goto_0
    if-eqz v2, :cond_13

    .line 78
    sget-object v0, Lbzv;->a:Ljava/lang/String;

    const-string v0, "copying theme params from cloud storyboard [cloudStoryboard_is_null:"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-nez p1, :cond_11

    const/4 v0, 0x1

    move v1, v0

    :goto_1
    if-eqz p1, :cond_12

    iget-object v0, p1, Lood;->h:Looe;

    iget v0, v0, Looe;->a:I

    .line 83
    invoke-static {v0}, Lbza;->a(I)Lbza;

    move-result-object v0

    if-ne v0, p3, :cond_12

    const/4 v0, 0x1

    :goto_2
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x3b

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " hasUserEdits:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " theme_matches:"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " isValid:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    iget-object v0, p0, Lbzv;->e:Lama;

    invoke-static {p1, p2, p3, v0}, Lbzz;->a(Lood;Ljava/util/List;Lbza;Lama;)Lbzy;

    move-result-object v0

    .line 143
    :goto_3
    return-object v0

    .line 76
    :cond_1
    iget-object v4, p1, Lood;->b:[Lonr;

    iget-object v0, p1, Lood;->h:Looe;

    iget-object v0, v0, Looe;->b:Looo;

    iget-object v5, v0, Looo;->b:[Look;

    const/4 v1, 0x0

    const/4 v0, 0x0

    move-object v2, v1

    move v1, v0

    :goto_4
    array-length v0, v4

    if-ge v1, v0, :cond_10

    aget-object v0, v5, v1

    iget-object v3, v0, Look;->g:Looh;

    if-nez v3, :cond_3

    const/4 v0, 0x0

    :cond_2
    add-int/lit8 v1, v1, 0x1

    move-object v2, v0

    goto :goto_4

    :cond_3
    iget-object v0, v0, Look;->g:Looh;

    iget-object v0, v0, Looh;->c:[Looi;

    array-length v3, v0

    const/4 v6, 0x2

    if-le v3, v6, :cond_4

    const/4 v0, 0x0

    move v2, v0

    goto :goto_0

    :cond_4
    if-nez v2, :cond_6

    array-length v2, v0

    const/4 v3, 0x2

    if-lt v2, v3, :cond_5

    const/4 v0, 0x0

    move v2, v0

    goto/16 :goto_0

    :cond_5
    array-length v2, v0

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    const/4 v2, 0x0

    aget-object v2, v0, v2

    iget-object v2, v2, Looi;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v3, v1, -0x1

    if-eq v2, v3, :cond_2

    const/4 v0, 0x0

    move v2, v0

    goto/16 :goto_0

    :cond_6
    array-length v3, v2

    array-length v6, v0

    if-eq v3, v6, :cond_7

    const/4 v3, 0x0

    :goto_5
    if-nez v3, :cond_2

    array-length v3, v0

    if-eqz v3, :cond_f

    const/4 v3, 0x0

    :goto_6
    array-length v6, v0

    if-ge v3, v6, :cond_b

    aget-object v6, v0, v3

    iget-object v6, v6, Looi;->b:Ljava/lang/Integer;

    if-nez v6, :cond_a

    const/4 v2, 0x0

    :goto_7
    if-nez v2, :cond_2

    const/4 v0, 0x0

    move v2, v0

    goto/16 :goto_0

    :cond_7
    const/4 v3, 0x0

    :goto_8
    array-length v6, v2

    if-ge v3, v6, :cond_9

    aget-object v6, v2, v3

    iget-object v6, v6, Looi;->b:Ljava/lang/Integer;

    aget-object v7, v0, v3

    iget-object v7, v7, Looi;->b:Ljava/lang/Integer;

    if-eq v6, v7, :cond_8

    const/4 v3, 0x0

    goto :goto_5

    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_9
    const/4 v3, 0x1

    goto :goto_5

    :cond_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    :cond_b
    array-length v3, v0

    add-int/lit8 v3, v3, -0x1

    aget-object v3, v0, v3

    iget-object v3, v3, Looi;->b:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v6, v1, -0x1

    if-eq v3, v6, :cond_c

    const/4 v2, 0x0

    goto :goto_7

    :cond_c
    array-length v3, v0

    array-length v6, v2

    add-int/lit8 v6, v6, 0x1

    if-le v3, v6, :cond_d

    const/4 v2, 0x0

    goto :goto_7

    :cond_d
    array-length v3, v2

    array-length v6, v0

    sub-int/2addr v3, v6

    add-int/lit8 v6, v3, 0x1

    const/4 v3, 0x0

    :goto_9
    array-length v7, v0

    add-int/lit8 v7, v7, -0x1

    if-ge v3, v7, :cond_f

    add-int v7, v6, v3

    aget-object v7, v2, v7

    iget-object v7, v7, Looi;->b:Ljava/lang/Integer;

    aget-object v8, v0, v3

    iget-object v8, v8, Looi;->b:Ljava/lang/Integer;

    if-eq v7, v8, :cond_e

    const/4 v2, 0x0

    goto :goto_7

    :cond_e
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_f
    const/4 v2, 0x1

    goto :goto_7

    :cond_10
    const/4 v0, 0x1

    move v2, v0

    goto/16 :goto_0

    .line 78
    :cond_11
    const/4 v0, 0x0

    move v1, v0

    goto/16 :goto_1

    .line 83
    :cond_12
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 91
    :cond_13
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 92
    const/4 v4, 0x0

    .line 93
    const/4 v3, 0x0

    .line 94
    new-instance v0, Loon;

    invoke-direct {v0}, Loon;-><init>()V

    .line 96
    iget-object v1, p1, Lood;->h:Looe;

    iget-object v1, v1, Looe;->b:Looo;

    iget-object v1, v1, Looo;->a:Loon;

    if-eqz v1, :cond_1c

    .line 97
    iget-object v0, p1, Lood;->h:Looe;

    iget-object v0, v0, Looe;->b:Looo;

    iget-object v0, v0, Looo;->a:Loon;

    move-object v2, v0

    .line 100
    :goto_a
    const/4 v0, 0x0

    move v5, v0

    .line 101
    :goto_b
    iget-object v0, p1, Lood;->b:[Lonr;

    array-length v0, v0

    if-ge v5, v0, :cond_15

    .line 102
    iget-object v0, p1, Lood;->h:Looe;

    iget-object v0, v0, Looe;->b:Looo;

    iget-object v0, v0, Looo;->b:[Look;

    aget-object v0, v0, v5

    .line 104
    iget-object v1, p1, Lood;->d:[Ljava/lang/Boolean;

    aget-object v1, v1, v5

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1b

    .line 105
    if-nez v5, :cond_14

    .line 106
    iget-object v1, p1, Lood;->b:[Lonr;

    aget-object v1, v1, v5

    invoke-direct {p0, v1, v0}, Lbzv;->a(Lonr;Look;)Lbmd;

    move-result-object v0

    move-object v1, v0

    move-object v0, v3

    .line 101
    :goto_c
    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move-object v4, v1

    move-object v3, v0

    goto :goto_b

    .line 109
    :cond_14
    const-string v1, "afterFadeOutClip"

    const/4 v7, 0x0

    invoke-static {v3, v1, v7}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 110
    iget-object v1, p1, Lood;->b:[Lonr;

    aget-object v1, v1, v5

    invoke-direct {p0, v1, v0}, Lbzv;->a(Lonr;Look;)Lbmd;

    move-result-object v0

    move-object v1, v4

    goto :goto_c

    .line 116
    :cond_15
    invoke-direct {p0, p1, p2}, Lbzv;->a(Lood;Ljava/util/List;)V

    .line 117
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lbzv;->f:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_18

    const/4 v0, 0x1

    :goto_d
    const-string v1, "clips and mClipsMetadata should have the same length!"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 121
    new-instance v0, Lbyo;

    invoke-direct {v0}, Lbyo;-><init>()V

    .line 122
    iget-object v1, p0, Lbzv;->f:Ljava/util/List;

    invoke-virtual {v0, p2, v1}, Lbyo;->a(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 123
    if-nez v0, :cond_16

    .line 124
    sget-object v1, Lbzv;->a:Ljava/lang/String;

    const-string v5, "clipGroups have size 0"

    invoke-static {v1, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 126
    :cond_16
    iget-object v1, p0, Lbzv;->g:Ljava/util/List;

    iget-object v5, p0, Lbzv;->f:Ljava/util/List;

    invoke-static {v0, v1, v5, p4}, Lbzv;->a(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V

    .line 128
    if-eqz v3, :cond_17

    .line 129
    invoke-static {p4}, Lbzv;->a(Ljava/util/List;)V

    .line 132
    :cond_17
    invoke-direct {p0, p5, p2, p1}, Lbzv;->a(Landroid/util/SparseArray;Ljava/util/List;Lood;)V

    .line 134
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    iget-object v1, p0, Lbzv;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_19

    const/4 v0, 0x1

    :goto_e
    const-string v1, "clips and mThemeDisplayableClipParams should have the same length!"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 137
    const/4 v0, 0x0

    move v5, v0

    :goto_f
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v5, v0, :cond_1a

    .line 138
    invoke-interface {p2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 139
    iget-object v1, p0, Lbzv;->g:Ljava/util/List;

    invoke-interface {v1, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Look;

    .line 140
    invoke-virtual {v0}, Lbmd;->a()Lbmf;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbmf;->a(Look;)Lbmf;

    move-result-object v0

    invoke-virtual {v0}, Lbmf;->a()Lbmd;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 137
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_f

    .line 117
    :cond_18
    const/4 v0, 0x0

    goto :goto_d

    .line 134
    :cond_19
    const/4 v0, 0x0

    goto :goto_e

    .line 143
    :cond_1a
    new-instance v0, Lbzy;

    invoke-direct {v0, v2, v6, v4, v3}, Lbzy;-><init>(Loon;Ljava/util/List;Lbmd;Lbmd;)V

    goto/16 :goto_3

    :cond_1b
    move-object v0, v3

    move-object v1, v4

    goto/16 :goto_c

    :cond_1c
    move-object v2, v0

    goto/16 :goto_a
.end method
