.class public final Lazj;
.super Layr;
.source "PG"


# instance fields
.field private final a:[Lazk;

.field private final b:[J

.field private final c:J

.field private final d:Landroid/graphics/Matrix;

.field private final e:F

.field private f:I


# direct methods
.method public varargs constructor <init>(IJF[Lazk;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Layr;-><init>()V

    .line 35
    iput-object p5, p0, Lazj;->a:[Lazk;

    .line 36
    iget-object v0, p0, Lazj;->a:[Lazk;

    array-length v0, v0

    new-array v0, v0, [J

    iput-object v0, p0, Lazj;->b:[J

    move v0, v1

    .line 37
    :goto_0
    iget-object v2, p0, Lazj;->a:[Lazk;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 38
    iget-object v2, p0, Lazj;->b:[J

    iget-object v3, p0, Lazj;->a:[Lazk;

    aget-object v3, v3, v0

    .line 39
    invoke-virtual {v3}, Lazk;->b()I

    move-result v3

    int-to-long v4, v3

    mul-long/2addr v4, p2

    aput-wide v4, v2, v0

    .line 40
    if-lez v0, :cond_0

    iget-object v2, p0, Lazj;->b:[J

    add-int/lit8 v3, v0, -0x1

    aget-wide v2, v2, v3

    iget-object v4, p0, Lazj;->b:[J

    aget-wide v4, v4, v0

    cmp-long v2, v2, v4

    if-ltz v2, :cond_0

    move v2, v1

    :goto_1
    const-string v3, "The animation phases must be sorted according to their start time"

    invoke-static {v2, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 37
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 40
    :cond_0
    const/4 v2, 0x1

    goto :goto_1

    .line 44
    :cond_1
    int-to-long v0, p1

    mul-long/2addr v0, p2

    iput-wide v0, p0, Lazj;->c:J

    .line 45
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lazj;->d:Landroid/graphics/Matrix;

    .line 46
    iput p4, p0, Lazj;->e:F

    .line 47
    return-void
.end method

.method private A(Layl;)F
    .locals 8

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    .line 105
    invoke-direct {p0, p1}, Lazj;->B(Layl;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 114
    :goto_0
    return v0

    .line 108
    :cond_0
    iget-wide v2, p1, Layl;->a:J

    iget-wide v4, p0, Lazj;->c:J

    rem-long/2addr v2, v4

    .line 109
    invoke-direct {p0, p1}, Lazj;->C(Layl;)I

    move-result v1

    .line 110
    iget-object v4, p0, Lazj;->b:[J

    aget-wide v4, v4, v1

    .line 111
    invoke-direct {p0, v1}, Lazj;->b(I)J

    move-result-wide v6

    .line 112
    long-to-float v1, v2

    mul-float/2addr v0, v1

    long-to-float v1, v4

    sub-float/2addr v0, v1

    sub-long v2, v6, v4

    long-to-float v1, v2

    div-float/2addr v0, v1

    .line 114
    float-to-double v0, v0

    const-wide v2, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    double-to-float v0, v0

    goto :goto_0
.end method

.method private B(Layl;)Z
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lazj;->a:[Lazk;

    invoke-direct {p0, p1}, Lazj;->C(Layl;)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lazk;->c()Z

    move-result v0

    return v0
.end method

.method private C(Layl;)I
    .locals 4

    .prologue
    .line 122
    iget-wide v0, p1, Layl;->a:J

    iget-wide v2, p0, Lazj;->c:J

    rem-long/2addr v0, v2

    .line 124
    :goto_0
    iget v2, p0, Lazj;->f:I

    invoke-direct {p0, v2}, Lazj;->b(I)J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-lez v2, :cond_0

    iget v2, p0, Lazj;->f:I

    .line 125
    iget-object v3, p0, Lazj;->b:[J

    aget-wide v2, v3, v2

    cmp-long v2, v2, v0

    if-lez v2, :cond_1

    .line 126
    :cond_0
    iget v2, p0, Lazj;->f:I

    .line 127
    invoke-direct {p0, v2}, Lazj;->a(I)I

    move-result v2

    iput v2, p0, Lazj;->f:I

    goto :goto_0

    .line 129
    :cond_1
    iget v0, p0, Lazj;->f:I

    return v0
.end method

.method private D(Layl;)I
    .locals 1

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lazj;->C(Layl;)I

    move-result v0

    invoke-direct {p0, v0}, Lazj;->a(I)I

    move-result v0

    return v0
.end method

.method private a(I)I
    .locals 2

    .prologue
    .line 137
    add-int/lit8 v0, p1, 0x1

    iget-object v1, p0, Lazj;->b:[J

    array-length v1, v1

    rem-int/2addr v0, v1

    return v0
.end method

.method private b(I)J
    .locals 2

    .prologue
    .line 145
    iget-object v0, p0, Lazj;->b:[J

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lazj;->b:[J

    add-int/lit8 v1, p1, 0x1

    aget-wide v0, v0, v1

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lazj;->c:J

    goto :goto_0
.end method


# virtual methods
.method public f(Layl;)I
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lazj;->a:[Lazk;

    invoke-direct {p0, p1}, Lazj;->C(Layl;)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lazk;->a()I

    move-result v0

    return v0
.end method

.method public f_()Ljava/util/List;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 95
    iget-object v2, p0, Lazj;->a:[Lazk;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 96
    invoke-virtual {v4}, Lazk;->a()I

    move-result v4

    .line 97
    if-eqz v4, :cond_0

    .line 98
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 101
    :cond_1
    return-object v1
.end method

.method public g(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lazj;->d:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public h(Layl;)F
    .locals 2

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lazj;->A(Layl;)F

    move-result v0

    iget v1, p0, Lazj;->e:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public i(Layl;)I
    .locals 2

    .prologue
    .line 74
    iget-object v0, p0, Lazj;->a:[Lazk;

    invoke-direct {p0, p1}, Lazj;->C(Layl;)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lazk;->d()I

    move-result v0

    return v0
.end method

.method public j(Layl;)I
    .locals 2

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lazj;->B(Layl;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    const/4 v0, 0x0

    .line 59
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lazj;->a:[Lazk;

    invoke-direct {p0, p1}, Lazj;->D(Layl;)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lazk;->a()I

    move-result v0

    goto :goto_0
.end method

.method public k(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lazj;->d:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public l(Layl;)F
    .locals 2

    .prologue
    .line 69
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, p1}, Lazj;->A(Layl;)F

    move-result v1

    sub-float/2addr v0, v1

    iget v1, p0, Lazj;->e:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public m(Layl;)I
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lazj;->a:[Lazk;

    invoke-direct {p0, p1}, Lazj;->D(Layl;)I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Lazk;->d()I

    move-result v0

    return v0
.end method
