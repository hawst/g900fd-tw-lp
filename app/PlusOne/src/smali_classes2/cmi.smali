.class public final Lcmi;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:I


# instance fields
.field private b:Lcmk;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 28
    const/high16 v1, -0x80000000

    .line 29
    invoke-static {}, Landroid/view/InputDevice;->getDeviceIds()[I

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 30
    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 29
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 32
    :cond_0
    add-int/lit8 v0, v1, 0x1

    sput v0, Lcmi;->a:I

    .line 33
    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    return-void
.end method

.method public static a()Lcmi;
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcmi;

    invoke-direct {v0}, Lcmi;-><init>()V

    return-object v0
.end method


# virtual methods
.method public a(Landroid/app/Activity;Lcmj;Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcmj;",
            "Ljava/util/List",
            "<",
            "Lcmd;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 53
    invoke-virtual {p0, v1}, Lcmi;->a(Z)V

    .line 54
    if-eqz p3, :cond_0

    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    new-instance v0, Lcmk;

    invoke-direct {v0, p1, p2, p3}, Lcmk;-><init>(Landroid/app/Activity;Lcmj;Ljava/util/List;)V

    iput-object v0, p0, Lcmi;->b:Lcmk;

    .line 58
    iget-object v0, p0, Lcmi;->b:Lcmk;

    invoke-virtual {v0, v1}, Lcmk;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcmi;->b:Lcmk;

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lcmi;->b:Lcmk;

    invoke-virtual {v0, p1}, Lcmk;->a(Z)V

    .line 47
    const/4 v0, 0x0

    iput-object v0, p0, Lcmi;->b:Lcmk;

    .line 49
    :cond_0
    return-void
.end method
