.class public final Lbzn;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbzp;


# static fields
.field public static final a:Lbzq;


# instance fields
.field private final b:Lama;

.field private final c:Lbza;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lbzo;

    invoke-direct {v0}, Lbzo;-><init>()V

    sput-object v0, Lbzn;->a:Lbzq;

    return-void
.end method

.method protected constructor <init>(Lama;Lbza;)V
    .locals 2

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput-object p2, p0, Lbzn;->c:Lbza;

    .line 51
    const-string v0, "clipIdentifierGenerator"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lama;

    iput-object v0, p0, Lbzn;->b:Lama;

    .line 53
    return-void
.end method


# virtual methods
.method public a(Ljava/util/Map;Ljava/util/Map;Ljava/util/List;Ljava/util/List;)Lboj;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbmb;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcac;",
            ">;)",
            "Lboj;"
        }
    .end annotation

    .prologue
    .line 113
    new-instance v0, Lboj;

    invoke-direct {v0}, Lboj;-><init>()V

    .line 114
    invoke-interface {p1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-virtual {v0, v1}, Lboj;->a(Ljava/util/Collection;)Lboj;

    move-result-object v0

    .line 115
    invoke-virtual {v0, p3}, Lboj;->a(Ljava/util/List;)Lboj;

    move-result-object v0

    .line 116
    invoke-virtual {v0, p4}, Lboj;->c(Ljava/util/List;)Lboj;

    move-result-object v0

    iget-object v1, p0, Lbzn;->c:Lbza;

    iget-object v1, v1, Lbza;->r:Layj;

    .line 117
    invoke-virtual {v0, v1}, Lboj;->a(Layj;)Lboj;

    move-result-object v1

    .line 119
    invoke-static {p1, p2, p3, p4}, Lbzr;->a(Ljava/util/Map;Ljava/util/Map;Ljava/util/List;Ljava/util/List;)Landroid/util/Pair;

    move-result-object v2

    .line 121
    iget-object v0, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Lbma;

    invoke-virtual {v1, v0}, Lboj;->a(Lbma;)Lboj;

    .line 122
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Lbma;

    invoke-virtual {v1, v0}, Lboj;->a(Lbma;)Lboj;

    .line 126
    :cond_0
    return-object v1
.end method

.method public a(Ljava/util/Map;Ljava/util/Map;Ljava/util/List;ZLbmd;Lbmd;)Lboj;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbmb;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;Z",
            "Lbmd;",
            "Lbmd;",
            ")",
            "Lboj;"
        }
    .end annotation

    .prologue
    .line 66
    invoke-interface {p3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    const/4 v0, 0x0

    .line 75
    :goto_0
    return-object v0

    .line 70
    :cond_0
    invoke-virtual {p0, p3, p4, p5, p6}, Lbzn;->a(Ljava/util/List;ZLbmd;Lbmd;)Ljava/util/List;

    move-result-object v0

    .line 73
    invoke-virtual {p0, p1, v0}, Lbzn;->a(Ljava/util/Map;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 75
    invoke-virtual {p0, p1, p2, v0, v1}, Lbzn;->a(Ljava/util/Map;Ljava/util/Map;Ljava/util/List;Ljava/util/List;)Lboj;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/util/List;ZLbmd;Lbmd;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;Z",
            "Lbmd;",
            "Lbmd;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 84
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 87
    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 88
    invoke-interface {v2, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    :cond_0
    invoke-interface {v2, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 95
    if-eqz p4, :cond_1

    .line 96
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 97
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-object v0, v0, Lbmd;->d:Lbmg;

    sget-object v3, Lbmg;->e:Lbmg;

    if-eq v0, v3, :cond_2

    .line 98
    add-int/lit8 v0, v1, 0x1

    invoke-interface {v2, v0, p4}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 104
    :cond_1
    return-object v2

    .line 96
    :cond_2
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0
.end method

.method protected a(Ljava/util/Map;Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lcac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 153
    const-string v0, "displayableByMediaId"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 154
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 155
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 180
    :goto_0
    return-object v0

    .line 157
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 162
    invoke-static {p1, p2}, Lbzr;->a(Ljava/util/Map;Ljava/util/List;)Z

    move-result v3

    .line 163
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v1, v0, :cond_4

    .line 164
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-object v4, v0, Lbmd;->d:Lbmg;

    .line 165
    add-int/lit8 v0, v1, 0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-object v0, v0, Lbmd;->d:Lbmg;

    .line 168
    if-eqz v3, :cond_2

    sget-object v5, Lbmg;->a:Lbmg;

    if-eq v4, v5, :cond_1

    sget-object v4, Lbmg;->a:Lbmg;

    if-ne v0, v4, :cond_2

    .line 170
    :cond_1
    invoke-static {}, Lcaa;->a()Lcaa;

    move-result-object v0

    .line 177
    :goto_2
    invoke-interface {v2, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 163
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 172
    :cond_2
    invoke-static {p2, v1}, Lbyi;->a(Ljava/util/List;I)J

    move-result-wide v4

    .line 174
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-nez v0, :cond_3

    .line 175
    invoke-static {}, Lcaa;->a()Lcaa;

    move-result-object v0

    goto :goto_2

    :cond_3
    new-instance v0, Lcab;

    invoke-direct {v0, v4, v5}, Lcab;-><init>(J)V

    goto :goto_2

    :cond_4
    move-object v0, v2

    .line 180
    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 131
    if-ne p1, p0, :cond_1

    .line 138
    :cond_0
    :goto_0
    return v0

    .line 133
    :cond_1
    instance-of v2, p1, Lbzn;

    if-nez v2, :cond_2

    move v0, v1

    .line 134
    goto :goto_0

    .line 136
    :cond_2
    check-cast p1, Lbzn;

    .line 137
    iget-object v2, p1, Lbzn;->b:Lama;

    iget-object v3, p0, Lbzn;->b:Lama;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lbzn;->c:Lbza;

    iget-object v3, p1, Lbzn;->c:Lbza;

    .line 138
    invoke-virtual {v2, v3}, Lbza;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lbzn;->b:Lama;

    iget-object v1, p0, Lbzn;->c:Lbza;

    invoke-virtual {v1}, Lbza;->hashCode()I

    move-result v1

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method
