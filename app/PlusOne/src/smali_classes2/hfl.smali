.class public final Lhfl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lher;


# instance fields
.field private final a:Landroid/accounts/AccountManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lhfl;->a:Landroid/accounts/AccountManager;

    .line 55
    return-void
.end method

.method static synthetic a(Lhfl;Lu;Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 23
    invoke-virtual {p1}, Lu;->q()Lae;

    move-result-object v1

    const-string v0, "new.account.launcher"

    invoke-virtual {v1, v0}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lhfo;

    if-nez v0, :cond_0

    new-instance v0, Lhfo;

    invoke-direct {v0}, Lhfo;-><init>()V

    invoke-virtual {v1}, Lae;->a()Lat;

    move-result-object v1

    const-string v2, "new.account.launcher"

    invoke-virtual {v1, v0, v2}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    move-result-object v1

    invoke-virtual {v1}, Lat;->c()I

    :cond_0
    new-instance v1, Lhfn;

    invoke-direct {v1, v0, p2}, Lhfn;-><init>(Lhfo;Landroid/content/Intent;)V

    invoke-static {v1}, Llsx;->a(Ljava/lang/Runnable;)V

    return-void
.end method


# virtual methods
.method public a(Lu;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Lu;",
            ":",
            "Lhes;",
            ">(TT;)V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 70
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 71
    const-string v0, "allowSkip"

    const/4 v1, 0x0

    invoke-virtual {v4, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 72
    iget-object v0, p0, Lhfl;->a:Landroid/accounts/AccountManager;

    const-string v1, "com.google"

    const-string v2, "webupdates"

    new-instance v6, Lhfm;

    invoke-direct {v6, p0, p1}, Lhfm;-><init>(Lhfl;Lu;)V

    move-object v5, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 93
    return-void
.end method

.method public a()[Ljava/lang/String;
    .locals 4

    .prologue
    .line 59
    iget-object v0, p0, Lhfl;->a:Landroid/accounts/AccountManager;

    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 60
    array-length v0, v1

    new-array v2, v0, [Ljava/lang/String;

    .line 61
    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 62
    aget-object v3, v1, v0

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v3, v2, v0

    .line 61
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64
    :cond_0
    return-object v2
.end method
