.class public final Laan;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Laak;",
            ">;"
        }
    .end annotation
.end field

.field b:[Laak;

.field c:Labx;

.field private d:Lacs;

.field private final e:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Laan;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/lang/Object;

.field private g:Laan;


# direct methods
.method constructor <init>(Lacs;Laan;)V
    .locals 1

    .prologue
    .line 608
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Laan;->a:Ljava/util/HashMap;

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Laan;->b:[Laak;

    .line 64
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Laan;->e:Ljava/util/HashSet;

    .line 67
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Laan;->f:Ljava/lang/Object;

    .line 609
    iput-object p1, p0, Laan;->d:Lacs;

    .line 610
    iget-object v0, p0, Laan;->d:Lacs;

    invoke-virtual {v0, p0}, Lacs;->a(Laan;)V

    .line 611
    if-eqz p2, :cond_0

    .line 612
    iput-object p2, p0, Laan;->g:Laan;

    .line 613
    iget-object v0, p0, Laan;->g:Laan;

    iget-object v0, v0, Laan;->e:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 615
    :cond_0
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 618
    iget-object v1, p0, Laan;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 619
    :try_start_0
    iget-object v0, p0, Laan;->e:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laan;

    .line 620
    invoke-direct {v0}, Laan;->g()V

    goto :goto_0

    .line 622
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 624
    iget-object v0, p0, Laan;->c:Labx;

    if-eqz v0, :cond_1

    .line 625
    iget-object v0, p0, Laan;->c:Labx;

    invoke-virtual {v0, p0}, Labx;->c(Laan;)V

    .line 627
    :cond_1
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Laak;
    .locals 1

    .prologue
    .line 379
    iget-object v0, p0, Laan;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laak;

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 351
    iget-object v0, p0, Laan;->g:Laan;

    if-eqz v0, :cond_0

    .line 352
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempting to tear down sub-graph!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 354
    :cond_0
    invoke-direct {p0}, Laan;->g()V

    .line 355
    return-void
.end method

.method public a(Labx;)V
    .locals 2

    .prologue
    .line 321
    iget-object v0, p0, Laan;->c:Labx;

    if-nez v0, :cond_2

    .line 322
    iget-object v0, p0, Laan;->e:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laan;

    .line 323
    invoke-virtual {v0, p1}, Laan;->a(Labx;)V

    goto :goto_0

    .line 325
    :cond_0
    invoke-virtual {p1, p0}, Labx;->b(Laan;)V

    .line 326
    iput-object p1, p0, Laan;->c:Labx;

    .line 331
    :cond_1
    return-void

    .line 327
    :cond_2
    iget-object v0, p0, Laan;->c:Labx;

    if-eq v0, p1, :cond_1

    .line 328
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot attach FilterGraph to GraphRunner that is already attached to another GraphRunner!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(Ljava/lang/String;)Landroidx/media/filterpacks/base/VariableSource;
    .locals 3

    .prologue
    .line 392
    iget-object v0, p0, Laan;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laak;

    .line 393
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroidx/media/filterpacks/base/VariableSource;

    if-eqz v1, :cond_0

    .line 394
    check-cast v0, Landroidx/media/filterpacks/base/VariableSource;

    return-object v0

    .line 396
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown variable \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' specified!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 361
    iget-object v0, p0, Laan;->g:Laan;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Labx;
    .locals 2

    .prologue
    .line 537
    iget-object v0, p0, Laan;->c:Labx;

    if-nez v0, :cond_0

    .line 538
    new-instance v0, Labx;

    iget-object v1, p0, Laan;->d:Lacs;

    invoke-direct {v0, v1}, Labx;-><init>(Lacs;)V

    .line 539
    invoke-virtual {p0, v0}, Laan;->a(Labx;)V

    .line 541
    :cond_0
    iget-object v0, p0, Laan;->c:Labx;

    return-object v0
.end method

.method public c(Ljava/lang/String;)Landroidx/media/filterpacks/base/GraphOutputTarget;
    .locals 3

    .prologue
    .line 407
    iget-object v0, p0, Laan;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laak;

    .line 408
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroidx/media/filterpacks/base/GraphOutputTarget;

    if-eqz v1, :cond_0

    .line 409
    check-cast v0, Landroidx/media/filterpacks/base/GraphOutputTarget;

    return-object v0

    .line 411
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown target \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' specified!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public d(Ljava/lang/String;)Landroidx/media/filterpacks/base/GraphInputSource;
    .locals 3

    .prologue
    .line 422
    iget-object v0, p0, Laan;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laak;

    .line 423
    if-eqz v0, :cond_0

    instance-of v1, v0, Landroidx/media/filterpacks/base/GraphInputSource;

    if-eqz v1, :cond_0

    .line 424
    check-cast v0, Landroidx/media/filterpacks/base/GraphInputSource;

    return-object v0

    .line 426
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown source \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' specified!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method d()[Laak;
    .locals 1

    .prologue
    .line 566
    iget-object v0, p0, Laan;->b:[Laak;

    return-object v0
.end method

.method e()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 586
    iget-object v1, p0, Laan;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 587
    :try_start_0
    iget-object v0, p0, Laan;->e:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 588
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 590
    iget-object v0, p0, Laan;->d:Lacs;

    invoke-virtual {v0, p0}, Lacs;->b(Laan;)V

    .line 591
    iput-object v2, p0, Laan;->b:[Laak;

    .line 592
    iput-object v2, p0, Laan;->a:Ljava/util/HashMap;

    .line 593
    iput-object v2, p0, Laan;->g:Laan;

    .line 594
    return-void

    .line 588
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method f()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 597
    iget-object v0, p0, Laan;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laak;

    .line 598
    iget-object v4, v0, Laak;->mConnectedInputPortArray:[Lacp;

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_1

    aget-object v6, v4, v2

    .line 599
    invoke-virtual {v6}, Lacp;->j()V

    .line 598
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 601
    :cond_1
    iget-object v2, v0, Laak;->mConnectedOutputPortArray:[Lacv;

    array-length v4, v2

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_0

    aget-object v5, v2, v0

    .line 602
    invoke-virtual {v5}, Lacv;->f()V

    .line 601
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 605
    :cond_2
    return-void
.end method
