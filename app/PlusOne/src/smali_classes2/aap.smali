.class public Laap;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lzp;

.field b:Z


# direct methods
.method constructor <init>(Labf;[ILaat;)V
    .locals 1

    .prologue
    .line 166
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Laap;->b:Z

    .line 167
    new-instance v0, Lzp;

    invoke-direct {v0, p1, p2, p3}, Lzp;-><init>(Labf;[ILaat;)V

    iput-object v0, p0, Laap;->a:Lzp;

    .line 168
    return-void
.end method

.method constructor <init>(Lzp;)V
    .locals 1

    .prologue
    .line 170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput-boolean v0, p0, Laap;->b:Z

    .line 171
    iput-object p1, p0, Laap;->a:Lzp;

    .line 172
    return-void
.end method

.method public static a(Labf;[I)Laap;
    .locals 2

    .prologue
    .line 137
    invoke-static {}, Laat;->a()Laat;

    move-result-object v0

    .line 138
    if-nez v0, :cond_0

    .line 139
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempting to create new Frame outside of FrameManager context!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 142
    :cond_0
    new-instance v1, Laap;

    invoke-direct {v1, p0, p1, v0}, Laap;-><init>(Labf;[ILaat;)V

    return-object v1
.end method


# virtual methods
.method a(Laat;)Laap;
    .locals 3

    .prologue
    .line 198
    new-instance v0, Laap;

    iget-object v1, p0, Laap;->a:Lzp;

    invoke-virtual {v1}, Lzp;->a()Labf;

    move-result-object v1

    invoke-virtual {p0}, Laap;->i()[I

    move-result-object v2

    invoke-direct {v0, v1, v2, p1}, Laap;-><init>(Labf;[ILaat;)V

    .line 199
    iget-object v1, v0, Laap;->a:Lzp;

    iget-object v2, p0, Laap;->a:Lzp;

    invoke-virtual {v1, v2}, Lzp;->a(Lzp;)V

    .line 200
    return-object v0
.end method

.method public final a()Labg;
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Laap;->a:Lzp;

    invoke-virtual {v0}, Lzp;->a()Labf;

    move-result-object v1

    invoke-static {v1}, Labg;->a(Labf;)V

    new-instance v1, Labg;

    invoke-direct {v1, v0}, Labg;-><init>(Lzp;)V

    return-object v1
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Laap;->a:Lzp;

    invoke-virtual {v0, p1, p2}, Lzp;->a(J)V

    .line 86
    return-void
.end method

.method a([I)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 186
    iget-object v1, p0, Laap;->a:Lzp;

    invoke-virtual {v1}, Lzp;->e()[I

    move-result-object v2

    .line 187
    if-nez v2, :cond_0

    move v1, v0

    .line 188
    :goto_0
    if-nez p1, :cond_1

    .line 189
    :goto_1
    if-eq v1, v0, :cond_2

    .line 190
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cannot resize "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "-dimensional Frame to "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-dimensional Frame!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 187
    :cond_0
    array-length v1, v2

    goto :goto_0

    .line 188
    :cond_1
    array-length v0, p1

    goto :goto_1

    .line 192
    :cond_2
    if-eqz p1, :cond_3

    invoke-static {v2, p1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    if-nez v0, :cond_3

    .line 193
    iget-object v0, p0, Laap;->a:Lzp;

    invoke-virtual {v0, p1}, Lzp;->a([I)V

    .line 195
    :cond_3
    return-void
.end method

.method public final b()Labh;
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Laap;->a:Lzp;

    invoke-virtual {v0}, Lzp;->a()Labf;

    move-result-object v1

    invoke-static {v1}, Labh;->a(Labf;)V

    new-instance v1, Labh;

    invoke-direct {v1, v0}, Labh;-><init>(Lzp;)V

    return-object v1
.end method

.method public final c()Laaq;
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Laap;->a:Lzp;

    invoke-static {v0}, Laaq;->a(Lzp;)V

    new-instance v1, Laaq;

    invoke-direct {v1, v0}, Laaq;-><init>(Lzp;)V

    return-object v1
.end method

.method public final d()Laar;
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Laap;->a:Lzp;

    invoke-static {v0}, Laar;->b(Lzp;)V

    new-instance v1, Laar;

    invoke-direct {v1, v0}, Laar;-><init>(Lzp;)V

    return-object v1
.end method

.method public final e()Laas;
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Laap;->a:Lzp;

    invoke-static {v0}, Laar;->b(Lzp;)V

    new-instance v1, Laas;

    invoke-direct {v1, v0}, Laas;-><init>(Lzp;)V

    return-object v1
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 133
    instance-of v0, p1, Laap;

    if-eqz v0, :cond_0

    check-cast p1, Laap;

    iget-object v0, p1, Laap;->a:Lzp;

    iget-object v1, p0, Laap;->a:Lzp;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Laap;
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Laap;->a:Lzp;

    invoke-virtual {v0}, Lzp;->d()Lzp;

    move-result-object v0

    iput-object v0, p0, Laap;->a:Lzp;

    .line 147
    iget-object v0, p0, Laap;->a:Lzp;

    if-eqz v0, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method public final g()Laap;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Laap;->a:Lzp;

    invoke-virtual {v0}, Lzp;->c()Lzp;

    move-result-object v0

    iput-object v0, p0, Laap;->a:Lzp;

    .line 152
    return-object p0
.end method

.method public h()V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Laap;->a:Lzp;

    invoke-virtual {v0}, Lzp;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 157
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempting to unlock frame that is not locked!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_0
    return-void
.end method

.method public i()[I
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Laap;->a:Lzp;

    invoke-virtual {v0}, Lzp;->e()[I

    move-result-object v0

    .line 163
    if-eqz v0, :cond_0

    array-length v1, v0

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 128
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Frame["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Laap;->a:Lzp;

    invoke-virtual {v1}, Lzp;->a()Labf;

    move-result-object v1

    invoke-virtual {v1}, Labf;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Laap;->a:Lzp;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
