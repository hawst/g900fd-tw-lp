.class public final Lhci;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:F

.field private final b:F

.field private final c:F

.field private final d:Landroid/graphics/Paint;

.field private final e:Landroid/graphics/Paint;

.field private final f:Landroid/graphics/Paint;

.field private final g:Landroid/graphics/Paint;

.field private h:I

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    const/4 v5, 0x1

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 43
    invoke-static {v5, v6, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    iput v1, p0, Lhci;->a:F

    .line 45
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lhci;->f:Landroid/graphics/Paint;

    .line 46
    iget-object v1, p0, Lhci;->f:Landroid/graphics/Paint;

    invoke-virtual {v1, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 47
    iget-object v1, p0, Lhci;->f:Landroid/graphics/Paint;

    const v2, 0x5fffffff

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 48
    iget-object v1, p0, Lhci;->f:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 49
    iget-object v1, p0, Lhci;->f:Landroid/graphics/Paint;

    iget v2, p0, Lhci;->a:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 50
    iget-object v1, p0, Lhci;->f:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 52
    invoke-static {v5, v6, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    iput v1, p0, Lhci;->b:F

    .line 54
    new-instance v1, Landroid/graphics/Paint;

    iget-object v2, p0, Lhci;->f:Landroid/graphics/Paint;

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v1, p0, Lhci;->g:Landroid/graphics/Paint;

    .line 55
    iget-object v1, p0, Lhci;->g:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 56
    iget-object v1, p0, Lhci;->g:Landroid/graphics/Paint;

    iget v2, p0, Lhci;->b:F

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 57
    iget-object v1, p0, Lhci;->g:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Join;->MITER:Landroid/graphics/Paint$Join;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    .line 59
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v5, v1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    .line 61
    new-instance v2, Landroid/graphics/Paint;

    iget-object v3, p0, Lhci;->f:Landroid/graphics/Paint;

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v2, p0, Lhci;->d:Landroid/graphics/Paint;

    .line 62
    iget-object v2, p0, Lhci;->d:Landroid/graphics/Paint;

    const v3, 0x3fffffff    # 1.9999999f

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 63
    iget-object v2, p0, Lhci;->d:Landroid/graphics/Paint;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 65
    invoke-static {v5, v6, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v2

    .line 67
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lhci;->e:Landroid/graphics/Paint;

    .line 68
    iget-object v3, p0, Lhci;->e:Landroid/graphics/Paint;

    const/high16 v4, 0x4f000000

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 69
    iget-object v3, p0, Lhci;->e:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 70
    iget-object v3, p0, Lhci;->e:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 72
    iget v2, p0, Lhci;->a:F

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    div-float/2addr v1, v6

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v1, v2

    iput v1, p0, Lhci;->h:I

    .line 74
    const/high16 v1, 0x41a00000    # 20.0f

    invoke-static {v5, v1, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    iput v0, p0, Lhci;->c:F

    .line 76
    return-void
.end method

.method private a(Landroid/graphics/Canvas;Landroid/graphics/RectF;IILandroid/graphics/Paint;)V
    .locals 10

    .prologue
    .line 128
    iget v0, p0, Lhci;->a:F

    const/high16 v1, 0x40000000    # 2.0f

    div-float v7, v0, v1

    .line 130
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v0

    int-to-float v1, p4

    div-float v8, v0, v1

    .line 131
    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v0

    int-to-float v1, p3

    div-float v9, v0, v1

    .line 132
    const/4 v0, 0x1

    move v6, v0

    :goto_0
    invoke-static {p3, p4}, Ljava/lang/Math;->max(II)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-gt v6, v0, :cond_2

    .line 133
    if-ge v6, p3, :cond_0

    .line 134
    iget v0, p2, Landroid/graphics/RectF;->top:F

    int-to-float v1, v6

    mul-float/2addr v1, v9

    add-float v2, v0, v1

    .line 135
    iget v0, p2, Landroid/graphics/RectF;->left:F

    add-float v1, v0, v7

    iget v0, p2, Landroid/graphics/RectF;->right:F

    sub-float v3, v0, v7

    move-object v0, p1

    move v4, v2

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 138
    :cond_0
    if-ge v6, p4, :cond_1

    .line 139
    iget v0, p2, Landroid/graphics/RectF;->left:F

    int-to-float v1, v6

    mul-float/2addr v1, v8

    add-float/2addr v1, v0

    .line 140
    iget v0, p2, Landroid/graphics/RectF;->top:F

    add-float v2, v0, v7

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    sub-float v4, v0, v7

    move-object v0, p1

    move v3, v1

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 132
    :cond_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 143
    :cond_2
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Lhci;->h:I

    return v0
.end method

.method public a(Landroid/graphics/Canvas;Landroid/graphics/RectF;II)V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/high16 v9, 0x40400000    # 3.0f

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v8, 0x40000000    # 2.0f

    .line 98
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpg-float v0, v0, v1

    if-ltz v0, :cond_0

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    .line 124
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    if-gtz p3, :cond_4

    move v3, v4

    .line 106
    :goto_1
    if-gtz p4, :cond_3

    .line 111
    :goto_2
    iget-object v5, p0, Lhci;->e:Landroid/graphics/Paint;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lhci;->a(Landroid/graphics/Canvas;Landroid/graphics/RectF;IILandroid/graphics/Paint;)V

    .line 114
    iget-object v5, p0, Lhci;->d:Landroid/graphics/Paint;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lhci;->a(Landroid/graphics/Canvas;Landroid/graphics/RectF;IILandroid/graphics/Paint;)V

    .line 117
    iget-object v0, p0, Lhci;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 120
    iget-boolean v0, p0, Lhci;->i:Z

    if-nez v0, :cond_0

    .line 121
    iget v6, p0, Lhci;->a:F

    iget v0, p0, Lhci;->b:F

    div-float v7, v0, v8

    iget v0, p2, Landroid/graphics/RectF;->left:F

    add-float v1, v0, v6

    iget v0, p2, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v6

    sub-float/2addr v0, v7

    iget v2, p0, Lhci;->c:F

    add-float/2addr v2, v0

    iget v0, p2, Landroid/graphics/RectF;->left:F

    add-float v3, v0, v6

    iget v0, p2, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v6

    sub-float v4, v0, v7

    iget-object v5, p0, Lhci;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v0, p2, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v6

    sub-float v1, v0, v7

    iget v0, p2, Landroid/graphics/RectF;->top:F

    add-float v2, v0, v6

    iget v0, p2, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v6

    sub-float/2addr v0, v7

    iget v3, p0, Lhci;->c:F

    add-float/2addr v3, v0

    iget v0, p2, Landroid/graphics/RectF;->top:F

    add-float v4, v0, v6

    iget-object v5, p0, Lhci;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v0, p2, Landroid/graphics/RectF;->right:F

    sub-float v1, v0, v6

    iget v0, p2, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v6

    sub-float/2addr v0, v7

    iget v2, p0, Lhci;->c:F

    add-float/2addr v2, v0

    iget v0, p2, Landroid/graphics/RectF;->right:F

    sub-float v3, v0, v6

    iget v0, p2, Landroid/graphics/RectF;->top:F

    add-float/2addr v0, v6

    sub-float v4, v0, v7

    iget-object v5, p0, Lhci;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v0, p2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v6

    add-float v1, v0, v7

    iget v0, p2, Landroid/graphics/RectF;->top:F

    add-float v2, v0, v6

    iget v0, p2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v6

    add-float/2addr v0, v7

    iget v3, p0, Lhci;->c:F

    sub-float v3, v0, v3

    iget v0, p2, Landroid/graphics/RectF;->top:F

    add-float v4, v0, v6

    iget-object v5, p0, Lhci;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v0, p2, Landroid/graphics/RectF;->right:F

    sub-float v1, v0, v6

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v6

    add-float/2addr v0, v7

    iget v2, p0, Lhci;->c:F

    sub-float v2, v0, v2

    iget v0, p2, Landroid/graphics/RectF;->right:F

    sub-float v3, v0, v6

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v6

    add-float v4, v0, v7

    iget-object v5, p0, Lhci;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v0, p2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v6

    add-float v1, v0, v7

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    sub-float v2, v0, v6

    iget v0, p2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v6

    add-float/2addr v0, v7

    iget v3, p0, Lhci;->c:F

    sub-float v3, v0, v3

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    sub-float v4, v0, v6

    iget-object v5, p0, Lhci;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v0, p2, Landroid/graphics/RectF;->left:F

    add-float v1, v0, v6

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v6

    add-float/2addr v0, v7

    iget v2, p0, Lhci;->c:F

    sub-float v2, v0, v2

    iget v0, p2, Landroid/graphics/RectF;->left:F

    add-float v3, v0, v6

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v0, v6

    add-float v4, v0, v7

    iget-object v5, p0, Lhci;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v0, p2, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v6

    sub-float v1, v0, v7

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    sub-float v2, v0, v6

    iget v0, p2, Landroid/graphics/RectF;->left:F

    add-float/2addr v0, v6

    sub-float/2addr v0, v7

    iget v3, p0, Lhci;->c:F

    add-float/2addr v3, v0

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    sub-float v4, v0, v6

    iget-object v5, p0, Lhci;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 122
    iget v6, p0, Lhci;->a:F

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v0

    iget v1, p0, Lhci;->c:F

    mul-float/2addr v1, v9

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_2

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerY()F

    move-result v7

    iget v0, p2, Landroid/graphics/RectF;->left:F

    add-float v1, v0, v6

    iget v0, p0, Lhci;->c:F

    div-float/2addr v0, v8

    sub-float v2, v7, v0

    iget v0, p2, Landroid/graphics/RectF;->left:F

    add-float v3, v0, v6

    iget v0, p0, Lhci;->c:F

    div-float/2addr v0, v8

    add-float v4, v7, v0

    iget-object v5, p0, Lhci;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v0, p2, Landroid/graphics/RectF;->right:F

    sub-float v1, v0, v6

    iget v0, p0, Lhci;->c:F

    div-float/2addr v0, v8

    sub-float v2, v7, v0

    iget v0, p2, Landroid/graphics/RectF;->right:F

    sub-float v3, v0, v6

    iget v0, p0, Lhci;->c:F

    div-float/2addr v0, v8

    add-float v4, v7, v0

    iget-object v5, p0, Lhci;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    :cond_2
    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget v1, p0, Lhci;->c:F

    mul-float/2addr v1, v9

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_0

    invoke-virtual {p2}, Landroid/graphics/RectF;->centerX()F

    move-result v7

    iget v0, p0, Lhci;->c:F

    div-float/2addr v0, v8

    sub-float v1, v7, v0

    iget v0, p2, Landroid/graphics/RectF;->top:F

    add-float v2, v0, v6

    iget v0, p0, Lhci;->c:F

    div-float/2addr v0, v8

    add-float v3, v7, v0

    iget v0, p2, Landroid/graphics/RectF;->top:F

    add-float v4, v0, v6

    iget-object v5, p0, Lhci;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget v0, p0, Lhci;->c:F

    div-float/2addr v0, v8

    sub-float v1, v7, v0

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    sub-float v2, v0, v6

    iget v0, p0, Lhci;->c:F

    div-float/2addr v0, v8

    add-float v3, v7, v0

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    sub-float v4, v0, v6

    iget-object v5, p0, Lhci;->g:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_3
    move v4, p4

    goto/16 :goto_2

    :cond_4
    move v3, p3

    goto/16 :goto_1
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 90
    iput-boolean p1, p0, Lhci;->i:Z

    .line 91
    return-void
.end method
