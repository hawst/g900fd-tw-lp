.class final Lbgl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbfw;


# instance fields
.field private final a:Lbgk;

.field private final b:I

.field private c:I

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private synthetic h:Lbgf;


# direct methods
.method constructor <init>(Lbgf;III)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1026
    invoke-static {p1}, Lbgf;->h(Lbgf;)Lbgk;

    move-result-object v0

    invoke-direct {p0, p1, v0, v2}, Lbgl;-><init>(Lbgf;Lbgk;Z)V

    .line 1027
    const-string v0, "width"

    invoke-static {p2, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lbgl;->e:I

    .line 1028
    const-string v0, "height"

    invoke-static {p3, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lbgl;->f:I

    .line 1029
    const-string v0, "filterMode"

    const/4 v1, 0x1

    invoke-static {p4, v0, v2, v1}, Lcec;->a(ILjava/lang/CharSequence;II)I

    move-result v0

    iput v0, p0, Lbgl;->g:I

    .line 1031
    iget v0, p0, Lbgl;->c:I

    iget v1, p0, Lbgl;->b:I

    iget v2, p0, Lbgl;->e:I

    iget v3, p0, Lbgl;->f:I

    invoke-static {v0, v1, v2, v3}, Lbgf;->a(IIII)V

    .line 1032
    return-void
.end method

.method constructor <init>(Lbgf;Landroid/graphics/Bitmap;I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1035
    invoke-static {p1}, Lbgf;->h(Lbgf;)Lbgk;

    move-result-object v0

    invoke-direct {p0, p1, v0, v2}, Lbgl;-><init>(Lbgf;Lbgk;Z)V

    .line 1036
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    const-string v1, "bitmap.getWidth()"

    invoke-static {v0, v1}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lbgl;->e:I

    .line 1037
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    const-string v1, "bitmap.getHeight()"

    invoke-static {v0, v1}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lbgl;->f:I

    .line 1038
    const-string v0, "filterMode"

    const/4 v1, 0x1

    invoke-static {p3, v0, v2, v1}, Lcec;->a(ILjava/lang/CharSequence;II)I

    move-result v0

    iput v0, p0, Lbgl;->g:I

    .line 1040
    iget v0, p0, Lbgl;->c:I

    iget v1, p0, Lbgl;->b:I

    invoke-static {v0, v1, p2}, Lbgf;->a(IILandroid/graphics/Bitmap;)V

    .line 1041
    return-void
.end method

.method private constructor <init>(Lbgf;Lbgk;Z)V
    .locals 2

    .prologue
    .line 1010
    iput-object p1, p0, Lbgl;->h:Lbgf;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1011
    const-string v0, "surfacePresentingRenderSink"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgk;

    iput-object v0, p0, Lbgl;->a:Lbgk;

    .line 1013
    iget-object v0, p0, Lbgl;->a:Lbgk;

    invoke-virtual {v0}, Lbgk;->d()I

    move-result v0

    iput v0, p0, Lbgl;->e:I

    .line 1014
    iget-object v0, p0, Lbgl;->a:Lbgk;

    invoke-virtual {v0}, Lbgk;->e()I

    move-result v0

    iput v0, p0, Lbgl;->f:I

    .line 1015
    const/4 v0, 0x1

    iput v0, p0, Lbgl;->g:I

    .line 1016
    iget-object v0, p0, Lbgl;->a:Lbgk;

    invoke-virtual {v0}, Lbgk;->g()V

    .line 1017
    invoke-static {}, Lbgf;->k()I

    move-result v0

    iput v0, p0, Lbgl;->c:I

    .line 1018
    if-eqz p3, :cond_0

    const v0, 0x8d65

    :goto_0
    iput v0, p0, Lbgl;->b:I

    .line 1020
    invoke-static {p1}, Lbgf;->i(Lbgf;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 1022
    return-void

    .line 1018
    :cond_0
    const/16 v0, 0xde1

    goto :goto_0
.end method

.method constructor <init>(Lbgf;Z)V
    .locals 1

    .prologue
    .line 1044
    invoke-static {p1}, Lbgf;->h(Lbgf;)Lbgk;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lbgl;-><init>(Lbgf;Lbgk;Z)V

    .line 1045
    return-void
.end method


# virtual methods
.method public a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Ljava/util/concurrent/Callable",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    const v4, 0x8d40

    const/4 v3, 0x0

    .line 1079
    iget v0, p0, Lbgl;->c:I

    const-string v1, "mTexId"

    const/4 v2, 0x0

    invoke-static {v0, v1, v3, v2}, Lcgp;->b(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)I

    .line 1081
    iget v0, p0, Lbgl;->d:I

    if-nez v0, :cond_0

    .line 1082
    iget-object v0, p0, Lbgl;->a:Lbgk;

    invoke-virtual {v0}, Lbgk;->g()V

    .line 1083
    invoke-static {}, Lbgf;->l()I

    move-result v0

    iput v0, p0, Lbgl;->d:I

    .line 1084
    iget v0, p0, Lbgl;->d:I

    invoke-static {v4, v0}, Landroid/opengl/GLES20;->glBindFramebuffer(II)V

    .line 1085
    const-string v0, "glBindFrameBuffer"

    invoke-static {v0}, Lbgc;->a(Ljava/lang/String;)V

    .line 1086
    const v0, 0x8ce0

    iget v1, p0, Lbgl;->b:I

    iget v2, p0, Lbgl;->c:I

    invoke-static {v4, v0, v1, v2, v3}, Landroid/opengl/GLES20;->glFramebufferTexture2D(IIIII)V

    .line 1092
    const-string v0, "glFramebufferTexture2D"

    invoke-static {v0}, Lbgc;->a(Ljava/lang/String;)V

    .line 1095
    :cond_0
    iget-object v0, p0, Lbgl;->a:Lbgk;

    iget v1, p0, Lbgl;->d:I

    invoke-virtual {v0, v1}, Lbgk;->a(I)V

    .line 1096
    iget v0, p0, Lbgl;->e:I

    iget v1, p0, Lbgl;->f:I

    invoke-static {v3, v3, v0, v1}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 1097
    const-string v0, "glViewport"

    invoke-static {v0}, Lbgc;->a(Ljava/lang/String;)V

    .line 1099
    invoke-interface {p1}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1104
    iget v0, p0, Lbgl;->c:I

    if-nez v0, :cond_0

    .line 1126
    :goto_0
    return-void

    .line 1108
    :cond_0
    iget-object v0, p0, Lbgl;->a:Lbgk;

    invoke-virtual {v0}, Lbgk;->g()V

    .line 1110
    iget v0, p0, Lbgl;->d:I

    if-eqz v0, :cond_1

    .line 1111
    new-array v0, v3, [I

    iget v1, p0, Lbgl;->d:I

    aput v1, v0, v2

    .line 1112
    invoke-static {v3, v0, v2}, Landroid/opengl/GLES20;->glDeleteFramebuffers(I[II)V

    .line 1113
    const-string v0, "glDeleteFramebuffers"

    invoke-static {v0}, Lbgc;->a(Ljava/lang/String;)V

    .line 1116
    :cond_1
    iget v0, p0, Lbgl;->c:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glIsTexture(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1117
    new-array v0, v3, [I

    iget v1, p0, Lbgl;->c:I

    aput v1, v0, v2

    .line 1118
    invoke-static {v3, v0, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 1119
    const-string v0, "glDeleteTextures"

    invoke-static {v0}, Lbgc;->a(Ljava/lang/String;)V

    .line 1122
    :cond_2
    iput v2, p0, Lbgl;->c:I

    .line 1123
    iget-object v0, p0, Lbgl;->h:Lbgf;

    invoke-static {v0}, Lbgf;->i(Lbgf;)Ljava/util/LinkedList;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 1125
    iget-object v0, p0, Lbgl;->h:Lbgf;

    invoke-static {v0}, Lbgf;->h(Lbgf;)Lbgk;

    move-result-object v0

    invoke-virtual {v0}, Lbgk;->g()V

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    .prologue
    .line 1049
    iget v0, p0, Lbgl;->b:I

    const v1, 0x8d65

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()I
    .locals 4

    .prologue
    .line 1069
    iget v0, p0, Lbgl;->c:I

    const-string v1, "mTexId"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcgp;->b(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)I

    move-result v0

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 1054
    iget v0, p0, Lbgl;->e:I

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 1059
    iget v0, p0, Lbgl;->f:I

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 1064
    const/4 v0, 0x0

    return v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 1074
    iget v0, p0, Lbgl;->g:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1130
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lbgl;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lbgl;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lbgl;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lbgl;->a:Lbgk;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget v3, p0, Lbgl;->b:I

    .line 1131
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget v3, p0, Lbgl;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 1130
    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1135
    return-object v0
.end method
