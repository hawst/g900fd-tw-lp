.class public final Lbkn;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:J

.field private final b:J

.field private final c:J

.field private final d:Lbbs;

.field private final e:Z


# direct methods
.method public constructor <init>(JJJLbbs;Z)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-wide p1, p0, Lbkn;->a:J

    .line 52
    iput-wide p3, p0, Lbkn;->b:J

    .line 53
    iput-wide p5, p0, Lbkn;->c:J

    .line 54
    iput-object p7, p0, Lbkn;->d:Lbbs;

    .line 55
    iput-boolean p8, p0, Lbkn;->e:Z

    .line 56
    return-void
.end method

.method static b(Lbvf;)J
    .locals 14

    .prologue
    const-wide/16 v10, 0x0

    const/4 v1, 0x0

    const-wide/16 v4, -0x1

    .line 255
    .line 257
    iget-boolean v0, p0, Lbvf;->f:Z

    if-nez v0, :cond_1

    move-wide v6, v10

    .line 276
    :cond_0
    :goto_0
    return-wide v6

    :cond_1
    move v0, v1

    move-wide v2, v4

    move-wide v6, v4

    .line 260
    :goto_1
    iget-object v8, p0, Lbvf;->c:[Lbvg;

    array-length v8, v8

    if-ge v0, v8, :cond_5

    .line 261
    iget-object v8, p0, Lbvf;->c:[Lbvg;

    aget-object v12, v8, v0

    .line 262
    iget-wide v8, v12, Lbvg;->b:J

    .line 263
    cmp-long v13, v6, v8

    if-gez v13, :cond_3

    .line 264
    iget-object v13, v12, Lbvg;->B:Lbvk;

    if-nez v13, :cond_2

    iget-object v12, v12, Lbvg;->c:Lbvn;

    if-eqz v12, :cond_4

    :cond_2
    const/4 v12, 0x1

    :goto_2
    if-eqz v12, :cond_3

    move-wide v2, v6

    move-wide v6, v8

    .line 260
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v12, v1

    .line 264
    goto :goto_2

    .line 269
    :cond_5
    cmp-long v0, v6, v4

    if-nez v0, :cond_6

    move-wide v6, v10

    .line 270
    goto :goto_0

    .line 272
    :cond_6
    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 276
    sub-long v0, v6, v2

    add-long/2addr v6, v0

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lbkn;->b:J

    return-wide v0
.end method

.method public a(Lbvf;)V
    .locals 5

    .prologue
    .line 59
    iget-wide v0, p0, Lbkn;->b:J

    iput-wide v0, p1, Lbvf;->b:J

    .line 60
    iget-wide v0, p0, Lbkn;->c:J

    iput-wide v0, p1, Lbvf;->g:J

    .line 61
    iget-boolean v0, p0, Lbkn;->e:Z

    iput-boolean v0, p1, Lbvf;->f:Z

    .line 62
    invoke-virtual {p0}, Lbkn;->d()Lbbs;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lbuz;

    invoke-direct {v1}, Lbuz;-><init>()V

    iget v2, v0, Lbbs;->b:F

    iput v2, v1, Lbuz;->a:F

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iget-object v0, v0, Lbbs;->a:Landroid/graphics/Bitmap;

    sget-object v3, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    iput-object v0, v1, Lbuz;->b:[B

    iput-object v1, p1, Lbvf;->e:Lbuz;

    .line 63
    :cond_0
    return-void
.end method

.method public b()J
    .locals 2

    .prologue
    .line 76
    iget-wide v0, p0, Lbkn;->c:J

    return-wide v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 83
    iget-wide v0, p0, Lbkn;->a:J

    return-wide v0
.end method

.method public d()Lbbs;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lbkn;->d:Lbbs;

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lbkn;->e:Z

    return v0
.end method
