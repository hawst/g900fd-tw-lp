.class public final Labf;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static e:Lacz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lacz",
            "<",
            "Ljava/lang/String;",
            "Labf;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 112
    new-instance v0, Lacz;

    const/16 v1, 0x40

    invoke-direct {v0, v1}, Lacz;-><init>(I)V

    sput-object v0, Labf;->e:Lacz;

    return-void
.end method

.method private constructor <init>(IIILjava/lang/Class;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/lang/Class",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 423
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 424
    iput p1, p0, Labf;->a:I

    .line 425
    iput p2, p0, Labf;->b:I

    .line 426
    iput-object p4, p0, Labf;->d:Ljava/lang/Class;

    .line 427
    iput p3, p0, Labf;->c:I

    .line 428
    return-void
.end method

.method public static a()Labf;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 120
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-static {v2, v0, v2, v1}, Labf;->a(IIILjava/lang/Class;)Labf;

    move-result-object v0

    return-object v0
.end method

.method public static a(I)Labf;
    .locals 3

    .prologue
    .line 164
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Labf;->a(IIILjava/lang/Class;)Labf;

    move-result-object v0

    return-object v0
.end method

.method public static a(II)Labf;
    .locals 2

    .prologue
    .line 183
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {p0, v0, p1, v1}, Labf;->a(IIILjava/lang/Class;)Labf;

    move-result-object v0

    return-object v0
.end method

.method private static a(IIILjava/lang/Class;)Labf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/lang/Class",
            "<*>;)",
            "Labf;"
        }
    .end annotation

    .prologue
    .line 414
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p3, :cond_1

    invoke-virtual {p3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 415
    sget-object v0, Labf;->e:Lacz;

    invoke-virtual {v0, v1}, Lacz;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labf;

    .line 416
    if-nez v0, :cond_0

    .line 417
    new-instance v0, Labf;

    invoke-direct {v0, p0, p1, p2, p3}, Labf;-><init>(IIILjava/lang/Class;)V

    .line 418
    sget-object v2, Labf;->e:Lacz;

    invoke-virtual {v2, v1, v0}, Lacz;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 420
    :cond_0
    return-object v0

    .line 414
    :cond_1
    const-string v0, "0"

    goto :goto_0
.end method

.method static a(Labf;Labf;)Labf;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 352
    iget v1, p0, Labf;->a:I

    if-nez v1, :cond_1

    move-object v0, p1

    .line 353
    :cond_0
    :goto_0
    if-nez v0, :cond_9

    .line 354
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Incompatible types in connection: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " vs. "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 352
    :cond_1
    iget v1, p1, Labf;->a:I

    if-nez v1, :cond_2

    move-object v0, p0

    goto :goto_0

    :cond_2
    iget v1, p0, Labf;->a:I

    if-ne v1, v3, :cond_8

    iget v1, p1, Labf;->a:I

    if-ne v1, v3, :cond_8

    iget v1, p0, Labf;->b:I

    iget v4, p1, Labf;->b:I

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v5

    iget-object v4, p0, Labf;->d:Ljava/lang/Class;

    iget-object v1, p1, Labf;->d:Ljava/lang/Class;

    if-nez v4, :cond_5

    move-object v4, v1

    :cond_3
    :goto_1
    if-nez v4, :cond_4

    iget-object v1, p0, Labf;->d:Ljava/lang/Class;

    if-nez v1, :cond_7

    :cond_4
    move v1, v3

    :goto_2
    if-eqz v1, :cond_0

    invoke-static {v3, v5, v2, v4}, Labf;->a(IIILjava/lang/Class;)Labf;

    move-result-object v0

    goto :goto_0

    :cond_5
    if-eqz v1, :cond_3

    invoke-virtual {v4, v1}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v6

    if-eqz v6, :cond_6

    move-object v4, v1

    goto :goto_1

    :cond_6
    invoke-virtual {v1, v4}, Ljava/lang/Class;->isAssignableFrom(Ljava/lang/Class;)Z

    move-result v1

    if-nez v1, :cond_3

    move-object v4, v0

    goto :goto_1

    :cond_7
    move v1, v2

    goto :goto_2

    :cond_8
    iget v1, p0, Labf;->b:I

    if-lez v1, :cond_0

    iget v1, p0, Labf;->a:I

    iget v2, p1, Labf;->a:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Labf;->b:I

    iget v2, p1, Labf;->b:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Labf;->c:I

    iget v2, p1, Labf;->c:I

    or-int/2addr v1, v2

    iget v2, p0, Labf;->a:I

    iget v3, p0, Labf;->b:I

    invoke-static {v2, v3, v1, v0}, Labf;->a(IIILjava/lang/Class;)Labf;

    move-result-object v0

    goto/16 :goto_0

    .line 357
    :cond_9
    return-object v0
.end method

.method public static a(Ljava/lang/Class;)Labf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Labf;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 138
    const/4 v0, 0x1

    invoke-static {v0, v1, v1, p0}, Labf;->a(IIILjava/lang/Class;)Labf;

    move-result-object v0

    return-object v0
.end method

.method public static b()Labf;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 129
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {v0, v2, v2, v1}, Labf;->a(IIILjava/lang/Class;)Labf;

    move-result-object v0

    return-object v0
.end method

.method public static b(I)Labf;
    .locals 3

    .prologue
    .line 173
    const/4 v0, 0x2

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Labf;->a(IIILjava/lang/Class;)Labf;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/Class;)Labf;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;)",
            "Labf;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 155
    const/4 v0, 0x0

    invoke-static {v1, v1, v0, p0}, Labf;->a(IIILjava/lang/Class;)Labf;

    move-result-object v0

    return-object v0
.end method

.method public static c()Labf;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 146
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-static {v2, v2, v0, v1}, Labf;->a(IIILjava/lang/Class;)Labf;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public d()Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 218
    iget-object v0, p0, Labf;->d:Ljava/lang/Class;

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 226
    iget v0, p0, Labf;->a:I

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 281
    instance-of v1, p1, Labf;

    if-eqz v1, :cond_0

    .line 282
    check-cast p1, Labf;

    .line 283
    iget v1, p0, Labf;->a:I

    iget v2, p1, Labf;->a:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Labf;->b:I

    iget v2, p1, Labf;->b:I

    if-ne v1, v2, :cond_0

    iget v1, p0, Labf;->c:I

    iget v2, p1, Labf;->c:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Labf;->d:Ljava/lang/Class;

    iget-object v2, p1, Labf;->d:Ljava/lang/Class;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 286
    :cond_0
    return v0
.end method

.method public f()I
    .locals 2

    .prologue
    const/4 v0, 0x4

    .line 235
    iget v1, p0, Labf;->a:I

    sparse-switch v1, :sswitch_data_0

    .line 248
    const/4 v0, 0x0

    :goto_0
    :sswitch_0
    return v0

    .line 237
    :sswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 239
    :sswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 235
    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_1
        0x65 -> :sswitch_2
        0x66 -> :sswitch_0
        0x67 -> :sswitch_0
        0xc8 -> :sswitch_0
        0xc9 -> :sswitch_0
        0x12d -> :sswitch_0
    .end sparse-switch
.end method

.method public g()I
    .locals 1

    .prologue
    .line 265
    iget v0, p0, Labf;->b:I

    return v0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 291
    iget v0, p0, Labf;->a:I

    iget v1, p0, Labf;->b:I

    xor-int/2addr v0, v1

    iget v1, p0, Labf;->c:I

    xor-int/2addr v0, v1

    iget-object v1, p0, Labf;->d:Ljava/lang/Class;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 296
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget v0, p0, Labf;->a:I

    iget-object v2, p0, Labf;->d:Ljava/lang/Class;

    sparse-switch v0, :sswitch_data_0

    const-string v0, "?"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Labf;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 297
    iget v1, p0, Labf;->c:I

    and-int/lit8 v1, v1, 0x1

    if-eqz v1, :cond_0

    .line 298
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(rcpu)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 300
    :cond_0
    iget v1, p0, Labf;->c:I

    and-int/lit8 v1, v1, 0x2

    if-eqz v1, :cond_1

    .line 301
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(rgpu)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 303
    :cond_1
    iget v1, p0, Labf;->c:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_2

    .line 304
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(ralloc)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 306
    :cond_2
    iget v1, p0, Labf;->c:I

    and-int/lit8 v1, v1, 0x8

    if-eqz v1, :cond_3

    .line 307
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(wcpu)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 309
    :cond_3
    iget v1, p0, Labf;->c:I

    and-int/lit8 v1, v1, 0x10

    if-eqz v1, :cond_4

    .line 310
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(wgpu)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 312
    :cond_4
    iget v1, p0, Labf;->c:I

    and-int/lit8 v1, v1, 0x20

    if-eqz v1, :cond_5

    .line 313
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(walloc)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 315
    :cond_5
    return-object v0

    .line 296
    :sswitch_0
    const-string v0, "int8"

    goto/16 :goto_0

    :sswitch_1
    const-string v0, "int16"

    goto/16 :goto_0

    :sswitch_2
    const-string v0, "int32"

    goto/16 :goto_0

    :sswitch_3
    const-string v0, "int64"

    goto/16 :goto_0

    :sswitch_4
    const-string v0, "float32"

    goto/16 :goto_0

    :sswitch_5
    const-string v0, "float64"

    goto/16 :goto_0

    :sswitch_6
    const-string v0, "rgba8888"

    goto/16 :goto_0

    :sswitch_7
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "<"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v2, :cond_6

    const-string v0, "*"

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ">"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :sswitch_8
    const-string v0, "*"

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_8
        0x1 -> :sswitch_7
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0x66 -> :sswitch_2
        0x67 -> :sswitch_3
        0xc8 -> :sswitch_4
        0xc9 -> :sswitch_5
        0x12d -> :sswitch_6
    .end sparse-switch
.end method
