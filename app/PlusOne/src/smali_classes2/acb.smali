.class final Lacb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lacj;


# instance fields
.field private a:I

.field private b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<[",
            "Laak;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Comparator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Comparator",
            "<",
            "Laak;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 718
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 720
    const/4 v0, 0x0

    iput v0, p0, Lacb;->a:I

    .line 721
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lacb;->b:Ljava/util/Set;

    .line 722
    new-instance v0, Lacc;

    invoke-direct {v0}, Lacc;-><init>()V

    iput-object v0, p0, Lacb;->c:Ljava/util/Comparator;

    return-void
.end method

.method private a(Ljava/util/List;Laak;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Laak;",
            ">;",
            "Laak;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 776
    iget-object v2, p2, Laak;->mConnectedInputPortArray:[Lacp;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 777
    invoke-virtual {v4}, Lacp;->a()Lacv;

    move-result-object v4

    .line 778
    if-eqz v4, :cond_0

    .line 779
    invoke-virtual {v4}, Lacv;->c()Laak;

    move-result-object v4

    .line 782
    if-eqz v4, :cond_0

    .line 783
    invoke-interface {p1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 788
    :goto_1
    return v0

    .line 776
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 788
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 768
    iget-object v0, p0, Lacb;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 769
    return-void
.end method

.method public a([Laak;Laci;)V
    .locals 7

    .prologue
    .line 747
    invoke-static {}, Labx;->v()I

    move-result v0

    int-to-long v2, v0

    .line 748
    const/4 v4, 0x0

    .line 749
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_2

    .line 750
    iget v1, p0, Lacb;->a:I

    aget-object v1, p1, v1

    .line 751
    iget v5, p0, Lacb;->a:I

    add-int/lit8 v5, v5, 0x1

    array-length v6, p1

    rem-int/2addr v5, v6

    iput v5, p0, Lacb;->a:I

    .line 752
    iget-object v5, v1, Laak;->mIsSleeping:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 753
    invoke-static {}, Labx;->u()I

    move-result v1

    int-to-long v2, v1

    .line 749
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 756
    :cond_1
    invoke-virtual {v1}, Laak;->o()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 758
    const-wide/16 v2, 0x0

    move-object v0, v1

    .line 762
    :goto_1
    iput-object v0, p2, Laci;->a:Laak;

    .line 763
    iput-wide v2, p2, Laci;->b:J

    .line 764
    return-void

    :cond_2
    move-object v0, v4

    goto :goto_1
.end method

.method public a([Laak;)[Laak;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 731
    iput v3, p0, Lacb;->a:I

    .line 732
    iget-object v0, p0, Lacb;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 737
    :goto_0
    return-object p1

    .line 735
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v5, Ljava/util/ArrayList;

    array-length v0, p1

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(I)V

    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    move v2, v3

    :goto_2
    if-ge v2, v1, :cond_1

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laak;

    invoke-direct {p0, v5, v0}, Lacb;->a(Ljava/util/List;Laak;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-interface {v4, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v1, -0x1

    :goto_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_2

    :cond_1
    iget-object v0, p0, Lacb;->c:Ljava/util/Comparator;

    invoke-static {v6, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-interface {v5, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_2
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Laak;

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laak;

    .line 736
    iget-object v1, p0, Lacb;->b:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object p1, v0

    .line 737
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_3
.end method
