.class public final Lfsk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lfsc;

.field private final b:Z

.field private final c:Landroid/view/View;

.field private synthetic d:Lfrw;


# direct methods
.method public constructor <init>(Lfrw;Lfsc;ZLandroid/view/View;)V
    .locals 0

    .prologue
    .line 306
    iput-object p1, p0, Lfsk;->d:Lfrw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 307
    iput-object p2, p0, Lfsk;->a:Lfsc;

    .line 308
    iput-boolean p3, p0, Lfsk;->b:Z

    .line 309
    iput-object p4, p0, Lfsk;->c:Landroid/view/View;

    .line 310
    return-void
.end method

.method private a(FFF)F
    .locals 3

    .prologue
    .line 369
    sub-float v1, p2, p1

    .line 370
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 371
    iget-object v2, p0, Lfsk;->a:Lfsc;

    iget-object v2, v2, Lfsc;->d:Landroid/view/animation/Interpolator;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lfsk;->a:Lfsc;

    iget-object v2, v2, Lfsc;->d:Landroid/view/animation/Interpolator;

    .line 372
    invoke-interface {v2, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v0

    .line 375
    :cond_0
    mul-float/2addr v0, v1

    add-float/2addr v0, p1

    return v0
.end method


# virtual methods
.method public a()Landroid/view/View;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lfsk;->c:Landroid/view/View;

    return-object v0
.end method

.method public a(F)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 329
    iget-boolean v0, p0, Lfsk;->b:Z

    if-eqz v0, :cond_0

    cmpl-float v0, p1, v1

    if-gtz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lfsk;->b:Z

    if-nez v0, :cond_2

    cmpg-float v0, p1, v1

    if-gez v0, :cond_2

    .line 362
    :cond_1
    return-void

    .line 335
    :cond_2
    iget-object v0, p0, Lfsk;->a:Lfsc;

    instance-of v0, v0, Lfsb;

    if-eqz v0, :cond_4

    .line 336
    iget-object v0, p0, Lfsk;->a:Lfsc;

    check-cast v0, Lfsb;

    .line 337
    iget-object v1, p0, Lfsk;->c:Landroid/view/View;

    iget v2, v0, Lfsb;->a:F

    iget v0, v0, Lfsb;->b:F

    invoke-direct {p0, v2, v0, p1}, Lfsk;->a(FFF)F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setAlpha(F)V

    .line 359
    :cond_3
    :goto_0
    iget-object v0, p0, Lfsk;->a:Lfsc;

    invoke-virtual {v0}, Lfsc;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 360
    iget-object v2, p0, Lfsk;->d:Lfrw;

    iget-object v3, p0, Lfsk;->c:Landroid/view/View;

    invoke-virtual {v2, v3, v0}, Lfrw;->a(Landroid/view/View;Ljava/lang/String;)V

    goto :goto_1

    .line 339
    :cond_4
    iget-object v0, p0, Lfsk;->a:Lfsc;

    instance-of v0, v0, Lfsn;

    if-eqz v0, :cond_5

    .line 340
    iget-object v0, p0, Lfsk;->a:Lfsc;

    check-cast v0, Lfsn;

    .line 342
    iget-object v1, p0, Lfsk;->c:Landroid/view/View;

    iget v2, v0, Lfsn;->a:I

    int-to-float v2, v2

    iget v3, v0, Lfsn;->f:I

    int-to-float v3, v3

    invoke-direct {p0, v2, v3, p1}, Lfsk;->a(FFF)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTranslationX(F)V

    .line 345
    iget-object v1, p0, Lfsk;->c:Landroid/view/View;

    iget v2, v0, Lfsn;->b:I

    int-to-float v2, v2

    iget v0, v0, Lfsn;->g:I

    int-to-float v0, v0

    invoke-direct {p0, v2, v0, p1}, Lfsk;->a(FFF)F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0

    .line 348
    :cond_5
    iget-object v0, p0, Lfsk;->a:Lfsc;

    instance-of v0, v0, Lfsh;

    if-eqz v0, :cond_3

    .line 349
    iget-object v0, p0, Lfsk;->a:Lfsc;

    check-cast v0, Lfsh;

    .line 351
    iget-object v1, p0, Lfsk;->c:Landroid/view/View;

    iget v2, v0, Lfsh;->a:F

    iget v3, v0, Lfsh;->b:F

    invoke-direct {p0, v2, v3, p1}, Lfsk;->a(FFF)F

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setScaleX(F)V

    .line 354
    iget-object v1, p0, Lfsk;->c:Landroid/view/View;

    iget v2, v0, Lfsh;->f:F

    iget v0, v0, Lfsh;->g:F

    invoke-direct {p0, v2, v0, p1}, Lfsk;->a(FFF)F

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setScaleY(F)V

    goto :goto_0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 319
    iget-object v0, p0, Lfsk;->a:Lfsc;

    invoke-virtual {v0}, Lfsc;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
