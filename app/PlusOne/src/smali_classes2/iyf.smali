.class public final Liyf;
.super Lloj;
.source "PG"

# interfaces
.implements Lag;
.implements Lhob;
.implements Llgs;


# instance fields
.field private Q:Ljava/lang/String;

.field private R:Ljava/lang/String;

.field private S:Landroid/view/ViewGroup;

.field private T:Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;

.field private U:I

.field private V:Z

.field private W:Z

.field private X:Liwa;

.field private Y:Liyj;

.field private final Z:Lhoc;

.field private aa:Lnpx;

.field private ab:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0}, Lloj;-><init>()V

    .line 86
    new-instance v0, Lhoc;

    iget-object v1, p0, Liyf;->P:Llqm;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    .line 87
    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    move-result-object v0

    iput-object v0, p0, Liyf;->Z:Lhoc;

    .line 86
    return-void
.end method

.method private W()I
    .locals 3

    .prologue
    .line 158
    invoke-virtual {p0}, Liyf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "dialog_state"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private X()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 317
    move v1, v2

    :goto_0
    iget-object v0, p0, Liyf;->S:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Liyf;->S:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lixo;

    invoke-virtual {v0}, Lixo;->d()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lixo;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    move v3, v4

    :goto_1
    move v1, v2

    .line 318
    :goto_2
    iget-object v0, p0, Liyf;->S:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 319
    iget-object v0, p0, Liyf;->S:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lixo;

    .line 320
    invoke-virtual {v0}, Lixo;->j()Ljava/lang/Integer;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 321
    invoke-virtual {v0}, Lixo;->j()Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-ne v5, v6, :cond_0

    .line 322
    if-nez v3, :cond_3

    move v5, v4

    :goto_3
    invoke-virtual {v0, v5}, Lixo;->a(Z)V

    .line 318
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 317
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1

    :cond_3
    move v5, v2

    .line 322
    goto :goto_3

    .line 326
    :cond_4
    iget-object v0, p0, Liyf;->T:Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_5
    :goto_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 327
    invoke-virtual {v0}, Landroid/widget/Button;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnpx;

    .line 328
    iget v1, v1, Lnpx;->b:I

    if-ne v1, v6, :cond_5

    .line 329
    if-nez v3, :cond_6

    move v1, v4

    :goto_5
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_5

    .line 332
    :cond_7
    return-void
.end method

.method private Y()V
    .locals 1

    .prologue
    .line 451
    invoke-direct {p0}, Liyf;->Z()V

    .line 452
    iget-object v0, p0, Liyf;->X:Liwa;

    invoke-interface {v0}, Liwa;->c()V

    .line 453
    return-void
.end method

.method private Z()V
    .locals 3

    .prologue
    .line 456
    invoke-virtual {p0}, Liyf;->p()Lae;

    move-result-object v0

    .line 457
    const-string v1, "oob.fragment"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lae;->a(Ljava/lang/String;I)V

    .line 458
    return-void
.end method

.method public static a(Lae;Ljava/lang/String;Ljava/lang/String;Lnql;IZ)V
    .locals 7

    .prologue
    .line 98
    const-string v6, "oob.fragment"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v6}, Liyf;->a(Lae;Ljava/lang/String;Ljava/lang/String;Lnql;IZLjava/lang/String;)V

    .line 100
    return-void
.end method

.method private static a(Lae;Ljava/lang/String;Ljava/lang/String;Lnql;IZLjava/lang/String;)V
    .locals 4

    .prologue
    .line 106
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 107
    const-string v1, "account_name"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v1, "effective_gaia_id"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const-string v1, "oob_resp"

    new-instance v2, Lhyt;

    invoke-direct {v2, p3}, Lhyt;-><init>(Loxu;)V

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 110
    const-string v1, "upgrade_origin"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 111
    const-string v1, "allow_non_google_account"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 113
    new-instance v1, Liyf;

    invoke-direct {v1}, Liyf;-><init>()V

    .line 114
    const/4 v2, 0x2

    const v3, 0x7f090065

    invoke-virtual {v1, v2, v3}, Liyf;->a(II)V

    .line 115
    invoke-virtual {v1, v0}, Liyf;->f(Landroid/os/Bundle;)V

    .line 116
    iget-object v0, p3, Lnql;->a:Lnqg;

    if-eqz v0, :cond_0

    iget-object v0, p3, Lnql;->a:Lnqg;

    iget-object v0, v0, Lnqg;->a:Lnpy;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, v0}, Liyf;->c(I)V

    .line 119
    invoke-virtual {p0}, Lae;->a()Lat;

    move-result-object v0

    .line 120
    invoke-virtual {v0, v1, p6}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    .line 121
    invoke-virtual {v0, p6}, Lat;->a(Ljava/lang/String;)Lat;

    .line 122
    invoke-virtual {v0}, Lat;->b()I

    .line 123
    return-void

    .line 116
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private aa()V
    .locals 2

    .prologue
    .line 467
    invoke-virtual {p0}, Liyf;->p()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->c()V

    .line 468
    const-string v0, "oob.fragment"

    invoke-virtual {p0}, Liyf;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Liyf;->ab:Z

    if-nez v0, :cond_0

    .line 469
    const/4 v0, 0x1

    iput-boolean v0, p0, Liyf;->ab:Z

    .line 470
    iget-object v0, p0, Liyf;->X:Liwa;

    invoke-interface {v0}, Liwa;->c()V

    .line 472
    :cond_0
    return-void
.end method

.method private b(Lnpx;)V
    .locals 10

    .prologue
    .line 390
    iput-object p1, p0, Liyf;->aa:Lnpx;

    .line 392
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 393
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Liyf;->S:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 394
    iget-object v0, p0, Liyf;->S:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lixo;

    .line 395
    invoke-virtual {v0}, Lixo;->c()Lnqa;

    move-result-object v2

    iget-object v2, v2, Lnqa;->c:Lnqe;

    if-eqz v2, :cond_0

    .line 396
    invoke-virtual {v0}, Lixo;->k()Lnqe;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 393
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 400
    :cond_1
    iget-object v0, p0, Liyf;->X:Liwa;

    iget-object v1, p0, Liyf;->N:Llnl;

    const v2, 0x7f0a01a7

    invoke-virtual {v1, v2}, Llnl;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Liwa;->b(Ljava/lang/String;)V

    .line 401
    iget-object v9, p0, Liyf;->Z:Lhoc;

    new-instance v0, Liyd;

    .line 402
    invoke-virtual {p0}, Liyf;->n()Lz;

    move-result-object v1

    const-string v2, "oob"

    iget-object v3, p0, Liyf;->Q:Ljava/lang/String;

    iget-object v4, p0, Liyf;->R:Ljava/lang/String;

    .line 403
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v6, v6, [Lnqe;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v6

    check-cast v6, [Lnqe;

    iget-boolean v7, p0, Liyf;->V:Z

    iget-boolean v8, p0, Liyf;->W:Z

    move-object v5, p1

    invoke-direct/range {v0 .. v8}, Liyd;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lnpx;[Lnqe;ZZ)V

    .line 401
    invoke-virtual {v9, v0}, Lhoc;->b(Lhny;)V

    .line 406
    return-void
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 162
    invoke-virtual {p0}, Liyf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "dialog_state"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 163
    return-void
.end method


# virtual methods
.method public A()V
    .locals 1

    .prologue
    .line 140
    invoke-super {p0}, Lloj;->A()V

    .line 141
    invoke-virtual {p0}, Liyf;->p()Lae;

    move-result-object v0

    invoke-virtual {v0, p0}, Lae;->b(Lag;)V

    .line 142
    return-void
.end method

.method public U()V
    .locals 2

    .prologue
    .line 383
    invoke-direct {p0}, Liyf;->W()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 384
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Liyf;->c(I)V

    .line 386
    :cond_0
    invoke-direct {p0}, Liyf;->aa()V

    .line 387
    return-void
.end method

.method public V()V
    .locals 0

    .prologue
    .line 437
    invoke-direct {p0}, Liyf;->X()V

    .line 438
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 168
    invoke-virtual {p0}, Liyf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liyf;->Q:Ljava/lang/String;

    .line 169
    invoke-virtual {p0}, Liyf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "effective_gaia_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Liyf;->R:Ljava/lang/String;

    .line 170
    invoke-virtual {p0}, Liyf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "allow_non_google_account"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Liyf;->V:Z

    .line 172
    invoke-virtual {p0}, Liyf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "oob_resp"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhyt;

    .line 173
    invoke-virtual {p0}, Liyf;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "upgrade_origin"

    const/high16 v3, -0x80000000

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Liyf;->U:I

    .line 175
    if-eqz p3, :cond_0

    .line 176
    const-string v1, "last_request"

    .line 177
    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lhyt;

    .line 178
    if-eqz v1, :cond_0

    .line 179
    new-instance v2, Lnpx;

    invoke-direct {v2}, Lnpx;-><init>()V

    invoke-virtual {v1, v2}, Lhyt;->a(Loxu;)Loxu;

    move-result-object v1

    check-cast v1, Lnpx;

    iput-object v1, p0, Liyf;->aa:Lnpx;

    .line 183
    :cond_0
    const v1, 0x7f040154

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 184
    const v1, 0x7f100470

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 185
    const v2, 0x7f100472

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    iput-object v2, p0, Liyf;->S:Landroid/view/ViewGroup;

    .line 186
    const v2, 0x7f100192

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;

    iput-object v2, p0, Liyf;->T:Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;

    .line 187
    new-instance v2, Liyg;

    iget-object v4, p0, Liyf;->S:Landroid/view/ViewGroup;

    iget-object v5, p0, Liyf;->T:Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;

    invoke-direct {v2, v1, v4, v5}, Liyg;-><init>(Landroid/view/ViewGroup;Landroid/view/ViewGroup;Lcom/google/android/libraries/social/login/plusi/impl/BottomActionBar;)V

    .line 190
    new-instance v1, Lnql;

    invoke-direct {v1}, Lnql;-><init>()V

    invoke-virtual {v0, v1}, Lhyt;->a(Loxu;)Loxu;

    move-result-object v0

    check-cast v0, Lnql;

    .line 191
    iget-object v1, v0, Lnql;->c:Ljava/lang/Boolean;

    invoke-static {v1}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v1

    iput-boolean v1, p0, Liyf;->W:Z

    .line 192
    iget-object v0, v0, Lnql;->a:Lnqg;

    invoke-virtual {v2, v0, p0}, Liyg;->a(Lnqg;Liyf;)V

    .line 193
    invoke-direct {p0}, Liyf;->X()V

    .line 195
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v0, v6, v6}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 196
    return-object v3
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 298
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 302
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 134
    invoke-super {p0, p1}, Lloj;->a(Landroid/os/Bundle;)V

    .line 135
    invoke-virtual {p0}, Liyf;->p()Lae;

    move-result-object v0

    invoke-virtual {v0, p0}, Lae;->a(Lag;)V

    .line 136
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 279
    const-string v0, "net_failure"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    iget-object v0, p0, Liyf;->aa:Lnpx;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Liyf;->aa:Lnpx;

    invoke-direct {p0, v0}, Liyf;->b(Lnpx;)V

    .line 284
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 411
    :try_start_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 418
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 430
    :pswitch_0
    new-instance v1, Lnpx;

    invoke-direct {v1}, Lnpx;-><init>()V

    .line 431
    iput v0, v1, Lnpx;->b:I

    .line 432
    invoke-direct {p0, v1}, Liyf;->b(Lnpx;)V

    .line 434
    :cond_0
    :goto_1
    return-void

    .line 413
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x3d

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unable to parse actionId: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", not calling action on this event."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 415
    const/4 v0, 0x1

    goto :goto_0

    .line 420
    :pswitch_1
    invoke-virtual {p0}, Liyf;->p()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 421
    invoke-direct {p0}, Liyf;->Y()V

    goto :goto_1

    .line 426
    :pswitch_2
    invoke-direct {p0}, Liyf;->Y()V

    goto :goto_1

    .line 418
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 220
    invoke-virtual {p3, v4}, Lhos;->a(Z)V

    .line 221
    invoke-virtual {p0}, Liyf;->p()Lae;

    move-result-object v0

    const-string v1, "net_failure"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lt;->a()V

    .line 223
    :cond_0
    if-nez p2, :cond_1

    .line 224
    invoke-direct {p0}, Liyf;->Y()V

    .line 260
    :goto_0
    return-void

    .line 228
    :cond_1
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 229
    iget-object v0, p0, Liyf;->X:Liwa;

    invoke-interface {v0}, Liwa;->e()V

    .line 230
    const v0, 0x7f0a01ab

    invoke-virtual {p0, v0}, Liyf;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a01ac

    invoke-virtual {p0, v1}, Liyf;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a01ad

    invoke-virtual {p0, v2}, Liyf;->e_(I)Ljava/lang/String;

    move-result-object v2

    const/high16 v3, 0x1040000

    invoke-virtual {p0, v3}, Liyf;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    invoke-virtual {v0, v4}, Lt;->b(Z)V

    invoke-virtual {v0, p0, v4}, Lt;->a(Lu;I)V

    invoke-virtual {p0}, Liyf;->p()Lae;

    move-result-object v1

    const-string v2, "net_failure"

    invoke-virtual {v0, v1, v2}, Lt;->a(Lae;Ljava/lang/String;)V

    goto :goto_0

    .line 234
    :cond_2
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    .line 235
    const-string v1, "oob_response"

    .line 236
    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhyt;

    .line 238
    const/4 v3, 0x0

    .line 239
    if-eqz v0, :cond_3

    .line 240
    new-instance v1, Lnql;

    invoke-direct {v1}, Lnql;-><init>()V

    invoke-virtual {v0, v1}, Lhyt;->a(Loxu;)Loxu;

    move-result-object v0

    check-cast v0, Lnql;

    move-object v3, v0

    .line 243
    :cond_3
    if-nez v3, :cond_4

    .line 244
    const/4 v0, 0x1

    iput-boolean v0, p0, Liyf;->ab:Z

    .line 245
    invoke-direct {p0}, Liyf;->Z()V

    .line 246
    iget-object v0, p0, Liyf;->Y:Liyj;

    invoke-interface {v0}, Liyj;->d()V

    goto :goto_0

    .line 250
    :cond_4
    iget-object v0, p0, Liyf;->X:Liwa;

    invoke-interface {v0}, Liwa;->e()V

    .line 252
    iget-object v0, v3, Lnql;->a:Lnqg;

    iget-object v0, v0, Lnqg;->a:Lnpy;

    if-eqz v0, :cond_5

    .line 253
    const-string v0, "oob.dialog"

    invoke-static {p0, v0, v3}, Liye;->a(Liyf;Ljava/lang/String;Lnql;)V

    goto :goto_0

    .line 255
    :cond_5
    invoke-virtual {p0}, Liyf;->p()Lae;

    move-result-object v0

    iget-object v1, p0, Liyf;->Q:Ljava/lang/String;

    iget-object v2, p0, Liyf;->R:Ljava/lang/String;

    iget v4, p0, Liyf;->U:I

    iget-boolean v5, p0, Liyf;->V:Z

    .line 258
    invoke-virtual {p0}, Liyf;->j()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    const-string v6, "-next"

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_6

    invoke-virtual {v7, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 255
    :goto_1
    invoke-static/range {v0 .. v6}, Liyf;->a(Lae;Ljava/lang/String;Ljava/lang/String;Lnql;IZLjava/lang/String;)V

    goto/16 :goto_0

    .line 258
    :cond_6
    new-instance v6, Ljava/lang/String;

    invoke-direct {v6, v7}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public a(Lnpx;)V
    .locals 2

    .prologue
    .line 348
    iget v0, p1, Lnpx;->b:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 351
    iget v0, p1, Lnpx;->b:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 352
    invoke-virtual {p0}, Liyf;->p()Lae;

    move-result-object v0

    invoke-virtual {v0}, Lae;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 353
    invoke-direct {p0}, Liyf;->Y()V

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 355
    :cond_1
    iget v0, p1, Lnpx;->b:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 356
    invoke-direct {p0}, Liyf;->Y()V

    goto :goto_0

    .line 358
    :cond_2
    invoke-direct {p0, p1}, Liyf;->b(Lnpx;)V

    goto :goto_0
.end method

.method public a(Lnql;Lnpx;)V
    .locals 10

    .prologue
    .line 363
    invoke-direct {p0}, Liyf;->W()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 364
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Liyf;->c(I)V

    .line 366
    :cond_0
    const/4 v6, 0x0

    .line 367
    iget-object v0, p1, Lnql;->a:Lnqg;

    iget-object v0, v0, Lnqg;->b:[Lnqa;

    if-eqz v0, :cond_1

    .line 368
    iget-object v0, p1, Lnql;->a:Lnqg;

    iget-object v0, v0, Lnqg;->b:[Lnqa;

    array-length v1, v0

    .line 369
    new-array v6, v1, [Lnqe;

    .line 370
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 371
    iget-object v2, p1, Lnql;->a:Lnqg;

    iget-object v2, v2, Lnqg;->b:[Lnqa;

    aget-object v2, v2, v0

    iget-object v2, v2, Lnqa;->c:Lnqe;

    aput-object v2, v6, v0

    .line 370
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 375
    :cond_1
    iget-object v0, p0, Liyf;->X:Liwa;

    iget-object v1, p0, Liyf;->N:Llnl;

    const v2, 0x7f0a01a7

    invoke-virtual {v1, v2}, Llnl;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Liwa;->b(Ljava/lang/String;)V

    .line 376
    iget-object v9, p0, Liyf;->Z:Lhoc;

    new-instance v0, Liyd;

    .line 377
    invoke-virtual {p0}, Liyf;->n()Lz;

    move-result-object v1

    const-string v2, "oob"

    iget-object v3, p0, Liyf;->Q:Ljava/lang/String;

    iget-object v4, p0, Liyf;->R:Ljava/lang/String;

    iget-boolean v7, p0, Liyf;->V:Z

    iget-boolean v8, p0, Liyf;->W:Z

    move-object v5, p2

    invoke-direct/range {v0 .. v8}, Liyd;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lnpx;[Lnqe;ZZ)V

    .line 376
    invoke-virtual {v9, v0}, Lhoc;->b(Lhny;)V

    .line 380
    return-void
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 201
    invoke-super {p0}, Lloj;->aO_()V

    .line 203
    invoke-direct {p0}, Liyf;->W()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 204
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Liyf;->c(I)V

    .line 205
    invoke-virtual {p0}, Liyf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "oob_resp"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhyt;

    .line 206
    new-instance v1, Lnql;

    invoke-direct {v1}, Lnql;-><init>()V

    invoke-virtual {v0, v1}, Lhyt;->a(Loxu;)Loxu;

    move-result-object v0

    check-cast v0, Lnql;

    .line 207
    const-string v1, "oob.dialog"

    invoke-static {p0, v1, v0}, Liye;->a(Liyf;Ljava/lang/String;Lnql;)V

    .line 209
    :cond_0
    return-void
.end method

.method public ah_()V
    .locals 2

    .prologue
    .line 146
    invoke-virtual {p0}, Liyf;->p()Lae;

    move-result-object v0

    .line 147
    invoke-virtual {v0}, Lae;->e()I

    move-result v1

    .line 148
    if-lez v1, :cond_0

    .line 149
    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lae;->b(I)Laf;

    move-result-object v0

    .line 151
    const-string v1, "oob.fragment"

    invoke-interface {v0}, Laf;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Liyf;->W()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 152
    invoke-direct {p0}, Liyf;->aa()V

    .line 155
    :cond_0
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 288
    invoke-direct {p0}, Liyf;->Y()V

    .line 289
    return-void
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 476
    invoke-super {p0, p1}, Lloj;->c(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 480
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 481
    return-object v0
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 293
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "OOB dialogs are not cancelable"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 213
    invoke-super {p0, p1}, Lloj;->e(Landroid/os/Bundle;)V

    .line 214
    const-string v0, "last_request"

    new-instance v1, Lhyt;

    iget-object v2, p0, Liyf;->aa:Lnpx;

    invoke-direct {v1, v2}, Lhyt;-><init>(Loxu;)V

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 215
    return-void
.end method

.method protected k(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 127
    invoke-super {p0, p1}, Lloj;->k(Landroid/os/Bundle;)V

    .line 128
    iget-object v0, p0, Liyf;->O:Llnh;

    const-class v1, Liwa;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwa;

    iput-object v0, p0, Liyf;->X:Liwa;

    .line 129
    iget-object v0, p0, Liyf;->O:Llnh;

    const-class v1, Liyj;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyj;

    iput-object v0, p0, Liyf;->Y:Liyj;

    .line 130
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 462
    invoke-super {p0, p1}, Lloj;->onCancel(Landroid/content/DialogInterface;)V

    .line 463
    invoke-direct {p0}, Liyf;->aa()V

    .line 464
    return-void
.end method
