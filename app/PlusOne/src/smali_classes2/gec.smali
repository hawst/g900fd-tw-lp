.class public final Lgec;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lgeb;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Content ID cannot be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lgeb;

    invoke-direct {v0, p1}, Lgeb;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lgec;->a:Lgeb;

    return-void
.end method


# virtual methods
.method public a()Lgeb;
    .locals 1

    iget-object v0, p0, Lgec;->a:Lgeb;

    invoke-virtual {v0}, Lgeb;->b()V

    iget-object v0, p0, Lgec;->a:Lgeb;

    return-object v0
.end method

.method public a(I)Lgec;
    .locals 1

    iget-object v0, p0, Lgec;->a:Lgeb;

    invoke-virtual {v0, p1}, Lgeb;->a(I)V

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lgec;
    .locals 1

    iget-object v0, p0, Lgec;->a:Lgeb;

    invoke-virtual {v0, p1}, Lgeb;->a(Ljava/lang/String;)V

    return-object p0
.end method
