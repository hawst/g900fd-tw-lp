.class public final Lbks;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/net/Uri;Lbkr;Lajl;)Lbkg;
    .locals 5

    .prologue
    .line 50
    invoke-interface {p1}, Lbkr;->d()Lbku;

    move-result-object v1

    .line 51
    invoke-interface {p1}, Lbkr;->e()Lbkp;

    move-result-object v2

    .line 52
    invoke-interface {p1}, Lbkr;->c()Lbkn;

    move-result-object v3

    .line 54
    new-instance v4, Lbvf;

    invoke-direct {v4}, Lbvf;-><init>()V

    .line 55
    if-nez p0, :cond_4

    const/4 v0, 0x0

    :goto_0
    iput-object v0, v4, Lbvf;->a:Ljava/lang/String;

    .line 56
    invoke-interface {p1}, Lbkr;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    invoke-interface {p1}, Lbkr;->b()[B

    move-result-object v0

    iput-object v0, v4, Lbvf;->h:[B

    .line 59
    :cond_0
    if-eqz v3, :cond_1

    .line 60
    invoke-virtual {v3, v4}, Lbkn;->a(Lbvf;)V

    .line 62
    :cond_1
    if-eqz v1, :cond_2

    .line 63
    invoke-virtual {v1, v4}, Lbku;->a(Lbvf;)V

    .line 65
    :cond_2
    if-eqz v2, :cond_3

    .line 66
    invoke-virtual {v2, v4}, Lbkp;->a(Lbvf;)V

    .line 69
    :cond_3
    invoke-interface {p2, p0, v4}, Lajl;->a(Landroid/net/Uri;Lbvf;)V

    .line 72
    new-instance v0, Lbkg;

    invoke-direct {v0, p0, p2}, Lbkg;-><init>(Landroid/net/Uri;Lajl;)V

    .line 73
    invoke-virtual {v0, p0}, Lbkg;->a(Landroid/net/Uri;)V

    .line 74
    return-object v0

    .line 55
    :cond_4
    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/net/Uri;Lajl;)Lbkr;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0, p1, p2}, Lbks;->b(Landroid/net/Uri;Lajl;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lbkg;

    invoke-direct {v0, p1, p2}, Lbkg;-><init>(Landroid/net/Uri;Lajl;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/net/Uri;Lajl;)Z
    .locals 1

    .prologue
    .line 38
    invoke-interface {p2, p1}, Lajl;->a(Landroid/net/Uri;)Lbvf;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
