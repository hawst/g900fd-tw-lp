.class public final Lakl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lajv;


# instance fields
.field private final c:[Lajj;

.field private final d:[Lajw;

.field private final e:[Lajj;

.field private final f:[Lajw;

.field private final g:Lakd;

.field private final h:Lakp;

.field private final i:Lcdu;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lald;Lamy;Lasy;Lacs;[Lajj;[Lajw;[Lajj;[Lajw;Lakq;Lcdu;Lbjf;)V
    .locals 7

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    const-string v1, "videoPostProcessors"

    const/4 v2, 0x0

    invoke-static {p6, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lajj;

    iput-object v1, p0, Lakl;->c:[Lajj;

    .line 51
    const-string v1, "videoSegmenters"

    const/4 v2, 0x0

    invoke-static {p7, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lajw;

    iput-object v1, p0, Lakl;->d:[Lajw;

    .line 52
    const-string v1, "photoPostProcessors"

    const/4 v2, 0x0

    invoke-static {p8, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lajj;

    iput-object v1, p0, Lakl;->e:[Lajj;

    .line 53
    const-string v1, "photoSegmenters"

    const/4 v2, 0x0

    move-object/from16 v0, p9

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lajw;

    iput-object v1, p0, Lakl;->f:[Lajw;

    .line 54
    new-instance v1, Lakd;

    move-object/from16 v0, p12

    invoke-direct {v1, p5, v0}, Lakd;-><init>(Lacs;Lbjf;)V

    iput-object v1, p0, Lakl;->g:Lakd;

    .line 55
    const-string v1, "analyticsSession"

    const/4 v2, 0x0

    move-object/from16 v0, p11

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcdu;

    iput-object v1, p0, Lakl;->i:Lcdu;

    .line 56
    new-instance v1, Lakp;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p10

    invoke-direct/range {v1 .. v6}, Lakp;-><init>(Landroid/content/Context;Lald;Lamy;Lasy;Lakq;)V

    iput-object v1, p0, Lakl;->h:Lakp;

    .line 62
    return-void
.end method

.method static synthetic a(Lakl;)Lcdu;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lakl;->i:Lcdu;

    return-object v0
.end method

.method static synthetic b(Lakl;)Lakp;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lakl;->h:Lakp;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lakl;->g:Lakd;

    invoke-virtual {v0}, Lakd;->a()V

    .line 92
    return-void
.end method

.method public a(Landroid/net/Uri;Laic;Lahy;)V
    .locals 9

    .prologue
    .line 66
    iget-object v0, p0, Lakl;->h:Lakp;

    invoke-virtual {v0, p1, p2}, Lakp;->a(Landroid/net/Uri;Laic;)V

    .line 67
    iget-object v0, p0, Lakl;->g:Lakd;

    iget-wide v2, p2, Laic;->b:J

    invoke-virtual {v0, v2, v3}, Lakd;->a(J)V

    .line 68
    iget-object v7, p0, Lakl;->g:Lakd;

    const/4 v8, 0x0

    new-instance v0, Lakm;

    iget-object v4, p0, Lakl;->c:[Lajj;

    iget-object v5, p0, Lakl;->d:[Lajw;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lakm;-><init>(Lakl;Laic;Lahy;[Lajj;[Lajw;B)V

    invoke-virtual {v7, p1, p2, v8, v0}, Lakd;->a(Landroid/net/Uri;Laic;Lbkr;Laki;)V

    .line 70
    return-void
.end method

.method public a(Landroid/net/Uri;Laic;Lbkr;Lahy;)V
    .locals 8

    .prologue
    .line 75
    iget-object v0, p0, Lakl;->h:Lakp;

    invoke-interface {p3}, Lbkr;->c()Lbkn;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lakp;->a(Landroid/net/Uri;Laic;Lbkn;)V

    .line 76
    iget-object v0, p0, Lakl;->g:Lakd;

    iget-wide v2, p2, Laic;->b:J

    invoke-virtual {v0, v2, v3}, Lakd;->a(J)V

    .line 77
    iget-object v7, p0, Lakl;->g:Lakd;

    new-instance v0, Lakm;

    iget-object v4, p0, Lakl;->c:[Lajj;

    iget-object v5, p0, Lakl;->d:[Lajw;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p2

    move-object v3, p4

    invoke-direct/range {v0 .. v6}, Lakm;-><init>(Lakl;Laic;Lahy;[Lajj;[Lajw;B)V

    invoke-virtual {v7, p1, p2, p3, v0}, Lakd;->a(Landroid/net/Uri;Laic;Lbkr;Laki;)V

    .line 79
    return-void
.end method

.method public b(Landroid/net/Uri;Laic;Lahy;)V
    .locals 9

    .prologue
    .line 83
    iget-object v0, p0, Lakl;->h:Lakp;

    invoke-virtual {v0, p1, p2}, Lakp;->a(Landroid/net/Uri;Laic;)V

    .line 84
    iget-object v0, p0, Lakl;->g:Lakd;

    iget-wide v2, p2, Laic;->b:J

    invoke-virtual {v0, v2, v3}, Lakd;->a(J)V

    .line 85
    iget-object v7, p0, Lakl;->g:Lakd;

    new-instance v8, Lbkm;

    invoke-direct {v8}, Lbkm;-><init>()V

    new-instance v0, Lakm;

    iget-object v4, p0, Lakl;->e:[Lajj;

    iget-object v5, p0, Lakl;->f:[Lajw;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lakm;-><init>(Lakl;Laic;Lahy;[Lajj;[Lajw;B)V

    invoke-virtual {v7, p1, v8, v0}, Lakd;->a(Landroid/net/Uri;Lbkm;Laki;)V

    .line 87
    return-void
.end method
