.class public final Lada;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:I

.field private b:I

.field private c:Z

.field private d:Z


# direct methods
.method private constructor <init>(IIZ)V
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x0

    iput-boolean v0, p0, Lada;->d:Z

    .line 116
    iput p1, p0, Lada;->a:I

    .line 117
    iput p2, p0, Lada;->b:I

    .line 118
    iput-boolean p3, p0, Lada;->c:Z

    .line 119
    return-void
.end method

.method public static a()Lada;
    .locals 4

    .prologue
    .line 41
    new-instance v0, Lada;

    invoke-static {}, Labi;->a()I

    move-result v1

    const/16 v2, 0xde1

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lada;-><init>(IIZ)V

    return-object v0
.end method

.method public static b()Lada;
    .locals 4

    .prologue
    .line 45
    new-instance v0, Lada;

    invoke-static {}, Labi;->a()I

    move-result v1

    const v2, 0x8d65

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lada;-><init>(IIZ)V

    return-object v0
.end method


# virtual methods
.method public a(II)V
    .locals 3

    .prologue
    .line 65
    iget v0, p0, Lada;->a:I

    iget v1, p0, Lada;->b:I

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1, p2}, Labi;->a(IILjava/nio/ByteBuffer;II)V

    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lada;->d:Z

    .line 67
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 77
    iget v0, p0, Lada;->a:I

    iget v1, p0, Lada;->b:I

    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glBindTexture(II)V

    invoke-static {v1, v2, p1, v2}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    const-string v0, "glTexImage2D"

    invoke-static {v0}, Labi;->a(Ljava/lang/String;)V

    invoke-static {}, Labi;->b()V

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lada;->d:Z

    .line 79
    return-void
.end method

.method public a(Ljava/nio/ByteBuffer;II)V
    .locals 2

    .prologue
    .line 71
    iget v0, p0, Lada;->a:I

    iget v1, p0, Lada;->b:I

    invoke-static {v0, v1, p1, p2, p3}, Labi;->a(IILjava/nio/ByteBuffer;II)V

    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lada;->d:Z

    .line 73
    return-void
.end method

.method public b(II)V
    .locals 2

    .prologue
    .line 91
    iget v0, p0, Lada;->b:I

    iget v1, p0, Lada;->a:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 92
    iget v0, p0, Lada;->b:I

    invoke-static {v0, p1, p2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 93
    iget v0, p0, Lada;->b:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 94
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lada;->a:I

    return v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lada;->b:I

    return v0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 59
    iget v0, p0, Lada;->b:I

    iget v1, p0, Lada;->a:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 60
    const-string v0, "glBindTexture"

    invoke-static {v0}, Labi;->a(Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public f()V
    .locals 3

    .prologue
    .line 82
    iget v0, p0, Lada;->b:I

    iget v1, p0, Lada;->a:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 83
    iget v0, p0, Lada;->b:I

    const/16 v1, 0x2801

    const/16 v2, 0x2703

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 86
    iget v0, p0, Lada;->b:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glGenerateMipmap(I)V

    .line 87
    iget v0, p0, Lada;->b:I

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 88
    return-void
.end method

.method public g()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 100
    iget v0, p0, Lada;->a:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glIsTexture(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lada;->c:Z

    if-eqz v0, :cond_0

    .line 101
    iget v0, p0, Lada;->a:I

    new-array v1, v3, [I

    aput v0, v1, v2

    const-string v0, "glDeleteTextures"

    invoke-static {v0}, Labi;->b(Ljava/lang/String;)V

    invoke-static {v3, v1, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    const-string v0, "glDeleteTextures"

    invoke-static {v0}, Labi;->a(Ljava/lang/String;)V

    .line 103
    :cond_0
    iput v2, p0, Lada;->a:I

    .line 104
    return-void
.end method

.method h()Z
    .locals 1

    .prologue
    .line 112
    iget-boolean v0, p0, Lada;->d:Z

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 108
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TextureSource(id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lada;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", target="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lada;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
