.class public final Lbag;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(DDLjava/util/Random;)D
    .locals 4

    .prologue
    .line 262
    invoke-virtual {p4}, Ljava/util/Random;->nextDouble()D

    move-result-wide v0

    .line 263
    sub-double v2, p2, p0

    mul-double/2addr v0, v2

    add-double/2addr v0, p0

    return-wide v0
.end method

.method public static a(FFI)F
    .locals 2

    .prologue
    .line 65
    invoke-static {p2}, Lbag;->a(I)F

    move-result v0

    .line 66
    sub-float v1, p1, p0

    mul-float/2addr v0, v1

    add-float/2addr v0, p0

    return v0
.end method

.method public static a(F[F)F
    .locals 4

    .prologue
    .line 274
    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    .line 275
    int-to-float v1, v0

    mul-float/2addr v1, p0

    float-to-int v1, v1

    .line 276
    if-gez v1, :cond_0

    .line 277
    const/4 v0, 0x0

    .line 283
    :goto_0
    return v0

    .line 279
    :cond_0
    int-to-float v2, v0

    mul-float/2addr v2, p0

    int-to-float v3, v1

    sub-float/2addr v2, v3

    .line 280
    if-lt v1, v0, :cond_1

    .line 281
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0

    .line 283
    :cond_1
    aget v0, p1, v1

    add-int/lit8 v1, v1, 0x1

    aget v1, p1, v1

    sub-float/2addr v1, v0

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    goto :goto_0
.end method

.method public static a(I)F
    .locals 3

    .prologue
    const v2, 0x3335b369

    .line 45
    ushr-int/lit8 v0, p0, 0x10

    xor-int/2addr v0, p0

    mul-int/2addr v0, v2

    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    mul-int/2addr v0, v2

    ushr-int/lit8 v1, v0, 0x10

    xor-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 46
    int-to-float v0, v0

    const/high16 v1, 0x4f000000

    div-float/2addr v0, v1

    return v0
.end method

.method public static a(Lbmj;)F
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 132
    iget v0, p0, Lbmj;->c:F

    sub-float v0, v2, v0

    iget v1, p0, Lbmj;->d:F

    sub-float/2addr v0, v1

    iget v1, p0, Lbmj;->a:F

    sub-float v1, v2, v1

    iget v2, p0, Lbmj;->b:F

    sub-float/2addr v1, v2

    div-float/2addr v0, v1

    return v0
.end method

.method public static a(III)I
    .locals 2

    .prologue
    .line 55
    invoke-static {p2}, Lbag;->a(I)F

    move-result v0

    .line 56
    sub-int v1, p1, p0

    int-to-float v1, v1

    mul-float/2addr v0, v1

    float-to-int v0, v0

    add-int/2addr v0, p0

    return v0
.end method

.method public static a(Landroid/graphics/Matrix;FF)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v1, 0x3f000000    # 0.5f

    .line 145
    invoke-virtual {p0, p2, v2, v1, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 146
    invoke-virtual {p0, p1, v1, v1}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 147
    div-float v0, v2, p2

    invoke-virtual {p0, v0, v2, v1, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 148
    return-void
.end method

.method public static a(ILook;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 247
    if-eqz p1, :cond_0

    iget-object v1, p1, Look;->b:[Ljava/lang/Integer;

    if-nez v1, :cond_1

    .line 257
    :cond_0
    :goto_0
    return v0

    .line 251
    :cond_1
    iget-object v2, p1, Look;->b:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 252
    if-ne v4, p0, :cond_2

    .line 253
    const/4 v0, 0x1

    goto :goto_0

    .line 251
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static a(Layl;)Z
    .locals 6

    .prologue
    .line 73
    iget-wide v0, p0, Layl;->d:J

    iget-wide v2, p0, Layl;->e:J

    iget-wide v4, p0, Layl;->o:J

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Layl;)F
    .locals 4

    .prologue
    .line 94
    iget-wide v0, p0, Layl;->d:J

    long-to-float v0, v0

    iget-wide v2, p0, Layl;->e:J

    long-to-float v1, v2

    div-float/2addr v0, v1

    return v0
.end method

.method public static c(Layl;)F
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 102
    iget v1, p0, Layl;->m:F

    cmpl-float v1, v1, v0

    if-nez v1, :cond_0

    .line 105
    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Layl;->d:J

    long-to-float v0, v0

    iget-wide v2, p0, Layl;->e:J

    iget-wide v4, p0, Layl;->o:J

    sub-long/2addr v2, v4

    long-to-float v1, v2

    sub-float/2addr v0, v1

    iget-wide v2, p0, Layl;->l:J

    long-to-float v1, v2

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method public static d(Layl;)Z
    .locals 2

    .prologue
    .line 151
    invoke-static {p0}, Lbag;->a(Layl;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Layl;->k:Lbmg;

    sget-object v1, Lbmg;->c:Lbmg;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e(Layl;)Z
    .locals 4

    .prologue
    .line 155
    iget-object v0, p0, Layl;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Layl;->r:Lbmp;

    iget-wide v2, p0, Layl;->a:J

    .line 156
    invoke-virtual {v0, v2, v3}, Lbmp;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
