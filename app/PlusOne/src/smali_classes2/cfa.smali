.class public Lcfa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcdt;


# instance fields
.field private a:Lagf;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcfb;",
            ">;"
        }
    .end annotation
.end field

.field private c:Lahb;

.field private d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcfa;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    invoke-static {p1}, Lagf;->a(Landroid/content/Context;)Lagf;

    move-result-object v0

    iput-object v0, p0, Lcfa;->a:Lagf;

    .line 91
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcfa;->b:Ljava/util/List;

    .line 96
    :goto_0
    return-void

    .line 93
    :cond_0
    iput-object v1, p0, Lcfa;->a:Lagf;

    .line 94
    iput-object v1, p0, Lcfa;->b:Ljava/util/List;

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 2

    .prologue
    .line 186
    iget-object v0, p0, Lcfa;->a:Lagf;

    if-nez v0, :cond_0

    .line 199
    :goto_0
    return-void

    .line 190
    :cond_0
    iget-object v0, p0, Lcfa;->c:Lahb;

    if-eqz v0, :cond_1

    .line 191
    iget-object v0, p0, Lcfa;->c:Lahb;

    invoke-virtual {v0, p1, p2, p3, p4}, Lahb;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0

    .line 197
    :cond_1
    iget-object v0, p0, Lcfa;->b:Ljava/util/List;

    new-instance v1, Lcfc;

    invoke-direct {v1, p1, p2, p3, p4}, Lcfc;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 148
    iget-object v0, p0, Lcfa;->a:Lagf;

    if-nez v0, :cond_0

    .line 161
    :goto_0
    return-void

    .line 152
    :cond_0
    iget-object v0, p0, Lcfa;->c:Lahb;

    if-eqz v0, :cond_1

    .line 153
    iget-object v0, p0, Lcfa;->c:Lahb;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lahb;->a(Z)V

    goto :goto_0

    .line 159
    :cond_1
    iget-object v0, p0, Lcfa;->b:Ljava/util/List;

    new-instance v1, Lcfe;

    invoke-direct {v1}, Lcfe;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcfa;->a:Lagf;

    if-nez v0, :cond_0

    .line 144
    :goto_0
    return-void

    .line 136
    :cond_0
    iget-object v0, p0, Lcfa;->c:Lahb;

    if-eqz v0, :cond_1

    .line 137
    iget-object v0, p0, Lcfa;->c:Lahb;

    invoke-virtual {v0, p1}, Lahb;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 142
    :cond_1
    iget-object v0, p0, Lcfa;->b:Ljava/util/List;

    new-instance v1, Lcfd;

    invoke-direct {v1, p1}, Lcfd;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 122
    const-string v0, ""

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcfa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 123
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3

    .prologue
    .line 127
    const-string v0, ""

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-direct {p0, p1, p2, v0, v1}, Lcfa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 128
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcfa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 113
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 2

    .prologue
    .line 117
    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-direct {p0, p1, p2, p3, v0}, Lcfa;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 118
    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 100
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcfa;->a:Lagf;

    .line 108
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    iget-object v0, p0, Lcfa;->a:Lagf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcfa;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 104
    iput-object p1, p0, Lcfa;->d:Ljava/lang/String;

    .line 105
    iget-object v0, p0, Lcfa;->a:Lagf;

    iget-object v1, p0, Lcfa;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lagf;->a(Ljava/lang/String;)Lahb;

    move-result-object v0

    iput-object v0, p0, Lcfa;->c:Lahb;

    .line 106
    iget-object v0, p0, Lcfa;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcfb;

    iget-object v2, p0, Lcfa;->c:Lahb;

    invoke-virtual {v0, v2}, Lcfb;->a(Lahb;)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcfa;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    goto :goto_0
.end method
