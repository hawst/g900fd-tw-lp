.class public Lbyo;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lbyo;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbyo;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 450
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/List;Ljava/util/List;ILjava/util/List;)Lbyr;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbyr;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbyq;",
            ">;I",
            "Ljava/util/List",
            "<",
            "Lbyn;",
            ">;)",
            "Lbyr;"
        }
    .end annotation

    .prologue
    .line 326
    const/4 v7, 0x0

    .line 327
    const/16 v5, 0xa

    .line 328
    const/4 v4, 0x0

    .line 329
    const/4 v3, 0x0

    .line 331
    sget-object v1, Lbyo;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x36

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "findAllMatchedTypes previousList: curbest: 0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 332
    const/4 v1, 0x0

    move v8, v1

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-ge v8, v1, :cond_2

    .line 333
    invoke-interface {p0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v2, v1

    check-cast v2, Lbyr;

    .line 334
    iget v1, v2, Lbyr;->b:I

    if-eqz v1, :cond_1

    .line 336
    sget-object v1, Lbyo;->a:Ljava/lang/String;

    invoke-virtual {v2}, Lbyr;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x12

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "ppp: "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v9, ": "

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 339
    iget v1, v2, Lbyr;->b:I

    add-int/lit8 v6, p2, 0x1

    invoke-interface {p1, v1, v6}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    .line 340
    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lbyo;->c(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 341
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbyn;

    .line 342
    sget-object v6, Lbyo;->a:Ljava/lang/String;

    invoke-virtual {v1}, Lbyn;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1}, Lbyn;->c()Ljava/util/List;

    move-result-object v10

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/String;->length()I

    move-result v12

    add-int/lit8 v12, v12, 0x6

    invoke-static {v10}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->length()I

    move-result v13

    add-int/2addr v12, v13

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v12, "tt: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v11, ": "

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 344
    invoke-virtual {v1}, Lbyn;->a()I

    move-result v6

    iget v10, v2, Lbyr;->d:I

    invoke-static {v6, v10}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 345
    new-instance v10, Ljava/util/HashSet;

    iget-object v11, v2, Lbyr;->c:Ljava/util/Set;

    invoke-direct {v10, v11}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 346
    sget-object v11, Lbyo;->a:Ljava/lang/String;

    invoke-interface {v10}, Ljava/util/Set;->size()I

    move-result v11

    new-instance v12, Ljava/lang/StringBuilder;

    const/16 v13, 0x10

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v13, "ddd: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 347
    invoke-virtual {v1}, Lbyn;->c()Ljava/util/List;

    move-result-object v11

    invoke-interface {v10, v11}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 350
    if-lt v6, v5, :cond_0

    if-ne v6, v5, :cond_a

    .line 351
    invoke-interface {v10}, Ljava/util/Set;->size()I

    move-result v11

    if-le v11, v4, :cond_a

    .line 353
    :cond_0
    invoke-interface {v10}, Ljava/util/Set;->size()I

    move-result v3

    move v4, v6

    move-object v5, v2

    :goto_2
    move-object v7, v5

    move v5, v4

    move v4, v3

    move-object v3, v1

    .line 357
    goto/16 :goto_1

    .line 332
    :cond_1
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto/16 :goto_0

    .line 361
    :cond_2
    sget-object v1, Lbyo;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x29

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "findAllMatchedTypes: curbest: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 362
    const/4 v1, 0x0

    add-int/lit8 v2, p2, 0x1

    .line 363
    invoke-interface {p1, v1, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lbyo;->c(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 364
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbyn;

    .line 365
    sget-object v2, Lbyo;->a:Ljava/lang/String;

    invoke-virtual {v1}, Lbyn;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lbyn;->c()Ljava/util/List;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/lit8 v10, v10, 0x6

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/2addr v10, v11

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v10, "tt: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v9, ": "

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 366
    invoke-virtual {v1}, Lbyn;->a()I

    move-result v2

    .line 367
    invoke-virtual {v1}, Lbyn;->c()Ljava/util/List;

    move-result-object v8

    .line 369
    if-lt v2, v5, :cond_3

    if-ne v2, v5, :cond_4

    .line 370
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    if-le v9, v4, :cond_4

    .line 372
    :cond_3
    const/4 v7, 0x0

    .line 373
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v4

    move-object v3, v1

    move v5, v2

    .line 376
    :cond_4
    sget-object v2, Lbyo;->a:Ljava/lang/String;

    const-string v2, "type: "

    invoke-virtual {v1}, Lbyn;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v8

    if-eqz v8, :cond_5

    invoke-virtual {v2, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_3

    :cond_5
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_3

    .line 380
    :cond_6
    new-instance v2, Lbyr;

    invoke-direct {v2}, Lbyr;-><init>()V

    .line 381
    if-nez v3, :cond_7

    move-object v1, v2

    .line 396
    :goto_4
    return-object v1

    .line 385
    :cond_7
    if-nez v7, :cond_8

    .line 386
    new-instance v1, Lbyp;

    const/4 v4, 0x0

    add-int/lit8 v5, p2, 0x1

    .line 387
    invoke-interface {p1, v4, v5}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v4, v3}, Lbyp;-><init>(Ljava/util/List;Lbyn;)V

    .line 386
    invoke-virtual {v2, v1}, Lbyr;->a(Lbyp;)V

    move-object v1, v2

    .line 388
    goto :goto_4

    .line 391
    :cond_8
    iget-object v1, v7, Lbyr;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbyp;

    .line 392
    invoke-virtual {v2, v1}, Lbyr;->a(Lbyp;)V

    goto :goto_5

    .line 394
    :cond_9
    new-instance v1, Lbyp;

    iget v4, v7, Lbyr;->b:I

    add-int/lit8 v5, p2, 0x1

    .line 395
    invoke-interface {p1, v4, v5}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v4, v3}, Lbyp;-><init>(Ljava/util/List;Lbyn;)V

    .line 394
    invoke-virtual {v2, v1}, Lbyr;->a(Lbyp;)V

    move-object v1, v2

    .line 396
    goto :goto_4

    :cond_a
    move-object v1, v3

    move v3, v4

    move v4, v5

    move-object v5, v7

    goto/16 :goto_2
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lbyn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 436
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 438
    const-class v0, Lbyn;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbyn;

    array-length v4, v0

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    aget-object v5, v0, v2

    .line 439
    invoke-virtual {v5}, Lbyn;->a()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 440
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 441
    invoke-virtual {v6, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 442
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 438
    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 447
    :cond_2
    return-object v3
.end method

.method private static a(Ljava/util/List;Lbyn;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbyq;",
            ">;",
            "Lbyn;",
            ")Z"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 413
    invoke-virtual {p1}, Lbyn;->d()Ljava/util/List;

    move-result-object v5

    .line 414
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 422
    :cond_0
    :goto_0
    return v3

    :cond_1
    move v2, v3

    .line 417
    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    .line 418
    invoke-interface {v5, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbyq;

    invoke-virtual {v1}, Lbyq;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v0, v4

    :goto_2
    if-eqz v0, :cond_0

    .line 417
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move v0, v3

    .line 418
    goto :goto_2

    :cond_4
    move v3, v4

    .line 422
    goto :goto_0
.end method

.method private b(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbyq;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbyn;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lbyp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 311
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 312
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 313
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 314
    invoke-static {v1, p1, v0, p2}, Lbyo;->a(Ljava/util/List;Ljava/util/List;ILjava/util/List;)Lbyr;

    move-result-object v3

    .line 316
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 313
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 319
    :cond_0
    add-int/lit8 v0, v2, -0x1

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbyr;

    iget-object v0, v0, Lbyr;->a:Ljava/util/List;

    return-object v0
.end method

.method private static c(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbyq;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbyn;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lbyn;",
            ">;"
        }
    .end annotation

    .prologue
    .line 402
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 403
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbyn;

    .line 404
    invoke-static {p0, v0}, Lbyo;->a(Ljava/util/List;Lbyn;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 405
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 408
    :cond_1
    return-object v1
.end method


# virtual methods
.method public a(Ljava/util/List;Ljava/util/List;)Ljava/util/List;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lont;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lbyp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 208
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 211
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v7

    .line 213
    if-nez v7, :cond_0

    move-object v0, v10

    .line 304
    :goto_0
    return-object v0

    .line 217
    :cond_0
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmd;

    .line 219
    iget-object v0, v1, Lbmd;->d:Lbmg;

    sget-object v2, Lbmg;->e:Lbmg;

    if-eq v0, v2, :cond_1

    iget-object v0, v1, Lbmd;->d:Lbmg;

    sget-object v2, Lbmg;->d:Lbmg;

    if-ne v0, v2, :cond_3

    .line 220
    :cond_1
    new-instance v0, Lbyq;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 221
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lont;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lbyq;-><init>(Lbmd;Lbmd;Lbmd;Lont;Lont;Lont;)V

    .line 222
    const/4 v2, 0x1

    new-array v2, v2, [Lbyp;

    const/4 v3, 0x0

    new-instance v4, Lbyp;

    const/4 v5, 0x1

    new-array v5, v5, [Lbyq;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    iget-object v0, v1, Lbmd;->d:Lbmg;

    sget-object v1, Lbmg;->e:Lbmg;

    if-ne v0, v1, :cond_2

    sget-object v0, Lbyn;->R:Lbyn;

    :goto_1
    invoke-direct {v4, v5, v0}, Lbyp;-><init>(Ljava/util/List;Lbyn;)V

    aput-object v4, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget-object v0, Lbyn;->Q:Lbyn;

    goto :goto_1

    .line 228
    :cond_3
    new-instance v0, Lbyq;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 229
    invoke-interface {p2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lont;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lbyq;-><init>(Lbmd;Lbmd;Lbmd;Lont;Lont;Lont;)V

    .line 230
    new-instance v2, Lbyp;

    const/4 v3, 0x1

    new-array v3, v3, [Lbyq;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    .line 231
    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    iget-object v0, v1, Lbmd;->d:Lbmg;

    sget-object v1, Lbmg;->a:Lbmg;

    if-ne v0, v1, :cond_4

    sget-object v0, Lbyn;->b:Lbyn;

    :goto_2
    invoke-direct {v2, v3, v0}, Lbyp;-><init>(Ljava/util/List;Lbyn;)V

    .line 234
    invoke-interface {v10, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 239
    const/4 v0, 0x0

    .line 240
    add-int/lit8 v1, v7, -0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmd;

    .line 241
    add-int/lit8 v2, v7, -0x1

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lont;

    .line 242
    iget-object v2, v1, Lbmd;->d:Lbmg;

    sget-object v3, Lbmg;->e:Lbmg;

    if-ne v2, v3, :cond_13

    .line 243
    new-instance v0, Lbyq;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lbyq;-><init>(Lbmd;Lbmd;Lbmd;Lont;Lont;Lont;)V

    .line 245
    new-instance v1, Lbyp;

    const/4 v2, 0x1

    new-array v2, v2, [Lbyq;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sget-object v2, Lbyn;->R:Lbyn;

    invoke-direct {v1, v0, v2}, Lbyp;-><init>(Ljava/util/List;Lbyn;)V

    .line 247
    add-int/lit8 v0, v7, -0x1

    move-object v7, v1

    move v8, v0

    .line 249
    :goto_3
    const/4 v0, 0x0

    .line 250
    add-int/lit8 v1, v8, -0x1

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmd;

    .line 251
    add-int/lit8 v2, v8, -0x1

    invoke-interface {p2, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lont;

    .line 252
    iget-object v2, v1, Lbmd;->d:Lbmg;

    sget-object v3, Lbmg;->d:Lbmg;

    if-ne v2, v3, :cond_12

    .line 253
    new-instance v0, Lbyq;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lbyq;-><init>(Lbmd;Lbmd;Lbmd;Lont;Lont;Lont;)V

    .line 255
    new-instance v1, Lbyp;

    const/4 v2, 0x1

    new-array v2, v2, [Lbyq;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sget-object v2, Lbyn;->Q:Lbyn;

    invoke-direct {v1, v0, v2}, Lbyp;-><init>(Ljava/util/List;Lbyn;)V

    .line 257
    add-int/lit8 v8, v8, -0x1

    move v9, v8

    move-object v8, v1

    .line 261
    :goto_4
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 262
    const/4 v0, 0x1

    move v11, v0

    :goto_5
    if-ge v11, v9, :cond_9

    .line 263
    const/4 v0, 0x1

    if-ne v11, v0, :cond_5

    const/4 v2, 0x0

    .line 264
    :goto_6
    add-int/lit8 v0, v9, -0x1

    if-ne v11, v0, :cond_6

    const/4 v3, 0x0

    .line 265
    :goto_7
    const/4 v0, 0x1

    if-ne v11, v0, :cond_7

    const/4 v5, 0x0

    .line 266
    :goto_8
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne v11, v0, :cond_8

    const/4 v6, 0x0

    .line 267
    :goto_9
    new-instance v0, Lbyq;

    .line 268
    invoke-interface {p1, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmd;

    invoke-interface {p2, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lont;

    invoke-direct/range {v0 .. v6}, Lbyq;-><init>(Lbmd;Lbmd;Lbmd;Lont;Lont;Lont;)V

    .line 267
    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 262
    add-int/lit8 v0, v11, 0x1

    move v11, v0

    goto :goto_5

    .line 231
    :cond_4
    sget-object v0, Lbyn;->a:Lbyn;

    goto/16 :goto_2

    .line 263
    :cond_5
    add-int/lit8 v0, v11, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    move-object v2, v0

    goto :goto_6

    .line 264
    :cond_6
    add-int/lit8 v0, v11, 0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    move-object v3, v0

    goto :goto_7

    .line 265
    :cond_7
    add-int/lit8 v0, v11, -0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lont;

    move-object v5, v0

    goto :goto_8

    .line 266
    :cond_8
    add-int/lit8 v0, v11, 0x1

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lont;

    move-object v6, v0

    goto :goto_9

    .line 271
    :cond_9
    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_c

    .line 272
    if-eqz v8, :cond_a

    .line 273
    invoke-interface {v10, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 275
    :cond_a
    if-eqz v7, :cond_b

    .line 276
    invoke-interface {v10, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_b
    move-object v0, v10

    .line 279
    goto/16 :goto_0

    .line 282
    :cond_c
    const/4 v0, 0x1

    move v1, v0

    :goto_a
    const/4 v0, 0x3

    if-gt v1, v0, :cond_11

    .line 283
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 284
    const/4 v0, 0x0

    :goto_b
    if-gt v0, v1, :cond_d

    .line 285
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 284
    add-int/lit8 v0, v0, 0x1

    goto :goto_b

    .line 288
    :cond_d
    invoke-static {v2}, Lbyo;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 289
    invoke-direct {p0, v12, v0}, Lbyo;->b(Ljava/util/List;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 290
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_10

    .line 291
    invoke-interface {v10, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 294
    if-eqz v8, :cond_e

    .line 295
    invoke-interface {v10, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 297
    :cond_e
    if-eqz v7, :cond_f

    .line 298
    invoke-interface {v10, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_f
    move-object v0, v10

    .line 300
    goto/16 :goto_0

    .line 282
    :cond_10
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 304
    :cond_11
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_12
    move v9, v8

    move-object v8, v0

    goto/16 :goto_4

    :cond_13
    move v8, v7

    move-object v7, v0

    goto/16 :goto_3
.end method
