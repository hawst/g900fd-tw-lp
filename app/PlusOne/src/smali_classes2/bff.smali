.class public final Lbff;
.super Lloj;
.source "PG"

# interfaces
.implements Lasl;
.implements Lcdw;


# instance fields
.field private Q:Lase;

.field private R:Lasm;

.field private S:Landroid/widget/SeekBar;

.field private T:Landroid/widget/TextView;

.field private U:Landroid/widget/LinearLayout;

.field private V:Landroid/widget/TextView;

.field private W:Landroid/widget/TextView;

.field private X:Landroid/widget/TextView;

.field private Y:Z

.field private Z:Z

.field private aa:Z

.field private final ab:Landroid/view/View$OnClickListener;

.field private final ac:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lloj;-><init>()V

    .line 52
    new-instance v0, Lbfg;

    invoke-direct {v0, p0}, Lbfg;-><init>(Lbff;)V

    iput-object v0, p0, Lbff;->ab:Landroid/view/View$OnClickListener;

    .line 69
    new-instance v0, Lbfh;

    invoke-direct {v0, p0}, Lbfh;-><init>(Lbff;)V

    iput-object v0, p0, Lbff;->ac:Landroid/content/DialogInterface$OnClickListener;

    .line 192
    return-void
.end method

.method private U()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 186
    iget-boolean v0, p0, Lbff;->Y:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iget-boolean v3, p0, Lbff;->Z:Z

    if-eqz v3, :cond_1

    move v3, v1

    :goto_1
    add-int/2addr v0, v3

    iget-boolean v3, p0, Lbff;->aa:Z

    if-eqz v3, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 189
    iget-object v1, p0, Lbff;->U:Landroid/widget/LinearLayout;

    int-to-float v0, v0

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setWeightSum(F)V

    .line 190
    return-void

    :cond_0
    move v0, v2

    .line 186
    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2
.end method

.method static synthetic a(Lbff;)Lasm;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lbff;->R:Lasm;

    return-object v0
.end method

.method static synthetic b(Lbff;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lbff;->V:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic c(Lbff;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lbff;->W:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lbff;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lbff;->X:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic e(Lbff;)Landroid/widget/SeekBar;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lbff;->S:Landroid/widget/SeekBar;

    return-object v0
.end method


# virtual methods
.method public a(J)V
    .locals 7

    .prologue
    .line 130
    iget-object v0, p0, Lbff;->T:Landroid/widget/TextView;

    invoke-virtual {p0}, Lbff;->o()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0a0157

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 131
    invoke-virtual {p0}, Lbff;->o()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {p1, p2, v5}, Lchf;->a(JLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 130
    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 132
    return-void
.end method

.method public a(Lasm;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lbff;->R:Lasm;

    .line 126
    return-void
.end method

.method public ae_()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lbff;->Q:Lase;

    invoke-virtual {v0, p0}, Lase;->b(Lasl;)V

    .line 119
    const/4 v0, 0x0

    iput-object v0, p0, Lbff;->Q:Lase;

    .line 120
    invoke-super {p0}, Lloj;->ae_()V

    .line 121
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lbff;->S:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 137
    return-void
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 83
    .line 84
    invoke-virtual {p0}, Lbff;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04021d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 86
    const v0, 0x7f100246

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbff;->T:Landroid/widget/TextView;

    .line 88
    const v0, 0x7f1005fd

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lbff;->S:Landroid/widget/SeekBar;

    .line 89
    iget-object v0, p0, Lbff;->S:Landroid/widget/SeekBar;

    new-instance v2, Lbfi;

    invoke-direct {v2, p0}, Lbfi;-><init>(Lbff;)V

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 91
    const v0, 0x7f1005fe

    .line 92
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbff;->U:Landroid/widget/LinearLayout;

    .line 94
    const v0, 0x7f1005ff

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbff;->V:Landroid/widget/TextView;

    .line 95
    iget-object v0, p0, Lbff;->V:Landroid/widget/TextView;

    iget-object v2, p0, Lbff;->ab:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    const v0, 0x7f100600

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbff;->W:Landroid/widget/TextView;

    .line 98
    iget-object v0, p0, Lbff;->W:Landroid/widget/TextView;

    iget-object v2, p0, Lbff;->ab:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 100
    const v0, 0x7f100601

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbff;->X:Landroid/widget/TextView;

    .line 101
    iget-object v0, p0, Lbff;->X:Landroid/widget/TextView;

    iget-object v2, p0, Lbff;->ab:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 103
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lbff;->n()Lz;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0a0153

    iget-object v3, p0, Lbff;->ac:Landroid/content/DialogInterface$OnClickListener;

    .line 104
    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 105
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 106
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lbff;->X:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 157
    return-void
.end method

.method public d()Lcdz;
    .locals 1

    .prologue
    .line 182
    sget-object v0, Lcdz;->d:Lcdz;

    return-object v0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 111
    invoke-super {p0, p1}, Lloj;->d(Landroid/os/Bundle;)V

    .line 112
    invoke-static {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->b(Lu;)Lcom/google/android/apps/moviemaker/MovieMakerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o()Laol;

    move-result-object v0

    invoke-virtual {v0}, Laol;->k()Lase;

    move-result-object v0

    iput-object v0, p0, Lbff;->Q:Lase;

    .line 113
    iget-object v0, p0, Lbff;->Q:Lase;

    invoke-virtual {v0, p0}, Lase;->a(Lasl;)V

    .line 114
    return-void
.end method

.method public e_(Z)V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lbff;->V:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 147
    return-void
.end method

.method public f(Z)V
    .locals 2

    .prologue
    .line 175
    iget-object v1, p0, Lbff;->X:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 176
    iput-boolean p1, p0, Lbff;->aa:Z

    .line 177
    invoke-direct {p0}, Lbff;->U()V

    .line 178
    return-void

    .line 175
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public f_(I)V
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lbff;->S:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setMax(I)V

    .line 142
    return-void
.end method

.method public f_(Z)V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lbff;->W:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 152
    return-void
.end method

.method public g_(Z)V
    .locals 2

    .prologue
    .line 161
    iget-object v1, p0, Lbff;->V:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 162
    iput-boolean p1, p0, Lbff;->Y:Z

    .line 163
    invoke-direct {p0}, Lbff;->U()V

    .line 164
    return-void

    .line 161
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public h_(Z)V
    .locals 2

    .prologue
    .line 168
    iget-object v1, p0, Lbff;->W:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 169
    iput-boolean p1, p0, Lbff;->Z:Z

    .line 170
    invoke-direct {p0}, Lbff;->U()V

    .line 171
    return-void

    .line 168
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
