.class public final Lfsg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field private final a:[Ljava/lang/Float;


# direct methods
.method public constructor <init>([Ljava/lang/Float;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lfsg;->a:[Ljava/lang/Float;

    .line 80
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 84
    cmpl-float v0, p1, v4

    if-nez v0, :cond_0

    .line 85
    iget-object v0, p0, Lfsg;->a:[Ljava/lang/Float;

    iget-object v1, p0, Lfsg;->a:[Ljava/lang/Float;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 90
    :goto_0
    return v0

    .line 87
    :cond_0
    iget-object v0, p0, Lfsg;->a:[Ljava/lang/Float;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    mul-float/2addr v0, p1

    .line 88
    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    .line 89
    iget-object v2, p0, Lfsg;->a:[Ljava/lang/Float;

    add-int/lit8 v3, v1, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    iget-object v3, p0, Lfsg;->a:[Ljava/lang/Float;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    sub-float/2addr v2, v3

    .line 90
    iget-object v3, p0, Lfsg;->a:[Ljava/lang/Float;

    aget-object v1, v3, v1

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    rem-float/2addr v0, v4

    mul-float/2addr v0, v2

    add-float/2addr v0, v1

    goto :goto_0
.end method
