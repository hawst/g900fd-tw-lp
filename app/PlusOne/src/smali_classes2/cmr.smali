.class public final Lcmr;
.super Landroid/widget/RelativeLayout;
.source "PG"


# instance fields
.field public a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/libraries/snapseed/ui/views/ToolButton;",
            ">;"
        }
    .end annotation
.end field

.field private final b:I

.field private final c:Landroid/widget/HorizontalScrollView;

.field private final d:Landroid/widget/LinearLayout;

.field private final e:Landroid/view/View;

.field private f:Lcmv;

.field private final g:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

.field private final h:Landroid/view/View$OnClickListener;

.field private synthetic i:Lcom/google/android/apps/photoeditor/views/ItemSelectorView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/photoeditor/views/ItemSelectorView;Landroid/content/Context;)V
    .locals 10

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, -0x1

    .line 516
    iput-object p1, p0, Lcmr;->i:Lcom/google/android/apps/photoeditor/views/ItemSelectorView;

    .line 517
    invoke-direct {p0, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 504
    new-instance v0, Lcms;

    invoke-direct {v0, p0}, Lcms;-><init>(Lcmr;)V

    iput-object v0, p0, Lcmr;->h:Landroid/view/View$OnClickListener;

    .line 520
    invoke-virtual {p0}, Lcmr;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0045

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcmr;->b:I

    .line 522
    new-instance v0, Landroid/widget/HorizontalScrollView;

    invoke-direct {v0, p2}, Landroid/widget/HorizontalScrollView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcmr;->c:Landroid/widget/HorizontalScrollView;

    .line 523
    iget-object v0, p0, Lcmr;->c:Landroid/widget/HorizontalScrollView;

    invoke-virtual {v0, v6}, Landroid/widget/HorizontalScrollView;->setId(I)V

    .line 525
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcmr;->e:Landroid/view/View;

    .line 526
    iget-object v0, p0, Lcmr;->e:Landroid/view/View;

    const v1, -0xd9d9da

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 527
    iget-object v0, p0, Lcmr;->e:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setId(I)V

    .line 529
    invoke-static {p1, p2}, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->a(Lcom/google/android/apps/photoeditor/views/ItemSelectorView;Landroid/content/Context;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    move-result-object v0

    iput-object v0, p0, Lcmr;->g:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 530
    iget-object v0, p0, Lcmr;->g:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setId(I)V

    .line 531
    iget-object v0, p0, Lcmr;->g:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    iget v1, p0, Lcmr;->b:I

    iget v2, p0, Lcmr;->b:I

    iget v3, p0, Lcmr;->b:I

    iget v4, p0, Lcmr;->b:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setPadding(IIII)V

    .line 533
    iget-object v0, p0, Lcmr;->g:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d0046

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setMinimumWidth(I)V

    .line 535
    iget-object v0, p0, Lcmr;->g:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    new-instance v1, Lcmt;

    invoke-direct {v1, p0}, Lcmt;-><init>(Lcmr;)V

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 544
    new-instance v0, Landroid/widget/LinearLayout;

    invoke-direct {v0, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcmr;->d:Landroid/widget/LinearLayout;

    .line 545
    iget-object v0, p0, Lcmr;->d:Landroid/widget/LinearLayout;

    iget v1, p0, Lcmr;->b:I

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcmr;->b:I

    div-int/lit8 v2, v2, 0x2

    invoke-virtual {v0, v1, v8, v2, v8}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 547
    invoke-static {p2}, Lhea;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 548
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xa

    invoke-virtual {v0, v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/16 v1, 0xb

    invoke-virtual {v0, v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {v0, v6, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v1, p0, Lcmr;->c:Landroid/widget/HorizontalScrollView;

    invoke-virtual {p0, v1, v0}, Lcmr;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v7, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {v0, v9, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/4 v1, 0x3

    invoke-virtual {v0, v6, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v1, p0, Lcmr;->e:Landroid/view/View;

    invoke-virtual {p0, v1, v0}, Lcmr;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0x9

    invoke-virtual {v0, v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {v0, v9, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v1, p0, Lcmr;->g:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    invoke-virtual {p0, v1, v0}, Lcmr;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcmr;->c:Landroid/widget/HorizontalScrollView;

    iget-object v1, p0, Lcmr;->d:Landroid/widget/LinearLayout;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/HorizontalScrollView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 552
    :goto_0
    return-void

    .line 550
    :cond_0
    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xa

    invoke-virtual {v0, v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/16 v1, 0x9

    invoke-virtual {v0, v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {v0, v8, v7}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v1, p0, Lcmr;->c:Landroid/widget/HorizontalScrollView;

    invoke-virtual {p0, v1, v0}, Lcmr;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v7, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v1, 0x6

    invoke-virtual {v0, v1, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {v0, v9, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    const/4 v1, 0x3

    invoke-virtual {v0, v8, v1}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v1, p0, Lcmr;->e:Landroid/view/View;

    invoke-virtual {p0, v1, v0}, Lcmr;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/4 v1, -0x2

    invoke-direct {v0, v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/16 v1, 0xb

    invoke-virtual {v0, v1, v5}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    invoke-virtual {v0, v9, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    iget-object v1, p0, Lcmr;->g:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    invoke-virtual {p0, v1, v0}, Lcmr;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcmr;->c:Landroid/widget/HorizontalScrollView;

    iget-object v1, p0, Lcmr;->d:Landroid/widget/LinearLayout;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v5, v5}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1, v2}, Landroid/widget/HorizontalScrollView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method static synthetic a(Lcmr;)Lcmv;
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lcmr;->f:Lcmv;

    return-object v0
.end method

.method private a(Lcom/google/android/libraries/snapseed/ui/views/ToolButton;[Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 705
    if-eqz p2, :cond_0

    array-length v0, p2

    if-gtz v0, :cond_1

    .line 720
    :cond_0
    :goto_0
    return-void

    .line 709
    :cond_1
    instance-of v0, p2, [Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    .line 711
    aget-object v0, p2, v3

    check-cast v0, Landroid/graphics/Bitmap;

    .line 712
    array-length v1, p2

    if-le v1, v2, :cond_2

    aget-object v1, p2, v2

    check-cast v1, Landroid/graphics/Bitmap;

    .line 713
    :goto_1
    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_2
    move-object v1, v0

    .line 712
    goto :goto_1

    .line 714
    :cond_3
    instance-of v0, p2, [Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 716
    aget-object v0, p2, v3

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 717
    array-length v0, p2

    if-le v0, v2, :cond_4

    aget-object v0, p2, v2

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 718
    :goto_2
    invoke-virtual {p1, v1, v0, v3}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->a(III)V

    goto :goto_0

    :cond_4
    move v0, v1

    .line 717
    goto :goto_2
.end method


# virtual methods
.method public a()Lcom/google/android/libraries/snapseed/ui/views/ToolButton;
    .locals 1

    .prologue
    .line 723
    iget-object v0, p0, Lcmr;->g:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    return-object v0
.end method

.method public a(Lcmu;)V
    .locals 14

    .prologue
    const/16 v13, 0x8

    const/4 v12, -0x2

    const/4 v6, 0x0

    .line 607
    iget-object v0, p0, Lcmr;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 609
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcmr;->a:Ljava/util/List;

    .line 611
    if-eqz p1, :cond_3

    invoke-interface {p1}, Lcmu;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 612
    iget-object v0, p0, Lcmr;->e:Landroid/view/View;

    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 613
    iget-object v0, p0, Lcmr;->g:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    invoke-interface {p1}, Lcmu;->c()I

    move-result v1

    const v2, -0x808081

    invoke-virtual {v0, v1, v2, v6}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->b(III)V

    .line 614
    iget-object v0, p0, Lcmr;->g:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    invoke-virtual {p0}, Lcmr;->getContext()Landroid/content/Context;

    invoke-interface {p1}, Lcmu;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setText(Ljava/lang/CharSequence;)V

    .line 615
    iget-object v0, p0, Lcmr;->g:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setVisibility(I)V

    .line 620
    :goto_1
    return-void

    .line 609
    :cond_0
    invoke-virtual {p0}, Lcmr;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-interface {p1}, Lcmu;->a()I

    move-result v8

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v8}, Ljava/util/ArrayList;-><init>(I)V

    const/16 v3, 0x3e8

    move v5, v6

    :goto_2
    if-ge v5, v8, :cond_2

    invoke-interface {p1, v5}, Lcmu;->b(I)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcmr;->i:Lcom/google/android/apps/photoeditor/views/ItemSelectorView;

    invoke-static {v1, v7}, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;->a(Lcom/google/android/apps/photoeditor/views/ItemSelectorView;Landroid/content/Context;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    move-result-object v1

    add-int/lit8 v2, v3, 0x1

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setId(I)V

    invoke-interface {p1, v0}, Lcmu;->a(Ljava/lang/Integer;)[Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, v1, v3}, Lcmr;->a(Lcom/google/android/libraries/snapseed/ui/views/ToolButton;[Ljava/lang/Object;)V

    invoke-interface {p1, v0}, Lcmu;->b(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setTag(Ljava/lang/Object;)V

    invoke-interface {p1, v0}, Lcmu;->c(Ljava/lang/Integer;)Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setSelected(Z)V

    iget-object v0, p0, Lcmr;->h:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v12, v12}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v3, 0x10

    iput v3, v0, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    iget v3, p0, Lcmr;->b:I

    div-int/lit8 v3, v3, 0x2

    iget v9, p0, Lcmr;->b:I

    iget v10, p0, Lcmr;->b:I

    div-int/lit8 v10, v10, 0x2

    iget v11, p0, Lcmr;->b:I

    invoke-virtual {v1, v3, v9, v10, v11}, Landroid/view/View;->setPadding(IIII)V

    :goto_3
    iget-object v3, p0, Lcmr;->d:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v3, v2

    goto :goto_2

    :cond_1
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x2

    invoke-direct {v0, v1, v12}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    new-instance v1, Landroid/view/View;

    invoke-virtual {p0}, Lcmr;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const v2, -0xd9d9da

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    move v2, v3

    goto :goto_3

    :cond_2
    move-object v0, v4

    goto/16 :goto_0

    .line 617
    :cond_3
    iget-object v0, p0, Lcmr;->e:Landroid/view/View;

    invoke-virtual {v0, v13}, Landroid/view/View;->setVisibility(I)V

    .line 618
    iget-object v0, p0, Lcmr;->g:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    invoke-virtual {v0, v13}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method public a(Lcmv;)V
    .locals 0

    .prologue
    .line 653
    iput-object p1, p0, Lcmr;->f:Lcmv;

    .line 654
    return-void
.end method

.method public a(Lcmu;Z)Z
    .locals 4

    .prologue
    .line 623
    iget-object v0, p0, Lcmr;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 624
    :cond_0
    const/4 v0, 0x0

    .line 639
    :goto_0
    return v0

    .line 627
    :cond_1
    iget-object v0, p0, Lcmr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 628
    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 630
    if-nez p2, :cond_2

    .line 631
    invoke-virtual {p0}, Lcmr;->getContext()Landroid/content/Context;

    .line 632
    invoke-interface {p1, v1}, Lcmu;->a(Ljava/lang/Integer;)[Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, v0, v3}, Lcmr;->a(Lcom/google/android/libraries/snapseed/ui/views/ToolButton;[Ljava/lang/Object;)V

    .line 633
    invoke-interface {p1, v1}, Lcmu;->b(Ljava/lang/Integer;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setText(Ljava/lang/CharSequence;)V

    .line 636
    :cond_2
    invoke-interface {p1, v1}, Lcmu;->c(Ljava/lang/Integer;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setSelected(Z)V

    goto :goto_1

    .line 639
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method
