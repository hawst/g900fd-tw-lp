.class public final Lhbf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhbi;


# instance fields
.field private a:Landroid/graphics/Bitmap;

.field private b:Landroid/graphics/Bitmap;

.field private c:Landroid/graphics/Bitmap;

.field private d:Z

.field private e:I

.field private f:I

.field private final g:Lcom/google/android/libraries/photoeditor/core/TilesProvider;

.field private final h:Lcom/google/android/libraries/photoeditor/core/Tile;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Lcom/google/android/libraries/photoeditor/core/TilesProvider;

    invoke-direct {v0}, Lcom/google/android/libraries/photoeditor/core/TilesProvider;-><init>()V

    iput-object v0, p0, Lhbf;->g:Lcom/google/android/libraries/photoeditor/core/TilesProvider;

    .line 24
    new-instance v0, Lcom/google/android/libraries/photoeditor/core/Tile;

    invoke-direct {v0}, Lcom/google/android/libraries/photoeditor/core/Tile;-><init>()V

    iput-object v0, p0, Lhbf;->h:Lcom/google/android/libraries/photoeditor/core/Tile;

    .line 27
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 62
    iput-object v1, p0, Lhbf;->a:Landroid/graphics/Bitmap;

    .line 63
    iput-object v1, p0, Lhbf;->b:Landroid/graphics/Bitmap;

    .line 65
    iget-object v0, p0, Lhbf;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 66
    iget-object v0, p0, Lhbf;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 67
    iput-object v1, p0, Lhbf;->c:Landroid/graphics/Bitmap;

    .line 69
    :cond_0
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lhbf;->a:Landroid/graphics/Bitmap;

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lhbf;->b:Landroid/graphics/Bitmap;

    if-ne v0, p2, :cond_0

    .line 56
    :goto_0
    return-void

    .line 44
    :cond_0
    invoke-virtual {p0}, Lhbf;->a()V

    .line 46
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-eq v0, v1, :cond_1

    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid pixel format"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 49
    :cond_1
    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v0

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-eq v0, v1, :cond_2

    .line 50
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid pixel format"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_2
    iput-object p1, p0, Lhbf;->a:Landroid/graphics/Bitmap;

    .line 54
    iput-object p2, p0, Lhbf;->b:Landroid/graphics/Bitmap;

    .line 55
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhbf;->d:Z

    goto :goto_0
.end method

.method a(ZZ)V
    .locals 4

    .prologue
    const v3, 0x812f

    const/16 v2, 0x2601

    const/16 v1, 0x1908

    .line 92
    iget-boolean v0, p0, Lhbf;->d:Z

    if-eqz v0, :cond_0

    .line 93
    invoke-virtual {p0}, Lhbf;->b()V

    .line 96
    :cond_0
    iget v0, p0, Lhbf;->e:I

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 97
    iget-object v0, p0, Lhbf;->b:Landroid/graphics/Bitmap;

    invoke-static {v2, v1, v3, v0}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->createRGBX8TextureFromBitmap(IIILandroid/graphics/Bitmap;)I

    move-result v0

    iput v0, p0, Lhbf;->e:I

    .line 101
    :cond_1
    iget v0, p0, Lhbf;->f:I

    if-nez v0, :cond_2

    if-eqz p2, :cond_2

    .line 103
    invoke-virtual {p0}, Lhbf;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 102
    invoke-static {v2, v1, v3, v0}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->createRGBX8TextureFromBitmap(IIILandroid/graphics/Bitmap;)I

    move-result v0

    iput v0, p0, Lhbf;->f:I

    .line 105
    :cond_2
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 75
    iget v0, p0, Lhbf;->e:I

    if-lez v0, :cond_0

    .line 76
    iget v0, p0, Lhbf;->e:I

    invoke-static {v0}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->deleteTexture(I)V

    .line 77
    iput v1, p0, Lhbf;->e:I

    .line 80
    :cond_0
    iget v0, p0, Lhbf;->f:I

    if-lez v0, :cond_1

    .line 81
    iget v0, p0, Lhbf;->f:I

    invoke-static {v0}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->deleteTexture(I)V

    .line 82
    iput v1, p0, Lhbf;->f:I

    .line 85
    :cond_1
    iput-boolean v1, p0, Lhbf;->d:Z

    .line 86
    return-void
.end method

.method public c()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lhbf;->a:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public d()Landroid/graphics/Bitmap;
    .locals 5

    .prologue
    .line 118
    iget-object v0, p0, Lhbf;->c:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 119
    iget-object v0, p0, Lhbf;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v3

    const/16 v4, 0x200

    if-le v3, v4, :cond_1

    const/high16 v4, 0x44000000    # 512.0f

    int-to-float v3, v3

    div-float v3, v4, v3

    int-to-float v1, v1

    mul-float/2addr v1, v3

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v2, v2

    mul-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->scaleImage(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lhbf;->c:Landroid/graphics/Bitmap;

    .line 121
    :cond_0
    iget-object v0, p0, Lhbf;->c:Landroid/graphics/Bitmap;

    return-object v0

    .line 119
    :cond_1
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 128
    iget v0, p0, Lhbf;->e:I

    return v0
.end method

.method public f()Lcom/google/android/libraries/photoeditor/core/TilesProvider;
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 135
    iget-object v1, p0, Lhbf;->c:Landroid/graphics/Bitmap;

    if-nez v1, :cond_0

    move v5, v0

    .line 136
    :goto_0
    iget-object v1, p0, Lhbf;->c:Landroid/graphics/Bitmap;

    if-nez v1, :cond_1

    move v6, v0

    .line 138
    :goto_1
    iget-object v0, p0, Lhbf;->g:Lcom/google/android/libraries/photoeditor/core/TilesProvider;

    iget-object v1, p0, Lhbf;->a:Landroid/graphics/Bitmap;

    .line 139
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lhbf;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    iget-object v3, p0, Lhbf;->b:Landroid/graphics/Bitmap;

    .line 140
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    iget-object v4, p0, Lhbf;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    iget v7, p0, Lhbf;->f:I

    iget v8, p0, Lhbf;->e:I

    .line 138
    invoke-virtual/range {v0 .. v8}, Lcom/google/android/libraries/photoeditor/core/TilesProvider;->a(IIIIIIII)V

    .line 144
    iget-object v0, p0, Lhbf;->g:Lcom/google/android/libraries/photoeditor/core/TilesProvider;

    return-object v0

    .line 135
    :cond_0
    iget-object v1, p0, Lhbf;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    goto :goto_0

    .line 136
    :cond_1
    iget-object v0, p0, Lhbf;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    goto :goto_1
.end method

.method public g()Lcom/google/android/libraries/photoeditor/core/Tile;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 151
    invoke-virtual {p0}, Lhbf;->f()Lcom/google/android/libraries/photoeditor/core/TilesProvider;

    move-result-object v2

    .line 152
    iget-object v0, p0, Lhbf;->h:Lcom/google/android/libraries/photoeditor/core/Tile;

    .line 153
    invoke-virtual {v2}, Lcom/google/android/libraries/photoeditor/core/TilesProvider;->getScaledWidth()I

    move-result v3

    invoke-virtual {v2}, Lcom/google/android/libraries/photoeditor/core/TilesProvider;->getScaledHeight()I

    move-result v4

    invoke-virtual {p0}, Lhbf;->e()I

    move-result v5

    move v2, v1

    .line 152
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/libraries/photoeditor/core/Tile;->a(IIIII)V

    .line 154
    iget-object v0, p0, Lhbf;->h:Lcom/google/android/libraries/photoeditor/core/Tile;

    return-object v0
.end method
