.class public final Lbmt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:[J

.field private b:I


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 183
    const-string v0, "size"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v0

    new-array v0, v0, [J

    iput-object v0, p0, Lbmt;->a:[J

    .line 184
    const/4 v0, 0x0

    iput v0, p0, Lbmt;->b:I

    .line 185
    return-void
.end method


# virtual methods
.method public a()Lbms;
    .locals 4

    .prologue
    .line 227
    iget v0, p0, Lbmt;->b:I

    const-string v1, "mTimestampsUs"

    iget-object v2, p0, Lbmt;->a:[J

    array-length v2, v2

    const-string v3, "not all values were set"

    invoke-static {v0, v1, v2, v3}, Lcgp;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 228
    iget-object v0, p0, Lbmt;->a:[J

    array-length v0, v0

    if-nez v0, :cond_0

    .line 229
    sget-object v0, Lbms;->a:Lbms;

    .line 233
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lbms;

    iget-object v1, p0, Lbmt;->a:[J

    invoke-direct {v0, v1}, Lbms;-><init>([J)V

    goto :goto_0
.end method

.method public a(J)Lbmt;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 207
    const-string v1, "timestampUs"

    invoke-static {p1, p2, v1}, Lcec;->b(JLjava/lang/CharSequence;)J

    .line 208
    iget v1, p0, Lbmt;->b:I

    const-string v2, "mIndex"

    iget-object v3, p0, Lbmt;->a:[J

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    const/4 v4, 0x0

    invoke-static {v1, v2, v0, v3, v4}, Lcgp;->a(ILjava/lang/CharSequence;IILjava/lang/CharSequence;)I

    .line 210
    iget v1, p0, Lbmt;->b:I

    if-lez v1, :cond_1

    .line 211
    iget-object v1, p0, Lbmt;->a:[J

    iget v2, p0, Lbmt;->b:I

    add-int/lit8 v2, v2, -0x1

    aget-wide v2, v1, v2

    cmp-long v1, v2, p1

    if-gez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    const-string v1, "timestamps should be strictly monotonic"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 215
    :cond_1
    iget-object v0, p0, Lbmt;->a:[J

    iget v1, p0, Lbmt;->b:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lbmt;->b:I

    aput-wide p1, v0, v1

    .line 217
    return-object p0
.end method
