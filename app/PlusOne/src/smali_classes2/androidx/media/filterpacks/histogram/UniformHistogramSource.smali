.class public final Landroidx/media/filterpacks/histogram/UniformHistogramSource;
.super Laak;
.source "PG"


# instance fields
.field private mNumBins:I


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 31
    const/16 v0, 0x32

    iput v0, p0, Landroidx/media/filterpacks/histogram/UniformHistogramSource;->mNumBins:I

    .line 35
    return-void
.end method


# virtual methods
.method public a(Lacp;)V
    .locals 2

    .prologue
    .line 47
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "binsize"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 48
    const-string v0, "mNumBins"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 49
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->a(Z)V

    .line 51
    :cond_0
    return-void
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    .line 39
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "binsize"

    const/4 v2, 0x1

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "histogram"

    const/4 v2, 0x2

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 0

    .prologue
    .line 55
    return-void
.end method

.method protected i()V
    .locals 3

    .prologue
    .line 59
    iget v0, p0, Landroidx/media/filterpacks/histogram/UniformHistogramSource;->mNumBins:I

    new-array v1, v0, [I

    .line 60
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Landroidx/media/filterpacks/histogram/UniformHistogramSource;->mNumBins:I

    if-ge v0, v2, :cond_0

    .line 61
    const/4 v2, 0x1

    aput v2, v1, v0

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64
    :cond_0
    const-string v0, "histogram"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/histogram/UniformHistogramSource;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 65
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->b()Labh;

    move-result-object v2

    .line 66
    invoke-virtual {v2, v1}, Labh;->b(Ljava/lang/Object;)V

    .line 67
    invoke-virtual {v0, v2}, Lacv;->a(Laap;)V

    .line 68
    return-void
.end method
