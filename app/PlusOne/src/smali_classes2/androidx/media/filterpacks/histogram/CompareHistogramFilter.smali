.class public final Landroidx/media/filterpacks/histogram/CompareHistogramFilter;
.super Laak;
.source "PG"


# static fields
.field public static final EMD:I


# instance fields
.field private mHistogram1:[I

.field private mHistogram2:[I

.field private mMetric:I


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 36
    iput-object v0, p0, Landroidx/media/filterpacks/histogram/CompareHistogramFilter;->mHistogram1:[I

    .line 37
    iput-object v0, p0, Landroidx/media/filterpacks/histogram/CompareHistogramFilter;->mHistogram2:[I

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Landroidx/media/filterpacks/histogram/CompareHistogramFilter;->mMetric:I

    .line 43
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 58
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "histogram1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 59
    const-string v0, "mHistogram1"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 68
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "histogram2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 62
    const-string v0, "mHistogram2"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 63
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 64
    :cond_2
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "metric"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const-string v0, "mMetric"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 47
    const/16 v0, 0x12d

    invoke-static {v0, v4}, Labf;->a(II)Labf;

    .line 48
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "histogram1"

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "histogram2"

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "metric"

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v4, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "value"

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 72
    iget v0, p0, Landroidx/media/filterpacks/histogram/CompareHistogramFilter;->mMetric:I

    packed-switch v0, :pswitch_data_0

    .line 105
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unknown metric to compare histograms!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :pswitch_0
    iget-object v0, p0, Landroidx/media/filterpacks/histogram/CompareHistogramFilter;->mHistogram1:[I

    array-length v0, v0

    iget-object v2, p0, Landroidx/media/filterpacks/histogram/CompareHistogramFilter;->mHistogram2:[I

    array-length v2, v2

    if-eq v0, v2, :cond_0

    .line 79
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can only compare histograms of same length!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v1

    move v2, v1

    move v3, v1

    .line 83
    :goto_0
    iget-object v4, p0, Landroidx/media/filterpacks/histogram/CompareHistogramFilter;->mHistogram1:[I

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 84
    iget-object v4, p0, Landroidx/media/filterpacks/histogram/CompareHistogramFilter;->mHistogram1:[I

    aget v4, v4, v0

    add-int/2addr v3, v4

    .line 85
    iget-object v4, p0, Landroidx/media/filterpacks/histogram/CompareHistogramFilter;->mHistogram2:[I

    aget v4, v4, v0

    add-int/2addr v2, v4

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 90
    :cond_1
    const/4 v0, 0x0

    move v4, v1

    move v5, v0

    move v0, v1

    .line 91
    :goto_1
    iget-object v6, p0, Landroidx/media/filterpacks/histogram/CompareHistogramFilter;->mHistogram1:[I

    array-length v6, v6

    if-ge v1, v6, :cond_2

    .line 92
    iget-object v6, p0, Landroidx/media/filterpacks/histogram/CompareHistogramFilter;->mHistogram1:[I

    aget v6, v6, v1

    add-int/2addr v4, v6

    .line 93
    iget-object v6, p0, Landroidx/media/filterpacks/histogram/CompareHistogramFilter;->mHistogram2:[I

    aget v6, v6, v1

    add-int/2addr v0, v6

    .line 94
    int-to-float v6, v4

    int-to-float v7, v3

    div-float/2addr v6, v7

    int-to-float v7, v0

    int-to-float v8, v2

    div-float/2addr v7, v8

    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    add-float/2addr v5, v6

    .line 91
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 101
    :cond_2
    iget-object v0, p0, Landroidx/media/filterpacks/histogram/CompareHistogramFilter;->mHistogram1:[I

    array-length v0, v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_3

    iget-object v0, p0, Landroidx/media/filterpacks/histogram/CompareHistogramFilter;->mHistogram1:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    int-to-float v0, v0

    :goto_2
    div-float v0, v5, v0

    .line 108
    const-string v1, "value"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/histogram/CompareHistogramFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    .line 109
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->a()Labg;

    move-result-object v2

    .line 110
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v2, v0}, Labg;->a(Ljava/lang/Object;)V

    .line 111
    invoke-virtual {v1, v2}, Lacv;->a(Laap;)V

    .line 112
    return-void

    .line 101
    :cond_3
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_2

    .line 72
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
