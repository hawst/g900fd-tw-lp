.class public final Landroidx/media/filterpacks/histogram/ChromaHistogramFilter;
.super Laak;
.source "PG"


# instance fields
.field private mHueBins:I

.field private mSaturationBins:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 101
    const-string v0, "filterframework_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 102
    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 40
    const/4 v0, 0x6

    iput v0, p0, Landroidx/media/filterpacks/histogram/ChromaHistogramFilter;->mHueBins:I

    .line 41
    const/4 v0, 0x3

    iput v0, p0, Landroidx/media/filterpacks/histogram/ChromaHistogramFilter;->mSaturationBins:I

    .line 45
    return-void
.end method

.method private static native extractChromaHistogram(Ljava/nio/ByteBuffer;Ljava/nio/FloatBuffer;II)V
.end method


# virtual methods
.method public a(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 62
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "huebins"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    const-string v0, "mHueBins"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 64
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "saturationbins"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    const-string v0, "mSaturationBins"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 49
    const/16 v0, 0x12d

    invoke-static {v0, v4}, Labf;->a(II)Labf;

    move-result-object v0

    .line 50
    const/16 v1, 0xc8

    invoke-static {v1}, Labf;->b(I)Labf;

    move-result-object v1

    .line 52
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "huebins"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "saturationbins"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "histogram"

    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 7

    .prologue
    const/4 v5, 0x1

    .line 78
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/histogram/ChromaHistogramFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v0

    .line 79
    const-string v1, "histogram"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/histogram/ChromaHistogramFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    .line 80
    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    iget v4, p0, Landroidx/media/filterpacks/histogram/ChromaHistogramFilter;->mHueBins:I

    aput v4, v2, v3

    iget v3, p0, Landroidx/media/filterpacks/histogram/ChromaHistogramFilter;->mSaturationBins:I

    aput v3, v2, v5

    .line 81
    invoke-virtual {v1, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->d()Laar;

    move-result-object v2

    .line 83
    invoke-virtual {v0, v5}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 84
    invoke-virtual {v2, v5}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 85
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 86
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v4

    .line 89
    iget v5, p0, Landroidx/media/filterpacks/histogram/ChromaHistogramFilter;->mHueBins:I

    iget v6, p0, Landroidx/media/filterpacks/histogram/ChromaHistogramFilter;->mSaturationBins:I

    invoke-static {v3, v4, v5, v6}, Landroidx/media/filterpacks/histogram/ChromaHistogramFilter;->extractChromaHistogram(Ljava/nio/ByteBuffer;Ljava/nio/FloatBuffer;II)V

    .line 91
    invoke-virtual {v0}, Laar;->h()V

    .line 92
    invoke-virtual {v2}, Laar;->h()V

    .line 94
    invoke-virtual {v1, v2}, Lacv;->a(Laap;)V

    .line 95
    return-void
.end method

.method public p()I
    .locals 1

    .prologue
    .line 73
    const/16 v0, 0x19

    return v0
.end method
