.class public final Landroidx/media/filterpacks/histogram/NewChromaHistogramFilter;
.super Laak;
.source "PG"


# instance fields
.field private mHueBins:I

.field private mSaturationBins:I

.field private mSaturationThreshold:I

.field private mValueBins:I

.field private mValueThreshold:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 115
    const-string v0, "filterframework_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 116
    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 42
    const/4 v0, 0x6

    iput v0, p0, Landroidx/media/filterpacks/histogram/NewChromaHistogramFilter;->mHueBins:I

    .line 43
    const/4 v0, 0x3

    iput v0, p0, Landroidx/media/filterpacks/histogram/NewChromaHistogramFilter;->mSaturationBins:I

    .line 46
    const/16 v0, 0x1a

    iput v0, p0, Landroidx/media/filterpacks/histogram/NewChromaHistogramFilter;->mSaturationThreshold:I

    .line 47
    const/16 v0, 0x33

    iput v0, p0, Landroidx/media/filterpacks/histogram/NewChromaHistogramFilter;->mValueThreshold:I

    .line 51
    return-void
.end method

.method private static native extractChromaHistogram(Ljava/nio/ByteBuffer;Ljava/nio/FloatBuffer;IIIII)V
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 71
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "huebins"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    const-string v0, "mHueBins"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 73
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 84
    :cond_0
    :goto_0
    return-void

    .line 74
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "saturationbins"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 75
    const-string v0, "mSaturationBins"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 77
    :cond_2
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "saturationthreshold"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 78
    const-string v0, "mSaturationThreshold"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 79
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 80
    :cond_3
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "valuethreshold"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const-string v0, "mValueThreshold"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 55
    const/16 v0, 0x12d

    invoke-static {v0, v4}, Labf;->a(II)Labf;

    move-result-object v0

    .line 56
    const/16 v1, 0xc8

    invoke-static {v1}, Labf;->b(I)Labf;

    move-result-object v1

    .line 58
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "huebins"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "saturationbins"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "saturationthreshold"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "valuethreshold"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "histogram"

    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 10

    .prologue
    const/4 v3, 0x1

    .line 88
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/histogram/NewChromaHistogramFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v7

    .line 89
    const-string v0, "histogram"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/histogram/NewChromaHistogramFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v8

    .line 91
    iget v0, p0, Landroidx/media/filterpacks/histogram/NewChromaHistogramFilter;->mHueBins:I

    iput v0, p0, Landroidx/media/filterpacks/histogram/NewChromaHistogramFilter;->mValueBins:I

    .line 92
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget v2, p0, Landroidx/media/filterpacks/histogram/NewChromaHistogramFilter;->mHueBins:I

    aput v2, v0, v1

    iget v1, p0, Landroidx/media/filterpacks/histogram/NewChromaHistogramFilter;->mSaturationBins:I

    add-int/lit8 v1, v1, 0x1

    aput v1, v0, v3

    .line 93
    invoke-virtual {v8, v0}, Lacv;->a([I)Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->d()Laar;

    move-result-object v9

    .line 95
    invoke-virtual {v7, v3}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 96
    invoke-virtual {v9, v3}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 97
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 98
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v1

    .line 101
    iget v2, p0, Landroidx/media/filterpacks/histogram/NewChromaHistogramFilter;->mHueBins:I

    iget v3, p0, Landroidx/media/filterpacks/histogram/NewChromaHistogramFilter;->mSaturationBins:I

    iget v4, p0, Landroidx/media/filterpacks/histogram/NewChromaHistogramFilter;->mValueBins:I

    iget v5, p0, Landroidx/media/filterpacks/histogram/NewChromaHistogramFilter;->mSaturationThreshold:I

    iget v6, p0, Landroidx/media/filterpacks/histogram/NewChromaHistogramFilter;->mValueThreshold:I

    invoke-static/range {v0 .. v6}, Landroidx/media/filterpacks/histogram/NewChromaHistogramFilter;->extractChromaHistogram(Ljava/nio/ByteBuffer;Ljava/nio/FloatBuffer;IIIII)V

    .line 104
    invoke-virtual {v7}, Laar;->h()V

    .line 105
    invoke-virtual {v9}, Laar;->h()V

    .line 107
    invoke-virtual {v8, v9}, Lacv;->a(Laap;)V

    .line 108
    return-void
.end method
