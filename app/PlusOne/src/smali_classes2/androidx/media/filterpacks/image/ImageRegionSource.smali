.class public Landroidx/media/filterpacks/image/ImageRegionSource;
.super Laak;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xa
.end annotation


# instance fields
.field private mCurrImageFrame:Laas;

.field private mCurrImagePath:Ljava/lang/String;

.field private mCurrImageRectF:Landroid/graphics/RectF;

.field private mImagePath:Ljava/lang/String;

.field private mImageType:Labf;

.field private mMaxHeight:I

.field private mMaxWidth:I

.field private mRectF:Landroid/graphics/RectF;

.field private mRectListener:Lacr;

.field private mRegionDecoder:Landroid/graphics/BitmapRegionDecoder;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/16 v0, 0x800

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 73
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 54
    iput v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mMaxWidth:I

    .line 55
    iput v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mMaxHeight:I

    .line 57
    iput-object v1, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mImagePath:Ljava/lang/String;

    .line 58
    iput-object v1, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mImageType:Labf;

    .line 59
    iput-object v1, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    .line 60
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v2, v2, v3, v3}, Landroid/graphics/RectF;-><init>(FFFF)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRectF:Landroid/graphics/RectF;

    .line 62
    iput-object v1, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mCurrImageFrame:Laas;

    .line 63
    iput-object v1, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mCurrImagePath:Ljava/lang/String;

    .line 64
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mCurrImageRectF:Landroid/graphics/RectF;

    .line 65
    new-instance v0, Laea;

    invoke-direct {v0, p0}, Laea;-><init>(Landroidx/media/filterpacks/image/ImageRegionSource;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRectListener:Lacr;

    .line 74
    return-void
.end method

.method public static synthetic a(Landroidx/media/filterpacks/image/ImageRegionSource;)Landroid/graphics/RectF;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRectF:Landroid/graphics/RectF;

    return-object v0
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 90
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "maxWidth"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    const-string v0, "mMaxWidth"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 106
    :cond_0
    :goto_0
    return-void

    .line 94
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "maxHeight"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 95
    const-string v0, "mMaxHeight"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 96
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 98
    :cond_2
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "imagePath"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 99
    const-string v0, "mImagePath"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 100
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 102
    :cond_3
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "rect"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRectListener:Lacr;

    invoke-virtual {p1, v0}, Lacp;->a(Lacr;)V

    .line 104
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 78
    const/16 v0, 0x12d

    const/16 v1, 0x8

    invoke-static {v0, v1}, Labf;->a(II)Labf;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mImageType:Labf;

    .line 79
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "imagePath"

    const-class v2, Ljava/lang/String;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v4, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "rect"

    const-class v2, Landroid/graphics/RectF;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "maxWidth"

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "maxHeight"

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "image"

    iget-object v2, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mImageType:Labf;

    invoke-virtual {v0, v1, v4, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 14

    .prologue
    const/4 v4, 0x0

    const-wide/high16 v12, 0x4000000000000000L    # 2.0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 110
    .line 112
    iget-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mImagePath:Ljava/lang/String;

    iget-object v3, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mCurrImagePath:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 113
    :cond_0
    iget-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v0, :cond_1

    .line 114
    iget-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->recycle()V

    .line 117
    :cond_1
    :try_start_0
    iget-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mImagePath:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v0, v3}, Landroid/graphics/BitmapRegionDecoder;->newInstance(Ljava/lang/String;Z)Landroid/graphics/BitmapRegionDecoder;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRegionDecoder:Landroid/graphics/BitmapRegionDecoder;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    iget-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mImagePath:Ljava/lang/String;

    iput-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mCurrImagePath:Ljava/lang/String;

    move v0, v1

    .line 126
    :goto_0
    if-nez v0, :cond_2

    iget-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRectF:Landroid/graphics/RectF;

    iget-object v3, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mCurrImageRectF:Landroid/graphics/RectF;

    invoke-virtual {v0, v3}, Landroid/graphics/RectF;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 127
    :cond_2
    iget-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRectF:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    cmpg-float v0, v0, v4

    if-lez v0, :cond_3

    iget-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRectF:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    cmpg-float v0, v0, v4

    if-gtz v0, :cond_4

    .line 128
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "invalid input rectangle: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRectF:Landroid/graphics/RectF;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 120
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed setting up BitmapRegionDecoder for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mImagePath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130
    :cond_4
    iget-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->getHeight()I

    move-result v0

    .line 131
    iget-object v3, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v3}, Landroid/graphics/BitmapRegionDecoder;->getWidth()I

    move-result v3

    .line 133
    new-instance v4, Landroid/graphics/Rect;

    iget-object v5, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRectF:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    int-to-float v6, v3

    mul-float/2addr v5, v6

    invoke-static {v5}, Landroid/util/FloatMath;->floor(F)F

    move-result v5

    float-to-int v5, v5

    iget-object v6, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRectF:Landroid/graphics/RectF;

    iget v6, v6, Landroid/graphics/RectF;->top:F

    int-to-float v7, v0

    mul-float/2addr v6, v7

    invoke-static {v6}, Landroid/util/FloatMath;->floor(F)F

    move-result v6

    float-to-int v6, v6

    iget-object v7, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRectF:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->right:F

    int-to-float v3, v3

    mul-float/2addr v3, v7

    invoke-static {v3}, Landroid/util/FloatMath;->floor(F)F

    move-result v3

    float-to-int v3, v3

    iget-object v7, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRectF:Landroid/graphics/RectF;

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    int-to-float v0, v0

    mul-float/2addr v0, v7

    invoke-static {v0}, Landroid/util/FloatMath;->floor(F)F

    move-result v0

    float-to-int v0, v0

    invoke-direct {v4, v5, v6, v3, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 138
    new-instance v3, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v3}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 139
    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v0

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-double v6, v0

    iget v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mMaxHeight:I

    int-to-double v8, v0

    div-double/2addr v6, v8

    int-to-double v8, v5

    iget v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mMaxWidth:I

    int-to-double v10, v0

    div-double/2addr v8, v10

    invoke-static {v6, v7, v8, v9}, Ljava/lang/Math;->max(DD)D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    cmpg-double v0, v6, v8

    if-gtz v0, :cond_7

    move v0, v1

    :goto_1
    iput v0, v3, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 141
    iget-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v0, v4, v3}, Landroid/graphics/BitmapRegionDecoder;->decodeRegion(Landroid/graphics/Rect;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 142
    const/4 v3, 0x2

    new-array v3, v3, [I

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    aput v4, v3, v2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    aput v2, v3, v1

    .line 144
    iget-object v1, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mCurrImageFrame:Laas;

    if-eqz v1, :cond_5

    .line 145
    iget-object v1, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mCurrImageFrame:Laas;

    invoke-virtual {v1}, Laas;->f()Laap;

    .line 147
    :cond_5
    iget-object v1, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mImageType:Labf;

    invoke-static {v1, v3}, Laap;->a(Labf;[I)Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v1

    iput-object v1, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mCurrImageFrame:Laas;

    .line 148
    iget-object v1, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mCurrImageFrame:Laas;

    invoke-virtual {v1, v0}, Laas;->a(Landroid/graphics/Bitmap;)V

    .line 149
    iget-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mCurrImageRectF:Landroid/graphics/RectF;

    iget-object v1, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRectF:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 153
    :cond_6
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/ImageRegionSource;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 154
    iget-object v1, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mCurrImageFrame:Laas;

    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    .line 155
    return-void

    .line 139
    :cond_7
    invoke-static {v6, v7}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    invoke-static {v12, v13}, Ljava/lang/Math;->log(D)D

    move-result-wide v8

    div-double/2addr v6, v8

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    invoke-static {v12, v13, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v6

    double-to-int v0, v6

    goto :goto_1

    :cond_8
    move v0, v2

    goto/16 :goto_0
.end method

.method protected k()V
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mRegionDecoder:Landroid/graphics/BitmapRegionDecoder;

    invoke-virtual {v0}, Landroid/graphics/BitmapRegionDecoder;->recycle()V

    .line 162
    :cond_0
    iget-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mCurrImageFrame:Laas;

    if-eqz v0, :cond_1

    .line 163
    iget-object v0, p0, Landroidx/media/filterpacks/image/ImageRegionSource;->mCurrImageFrame:Laas;

    invoke-virtual {v0}, Laas;->f()Laap;

    .line 165
    :cond_1
    return-void
.end method
