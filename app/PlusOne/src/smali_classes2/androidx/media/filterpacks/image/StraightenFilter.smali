.class public Landroidx/media/filterpacks/image/StraightenFilter;
.super Laak;
.source "PG"


# static fields
.field private static final DEGREE_TO_RADIAN:F = 0.017453292f


# instance fields
.field private mAngle:F

.field private mHeight:I

.field private mMaxAngle:F

.field private mShader:Lacm;

.field private mWidth:I


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mAngle:F

    .line 28
    const/high16 v0, 0x42340000    # 45.0f

    iput v0, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mMaxAngle:F

    .line 30
    iput v1, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mWidth:I

    .line 31
    iput v1, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mHeight:I

    .line 39
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 55
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "angle"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 56
    const-string v0, "mAngle"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 57
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 62
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "maxAngle"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    const-string v0, "mMaxAngle"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 60
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 43
    invoke-static {v2, v4}, Labf;->a(II)Labf;

    move-result-object v0

    .line 44
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 45
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "angle"

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v5, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "maxAngle"

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v5, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 66
    invoke-static {}, Lacm;->a()Lacm;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mShader:Lacm;

    .line 68
    return-void
.end method

.method protected declared-synchronized i()V
    .locals 12

    .prologue
    const/high16 v0, 0x42b40000    # 90.0f

    const v6, 0x3c8efa35

    const/high16 v11, 0x3f000000    # 0.5f

    .line 73
    monitor-enter p0

    :try_start_0
    const-string v1, "image"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/image/StraightenFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    .line 75
    const-string v2, "image"

    invoke-virtual {p0, v2}, Landroidx/media/filterpacks/image/StraightenFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v2

    invoke-virtual {v2}, Lacp;->c()Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->e()Laas;

    move-result-object v2

    .line 76
    invoke-virtual {v2}, Laas;->i()[I

    move-result-object v3

    .line 77
    invoke-virtual {v1, v3}, Lacv;->a([I)Laap;

    move-result-object v3

    invoke-virtual {v3}, Laap;->e()Laas;

    move-result-object v3

    .line 78
    invoke-virtual {v2}, Laas;->j()I

    move-result v4

    iget v5, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mWidth:I

    if-ne v4, v5, :cond_0

    invoke-virtual {v2}, Laas;->k()I

    move-result v4

    iget v5, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mHeight:I

    if-eq v4, v5, :cond_1

    .line 79
    :cond_0
    invoke-virtual {v2}, Laas;->j()I

    move-result v4

    iput v4, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mWidth:I

    .line 80
    invoke-virtual {v2}, Laas;->k()I

    move-result v4

    iput v4, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mHeight:I

    .line 83
    :cond_1
    iget v4, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mAngle:F

    mul-float/2addr v4, v6

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->cos(D)D

    move-result-wide v4

    double-to-float v4, v4

    iget v5, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mAngle:F

    mul-float/2addr v5, v6

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->sin(D)D

    move-result-wide v6

    double-to-float v5, v6

    iget v6, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mMaxAngle:F

    const/4 v7, 0x0

    cmpg-float v6, v6, v7

    if-gtz v6, :cond_2

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Max angle is out of range (0-180)."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 83
    :cond_2
    :try_start_1
    iget v6, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mMaxAngle:F

    cmpl-float v6, v6, v0

    if-lez v6, :cond_3

    :goto_0
    iput v0, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mMaxAngle:F

    new-instance v0, Landroid/graphics/PointF;

    neg-float v6, v4

    iget v7, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mWidth:I

    int-to-float v7, v7

    mul-float/2addr v6, v7

    iget v7, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mHeight:I

    int-to-float v7, v7

    mul-float/2addr v7, v5

    add-float/2addr v6, v7

    neg-float v7, v5

    iget v8, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mWidth:I

    int-to-float v8, v8

    mul-float/2addr v7, v8

    iget v8, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mHeight:I

    int-to-float v8, v8

    mul-float/2addr v8, v4

    sub-float/2addr v7, v8

    invoke-direct {v0, v6, v7}, Landroid/graphics/PointF;-><init>(FF)V

    new-instance v6, Landroid/graphics/PointF;

    iget v7, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mWidth:I

    int-to-float v7, v7

    mul-float/2addr v7, v4

    iget v8, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mHeight:I

    int-to-float v8, v8

    mul-float/2addr v8, v5

    add-float/2addr v7, v8

    iget v8, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mWidth:I

    int-to-float v8, v8

    mul-float/2addr v8, v5

    iget v9, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mHeight:I

    int-to-float v9, v9

    mul-float/2addr v9, v4

    sub-float/2addr v8, v9

    invoke-direct {v6, v7, v8}, Landroid/graphics/PointF;-><init>(FF)V

    new-instance v7, Landroid/graphics/PointF;

    neg-float v8, v4

    iget v9, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mWidth:I

    int-to-float v9, v9

    mul-float/2addr v8, v9

    iget v9, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mHeight:I

    int-to-float v9, v9

    mul-float/2addr v9, v5

    sub-float/2addr v8, v9

    neg-float v9, v5

    iget v10, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mWidth:I

    int-to-float v10, v10

    mul-float/2addr v9, v10

    iget v10, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mHeight:I

    int-to-float v10, v10

    mul-float/2addr v10, v4

    add-float/2addr v9, v10

    invoke-direct {v7, v8, v9}, Landroid/graphics/PointF;-><init>(FF)V

    new-instance v8, Landroid/graphics/PointF;

    iget v9, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mWidth:I

    int-to-float v9, v9

    mul-float/2addr v9, v4

    iget v10, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mHeight:I

    int-to-float v10, v10

    mul-float/2addr v10, v5

    sub-float/2addr v9, v10

    iget v10, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mWidth:I

    int-to-float v10, v10

    mul-float/2addr v5, v10

    iget v10, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mHeight:I

    int-to-float v10, v10

    mul-float/2addr v4, v10

    add-float/2addr v4, v5

    invoke-direct {v8, v9, v4}, Landroid/graphics/PointF;-><init>(FF)V

    iget v4, v0, Landroid/graphics/PointF;->x:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v5, v6, Landroid/graphics/PointF;->x:F

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iget v5, v0, Landroid/graphics/PointF;->y:F

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    iget v9, v6, Landroid/graphics/PointF;->y:F

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    invoke-static {v5, v9}, Ljava/lang/Math;->max(FF)F

    move-result v5

    iget v9, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mWidth:I

    int-to-float v9, v9

    div-float v4, v9, v4

    iget v9, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mHeight:I

    int-to-float v9, v9

    div-float v5, v9, v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    mul-float/2addr v4, v11

    iget v5, v0, Landroid/graphics/PointF;->x:F

    mul-float/2addr v5, v4

    iget v9, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mWidth:I

    int-to-float v9, v9

    div-float/2addr v5, v9

    add-float/2addr v5, v11

    iget v9, v0, Landroid/graphics/PointF;->y:F

    mul-float/2addr v9, v4

    iget v10, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mHeight:I

    int-to-float v10, v10

    div-float/2addr v9, v10

    add-float/2addr v9, v11

    invoke-virtual {v0, v5, v9}, Landroid/graphics/PointF;->set(FF)V

    iget v5, v6, Landroid/graphics/PointF;->x:F

    mul-float/2addr v5, v4

    iget v9, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mWidth:I

    int-to-float v9, v9

    div-float/2addr v5, v9

    add-float/2addr v5, v11

    iget v9, v6, Landroid/graphics/PointF;->y:F

    mul-float/2addr v9, v4

    iget v10, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mHeight:I

    int-to-float v10, v10

    div-float/2addr v9, v10

    add-float/2addr v9, v11

    invoke-virtual {v6, v5, v9}, Landroid/graphics/PointF;->set(FF)V

    iget v5, v7, Landroid/graphics/PointF;->x:F

    mul-float/2addr v5, v4

    iget v9, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mWidth:I

    int-to-float v9, v9

    div-float/2addr v5, v9

    add-float/2addr v5, v11

    iget v9, v7, Landroid/graphics/PointF;->y:F

    mul-float/2addr v9, v4

    iget v10, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mHeight:I

    int-to-float v10, v10

    div-float/2addr v9, v10

    add-float/2addr v9, v11

    invoke-virtual {v7, v5, v9}, Landroid/graphics/PointF;->set(FF)V

    iget v5, v8, Landroid/graphics/PointF;->x:F

    mul-float/2addr v5, v4

    iget v9, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mWidth:I

    int-to-float v9, v9

    div-float/2addr v5, v9

    add-float/2addr v5, v11

    iget v9, v8, Landroid/graphics/PointF;->y:F

    mul-float/2addr v4, v9

    iget v9, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mHeight:I

    int-to-float v9, v9

    div-float/2addr v4, v9

    add-float/2addr v4, v11

    invoke-virtual {v8, v5, v4}, Landroid/graphics/PointF;->set(FF)V

    iget-object v4, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mShader:Lacm;

    const/16 v5, 0x8

    new-array v5, v5, [F

    const/4 v9, 0x0

    iget v10, v0, Landroid/graphics/PointF;->x:F

    aput v10, v5, v9

    const/4 v9, 0x1

    iget v0, v0, Landroid/graphics/PointF;->y:F

    aput v0, v5, v9

    const/4 v0, 0x2

    iget v9, v6, Landroid/graphics/PointF;->x:F

    aput v9, v5, v0

    const/4 v0, 0x3

    iget v6, v6, Landroid/graphics/PointF;->y:F

    aput v6, v5, v0

    const/4 v0, 0x4

    iget v6, v7, Landroid/graphics/PointF;->x:F

    aput v6, v5, v0

    const/4 v0, 0x5

    iget v6, v7, Landroid/graphics/PointF;->y:F

    aput v6, v5, v0

    const/4 v0, 0x6

    iget v6, v8, Landroid/graphics/PointF;->x:F

    aput v6, v5, v0

    const/4 v0, 0x7

    iget v6, v8, Landroid/graphics/PointF;->y:F

    aput v6, v5, v0

    invoke-virtual {v4, v5}, Lacm;->a([F)V

    .line 84
    iget-object v0, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mShader:Lacm;

    invoke-virtual {v0, v2, v3}, Lacm;->a(Laas;Laas;)V

    .line 86
    invoke-virtual {v1, v3}, Lacv;->a(Laap;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 87
    monitor-exit p0

    return-void

    .line 83
    :cond_3
    :try_start_2
    iget v0, p0, Landroidx/media/filterpacks/image/StraightenFilter;->mMaxAngle:F
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method
