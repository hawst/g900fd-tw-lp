.class public Landroidx/media/filterpacks/image/NegativeFilter;
.super Laak;
.source "PG"


# static fields
.field private static final mNegativeShaderCode:Ljava/lang/String; = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  gl_FragColor = vec4(1.0 - color.rgb, color.a);\n}\n"


# instance fields
.field private mShader:Lacm;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 36
    return-void
.end method


# virtual methods
.method public c()Lacx;
    .locals 5

    .prologue
    const/16 v2, 0x12d

    const/4 v4, 0x2

    .line 40
    invoke-static {v2, v4}, Labf;->a(II)Labf;

    move-result-object v0

    .line 41
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 42
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 50
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  gl_FragColor = vec4(1.0 - color.rgb, color.a);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/NegativeFilter;->mShader:Lacm;

    .line 51
    return-void
.end method

.method protected i()V
    .locals 4

    .prologue
    .line 55
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/NegativeFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 56
    const-string v1, "image"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/image/NegativeFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v1

    .line 57
    invoke-virtual {v1}, Laas;->i()[I

    move-result-object v2

    .line 58
    invoke-virtual {v0, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->e()Laas;

    move-result-object v2

    .line 59
    iget-object v3, p0, Landroidx/media/filterpacks/image/NegativeFilter;->mShader:Lacm;

    invoke-virtual {v3, v1, v2}, Lacm;->a(Laas;Laas;)V

    .line 60
    invoke-virtual {v0, v2}, Lacv;->a(Laap;)V

    .line 61
    return-void
.end method
