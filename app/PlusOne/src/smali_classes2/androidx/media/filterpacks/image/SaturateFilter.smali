.class public Landroidx/media/filterpacks/image/SaturateFilter;
.super Laak;
.source "PG"


# static fields
.field private static final mBenSaturateShaderCode:Ljava/lang/String; = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform float scale;\nuniform float shift;\nuniform vec3 weights;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float kv = dot(color.rgb, weights) + shift;\n  vec3 new_color = scale * color.rgb + (1.0 - scale) * kv;\n  gl_FragColor = vec4(new_color, color.a);\n}\n"

.field private static final mHerfSaturateShaderCode:Ljava/lang/String; = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec3 weights;\nuniform vec3 exponents;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float de = dot(color.rgb, weights);\n  float inv_de = 1.0 / de;\n  vec3 new_color = de * pow(color.rgb * inv_de, exponents);\n  float max_color = max(max(max(new_color.r, new_color.g), new_color.b), 1.0);\n  gl_FragColor = vec4(new_color / max_color, color.a);\n}\n"


# instance fields
.field private mBenShader:Lacm;

.field private mHerfShader:Lacm;

.field private mScale:F


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 23
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroidx/media/filterpacks/image/SaturateFilter;->mScale:F

    .line 59
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 2

    .prologue
    .line 74
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "scale"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    const-string v0, "mScale"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 76
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->a(Z)V

    .line 78
    :cond_0
    return-void
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x2

    .line 63
    invoke-static {v2, v5}, Labf;->a(II)Labf;

    move-result-object v0

    .line 64
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 65
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "scale"

    const/4 v3, 0x1

    sget-object v4, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v4}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 4

    .prologue
    .line 82
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform float scale;\nuniform float shift;\nuniform vec3 weights;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float kv = dot(color.rgb, weights) + shift;\n  vec3 new_color = scale * color.rgb + (1.0 - scale) * kv;\n  gl_FragColor = vec4(new_color, color.a);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/SaturateFilter;->mBenShader:Lacm;

    .line 83
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec3 weights;\nuniform vec3 exponents;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float de = dot(color.rgb, weights);\n  float inv_de = 1.0 / de;\n  vec3 new_color = de * pow(color.rgb * inv_de, exponents);\n  float max_color = max(max(max(new_color.r, new_color.g), new_color.b), 1.0);\n  gl_FragColor = vec4(new_color / max_color, color.a);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/SaturateFilter;->mHerfShader:Lacm;

    .line 84
    const/4 v0, 0x3

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    .line 87
    iget-object v1, p0, Landroidx/media/filterpacks/image/SaturateFilter;->mBenShader:Lacm;

    const-string v2, "weights"

    invoke-virtual {v1, v2, v0}, Lacm;->a(Ljava/lang/String;[F)V

    .line 88
    iget-object v1, p0, Landroidx/media/filterpacks/image/SaturateFilter;->mBenShader:Lacm;

    const-string v2, "shift"

    const v3, 0x3b808081

    invoke-virtual {v1, v2, v3}, Lacm;->a(Ljava/lang/String;F)V

    .line 90
    iget-object v1, p0, Landroidx/media/filterpacks/image/SaturateFilter;->mHerfShader:Lacm;

    const-string v2, "weights"

    invoke-virtual {v1, v2, v0}, Lacm;->a(Ljava/lang/String;[F)V

    .line 91
    return-void

    .line 84
    nop

    :array_0
    .array-data 4
        0x3e800000    # 0.25f
        0x3f200000    # 0.625f
        0x3e000000    # 0.125f
    .end array-data
.end method

.method protected i()V
    .locals 8

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    .line 95
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/SaturateFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 96
    const-string v1, "image"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/image/SaturateFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v1

    .line 97
    invoke-virtual {v1}, Laas;->i()[I

    move-result-object v2

    .line 98
    invoke-virtual {v0, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->e()Laas;

    move-result-object v2

    .line 99
    iget v3, p0, Landroidx/media/filterpacks/image/SaturateFilter;->mScale:F

    const/4 v4, 0x0

    cmpl-float v3, v3, v4

    if-lez v3, :cond_0

    .line 100
    const/4 v3, 0x3

    new-array v3, v3, [F

    .line 102
    const/4 v4, 0x0

    const v5, 0x3f666666    # 0.9f

    iget v6, p0, Landroidx/media/filterpacks/image/SaturateFilter;->mScale:F

    mul-float/2addr v5, v6

    add-float/2addr v5, v7

    aput v5, v3, v4

    .line 103
    const/4 v4, 0x1

    const v5, 0x40066666    # 2.1f

    iget v6, p0, Landroidx/media/filterpacks/image/SaturateFilter;->mScale:F

    mul-float/2addr v5, v6

    add-float/2addr v5, v7

    aput v5, v3, v4

    .line 104
    const/4 v4, 0x2

    const v5, 0x402ccccd    # 2.7f

    iget v6, p0, Landroidx/media/filterpacks/image/SaturateFilter;->mScale:F

    mul-float/2addr v5, v6

    add-float/2addr v5, v7

    aput v5, v3, v4

    .line 106
    iget-object v4, p0, Landroidx/media/filterpacks/image/SaturateFilter;->mHerfShader:Lacm;

    const-string v5, "exponents"

    invoke-virtual {v4, v5, v3}, Lacm;->a(Ljava/lang/String;[F)V

    .line 107
    iget-object v3, p0, Landroidx/media/filterpacks/image/SaturateFilter;->mHerfShader:Lacm;

    invoke-virtual {v3, v1, v2}, Lacm;->a(Laas;Laas;)V

    .line 112
    :goto_0
    invoke-virtual {v0, v2}, Lacv;->a(Laap;)V

    .line 113
    return-void

    .line 109
    :cond_0
    iget-object v3, p0, Landroidx/media/filterpacks/image/SaturateFilter;->mBenShader:Lacm;

    const-string v4, "scale"

    iget v5, p0, Landroidx/media/filterpacks/image/SaturateFilter;->mScale:F

    add-float/2addr v5, v7

    invoke-virtual {v3, v4, v5}, Lacm;->a(Ljava/lang/String;F)V

    .line 110
    iget-object v3, p0, Landroidx/media/filterpacks/image/SaturateFilter;->mBenShader:Lacm;

    invoke-virtual {v3, v1, v2}, Lacm;->a(Laas;Laas;)V

    goto :goto_0
.end method
