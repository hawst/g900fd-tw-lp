.class public Landroidx/media/filterpacks/image/ToGrayFilter;
.super Laak;
.source "PG"


# instance fields
.field private final mColorToGray4Shader:Ljava/lang/String;

.field private mShader:Lacm;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 25
    const-string v0, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float y = dot(color, vec4(0.299, 0.587, 0.114, 0));\n  gl_FragColor = vec4(y, y, y, color.a);\n}\n"

    iput-object v0, p0, Landroidx/media/filterpacks/image/ToGrayFilter;->mColorToGray4Shader:Ljava/lang/String;

    .line 37
    return-void
.end method


# virtual methods
.method public c()Lacx;
    .locals 5

    .prologue
    const/16 v2, 0x12d

    const/4 v4, 0x2

    .line 41
    invoke-static {v2, v4}, Labf;->a(II)Labf;

    move-result-object v0

    .line 42
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 43
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float y = dot(color, vec4(0.299, 0.587, 0.114, 0));\n  gl_FragColor = vec4(y, y, y, color.a);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/ToGrayFilter;->mShader:Lacm;

    .line 52
    return-void
.end method

.method protected i()V
    .locals 4

    .prologue
    .line 56
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/ToGrayFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 57
    const-string v1, "image"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/image/ToGrayFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v1

    .line 58
    invoke-virtual {v1}, Laas;->i()[I

    move-result-object v2

    .line 59
    invoke-virtual {v0, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->e()Laas;

    move-result-object v2

    .line 60
    iget-object v3, p0, Landroidx/media/filterpacks/image/ToGrayFilter;->mShader:Lacm;

    invoke-virtual {v3, v1, v2}, Lacm;->a(Laas;Laas;)V

    .line 61
    invoke-virtual {v0, v2}, Lacv;->a(Laap;)V

    .line 62
    return-void
.end method
