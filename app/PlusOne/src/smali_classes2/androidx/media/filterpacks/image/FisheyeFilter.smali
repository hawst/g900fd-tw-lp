.class public Landroidx/media/filterpacks/image/FisheyeFilter;
.super Laak;
.source "PG"


# static fields
.field private static final mFisheyeShader:Ljava/lang/String; = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec2 scale;\nuniform float alpha;\nuniform float radius2;\nuniform float factor;\nvarying vec2 v_texcoord;\nvoid main() {\n  const float m_pi_2 = 1.570963;\n  vec2 coord = v_texcoord - vec2(0.5, 0.5);\n  float dist = length(coord * scale);\n  float radian = m_pi_2 - atan(alpha * sqrt(radius2 - dist * dist), dist);\n  float scalar = radian * factor / dist;\n  vec2 new_coord = coord * scalar + vec2(0.5, 0.5);\n  gl_FragColor = texture2D(tex_sampler_0, new_coord);\n}\n"


# instance fields
.field private mHeight:I

.field private mScale:F

.field private mShader:Lacm;

.field private mWidth:I


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 51
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 25
    const/high16 v0, 0x3f000000    # 0.5f

    iput v0, p0, Landroidx/media/filterpacks/image/FisheyeFilter;->mScale:F

    .line 27
    iput v1, p0, Landroidx/media/filterpacks/image/FisheyeFilter;->mWidth:I

    .line 28
    iput v1, p0, Landroidx/media/filterpacks/image/FisheyeFilter;->mHeight:I

    .line 52
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 2

    .prologue
    .line 67
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "scale"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    const-string v0, "mScale"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 69
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->a(Z)V

    .line 71
    :cond_0
    return-void
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x2

    .line 56
    invoke-static {v2, v5}, Labf;->a(II)Labf;

    move-result-object v0

    .line 57
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 58
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "scale"

    const/4 v3, 0x1

    sget-object v4, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v4}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 75
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec2 scale;\nuniform float alpha;\nuniform float radius2;\nuniform float factor;\nvarying vec2 v_texcoord;\nvoid main() {\n  const float m_pi_2 = 1.570963;\n  vec2 coord = v_texcoord - vec2(0.5, 0.5);\n  float dist = length(coord * scale);\n  float radian = m_pi_2 - atan(alpha * sqrt(radius2 - dist * dist), dist);\n  float scalar = radian * factor / dist;\n  vec2 new_coord = coord * scalar + vec2(0.5, 0.5);\n  gl_FragColor = texture2D(tex_sampler_0, new_coord);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/FisheyeFilter;->mShader:Lacm;

    .line 76
    return-void
.end method

.method protected declared-synchronized i()V
    .locals 12

    .prologue
    .line 113
    monitor-enter p0

    :try_start_0
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/FisheyeFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 114
    const-string v1, "image"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/image/FisheyeFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v1

    .line 115
    invoke-virtual {v1}, Laas;->i()[I

    move-result-object v2

    .line 116
    invoke-virtual {v0, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->e()Laas;

    move-result-object v2

    .line 119
    invoke-virtual {v1}, Laas;->j()I

    move-result v3

    iget v4, p0, Landroidx/media/filterpacks/image/FisheyeFilter;->mWidth:I

    if-ne v3, v4, :cond_0

    invoke-virtual {v1}, Laas;->k()I

    move-result v3

    iget v4, p0, Landroidx/media/filterpacks/image/FisheyeFilter;->mHeight:I

    if-eq v3, v4, :cond_1

    .line 120
    :cond_0
    invoke-virtual {v1}, Laas;->j()I

    move-result v3

    invoke-virtual {v1}, Laas;->k()I

    move-result v4

    iput v3, p0, Landroidx/media/filterpacks/image/FisheyeFilter;->mWidth:I

    iput v4, p0, Landroidx/media/filterpacks/image/FisheyeFilter;->mHeight:I

    .line 125
    :cond_1
    const/4 v3, 0x2

    new-array v3, v3, [F

    iget v4, p0, Landroidx/media/filterpacks/image/FisheyeFilter;->mWidth:I

    iget v5, p0, Landroidx/media/filterpacks/image/FisheyeFilter;->mHeight:I

    if-le v4, v5, :cond_2

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    aput v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Landroidx/media/filterpacks/image/FisheyeFilter;->mHeight:I

    int-to-float v5, v5

    iget v6, p0, Landroidx/media/filterpacks/image/FisheyeFilter;->mWidth:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    aput v5, v3, v4

    :goto_0
    iget v4, p0, Landroidx/media/filterpacks/image/FisheyeFilter;->mScale:F

    const/high16 v5, 0x40000000    # 2.0f

    mul-float/2addr v4, v5

    const/high16 v5, 0x3f400000    # 0.75f

    add-float/2addr v4, v5

    const/high16 v5, 0x3e800000    # 0.25f

    const/4 v6, 0x0

    aget v6, v3, v6

    const/4 v7, 0x0

    aget v7, v3, v7

    mul-float/2addr v6, v7

    const/4 v7, 0x1

    aget v7, v3, v7

    const/4 v8, 0x1

    aget v8, v3, v8

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    mul-float/2addr v5, v6

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v6

    double-to-float v6, v6

    const v7, 0x3f933333    # 1.15f

    mul-float/2addr v7, v6

    mul-float/2addr v7, v7

    const v8, 0x3fc90fdb

    div-float v9, v4, v6

    sub-float v5, v7, v5

    float-to-double v10, v5

    invoke-static {v10, v11}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v10

    double-to-float v5, v10

    mul-float/2addr v5, v9

    float-to-double v10, v5

    invoke-static {v10, v11}, Ljava/lang/Math;->atan(D)D

    move-result-wide v10

    double-to-float v5, v10

    sub-float v5, v8, v5

    div-float v5, v6, v5

    iget-object v6, p0, Landroidx/media/filterpacks/image/FisheyeFilter;->mShader:Lacm;

    const-string v8, "scale"

    invoke-virtual {v6, v8, v3}, Lacm;->a(Ljava/lang/String;[F)V

    iget-object v3, p0, Landroidx/media/filterpacks/image/FisheyeFilter;->mShader:Lacm;

    const-string v6, "radius2"

    invoke-virtual {v3, v6, v7}, Lacm;->a(Ljava/lang/String;F)V

    iget-object v3, p0, Landroidx/media/filterpacks/image/FisheyeFilter;->mShader:Lacm;

    const-string v6, "factor"

    invoke-virtual {v3, v6, v5}, Lacm;->a(Ljava/lang/String;F)V

    iget-object v3, p0, Landroidx/media/filterpacks/image/FisheyeFilter;->mShader:Lacm;

    const-string v5, "alpha"

    invoke-virtual {v3, v5, v4}, Lacm;->a(Ljava/lang/String;F)V

    .line 126
    iget-object v3, p0, Landroidx/media/filterpacks/image/FisheyeFilter;->mShader:Lacm;

    invoke-virtual {v3, v1, v2}, Lacm;->a(Laas;Laas;)V

    .line 128
    invoke-virtual {v0, v2}, Lacv;->a(Laap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    monitor-exit p0

    return-void

    .line 125
    :cond_2
    const/4 v4, 0x0

    :try_start_1
    iget v5, p0, Landroidx/media/filterpacks/image/FisheyeFilter;->mWidth:I

    int-to-float v5, v5

    iget v6, p0, Landroidx/media/filterpacks/image/FisheyeFilter;->mHeight:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    aput v5, v3, v4

    const/4 v4, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    aput v5, v3, v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 113
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
