.class public Landroidx/media/filterpacks/image/SobelFilter;
.super Laak;
.source "PG"


# static fields
.field private static final mDirectionSource:Ljava/lang/String; = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 gy = 2.0 * texture2D(tex_sampler_1, v_texcoord) - 1.0;\n  vec4 gx = 2.0 * texture2D(tex_sampler_0, v_texcoord) - 1.0;\n  gl_FragColor = vec4((atan(gy.rgb, gx.rgb) + 3.14) / (2.0 * 3.14), 1.0);\n}\n"

.field private static final mGradientXSource:Ljava/lang/String; = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec2 pix;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 a1 = -1.0 * texture2D(tex_sampler_0, v_texcoord + vec2(-pix.x, -pix.y));\n  vec4 a2 = -2.0 * texture2D(tex_sampler_0, v_texcoord + vec2(-pix.x, 0.0));\n  vec4 a3 = -1.0 * texture2D(tex_sampler_0, v_texcoord + vec2(-pix.x, +pix.y));\n  vec4 b1 = +1.0 * texture2D(tex_sampler_0, v_texcoord + vec2(+pix.x, -pix.y));\n  vec4 b2 = +2.0 * texture2D(tex_sampler_0, v_texcoord + vec2(+pix.x, 0.0));\n  vec4 b3 = +1.0 * texture2D(tex_sampler_0, v_texcoord + vec2(+pix.x, +pix.y));\n  gl_FragColor = 0.5 + (a1 + a2 + a3 + b1 + b2 + b3) / 8.0;\n}\n"

.field private static final mGradientYSource:Ljava/lang/String; = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec2 pix;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 a1 = -1.0 * texture2D(tex_sampler_0, v_texcoord + vec2(-pix.x, -pix.y));\n  vec4 a2 = -2.0 * texture2D(tex_sampler_0, v_texcoord + vec2(0.0,    -pix.y));\n  vec4 a3 = -1.0 * texture2D(tex_sampler_0, v_texcoord + vec2(+pix.x, -pix.y));\n  vec4 b1 = +1.0 * texture2D(tex_sampler_0, v_texcoord + vec2(-pix.x, +pix.y));\n  vec4 b2 = +2.0 * texture2D(tex_sampler_0, v_texcoord + vec2(0.0,    +pix.y));\n  vec4 b3 = +1.0 * texture2D(tex_sampler_0, v_texcoord + vec2(+pix.x, +pix.y));\n  gl_FragColor = 0.5 + (a1 + a2 + a3 + b1 + b2 + b3) / 8.0;\n}\n"

.field private static final mMagnitudeSource:Ljava/lang/String; = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 gx = 2.0 * texture2D(tex_sampler_0, v_texcoord) - 1.0;\n  vec4 gy = 2.0 * texture2D(tex_sampler_1, v_texcoord) - 1.0;\n  gl_FragColor = vec4(sqrt(gx.rgb * gx.rgb + gy.rgb * gy.rgb), 1.0);\n}\n"


# instance fields
.field private mDirectionShader:Lacm;

.field private mGradientXShader:Lacm;

.field private mGradientYShader:Lacm;

.field private mImageType:Labf;

.field private mMagnitudeShader:Lacm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 158
    const-string v0, "filterframework_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 159
    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 79
    return-void
.end method

.method private static native sobelOperator(IILjava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Z
.end method


# virtual methods
.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 84
    invoke-static {v2, v5}, Labf;->a(II)Labf;

    move-result-object v0

    .line 85
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 86
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "direction"

    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "magnitude"

    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 93
    invoke-virtual {p0}, Landroidx/media/filterpacks/image/SobelFilter;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 94
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec2 pix;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 a1 = -1.0 * texture2D(tex_sampler_0, v_texcoord + vec2(-pix.x, -pix.y));\n  vec4 a2 = -2.0 * texture2D(tex_sampler_0, v_texcoord + vec2(-pix.x, 0.0));\n  vec4 a3 = -1.0 * texture2D(tex_sampler_0, v_texcoord + vec2(-pix.x, +pix.y));\n  vec4 b1 = +1.0 * texture2D(tex_sampler_0, v_texcoord + vec2(+pix.x, -pix.y));\n  vec4 b2 = +2.0 * texture2D(tex_sampler_0, v_texcoord + vec2(+pix.x, 0.0));\n  vec4 b3 = +1.0 * texture2D(tex_sampler_0, v_texcoord + vec2(+pix.x, +pix.y));\n  gl_FragColor = 0.5 + (a1 + a2 + a3 + b1 + b2 + b3) / 8.0;\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/SobelFilter;->mGradientXShader:Lacm;

    .line 95
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec2 pix;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 a1 = -1.0 * texture2D(tex_sampler_0, v_texcoord + vec2(-pix.x, -pix.y));\n  vec4 a2 = -2.0 * texture2D(tex_sampler_0, v_texcoord + vec2(0.0,    -pix.y));\n  vec4 a3 = -1.0 * texture2D(tex_sampler_0, v_texcoord + vec2(+pix.x, -pix.y));\n  vec4 b1 = +1.0 * texture2D(tex_sampler_0, v_texcoord + vec2(-pix.x, +pix.y));\n  vec4 b2 = +2.0 * texture2D(tex_sampler_0, v_texcoord + vec2(0.0,    +pix.y));\n  vec4 b3 = +1.0 * texture2D(tex_sampler_0, v_texcoord + vec2(+pix.x, +pix.y));\n  gl_FragColor = 0.5 + (a1 + a2 + a3 + b1 + b2 + b3) / 8.0;\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/SobelFilter;->mGradientYShader:Lacm;

    .line 96
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 gx = 2.0 * texture2D(tex_sampler_0, v_texcoord) - 1.0;\n  vec4 gy = 2.0 * texture2D(tex_sampler_1, v_texcoord) - 1.0;\n  gl_FragColor = vec4(sqrt(gx.rgb * gx.rgb + gy.rgb * gy.rgb), 1.0);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/SobelFilter;->mMagnitudeShader:Lacm;

    .line 97
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 gy = 2.0 * texture2D(tex_sampler_1, v_texcoord) - 1.0;\n  vec4 gx = 2.0 * texture2D(tex_sampler_0, v_texcoord) - 1.0;\n  gl_FragColor = vec4((atan(gy.rgb, gx.rgb) + 3.14) / (2.0 * 3.14), 1.0);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/SobelFilter;->mDirectionShader:Lacm;

    .line 98
    const/16 v0, 0x12d

    const/16 v1, 0x12

    invoke-static {v0, v1}, Labf;->a(II)Labf;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/image/SobelFilter;->mImageType:Labf;

    .line 101
    :cond_0
    return-void
.end method

.method protected i()V
    .locals 14

    .prologue
    .line 105
    const-string v0, "magnitude"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/SobelFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v4

    .line 106
    const-string v0, "direction"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/SobelFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v5

    .line 107
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/SobelFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v6

    .line 108
    invoke-virtual {v6}, Laas;->i()[I

    move-result-object v1

    .line 110
    if-eqz v4, :cond_5

    invoke-virtual {v4, v1}, Lacv;->a([I)Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v0

    move-object v3, v0

    .line 112
    :goto_0
    if-eqz v5, :cond_6

    invoke-virtual {v5, v1}, Lacv;->a([I)Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v0

    .line 114
    :goto_1
    invoke-virtual {p0}, Landroidx/media/filterpacks/image/SobelFilter;->v()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 115
    iget-object v2, p0, Landroidx/media/filterpacks/image/SobelFilter;->mImageType:Labf;

    invoke-static {v2, v1}, Laap;->a(Labf;[I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->e()Laas;

    move-result-object v2

    .line 116
    iget-object v7, p0, Landroidx/media/filterpacks/image/SobelFilter;->mImageType:Labf;

    invoke-static {v7, v1}, Laap;->a(Labf;[I)Laap;

    move-result-object v7

    invoke-virtual {v7}, Laap;->e()Laas;

    move-result-object v7

    .line 117
    iget-object v8, p0, Landroidx/media/filterpacks/image/SobelFilter;->mGradientXShader:Lacm;

    const-string v9, "pix"

    const/4 v10, 0x2

    new-array v10, v10, [F

    const/4 v11, 0x0

    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v13, 0x0

    aget v13, v1, v13

    int-to-float v13, v13

    div-float/2addr v12, v13

    aput v12, v10, v11

    const/4 v11, 0x1

    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v13, 0x1

    aget v13, v1, v13

    int-to-float v13, v13

    div-float/2addr v12, v13

    aput v12, v10, v11

    invoke-virtual {v8, v9, v10}, Lacm;->a(Ljava/lang/String;[F)V

    .line 118
    iget-object v8, p0, Landroidx/media/filterpacks/image/SobelFilter;->mGradientYShader:Lacm;

    const-string v9, "pix"

    const/4 v10, 0x2

    new-array v10, v10, [F

    const/4 v11, 0x0

    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v13, 0x0

    aget v13, v1, v13

    int-to-float v13, v13

    div-float/2addr v12, v13

    aput v12, v10, v11

    const/4 v11, 0x1

    const/high16 v12, 0x3f800000    # 1.0f

    const/4 v13, 0x1

    aget v1, v1, v13

    int-to-float v1, v1

    div-float v1, v12, v1

    aput v1, v10, v11

    invoke-virtual {v8, v9, v10}, Lacm;->a(Ljava/lang/String;[F)V

    .line 119
    iget-object v1, p0, Landroidx/media/filterpacks/image/SobelFilter;->mGradientXShader:Lacm;

    invoke-virtual {v1, v6, v2}, Lacm;->a(Laas;Laas;)V

    .line 120
    iget-object v1, p0, Landroidx/media/filterpacks/image/SobelFilter;->mGradientYShader:Lacm;

    invoke-virtual {v1, v6, v7}, Lacm;->a(Laas;Laas;)V

    .line 121
    const/4 v1, 0x2

    new-array v1, v1, [Laas;

    const/4 v6, 0x0

    aput-object v2, v1, v6

    const/4 v6, 0x1

    aput-object v7, v1, v6

    .line 122
    if-eqz v4, :cond_0

    .line 123
    iget-object v6, p0, Landroidx/media/filterpacks/image/SobelFilter;->mMagnitudeShader:Lacm;

    invoke-virtual {v6, v1, v3}, Lacm;->a([Laas;Laas;)V

    .line 125
    :cond_0
    if-eqz v5, :cond_1

    .line 126
    iget-object v6, p0, Landroidx/media/filterpacks/image/SobelFilter;->mDirectionShader:Lacm;

    invoke-virtual {v6, v1, v0}, Lacm;->a([Laas;Laas;)V

    .line 128
    :cond_1
    invoke-virtual {v2}, Laas;->f()Laap;

    .line 129
    invoke-virtual {v7}, Laas;->f()Laap;

    .line 146
    :cond_2
    :goto_2
    if-eqz v3, :cond_3

    .line 147
    invoke-virtual {v4, v3}, Lacv;->a(Laap;)V

    .line 149
    :cond_3
    if-eqz v0, :cond_4

    .line 150
    invoke-virtual {v5, v0}, Lacv;->a(Laap;)V

    .line 152
    :cond_4
    return-void

    .line 110
    :cond_5
    const/4 v0, 0x0

    move-object v3, v0

    goto/16 :goto_0

    .line 112
    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 131
    :cond_7
    const/4 v1, 0x1

    invoke-virtual {v6, v1}, Laas;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v7

    .line 132
    if-eqz v3, :cond_9

    const/4 v1, 0x2

    invoke-virtual {v3, v1}, Laas;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    move-object v2, v1

    .line 134
    :goto_3
    if-eqz v0, :cond_a

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Laas;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 136
    :goto_4
    invoke-virtual {v6}, Laas;->j()I

    move-result v8

    invoke-virtual {v6}, Laas;->k()I

    move-result v9

    invoke-static {v8, v9, v7, v2, v1}, Landroidx/media/filterpacks/image/SobelFilter;->sobelOperator(IILjava/nio/ByteBuffer;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Z

    .line 138
    invoke-virtual {v6}, Laas;->h()V

    .line 139
    if-eqz v3, :cond_8

    .line 140
    invoke-virtual {v3}, Laas;->h()V

    .line 142
    :cond_8
    if-eqz v0, :cond_2

    .line 143
    invoke-virtual {v0}, Laas;->h()V

    goto :goto_2

    .line 132
    :cond_9
    const/4 v1, 0x0

    move-object v2, v1

    goto :goto_3

    .line 134
    :cond_a
    const/4 v1, 0x0

    goto :goto_4
.end method
