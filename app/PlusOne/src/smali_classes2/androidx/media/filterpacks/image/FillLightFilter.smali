.class public Landroidx/media/filterpacks/image/FillLightFilter;
.super Laak;
.source "PG"


# instance fields
.field private mBacklight:F

.field private final mFillLightShader:Ljava/lang/String;

.field private mShader:Lacm;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Landroidx/media/filterpacks/image/FillLightFilter;->mBacklight:F

    .line 29
    const-string v0, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform float mult;\nuniform float igamma;\nvarying vec2 v_texcoord;\nvoid main()\n{\n  const vec3 color_weights = vec3(0.25, 0.5, 0.25);\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float lightmask = dot(color.rgb, color_weights);\n  float backmask = (1.0 - lightmask);\n  vec3 ones = vec3(1.0, 1.0, 1.0);\n  vec3 diff = pow(mult * color.rgb, igamma * ones) - color.rgb;\n  diff = min(diff, 1.0);\n  vec3 new_color = min(color.rgb + diff * backmask, 1.0);\n  gl_FragColor = vec4(new_color, color.a);\n}\n"

    iput-object v0, p0, Landroidx/media/filterpacks/image/FillLightFilter;->mFillLightShader:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 2

    .prologue
    .line 61
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "strength"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    const-string v0, "mBacklight"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 63
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->a(Z)V

    .line 65
    :cond_0
    return-void
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x2

    .line 50
    invoke-static {v2, v5}, Labf;->a(II)Labf;

    move-result-object v0

    .line 51
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 52
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "strength"

    const/4 v3, 0x1

    sget-object v4, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v4}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 69
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform float mult;\nuniform float igamma;\nvarying vec2 v_texcoord;\nvoid main()\n{\n  const vec3 color_weights = vec3(0.25, 0.5, 0.25);\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float lightmask = dot(color.rgb, color_weights);\n  float backmask = (1.0 - lightmask);\n  vec3 ones = vec3(1.0, 1.0, 1.0);\n  vec3 diff = pow(mult * color.rgb, igamma * ones) - color.rgb;\n  diff = min(diff, 1.0);\n  vec3 new_color = min(color.rgb + diff * backmask, 1.0);\n  gl_FragColor = vec4(new_color, color.a);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/FillLightFilter;->mShader:Lacm;

    .line 70
    return-void
.end method

.method protected i()V
    .locals 7

    .prologue
    const v4, 0x3f333333    # 0.7f

    const v6, 0x3e99999a    # 0.3f

    const/high16 v5, 0x3f800000    # 1.0f

    .line 85
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/FillLightFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 86
    const-string v1, "image"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/image/FillLightFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v1

    .line 87
    invoke-virtual {v1}, Laas;->i()[I

    move-result-object v2

    .line 88
    invoke-virtual {v0, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->e()Laas;

    move-result-object v2

    .line 89
    iget v3, p0, Landroidx/media/filterpacks/image/FillLightFilter;->mBacklight:F

    sub-float v3, v5, v3

    mul-float/2addr v3, v4

    add-float/2addr v3, v6

    div-float v3, v5, v3

    mul-float/2addr v4, v3

    add-float/2addr v4, v6

    div-float v4, v5, v4

    iget-object v5, p0, Landroidx/media/filterpacks/image/FillLightFilter;->mShader:Lacm;

    const-string v6, "mult"

    invoke-virtual {v5, v6, v3}, Lacm;->a(Ljava/lang/String;F)V

    iget-object v3, p0, Landroidx/media/filterpacks/image/FillLightFilter;->mShader:Lacm;

    const-string v5, "igamma"

    invoke-virtual {v3, v5, v4}, Lacm;->a(Ljava/lang/String;F)V

    .line 90
    iget-object v3, p0, Landroidx/media/filterpacks/image/FillLightFilter;->mShader:Lacm;

    invoke-virtual {v3, v1, v2}, Lacm;->a(Laas;Laas;)V

    .line 91
    invoke-virtual {v0, v2}, Lacv;->a(Laap;)V

    .line 92
    return-void
.end method
