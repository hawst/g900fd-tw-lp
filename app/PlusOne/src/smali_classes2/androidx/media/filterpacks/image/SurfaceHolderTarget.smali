.class public Landroidx/media/filterpacks/image/SurfaceHolderTarget;
.super Landroidx/media/filterfw/ViewFilter;
.source "PG"


# instance fields
.field private mHasSurface:Z

.field private mRenderTarget:Lacw;

.field private mShader:Lacm;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mSurfaceHolderListener:Landroid/view/SurfaceHolder$Callback;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 65
    invoke-direct {p0, p1, p2}, Landroidx/media/filterfw/ViewFilter;-><init>(Lacs;Ljava/lang/String;)V

    .line 41
    iput-object v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    .line 42
    iput-object v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mRenderTarget:Lacw;

    .line 43
    iput-object v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mShader:Lacm;

    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mHasSurface:Z

    .line 46
    new-instance v0, Laeb;

    invoke-direct {v0, p0}, Laeb;-><init>(Landroidx/media/filterpacks/image/SurfaceHolderTarget;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mSurfaceHolderListener:Landroid/view/SurfaceHolder$Callback;

    .line 66
    return-void
.end method

.method private declared-synchronized a(Landroid/view/SurfaceHolder;)V
    .locals 2

    .prologue
    .line 190
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eq v0, p1, :cond_0

    .line 191
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unexpected Holder!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 193
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mHasSurface:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 194
    monitor-exit p0

    return-void
.end method

.method public static synthetic a(Landroidx/media/filterpacks/image/SurfaceHolderTarget;Landroid/view/SurfaceHolder;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->a(Landroid/view/SurfaceHolder;)V

    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 1

    .prologue
    .line 90
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mRenderTarget:Lacw;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mRenderTarget:Lacw;

    invoke-virtual {v0}, Lacw;->i()V

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mRenderTarget:Lacw;

    .line 94
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mHasSurface:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    monitor-exit p0

    return-void

    .line 90
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected b(Lacp;)V
    .locals 0

    .prologue
    .line 107
    invoke-super {p0, p1}, Landroidx/media/filterfw/ViewFilter;->c(Lacp;)V

    .line 108
    return-void
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 99
    const/16 v0, 0x12d

    invoke-static {v0, v3}, Labf;->a(II)Labf;

    move-result-object v0

    .line 100
    invoke-super {p0}, Landroidx/media/filterfw/ViewFilter;->c()Lacx;

    move-result-object v1

    const-string v2, "image"

    invoke-virtual {v1, v2, v3, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected declared-synchronized g()V
    .locals 1

    .prologue
    .line 112
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 113
    invoke-static {}, Lacm;->a()Lacm;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mShader:Lacm;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    :cond_0
    monitor-exit p0

    return-void

    .line 112
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized h()V
    .locals 2

    .prologue
    .line 119
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iget-object v1, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mSurfaceHolderListener:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 120
    iget-object v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    .line 121
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mHasSurface:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    monitor-exit p0

    return-void

    .line 121
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized i()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 126
    monitor-enter p0

    :try_start_0
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v0

    .line 127
    iget-boolean v1, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mHasSurface:Z

    if-eqz v1, :cond_1

    .line 129
    iget-object v1, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 130
    :try_start_1
    invoke-virtual {p0}, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->v()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 131
    iget-object v2, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mRenderTarget:Lacw;

    if-nez v2, :cond_0

    invoke-static {}, Lacw;->a()Lacw;

    move-result-object v2

    iget-object v3, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v2, v3}, Lacw;->a(Landroid/view/SurfaceHolder;)Lacw;

    move-result-object v2

    iput-object v2, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mRenderTarget:Lacw;

    iget-object v2, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mRenderTarget:Lacw;

    invoke-virtual {v2}, Lacw;->b()V

    :cond_0
    new-instance v2, Landroid/graphics/Rect;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v0}, Laas;->j()I

    move-result v5

    invoke-virtual {v0}, Laas;->k()I

    move-result v6

    invoke-direct {v2, v3, v4, v5, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v3, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v3}, Landroid/view/SurfaceHolder;->getSurfaceFrame()Landroid/graphics/Rect;

    move-result-object v3

    iget-object v4, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mShader:Lacm;

    invoke-virtual {p0, v4, v2, v3}, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->a(Lacm;Landroid/graphics/Rect;Landroid/graphics/Rect;)V

    iget-object v2, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mShader:Lacm;

    invoke-virtual {v0}, Laas;->l()Lada;

    move-result-object v4

    iget-object v5, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mRenderTarget:Lacw;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-virtual {v2, v4, v5, v6, v3}, Lacm;->a(Lada;Lacw;II)V

    invoke-virtual {v0}, Laas;->h()V

    iget-object v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mRenderTarget:Lacw;

    invoke-virtual {v0}, Lacw;->f()V

    .line 135
    :goto_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 137
    :cond_1
    monitor-exit p0

    return-void

    .line 133
    :cond_2
    :try_start_2
    iget-object v2, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v2}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v2

    invoke-virtual {v0}, Laas;->n()Landroid/graphics/Bitmap;

    move-result-object v0

    new-instance v3, Landroid/graphics/Rect;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v4, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v4}, Landroid/view/SurfaceHolder;->getSurfaceFrame()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->a(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/RectF;

    move-result-object v5

    const/high16 v6, -0x1000000

    invoke-virtual {v2, v6}, Landroid/graphics/Canvas;->drawColor(I)V

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v6

    cmpl-float v6, v6, v8

    if-lez v6, :cond_3

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v6

    cmpl-float v6, v6, v8

    if-lez v6, :cond_3

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual {v2, v6, v4}, Landroid/graphics/Canvas;->scale(FF)V

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v2, v0, v3, v5, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    :cond_3
    iget-object v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, v2}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 126
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected declared-synchronized j()V
    .locals 2

    .prologue
    .line 179
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mRenderTarget:Lacw;

    if-eqz v0, :cond_0

    .line 180
    iget-object v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mRenderTarget:Lacw;

    invoke-virtual {v0}, Lacw;->c()V

    .line 181
    iget-object v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mRenderTarget:Lacw;

    invoke-virtual {v0}, Lacw;->i()V

    .line 182
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mRenderTarget:Lacw;

    .line 184
    :cond_0
    iget-object v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_1

    .line 185
    iget-object v0, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iget-object v1, p0, Landroidx/media/filterpacks/image/SurfaceHolderTarget;->mSurfaceHolderListener:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    :cond_1
    monitor-exit p0

    return-void

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
