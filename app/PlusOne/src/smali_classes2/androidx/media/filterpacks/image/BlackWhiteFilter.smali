.class public Landroidx/media/filterpacks/image/BlackWhiteFilter;
.super Laak;
.source "PG"


# instance fields
.field private mBlack:F

.field private final mBlackWhiteShader:Ljava/lang/String;

.field private mRandom:Ljava/util/Random;

.field private mShader:Lacm;

.field private mWhite:F


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Landroidx/media/filterpacks/image/BlackWhiteFilter;->mBlack:F

    .line 27
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroidx/media/filterpacks/image/BlackWhiteFilter;->mWhite:F

    .line 29
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Landroidx/media/filterpacks/image/BlackWhiteFilter;->mRandom:Ljava/util/Random;

    .line 31
    const-string v0, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec2 seed;\nuniform float black;\nuniform float scale;\nuniform float stepsize;\nvarying vec2 v_texcoord;\nfloat rand(vec2 loc) {\n  return fract(sin(dot((loc + seed), vec2(12.9898, 78.233))) * 43758.5453);\n}\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float dither = rand(v_texcoord);\n  vec3 xform = clamp((color.rgb - black) * scale, 0.0, 1.0);\n  vec3 temp = clamp((color.rgb + stepsize - black) * scale, 0.0, 1.0);\n  vec3 new_color = clamp(xform + (temp - xform) * (dither - 0.5), 0.0, 1.0);\n  gl_FragColor = vec4(new_color, color.a);\n}\n"

    iput-object v0, p0, Landroidx/media/filterpacks/image/BlackWhiteFilter;->mBlackWhiteShader:Ljava/lang/String;

    .line 54
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 70
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "black"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    const-string v0, "mBlack"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 73
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "white"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    const-string v0, "mWhite"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 75
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 58
    invoke-static {v2, v4}, Labf;->a(II)Labf;

    move-result-object v0

    .line 59
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 60
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "black"

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v5, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "white"

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v5, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 92
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec2 seed;\nuniform float black;\nuniform float scale;\nuniform float stepsize;\nvarying vec2 v_texcoord;\nfloat rand(vec2 loc) {\n  return fract(sin(dot((loc + seed), vec2(12.9898, 78.233))) * 43758.5453);\n}\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float dither = rand(v_texcoord);\n  vec3 xform = clamp((color.rgb - black) * scale, 0.0, 1.0);\n  vec3 temp = clamp((color.rgb + stepsize - black) * scale, 0.0, 1.0);\n  vec3 new_color = clamp(xform + (temp - xform) * (dither - 0.5), 0.0, 1.0);\n  gl_FragColor = vec4(new_color, color.a);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/BlackWhiteFilter;->mShader:Lacm;

    .line 93
    return-void
.end method

.method protected i()V
    .locals 8

    .prologue
    .line 97
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/BlackWhiteFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    .line 98
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/BlackWhiteFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v2

    .line 99
    invoke-virtual {v2}, Laas;->i()[I

    move-result-object v0

    .line 100
    invoke-virtual {v1, v0}, Lacv;->a([I)Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v3

    .line 101
    iget v0, p0, Landroidx/media/filterpacks/image/BlackWhiteFilter;->mBlack:F

    iget v4, p0, Landroidx/media/filterpacks/image/BlackWhiteFilter;->mWhite:F

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    iget v4, p0, Landroidx/media/filterpacks/image/BlackWhiteFilter;->mWhite:F

    iget v5, p0, Landroidx/media/filterpacks/image/BlackWhiteFilter;->mBlack:F

    sub-float/2addr v4, v5

    div-float/2addr v0, v4

    :goto_0
    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    iget-object v6, p0, Landroidx/media/filterpacks/image/BlackWhiteFilter;->mRandom:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextFloat()F

    move-result v6

    aput v6, v4, v5

    const/4 v5, 0x1

    iget-object v6, p0, Landroidx/media/filterpacks/image/BlackWhiteFilter;->mRandom:Ljava/util/Random;

    invoke-virtual {v6}, Ljava/util/Random;->nextFloat()F

    move-result v6

    aput v6, v4, v5

    iget-object v5, p0, Landroidx/media/filterpacks/image/BlackWhiteFilter;->mShader:Lacm;

    const-string v6, "black"

    iget v7, p0, Landroidx/media/filterpacks/image/BlackWhiteFilter;->mBlack:F

    invoke-virtual {v5, v6, v7}, Lacm;->a(Ljava/lang/String;F)V

    iget-object v5, p0, Landroidx/media/filterpacks/image/BlackWhiteFilter;->mShader:Lacm;

    const-string v6, "scale"

    invoke-virtual {v5, v6, v0}, Lacm;->a(Ljava/lang/String;F)V

    iget-object v0, p0, Landroidx/media/filterpacks/image/BlackWhiteFilter;->mShader:Lacm;

    const-string v5, "stepsize"

    const v6, 0x3b808081

    invoke-virtual {v0, v5, v6}, Lacm;->a(Ljava/lang/String;F)V

    iget-object v0, p0, Landroidx/media/filterpacks/image/BlackWhiteFilter;->mShader:Lacm;

    const-string v5, "seed"

    invoke-virtual {v0, v5, v4}, Lacm;->a(Ljava/lang/String;[F)V

    .line 102
    iget-object v0, p0, Landroidx/media/filterpacks/image/BlackWhiteFilter;->mShader:Lacm;

    invoke-virtual {v0, v2, v3}, Lacm;->a(Laas;Laas;)V

    .line 103
    invoke-virtual {v1, v3}, Lacv;->a(Laap;)V

    .line 104
    return-void

    .line 101
    :cond_0
    const/high16 v0, 0x44fa0000    # 2000.0f

    goto :goto_0
.end method
