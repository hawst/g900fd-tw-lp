.class public Landroidx/media/filterpacks/image/CrossProcessFilter;
.super Laak;
.source "PG"


# instance fields
.field private final mCrossProcessShader:Ljava/lang/String;

.field private mShader:Lacm;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 25
    const-string v0, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  vec3 ncolor = vec3(0.0, 0.0, 0.0);\n  float value;\n  if (color.r < 0.5) {\n    value = color.r;\n  } else {\n    value = 1.0 - color.r;\n  }\n  float red = 4.0 * value * value * value;\n  if (color.r < 0.5) {\n    ncolor.r = red;\n  } else {\n    ncolor.r = 1.0 - red;\n  }\n  if (color.g < 0.5) {\n    value = color.g;\n  } else {\n    value = 1.0 - color.g;\n  }\n  float green = 2.0 * value * value;\n  if (color.g < 0.5) {\n    ncolor.g = green;\n  } else {\n    ncolor.g = 1.0 - green;\n  }\n  ncolor.b = color.b * 0.5 + 0.25;\n  gl_FragColor = vec4(ncolor.rgb, color.a);\n}\n"

    iput-object v0, p0, Landroidx/media/filterpacks/image/CrossProcessFilter;->mCrossProcessShader:Ljava/lang/String;

    .line 61
    return-void
.end method


# virtual methods
.method public c()Lacx;
    .locals 5

    .prologue
    const/16 v2, 0x12d

    const/4 v4, 0x2

    .line 65
    invoke-static {v2, v4}, Labf;->a(II)Labf;

    move-result-object v0

    .line 66
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 67
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 75
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  vec3 ncolor = vec3(0.0, 0.0, 0.0);\n  float value;\n  if (color.r < 0.5) {\n    value = color.r;\n  } else {\n    value = 1.0 - color.r;\n  }\n  float red = 4.0 * value * value * value;\n  if (color.r < 0.5) {\n    ncolor.r = red;\n  } else {\n    ncolor.r = 1.0 - red;\n  }\n  if (color.g < 0.5) {\n    value = color.g;\n  } else {\n    value = 1.0 - color.g;\n  }\n  float green = 2.0 * value * value;\n  if (color.g < 0.5) {\n    ncolor.g = green;\n  } else {\n    ncolor.g = 1.0 - green;\n  }\n  ncolor.b = color.b * 0.5 + 0.25;\n  gl_FragColor = vec4(ncolor.rgb, color.a);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/CrossProcessFilter;->mShader:Lacm;

    .line 76
    return-void
.end method

.method protected i()V
    .locals 4

    .prologue
    .line 80
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/CrossProcessFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 81
    const-string v1, "image"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/image/CrossProcessFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v1

    .line 82
    invoke-virtual {v1}, Laas;->i()[I

    move-result-object v2

    .line 83
    invoke-virtual {v0, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->e()Laas;

    move-result-object v2

    .line 84
    iget-object v3, p0, Landroidx/media/filterpacks/image/CrossProcessFilter;->mShader:Lacm;

    invoke-virtual {v3, v1, v2}, Lacm;->a(Laas;Laas;)V

    .line 85
    invoke-virtual {v0, v2}, Lacv;->a(Laap;)V

    .line 86
    return-void
.end method
