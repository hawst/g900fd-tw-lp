.class public Landroidx/media/filterpacks/image/BrightnessFilter;
.super Laak;
.source "PG"


# static fields
.field private static final mBrightnessShader:Ljava/lang/String; = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform float brightness;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  if (brightness < 0.5) {\n    gl_FragColor = color * (2.0 * brightness);\n  } else {\n    vec4 diff = 1.0 - color;\n    gl_FragColor = color + diff * (2.0 * (brightness - 0.5));\n  }\n}\n"


# instance fields
.field private mBrightness:F

.field private mShader:Lacm;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 30
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroidx/media/filterpacks/image/BrightnessFilter;->mBrightness:F

    .line 51
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 2

    .prologue
    .line 66
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "brightness"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    const-string v0, "mBrightness"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 68
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->a(Z)V

    .line 70
    :cond_0
    return-void
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x2

    .line 55
    invoke-static {v2, v5}, Labf;->a(II)Labf;

    move-result-object v0

    .line 56
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 57
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "brightness"

    const/4 v3, 0x1

    sget-object v4, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v4}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 74
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform float brightness;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  if (brightness < 0.5) {\n    gl_FragColor = color * (2.0 * brightness);\n  } else {\n    vec4 diff = 1.0 - color;\n    gl_FragColor = color + diff * (2.0 * (brightness - 0.5));\n  }\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/BrightnessFilter;->mShader:Lacm;

    .line 75
    return-void
.end method

.method protected i()V
    .locals 6

    .prologue
    .line 79
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/BrightnessFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 80
    const-string v1, "image"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/image/BrightnessFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v1

    .line 81
    invoke-virtual {v1}, Laas;->i()[I

    move-result-object v2

    .line 82
    invoke-virtual {v0, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->e()Laas;

    move-result-object v2

    .line 83
    iget-object v3, p0, Landroidx/media/filterpacks/image/BrightnessFilter;->mShader:Lacm;

    const-string v4, "brightness"

    iget v5, p0, Landroidx/media/filterpacks/image/BrightnessFilter;->mBrightness:F

    invoke-virtual {v3, v4, v5}, Lacm;->a(Ljava/lang/String;F)V

    .line 84
    iget-object v3, p0, Landroidx/media/filterpacks/image/BrightnessFilter;->mShader:Lacm;

    invoke-virtual {v3, v1, v2}, Lacm;->a(Laas;Laas;)V

    .line 85
    invoke-virtual {v0, v2}, Lacv;->a(Laap;)V

    .line 86
    return-void
.end method
