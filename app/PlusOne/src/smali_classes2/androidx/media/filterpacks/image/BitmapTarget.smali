.class public Landroidx/media/filterpacks/image/BitmapTarget;
.super Landroidx/media/filterfw/ViewFilter;
.source "PG"


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mImageView:Landroid/widget/ImageView;

.field private mListener:Ladz;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0, p1, p2}, Landroidx/media/filterfw/ViewFilter;-><init>(Lacs;Ljava/lang/String;)V

    .line 38
    iput-object v0, p0, Landroidx/media/filterpacks/image/BitmapTarget;->mListener:Ladz;

    .line 39
    iput-object v0, p0, Landroidx/media/filterpacks/image/BitmapTarget;->mHandler:Landroid/os/Handler;

    .line 40
    iput-object v0, p0, Landroidx/media/filterpacks/image/BitmapTarget;->mImageView:Landroid/widget/ImageView;

    .line 44
    return-void
.end method

.method public static synthetic a(Landroidx/media/filterpacks/image/BitmapTarget;)Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Landroidx/media/filterpacks/image/BitmapTarget;->mImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method public static synthetic b(Landroidx/media/filterpacks/image/BitmapTarget;)Ladz;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Landroidx/media/filterpacks/image/BitmapTarget;->mListener:Ladz;

    return-object v0
.end method


# virtual methods
.method public c()Lacx;
    .locals 4

    .prologue
    .line 72
    const/16 v0, 0x12d

    const/4 v1, 0x1

    invoke-static {v0, v1}, Labf;->a(II)Labf;

    move-result-object v0

    .line 73
    new-instance v1, Lacx;

    invoke-direct {v1}, Lacx;-><init>()V

    const-string v2, "image"

    const/4 v3, 0x2

    invoke-virtual {v1, v2, v3, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 3

    .prologue
    .line 80
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/BitmapTarget;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v0

    .line 81
    invoke-virtual {v0}, Laas;->n()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 82
    iget-object v1, p0, Landroidx/media/filterpacks/image/BitmapTarget;->mImageView:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 83
    iget-object v1, p0, Landroidx/media/filterpacks/image/BitmapTarget;->mImageView:Landroid/widget/ImageView;

    new-instance v2, Ladx;

    invoke-direct {v2, p0, v0}, Ladx;-><init>(Landroidx/media/filterpacks/image/BitmapTarget;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    .line 90
    :cond_0
    iget-object v1, p0, Landroidx/media/filterpacks/image/BitmapTarget;->mListener:Ladz;

    if-eqz v1, :cond_1

    .line 91
    iget-object v1, p0, Landroidx/media/filterpacks/image/BitmapTarget;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_2

    .line 92
    iget-object v1, p0, Landroidx/media/filterpacks/image/BitmapTarget;->mHandler:Landroid/os/Handler;

    new-instance v2, Lady;

    invoke-direct {v2, p0, v0}, Lady;-><init>(Landroidx/media/filterpacks/image/BitmapTarget;Landroid/graphics/Bitmap;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 97
    :cond_1
    :goto_0
    return-void

    .line 94
    :cond_2
    iget-object v0, p0, Landroidx/media/filterpacks/image/BitmapTarget;->mListener:Ladz;

    goto :goto_0
.end method
