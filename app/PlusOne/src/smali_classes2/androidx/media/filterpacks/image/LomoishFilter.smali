.class public Landroidx/media/filterpacks/image/LomoishFilter;
.super Laak;
.source "PG"


# static fields
.field private static final mLomoishShaderCode:Ljava/lang/String; = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec2 seed;\nuniform float stepsizeX;\nuniform float stepsizeY;\nuniform float stepsize;\nuniform vec2 center;\nuniform float inv_max_dist;\nvarying vec2 v_texcoord;\nfloat rand(vec2 loc) {\n  return fract(sin(dot((loc + seed), vec2(12.9898, 78.233))) * 43758.5453);\n}\nvoid main() {\n  vec3 nbr_color = vec3(0.0, 0.0, 0.0);\n  vec2 coord;\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  coord.x = v_texcoord.x - 0.5 * stepsizeX;\n  coord.y = v_texcoord.y - stepsizeY;\n  nbr_color += texture2D(tex_sampler_0, coord).rgb - color.rgb;\n  coord.x = v_texcoord.x - stepsizeX;\n  coord.y = v_texcoord.y + 0.5 * stepsizeY;\n  nbr_color += texture2D(tex_sampler_0, coord).rgb - color.rgb;\n  coord.x = v_texcoord.x + stepsizeX;\n  coord.y = v_texcoord.y - 0.5 * stepsizeY;\n  nbr_color += texture2D(tex_sampler_0, coord).rgb - color.rgb;\n  coord.x = v_texcoord.x + stepsizeX;\n  coord.y = v_texcoord.y + 0.5 * stepsizeY;\n  nbr_color += texture2D(tex_sampler_0, coord).rgb - color.rgb;\n  vec3 s_color = vec3(color.rgb + 0.3 * nbr_color);\n  vec3 c_color = vec3(0.0, 0.0, 0.0);\n  float value;\n  if (s_color.r < 0.5) {\n    value = s_color.r;\n  } else {\n    value = 1.0 - s_color.r;\n  }\n  float red = 4.0 * value * value * value;\n  if (s_color.r < 0.5) {\n    c_color.r = red;\n  } else {\n    c_color.r = 1.0 - red;\n  }\n  if (s_color.g < 0.5) {\n    value = s_color.g;\n  } else {\n    value = 1.0 - s_color.g;\n  }\n  float green = 2.0 * value * value;\n  if (s_color.g < 0.5) {\n    c_color.g = green;\n  } else {\n    c_color.g = 1.0 - green;\n  }\n  c_color.b = s_color.b * 0.5 + 0.25;\n  float dither = rand(v_texcoord);;\n  vec3 xform = clamp((c_color.rgb - 0.15) * 1.53846, 0.0, 1.0);\n  vec3 temp = clamp((color.rgb + stepsize - 0.15) * 1.53846, 0.0, 1.0);\n  vec3 bw_color = clamp(xform + (temp - xform) * (dither - 0.5), 0.0, 1.0);\n  float dist = distance(gl_FragCoord.xy, center);\n  float lumen = 0.85 / (1.0 + exp((dist * inv_max_dist - 0.73) * 20.0)) + 0.15;\n  gl_FragColor = vec4(bw_color * lumen, color.a);\n}\n"


# instance fields
.field private mHeight:I

.field private mRandom:Ljava/util/Random;

.field private mShader:Lacm;

.field private mWidth:I


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 103
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 28
    iput v0, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mWidth:I

    .line 29
    iput v0, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mHeight:I

    .line 31
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mRandom:Ljava/util/Random;

    .line 104
    return-void
.end method


# virtual methods
.method public c()Lacx;
    .locals 5

    .prologue
    const/16 v2, 0x12d

    const/4 v4, 0x2

    .line 108
    invoke-static {v2, v4}, Labf;->a(II)Labf;

    move-result-object v0

    .line 109
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 110
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 118
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec2 seed;\nuniform float stepsizeX;\nuniform float stepsizeY;\nuniform float stepsize;\nuniform vec2 center;\nuniform float inv_max_dist;\nvarying vec2 v_texcoord;\nfloat rand(vec2 loc) {\n  return fract(sin(dot((loc + seed), vec2(12.9898, 78.233))) * 43758.5453);\n}\nvoid main() {\n  vec3 nbr_color = vec3(0.0, 0.0, 0.0);\n  vec2 coord;\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  coord.x = v_texcoord.x - 0.5 * stepsizeX;\n  coord.y = v_texcoord.y - stepsizeY;\n  nbr_color += texture2D(tex_sampler_0, coord).rgb - color.rgb;\n  coord.x = v_texcoord.x - stepsizeX;\n  coord.y = v_texcoord.y + 0.5 * stepsizeY;\n  nbr_color += texture2D(tex_sampler_0, coord).rgb - color.rgb;\n  coord.x = v_texcoord.x + stepsizeX;\n  coord.y = v_texcoord.y - 0.5 * stepsizeY;\n  nbr_color += texture2D(tex_sampler_0, coord).rgb - color.rgb;\n  coord.x = v_texcoord.x + stepsizeX;\n  coord.y = v_texcoord.y + 0.5 * stepsizeY;\n  nbr_color += texture2D(tex_sampler_0, coord).rgb - color.rgb;\n  vec3 s_color = vec3(color.rgb + 0.3 * nbr_color);\n  vec3 c_color = vec3(0.0, 0.0, 0.0);\n  float value;\n  if (s_color.r < 0.5) {\n    value = s_color.r;\n  } else {\n    value = 1.0 - s_color.r;\n  }\n  float red = 4.0 * value * value * value;\n  if (s_color.r < 0.5) {\n    c_color.r = red;\n  } else {\n    c_color.r = 1.0 - red;\n  }\n  if (s_color.g < 0.5) {\n    value = s_color.g;\n  } else {\n    value = 1.0 - s_color.g;\n  }\n  float green = 2.0 * value * value;\n  if (s_color.g < 0.5) {\n    c_color.g = green;\n  } else {\n    c_color.g = 1.0 - green;\n  }\n  c_color.b = s_color.b * 0.5 + 0.25;\n  float dither = rand(v_texcoord);;\n  vec3 xform = clamp((c_color.rgb - 0.15) * 1.53846, 0.0, 1.0);\n  vec3 temp = clamp((color.rgb + stepsize - 0.15) * 1.53846, 0.0, 1.0);\n  vec3 bw_color = clamp(xform + (temp - xform) * (dither - 0.5), 0.0, 1.0);\n  float dist = distance(gl_FragCoord.xy, center);\n  float lumen = 0.85 / (1.0 + exp((dist * inv_max_dist - 0.73) * 20.0)) + 0.15;\n  gl_FragColor = vec4(bw_color * lumen, color.a);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mShader:Lacm;

    .line 119
    return-void
.end method

.method protected i()V
    .locals 12

    .prologue
    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v6, 0x0

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    const/high16 v7, 0x3f800000    # 1.0f

    .line 142
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/LomoishFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 143
    const-string v1, "image"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/image/LomoishFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v1

    .line 144
    invoke-virtual {v1}, Laas;->j()I

    move-result v2

    iget v3, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mWidth:I

    if-ne v2, v3, :cond_0

    invoke-virtual {v1}, Laas;->k()I

    move-result v2

    iget v3, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mHeight:I

    if-eq v2, v3, :cond_1

    .line 145
    :cond_0
    invoke-virtual {v1}, Laas;->j()I

    move-result v2

    iput v2, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mWidth:I

    .line 146
    invoke-virtual {v1}, Laas;->k()I

    move-result v2

    iput v2, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mHeight:I

    .line 147
    iget-object v2, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mShader:Lacm;

    if-eqz v2, :cond_1

    iget v2, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mWidth:I

    int-to-double v2, v2

    mul-double/2addr v2, v8

    double-to-float v2, v2

    iget v3, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mHeight:I

    int-to-double v4, v3

    mul-double/2addr v4, v8

    double-to-float v3, v4

    new-array v4, v11, [F

    aput v2, v4, v6

    aput v3, v4, v10

    mul-float/2addr v2, v2

    mul-float/2addr v3, v3

    add-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-float v2, v2

    new-array v3, v11, [F

    iget-object v5, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mRandom:Ljava/util/Random;

    invoke-virtual {v5}, Ljava/util/Random;->nextFloat()F

    move-result v5

    aput v5, v3, v6

    iget-object v5, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mRandom:Ljava/util/Random;

    invoke-virtual {v5}, Ljava/util/Random;->nextFloat()F

    move-result v5

    aput v5, v3, v10

    iget-object v5, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mShader:Lacm;

    const-string v6, "center"

    invoke-virtual {v5, v6, v4}, Lacm;->a(Ljava/lang/String;[F)V

    iget-object v4, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mShader:Lacm;

    const-string v5, "inv_max_dist"

    div-float v2, v7, v2

    invoke-virtual {v4, v5, v2}, Lacm;->a(Ljava/lang/String;F)V

    iget-object v2, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mShader:Lacm;

    const-string v4, "stepsize"

    const v5, 0x3b808081

    invoke-virtual {v2, v4, v5}, Lacm;->a(Ljava/lang/String;F)V

    iget-object v2, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mShader:Lacm;

    const-string v4, "stepsizeX"

    iget v5, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mWidth:I

    int-to-float v5, v5

    div-float v5, v7, v5

    invoke-virtual {v2, v4, v5}, Lacm;->a(Ljava/lang/String;F)V

    iget-object v2, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mShader:Lacm;

    const-string v4, "stepsizeY"

    iget v5, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mHeight:I

    int-to-float v5, v5

    div-float v5, v7, v5

    invoke-virtual {v2, v4, v5}, Lacm;->a(Ljava/lang/String;F)V

    iget-object v2, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mShader:Lacm;

    const-string v4, "seed"

    invoke-virtual {v2, v4, v3}, Lacm;->a(Ljava/lang/String;[F)V

    .line 149
    :cond_1
    invoke-virtual {v1}, Laas;->i()[I

    move-result-object v2

    .line 150
    invoke-virtual {v0, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->e()Laas;

    move-result-object v2

    .line 151
    iget-object v3, p0, Landroidx/media/filterpacks/image/LomoishFilter;->mShader:Lacm;

    invoke-virtual {v3, v1, v2}, Lacm;->a(Laas;Laas;)V

    .line 152
    invoke-virtual {v0, v2}, Lacv;->a(Laap;)V

    .line 153
    return-void
.end method
