.class public Landroidx/media/filterpacks/image/BitmapSource;
.super Laak;
.source "PG"


# instance fields
.field private mAlwaysRead:Z

.field private mImageFrame:Laas;

.field private mImageType:Labf;

.field private mLastBitmap:Landroid/graphics/Bitmap;

.field private mTimestamp:J


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 33
    iput-object v0, p0, Landroidx/media/filterpacks/image/BitmapSource;->mLastBitmap:Landroid/graphics/Bitmap;

    .line 34
    iput-object v0, p0, Landroidx/media/filterpacks/image/BitmapSource;->mImageType:Labf;

    .line 35
    iput-object v0, p0, Landroidx/media/filterpacks/image/BitmapSource;->mImageFrame:Laas;

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroidx/media/filterpacks/image/BitmapSource;->mAlwaysRead:Z

    .line 37
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroidx/media/filterpacks/image/BitmapSource;->mTimestamp:J

    .line 41
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 56
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "alwaysRead"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    const-string v0, "mAlwaysRead"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 58
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 63
    :cond_0
    :goto_0
    return-void

    .line 59
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "timestamp"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 60
    const-string v0, "mTimestamp"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 61
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 45
    const/16 v0, 0x12d

    const/16 v1, 0x8

    invoke-static {v0, v1}, Labf;->a(II)Labf;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/image/BitmapSource;->mImageType:Labf;

    .line 46
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "bitmap"

    const-class v2, Landroid/graphics/Bitmap;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v4, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "alwaysRead"

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "timestamp"

    sget-object v2, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "image"

    iget-object v2, p0, Landroidx/media/filterpacks/image/BitmapSource;->mImageType:Labf;

    invoke-virtual {v0, v1, v4, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 6

    .prologue
    .line 68
    const-string v0, "bitmap"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/BitmapSource;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->a()Labg;

    move-result-object v0

    .line 69
    invoke-virtual {v0}, Labg;->j()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 72
    const-string v1, "image"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/image/BitmapSource;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    .line 75
    iget-object v2, p0, Landroidx/media/filterpacks/image/BitmapSource;->mLastBitmap:Landroid/graphics/Bitmap;

    if-ne v2, v0, :cond_0

    iget-boolean v2, p0, Landroidx/media/filterpacks/image/BitmapSource;->mAlwaysRead:Z

    if-eqz v2, :cond_2

    .line 76
    :cond_0
    iget-object v2, p0, Landroidx/media/filterpacks/image/BitmapSource;->mImageFrame:Laas;

    if-eqz v2, :cond_1

    .line 77
    iget-object v2, p0, Landroidx/media/filterpacks/image/BitmapSource;->mImageFrame:Laas;

    invoke-virtual {v2}, Laas;->f()Laap;

    .line 79
    :cond_1
    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    aput v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    aput v4, v2, v3

    .line 80
    iget-object v3, p0, Landroidx/media/filterpacks/image/BitmapSource;->mImageType:Labf;

    invoke-static {v3, v2}, Laap;->a(Labf;[I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->e()Laas;

    move-result-object v2

    iput-object v2, p0, Landroidx/media/filterpacks/image/BitmapSource;->mImageFrame:Laas;

    .line 81
    iget-object v2, p0, Landroidx/media/filterpacks/image/BitmapSource;->mImageFrame:Laas;

    invoke-virtual {v2, v0}, Laas;->a(Landroid/graphics/Bitmap;)V

    .line 82
    iput-object v0, p0, Landroidx/media/filterpacks/image/BitmapSource;->mLastBitmap:Landroid/graphics/Bitmap;

    .line 85
    :cond_2
    iget-object v0, p0, Landroidx/media/filterpacks/image/BitmapSource;->mImageFrame:Laas;

    if-nez v0, :cond_3

    .line 86
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "BitmapSource trying to push out an undefined frame! Most likely, graph.getVariable(<BitmapSource filter>).setValue(<Bitmap>) has not been called."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 90
    :cond_3
    iget-wide v2, p0, Landroidx/media/filterpacks/image/BitmapSource;->mTimestamp:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-ltz v0, :cond_4

    .line 91
    iget-object v0, p0, Landroidx/media/filterpacks/image/BitmapSource;->mImageFrame:Laas;

    iget-wide v2, p0, Landroidx/media/filterpacks/image/BitmapSource;->mTimestamp:J

    invoke-virtual {v0, v2, v3}, Laas;->a(J)V

    .line 94
    :cond_4
    iget-object v0, p0, Landroidx/media/filterpacks/image/BitmapSource;->mImageFrame:Laas;

    invoke-virtual {v1, v0}, Lacv;->a(Laap;)V

    .line 95
    return-void
.end method

.method protected k()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Landroidx/media/filterpacks/image/BitmapSource;->mImageFrame:Laas;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Landroidx/media/filterpacks/image/BitmapSource;->mImageFrame:Laas;

    invoke-virtual {v0}, Laas;->f()Laap;

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/image/BitmapSource;->mImageFrame:Laas;

    .line 103
    :cond_0
    return-void
.end method
