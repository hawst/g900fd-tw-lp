.class public Landroidx/media/filterpacks/image/ToGrayValuesFilter;
.super Laak;
.source "PG"


# static fields
.field private static final mGrayPackFragment:Ljava/lang/String; = "precision mediump float;\nconst vec4 coeff_y = vec4(0.299, 0.587, 0.114, 0);\nuniform sampler2D tex_sampler_0;\nuniform float pix_stride;\nvarying vec2 v_texcoord;\nvoid main() {\n  for (int i = 0; i < 4; i++) {\n    vec4 p = texture2D(tex_sampler_0,\n                       v_texcoord + vec2(pix_stride * (float(i) - 1.5), 0.0));\n    gl_FragColor[i] = dot(p, coeff_y);\n  }\n}\n"


# instance fields
.field private mImageInType:Labf;

.field private mShader:Lacm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 108
    const-string v0, "filterframework_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 109
    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 48
    return-void
.end method

.method private static native toGrayValues(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Z
.end method


# virtual methods
.method public c()Lacx;
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 52
    const/16 v0, 0x12d

    invoke-static {v0, v4}, Labf;->a(II)Labf;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/image/ToGrayValuesFilter;->mImageInType:Labf;

    .line 53
    const/16 v0, 0x64

    invoke-static {v0}, Labf;->b(I)Labf;

    move-result-object v0

    .line 54
    new-instance v1, Lacx;

    invoke-direct {v1}, Lacx;-><init>()V

    const-string v2, "image"

    iget-object v3, p0, Landroidx/media/filterpacks/image/ToGrayValuesFilter;->mImageInType:Labf;

    invoke-virtual {v1, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v1

    const-string v2, "image"

    invoke-virtual {v1, v2, v4, v0}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 62
    invoke-virtual {p0}, Landroidx/media/filterpacks/image/ToGrayValuesFilter;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nconst vec4 coeff_y = vec4(0.299, 0.587, 0.114, 0);\nuniform sampler2D tex_sampler_0;\nuniform float pix_stride;\nvarying vec2 v_texcoord;\nvoid main() {\n  for (int i = 0; i < 4; i++) {\n    vec4 p = texture2D(tex_sampler_0,\n                       v_texcoord + vec2(pix_stride * (float(i) - 1.5), 0.0));\n    gl_FragColor[i] = dot(p, coeff_y);\n  }\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/ToGrayValuesFilter;->mShader:Lacm;

    .line 65
    :cond_0
    return-void
.end method

.method protected i()V
    .locals 14

    .prologue
    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v12, 0x0

    const/4 v6, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 69
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/ToGrayValuesFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    .line 70
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/ToGrayValuesFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v2

    .line 71
    invoke-virtual {v2}, Laas;->i()[I

    move-result-object v3

    .line 75
    invoke-virtual {p0}, Landroidx/media/filterpacks/image/ToGrayValuesFilter;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    aget v0, v3, v10

    rem-int/lit8 v0, v0, 0x4

    .line 78
    new-array v4, v6, [I

    aget v5, v3, v10

    sub-int v0, v5, v0

    aput v0, v4, v10

    aget v0, v3, v11

    aput v0, v4, v11

    .line 79
    invoke-virtual {v1, v4}, Lacv;->a([I)Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->d()Laar;

    move-result-object v0

    .line 80
    invoke-virtual {v0, v6}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 82
    new-array v6, v6, [I

    aget v7, v4, v10

    div-int/lit8 v7, v7, 0x4

    aput v7, v6, v10

    aget v7, v4, v11

    aput v7, v6, v11

    .line 83
    iget-object v7, p0, Landroidx/media/filterpacks/image/ToGrayValuesFilter;->mImageInType:Labf;

    invoke-static {v7, v6}, Laap;->a(Labf;[I)Laap;

    move-result-object v7

    invoke-virtual {v7}, Laap;->e()Laas;

    move-result-object v7

    .line 84
    iget-object v8, p0, Landroidx/media/filterpacks/image/ToGrayValuesFilter;->mShader:Lacm;

    aget v9, v4, v10

    int-to-float v9, v9

    aget v3, v3, v10

    int-to-float v3, v3

    div-float v3, v9, v3

    invoke-static {v12, v12, v3, v13}, Ladp;->a(FFFF)Ladp;

    move-result-object v3

    invoke-virtual {v8, v3}, Lacm;->a(Ladp;)V

    .line 85
    iget-object v3, p0, Landroidx/media/filterpacks/image/ToGrayValuesFilter;->mShader:Lacm;

    const-string v8, "pix_stride"

    aget v4, v4, v10

    int-to-float v4, v4

    div-float v4, v13, v4

    invoke-virtual {v3, v8, v4}, Lacm;->a(Ljava/lang/String;F)V

    .line 86
    iget-object v3, p0, Landroidx/media/filterpacks/image/ToGrayValuesFilter;->mShader:Lacm;

    invoke-virtual {v3, v2, v7}, Lacm;->a(Laas;Laas;)V

    .line 87
    invoke-virtual {v7}, Laas;->m()Lacw;

    move-result-object v2

    .line 88
    aget v3, v6, v10

    aget v4, v6, v11

    invoke-virtual {v2, v5, v3, v4}, Lacw;->a(Ljava/nio/ByteBuffer;II)V

    .line 89
    invoke-virtual {v7}, Laas;->h()V

    .line 90
    invoke-virtual {v7}, Laas;->f()Laap;

    .line 101
    :goto_0
    invoke-virtual {v0}, Laar;->h()V

    .line 102
    invoke-virtual {v1, v0}, Lacv;->a(Laap;)V

    .line 103
    return-void

    .line 92
    :cond_0
    invoke-virtual {v1, v3}, Lacv;->a([I)Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->d()Laar;

    move-result-object v0

    .line 93
    invoke-virtual {v0, v6}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    .line 94
    invoke-virtual {v2, v11}, Laas;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 95
    invoke-static {v4, v3}, Landroidx/media/filterpacks/image/ToGrayValuesFilter;->toGrayValues(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 96
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Native implementation encountered an error during processing!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 99
    :cond_1
    invoke-virtual {v2}, Laas;->h()V

    goto :goto_0
.end method
