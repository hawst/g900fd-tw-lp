.class public Landroidx/media/filterpacks/image/GrainFilter;
.super Laak;
.source "PG"


# instance fields
.field private mGrainShader:Lacm;

.field private final mGrainSource:Ljava/lang/String;

.field private mHeight:I

.field private mNoiseFrame:Laas;

.field private mNoiseShader:Lacm;

.field private final mNoiseSource:Ljava/lang/String;

.field private mRandom:Ljava/util/Random;

.field private mScale:F

.field private mWidth:I


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 75
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 24
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroidx/media/filterpacks/image/GrainFilter;->mScale:F

    .line 29
    iput v1, p0, Landroidx/media/filterpacks/image/GrainFilter;->mWidth:I

    .line 30
    iput v1, p0, Landroidx/media/filterpacks/image/GrainFilter;->mHeight:I

    .line 32
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/image/GrainFilter;->mNoiseFrame:Laas;

    .line 33
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Landroidx/media/filterpacks/image/GrainFilter;->mRandom:Ljava/util/Random;

    .line 35
    const-string v0, "precision mediump float;\nuniform vec2 seed;\nvarying vec2 v_texcoord;\nfloat rand(vec2 loc) {\n  const float divide = 0.0003630780547;\n  const float factor = 2754.228703;\n  float value = sin(dot(loc, vec2(12.9898, 78.233)));\n  float residual = mod(dot(mod(loc, divide), vec2(0.9898, 0.233)), divide);\n  float part2 = mod(value, divide);\n  float part1 = value - part2;\n  return fract(0.5453 * part1 + factor * (part2 + residual));\n}\nvoid main() {\n  gl_FragColor = vec4(rand(v_texcoord + seed), 0.0, 0.0, 1.0);\n}\n"

    iput-object v0, p0, Landroidx/media/filterpacks/image/GrainFilter;->mNoiseSource:Ljava/lang/String;

    .line 52
    const-string v0, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform float scale;\nuniform float stepX;\nuniform float stepY;\nvarying vec2 v_texcoord;\nvoid main() {\n  float noise = texture2D(tex_sampler_1, v_texcoord + vec2(-stepX, -stepY)).r * 0.224;\n  noise += texture2D(tex_sampler_1, v_texcoord + vec2(-stepX, stepY)).r * 0.224;\n  noise += texture2D(tex_sampler_1, v_texcoord + vec2(stepX, -stepY)).r * 0.224;\n  noise += texture2D(tex_sampler_1, v_texcoord + vec2(stepX, stepY)).r * 0.224;\n  noise += 0.4448;\n  noise *= scale;\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float energy = 0.33333 * color.r + 0.33333 * color.g + 0.33333 * color.b;\n  float mask = (1.0 - sqrt(energy));\n  float weight = 1.0 - 1.333 * mask * noise;\n  gl_FragColor = vec4(color.rgb * weight, color.a);\n}\n"

    iput-object v0, p0, Landroidx/media/filterpacks/image/GrainFilter;->mGrainSource:Ljava/lang/String;

    .line 76
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 2

    .prologue
    .line 91
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "scale"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    const-string v0, "mScale"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 93
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->a(Z)V

    .line 95
    :cond_0
    return-void
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x2

    .line 80
    invoke-static {v2, v5}, Labf;->a(II)Labf;

    move-result-object v0

    .line 81
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 82
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "scale"

    const/4 v3, 0x1

    sget-object v4, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v4}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 120
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform vec2 seed;\nvarying vec2 v_texcoord;\nfloat rand(vec2 loc) {\n  const float divide = 0.0003630780547;\n  const float factor = 2754.228703;\n  float value = sin(dot(loc, vec2(12.9898, 78.233)));\n  float residual = mod(dot(mod(loc, divide), vec2(0.9898, 0.233)), divide);\n  float part2 = mod(value, divide);\n  float part1 = value - part2;\n  return fract(0.5453 * part1 + factor * (part2 + residual));\n}\nvoid main() {\n  gl_FragColor = vec4(rand(v_texcoord + seed), 0.0, 0.0, 1.0);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/GrainFilter;->mNoiseShader:Lacm;

    .line 121
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform sampler2D tex_sampler_1;\nuniform float scale;\nuniform float stepX;\nuniform float stepY;\nvarying vec2 v_texcoord;\nvoid main() {\n  float noise = texture2D(tex_sampler_1, v_texcoord + vec2(-stepX, -stepY)).r * 0.224;\n  noise += texture2D(tex_sampler_1, v_texcoord + vec2(-stepX, stepY)).r * 0.224;\n  noise += texture2D(tex_sampler_1, v_texcoord + vec2(stepX, -stepY)).r * 0.224;\n  noise += texture2D(tex_sampler_1, v_texcoord + vec2(stepX, stepY)).r * 0.224;\n  noise += 0.4448;\n  noise *= scale;\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float energy = 0.33333 * color.r + 0.33333 * color.g + 0.33333 * color.b;\n  float mask = (1.0 - sqrt(energy));\n  float weight = 1.0 - 1.333 * mask * noise;\n  gl_FragColor = vec4(color.rgb * weight, color.a);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/GrainFilter;->mGrainShader:Lacm;

    .line 122
    return-void
.end method

.method protected declared-synchronized i()V
    .locals 8

    .prologue
    const/high16 v7, 0x3f000000    # 0.5f

    .line 128
    monitor-enter p0

    :try_start_0
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/GrainFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 129
    const-string v1, "image"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/image/GrainFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v1

    .line 130
    invoke-virtual {v1}, Laas;->i()[I

    move-result-object v2

    .line 131
    invoke-virtual {v0, v2}, Lacv;->a([I)Laap;

    move-result-object v3

    invoke-virtual {v3}, Laap;->e()Laas;

    move-result-object v3

    .line 133
    const/4 v4, 0x0

    aget v4, v2, v4

    iget v5, p0, Landroidx/media/filterpacks/image/GrainFilter;->mWidth:I

    if-ne v4, v5, :cond_0

    const/4 v4, 0x1

    aget v4, v2, v4

    iget v5, p0, Landroidx/media/filterpacks/image/GrainFilter;->mHeight:I

    if-ne v4, v5, :cond_0

    iget-object v4, p0, Landroidx/media/filterpacks/image/GrainFilter;->mNoiseFrame:Laas;

    if-nez v4, :cond_2

    .line 134
    :cond_0
    const/4 v4, 0x0

    aget v4, v2, v4

    const/4 v5, 0x1

    aget v5, v2, v5

    iput v4, p0, Landroidx/media/filterpacks/image/GrainFilter;->mWidth:I

    iput v5, p0, Landroidx/media/filterpacks/image/GrainFilter;->mHeight:I

    iget-object v4, p0, Landroidx/media/filterpacks/image/GrainFilter;->mGrainShader:Lacm;

    const-string v5, "stepX"

    iget v6, p0, Landroidx/media/filterpacks/image/GrainFilter;->mWidth:I

    int-to-float v6, v6

    div-float v6, v7, v6

    invoke-virtual {v4, v5, v6}, Lacm;->a(Ljava/lang/String;F)V

    iget-object v4, p0, Landroidx/media/filterpacks/image/GrainFilter;->mGrainShader:Lacm;

    const-string v5, "stepY"

    iget v6, p0, Landroidx/media/filterpacks/image/GrainFilter;->mHeight:I

    int-to-float v6, v6

    div-float v6, v7, v6

    invoke-virtual {v4, v5, v6}, Lacm;->a(Ljava/lang/String;F)V

    .line 135
    const/4 v4, 0x0

    aget v4, v2, v4

    div-int/lit8 v4, v4, 0x2

    const/4 v5, 0x1

    aget v2, v2, v5

    div-int/lit8 v2, v2, 0x2

    iget-object v5, p0, Landroidx/media/filterpacks/image/GrainFilter;->mNoiseFrame:Laas;

    if-eqz v5, :cond_1

    iget-object v5, p0, Landroidx/media/filterpacks/image/GrainFilter;->mNoiseFrame:Laas;

    invoke-virtual {v5}, Laas;->f()Laap;

    :cond_1
    const/4 v5, 0x2

    new-array v5, v5, [I

    const/4 v6, 0x0

    aput v4, v5, v6

    const/4 v4, 0x1

    aput v2, v5, v4

    const/16 v2, 0x12d

    const/16 v4, 0x12

    invoke-static {v2, v4}, Labf;->a(II)Labf;

    move-result-object v2

    invoke-static {v2, v5}, Laap;->a(Labf;[I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->e()Laas;

    move-result-object v2

    iput-object v2, p0, Landroidx/media/filterpacks/image/GrainFilter;->mNoiseFrame:Laas;

    .line 138
    :cond_2
    iget-object v2, p0, Landroidx/media/filterpacks/image/GrainFilter;->mGrainShader:Lacm;

    const-string v4, "scale"

    iget v5, p0, Landroidx/media/filterpacks/image/GrainFilter;->mScale:F

    invoke-virtual {v2, v4, v5}, Lacm;->a(Ljava/lang/String;F)V

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v4, 0x0

    iget-object v5, p0, Landroidx/media/filterpacks/image/GrainFilter;->mRandom:Ljava/util/Random;

    invoke-virtual {v5}, Ljava/util/Random;->nextFloat()F

    move-result v5

    aput v5, v2, v4

    const/4 v4, 0x1

    iget-object v5, p0, Landroidx/media/filterpacks/image/GrainFilter;->mRandom:Ljava/util/Random;

    invoke-virtual {v5}, Ljava/util/Random;->nextFloat()F

    move-result v5

    aput v5, v2, v4

    .line 140
    iget-object v2, p0, Landroidx/media/filterpacks/image/GrainFilter;->mNoiseShader:Lacm;

    const/4 v4, 0x0

    new-array v4, v4, [Laas;

    iget-object v5, p0, Landroidx/media/filterpacks/image/GrainFilter;->mNoiseFrame:Laas;

    invoke-virtual {v2, v4, v5}, Lacm;->a([Laas;Laas;)V

    .line 141
    const/4 v2, 0x2

    new-array v2, v2, [Laas;

    const/4 v4, 0x0

    aput-object v1, v2, v4

    const/4 v1, 0x1

    iget-object v4, p0, Landroidx/media/filterpacks/image/GrainFilter;->mNoiseFrame:Laas;

    aput-object v4, v2, v1

    .line 142
    iget-object v1, p0, Landroidx/media/filterpacks/image/GrainFilter;->mGrainShader:Lacm;

    invoke-virtual {v1, v2, v3}, Lacm;->a([Laas;Laas;)V

    .line 144
    invoke-virtual {v0, v3}, Lacv;->a(Laap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 145
    monitor-exit p0

    return-void

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public k()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Landroidx/media/filterpacks/image/GrainFilter;->mNoiseFrame:Laas;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Landroidx/media/filterpacks/image/GrainFilter;->mNoiseFrame:Laas;

    invoke-virtual {v0}, Laas;->f()Laap;

    .line 101
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/image/GrainFilter;->mNoiseFrame:Laas;

    .line 103
    :cond_0
    return-void
.end method
