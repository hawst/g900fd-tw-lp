.class public Landroidx/media/filterpacks/image/DuotoneFilter;
.super Laak;
.source "PG"


# instance fields
.field private final mDuotoneShader:Ljava/lang/String;

.field private mFirstColor:I

.field private mSecondColor:I

.field private mShader:Lacm;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 23
    const/high16 v0, -0x10000

    iput v0, p0, Landroidx/media/filterpacks/image/DuotoneFilter;->mFirstColor:I

    .line 24
    const/16 v0, -0x100

    iput v0, p0, Landroidx/media/filterpacks/image/DuotoneFilter;->mSecondColor:I

    .line 28
    const-string v0, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec3 first;\nuniform vec3 second;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float energy = (color.r + color.g + color.b) * 0.3333;\n  vec3 new_color = (1.0 - energy) * first + energy * second;\n  gl_FragColor = vec4(new_color.rgb, color.a);\n}\n"

    iput-object v0, p0, Landroidx/media/filterpacks/image/DuotoneFilter;->mDuotoneShader:Ljava/lang/String;

    .line 44
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 60
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "firstColor"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 61
    const-string v0, "mFirstColor"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 62
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 63
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "secondColor"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "mSecondColor"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 65
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 48
    invoke-static {v2, v4}, Labf;->a(II)Labf;

    move-result-object v0

    .line 49
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 50
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "firstColor"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v5, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "secondColor"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v5, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 71
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec3 first;\nuniform vec3 second;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  float energy = (color.r + color.g + color.b) * 0.3333;\n  vec3 new_color = (1.0 - energy) * first + energy * second;\n  gl_FragColor = vec4(new_color.rgb, color.a);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/DuotoneFilter;->mShader:Lacm;

    .line 72
    return-void
.end method

.method protected i()V
    .locals 10

    .prologue
    const/4 v5, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/high16 v6, 0x437f0000    # 255.0f

    .line 88
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/DuotoneFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 89
    const-string v1, "image"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/image/DuotoneFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v1

    .line 90
    invoke-virtual {v1}, Laas;->i()[I

    move-result-object v2

    .line 91
    invoke-virtual {v0, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->e()Laas;

    move-result-object v2

    .line 92
    new-array v3, v5, [F

    iget v4, p0, Landroidx/media/filterpacks/image/DuotoneFilter;->mFirstColor:I

    invoke-static {v4}, Landroid/graphics/Color;->red(I)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v6

    aput v4, v3, v7

    iget v4, p0, Landroidx/media/filterpacks/image/DuotoneFilter;->mFirstColor:I

    invoke-static {v4}, Landroid/graphics/Color;->green(I)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v6

    aput v4, v3, v8

    iget v4, p0, Landroidx/media/filterpacks/image/DuotoneFilter;->mFirstColor:I

    invoke-static {v4}, Landroid/graphics/Color;->blue(I)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v6

    aput v4, v3, v9

    new-array v4, v5, [F

    iget v5, p0, Landroidx/media/filterpacks/image/DuotoneFilter;->mSecondColor:I

    invoke-static {v5}, Landroid/graphics/Color;->red(I)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v6

    aput v5, v4, v7

    iget v5, p0, Landroidx/media/filterpacks/image/DuotoneFilter;->mSecondColor:I

    invoke-static {v5}, Landroid/graphics/Color;->green(I)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v6

    aput v5, v4, v8

    iget v5, p0, Landroidx/media/filterpacks/image/DuotoneFilter;->mSecondColor:I

    invoke-static {v5}, Landroid/graphics/Color;->blue(I)I

    move-result v5

    int-to-float v5, v5

    div-float/2addr v5, v6

    aput v5, v4, v9

    iget-object v5, p0, Landroidx/media/filterpacks/image/DuotoneFilter;->mShader:Lacm;

    const-string v6, "first"

    invoke-virtual {v5, v6, v3}, Lacm;->a(Ljava/lang/String;[F)V

    iget-object v3, p0, Landroidx/media/filterpacks/image/DuotoneFilter;->mShader:Lacm;

    const-string v5, "second"

    invoke-virtual {v3, v5, v4}, Lacm;->a(Ljava/lang/String;[F)V

    .line 93
    iget-object v3, p0, Landroidx/media/filterpacks/image/DuotoneFilter;->mShader:Lacm;

    invoke-virtual {v3, v1, v2}, Lacm;->a(Laas;Laas;)V

    .line 94
    invoke-virtual {v0, v2}, Lacv;->a(Laap;)V

    .line 95
    return-void
.end method
