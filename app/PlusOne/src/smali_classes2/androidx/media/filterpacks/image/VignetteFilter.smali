.class public Landroidx/media/filterpacks/image/VignetteFilter;
.super Laak;
.source "PG"


# static fields
.field private static final mVignetteShaderCode:Ljava/lang/String; = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform float range;\nuniform float inv_max_dist;\nuniform float shade;\nuniform vec2 scale;\nvarying vec2 v_texcoord;\nvoid main() {\n  const float slope = 20.0;\n  vec2 coord = v_texcoord - vec2(0.5, 0.5);\n  float dist = length(coord * scale);\n  float lumen = shade / (1.0 + exp((dist * inv_max_dist - range) * slope)) + (1.0 - shade);\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  gl_FragColor = vec4(color.rgb * lumen, color.a);\n}\n"


# instance fields
.field private mHeight:I

.field private mScale:F

.field private final mShade:F

.field private mShader:Lacm;

.field private mWidth:I


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 58
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 32
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mScale:F

    .line 33
    iput v1, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mWidth:I

    .line 34
    iput v1, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mHeight:I

    .line 36
    const v0, 0x3f59999a    # 0.85f

    iput v0, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mShade:F

    .line 59
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 2

    .prologue
    .line 74
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "scale"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    const-string v0, "mScale"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 76
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->a(Z)V

    .line 78
    :cond_0
    return-void
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x2

    .line 63
    invoke-static {v2, v5}, Labf;->a(II)Labf;

    move-result-object v0

    .line 64
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 65
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "scale"

    const/4 v3, 0x1

    sget-object v4, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v4}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 82
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform float range;\nuniform float inv_max_dist;\nuniform float shade;\nuniform vec2 scale;\nvarying vec2 v_texcoord;\nvoid main() {\n  const float slope = 20.0;\n  vec2 coord = v_texcoord - vec2(0.5, 0.5);\n  float dist = length(coord * scale);\n  float lumen = shade / (1.0 + exp((dist * inv_max_dist - range) * slope)) + (1.0 - shade);\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  gl_FragColor = vec4(color.rgb * lumen, color.a);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mShader:Lacm;

    .line 83
    return-void
.end method

.method protected declared-synchronized i()V
    .locals 8

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    .line 104
    monitor-enter p0

    :try_start_0
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/VignetteFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 105
    const-string v1, "image"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/image/VignetteFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v1

    .line 106
    invoke-virtual {v1}, Laas;->i()[I

    move-result-object v2

    .line 107
    invoke-virtual {v0, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->e()Laas;

    move-result-object v2

    .line 109
    invoke-virtual {v1}, Laas;->j()I

    move-result v3

    iget v4, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mWidth:I

    if-ne v3, v4, :cond_0

    invoke-virtual {v1}, Laas;->k()I

    move-result v3

    iget v4, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mHeight:I

    if-eq v3, v4, :cond_1

    .line 110
    :cond_0
    invoke-virtual {v1}, Laas;->j()I

    move-result v3

    iput v3, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mWidth:I

    .line 111
    invoke-virtual {v1}, Laas;->k()I

    move-result v3

    iput v3, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mHeight:I

    .line 112
    iget-object v3, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mShader:Lacm;

    if-eqz v3, :cond_1

    const/4 v3, 0x2

    new-array v3, v3, [F

    iget v4, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mWidth:I

    iget v5, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mHeight:I

    if-le v4, v5, :cond_2

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    aput v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mHeight:I

    int-to-float v5, v5

    iget v6, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mWidth:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    aput v5, v3, v4

    :goto_0
    const/4 v4, 0x0

    aget v4, v3, v4

    const/4 v5, 0x0

    aget v5, v3, v5

    mul-float/2addr v4, v5

    const/4 v5, 0x1

    aget v5, v3, v5

    const/4 v6, 0x1

    aget v6, v3, v6

    mul-float/2addr v5, v6

    add-float/2addr v4, v5

    invoke-static {v4}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v4

    const/high16 v5, 0x3f000000    # 0.5f

    mul-float/2addr v4, v5

    iget-object v5, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mShader:Lacm;

    const-string v6, "scale"

    invoke-virtual {v5, v6, v3}, Lacm;->a(Ljava/lang/String;[F)V

    iget-object v3, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mShader:Lacm;

    const-string v5, "inv_max_dist"

    div-float v4, v7, v4

    invoke-virtual {v3, v5, v4}, Lacm;->a(Ljava/lang/String;F)V

    iget-object v3, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mShader:Lacm;

    const-string v4, "shade"

    const v5, 0x3f59999a    # 0.85f

    invoke-virtual {v3, v4, v5}, Lacm;->a(Ljava/lang/String;F)V

    .line 118
    :cond_1
    iget-object v3, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mShader:Lacm;

    const-string v4, "range"

    const v5, 0x3fa66666    # 1.3f

    iget v6, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mScale:F

    invoke-static {v6}, Landroid/util/FloatMath;->sqrt(F)F

    move-result v6

    const v7, 0x3f333333    # 0.7f

    mul-float/2addr v6, v7

    sub-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Lacm;->a(Ljava/lang/String;F)V

    .line 119
    iget-object v3, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mShader:Lacm;

    invoke-virtual {v3, v1, v2}, Lacm;->a(Laas;Laas;)V

    .line 121
    invoke-virtual {v0, v2}, Lacv;->a(Laap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    monitor-exit p0

    return-void

    .line 112
    :cond_2
    const/4 v4, 0x0

    :try_start_1
    iget v5, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mWidth:I

    int-to-float v5, v5

    iget v6, p0, Landroidx/media/filterpacks/image/VignetteFilter;->mHeight:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    aput v5, v3, v4

    const/4 v4, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    aput v5, v3, v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 104
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
