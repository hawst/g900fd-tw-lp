.class public Landroidx/media/filterpacks/image/PosterizeFilter;
.super Laak;
.source "PG"


# static fields
.field private static final mPosterizeShaderCode:Ljava/lang/String; = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform float binSize;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  vec4 bc = mod(color, binSize);\n  float bs2 = binSize / 2.0;\n  vec3 result;\n  result.r = (bc.r >= bs2) ? color.r + binSize - bc.r : color.r - bc.r;\n  result.g = (bc.g >= bs2) ? color.g + binSize - bc.g : color.g - bc.g;\n  result.b = (bc.b >= bs2) ? color.b + binSize - bc.b : color.b - bc.b;\n  gl_FragColor = vec4(result, color.a);\n}\n"


# instance fields
.field private mLevels:I

.field private mShader:Lacm;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 24
    const/4 v0, 0x2

    iput v0, p0, Landroidx/media/filterpacks/image/PosterizeFilter;->mLevels:I

    .line 45
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 2

    .prologue
    .line 60
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "levels"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    const-string v0, "mLevels"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 62
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->a(Z)V

    .line 64
    :cond_0
    return-void
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x2

    .line 49
    invoke-static {v2, v5}, Labf;->a(II)Labf;

    move-result-object v0

    .line 50
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 51
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "levels"

    const/4 v3, 0x1

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v4}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 68
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform float binSize;\nvarying vec2 v_texcoord;\nvoid main() {\n  vec4 color = texture2D(tex_sampler_0, v_texcoord);\n  vec4 bc = mod(color, binSize);\n  float bs2 = binSize / 2.0;\n  vec3 result;\n  result.r = (bc.r >= bs2) ? color.r + binSize - bc.r : color.r - bc.r;\n  result.g = (bc.g >= bs2) ? color.g + binSize - bc.g : color.g - bc.g;\n  result.b = (bc.b >= bs2) ? color.b + binSize - bc.b : color.b - bc.b;\n  gl_FragColor = vec4(result, color.a);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/image/PosterizeFilter;->mShader:Lacm;

    .line 69
    return-void
.end method

.method protected i()V
    .locals 7

    .prologue
    .line 73
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/PosterizeFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 74
    const-string v1, "image"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/image/PosterizeFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v1

    .line 75
    invoke-virtual {v1}, Laas;->i()[I

    move-result-object v2

    .line 76
    invoke-virtual {v0, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->e()Laas;

    move-result-object v2

    .line 77
    iget v3, p0, Landroidx/media/filterpacks/image/PosterizeFilter;->mLevels:I

    const/4 v4, 0x2

    if-ge v3, v4, :cond_0

    .line 78
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Posterize filter obtained levels less than 2 ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Landroidx/media/filterpacks/image/PosterizeFilter;->mLevels:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_0
    iget-object v3, p0, Landroidx/media/filterpacks/image/PosterizeFilter;->mShader:Lacm;

    const-string v4, "binSize"

    const/high16 v5, 0x3f800000    # 1.0f

    iget v6, p0, Landroidx/media/filterpacks/image/PosterizeFilter;->mLevels:I

    add-int/lit8 v6, v6, -0x1

    int-to-float v6, v6

    div-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Lacm;->a(Ljava/lang/String;F)V

    .line 82
    iget-object v3, p0, Landroidx/media/filterpacks/image/PosterizeFilter;->mShader:Lacm;

    invoke-virtual {v3, v1, v2}, Lacm;->a(Laas;Laas;)V

    .line 83
    invoke-virtual {v0, v2}, Lacv;->a(Laap;)V

    .line 84
    return-void
.end method
