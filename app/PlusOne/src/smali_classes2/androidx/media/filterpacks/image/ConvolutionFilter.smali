.class public final Landroidx/media/filterpacks/image/ConvolutionFilter;
.super Laak;
.source "PG"


# static fields
.field private static final mConvolutionShader:Ljava/lang/String; = "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = __CONVOLUTION__;\n}\n"


# instance fields
.field private mMask:[F

.field private mMaskHeight:I

.field private mMaskWidth:I

.field private mOldDim:[I

.field private mOldMask:[F

.field private mShader:Lacm;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 44
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 28
    iput-object v0, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mMask:[F

    .line 29
    iput-object v0, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mOldMask:[F

    .line 30
    iput-object v0, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mOldDim:[I

    .line 31
    iput v1, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mMaskWidth:I

    .line 32
    iput v1, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mMaskHeight:I

    .line 45
    return-void
.end method

.method private a(II)Ljava/lang/String;
    .locals 11

    .prologue
    .line 120
    const/4 v1, 0x0

    .line 121
    iget v0, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mMaskWidth:I

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v4, v0, 0x2

    .line 122
    iget v0, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mMaskHeight:I

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v5, v0, 0x2

    .line 123
    new-instance v6, Ljava/util/Vector;

    invoke-direct {v6}, Ljava/util/Vector;-><init>()V

    .line 124
    neg-int v0, v5

    move v3, v0

    :goto_0
    if-gt v3, v5, :cond_1

    .line 125
    neg-int v0, v4

    :goto_1
    if-gt v0, v4, :cond_0

    .line 126
    int-to-float v2, v0

    int-to-float v7, p1

    div-float v7, v2, v7

    .line 127
    int-to-float v2, v3

    int-to-float v8, p2

    div-float v8, v2, v8

    .line 128
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mMask:[F

    add-int/lit8 v2, v1, 0x1

    aget v1, v10, v1

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v9, " * texture2D(tex_sampler_0, vec2(v_texcoord.x + "

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, ", v_texcoord.y + "

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v7, "))"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 125
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_1

    .line 124
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 132
    :cond_1
    const-string v0, " + "

    invoke-static {v0, v6}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 62
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mask"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    const-string v0, "mMask"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 64
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 72
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "maskWidth"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 66
    const-string v0, "mMaskWidth"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 68
    :cond_2
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "maskHeight"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    const-string v0, "mMaskHeight"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 70
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 49
    invoke-static {v2, v5}, Labf;->a(II)Labf;

    move-result-object v0

    .line 50
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 51
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "mask"

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "maskWidth"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "maskHeight"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 12

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 76
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/ConvolutionFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v3

    .line 77
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/image/ConvolutionFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v4

    .line 78
    invoke-virtual {v4}, Laas;->i()[I

    move-result-object v5

    .line 79
    invoke-virtual {v3, v5}, Lacv;->a([I)Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v6

    .line 80
    iget-object v0, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mMask:[F

    if-nez v0, :cond_0

    .line 81
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "No mask specified!"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_0
    iget-object v0, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mOldMask:[F

    iget-object v7, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mMask:[F

    invoke-static {v0, v7}, Ljava/util/Arrays;->equals([F[F)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mOldDim:[I

    invoke-static {v5, v0}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    if-nez v0, :cond_7

    .line 84
    :cond_1
    iget v0, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mMaskWidth:I

    if-eqz v0, :cond_2

    iget v0, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mMaskHeight:I

    if-nez v0, :cond_6

    :cond_2
    iget-object v0, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mMask:[F

    array-length v0, v0

    int-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-int v0, v8

    int-to-double v10, v0

    cmpl-double v0, v8, v10

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal mask size "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mMask:[F

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "! Must be power of 2 size!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    iget-object v0, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mMask:[F

    array-length v0, v0

    int-to-double v8, v0

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-int v0, v8

    rem-int/lit8 v7, v0, 0x2

    if-eq v7, v1, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal mask size "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mMask:[F

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "! Each dimension must contain odd number of entries!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    iput v0, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mMaskWidth:I

    iput v0, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mMaskHeight:I

    .line 85
    :cond_6
    aget v0, v5, v2

    aget v1, v5, v1

    invoke-direct {p0, v0, v1}, Landroidx/media/filterpacks/image/ConvolutionFilter;->a(II)Ljava/lang/String;

    move-result-object v0

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = __CONVOLUTION__;\n}\n"

    const-string v2, "__CONVOLUTION__"

    invoke-virtual {v1, v2, v0}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lacm;

    invoke-direct {v1, v0}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mShader:Lacm;

    .line 86
    iget-object v0, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mMask:[F

    iput-object v0, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mOldMask:[F

    .line 87
    iput-object v5, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mOldDim:[I

    .line 89
    :cond_7
    iget-object v0, p0, Landroidx/media/filterpacks/image/ConvolutionFilter;->mShader:Lacm;

    invoke-virtual {v0, v4, v6}, Lacm;->a(Laas;Laas;)V

    .line 90
    invoke-virtual {v3, v6}, Lacv;->a(Laap;)V

    .line 91
    return-void
.end method
