.class public final Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;
.super Laak;
.source "PG"


# instance fields
.field private final mFragmentShader:Ljava/lang/String;

.field private mGraphShader:Lacm;

.field private mHistogram:[I

.field private mIdShader:Lacm;

.field private final mVertexShader:Ljava/lang/String;

.field private mVertices:[F

.field private mYScale:F


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 38
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->mYScale:F

    .line 42
    const-string v0, "attribute vec4 a_position2;\nvoid main() {\n  gl_Position = a_position2;\n}\n"

    iput-object v0, p0, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->mVertexShader:Ljava/lang/String;

    .line 48
    const-string v0, "precision mediump float;\nvoid main() {\n  gl_FragColor = vec4(1.0, 0.0, 0.0, 0.5);\n}\n"

    iput-object v0, p0, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->mFragmentShader:Ljava/lang/String;

    .line 56
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 72
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "YScale"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 73
    const-string v0, "mYScale"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 79
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "histogram"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    const-string v0, "mHistogram"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 77
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x2

    .line 60
    invoke-static {v2, v5}, Labf;->a(II)Labf;

    move-result-object v0

    .line 61
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 62
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "histogram"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v5, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "YScale"

    const/4 v3, 0x1

    sget-object v4, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v4}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "composite"

    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 3

    .prologue
    .line 83
    invoke-static {}, Lacm;->a()Lacm;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->mIdShader:Lacm;

    .line 84
    new-instance v0, Lacm;

    const-string v1, "attribute vec4 a_position2;\nvoid main() {\n  gl_Position = a_position2;\n}\n"

    const-string v2, "precision mediump float;\nvoid main() {\n  gl_FragColor = vec4(1.0, 0.0, 0.0, 0.5);\n}\n"

    invoke-direct {v0, v1, v2}, Lacm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->mGraphShader:Lacm;

    .line 85
    return-void
.end method

.method protected i()V
    .locals 13

    .prologue
    .line 90
    const-string v0, "composite"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->b(Ljava/lang/String;)Lacv;

    move-result-object v2

    .line 91
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Laas;->i()[I

    move-result-object v3

    .line 93
    invoke-virtual {v2, v3}, Lacv;->a([I)Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v4

    .line 94
    iget-object v1, p0, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->mIdShader:Lacm;

    invoke-virtual {v1, v0, v4}, Lacm;->a(Laas;Laas;)V

    .line 95
    const/4 v0, 0x0

    aget v0, v3, v0

    const/4 v1, 0x1

    aget v1, v3, v1

    mul-int v5, v0, v1

    .line 98
    iget-object v0, p0, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->mHistogram:[I

    array-length v6, v0

    .line 99
    const/high16 v0, 0x40000000    # 2.0f

    int-to-float v1, v6

    const/high16 v7, 0x40000000    # 2.0f

    add-float/2addr v1, v7

    div-float v7, v0, v1

    .line 101
    mul-int/lit8 v0, v6, 0x4

    new-array v0, v0, [F

    iput-object v0, p0, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->mVertices:[F

    .line 102
    const/high16 v0, -0x40800000    # -1.0f

    add-float v1, v7, v0

    .line 103
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v6, :cond_0

    .line 104
    iget-object v8, p0, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->mVertices:[F

    mul-int/lit8 v9, v0, 0x4

    aput v1, v8, v9

    .line 105
    iget-object v8, p0, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->mVertices:[F

    mul-int/lit8 v9, v0, 0x4

    add-int/lit8 v9, v9, 0x1

    const/high16 v10, 0x3f800000    # 1.0f

    aput v10, v8, v9

    .line 106
    iget-object v8, p0, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->mVertices:[F

    mul-int/lit8 v9, v0, 0x4

    add-int/lit8 v9, v9, 0x2

    aput v1, v8, v9

    .line 107
    iget-object v8, p0, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->mVertices:[F

    mul-int/lit8 v9, v0, 0x4

    add-int/lit8 v9, v9, 0x3

    const/high16 v10, 0x3f800000    # 1.0f

    const/high16 v11, 0x40000000    # 2.0f

    iget-object v12, p0, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->mHistogram:[I

    aget v12, v12, v0

    int-to-float v12, v12

    mul-float/2addr v11, v12

    iget v12, p0, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->mYScale:F

    mul-float/2addr v11, v12

    int-to-float v12, v5

    div-float/2addr v11, v12

    sub-float/2addr v10, v11

    aput v10, v8, v9

    .line 111
    add-float/2addr v1, v7

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 115
    :cond_0
    iget-object v0, p0, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->mGraphShader:Lacm;

    const-string v1, "a_position2"

    iget-object v5, p0, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->mVertices:[F

    const/4 v8, 0x2

    invoke-virtual {v0, v1, v5, v8}, Lacm;->a(Ljava/lang/String;[FI)V

    .line 116
    const v0, 0x3f19999a    # 0.6f

    mul-float/2addr v0, v7

    const/4 v1, 0x0

    aget v1, v3, v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Landroid/opengl/GLES20;->glLineWidth(F)V

    .line 117
    iget-object v0, p0, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->mGraphShader:Lacm;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lacm;->a(I)V

    .line 118
    iget-object v0, p0, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->mGraphShader:Lacm;

    mul-int/lit8 v1, v6, 0x2

    invoke-virtual {v0, v1}, Lacm;->b(I)V

    .line 119
    iget-object v0, p0, Landroidx/media/filterpacks/miscellaneous/DisplayHistogram;->mGraphShader:Lacm;

    invoke-virtual {v0, v4}, Lacm;->a(Laas;)V

    .line 122
    invoke-virtual {v2, v4}, Lacv;->a(Laap;)V

    .line 123
    return-void
.end method
