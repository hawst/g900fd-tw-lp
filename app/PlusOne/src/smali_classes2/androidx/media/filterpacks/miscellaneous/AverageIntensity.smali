.class public final Landroidx/media/filterpacks/miscellaneous/AverageIntensity;
.super Laak;
.source "PG"


# static fields
.field private static mBinHeight:I

.field private static mBinWidth:I

.field private static mBins:I


# instance fields
.field private final mFragShader:Ljava/lang/String;

.field private mHistogram:[I

.field private mQuad:Ladp;

.field private mShader:Lacm;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x2

    .line 44
    sput v0, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mBinWidth:I

    .line 45
    sput v0, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mBinHeight:I

    .line 139
    const-string v0, "filterframework_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 140
    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    .line 58
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 48
    invoke-static {v0, v0, v1, v1}, Ladp;->a(FFFF)Ladp;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mQuad:Ladp;

    .line 49
    const-string v0, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n    gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n"

    iput-object v0, p0, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mFragShader:Ljava/lang/String;

    .line 59
    return-void
.end method

.method private native averageIntensity(Ljava/nio/ByteBuffer;Ljava/nio/IntBuffer;IIII)V
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 80
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "wnum"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const-string v0, "mBinWidth"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 82
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 84
    :cond_0
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "hnum"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    const-string v0, "mBinHeight"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 86
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 88
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "targetQuad"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 89
    const-string v0, "mQuad"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 90
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 92
    :cond_2
    return-void
.end method

.method public c()Lacx;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 63
    const/16 v0, 0x12d

    invoke-static {v0, v3}, Labf;->a(II)Labf;

    move-result-object v0

    .line 64
    new-instance v1, Lacx;

    invoke-direct {v1}, Lacx;-><init>()V

    const-string v2, "image"

    invoke-virtual {v1, v2, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "wnum"

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "hnum"

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "targetQuad"

    const-class v2, Ladp;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "histogram"

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v4, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 75
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n    gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mShader:Lacm;

    .line 76
    return-void
.end method

.method protected i()V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 96
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v0

    .line 97
    invoke-virtual {v0}, Laas;->i()[I

    move-result-object v4

    .line 98
    const-string v1, "AverageIntensity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "width "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget v3, v4, v7

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, v4, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    aget v1, v4, v7

    int-to-float v1, v1

    iget-object v2, p0, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mQuad:Ladp;

    invoke-virtual {v2}, Ladp;->g()Landroid/graphics/PointF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/PointF;->length()F

    move-result v2

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    aput v1, v4, v7

    .line 102
    aget v1, v4, v5

    int-to-float v1, v1

    iget-object v2, p0, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mQuad:Ladp;

    invoke-virtual {v2}, Ladp;->h()Landroid/graphics/PointF;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/PointF;->length()F

    move-result v2

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    aput v1, v4, v5

    .line 103
    const/16 v1, 0x12d

    const/16 v2, 0x12

    invoke-static {v1, v2}, Labf;->a(II)Labf;

    move-result-object v1

    .line 104
    invoke-static {v1, v4}, Laap;->a(Labf;[I)Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v8

    .line 105
    iget-object v1, p0, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mShader:Lacm;

    iget-object v2, p0, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mQuad:Ladp;

    invoke-virtual {v1, v2}, Lacm;->a(Ladp;)V

    .line 106
    iget-object v1, p0, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mShader:Lacm;

    invoke-virtual {v1, v0, v8}, Lacm;->a(Laas;Laas;)V

    .line 108
    const-string v0, "AverageIntensity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "width "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget v2, v4, v7

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " height "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget v2, v4, v5

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 110
    sget v0, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mBinWidth:I

    sget v1, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mBinHeight:I

    mul-int/2addr v0, v1

    sput v0, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mBins:I

    .line 111
    sget v0, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mBins:I

    new-array v0, v0, [I

    iput-object v0, p0, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mHistogram:[I

    .line 114
    sget v0, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mBins:I

    shl-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 115
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 116
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asIntBuffer()Ljava/nio/IntBuffer;

    move-result-object v2

    .line 117
    invoke-virtual {v8, v5}, Laas;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 120
    aget v3, v4, v7

    aget v4, v4, v5

    sget v5, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mBinWidth:I

    sget v6, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mBinHeight:I

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->averageIntensity(Ljava/nio/ByteBuffer;Ljava/nio/IntBuffer;IIII)V

    .line 123
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 124
    invoke-virtual {v8}, Laas;->h()V

    .line 125
    invoke-virtual {v2}, Ljava/nio/IntBuffer;->rewind()Ljava/nio/Buffer;

    move v0, v7

    .line 126
    :goto_0
    sget v1, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mBins:I

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mHistogram:[I

    invoke-virtual {v2}, Ljava/nio/IntBuffer;->get()I

    move-result v3

    aput v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 128
    :cond_0
    const-string v0, "histogram"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 129
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lacv;->a([I)Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->b()Labh;

    move-result-object v1

    .line 130
    iget-object v2, p0, Landroidx/media/filterpacks/miscellaneous/AverageIntensity;->mHistogram:[I

    invoke-virtual {v1, v2}, Labg;->a(Ljava/lang/Object;)V

    .line 131
    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    .line 133
    invoke-virtual {v8}, Laas;->f()Laap;

    .line 134
    return-void
.end method
