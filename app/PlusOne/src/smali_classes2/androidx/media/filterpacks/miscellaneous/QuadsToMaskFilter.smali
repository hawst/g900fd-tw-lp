.class public final Landroidx/media/filterpacks/miscellaneous/QuadsToMaskFilter;
.super Laak;
.source "PG"


# instance fields
.field private mBackgroundShader:Lacm;

.field private final mBgFragmentShader:Ljava/lang/String;

.field private mImageSize:[I

.field private final mQuadFragmentShader:Ljava/lang/String;

.field private mQuadShader:Lacm;

.field private mQuads:[Ladp;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 39
    const-string v0, "precision mediump float;\nvoid main() {\n  gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);\n}\n"

    iput-object v0, p0, Landroidx/media/filterpacks/miscellaneous/QuadsToMaskFilter;->mBgFragmentShader:Ljava/lang/String;

    .line 47
    const-string v0, "precision mediump float;\nvoid main() {\n  gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);\n}\n"

    iput-object v0, p0, Landroidx/media/filterpacks/miscellaneous/QuadsToMaskFilter;->mQuadFragmentShader:Ljava/lang/String;

    .line 55
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 69
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "imageSize"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    const-string v0, "mImageSize"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 71
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 72
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "quads"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    const-string v0, "mQuads"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 59
    const/16 v0, 0x12d

    const/16 v1, 0x10

    invoke-static {v0, v1}, Labf;->a(II)Labf;

    move-result-object v0

    .line 60
    new-instance v1, Lacx;

    invoke-direct {v1}, Lacx;-><init>()V

    const-string v2, "quads"

    const-class v3, Ladp;

    invoke-static {v3}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v1, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v1

    const-string v2, "imageSize"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v1, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v1

    const-string v2, "mask"

    invoke-virtual {v1, v2, v4, v0}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 80
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nvoid main() {\n  gl_FragColor = vec4(0.0, 0.0, 0.0, 0.0);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/miscellaneous/QuadsToMaskFilter;->mBackgroundShader:Lacm;

    .line 81
    new-instance v0, Lacm;

    const-string v1, "precision mediump float;\nvoid main() {\n  gl_FragColor = vec4(1.0, 1.0, 1.0, 1.0);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/miscellaneous/QuadsToMaskFilter;->mQuadShader:Lacm;

    .line 82
    return-void
.end method

.method protected i()V
    .locals 5

    .prologue
    .line 86
    const-string v0, "mask"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/miscellaneous/QuadsToMaskFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    .line 87
    iget-object v0, p0, Landroidx/media/filterpacks/miscellaneous/QuadsToMaskFilter;->mImageSize:[I

    invoke-virtual {v1, v0}, Lacv;->a([I)Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v2

    .line 90
    iget-object v0, p0, Landroidx/media/filterpacks/miscellaneous/QuadsToMaskFilter;->mBackgroundShader:Lacm;

    invoke-virtual {v0, v2}, Lacm;->a(Laas;)V

    .line 93
    const/4 v0, 0x0

    :goto_0
    iget-object v3, p0, Landroidx/media/filterpacks/miscellaneous/QuadsToMaskFilter;->mQuads:[Ladp;

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 94
    iget-object v3, p0, Landroidx/media/filterpacks/miscellaneous/QuadsToMaskFilter;->mQuadShader:Lacm;

    iget-object v4, p0, Landroidx/media/filterpacks/miscellaneous/QuadsToMaskFilter;->mQuads:[Ladp;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Lacm;->b(Ladp;)V

    .line 95
    iget-object v3, p0, Landroidx/media/filterpacks/miscellaneous/QuadsToMaskFilter;->mQuadShader:Lacm;

    invoke-virtual {v3, v2}, Lacm;->a(Laas;)V

    .line 93
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 99
    :cond_0
    invoke-virtual {v1, v2}, Lacv;->a(Laap;)V

    .line 100
    return-void
.end method
