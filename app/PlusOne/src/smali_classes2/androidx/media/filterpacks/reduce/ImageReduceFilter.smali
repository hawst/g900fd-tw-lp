.class public Landroidx/media/filterpacks/reduce/ImageReduceFilter;
.super Laak;
.source "PG"


# static fields
.field public static final CHANNEL_AVG:I = 0x4

.field public static final CHANNEL_BLUE:I = 0x3

.field public static final CHANNEL_GRAY:I = 0x8

.field public static final CHANNEL_GREEN:I = 0x2

.field public static final CHANNEL_MAX:I = 0x7

.field public static final CHANNEL_MIN:I = 0x6

.field public static final CHANNEL_RED:I = 0x1

.field public static final CHANNEL_SUM:I = 0x5

.field public static final OPERATION_AVG:I = 0x3

.field public static final OPERATION_MAX:I = 0x1

.field public static final OPERATION_MIN:I = 0x2

.field public static final OPERATION_PRODUCT:I = 0x5

.field public static final OPERATION_SUM:I = 0x4


# instance fields
.field private mChannel:I

.field private mChannelListener:Lacr;

.field private mCurrentHeight:I

.field private mCurrentWidth:I

.field private mLevel:I

.field private mOperation:I

.field private mOperationListener:Lacr;

.field private mPyramid:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Laas;",
            ">;"
        }
    .end annotation
.end field

.field private mShader:Lacm;

.field private mShaderDirtyFlag:Z


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 108
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 94
    const/4 v0, -0x1

    iput v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mLevel:I

    .line 96
    iput v1, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mCurrentWidth:I

    .line 97
    iput v1, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mCurrentHeight:I

    .line 99
    const/4 v0, 0x3

    iput v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mOperation:I

    .line 100
    const/16 v0, 0x8

    iput v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mChannel:I

    .line 102
    iput-boolean v1, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mShaderDirtyFlag:Z

    .line 103
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mShader:Lacm;

    .line 105
    new-instance v0, Ljava/util/Vector;

    invoke-direct {v0}, Ljava/util/Vector;-><init>()V

    iput-object v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mPyramid:Ljava/util/Vector;

    .line 295
    new-instance v0, Laee;

    invoke-direct {v0, p0}, Laee;-><init>(Landroidx/media/filterpacks/reduce/ImageReduceFilter;)V

    iput-object v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mOperationListener:Lacr;

    .line 306
    new-instance v0, Laef;

    invoke-direct {v0, p0}, Laef;-><init>(Landroidx/media/filterpacks/reduce/ImageReduceFilter;)V

    iput-object v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mChannelListener:Lacr;

    .line 109
    return-void
.end method

.method public static synthetic a(Landroidx/media/filterpacks/reduce/ImageReduceFilter;)I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mOperation:I

    return v0
.end method

.method public static synthetic a(Landroidx/media/filterpacks/reduce/ImageReduceFilter;I)I
    .locals 0

    .prologue
    .line 49
    iput p1, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mOperation:I

    return p1
.end method

.method private a(ILaas;Laas;)Laas;
    .locals 1

    .prologue
    .line 197
    if-nez p1, :cond_0

    .line 202
    :goto_0
    return-object p2

    .line 199
    :cond_0
    iget v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mLevel:I

    if-lt p1, v0, :cond_1

    move-object p2, p3

    .line 200
    goto :goto_0

    .line 202
    :cond_1
    iget-object v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mPyramid:Ljava/util/Vector;

    invoke-virtual {v0, p1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laas;

    move-object p2, v0

    goto :goto_0
.end method

.method private a()V
    .locals 5

    .prologue
    .line 229
    iget-object v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mPyramid:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laas;

    .line 230
    invoke-virtual {v0}, Laas;->f()Laap;

    goto :goto_0

    .line 232
    :cond_0
    iget-object v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mPyramid:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 235
    iget v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mCurrentWidth:I

    iget v1, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mCurrentHeight:I

    invoke-virtual {p0, v0, v1}, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->a(II)[Laeg;

    move-result-object v1

    .line 238
    const/16 v0, 0x12d

    const/16 v2, 0x12

    invoke-static {v0, v2}, Labf;->a(II)Labf;

    move-result-object v2

    .line 240
    const/4 v0, 0x0

    :goto_1
    array-length v3, v1

    if-ge v0, v3, :cond_1

    .line 241
    aget-object v3, v1, v0

    invoke-virtual {v3}, Laeg;->c()[I

    move-result-object v3

    .line 242
    invoke-static {v2, v3}, Laap;->a(Labf;[I)Laap;

    move-result-object v3

    invoke-virtual {v3}, Laap;->e()Laas;

    move-result-object v3

    .line 243
    iget-object v4, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mPyramid:Ljava/util/Vector;

    invoke-virtual {v4, v3}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 240
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 245
    :cond_1
    return-void
.end method

.method public static synthetic a(Landroidx/media/filterpacks/reduce/ImageReduceFilter;Z)Z
    .locals 0

    .prologue
    .line 49
    iput-boolean p1, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mShaderDirtyFlag:Z

    return p1
.end method

.method public static synthetic b(Landroidx/media/filterpacks/reduce/ImageReduceFilter;)I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mChannel:I

    return v0
.end method

.method public static synthetic b(Landroidx/media/filterpacks/reduce/ImageReduceFilter;I)I
    .locals 0

    .prologue
    .line 49
    iput p1, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mChannel:I

    return p1
.end method


# virtual methods
.method public a(II)[Laeg;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 113
    if-lez p1, :cond_0

    if-gtz p2, :cond_1

    .line 114
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal image dimensions: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :cond_1
    new-instance v2, Ljava/util/Vector;

    invoke-direct {v2}, Ljava/util/Vector;-><init>()V

    .line 120
    new-instance v0, Laeg;

    invoke-direct {v0, p1, p2}, Laeg;-><init>(II)V

    .line 121
    :goto_0
    invoke-virtual {v2, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    .line 123
    invoke-virtual {v0}, Laeg;->a()I

    move-result v1

    if-ne v1, v4, :cond_2

    invoke-virtual {v0}, Laeg;->b()I

    move-result v1

    if-eq v1, v4, :cond_3

    .line 124
    :cond_2
    new-instance v1, Laeg;

    invoke-virtual {v0}, Laeg;->a()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v0}, Laeg;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    div-int/lit8 v0, v0, 0x2

    invoke-direct {v1, v3, v0}, Laeg;-><init>(II)V

    move-object v0, v1

    .line 121
    goto :goto_0

    .line 127
    :cond_3
    const/4 v0, 0x0

    new-array v0, v0, [Laeg;

    invoke-virtual {v2, v0}, Ljava/util/Vector;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laeg;

    return-object v0
.end method

.method protected b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 145
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "level"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 146
    const-string v0, "mLevel"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 147
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 148
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "operation"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 149
    iget-object v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mOperationListener:Lacr;

    invoke-virtual {p1, v0}, Lacp;->a(Lacr;)V

    .line 150
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 151
    :cond_2
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "channel"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mChannelListener:Lacr;

    invoke-virtual {p1, v0}, Lacp;->a(Lacr;)V

    .line 153
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 132
    invoke-static {v2, v5}, Labf;->a(II)Labf;

    move-result-object v0

    .line 133
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 134
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "operation"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "level"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "channel"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected h()V
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mShaderDirtyFlag:Z

    .line 160
    return-void
.end method

.method protected i()V
    .locals 13

    .prologue
    .line 164
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v3

    .line 165
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v4

    .line 166
    invoke-virtual {v4}, Laas;->i()[I

    move-result-object v1

    .line 169
    iget-boolean v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mShaderDirtyFlag:Z

    if-eqz v0, :cond_0

    .line 170
    new-instance v2, Lacm;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, "precision mediump float;\nuniform sampler2D tex_sampler_0;\nuniform vec2 pix;\nvarying vec2 v_texcoord;\n\nfloat reduce(float v0, float v1, float v2, float v3) {\n  return "

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mOperation:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown operation: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mOperation:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const-string v0, "max(max(v0, v1), max(v2, v3))"

    :goto_0
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ";\n}\n\nfloat colorValue(vec4 color) {\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "  return "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mChannel:I

    packed-switch v0, :pswitch_data_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown channel: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mChannel:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const-string v0, "min(min(v0, v1), min(v2, v3))"

    goto :goto_0

    :pswitch_2
    const-string v0, "(v0 + v1 + v2 + v3) / 4.0"

    goto :goto_0

    :pswitch_3
    const-string v0, "(v0 + v1 + v2 + v3)"

    goto :goto_0

    :pswitch_4
    const-string v0, "(v0 * v1 * v2 * v3)"

    goto :goto_0

    :pswitch_5
    const-string v0, "color.r"

    :goto_1
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ";\n}\nvoid main() {\n  float c0 = colorValue(texture2D(tex_sampler_0, v_texcoord + vec2(-pix.x, -pix.y)));\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "  float c1 = colorValue(texture2D(tex_sampler_0, v_texcoord + vec2(+pix.x, -pix.y)));\n  float c2 = colorValue(texture2D(tex_sampler_0, v_texcoord + vec2(-pix.x, +pix.y)));\n  float c3 = colorValue(texture2D(tex_sampler_0, v_texcoord + vec2(+pix.x, +pix.y)));\n  float r = reduce(c0, c1, c2, c3);\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "  gl_FragColor = vec4(r, r, r, 1.0);\n}\n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mShader:Lacm;

    .line 171
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mShaderDirtyFlag:Z

    .line 175
    :cond_0
    const/4 v0, 0x0

    aget v0, v1, v0

    iget v2, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mCurrentWidth:I

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    aget v0, v1, v0

    iget v2, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mCurrentHeight:I

    if-eq v0, v2, :cond_2

    .line 176
    :cond_1
    const/4 v0, 0x0

    aget v0, v1, v0

    iput v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mCurrentWidth:I

    .line 177
    const/4 v0, 0x1

    aget v0, v1, v0

    iput v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mCurrentHeight:I

    .line 178
    invoke-direct {p0}, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->a()V

    .line 182
    :cond_2
    iget v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mLevel:I

    iget-object v1, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mPyramid:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    iget v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mLevel:I

    if-gez v0, :cond_4

    :cond_3
    iget-object v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mPyramid:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mLevel:I

    .line 185
    :cond_4
    iget-object v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mPyramid:Ljava/util/Vector;

    iget v1, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mLevel:I

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laas;

    invoke-virtual {v0}, Laas;->i()[I

    move-result-object v0

    .line 186
    invoke-virtual {v3, v0}, Lacv;->a([I)Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v5

    .line 189
    const/4 v0, 0x0

    :goto_2
    iget v1, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mLevel:I

    if-ge v0, v1, :cond_7

    .line 190
    invoke-direct {p0, v0, v4, v5}, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->a(ILaas;Laas;)Laas;

    move-result-object v6

    add-int/lit8 v1, v0, 0x1

    invoke-direct {p0, v1, v4, v5}, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->a(ILaas;Laas;)Laas;

    move-result-object v7

    invoke-virtual {v6}, Laas;->j()I

    move-result v8

    invoke-virtual {v6}, Laas;->k()I

    move-result v9

    invoke-virtual {v7}, Laas;->j()I

    move-result v1

    invoke-virtual {v7}, Laas;->k()I

    move-result v10

    if-ne v1, v8, :cond_5

    const/high16 v1, 0x3f800000    # 1.0f

    move v2, v1

    :goto_3
    if-ne v10, v9, :cond_6

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_4
    iget-object v10, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mShader:Lacm;

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12, v2, v1}, Lacm;->a(FFFF)V

    iget-object v1, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mShader:Lacm;

    const-string v2, "pix"

    const/4 v10, 0x2

    new-array v10, v10, [F

    const/4 v11, 0x0

    const/high16 v12, 0x3f000000    # 0.5f

    int-to-float v8, v8

    div-float v8, v12, v8

    aput v8, v10, v11

    const/4 v8, 0x1

    const/high16 v11, 0x3f000000    # 0.5f

    int-to-float v9, v9

    div-float v9, v11, v9

    aput v9, v10, v8

    invoke-virtual {v1, v2, v10}, Lacm;->a(Ljava/lang/String;[F)V

    iget-object v1, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mShader:Lacm;

    invoke-virtual {v1, v6, v7}, Lacm;->a(Laas;Laas;)V

    .line 189
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 170
    :pswitch_6
    const-string v0, "color.g"

    goto/16 :goto_1

    :pswitch_7
    const-string v0, "color.b"

    goto/16 :goto_1

    :pswitch_8
    const-string v0, "(color.r + color.g + color.b) / 3.0"

    goto/16 :goto_1

    :pswitch_9
    const-string v0, "(color.r + color.g + color.b)"

    goto/16 :goto_1

    :pswitch_a
    const-string v0, "min(color.r, min(color.g, color.b))"

    goto/16 :goto_1

    :pswitch_b
    const-string v0, "max(color.r, max(color.g, color.b))"

    goto/16 :goto_1

    :pswitch_c
    const-string v0, "dot(color, vec4(0.299, 0.587, 0.114, 0))"

    goto/16 :goto_1

    .line 190
    :cond_5
    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v1, v2

    int-to-float v2, v8

    div-float/2addr v1, v2

    move v2, v1

    goto :goto_3

    :cond_6
    int-to-float v1, v10

    const/high16 v10, 0x40000000    # 2.0f

    mul-float/2addr v1, v10

    int-to-float v10, v9

    div-float/2addr v1, v10

    goto :goto_4

    .line 193
    :cond_7
    iget v0, p0, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->mLevel:I

    invoke-direct {p0, v0, v4, v5}, Landroidx/media/filterpacks/reduce/ImageReduceFilter;->a(ILaas;Laas;)Laas;

    move-result-object v0

    invoke-virtual {v3, v0}, Lacv;->a(Laap;)V

    .line 194
    return-void

    .line 170
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method
