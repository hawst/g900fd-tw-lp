.class public Landroidx/media/filterpacks/video/CameraSource;
.super Laak;
.source "PG"

# interfaces
.implements Laai;


# instance fields
.field private mNewFrameAvailable:Z

.field private mOutputType:Labf;

.field private mUseWallClock:Z


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 32
    iput-boolean v0, p0, Landroidx/media/filterpacks/video/CameraSource;->mNewFrameAvailable:Z

    .line 34
    iput-boolean v0, p0, Landroidx/media/filterpacks/video/CameraSource;->mUseWallClock:Z

    .line 38
    const/16 v0, 0x12d

    const/16 v1, 0x10

    invoke-static {v0, v1}, Labf;->a(II)Labf;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/video/CameraSource;->mOutputType:Labf;

    .line 39
    return-void
.end method

.method private declared-synchronized y()Z
    .locals 2

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Landroidx/media/filterpacks/video/CameraSource;->mNewFrameAvailable:Z

    .line 92
    if-eqz v0, :cond_0

    .line 93
    const/4 v1, 0x0

    iput-boolean v1, p0, Landroidx/media/filterpacks/video/CameraSource;->mNewFrameAvailable:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    :goto_0
    monitor-exit p0

    return v0

    .line 95
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Landroidx/media/filterpacks/video/CameraSource;->t()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 102
    monitor-enter p0

    .line 103
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Landroidx/media/filterpacks/video/CameraSource;->mNewFrameAvailable:Z

    .line 104
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    invoke-virtual {p0}, Landroidx/media/filterpacks/video/CameraSource;->u()V

    .line 106
    return-void

    .line 104
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b(Lacp;)V
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "useWallClock"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    const-string v0, "mUseWallClock"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 53
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->a(Z)V

    .line 55
    :cond_0
    return-void
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    .line 43
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "useWallClock"

    const/4 v2, 0x1

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "video"

    const/4 v2, 0x2

    iget-object v3, p0, Landroidx/media/filterpacks/video/CameraSource;->mOutputType:Labf;

    invoke-virtual {v0, v1, v2, v3}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected h()V
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Landroidx/media/filterpacks/video/CameraSource;->d()Lacs;

    move-result-object v0

    invoke-virtual {v0}, Lacs;->c()Lzw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lzw;->a(Laai;)V

    .line 60
    return-void
.end method

.method protected i()V
    .locals 8

    .prologue
    .line 64
    invoke-direct {p0}, Landroidx/media/filterpacks/video/CameraSource;->y()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 65
    invoke-virtual {p0}, Landroidx/media/filterpacks/video/CameraSource;->d()Lacs;

    move-result-object v0

    invoke-virtual {v0}, Lacs;->c()Lzw;

    move-result-object v0

    .line 66
    const-string v1, "video"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/video/CameraSource;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    .line 69
    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    .line 70
    iget-object v3, p0, Landroidx/media/filterpacks/video/CameraSource;->mOutputType:Labf;

    invoke-static {v3, v2}, Laap;->a(Labf;[I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->e()Laas;

    move-result-object v2

    .line 73
    invoke-virtual {v0, v2}, Lzw;->a(Laas;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    iget-boolean v0, p0, Landroidx/media/filterpacks/video/CameraSource;->mUseWallClock:Z

    if-eqz v0, :cond_0

    .line 76
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/32 v6, 0xf4240

    mul-long/2addr v4, v6

    invoke-virtual {v2, v4, v5}, Laas;->a(J)V

    .line 78
    :cond_0
    invoke-virtual {v1, v2}, Lacv;->a(Laap;)V

    .line 80
    :cond_1
    invoke-virtual {v2}, Laas;->f()Laap;

    .line 82
    :cond_2
    return-void

    .line 69
    :array_0
    .array-data 4
        0x1
        0x1
    .end array-data
.end method

.method protected j()V
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Landroidx/media/filterpacks/video/CameraSource;->d()Lacs;

    move-result-object v0

    invoke-virtual {v0}, Lacs;->c()Lzw;

    move-result-object v0

    invoke-virtual {v0, p0}, Lzw;->b(Laai;)V

    .line 87
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroidx/media/filterpacks/video/CameraSource;->mNewFrameAvailable:Z

    .line 88
    return-void
.end method
