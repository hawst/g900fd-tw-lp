.class public Landroidx/media/filterpacks/video/MediaPlayerSource;
.super Laak;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# static fields
.field private static final INPUT_ASSET_TYPE:Labf;

.field private static final INPUT_PATH_TYPE:Labf;

.field private static final OUTPUT_VIDEO_TYPE:Labf;

.field private static final TAG:Ljava/lang/String; = "MediaPlayerSource"

.field private static final TARGET_COORDS_0:[F

.field private static final TARGET_COORDS_180:[F

.field private static final TARGET_COORDS_270:[F

.field private static final TARGET_COORDS_90:[F

.field private static final mFrameShader:Ljava/lang/String; = "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n"

.field private static mSurfaceTransform:[F


# instance fields
.field private mCompleted:Z

.field private mFrameExtractor:Lacm;

.field private mFrameMutex:Ljava/lang/Object;

.field private mHeight:I

.field private final mLogVerbose:Z

.field private mLooping:Z

.field private mMediaFrame:Lada;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mNewFrameAvailable:Z

.field private mPaused:Z

.field private mRotation:I

.field private mSourceUri:Landroid/net/Uri;

.field private mSurfaceTexture:Landroid/graphics/SurfaceTexture;

.field private mVolume:F

.field private mWidth:I

.field private onCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private onMediaFrameAvailableListener:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;

.field private onPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private onVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v2, 0x10

    const/16 v1, 0x8

    .line 52
    const-class v0, Landroid/net/Uri;

    invoke-static {v0}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v0

    sput-object v0, Landroidx/media/filterpacks/video/MediaPlayerSource;->INPUT_PATH_TYPE:Labf;

    .line 53
    const-class v0, Landroid/content/res/AssetFileDescriptor;

    invoke-static {v0}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v0

    sput-object v0, Landroidx/media/filterpacks/video/MediaPlayerSource;->INPUT_ASSET_TYPE:Labf;

    .line 54
    const/16 v0, 0x12d

    invoke-static {v0, v2}, Labf;->a(II)Labf;

    move-result-object v0

    sput-object v0, Landroidx/media/filterpacks/video/MediaPlayerSource;->OUTPUT_VIDEO_TYPE:Labf;

    .line 57
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    sput-object v0, Landroidx/media/filterpacks/video/MediaPlayerSource;->TARGET_COORDS_0:[F

    .line 58
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    sput-object v0, Landroidx/media/filterpacks/video/MediaPlayerSource;->TARGET_COORDS_90:[F

    .line 59
    new-array v0, v1, [F

    fill-array-data v0, :array_2

    sput-object v0, Landroidx/media/filterpacks/video/MediaPlayerSource;->TARGET_COORDS_180:[F

    .line 60
    new-array v0, v1, [F

    fill-array-data v0, :array_3

    sput-object v0, Landroidx/media/filterpacks/video/MediaPlayerSource;->TARGET_COORDS_270:[F

    .line 103
    new-array v0, v2, [F

    sput-object v0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mSurfaceTransform:[F

    return-void

    .line 57
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 58
    :array_1
    .array-data 4
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data

    .line 59
    :array_2
    .array-data 4
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 60
    :array_3
    .array-data 4
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
    .end array-data
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 139
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 87
    iput-boolean v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mLooping:Z

    .line 92
    const/4 v0, 0x0

    iput v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mVolume:F

    .line 97
    iput v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mRotation:I

    .line 107
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mFrameMutex:Ljava/lang/Object;

    .line 348
    new-instance v0, Laej;

    invoke-direct {v0, p0}, Laej;-><init>(Landroidx/media/filterpacks/video/MediaPlayerSource;)V

    iput-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->onPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 359
    new-instance v0, Laek;

    invoke-direct {v0, p0}, Laek;-><init>(Landroidx/media/filterpacks/video/MediaPlayerSource;)V

    iput-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->onVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    .line 371
    new-instance v0, Lael;

    invoke-direct {v0, p0}, Lael;-><init>(Landroidx/media/filterpacks/video/MediaPlayerSource;)V

    iput-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->onCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 383
    new-instance v0, Laem;

    invoke-direct {v0, p0}, Laem;-><init>(Landroidx/media/filterpacks/video/MediaPlayerSource;)V

    iput-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->onMediaFrameAvailableListener:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;

    .line 140
    const-string v0, "MediaPlayerSource"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    iput-boolean v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mLogVerbose:Z

    .line 141
    return-void
.end method

.method public static synthetic a(Landroidx/media/filterpacks/video/MediaPlayerSource;I)I
    .locals 0

    .prologue
    .line 48
    iput p1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mWidth:I

    return p1
.end method

.method public static synthetic a(Landroidx/media/filterpacks/video/MediaPlayerSource;)Landroid/media/MediaPlayer;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    return-object v0
.end method

.method public static synthetic a(Landroidx/media/filterpacks/video/MediaPlayerSource;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mLogVerbose:Z

    return-void
.end method

.method private a()Z
    .locals 3

    .prologue
    .line 195
    iget-object v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mFrameMutex:Ljava/lang/Object;

    monitor-enter v1

    .line 196
    :try_start_0
    iget-boolean v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mNewFrameAvailable:Z

    .line 197
    if-eqz v0, :cond_0

    .line 198
    const/4 v2, 0x0

    iput-boolean v2, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mNewFrameAvailable:Z

    .line 202
    :goto_0
    monitor-exit v1

    return v0

    .line 200
    :cond_0
    invoke-virtual {p0}, Landroidx/media/filterpacks/video/MediaPlayerSource;->t()V

    goto :goto_0

    .line 203
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static synthetic a(Landroidx/media/filterpacks/video/MediaPlayerSource;Z)Z
    .locals 0

    .prologue
    .line 48
    iput-boolean p1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mCompleted:Z

    return p1
.end method

.method public static synthetic b(Landroidx/media/filterpacks/video/MediaPlayerSource;I)I
    .locals 0

    .prologue
    .line 48
    iput p1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mHeight:I

    return p1
.end method

.method public static synthetic b(Landroidx/media/filterpacks/video/MediaPlayerSource;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mFrameMutex:Ljava/lang/Object;

    return-object v0
.end method

.method public static synthetic b(Landroidx/media/filterpacks/video/MediaPlayerSource;Z)Z
    .locals 0

    .prologue
    .line 48
    iput-boolean p1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mNewFrameAvailable:Z

    return p1
.end method

.method public static synthetic c(Landroidx/media/filterpacks/video/MediaPlayerSource;)V
    .locals 0

    .prologue
    .line 48
    invoke-virtual {p0}, Landroidx/media/filterpacks/video/MediaPlayerSource;->u()V

    return-void
.end method

.method public static synthetic d(Landroidx/media/filterpacks/video/MediaPlayerSource;)Z
    .locals 1

    .prologue
    .line 48
    iget-boolean v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mLogVerbose:Z

    return v0
.end method

.method public static synthetic e(Landroidx/media/filterpacks/video/MediaPlayerSource;)V
    .locals 0

    .prologue
    .line 48
    invoke-virtual {p0}, Landroidx/media/filterpacks/video/MediaPlayerSource;->u()V

    return-void
.end method

.method private declared-synchronized y()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 291
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mPaused:Z

    .line 292
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mCompleted:Z

    .line 293
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mNewFrameAvailable:Z

    .line 295
    iget-boolean v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mLogVerbose:Z

    .line 297
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 299
    iget-boolean v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mLogVerbose:Z

    .line 300
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 307
    :goto_0
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_1

    .line 308
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unable to create a MediaPlayer!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 291
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 303
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mLogVerbose:Z

    .line 304
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 313
    :cond_1
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Setting MediaPlayer source to "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mSourceUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mLogVerbose:Z

    .line 314
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {p0}, Landroidx/media/filterpacks/video/MediaPlayerSource;->d()Lacs;

    move-result-object v1

    invoke-virtual {v1}, Lacs;->b()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mSourceUri:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 327
    :try_start_3
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-boolean v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mLooping:Z

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 328
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mVolume:F

    iget v2, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mVolume:F

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 331
    new-instance v0, Landroid/view/Surface;

    iget-object v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-direct {v0, v1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    .line 332
    iget-object v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, v0}, Landroid/media/MediaPlayer;->setSurface(Landroid/view/Surface;)V

    .line 333
    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 336
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->onVideoSizeChangedListener:Landroid/media/MediaPlayer$OnVideoSizeChangedListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnVideoSizeChangedListener(Landroid/media/MediaPlayer$OnVideoSizeChangedListener;)V

    .line 337
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->onPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 338
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->onCompletionListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 341
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->onMediaFrameAvailableListener:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 343
    iget-boolean v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mLogVerbose:Z

    .line 344
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 345
    monitor-exit p0

    return v3

    .line 315
    :catch_0
    move-exception v0

    .line 316
    :try_start_4
    iget-object v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 317
    const/4 v1, 0x0

    iput-object v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 318
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to set MediaPlayer to s!"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mSourceUri:Landroid/net/Uri;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 320
    :catch_1
    move-exception v0

    .line 321
    iget-object v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->release()V

    .line 322
    const/4 v1, 0x0

    iput-object v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 323
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to set MediaPlayer to URL %s!"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mSourceUri:Landroid/net/Uri;

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 162
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sourceUri"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    const-string v0, "mSourceUri"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 164
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 175
    :cond_0
    :goto_0
    return-void

    .line 165
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "loop"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 166
    const-string v0, "mLooping"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 167
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 168
    :cond_2
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "volume"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 169
    const-string v0, "mVolume"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 170
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 171
    :cond_3
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "rotation"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    const-string v0, "mRotation"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 173
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 145
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "sourceUri"

    sget-object v2, Landroidx/media/filterpacks/video/MediaPlayerSource;->INPUT_PATH_TYPE:Labf;

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "sourceAsset"

    sget-object v2, Landroidx/media/filterpacks/video/MediaPlayerSource;->INPUT_ASSET_TYPE:Labf;

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "context"

    const-class v2, Landroid/content/Context;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "loop"

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "volume"

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "rotation"

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "video"

    const/4 v2, 0x2

    sget-object v3, Landroidx/media/filterpacks/video/MediaPlayerSource;->OUTPUT_VIDEO_TYPE:Labf;

    invoke-virtual {v0, v1, v2, v3}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 179
    new-instance v0, Lacm;

    const-string v1, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mFrameExtractor:Lacm;

    .line 180
    return-void
.end method

.method public h()V
    .locals 2

    .prologue
    .line 184
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Current URL is "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mSourceUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    iget-boolean v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mLogVerbose:Z

    .line 186
    invoke-static {}, Lada;->b()Lada;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaFrame:Lada;

    .line 187
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaFrame:Lada;

    invoke-virtual {v1}, Lada;->c()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 189
    invoke-direct {p0}, Landroidx/media/filterpacks/video/MediaPlayerSource;->y()Z

    move-result v0

    if-nez v0, :cond_0

    .line 190
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Error setting up MediaPlayer!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 192
    :cond_0
    return-void
.end method

.method public i()V
    .locals 8

    .prologue
    .line 208
    iget-boolean v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mLogVerbose:Z

    .line 210
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_0

    .line 212
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Unexpected null media player!"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 215
    :cond_0
    iget-boolean v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mCompleted:Z

    if-eqz v0, :cond_2

    .line 217
    invoke-virtual {p0}, Landroidx/media/filterpacks/video/MediaPlayerSource;->s()V

    .line 253
    :cond_1
    :goto_0
    return-void

    .line 221
    :cond_2
    invoke-direct {p0}, Landroidx/media/filterpacks/video/MediaPlayerSource;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 225
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 226
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    sget-object v1, Landroidx/media/filterpacks/video/MediaPlayerSource;->mSurfaceTransform:[F

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    .line 227
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mFrameExtractor:Lacm;

    sget-object v1, Landroidx/media/filterpacks/video/MediaPlayerSource;->mSurfaceTransform:[F

    invoke-virtual {v0, v1}, Lacm;->b([F)V

    .line 228
    iget-object v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mFrameExtractor:Lacm;

    iget v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mRotation:I

    sparse-switch v0, :sswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported rotation angle."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    sget-object v0, Landroidx/media/filterpacks/video/MediaPlayerSource;->TARGET_COORDS_0:[F

    :goto_1
    invoke-virtual {v1, v0}, Lacm;->c([F)V

    .line 232
    iget-object v2, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mFrameMutex:Ljava/lang/Object;

    monitor-enter v2

    .line 233
    :try_start_0
    iget v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mWidth:I

    .line 234
    iget v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mHeight:I

    .line 235
    iget v3, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mRotation:I

    const/16 v4, 0x5a

    if-eq v3, v4, :cond_3

    iget v3, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mRotation:I

    const/16 v4, 0x10e

    if-ne v3, v4, :cond_4

    .line 236
    :cond_3
    iget v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mHeight:I

    .line 237
    iget v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mWidth:I

    .line 239
    :cond_4
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 241
    const/4 v2, 0x2

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput v1, v2, v3

    const/4 v3, 0x1

    aput v0, v2, v3

    .line 242
    const-string v3, "video"

    invoke-virtual {p0, v3}, Landroidx/media/filterpacks/video/MediaPlayerSource;->b(Ljava/lang/String;)Lacv;

    move-result-object v3

    .line 243
    invoke-virtual {v3, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->e()Laas;

    move-result-object v2

    .line 245
    iget-object v4, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mFrameExtractor:Lacm;

    iget-object v5, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaFrame:Lada;

    invoke-virtual {v2}, Laas;->m()Lacw;

    move-result-object v6

    invoke-virtual {v4, v5, v6, v1, v0}, Lacm;->a(Lada;Lacw;II)V

    .line 248
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->getTimestamp()J

    move-result-wide v0

    .line 249
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Timestamp: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/32 v6, 0xf4240

    div-long v6, v0, v6

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v4, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mLogVerbose:Z

    .line 250
    invoke-virtual {v2, v0, v1}, Laas;->a(J)V

    .line 251
    invoke-virtual {v2}, Laas;->h()V

    .line 252
    invoke-virtual {v3, v2}, Lacv;->a(Laap;)V

    goto/16 :goto_0

    .line 228
    :sswitch_1
    sget-object v0, Landroidx/media/filterpacks/video/MediaPlayerSource;->TARGET_COORDS_90:[F

    goto :goto_1

    :sswitch_2
    sget-object v0, Landroidx/media/filterpacks/video/MediaPlayerSource;->TARGET_COORDS_180:[F

    goto :goto_1

    :sswitch_3
    sget-object v0, Landroidx/media/filterpacks/video/MediaPlayerSource;->TARGET_COORDS_270:[F

    goto :goto_1

    .line 239
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 228
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_1
        0xb4 -> :sswitch_2
        0x10e -> :sswitch_3
    .end sparse-switch
.end method

.method public j()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 257
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 260
    :cond_0
    iput-boolean v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mPaused:Z

    .line 261
    iput-boolean v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mCompleted:Z

    .line 262
    iput-boolean v1, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mNewFrameAvailable:Z

    .line 264
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 265
    iput-object v2, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 266
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 267
    iput-object v2, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mSurfaceTexture:Landroid/graphics/SurfaceTexture;

    .line 268
    iget-boolean v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mLogVerbose:Z

    .line 269
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaFrame:Lada;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p0, Landroidx/media/filterpacks/video/MediaPlayerSource;->mMediaFrame:Lada;

    invoke-virtual {v0}, Lada;->g()V

    .line 276
    :cond_0
    return-void
.end method
