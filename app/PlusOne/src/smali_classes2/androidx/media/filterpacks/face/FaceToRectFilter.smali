.class public final Landroidx/media/filterpacks/face/FaceToRectFilter;
.super Laak;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field private mScale:F


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 42
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroidx/media/filterpacks/face/FaceToRectFilter;->mScale:F

    .line 46
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 2

    .prologue
    .line 62
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "scale"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    const-string v0, "mScale"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 64
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->a(Z)V

    .line 66
    :cond_0
    return-void
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 50
    const-class v0, Landroid/hardware/Camera$Face;

    invoke-static {v0}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v0

    .line 51
    const-class v1, Ladp;

    invoke-static {v1}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v1

    .line 52
    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    .line 53
    new-instance v3, Lacx;

    invoke-direct {v3}, Lacx;-><init>()V

    const-string v4, "faces"

    invoke-virtual {v3, v4, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v3, "scale"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "quads"

    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/high16 v7, 0x44fa0000    # 2000.0f

    const/high16 v6, 0x3f000000    # 0.5f

    .line 71
    const-string v0, "faces"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/face/FaceToRectFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->b()Labh;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Labh;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/hardware/Camera$Face;

    .line 74
    if-nez v0, :cond_0

    .line 75
    new-array v0, v2, [Landroid/hardware/Camera$Face;

    .line 79
    :cond_0
    array-length v1, v0

    new-array v3, v1, [Ladp;

    move v1, v2

    .line 80
    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_1

    .line 81
    aget-object v4, v0, v1

    new-instance v5, Landroid/graphics/RectF;

    iget-object v4, v4, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    invoke-direct {v5, v4}, Landroid/graphics/RectF;-><init>(Landroid/graphics/Rect;)V

    iget v4, v5, Landroid/graphics/RectF;->left:F

    div-float/2addr v4, v7

    add-float/2addr v4, v6

    iput v4, v5, Landroid/graphics/RectF;->left:F

    iget v4, v5, Landroid/graphics/RectF;->right:F

    div-float/2addr v4, v7

    add-float/2addr v4, v6

    iput v4, v5, Landroid/graphics/RectF;->right:F

    iget v4, v5, Landroid/graphics/RectF;->top:F

    div-float/2addr v4, v7

    add-float/2addr v4, v6

    iput v4, v5, Landroid/graphics/RectF;->top:F

    iget v4, v5, Landroid/graphics/RectF;->bottom:F

    div-float/2addr v4, v7

    add-float/2addr v4, v6

    iput v4, v5, Landroid/graphics/RectF;->bottom:F

    invoke-static {v5}, Ladp;->a(Landroid/graphics/RectF;)Ladp;

    move-result-object v4

    iget v5, p0, Landroidx/media/filterpacks/face/FaceToRectFilter;->mScale:F

    invoke-virtual {v4, v5}, Ladp;->b(F)Ladp;

    move-result-object v4

    aput-object v4, v3, v1

    .line 80
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 85
    :cond_1
    const-string v1, "quads"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/face/FaceToRectFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    .line 86
    const/4 v4, 0x1

    new-array v4, v4, [I

    array-length v0, v0

    aput v0, v4, v2

    .line 87
    invoke-virtual {v1, v4}, Lacv;->a([I)Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->b()Labh;

    move-result-object v0

    .line 88
    invoke-virtual {v0, v3}, Labh;->b(Ljava/lang/Object;)V

    .line 89
    invoke-virtual {v1, v0}, Lacv;->a(Laap;)V

    .line 90
    return-void
.end method
