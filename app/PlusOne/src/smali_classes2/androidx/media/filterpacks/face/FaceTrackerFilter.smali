.class public final Landroidx/media/filterpacks/face/FaceTrackerFilter;
.super Laak;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# static fields
.field private static final FACE_TRACKER_EFFECT:Ljava/lang/String; = "com.google.android.media.effect.effects.FaceTrackingEffect"


# instance fields
.field private mEffectContext:Landroid/media/effect/EffectContext;

.field private mFaceListener:Landroid/media/effect/EffectUpdateListener;

.field private mFaceTracker:Landroid/media/effect/Effect;

.field private mFaces:[Landroid/hardware/Camera$Face;

.field private mIgnoreTex:Lada;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 41
    iput-object v0, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mEffectContext:Landroid/media/effect/EffectContext;

    .line 42
    iput-object v0, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mFaceTracker:Landroid/media/effect/Effect;

    .line 43
    iput-object v0, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mFaces:[Landroid/hardware/Camera$Face;

    .line 44
    iput-object v0, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mIgnoreTex:Lada;

    .line 111
    new-instance v0, Ladw;

    invoke-direct {v0, p0}, Ladw;-><init>(Landroidx/media/filterpacks/face/FaceTrackerFilter;)V

    iput-object v0, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mFaceListener:Landroid/media/effect/EffectUpdateListener;

    .line 48
    return-void
.end method

.method public static synthetic a(Landroidx/media/filterpacks/face/FaceTrackerFilter;[Landroid/hardware/Camera$Face;)[Landroid/hardware/Camera$Face;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mFaces:[Landroid/hardware/Camera$Face;

    return-object p1
.end method


# virtual methods
.method public c()Lacx;
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 56
    const/16 v0, 0x12d

    invoke-static {v0, v4}, Labf;->a(II)Labf;

    move-result-object v0

    .line 57
    const-class v1, Landroid/hardware/Camera$Face;

    invoke-static {v1}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v1

    .line 58
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "faces"

    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 3

    .prologue
    .line 66
    invoke-static {}, Landroid/media/effect/EffectContext;->createWithCurrentGlContext()Landroid/media/effect/EffectContext;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mEffectContext:Landroid/media/effect/EffectContext;

    .line 67
    iget-object v0, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mEffectContext:Landroid/media/effect/EffectContext;

    invoke-virtual {v0}, Landroid/media/effect/EffectContext;->getFactory()Landroid/media/effect/EffectFactory;

    move-result-object v0

    .line 68
    const-string v1, "com.google.android.media.effect.effects.FaceTrackingEffect"

    invoke-static {v1}, Landroid/media/effect/EffectFactory;->isEffectSupported(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    const-string v1, "com.google.android.media.effect.effects.FaceTrackingEffect"

    invoke-virtual {v0, v1}, Landroid/media/effect/EffectFactory;->createEffect(Ljava/lang/String;)Landroid/media/effect/Effect;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mFaceTracker:Landroid/media/effect/Effect;

    .line 70
    iget-object v0, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mFaceTracker:Landroid/media/effect/Effect;

    const-string v1, "ignoreOutput"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/media/effect/Effect;->setParameter(Ljava/lang/String;Ljava/lang/Object;)V

    .line 71
    iget-object v0, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mFaceTracker:Landroid/media/effect/Effect;

    iget-object v1, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mFaceListener:Landroid/media/effect/EffectUpdateListener;

    invoke-virtual {v0, v1}, Landroid/media/effect/Effect;->setUpdateListener(Landroid/media/effect/EffectUpdateListener;)V

    .line 75
    invoke-static {}, Lada;->a()Lada;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mIgnoreTex:Lada;

    .line 76
    iget-object v0, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mIgnoreTex:Lada;

    const/16 v1, 0x280

    const/16 v2, 0x1e0

    invoke-virtual {v0, v1, v2}, Lada;->a(II)V

    .line 77
    return-void

    .line 73
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Cannot find a face-tracker engine for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected i()V
    .locals 7

    .prologue
    .line 81
    const-string v0, "faces"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/face/FaceTrackerFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 82
    const-string v1, "image"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/face/FaceTrackerFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v1

    .line 84
    invoke-virtual {v1}, Laas;->l()Lada;

    move-result-object v2

    invoke-virtual {v2}, Lada;->c()I

    move-result v2

    .line 85
    iget-object v3, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mIgnoreTex:Lada;

    invoke-virtual {v3}, Lada;->c()I

    move-result v3

    .line 87
    iget-object v4, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mFaceTracker:Landroid/media/effect/Effect;

    invoke-virtual {v1}, Laas;->j()I

    move-result v5

    invoke-virtual {v1}, Laas;->k()I

    move-result v6

    invoke-virtual {v4, v2, v5, v6, v3}, Landroid/media/effect/Effect;->apply(IIII)V

    .line 88
    invoke-virtual {v1}, Laas;->h()V

    .line 90
    monitor-enter p0

    .line 91
    :try_start_0
    iget-object v1, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mFaces:[Landroid/hardware/Camera$Face;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    new-array v1, v1, [Landroid/hardware/Camera$Face;

    iput-object v1, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mFaces:[Landroid/hardware/Camera$Face;

    .line 92
    :cond_0
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    iget-object v3, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mFaces:[Landroid/hardware/Camera$Face;

    array-length v3, v3

    aput v3, v1, v2

    .line 93
    invoke-virtual {v0, v1}, Lacv;->a([I)Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->b()Labh;

    move-result-object v1

    .line 94
    iget-object v2, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mFaces:[Landroid/hardware/Camera$Face;

    invoke-virtual {v1, v2}, Labh;->b(Ljava/lang/Object;)V

    .line 95
    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    .line 96
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected k()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 101
    iget-object v0, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mEffectContext:Landroid/media/effect/EffectContext;

    if-eqz v0, :cond_0

    .line 102
    iget-object v0, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mEffectContext:Landroid/media/effect/EffectContext;

    invoke-virtual {v0}, Landroid/media/effect/EffectContext;->release()V

    .line 103
    iput-object v1, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mEffectContext:Landroid/media/effect/EffectContext;

    .line 105
    :cond_0
    iget-object v0, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mIgnoreTex:Lada;

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mIgnoreTex:Lada;

    invoke-virtual {v0}, Lada;->g()V

    .line 107
    iput-object v1, p0, Landroidx/media/filterpacks/face/FaceTrackerFilter;->mIgnoreTex:Lada;

    .line 109
    :cond_1
    return-void
.end method
