.class public final Landroidx/media/filterpacks/face/FaceToQuadFilter;
.super Laak;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field private mScale:F


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 43
    const/high16 v0, 0x40000000    # 2.0f

    iput v0, p0, Landroidx/media/filterpacks/face/FaceToQuadFilter;->mScale:F

    .line 47
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 2

    .prologue
    .line 63
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "scale"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "mScale"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 65
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->a(Z)V

    .line 67
    :cond_0
    return-void
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 51
    const-class v0, Landroid/hardware/Camera$Face;

    invoke-static {v0}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v0

    .line 52
    const-class v1, Ladp;

    invoke-static {v1}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v1

    .line 53
    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    .line 54
    new-instance v3, Lacx;

    invoke-direct {v3}, Lacx;-><init>()V

    const-string v4, "faces"

    invoke-virtual {v3, v4, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v3, "scale"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "quads"

    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 13

    .prologue
    const/4 v2, 0x0

    const/high16 v12, 0x44fa0000    # 2000.0f

    const/high16 v11, 0x3f000000    # 0.5f

    .line 72
    const-string v0, "faces"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/face/FaceToQuadFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->b()Labh;

    move-result-object v0

    .line 73
    invoke-virtual {v0}, Labh;->k()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/hardware/Camera$Face;

    .line 76
    array-length v1, v0

    new-array v3, v1, [Ladp;

    move v1, v2

    .line 77
    :goto_0
    array-length v4, v3

    if-ge v1, v4, :cond_0

    .line 78
    aget-object v4, v0, v1

    new-instance v5, Landroid/graphics/PointF;

    iget-object v6, v4, Landroid/hardware/Camera$Face;->leftEye:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    int-to-float v6, v6

    div-float/2addr v6, v12

    add-float/2addr v6, v11

    iget-object v7, v4, Landroid/hardware/Camera$Face;->leftEye:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->y:I

    int-to-float v7, v7

    div-float/2addr v7, v12

    add-float/2addr v7, v11

    invoke-direct {v5, v6, v7}, Landroid/graphics/PointF;-><init>(FF)V

    new-instance v6, Landroid/graphics/PointF;

    iget-object v7, v4, Landroid/hardware/Camera$Face;->rightEye:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->x:I

    int-to-float v7, v7

    div-float/2addr v7, v12

    add-float/2addr v7, v11

    iget-object v8, v4, Landroid/hardware/Camera$Face;->rightEye:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->y:I

    int-to-float v8, v8

    div-float/2addr v8, v12

    add-float/2addr v8, v11

    invoke-direct {v6, v7, v8}, Landroid/graphics/PointF;-><init>(FF)V

    new-instance v7, Landroid/graphics/PointF;

    iget-object v8, v4, Landroid/hardware/Camera$Face;->mouth:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->x:I

    int-to-float v8, v8

    div-float/2addr v8, v12

    add-float/2addr v8, v11

    iget-object v4, v4, Landroid/hardware/Camera$Face;->mouth:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    div-float/2addr v4, v12

    add-float/2addr v4, v11

    invoke-direct {v7, v8, v4}, Landroid/graphics/PointF;-><init>(FF)V

    iget v4, v6, Landroid/graphics/PointF;->x:F

    iget v8, v5, Landroid/graphics/PointF;->x:F

    sub-float/2addr v4, v8

    iget v8, v5, Landroid/graphics/PointF;->y:F

    iget v9, v7, Landroid/graphics/PointF;->y:F

    sub-float/2addr v8, v9

    mul-float/2addr v4, v8

    iget v8, v5, Landroid/graphics/PointF;->x:F

    iget v7, v7, Landroid/graphics/PointF;->x:F

    sub-float v7, v8, v7

    iget v8, v6, Landroid/graphics/PointF;->y:F

    iget v9, v5, Landroid/graphics/PointF;->y:F

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    sub-float/2addr v4, v7

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    iget v7, v6, Landroid/graphics/PointF;->x:F

    iget v8, v5, Landroid/graphics/PointF;->x:F

    sub-float/2addr v7, v8

    iget v8, v6, Landroid/graphics/PointF;->x:F

    iget v9, v5, Landroid/graphics/PointF;->x:F

    sub-float/2addr v8, v9

    mul-float/2addr v7, v8

    iget v8, v6, Landroid/graphics/PointF;->y:F

    iget v9, v5, Landroid/graphics/PointF;->y:F

    sub-float/2addr v8, v9

    iget v9, v6, Landroid/graphics/PointF;->y:F

    iget v10, v5, Landroid/graphics/PointF;->y:F

    sub-float/2addr v9, v10

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    float-to-double v8, v7

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-float v7, v8

    div-float/2addr v4, v7

    invoke-static {v5, v6, v4}, Ladp;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;F)Ladp;

    move-result-object v4

    iget v5, p0, Landroidx/media/filterpacks/face/FaceToQuadFilter;->mScale:F

    invoke-virtual {v4, v5}, Ladp;->b(F)Ladp;

    move-result-object v4

    aput-object v4, v3, v1

    .line 77
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 82
    :cond_0
    const-string v1, "quads"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/face/FaceToQuadFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    .line 83
    const/4 v4, 0x1

    new-array v4, v4, [I

    array-length v0, v0

    aput v0, v4, v2

    .line 84
    invoke-virtual {v1, v4}, Lacv;->a([I)Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->b()Labh;

    move-result-object v0

    .line 85
    invoke-virtual {v0, v3}, Labh;->b(Ljava/lang/Object;)V

    .line 86
    invoke-virtual {v1, v0}, Lacv;->a(Laap;)V

    .line 87
    return-void
.end method
