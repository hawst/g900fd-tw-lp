.class public Landroidx/media/filterpacks/numeric/StatsFilter;
.super Laak;
.source "PG"


# static fields
.field private static final MEAN_INDEX:I = 0x0

.field private static final STDEV_INDEX:I = 0x1

.field private static final TAG:Ljava/lang/String; = "StatsFilter"

.field private static mLogVerbose:Z


# instance fields
.field private mCropRect:Ladp;

.field private final mStats:[F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 52
    const-string v0, "StatsFilter"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Landroidx/media/filterpacks/numeric/StatsFilter;->mLogVerbose:Z

    .line 143
    const-string v0, "filterframework_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 144
    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 59
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 48
    const/4 v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Landroidx/media/filterpacks/numeric/StatsFilter;->mStats:[F

    .line 50
    invoke-static {v1, v1, v2, v2}, Ladp;->a(FFFF)Ladp;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/numeric/StatsFilter;->mCropRect:Ladp;

    .line 60
    return-void
.end method

.method private static a(III)I
    .locals 0

    .prologue
    .line 100
    if-ge p0, p1, :cond_0

    :goto_0
    return p1

    :cond_0
    if-le p0, p2, :cond_1

    move p1, p2

    goto :goto_0

    :cond_1
    move p1, p0

    goto :goto_0
.end method

.method private native regionscore(Ljava/nio/ByteBuffer;IIIII[F)V
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 2

    .prologue
    .line 76
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "cropRect"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    const-string v0, "mCropRect"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 78
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->a(Z)V

    .line 80
    :cond_0
    return-void
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/4 v5, 0x2

    .line 64
    const/16 v0, 0x64

    invoke-static {v0}, Labf;->b(I)Labf;

    move-result-object v0

    .line 65
    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v1}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v1

    .line 66
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "buffer"

    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "cropRect"

    const/4 v3, 0x1

    const-class v4, Ladp;

    invoke-static {v4}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "mean"

    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "stdev"

    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 108
    const-string v0, "buffer"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/numeric/StatsFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v8

    .line 109
    invoke-virtual {v8, v10}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 111
    invoke-virtual {v8}, Laar;->j()I

    move-result v2

    invoke-virtual {v8}, Laar;->k()I

    move-result v0

    iget-object v3, p0, Landroidx/media/filterpacks/numeric/StatsFilter;->mCropRect:Ladp;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    invoke-virtual {v3}, Ladp;->i()Landroid/graphics/RectF;

    move-result-object v6

    iget v3, v6, Landroid/graphics/RectF;->left:F

    int-to-float v4, v2

    mul-float/2addr v3, v4

    float-to-int v3, v3

    add-int/lit8 v4, v2, -0x1

    invoke-static {v3, v9, v4}, Landroidx/media/filterpacks/numeric/StatsFilter;->a(III)I

    move-result v3

    iget v4, v6, Landroid/graphics/RectF;->top:F

    int-to-float v5, v0

    mul-float/2addr v4, v5

    float-to-int v4, v4

    add-int/lit8 v5, v0, -0x1

    invoke-static {v4, v9, v5}, Landroidx/media/filterpacks/numeric/StatsFilter;->a(III)I

    move-result v4

    iget v5, v6, Landroid/graphics/RectF;->right:F

    int-to-float v7, v2

    mul-float/2addr v5, v7

    float-to-int v5, v5

    add-int/lit8 v7, v2, -0x1

    invoke-static {v5, v9, v7}, Landroidx/media/filterpacks/numeric/StatsFilter;->a(III)I

    move-result v5

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    int-to-float v7, v0

    mul-float/2addr v6, v7

    float-to-int v6, v6

    add-int/lit8 v0, v0, -0x1

    invoke-static {v6, v9, v0}, Landroidx/media/filterpacks/numeric/StatsFilter;->a(III)I

    move-result v6

    iget-object v7, p0, Landroidx/media/filterpacks/numeric/StatsFilter;->mStats:[F

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Landroidx/media/filterpacks/numeric/StatsFilter;->regionscore(Ljava/nio/ByteBuffer;IIIII[F)V

    sget-boolean v0, Landroidx/media/filterpacks/numeric/StatsFilter;->mLogVerbose:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Native calc stats: Mean = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Landroidx/media/filterpacks/numeric/StatsFilter;->mStats:[F

    aget v1, v1, v9

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Stdev = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroidx/media/filterpacks/numeric/StatsFilter;->mStats:[F

    aget v1, v1, v10

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    .line 112
    :cond_0
    invoke-virtual {v8}, Laar;->h()V

    .line 114
    const-string v0, "mean"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/numeric/StatsFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 115
    invoke-virtual {v0, v11}, Lacv;->a([I)Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->a()Labg;

    move-result-object v1

    .line 116
    iget-object v2, p0, Landroidx/media/filterpacks/numeric/StatsFilter;->mStats:[F

    aget v2, v2, v9

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Labg;->a(Ljava/lang/Object;)V

    .line 117
    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    .line 119
    const-string v0, "stdev"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/numeric/StatsFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 120
    invoke-virtual {v0, v11}, Lacv;->a([I)Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->a()Labg;

    move-result-object v1

    .line 121
    iget-object v2, p0, Landroidx/media/filterpacks/numeric/StatsFilter;->mStats:[F

    aget v2, v2, v10

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v1, v2}, Labg;->a(Ljava/lang/Object;)V

    .line 122
    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    .line 123
    return-void
.end method
