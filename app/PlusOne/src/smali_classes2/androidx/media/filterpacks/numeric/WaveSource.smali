.class public final Landroidx/media/filterpacks/numeric/WaveSource;
.super Laak;
.source "PG"


# static fields
.field public static final WAVESOURCE_CONST:I = 0x0

.field public static final WAVESOURCE_COS:I = 0x2

.field public static final WAVESOURCE_SAWTOOTH:I = 0x3

.field public static final WAVESOURCE_SIN:I = 0x1


# instance fields
.field private mAmplitude:F

.field private mMode:I

.field private mSpeed:F

.field private mXOffset:F

.field private mYOffset:F


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 37
    const v0, 0x3c23d70a    # 0.01f

    iput v0, p0, Landroidx/media/filterpacks/numeric/WaveSource;->mSpeed:F

    .line 38
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroidx/media/filterpacks/numeric/WaveSource;->mAmplitude:F

    .line 39
    iput v1, p0, Landroidx/media/filterpacks/numeric/WaveSource;->mXOffset:F

    .line 40
    iput v1, p0, Landroidx/media/filterpacks/numeric/WaveSource;->mYOffset:F

    .line 42
    const/4 v0, 0x1

    iput v0, p0, Landroidx/media/filterpacks/numeric/WaveSource;->mMode:I

    .line 46
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 62
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "speed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    const-string v0, "mSpeed"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 64
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "amplitude"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 66
    const-string v0, "mAmplitude"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 68
    :cond_2
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "xOffset"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 69
    const-string v0, "mXOffset"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 70
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 71
    :cond_3
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "yOffset"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 72
    const-string v0, "mYOffset"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 73
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 74
    :cond_4
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    const-string v0, "mMode"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 50
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "speed"

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "amplitude"

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "xOffset"

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "yOffset"

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "mode"

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "value"

    const/4 v2, 0x2

    invoke-static {}, Labf;->b()Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected declared-synchronized i()V
    .locals 6

    .prologue
    .line 82
    monitor-enter p0

    :try_start_0
    const-string v0, "value"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/numeric/WaveSource;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    .line 83
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lacv;->a([I)Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->a()Labg;

    move-result-object v2

    .line 85
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 87
    iget v0, p0, Landroidx/media/filterpacks/numeric/WaveSource;->mMode:I

    packed-switch v0, :pswitch_data_0

    .line 101
    iget v0, p0, Landroidx/media/filterpacks/numeric/WaveSource;->mYOffset:F

    .line 105
    :goto_0
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v2, v0}, Labg;->a(Ljava/lang/Object;)V

    .line 106
    invoke-virtual {v1, v2}, Lacv;->a(Laap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    monitor-exit p0

    return-void

    .line 89
    :pswitch_0
    :try_start_1
    iget v0, p0, Landroidx/media/filterpacks/numeric/WaveSource;->mYOffset:F

    goto :goto_0

    .line 92
    :pswitch_1
    iget v0, p0, Landroidx/media/filterpacks/numeric/WaveSource;->mXOffset:F

    long-to-float v3, v4

    iget v4, p0, Landroidx/media/filterpacks/numeric/WaveSource;->mSpeed:F

    mul-float/2addr v3, v4

    add-float/2addr v0, v3

    invoke-static {v0}, Landroid/util/FloatMath;->sin(F)F

    move-result v0

    iget v3, p0, Landroidx/media/filterpacks/numeric/WaveSource;->mAmplitude:F

    mul-float/2addr v0, v3

    iget v3, p0, Landroidx/media/filterpacks/numeric/WaveSource;->mYOffset:F

    add-float/2addr v0, v3

    .line 93
    goto :goto_0

    .line 95
    :pswitch_2
    iget v0, p0, Landroidx/media/filterpacks/numeric/WaveSource;->mXOffset:F

    long-to-float v3, v4

    iget v4, p0, Landroidx/media/filterpacks/numeric/WaveSource;->mSpeed:F

    mul-float/2addr v3, v4

    add-float/2addr v0, v3

    invoke-static {v0}, Landroid/util/FloatMath;->cos(F)F

    move-result v0

    iget v3, p0, Landroidx/media/filterpacks/numeric/WaveSource;->mAmplitude:F

    mul-float/2addr v0, v3

    iget v3, p0, Landroidx/media/filterpacks/numeric/WaveSource;->mYOffset:F

    add-float/2addr v0, v3

    .line 96
    goto :goto_0

    .line 98
    :pswitch_3
    iget v0, p0, Landroidx/media/filterpacks/numeric/WaveSource;->mXOffset:F

    long-to-float v3, v4

    iget v4, p0, Landroidx/media/filterpacks/numeric/WaveSource;->mSpeed:F

    mul-float/2addr v3, v4

    add-float/2addr v0, v3

    const/high16 v3, 0x3f800000    # 1.0f

    rem-float/2addr v0, v3

    iget v3, p0, Landroidx/media/filterpacks/numeric/WaveSource;->mAmplitude:F

    mul-float/2addr v0, v3

    iget v3, p0, Landroidx/media/filterpacks/numeric/WaveSource;->mYOffset:F
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    add-float/2addr v0, v3

    .line 99
    goto :goto_0

    .line 82
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 87
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
