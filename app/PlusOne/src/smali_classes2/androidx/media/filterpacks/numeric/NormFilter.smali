.class public final Landroidx/media/filterpacks/numeric/NormFilter;
.super Laak;
.source "PG"


# static fields
.field private static final TAG:Ljava/lang/String; = "NormFilter"

.field private static mLogVerbose:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 33
    const-string v0, "NormFilter"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Landroidx/media/filterpacks/numeric/NormFilter;->mLogVerbose:Z

    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 37
    return-void
.end method


# virtual methods
.method public c()Lacx;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 41
    sget-object v0, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v0}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v0

    .line 42
    new-instance v1, Lacx;

    invoke-direct {v1}, Lacx;-><init>()V

    const-string v2, "x"

    invoke-virtual {v1, v2, v3, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v1

    const-string v2, "y"

    invoke-virtual {v1, v2, v3, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v1

    const-string v2, "norm"

    invoke-virtual {v1, v2, v3, v0}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 4

    .prologue
    .line 51
    const-string v0, "x"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/numeric/NormFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->a()Labg;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Labg;->j()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 53
    const-string v0, "y"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/numeric/NormFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->a()Labg;

    move-result-object v0

    .line 54
    invoke-virtual {v0}, Labg;->j()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 56
    float-to-double v2, v1

    float-to-double v0, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v0

    double-to-float v0, v0

    .line 57
    sget-boolean v1, Landroidx/media/filterpacks/numeric/NormFilter;->mLogVerbose:Z

    if-eqz v1, :cond_0

    .line 58
    :cond_0
    const-string v1, "norm"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/numeric/NormFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    .line 59
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->a()Labg;

    move-result-object v2

    .line 60
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v2, v0}, Labg;->a(Ljava/lang/Object;)V

    .line 61
    invoke-virtual {v1, v2}, Lacv;->a(Laap;)V

    .line 62
    return-void
.end method
