.class public Landroidx/media/filterpacks/base/DispatchFilter;
.super Landroidx/media/filterpacks/base/MetaFilter;
.source "PG"


# instance fields
.field private mOutputFrames:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Laap;",
            ">;"
        }
    .end annotation
.end field

.field private mRunListener:Lacg;

.field private mRunner:Labx;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Landroidx/media/filterpacks/base/MetaFilter;-><init>(Lacs;Ljava/lang/String;)V

    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mOutputFrames:Ljava/util/HashMap;

    .line 22
    new-instance v0, Ladq;

    invoke-direct {v0, p0}, Ladq;-><init>(Landroidx/media/filterpacks/base/DispatchFilter;)V

    iput-object v0, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mRunListener:Lacg;

    .line 43
    return-void
.end method


# virtual methods
.method protected A()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 107
    iget-object v1, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mState:Ladt;

    iput v0, v1, Ladt;->a:I

    .line 108
    iget-object v1, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mOutputFrames:Ljava/util/HashMap;

    if-eqz v1, :cond_1

    .line 109
    :goto_0
    iget-object v1, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mCurrentGraph:Laan;

    iget-object v2, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mRunner:Labx;

    invoke-virtual {v1, v2}, Laan;->a(Labx;)V

    .line 110
    iget-object v1, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mRunner:Labx;

    iget-object v2, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mCurrentGraph:Laan;

    invoke-virtual {v1, v2}, Labx;->a(Laan;)V

    .line 111
    if-nez v0, :cond_0

    .line 112
    iget-object v0, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mRunner:Labx;

    invoke-virtual {v0}, Labx;->c()V

    .line 113
    iget-object v0, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mState:Ladt;

    const/4 v1, 0x2

    iput v1, v0, Ladt;->a:I

    .line 115
    :cond_0
    return-void

    .line 108
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Laap;Lacv;)V
    .locals 3

    .prologue
    .line 100
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/DispatchFilter;->q()Laat;

    move-result-object v0

    invoke-virtual {v0, p1}, Laat;->a(Laap;)Laap;

    move-result-object v1

    .line 101
    invoke-virtual {p2, v1}, Lacv;->a(Laap;)V

    .line 102
    invoke-virtual {p2}, Lacv;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mOutputFrames:Ljava/util/HashMap;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mOutputFrames:Ljava/util/HashMap;

    :cond_0
    iget-object v0, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mOutputFrames:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laap;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Laap;->f()Laap;

    :cond_1
    iget-object v0, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mOutputFrames:Ljava/util/HashMap;

    invoke-virtual {v1}, Laap;->g()Laap;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 103
    return-void
.end method

.method protected a(Landroidx/media/filterpacks/base/GraphInputSource;Laap;)V
    .locals 0

    .prologue
    .line 94
    invoke-virtual {p1, p2}, Landroidx/media/filterpacks/base/GraphInputSource;->b(Laap;)V

    .line 95
    invoke-virtual {p2}, Laap;->f()Laap;

    .line 96
    return-void
.end method

.method protected g()V
    .locals 2

    .prologue
    .line 47
    new-instance v0, Labx;

    invoke-virtual {p0}, Landroidx/media/filterpacks/base/DispatchFilter;->d()Lacs;

    move-result-object v1

    invoke-direct {v0, v1}, Labx;-><init>(Lacs;)V

    iput-object v0, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mRunner:Labx;

    .line 48
    iget-object v0, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mRunner:Labx;

    iget-object v1, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mRunListener:Lacg;

    invoke-virtual {v0, v1}, Labx;->a(Lacg;)V

    .line 49
    return-void
.end method

.method protected i()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 53
    iget-object v2, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mState:Ladt;

    monitor-enter v2

    .line 54
    :try_start_0
    iget-object v1, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mState:Ladt;

    iget v1, v1, Ladt;->a:I

    if-nez v1, :cond_2

    .line 55
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/DispatchFilter;->z()V

    .line 56
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/DispatchFilter;->A()V

    .line 61
    :cond_0
    iget-object v1, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mState:Ladt;

    iget v1, v1, Ladt;->a:I

    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 62
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/DispatchFilter;->E()V

    .line 64
    :cond_1
    iget-object v1, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mState:Ladt;

    iget v1, v1, Ladt;->a:I

    const/4 v3, 0x1

    if-ne v1, v3, :cond_4

    .line 65
    iget-object v3, p0, Laak;->mConnectedOutputPortArray:[Lacv;

    array-length v4, v3

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    iget-object v0, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mOutputFrames:Ljava/util/HashMap;

    invoke-virtual {v5}, Lacv;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laap;

    if-eqz v0, :cond_3

    invoke-virtual {v5, v0}, Lacv;->a(Laap;)V

    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 58
    :cond_2
    iget-object v3, p0, Laak;->mConnectedInputPortArray:[Lacp;

    array-length v4, v3

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_0

    aget-object v5, v3, v1

    invoke-virtual {v5}, Lacp;->c()Laap;

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 65
    :cond_3
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v6, "No output frame produced for "

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "!"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_4
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method protected j()V
    .locals 1

    .prologue
    .line 72
    invoke-super {p0}, Landroidx/media/filterpacks/base/MetaFilter;->j()V

    .line 73
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mOutputFrames:Ljava/util/HashMap;

    .line 74
    return-void
.end method

.method protected y()Z
    .locals 1

    .prologue
    .line 78
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/DispatchFilter;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/media/filterpacks/base/DispatchFilter;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/media/filterpacks/base/DispatchFilter;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected z()V
    .locals 7

    .prologue
    .line 83
    iget-object v0, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mInputFrames:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 84
    iget-object v0, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mRunner:Labx;

    invoke-virtual {v0}, Labx;->k()Laat;

    move-result-object v1

    .line 85
    iget-object v2, p0, Laak;->mConnectedInputPortArray:[Lacp;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 86
    invoke-virtual {v4}, Lacp;->c()Laap;

    move-result-object v5

    invoke-virtual {v1, v5}, Laat;->a(Laap;)Laap;

    move-result-object v5

    .line 87
    iget-object v6, p0, Landroidx/media/filterpacks/base/DispatchFilter;->mInputFrames:Ljava/util/HashMap;

    invoke-virtual {v4}, Lacp;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 89
    :cond_0
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/DispatchFilter;->C()V

    .line 90
    return-void
.end method
