.class public final Landroidx/media/filterpacks/base/CountLimitFilter;
.super Laak;
.source "PG"


# instance fields
.field private mCount:I

.field private mMaxCount:I


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 28
    const/4 v0, 0x0

    iput v0, p0, Landroidx/media/filterpacks/base/CountLimitFilter;->mCount:I

    .line 29
    const/4 v0, 0x1

    iput v0, p0, Landroidx/media/filterpacks/base/CountLimitFilter;->mMaxCount:I

    .line 33
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 2

    .prologue
    .line 50
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "maxCount"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    const-string v0, "mMaxCount"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 52
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->a(Z)V

    .line 56
    :goto_0
    return-void

    .line 54
    :cond_0
    const-string v0, "frame"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/CountLimitFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    invoke-virtual {p1, v0}, Lacp;->a(Lacv;)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 41
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "frame"

    invoke-static {}, Labf;->a()Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v4, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "maxCount"

    const/4 v2, 0x1

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "frame"

    invoke-static {}, Labf;->a()Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v4, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected h()V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    iput v0, p0, Landroidx/media/filterpacks/base/CountLimitFilter;->mCount:I

    .line 61
    return-void
.end method

.method protected declared-synchronized i()V
    .locals 2

    .prologue
    .line 70
    monitor-enter p0

    :try_start_0
    iget v0, p0, Landroidx/media/filterpacks/base/CountLimitFilter;->mCount:I

    iget v1, p0, Landroidx/media/filterpacks/base/CountLimitFilter;->mMaxCount:I

    if-ge v0, v1, :cond_0

    .line 71
    const-string v0, "frame"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/CountLimitFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    .line 72
    const-string v1, "frame"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/base/CountLimitFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    invoke-virtual {v1, v0}, Lacv;->a(Laap;)V

    .line 74
    :cond_0
    iget v0, p0, Landroidx/media/filterpacks/base/CountLimitFilter;->mCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroidx/media/filterpacks/base/CountLimitFilter;->mCount:I

    .line 75
    iget v0, p0, Landroidx/media/filterpacks/base/CountLimitFilter;->mCount:I

    iget v1, p0, Landroidx/media/filterpacks/base/CountLimitFilter;->mMaxCount:I

    if-ne v0, v1, :cond_1

    .line 76
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/CountLimitFilter;->s()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 78
    :cond_1
    monitor-exit p0

    return-void

    .line 70
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected j()V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    iput v0, p0, Landroidx/media/filterpacks/base/CountLimitFilter;->mCount:I

    .line 66
    return-void
.end method
