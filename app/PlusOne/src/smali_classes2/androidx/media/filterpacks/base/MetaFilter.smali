.class public Landroidx/media/filterpacks/base/MetaFilter;
.super Laak;
.source "PG"

# interfaces
.implements Lacl;


# instance fields
.field public mCurrentGraph:Laan;

.field public mGraphProvider:Lads;

.field public mInputFrames:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Laap;",
            ">;"
        }
    .end annotation
.end field

.field public mState:Ladt;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 33
    new-instance v0, Ladt;

    invoke-direct {v0}, Ladt;-><init>()V

    iput-object v0, p0, Landroidx/media/filterpacks/base/MetaFilter;->mState:Ladt;

    .line 35
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Landroidx/media/filterpacks/base/MetaFilter;->mInputFrames:Ljava/util/HashMap;

    .line 56
    return-void
.end method


# virtual methods
.method protected A()V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Landroidx/media/filterpacks/base/MetaFilter;->mState:Ladt;

    const/4 v1, 0x1

    iput v1, v0, Ladt;->a:I

    .line 133
    invoke-static {}, Labx;->a()Labx;

    move-result-object v0

    .line 134
    iget-object v1, p0, Landroidx/media/filterpacks/base/MetaFilter;->mCurrentGraph:Laan;

    invoke-virtual {v0, v1, p0}, Labx;->a(Laan;Lacl;)V

    .line 135
    return-void
.end method

.method protected C()V
    .locals 4

    .prologue
    .line 146
    iget-object v0, p0, Landroidx/media/filterpacks/base/MetaFilter;->mGraphProvider:Lads;

    iget-object v1, p0, Landroidx/media/filterpacks/base/MetaFilter;->mInputFrames:Ljava/util/HashMap;

    invoke-interface {v0}, Lads;->a()Laan;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/base/MetaFilter;->mCurrentGraph:Laan;

    .line 147
    iget-object v0, p0, Landroidx/media/filterpacks/base/MetaFilter;->mInputFrames:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 148
    iget-object v3, p0, Landroidx/media/filterpacks/base/MetaFilter;->mCurrentGraph:Laan;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Laan;->d(Ljava/lang/String;)Landroidx/media/filterpacks/base/GraphInputSource;

    move-result-object v1

    .line 149
    if-nez v1, :cond_0

    .line 150
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Input port \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' could not be mapped to any input in the filter graph!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 153
    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laap;

    invoke-virtual {p0, v1, v0}, Landroidx/media/filterpacks/base/MetaFilter;->a(Landroidx/media/filterpacks/base/GraphInputSource;Laap;)V

    goto :goto_0

    .line 156
    :cond_1
    return-void
.end method

.method protected E()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 108
    iget-object v2, p0, Laak;->mConnectedOutputPortArray:[Lacv;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_2

    aget-object v4, v2, v0

    .line 109
    invoke-virtual {v4}, Lacv;->b()Ljava/lang/String;

    move-result-object v5

    .line 110
    iget-object v6, p0, Landroidx/media/filterpacks/base/MetaFilter;->mCurrentGraph:Laan;

    invoke-virtual {v6, v5}, Laan;->c(Ljava/lang/String;)Landroidx/media/filterpacks/base/GraphOutputTarget;

    move-result-object v6

    .line 111
    if-nez v6, :cond_0

    .line 112
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Output port \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' could not be mapped to any output in the filter graph!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 115
    :cond_0
    invoke-virtual {v6}, Landroidx/media/filterpacks/base/GraphOutputTarget;->y()Laap;

    move-result-object v5

    .line 116
    if-eqz v5, :cond_1

    .line 117
    invoke-virtual {p0, v5, v4}, Landroidx/media/filterpacks/base/MetaFilter;->a(Laap;Lacv;)V

    .line 118
    invoke-virtual {v5}, Laap;->f()Laap;

    .line 108
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 120
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "No output frame produced for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 124
    :cond_2
    iget-object v0, p0, Landroidx/media/filterpacks/base/MetaFilter;->mState:Ladt;

    iput v1, v0, Ladt;->a:I

    .line 125
    return-void
.end method

.method public a()V
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Landroidx/media/filterpacks/base/MetaFilter;->mState:Ladt;

    iget v0, v0, Ladt;->a:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 165
    iget-object v0, p0, Landroidx/media/filterpacks/base/MetaFilter;->mState:Ladt;

    const/4 v1, 0x2

    iput v1, v0, Ladt;->a:I

    .line 167
    :cond_0
    return-void
.end method

.method public a(Laan;)V
    .locals 2

    .prologue
    .line 59
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/MetaFilter;->r()Z

    move-result v0

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Ladr;

    invoke-direct {v0, p1}, Ladr;-><init>(Laan;)V

    iput-object v0, p0, Landroidx/media/filterpacks/base/MetaFilter;->mGraphProvider:Lads;

    return-void

    .line 62
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot set FilterGraphProvider while MetaFilter is running!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected a(Laap;Lacv;)V
    .locals 0

    .prologue
    .line 128
    invoke-virtual {p2, p1}, Lacv;->a(Laap;)V

    .line 129
    return-void
.end method

.method protected a(Landroidx/media/filterpacks/base/GraphInputSource;Laap;)V
    .locals 0

    .prologue
    .line 159
    invoke-virtual {p1, p2}, Landroidx/media/filterpacks/base/GraphInputSource;->b(Laap;)V

    .line 160
    return-void
.end method

.method protected h()V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Landroidx/media/filterpacks/base/MetaFilter;->mState:Ladt;

    const/4 v1, 0x0

    iput v1, v0, Ladt;->a:I

    .line 79
    return-void
.end method

.method protected i()V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Landroidx/media/filterpacks/base/MetaFilter;->mState:Ladt;

    iget v0, v0, Ladt;->a:I

    if-nez v0, :cond_1

    .line 84
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/MetaFilter;->z()V

    .line 85
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/MetaFilter;->A()V

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 86
    :cond_1
    iget-object v0, p0, Landroidx/media/filterpacks/base/MetaFilter;->mState:Ladt;

    iget v0, v0, Ladt;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 87
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/MetaFilter;->E()V

    goto :goto_0
.end method

.method protected j()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Landroidx/media/filterpacks/base/MetaFilter;->mState:Ladt;

    const/4 v1, 0x0

    iput v1, v0, Ladt;->a:I

    .line 94
    return-void
.end method

.method protected o()Z
    .locals 1

    .prologue
    .line 98
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/MetaFilter;->y()Z

    move-result v0

    return v0
.end method

.method protected y()Z
    .locals 2

    .prologue
    .line 102
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/MetaFilter;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroidx/media/filterpacks/base/MetaFilter;->l()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroidx/media/filterpacks/base/MetaFilter;->mState:Ladt;

    iget v0, v0, Ladt;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/MetaFilter;->m()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected z()V
    .locals 6

    .prologue
    .line 138
    iget-object v0, p0, Landroidx/media/filterpacks/base/MetaFilter;->mInputFrames:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 139
    iget-object v1, p0, Laak;->mConnectedInputPortArray:[Lacp;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 140
    iget-object v4, p0, Landroidx/media/filterpacks/base/MetaFilter;->mInputFrames:Ljava/util/HashMap;

    invoke-virtual {v3}, Lacp;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lacp;->c()Laap;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 142
    :cond_0
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/MetaFilter;->C()V

    .line 143
    return-void
.end method
