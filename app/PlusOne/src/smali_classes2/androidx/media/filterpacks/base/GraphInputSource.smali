.class public Landroidx/media/filterpacks/base/GraphInputSource;
.super Laak;
.source "PG"


# instance fields
.field private mFrame:Laap;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/base/GraphInputSource;->mFrame:Laap;

    .line 17
    return-void
.end method


# virtual methods
.method public b(Laap;)V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p0, Landroidx/media/filterpacks/base/GraphInputSource;->mFrame:Laap;

    if-eqz v0, :cond_0

    .line 28
    iget-object v0, p0, Landroidx/media/filterpacks/base/GraphInputSource;->mFrame:Laap;

    invoke-virtual {v0}, Laap;->f()Laap;

    .line 30
    :cond_0
    if-nez p1, :cond_1

    .line 31
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempting to assign null-frame!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 33
    :cond_1
    invoke-virtual {p1}, Laap;->g()Laap;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/base/GraphInputSource;->mFrame:Laap;

    .line 34
    return-void
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    .line 21
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "frame"

    const/4 v2, 0x2

    invoke-static {}, Labf;->a()Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->a()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Landroidx/media/filterpacks/base/GraphInputSource;->mFrame:Laap;

    if-eqz v0, :cond_0

    .line 39
    const-string v0, "frame"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/GraphInputSource;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    iget-object v1, p0, Landroidx/media/filterpacks/base/GraphInputSource;->mFrame:Laap;

    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    .line 40
    iget-object v0, p0, Landroidx/media/filterpacks/base/GraphInputSource;->mFrame:Laap;

    invoke-virtual {v0}, Laap;->f()Laap;

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/base/GraphInputSource;->mFrame:Laap;

    .line 43
    :cond_0
    return-void
.end method

.method protected k()V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Landroidx/media/filterpacks/base/GraphInputSource;->mFrame:Laap;

    if-eqz v0, :cond_0

    .line 48
    iget-object v0, p0, Landroidx/media/filterpacks/base/GraphInputSource;->mFrame:Laap;

    invoke-virtual {v0}, Laap;->f()Laap;

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/base/GraphInputSource;->mFrame:Laap;

    .line 51
    :cond_0
    return-void
.end method

.method protected o()Z
    .locals 1

    .prologue
    .line 55
    invoke-super {p0}, Laak;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroidx/media/filterpacks/base/GraphInputSource;->mFrame:Laap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
