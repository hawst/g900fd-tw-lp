.class public Landroidx/media/filterpacks/base/IterateFilter;
.super Landroidx/media/filterpacks/base/MetaFilter;
.source "PG"


# instance fields
.field mIndex:I

.field mInputs:Ljava/lang/Object;

.field mOutputs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroidx/media/filterpacks/base/MetaFilter;-><init>(Lacs;Ljava/lang/String;)V

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Landroidx/media/filterpacks/base/IterateFilter;->mIndex:I

    .line 21
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/base/IterateFilter;->mInputs:Ljava/lang/Object;

    .line 22
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Landroidx/media/filterpacks/base/IterateFilter;->mOutputs:Ljava/util/HashMap;

    .line 26
    return-void
.end method


# virtual methods
.method protected B()V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Landroidx/media/filterpacks/base/IterateFilter;->mInputFrames:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laap;

    .line 57
    invoke-virtual {v0}, Laap;->f()Laap;

    goto :goto_0

    .line 59
    :cond_0
    iget-object v0, p0, Landroidx/media/filterpacks/base/IterateFilter;->mInputFrames:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 60
    return-void
.end method

.method protected C()V
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 73
    iget-object v0, p0, Landroidx/media/filterpacks/base/IterateFilter;->mGraphProvider:Lads;

    iget-object v1, p0, Landroidx/media/filterpacks/base/IterateFilter;->mInputFrames:Ljava/util/HashMap;

    invoke-interface {v0}, Lads;->a()Laan;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/base/IterateFilter;->mCurrentGraph:Laan;

    .line 74
    iget-object v0, p0, Landroidx/media/filterpacks/base/IterateFilter;->mInputFrames:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 78
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v3, "array"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 79
    iget-object v1, p0, Landroidx/media/filterpacks/base/IterateFilter;->mInputs:Ljava/lang/Object;

    if-nez v1, :cond_1

    .line 80
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laap;

    invoke-virtual {v1}, Laap;->b()Labh;

    move-result-object v1

    invoke-virtual {v1}, Labh;->k()Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Landroidx/media/filterpacks/base/IterateFilter;->mInputs:Ljava/lang/Object;

    iput v2, p0, Landroidx/media/filterpacks/base/IterateFilter;->mIndex:I

    iget-object v1, p0, Landroidx/media/filterpacks/base/IterateFilter;->mOutputs:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    iget-object v1, p0, Landroidx/media/filterpacks/base/IterateFilter;->mInputs:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v3

    iget-object v5, p0, Laak;->mConnectedOutputPortArray:[Lacv;

    array-length v7, v5

    move v1, v2

    :goto_1
    if-ge v1, v7, :cond_1

    aget-object v8, v5, v1

    iget-object v9, p0, Landroidx/media/filterpacks/base/IterateFilter;->mCurrentGraph:Laan;

    invoke-virtual {v8}, Lacv;->b()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Laan;->c(Ljava/lang/String;)Landroidx/media/filterpacks/base/GraphOutputTarget;

    move-result-object v9

    invoke-virtual {v9}, Landroidx/media/filterpacks/base/GraphOutputTarget;->a()Labf;

    move-result-object v9

    invoke-virtual {v9}, Labf;->d()Ljava/lang/Class;

    move-result-object v9

    invoke-static {v9, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v9

    iget-object v10, p0, Landroidx/media/filterpacks/base/IterateFilter;->mOutputs:Ljava/util/HashMap;

    invoke-virtual {v8}, Lacv;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v10, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 82
    :cond_1
    iget-object v1, p0, Landroidx/media/filterpacks/base/IterateFilter;->mInputs:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v1

    if-lez v1, :cond_5

    .line 83
    iget-object v1, p0, Landroidx/media/filterpacks/base/IterateFilter;->mCurrentGraph:Laan;

    const-string v3, "elem"

    invoke-virtual {v1, v3}, Laan;->d(Ljava/lang/String;)Landroidx/media/filterpacks/base/GraphInputSource;

    move-result-object v5

    .line 84
    iget-object v1, p0, Landroidx/media/filterpacks/base/IterateFilter;->mInputs:Ljava/lang/Object;

    iget v3, p0, Landroidx/media/filterpacks/base/IterateFilter;->mIndex:I

    invoke-static {v1, v3}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v1

    .line 85
    invoke-static {}, Labf;->b()Labf;

    move-result-object v3

    invoke-static {v3, v4}, Laap;->a(Labf;[I)Laap;

    move-result-object v3

    .line 86
    invoke-virtual {v3}, Laap;->a()Labg;

    move-result-object v7

    invoke-virtual {v7, v1}, Labg;->a(Ljava/lang/Object;)V

    .line 87
    const/4 v1, 0x1

    .line 93
    :goto_2
    if-eqz v3, :cond_0

    .line 94
    if-nez v5, :cond_3

    .line 95
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Input port \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' could not be mapped to any input in the filter graph!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 90
    :cond_2
    iget-object v3, p0, Landroidx/media/filterpacks/base/IterateFilter;->mCurrentGraph:Laan;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Laan;->d(Ljava/lang/String;)Landroidx/media/filterpacks/base/GraphInputSource;

    move-result-object v3

    .line 91
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Laap;

    move-object v5, v3

    move-object v3, v1

    move v1, v2

    goto :goto_2

    .line 98
    :cond_3
    invoke-virtual {v5, v3}, Landroidx/media/filterpacks/base/GraphInputSource;->b(Laap;)V

    .line 99
    if-eqz v1, :cond_0

    .line 100
    invoke-virtual {v3}, Laap;->f()Laap;

    goto/16 :goto_0

    .line 104
    :cond_4
    return-void

    :cond_5
    move v1, v2

    move-object v3, v4

    move-object v5, v4

    goto :goto_2
.end method

.method protected D()V
    .locals 7

    .prologue
    .line 107
    iget-object v1, p0, Laak;->mConnectedOutputPortArray:[Lacv;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 108
    invoke-virtual {v3}, Lacv;->b()Ljava/lang/String;

    move-result-object v3

    .line 109
    iget-object v4, p0, Landroidx/media/filterpacks/base/IterateFilter;->mCurrentGraph:Laan;

    invoke-virtual {v4, v3}, Laan;->c(Ljava/lang/String;)Landroidx/media/filterpacks/base/GraphOutputTarget;

    move-result-object v4

    .line 110
    if-nez v4, :cond_0

    .line 111
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Output port \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' could not be mapped to any output in the filter graph!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :cond_0
    iget-object v5, p0, Landroidx/media/filterpacks/base/IterateFilter;->mOutputs:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .line 116
    invoke-virtual {v4}, Landroidx/media/filterpacks/base/GraphOutputTarget;->y()Laap;

    move-result-object v5

    .line 117
    if-nez v5, :cond_1

    .line 119
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "Output \'"

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Landroidx/media/filterpacks/base/GraphOutputTarget;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' has no frame!"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 107
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 121
    :cond_1
    iget v4, p0, Landroidx/media/filterpacks/base/IterateFilter;->mIndex:I

    invoke-virtual {v5}, Laap;->a()Labg;

    move-result-object v6

    invoke-virtual {v6}, Labg;->j()Ljava/lang/Object;

    move-result-object v6

    invoke-static {v3, v4, v6}, Ljava/lang/reflect/Array;->set(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 122
    invoke-virtual {v5}, Laap;->f()Laap;

    goto :goto_1

    .line 126
    :cond_2
    iget v0, p0, Landroidx/media/filterpacks/base/IterateFilter;->mIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroidx/media/filterpacks/base/IterateFilter;->mIndex:I

    .line 127
    return-void
.end method

.method protected E()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 131
    iget-object v2, p0, Laak;->mConnectedOutputPortArray:[Lacv;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 132
    invoke-virtual {v4}, Lacv;->b()Ljava/lang/String;

    move-result-object v5

    .line 133
    iget-object v6, p0, Landroidx/media/filterpacks/base/IterateFilter;->mOutputs:Ljava/util/HashMap;

    invoke-virtual {v6, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    .line 134
    invoke-static {}, Labf;->c()Labf;

    move-result-object v6

    const/4 v7, 0x1

    new-array v7, v7, [I

    invoke-static {v5}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v8

    aput v8, v7, v1

    invoke-static {v6, v7}, Laap;->a(Labf;[I)Laap;

    move-result-object v6

    .line 135
    invoke-virtual {v6}, Laap;->b()Labh;

    move-result-object v7

    invoke-virtual {v7, v5}, Labh;->b(Ljava/lang/Object;)V

    .line 136
    invoke-virtual {v4, v6}, Lacv;->a(Laap;)V

    .line 137
    invoke-virtual {v6}, Laap;->f()Laap;

    .line 131
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 139
    :cond_0
    iget-object v0, p0, Landroidx/media/filterpacks/base/IterateFilter;->mState:Ladt;

    iput v1, v0, Ladt;->a:I

    .line 140
    return-void
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    .line 30
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "array"

    const/4 v2, 0x2

    invoke-static {}, Labf;->c()Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Landroidx/media/filterpacks/base/IterateFilter;->mState:Ladt;

    iget v0, v0, Ladt;->a:I

    if-nez v0, :cond_2

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/base/IterateFilter;->mInputs:Ljava/lang/Object;

    .line 38
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/IterateFilter;->z()V

    .line 39
    iget-object v0, p0, Landroidx/media/filterpacks/base/IterateFilter;->mInputs:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v0

    if-lez v0, :cond_1

    .line 40
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/IterateFilter;->A()V

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 42
    :cond_1
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/IterateFilter;->E()V

    goto :goto_0

    .line 44
    :cond_2
    iget-object v0, p0, Landroidx/media/filterpacks/base/IterateFilter;->mState:Ladt;

    iget v0, v0, Ladt;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 45
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/IterateFilter;->D()V

    .line 46
    iget v0, p0, Landroidx/media/filterpacks/base/IterateFilter;->mIndex:I

    iget-object v1, p0, Landroidx/media/filterpacks/base/IterateFilter;->mInputs:Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 47
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/IterateFilter;->C()V

    .line 48
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/IterateFilter;->A()V

    goto :goto_0

    .line 50
    :cond_3
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/IterateFilter;->E()V

    goto :goto_0
.end method

.method protected z()V
    .locals 6

    .prologue
    .line 64
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/IterateFilter;->B()V

    .line 65
    iget-object v1, p0, Laak;->mConnectedInputPortArray:[Lacp;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 66
    iget-object v4, p0, Landroidx/media/filterpacks/base/IterateFilter;->mInputFrames:Ljava/util/HashMap;

    invoke-virtual {v3}, Lacp;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3}, Lacp;->c()Laap;

    move-result-object v3

    invoke-virtual {v3}, Laap;->g()Laap;

    move-result-object v3

    invoke-virtual {v4, v5, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 68
    :cond_0
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/IterateFilter;->C()V

    .line 69
    return-void
.end method
