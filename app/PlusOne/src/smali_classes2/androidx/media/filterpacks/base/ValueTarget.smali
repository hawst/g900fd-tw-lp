.class public final Landroidx/media/filterpacks/base/ValueTarget;
.super Laak;
.source "PG"


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mListener:Ladv;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 30
    iput-object v0, p0, Landroidx/media/filterpacks/base/ValueTarget;->mListener:Ladv;

    .line 31
    iput-object v0, p0, Landroidx/media/filterpacks/base/ValueTarget;->mHandler:Landroid/os/Handler;

    .line 35
    return-void
.end method

.method public static synthetic a(Landroidx/media/filterpacks/base/ValueTarget;)Ladv;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Landroidx/media/filterpacks/base/ValueTarget;->mListener:Ladv;

    return-object v0
.end method


# virtual methods
.method public c()Lacx;
    .locals 4

    .prologue
    .line 54
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "value"

    const/4 v2, 0x2

    invoke-static {}, Labf;->b()Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 3

    .prologue
    .line 61
    const-string v0, "value"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/ValueTarget;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->a()Labg;

    move-result-object v0

    .line 62
    iget-object v1, p0, Landroidx/media/filterpacks/base/ValueTarget;->mListener:Ladv;

    if-eqz v1, :cond_0

    .line 63
    iget-object v1, p0, Landroidx/media/filterpacks/base/ValueTarget;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 64
    invoke-virtual {v0}, Labg;->j()Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Landroidx/media/filterpacks/base/ValueTarget;->mHandler:Landroid/os/Handler;

    new-instance v2, Ladu;

    invoke-direct {v2, p0, v0}, Ladu;-><init>(Landroidx/media/filterpacks/base/ValueTarget;Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 66
    :cond_1
    iget-object v1, p0, Landroidx/media/filterpacks/base/ValueTarget;->mListener:Ladv;

    invoke-virtual {v0}, Labg;->j()Ljava/lang/Object;

    goto :goto_0
.end method
