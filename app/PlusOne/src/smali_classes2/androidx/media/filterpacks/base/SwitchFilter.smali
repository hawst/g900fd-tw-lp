.class public final Landroidx/media/filterpacks/base/SwitchFilter;
.super Laak;
.source "PG"


# instance fields
.field private mTarget:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 29
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/base/SwitchFilter;->mTarget:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method public a(Lacv;)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lacv;->a(Z)V

    .line 46
    return-void
.end method

.method public b(Lacp;)V
    .locals 4

    .prologue
    .line 50
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "input"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v1, p0, Laak;->mConnectedOutputPortArray:[Lacv;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 52
    invoke-virtual {p1, v3}, Lacp;->a(Lacv;)V

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54
    :cond_0
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "target"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    const-string v0, "mTarget"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 56
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->a(Z)V

    .line 58
    :cond_1
    return-void
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    .line 37
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "input"

    const/4 v2, 0x2

    invoke-static {}, Labf;->a()Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "target"

    const/4 v2, 0x1

    const-class v3, Ljava/lang/String;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->a()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 3

    .prologue
    .line 62
    iget-object v0, p0, Landroidx/media/filterpacks/base/SwitchFilter;->mTarget:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Landroidx/media/filterpacks/base/SwitchFilter;->mTarget:Ljava/lang/String;

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/SwitchFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 64
    if-eqz v0, :cond_1

    .line 65
    invoke-virtual {v0}, Lacv;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    const-string v1, "input"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/base/SwitchFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    .line 67
    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    .line 73
    :cond_0
    return-void

    .line 70
    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown target port \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroidx/media/filterpacks/base/SwitchFilter;->mTarget:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
