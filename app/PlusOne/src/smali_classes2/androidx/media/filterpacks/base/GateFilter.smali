.class public final Landroidx/media/filterpacks/base/GateFilter;
.super Laak;
.source "PG"


# instance fields
.field private mPassFrames:I


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 27
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 1

    .prologue
    .line 47
    const-string v0, "frame"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/GateFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    invoke-virtual {p1, v0}, Lacp;->a(Lacv;)V

    .line 48
    return-void
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 39
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "frame"

    invoke-static {}, Labf;->a()Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "frame"

    invoke-static {}, Labf;->a()Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected h()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    iput v0, p0, Landroidx/media/filterpacks/base/GateFilter;->mPassFrames:I

    .line 53
    return-void
.end method

.method protected declared-synchronized i()V
    .locals 2

    .prologue
    .line 57
    monitor-enter p0

    :try_start_0
    const-string v0, "frame"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/GateFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    .line 58
    iget v1, p0, Landroidx/media/filterpacks/base/GateFilter;->mPassFrames:I

    if-lez v1, :cond_0

    .line 59
    const-string v1, "frame"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/base/GateFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    invoke-virtual {v1, v0}, Lacv;->a(Laap;)V

    .line 60
    iget v0, p0, Landroidx/media/filterpacks/base/GateFilter;->mPassFrames:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroidx/media/filterpacks/base/GateFilter;->mPassFrames:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    :cond_0
    monitor-exit p0

    return-void

    .line 57
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
