.class public final Landroidx/media/filterpacks/base/SequenceToArrayFilter;
.super Laak;
.source "PG"


# instance fields
.field private mNumRemainingElements:I

.field private mStage:I

.field mValues:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 36
    const/4 v0, 0x1

    iput v0, p0, Landroidx/media/filterpacks/base/SequenceToArrayFilter;->mStage:I

    .line 37
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Landroidx/media/filterpacks/base/SequenceToArrayFilter;->mValues:Ljava/util/Queue;

    .line 41
    return-void
.end method

.method private c(Lacv;)Ljava/lang/Class;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lacv;",
            ")",
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation

    .prologue
    .line 131
    invoke-virtual {p1}, Lacv;->d()Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->f()Labf;

    move-result-object v0

    invoke-virtual {v0}, Labf;->d()Ljava/lang/Class;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Lacp;)V
    .locals 2

    .prologue
    .line 54
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "remainingElements"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->b(Z)V

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 56
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "inputSequence"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lacp;->b(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 45
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "inputSequence"

    invoke-static {}, Labf;->b()Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "remainingElements"

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "outputArray"

    invoke-static {}, Labf;->c()Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 69
    const-string v0, "remainingElements"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/SequenceToArrayFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v3

    .line 70
    const-string v0, "inputSequence"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/SequenceToArrayFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v5

    .line 71
    const-string v0, "outputArray"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/SequenceToArrayFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v6

    .line 76
    iget v0, p0, Landroidx/media/filterpacks/base/SequenceToArrayFilter;->mStage:I

    packed-switch v0, :pswitch_data_0

    .line 118
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "SequenceToArray: invalid state!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 78
    :pswitch_0
    invoke-virtual {v3}, Lacp;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 79
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "SequenceToArray expected frame on port remainingElements, but no frame is available!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_0
    invoke-virtual {v3}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->a()Labg;

    move-result-object v0

    invoke-virtual {v0}, Labg;->j()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Landroidx/media/filterpacks/base/SequenceToArrayFilter;->mNumRemainingElements:I

    .line 84
    iget v0, p0, Landroidx/media/filterpacks/base/SequenceToArrayFilter;->mNumRemainingElements:I

    if-lez v0, :cond_2

    .line 85
    invoke-virtual {v3, v1}, Lacp;->b(Z)V

    .line 86
    invoke-virtual {v5, v4}, Lacp;->b(Z)V

    .line 87
    const/4 v0, 0x2

    iput v0, p0, Landroidx/media/filterpacks/base/SequenceToArrayFilter;->mStage:I

    move-object v0, v2

    .line 122
    :goto_0
    if-eqz v1, :cond_1

    .line 123
    invoke-virtual {v6, v2}, Lacv;->a([I)Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->b()Labh;

    move-result-object v1

    .line 124
    invoke-virtual {v1, v0}, Labg;->a(Ljava/lang/Object;)V

    .line 125
    invoke-virtual {v6, v1}, Lacv;->a(Laap;)V

    .line 126
    iget-object v0, p0, Landroidx/media/filterpacks/base/SequenceToArrayFilter;->mValues:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 128
    :cond_1
    return-void

    .line 89
    :cond_2
    invoke-direct {p0, v6}, Landroidx/media/filterpacks/base/SequenceToArrayFilter;->c(Lacv;)Ljava/lang/Class;

    move-result-object v0

    .line 90
    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    move v1, v4

    .line 93
    goto :goto_0

    .line 96
    :pswitch_1
    invoke-virtual {v5}, Lacp;->d()Z

    move-result v0

    if-nez v0, :cond_3

    .line 97
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "SequenceToArray expected frame on port inputSequence, but no frame is available!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 99
    :cond_3
    iget-object v0, p0, Landroidx/media/filterpacks/base/SequenceToArrayFilter;->mValues:Ljava/util/Queue;

    invoke-virtual {v5}, Lacp;->c()Laap;

    move-result-object v7

    invoke-virtual {v7}, Laap;->a()Labg;

    move-result-object v7

    invoke-virtual {v7}, Labg;->j()Ljava/lang/Object;

    move-result-object v7

    invoke-interface {v0, v7}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 102
    invoke-virtual {v3, v4}, Lacp;->b(Z)V

    .line 103
    invoke-virtual {v5, v1}, Lacp;->b(Z)V

    .line 104
    iput v4, p0, Landroidx/media/filterpacks/base/SequenceToArrayFilter;->mStage:I

    .line 108
    iget v0, p0, Landroidx/media/filterpacks/base/SequenceToArrayFilter;->mNumRemainingElements:I

    if-ne v0, v4, :cond_5

    .line 109
    invoke-direct {p0, v6}, Landroidx/media/filterpacks/base/SequenceToArrayFilter;->c(Lacv;)Ljava/lang/Class;

    move-result-object v0

    .line 110
    iget-object v3, p0, Landroidx/media/filterpacks/base/SequenceToArrayFilter;->mValues:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->size()I

    move-result v3

    invoke-static {v0, v3}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v3

    move v0, v1

    .line 111
    :goto_1
    invoke-static {v3}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v1

    if-ge v0, v1, :cond_4

    .line 112
    iget-object v1, p0, Landroidx/media/filterpacks/base/SequenceToArrayFilter;->mValues:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v1

    invoke-static {v3, v0, v1}, Ljava/lang/reflect/Array;->set(Ljava/lang/Object;ILjava/lang/Object;)V

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move-object v0, v3

    move v1, v4

    .line 115
    goto :goto_0

    :cond_5
    move-object v0, v2

    goto :goto_0

    .line 76
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
