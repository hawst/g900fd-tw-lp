.class public final Landroidx/media/filterpacks/base/ArraySelectFilter;
.super Laak;
.source "PG"


# instance fields
.field private mDefaultValue:Ljava/lang/Object;

.field private mIndex:I


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Landroidx/media/filterpacks/base/ArraySelectFilter;->mIndex:I

    .line 28
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/base/ArraySelectFilter;->mDefaultValue:Ljava/lang/Object;

    .line 32
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 46
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "index"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 47
    const-string v0, "mIndex"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 48
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 53
    :cond_0
    :goto_0
    return-void

    .line 49
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "defaultValue"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    const-string v0, "mDefaultValue"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 51
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 36
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "array"

    invoke-static {}, Labf;->c()Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v4, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "index"

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "defaultValue"

    invoke-static {}, Labf;->b()Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "element"

    invoke-static {}, Labf;->b()Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v4, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 4

    .prologue
    .line 57
    const-string v0, "array"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/ArraySelectFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    .line 58
    const-string v1, "element"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/base/ArraySelectFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    .line 59
    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->b()Labh;

    move-result-object v0

    invoke-virtual {v0}, Labh;->k()Ljava/lang/Object;

    move-result-object v0

    .line 60
    invoke-static {v0}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v2

    .line 61
    iget v3, p0, Landroidx/media/filterpacks/base/ArraySelectFilter;->mIndex:I

    if-le v2, v3, :cond_0

    iget v2, p0, Landroidx/media/filterpacks/base/ArraySelectFilter;->mIndex:I

    invoke-static {v0, v2}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v0

    .line 62
    :goto_0
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->a()Labg;

    move-result-object v2

    .line 63
    invoke-virtual {v2, v0}, Labg;->a(Ljava/lang/Object;)V

    .line 64
    invoke-virtual {v1, v2}, Lacv;->a(Laap;)V

    .line 65
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Landroidx/media/filterpacks/base/ArraySelectFilter;->mDefaultValue:Ljava/lang/Object;

    goto :goto_0
.end method
