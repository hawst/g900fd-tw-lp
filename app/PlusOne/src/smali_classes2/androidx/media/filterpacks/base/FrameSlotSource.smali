.class public final Landroidx/media/filterpacks/base/FrameSlotSource;
.super Landroidx/media/filterfw/SlotFilter;
.source "PG"


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3}, Landroidx/media/filterfw/SlotFilter;-><init>(Lacs;Ljava/lang/String;Ljava/lang/String;)V

    .line 25
    return-void
.end method


# virtual methods
.method public c()Lacx;
    .locals 4

    .prologue
    .line 31
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "frame"

    const/4 v2, 0x2

    invoke-static {}, Labf;->a()Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 2

    .prologue
    .line 43
    invoke-virtual {p0}, Landroidx/media/filterpacks/base/FrameSlotSource;->q()Laat;

    move-result-object v0

    iget-object v1, p0, Landroidx/media/filterpacks/base/FrameSlotSource;->mSlotName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Laat;->a(Ljava/lang/String;)Laap;

    move-result-object v0

    .line 44
    const-string v1, "frame"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/base/FrameSlotSource;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    invoke-virtual {v1, v0}, Lacv;->a(Laap;)V

    .line 45
    invoke-virtual {v0}, Laap;->f()Laap;

    .line 46
    return-void
.end method

.method protected o()Z
    .locals 2

    .prologue
    .line 38
    invoke-super {p0}, Landroidx/media/filterfw/SlotFilter;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroidx/media/filterfw/SlotFilter;->q()Laat;

    move-result-object v0

    iget-object v1, p0, Landroidx/media/filterfw/SlotFilter;->mSlotName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Laat;->b(Ljava/lang/String;)Laay;

    move-result-object v0

    invoke-virtual {v0}, Laay;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
