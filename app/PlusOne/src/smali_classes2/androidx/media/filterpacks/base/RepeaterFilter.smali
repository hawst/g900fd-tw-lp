.class public final Landroidx/media/filterpacks/base/RepeaterFilter;
.super Laak;
.source "PG"


# instance fields
.field mCachedFrame:Laap;

.field mRepeat:I


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/base/RepeaterFilter;->mCachedFrame:Laap;

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Landroidx/media/filterpacks/base/RepeaterFilter;->mRepeat:I

    .line 35
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "input"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    const-string v0, "output"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/RepeaterFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    invoke-virtual {p1, v0}, Lacp;->a(Lacv;)V

    .line 54
    :cond_0
    return-void
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 39
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "input"

    invoke-static {}, Labf;->a()Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "repeat"

    sget-object v2, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "output"

    invoke-static {}, Labf;->a()Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 58
    const-string v0, "repeat"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/RepeaterFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    .line 59
    const-string v2, "input"

    invoke-virtual {p0, v2}, Landroidx/media/filterpacks/base/RepeaterFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v2

    .line 61
    iget v3, p0, Landroidx/media/filterpacks/base/RepeaterFilter;->mRepeat:I

    if-gt v3, v1, :cond_1

    .line 62
    iget-object v3, p0, Landroidx/media/filterpacks/base/RepeaterFilter;->mCachedFrame:Laap;

    if-eqz v3, :cond_0

    iget-object v3, p0, Landroidx/media/filterpacks/base/RepeaterFilter;->mCachedFrame:Laap;

    invoke-virtual {v3}, Laap;->f()Laap;

    .line 63
    :cond_0
    invoke-virtual {v2}, Lacp;->c()Laap;

    move-result-object v3

    invoke-virtual {v3}, Laap;->g()Laap;

    move-result-object v3

    iput-object v3, p0, Landroidx/media/filterpacks/base/RepeaterFilter;->mCachedFrame:Laap;

    .line 67
    :cond_1
    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->a()Labg;

    move-result-object v0

    invoke-virtual {v0}, Labg;->j()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Landroidx/media/filterpacks/base/RepeaterFilter;->mRepeat:I

    .line 68
    iget v0, p0, Landroidx/media/filterpacks/base/RepeaterFilter;->mRepeat:I

    if-le v0, v1, :cond_3

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Lacp;->b(Z)V

    .line 71
    iget v0, p0, Landroidx/media/filterpacks/base/RepeaterFilter;->mRepeat:I

    if-lez v0, :cond_2

    .line 72
    const-string v0, "output"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/RepeaterFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    iget-object v1, p0, Landroidx/media/filterpacks/base/RepeaterFilter;->mCachedFrame:Laap;

    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    .line 74
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 68
    goto :goto_0
.end method

.method protected k()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Landroidx/media/filterpacks/base/RepeaterFilter;->mCachedFrame:Laap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroidx/media/filterpacks/base/RepeaterFilter;->mCachedFrame:Laap;

    invoke-virtual {v0}, Laap;->f()Laap;

    .line 79
    :cond_0
    return-void
.end method
