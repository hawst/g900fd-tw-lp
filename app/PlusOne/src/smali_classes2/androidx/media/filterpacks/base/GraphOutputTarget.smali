.class public Landroidx/media/filterpacks/base/GraphOutputTarget;
.super Laak;
.source "PG"


# instance fields
.field private mFrame:Laap;

.field private mType:Labf;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/base/GraphOutputTarget;->mFrame:Laap;

    .line 14
    invoke-static {}, Labf;->a()Labf;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/base/GraphOutputTarget;->mType:Labf;

    .line 18
    return-void
.end method


# virtual methods
.method public a()Labf;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Landroidx/media/filterpacks/base/GraphOutputTarget;->mType:Labf;

    return-object v0
.end method

.method public a(Labf;)V
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Landroidx/media/filterpacks/base/GraphOutputTarget;->mType:Labf;

    .line 23
    return-void
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    .line 31
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "frame"

    const/4 v2, 0x2

    iget-object v3, p0, Landroidx/media/filterpacks/base/GraphOutputTarget;->mType:Labf;

    invoke-virtual {v0, v1, v2, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->a()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 2

    .prologue
    .line 48
    const-string v0, "frame"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/GraphOutputTarget;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    .line 49
    iget-object v1, p0, Landroidx/media/filterpacks/base/GraphOutputTarget;->mFrame:Laap;

    if-eqz v1, :cond_0

    .line 50
    iget-object v1, p0, Landroidx/media/filterpacks/base/GraphOutputTarget;->mFrame:Laap;

    invoke-virtual {v1}, Laap;->f()Laap;

    .line 52
    :cond_0
    invoke-virtual {v0}, Laap;->g()Laap;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/base/GraphOutputTarget;->mFrame:Laap;

    .line 53
    return-void
.end method

.method protected o()Z
    .locals 1

    .prologue
    .line 57
    invoke-super {p0}, Laak;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroidx/media/filterpacks/base/GraphOutputTarget;->mFrame:Laap;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public y()Laap;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    .line 39
    iget-object v0, p0, Landroidx/media/filterpacks/base/GraphOutputTarget;->mFrame:Laap;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Landroidx/media/filterpacks/base/GraphOutputTarget;->mFrame:Laap;

    .line 41
    iput-object v1, p0, Landroidx/media/filterpacks/base/GraphOutputTarget;->mFrame:Laap;

    .line 43
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method
