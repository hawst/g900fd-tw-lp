.class public final Landroidx/media/filterpacks/base/VariableSource;
.super Laak;
.source "PG"


# instance fields
.field private mOutputPort:Lacv;

.field private mValue:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 25
    iput-object v0, p0, Landroidx/media/filterpacks/base/VariableSource;->mValue:Ljava/lang/Object;

    .line 26
    iput-object v0, p0, Landroidx/media/filterpacks/base/VariableSource;->mOutputPort:Lacv;

    .line 30
    return-void
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 33
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Landroidx/media/filterpacks/base/VariableSource;->mValue:Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 34
    monitor-exit p0

    return-void

    .line 33
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    .line 42
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "value"

    const/4 v2, 0x2

    invoke-static {}, Labf;->b()Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 49
    const-string v0, "value"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/VariableSource;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/base/VariableSource;->mOutputPort:Lacv;

    .line 50
    return-void
.end method

.method protected declared-synchronized i()V
    .locals 2

    .prologue
    .line 54
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Landroidx/media/filterpacks/base/VariableSource;->mOutputPort:Lacv;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lacv;->a([I)Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->a()Labg;

    move-result-object v0

    .line 55
    iget-object v1, p0, Landroidx/media/filterpacks/base/VariableSource;->mValue:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Labg;->a(Ljava/lang/Object;)V

    .line 56
    iget-object v1, p0, Landroidx/media/filterpacks/base/VariableSource;->mOutputPort:Lacv;

    invoke-virtual {v1, v0}, Lacv;->a(Laap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    monitor-exit p0

    return-void

    .line 54
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
