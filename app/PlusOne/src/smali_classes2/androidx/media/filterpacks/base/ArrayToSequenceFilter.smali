.class public final Landroidx/media/filterpacks/base/ArrayToSequenceFilter;
.super Laak;
.source "PG"


# instance fields
.field mValues:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 29
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Landroidx/media/filterpacks/base/ArrayToSequenceFilter;->mValues:Ljava/util/Queue;

    .line 33
    return-void
.end method


# virtual methods
.method public c()Lacx;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 37
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "inputArray"

    invoke-static {}, Labf;->c()Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "outputSequence"

    invoke-static {}, Labf;->b()Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "remainingElements"

    const/4 v2, 0x1

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 50
    const-string v0, "inputArray"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/ArrayToSequenceFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v2

    .line 53
    iget-object v0, p0, Landroidx/media/filterpacks/base/ArrayToSequenceFilter;->mValues:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 59
    invoke-virtual {v2}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->b()Labh;

    move-result-object v0

    invoke-virtual {v0}, Labh;->k()Ljava/lang/Object;

    move-result-object v3

    move v0, v1

    .line 60
    :goto_0
    invoke-static {v3}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 61
    iget-object v4, p0, Landroidx/media/filterpacks/base/ArrayToSequenceFilter;->mValues:Ljava/util/Queue;

    invoke-static {v3, v0}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 60
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 65
    :cond_0
    const-string v0, "remainingElements"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/ArrayToSequenceFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 66
    if-eqz v0, :cond_1

    .line 67
    invoke-virtual {v0, v7}, Lacv;->a([I)Laap;

    move-result-object v3

    invoke-virtual {v3}, Laap;->a()Labg;

    move-result-object v3

    .line 68
    iget-object v4, p0, Landroidx/media/filterpacks/base/ArrayToSequenceFilter;->mValues:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Labg;->a(Ljava/lang/Object;)V

    .line 69
    invoke-virtual {v0, v3}, Lacv;->a(Laap;)V

    .line 72
    :cond_1
    iget-object v0, p0, Landroidx/media/filterpacks/base/ArrayToSequenceFilter;->mValues:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 73
    const-string v0, "outputSequence"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/ArrayToSequenceFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 74
    invoke-virtual {v0, v7}, Lacv;->a([I)Laap;

    move-result-object v3

    invoke-virtual {v3}, Laap;->a()Labg;

    move-result-object v3

    .line 75
    iget-object v4, p0, Landroidx/media/filterpacks/base/ArrayToSequenceFilter;->mValues:Ljava/util/Queue;

    invoke-interface {v4}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v4

    .line 76
    invoke-virtual {v3, v4}, Labg;->a(Ljava/lang/Object;)V

    .line 77
    invoke-virtual {v0, v3}, Lacv;->a(Laap;)V

    .line 80
    :cond_2
    iget-object v0, p0, Landroidx/media/filterpacks/base/ArrayToSequenceFilter;->mValues:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    if-nez v0, :cond_3

    .line 82
    invoke-virtual {v2, v6}, Lacp;->b(Z)V

    .line 83
    iput v6, p0, Laak;->mMinimumAvailableInputs:I

    .line 89
    :goto_1
    return-void

    .line 86
    :cond_3
    invoke-virtual {v2, v1}, Lacp;->b(Z)V

    .line 87
    iput v1, p0, Laak;->mMinimumAvailableInputs:I

    goto :goto_1
.end method
