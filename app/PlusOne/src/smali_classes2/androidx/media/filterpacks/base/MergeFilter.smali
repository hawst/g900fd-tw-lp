.class public final Landroidx/media/filterpacks/base/MergeFilter;
.super Laak;
.source "PG"


# static fields
.field public static final MODE_LFU:I = 0x2

.field public static final MODE_LRU:I = 0x1


# instance fields
.field private mMode:I

.field private mPortScores:[J


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 33
    const/4 v0, 0x1

    iput v0, p0, Landroidx/media/filterpacks/base/MergeFilter;->mMode:I

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/base/MergeFilter;->mPortScores:[J

    .line 38
    return-void
.end method


# virtual methods
.method public a(Lacp;)V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lacp;->b(Z)V

    .line 57
    return-void
.end method

.method public b(Lacp;)V
    .locals 1

    .prologue
    .line 61
    const-string v0, "output"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/MergeFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    invoke-virtual {p1, v0}, Lacp;->a(Lacv;)V

    .line 62
    return-void
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    .line 49
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "output"

    const/4 v2, 0x2

    invoke-static {}, Labf;->a()Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->b()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected h()V
    .locals 4

    .prologue
    .line 66
    iget-object v0, p0, Laak;->mConnectedInputPortArray:[Lacp;

    array-length v0, v0

    new-array v0, v0, [J

    iput-object v0, p0, Landroidx/media/filterpacks/base/MergeFilter;->mPortScores:[J

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Landroidx/media/filterpacks/base/MergeFilter;->mPortScores:[J

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Landroidx/media/filterpacks/base/MergeFilter;->mPortScores:[J

    const-wide/high16 v2, -0x8000000000000000L

    aput-wide v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 67
    :cond_0
    return-void
.end method

.method protected i()V
    .locals 8

    .prologue
    .line 71
    const-wide/high16 v2, -0x8000000000000000L

    .line 72
    const/4 v1, -0x1

    .line 73
    iget-object v6, p0, Laak;->mConnectedInputPortArray:[Lacp;

    .line 74
    const/4 v0, 0x0

    :goto_0
    array-length v4, v6

    if-ge v0, v4, :cond_1

    .line 75
    aget-object v4, v6, v0

    invoke-virtual {v4}, Lacp;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 76
    iget-object v4, p0, Landroidx/media/filterpacks/base/MergeFilter;->mPortScores:[J

    aget-wide v4, v4, v0

    .line 77
    cmp-long v7, v4, v2

    if-ltz v7, :cond_0

    move v1, v0

    move-wide v2, v4

    .line 74
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_1
    if-ltz v1, :cond_2

    .line 84
    aget-object v0, v6, v1

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    .line 85
    const-string v2, "output"

    invoke-virtual {p0, v2}, Landroidx/media/filterpacks/base/MergeFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v2

    invoke-virtual {v2, v0}, Lacv;->a(Laap;)V

    .line 86
    iget v0, p0, Landroidx/media/filterpacks/base/MergeFilter;->mMode:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown merge mode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Landroidx/media/filterpacks/base/MergeFilter;->mMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Landroidx/media/filterpacks/base/MergeFilter;->mPortScores:[J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    neg-long v2, v2

    aput-wide v2, v0, v1

    .line 88
    :cond_2
    :goto_1
    return-void

    .line 86
    :pswitch_1
    iget-object v0, p0, Landroidx/media/filterpacks/base/MergeFilter;->mPortScores:[J

    iget-object v2, p0, Landroidx/media/filterpacks/base/MergeFilter;->mPortScores:[J

    aget-wide v2, v2, v1

    const-wide/16 v4, 0x1

    sub-long/2addr v2, v4

    aput-wide v2, v0, v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
