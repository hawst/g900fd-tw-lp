.class public final Landroidx/media/filterpacks/base/BranchFilter;
.super Laak;
.source "PG"


# instance fields
.field private mSynchronized:Z


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroidx/media/filterpacks/base/BranchFilter;->mSynchronized:Z

    .line 33
    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 29
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroidx/media/filterpacks/base/BranchFilter;->mSynchronized:Z

    .line 37
    iput-boolean p3, p0, Landroidx/media/filterpacks/base/BranchFilter;->mSynchronized:Z

    .line 38
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 4

    .prologue
    .line 50
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "input"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    iget-object v1, p0, Laak;->mConnectedOutputPortArray:[Lacv;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 52
    invoke-virtual {p1, v3}, Lacp;->a(Lacv;)V

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54
    :cond_0
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "synchronized"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    const-string v0, "mSynchronized"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 56
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacp;->a(Z)V

    .line 58
    :cond_1
    return-void
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    .line 42
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "input"

    const/4 v2, 0x2

    invoke-static {}, Labf;->a()Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "synchronized"

    const/4 v2, 0x1

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->a()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected h()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 62
    iget-boolean v0, p0, Landroidx/media/filterpacks/base/BranchFilter;->mSynchronized:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Laak;->mConnectedOutputPortArray:[Lacv;

    array-length v2, v0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lacv;->a(Z)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Laak;->mConnectedOutputPortArray:[Lacv;

    array-length v3, v2

    move v0, v1

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    invoke-virtual {v4, v1}, Lacv;->a(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 63
    :cond_1
    return-void
.end method

.method protected i()V
    .locals 6

    .prologue
    .line 67
    const-string v0, "input"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/base/BranchFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v1

    .line 68
    iget-object v2, p0, Laak;->mConnectedOutputPortArray:[Lacv;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 69
    invoke-virtual {v4}, Lacv;->a()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 70
    invoke-virtual {v4, v1}, Lacv;->a(Laap;)V

    .line 68
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 73
    :cond_1
    return-void
.end method
