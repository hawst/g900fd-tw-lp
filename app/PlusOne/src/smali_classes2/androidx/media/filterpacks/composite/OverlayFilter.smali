.class public final Landroidx/media/filterpacks/composite/OverlayFilter;
.super Laak;
.source "PG"


# static fields
.field public static final OVERLAY_ADD:I = 0x2

.field public static final OVERLAY_BURN:I = 0x8

.field public static final OVERLAY_DARKEN:I = 0xb

.field public static final OVERLAY_DIFFERENCE:I = 0x5

.field public static final OVERLAY_DIVIDE:I = 0x3

.field public static final OVERLAY_DODGE:I = 0x7

.field public static final OVERLAY_HARDLIGHT:I = 0x9

.field public static final OVERLAY_LIGHTEN:I = 0xc

.field public static final OVERLAY_MULTIPLY:I = 0x1

.field public static final OVERLAY_NORMAL:I = 0x0

.field public static final OVERLAY_OVERLAY:I = 0xd

.field public static final OVERLAY_SCREEN:I = 0x6

.field public static final OVERLAY_SOFTLIGHT:I = 0xa

.field public static final OVERLAY_SQUARED_DIFFERENCE:I = 0xe

.field public static final OVERLAY_SUBTRACT:I = 0x4

.field private static final mDefaultQuads:[Ladp;


# instance fields
.field private mHasMask:Z

.field private mIdShader:Lacm;

.field private mInputFrameCount:I

.field private mOldOverlayMode:I

.field private mOpacity:F

.field private mOverlayMode:I

.field private mOverlayShader:Lacm;

.field private mSourceQuads:[Ladp;

.field private mTargetQuads:[Ladp;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 49
    const/4 v0, 0x1

    new-array v0, v0, [Ladp;

    const/4 v1, 0x0

    invoke-static {v2, v2, v3, v3}, Ladp;->a(FFFF)Ladp;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Landroidx/media/filterpacks/composite/OverlayFilter;->mDefaultQuads:[Ladp;

    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 62
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 51
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mOpacity:F

    .line 54
    iput-object v2, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mSourceQuads:[Ladp;

    .line 55
    iput-object v2, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mTargetQuads:[Ladp;

    .line 56
    iput-boolean v1, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mHasMask:Z

    .line 57
    iput v1, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mOverlayMode:I

    .line 58
    const/4 v0, -0x1

    iput v0, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mOldOverlayMode:I

    .line 59
    const/4 v0, 0x1

    iput v0, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mInputFrameCount:I

    .line 63
    return-void
.end method


# virtual methods
.method public a(Lacp;)V
    .locals 2

    .prologue
    .line 83
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mask"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mHasMask:Z

    .line 86
    :cond_0
    return-void
.end method

.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 90
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "opacity"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    const-string v0, "mOpacity"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sourceQuads"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 94
    const-string v0, "mSourceQuads"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 95
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 96
    :cond_2
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "targetQuads"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 97
    const-string v0, "mTargetQuads"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 98
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 99
    :cond_3
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    const-string v0, "mOverlayMode"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 101
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 67
    invoke-static {v2, v5}, Labf;->a(II)Labf;

    move-result-object v0

    .line 68
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 69
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "source"

    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v2

    const-string v3, "overlay"

    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v2

    const-string v3, "mask"

    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "opacity"

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "mode"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "sourceQuads"

    const-class v3, Ladp;

    invoke-static {v3}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "targetQuads"

    const-class v3, Ladp;

    invoke-static {v3}, Labf;->b(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "composite"

    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 107
    invoke-static {}, Lacm;->a()Lacm;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mIdShader:Lacm;

    .line 108
    return-void
.end method

.method protected i()V
    .locals 14

    .prologue
    .line 112
    const-string v0, "composite"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/composite/OverlayFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v7

    .line 113
    iget v0, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mOverlayMode:I

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    .line 115
    :goto_0
    iget v1, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mOverlayMode:I

    iget v2, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mOldOverlayMode:I

    if-eq v1, v2, :cond_3

    .line 116
    iget-boolean v2, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mHasMask:Z

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v1, "attribute vec4 a_position;\nattribute vec2 a_texcoord;\n"

    invoke-direct {v3, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v2, :cond_6

    const-string v1, "attribute vec2 a_texcoord_full;\n"

    :goto_1
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v0, :cond_7

    const-string v1, "attribute vec2 a_texcoord_src;\n"

    :goto_2
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "varying vec2 v_texcoord;\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v2, :cond_8

    const-string v1, "varying vec2 v_texcoord_mask;\n"

    :goto_3
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v0, :cond_9

    const-string v1, "varying vec2 v_texcoord_src;\n"

    :goto_4
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "void main() {\n  gl_Position = a_position;\n  v_texcoord = a_texcoord;\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v2, :cond_a

    const-string v1, "v_texcoord_mask = a_texcoord_full;\n"

    :goto_5
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v0, :cond_b

    const-string v1, "v_texcoord_src = a_texcoord_src;\n"

    :goto_6
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "}\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-boolean v4, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mHasMask:Z

    if-eqz v0, :cond_c

    const-string v1, "tex_sampler_2"

    :goto_7
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v2, "precision mediump float;\nuniform sampler2D tex_sampler_0;\n"

    invoke-direct {v5, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v0, :cond_d

    const-string v2, "uniform sampler2D tex_sampler_1;\n"

    :goto_8
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz v4, :cond_e

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "uniform sampler2D "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v6, ";\n"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_9
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "uniform float opacity;\nvarying vec2 v_texcoord;\n"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz v4, :cond_f

    const-string v2, "varying vec2 v_texcoord_mask;\n"

    :goto_a
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz v0, :cond_10

    const-string v2, "varying vec2 v_texcoord_src;\n"

    :goto_b
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "void main() {\n  vec4 ovlColor = texture2D(tex_sampler_0, v_texcoord);\n"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-eqz v0, :cond_11

    const-string v2, "  vec4 srcColor = texture2D(tex_sampler_1, v_texcoord_src);\n"

    :goto_c
    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-eqz v4, :cond_12

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ovlColor.a = texture2D("

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ", v_texcoord_mask).a;\n"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_d
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  gl_FragColor = vec4("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v1, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mOverlayMode:I

    packed-switch v1, :pswitch_data_0

    const-string v1, "ovlColor.rgb"

    :goto_e
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", ovlColor.a * opacity);\n}\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lacm;

    invoke-direct {v2, v3, v1}, Lacm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mOverlayShader:Lacm;

    iget-boolean v1, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mHasMask:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x8

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    iget-object v2, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mOverlayShader:Lacm;

    const-string v3, "a_texcoord_full"

    const/4 v4, 0x2

    invoke-virtual {v2, v3, v1, v4}, Lacm;->a(Ljava/lang/String;[FI)V

    :cond_0
    iget-object v1, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mOverlayShader:Lacm;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lacm;->b(Z)V

    iget-object v1, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mOverlayShader:Lacm;

    const/16 v2, 0x302

    const/16 v3, 0x303

    invoke-virtual {v1, v2, v3}, Lacm;->a(II)V

    .line 117
    const/4 v1, 0x1

    iput v1, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mInputFrameCount:I

    if-eqz v0, :cond_1

    iget v1, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mInputFrameCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mInputFrameCount:I

    :cond_1
    iget-boolean v1, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mHasMask:Z

    if-eqz v1, :cond_2

    iget v1, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mInputFrameCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mInputFrameCount:I

    .line 118
    :cond_2
    iget v1, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mOverlayMode:I

    iput v1, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mOldOverlayMode:I

    .line 121
    :cond_3
    const-string v1, "source"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/composite/OverlayFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v8

    .line 122
    const-string v1, "overlay"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/composite/OverlayFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v9

    .line 123
    const/4 v1, 0x0

    .line 124
    iget-boolean v2, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mHasMask:Z

    if-eqz v2, :cond_4

    .line 125
    const-string v1, "mask"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/composite/OverlayFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v1

    .line 128
    :cond_4
    invoke-virtual {v8}, Laas;->i()[I

    move-result-object v2

    .line 129
    invoke-virtual {v7, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->e()Laas;

    move-result-object v10

    .line 131
    iget-object v2, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mIdShader:Lacm;

    invoke-virtual {v2, v8, v10}, Lacm;->a(Laas;Laas;)V

    .line 132
    iget-object v2, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mOverlayShader:Lacm;

    const-string v3, "opacity"

    iget v4, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mOpacity:F

    invoke-virtual {v2, v3, v4}, Lacm;->a(Ljava/lang/String;F)V

    .line 134
    iget-object v2, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mSourceQuads:[Ladp;

    if-eqz v2, :cond_13

    iget-object v2, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mTargetQuads:[Ladp;

    if-eqz v2, :cond_13

    iget-object v2, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mSourceQuads:[Ladp;

    array-length v2, v2

    iget-object v3, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mTargetQuads:[Ladp;

    array-length v3, v3

    if-eq v2, v3, :cond_13

    .line 136
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Mismatch between input source quad count ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mSourceQuads:[Ladp;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") and target quad count ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mTargetQuads:[Ladp;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_5
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 116
    :cond_6
    const-string v1, ""

    goto/16 :goto_1

    :cond_7
    const-string v1, ""

    goto/16 :goto_2

    :cond_8
    const-string v1, ""

    goto/16 :goto_3

    :cond_9
    const-string v1, ""

    goto/16 :goto_4

    :cond_a
    const-string v1, ""

    goto/16 :goto_5

    :cond_b
    const-string v1, ""

    goto/16 :goto_6

    :cond_c
    const-string v1, "tex_sampler_1"

    goto/16 :goto_7

    :cond_d
    const-string v2, ""

    goto/16 :goto_8

    :cond_e
    const-string v2, ""

    goto/16 :goto_9

    :cond_f
    const-string v2, ""

    goto/16 :goto_a

    :cond_10
    const-string v2, ""

    goto/16 :goto_b

    :cond_11
    const-string v2, ""

    goto/16 :goto_c

    :cond_12
    const-string v1, ""

    goto/16 :goto_d

    :pswitch_0
    const-string v1, "srcColor.rgb * ovlColor.rgb"

    goto/16 :goto_e

    :pswitch_1
    const-string v1, "srcColor.rgb + ovlColor.rgb"

    goto/16 :goto_e

    :pswitch_2
    const-string v1, "srcColor.rgb / ovlColor.rgb"

    goto/16 :goto_e

    :pswitch_3
    const-string v1, "srcColor.rgb - ovlColor.rgb"

    goto/16 :goto_e

    :pswitch_4
    const-string v1, "abs(srcColor.rgb - ovlColor.rgb)"

    goto/16 :goto_e

    :pswitch_5
    const-string v1, "1.0 - ((1.0 - ovlColor.rgb) * (1.0 - srcColor.rgb))"

    goto/16 :goto_e

    :pswitch_6
    const-string v1, "srcColor.rgb / (1.0 - ovlColor.rgb)"

    goto/16 :goto_e

    :pswitch_7
    const-string v1, "1.0 - ((1.0 - srcColor.rgb) / ovlColor.rgb)"

    goto/16 :goto_e

    :pswitch_8
    const-string v1, "vec3(ovlColor.r > 0.5 ? 1.0 - ((1.0 - 2.0 * (ovlColor.r - 0.5)) * (1.0 - srcColor.r)) : (2.0 * ovlColor.r * srcColor.r),     ovlColor.g > 0.5 ? 1.0 - ((1.0 - 2.0 * (ovlColor.g - 0.5)) * (1.0 - srcColor.g)) : (2.0 * ovlColor.g * srcColor.g),     ovlColor.b > 0.5 ? 1.0 - ((1.0 - 2.0 * (ovlColor.b - 0.5)) * (1.0 - srcColor.b)) : (2.0 * ovlColor.b * srcColor.b))"

    goto/16 :goto_e

    :pswitch_9
    const-string v1, "srcColor.rgb * ((1.0 - srcColor.rgb) * ovlColor.rgb + (1.0 - ((1.0 - ovlColor.rgb) * (1.0 - srcColor.rgb))))"

    goto/16 :goto_e

    :pswitch_a
    const-string v1, "min(srcColor.rgb, ovlColor.rgb)"

    goto/16 :goto_e

    :pswitch_b
    const-string v1, "max(srcColor.rgb, ovlColor.rgb)"

    goto/16 :goto_e

    :pswitch_c
    const-string v1, "srcColor.rgb * (srcColor.rgb + (2.0 * ovlColor.rgb) * (1.0 - srcColor.rgb))"

    goto/16 :goto_e

    :pswitch_d
    const-string v1, "(srcColor.rgb - ovlColor.rgb) * (srcColor.rgb - ovlColor.rgb)"

    goto/16 :goto_e

    .line 141
    :cond_13
    sget-object v3, Landroidx/media/filterpacks/composite/OverlayFilter;->mDefaultQuads:[Ladp;

    .line 142
    sget-object v4, Landroidx/media/filterpacks/composite/OverlayFilter;->mDefaultQuads:[Ladp;

    .line 143
    const/4 v2, 0x0

    .line 144
    iget-object v5, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mSourceQuads:[Ladp;

    if-eqz v5, :cond_14

    .line 145
    iget-object v3, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mSourceQuads:[Ladp;

    .line 146
    const/4 v2, 0x1

    .line 148
    :cond_14
    iget-object v5, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mTargetQuads:[Ladp;

    if-eqz v5, :cond_1f

    .line 149
    iget-object v4, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mTargetQuads:[Ladp;

    .line 150
    const/4 v2, 0x1

    move-object v6, v4

    .line 152
    :goto_f
    if-eqz v2, :cond_1b

    iget-object v2, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mSourceQuads:[Ladp;

    iget-object v4, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mTargetQuads:[Ladp;

    if-nez v2, :cond_18

    array-length v2, v4

    .line 153
    :goto_10
    const/4 v5, 0x0

    :goto_11
    if-ge v5, v2, :cond_1e

    .line 154
    array-length v4, v3

    if-ge v5, v4, :cond_1c

    move v4, v5

    :goto_12
    aget-object v11, v3, v4

    .line 155
    array-length v4, v6

    if-ge v5, v4, :cond_1d

    move v4, v5

    :goto_13
    aget-object v4, v6, v4

    .line 156
    iget-object v12, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mOverlayShader:Lacm;

    invoke-virtual {v12, v11}, Lacm;->a(Ladp;)V

    .line 157
    iget-object v11, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mOverlayShader:Lacm;

    invoke-virtual {v11, v4}, Lacm;->b(Ladp;)V

    .line 159
    if-eqz v0, :cond_15

    .line 160
    iget-object v11, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mOverlayShader:Lacm;

    const-string v12, "a_texcoord_src"

    invoke-virtual {v4}, Ladp;->f()[F

    move-result-object v4

    const/4 v13, 0x2

    invoke-virtual {v11, v12, v4, v13}, Lacm;->a(Ljava/lang/String;[FI)V

    .line 164
    :cond_15
    iget v4, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mInputFrameCount:I

    new-array v11, v4, [Laas;

    .line 165
    const/4 v12, 0x0

    const/4 v4, 0x1

    aput-object v9, v11, v12

    .line 166
    if-eqz v0, :cond_16

    .line 167
    const/4 v12, 0x1

    const/4 v4, 0x2

    aput-object v8, v11, v12

    .line 169
    :cond_16
    iget-boolean v12, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mHasMask:Z

    if-eqz v12, :cond_17

    .line 170
    aput-object v1, v11, v4

    .line 172
    :cond_17
    iget-object v4, p0, Landroidx/media/filterpacks/composite/OverlayFilter;->mOverlayShader:Lacm;

    invoke-virtual {v4, v11, v10}, Lacm;->a([Laas;Laas;)V

    .line 153
    add-int/lit8 v5, v5, 0x1

    goto :goto_11

    .line 152
    :cond_18
    if-nez v4, :cond_19

    array-length v2, v2

    goto :goto_10

    :cond_19
    array-length v5, v2

    array-length v11, v4

    if-eq v5, v11, :cond_1a

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Mismatch between input source quad count ("

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") and target quad count ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v2, v4

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1a
    array-length v2, v2

    goto :goto_10

    :cond_1b
    const/4 v2, 0x1

    goto :goto_10

    .line 154
    :cond_1c
    const/4 v4, 0x0

    goto :goto_12

    .line 155
    :cond_1d
    const/4 v4, 0x0

    goto :goto_13

    .line 175
    :cond_1e
    iget-object v0, v8, Laap;->a:Lzp;

    invoke-virtual {v0}, Lzp;->g()J

    move-result-wide v0

    invoke-virtual {v10, v0, v1}, Laas;->a(J)V

    .line 176
    invoke-virtual {v7, v10}, Lacv;->a(Laap;)V

    .line 177
    return-void

    :cond_1f
    move-object v6, v4

    goto/16 :goto_f

    .line 116
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch

    :array_0
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method
