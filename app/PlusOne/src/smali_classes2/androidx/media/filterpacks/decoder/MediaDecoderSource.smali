.class public Landroidx/media/filterpacks/decoder/MediaDecoderSource;
.super Laak;
.source "PG"

# interfaces
.implements Ladk;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field private static final INPUT_ROTATION_TYPE:Labf;

.field private static final INPUT_START_TYPE:Labf;

.field private static final INPUT_URI_TYPE:Labf;

.field private static final OUTPUT_AUDIO_TYPE:Labf;

.field private static final OUTPUT_DURATION_TYPE:Labf;

.field private static final OUTPUT_INFO_TYPE:Labf;

.field private static final OUTPUT_VIDEO_TYPE:Labf;

.field private static final STATUS_AUDIO_FRAME:I = 0x1

.field private static final STATUS_NO_FRAME:I = 0x0

.field private static final STATUS_VIDEO_FRAME:I = 0x2


# instance fields
.field private mDurationAvailable:Z

.field private mHasVideoRotation:Z

.field private final mLock:Ljava/lang/Object;

.field private mMediaDecoder:Ladj;

.field private mMediaDecoderException:Ljava/lang/Exception;

.field private mNewAudioFramesAvailable:I

.field private mNewVideoFrameAvailable:Z

.field private mStartMicros:J

.field private mVideoRotation:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 43
    const-class v0, Landroid/net/Uri;

    invoke-static {v0}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v0

    sput-object v0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->INPUT_URI_TYPE:Labf;

    .line 44
    sget-object v0, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v0}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v0

    sput-object v0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->INPUT_ROTATION_TYPE:Labf;

    .line 45
    sget-object v0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-static {v0}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v0

    sput-object v0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->INPUT_START_TYPE:Labf;

    .line 46
    const/16 v0, 0x12d

    const/16 v1, 0x10

    invoke-static {v0, v1}, Labf;->a(II)Labf;

    move-result-object v0

    sput-object v0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->OUTPUT_VIDEO_TYPE:Labf;

    .line 48
    const-class v0, Ladn;

    invoke-static {v0}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v0

    sput-object v0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->OUTPUT_INFO_TYPE:Labf;

    .line 49
    const-class v0, Ladc;

    invoke-static {v0}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v0

    sput-object v0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->OUTPUT_AUDIO_TYPE:Labf;

    .line 50
    sget-object v0, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-static {v0}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v0

    sput-object v0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->OUTPUT_DURATION_TYPE:Labf;

    return-void
.end method

.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 69
    const/4 v0, 0x0

    iput v0, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mVideoRotation:I

    .line 72
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mStartMicros:J

    .line 76
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mLock:Ljava/lang/Object;

    .line 77
    return-void
.end method

.method private y()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 130
    .line 132
    iget-object v1, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 133
    :try_start_0
    iget v2, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mNewAudioFramesAvailable:I

    if-lez v2, :cond_0

    .line 134
    const/4 v0, 0x1

    .line 135
    iget v2, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mNewAudioFramesAvailable:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mNewAudioFramesAvailable:I

    .line 138
    :cond_0
    iget-boolean v2, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mNewVideoFrameAvailable:Z

    if-eqz v2, :cond_1

    .line 146
    iget v2, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mNewAudioFramesAvailable:I

    if-nez v2, :cond_1

    .line 147
    or-int/lit8 v0, v0, 0x2

    .line 148
    const/4 v2, 0x0

    iput-boolean v2, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mNewVideoFrameAvailable:Z

    .line 152
    :cond_1
    if-nez v0, :cond_2

    .line 153
    invoke-virtual {p0}, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->t()V

    .line 155
    :cond_2
    monitor-exit v1

    .line 157
    return v0

    .line 155
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 170
    iget-object v1, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 171
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mNewVideoFrameAvailable:Z

    .line 172
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    invoke-virtual {p0}, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->u()V

    .line 174
    return-void

    .line 172
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 186
    iget-object v1, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 187
    :try_start_0
    iput-object p1, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mMediaDecoderException:Ljava/lang/Exception;

    .line 188
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 189
    invoke-virtual {p0}, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->u()V

    .line 190
    return-void

    .line 188
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 99
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "rotation"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 100
    const-string v0, "mVideoRotation"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 101
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 102
    iput-boolean v2, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mHasVideoRotation:Z

    .line 107
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "start"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    const-string v0, "mStartMicros"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 105
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public b_()V
    .locals 2

    .prologue
    .line 162
    iget-object v1, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 163
    :try_start_0
    iget v0, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mNewAudioFramesAvailable:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mNewAudioFramesAvailable:I

    .line 164
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 165
    invoke-virtual {p0}, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->u()V

    .line 166
    return-void

    .line 164
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public c()Lacx;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 86
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "uri"

    const/4 v2, 0x2

    sget-object v3, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->INPUT_URI_TYPE:Labf;

    invoke-virtual {v0, v1, v2, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "rotation"

    sget-object v2, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->INPUT_ROTATION_TYPE:Labf;

    invoke-virtual {v0, v1, v4, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "start"

    sget-object v2, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->INPUT_START_TYPE:Labf;

    invoke-virtual {v0, v1, v4, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "video"

    sget-object v2, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->OUTPUT_VIDEO_TYPE:Labf;

    invoke-virtual {v0, v1, v4, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "videoInfo"

    sget-object v2, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->OUTPUT_INFO_TYPE:Labf;

    invoke-virtual {v0, v1, v4, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "audio"

    sget-object v2, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->OUTPUT_AUDIO_TYPE:Labf;

    invoke-virtual {v0, v1, v4, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "duration"

    sget-object v2, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->OUTPUT_DURATION_TYPE:Labf;

    invoke-virtual {v0, v1, v4, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method public c_()V
    .locals 2

    .prologue
    .line 178
    iget-object v1, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mLock:Ljava/lang/Object;

    monitor-enter v1

    .line 179
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mDurationAvailable:Z

    .line 180
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    invoke-virtual {p0}, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->u()V

    .line 182
    return-void

    .line 180
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public d_()V
    .locals 0

    .prologue
    .line 194
    invoke-virtual {p0}, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->s()V

    .line 195
    invoke-virtual {p0}, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->u()V

    .line 196
    return-void
.end method

.method protected g()V
    .locals 7

    .prologue
    .line 111
    invoke-super {p0}, Laak;->g()V

    .line 113
    const-string v0, "uri"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->a()Labg;

    move-result-object v0

    invoke-virtual {v0}, Labg;->j()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/net/Uri;

    .line 114
    new-instance v1, Ladj;

    invoke-virtual {p0}, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->d()Lacs;

    move-result-object v0

    invoke-virtual {v0}, Lacs;->b()Landroid/content/Context;

    move-result-object v2

    iget-wide v4, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mStartMicros:J

    move-object v6, p0

    invoke-direct/range {v1 .. v6}, Ladj;-><init>(Landroid/content/Context;Landroid/net/Uri;JLadk;)V

    iput-object v1, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mMediaDecoder:Ladj;

    .line 116
    iget-object v0, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mMediaDecoder:Ladj;

    invoke-virtual {p0}, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->v()Z

    move-result v1

    invoke-virtual {v0, v1}, Ladj;->a(Z)V

    .line 117
    iget-object v0, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mMediaDecoder:Ladj;

    invoke-virtual {v0}, Ladj;->a()V

    .line 118
    return-void
.end method

.method protected i()V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 200
    iget-object v3, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mLock:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v4, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mMediaDecoderException:Ljava/lang/Exception;

    if-eqz v4, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    iget-object v1, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mMediaDecoderException:Ljava/lang/Exception;

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 203
    iget-object v3, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mLock:Ljava/lang/Object;

    monitor-enter v3

    .line 204
    :try_start_2
    iget-boolean v4, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mDurationAvailable:Z

    .line 205
    const/4 v5, 0x0

    iput-boolean v5, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mDurationAvailable:Z

    .line 206
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 208
    if-eqz v4, :cond_1

    .line 209
    const-string v3, "duration"

    invoke-virtual {p0, v3}, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->b(Ljava/lang/String;)Lacv;

    move-result-object v3

    .line 210
    if-eqz v3, :cond_1

    .line 211
    sget-object v4, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->OUTPUT_DURATION_TYPE:Labf;

    new-array v5, v1, [I

    aput v1, v5, v2

    invoke-static {v4, v5}, Laap;->a(Labf;[I)Laap;

    move-result-object v4

    invoke-virtual {v4}, Laap;->a()Labg;

    move-result-object v4

    .line 213
    iget-object v5, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mMediaDecoder:Ladj;

    invoke-virtual {v5}, Ladj;->d()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Labg;->a(Ljava/lang/Object;)V

    .line 214
    invoke-virtual {v3, v4}, Lacv;->a(Laap;)V

    .line 215
    invoke-virtual {v4}, Labg;->f()Laap;

    .line 216
    invoke-virtual {v3, v2}, Lacv;->a(Z)V

    .line 220
    :cond_1
    invoke-direct {p0}, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->y()I

    move-result v4

    .line 221
    and-int/lit8 v3, v4, 0x2

    if-eqz v3, :cond_4

    .line 222
    const-string v3, "video"

    invoke-virtual {p0, v3}, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->b(Ljava/lang/String;)Lacv;

    move-result-object v5

    .line 223
    const-string v3, "videoInfo"

    invoke-virtual {p0, v3}, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->b(Ljava/lang/String;)Lacv;

    move-result-object v6

    .line 226
    if-eqz v5, :cond_9

    .line 227
    sget-object v3, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->OUTPUT_VIDEO_TYPE:Labf;

    const/4 v7, 0x2

    new-array v7, v7, [I

    fill-array-data v7, :array_0

    invoke-static {v3, v7}, Laap;->a(Labf;[I)Laap;

    move-result-object v3

    invoke-virtual {v3}, Laap;->e()Laas;

    move-result-object v3

    .line 229
    :goto_0
    if-eqz v6, :cond_2

    .line 230
    sget-object v7, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->OUTPUT_INFO_TYPE:Labf;

    invoke-static {v7, v0}, Laap;->a(Labf;[I)Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->a()Labg;

    move-result-object v0

    .line 233
    :cond_2
    iget-boolean v7, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mHasVideoRotation:Z

    if-eqz v7, :cond_6

    .line 234
    iget-object v7, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mMediaDecoder:Ladj;

    iget v8, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mVideoRotation:I

    invoke-virtual {v7, v3, v0, v8}, Ladj;->a(Laas;Labg;I)V

    .line 239
    :goto_1
    if-eqz v3, :cond_3

    .line 240
    invoke-virtual {v5, v3}, Lacv;->a(Laap;)V

    .line 241
    invoke-virtual {v3}, Laas;->f()Laap;

    .line 244
    :cond_3
    if-eqz v0, :cond_4

    .line 245
    invoke-virtual {v6, v0}, Lacv;->a(Laap;)V

    .line 246
    invoke-virtual {v0}, Labg;->f()Laap;

    .line 251
    :cond_4
    and-int/lit8 v0, v4, 0x1

    if-eqz v0, :cond_5

    .line 252
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->b(Ljava/lang/String;)Lacv;

    move-result-object v3

    .line 253
    if-eqz v3, :cond_7

    invoke-virtual {v3}, Lacv;->d()Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->g()Laak;

    move-result-object v0

    invoke-virtual {v0}, Laak;->e()Z

    move-result v0

    if-eqz v0, :cond_7

    move v0, v1

    .line 254
    :goto_2
    if-eqz v0, :cond_8

    .line 255
    sget-object v0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->OUTPUT_AUDIO_TYPE:Labf;

    new-array v4, v1, [I

    aput v1, v4, v2

    invoke-static {v0, v4}, Laap;->a(Labf;[I)Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->a()Labg;

    move-result-object v0

    .line 257
    iget-object v1, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mMediaDecoder:Ladj;

    invoke-virtual {v1, v0}, Ladj;->a(Labg;)V

    .line 258
    invoke-virtual {v3, v0}, Lacv;->a(Laap;)V

    .line 259
    invoke-virtual {v0}, Labg;->f()Laap;

    .line 265
    :cond_5
    :goto_3
    return-void

    .line 206
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0

    .line 236
    :cond_6
    iget-object v7, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mMediaDecoder:Ladj;

    invoke-virtual {v7, v3, v0}, Ladj;->a(Laas;Labg;)V

    goto :goto_1

    :cond_7
    move v0, v2

    .line 253
    goto :goto_2

    .line 261
    :cond_8
    iget-object v0, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mMediaDecoder:Ladj;

    invoke-virtual {v0}, Ladj;->c()V

    goto :goto_3

    :cond_9
    move-object v3, v0

    goto :goto_0

    .line 227
    :array_0
    .array-data 4
        0x1
        0x1
    .end array-data
.end method

.method protected k()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mMediaDecoder:Ladj;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mMediaDecoder:Ladj;

    invoke-virtual {v0}, Ladj;->b()V

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/decoder/MediaDecoderSource;->mMediaDecoder:Ladj;

    .line 126
    :cond_0
    invoke-super {p0}, Laak;->k()V

    .line 127
    return-void
.end method

.method public p()I
    .locals 1

    .prologue
    .line 81
    const/16 v0, 0x19

    return v0
.end method
