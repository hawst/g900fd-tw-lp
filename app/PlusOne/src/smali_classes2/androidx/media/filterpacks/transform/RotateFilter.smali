.class public Landroidx/media/filterpacks/transform/RotateFilter;
.super Laak;
.source "PG"


# instance fields
.field private mRotateAngle:F

.field private mShader:Lacm;

.field private mSourceRect:Ladp;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 36
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 31
    invoke-static {v1, v1, v0, v0}, Ladp;->a(FFFF)Ladp;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/transform/RotateFilter;->mSourceRect:Ladp;

    .line 32
    iput v1, p0, Landroidx/media/filterpacks/transform/RotateFilter;->mRotateAngle:F

    .line 37
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 53
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "rotateAngle"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    const-string v0, "mRotateAngle"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 55
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 60
    :cond_0
    :goto_0
    return-void

    .line 56
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "sourceRect"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    const-string v0, "mSourceRect"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 58
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x2

    .line 41
    invoke-static {v2, v5}, Labf;->a(II)Labf;

    move-result-object v0

    .line 42
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 43
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "rotateAngle"

    sget-object v3, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v5, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "sourceRect"

    const/4 v3, 0x1

    const-class v4, Ladp;

    invoke-static {v4}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v4

    invoke-virtual {v0, v2, v3, v4}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 64
    invoke-static {}, Lacm;->a()Lacm;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/transform/RotateFilter;->mShader:Lacm;

    .line 65
    return-void
.end method

.method protected i()V
    .locals 8

    .prologue
    .line 69
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/transform/RotateFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 70
    const-string v1, "image"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/transform/RotateFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v1

    .line 71
    invoke-virtual {v1}, Laas;->i()[I

    move-result-object v2

    .line 73
    invoke-virtual {v0, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->e()Laas;

    move-result-object v2

    .line 74
    iget-object v3, p0, Landroidx/media/filterpacks/transform/RotateFilter;->mShader:Lacm;

    iget-object v4, p0, Landroidx/media/filterpacks/transform/RotateFilter;->mSourceRect:Ladp;

    invoke-virtual {v3, v4}, Lacm;->a(Ladp;)V

    .line 75
    iget-object v3, p0, Landroidx/media/filterpacks/transform/RotateFilter;->mSourceRect:Ladp;

    iget v4, p0, Landroidx/media/filterpacks/transform/RotateFilter;->mRotateAngle:F

    const/high16 v5, 0x43340000    # 180.0f

    div-float/2addr v4, v5

    float-to-double v4, v4

    const-wide v6, 0x400921fb54442d18L    # Math.PI

    mul-double/2addr v4, v6

    double-to-float v4, v4

    invoke-virtual {v3, v4}, Ladp;->a(F)Ladp;

    move-result-object v3

    .line 76
    iget-object v4, p0, Landroidx/media/filterpacks/transform/RotateFilter;->mShader:Lacm;

    invoke-virtual {v4, v3}, Lacm;->b(Ladp;)V

    .line 77
    iget-object v3, p0, Landroidx/media/filterpacks/transform/RotateFilter;->mShader:Lacm;

    invoke-virtual {v3, v1, v2}, Lacm;->a(Laas;Laas;)V

    .line 78
    invoke-virtual {v0, v2}, Lacv;->a(Laap;)V

    .line 79
    return-void
.end method
