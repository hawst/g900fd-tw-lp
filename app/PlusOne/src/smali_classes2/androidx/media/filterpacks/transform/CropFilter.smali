.class public Landroidx/media/filterpacks/transform/CropFilter;
.super Laak;
.source "PG"


# instance fields
.field private mCropRect:Ladp;

.field private mOutputHeight:I

.field private mOutputWidth:I

.field private mPow2Frame:Laas;

.field private mShader:Lacm;

.field private mUseMipmaps:Z


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 45
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 37
    invoke-static {v0, v0, v2, v2}, Ladp;->a(FFFF)Ladp;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/transform/CropFilter;->mCropRect:Ladp;

    .line 38
    iput v1, p0, Landroidx/media/filterpacks/transform/CropFilter;->mOutputWidth:I

    .line 39
    iput v1, p0, Landroidx/media/filterpacks/transform/CropFilter;->mOutputHeight:I

    .line 41
    iput-boolean v1, p0, Landroidx/media/filterpacks/transform/CropFilter;->mUseMipmaps:Z

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/transform/CropFilter;->mPow2Frame:Laas;

    .line 46
    return-void
.end method


# virtual methods
.method protected a(II)I
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Landroidx/media/filterpacks/transform/CropFilter;->mOutputWidth:I

    if-gtz v0, :cond_0

    :goto_0
    return p1

    :cond_0
    iget p1, p0, Landroidx/media/filterpacks/transform/CropFilter;->mOutputWidth:I

    goto :goto_0
.end method

.method protected b(II)I
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Landroidx/media/filterpacks/transform/CropFilter;->mOutputHeight:I

    if-gtz v0, :cond_0

    :goto_0
    return p2

    :cond_0
    iget p2, p0, Landroidx/media/filterpacks/transform/CropFilter;->mOutputHeight:I

    goto :goto_0
.end method

.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 64
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "cropRect"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    const-string v0, "mCropRect"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "outputWidth"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 68
    const-string v0, "mOutputWidth"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 69
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 70
    :cond_2
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "outputHeight"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 71
    const-string v0, "mOutputHeight"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 73
    :cond_3
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "useMipmaps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    const-string v0, "mUseMipmaps"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 75
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 50
    invoke-static {v2, v4}, Labf;->a(II)Labf;

    move-result-object v0

    .line 51
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 52
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "cropRect"

    const-class v3, Ladp;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "outputWidth"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v5, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "outputHeight"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v5, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "useMipmaps"

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v5, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Landroidx/media/filterpacks/transform/CropFilter;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    invoke-static {}, Lacm;->a()Lacm;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/transform/CropFilter;->mShader:Lacm;

    .line 84
    :cond_0
    return-void
.end method

.method protected i()V
    .locals 11

    .prologue
    const/4 v8, 0x2

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 88
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/transform/CropFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v4

    .line 91
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/transform/CropFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->e()Laas;

    move-result-object v1

    .line 92
    invoke-virtual {v1}, Laas;->i()[I

    move-result-object v0

    .line 93
    new-array v5, v8, [I

    iget-object v6, p0, Landroidx/media/filterpacks/transform/CropFilter;->mCropRect:Ladp;

    invoke-virtual {v6}, Ladp;->g()Landroid/graphics/PointF;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/PointF;->length()F

    move-result v6

    aget v7, v0, v2

    int-to-float v7, v7

    mul-float/2addr v6, v7

    invoke-static {v6}, Landroid/util/FloatMath;->ceil(F)F

    move-result v6

    float-to-int v6, v6

    aput v6, v5, v2

    iget-object v6, p0, Landroidx/media/filterpacks/transform/CropFilter;->mCropRect:Ladp;

    invoke-virtual {v6}, Ladp;->h()Landroid/graphics/PointF;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/PointF;->length()F

    move-result v6

    aget v7, v0, v3

    int-to-float v7, v7

    mul-float/2addr v6, v7

    invoke-static {v6}, Landroid/util/FloatMath;->ceil(F)F

    move-result v6

    float-to-int v6, v6

    aput v6, v5, v3

    .line 95
    new-array v6, v8, [I

    aget v7, v5, v2

    aget v8, v5, v3

    invoke-virtual {p0, v7, v8}, Landroidx/media/filterpacks/transform/CropFilter;->a(II)I

    move-result v7

    aput v7, v6, v2

    aget v7, v5, v2

    aget v8, v5, v3

    invoke-virtual {p0, v7, v8}, Landroidx/media/filterpacks/transform/CropFilter;->b(II)I

    move-result v7

    aput v7, v6, v3

    .line 97
    invoke-virtual {v4, v6}, Lacv;->a([I)Laap;

    move-result-object v7

    invoke-virtual {v7}, Laap;->e()Laas;

    move-result-object v7

    .line 99
    invoke-virtual {p0}, Landroidx/media/filterpacks/transform/CropFilter;->v()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 101
    aget v0, v6, v2

    aget v8, v5, v2

    if-lt v0, v8, :cond_0

    aget v0, v6, v3

    aget v6, v5, v3

    if-ge v0, v6, :cond_1

    :cond_0
    move v0, v3

    .line 103
    :goto_0
    if-eqz v0, :cond_2

    iget-boolean v0, p0, Landroidx/media/filterpacks/transform/CropFilter;->mUseMipmaps:Z

    if-eqz v0, :cond_2

    .line 104
    iget-object v0, p0, Landroidx/media/filterpacks/transform/CropFilter;->mPow2Frame:Laas;

    invoke-static {v0, v5}, Laei;->a(Laas;[I)Laas;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/transform/CropFilter;->mPow2Frame:Laas;

    .line 105
    iget-object v0, p0, Landroidx/media/filterpacks/transform/CropFilter;->mPow2Frame:Laas;

    invoke-virtual {v0}, Laas;->i()[I

    move-result-object v0

    .line 106
    aget v6, v5, v2

    int-to-float v6, v6

    aget v2, v0, v2

    int-to-float v2, v2

    div-float v2, v6, v2

    .line 107
    aget v5, v5, v3

    int-to-float v5, v5

    aget v0, v0, v3

    int-to-float v0, v0

    div-float v0, v5, v0

    .line 108
    invoke-static {v10, v10, v2, v0}, Ladp;->a(FFFF)Ladp;

    move-result-object v0

    .line 109
    iget-object v2, p0, Landroidx/media/filterpacks/transform/CropFilter;->mShader:Lacm;

    iget-object v3, p0, Landroidx/media/filterpacks/transform/CropFilter;->mCropRect:Ladp;

    invoke-virtual {v2, v3}, Lacm;->a(Ladp;)V

    .line 110
    iget-object v2, p0, Landroidx/media/filterpacks/transform/CropFilter;->mShader:Lacm;

    invoke-virtual {v2, v0}, Lacm;->b(Ladp;)V

    .line 111
    iget-object v2, p0, Landroidx/media/filterpacks/transform/CropFilter;->mShader:Lacm;

    iget-object v3, p0, Landroidx/media/filterpacks/transform/CropFilter;->mPow2Frame:Laas;

    invoke-virtual {v2, v1, v3}, Lacm;->a(Laas;Laas;)V

    .line 112
    iget-object v1, p0, Landroidx/media/filterpacks/transform/CropFilter;->mPow2Frame:Laas;

    invoke-static {v1}, Laei;->a(Laas;)V

    .line 113
    iget-object v1, p0, Landroidx/media/filterpacks/transform/CropFilter;->mPow2Frame:Laas;

    .line 120
    :goto_1
    iget-object v2, p0, Landroidx/media/filterpacks/transform/CropFilter;->mShader:Lacm;

    invoke-virtual {v2, v0}, Lacm;->a(Ladp;)V

    .line 121
    iget-object v0, p0, Landroidx/media/filterpacks/transform/CropFilter;->mShader:Lacm;

    invoke-virtual {v0, v10, v10, v9, v9}, Lacm;->b(FFFF)V

    .line 122
    iget-object v0, p0, Landroidx/media/filterpacks/transform/CropFilter;->mShader:Lacm;

    invoke-virtual {v0, v1, v7}, Lacm;->a(Laas;Laas;)V

    .line 147
    :goto_2
    invoke-virtual {v4, v7}, Lacv;->a(Laap;)V

    .line 148
    return-void

    :cond_1
    move v0, v2

    .line 101
    goto :goto_0

    .line 117
    :cond_2
    iget-object v0, p0, Landroidx/media/filterpacks/transform/CropFilter;->mCropRect:Ladp;

    goto :goto_1

    .line 125
    :cond_3
    iget-object v5, p0, Landroidx/media/filterpacks/transform/CropFilter;->mCropRect:Ladp;

    aget v8, v0, v2

    int-to-float v8, v8

    aget v9, v0, v3

    int-to-float v9, v9

    invoke-virtual {v5, v8, v9}, Ladp;->a(FF)Ladp;

    move-result-object v5

    .line 126
    aget v8, v0, v2

    int-to-float v8, v8

    aget v9, v0, v3

    int-to-float v9, v9

    invoke-static {v10, v10, v8, v9}, Ladp;->a(FFFF)Ladp;

    move-result-object v8

    .line 129
    invoke-static {v5, v8}, Ladp;->a(Ladp;Ladp;)Landroid/graphics/Matrix;

    move-result-object v5

    .line 130
    aget v8, v6, v2

    int-to-float v8, v8

    aget v9, v0, v2

    int-to-float v9, v9

    div-float/2addr v8, v9

    aget v9, v6, v3

    int-to-float v9, v9

    aget v0, v0, v3

    int-to-float v0, v0

    div-float v0, v9, v0

    invoke-virtual {v5, v8, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 133
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 134
    aget v2, v6, v2

    aget v6, v6, v3

    invoke-static {v2, v6, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 135
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 138
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 139
    invoke-virtual {v6, v3}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 140
    invoke-virtual {v1}, Laas;->n()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 141
    invoke-virtual {v2, v1, v5, v6}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 144
    invoke-virtual {v7, v0}, Laas;->a(Landroid/graphics/Bitmap;)V

    goto :goto_2
.end method

.method protected j()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Landroidx/media/filterpacks/transform/CropFilter;->mPow2Frame:Laas;

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Landroidx/media/filterpacks/transform/CropFilter;->mPow2Frame:Laas;

    invoke-virtual {v0}, Laas;->f()Laap;

    .line 154
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/transform/CropFilter;->mPow2Frame:Laas;

    .line 156
    :cond_0
    return-void
.end method
