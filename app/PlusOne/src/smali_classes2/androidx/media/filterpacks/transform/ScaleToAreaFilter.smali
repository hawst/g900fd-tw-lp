.class public Landroidx/media/filterpacks/transform/ScaleToAreaFilter;
.super Landroidx/media/filterpacks/transform/ResizeFilter;
.source "PG"


# instance fields
.field private mHeightMultiple:I

.field private mTargetArea:I

.field private mWidthMultiple:I


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 35
    invoke-direct {p0, p1, p2}, Landroidx/media/filterpacks/transform/ResizeFilter;-><init>(Lacs;Ljava/lang/String;)V

    .line 30
    const v0, 0x12c00

    iput v0, p0, Landroidx/media/filterpacks/transform/ScaleToAreaFilter;->mTargetArea:I

    .line 31
    iput v1, p0, Landroidx/media/filterpacks/transform/ScaleToAreaFilter;->mWidthMultiple:I

    .line 32
    iput v1, p0, Landroidx/media/filterpacks/transform/ScaleToAreaFilter;->mHeightMultiple:I

    .line 36
    return-void
.end method

.method private c(II)F
    .locals 2

    .prologue
    .line 73
    iget v0, p0, Landroidx/media/filterpacks/transform/ScaleToAreaFilter;->mTargetArea:I

    int-to-float v0, v0

    mul-int v1, p1, p2

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method


# virtual methods
.method protected a(II)I
    .locals 3

    .prologue
    .line 83
    invoke-direct {p0, p1, p2}, Landroidx/media/filterpacks/transform/ScaleToAreaFilter;->c(II)F

    move-result v0

    .line 84
    int-to-float v1, p1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 85
    iget v1, p0, Landroidx/media/filterpacks/transform/ScaleToAreaFilter;->mWidthMultiple:I

    iget v2, p0, Landroidx/media/filterpacks/transform/ScaleToAreaFilter;->mWidthMultiple:I

    rem-int v2, v0, v2

    sub-int/2addr v1, v2

    iget v2, p0, Landroidx/media/filterpacks/transform/ScaleToAreaFilter;->mWidthMultiple:I

    rem-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method protected b(II)I
    .locals 3

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Landroidx/media/filterpacks/transform/ScaleToAreaFilter;->c(II)F

    move-result v0

    .line 96
    int-to-float v1, p2

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 97
    iget v1, p0, Landroidx/media/filterpacks/transform/ScaleToAreaFilter;->mHeightMultiple:I

    iget v2, p0, Landroidx/media/filterpacks/transform/ScaleToAreaFilter;->mHeightMultiple:I

    rem-int v2, v0, v2

    sub-int/2addr v1, v2

    iget v2, p0, Landroidx/media/filterpacks/transform/ScaleToAreaFilter;->mHeightMultiple:I

    rem-int/2addr v1, v2

    add-int/2addr v0, v1

    return v0
.end method

.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 54
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "targetArea"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    const-string v0, "mTargetArea"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 56
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 57
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "useMipmaps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 58
    const-string v0, "mUseMipmaps"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 59
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 60
    :cond_2
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "widthMultiple"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 61
    const-string v0, "mWidthMultiple"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 62
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 63
    :cond_3
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "heightMultiple"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "mHeightMultiple"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 65
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 40
    invoke-static {v2, v5}, Labf;->a(II)Labf;

    move-result-object v0

    .line 41
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 42
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "targetArea"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "widthMultiple"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "heightMultiple"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "useMipmaps"

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method
