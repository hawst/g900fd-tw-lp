.class public Landroidx/media/filterpacks/transform/GridFilter;
.super Laak;
.source "PG"


# instance fields
.field private mOutputHeight:I

.field private mOutputWidth:I

.field private mShader:Lacm;

.field private mTileFrame:Laas;

.field private mUseMipmaps:Z

.field private mXCount:I

.field private mYCount:I


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 42
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 32
    iput v0, p0, Landroidx/media/filterpacks/transform/GridFilter;->mXCount:I

    .line 33
    iput v0, p0, Landroidx/media/filterpacks/transform/GridFilter;->mYCount:I

    .line 34
    iput v1, p0, Landroidx/media/filterpacks/transform/GridFilter;->mOutputWidth:I

    .line 35
    iput v1, p0, Landroidx/media/filterpacks/transform/GridFilter;->mOutputHeight:I

    .line 36
    iput-boolean v0, p0, Landroidx/media/filterpacks/transform/GridFilter;->mUseMipmaps:Z

    .line 39
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/transform/GridFilter;->mTileFrame:Laas;

    .line 43
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 62
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "xCount"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 63
    const-string v0, "mXCount"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 64
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 78
    :cond_0
    :goto_0
    return-void

    .line 65
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "yCount"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 66
    const-string v0, "mYCount"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 67
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 68
    :cond_2
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "useMipmaps"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 69
    const-string v0, "mUseMipmaps"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 70
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 71
    :cond_3
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "outputWidth"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 72
    const-string v0, "mOutputWidth"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 73
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0

    .line 74
    :cond_4
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "outputHeight"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 75
    const-string v0, "mOutputHeight"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 6

    .prologue
    const/16 v2, 0x12d

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 47
    invoke-static {v2, v5}, Labf;->a(II)Labf;

    move-result-object v0

    .line 48
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 49
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v5, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "xCount"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "yCount"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "useMipmaps"

    sget-object v3, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "outputWidth"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "outputHeight"

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v5, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected g()V
    .locals 1

    .prologue
    .line 82
    invoke-static {}, Lacm;->a()Lacm;

    move-result-object v0

    iput-object v0, p0, Landroidx/media/filterpacks/transform/GridFilter;->mShader:Lacm;

    .line 83
    return-void
.end method

.method protected i()V
    .locals 10

    .prologue
    const/16 v9, 0x2901

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 88
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/transform/GridFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 89
    const-string v1, "image"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/transform/GridFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v1

    .line 90
    invoke-virtual {v1}, Laas;->i()[I

    move-result-object v2

    .line 93
    iget-object v3, p0, Landroidx/media/filterpacks/transform/GridFilter;->mTileFrame:Laas;

    invoke-static {v3, v2}, Laei;->a(Laas;[I)Laas;

    move-result-object v3

    iput-object v3, p0, Landroidx/media/filterpacks/transform/GridFilter;->mTileFrame:Laas;

    .line 94
    iget-object v3, p0, Landroidx/media/filterpacks/transform/GridFilter;->mTileFrame:Laas;

    const/16 v4, 0x2802

    invoke-static {v3, v4, v9}, Laei;->a(Laas;II)V

    .line 95
    iget-object v3, p0, Landroidx/media/filterpacks/transform/GridFilter;->mTileFrame:Laas;

    const/16 v4, 0x2803

    invoke-static {v3, v4, v9}, Laei;->a(Laas;II)V

    .line 98
    iget-object v3, p0, Landroidx/media/filterpacks/transform/GridFilter;->mShader:Lacm;

    invoke-virtual {v3, v5, v5, v8, v8}, Lacm;->a(FFFF)V

    .line 99
    iget-object v3, p0, Landroidx/media/filterpacks/transform/GridFilter;->mShader:Lacm;

    iget-object v4, p0, Landroidx/media/filterpacks/transform/GridFilter;->mTileFrame:Laas;

    invoke-virtual {v3, v1, v4}, Lacm;->a(Laas;Laas;)V

    .line 102
    iget-boolean v1, p0, Landroidx/media/filterpacks/transform/GridFilter;->mUseMipmaps:Z

    if-eqz v1, :cond_0

    .line 103
    iget-object v1, p0, Landroidx/media/filterpacks/transform/GridFilter;->mTileFrame:Laas;

    invoke-static {v1}, Laei;->a(Laas;)V

    .line 107
    :cond_0
    const/4 v1, 0x2

    new-array v1, v1, [I

    iget v3, p0, Landroidx/media/filterpacks/transform/GridFilter;->mOutputWidth:I

    aput v3, v1, v6

    iget v3, p0, Landroidx/media/filterpacks/transform/GridFilter;->mOutputHeight:I

    aput v3, v1, v7

    .line 108
    aget v3, v1, v6

    if-gtz v3, :cond_1

    aget v3, v2, v6

    iget v4, p0, Landroidx/media/filterpacks/transform/GridFilter;->mXCount:I

    mul-int/2addr v3, v4

    aput v3, v1, v6

    .line 109
    :cond_1
    aget v3, v1, v7

    if-gtz v3, :cond_2

    aget v2, v2, v7

    iget v3, p0, Landroidx/media/filterpacks/transform/GridFilter;->mYCount:I

    mul-int/2addr v2, v3

    aput v2, v1, v7

    .line 110
    :cond_2
    invoke-virtual {v0, v1}, Lacv;->a([I)Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v1

    .line 113
    iget-object v2, p0, Landroidx/media/filterpacks/transform/GridFilter;->mShader:Lacm;

    iget v3, p0, Landroidx/media/filterpacks/transform/GridFilter;->mXCount:I

    int-to-float v3, v3

    iget v4, p0, Landroidx/media/filterpacks/transform/GridFilter;->mYCount:I

    int-to-float v4, v4

    invoke-virtual {v2, v5, v5, v3, v4}, Lacm;->a(FFFF)V

    .line 114
    iget-object v2, p0, Landroidx/media/filterpacks/transform/GridFilter;->mShader:Lacm;

    iget-object v3, p0, Landroidx/media/filterpacks/transform/GridFilter;->mTileFrame:Laas;

    invoke-virtual {v2, v3, v1}, Lacm;->a(Laas;Laas;)V

    .line 117
    invoke-virtual {v0, v1}, Lacv;->a(Laap;)V

    .line 118
    return-void
.end method

.method protected j()V
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Landroidx/media/filterpacks/transform/GridFilter;->mTileFrame:Laas;

    if-eqz v0, :cond_0

    .line 123
    iget-object v0, p0, Landroidx/media/filterpacks/transform/GridFilter;->mTileFrame:Laas;

    invoke-virtual {v0}, Laas;->f()Laap;

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/transform/GridFilter;->mTileFrame:Laas;

    .line 126
    :cond_0
    return-void
.end method
