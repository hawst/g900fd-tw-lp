.class public Landroidx/media/filterpacks/text/TextViewTarget;
.super Landroidx/media/filterfw/ViewFilter;
.source "PG"


# instance fields
.field private mTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroidx/media/filterfw/ViewFilter;-><init>(Lacs;Ljava/lang/String;)V

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterpacks/text/TextViewTarget;->mTextView:Landroid/widget/TextView;

    .line 34
    return-void
.end method

.method public static synthetic a(Landroidx/media/filterpacks/text/TextViewTarget;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Landroidx/media/filterpacks/text/TextViewTarget;->mTextView:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public c()Lacx;
    .locals 4

    .prologue
    .line 47
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "text"

    const/4 v2, 0x2

    const-class v3, Ljava/lang/String;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 3

    .prologue
    .line 54
    const-string v0, "text"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/text/TextViewTarget;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->a()Labg;

    move-result-object v0

    .line 55
    invoke-virtual {v0}, Labg;->j()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 56
    iget-object v1, p0, Landroidx/media/filterpacks/text/TextViewTarget;->mTextView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 57
    iget-object v1, p0, Landroidx/media/filterpacks/text/TextViewTarget;->mTextView:Landroid/widget/TextView;

    new-instance v2, Laeh;

    invoke-direct {v2, p0, v0}, Laeh;-><init>(Landroidx/media/filterpacks/text/TextViewTarget;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->post(Ljava/lang/Runnable;)Z

    .line 64
    :cond_0
    return-void
.end method
