.class public final Landroidx/media/filterpacks/colorspace/ColorfulnessFilter;
.super Laak;
.source "PG"


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 43
    return-void
.end method


# virtual methods
.method public c()Lacx;
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 47
    const/16 v0, 0xc8

    invoke-static {v0}, Labf;->b(I)Labf;

    move-result-object v0

    .line 48
    new-instance v1, Lacx;

    invoke-direct {v1}, Lacx;-><init>()V

    const-string v2, "histogram"

    invoke-virtual {v1, v2, v3, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "score"

    sget-object v2, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 13

    .prologue
    .line 56
    const-string v0, "histogram"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/colorspace/ColorfulnessFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    invoke-virtual {v0}, Laap;->d()Laar;

    move-result-object v4

    .line 58
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Laar;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 59
    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 60
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v5

    .line 61
    invoke-virtual {v5}, Ljava/nio/FloatBuffer;->rewind()Ljava/nio/Buffer;

    .line 64
    invoke-virtual {v4}, Laar;->j()I

    move-result v6

    .line 65
    invoke-virtual {v4}, Laar;->k()I

    move-result v0

    add-int/lit8 v7, v0, -0x1

    .line 66
    new-array v8, v6, [F

    .line 67
    const/4 v2, 0x0

    .line 68
    const/4 v0, 0x0

    move v3, v0

    :goto_0
    if-ge v3, v7, :cond_1

    .line 69
    const-wide/high16 v0, 0x4000000000000000L    # 2.0

    int-to-double v10, v3

    invoke-static {v0, v1, v10, v11}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v0

    double-to-float v9, v0

    .line 70
    const/4 v0, 0x0

    move v1, v2

    :goto_1
    if-ge v0, v6, :cond_0

    .line 71
    invoke-virtual {v5}, Ljava/nio/FloatBuffer;->get()F

    move-result v2

    mul-float/2addr v2, v9

    .line 72
    aget v10, v8, v0

    add-float/2addr v10, v2

    aput v10, v8, v0

    .line 73
    add-float/2addr v1, v2

    .line 70
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 68
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v2, v1

    goto :goto_0

    .line 77
    :cond_1
    const/4 v1, 0x0

    .line 78
    const/4 v0, 0x0

    move v12, v0

    move v0, v1

    move v1, v12

    :goto_2
    if-ge v1, v6, :cond_3

    .line 79
    aget v3, v8, v1

    div-float/2addr v3, v2

    .line 80
    const/4 v5, 0x0

    cmpl-float v5, v3, v5

    if-lez v5, :cond_2

    .line 81
    float-to-double v10, v3

    invoke-static {v10, v11}, Ljava/lang/Math;->log(D)D

    move-result-wide v10

    double-to-float v5, v10

    mul-float/2addr v3, v5

    sub-float/2addr v0, v3

    .line 78
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 84
    :cond_3
    float-to-double v0, v0

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    div-double/2addr v0, v2

    double-to-float v0, v0

    .line 86
    invoke-virtual {v4}, Laar;->h()V

    .line 87
    const-string v1, "score"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/colorspace/ColorfulnessFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    .line 88
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lacv;->a([I)Laap;

    move-result-object v2

    invoke-virtual {v2}, Laap;->a()Labg;

    move-result-object v2

    .line 89
    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v2, v0}, Labg;->a(Ljava/lang/Object;)V

    .line 90
    invoke-virtual {v1, v2}, Lacv;->a(Laap;)V

    .line 91
    return-void
.end method
