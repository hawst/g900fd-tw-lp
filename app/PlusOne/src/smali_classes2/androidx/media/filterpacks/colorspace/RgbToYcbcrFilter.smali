.class public Landroidx/media/filterpacks/colorspace/RgbToYcbcrFilter;
.super Laak;
.source "PG"


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 30
    return-void
.end method


# virtual methods
.method public c()Lacx;
    .locals 5

    .prologue
    const/16 v2, 0x12d

    const/4 v4, 0x2

    .line 34
    invoke-static {v2, v4}, Labf;->a(II)Labf;

    move-result-object v0

    .line 35
    const/16 v1, 0x10

    invoke-static {v2, v1}, Labf;->a(II)Labf;

    move-result-object v1

    .line 36
    new-instance v2, Lacx;

    invoke-direct {v2}, Lacx;-><init>()V

    const-string v3, "image"

    invoke-virtual {v2, v3, v4, v0}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v2, "image"

    invoke-virtual {v0, v2, v4, v1}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected i()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 44
    const-string v0, "image"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/colorspace/RgbToYcbcrFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    .line 45
    const-string v1, "image"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/colorspace/RgbToYcbcrFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v1

    invoke-virtual {v1}, Lacp;->c()Laap;

    move-result-object v1

    invoke-virtual {v1}, Laap;->e()Laas;

    move-result-object v1

    .line 46
    invoke-virtual {v1}, Laas;->i()[I

    move-result-object v2

    .line 47
    invoke-virtual {v0, v2}, Lacv;->a([I)Laap;

    move-result-object v3

    invoke-virtual {v3}, Laap;->e()Laas;

    move-result-object v3

    .line 49
    invoke-virtual {v1, v7}, Laas;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 50
    invoke-virtual {v3, v7}, Laas;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 52
    const/4 v6, 0x0

    aget v6, v2, v6

    aget v2, v2, v7

    invoke-static {v4, v5, v6, v2}, Landroidx/media/filterfw/ColorSpace;->b(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;II)V

    .line 54
    invoke-virtual {v1}, Laas;->h()V

    .line 55
    invoke-virtual {v3}, Laas;->h()V

    .line 57
    invoke-virtual {v0, v3}, Lacv;->a(Laap;)V

    .line 58
    return-void
.end method
