.class public Landroidx/media/filterpacks/performance/ThroughputFilter;
.super Laak;
.source "PG"


# instance fields
.field private mLastTime:J

.field private mPeriod:I

.field private mPeriodFrameCount:I

.field private mTotalFrameCount:I


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 33
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 27
    const/4 v0, 0x3

    iput v0, p0, Landroidx/media/filterpacks/performance/ThroughputFilter;->mPeriod:I

    .line 28
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroidx/media/filterpacks/performance/ThroughputFilter;->mLastTime:J

    .line 29
    iput v2, p0, Landroidx/media/filterpacks/performance/ThroughputFilter;->mTotalFrameCount:I

    .line 30
    iput v2, p0, Landroidx/media/filterpacks/performance/ThroughputFilter;->mPeriodFrameCount:I

    .line 34
    return-void
.end method


# virtual methods
.method public b(Lacp;)V
    .locals 2

    .prologue
    .line 50
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "period"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 51
    const-string v0, "mPeriod"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 55
    :goto_0
    return-void

    .line 53
    :cond_0
    const-string v0, "frame"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/performance/ThroughputFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    invoke-virtual {p1, v0}, Lacp;->a(Lacv;)V

    goto :goto_0
.end method

.method public c()Lacx;
    .locals 5

    .prologue
    const/4 v4, 0x2

    .line 39
    const-class v0, Laed;

    invoke-static {v0}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v0

    .line 40
    new-instance v1, Lacx;

    invoke-direct {v1}, Lacx;-><init>()V

    const-string v2, "frame"

    invoke-static {}, Labf;->a()Labf;

    move-result-object v3

    invoke-virtual {v1, v2, v4, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v1

    const-string v2, "throughput"

    invoke-virtual {v1, v2, v4, v0}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "frame"

    invoke-static {}, Labf;->a()Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v4, v2}, Lacx;->b(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "period"

    const/4 v2, 0x1

    sget-object v3, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v3}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    invoke-virtual {v0}, Lacx;->c()Lacx;

    move-result-object v0

    return-object v0
.end method

.method protected h()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 59
    iput v0, p0, Landroidx/media/filterpacks/performance/ThroughputFilter;->mTotalFrameCount:I

    .line 60
    iput v0, p0, Landroidx/media/filterpacks/performance/ThroughputFilter;->mPeriodFrameCount:I

    .line 61
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Landroidx/media/filterpacks/performance/ThroughputFilter;->mLastTime:J

    .line 62
    return-void
.end method

.method protected declared-synchronized i()V
    .locals 9

    .prologue
    .line 66
    monitor-enter p0

    :try_start_0
    const-string v0, "frame"

    invoke-virtual {p0, v0}, Landroidx/media/filterpacks/performance/ThroughputFilter;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    invoke-virtual {v0}, Lacp;->c()Laap;

    move-result-object v0

    .line 69
    iget v1, p0, Landroidx/media/filterpacks/performance/ThroughputFilter;->mTotalFrameCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Landroidx/media/filterpacks/performance/ThroughputFilter;->mTotalFrameCount:I

    .line 70
    iget v1, p0, Landroidx/media/filterpacks/performance/ThroughputFilter;->mPeriodFrameCount:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Landroidx/media/filterpacks/performance/ThroughputFilter;->mPeriodFrameCount:I

    .line 73
    iget-wide v2, p0, Landroidx/media/filterpacks/performance/ThroughputFilter;->mLastTime:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 74
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Landroidx/media/filterpacks/performance/ThroughputFilter;->mLastTime:J

    .line 76
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 79
    iget-wide v4, p0, Landroidx/media/filterpacks/performance/ThroughputFilter;->mLastTime:J

    sub-long v4, v2, v4

    iget v1, p0, Landroidx/media/filterpacks/performance/ThroughputFilter;->mPeriod:I

    mul-int/lit16 v1, v1, 0x3e8

    int-to-long v6, v1

    cmp-long v1, v4, v6

    if-ltz v1, :cond_1

    .line 80
    const-string v1, "throughput"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/performance/ThroughputFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    .line 82
    new-instance v4, Laed;

    iget v5, p0, Landroidx/media/filterpacks/performance/ThroughputFilter;->mTotalFrameCount:I

    iget v5, p0, Landroidx/media/filterpacks/performance/ThroughputFilter;->mPeriodFrameCount:I

    iget-wide v6, p0, Landroidx/media/filterpacks/performance/ThroughputFilter;->mLastTime:J

    sub-long v6, v2, v6

    iget-object v8, v0, Laap;->a:Lzp;

    invoke-virtual {v8}, Lzp;->f()I

    invoke-direct {v4, v5, v6, v7}, Laed;-><init>(IJ)V

    .line 86
    const/4 v5, 0x0

    invoke-virtual {v1, v5}, Lacv;->a([I)Laap;

    move-result-object v5

    invoke-virtual {v5}, Laap;->a()Labg;

    move-result-object v5

    .line 87
    invoke-virtual {v5, v4}, Labg;->a(Ljava/lang/Object;)V

    .line 88
    invoke-virtual {v1, v5}, Lacv;->a(Laap;)V

    .line 89
    iput-wide v2, p0, Landroidx/media/filterpacks/performance/ThroughputFilter;->mLastTime:J

    .line 90
    const/4 v1, 0x0

    iput v1, p0, Landroidx/media/filterpacks/performance/ThroughputFilter;->mPeriodFrameCount:I

    .line 93
    :cond_1
    const-string v1, "frame"

    invoke-virtual {p0, v1}, Landroidx/media/filterpacks/performance/ThroughputFilter;->b(Ljava/lang/String;)Lacv;

    move-result-object v1

    invoke-virtual {v1, v0}, Lacv;->a(Laap;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    monitor-exit p0

    return-void

    .line 66
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
