.class public abstract Landroidx/media/filterfw/ViewFilter;
.super Laak;
.source "PG"


# static fields
.field public static final SCALE_FILL:I = 0x3

.field public static final SCALE_FIT:I = 0x2

.field public static final SCALE_STRETCH:I = 0x1


# instance fields
.field public mClearColor:[F

.field public mFlipVertically:Z

.field private mRequestedScaleMode:Ljava/lang/String;

.field public mScaleMode:I

.field private mScaleModeListener:Lacr;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Laak;-><init>(Lacs;Ljava/lang/String;)V

    .line 32
    const/4 v0, 0x2

    iput v0, p0, Landroidx/media/filterfw/ViewFilter;->mScaleMode:I

    .line 33
    const/4 v0, 0x4

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Landroidx/media/filterfw/ViewFilter;->mClearColor:[F

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroidx/media/filterfw/ViewFilter;->mFlipVertically:Z

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Landroidx/media/filterfw/ViewFilter;->mRequestedScaleMode:Ljava/lang/String;

    .line 140
    new-instance v0, Ladb;

    invoke-direct {v0, p0}, Ladb;-><init>(Landroidx/media/filterfw/ViewFilter;)V

    iput-object v0, p0, Landroidx/media/filterfw/ViewFilter;->mScaleModeListener:Lacr;

    .line 40
    return-void

    .line 33
    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static synthetic a(Landroidx/media/filterfw/ViewFilter;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Landroidx/media/filterfw/ViewFilter;->mRequestedScaleMode:Ljava/lang/String;

    return-object v0
.end method

.method public static synthetic a(Landroidx/media/filterfw/ViewFilter;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 26
    iput-object p1, p0, Landroidx/media/filterfw/ViewFilter;->mRequestedScaleMode:Ljava/lang/String;

    return-object p1
.end method


# virtual methods
.method public a(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/RectF;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/high16 v5, 0x3f000000    # 0.5f

    const/high16 v4, 0x3f800000    # 1.0f

    .line 85
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    .line 86
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v1

    if-lez v1, :cond_0

    .line 87
    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    .line 88
    invoke-virtual {p2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/graphics/Rect;->height()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 89
    div-float v1, v2, v1

    .line 90
    iget v2, p0, Landroidx/media/filterfw/ViewFilter;->mScaleMode:I

    packed-switch v2, :pswitch_data_0

    .line 118
    :cond_0
    :goto_0
    return-object v0

    .line 92
    :pswitch_0
    invoke-virtual {v0, v6, v6, v4, v4}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0

    .line 95
    :pswitch_1
    cmpl-float v2, v1, v4

    if-gtz v2, :cond_1

    .line 96
    mul-float v2, v5, v1

    sub-float v2, v5, v2

    .line 102
    add-float/2addr v1, v2

    invoke-virtual {v0, v6, v2, v4, v1}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0

    .line 106
    :pswitch_2
    cmpl-float v2, v1, v4

    if-lez v2, :cond_1

    .line 107
    mul-float v2, v5, v1

    sub-float v2, v5, v2

    .line 109
    add-float/2addr v1, v2

    invoke-virtual {v0, v6, v2, v4, v1}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0

    .line 111
    :cond_1
    div-float v2, v5, v1

    sub-float v2, v5, v2

    .line 112
    div-float v1, v4, v1

    add-float/2addr v1, v2

    invoke-virtual {v0, v2, v6, v1, v4}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0

    .line 90
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lacm;Landroid/graphics/Rect;Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 132
    invoke-virtual {p0, p2, p3}, Landroidx/media/filterfw/ViewFilter;->a(Landroid/graphics/Rect;Landroid/graphics/Rect;)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {p1, v0}, Lacm;->b(Landroid/graphics/RectF;)V

    .line 133
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lacm;->a(Z)V

    .line 134
    iget-object v0, p0, Landroidx/media/filterfw/ViewFilter;->mClearColor:[F

    invoke-virtual {p1, v0}, Lacm;->d([F)V

    .line 135
    iget-boolean v0, p0, Landroidx/media/filterfw/ViewFilter;->mFlipVertically:Z

    if-eqz v0, :cond_0

    .line 136
    const/4 v0, 0x0

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {p1, v0, v2, v2, v1}, Lacm;->a(FFFF)V

    .line 138
    :cond_0
    return-void
.end method

.method public c()Lacx;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 68
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    const-string v1, "scaleMode"

    const-class v2, Ljava/lang/String;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    const-string v1, "flip"

    sget-object v2, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v2}, Labf;->a(Ljava/lang/Class;)Labf;

    move-result-object v2

    invoke-virtual {v0, v1, v3, v2}, Lacx;->a(Ljava/lang/String;ILabf;)Lacx;

    move-result-object v0

    return-object v0
.end method

.method public c(Lacp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 122
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "scaleMode"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    iget-object v0, p0, Landroidx/media/filterfw/ViewFilter;->mScaleModeListener:Lacr;

    invoke-virtual {p1, v0}, Lacp;->a(Lacr;)V

    .line 124
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    invoke-virtual {p1}, Lacp;->e()Ljava/lang/String;

    move-result-object v0

    const-string v1, "flip"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    const-string v0, "mFlipVertically"

    invoke-virtual {p1, v0}, Lacp;->a(Ljava/lang/String;)V

    .line 127
    invoke-virtual {p1, v2}, Lacp;->a(Z)V

    goto :goto_0
.end method
