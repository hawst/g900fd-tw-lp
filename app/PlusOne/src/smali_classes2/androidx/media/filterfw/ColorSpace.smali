.class public Landroidx/media/filterfw/ColorSpace;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 149
    const-string v0, "filterframework_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 150
    return-void
.end method

.method private static a(Ljava/nio/ByteBuffer;I)V
    .locals 3

    .prologue
    .line 118
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-ge v0, p1, :cond_0

    .line 119
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Input buffer\'s size does not fit given width and height! Expected: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Got: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 123
    :cond_0
    return-void
.end method

.method public static a(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;II)V
    .locals 1

    .prologue
    .line 71
    mul-int v0, p2, p3

    shl-int/lit8 v0, v0, 0x2

    invoke-static {p0, v0}, Landroidx/media/filterfw/ColorSpace;->a(Ljava/nio/ByteBuffer;I)V

    .line 72
    mul-int v0, p2, p3

    shl-int/lit8 v0, v0, 0x2

    invoke-static {p1, v0}, Landroidx/media/filterfw/ColorSpace;->b(Ljava/nio/ByteBuffer;I)V

    .line 73
    invoke-static {p0, p1, p2, p3}, Landroidx/media/filterfw/ColorSpace;->nativeRgba8888ToHsva8888(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;II)V

    .line 74
    return-void
.end method

.method public static a(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;III)V
    .locals 1

    .prologue
    .line 32
    mul-int/lit8 v0, p4, 0x3

    mul-int/2addr v0, p3

    div-int/lit8 v0, v0, 0x2

    invoke-static {p0, v0}, Landroidx/media/filterfw/ColorSpace;->a(Ljava/nio/ByteBuffer;I)V

    .line 33
    mul-int v0, p2, p3

    shl-int/lit8 v0, v0, 0x2

    invoke-static {p1, v0}, Landroidx/media/filterfw/ColorSpace;->b(Ljava/nio/ByteBuffer;I)V

    .line 34
    invoke-static {p0, p1, p2, p3, p4}, Landroidx/media/filterfw/ColorSpace;->nativeYuv420pToRgba8888(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;III)V

    .line 35
    return-void
.end method

.method public static a(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;IIIIII)V
    .locals 2

    .prologue
    .line 112
    mul-int v0, p2, p3

    shl-int/lit8 v0, v0, 0x2

    invoke-static {p0, v0}, Landroidx/media/filterfw/ColorSpace;->a(Ljava/nio/ByteBuffer;I)V

    .line 113
    add-int/lit8 v0, p6, 0x1

    sub-int/2addr v0, p4

    add-int/lit8 v1, p7, 0x1

    sub-int/2addr v1, p5

    mul-int/2addr v0, v1

    shl-int/lit8 v0, v0, 0x2

    invoke-static {p1, v0}, Landroidx/media/filterfw/ColorSpace;->b(Ljava/nio/ByteBuffer;I)V

    .line 114
    invoke-static/range {p0 .. p7}, Landroidx/media/filterfw/ColorSpace;->nativeCropRgbaImage(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;IIIIII)V

    .line 115
    return-void
.end method

.method private static b(Ljava/nio/ByteBuffer;I)V
    .locals 3

    .prologue
    .line 126
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    if-ge v0, p1, :cond_0

    .line 127
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Output buffer\'s size does not fit given width and height! Expected: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Got: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 131
    :cond_0
    return-void
.end method

.method public static b(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;II)V
    .locals 1

    .prologue
    .line 90
    mul-int v0, p2, p3

    shl-int/lit8 v0, v0, 0x2

    invoke-static {p0, v0}, Landroidx/media/filterfw/ColorSpace;->a(Ljava/nio/ByteBuffer;I)V

    .line 91
    mul-int v0, p2, p3

    shl-int/lit8 v0, v0, 0x2

    invoke-static {p1, v0}, Landroidx/media/filterfw/ColorSpace;->b(Ljava/nio/ByteBuffer;I)V

    .line 92
    invoke-static {p0, p1, p2, p3}, Landroidx/media/filterfw/ColorSpace;->nativeRgba8888ToYcbcra8888(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;II)V

    .line 93
    return-void
.end method

.method public static b(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;III)V
    .locals 1

    .prologue
    .line 52
    mul-int v0, p4, p3

    invoke-static {p0, v0}, Landroidx/media/filterfw/ColorSpace;->a(Ljava/nio/ByteBuffer;I)V

    .line 53
    mul-int v0, p2, p3

    shl-int/lit8 v0, v0, 0x2

    invoke-static {p1, v0}, Landroidx/media/filterfw/ColorSpace;->b(Ljava/nio/ByteBuffer;I)V

    .line 54
    invoke-static {p0, p1, p2, p3, p4}, Landroidx/media/filterfw/ColorSpace;->nativeArgb8888ToRgba8888(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;III)V

    .line 55
    return-void
.end method

.method private static native nativeArgb8888ToRgba8888(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;III)V
.end method

.method private static native nativeCropRgbaImage(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;IIIIII)V
.end method

.method private static native nativeRgba8888ToHsva8888(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;II)V
.end method

.method private static native nativeRgba8888ToYcbcra8888(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;II)V
.end method

.method private static native nativeYuv420pToRgba8888(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;III)V
.end method
