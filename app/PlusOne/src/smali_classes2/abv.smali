.class final Labv;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljavax/xml/parsers/SAXParserFactory;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 450
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 451
    invoke-static {}, Ljavax/xml/parsers/SAXParserFactory;->newInstance()Ljavax/xml/parsers/SAXParserFactory;

    move-result-object v0

    iput-object v0, p0, Labv;->a:Ljavax/xml/parsers/SAXParserFactory;

    .line 452
    return-void
.end method

.method private a(Labq;)Lorg/xml/sax/XMLReader;
    .locals 3

    .prologue
    .line 475
    :try_start_0
    iget-object v0, p0, Labv;->a:Ljavax/xml/parsers/SAXParserFactory;

    invoke-virtual {v0}, Ljavax/xml/parsers/SAXParserFactory;->newSAXParser()Ljavax/xml/parsers/SAXParser;

    move-result-object v0

    .line 476
    invoke-virtual {v0}, Ljavax/xml/parsers/SAXParser;->getXMLReader()Lorg/xml/sax/XMLReader;

    move-result-object v0

    .line 477
    new-instance v1, Labw;

    invoke-direct {v1, p1}, Labw;-><init>(Labq;)V

    .line 478
    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->setContentHandler(Lorg/xml/sax/ContentHandler;)V
    :try_end_0
    .catch Ljavax/xml/parsers/ParserConfigurationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_1

    .line 479
    return-object v0

    .line 480
    :catch_0
    move-exception v0

    .line 481
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Error creating SAXParser for graph parsing!"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 482
    :catch_1
    move-exception v0

    .line 483
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Error creating XMLReader for graph parsing!"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public a(Ljava/io/InputStream;Labq;)V
    .locals 3

    .prologue
    .line 466
    :try_start_0
    invoke-direct {p0, p2}, Labv;->a(Labq;)Lorg/xml/sax/XMLReader;

    move-result-object v0

    .line 467
    new-instance v1, Lorg/xml/sax/InputSource;

    invoke-direct {v1, p1}, Lorg/xml/sax/InputSource;-><init>(Ljava/io/InputStream;)V

    invoke-interface {v0, v1}, Lorg/xml/sax/XMLReader;->parse(Lorg/xml/sax/InputSource;)V
    :try_end_0
    .catch Lorg/xml/sax/SAXException; {:try_start_0 .. :try_end_0} :catch_0

    .line 470
    return-void

    .line 468
    :catch_0
    move-exception v0

    .line 469
    new-instance v1, Ljava/io/IOException;

    const-string v2, "XML parse error during graph parsing!"

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
