.class public Lkan;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljxy;
.implements Llnx;
.implements Llql;
.implements Llrd;
.implements Llrg;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;


# instance fields
.field private c:Landroid/os/Bundle;

.field private d:Ljxx;

.field private e:Lkap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lkan;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lkan;->a:Ljava/lang/String;

    .line 37
    const-class v0, Lkan;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lkan;->b:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Llqr;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 47
    return-void
.end method

.method constructor <init>(Llqr;B)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 51
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Ljxx;
    .locals 3

    .prologue
    .line 60
    new-instance v0, Lkad;

    invoke-direct {v0, p1}, Lkad;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lkan;->d:Ljxx;

    .line 61
    iget-object v0, p0, Lkan;->d:Ljxx;

    invoke-interface {v0}, Ljxx;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 62
    iget-object v0, p0, Lkan;->d:Ljxx;

    check-cast v0, Lkad;

    .line 64
    invoke-virtual {p0}, Lkan;->a()Lkap;

    move-result-object v1

    .line 66
    invoke-virtual {v0}, Lkad;->r()Ljxp;

    move-result-object v2

    .line 65
    invoke-virtual {v1, v2}, Lkap;->a(Ljxp;)V

    .line 68
    invoke-virtual {v0}, Lkad;->q()Ljxr;

    move-result-object v0

    .line 67
    invoke-virtual {v1, v0}, Lkap;->a(Ljxr;)V

    .line 70
    :cond_0
    iget-object v0, p0, Lkan;->d:Ljxx;

    return-object v0
.end method

.method a()Lkap;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lkan;->e:Lkap;

    if-nez v0, :cond_0

    .line 125
    new-instance v0, Lkap;

    invoke-direct {v0}, Lkap;-><init>()V

    iput-object v0, p0, Lkan;->e:Lkap;

    .line 127
    :cond_0
    iget-object v0, p0, Lkan;->e:Lkap;

    return-object v0
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 55
    const-class v0, Lhoc;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    invoke-virtual {p0}, Lkan;->a()Lkap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhoc;->a(Lhob;)Lhoc;

    .line 56
    return-void
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 98
    if-nez p2, :cond_0

    .line 102
    :goto_0
    return-void

    .line 101
    :cond_0
    sget-object v0, Lkan;->b:Ljava/lang/String;

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lkan;->c:Landroid/os/Bundle;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)[B
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 106
    iget-object v1, p0, Lkan;->c:Landroid/os/Bundle;

    if-nez v1, :cond_1

    .line 110
    :cond_0
    :goto_0
    return-object v0

    .line 109
    :cond_1
    iget-object v1, p0, Lkan;->c:Landroid/os/Bundle;

    invoke-virtual {v1, p1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 110
    if-eqz v1, :cond_0

    const-string v0, "db_embed_poll"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 75
    iget-object v0, p0, Lkan;->d:Ljxx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkan;->d:Ljxx;

    invoke-interface {v0}, Ljxx;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 76
    :cond_0
    sget-object v0, Lkan;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 94
    :goto_0
    return-void

    .line 80
    :cond_1
    :try_start_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lkan;->c:Landroid/os/Bundle;

    .line 81
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 82
    const-string v1, "db_embed_poll"

    iget-object v2, p0, Lkan;->d:Ljxx;

    .line 83
    invoke-interface {v2}, Ljxx;->e()Lkzw;

    move-result-object v2

    invoke-static {v2}, Lkzw;->a(Lkzw;)[B

    move-result-object v2

    .line 82
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 84
    const-string v1, "show_poll_results"

    iget-object v2, p0, Lkan;->d:Ljxx;

    .line 85
    invoke-interface {v2}, Ljxx;->i()Z

    move-result v2

    .line 84
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 86
    iget-object v1, p0, Lkan;->c:Landroid/os/Bundle;

    iget-object v2, p0, Lkan;->d:Ljxx;

    invoke-interface {v2}, Ljxx;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 87
    sget-object v0, Lkan;->b:Ljava/lang/String;

    iget-object v1, p0, Lkan;->c:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    sget-object v0, Lkan;->a:Ljava/lang/String;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 90
    sget-object v0, Lkan;->a:Ljava/lang/String;

    .line 92
    :cond_2
    sget-object v0, Lkan;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 115
    iget-object v1, p0, Lkan;->c:Landroid/os/Bundle;

    if-nez v1, :cond_1

    .line 119
    :cond_0
    :goto_0
    return v0

    .line 118
    :cond_1
    iget-object v1, p0, Lkan;->c:Landroid/os/Bundle;

    invoke-virtual {v1, p1}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    .line 119
    if-eqz v1, :cond_0

    const-string v2, "show_poll_results"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
