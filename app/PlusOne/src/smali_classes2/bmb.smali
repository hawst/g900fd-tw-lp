.class public final Lbmb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lbku;


# direct methods
.method public constructor <init>(Lbku;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const-string v0, "pointMetrics"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbku;

    iput-object v0, p0, Lbmb;->a:Lbku;

    .line 41
    return-void
.end method

.method public static a(Ljava/util/List;)Ljava/util/Map;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbon;",
            ">;)",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbmb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0}, Lcfm;->a(I)Ljava/util/HashMap;

    move-result-object v1

    .line 31
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbon;

    .line 32
    invoke-virtual {v0}, Lbon;->a()Ljeg;

    move-result-object v3

    invoke-virtual {v0}, Lbon;->c()Lbol;

    move-result-object v0

    invoke-virtual {v0}, Lbol;->b()Lbmb;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 34
    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Landroid/util/LongSparseArray;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    iget-object v0, p0, Lbmb;->a:Lbku;

    sget-object v1, Lbmz;->k:Lbmz;

    .line 54
    invoke-virtual {v0, v1}, Lbku;->b(Lbmz;)Landroid/util/LongSparseArray;

    move-result-object v0

    .line 55
    return-object v0
.end method

.method public b()Landroid/util/LongSparseArray;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 68
    iget-object v0, p0, Lbmb;->a:Lbku;

    sget-object v1, Lbmz;->u:Lbmz;

    .line 69
    invoke-virtual {v0, v1}, Lbku;->b(Lbmz;)Landroid/util/LongSparseArray;

    move-result-object v0

    .line 70
    return-object v0
.end method
