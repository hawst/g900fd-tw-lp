.class public final Lbvm;
.super Loxu;
.source "PG"


# instance fields
.field public a:[J

.field public b:[J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1090
    invoke-direct {p0}, Loxu;-><init>()V

    .line 1093
    sget-object v0, Loxx;->b:[J

    iput-object v0, p0, Lbvm;->a:[J

    .line 1096
    sget-object v0, Loxx;->b:[J

    iput-object v0, p0, Lbvm;->b:[J

    .line 1090
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1122
    .line 1123
    iget-object v0, p0, Lbvm;->a:[J

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbvm;->a:[J

    array-length v0, v0

    if-lez v0, :cond_3

    .line 1125
    iget-object v3, p0, Lbvm;->a:[J

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_0
    if-ge v0, v4, :cond_0

    aget-wide v6, v3, v0

    .line 1127
    invoke-static {v6, v7}, Loxo;->f(J)I

    move-result v5

    add-int/2addr v2, v5

    .line 1125
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1130
    :cond_0
    iget-object v0, p0, Lbvm;->a:[J

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v2

    .line 1132
    :goto_1
    iget-object v2, p0, Lbvm;->b:[J

    if-eqz v2, :cond_2

    iget-object v2, p0, Lbvm;->b:[J

    array-length v2, v2

    if-lez v2, :cond_2

    .line 1134
    iget-object v3, p0, Lbvm;->b:[J

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v1, v4, :cond_1

    aget-wide v6, v3, v1

    .line 1136
    invoke-static {v6, v7}, Loxo;->f(J)I

    move-result v5

    add-int/2addr v2, v5

    .line 1134
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1138
    :cond_1
    add-int/2addr v0, v2

    .line 1139
    iget-object v1, p0, Lbvm;->b:[J

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1141
    :cond_2
    iput v0, p0, Lbvm;->ai:I

    .line 1142
    return v0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public a(Loxn;)Lbvm;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1150
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1151
    sparse-switch v0, :sswitch_data_0

    .line 1155
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1156
    :sswitch_0
    return-object p0

    .line 1161
    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 1162
    iget-object v0, p0, Lbvm;->a:[J

    array-length v0, v0

    .line 1163
    add-int/2addr v1, v0

    new-array v1, v1, [J

    .line 1164
    iget-object v2, p0, Lbvm;->a:[J

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1165
    iput-object v1, p0, Lbvm;->a:[J

    .line 1166
    :goto_1
    iget-object v1, p0, Lbvm;->a:[J

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    .line 1167
    iget-object v1, p0, Lbvm;->a:[J

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    aput-wide v2, v1, v0

    .line 1168
    invoke-virtual {p1}, Loxn;->a()I

    .line 1166
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1171
    :cond_1
    iget-object v1, p0, Lbvm;->a:[J

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    aput-wide v2, v1, v0

    goto :goto_0

    .line 1175
    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 1176
    iget-object v0, p0, Lbvm;->b:[J

    array-length v0, v0

    .line 1177
    add-int/2addr v1, v0

    new-array v1, v1, [J

    .line 1178
    iget-object v2, p0, Lbvm;->b:[J

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1179
    iput-object v1, p0, Lbvm;->b:[J

    .line 1180
    :goto_2
    iget-object v1, p0, Lbvm;->b:[J

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 1181
    iget-object v1, p0, Lbvm;->b:[J

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    aput-wide v2, v1, v0

    .line 1182
    invoke-virtual {p1}, Loxn;->a()I

    .line 1180
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1185
    :cond_2
    iget-object v1, p0, Lbvm;->b:[J

    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    aput-wide v2, v1, v0

    goto :goto_0

    .line 1151
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 1108
    iget-object v1, p0, Lbvm;->a:[J

    if-eqz v1, :cond_0

    .line 1109
    iget-object v2, p0, Lbvm;->a:[J

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-wide v4, v2, v1

    .line 1110
    const/4 v6, 0x1

    invoke-virtual {p1, v6, v4, v5}, Loxo;->b(IJ)V

    .line 1109
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1113
    :cond_0
    iget-object v1, p0, Lbvm;->b:[J

    if-eqz v1, :cond_1

    .line 1114
    iget-object v1, p0, Lbvm;->b:[J

    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_1

    aget-wide v4, v1, v0

    .line 1115
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v4, v5}, Loxo;->b(IJ)V

    .line 1114
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1118
    :cond_1
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1086
    invoke-virtual {p0, p1}, Lbvm;->a(Loxn;)Lbvm;

    move-result-object v0

    return-object v0
.end method
