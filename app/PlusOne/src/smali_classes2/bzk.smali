.class final Lbzk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;>;"
        }
    .end annotation
.end field

.field private c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbzk;->a:Ljava/util/Map;

    .line 38
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lbzk;->b:Landroid/util/SparseArray;

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbzk;->c:Ljava/util/List;

    return-void
.end method

.method private a(Ljava/util/List;Ljava/util/List;Ljava/util/Comparator;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Ljava/util/Comparator",
            "<",
            "Lbmd;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 127
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 128
    iget-object v0, p0, Lbzk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 130
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 131
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 135
    :goto_0
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v1, v0

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 136
    iget-object v6, p0, Lbzk;->b:Landroid/util/SparseArray;

    iget v0, v0, Lbmd;->b:I

    invoke-virtual {v6, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 137
    if-eqz v0, :cond_3

    .line 138
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 141
    :goto_3
    if-eqz v1, :cond_2

    .line 142
    invoke-interface {p3, v1, v0}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v7

    if-gez v7, :cond_2

    .line 143
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 145
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmd;

    goto :goto_3

    :cond_0
    move-object v0, v2

    .line 131
    goto :goto_0

    :cond_1
    move-object v1, v2

    .line 145
    goto :goto_3

    .line 147
    :cond_2
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    move-object v0, v1

    move-object v1, v0

    .line 150
    goto :goto_1

    :cond_4
    move-object v0, v2

    :goto_4
    move-object v1, v0

    .line 151
    :cond_5
    if-eqz v1, :cond_6

    .line 152
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 153
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 154
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    goto :goto_4

    .line 156
    :cond_6
    return-object v3
.end method

.method private a()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lbzk;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 86
    iget-object v0, p0, Lbzk;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 87
    iget-object v0, p0, Lbzk;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 88
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 23
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 105
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzk;->b:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    .line 106
    move-object/from16 v0, p0

    iget-object v2, v0, Lbzk;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 107
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbmd;

    .line 108
    move-object/from16 v0, p0

    iget-object v3, v0, Lbzk;->a:Ljava/util/Map;

    iget-object v4, v2, Lbmd;->e:Ljeg;

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    if-nez v3, :cond_1

    const/4 v6, 0x0

    .line 109
    :cond_0
    if-nez v6, :cond_3

    .line 110
    move-object/from16 v0, p0

    iget-object v3, v0, Lbzk;->c:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 108
    :cond_1
    const/4 v6, 0x0

    const v5, 0x7f7fffff    # Float.MAX_VALUE

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbmd;

    iget-object v9, v3, Lbmd;->f:Lbmp;

    iget-object v10, v2, Lbmd;->f:Lbmp;

    iget-wide v12, v9, Lbmp;->c:J

    iget-wide v14, v10, Lbmp;->c:J

    sub-long/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(J)J

    move-result-wide v12

    iget-wide v14, v9, Lbmp;->b:J

    iget-wide v0, v10, Lbmp;->b:J

    move-wide/from16 v16, v0

    sub-long v14, v14, v16

    invoke-static {v14, v15}, Ljava/lang/Math;->abs(J)J

    move-result-wide v14

    iget-wide v0, v9, Lbmp;->c:J

    move-wide/from16 v16, v0

    iget-wide v0, v10, Lbmp;->c:J

    move-wide/from16 v18, v0

    invoke-static/range {v16 .. v19}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v16

    iget-wide v0, v9, Lbmp;->b:J

    move-wide/from16 v18, v0

    iget-wide v0, v10, Lbmp;->b:J

    move-wide/from16 v20, v0

    invoke-static/range {v18 .. v21}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v18

    const/high16 v4, 0x3f000000    # 0.5f

    add-long v20, v12, v14

    move-wide/from16 v0, v20

    long-to-float v11, v0

    mul-float/2addr v4, v11

    invoke-static {v9, v10}, Lbpx;->a(Lbmp;Lbmp;)Z

    move-result v11

    invoke-static {v10, v9}, Lbpx;->a(Lbmp;Lbmp;)Z

    move-result v9

    xor-int/2addr v9, v11

    if-eqz v9, :cond_2

    long-to-float v9, v12

    long-to-float v10, v14

    mul-float/2addr v9, v10

    sub-long v10, v16, v18

    long-to-float v10, v10

    div-float/2addr v9, v10

    sub-float/2addr v4, v9

    :cond_2
    cmpg-float v9, v4, v5

    if-gez v9, :cond_6

    move/from16 v22, v4

    move-object v4, v3

    move/from16 v3, v22

    :goto_2
    move v5, v3

    move-object v6, v4

    goto :goto_1

    .line 112
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lbzk;->b:Landroid/util/SparseArray;

    iget v4, v6, Lbmd;->b:I

    .line 113
    invoke-virtual {v3, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/List;

    .line 114
    if-nez v3, :cond_4

    .line 115
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 116
    move-object/from16 v0, p0

    iget-object v4, v0, Lbzk;->b:Landroid/util/SparseArray;

    iget v5, v6, Lbmd;->b:I

    invoke-virtual {v4, v5, v3}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 118
    :cond_4
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 121
    :cond_5
    return-void

    :cond_6
    move v3, v5

    move-object v4, v6

    goto :goto_2
.end method


# virtual methods
.method public a(Ljava/util/Map;Lbox;Ljava/util/List;)Ljava/util/List;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "+",
            "Lbml;",
            ">;",
            "Lbox;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 70
    new-instance v2, Lbpo;

    invoke-direct {v2, p1}, Lbpo;-><init>(Ljava/util/Map;)V

    .line 71
    invoke-interface {p3}, Ljava/util/Collection;->toArray()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    invoke-static {v0, v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 73
    :try_start_0
    iget-object v0, p2, Lbox;->a:Ljava/util/List;

    iget-object v1, p0, Lbzk;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-object v1, p0, Lbzk;->a:Ljava/util/Map;

    iget-object v5, v0, Lbmd;->e:Ljeg;

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/List;

    if-nez v1, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v5, p0, Lbzk;->a:Ljava/util/Map;

    iget-object v6, v0, Lbmd;->e:Ljeg;

    invoke-interface {v5, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 79
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lbzk;->a()V

    throw v0

    .line 74
    :cond_1
    :try_start_1
    invoke-direct {p0, v3}, Lbzk;->a(Ljava/util/List;)V

    .line 76
    iget-object v0, p2, Lbox;->a:Ljava/util/List;

    invoke-direct {p0, v0, v3, v2}, Lbzk;->a(Ljava/util/List;Ljava/util/List;Ljava/util/Comparator;)Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 79
    invoke-direct {p0}, Lbzk;->a()V

    return-object v0
.end method
