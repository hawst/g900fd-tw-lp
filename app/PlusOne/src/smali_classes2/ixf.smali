.class public final Lixf;
.super Lt;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lt;-><init>()V

    return-void
.end method

.method public static a(Lz;)V
    .locals 2

    .prologue
    .line 33
    invoke-virtual {p0}, Lz;->f()Lae;

    move-result-object v0

    .line 34
    const-string v1, "login.progress"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 35
    if-eqz v0, :cond_0

    .line 37
    :try_start_0
    invoke-virtual {v0}, Lt;->a()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 42
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public static a(Lz;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 20
    invoke-static {p0}, Lixf;->b(Lz;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 21
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Progress dialog is already showing"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 24
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 25
    const-string v1, "message"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 26
    new-instance v1, Lixf;

    invoke-direct {v1}, Lixf;-><init>()V

    .line 27
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lixf;->b(Z)V

    .line 28
    invoke-virtual {v1, v0}, Lixf;->f(Landroid/os/Bundle;)V

    .line 29
    invoke-virtual {p0}, Lz;->f()Lae;

    move-result-object v0

    const-string v2, "login.progress"

    invoke-virtual {v1, v0, v2}, Lixf;->a(Lae;Ljava/lang/String;)V

    .line 30
    return-void
.end method

.method public static b(Lz;)Z
    .locals 2

    .prologue
    .line 45
    invoke-virtual {p0}, Lz;->f()Lae;

    move-result-object v0

    const-string v1, "login.progress"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 50
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lixf;->n()Lz;

    move-result-object v1

    const v2, 0x7f090066

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 51
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 52
    invoke-virtual {p0}, Lixf;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "message"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 53
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 54
    return-object v1
.end method
