.class public final Lbvj;
.super Loxu;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field public c:[F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 808
    invoke-direct {p0}, Loxu;-><init>()V

    .line 811
    iput v0, p0, Lbvj;->a:I

    .line 814
    iput v0, p0, Lbvj;->b:I

    .line 817
    sget-object v0, Loxx;->c:[F

    iput-object v0, p0, Lbvj;->c:[F

    .line 808
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 841
    const/4 v0, 0x1

    iget v1, p0, Lbvj;->a:I

    .line 843
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 844
    const/4 v1, 0x2

    iget v2, p0, Lbvj;->b:I

    .line 845
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 846
    iget-object v1, p0, Lbvj;->c:[F

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbvj;->c:[F

    array-length v1, v1

    if-lez v1, :cond_0

    .line 847
    iget-object v1, p0, Lbvj;->c:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 848
    add-int/2addr v0, v1

    .line 849
    iget-object v1, p0, Lbvj;->c:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 851
    :cond_0
    iput v0, p0, Lbvj;->ai:I

    .line 852
    return v0
.end method

.method public a(Loxn;)Lbvj;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 860
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 861
    sparse-switch v0, :sswitch_data_0

    .line 865
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 866
    :sswitch_0
    return-object p0

    .line 871
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    iput v0, p0, Lbvj;->a:I

    goto :goto_0

    .line 875
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    iput v0, p0, Lbvj;->b:I

    goto :goto_0

    .line 879
    :sswitch_3
    const/16 v0, 0x1d

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 880
    iget-object v0, p0, Lbvj;->c:[F

    array-length v0, v0

    .line 881
    add-int/2addr v1, v0

    new-array v1, v1, [F

    .line 882
    iget-object v2, p0, Lbvj;->c:[F

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 883
    iput-object v1, p0, Lbvj;->c:[F

    .line 884
    :goto_1
    iget-object v1, p0, Lbvj;->c:[F

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    .line 885
    iget-object v1, p0, Lbvj;->c:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v2

    aput v2, v1, v0

    .line 886
    invoke-virtual {p1}, Loxn;->a()I

    .line 884
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 889
    :cond_1
    iget-object v1, p0, Lbvj;->c:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 861
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1d -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 830
    const/4 v0, 0x1

    iget v1, p0, Lbvj;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 831
    const/4 v0, 0x2

    iget v1, p0, Lbvj;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 832
    iget-object v0, p0, Lbvj;->c:[F

    if-eqz v0, :cond_0

    .line 833
    iget-object v1, p0, Lbvj;->c:[F

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 834
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->a(IF)V

    .line 833
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 837
    :cond_0
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 804
    invoke-virtual {p0, p1}, Lbvj;->a(Loxn;)Lbvj;

    move-result-object v0

    return-object v0
.end method
