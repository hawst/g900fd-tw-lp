.class final Lbpq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lbpq;",
        ">;"
    }
.end annotation


# instance fields
.field public final a:F

.field public final b:F

.field public final c:F

.field public final d:F

.field public final e:F

.field public final f:F

.field private g:F

.field private h:J


# direct methods
.method public constructor <init>(FFFFFF)V
    .locals 2

    .prologue
    .line 491
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 492
    iput p1, p0, Lbpq;->a:F

    .line 493
    iput p2, p0, Lbpq;->c:F

    .line 494
    sub-float v0, p2, p1

    iput v0, p0, Lbpq;->g:F

    .line 495
    iput p3, p0, Lbpq;->b:F

    .line 496
    iput p4, p0, Lbpq;->d:F

    .line 497
    iput p5, p0, Lbpq;->e:F

    .line 498
    iput p6, p0, Lbpq;->f:F

    .line 499
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbpq;->h:J

    .line 500
    return-void
.end method

.method public constructor <init>(FFFFJ)V
    .locals 3

    .prologue
    .line 479
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 480
    iput p1, p0, Lbpq;->a:F

    .line 481
    iput p2, p0, Lbpq;->c:F

    .line 482
    iget v0, p0, Lbpq;->c:F

    iget v1, p0, Lbpq;->a:F

    sub-float/2addr v0, v1

    iput v0, p0, Lbpq;->g:F

    .line 483
    iget v0, p0, Lbpq;->a:F

    iget v1, p0, Lbpq;->g:F

    const/high16 v2, 0x40a00000    # 5.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lbpq;->b:F

    .line 484
    iget v0, p0, Lbpq;->c:F

    iget v1, p0, Lbpq;->g:F

    const/high16 v2, 0x40800000    # 4.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    iput v0, p0, Lbpq;->d:F

    .line 485
    iput p3, p0, Lbpq;->e:F

    .line 486
    iput p4, p0, Lbpq;->f:F

    .line 487
    iput-wide p5, p0, Lbpq;->h:J

    .line 488
    return-void
.end method


# virtual methods
.method public a()F
    .locals 3

    .prologue
    .line 503
    iget v0, p0, Lbpq;->b:F

    const v1, 0x3dcccccd    # 0.1f

    iget v2, p0, Lbpq;->g:F

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public a(Lbpq;)I
    .locals 4

    .prologue
    const/high16 v2, 0x41f00000    # 30.0f

    .line 517
    iget v0, p0, Lbpq;->g:F

    mul-float/2addr v0, v2

    float-to-int v0, v0

    .line 518
    iget v1, p1, Lbpq;->g:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 521
    if-ne v0, v1, :cond_1

    .line 522
    iget-wide v0, p0, Lbpq;->h:J

    iget-wide v2, p1, Lbpq;->h:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 523
    const/4 v0, -0x1

    .line 529
    :goto_0
    return v0

    .line 524
    :cond_0
    iget-wide v0, p0, Lbpq;->h:J

    iget-wide v2, p1, Lbpq;->h:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 525
    const/4 v0, 0x1

    goto :goto_0

    .line 529
    :cond_1
    iget v0, p1, Lbpq;->g:F

    iget v1, p0, Lbpq;->g:F

    invoke-static {v0, v1}, Ljava/lang/Float;->compare(FF)I

    move-result v0

    goto :goto_0
.end method

.method public b()F
    .locals 2

    .prologue
    .line 507
    iget v0, p0, Lbpq;->f:F

    iget v1, p0, Lbpq;->e:F

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 442
    check-cast p1, Lbpq;

    invoke-virtual {p0, p1}, Lbpq;->a(Lbpq;)I

    move-result v0

    return v0
.end method
