.class public final Lbok;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljeg;

.field private final b:Lbmv;

.field private final c:J

.field private final d:J


# direct methods
.method public constructor <init>(Ljeg;Lbmv;JJ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const-string v0, "mediaIdentifier"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljeg;

    iput-object v0, p0, Lbok;->a:Ljeg;

    .line 68
    const-string v0, "type"

    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmv;

    iput-object v0, p0, Lbok;->b:Lbmv;

    .line 69
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    const-string v1, "creationTimeUs"

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lbok;->c:J

    .line 70
    const-string v0, "durationUs"

    invoke-static {p5, p6, v0}, Lcec;->b(JLjava/lang/CharSequence;)J

    move-result-wide v0

    iput-wide v0, p0, Lbok;->d:J

    .line 71
    return-void
.end method

.method public static a(Lbml;)Lbok;
    .locals 8

    .prologue
    .line 18
    new-instance v1, Lbok;

    invoke-interface {p0}, Lbml;->a()Ljeg;

    move-result-object v2

    .line 19
    invoke-interface {p0}, Lbml;->b()Lbmu;

    move-result-object v0

    iget-object v3, v0, Lbmu;->e:Lbmv;

    .line 20
    invoke-interface {p0}, Lbml;->b()Lbmu;

    move-result-object v0

    iget-wide v4, v0, Lbmu;->b:J

    .line 21
    invoke-interface {p0}, Lbml;->e()J

    move-result-wide v6

    .line 22
    invoke-interface {p0}, Lbml;->f()Z

    invoke-direct/range {v1 .. v7}, Lbok;-><init>(Ljeg;Lbmv;JJ)V

    return-object v1
.end method


# virtual methods
.method public a()Ljeg;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lbok;->a:Ljeg;

    return-object v0
.end method

.method public b()Lbmv;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lbok;->b:Lbmv;

    return-object v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 83
    iget-wide v0, p0, Lbok;->c:J

    return-wide v0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 87
    iget-wide v0, p0, Lbok;->d:J

    return-wide v0
.end method
