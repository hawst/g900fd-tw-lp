.class public final Lbic;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbhr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbhr",
        "<",
        "Lbhk;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lbhr;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbhr",
            "<",
            "Lbhk;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lbmp;


# direct methods
.method public constructor <init>(Lbhr;Lbmp;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbhr",
            "<",
            "Lbhk;",
            ">;",
            "Lbmp;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string v0, "source"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    iput-object v0, p0, Lbic;->a:Lbhr;

    .line 22
    const-string v0, "interval"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmp;

    iput-object v0, p0, Lbic;->b:Lbmp;

    .line 23
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lbic;->a:Lbhr;

    invoke-interface {v0}, Lbhr;->a()V

    .line 97
    return-void
.end method

.method public a(Lbhk;)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lbic;->a:Lbhr;

    invoke-interface {v0, p1}, Lbhr;->a(Ljava/lang/Object;)V

    .line 92
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 15
    check-cast p1, Lbhk;

    invoke-virtual {p0, p1}, Lbic;->a(Lbhk;)V

    return-void
.end method

.method public b()Lbhk;
    .locals 12

    .prologue
    const/4 v4, 0x0

    .line 27
    iget-object v0, p0, Lbic;->a:Lbhr;

    invoke-interface {v0}, Lbhr;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhk;

    .line 29
    if-nez v0, :cond_0

    move-object v0, v4

    .line 86
    :goto_0
    return-object v0

    .line 33
    :cond_0
    iget v2, v0, Lbhk;->h:I

    .line 35
    iget-wide v6, v0, Lbhk;->a:J

    .line 36
    iget-wide v8, v0, Lbhk;->a:J

    iget v1, v0, Lbhk;->c:I

    iget v3, v0, Lbhk;->e:I

    iget v5, v0, Lbhk;->d:I

    invoke-static {v2, v1, v3, v5}, Lceg;->a(IIII)J

    move-result-wide v10

    sub-long/2addr v8, v10

    .line 42
    iget v1, v0, Lbhk;->g:I

    .line 46
    iget-object v3, p0, Lbic;->b:Lbmp;

    iget-wide v10, v3, Lbmp;->b:J

    cmp-long v3, v8, v10

    if-gez v3, :cond_3

    .line 47
    iget-object v3, p0, Lbic;->b:Lbmp;

    iget-wide v10, v3, Lbmp;->b:J

    sub-long v8, v10, v8

    .line 49
    iget v3, v0, Lbhk;->c:I

    iget v5, v0, Lbhk;->e:I

    iget v10, v0, Lbhk;->d:I

    .line 50
    invoke-static {v8, v9, v3, v5, v10}, Lceg;->a(JIII)I

    move-result v3

    .line 56
    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 57
    add-int v3, v1, v5

    .line 58
    sub-int v1, v2, v5

    .line 62
    :goto_1
    iget-object v5, p0, Lbic;->b:Lbmp;

    iget-wide v8, v5, Lbmp;->c:J

    cmp-long v5, v6, v8

    if-lez v5, :cond_1

    .line 63
    iget-object v1, p0, Lbic;->b:Lbmp;

    iget-wide v8, v1, Lbmp;->c:J

    sub-long/2addr v6, v8

    .line 67
    iget v1, v0, Lbhk;->c:I

    iget v5, v0, Lbhk;->e:I

    iget v8, v0, Lbhk;->d:I

    .line 68
    invoke-static {v6, v7, v1, v5, v8}, Lceg;->a(JIII)I

    move-result v1

    .line 73
    const/4 v5, 0x0

    sub-int v1, v2, v1

    sub-int/2addr v1, v3

    invoke-static {v5, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 78
    :cond_1
    if-nez v1, :cond_2

    iget-boolean v2, v0, Lbhk;->b:Z

    if-nez v2, :cond_2

    .line 79
    invoke-virtual {p0, v0}, Lbic;->a(Lbhk;)V

    move-object v0, v4

    .line 80
    goto :goto_0

    .line 83
    :cond_2
    iput v3, v0, Lbhk;->g:I

    .line 84
    iput v1, v0, Lbhk;->h:I

    goto :goto_0

    :cond_3
    move v3, v1

    move v1, v2

    goto :goto_1
.end method

.method public synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 15
    invoke-virtual {p0}, Lbic;->b()Lbhk;

    move-result-object v0

    return-object v0
.end method
