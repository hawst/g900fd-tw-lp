.class public final Lhcq;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:[F


# instance fields
.field private final b:Landroid/graphics/Paint;

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Paint;

.field private final e:Landroid/graphics/Paint;

.field private final f:Landroid/graphics/RectF;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lhcq;->a:[F

    return-void

    nop

    :array_0
    .array-data 4
        0x41a00000    # 20.0f
        0x41400000    # 12.0f
    .end array-data
.end method

.method public constructor <init>(Landroid/content/res/Resources;)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/4 v3, 0x1

    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lhcq;->f:Landroid/graphics/RectF;

    .line 29
    const/high16 v0, 0x3f800000    # 1.0f

    .line 30
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 29
    invoke-static {v3, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    .line 32
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lhcq;->b:Landroid/graphics/Paint;

    .line 33
    iget-object v1, p0, Lhcq;->b:Landroid/graphics/Paint;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 34
    iget-object v1, p0, Lhcq;->b:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 35
    iget-object v1, p0, Lhcq;->b:Landroid/graphics/Paint;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 36
    iget-object v1, p0, Lhcq;->b:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 38
    const/high16 v0, 0x3fc00000    # 1.5f

    .line 39
    invoke-virtual {p1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 38
    invoke-static {v3, v0, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    .line 41
    new-instance v1, Landroid/graphics/Paint;

    iget-object v2, p0, Lhcq;->b:Landroid/graphics/Paint;

    invoke-direct {v1, v2}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v1, p0, Lhcq;->c:Landroid/graphics/Paint;

    .line 42
    iget-object v1, p0, Lhcq;->c:Landroid/graphics/Paint;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 43
    iget-object v1, p0, Lhcq;->c:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 45
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Lhcq;->c:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lhcq;->e:Landroid/graphics/Paint;

    .line 46
    iget-object v0, p0, Lhcq;->e:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/DashPathEffect;

    sget-object v2, Lhcq;->a:[F

    invoke-direct {v1, v2, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 48
    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Lhcq;->b:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lhcq;->d:Landroid/graphics/Paint;

    .line 49
    iget-object v0, p0, Lhcq;->d:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/DashPathEffect;

    sget-object v2, Lhcq;->a:[F

    invoke-direct {v1, v2, v4}, Landroid/graphics/DashPathEffect;-><init>([FF)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 50
    return-void
.end method


# virtual methods
.method public a(FFFZLandroid/graphics/RectF;)V
    .locals 4

    .prologue
    .line 101
    if-nez p5, :cond_0

    .line 108
    :goto_0
    return-void

    .line 105
    :cond_0
    sub-float v0, p1, p3

    sub-float v1, p2, p3

    add-float v2, p1, p3

    add-float v3, p2, p3

    invoke-virtual {p5, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 107
    if-eqz p4, :cond_1

    iget-object v0, p0, Lhcq;->b:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    iget-object v1, p0, Lhcq;->c:Landroid/graphics/Paint;

    invoke-virtual {v1}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    :goto_1
    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    neg-float v1, v0

    neg-float v0, v0

    invoke-virtual {p5, v1, v0}, Landroid/graphics/RectF;->inset(FF)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lhcq;->b:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v0

    goto :goto_1
.end method

.method public a(Landroid/graphics/Canvas;FFF)V
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lhcq;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, p3, p4, v0}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lhcq;->b:Landroid/graphics/Paint;

    invoke-virtual {p1, p2, p3, p4, v0}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 88
    return-void
.end method

.method public a(Landroid/graphics/Canvas;FFFF)V
    .locals 6

    .prologue
    .line 58
    iget-object v5, p0, Lhcq;->c:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    iget-object v5, p0, Lhcq;->b:Landroid/graphics/Paint;

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 59
    return-void
.end method

.method public b(Landroid/graphics/Canvas;FFFF)V
    .locals 3

    .prologue
    .line 116
    iget-object v0, p0, Lhcq;->b:Landroid/graphics/Paint;

    iget-object v1, p0, Lhcq;->c:Landroid/graphics/Paint;

    iget-object v2, p0, Lhcq;->f:Landroid/graphics/RectF;

    invoke-virtual {v2, p2, p3, p4, p5}, Landroid/graphics/RectF;->set(FFFF)V

    iget-object v2, p0, Lhcq;->f:Landroid/graphics/RectF;

    invoke-virtual {p1, v2, v1}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    iget-object v1, p0, Lhcq;->f:Landroid/graphics/RectF;

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->drawOval(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 117
    return-void
.end method
