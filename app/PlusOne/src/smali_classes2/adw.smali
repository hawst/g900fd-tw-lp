.class public final Ladw;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/media/effect/EffectUpdateListener;


# instance fields
.field private synthetic a:Landroidx/media/filterpacks/face/FaceTrackerFilter;


# direct methods
.method public constructor <init>(Landroidx/media/filterpacks/face/FaceTrackerFilter;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Ladw;->a:Landroidx/media/filterpacks/face/FaceTrackerFilter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onEffectUpdated(Landroid/media/effect/Effect;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 114
    iget-object v1, p0, Ladw;->a:Landroidx/media/filterpacks/face/FaceTrackerFilter;

    monitor-enter v1

    .line 115
    :try_start_0
    iget-object v0, p0, Ladw;->a:Landroidx/media/filterpacks/face/FaceTrackerFilter;

    check-cast p2, [Landroid/hardware/Camera$Face;

    invoke-static {v0, p2}, Landroidx/media/filterpacks/face/FaceTrackerFilter;->a(Landroidx/media/filterpacks/face/FaceTrackerFilter;[Landroid/hardware/Camera$Face;)[Landroid/hardware/Camera$Face;

    .line 116
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
