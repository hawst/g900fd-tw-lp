.class public final Lbce;
.super Lloj;
.source "PG"

# interfaces
.implements Lami;
.implements Lcdw;


# static fields
.field private static final Q:Lamj;


# instance fields
.field private R:Lamb;

.field private S:Lamj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lamj;

    .line 29
    invoke-static {v0}, Lcgf;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamj;

    sput-object v0, Lbce;->Q:Lamj;

    .line 28
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lloj;-><init>()V

    .line 33
    sget-object v0, Lbce;->Q:Lamj;

    iput-object v0, p0, Lbce;->S:Lamj;

    return-void
.end method

.method static synthetic a(Lbce;)Lamj;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lbce;->S:Lamj;

    return-object v0
.end method


# virtual methods
.method public a(Lamj;)V
    .locals 1

    .prologue
    .line 82
    if-nez p1, :cond_0

    .line 83
    sget-object v0, Lbce;->Q:Lamj;

    iput-object v0, p0, Lbce;->S:Lamj;

    .line 87
    :goto_0
    return-void

    .line 85
    :cond_0
    iput-object p1, p0, Lbce;->S:Lamj;

    goto :goto_0
.end method

.method public ae_()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lbce;->R:Lamb;

    invoke-virtual {v0, p0}, Lamb;->b(Lami;)V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lbce;->R:Lamb;

    .line 52
    invoke-super {p0}, Lloj;->ae_()V

    .line 53
    return-void
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 57
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lbce;->n()Lz;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 58
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0132

    .line 59
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0133

    .line 60
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0134

    new-instance v2, Lbcg;

    invoke-direct {v2, p0}, Lbcg;-><init>(Lbce;)V

    .line 61
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a0135

    new-instance v2, Lbcf;

    invoke-direct {v2, p0}, Lbcf;-><init>(Lbce;)V

    .line 68
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 75
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 76
    invoke-virtual {v0, v3}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 77
    return-object v0
.end method

.method public d()Lcdz;
    .locals 1

    .prologue
    .line 37
    sget-object v0, Lcdz;->k:Lcdz;

    return-object v0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 42
    invoke-super {p0, p1}, Lloj;->d(Landroid/os/Bundle;)V

    .line 44
    invoke-static {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->b(Lu;)Lcom/google/android/apps/moviemaker/MovieMakerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o()Laol;

    move-result-object v0

    invoke-virtual {v0}, Laol;->i()Lamb;

    move-result-object v0

    iput-object v0, p0, Lbce;->R:Lamb;

    .line 45
    iget-object v0, p0, Lbce;->R:Lamb;

    invoke-virtual {v0, p0}, Lamb;->a(Lami;)V

    .line 46
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lbce;->S:Lamj;

    invoke-interface {v0}, Lamj;->b()V

    .line 92
    invoke-super {p0, p1}, Lloj;->onCancel(Landroid/content/DialogInterface;)V

    .line 93
    return-void
.end method
