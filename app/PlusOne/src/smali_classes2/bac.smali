.class public final Lbac;
.super Layr;
.source "PG"


# instance fields
.field private a:Lbaf;

.field private b:Landroid/animation/TimeInterpolator;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 34
    new-instance v0, Lays;

    invoke-direct {v0}, Lays;-><init>()V

    const v1, 0x7f020432

    .line 35
    invoke-virtual {v0, v1}, Lays;->a(I)Lays;

    move-result-object v0

    const v1, 0x7f0205d0

    .line 36
    invoke-virtual {v0, v1}, Lays;->b(I)Lays;

    move-result-object v0

    const/high16 v1, 0x3e800000    # 0.25f

    .line 37
    invoke-virtual {v0, v1}, Lays;->a(F)Lays;

    move-result-object v0

    const/4 v1, 0x1

    .line 38
    invoke-virtual {v0, v1}, Lays;->c(I)Lays;

    move-result-object v0

    .line 34
    invoke-direct {p0, v0}, Layr;-><init>(Lays;)V

    .line 29
    new-instance v0, Lbao;

    const-wide/32 v2, 0x7a120

    invoke-direct {v0, v2, v3}, Lbao;-><init>(J)V

    iput-object v0, p0, Lbac;->a:Lbaf;

    .line 31
    new-instance v0, Lbai;

    invoke-direct {v0}, Lbai;-><init>()V

    iput-object v0, p0, Lbac;->b:Landroid/animation/TimeInterpolator;

    .line 40
    return-void
.end method


# virtual methods
.method protected a(Layj;Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 45
    new-instance v0, Layu;

    invoke-direct {v0}, Layu;-><init>()V

    .line 46
    invoke-virtual {v0, p1}, Layu;->a(Layj;)Layu;

    move-result-object v0

    const v1, 0x7f0b00b1

    .line 47
    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Layu;->a(I)Layu;

    move-result-object v0

    const/high16 v1, 0x42100000    # 36.0f

    .line 48
    invoke-virtual {v0, v1}, Layu;->a(F)Layu;

    move-result-object v0

    const/high16 v1, 0x42300000    # 44.0f

    .line 49
    invoke-virtual {v0, v1}, Layu;->b(F)Layu;

    move-result-object v0

    const-string v1, "sans-serif"

    .line 50
    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Layu;->a(Landroid/graphics/Typeface;)Layu;

    move-result-object v0

    .line 51
    invoke-virtual {v0, v2}, Layu;->a(Z)Layu;

    move-result-object v0

    const/16 v1, 0x32

    .line 52
    invoke-virtual {v0, v1}, Layu;->c(I)Layu;

    move-result-object v0

    .line 53
    invoke-virtual {v0, v3}, Layu;->b(Z)Layu;

    move-result-object v0

    .line 54
    new-instance v1, Lazp;

    const v2, 0x7f0205d1

    invoke-direct {v1, v0, v2, v3}, Lazp;-><init>(Layu;IZ)V

    return-object v1
.end method

.method public b(Lbmd;)Lool;
    .locals 9

    .prologue
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    .line 60
    iget-object v2, p1, Lbmd;->d:Lbmg;

    sget-object v3, Lbmg;->c:Lbmg;

    if-eq v2, v3, :cond_0

    .line 61
    const/4 v0, 0x0

    .line 63
    :goto_0
    return-object v0

    :cond_0
    const-wide v2, 0x3fa999999999999aL    # 0.05

    const-wide/16 v4, 0x0

    move-wide v6, v0

    move-object v8, p1

    invoke-static/range {v0 .. v8}, Lbah;->a(DDDDLbmd;)Lool;

    move-result-object v0

    goto :goto_0
.end method

.method protected f()Layj;
    .locals 1

    .prologue
    .line 69
    new-instance v0, Lbad;

    invoke-direct {v0, p0}, Lbad;-><init>(Layj;)V

    return-object v0
.end method

.method public j(Layl;)I
    .locals 1

    .prologue
    .line 74
    const v0, 0x7f020595

    return v0
.end method

.method public l(Layl;)F
    .locals 2

    .prologue
    .line 84
    iget-boolean v0, p1, Layl;->w:Z

    if-nez v0, :cond_0

    invoke-static {p1}, Lbag;->e(Layl;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 85
    :cond_0
    const/4 v0, 0x0

    .line 87
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lbac;->b:Landroid/animation/TimeInterpolator;

    iget-object v1, p0, Lbac;->a:Lbaf;

    invoke-virtual {v1, p1}, Lbaf;->b(Layl;)F

    move-result v1

    invoke-interface {v0, v1}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    goto :goto_0
.end method

.method public m(Layl;)I
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x4

    return v0
.end method
