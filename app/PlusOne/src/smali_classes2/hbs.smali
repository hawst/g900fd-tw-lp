.class public abstract Lhbs;
.super Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
.source "PG"


# static fields
.field public static final g:[I

.field private static h:Ljava/lang/Integer;

.field private static i:Ljava/lang/Integer;

.field private static j:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/16 v0, 0x32

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lhbs;->h:Ljava/lang/Integer;

    .line 10
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lhbs;->i:Ljava/lang/Integer;

    .line 11
    const/16 v0, 0x64

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lhbs;->j:Ljava/lang/Integer;

    .line 13
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lhbs;->g:[I

    return-void

    :array_0
    .array-data 4
        0x1f5
        0x1f6
        0x4
        0xca
        0xcb
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 39
    invoke-static {p1}, Lhbs;->h(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    invoke-super {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 55
    :goto_0
    return-object v0

    .line 43
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 55
    sget-object v0, Lhbs;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 46
    :sswitch_0
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    .line 49
    :sswitch_1
    sget-object v0, Lhbs;->i:Ljava/lang/Integer;

    goto :goto_0

    .line 52
    :sswitch_2
    const/high16 v0, -0x80000000

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 43
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0xc9 -> :sswitch_2
        0x1f5 -> :sswitch_0
        0x1f6 -> :sswitch_0
    .end sparse-switch
.end method

.method public b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 61
    invoke-static {p1}, Lhbs;->h(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    invoke-super {p0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b(I)Ljava/lang/Object;

    move-result-object v0

    .line 77
    :goto_0
    return-object v0

    .line 65
    :cond_0
    sparse-switch p1, :sswitch_data_0

    .line 77
    sget-object v0, Lhbs;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 68
    :sswitch_0
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0

    .line 71
    :sswitch_1
    sget-object v0, Lhbs;->j:Ljava/lang/Integer;

    goto :goto_0

    .line 74
    :sswitch_2
    const v0, 0x7fffffff

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 65
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0xc9 -> :sswitch_2
        0x1f5 -> :sswitch_0
        0x1f6 -> :sswitch_0
    .end sparse-switch
.end method

.method public c(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x4

    if-ne p1, v0, :cond_0

    sget-object v0, Lhbs;->h:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
