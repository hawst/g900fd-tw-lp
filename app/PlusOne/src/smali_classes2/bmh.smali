.class public final Lbmh;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lbmh;


# instance fields
.field private final b:Lbms;

.field private final c:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Lbxi;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lbbs;

.field private final e:[F

.field private final f:Lbmi;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 35
    new-instance v7, Lbmh;

    new-instance v6, Landroid/util/LongSparseArray;

    invoke-direct {v6}, Landroid/util/LongSparseArray;-><init>()V

    const/4 v8, 0x0

    sget-object v9, Lbms;->a:Lbms;

    sget-object v10, Lbqu;->a:[F

    new-instance v0, Lbmj;

    move v2, v1

    move v3, v1

    move v4, v1

    move v5, v1

    invoke-direct/range {v0 .. v5}, Lbmj;-><init>(FFFFF)V

    move-object v1, v7

    move-object v2, v6

    move-object v3, v8

    move-object v4, v9

    move-object v5, v10

    move-object v6, v0

    invoke-direct/range {v1 .. v6}, Lbmh;-><init>(Landroid/util/LongSparseArray;Lbbs;Lbms;[FLbmi;)V

    sput-object v7, Lbmh;->a:Lbmh;

    return-void
.end method

.method constructor <init>(Landroid/util/LongSparseArray;Lbbs;Lbms;[FLbmi;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Lbxi;",
            ">;",
            "Lbbs;",
            "Lbms;",
            "[F",
            "Lbmi;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    const-string v0, "stabilizationMetrics"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/LongSparseArray;

    iput-object v0, p0, Lbmh;->c:Landroid/util/LongSparseArray;

    .line 63
    iput-object p2, p0, Lbmh;->d:Lbbs;

    .line 64
    const-string v0, "keyFrameMetrics"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbms;

    iput-object v0, p0, Lbmh;->b:Lbms;

    .line 65
    const-string v0, "rotationTransformationMatrix"

    .line 66
    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    iput-object v0, p0, Lbmh;->e:[F

    .line 67
    const-string v0, "cropSpecification"

    invoke-static {p5, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmi;

    iput-object v0, p0, Lbmh;->f:Lbmi;

    .line 68
    return-void
.end method

.method public static a(Lbmd;Lont;II)Lbmh;
    .locals 6

    .prologue
    .line 136
    invoke-virtual {p0}, Lbmd;->c()Look;

    move-result-object v0

    invoke-static {v0}, Lblx;->a(Look;)Lbmi;

    move-result-object v5

    .line 137
    if-nez v5, :cond_0

    .line 138
    invoke-static {p1, p2, p3}, Lbpp;->a(Lont;II)Lbmj;

    move-result-object v5

    .line 141
    :cond_0
    if-nez v5, :cond_1

    sget-object v0, Lbmh;->a:Lbmh;

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lbmh;

    new-instance v1, Landroid/util/LongSparseArray;

    invoke-direct {v1}, Landroid/util/LongSparseArray;-><init>()V

    const/4 v2, 0x0

    sget-object v3, Lbms;->a:Lbms;

    sget-object v4, Lbqu;->a:[F

    invoke-direct/range {v0 .. v5}, Lbmh;-><init>(Landroid/util/LongSparseArray;Lbbs;Lbms;[FLbmi;)V

    goto :goto_0
.end method

.method public static a(Look;Lont;II)Lbmh;
    .locals 6

    .prologue
    .line 121
    invoke-static {p0}, Lblx;->a(Look;)Lbmi;

    move-result-object v5

    .line 122
    if-nez v5, :cond_0

    .line 123
    invoke-static {p1, p2, p3}, Lbpp;->a(Lont;II)Lbmj;

    move-result-object v5

    .line 126
    :cond_0
    if-nez v5, :cond_1

    sget-object v0, Lbmh;->a:Lbmh;

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lbmh;

    new-instance v1, Landroid/util/LongSparseArray;

    invoke-direct {v1}, Landroid/util/LongSparseArray;-><init>()V

    const/4 v2, 0x0

    sget-object v3, Lbms;->a:Lbms;

    sget-object v4, Lbqu;->a:[F

    invoke-direct/range {v0 .. v5}, Lbmh;-><init>(Landroid/util/LongSparseArray;Lbbs;Lbms;[FLbmi;)V

    goto :goto_0
.end method

.method public static a(Ljava/util/List;Ljava/util/Map;II)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;II)",
            "Ljava/util/List",
            "<",
            "Lbmh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 102
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 103
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 111
    :goto_0
    return-object v2

    .line 105
    :cond_0
    new-instance v14, Ljava/util/ArrayList;

    .line 106
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v14, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 107
    const/4 v2, 0x0

    move v11, v2

    :goto_1
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v11, v2, :cond_6

    .line 109
    move-object/from16 v0, p0

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Lbmd;

    .line 108
    iget-object v2, v10, Lbmd;->d:Lbmg;

    sget-object v3, Lbmg;->d:Lbmg;

    if-ne v2, v3, :cond_1

    sget-object v2, Lbmh;->a:Lbmh;

    :goto_2
    invoke-interface {v14, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 107
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    goto :goto_1

    .line 108
    :cond_1
    iget-object v2, v10, Lbmd;->e:Ljeg;

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "displayable"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Lbml;

    invoke-interface {v4}, Lbml;->d()Lbmy;

    move-result-object v2

    const-string v3, "playbackMetrics"

    const/4 v5, 0x0

    invoke-static {v2, v3, v5}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbmy;

    invoke-interface {v4}, Lbml;->b()Lbmu;

    move-result-object v2

    iget v5, v2, Lbmu;->a:I

    const/16 v6, 0x5a

    if-eq v5, v6, :cond_2

    iget v5, v2, Lbmu;->a:I

    const/16 v6, 0x10e

    if-ne v5, v6, :cond_4

    :cond_2
    iget v5, v2, Lbmu;->d:I

    iget v2, v2, Lbmu;->c:I

    :goto_3
    int-to-float v5, v5

    int-to-float v2, v2

    div-float v2, v5, v2

    move/from16 v0, p2

    int-to-float v5, v0

    move/from16 v0, p3

    int-to-float v6, v0

    div-float/2addr v5, v6

    div-float/2addr v2, v5

    invoke-interface {v4}, Lbml;->b()Lbmu;

    move-result-object v4

    invoke-static {v4}, Lbqu;->a(Lbmu;)[F

    move-result-object v15

    invoke-virtual {v10}, Lbmd;->c()Look;

    move-result-object v4

    invoke-static {v4}, Lblx;->a(Look;)Lbmi;

    move-result-object v7

    if-nez v7, :cond_3

    const/4 v13, 0x0

    const/4 v12, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x0

    iget-object v4, v10, Lbmd;->f:Lbmp;

    iget-wide v4, v4, Lbmp;->b:J

    iget-object v6, v10, Lbmd;->f:Lbmp;

    iget-wide v6, v6, Lbmp;->c:J

    invoke-static/range {v2 .. v7}, Lbpp;->a(FLbmy;JJ)F

    move-result v4

    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v5, v2, v5

    if-lez v5, :cond_5

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f800000    # 1.0f

    div-float v2, v6, v2

    sub-float v2, v5, v2

    sub-float v6, v2, v4

    move v7, v9

    move v5, v4

    :goto_4
    new-instance v4, Lbmj;

    const/4 v9, 0x0

    invoke-direct/range {v4 .. v9}, Lbmj;-><init>(FFFFF)V

    move-object v7, v4

    :cond_3
    new-instance v2, Lbmh;

    iget-object v4, v10, Lbmd;->f:Lbmp;

    invoke-virtual {v3}, Lbmy;->b()Landroid/util/LongSparseArray;

    move-result-object v5

    invoke-static {v4, v5}, Lcfk;->a(Lbmp;Landroid/util/LongSparseArray;)Landroid/util/LongSparseArray;

    move-result-object v6

    invoke-virtual {v3}, Lbmy;->d()Lbbs;

    move-result-object v4

    invoke-virtual {v3}, Lbmy;->a()Lbms;

    move-result-object v3

    iget-object v5, v10, Lbmd;->f:Lbmp;

    invoke-virtual {v3, v5}, Lbms;->a(Lbmp;)Lbms;

    move-result-object v5

    move-object v3, v6

    move-object v6, v15

    invoke-direct/range {v2 .. v7}, Lbmh;-><init>(Landroid/util/LongSparseArray;Lbbs;Lbms;[FLbmi;)V

    goto/16 :goto_2

    :cond_4
    iget v5, v2, Lbmu;->c:I

    iget v2, v2, Lbmu;->d:I

    goto :goto_3

    :cond_5
    const/high16 v5, 0x3f800000    # 1.0f

    sub-float v2, v5, v2

    sub-float v7, v2, v4

    move v8, v4

    move v6, v12

    move v5, v13

    goto :goto_4

    :cond_6
    move-object v2, v14

    .line 111
    goto/16 :goto_0
.end method


# virtual methods
.method public a(J)Lbxi;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lbmh;->c:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxi;

    return-object v0
.end method

.method public a()[F
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lbmh;->e:[F

    return-object v0
.end method

.method public b()Lbmi;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lbmh;->f:Lbmi;

    return-object v0
.end method

.method public b(J)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lbmh;->b:Lbms;

    invoke-virtual {v0, p1, p2}, Lbms;->d(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public c()Lbbs;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lbmh;->d:Lbbs;

    return-object v0
.end method
