.class public final Lkbb;
.super Lkbl;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lkbl;-><init>(Landroid/content/Context;)V

    .line 38
    return-void
.end method


# virtual methods
.method public b()V
    .locals 5

    .prologue
    .line 50
    invoke-virtual {p0}, Lkbb;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 51
    iget-object v0, p0, Lkbb;->b:Lkzw;

    invoke-virtual {v0}, Lkzw;->j()[Lkzx;

    move-result-object v2

    .line 52
    const/4 v0, 0x0

    :goto_0
    iget v3, p0, Lkbb;->r:I

    if-ge v0, v3, :cond_0

    .line 53
    invoke-virtual {p0, v1, v0}, Lkbb;->f(Landroid/content/Context;I)Landroid/view/View;

    .line 54
    invoke-virtual {p0, v1, v0}, Lkbb;->e(Landroid/content/Context;I)Landroid/view/View;

    .line 55
    invoke-virtual {p0, v1, v0}, Lkbb;->c(Landroid/content/Context;I)Landroid/widget/TextView;

    move-result-object v3

    aget-object v4, v2, v0

    invoke-virtual {v4}, Lkzx;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 56
    invoke-virtual {p0, v1, v0}, Lkbb;->d(Landroid/content/Context;I)Landroid/widget/TextView;

    .line 52
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 58
    :cond_0
    iget-object v0, p0, Lkbb;->p:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lkbb;->addView(Landroid/view/View;)V

    .line 59
    return-void
.end method

.method c(I)V
    .locals 4

    .prologue
    .line 171
    invoke-super {p0, p1}, Lkbl;->c(I)V

    .line 172
    iget-object v0, p0, Lkbb;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 174
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    .line 173
    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 175
    iget-object v1, p0, Lkbb;->m:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 176
    const/4 v3, -0x2

    invoke-virtual {v1, v3, v2}, Landroid/widget/TextView;->measure(II)V

    .line 179
    sget v2, Lkbb;->k:I

    const/4 v3, 0x0

    invoke-virtual {p0, v1, v0, v2, v3}, Lkbb;->b(Landroid/view/View;Landroid/view/View;II)V

    .line 182
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 117
    sget-object v0, Lkbb;->a:Llct;

    iget v0, v0, Llct;->m:I

    .line 118
    sget v5, Lkbb;->i:I

    .line 120
    iget-object v1, p0, Lkbb;->b:Lkzw;

    invoke-virtual {v1}, Lkzw;->f()I

    move-result v6

    move v1, v2

    move v3, v0

    .line 121
    :goto_0
    iget v0, p0, Lkbb;->r:I

    if-ge v1, v0, :cond_1

    .line 123
    if-lez v1, :cond_3

    .line 124
    sget v0, Lkbl;->j:I

    move v4, v0

    .line 127
    :goto_1
    iget-object v0, p0, Lkbb;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 128
    add-int v7, v3, v4

    .line 131
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v8, v5

    .line 132
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v3

    add-int/2addr v9, v4

    .line 128
    invoke-virtual {v0, v5, v7, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 136
    iget-object v0, p0, Lkbb;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 137
    add-int v7, v3, v4

    .line 140
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v8, v5

    .line 141
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v3

    add-int/2addr v9, v4

    .line 137
    invoke-virtual {v0, v5, v7, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 145
    invoke-virtual {p0, v1}, Lkbb;->c(I)V

    .line 148
    iget-object v0, p0, Lkbb;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 149
    invoke-virtual {p0}, Lkbb;->m()Z

    move-result v7

    if-nez v7, :cond_0

    .line 150
    add-int v7, v3, v4

    .line 153
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v8, v5

    .line 154
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v3

    add-int/2addr v9, v4

    .line 150
    invoke-virtual {v0, v5, v7, v8, v9}, Landroid/widget/TextView;->layout(IIII)V

    .line 159
    :goto_2
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    add-int/2addr v0, v4

    add-int/2addr v3, v0

    .line 121
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 157
    :cond_0
    invoke-virtual {p0, v1}, Lkbb;->d(I)V

    goto :goto_2

    .line 162
    :cond_1
    const/4 v0, -0x1

    if-eq v6, v0, :cond_2

    .line 163
    iget-object v0, p0, Lkbb;->b:Lkzw;

    invoke-virtual {v0}, Lkzw;->f()I

    move-result v0

    invoke-virtual {p0, v0}, Lkbb;->f(I)V

    .line 165
    :cond_2
    invoke-virtual {p0}, Lkbb;->d()V

    .line 166
    iget v0, p0, Lkbb;->c:I

    int-to-float v1, v0

    iget-object v0, p0, Lkbb;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v1, v0

    iput v0, p0, Lkbb;->t:F

    .line 167
    return-void

    :cond_3
    move v4, v2

    goto/16 :goto_1
.end method

.method protected onMeasure(II)V
    .locals 11

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/4 v3, 0x0

    const/4 v9, -0x2

    .line 63
    iget-object v0, p0, Lkbb;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, v9, v9}, Landroid/widget/ImageView;->measure(II)V

    .line 64
    iget v0, p0, Lkbb;->c:I

    sget v1, Lkbb;->i:I

    shl-int/lit8 v1, v1, 0x1

    sub-int v6, v0, v1

    .line 65
    invoke-static {v6, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    move v2, v3

    move v4, v3

    .line 69
    :goto_0
    iget v0, p0, Lkbb;->r:I

    if-ge v2, v0, :cond_1

    .line 70
    iget v0, p0, Lkbb;->q:I

    if-ne v2, v0, :cond_0

    iget-object v0, p0, Lkbb;->p:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    move v5, v0

    .line 71
    :goto_1
    iget-object v0, p0, Lkbb;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 72
    invoke-virtual {v0, v9, v9}, Landroid/widget/TextView;->measure(II)V

    .line 74
    iget-object v1, p0, Lkbb;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 76
    sub-int v5, v6, v5

    sget v8, Lkbb;->k:I

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v5, v8

    .line 79
    invoke-virtual {v0}, Landroid/widget/TextView;->getWidth()I

    move-result v0

    sub-int v0, v5, v0

    .line 76
    invoke-static {v0, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 81
    invoke-virtual {v1, v0, v9}, Landroid/widget/TextView;->measure(II)V

    .line 83
    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v0

    if-le v0, v4, :cond_3

    .line 84
    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    .line 69
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v4, v1

    goto :goto_0

    :cond_0
    move v5, v3

    .line 70
    goto :goto_1

    .line 88
    :cond_1
    :goto_3
    iget v0, p0, Lkbb;->r:I

    if-ge v3, v0, :cond_2

    .line 90
    iget-object v0, p0, Lkbb;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 92
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    invoke-static {v1, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 93
    invoke-static {v4, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 97
    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->measure(II)V

    .line 101
    iget-object v0, p0, Lkbb;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v7, v2}, Landroid/view/View;->measure(II)V

    .line 106
    iget-object v0, p0, Lkbb;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v9, v2}, Landroid/view/View;->measure(II)V

    .line 89
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 109
    :cond_2
    iget v0, p0, Lkbb;->r:I

    mul-int/2addr v0, v4

    add-int/lit8 v0, v0, 0x0

    .line 110
    sget v1, Lkbb;->j:I

    iget v2, p0, Lkbb;->r:I

    add-int/lit8 v2, v2, -0x1

    mul-int/2addr v1, v2

    sget-object v2, Lkbb;->a:Llct;

    iget v2, v2, Llct;->m:I

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    .line 112
    iget v1, p0, Lkbb;->c:I

    invoke-virtual {p0, v1, v0}, Lkbb;->setMeasuredDimension(II)V

    .line 113
    return-void

    :cond_3
    move v1, v4

    goto :goto_2
.end method
