.class final Lbsg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbtq;


# instance fields
.field private final a:Ljava/lang/Thread;

.field private synthetic b:Lbsc;


# direct methods
.method public constructor <init>(Lbsc;Ljava/lang/Thread;)V
    .locals 2

    .prologue
    .line 258
    iput-object p1, p0, Lbsg;->b:Lbsc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 259
    const-string v0, "playerThread"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Thread;

    iput-object v0, p0, Lbsg;->a:Ljava/lang/Thread;

    .line 260
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Lbsg;->a:Ljava/lang/Thread;

    iget-object v1, p0, Lbsg;->b:Lbsc;

    invoke-static {v1}, Lbsc;->g(Lbsc;)Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbsg;->b:Lbsc;

    invoke-static {v0}, Lbsc;->e(Lbsc;)Lbtq;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lbsg;->b:Lbsc;

    invoke-static {v0}, Lbsc;->e(Lbsc;)Lbtq;

    move-result-object v0

    invoke-interface {v0}, Lbtq;->a()V

    .line 267
    :cond_0
    return-void
.end method

.method public a(IZ)V
    .locals 2

    .prologue
    .line 292
    iget-object v0, p0, Lbsg;->a:Ljava/lang/Thread;

    iget-object v1, p0, Lbsg;->b:Lbsc;

    invoke-static {v1}, Lbsc;->g(Lbsc;)Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 293
    iget-object v0, p0, Lbsg;->b:Lbsc;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbsc;->a(Lbsc;Ljava/lang/Thread;)Ljava/lang/Thread;

    .line 294
    iget-object v0, p0, Lbsg;->b:Lbsc;

    invoke-static {v0}, Lbsc;->e(Lbsc;)Lbtq;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lbsg;->b:Lbsc;

    invoke-static {v0}, Lbsc;->e(Lbsc;)Lbtq;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lbtq;->a(IZ)V

    .line 298
    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 278
    iget-object v0, p0, Lbsg;->a:Ljava/lang/Thread;

    iget-object v1, p0, Lbsg;->b:Lbsc;

    invoke-static {v1}, Lbsc;->g(Lbsc;)Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbsg;->b:Lbsc;

    invoke-static {v0}, Lbsc;->e(Lbsc;)Lbtq;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lbsg;->b:Lbsc;

    invoke-static {v0}, Lbsc;->e(Lbsc;)Lbtq;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lbtq;->a(J)V

    .line 281
    :cond_0
    return-void
.end method

.method public a(JLandroid/graphics/RectF;)V
    .locals 3

    .prologue
    .line 309
    iget-object v0, p0, Lbsg;->a:Ljava/lang/Thread;

    iget-object v1, p0, Lbsg;->b:Lbsc;

    invoke-static {v1}, Lbsc;->g(Lbsc;)Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbsg;->b:Lbsc;

    invoke-static {v0}, Lbsc;->e(Lbsc;)Lbtq;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lbsg;->b:Lbsc;

    invoke-static {v0}, Lbsc;->e(Lbsc;)Lbtq;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3}, Lbtq;->a(JLandroid/graphics/RectF;)V

    .line 312
    :cond_0
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 271
    iget-object v0, p0, Lbsg;->a:Ljava/lang/Thread;

    iget-object v1, p0, Lbsg;->b:Lbsc;

    invoke-static {v1}, Lbsc;->g(Lbsc;)Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbsg;->b:Lbsc;

    invoke-static {v0}, Lbsc;->e(Lbsc;)Lbtq;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lbsg;->b:Lbsc;

    invoke-static {v0}, Lbsc;->e(Lbsc;)Lbtq;

    move-result-object v0

    invoke-interface {v0, p1}, Lbtq;->a(Landroid/graphics/Bitmap;)V

    .line 274
    :cond_0
    return-void
.end method

.method public a(Lcdx;)V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lbsg;->a:Ljava/lang/Thread;

    iget-object v1, p0, Lbsg;->b:Lbsc;

    invoke-static {v1}, Lbsc;->g(Lbsc;)Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbsg;->b:Lbsc;

    invoke-static {v0}, Lbsc;->e(Lbsc;)Lbtq;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lbsg;->b:Lbsc;

    invoke-static {v0}, Lbsc;->e(Lbsc;)Lbtq;

    move-result-object v0

    invoke-interface {v0, p1}, Lbtq;->a(Lcdx;)V

    .line 319
    :cond_0
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 302
    iget-object v0, p0, Lbsg;->a:Ljava/lang/Thread;

    iget-object v1, p0, Lbsg;->b:Lbsc;

    invoke-static {v1}, Lbsc;->g(Lbsc;)Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbsg;->b:Lbsc;

    invoke-static {v0}, Lbsc;->e(Lbsc;)Lbtq;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lbsg;->b:Lbsc;

    invoke-static {v0}, Lbsc;->e(Lbsc;)Lbtq;

    move-result-object v0

    invoke-interface {v0}, Lbtq;->b()V

    .line 305
    :cond_0
    return-void
.end method

.method public b(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 285
    iget-object v0, p0, Lbsg;->a:Ljava/lang/Thread;

    iget-object v1, p0, Lbsg;->b:Lbsc;

    invoke-static {v1}, Lbsc;->g(Lbsc;)Ljava/lang/Thread;

    move-result-object v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbsg;->b:Lbsc;

    invoke-static {v0}, Lbsc;->e(Lbsc;)Lbtq;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 286
    iget-object v0, p0, Lbsg;->b:Lbsc;

    invoke-static {v0}, Lbsc;->e(Lbsc;)Lbtq;

    move-result-object v0

    invoke-interface {v0, p1}, Lbtq;->b(Landroid/graphics/Bitmap;)V

    .line 288
    :cond_0
    return-void
.end method
