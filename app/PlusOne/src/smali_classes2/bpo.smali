.class public final Lbpo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lbmd;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "+",
            "Lbml;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "+",
            "Lbml;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 424
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 425
    const-string v0, "displayableByMediaId"

    .line 426
    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lbpo;->a:Ljava/util/Map;

    .line 427
    return-void
.end method


# virtual methods
.method public a(Lbmd;Lbmd;)I
    .locals 8

    .prologue
    const-wide/16 v2, 0x0

    .line 431
    iget-object v0, p0, Lbpo;->a:Ljava/util/Map;

    iget-object v1, p1, Lbmd;->e:Ljeg;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbml;

    invoke-interface {v0}, Lbml;->b()Lbmu;

    move-result-object v4

    .line 432
    iget-object v0, p0, Lbpo;->a:Ljava/util/Map;

    iget-object v1, p2, Lbmd;->e:Ljeg;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbml;

    invoke-interface {v0}, Lbml;->b()Lbmu;

    move-result-object v5

    .line 434
    iget-object v0, v4, Lbmu;->e:Lbmv;

    sget-object v1, Lbmv;->a:Lbmv;

    if-ne v0, v1, :cond_1

    .line 435
    invoke-virtual {v4}, Lbmu;->a()Lboo;

    move-result-object v0

    iget-wide v0, v0, Lboo;->f:J

    .line 436
    :goto_0
    iget-object v6, v5, Lbmu;->e:Lbmv;

    sget-object v7, Lbmv;->a:Lbmv;

    if-ne v6, v7, :cond_0

    .line 437
    invoke-virtual {v5}, Lbmu;->a()Lboo;

    move-result-object v2

    iget-wide v2, v2, Lboo;->f:J

    .line 440
    :cond_0
    iget-wide v6, v4, Lbmu;->b:J

    sub-long v0, v6, v0

    .line 441
    iget-wide v4, v5, Lbmu;->b:J

    sub-long v2, v4, v2

    .line 442
    iget-object v4, p1, Lbmd;->f:Lbmp;

    iget-wide v4, v4, Lbmp;->b:J

    add-long/2addr v0, v4

    iget-object v4, p2, Lbmd;->f:Lbmp;

    iget-wide v4, v4, Lbmp;->b:J

    add-long/2addr v2, v4

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Long;->signum(J)I

    move-result v0

    return v0

    :cond_1
    move-wide v0, v2

    .line 435
    goto :goto_0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 419
    check-cast p1, Lbmd;

    check-cast p2, Lbmd;

    invoke-virtual {p0, p1, p2}, Lbpo;->a(Lbmd;Lbmd;)I

    move-result v0

    return v0
.end method
