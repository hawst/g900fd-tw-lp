.class public final Lixv;
.super Llol;
.source "PG"

# interfaces
.implements Lhob;
.implements Liwj;
.implements Lixn;
.implements Liyj;


# instance fields
.field private N:Liwa;

.field private O:Lhei;

.field private P:Lhoc;

.field private Q:Lhov;

.field private R:I

.field private S:I

.field private T:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Llol;-><init>()V

    .line 46
    new-instance v0, Lhoc;

    iget-object v1, p0, Lixv;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    .line 48
    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    move-result-object v0

    iput-object v0, p0, Lixv;->P:Lhoc;

    .line 49
    new-instance v0, Lhov;

    iget-object v1, p0, Lixv;->av:Llqm;

    invoke-direct {v0, v1}, Lhov;-><init>(Llqr;)V

    iput-object v0, p0, Lixv;->Q:Lhov;

    .line 52
    const/4 v0, -0x1

    iput v0, p0, Lixv;->R:I

    .line 59
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lixv;->T:J

    return-void
.end method

.method static synthetic a(Lixv;)I
    .locals 1

    .prologue
    .line 34
    iget v0, p0, Lixv;->R:I

    return v0
.end method

.method static synthetic a(Lixv;J)J
    .locals 1

    .prologue
    .line 34
    iput-wide p1, p0, Lixv;->T:J

    return-wide p1
.end method

.method static synthetic b(Lixv;)Lhei;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lixv;->O:Lhei;

    return-object v0
.end method

.method static synthetic c(Lixv;)Liwa;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lixv;->N:Liwa;

    return-object v0
.end method

.method private e()V
    .locals 4

    .prologue
    .line 130
    iget-wide v0, p0, Lixv;->T:J

    const-wide/high16 v2, -0x8000000000000000L

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 131
    iget-wide v0, p0, Lixv;->T:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 132
    const-wide/16 v2, 0x0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 134
    iget-object v2, p0, Lixv;->Q:Lhov;

    new-instance v3, Lixw;

    invoke-direct {v3, p0}, Lixw;-><init>(Lixv;)V

    invoke-virtual {v2, v3, v0, v1}, Lhov;->a(Ljava/lang/Runnable;J)Lhox;

    .line 145
    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lixv;->N:Liwa;

    invoke-interface {v0}, Liwa;->c()V

    .line 208
    return-void
.end method

.method public a(I)V
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 92
    iput p1, p0, Lixv;->R:I

    .line 94
    iget-object v0, p0, Lixv;->O:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v6

    .line 95
    const-string v0, "account_name"

    invoke-interface {v6, v0}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 96
    const-string v0, "effective_gaia_id"

    invoke-interface {v6, v0}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 100
    const-string v0, "PlusiAccountUpdateExtension.oob_required"

    invoke-interface {v6, v0}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 101
    iget-object v9, p0, Lixv;->P:Lhoc;

    new-instance v0, Liyd;

    iget-object v1, p0, Lixv;->at:Llnl;

    const-string v2, "oob"

    const-string v10, "page_count"

    .line 104
    invoke-interface {v6, v10, v8}, Lhej;->a(Ljava/lang/String;I)I

    move-result v6

    if-lez v6, :cond_1

    :goto_0
    move-object v6, v5

    invoke-direct/range {v0 .. v8}, Liyd;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lnpx;[Lnqe;ZZ)V

    .line 101
    invoke-virtual {v9, v0}, Lhoc;->b(Lhny;)V

    .line 115
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v7, v8

    .line 104
    goto :goto_0

    .line 107
    :cond_2
    const-string v0, "gplus_skinny_page"

    invoke-interface {v6, v0}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 108
    iget-object v0, p0, Lixv;->N:Liwa;

    invoke-interface {v0}, Liwa;->e()V

    .line 109
    invoke-virtual {p0}, Lixv;->q()Lae;

    move-result-object v0

    invoke-static {v0, v3}, Liyk;->b(Lae;Ljava/lang/String;)V

    goto :goto_1

    .line 111
    :cond_3
    iget v0, p0, Lixv;->S:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_4

    :goto_2
    if-nez v8, :cond_0

    .line 112
    iget-object v0, p0, Lixv;->N:Liwa;

    const v1, 0x7f0a01ae

    invoke-virtual {p0, v1}, Lixv;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Liwa;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 111
    :cond_4
    iget v0, p0, Lixv;->S:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lixv;->S:I

    const-wide/16 v0, 0x7d0

    const-wide/16 v2, 0x1

    iget v4, p0, Lixv;->S:I

    shl-long/2addr v2, v4

    mul-long/2addr v0, v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lixv;->T:J

    invoke-direct {p0}, Lixv;->e()V

    move v8, v7

    goto :goto_2
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 72
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 73
    if-eqz p1, :cond_0

    .line 74
    const-string v0, "account_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lixv;->R:I

    .line 75
    const-string v0, "retry_count"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lixv;->S:I

    .line 76
    const-string v0, "next_retry_time_ms"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lixv;->T:J

    .line 78
    invoke-direct {p0}, Lixv;->e()V

    .line 80
    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 6

    .prologue
    .line 222
    const-string v0, "oob"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 223
    if-nez p2, :cond_1

    iget-object v0, p0, Lixv;->N:Liwa;

    invoke-interface {v0}, Liwa;->c()V

    .line 225
    :cond_0
    :goto_0
    return-void

    .line 223
    :cond_1
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lixv;->N:Liwa;

    invoke-interface {v0}, Liwa;->d()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lixv;->O:Lhei;

    iget v1, p0, Lixv;->R:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "effective_gaia_id"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v4

    const-string v0, "oob_response"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhyt;

    if-eqz v0, :cond_5

    iget-object v3, p0, Lixv;->N:Liwa;

    invoke-interface {v3}, Liwa;->e()V

    new-instance v3, Lnql;

    invoke-direct {v3}, Lnql;-><init>()V

    invoke-virtual {v0, v3}, Lhyt;->a(Loxu;)Loxu;

    move-result-object v3

    check-cast v3, Lnql;

    const-string v0, "allow_non_google_accounts"

    invoke-virtual {v4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    invoke-static {v3}, Lixm;->a(Lnql;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lixv;->at:Llnl;

    invoke-virtual {p0}, Lixv;->q()Lae;

    move-result-object v1

    invoke-static {v0, v1, v3, v5}, Lixm;->a(Landroid/content/Context;Lae;Lnql;Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lixv;->at:Llnl;

    const-class v4, Lieh;

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    sget-object v4, Liyr;->a:Lief;

    invoke-interface {v0, v4, v1}, Lieh;->b(Lief;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lixv;->q()Lae;

    move-result-object v0

    new-instance v2, Liza;

    invoke-direct {v2}, Liza;-><init>()V

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "AccountName"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Liza;->f(Landroid/os/Bundle;)V

    invoke-virtual {v0}, Lae;->a()Lat;

    move-result-object v0

    const-string v1, "signup"

    invoke-virtual {v0, v2, v1}, Lat;->a(Lu;Ljava/lang/String;)Lat;

    const-string v1, "signup"

    invoke-virtual {v0, v1}, Lat;->a(Ljava/lang/String;)Lat;

    invoke-virtual {v0}, Lat;->b()I

    goto/16 :goto_0

    :cond_4
    invoke-virtual {p0}, Lixv;->q()Lae;

    move-result-object v0

    const/4 v4, 0x0

    invoke-static/range {v0 .. v5}, Liyf;->a(Lae;Ljava/lang/String;Ljava/lang/String;Lnql;IZ)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lixv;->N:Liwa;

    invoke-interface {v0, v1, v2}, Liwa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public a(ZZ)V
    .locals 10

    .prologue
    .line 192
    iget-object v0, p0, Lixv;->O:Lhei;

    iget v1, p0, Lixv;->R:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 193
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 194
    const-string v1, "effective_gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 196
    iget-object v0, p0, Lixv;->N:Liwa;

    iget-object v1, p0, Lixv;->at:Llnl;

    const v2, 0x7f0a01a8

    invoke-virtual {v1, v2}, Llnl;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Liwa;->b(Ljava/lang/String;)V

    .line 197
    new-instance v5, Lnpx;

    invoke-direct {v5}, Lnpx;-><init>()V

    .line 198
    const/4 v0, 0x2

    iput v0, v5, Lnpx;->b:I

    .line 199
    iget-object v9, p0, Lixv;->P:Lhoc;

    new-instance v0, Liyd;

    iget-object v1, p0, Lixv;->at:Llnl;

    const-string v2, "oob"

    const/4 v6, 0x0

    move v7, p1

    move v8, p2

    invoke-direct/range {v0 .. v8}, Liyd;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lnpx;[Lnqe;ZZ)V

    invoke-virtual {v9, v0}, Lhoc;->b(Lhny;)V

    .line 203
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lixv;->au:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lixv;->O:Lhei;

    .line 64
    iget-object v0, p0, Lixv;->au:Llnh;

    const-class v1, Liwa;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwa;

    iput-object v0, p0, Lixv;->N:Liwa;

    .line 66
    iget-object v0, p0, Lixv;->au:Llnh;

    const-class v1, Lixn;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 67
    iget-object v0, p0, Lixv;->au:Llnh;

    const-class v1, Liyj;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 68
    return-void
.end method

.method public d()V
    .locals 3

    .prologue
    .line 212
    iget-object v0, p0, Lixv;->O:Lhei;

    iget v1, p0, Lixv;->R:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 213
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 214
    const-string v2, "effective_gaia_id"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 216
    iget-object v2, p0, Lixv;->N:Liwa;

    invoke-interface {v2, v1, v0}, Liwa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 84
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 85
    const-string v0, "account_id"

    iget v1, p0, Lixv;->R:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 86
    const-string v0, "retry_count"

    iget v1, p0, Lixv;->S:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 87
    const-string v0, "next_retry_time_ms"

    iget-wide v2, p0, Lixv;->T:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 88
    return-void
.end method
