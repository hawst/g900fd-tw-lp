.class public final Ljgt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljgn;


# instance fields
.field private final a:Ljgw;

.field private final b:Landroid/net/ConnectivityManager;

.field private final c:Ljgv;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const-class v0, Ljgw;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljgw;

    iput-object v0, p0, Ljgt;->a:Ljgw;

    .line 34
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Ljgt;->b:Landroid/net/ConnectivityManager;

    .line 36
    new-instance v0, Ljgv;

    invoke-direct {v0, p0}, Ljgv;-><init>(Ljgn;)V

    iput-object v0, p0, Ljgt;->c:Ljgv;

    .line 37
    return-void
.end method

.method private a(Landroid/net/NetworkInfo;)Z
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 152
    const-string v0, "NetworkCapability"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x10

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Active network: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    .line 159
    :goto_0
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-lt v3, v4, :cond_3

    .line 160
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/net/NetworkInfo;->getDetailedState()Landroid/net/NetworkInfo$DetailedState;

    move-result-object v0

    sget-object v3, Landroid/net/NetworkInfo$DetailedState;->BLOCKED:Landroid/net/NetworkInfo$DetailedState;

    if-eq v0, v3, :cond_2

    .line 162
    :goto_1
    return v1

    :cond_1
    move v0, v2

    .line 156
    goto :goto_0

    :cond_2
    move v1, v2

    .line 160
    goto :goto_1

    :cond_3
    move v1, v0

    .line 162
    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljgq;)V
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Ljgt;->c:Ljgv;

    invoke-virtual {v0, p1, p2}, Ljgv;->a(Landroid/content/Context;Ljgq;)V

    .line 168
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Ljgt;->b:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 42
    invoke-direct {p0, v0}, Ljgt;->a(Landroid/net/NetworkInfo;)Z

    move-result v0

    return v0
.end method

.method public b(Landroid/content/Context;Ljgq;)V
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Ljgt;->c:Ljgv;

    invoke-virtual {v0, p1, p2}, Ljgv;->b(Landroid/content/Context;Ljgq;)V

    .line 173
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 49
    invoke-virtual {p0}, Ljgt;->d()Z

    move-result v0

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Ljgt;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Ljgt;->b:Landroid/net/ConnectivityManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 60
    invoke-direct {p0, v0}, Ljgt;->a(Landroid/net/NetworkInfo;)Z

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 65
    iget-object v1, p0, Ljgt;->b:Landroid/net/ConnectivityManager;

    invoke-virtual {v1, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Ljgt;->b:Landroid/net/ConnectivityManager;

    invoke-static {v0}, Ler;->a(Landroid/net/ConnectivityManager;)Z

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Ljgt;->b:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 76
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 81
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    .line 83
    iget-object v0, p0, Ljgt;->b:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getBackgroundDataSetting()Z

    move-result v0

    .line 94
    :goto_0
    return v0

    .line 93
    :cond_0
    iget-object v0, p0, Ljgt;->b:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 94
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 100
    invoke-virtual {p0}, Ljgt;->m()I

    move-result v1

    .line 102
    const-string v2, "NetworkCapability"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 103
    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Active network type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 106
    :cond_0
    packed-switch v1, :pswitch_data_0

    .line 120
    :goto_0
    :pswitch_0
    return v0

    .line 117
    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    .line 106
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0}, Ljgt;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljgt;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Ljgt;->a:Ljgw;

    invoke-virtual {v0}, Ljgw;->a()I

    move-result v0

    return v0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 177
    iget-object v0, p0, Ljgt;->a:Ljgw;

    invoke-virtual {v0}, Ljgw;->b()I

    move-result v0

    return v0
.end method

.method m()I
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Ljgt;->b:Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 142
    if-nez v0, :cond_0

    .line 145
    const/4 v0, -0x1

    .line 147
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    goto :goto_0
.end method
