.class public final Lazt;
.super Laye;
.source "PG"


# instance fields
.field private final a:Lazv;

.field private final b:F

.field private final c:Landroid/animation/TimeInterpolator;

.field private final d:Lazw;

.field private e:F

.field private f:F

.field private g:Landroid/graphics/Matrix;

.field private h:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Layj;Lazv;F)V
    .locals 2

    .prologue
    .line 74
    invoke-direct {p0, p1}, Laye;-><init>(Layj;)V

    .line 44
    new-instance v0, Lazw;

    invoke-direct {v0}, Lazw;-><init>()V

    iput-object v0, p0, Lazt;->d:Lazw;

    .line 59
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lazt;->g:Landroid/graphics/Matrix;

    .line 60
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lazt;->h:Landroid/graphics/Matrix;

    .line 75
    const/4 v0, 0x0

    cmpl-float v0, p3, v0

    if-lez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    cmpg-float v0, p3, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "scale must be > 0 and <= 1"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 76
    iput p3, p0, Lazt;->b:F

    .line 77
    const-string v0, "direction"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lazv;

    iput-object v0, p0, Lazt;->a:Lazv;

    .line 78
    sget-object v0, Lcak;->a:Lcak;

    iput-object v0, p0, Lazt;->c:Landroid/animation/TimeInterpolator;

    .line 79
    return-void

    .line 75
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private A(Layl;)Lazv;
    .locals 4

    .prologue
    .line 292
    iget-object v0, p0, Lazt;->a:Lazv;

    sget-object v1, Lazv;->e:Lazv;

    if-ne v0, v1, :cond_0

    .line 293
    const/4 v0, 0x0

    const/4 v1, 0x4

    iget v2, p1, Layl;->f:I

    iget v3, p1, Layl;->h:I

    mul-int/2addr v2, v3

    invoke-static {v0, v1, v2}, Lbag;->a(III)I

    move-result v0

    .line 294
    invoke-static {}, Lazv;->values()[Lazv;

    move-result-object v1

    aget-object v0, v1, v0

    .line 296
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lazt;->a:Lazv;

    goto :goto_0
.end method

.method private a(Lazv;I)F
    .locals 4

    .prologue
    const/16 v3, 0x1f

    const/high16 v0, 0x3f800000    # 1.0f

    .line 183
    :goto_0
    sget-object v1, Lazu;->a:[I

    invoke-virtual {p1}, Lazv;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 244
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x15

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unexpected direction "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 199
    :pswitch_0
    packed-switch p2, :pswitch_data_1

    .line 217
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unexpected position "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 203
    :pswitch_1
    iget-object v0, p0, Lazt;->d:Lazw;

    iget v0, v0, Lazw;->a:F

    neg-float v0, v0

    .line 233
    :goto_1
    :pswitch_2
    return v0

    .line 207
    :pswitch_3
    iget v0, p0, Lazt;->f:F

    goto :goto_1

    .line 211
    :pswitch_4
    iget v0, p0, Lazt;->e:F

    iget v1, p0, Lazt;->f:F

    add-float/2addr v0, v1

    iget-object v1, p0, Lazt;->d:Lazw;

    iget v1, v1, Lazw;->a:F

    sub-float/2addr v0, v1

    goto :goto_1

    .line 221
    :pswitch_5
    sget-object p1, Lazv;->d:Lazv;

    .line 222
    rsub-int/lit8 p2, p2, 0x3

    .line 221
    goto :goto_0

    .line 225
    :pswitch_6
    packed-switch p2, :pswitch_data_2

    .line 235
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unexpected position "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 227
    :pswitch_7
    iget-object v0, p0, Lazt;->d:Lazw;

    iget v0, v0, Lazw;->b:F

    neg-float v0, v0

    goto :goto_1

    .line 229
    :pswitch_8
    iget v0, p0, Lazt;->f:F

    goto :goto_1

    .line 231
    :pswitch_9
    iget v0, p0, Lazt;->e:F

    iget v1, p0, Lazt;->f:F

    add-float/2addr v0, v1

    iget-object v1, p0, Lazt;->d:Lazw;

    iget v1, v1, Lazw;->b:F

    sub-float/2addr v0, v1

    goto :goto_1

    .line 239
    :pswitch_a
    sget-object p1, Lazv;->a:Lazv;

    .line 240
    rsub-int/lit8 p2, p2, 0x3

    .line 239
    goto/16 :goto_0

    .line 242
    :pswitch_b
    const-string v0, "RANDOM_PER_CLIP direction has not been resolved"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 183
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_5
        :pswitch_0
        :pswitch_6
        :pswitch_a
        :pswitch_b
    .end packed-switch

    .line 199
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_2
    .end packed-switch

    .line 225
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_2
    .end packed-switch
.end method

.method private a(Landroid/graphics/Matrix;)V
    .locals 2

    .prologue
    .line 287
    iget-object v0, p0, Lazt;->d:Lazw;

    iget v0, v0, Lazw;->a:F

    iget-object v1, p0, Lazt;->d:Lazw;

    iget v1, v1, Lazw;->b:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 288
    iget-object v0, p0, Lazt;->d:Lazw;

    iget v0, v0, Lazw;->c:F

    iget-object v1, p0, Lazt;->d:Lazw;

    iget v1, v1, Lazw;->d:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 289
    return-void
.end method

.method private a(Lbmj;Lazv;)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v2, 0x3f800000    # 1.0f

    .line 258
    iget v0, p1, Lbmj;->a:F

    sub-float v0, v2, v0

    iget v1, p1, Lbmj;->b:F

    sub-float/2addr v0, v1

    .line 259
    iget v1, p1, Lbmj;->c:F

    sub-float v1, v2, v1

    iget v2, p1, Lbmj;->d:F

    sub-float/2addr v1, v2

    .line 260
    iget-object v2, p0, Lazt;->d:Lazw;

    iget v3, p0, Lazt;->b:F

    mul-float/2addr v3, v0

    iput v3, v2, Lazw;->a:F

    .line 261
    iget-object v2, p0, Lazt;->d:Lazw;

    iget v3, p0, Lazt;->b:F

    mul-float/2addr v3, v1

    iput v3, v2, Lazw;->b:F

    .line 262
    sget-object v2, Lazu;->a:[I

    invoke-virtual {p2}, Lazv;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 280
    :goto_0
    return-void

    .line 265
    :pswitch_0
    iput v0, p0, Lazt;->e:F

    .line 266
    iget-object v0, p0, Lazt;->d:Lazw;

    iget v2, p1, Lbmj;->d:F

    iget-object v3, p0, Lazt;->d:Lazw;

    iget v3, v3, Lazw;->b:F

    sub-float/2addr v1, v3

    div-float/2addr v1, v4

    add-float/2addr v1, v2

    iput v1, v0, Lazw;->d:F

    .line 268
    iget v0, p1, Lbmj;->a:F

    iput v0, p0, Lazt;->f:F

    goto :goto_0

    .line 272
    :pswitch_1
    iput v1, p0, Lazt;->e:F

    .line 273
    iget-object v1, p0, Lazt;->d:Lazw;

    iget v2, p1, Lbmj;->a:F

    iget-object v3, p0, Lazt;->d:Lazw;

    iget v3, v3, Lazw;->a:F

    sub-float/2addr v0, v3

    div-float/2addr v0, v4

    add-float/2addr v0, v2

    iput v0, v1, Lazw;->c:F

    .line 275
    iget v0, p1, Lbmj;->d:F

    iput v0, p0, Lazt;->f:F

    goto :goto_0

    .line 278
    :pswitch_2
    const-string v0, "Direction parameter must not be RANDOM_PER_CLIP"

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 262
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a(Layl;Lbof;)Landroid/graphics/Matrix;
    .locals 8

    .prologue
    const/4 v4, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 87
    invoke-direct {p0, p1}, Lazt;->A(Layl;)Lazv;

    move-result-object v0

    .line 88
    iget-object v1, p1, Layl;->s:Lbmj;

    invoke-direct {p0, v1, v0}, Lazt;->a(Lbmj;Lazv;)V

    .line 90
    invoke-static {p1}, Lbag;->a(Layl;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 93
    iget-wide v2, p1, Layl;->d:J

    iget-wide v4, p1, Layl;->n:J

    sub-long/2addr v2, v4

    long-to-float v1, v2

    iget-wide v2, p1, Layl;->e:J

    iget-wide v4, p1, Layl;->n:J

    sub-long/2addr v2, v4

    iget-wide v4, p1, Layl;->o:J

    sub-long/2addr v2, v4

    long-to-float v2, v2

    div-float/2addr v1, v2

    .line 97
    sget-object v2, Lazu;->a:[I

    invoke-virtual {v0}, Lazv;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 140
    :goto_0
    iget-object v0, p0, Lazt;->g:Landroid/graphics/Matrix;

    invoke-direct {p0, v0}, Lazt;->a(Landroid/graphics/Matrix;)V

    .line 141
    iget-object v0, p0, Lazt;->g:Landroid/graphics/Matrix;

    return-object v0

    .line 100
    :pswitch_0
    iget-object v2, p0, Lazt;->d:Lazw;

    .line 101
    invoke-direct {p0, v0, v7}, Lazt;->a(Lazv;I)F

    move-result v3

    .line 102
    invoke-direct {p0, v0, v6}, Lazt;->a(Lazv;I)F

    move-result v0

    .line 100
    sub-float/2addr v0, v3

    mul-float/2addr v0, v1

    add-float/2addr v0, v3

    iput v0, v2, Lazw;->c:F

    goto :goto_0

    .line 107
    :pswitch_1
    iget-object v2, p0, Lazt;->d:Lazw;

    .line 108
    invoke-direct {p0, v0, v7}, Lazt;->a(Lazv;I)F

    move-result v3

    .line 109
    invoke-direct {p0, v0, v6}, Lazt;->a(Lazv;I)F

    move-result v0

    .line 107
    sub-float/2addr v0, v3

    mul-float/2addr v0, v1

    add-float/2addr v0, v3

    iput v0, v2, Lazw;->d:F

    goto :goto_0

    .line 113
    :pswitch_2
    const-string v0, "RANDOM_PER_CLIP direction has not been resolved"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 119
    :cond_0
    iget-object v1, p0, Lazt;->c:Landroid/animation/TimeInterpolator;

    iget v2, p1, Layl;->m:F

    invoke-interface {v1, v2}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v1

    .line 120
    sget-object v2, Lazu;->a:[I

    invoke-virtual {v0}, Lazv;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 123
    :pswitch_3
    iget-object v2, p0, Lazt;->d:Lazw;

    .line 124
    invoke-direct {p0, v0, v6}, Lazt;->a(Lazv;I)F

    move-result v3

    .line 125
    invoke-direct {p0, v0, v4}, Lazt;->a(Lazv;I)F

    move-result v0

    .line 123
    sub-float/2addr v0, v3

    mul-float/2addr v0, v1

    add-float/2addr v0, v3

    iput v0, v2, Lazw;->c:F

    goto :goto_0

    .line 130
    :pswitch_4
    iget-object v2, p0, Lazt;->d:Lazw;

    .line 131
    invoke-direct {p0, v0, v6}, Lazt;->a(Lazv;I)F

    move-result v3

    .line 132
    invoke-direct {p0, v0, v4}, Lazt;->a(Lazv;I)F

    move-result v0

    .line 130
    sub-float/2addr v0, v3

    mul-float/2addr v0, v1

    add-float/2addr v0, v3

    iput v0, v2, Lazw;->d:F

    goto :goto_0

    .line 136
    :pswitch_5
    const-string v0, "RANDOM_PER_CLIP direction has not been resolved"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 97
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 120
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public b(Layl;)Landroid/graphics/Matrix;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 150
    invoke-direct {p0, p1}, Lazt;->A(Layl;)Lazv;

    move-result-object v0

    .line 151
    iget-object v1, p1, Layl;->t:Lbmj;

    invoke-direct {p0, v1, v0}, Lazt;->a(Lbmj;Lazv;)V

    .line 153
    iget-object v1, p0, Lazt;->c:Landroid/animation/TimeInterpolator;

    iget v2, p1, Layl;->m:F

    invoke-interface {v1, v2}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v1

    .line 154
    sget-object v2, Lazu;->a:[I

    invoke-virtual {v0}, Lazv;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 173
    :goto_0
    iget-object v0, p0, Lazt;->h:Landroid/graphics/Matrix;

    invoke-direct {p0, v0}, Lazt;->a(Landroid/graphics/Matrix;)V

    .line 174
    iget-object v0, p0, Lazt;->h:Landroid/graphics/Matrix;

    return-object v0

    .line 157
    :pswitch_0
    iget-object v2, p0, Lazt;->d:Lazw;

    .line 158
    invoke-direct {p0, v0, v4}, Lazt;->a(Lazv;I)F

    move-result v3

    .line 159
    invoke-direct {p0, v0, v5}, Lazt;->a(Lazv;I)F

    move-result v0

    .line 157
    sub-float/2addr v0, v3

    mul-float/2addr v0, v1

    add-float/2addr v0, v3

    iput v0, v2, Lazw;->c:F

    goto :goto_0

    .line 164
    :pswitch_1
    iget-object v2, p0, Lazt;->d:Lazw;

    .line 165
    invoke-direct {p0, v0, v4}, Lazt;->a(Lazv;I)F

    move-result v3

    .line 166
    invoke-direct {p0, v0, v5}, Lazt;->a(Lazv;I)F

    move-result v0

    .line 164
    sub-float/2addr v0, v3

    mul-float/2addr v0, v1

    add-float/2addr v0, v3

    iput v0, v2, Lazw;->d:F

    goto :goto_0

    .line 170
    :pswitch_2
    const-string v0, "RANDOM_PER_CLIP direction has not been resolved"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 154
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public w(Layl;)F
    .locals 1

    .prologue
    .line 179
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method
