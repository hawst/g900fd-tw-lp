.class public final Lhbg;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(I)F
    .locals 4

    .prologue
    .line 21
    packed-switch p0, :pswitch_data_0

    .line 35
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Illegal value of postRotationType:"

    .line 36
    invoke-static {p0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 23
    :pswitch_0
    const/4 v0, 0x0

    .line 32
    :goto_1
    return v0

    .line 26
    :pswitch_1
    const/high16 v0, 0x42b40000    # 90.0f

    goto :goto_1

    .line 29
    :pswitch_2
    const/high16 v0, 0x43340000    # 180.0f

    goto :goto_1

    .line 32
    :pswitch_3
    const/high16 v0, 0x43870000    # 270.0f

    goto :goto_1

    .line 36
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 21
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(F)I
    .locals 2

    .prologue
    .line 45
    const/high16 v0, 0x43b40000    # 360.0f

    rem-float v0, p0, v0

    const/high16 v1, 0x42b40000    # 90.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 46
    packed-switch v0, :pswitch_data_0

    .line 57
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 48
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 51
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 54
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 46
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/graphics/PointF;IFF)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 71
    cmpg-float v0, p2, v1

    if-lez v0, :cond_0

    cmpg-float v0, p3, v1

    if-gtz v0, :cond_1

    .line 72
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal value of width or height"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 74
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 91
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Illegal value of postRotation:"

    .line 92
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 79
    :pswitch_0
    iget v0, p0, Landroid/graphics/PointF;->y:F

    iget v1, p0, Landroid/graphics/PointF;->x:F

    sub-float v1, p2, v1

    invoke-virtual {p0, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    .line 88
    :goto_1
    :pswitch_1
    return-void

    .line 83
    :pswitch_2
    iget v0, p0, Landroid/graphics/PointF;->x:F

    sub-float v0, p2, v0

    iget v1, p0, Landroid/graphics/PointF;->y:F

    sub-float v1, p3, v1

    invoke-virtual {p0, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_1

    .line 87
    :pswitch_3
    iget v0, p0, Landroid/graphics/PointF;->y:F

    sub-float v0, p3, v0

    iget v1, p0, Landroid/graphics/PointF;->x:F

    invoke-virtual {p0, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    goto :goto_1

    .line 92
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 74
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b(Landroid/graphics/PointF;IFF)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 107
    cmpg-float v0, p2, v1

    if-lez v0, :cond_0

    cmpg-float v0, p3, v1

    if-gtz v0, :cond_1

    .line 108
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Illegal value of width or height"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 110
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 127
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Illegal value of postRotation:"

    .line 128
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 115
    :pswitch_0
    const/4 v0, 0x3

    invoke-static {p0, v0, p2, p3}, Lhbg;->a(Landroid/graphics/PointF;IFF)V

    .line 124
    :goto_1
    :pswitch_1
    return-void

    .line 119
    :pswitch_2
    const/4 v0, 0x2

    invoke-static {p0, v0, p2, p3}, Lhbg;->a(Landroid/graphics/PointF;IFF)V

    goto :goto_1

    .line 123
    :pswitch_3
    const/4 v0, 0x1

    invoke-static {p0, v0, p2, p3}, Lhbg;->a(Landroid/graphics/PointF;IFF)V

    goto :goto_1

    .line 128
    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 110
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
