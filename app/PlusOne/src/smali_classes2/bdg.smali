.class final Lbdg;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PG"


# instance fields
.field private synthetic a:Landroid/graphics/Bitmap;

.field private synthetic b:I

.field private synthetic c:Lbco;


# direct methods
.method constructor <init>(Lbco;Landroid/graphics/Bitmap;I)V
    .locals 0

    .prologue
    .line 1118
    iput-object p1, p0, Lbdg;->c:Lbco;

    iput-object p2, p0, Lbdg;->a:Landroid/graphics/Bitmap;

    iput p3, p0, Lbdg;->b:I

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 1121
    iget-object v0, p0, Lbdg;->c:Lbco;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbco;->b(Lbco;Landroid/animation/ObjectAnimator;)Landroid/animation/ObjectAnimator;

    .line 1122
    iget-object v0, p0, Lbdg;->c:Lbco;

    invoke-static {v0}, Lbco;->p(Lbco;)Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->c(F)V

    .line 1123
    iget-object v0, p0, Lbdg;->c:Lbco;

    invoke-static {v0}, Lbco;->p(Lbco;)Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    move-result-object v0

    iget-object v1, p0, Lbdg;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->a(Landroid/graphics/Bitmap;)V

    .line 1124
    iget-object v0, p0, Lbdg;->c:Lbco;

    invoke-static {v0}, Lbco;->p(Lbco;)Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->a(F)V

    .line 1125
    iget-object v0, p0, Lbdg;->c:Lbco;

    invoke-static {v0}, Lbco;->v(Lbco;)Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->setVisibility(I)V

    .line 1126
    iget v0, p0, Lbdg;->b:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lbdg;->c:Lbco;

    .line 1127
    invoke-static {v0}, Lbco;->f(Lbco;)Laqw;

    move-result-object v0

    invoke-interface {v0}, Laqw;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1128
    iget-object v0, p0, Lbdg;->c:Lbco;

    invoke-static {v0}, Lbco;->w(Lbco;)V

    .line 1129
    iget-object v0, p0, Lbdg;->c:Lbco;

    invoke-static {v0}, Lbco;->f(Lbco;)Laqw;

    move-result-object v0

    invoke-interface {v0}, Laqw;->d()V

    .line 1133
    :cond_0
    :goto_0
    iget-object v0, p0, Lbdg;->c:Lbco;

    invoke-static {v0}, Lbco;->v(Lbco;)Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/CircleBitmapView;->a(Z)V

    .line 1134
    return-void

    .line 1130
    :cond_1
    iget v0, p0, Lbdg;->b:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 1131
    iget-object v0, p0, Lbdg;->c:Lbco;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lbco;->f(Lbco;Z)Z

    goto :goto_0
.end method
