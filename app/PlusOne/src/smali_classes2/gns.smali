.class public interface abstract Lgns;
.super Ljava/lang/Object;

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a(Landroid/net/Uri;)Landroid/os/Bundle;
.end method

.method public abstract a(Lgnp;ZLjava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;J)Landroid/os/Bundle;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;JZ)Landroid/os/Bundle;
.end method

.method public abstract a(Ljava/lang/String;Ljava/lang/String;JZZ)Landroid/os/Bundle;
.end method

.method public abstract a(Lgnp;Lcom/google/android/gms/people/model/AvatarReference;Lcom/google/android/gms/internal/pi;)Lghw;
.end method

.method public abstract a(Lgnp;Ljava/lang/String;I)Lghw;
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;IIIZ)Lghw;
.end method

.method public abstract a(Lgnp;JZ)V
.end method

.method public abstract a(Lgnp;Landroid/os/Bundle;)V
.end method

.method public abstract a(Lgnp;Lcom/google/android/gms/internal/or;Ljava/util/List;Lcom/google/android/gms/internal/ow;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgnp;",
            "Lcom/google/android/gms/internal/or;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/gms/internal/ow;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lgnp;Ljava/lang/String;)V
.end method

.method public abstract a(Lgnp;Ljava/lang/String;II)V
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;II)V
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Z)V
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;)V
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;Z)V
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;ZII)V
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgnp;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJ)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgnp;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IZJ)V"
        }
    .end annotation
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgnp;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IZJ",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;II)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgnp;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IZJ",
            "Ljava/lang/String;",
            "II)V"
        }
    .end annotation
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;III)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgnp;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;IZJ",
            "Ljava/lang/String;",
            "III)V"
        }
    .end annotation
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgnp;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/internal/jn;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgnp;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/gms/internal/jn;",
            ")V"
        }
    .end annotation
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)V
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZII)V
.end method

.method public abstract a(Lgnp;Ljava/lang/String;Z[Ljava/lang/String;)V
.end method

.method public abstract a(Lgnp;ZZLjava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Lgnp;ZZLjava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract a()Z
.end method

.method public abstract b(Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
.end method

.method public abstract b(Lgnp;JZ)Lghw;
.end method

.method public abstract b(Lgnp;Ljava/lang/String;)Lghw;
.end method

.method public abstract b(Lgnp;Ljava/lang/String;II)Lghw;
.end method

.method public abstract b(Lgnp;Ljava/lang/String;Ljava/lang/String;II)Lghw;
.end method

.method public abstract b(Lgnp;Landroid/os/Bundle;)V
.end method

.method public abstract b(Lgnp;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract b(Lgnp;Ljava/lang/String;Ljava/lang/String;I)V
.end method

.method public abstract b(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
.end method

.method public abstract c(Lgnp;Ljava/lang/String;Ljava/lang/String;I)Lghw;
.end method

.method public abstract c(Lgnp;Ljava/lang/String;Ljava/lang/String;)V
.end method
