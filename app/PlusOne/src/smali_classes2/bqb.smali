.class public abstract Lbqb;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<K:",
        "Ljava/lang/Object;",
        "V::",
        "Lbqq;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private a:Lbqc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbqc",
            "<TK;TV;>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Lbqc;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbqc",
            "<TK;TV;>;)V"
        }
    .end annotation

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lbqb;->a:Lbqc;

    .line 29
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;)Lbqq;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;)TV;"
        }
    .end annotation

    .prologue
    .line 40
    iget-object v0, p0, Lbqb;->a:Lbqc;

    invoke-virtual {v0, p1}, Lbqc;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqq;

    .line 41
    if-eqz v0, :cond_0

    .line 42
    invoke-interface {v0}, Lbqq;->a()V

    .line 46
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Lbqq;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TK;TV;)V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-interface {p2}, Lbqq;->a()V

    .line 54
    iget-object v0, p0, Lbqb;->a:Lbqc;

    invoke-virtual {v0, p1, p2}, Lbqc;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lbqb;->a:Lbqc;

    invoke-virtual {v0}, Lbqc;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
