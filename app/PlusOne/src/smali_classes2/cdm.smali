.class public Lcdm;
.super Lccv;
.source "PG"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:F

.field private d:I

.field private e:F

.field private f:I

.field private g:I

.field private h:I

.field private i:Lcdn;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcdm;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcdm;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcci;)V
    .locals 1

    .prologue
    .line 88
    const/high16 v0, 0x3f000000    # 0.5f

    invoke-direct {p0, p1, v0}, Lcdm;-><init>(Lcci;F)V

    .line 89
    return-void
.end method

.method public constructor <init>(Lcci;F)V
    .locals 1

    .prologue
    .line 100
    invoke-direct {p0, p1}, Lccv;-><init>(Lcci;)V

    .line 69
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcdm;->e:F

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcdm;->f:I

    .line 101
    iput p2, p0, Lcdm;->c:F

    .line 102
    return-void
.end method

.method private a(Lccj;IIZ)I
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 410
    iget-object v0, p0, Lcdm;->a:Lcci;

    iget-object v1, p1, Lccj;->h:Landroid/widget/Adapter;

    invoke-interface {v0, v1, p2}, Lcci;->a(Landroid/widget/Adapter;I)Lccg;

    move-result-object v0

    .line 411
    iget-object v1, v0, Lccg;->a:Landroid/view/View;

    invoke-virtual {p1, v1}, Lccj;->b(Landroid/view/View;)I

    move-result v1

    .line 412
    iget-object v2, v0, Lccg;->a:Landroid/view/View;

    invoke-virtual {p1, v2}, Lccj;->c(Landroid/view/View;)I

    move-result v2

    .line 414
    if-nez p4, :cond_0

    .line 415
    sub-int/2addr p3, v1

    .line 419
    :cond_0
    add-int v3, p3, v1

    .line 421
    iget-boolean v4, p1, Lccj;->a:Z

    if-eqz v4, :cond_3

    .line 422
    iget-object v4, v0, Lccg;->a:Landroid/view/View;

    invoke-virtual {v4, p3, v5, v3, v2}, Landroid/view/View;->layout(IIII)V

    .line 427
    :goto_0
    invoke-direct {p0, p1}, Lcdm;->d(Lccj;)I

    move-result v2

    if-lt v3, v2, :cond_1

    invoke-direct {p0, p1}, Lcdm;->e(Lccj;)I

    move-result v2

    if-gt p3, v2, :cond_1

    .line 428
    iget-object v2, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, p2, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 431
    :cond_1
    if-eqz p4, :cond_2

    .line 432
    add-int/2addr p3, v1

    .line 435
    :cond_2
    return p3

    .line 424
    :cond_3
    iget-object v4, v0, Lccg;->a:Landroid/view/View;

    invoke-virtual {v4, v5, p3, v2, v3}, Landroid/view/View;->layout(IIII)V

    goto :goto_0
.end method

.method private a(Lccj;III)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 293
    iget-object v0, p0, Lcdm;->a:Lcci;

    iget-object v1, p1, Lccj;->h:Landroid/widget/Adapter;

    invoke-interface {v0, v1, p2}, Lcci;->a(Landroid/widget/Adapter;I)Lccg;

    move-result-object v1

    .line 295
    iget v2, p1, Lccj;->i:I

    iget-boolean v0, p1, Lccj;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lccj;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    sub-int v0, p3, v0

    iget-object v3, v1, Lccg;->a:Landroid/view/View;

    .line 296
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v0, v3

    .line 297
    :goto_0
    add-int/2addr v0, v2

    .line 299
    iget-object v2, v1, Lccg;->a:Landroid/view/View;

    invoke-virtual {p1, v2}, Lccj;->b(Landroid/view/View;)I

    move-result v2

    .line 300
    iget-object v3, v1, Lccg;->a:Landroid/view/View;

    invoke-virtual {p1, v3}, Lccj;->c(Landroid/view/View;)I

    move-result v3

    .line 302
    iget-boolean v4, p1, Lccj;->a:Z

    if-eqz v4, :cond_1

    .line 303
    iget-object v4, v1, Lccg;->a:Landroid/view/View;

    add-int/2addr v2, v0

    invoke-virtual {v4, v0, v5, v2, v3}, Landroid/view/View;->layout(IIII)V

    .line 308
    :goto_1
    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, p2, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 309
    return-void

    .line 296
    :cond_0
    iget-object v0, p1, Lccj;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int v0, p4, v0

    iget-object v3, v1, Lccg;->a:Landroid/view/View;

    .line 297
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    sub-int/2addr v0, v3

    goto :goto_0

    .line 305
    :cond_1
    iget-object v4, v1, Lccg;->a:Landroid/view/View;

    add-int/2addr v2, v0

    invoke-virtual {v4, v5, v0, v3, v2}, Landroid/view/View;->layout(IIII)V

    goto :goto_1
.end method

.method private b(Lccj;)I
    .locals 2

    .prologue
    .line 369
    iget v1, p1, Lccj;->i:I

    iget-boolean v0, p1, Lccj;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lccj;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p1, Lccj;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    goto :goto_0
.end method

.method private c(Lccj;)I
    .locals 2

    .prologue
    .line 377
    iget v1, p1, Lccj;->i:I

    iget-boolean v0, p1, Lccj;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lccj;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    iget-object v0, p1, Lccj;->d:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    goto :goto_0
.end method

.method private d(Lccj;)I
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 385
    invoke-direct {p0, p1}, Lcdm;->b(Lccj;)I

    move-result v1

    iget-boolean v0, p1, Lccj;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lccj;->d:Landroid/graphics/Rect;

    .line 386
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, Lcdm;->c:F

    mul-float/2addr v0, v2

    div-float/2addr v0, v3

    .line 387
    :goto_0
    float-to-int v0, v0

    sub-int v0, v1, v0

    return v0

    .line 386
    :cond_0
    iget-object v0, p1, Lccj;->d:Landroid/graphics/Rect;

    .line 387
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, Lcdm;->c:F

    mul-float/2addr v0, v2

    div-float/2addr v0, v3

    goto :goto_0
.end method

.method private e(Lccj;)I
    .locals 4

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 394
    invoke-direct {p0, p1}, Lcdm;->c(Lccj;)I

    move-result v1

    iget-boolean v0, p1, Lccj;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lccj;->d:Landroid/graphics/Rect;

    .line 395
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, Lcdm;->c:F

    mul-float/2addr v0, v2

    div-float/2addr v0, v3

    .line 396
    :goto_0
    float-to-int v0, v0

    add-int/2addr v0, v1

    return v0

    .line 395
    :cond_0
    iget-object v0, p1, Lccj;->d:Landroid/graphics/Rect;

    .line 396
    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    int-to-float v0, v0

    iget v2, p0, Lcdm;->c:F

    mul-float/2addr v0, v2

    div-float/2addr v0, v3

    goto :goto_0
.end method


# virtual methods
.method public a()F
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lcdm;->e:F

    return v0
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 122
    iput p1, p0, Lcdm;->e:F

    .line 123
    const/4 v0, 0x1

    iput v0, p0, Lcdm;->d:I

    .line 124
    iget-object v0, p0, Lcdm;->a:Lcci;

    invoke-interface {v0}, Lcci;->d()V

    .line 125
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x3

    iput v0, p0, Lcdm;->d:I

    .line 144
    iput p1, p0, Lcdm;->f:I

    .line 145
    iget-object v0, p0, Lcdm;->a:Lcci;

    invoke-interface {v0}, Lcci;->d()V

    .line 146
    return-void
.end method

.method public a(III)V
    .locals 1

    .prologue
    .line 131
    const/4 v0, 0x2

    iput v0, p0, Lcdm;->d:I

    .line 132
    iput p1, p0, Lcdm;->f:I

    .line 133
    iput p2, p0, Lcdm;->g:I

    .line 134
    iput p3, p0, Lcdm;->h:I

    .line 135
    iget-object v0, p0, Lcdm;->a:Lcci;

    invoke-interface {v0}, Lcci;->d()V

    .line 136
    return-void
.end method

.method public a(Lccj;)V
    .locals 14

    .prologue
    const/4 v13, 0x2

    const/4 v1, -0x1

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 152
    iget-object v0, p1, Lccj;->h:Landroid/widget/Adapter;

    instance-of v0, v0, Lcau;

    const-string v2, "LayoutModifier can only be used with adapters that implement the PositionFromIdAdapter interface"

    invoke-static {v0, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 156
    iget-object v0, p1, Lccj;->h:Landroid/widget/Adapter;

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 214
    :cond_0
    :goto_0
    return-void

    .line 163
    :cond_1
    iget v0, p0, Lcdm;->d:I

    packed-switch v0, :pswitch_data_0

    move v0, v1

    .line 192
    :goto_1
    iput v3, p0, Lcdm;->d:I

    .line 195
    if-ne v0, v1, :cond_2

    .line 196
    iget-object v0, p1, Lccj;->h:Landroid/widget/Adapter;

    check-cast v0, Lcau;

    iget-object v1, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v4

    move v2, v3

    :goto_2
    if-ge v2, v4, :cond_6

    iget-object v1, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lccg;

    iget-wide v6, v1, Lccg;->b:J

    invoke-interface {v0, v6, v7}, Lcau;->a(J)I

    move-result v1

    if-ltz v1, :cond_5

    move v0, v1

    .line 197
    :goto_3
    iget-boolean v1, p1, Lccj;->f:Z

    if-eqz v1, :cond_2

    .line 198
    sget-object v1, Lcdm;->b:Ljava/lang/String;

    const-string v1, "Anchor Position = %s, Item = %s"

    new-array v2, v13, [Ljava/lang/Object;

    .line 199
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    iget-object v4, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v5

    .line 198
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 204
    :cond_2
    iget-object v1, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->clear()V

    move-object v1, p0

    .line 206
    :goto_4
    iget-object v2, p1, Lccj;->h:Landroid/widget/Adapter;

    invoke-interface {v2}, Landroid/widget/Adapter;->getCount()I

    move-result v7

    iget-object v2, v1, Lcdm;->a:Lcci;

    iget-object v4, p1, Lccj;->h:Landroid/widget/Adapter;

    invoke-interface {v2, v4, v0}, Lcci;->a(Landroid/widget/Adapter;I)Lccg;

    move-result-object v2

    iget-object v4, v2, Lccg;->a:Landroid/view/View;

    invoke-virtual {p1, v4}, Lccj;->a(Landroid/view/View;)I

    move-result v2

    invoke-direct {v1, p1, v0, v2, v5}, Lcdm;->a(Lccj;IIZ)I

    iget-boolean v2, p1, Lccj;->a:Z

    if-eqz v2, :cond_7

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v2

    :goto_5
    iget-boolean v6, p1, Lccj;->a:Z

    if-eqz v6, :cond_8

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v4

    :goto_6
    invoke-direct {v1, p1}, Lcdm;->d(Lccj;)I

    move-result v8

    invoke-direct {v1, p1}, Lcdm;->e(Lccj;)I

    move-result v9

    invoke-direct {v1, p1}, Lcdm;->b(Lccj;)I

    move-result v10

    invoke-direct {v1, p1}, Lcdm;->c(Lccj;)I

    move-result v11

    move v6, v2

    move v2, v0

    :goto_7
    if-lez v2, :cond_9

    if-le v6, v8, :cond_9

    iget v12, p1, Lccj;->b:I

    sub-int/2addr v6, v12

    add-int/lit8 v2, v2, -0x1

    invoke-direct {v1, p1, v2, v6, v3}, Lcdm;->a(Lccj;IIZ)I

    move-result v6

    goto :goto_7

    :pswitch_0
    move v0, v1

    .line 165
    goto/16 :goto_1

    .line 168
    :pswitch_1
    iget v0, p0, Lcdm;->e:F

    float-to-int v2, v0

    iget-object v4, p1, Lccj;->h:Landroid/widget/Adapter;

    invoke-interface {v4}, Landroid/widget/Adapter;->getCount()I

    move-result v4

    if-lt v2, v4, :cond_3

    move v0, v1

    goto/16 :goto_1

    :cond_3
    iget-object v4, p0, Lcdm;->a:Lcci;

    iget-object v6, p1, Lccj;->h:Landroid/widget/Adapter;

    invoke-interface {v4, v6, v2}, Lcci;->a(Landroid/widget/Adapter;I)Lccg;

    move-result-object v4

    iget-object v4, v4, Lccg;->a:Landroid/view/View;

    int-to-float v6, v2

    sub-float/2addr v0, v6

    iget-boolean v6, p1, Lccj;->a:Z

    if-eqz v6, :cond_4

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v6

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v6

    iget-object v6, p1, Lccj;->d:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iget-object v7, p1, Lccj;->d:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->right:I

    invoke-static {v6, v7, v0}, Lcfn;->a(IIF)I

    move-result v0

    sub-int v0, v4, v0

    :goto_8
    iput v0, p1, Lccj;->i:I

    move v0, v2

    goto/16 :goto_1

    :cond_4
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v6

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v4, v6

    iget-object v6, p1, Lccj;->d:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iget-object v7, p1, Lccj;->d:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->bottom:I

    invoke-static {v6, v7, v0}, Lcfn;->a(IIF)I

    move-result v0

    sub-int v0, v4, v0

    goto :goto_8

    .line 172
    :pswitch_2
    iget v0, p0, Lcdm;->f:I

    iget-object v2, p1, Lccj;->h:Landroid/widget/Adapter;

    invoke-interface {v2}, Landroid/widget/Adapter;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_15

    .line 173
    iget v0, p0, Lcdm;->f:I

    .line 174
    iget v2, p0, Lcdm;->f:I

    iget v4, p0, Lcdm;->g:I

    iget v6, p0, Lcdm;->h:I

    invoke-direct {p0, p1, v2, v4, v6}, Lcdm;->a(Lccj;III)V

    .line 177
    :goto_9
    iput v1, p0, Lcdm;->f:I

    .line 178
    iput v3, p0, Lcdm;->g:I

    .line 179
    iput v3, p0, Lcdm;->h:I

    goto/16 :goto_1

    .line 183
    :pswitch_3
    iget v0, p0, Lcdm;->f:I

    iget-object v2, p1, Lccj;->h:Landroid/widget/Adapter;

    invoke-interface {v2}, Landroid/widget/Adapter;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_14

    .line 184
    iget v0, p0, Lcdm;->f:I

    .line 185
    iget-object v2, p1, Lccj;->e:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    .line 186
    iget-object v4, p1, Lccj;->e:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerY()I

    move-result v4

    .line 187
    iget v6, p0, Lcdm;->f:I

    invoke-direct {p0, p1, v6, v2, v4}, Lcdm;->a(Lccj;III)V

    .line 189
    :goto_a
    iput v1, p0, Lcdm;->f:I

    goto/16 :goto_1

    .line 196
    :cond_5
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_2

    :cond_6
    move v0, v3

    goto/16 :goto_3

    .line 206
    :cond_7
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v2

    goto/16 :goto_5

    :cond_8
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v4

    goto/16 :goto_6

    :cond_9
    if-nez v2, :cond_a

    move v2, v5

    :goto_b
    if-eqz v2, :cond_b

    if-le v6, v10, :cond_b

    iget v2, p1, Lccj;->i:I

    sub-int v4, v6, v10

    add-int/2addr v2, v4

    iput v2, p1, Lccj;->i:I

    goto/16 :goto_4

    :cond_a
    move v2, v3

    goto :goto_b

    :cond_b
    if-eqz v2, :cond_c

    if-ne v6, v10, :cond_c

    move v2, v5

    :goto_c
    iput-boolean v2, p1, Lccj;->j:Z

    move v2, v0

    :goto_d
    add-int/lit8 v6, v7, -0x1

    if-ge v2, v6, :cond_d

    if-ge v4, v9, :cond_d

    iget v6, p1, Lccj;->b:I

    add-int/2addr v4, v6

    add-int/lit8 v2, v2, 0x1

    invoke-direct {v1, p1, v2, v4, v5}, Lcdm;->a(Lccj;IIZ)I

    move-result v4

    goto :goto_d

    :cond_c
    move v2, v3

    goto :goto_c

    :cond_d
    add-int/lit8 v6, v7, -0x1

    if-ne v2, v6, :cond_e

    move v2, v5

    :goto_e
    iget-boolean v6, p1, Lccj;->j:Z

    if-nez v6, :cond_f

    if-eqz v2, :cond_f

    if-ge v4, v11, :cond_f

    iget v2, p1, Lccj;->i:I

    sub-int/2addr v4, v11

    add-int/2addr v2, v4

    iput v2, p1, Lccj;->i:I

    goto/16 :goto_4

    :cond_e
    move v2, v3

    goto :goto_e

    :cond_f
    if-eqz v2, :cond_10

    if-gt v4, v11, :cond_10

    move v0, v5

    :goto_f
    iput-boolean v0, p1, Lccj;->k:Z

    iget-boolean v0, p1, Lccj;->f:Z

    if-eqz v0, :cond_11

    sget-object v0, Lcdm;->b:Ljava/lang/String;

    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v2

    move v1, v3

    :goto_10
    if-ge v1, v2, :cond_11

    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    sget-object v6, Lcdm;->b:Ljava/lang/String;

    const-string v6, "(%s): Position=%s Major=%s Item=%s"

    const/4 v7, 0x4

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v5

    iget-object v4, v0, Lccg;->a:Landroid/view/View;

    invoke-virtual {p1, v4}, Lccj;->a(Landroid/view/View;)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v7, v13

    const/4 v4, 0x3

    aput-object v0, v7, v4

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_10

    :cond_10
    move v0, v3

    goto :goto_f

    .line 209
    :cond_11
    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_12

    invoke-virtual {p0, p1}, Lcdm;->a(Lccj;)V

    :cond_12
    iget-object v1, p1, Lccj;->c:Landroid/util/SparseArray;

    div-int/lit8 v2, v0, 0x2

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v1

    iget-object v2, p1, Lccj;->c:Landroid/util/SparseArray;

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    iget-object v0, v0, Lccg;->a:Landroid/view/View;

    iget-boolean v2, p1, Lccj;->a:Z

    if-eqz v2, :cond_13

    iget-object v2, p1, Lccj;->d:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget-object v3, p1, Lccj;->d:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    int-to-float v3, v3

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v4

    iget v4, p1, Lccj;->i:I

    sub-int/2addr v0, v4

    int-to-float v0, v0

    invoke-static {v2, v3, v0}, Lcfn;->c(FFF)F

    move-result v0

    :goto_11
    int-to-float v1, v1

    add-float/2addr v0, v1

    iput v0, p0, Lcdm;->e:F

    .line 211
    iget-object v0, p0, Lcdm;->i:Lcdn;

    if-eqz v0, :cond_0

    .line 212
    iget-object v0, p0, Lcdm;->i:Lcdn;

    invoke-interface {v0}, Lcdn;->a()V

    goto/16 :goto_0

    .line 209
    :cond_13
    iget-object v2, p1, Lccj;->d:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    iget-object v3, p1, Lccj;->d:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    add-int/2addr v0, v4

    iget v4, p1, Lccj;->i:I

    sub-int/2addr v0, v4

    int-to-float v0, v0

    invoke-static {v2, v3, v0}, Lcfn;->c(FFF)F

    move-result v0

    goto :goto_11

    :cond_14
    move v0, v1

    goto/16 :goto_a

    :cond_15
    move v0, v1

    goto/16 :goto_9

    .line 163
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Lcdn;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcdm;->i:Lcdn;

    .line 108
    return-void
.end method
