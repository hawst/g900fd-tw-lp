.class final Lfsi;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:[[Landroid/graphics/Point;

.field private final b:I

.field private final c:Lfsj;


# direct methods
.method public constructor <init>([[Landroid/graphics/Point;ILfsj;)V
    .locals 0

    .prologue
    .line 894
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 895
    iput-object p1, p0, Lfsi;->a:[[Landroid/graphics/Point;

    .line 896
    iput p2, p0, Lfsi;->b:I

    .line 897
    iput-object p3, p0, Lfsi;->c:Lfsj;

    .line 898
    return-void
.end method


# virtual methods
.method public a(Lfrt;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 901
    move v0, v1

    :goto_0
    invoke-interface {p1}, Lfrt;->i()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 902
    invoke-interface {p1, v0}, Lfrt;->b(I)I

    move-result v2

    iget-object v3, p0, Lfsi;->a:[[Landroid/graphics/Point;

    aget-object v3, v3, v0

    int-to-float v2, v2

    iget v4, p0, Lfsi;->b:I

    int-to-float v4, v4

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    add-float/2addr v2, v4

    float-to-int v4, v2

    move v2, v1

    :goto_1
    array-length v5, v3

    if-ge v2, v5, :cond_1

    aget-object v5, v3, v2

    iget v5, v5, Landroid/graphics/Point;->x:I

    sub-int v5, v4, v5

    int-to-float v5, v5

    iget v6, p0, Lfsi;->b:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    const/high16 v6, -0x40800000    # -1.0f

    cmpl-float v6, v5, v6

    if-ltz v6, :cond_0

    const/high16 v6, 0x3f800000    # 1.0f

    cmpg-float v6, v5, v6

    if-gtz v6, :cond_0

    iget-object v6, p0, Lfsi;->c:Lfsj;

    invoke-interface {v6, v0, v2, v5}, Lfsj;->a(IIF)V

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 901
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 904
    :cond_2
    return-void
.end method
