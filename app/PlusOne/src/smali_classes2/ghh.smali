.class public abstract Lghh;
.super Ljava/lang/Object;

# interfaces
.implements Lgfr;
.implements Lghr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/os/IInterface;",
        ">",
        "Ljava/lang/Object;",
        "Lgfr;",
        "Lghr;"
    }
.end annotation


# static fields
.field public static final c:[Ljava/lang/String;


# instance fields
.field public final a:Landroid/content/Context;

.field final b:Landroid/os/Handler;

.field private final d:Landroid/os/Looper;

.field private e:Landroid/os/IInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lghh",
            "<TT;>.ghj<*>;>;"
        }
    .end annotation
.end field

.field private g:Lghm;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lghh",
            "<TT;>.ghm;"
        }
    .end annotation
.end field

.field private volatile h:I

.field private i:Z

.field private final j:Lghp;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "service_esmobile"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "service_googleme"

    aput-object v2, v0, v1

    sput-object v0, Lghh;->c:[Ljava/lang/String;

    return-void
.end method

.method public varargs constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lggb;Lggc;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lghh;->f:Ljava/util/ArrayList;

    const/4 v0, 0x1

    iput v0, p0, Lghh;->h:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lghh;->i:Z

    invoke-static {p1}, Lgih;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lghh;->a:Landroid/content/Context;

    const-string v0, "Looper must not be null"

    invoke-static {p2, v0}, Lgih;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Looper;

    iput-object v0, p0, Lghh;->d:Landroid/os/Looper;

    new-instance v0, Lghp;

    invoke-direct {v0, p2, p0}, Lghp;-><init>(Landroid/os/Looper;Lghr;)V

    iput-object v0, p0, Lghh;->j:Lghp;

    new-instance v0, Lghi;

    invoke-direct {v0, p0, p2}, Lghi;-><init>(Lghh;Landroid/os/Looper;)V

    iput-object v0, p0, Lghh;->b:Landroid/os/Handler;

    invoke-static {p3}, Lgih;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggb;

    invoke-virtual {p0, v0}, Lghh;->a(Lggb;)V

    invoke-static {p4}, Lgih;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggc;

    invoke-virtual {p0, v0}, Lghh;->a(Lggc;)V

    return-void
.end method

.method public varargs constructor <init>(Landroid/content/Context;Lgfg;Lgfh;[Ljava/lang/String;)V
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    new-instance v1, Lghk;

    invoke-direct {v1, p2}, Lghk;-><init>(Lgfg;)V

    new-instance v2, Lghn;

    invoke-direct {v2, p3}, Lghn;-><init>(Lgfh;)V

    invoke-direct {p0, p1, v0, v1, v2}, Lghh;-><init>(Landroid/content/Context;Landroid/os/Looper;Lggb;Lggc;)V

    return-void
.end method

.method static synthetic a(Lghh;Landroid/os/IInterface;)Landroid/os/IInterface;
    .locals 0

    iput-object p1, p0, Lghh;->e:Landroid/os/IInterface;

    return-object p1
.end method

.method static synthetic a(Lghh;Lghm;)Lghm;
    .locals 0

    iput-object p1, p0, Lghh;->g:Lghm;

    return-object p1
.end method

.method static synthetic a(Lghh;)Lghp;
    .locals 1

    iget-object v0, p0, Lghh;->j:Lghp;

    return-object v0
.end method

.method static synthetic a(Lghh;I)V
    .locals 0

    invoke-direct {p0, p1}, Lghh;->b(I)V

    return-void
.end method

.method static synthetic b(Lghh;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lghh;->f:Ljava/util/ArrayList;

    return-object v0
.end method

.method private b(I)V
    .locals 1

    iget v0, p0, Lghh;->h:I

    iput p1, p0, Lghh;->h:I

    return-void
.end method

.method static synthetic c(Lghh;)Landroid/os/IInterface;
    .locals 1

    iget-object v0, p0, Lghh;->e:Landroid/os/IInterface;

    return-object v0
.end method

.method static synthetic d(Lghh;)Lghm;
    .locals 1

    iget-object v0, p0, Lghh;->g:Lghm;

    return-object v0
.end method

.method static synthetic e(Lghh;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lghh;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public abstract a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/os/IBinder;",
            ")TT;"
        }
    .end annotation
.end method

.method public a()V
    .locals 4

    const/4 v3, 0x3

    const/4 v1, 0x1

    iput-boolean v1, p0, Lghh;->i:Z

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lghh;->b(I)V

    iget-object v0, p0, Lghh;->a:Landroid/content/Context;

    invoke-static {v0}, Lgfk;->a(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0, v1}, Lghh;->b(I)V

    iget-object v1, p0, Lghh;->b:Landroid/os/Handler;

    iget-object v2, p0, Lghh;->b:Landroid/os/Handler;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lghh;->g:Lghm;

    if-eqz v0, :cond_2

    const-string v0, "GmsClient"

    const-string v1, "Calling connect() while still connected, missing disconnect()."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lghh;->e:Landroid/os/IInterface;

    iget-object v0, p0, Lghh;->a:Landroid/content/Context;

    invoke-static {v0}, Lghs;->a(Landroid/content/Context;)Lghs;

    move-result-object v0

    invoke-virtual {p0}, Lghh;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lghh;->g:Lghm;

    invoke-virtual {v0, v1, v2}, Lghs;->b(Ljava/lang/String;Lghm;)V

    :cond_2
    new-instance v0, Lghm;

    invoke-direct {v0, p0}, Lghm;-><init>(Lghh;)V

    iput-object v0, p0, Lghh;->g:Lghm;

    iget-object v0, p0, Lghh;->a:Landroid/content/Context;

    invoke-static {v0}, Lghs;->a(Landroid/content/Context;)Lghs;

    move-result-object v0

    invoke-virtual {p0}, Lghh;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lghh;->g:Lghm;

    invoke-virtual {v0, v1, v2}, Lghs;->a(Ljava/lang/String;Lghm;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GmsClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unable to connect to service: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lghh;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lghh;->b:Landroid/os/Handler;

    iget-object v1, p0, Lghh;->b:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public a(I)V
    .locals 4

    iget-object v0, p0, Lghh;->b:Landroid/os/Handler;

    iget-object v1, p0, Lghh;->b:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 4

    iget-object v0, p0, Lghh;->b:Landroid/os/Handler;

    iget-object v1, p0, Lghh;->b:Landroid/os/Handler;

    const/4 v2, 0x1

    new-instance v3, Lgho;

    invoke-direct {v3, p0, p1, p2, p3}, Lgho;-><init>(Lghh;ILandroid/os/IBinder;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public a(Lggb;)V
    .locals 1

    iget-object v0, p0, Lghh;->j:Lghp;

    invoke-virtual {v0, p1}, Lghp;->a(Lggb;)V

    return-void
.end method

.method public a(Lggc;)V
    .locals 1

    iget-object v0, p0, Lghh;->j:Lghp;

    invoke-virtual {v0, p1}, Lghp;->a(Lgfh;)V

    return-void
.end method

.method public final a(Lghj;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lghh",
            "<TT;>.ghj<*>;)V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v1, p0, Lghh;->f:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lghh;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lghh;->b:Landroid/os/Handler;

    iget-object v1, p0, Lghh;->b:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public abstract a(Lgic;Lghl;)V
.end method

.method public aH_()Landroid/os/Bundle;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public aI_()Z
    .locals 1

    iget-boolean v0, p0, Lghh;->i:Z

    return v0
.end method

.method public b()V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lghh;->i:Z

    iget-object v2, p0, Lghh;->f:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v1, p0, Lghh;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lghh;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lghj;

    invoke-virtual {v0}, Lghj;->f()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lghh;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lghh;->b(I)V

    iput-object v4, p0, Lghh;->e:Landroid/os/IInterface;

    iget-object v0, p0, Lghh;->g:Lghm;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lghh;->a:Landroid/content/Context;

    invoke-static {v0}, Lghs;->a(Landroid/content/Context;)Lghs;

    move-result-object v0

    invoke-virtual {p0}, Lghh;->e()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lghh;->g:Lghm;

    invoke-virtual {v0, v1, v2}, Lghs;->b(Ljava/lang/String;Lghm;)V

    iput-object v4, p0, Lghh;->g:Lghm;

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public c()Z
    .locals 2

    iget v0, p0, Lghh;->h:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Landroid/os/Looper;
    .locals 1

    iget-object v0, p0, Lghh;->d:Landroid/os/Looper;

    return-object v0
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public abstract f()Ljava/lang/String;
.end method

.method public g()Z
    .locals 2

    iget v0, p0, Lghh;->h:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()V
    .locals 2

    invoke-virtual {p0}, Lghh;->c()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public final i()Landroid/os/IInterface;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    invoke-virtual {p0}, Lghh;->h()V

    iget-object v0, p0, Lghh;->e:Landroid/os/IInterface;

    return-object v0
.end method
