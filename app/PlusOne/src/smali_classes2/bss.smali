.class public Lbss;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Lbss;


# instance fields
.field private final b:Lbst;

.field private final c:J

.field private volatile d:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 14
    new-instance v0, Lbss;

    sget-object v1, Lbst;->a:Lbst;

    invoke-direct {v0, v1, v2, v3}, Lbss;-><init>(Lbst;J)V

    sput-object v0, Lbss;->a:Lbss;

    .line 16
    new-instance v0, Lbss;

    sget-object v1, Lbst;->c:Lbst;

    invoke-direct {v0, v1, v2, v3}, Lbss;-><init>(Lbst;J)V

    return-void
.end method

.method private constructor <init>(Lbst;J)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Lbss;->b:Lbst;

    .line 33
    iput-wide p2, p0, Lbss;->c:J

    .line 34
    return-void
.end method

.method public static a()Lbss;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lbss;->a:Lbss;

    return-object v0
.end method

.method public static a(J)Lbss;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lbss;

    sget-object v1, Lbst;->a:Lbst;

    invoke-direct {v0, v1, p0, p1}, Lbss;-><init>(Lbst;J)V

    return-object v0
.end method

.method public static b(J)Lbss;
    .locals 2

    .prologue
    .line 61
    new-instance v0, Lbss;

    sget-object v1, Lbst;->b:Lbst;

    invoke-direct {v0, v1, p0, p1}, Lbss;-><init>(Lbst;J)V

    return-object v0
.end method


# virtual methods
.method public b()Z
    .locals 2

    .prologue
    .line 104
    iget-object v0, p0, Lbss;->b:Lbst;

    sget-object v1, Lbst;->b:Lbst;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 112
    iget-wide v0, p0, Lbss;->c:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 81
    if-ne p0, p1, :cond_1

    .line 90
    :cond_0
    :goto_0
    return v0

    .line 85
    :cond_1
    instance-of v2, p1, Lbss;

    if-nez v2, :cond_2

    move v0, v1

    .line 86
    goto :goto_0

    .line 89
    :cond_2
    check-cast p1, Lbss;

    .line 90
    iget-object v2, p0, Lbss;->b:Lbst;

    iget-object v3, p1, Lbss;->b:Lbst;

    if-ne v2, v3, :cond_3

    iget-wide v2, p0, Lbss;->c:J

    iget-wide v4, p1, Lbss;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 96
    iget v0, p0, Lbss;->d:I

    if-nez v0, :cond_0

    .line 97
    iget-object v0, p0, Lbss;->b:Lbst;

    iget-wide v2, p0, Lbss;->c:J

    const/16 v1, 0x11

    .line 98
    invoke-static {v2, v3, v1}, Lbqh;->a(JI)I

    move-result v1

    .line 97
    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v0

    iput v0, p0, Lbss;->d:I

    .line 100
    :cond_0
    iget v0, p0, Lbss;->d:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 75
    const-class v0, Lbss;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lbss;->b:Lbst;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lbss;->c:J

    .line 76
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 75
    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
