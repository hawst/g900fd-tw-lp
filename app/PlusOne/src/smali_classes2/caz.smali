.class public final Lcaz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field private synthetic a:Landroid/view/ViewTreeObserver;

.field private synthetic b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;Landroid/view/ViewTreeObserver;)V
    .locals 0

    .prologue
    .line 229
    iput-object p1, p0, Lcaz;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    iput-object p2, p0, Lcaz;->a:Landroid/view/ViewTreeObserver;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 232
    iget-object v0, p0, Lcaz;->a:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v0

    if-nez v0, :cond_1

    .line 246
    :cond_0
    return v3

    .line 235
    :cond_1
    iget-object v0, p0, Lcaz;->a:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 237
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcaz;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 241
    iget-object v1, p0, Lcaz;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-static {v1}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a(Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;)[Landroid/animation/ObjectAnimator;

    move-result-object v1

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->setupEndValues()V

    .line 243
    iget-object v1, p0, Lcaz;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-static {v1}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a(Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;)[Landroid/animation/ObjectAnimator;

    move-result-object v1

    aget-object v1, v1, v0

    iget-object v2, p0, Lcaz;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-static {v2}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->b(Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;)Landroid/animation/AnimatorListenerAdapter;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 244
    iget-object v1, p0, Lcaz;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-static {v1}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a(Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;)[Landroid/animation/ObjectAnimator;

    move-result-object v1

    aget-object v1, v1, v0

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 237
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
