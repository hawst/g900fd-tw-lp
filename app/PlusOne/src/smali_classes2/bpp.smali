.class public Lbpp;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lbpp;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbpp;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 442
    return-void
.end method

.method public static a(FLbmy;JJ)F
    .locals 26

    .prologue
    .line 59
    const/high16 v4, 0x3f800000    # 1.0f

    cmpl-float v4, p0, v4

    if-nez v4, :cond_0

    .line 60
    const/4 v4, 0x0

    .line 81
    :goto_0
    return v4

    .line 63
    :cond_0
    const/4 v4, 0x1

    new-array v15, v4, [F

    .line 65
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual/range {p1 .. p1}, Lbmy;->c()Landroid/util/LongSparseArray;

    move-result-object v19

    invoke-virtual/range {p1 .. p1}, Lbmy;->b()Landroid/util/LongSparseArray;

    move-result-object v20

    add-long v4, p2, p4

    const-wide/16 v6, 0x2

    div-long v22, v4, v6

    const/4 v5, 0x0

    move-object/from16 v0, v19

    move-wide/from16 v1, p2

    invoke-static {v0, v1, v2}, Lcfk;->c(Landroid/util/LongSparseArray;J)I

    move-result v4

    const/4 v6, -0x1

    if-ne v4, v6, :cond_2

    const/4 v5, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    move-object v6, v15

    :goto_1
    aput v4, v6, v5

    .line 68
    const/high16 v4, 0x3f800000    # 1.0f

    cmpg-float v4, p0, v4

    if-gez v4, :cond_b

    .line 69
    const/4 v10, 0x0

    move-object/from16 v5, p1

    move-wide/from16 v6, p2

    move-wide/from16 v8, p4

    invoke-static/range {v5 .. v10}, Lbpp;->a(Lbmy;JJZ)[J

    move-result-object v4

    .line 73
    const/4 v5, 0x0

    aget v5, v15, v5

    const/high16 v6, 0x3f800000    # 1.0f

    sub-float v6, v6, p0

    div-float v5, p0, v5

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_9

    move-object/from16 v0, v18

    invoke-static {v0, v5}, Lbpp;->a(Ljava/util/List;F)Lbpq;

    move-result-object v7

    iget v4, v7, Lbpq;->d:F

    iget v8, v7, Lbpq;->a:F

    sub-float/2addr v4, v8

    cmpl-float v8, v4, v5

    if-lez v8, :cond_8

    iget v4, v7, Lbpq;->b:F

    const/4 v8, 0x0

    iget v9, v7, Lbpq;->c:F

    sub-float v9, v5, v9

    iget v10, v7, Lbpq;->b:F

    add-float/2addr v9, v10

    const/high16 v10, 0x40400000    # 3.0f

    div-float/2addr v9, v10

    invoke-static {v8, v9}, Ljava/lang/Math;->max(FF)F

    move-result v8

    sub-float/2addr v4, v8

    :goto_2
    iget v8, v7, Lbpq;->b:F

    invoke-static {v8, v4}, Ljava/lang/Math;->min(FF)F

    move-result v4

    iget v8, v7, Lbpq;->c:F

    iget v9, v7, Lbpq;->b:F

    sub-float/2addr v8, v9

    const v9, 0x3fa66666    # 1.3f

    mul-float/2addr v9, v5

    cmpl-float v8, v8, v9

    if-lez v8, :cond_1

    invoke-virtual {v7}, Lbpq;->a()F

    move-result v4

    const/high16 v7, 0x40000000    # 2.0f

    div-float v7, p0, v7

    sub-float/2addr v4, v7

    :cond_1
    const/4 v7, 0x0

    sub-float v5, p0, v5

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v5, v8

    sub-float/2addr v4, v5

    invoke-static {v7, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    :goto_3
    const/4 v5, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    sub-float v7, v7, p0

    invoke-static {v4, v5, v7}, Lcfn;->a(FFF)F

    move-result v4

    sub-float v4, v6, v4

    goto/16 :goto_0

    :cond_2
    move v13, v4

    .line 65
    :goto_4
    invoke-virtual/range {v19 .. v19}, Landroid/util/LongSparseArray;->size()I

    move-result v4

    if-ge v13, v4, :cond_6

    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v24

    cmp-long v4, v24, p4

    if-gez v4, :cond_6

    move-object/from16 v0, v20

    move-wide/from16 v1, v24

    invoke-virtual {v0, v1, v2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbxi;

    if-nez v5, :cond_11

    move-object v14, v4

    :goto_5
    if-nez v4, :cond_3

    const/4 v5, 0x0

    move/from16 v17, v5

    :goto_6
    if-nez v4, :cond_4

    const/4 v4, 0x0

    move v12, v4

    :goto_7
    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Lbbo;

    array-length v0, v4

    move/from16 v21, v0

    const/4 v5, 0x0

    move/from16 v16, v5

    :goto_8
    move/from16 v0, v16

    move/from16 v1, v21

    if-ge v0, v1, :cond_5

    aget-object v5, v4, v16

    invoke-virtual {v5}, Lbbo;->b()Landroid/hardware/Camera$Face;

    move-result-object v5

    iget-object v9, v5, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    new-instance v5, Lbpq;

    iget v6, v9, Landroid/graphics/Rect;->top:I

    int-to-float v6, v6

    const/high16 v7, 0x447a0000    # 1000.0f

    add-float/2addr v6, v7

    const/high16 v7, 0x44fa0000    # 2000.0f

    div-float/2addr v6, v7

    sub-float/2addr v6, v12

    iget v7, v9, Landroid/graphics/Rect;->bottom:I

    int-to-float v7, v7

    const/high16 v8, 0x447a0000    # 1000.0f

    add-float/2addr v7, v8

    const/high16 v8, 0x44fa0000    # 2000.0f

    div-float/2addr v7, v8

    sub-float/2addr v7, v12

    iget v8, v9, Landroid/graphics/Rect;->left:I

    int-to-float v8, v8

    const/high16 v10, 0x447a0000    # 1000.0f

    add-float/2addr v8, v10

    const/high16 v10, 0x44fa0000    # 2000.0f

    div-float/2addr v8, v10

    sub-float v8, v8, v17

    iget v9, v9, Landroid/graphics/Rect;->right:I

    int-to-float v9, v9

    const/high16 v10, 0x447a0000    # 1000.0f

    add-float/2addr v9, v10

    const/high16 v10, 0x44fa0000    # 2000.0f

    div-float/2addr v9, v10

    sub-float v9, v9, v17

    sub-long v10, v24, v22

    invoke-static {v10, v11}, Ljava/lang/Math;->abs(J)J

    move-result-wide v10

    invoke-direct/range {v5 .. v11}, Lbpq;-><init>(FFFFJ)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v16, 0x1

    move/from16 v16, v5

    goto :goto_8

    :cond_3
    invoke-virtual {v4}, Lbxi;->d()F

    move-result v5

    move/from16 v17, v5

    goto :goto_6

    :cond_4
    invoke-virtual {v4}, Lbxi;->e()F

    move-result v4

    move v12, v4

    goto :goto_7

    :cond_5
    add-int/lit8 v4, v13, 0x1

    move v13, v4

    move-object v5, v14

    goto/16 :goto_4

    :cond_6
    const/4 v6, 0x0

    if-nez v5, :cond_7

    const/high16 v4, 0x3f800000    # 1.0f

    move v5, v6

    move-object v6, v15

    goto/16 :goto_1

    :cond_7
    invoke-virtual {v5}, Lbxi;->f()F

    move-result v4

    move v5, v6

    move-object v6, v15

    goto/16 :goto_1

    .line 73
    :cond_8
    iget v8, v7, Lbpq;->a:F

    sub-float v4, v5, v4

    const/high16 v9, 0x40400000    # 3.0f

    div-float/2addr v4, v9

    sub-float v4, v8, v4

    goto/16 :goto_2

    :cond_9
    if-eqz v4, :cond_a

    move/from16 v0, p0

    invoke-static {v4, v0}, Lbpp;->a([JF)F

    move-result v4

    goto/16 :goto_3

    :cond_a
    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v5, 0x3f800000    # 1.0f

    sub-float v5, v5, p0

    mul-float/2addr v4, v5

    const/high16 v5, 0x40a00000    # 5.0f

    div-float/2addr v4, v5

    goto/16 :goto_3

    .line 77
    :cond_b
    const/4 v10, 0x1

    move-object/from16 v5, p1

    move-wide/from16 v6, p2

    move-wide/from16 v8, p4

    invoke-static/range {v5 .. v10}, Lbpp;->a(Lbmy;JJZ)[J

    move-result-object v4

    .line 81
    const/high16 v5, 0x3f800000    # 1.0f

    div-float v5, v5, p0

    const/4 v6, 0x0

    aget v6, v15, v6

    invoke-interface/range {v18 .. v18}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_f

    div-float v6, v5, v6

    move-object/from16 v0, v18

    invoke-static {v0, v6}, Lbpp;->b(Ljava/util/List;F)Lbpq;

    move-result-object v7

    iget v4, v7, Lbpq;->f:F

    iget v8, v7, Lbpq;->e:F

    sub-float/2addr v4, v8

    invoke-virtual {v7}, Lbpq;->b()F

    move-result v8

    cmpl-float v4, v4, v6

    if-lez v4, :cond_d

    const/high16 v4, 0x40000000    # 2.0f

    div-float v4, v6, v4

    sub-float v4, v8, v4

    :cond_c
    :goto_9
    const/4 v7, 0x0

    sub-float v6, v5, v6

    const/high16 v8, 0x40000000    # 2.0f

    div-float/2addr v6, v8

    sub-float/2addr v4, v6

    invoke-static {v7, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    :goto_a
    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    sub-float v5, v7, v5

    invoke-static {v4, v6, v5}, Lcfn;->a(FFF)F

    move-result v4

    goto/16 :goto_0

    :cond_d
    mul-float v4, v6, v8

    sub-float v4, v8, v4

    iget v8, v7, Lbpq;->e:F

    cmpl-float v8, v4, v8

    if-lez v8, :cond_e

    iget v4, v7, Lbpq;->e:F

    :cond_e
    add-float v8, v4, v6

    iget v9, v7, Lbpq;->f:F

    cmpg-float v8, v8, v9

    if-gez v8, :cond_c

    iget v4, v7, Lbpq;->f:F

    sub-float/2addr v4, v6

    goto :goto_9

    :cond_f
    if-eqz v4, :cond_10

    invoke-static {v4, v5}, Lbpp;->a([JF)F

    move-result v4

    goto :goto_a

    :cond_10
    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v4, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v4, v6

    goto :goto_a

    :cond_11
    move-object v14, v5

    goto/16 :goto_5
.end method

.method public static a([JF)F
    .locals 11

    .prologue
    const-wide/16 v4, 0x0

    const/4 v10, 0x1

    const/4 v0, 0x0

    .line 202
    const-string v1, "proportion"

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {p1, v1, v2, v3}, Lcec;->a(FLjava/lang/CharSequence;FF)F

    .line 207
    array-length v1, p0

    int-to-float v1, v1

    mul-float/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    invoke-static {v10, v1}, Ljava/lang/Math;->max(II)I

    move-result v6

    move v1, v0

    move-wide v2, v4

    .line 210
    :goto_0
    add-int/lit8 v7, v6, -0x1

    if-ge v1, v7, :cond_0

    .line 211
    aget-wide v8, p0, v1

    add-long/2addr v2, v8

    .line 210
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    move v1, v0

    .line 213
    :goto_1
    array-length v7, p0

    sub-int/2addr v7, v6

    if-gt v0, v7, :cond_3

    .line 214
    add-int v7, v0, v6

    add-int/lit8 v7, v7, -0x1

    aget-wide v8, p0, v7

    add-long/2addr v2, v8

    .line 215
    if-eqz v0, :cond_1

    cmp-long v7, v2, v4

    if-lez v7, :cond_2

    :cond_1
    move v1, v0

    move-wide v4, v2

    .line 219
    :cond_2
    aget-wide v8, p0, v0

    sub-long/2addr v2, v8

    .line 213
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 222
    :cond_3
    int-to-float v0, v1

    array-length v1, p0

    invoke-static {v10, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    return v0
.end method

.method public static a(Lont;II)Lbmj;
    .locals 2

    .prologue
    .line 536
    invoke-static {p0, p1, p2}, Lbpp;->b(Lont;II)Loog;

    move-result-object v0

    .line 538
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lbmj;->a(Loog;F)Lbmj;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/util/List;F)Lbpq;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbpq;",
            ">;F)",
            "Lbpq;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 298
    invoke-static {p0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 300
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpq;

    .line 301
    iget v3, v0, Lbpq;->b:F

    .line 302
    iget v1, v0, Lbpq;->a:F

    .line 303
    iget v2, v0, Lbpq;->c:F

    .line 304
    iget v4, v0, Lbpq;->d:F

    .line 306
    const/4 v0, 0x1

    move v6, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_0

    .line 307
    invoke-interface {p0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpq;

    .line 308
    iget v7, v0, Lbpq;->b:F

    invoke-static {v7, v3}, Ljava/lang/Math;->min(FF)F

    move-result v8

    .line 310
    iget v7, v0, Lbpq;->c:F

    invoke-static {v7, v2}, Ljava/lang/Math;->max(FF)F

    move-result v7

    .line 311
    sub-float v9, v7, v8

    cmpl-float v9, v9, p1

    if-gtz v9, :cond_0

    .line 318
    iget v2, v0, Lbpq;->a:F

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 321
    iget v0, v0, Lbpq;->d:F

    invoke-static {v0, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    .line 306
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move v2, v7

    move v3, v8

    goto :goto_0

    .line 324
    :cond_0
    new-instance v0, Lbpq;

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lbpq;-><init>(FFFFFF)V

    return-object v0
.end method

.method private static a(Lbmy;JJZ)[J
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v7, -0x1

    .line 381
    cmp-long v0, p1, p3

    if-gtz v0, :cond_4

    move v0, v1

    :goto_0
    const-string v3, "startPointUs must be less or equal to endPointUs"

    invoke-static {v0, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 384
    if-eqz p5, :cond_5

    .line 385
    invoke-virtual {p0}, Lbmy;->e()Landroid/util/LongSparseArray;

    move-result-object v0

    move-object v3, v0

    .line 388
    :goto_1
    invoke-static {v3, p1, p2}, Lcfk;->b(Landroid/util/LongSparseArray;J)I

    move-result v6

    .line 390
    if-eq v6, v7, :cond_6

    .line 391
    invoke-virtual {v3, v6}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    move-object v4, v0

    .line 394
    :goto_2
    invoke-static {v3, p3, p4}, Lcfk;->a(Landroid/util/LongSparseArray;J)I

    move-result v0

    .line 398
    if-eq v0, v7, :cond_0

    if-ne v0, v6, :cond_1

    .line 399
    :cond_0
    invoke-static {v3, p3, p4}, Lcfk;->c(Landroid/util/LongSparseArray;J)I

    move-result v0

    .line 401
    :cond_1
    if-eq v0, v7, :cond_7

    .line 402
    invoke-virtual {v3, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    .line 405
    :goto_3
    if-eqz v0, :cond_2

    if-ne v4, v0, :cond_8

    :cond_2
    move-object v5, v4

    .line 411
    :cond_3
    :goto_4
    return-object v5

    :cond_4
    move v0, v2

    .line 381
    goto :goto_0

    .line 386
    :cond_5
    invoke-virtual {p0}, Lbmy;->f()Landroid/util/LongSparseArray;

    move-result-object v0

    move-object v3, v0

    goto :goto_1

    :cond_6
    move-object v4, v5

    .line 391
    goto :goto_2

    :cond_7
    move-object v0, v5

    .line 402
    goto :goto_3

    .line 410
    :cond_8
    if-eqz v4, :cond_a

    .line 411
    array-length v3, v4

    array-length v6, v0

    if-eq v3, v6, :cond_9

    sget-object v3, Lbpp;->a:Ljava/lang/String;

    const-string v3, "Invalid length of projection, %d was expected, but it was %d."

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    array-length v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v2

    array-length v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v1

    invoke-static {v3, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    goto :goto_4

    :cond_9
    array-length v1, v4

    new-array v5, v1, [J

    :goto_5
    array-length v1, v4

    if-ge v2, v1, :cond_3

    aget-wide v6, v0, v2

    aget-wide v8, v4, v2

    sub-long/2addr v6, v8

    aput-wide v6, v5, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_a
    move-object v5, v0

    goto :goto_4
.end method

.method private static b(Ljava/util/List;F)Lbpq;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbpq;",
            ">;F)",
            "Lbpq;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 341
    invoke-static {p0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 343
    const/4 v0, 0x0

    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpq;

    .line 344
    iget v5, v0, Lbpq;->e:F

    .line 345
    iget v6, v0, Lbpq;->f:F

    .line 347
    const/4 v0, 0x1

    move v2, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 348
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbpq;

    .line 349
    iget v3, v0, Lbpq;->e:F

    invoke-static {v3, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    .line 350
    iget v0, v0, Lbpq;->f:F

    invoke-static {v0, v6}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 351
    sub-float v0, v3, v4

    cmpl-float v0, v0, p1

    if-gtz v0, :cond_0

    .line 347
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v6, v3

    move v5, v4

    goto :goto_0

    .line 361
    :cond_0
    new-instance v0, Lbpq;

    move v2, v1

    move v3, v1

    move v4, v1

    invoke-direct/range {v0 .. v6}, Lbpq;-><init>(FFFFFF)V

    return-object v0
.end method

.method public static b(Lont;II)Loog;
    .locals 11

    .prologue
    const/high16 v10, 0x40000000    # 2.0f

    const/4 v0, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    .line 544
    iget-object v1, p0, Lont;->a:Lonu;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lont;->a:Lonu;

    iget-object v1, v1, Lonu;->a:[Loog;

    array-length v1, v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lont;->a:Lonu;

    iget-object v1, v1, Lonu;->a:[Loog;

    aget-object v1, v1, v0

    iget-object v1, v1, Loog;->b:Ljava/lang/Float;

    if-nez v1, :cond_2

    .line 547
    :cond_0
    const/4 v0, 0x0

    .line 551
    :cond_1
    :goto_0
    return-object v0

    :cond_2
    iget-object v1, p0, Lont;->a:Lonu;

    iget-object v1, v1, Lonu;->a:[Loog;

    iget-object v2, p0, Lont;->a:Lonu;

    iget-object v2, v2, Lonu;->a:[Loog;

    aget-object v2, v2, v0

    iget-object v2, v2, Loog;->b:Ljava/lang/Float;

    .line 553
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 551
    int-to-float v3, p1

    mul-float/2addr v3, v9

    int-to-float v4, p2

    div-float/2addr v3, v4

    :goto_1
    array-length v4, v1

    if-ge v0, v4, :cond_4

    aget-object v4, v1, v0

    iget-object v4, v4, Loog;->b:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    sub-float/2addr v4, v3

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-double v4, v4

    const-wide v6, 0x3f847ae147ae147bL    # 0.01

    cmpg-double v4, v4, v6

    if-gez v4, :cond_3

    aget-object v0, v1, v0

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    new-instance v0, Loog;

    invoke-direct {v0}, Loog;-><init>()V

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Loog;->b:Ljava/lang/Float;

    cmpg-float v1, v3, v2

    if-gez v1, :cond_5

    div-float v1, v3, v2

    sub-float v1, v9, v1

    div-float/2addr v1, v10

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v0, Loog;->c:Ljava/lang/Float;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Loog;->d:Ljava/lang/Float;

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Loog;->e:Ljava/lang/Float;

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Loog;->f:Ljava/lang/Float;

    goto :goto_0

    :cond_5
    cmpl-float v1, v3, v2

    if-lez v1, :cond_1

    div-float v1, v2, v3

    sub-float v1, v9, v1

    div-float/2addr v1, v10

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v0, Loog;->c:Ljava/lang/Float;

    invoke-static {v8}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v0, Loog;->d:Ljava/lang/Float;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    iput-object v2, v0, Loog;->e:Ljava/lang/Float;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Loog;->f:Ljava/lang/Float;

    goto/16 :goto_0
.end method
