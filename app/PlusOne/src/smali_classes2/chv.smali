.class public final Lchv;
.super Lcjy;
.source "PG"

# interfaces
.implements Lhbm;


# instance fields
.field private W:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

.field private X:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

.field private Y:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcjy;-><init>()V

    return-void
.end method

.method static synthetic a(Lchv;I)V
    .locals 3

    .prologue
    const/16 v2, 0xc

    .line 28
    invoke-virtual {p0}, Lchv;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lchv;->N:Lcjk;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lchv;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    if-eq v0, p1, :cond_0

    invoke-direct {p0, p1}, Lchv;->i(I)V

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v2, v0, v1}, Lchv;->a(ILjava/lang/Object;Z)Z

    :cond_0
    return-void
.end method

.method private i(I)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 220
    iget-object v0, p0, Lchv;->W:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    if-nez v0, :cond_0

    .line 227
    :goto_0
    return-void

    .line 224
    :cond_0
    iget-object v3, p0, Lchv;->W:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    if-nez p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setSelected(Z)V

    .line 225
    iget-object v3, p0, Lchv;->X:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    if-ne p1, v1, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setSelected(Z)V

    .line 226
    iget-object v0, p0, Lchv;->Y:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    const/4 v3, 0x2

    if-ne p1, v3, :cond_3

    :goto_3
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setSelected(Z)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 224
    goto :goto_1

    :cond_2
    move v0, v2

    .line 225
    goto :goto_2

    :cond_3
    move v1, v2

    .line 226
    goto :goto_3
.end method


# virtual methods
.method protected U()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
    .locals 1

    .prologue
    .line 71
    invoke-virtual {p0}, Lchv;->ah()Lchp;

    move-result-object v0

    invoke-virtual {v0}, Lchp;->h()Lcom/google/android/libraries/photoeditor/core/FilterChain;

    move-result-object v0

    .line 72
    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->b()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    .line 76
    if-nez v0, :cond_0

    .line 77
    invoke-virtual {p0}, Lchv;->c()I

    move-result v0

    invoke-static {v0}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->e()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    goto :goto_0
.end method

.method protected V()V
    .locals 2

    .prologue
    .line 139
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a:Lcom/google/android/libraries/photoeditor/core/NativeCore;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a(Lhbm;)V

    .line 140
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lchv;->k(Z)V

    .line 142
    invoke-super {p0}, Lcjy;->V()V

    .line 143
    return-void
.end method

.method protected W()Lhbi;
    .locals 4

    .prologue
    .line 187
    iget-object v0, p0, Lchv;->V:Lhbf;

    if-nez v0, :cond_0

    .line 188
    new-instance v0, Lhbf;

    invoke-direct {v0}, Lhbf;-><init>()V

    iput-object v0, p0, Lchv;->V:Lhbf;

    .line 190
    :cond_0
    iget-object v0, p0, Lchv;->V:Lhbf;

    .line 191
    invoke-virtual {p0}, Lchv;->ah()Lchp;

    move-result-object v1

    invoke-virtual {v1}, Lchp;->e()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {p0}, Lchv;->ah()Lchp;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lchp;->a(Z)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 190
    invoke-virtual {v0, v1, v2}, Lhbf;->a(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 193
    iget-object v0, p0, Lchv;->V:Lhbf;

    return-object v0
.end method

.method public Y()V
    .locals 2

    .prologue
    .line 248
    invoke-virtual {p0}, Lchv;->x()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcia;

    invoke-direct {v1, p0}, Lcia;-><init>(Lchv;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 258
    return-void
.end method

.method public a(ILjava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    invoke-virtual {p0}, Lchv;->u()Z

    move-result v0

    if-nez v0, :cond_0

    .line 161
    const-string v0, "*UNKNOWN*"

    .line 181
    :goto_0
    return-object v0

    .line 164
    :cond_0
    const/16 v0, 0xc

    if-ne p1, v0, :cond_1

    .line 165
    check-cast p2, Ljava/lang/Integer;

    .line 166
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 177
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 168
    :pswitch_0
    const v0, 0x7f0a00a5

    invoke-virtual {p0, v0}, Lchv;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 171
    :pswitch_1
    const v0, 0x7f0a00a6

    invoke-virtual {p0, v0}, Lchv;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 174
    :pswitch_2
    const v0, 0x7f0a00a7

    invoke-virtual {p0, v0}, Lchv;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 181
    :cond_1
    invoke-super {p0, p1, p2}, Lcjy;->a(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 166
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 265
    invoke-virtual {p0}, Lchv;->x()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcib;

    invoke-direct {v1, p0}, Lcib;-><init>(Lchv;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 273
    return-void
.end method

.method protected a(Lckp;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 82
    const v1, 0x7f020378

    const v2, 0x7f020377

    const v0, 0x7f0a00a7

    .line 86
    invoke-virtual {p0, v0}, Lchv;->e_(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lchw;

    invoke-direct {v5, p0}, Lchw;-><init>(Lchv;)V

    move-object v0, p1

    .line 82
    invoke-virtual/range {v0 .. v5}, Lckp;->a(IIILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    move-result-object v0

    iput-object v0, p0, Lchv;->Y:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 94
    const v1, 0x7f02037c

    const v2, 0x7f02037b

    const v0, 0x7f0a00a6

    .line 98
    invoke-virtual {p0, v0}, Lchv;->e_(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lchx;

    invoke-direct {v5, p0}, Lchx;-><init>(Lchv;)V

    move-object v0, p1

    .line 94
    invoke-virtual/range {v0 .. v5}, Lckp;->a(IIILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    move-result-object v0

    iput-object v0, p0, Lchv;->X:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 106
    const v1, 0x7f02037a

    const v2, 0x7f020379

    const v0, 0x7f0a00a5

    .line 110
    invoke-virtual {p0, v0}, Lchv;->e_(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lchy;

    invoke-direct {v5, p0}, Lchy;-><init>(Lchv;)V

    move-object v0, p1

    .line 106
    invoke-virtual/range {v0 .. v5}, Lckp;->a(IIILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    move-result-object v0

    iput-object v0, p0, Lchv;->W:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 118
    invoke-virtual {p0}, Lchv;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    .line 119
    invoke-direct {p0, v0}, Lchv;->i(I)V

    .line 123
    sget-object v1, Ldxc;->i:Ldxc;

    .line 124
    if-eqz v1, :cond_0

    .line 125
    invoke-virtual {v1}, Ldxc;->f()I

    move-result v2

    .line 126
    invoke-virtual {v1}, Ldxc;->g()Ljava/lang/String;

    move-result-object v0

    .line 127
    invoke-virtual {v1}, Ldxc;->h()Ljava/lang/String;

    move-result-object v1

    .line 128
    invoke-virtual {p0}, Lchv;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v3

    .line 129
    const/16 v4, 0x1e2

    invoke-virtual {v3, v4, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterInteger(II)Z

    .line 130
    const/16 v2, 0x1e0

    if-eqz v0, :cond_1

    :goto_0
    invoke-virtual {v3, v2, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterString(ILjava/lang/String;)Z

    .line 132
    const/16 v2, 0x1e1

    if-eqz v1, :cond_2

    move-object v0, v1

    :goto_1
    invoke-virtual {v3, v2, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterString(ILjava/lang/String;)Z

    .line 135
    :cond_0
    return-void

    .line 130
    :cond_1
    const-string v0, ""

    goto :goto_0

    .line 132
    :cond_2
    const-string v0, ""

    goto :goto_1
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0}, Lcjy;->aO_()V

    .line 48
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a:Lcom/google/android/libraries/photoeditor/core/NativeCore;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a(Lhbm;)V

    .line 49
    return-void
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 36
    new-instance v0, Lhmk;

    sget-object v1, Lonc;->b:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method public b(ILjava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 214
    const/16 v0, 0xc

    if-ne p1, v0, :cond_0

    check-cast p2, Ljava/lang/Integer;

    .line 215
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid \"Strength\" value!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const v0, 0x7f0a00a7

    invoke-virtual {p0, v0}, Lchv;->e_(I)Ljava/lang/String;

    move-result-object v0

    .line 216
    :goto_0
    return-object v0

    .line 215
    :pswitch_1
    const v0, 0x7f0a00a6

    invoke-virtual {p0, v0}, Lchv;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0a00a5

    invoke-virtual {p0, v0}, Lchv;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 216
    :cond_0
    invoke-super {p0, p1, p2}, Lcjy;->b(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 215
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public c()I
    .locals 1

    .prologue
    .line 41
    const/16 v0, 0x12

    return v0
.end method

.method protected d()Z
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    return v0
.end method

.method protected e()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    const/4 v0, 0x0

    return-object v0
.end method

.method public x_()V
    .locals 2

    .prologue
    .line 234
    invoke-virtual {p0}, Lchv;->x()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lchz;

    invoke-direct {v1, p0}, Lchz;-><init>(Lchv;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 242
    return-void
.end method

.method public z()V
    .locals 2

    .prologue
    .line 53
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a:Lcom/google/android/libraries/photoeditor/core/NativeCore;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a(Lhbm;)V

    .line 54
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lchv;->k(Z)V

    .line 56
    invoke-super {p0}, Lcjy;->z()V

    .line 57
    return-void
.end method
