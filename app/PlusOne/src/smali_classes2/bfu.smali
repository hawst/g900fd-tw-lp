.class public final Lbfu;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Lbhd;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 30
    new-instance v0, Lbhg;

    const/4 v1, 0x4

    const/16 v2, 0x10

    new-array v2, v2, [F

    fill-array-data v2, :array_0

    invoke-direct {v0, v1, v2}, Lbhg;-><init>(I[F)V

    .line 36
    new-instance v1, Lbhg;

    const/4 v2, 0x3

    const/16 v3, 0xc

    new-array v3, v3, [F

    fill-array-data v3, :array_1

    invoke-direct {v1, v2, v3}, Lbhg;-><init>(I[F)V

    .line 43
    new-instance v2, Lbhe;

    invoke-direct {v2}, Lbhe;-><init>()V

    const-string v3, "a_position"

    .line 44
    invoke-virtual {v2, v3, v0}, Lbhe;->a(Ljava/lang/String;Lbhg;)Lbhe;

    move-result-object v0

    const-string v2, "a_texcoord"

    .line 45
    invoke-virtual {v0, v2, v1}, Lbhe;->a(Ljava/lang/String;Lbhg;)Lbhe;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Lbhe;->a()Lbhd;

    move-result-object v0

    sput-object v0, Lbfu;->a:Lbhd;

    .line 47
    return-void

    .line 30
    :array_0
    .array-data 4
        -0x40800000    # -1.0f
        -0x40800000    # -1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        0x0
        0x3f800000    # 1.0f
        -0x40800000    # -1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 36
    :array_1
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static a(Lbgn;ILbgs;)V
    .locals 2

    .prologue
    .line 57
    const/4 v0, 0x1

    sget-object v1, Lbfu;->a:Lbhd;

    invoke-static {v0, p0, v1, p1, p2}, Lbfz;->a(ILbgn;Lbhd;ILbgs;)V

    .line 63
    return-void
.end method

.method public static a(Lbgn;Lbhb;Landroid/graphics/Matrix;ILbgs;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 129
    const-string v0, "target"

    invoke-static {p0, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 130
    const-string v0, "source"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 131
    if-eq p0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Attempting to copy source to target"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 133
    const-string v0, "texcoord_transform"

    invoke-virtual {p4, v0, p2}, Lbgs;->b(Ljava/lang/String;Landroid/graphics/Matrix;)Lbgs;

    move-result-object v0

    const-string v1, "sampler_source"

    .line 134
    invoke-virtual {v0, v1, p1}, Lbgs;->b(Ljava/lang/String;Lbhb;)Lbgs;

    .line 136
    invoke-static {p0, p3, p4}, Lbfu;->a(Lbgn;ILbgs;)V

    .line 137
    return-void

    .line 131
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lbgn;Lbhb;Landroid/graphics/Matrix;Lbgx;)V
    .locals 2

    .prologue
    .line 107
    invoke-static {p3}, Lbgs;->a(Lbgx;)Lbgs;

    move-result-object v0

    .line 108
    const/4 v1, 0x0

    invoke-static {p0, p1, p2, v1, v0}, Lbfu;->a(Lbgn;Lbhb;Landroid/graphics/Matrix;ILbgs;)V

    .line 109
    return-void
.end method

.method public static a(Lbgn;Lbhb;Lbgs;)V
    .locals 2

    .prologue
    .line 88
    sget-object v0, Lcfn;->a:Landroid/graphics/Matrix;

    const/4 v1, 0x0

    invoke-static {p0, p1, v0, v1, p2}, Lbfu;->a(Lbgn;Lbhb;Landroid/graphics/Matrix;ILbgs;)V

    .line 89
    return-void
.end method

.method public static a(Lbgn;Lbhb;Lbgx;)V
    .locals 1

    .prologue
    .line 75
    invoke-static {p2}, Lbgs;->a(Lbgx;)Lbgs;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lbfu;->a(Lbgn;Lbhb;Lbgs;)V

    .line 76
    return-void
.end method

.method public static a(Lbgn;Lbhb;Lbhb;FLbgx;)V
    .locals 8

    .prologue
    .line 158
    sget-object v3, Lcfn;->a:Landroid/graphics/Matrix;

    sget-object v4, Lcfn;->a:Landroid/graphics/Matrix;

    const/4 v6, 0x0

    .line 165
    invoke-static {p4}, Lbgs;->a(Lbgx;)Lbgs;

    move-result-object v7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v5, p3

    .line 158
    invoke-static/range {v0 .. v7}, Lbfu;->a(Lbgn;Lbhb;Lbhb;Landroid/graphics/Matrix;Landroid/graphics/Matrix;FILbgs;)V

    .line 166
    return-void
.end method

.method public static a(Lbgn;Lbhb;Lbhb;Landroid/graphics/Matrix;Landroid/graphics/Matrix;FILbgs;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 195
    const-string v0, "target"

    invoke-static {p0, v0, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 196
    const-string v0, "sourceA"

    invoke-static {p1, v0, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 197
    const-string v0, "sourceB"

    invoke-static {p2, v0, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 198
    if-eq p0, p1, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Attempting to copy sourceA to target"

    invoke-static {v0, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 199
    if-eq p0, p2, :cond_1

    :goto_1
    const-string v0, "Attempting to copy sourceB to target"

    invoke-static {v1, v0}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 201
    const-string v0, "sampler_source_a"

    invoke-virtual {p7, v0, p1}, Lbgs;->b(Ljava/lang/String;Lbhb;)Lbgs;

    move-result-object v0

    const-string v1, "sampler_source_b"

    .line 202
    invoke-virtual {v0, v1, p2}, Lbgs;->b(Ljava/lang/String;Lbhb;)Lbgs;

    move-result-object v0

    const-string v1, "texcoord_transform_a"

    .line 203
    invoke-virtual {v0, v1, p3}, Lbgs;->b(Ljava/lang/String;Landroid/graphics/Matrix;)Lbgs;

    move-result-object v0

    const-string v1, "texcoord_transform_b"

    .line 204
    invoke-virtual {v0, v1, p4}, Lbgs;->b(Ljava/lang/String;Landroid/graphics/Matrix;)Lbgs;

    move-result-object v0

    const-string v1, "blend_weight"

    .line 205
    invoke-virtual {v0, v1, p5}, Lbgs;->b(Ljava/lang/String;F)Lbgs;

    .line 207
    invoke-static {p0, p6, p7}, Lbfu;->a(Lbgn;ILbgs;)V

    .line 208
    return-void

    :cond_0
    move v0, v2

    .line 198
    goto :goto_0

    :cond_1
    move v1, v2

    .line 199
    goto :goto_1
.end method
