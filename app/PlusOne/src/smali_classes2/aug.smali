.class public Laug;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lbrc;

.field private final c:J

.field private final d:Lbmk;

.field private final e:Lauh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Laug;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Laug;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbrc;JLbmk;Lauh;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 27
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 28
    const-string v0, "musicLibrary"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrc;

    iput-object v0, p0, Laug;->b:Lbrc;

    .line 29
    const-string v0, "endPointUs"

    invoke-static {p2, p3, v0}, Lcec;->b(JLjava/lang/CharSequence;)J

    move-result-wide v0

    iput-wide v0, p0, Laug;->c:J

    .line 30
    const-string v0, "soundtrack"

    invoke-static {p4, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmk;

    iput-object v0, p0, Laug;->d:Lbmk;

    .line 31
    const-string v0, "listener"

    invoke-static {p5, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lauh;

    iput-object v0, p0, Laug;->e:Lauh;

    .line 32
    return-void
.end method


# virtual methods
.method protected varargs a()Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 36
    iget-object v0, p0, Laug;->d:Lbmk;

    iget-wide v2, p0, Laug;->c:J

    invoke-static {v0, v2, v3}, Lbrc;->a(Lbmk;J)I

    move-result v0

    .line 38
    iget-object v1, p0, Laug;->b:Lbrc;

    iget-object v2, p0, Laug;->d:Lbmk;

    invoke-virtual {v1, v2, v0}, Lbrc;->a(Lbmk;I)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 4

    .prologue
    .line 43
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 44
    sget-object v0, Laug;->a:Ljava/lang/String;

    iget-object v1, p0, Laug;->d:Lbmk;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x28

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Couldn\'t load music data for soundtrack "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    :cond_0
    iget-object v0, p0, Laug;->e:Lauh;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-interface {v0, v1}, Lauh;->a(Z)V

    .line 47
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 13
    invoke-virtual {p0}, Laug;->a()Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 13
    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Laug;->a(Ljava/lang/Boolean;)V

    return-void
.end method
