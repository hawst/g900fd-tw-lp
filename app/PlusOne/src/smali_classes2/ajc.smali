.class public final Lajc;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Landroid/net/Uri;

.field public final b:Lbkr;

.field public final c:Ljava/lang/Exception;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lbkr;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const-string v0, "uri"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lajc;->a:Landroid/net/Uri;

    .line 46
    iput-object p2, p0, Lajc;->b:Lbkr;

    .line 47
    iput-object p3, p0, Lajc;->c:Ljava/lang/Exception;

    .line 48
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 52
    if-ne p0, p1, :cond_1

    .line 60
    :cond_0
    :goto_0
    return v0

    .line 54
    :cond_1
    instance-of v2, p1, Lajc;

    if-nez v2, :cond_2

    move v0, v1

    .line 55
    goto :goto_0

    .line 57
    :cond_2
    check-cast p1, Lajc;

    .line 58
    iget-object v2, p0, Lajc;->a:Landroid/net/Uri;

    iget-object v3, p1, Lajc;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lajc;->b:Lbkr;

    iget-object v3, p1, Lajc;->b:Lbkr;

    .line 59
    invoke-static {v2, v3}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lajc;->c:Ljava/lang/Exception;

    iget-object v3, p1, Lajc;->c:Ljava/lang/Exception;

    .line 60
    invoke-static {v2, v3}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 65
    iget-object v0, p0, Lajc;->a:Landroid/net/Uri;

    iget-object v1, p0, Lajc;->b:Lbkr;

    iget-object v2, p0, Lajc;->c:Ljava/lang/Exception;

    const/16 v3, 0x11

    .line 66
    invoke-static {v2, v3}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v2

    invoke-static {v1, v2}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v1

    .line 65
    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method
