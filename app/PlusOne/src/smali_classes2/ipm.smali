.class public final Lipm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lipl;


# static fields
.field private static final a:Likg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Likg",
            "<",
            "Lijs;",
            "Lggf;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Likg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Likg",
            "<",
            "Lijs;",
            "Lggf;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lipn;

    invoke-direct {v0}, Lipn;-><init>()V

    sput-object v0, Lipm;->a:Likg;

    .line 27
    new-instance v0, Lipo;

    invoke-direct {v0}, Lipo;-><init>()V

    sput-object v0, Lipm;->b:Likg;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lipj;)Lgfz;
    .locals 1

    .prologue
    .line 62
    instance-of v0, p0, Lipk;

    if-eqz v0, :cond_0

    check-cast p0, Lipk;

    .line 63
    invoke-virtual {p0}, Lipk;->e()Lgfz;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lipj;Ljava/lang/String;Ljava/lang/String;)Lijq;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lipj;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lijq",
            "<",
            "Lijs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 38
    sget-object v0, Lgzf;->e:Lgzo;

    .line 39
    invoke-static {p1}, Lipm;->a(Lipj;)Lgfz;

    move-result-object v1

    .line 38
    invoke-interface {v0, v1, p2, p3}, Lgzo;->a(Lgfz;Ljava/lang/String;Ljava/lang/String;)Lggd;

    move-result-object v0

    .line 41
    new-instance v1, Lijr;

    sget-object v2, Lipm;->a:Likg;

    invoke-direct {v1, v0, v2}, Lijr;-><init>(Lggd;Likg;)V

    return-object v1
.end method

.method public a(Lipj;Ljava/lang/String;Ljava/lang/String;JZ)Lijq;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lipj;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "JZ)",
            "Lijq",
            "<",
            "Lijs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 49
    sget-object v0, Lgzf;->e:Lgzo;

    invoke-static {p1}, Lipm;->a(Lipj;)Lgfz;

    move-result-object v1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lgzo;->a(Lgfz;Ljava/lang/String;Ljava/lang/String;JZ)Lggd;

    move-result-object v0

    .line 52
    new-instance v1, Lijr;

    sget-object v2, Lipm;->b:Likg;

    invoke-direct {v1, v0, v2}, Lijr;-><init>(Lggd;Likg;)V

    return-object v1
.end method
