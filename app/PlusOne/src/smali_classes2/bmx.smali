.class public final Lbmx;
.super Lbmu;
.source "PG"


# instance fields
.field private volatile f:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 20
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "creationTimeUs"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "rotation"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "width"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "height"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "type"

    aput-object v2, v0, v1

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lbmv;->b:Lbmv;

    invoke-direct {p0, v0}, Lbmu;-><init>(Lbmv;)V

    .line 37
    return-void
.end method

.method public constructor <init>(JIII)V
    .locals 9

    .prologue
    .line 32
    sget-object v7, Lbmv;->b:Lbmv;

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v1 .. v7}, Lbmu;-><init>(JIIILbmv;)V

    .line 33
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 91
    if-ne p1, p0, :cond_1

    .line 98
    :cond_0
    :goto_0
    return v0

    .line 94
    :cond_1
    instance-of v2, p1, Lbmx;

    if-nez v2, :cond_2

    move v0, v1

    .line 95
    goto :goto_0

    .line 97
    :cond_2
    check-cast p1, Lbmx;

    .line 98
    iget v2, p0, Lbmx;->a:I

    iget v3, p1, Lbmx;->a:I

    if-ne v2, v3, :cond_3

    iget-wide v2, p0, Lbmx;->b:J

    iget-wide v4, p1, Lbmx;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget v2, p0, Lbmx;->c:I

    iget v3, p1, Lbmx;->c:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lbmx;->d:I

    iget v3, p1, Lbmx;->d:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 104
    iget v0, p0, Lbmx;->f:I

    if-nez v0, :cond_0

    .line 105
    iget v0, p0, Lbmx;->a:I

    iget-wide v2, p0, Lbmx;->b:J

    iget v1, p0, Lbmx;->c:I

    iget v4, p0, Lbmx;->d:I

    .line 108
    add-int/lit16 v4, v4, 0x20f

    .line 107
    mul-int/lit8 v4, v4, 0x1f

    add-int/2addr v1, v4

    .line 106
    invoke-static {v2, v3, v1}, Lbqh;->a(JI)I

    move-result v1

    .line 105
    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    iput v0, p0, Lbmx;->f:I

    .line 110
    :cond_0
    iget v0, p0, Lbmx;->f:I

    return v0
.end method
