.class public final Lbgy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Lbgq;

.field private b:Lbgq;


# direct methods
.method public constructor <init>(Lbgq;Lbgq;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const-string v0, "vertexShaderIdentifier"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 43
    const-string v0, "fragmentShaderIdentifier"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 45
    iput-object p1, p0, Lbgy;->a:Lbgq;

    .line 46
    iput-object p2, p0, Lbgy;->b:Lbgq;

    .line 47
    return-void
.end method

.method public constructor <init>(Lbhf;Lbfv;)V
    .locals 2

    .prologue
    .line 33
    iget-object v0, p1, Lbhf;->a:Lbgq;

    iget-object v1, p2, Lbfv;->a:Lbgq;

    invoke-direct {p0, v0, v1}, Lbgy;-><init>(Lbgq;Lbgq;)V

    .line 34
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 56
    instance-of v1, p1, Lbgy;

    if-nez v1, :cond_1

    .line 61
    :cond_0
    :goto_0
    return v0

    .line 59
    :cond_1
    check-cast p1, Lbgy;

    .line 60
    iget-object v1, p0, Lbgy;->a:Lbgq;

    iget-object v2, p1, Lbgy;->a:Lbgq;

    invoke-virtual {v1, v2}, Lbgq;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbgy;->b:Lbgq;

    iget-object v2, p1, Lbgy;->b:Lbgq;

    .line 61
    invoke-virtual {v1, v2}, Lbgq;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lbgy;->a:Lbgq;

    invoke-virtual {v0}, Lbgq;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lbgy;->b:Lbgq;

    invoke-virtual {v1}, Lbgq;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method
