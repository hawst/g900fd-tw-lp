.class final enum Lbta;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbta;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lbta;

.field public static final enum b:Lbta;

.field public static final enum c:Lbta;

.field public static final enum d:Lbta;

.field public static final enum e:Lbta;

.field public static final enum f:Lbta;

.field private static final synthetic g:[Lbta;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 63
    new-instance v0, Lbta;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v3}, Lbta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbta;->a:Lbta;

    new-instance v0, Lbta;

    const-string v1, "PLAYING"

    invoke-direct {v0, v1, v4}, Lbta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbta;->b:Lbta;

    new-instance v0, Lbta;

    const-string v1, "STOPPED"

    invoke-direct {v0, v1, v5}, Lbta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbta;->c:Lbta;

    new-instance v0, Lbta;

    const-string v1, "SEEKING"

    invoke-direct {v0, v1, v6}, Lbta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbta;->d:Lbta;

    new-instance v0, Lbta;

    const-string v1, "RESYNCING"

    invoke-direct {v0, v1, v7}, Lbta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbta;->e:Lbta;

    new-instance v0, Lbta;

    const-string v1, "UPDATING_STORYBOARD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lbta;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbta;->f:Lbta;

    .line 62
    const/4 v0, 0x6

    new-array v0, v0, [Lbta;

    sget-object v1, Lbta;->a:Lbta;

    aput-object v1, v0, v3

    sget-object v1, Lbta;->b:Lbta;

    aput-object v1, v0, v4

    sget-object v1, Lbta;->c:Lbta;

    aput-object v1, v0, v5

    sget-object v1, Lbta;->d:Lbta;

    aput-object v1, v0, v6

    sget-object v1, Lbta;->e:Lbta;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lbta;->f:Lbta;

    aput-object v2, v0, v1

    sput-object v0, Lbta;->g:[Lbta;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbta;
    .locals 1

    .prologue
    .line 62
    const-class v0, Lbta;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbta;

    return-object v0
.end method

.method public static values()[Lbta;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lbta;->g:[Lbta;

    invoke-virtual {v0}, [Lbta;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbta;

    return-object v0
.end method
