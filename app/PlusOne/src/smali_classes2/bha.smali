.class public final Lbha;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:I

.field public final d:I


# direct methods
.method public constructor <init>(II)V
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    new-array v0, v2, [I

    .line 77
    const v1, 0x8b8a

    invoke-static {p1, v1, v0, v4}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    .line 79
    new-array v7, v2, [I

    .line 80
    new-array v5, v2, [I

    .line 81
    aget v1, v0, v4

    new-array v9, v1, [B

    .line 82
    new-array v3, v2, [I

    .line 84
    aget v2, v0, v4

    move v0, p1

    move v1, p2

    move v6, v4

    move v8, v4

    move v10, v4

    invoke-static/range {v0 .. v10}, Landroid/opengl/GLES20;->glGetActiveAttrib(III[II[II[II[BI)V

    .line 85
    const-string v0, "Initializing attribute"

    invoke-static {v0}, Lbgc;->a(Ljava/lang/String;)V

    .line 87
    new-instance v0, Ljava/lang/String;

    invoke-static {v9}, Lbgx;->a([B)I

    move-result v1

    invoke-direct {v0, v9, v4, v1}, Ljava/lang/String;-><init>([BII)V

    iput-object v0, p0, Lbha;->a:Ljava/lang/String;

    .line 88
    iget-object v0, p0, Lbha;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lbha;->b:I

    .line 89
    aget v0, v7, v4

    iput v0, p0, Lbha;->c:I

    .line 90
    aget v0, v5, v4

    iput v0, p0, Lbha;->d:I

    .line 91
    return-void
.end method
