.class public final Lbah;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method private static a(DDDLjava/util/Random;)D
    .locals 6

    .prologue
    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    .line 148
    const-wide/16 v0, 0x0

    invoke-static {v0, v1, p0, p1, p6}, Lbag;->a(DDLjava/util/Random;)D

    move-result-wide v0

    .line 150
    invoke-static {v0, v1, p2, p3}, Ljava/lang/Math;->min(DD)D

    move-result-wide v2

    add-double/2addr v2, v0

    cmpl-double v2, v2, p4

    if-lez v2, :cond_0

    .line 152
    div-double v0, p4, v4

    cmpg-double v0, v0, p2

    if-gtz v0, :cond_2

    .line 154
    div-double v0, p4, v4

    .line 163
    :cond_0
    :goto_0
    invoke-virtual {p6}, Ljava/util/Random;->nextBoolean()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 164
    neg-double v0, v0

    .line 167
    :cond_1
    return-wide v0

    .line 159
    :cond_2
    sub-double v0, p4, p2

    goto :goto_0
.end method

.method public static a(DDDDLbmd;)Lool;
    .locals 22

    .prologue
    .line 124
    .line 125
    invoke-virtual/range {p8 .. p8}, Lbmd;->b()J

    move-result-wide v2

    new-instance v8, Ljava/util/Random;

    invoke-virtual/range {p8 .. p8}, Lbmd;->hashCode()I

    move-result v4

    int-to-long v4, v4

    invoke-direct {v8, v4, v5}, Ljava/util/Random;-><init>(J)V

    .line 124
    long-to-double v2, v2

    const-wide v4, 0x412e848000000000L    # 1000000.0

    div-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    mul-double v6, p2, v2

    add-double v6, v6, p0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(DD)D

    move-result-wide v16

    mul-double v6, p4, v2

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    div-double v4, v4, p0

    sub-double/2addr v2, v4

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    div-double v4, v2, v4

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    div-double v10, v10, v16

    sub-double/2addr v2, v10

    const-wide/high16 v10, 0x4000000000000000L    # 2.0

    div-double/2addr v2, v10

    invoke-static/range {v2 .. v8}, Lbah;->a(DDDLjava/util/Random;)D

    move-result-wide v18

    move-wide/from16 v0, v18

    neg-double v10, v0

    neg-double v12, v4

    move-wide v14, v4

    invoke-static/range {v10 .. v15}, Lcfn;->a(DDD)D

    move-result-wide v20

    invoke-static/range {v2 .. v8}, Lbah;->a(DDDLjava/util/Random;)D

    move-result-wide v2

    neg-double v10, v2

    neg-double v12, v4

    move-wide v14, v4

    invoke-static/range {v10 .. v15}, Lcfn;->a(DDD)D

    move-result-wide v4

    new-instance v6, Lool;

    invoke-direct {v6}, Lool;-><init>()V

    invoke-virtual {v8}, Ljava/util/Random;->nextDouble()D

    move-result-wide v8

    cmpg-double v7, v8, p6

    if-gez v7, :cond_0

    invoke-static/range {v16 .. v17}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    iput-object v7, v6, Lool;->a:Ljava/lang/Double;

    invoke-static/range {p0 .. p1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    iput-object v7, v6, Lool;->b:Ljava/lang/Double;

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    add-double v8, v8, v18

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    iput-object v7, v6, Lool;->c:Ljava/lang/Double;

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    add-double/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v6, Lool;->d:Ljava/lang/Double;

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double v2, v2, v20

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v6, Lool;->e:Ljava/lang/Double;

    const-wide/high16 v2, 0x3fe0000000000000L    # 0.5

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v6, Lool;->f:Ljava/lang/Double;

    :goto_0
    return-object v6

    :cond_0
    invoke-static/range {p0 .. p1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    iput-object v7, v6, Lool;->a:Ljava/lang/Double;

    invoke-static/range {v16 .. v17}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    iput-object v7, v6, Lool;->b:Ljava/lang/Double;

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    add-double v8, v8, v20

    invoke-static {v8, v9}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v7

    iput-object v7, v6, Lool;->c:Ljava/lang/Double;

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    add-double/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    iput-object v4, v6, Lool;->d:Ljava/lang/Double;

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    add-double v4, v4, v18

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    iput-object v4, v6, Lool;->e:Ljava/lang/Double;

    const-wide/high16 v4, 0x3fe0000000000000L    # 0.5

    add-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    iput-object v2, v6, Lool;->f:Ljava/lang/Double;

    goto :goto_0
.end method

.method public static a(Landroid/graphics/Matrix;Layl;Z)V
    .locals 11

    .prologue
    const/high16 v10, 0x3f000000    # 0.5f

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    .line 30
    if-eqz p2, :cond_2

    .line 31
    iget-object v0, p1, Layl;->x:Look;

    if-eqz v0, :cond_0

    iget-object v0, p1, Layl;->x:Look;

    iget-object v0, v0, Look;->e:Lool;

    if-nez v0, :cond_1

    .line 57
    :cond_0
    :goto_0
    return-void

    .line 35
    :cond_1
    iget-object v0, p1, Layl;->x:Look;

    iget-object v1, v0, Look;->e:Lool;

    .line 36
    invoke-static {p1}, Lbag;->b(Layl;)F

    move-result v0

    .line 46
    :goto_1
    iget-object v2, v1, Lool;->a:Ljava/lang/Double;

    .line 47
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    div-double v2, v6, v2

    double-to-float v2, v2

    iget-object v3, v1, Lool;->b:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    div-double v4, v6, v4

    double-to-float v3, v4

    .line 46
    sub-float/2addr v3, v2

    mul-float/2addr v3, v0

    add-float/2addr v2, v3

    .line 48
    iget-object v3, v1, Lool;->c:Ljava/lang/Double;

    .line 49
    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    sub-double/2addr v4, v8

    double-to-float v3, v4

    iget-object v4, v1, Lool;->e:Ljava/lang/Double;

    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    sub-double/2addr v4, v8

    double-to-float v4, v4

    .line 48
    sub-float/2addr v4, v3

    mul-float/2addr v4, v0

    add-float/2addr v3, v4

    .line 51
    iget-object v4, v1, Lool;->d:Ljava/lang/Double;

    .line 52
    invoke-virtual {v4}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    sub-double/2addr v4, v8

    double-to-float v4, v4

    iget-object v1, v1, Lool;->f:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v6

    sub-double/2addr v6, v8

    double-to-float v1, v6

    .line 51
    sub-float/2addr v1, v4

    mul-float/2addr v0, v1

    add-float/2addr v0, v4

    .line 55
    invoke-virtual {p0, v2, v2, v10, v10}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 56
    invoke-virtual {p0, v3, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0

    .line 38
    :cond_2
    iget-object v0, p1, Layl;->y:Look;

    if-eqz v0, :cond_0

    iget-object v0, p1, Layl;->y:Look;

    iget-object v0, v0, Look;->e:Lool;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p1, Layl;->y:Look;

    iget-object v1, v0, Look;->e:Lool;

    .line 43
    invoke-static {p1}, Lbag;->c(Layl;)F

    move-result v0

    goto :goto_1
.end method
