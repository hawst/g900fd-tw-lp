.class public final Lilm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgfg;
.implements Lgfh;
.implements Lilj;


# instance fields
.field private a:Lgri;

.field private b:Liiy;

.field private c:Liiz;


# direct methods
.method public constructor <init>(Landroid/content/Context;Liiy;Liiz;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p2, p0, Lilm;->b:Liiy;

    .line 43
    iput-object p3, p0, Lilm;->c:Liiz;

    .line 44
    new-instance v0, Lgri;

    invoke-direct {v0, p1, p0, p0}, Lgri;-><init>(Landroid/content/Context;Lgfg;Lgfh;)V

    iput-object v0, p0, Lilm;->a:Lgri;

    .line 45
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lilm;->b:Liiy;

    invoke-interface {v0}, Liiy;->a()V

    .line 55
    return-void
.end method

.method public a(Lgfd;)V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lilm;->c:Liiz;

    new-instance v1, Liny;

    invoke-direct {v1, p1}, Liny;-><init>(Lgfd;)V

    invoke-interface {v0, v1}, Liiz;->a(Liiw;)V

    .line 50
    return-void
.end method

.method public a(Lilp;Lilo;)V
    .locals 4

    .prologue
    .line 69
    new-instance v0, Lcom/google/android/gms/location/LocationRequest;

    invoke-direct {v0}, Lcom/google/android/gms/location/LocationRequest;-><init>()V

    .line 70
    invoke-interface {p1}, Lilp;->a()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->a(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    .line 71
    invoke-interface {p1}, Lilp;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->b(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    .line 72
    invoke-interface {p1}, Lilp;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/location/LocationRequest;->c(J)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    .line 73
    invoke-interface {p1}, Lilp;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/location/LocationRequest;->b(I)Lcom/google/android/gms/location/LocationRequest;

    move-result-object v0

    .line 75
    iget-object v1, p0, Lilm;->a:Lgri;

    new-instance v2, Liln;

    invoke-direct {v2, p2}, Liln;-><init>(Lilo;)V

    invoke-virtual {v1, v0, v2}, Lgri;->a(Lcom/google/android/gms/location/LocationRequest;Lgrj;)V

    .line 76
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lilm;->b:Liiy;

    .line 60
    return-void
.end method

.method public c()Landroid/location/Location;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lilm;->a:Lgri;

    invoke-virtual {v0}, Lgri;->a()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lilm;->a:Lgri;

    invoke-virtual {v0}, Lgri;->b()V

    .line 81
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lilm;->a:Lgri;

    invoke-virtual {v0}, Lgri;->c()V

    .line 86
    return-void
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lilm;->a:Lgri;

    invoke-virtual {v0}, Lgri;->d()Z

    move-result v0

    return v0
.end method
