.class public final Lgjn;
.super Lgjm;


# static fields
.field private static synthetic g:Z


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lgjl;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lgjn;->g:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lgjm;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lgjn;->c:I

    return-void
.end method


# virtual methods
.method public a()Lgjl;
    .locals 2

    sget-boolean v0, Lgjn;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lgjn;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sget-boolean v0, Lgjn;->g:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lgjn;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_1
    sget-boolean v0, Lgjn;->g:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lgjn;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_2
    sget-boolean v0, Lgjn;->g:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lgjn;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_3
    sget-boolean v0, Lgjn;->g:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lgjn;->f:Ljava/lang/String;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_4
    new-instance v0, Landroid/app/ApplicationErrorReport$CrashInfo;

    invoke-direct {v0}, Landroid/app/ApplicationErrorReport$CrashInfo;-><init>()V

    iget-object v1, p0, Lgjn;->a:Ljava/lang/String;

    iput-object v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionMessage:Ljava/lang/String;

    iget-object v1, p0, Lgjn;->b:Ljava/lang/String;

    iput-object v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->throwFileName:Ljava/lang/String;

    iget-object v1, p0, Lgjn;->a:Ljava/lang/String;

    iput-object v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionClassName:Ljava/lang/String;

    iget-object v1, p0, Lgjn;->f:Ljava/lang/String;

    iput-object v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    iget-object v1, p0, Lgjn;->d:Ljava/lang/String;

    iput-object v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->throwClassName:Ljava/lang/String;

    iget v1, p0, Lgjn;->c:I

    iput v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->throwLineNumber:I

    iget-object v1, p0, Lgjn;->e:Ljava/lang/String;

    iput-object v1, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->throwMethodName:Ljava/lang/String;

    invoke-super {p0}, Lgjm;->a()Lgjl;

    move-result-object v1

    invoke-static {v1, v0}, Lgjl;->a(Lgjl;Landroid/app/ApplicationErrorReport$CrashInfo;)Lgjl;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Lgjn;
    .locals 0

    iput p1, p0, Lgjn;->c:I

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lgjn;
    .locals 0

    iput-object p1, p0, Lgjn;->a:Ljava/lang/String;

    return-object p0
.end method

.method public c(Ljava/lang/String;)Lgjn;
    .locals 0

    iput-object p1, p0, Lgjn;->b:Ljava/lang/String;

    return-object p0
.end method

.method public d(Ljava/lang/String;)Lgjn;
    .locals 0

    iput-object p1, p0, Lgjn;->d:Ljava/lang/String;

    return-object p0
.end method

.method public e(Ljava/lang/String;)Lgjn;
    .locals 0

    iput-object p1, p0, Lgjn;->e:Ljava/lang/String;

    return-object p0
.end method

.method public f(Ljava/lang/String;)Lgjn;
    .locals 0

    iput-object p1, p0, Lgjn;->f:Ljava/lang/String;

    return-object p0
.end method
