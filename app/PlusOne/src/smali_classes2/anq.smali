.class final Lanq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lalp;


# instance fields
.field private synthetic a:Lanj;


# direct methods
.method constructor <init>(Lanj;)V
    .locals 0

    .prologue
    .line 387
    iput-object p1, p0, Lanq;->a:Lanj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lalm;Lalk;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 391
    iget-object v0, p0, Lanq;->a:Lanj;

    invoke-virtual {v0}, Lanj;->e()V

    .line 396
    const-string v0, "task"

    iget-object v1, p0, Lanq;->a:Lanj;

    invoke-static {v1}, Lanj;->a(Lanj;)Landroid/os/AsyncTask;

    move-result-object v1

    invoke-static {p1, v0, v1, v3}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/lang/CharSequence;)V

    .line 397
    iget-object v0, p0, Lanq;->a:Lanj;

    invoke-static {v0, v3}, Lanj;->a(Lanj;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    .line 399
    iget v0, p2, Lalk;->a:I

    const-string v1, "numInputUris"

    invoke-static {v0, v1, v2, v3}, Lcgp;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 400
    iget-object v0, p2, Lalk;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v2, :cond_0

    iget-object v0, p2, Lalk;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eq v0, v2, :cond_2

    .line 401
    :cond_0
    iget-object v0, p0, Lanq;->a:Lanj;

    invoke-static {v0}, Lanj;->d(Lanj;)Lawb;

    move-result-object v0

    invoke-interface {v0}, Lawb;->aM()V

    .line 402
    iget-object v0, p0, Lanq;->a:Lanj;

    invoke-static {v0, v3}, Lanj;->a(Lanj;Lavz;)Lavz;

    .line 404
    iget-object v0, p0, Lanq;->a:Lanj;

    invoke-static {v0}, Lanj;->c(Lanj;)V

    .line 406
    iget v0, p2, Lalk;->b:I

    if-eq v0, v2, :cond_1

    .line 407
    iget-object v0, p0, Lanq;->a:Lanj;

    invoke-static {v0}, Lanj;->g(Lanj;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lano;

    invoke-interface {v0}, Lano;->a()V

    .line 427
    :cond_1
    :goto_0
    return-void

    .line 412
    :cond_2
    iget-object v0, p0, Lanq;->a:Lanj;

    invoke-static {v0}, Lanj;->h(Lanj;)Lasn;

    move-result-object v0

    invoke-virtual {v0}, Lasn;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 415
    iget-object v0, p0, Lanq;->a:Lanj;

    invoke-static {v0}, Lanj;->h(Lanj;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lanl;

    sget-object v1, Lanm;->a:Lanm;

    invoke-interface {v0, v1}, Lanl;->a(Lanm;)V

    .line 422
    :goto_1
    iget-object v1, p0, Lanq;->a:Lanj;

    iget-object v0, p2, Lalk;->g:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmu;

    invoke-static {v1, v0}, Lanj;->a(Lanj;Lbmu;)Lbmu;

    .line 424
    iget-object v0, p0, Lanq;->a:Lanj;

    invoke-static {v0}, Lanj;->i(Lanj;)Lbmu;

    move-result-object v0

    iget-object v0, v0, Lbmu;->e:Lbmv;

    sget-object v1, Lbmv;->b:Lbmv;

    if-ne v0, v1, :cond_4

    .line 425
    iget-object v0, p0, Lanq;->a:Lanj;

    invoke-static {v0}, Lanj;->k(Lanj;)Lahx;

    move-result-object v0

    iget-object v1, p0, Lanq;->a:Lanj;

    invoke-static {v1}, Lanj;->b(Lanj;)Lavz;

    move-result-object v1

    iget-object v1, v1, Lavz;->a:Landroid/net/Uri;

    sget-object v2, Lanj;->a:Laic;

    iget-object v3, p0, Lanq;->a:Lanj;

    invoke-static {v3}, Lanj;->j(Lanj;)Lahy;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lahx;->b(Landroid/net/Uri;Laic;Lahy;)V

    goto :goto_0

    .line 419
    :cond_3
    iget-object v0, p0, Lanq;->a:Lanj;

    invoke-static {v0}, Lanj;->f(Lanj;)Lann;

    move-result-object v0

    sget-object v1, Lanm;->a:Lanm;

    invoke-interface {v0, v1}, Lann;->a(Lanm;)V

    goto :goto_1

    .line 426
    :cond_4
    iget-object v0, p0, Lanq;->a:Lanj;

    invoke-static {v0}, Lanj;->i(Lanj;)Lbmu;

    move-result-object v0

    iget-object v0, v0, Lbmu;->e:Lbmv;

    sget-object v1, Lbmv;->a:Lbmv;

    if-ne v0, v1, :cond_5

    .line 427
    iget-object v0, p0, Lanq;->a:Lanj;

    invoke-static {v0}, Lanj;->k(Lanj;)Lahx;

    move-result-object v0

    iget-object v1, p0, Lanq;->a:Lanj;

    invoke-static {v1}, Lanj;->b(Lanj;)Lavz;

    move-result-object v1

    iget-object v1, v1, Lavz;->a:Landroid/net/Uri;

    sget-object v2, Lanj;->a:Laic;

    iget-object v3, p0, Lanq;->a:Lanj;

    invoke-static {v3}, Lanj;->j(Lanj;)Lahy;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lahx;->a(Landroid/net/Uri;Laic;Lahy;)V

    goto/16 :goto_0

    .line 429
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    iget-object v1, p0, Lanq;->a:Lanj;

    invoke-static {v1}, Lanj;->i(Lanj;)Lbmu;

    move-result-object v1

    iget-object v1, v1, Lbmu;->e:Lbmv;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0xe

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Invalid type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
