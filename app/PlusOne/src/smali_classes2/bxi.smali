.class public final Lbxi;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:[F

.field private final b:Z


# direct methods
.method private constructor <init>(FFF)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lbxi;->a:[F

    .line 111
    iget-object v0, p0, Lbxi;->a:[F

    const/4 v1, 0x0

    const-string v2, "zoom"

    invoke-static {p3, v2}, Lcec;->a(FLjava/lang/CharSequence;)F

    move-result v2

    aput v2, v0, v1

    .line 112
    iget-object v0, p0, Lbxi;->a:[F

    aput v3, v0, v4

    .line 113
    iget-object v0, p0, Lbxi;->a:[F

    const/4 v1, 0x2

    const/high16 v2, -0x40000000    # -2.0f

    mul-float/2addr v2, p1

    aput v2, v0, v1

    .line 114
    iget-object v0, p0, Lbxi;->a:[F

    const/4 v1, 0x3

    aput v3, v0, v1

    .line 115
    iget-object v0, p0, Lbxi;->a:[F

    const/4 v1, 0x4

    const-string v2, "zoom"

    invoke-static {p3, v2}, Lcec;->a(FLjava/lang/CharSequence;)F

    move-result v2

    aput v2, v0, v1

    .line 116
    iget-object v0, p0, Lbxi;->a:[F

    const/4 v1, 0x5

    const/high16 v2, 0x40000000    # 2.0f

    mul-float/2addr v2, p2

    aput v2, v0, v1

    .line 117
    iget-object v0, p0, Lbxi;->a:[F

    const/4 v1, 0x6

    aput v3, v0, v1

    .line 118
    iget-object v0, p0, Lbxi;->a:[F

    const/4 v1, 0x7

    aput v3, v0, v1

    .line 119
    iget-object v0, p0, Lbxi;->a:[F

    const/16 v1, 0x8

    const/high16 v2, 0x3f800000    # 1.0f

    aput v2, v0, v1

    .line 120
    iput-boolean v4, p0, Lbxi;->b:Z

    .line 121
    return-void
.end method

.method private constructor <init>([F)V
    .locals 4

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    array-length v0, p1

    const-string v1, "transform.length"

    const/16 v2, 0x9

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 94
    iput-object p1, p0, Lbxi;->a:[F

    .line 95
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbxi;->b:Z

    .line 96
    return-void
.end method

.method public static a(FFF)Lbxi;
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lbxi;

    invoke-direct {v0, p0, p1, p2}, Lbxi;-><init>(FFF)V

    return-object v0
.end method

.method public static a([F)Lbxi;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lbxi;

    array-length v1, p0

    invoke-static {p0, v1}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v1

    invoke-direct {v0, v1}, Lbxi;-><init>([F)V

    return-object v0
.end method

.method public static a()[F
    .locals 1

    .prologue
    .line 82
    const/16 v0, 0x9

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    return-object v0

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method


# virtual methods
.method public a(I)F
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lbxi;->a:[F

    aget v0, v0, p1

    return v0
.end method

.method public b([F)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 189
    iget-object v0, p0, Lbxi;->a:[F

    aget v0, v0, v4

    neg-float v0, v0

    iget-object v1, p0, Lbxi;->a:[F

    aget v1, v1, v5

    sub-float/2addr v0, v1

    iget-object v1, p0, Lbxi;->a:[F

    aget v1, v1, v6

    add-float/2addr v0, v1

    aput v0, p1, v4

    .line 190
    iget-object v0, p0, Lbxi;->a:[F

    aget v0, v0, v7

    neg-float v0, v0

    iget-object v1, p0, Lbxi;->a:[F

    aget v1, v1, v8

    sub-float/2addr v0, v1

    iget-object v1, p0, Lbxi;->a:[F

    const/4 v2, 0x5

    aget v1, v1, v2

    add-float/2addr v0, v1

    aput v0, p1, v5

    .line 191
    const/4 v0, 0x0

    aput v0, p1, v6

    .line 192
    iget-object v0, p0, Lbxi;->a:[F

    const/4 v1, 0x6

    aget v0, v0, v1

    neg-float v0, v0

    iget-object v1, p0, Lbxi;->a:[F

    const/4 v2, 0x7

    aget v1, v1, v2

    sub-float/2addr v0, v1

    iget-object v1, p0, Lbxi;->a:[F

    const/16 v2, 0x8

    aget v1, v1, v2

    add-float/2addr v0, v1

    aput v0, p1, v7

    .line 193
    iget-object v0, p0, Lbxi;->a:[F

    aget v0, v0, v4

    iget-object v1, p0, Lbxi;->a:[F

    aget v1, v1, v5

    sub-float/2addr v0, v1

    iget-object v1, p0, Lbxi;->a:[F

    aget v1, v1, v6

    add-float/2addr v0, v1

    aput v0, p1, v8

    .line 194
    const/4 v0, 0x5

    iget-object v1, p0, Lbxi;->a:[F

    aget v1, v1, v7

    iget-object v2, p0, Lbxi;->a:[F

    aget v2, v2, v8

    sub-float/2addr v1, v2

    iget-object v2, p0, Lbxi;->a:[F

    const/4 v3, 0x5

    aget v2, v2, v3

    add-float/2addr v1, v2

    aput v1, p1, v0

    .line 195
    const/4 v0, 0x6

    const/4 v1, 0x0

    aput v1, p1, v0

    .line 196
    const/4 v0, 0x7

    iget-object v1, p0, Lbxi;->a:[F

    const/4 v2, 0x6

    aget v1, v1, v2

    iget-object v2, p0, Lbxi;->a:[F

    const/4 v3, 0x7

    aget v2, v2, v3

    sub-float/2addr v1, v2

    iget-object v2, p0, Lbxi;->a:[F

    const/16 v3, 0x8

    aget v2, v2, v3

    add-float/2addr v1, v2

    aput v1, p1, v0

    .line 197
    const/16 v0, 0x8

    iget-object v1, p0, Lbxi;->a:[F

    aget v1, v1, v4

    neg-float v1, v1

    iget-object v2, p0, Lbxi;->a:[F

    aget v2, v2, v5

    add-float/2addr v1, v2

    iget-object v2, p0, Lbxi;->a:[F

    aget v2, v2, v6

    add-float/2addr v1, v2

    aput v1, p1, v0

    .line 198
    const/16 v0, 0x9

    iget-object v1, p0, Lbxi;->a:[F

    aget v1, v1, v7

    neg-float v1, v1

    iget-object v2, p0, Lbxi;->a:[F

    aget v2, v2, v8

    add-float/2addr v1, v2

    iget-object v2, p0, Lbxi;->a:[F

    const/4 v3, 0x5

    aget v2, v2, v3

    add-float/2addr v1, v2

    aput v1, p1, v0

    .line 199
    const/16 v0, 0xa

    const/4 v1, 0x0

    aput v1, p1, v0

    .line 200
    const/16 v0, 0xb

    iget-object v1, p0, Lbxi;->a:[F

    const/4 v2, 0x6

    aget v1, v1, v2

    neg-float v1, v1

    iget-object v2, p0, Lbxi;->a:[F

    const/4 v3, 0x7

    aget v2, v2, v3

    add-float/2addr v1, v2

    iget-object v2, p0, Lbxi;->a:[F

    const/16 v3, 0x8

    aget v2, v2, v3

    add-float/2addr v1, v2

    aput v1, p1, v0

    .line 201
    const/16 v0, 0xc

    iget-object v1, p0, Lbxi;->a:[F

    aget v1, v1, v4

    iget-object v2, p0, Lbxi;->a:[F

    aget v2, v2, v5

    add-float/2addr v1, v2

    iget-object v2, p0, Lbxi;->a:[F

    aget v2, v2, v6

    add-float/2addr v1, v2

    aput v1, p1, v0

    .line 202
    const/16 v0, 0xd

    iget-object v1, p0, Lbxi;->a:[F

    aget v1, v1, v7

    iget-object v2, p0, Lbxi;->a:[F

    aget v2, v2, v8

    add-float/2addr v1, v2

    iget-object v2, p0, Lbxi;->a:[F

    const/4 v3, 0x5

    aget v2, v2, v3

    add-float/2addr v1, v2

    aput v1, p1, v0

    .line 203
    const/16 v0, 0xe

    const/4 v1, 0x0

    aput v1, p1, v0

    .line 204
    const/16 v0, 0xf

    iget-object v1, p0, Lbxi;->a:[F

    const/4 v2, 0x6

    aget v1, v1, v2

    iget-object v2, p0, Lbxi;->a:[F

    const/4 v3, 0x7

    aget v2, v2, v3

    add-float/2addr v1, v2

    iget-object v2, p0, Lbxi;->a:[F

    const/16 v3, 0x8

    aget v2, v2, v3

    add-float/2addr v1, v2

    aput v1, p1, v0

    .line 205
    return-void
.end method

.method public b()[F
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lbxi;->a:[F

    iget-object v1, p0, Lbxi;->a:[F

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([FI)[F

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 149
    iget-boolean v0, p0, Lbxi;->b:Z

    return v0
.end method

.method public d()F
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lbxi;->a:[F

    const/4 v1, 0x2

    aget v0, v0, v1

    neg-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public e()F
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lbxi;->a:[F

    const/4 v1, 0x5

    aget v0, v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 209
    if-ne p1, p0, :cond_0

    .line 210
    const/4 v0, 0x1

    .line 216
    :goto_0
    return v0

    .line 212
    :cond_0
    instance-of v0, p1, Lbxi;

    if-nez v0, :cond_1

    .line 213
    const/4 v0, 0x0

    goto :goto_0

    .line 215
    :cond_1
    check-cast p1, Lbxi;

    .line 216
    iget-object v0, p0, Lbxi;->a:[F

    iget-object v1, p1, Lbxi;->a:[F

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([F[F)Z

    move-result v0

    goto :goto_0
.end method

.method public f()F
    .locals 3

    .prologue
    .line 179
    iget-object v0, p0, Lbxi;->a:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Lbxi;->a:[F

    const/4 v2, 0x4

    aget v1, v1, v2

    add-float/2addr v0, v1

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lbxi;->a:[F

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([F)I

    move-result v0

    return v0
.end method
