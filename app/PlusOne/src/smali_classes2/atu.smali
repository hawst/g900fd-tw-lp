.class public final enum Latu;
.super Ljava/lang/Enum;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Latu;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Latu;",
            ">;"
        }
    .end annotation
.end field

.field public static final enum a:Latu;

.field public static final enum b:Latu;

.field public static final enum c:Latu;

.field private static final synthetic f:[Latu;


# instance fields
.field public final d:Ljava/lang/String;

.field public final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 18
    new-instance v0, Latu;

    const-string v1, "SIZE_480P"

    const-string v2, "480P"

    const/4 v3, 0x4

    invoke-direct {v0, v1, v4, v2, v3}, Latu;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Latu;->a:Latu;

    .line 19
    new-instance v0, Latu;

    const-string v1, "SIZE_720P"

    const-string v2, "720P"

    const/4 v3, 0x5

    invoke-direct {v0, v1, v5, v2, v3}, Latu;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Latu;->b:Latu;

    .line 20
    new-instance v0, Latu;

    const-string v1, "SIZE_1080P"

    const-string v2, "1080P"

    const/4 v3, 0x6

    invoke-direct {v0, v1, v6, v2, v3}, Latu;-><init>(Ljava/lang/String;ILjava/lang/String;I)V

    sput-object v0, Latu;->c:Latu;

    .line 16
    const/4 v0, 0x3

    new-array v0, v0, [Latu;

    sget-object v1, Latu;->a:Latu;

    aput-object v1, v0, v4

    sget-object v1, Latu;->b:Latu;

    aput-object v1, v0, v5

    sget-object v1, Latu;->c:Latu;

    aput-object v1, v0, v6

    sput-object v0, Latu;->f:[Latu;

    .line 45
    new-instance v0, Latv;

    invoke-direct {v0}, Latv;-><init>()V

    sput-object v0, Latu;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 26
    iput-object p3, p0, Latu;->d:Ljava/lang/String;

    .line 27
    iput p4, p0, Latu;->e:I

    .line 28
    return-void
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;)[Latu;
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, [Latu;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Latu;
    .locals 1

    .prologue
    .line 16
    const-class v0, Latu;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Latu;

    return-object v0
.end method

.method public static values()[Latu;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Latu;->f:[Latu;

    invoke-virtual {v0}, [Latu;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Latu;

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Latu;->d:Ljava/lang/String;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0}, Latu;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 38
    return-void
.end method
