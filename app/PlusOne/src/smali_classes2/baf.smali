.class public Lbaf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:J

.field private final b:I

.field private c:J

.field private d:F


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 29
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lbaf;-><init>(JI)V

    .line 30
    return-void
.end method

.method public constructor <init>(JI)V
    .locals 3

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbaf;->c:J

    .line 39
    iput-wide p1, p0, Lbaf;->a:J

    .line 40
    iput p3, p0, Lbaf;->b:I

    .line 41
    return-void
.end method

.method private a(Look;)Z
    .locals 2

    .prologue
    .line 95
    iget v0, p0, Lbaf;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lbaf;->b:I

    .line 96
    invoke-static {v0, p1}, Lbag;->a(ILook;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(IILbmg;Lbmg;)Z
    .locals 1

    .prologue
    .line 60
    if-lez p1, :cond_0

    if-ge p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Layl;)Z
    .locals 2

    .prologue
    .line 47
    invoke-virtual {p0, p1}, Lbaf;->b(Layl;)F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Layl;)F
    .locals 9

    .prologue
    .line 68
    iget-wide v0, p1, Layl;->a:J

    iget-wide v2, p0, Lbaf;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 69
    iget v0, p0, Lbaf;->d:F

    .line 91
    :goto_0
    return v0

    .line 71
    :cond_0
    const/4 v0, 0x0

    .line 72
    iget-wide v2, p0, Lbaf;->a:J

    const-wide/16 v4, 0x2

    div-long/2addr v2, v4

    .line 73
    iget-wide v4, p1, Layl;->d:J

    cmp-long v1, v4, v2

    if-gtz v1, :cond_2

    .line 74
    iget-object v1, p1, Layl;->z:Look;

    invoke-direct {p0, v1}, Lbaf;->a(Look;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p1, Layl;->f:I

    iget v2, p1, Layl;->h:I

    iget-object v3, p1, Layl;->j:Lbmg;

    iget-object v4, p1, Layl;->i:Lbmg;

    .line 75
    invoke-virtual {p0, v1, v2, v3, v4}, Lbaf;->a(IILbmg;Lbmg;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 77
    const/high16 v0, 0x3f000000    # 0.5f

    iget-wide v2, p1, Layl;->d:J

    long-to-float v1, v2

    iget-wide v2, p0, Lbaf;->a:J

    long-to-float v2, v2

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 89
    :cond_1
    :goto_1
    iput v0, p0, Lbaf;->d:F

    .line 90
    iget-wide v2, p1, Layl;->a:J

    iput-wide v2, p0, Lbaf;->c:J

    goto :goto_0

    .line 80
    :cond_2
    iget-wide v4, p1, Layl;->e:J

    iget-wide v6, p1, Layl;->d:J

    sub-long/2addr v4, v6

    .line 81
    cmp-long v1, v4, v2

    if-gez v1, :cond_1

    .line 82
    iget-object v1, p1, Layl;->x:Look;

    invoke-direct {p0, v1}, Lbaf;->a(Look;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p1, Layl;->f:I

    add-int/lit8 v1, v1, 0x1

    iget v6, p1, Layl;->h:I

    iget-object v7, p1, Layl;->i:Lbmg;

    iget-object v8, p1, Layl;->k:Lbmg;

    .line 83
    invoke-virtual {p0, v1, v6, v7, v8}, Lbaf;->a(IILbmg;Lbmg;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 85
    sub-long v0, v2, v4

    long-to-float v0, v0

    iget-wide v2, p0, Lbaf;->a:J

    long-to-float v1, v2

    div-float/2addr v0, v1

    goto :goto_1
.end method
