.class public final Lcfl;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:[J

.field private b:[I

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcfl;-><init>(I)V

    .line 29
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-string v0, "capacity"

    invoke-static {p1, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    .line 39
    new-array v0, p1, [J

    iput-object v0, p0, Lcfl;->a:[J

    .line 40
    new-array v0, p1, [I

    iput-object v0, p0, Lcfl;->b:[I

    .line 41
    const/4 v0, 0x0

    iput v0, p0, Lcfl;->c:I

    .line 42
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcfl;->a:[J

    array-length v0, v0

    return v0
.end method

.method public a(JI)V
    .locals 5

    .prologue
    .line 76
    iget-object v0, p0, Lcfl;->a:[J

    const/4 v1, 0x0

    iget v2, p0, Lcfl;->c:I

    invoke-static {v0, v1, v2, p1, p2}, Ljava/util/Arrays;->binarySearch([JIIJ)I

    move-result v0

    .line 77
    if-ltz v0, :cond_0

    .line 79
    iget-object v1, p0, Lcfl;->b:[I

    aput p3, v1, v0

    .line 100
    :goto_0
    return-void

    .line 82
    :cond_0
    xor-int/lit8 v0, v0, -0x1

    .line 84
    iget v1, p0, Lcfl;->c:I

    invoke-virtual {p0}, Lcfl;->a()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 85
    invoke-virtual {p0}, Lcfl;->a()I

    move-result v1

    shl-int/lit8 v1, v1, 0x1

    iget-object v2, p0, Lcfl;->a:[J

    invoke-static {v2, v1}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v2

    iput-object v2, p0, Lcfl;->a:[J

    iget-object v2, p0, Lcfl;->b:[I

    invoke-static {v2, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v1

    iput-object v1, p0, Lcfl;->b:[I

    .line 88
    :cond_1
    iget v1, p0, Lcfl;->c:I

    if-eq v1, v0, :cond_2

    .line 90
    iget-object v1, p0, Lcfl;->a:[J

    iget-object v2, p0, Lcfl;->a:[J

    add-int/lit8 v3, v0, 0x1

    iget v4, p0, Lcfl;->c:I

    sub-int/2addr v4, v0

    invoke-static {v1, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 91
    iget-object v1, p0, Lcfl;->b:[I

    iget-object v2, p0, Lcfl;->b:[I

    add-int/lit8 v3, v0, 0x1

    iget v4, p0, Lcfl;->c:I

    sub-int/2addr v4, v0

    invoke-static {v1, v0, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 95
    :cond_2
    iget-object v1, p0, Lcfl;->a:[J

    aput-wide p1, v1, v0

    .line 96
    iget-object v1, p0, Lcfl;->b:[I

    aput p3, v1, v0

    .line 98
    iget v0, p0, Lcfl;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcfl;->c:I

    goto :goto_0
.end method

.method public b(JI)I
    .locals 3

    .prologue
    .line 107
    iget-object v0, p0, Lcfl;->a:[J

    const/4 v1, 0x0

    iget v2, p0, Lcfl;->c:I

    invoke-static {v0, v1, v2, p1, p2}, Ljava/util/Arrays;->binarySearch([JIIJ)I

    move-result v0

    .line 108
    if-ltz v0, :cond_0

    iget-object v1, p0, Lcfl;->b:[I

    aget p3, v1, v0

    :cond_0
    return p3
.end method

.method public b()V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    iput v0, p0, Lcfl;->c:I

    .line 69
    return-void
.end method
