.class final Lbzz;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lbza;Lama;)Lbmd;
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    .line 119
    iget-boolean v0, p0, Lbza;->q:Z

    if-eqz v0, :cond_0

    .line 120
    new-instance v0, Look;

    invoke-direct {v0}, Look;-><init>()V

    .line 121
    new-instance v1, Loom;

    invoke-direct {v1}, Loom;-><init>()V

    iput-object v1, v0, Look;->c:Loom;

    .line 122
    iget-object v1, v0, Look;->c:Loom;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Loom;->a:Ljava/lang/Long;

    .line 123
    iget-object v1, v0, Look;->c:Loom;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Loom;->b:Ljava/lang/Long;

    .line 124
    new-instance v1, Loom;

    invoke-direct {v1}, Loom;-><init>()V

    iput-object v1, v0, Look;->d:Loom;

    .line 125
    iget-object v1, v0, Look;->d:Loom;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Loom;->a:Ljava/lang/Long;

    .line 126
    iget-object v1, v0, Look;->d:Loom;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iput-object v2, v1, Loom;->b:Ljava/lang/Long;

    .line 128
    new-instance v1, Lbmf;

    invoke-direct {v1}, Lbmf;-><init>()V

    .line 129
    invoke-interface {p1}, Lama;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Lbmf;->a(I)Lbmf;

    move-result-object v1

    sget-object v2, Lbmg;->d:Lbmg;

    .line 130
    invoke-virtual {v1, v2}, Lbmf;->a(Lbmg;)Lbmf;

    move-result-object v1

    sget-object v2, Lbmd;->a:Ljeg;

    .line 131
    invoke-virtual {v1, v2}, Lbmf;->a(Ljeg;)Lbmf;

    move-result-object v1

    new-instance v2, Lbmp;

    const-wide/32 v4, 0x2dc6c0

    invoke-direct {v2, v6, v7, v4, v5}, Lbmp;-><init>(JJ)V

    .line 132
    invoke-virtual {v1, v2}, Lbmf;->a(Lbmp;)Lbmf;

    move-result-object v1

    const/4 v2, 0x1

    .line 133
    invoke-virtual {v1, v2}, Lbmf;->a(Z)Lbmf;

    move-result-object v1

    .line 134
    invoke-virtual {v1, v0}, Lbmf;->a(Look;)Lbmf;

    move-result-object v0

    .line 135
    invoke-virtual {v0}, Lbmf;->a()Lbmd;

    move-result-object v0

    .line 137
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lonr;Look;Lama;)Lbmd;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 100
    iget v0, p0, Lonr;->b:I

    const-string v1, "cloud clip type"

    const/4 v2, 0x4

    invoke-static {v0, v1, v2, v3}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 102
    new-instance v0, Lbmf;

    invoke-direct {v0}, Lbmf;-><init>()V

    .line 103
    invoke-interface {p2}, Lama;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lbmf;->a(I)Lbmf;

    move-result-object v0

    iget-object v1, p0, Lonr;->d:Lons;

    .line 104
    invoke-static {v1}, Lbpx;->a(Lons;)Lbmp;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbmf;->a(Lbmp;)Lbmf;

    move-result-object v0

    sget-object v1, Lbmg;->d:Lbmg;

    .line 105
    invoke-virtual {v0, v1}, Lbmf;->a(Lbmg;)Lbmf;

    move-result-object v0

    sget-object v1, Lbmd;->a:Ljeg;

    .line 106
    invoke-virtual {v0, v1}, Lbmf;->a(Ljeg;)Lbmf;

    move-result-object v0

    const/4 v1, 0x1

    .line 107
    invoke-virtual {v0, v1}, Lbmf;->a(Z)Lbmf;

    move-result-object v1

    const-string v0, "themeParams"

    .line 108
    invoke-static {p1, v0, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Look;

    invoke-virtual {v1, v0}, Lbmf;->a(Look;)Lbmf;

    move-result-object v0

    .line 109
    invoke-virtual {v0}, Lbmf;->a()Lbmd;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lood;Ljava/util/List;Lbza;Lama;)Lbzy;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lood;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Lbza;",
            "Lama;",
            ")",
            "Lbzy;"
        }
    .end annotation

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 36
    iget-object v0, p0, Lood;->h:Looe;

    iget-object v0, v0, Looe;->b:Looo;

    iget-object v0, v0, Looo;->a:Loon;

    .line 38
    if-nez v0, :cond_7

    .line 39
    new-instance v0, Loon;

    invoke-direct {v0}, Loon;-><init>()V

    move-object v1, v0

    .line 43
    :goto_0
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    move v4, v5

    move-object v2, v6

    move-object v3, v6

    .line 46
    :goto_1
    iget-object v0, p0, Lood;->b:[Lonr;

    array-length v0, v0

    if-ge v4, v0, :cond_1

    .line 47
    iget-object v0, p0, Lood;->b:[Lonr;

    aget-object v0, v0, v4

    .line 48
    iget-object v8, p0, Lood;->d:[Ljava/lang/Boolean;

    aget-object v8, v8, v4

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    .line 49
    iget-object v9, p0, Lood;->h:Looe;

    iget-object v9, v9, Looe;->b:Looo;

    iget-object v9, v9, Looo;->b:[Look;

    aget-object v9, v9, v4

    .line 51
    if-eqz v8, :cond_6

    .line 52
    if-nez v4, :cond_0

    .line 55
    invoke-static {v0, v9, p3}, Lbzz;->a(Lonr;Look;Lama;)Lbmd;

    move-result-object v0

    move-object v10, v2

    move-object v2, v0

    move-object v0, v10

    .line 46
    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-object v3, v2

    move-object v2, v0

    goto :goto_1

    .line 58
    :cond_0
    const-string v8, "afterFadeOutClip"

    invoke-static {v2, v8, v6}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 59
    invoke-static {v0, v9, p3}, Lbzz;->a(Lonr;Look;Lama;)Lbmd;

    move-result-object v0

    move-object v2, v3

    goto :goto_2

    :cond_1
    move v4, v5

    .line 64
    :goto_3
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v4, v0, :cond_4

    .line 65
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 66
    iget-object v0, v0, Lbmd;->e:Ljeg;

    iget-object v6, v0, Ljeg;->a:Ljdx;

    move v0, v5

    .line 69
    :goto_4
    iget-object v8, p0, Lood;->b:[Lonr;

    array-length v8, v8

    if-ge v0, v8, :cond_2

    .line 70
    iget-object v8, p0, Lood;->d:[Ljava/lang/Boolean;

    aget-object v8, v8, v0

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v8

    if-nez v8, :cond_3

    .line 71
    iget-object v8, p0, Lood;->b:[Lonr;

    aget-object v8, v8, v0

    .line 72
    iget-object v8, v8, Lonr;->c:Lonw;

    invoke-static {v8}, Ljdx;->a(Lonw;)Ljdx;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljdx;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 73
    iget-object v6, p0, Lood;->h:Looe;

    iget-object v6, v6, Looe;->b:Looo;

    iget-object v6, v6, Looo;->b:[Look;

    aget-object v6, v6, v0

    .line 76
    invoke-interface {p1, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    invoke-virtual {v0}, Lbmd;->a()Lbmf;

    move-result-object v0

    .line 77
    invoke-virtual {v0, v6}, Lbmf;->a(Look;)Lbmf;

    move-result-object v0

    .line 78
    invoke-virtual {v0}, Lbmf;->a()Lbmd;

    move-result-object v0

    .line 75
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 64
    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_3

    .line 69
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 88
    :cond_4
    iget-object v0, p0, Lood;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 89
    invoke-static {p2, p3}, Lbzz;->a(Lbza;Lama;)Lbmd;

    move-result-object v3

    .line 92
    :cond_5
    new-instance v0, Lbzy;

    invoke-direct {v0, v1, v7, v3, v2}, Lbzy;-><init>(Loon;Ljava/util/List;Lbmd;Lbmd;)V

    return-object v0

    :cond_6
    move-object v0, v2

    move-object v2, v3

    goto :goto_2

    :cond_7
    move-object v1, v0

    goto/16 :goto_0
.end method
