.class public final Lbku;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lbmz;",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;>;"
        }
    .end annotation
.end field

.field private final b:[J

.field private final c:Lbms;

.field private final d:[J


# direct methods
.method public constructor <init>(Lbvf;)V
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbku;->a:Ljava/util/Map;

    .line 48
    iget-object v3, p1, Lbvf;->c:[Lbvg;

    .line 49
    invoke-static {v3}, Lbku;->a([Lbvg;)[J

    move-result-object v0

    iput-object v0, p0, Lbku;->b:[J

    .line 50
    invoke-static {}, Lbmz;->values()[Lbmz;

    move-result-object v4

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_2

    aget-object v6, v4, v2

    .line 51
    new-instance v7, Landroid/util/LongSparseArray;

    invoke-direct {v7}, Landroid/util/LongSparseArray;-><init>()V

    .line 52
    array-length v8, v3

    move v0, v1

    :goto_1
    if-ge v0, v8, :cond_1

    aget-object v9, v3, v0

    .line 53
    invoke-virtual {v6, v9}, Lbmz;->a(Lbvg;)Ljava/lang/Object;

    move-result-object v10

    .line 54
    if-eqz v10, :cond_0

    .line 55
    iget-wide v12, v9, Lbvg;->b:J

    invoke-virtual {v7, v12, v13, v10}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 52
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 58
    :cond_1
    iget-object v0, p0, Lbku;->a:Ljava/util/Map;

    invoke-interface {v0, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 60
    :cond_2
    iget-object v0, p0, Lbku;->a:Ljava/util/Map;

    invoke-static {v0}, Lbku;->a(Ljava/util/Map;)Lbms;

    move-result-object v0

    iput-object v0, p0, Lbku;->c:Lbms;

    .line 61
    iget-object v0, p0, Lbku;->a:Ljava/util/Map;

    invoke-static {v0}, Lbku;->b(Ljava/util/Map;)[J

    move-result-object v0

    iput-object v0, p0, Lbku;->d:[J

    .line 62
    return-void
.end method

.method constructor <init>(Ljava/util/Map;Ljava/util/TreeSet;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lbmz;",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;>;",
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Long;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    iput-object p1, p0, Lbku;->a:Ljava/util/Map;

    .line 41
    invoke-static {p2}, Lbku;->a(Ljava/util/TreeSet;)[J

    move-result-object v0

    iput-object v0, p0, Lbku;->b:[J

    .line 42
    iget-object v0, p0, Lbku;->a:Ljava/util/Map;

    invoke-static {v0}, Lbku;->b(Ljava/util/Map;)[J

    move-result-object v0

    iput-object v0, p0, Lbku;->d:[J

    .line 43
    iget-object v0, p0, Lbku;->a:Ljava/util/Map;

    invoke-static {v0}, Lbku;->a(Ljava/util/Map;)Lbms;

    move-result-object v0

    iput-object v0, p0, Lbku;->c:Lbms;

    .line 44
    return-void
.end method

.method private static a(Ljava/util/Map;)Lbms;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lbmz;",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;>;)",
            "Lbms;"
        }
    .end annotation

    .prologue
    .line 326
    sget-object v0, Lbmz;->o:Lbmz;

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/LongSparseArray;

    .line 327
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    if-eqz v1, :cond_1

    .line 328
    new-instance v2, Lbmt;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    invoke-direct {v2, v1}, Lbmt;-><init>(I)V

    .line 329
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 330
    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lbmt;->a(J)Lbmt;

    .line 329
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 332
    :cond_0
    invoke-virtual {v2}, Lbmt;->a()Lbms;

    move-result-object v0

    .line 334
    :goto_1
    return-object v0

    :cond_1
    sget-object v0, Lbms;->a:Lbms;

    goto :goto_1
.end method

.method private static a(Ljava/util/TreeSet;)[J
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/TreeSet",
            "<",
            "Ljava/lang/Long;",
            ">;)[J"
        }
    .end annotation

    .prologue
    .line 316
    const/4 v0, 0x0

    .line 317
    invoke-virtual {p0}, Ljava/util/TreeSet;->size()I

    move-result v1

    new-array v3, v1, [J

    .line 318
    invoke-virtual {p0}, Ljava/util/TreeSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 319
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v3, v1

    move v1, v2

    .line 320
    goto :goto_0

    .line 321
    :cond_0
    return-object v3
.end method

.method private static a([Lbvg;)[J
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 306
    array-length v1, p0

    new-array v3, v1, [J

    .line 308
    array-length v4, p0

    move v1, v0

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, p0, v0

    .line 309
    add-int/lit8 v2, v1, 0x1

    iget-wide v6, v5, Lbvg;->b:J

    aput-wide v6, v3, v1

    .line 308
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0

    .line 311
    :cond_0
    invoke-static {v3}, Ljava/util/Arrays;->sort([J)V

    .line 312
    return-object v3
.end method

.method private static b(Ljava/util/Map;)[J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lbmz;",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;>;)[J"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 341
    sget-object v0, Lbmz;->b:Lbmz;

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/LongSparseArray;

    .line 342
    if-nez v0, :cond_2

    .line 343
    sget-object v0, Lbmz;->a:Lbmz;

    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/LongSparseArray;

    move-object v3, v0

    .line 345
    :goto_0
    if-eqz v3, :cond_1

    .line 346
    invoke-virtual {v3}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    new-array v2, v0, [J

    move v0, v1

    .line 347
    :goto_1
    invoke-virtual {v3}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 348
    invoke-virtual {v3, v0}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    aput-wide v4, v2, v0

    .line 347
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    move-object v0, v2

    .line 352
    :goto_2
    return-object v0

    :cond_1
    new-array v0, v1, [J

    goto :goto_2

    :cond_2
    move-object v3, v0

    goto :goto_0
.end method


# virtual methods
.method public a(Lbmz;J)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 72
    invoke-virtual {p0, p1}, Lbku;->b(Lbmz;)Landroid/util/LongSparseArray;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbvf;)V
    .locals 10

    .prologue
    .line 281
    new-instance v5, Ljava/util/TreeMap;

    invoke-direct {v5}, Ljava/util/TreeMap;-><init>()V

    .line 284
    iget-object v0, p0, Lbku;->a:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 285
    const/4 v1, 0x0

    move v3, v1

    :goto_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/LongSparseArray;

    invoke-virtual {v1}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    if-ge v3, v1, :cond_0

    .line 286
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/util/LongSparseArray;

    invoke-virtual {v1, v3}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v8

    .line 287
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbvg;

    .line 288
    if-nez v1, :cond_2

    .line 289
    new-instance v1, Lbvg;

    invoke-direct {v1}, Lbvg;-><init>()V

    .line 290
    iput-wide v8, v1, Lbvg;->b:J

    .line 291
    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v2, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v4, v1

    .line 293
    :goto_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmz;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/LongSparseArray;

    invoke-virtual {v2, v3}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2, v4}, Lbmz;->a(Ljava/lang/Object;Lbvg;)V

    .line 285
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 297
    :cond_1
    invoke-virtual {v5}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v0

    .line 298
    invoke-virtual {v5}, Ljava/util/TreeMap;->size()I

    move-result v1

    new-array v1, v1, [Lbvg;

    .line 297
    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbvg;

    iput-object v0, p1, Lbvf;->c:[Lbvg;

    .line 299
    return-void

    :cond_2
    move-object v4, v1

    goto :goto_1
.end method

.method public a(Lbmz;)Z
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lbku;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lbku;->b(Lbmz;)Landroid/util/LongSparseArray;

    move-result-object v0

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()[J
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lbku;->b:[J

    iget-object v1, p0, Lbku;->b:[J

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v0

    return-object v0
.end method

.method public a(JJ)[J
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 135
    iget-object v0, p0, Lbku;->b:[J

    array-length v0, v0

    if-nez v0, :cond_0

    .line 136
    new-array v0, v2, [J

    .line 142
    :goto_0
    return-object v0

    .line 138
    :cond_0
    iget-object v0, p0, Lbku;->b:[J

    .line 139
    invoke-static {p1, p2, v0}, Lcgv;->a(J[J)I

    move-result v0

    iget-object v1, p0, Lbku;->b:[J

    array-length v1, v1

    .line 138
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 141
    iget-object v1, p0, Lbku;->b:[J

    invoke-static {p3, p4, v1}, Lcgv;->b(J[J)I

    move-result v1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 142
    iget-object v2, p0, Lbku;->b:[J

    add-int/lit8 v1, v1, 0x1

    invoke-static {v2, v0, v1}, Ljava/util/Arrays;->copyOfRange([JII)[J

    move-result-object v0

    goto :goto_0
.end method

.method public b(Lbmz;)Landroid/util/LongSparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbmz;",
            ")",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 95
    iget-object v0, p0, Lbku;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/LongSparseArray;

    .line 96
    if-nez v0, :cond_0

    invoke-static {}, Lcfh;->a()Lcfh;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public b()[J
    .locals 2

    .prologue
    .line 123
    iget-object v0, p0, Lbku;->d:[J

    iget-object v1, p0, Lbku;->d:[J

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([JI)[J

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lbku;->b:[J

    array-length v0, v0

    return v0
.end method

.method public c(Lbmz;)V
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lbku;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 106
    return-void
.end method

.method public d()Lbms;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lbku;->c:Lbms;

    return-object v0
.end method
