.class final Ljhg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lqbk;",
            ">;"
        }
    .end annotation
.end field

.field private b:[Ljava/lang/Long;

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->synchronizedList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Ljhg;->a:Ljava/util/List;

    .line 167
    return-void
.end method


# virtual methods
.method a(Lqbk;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 224
    iput-object p2, p0, Ljhg;->c:Ljava/lang/String;

    .line 225
    iget-object v0, p0, Ljhg;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    return-void
.end method

.method a(Landroid/content/Context;)Z
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 186
    iget-object v0, p0, Ljhg;->b:[Ljava/lang/Long;

    if-nez v0, :cond_5

    iget-object v0, p0, Ljhg;->c:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 187
    const-class v0, Lieh;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 188
    invoke-interface {v0}, Lieh;->a()Ljava/util/List;

    move-result-object v4

    .line 189
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 190
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    move v3, v2

    :goto_0
    if-ge v3, v6, :cond_2

    .line 191
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lief;

    .line 192
    iget-object v7, p0, Ljhg;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v7}, Lieh;->b(Lief;Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 194
    :try_start_0
    invoke-virtual {v1}, Lief;->e()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v7

    const/16 v8, 0x10

    invoke-static {v7, v8}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 190
    :cond_0
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 196
    :catch_0
    move-exception v7

    const-string v7, "LogSinkImpl"

    const/4 v8, 0x6

    invoke-static {v7, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 197
    const-string v7, "LogSinkImpl"

    const-string v8, "Number cannot be parsed from experiment: "

    .line 198
    invoke-virtual {v1}, Lief;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v9

    if-eqz v9, :cond_1

    invoke-virtual {v8, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 197
    :goto_2
    invoke-static {v7, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 198
    :cond_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 204
    :cond_2
    new-instance v3, Llnp;

    const-class v0, Ljhb;

    invoke-direct {v3, p1, v0}, Llnp;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 206
    invoke-virtual {v3}, Llnp;->a()Ljava/util/ArrayList;

    move-result-object v4

    move v1, v2

    .line 207
    :goto_3
    invoke-virtual {v3}, Llnp;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 208
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Llnp;->a(Ljava/lang/Object;)Llno;

    move-result-object v0

    check-cast v0, Ljhb;

    .line 209
    invoke-virtual {v0}, Ljhb;->c()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 210
    invoke-virtual {v0}, Ljhb;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 213
    :cond_4
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Long;

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Long;

    iput-object v0, p0, Ljhg;->b:[Ljava/lang/Long;

    .line 214
    const/4 v2, 0x1

    .line 216
    :cond_5
    return v2
.end method

.method a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Ljhg;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljhg;->c:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a()[Lqbk;
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Ljhg;->a:Ljava/util/List;

    iget-object v1, p0, Ljhg;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Lqbk;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lqbk;

    return-object v0
.end method

.method b()[Ljava/lang/Long;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Ljhg;->b:[Ljava/lang/Long;

    return-object v0
.end method

.method c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Ljhg;->c:Ljava/lang/String;

    return-object v0
.end method

.method d()V
    .locals 1

    .prologue
    .line 220
    const/4 v0, 0x0

    iput-object v0, p0, Ljhg;->b:[Ljava/lang/Long;

    .line 221
    return-void
.end method

.method e()V
    .locals 1

    .prologue
    .line 229
    const/4 v0, 0x0

    iput-object v0, p0, Ljhg;->c:Ljava/lang/String;

    .line 230
    iget-object v0, p0, Ljhg;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 231
    return-void
.end method

.method f()Z
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Ljhg;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v1, 0x32

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
