.class public final Lhaj;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 426
    new-instance v0, Landroid/util/SparseArray;

    const/16 v1, 0x80

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    .line 428
    sput-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/4 v1, 0x0

    const-string v2, "Brightness"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 429
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/4 v1, 0x1

    const-string v2, "Contrast"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 430
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/4 v1, 0x2

    const-string v2, "Saturation"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 432
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/4 v1, 0x3

    const-string v2, "Style"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 433
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/4 v1, 0x4

    const-string v2, "CenterSize"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 434
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/4 v1, 0x5

    const-string v2, "Center"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 435
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/4 v1, 0x6

    const-string v2, "VignetteStrength"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 437
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/4 v1, 0x7

    const-string v2, "FixExposure"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 438
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x8

    const-string v2, "FineTuneColor"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 440
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x9

    const-string v2, "StyleStrength"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 442
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xa

    const-string v2, "Ambiance"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 443
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xb

    const-string v2, "Warmth"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 445
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xc

    const-string v2, "FilterStrength"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 447
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xd

    const-string v2, "BlurRadius"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 449
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xe

    const-string v2, "Grain"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 451
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xf

    const-string v2, "Sharpening"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 452
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x10

    const-string v2, "Structure"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 454
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x11

    const-string v2, "Transition"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 455
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x12

    const-string v2, "Rotation"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 456
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x13

    const-string v2, "BlurStrength"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 458
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x14

    const-string v2, "Shadows"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 459
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x15

    const-string v2, "Highlights"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 461
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x16

    const-string v2, "InnerBrightness"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 462
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x17

    const-string v2, "OuterBrightness"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 464
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x18

    const-string v2, "CenterX"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 465
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x19

    const-string v2, "CenterY"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 467
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1a

    const-string v2, "LowTones"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 468
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1b

    const-string v2, "MidTones"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 469
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1c

    const-string v2, "HighTones"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 470
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1d

    const-string v2, "ProtectShadows"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 471
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1e

    const-string v2, "ProtectHighlights"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 472
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1f

    const-string v2, "Exposure"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 473
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x20

    const-string v2, "FilePath"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 475
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x26

    const-string v2, "StraightenAngle"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 476
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x27

    const-string v2, "PostRotate"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 478
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x28

    const-string v2, "CropRotate"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 479
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x29

    const-string v2, "CropCoordinates"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 480
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x2a

    const-string v2, "CropAspectRatio"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 481
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x2b

    const-string v2, "X1"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 482
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x2c

    const-string v2, "X2"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 483
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x2d

    const-string v2, "Y1"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 484
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x2e

    const-string v2, "Y2"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 486
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x32

    const-string v2, "PerspectiveX"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 487
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x33

    const-string v2, "PerspectiveY"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 489
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x65

    const-string v2, "Texture"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 490
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x66

    const-string v2, "TextureOption"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 491
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x67

    const-string v2, "TextureOffset"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 492
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x68

    const-string v2, "TextureStrength"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 494
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x69

    const-string v2, "TextureOption2"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 496
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x6a

    const-string v2, "TextureOffsetX"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 497
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x6b

    const-string v2, "TextureOffsetY"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 499
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x71

    const-string v2, "TextureOffset2"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 501
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xc9

    const-string v2, "Generic1"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 502
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xca

    const-string v2, "Generic2"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 503
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xcb

    const-string v2, "Generic3"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 504
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xcc

    const-string v2, "Generic4"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 505
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xcd

    const-string v2, "Generic5"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 507
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xd3

    const-string v2, "VignetteTexture1"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 508
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xd4

    const-string v2, "VignetteTexture2"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 510
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xdd

    const-string v2, "FrameOffset"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 511
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xde

    const-string v2, "FrameWidth"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 512
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xdf

    const-string v2, "FrameType"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 513
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xe0

    const-string v2, "FrameFormat"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 515
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xe7

    const-string v2, "SaturationVariance"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 517
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xe8

    const-string v2, "LightLeakStrength"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 518
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xe9

    const-string v2, "ScratchesStrength"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 519
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xea

    const-string v2, "LightLeakId"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 520
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xeb

    const-string v2, "ScratchesId"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 522
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0xf1

    const-string v2, "ColorStyle"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 524
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1f5

    const-string v2, "X"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 525
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1f6

    const-string v2, "Y"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 526
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1f7

    const-string v2, "Width"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 527
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1f8

    const-string v2, "Height"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 529
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x262

    const-string v2, "CropRotateRectLeft"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 530
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x263

    const-string v2, "CropRotateRectTop"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 531
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x264

    const-string v2, "CropRotateRectWidth"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 532
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x265

    const-string v2, "CropRotateRectHeight"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 533
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x266

    const-string v2, "CropRotateRotationAngle"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 534
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x267

    const-string v2, "CropRotateAspectRatio"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 535
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x268

    const-string v2, "CropRotatePostRotation"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 536
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x269

    const-string v2, "CropRotateImageWidth"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 537
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x26a

    const-string v2, "CropRotateImageHeight"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 539
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x280

    const-string v2, "ComplementaryStrength"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 540
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x281

    const-string v2, "LutStrength"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 541
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x283

    const-string v2, "SplitColor"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 542
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x284

    const-string v2, "ComplementaryMode"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 543
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x285

    const-string v2, "Hue"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 545
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x28a

    const-string v2, "Alpha"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 546
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x28b

    const-string v2, "Beta"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 547
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x28c

    const-string v2, "BlackPoint"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 548
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x28d

    const-string v2, "WhitePoint"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 549
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x28e

    const-string v2, "Denoise"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 551
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x320

    const-string v2, "Preset"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 553
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x352

    const-string v2, "PreviousLayer"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 555
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x385

    const-string v2, "BrushMode"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 556
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x386

    const-string v2, "BrushModeInvert"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 557
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x38f

    const-string v2, "BrushTipType"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 558
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x390

    const-string v2, "BrushTipSize"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 559
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x391

    const-string v2, "BrushStrokePath"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 560
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x392

    const-string v2, "BrushStrokePathIndex"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 561
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x393

    const-string v2, "BrushStrokeStrength"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 562
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x394

    const-string v2, "BrushStrokeRelativeStrength"

    .line 563
    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 564
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x399

    const-string v2, "BrushStrokeThickness"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 565
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x3a3

    const-string v2, "BrushInking"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 567
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x457

    const-string v2, "Colorstyle"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 568
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x458

    const-string v2, "Strength"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 569
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x459

    const-string v2, "Adjust"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 570
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x45a

    const-string v2, "Neutralize"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 571
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x45b

    const-string v2, "Red"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 572
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x45c

    const-string v2, "Green"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 573
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x45d

    const-string v2, "Blue"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 574
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x45e

    const-string v2, "Glow"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 576
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x5dc

    const-string v2, "Tilt"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 577
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x5dd

    const-string v2, "Pan"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 578
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x5de

    const-string v2, "Roll"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 579
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x5df

    const-string v2, "EyeSize"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 580
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x5e0

    const-string v2, "NoseSize"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 581
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x5e1

    const-string v2, "MouthSize"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 582
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x5e2

    const-string v2, "FocalLength"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 584
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const/16 v1, 0x3e8

    const-string v2, "None"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 585
    return-void
.end method

.method public static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 591
    sget-object v0, Lhaj;->a:Landroid/util/SparseArray;

    const-string v1, "unknown"

    invoke-virtual {v0, p0, v1}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
