.class public abstract Lkaz;
.super Lkbl;
.source "PG"


# instance fields
.field d:I

.field e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/libraries/social/media/ui/MediaView;",
            ">;"
        }
    .end annotation
.end field

.field f:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/libraries/social/media/ui/MediaView;",
            ">;"
        }
    .end annotation
.end field

.field private g:Landroid/view/View$OnClickListener;

.field private h:Landroid/view/View$OnLongClickListener;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lkbl;-><init>(Landroid/content/Context;)V

    .line 45
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)I
    .locals 4

    .prologue
    .line 172
    const/4 v1, -0x1

    .line 173
    const/4 v0, 0x0

    iget-object v2, p0, Lkaz;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_1

    .line 174
    iget-object v3, p0, Lkaz;->e:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-ne p1, v3, :cond_0

    .line 179
    :goto_1
    return v0

    .line 173
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method a(Landroid/content/Context;I)Lcom/google/android/libraries/social/media/ui/MediaView;
    .locals 8

    .prologue
    const v7, 0x7f110004

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 102
    iget-object v0, p0, Lkaz;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 103
    iget-object v0, p0, Lkaz;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 122
    :goto_0
    invoke-virtual {p0, v0}, Lkaz;->addView(Landroid/view/View;)V

    .line 123
    return-object v0

    .line 105
    :cond_0
    new-instance v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;)V

    .line 106
    invoke-virtual {v0, v6}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(I)V

    .line 107
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->i(I)V

    .line 108
    invoke-virtual {v0, v5}, Lcom/google/android/libraries/social/media/ui/MediaView;->setClickable(Z)V

    .line 109
    iget-object v1, p0, Lkaz;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 110
    iget-object v1, p0, Lkaz;->h:Landroid/view/View$OnLongClickListener;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 111
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 112
    iget-object v2, p0, Lkaz;->b:Lkzw;

    invoke-virtual {v2}, Lkzw;->n()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lkaz;->b:Lkzw;

    invoke-virtual {v2}, Lkzw;->m()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 113
    :cond_1
    invoke-virtual {v1, v7, v5}, Landroid/content/res/Resources;->getQuantityString(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 120
    :goto_1
    iget-object v1, p0, Lkaz;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 116
    :cond_2
    iget-object v2, p0, Lkaz;->b:Lkzw;

    .line 117
    invoke-virtual {v2}, Lkzw;->b()I

    move-result v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    add-int/lit8 v4, p2, 0x1

    .line 118
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    iget-object v4, p0, Lkaz;->b:Lkzw;

    invoke-virtual {v4}, Lkzw;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    .line 116
    invoke-virtual {v1, v7, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v1, 0x0

    .line 146
    invoke-super {p0}, Lkbl;->a()V

    .line 147
    invoke-virtual {p0}, Lkaz;->removeAllViews()V

    .line 148
    iget-object v0, p0, Lkaz;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    .line 149
    iget-object v0, p0, Lkaz;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->clearAnimation()V

    .line 148
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 151
    :cond_0
    iget-object v0, p0, Lkaz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_1

    .line 152
    iget-object v0, p0, Lkaz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->clearAnimation()V

    .line 151
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 154
    :cond_1
    iget-object v0, p0, Lkaz;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v4, :cond_2

    .line 155
    iget-object v0, p0, Lkaz;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 157
    :cond_2
    iget-object v0, p0, Lkaz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v4, :cond_3

    .line 158
    iget-object v0, p0, Lkaz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 160
    :cond_3
    return-void
.end method

.method public a(I)V
    .locals 10

    .prologue
    .line 65
    invoke-super {p0, p1}, Lkbl;->a(I)V

    .line 66
    invoke-virtual {p0}, Lkaz;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 67
    iget-object v1, p0, Lkaz;->b:Lkzw;

    invoke-virtual {v1}, Lkzw;->j()[Lkzx;

    move-result-object v9

    .line 68
    const/4 v1, 0x0

    move v8, v1

    :goto_0
    iget v1, p0, Lkaz;->r:I

    if-ge v8, v1, :cond_1

    .line 69
    aget-object v1, v9, v8

    invoke-virtual {v1}, Lkzx;->d()Lkzv;

    move-result-object v6

    .line 70
    if-eqz v6, :cond_0

    invoke-virtual {v6}, Lkzv;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 71
    iget-object v1, p0, Lkaz;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v6}, Lkzv;->f()Ljava/lang/String;

    move-result-object v1

    .line 72
    invoke-virtual {v6}, Lkzv;->h()J

    move-result-wide v2

    invoke-virtual {v6}, Lkzv;->e()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v6}, Lkzv;->m()Ljac;

    move-result-object v6

    .line 71
    invoke-static/range {v0 .. v6}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v1

    invoke-virtual {v7, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 68
    :cond_0
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto :goto_0

    .line 76
    :cond_1
    invoke-virtual {p0}, Lkaz;->c()I

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iput v0, p0, Lkaz;->d:I

    .line 78
    return-void
.end method

.method protected a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 57
    invoke-super {p0, p1}, Lkbl;->a(Landroid/content/Context;)V

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkaz;->e:Ljava/util/ArrayList;

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkaz;->f:Ljava/util/ArrayList;

    .line 61
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lkaz;->g:Landroid/view/View$OnClickListener;

    .line 164
    return-void
.end method

.method public a(Landroid/view/View$OnLongClickListener;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lkaz;->h:Landroid/view/View$OnLongClickListener;

    .line 169
    return-void
.end method

.method b(Landroid/content/Context;I)Lcom/google/android/libraries/social/media/ui/MediaView;
    .locals 3

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 128
    iget-object v0, p0, Lkaz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 129
    iget-object v0, p0, Lkaz;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 140
    :goto_0
    invoke-virtual {p0, v0}, Lkaz;->addView(Landroid/view/View;)V

    .line 141
    return-object v0

    .line 131
    :cond_0
    new-instance v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/social/media/ui/MediaView;-><init>(Landroid/content/Context;)V

    .line 132
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(I)V

    .line 133
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->d(I)V

    .line 134
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setClickable(Z)V

    .line 135
    iget-object v1, p0, Lkaz;->g:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    invoke-static {v0, v2}, Liu;->c(Landroid/view/View;I)V

    .line 138
    iget-object v1, p0, Lkaz;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public b()V
    .locals 6

    .prologue
    .line 82
    invoke-virtual {p0}, Lkaz;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 83
    iget-object v0, p0, Lkaz;->b:Lkzw;

    invoke-virtual {v0}, Lkzw;->j()[Lkzx;

    move-result-object v2

    .line 84
    const/4 v0, 0x0

    :goto_0
    iget v3, p0, Lkaz;->r:I

    if-ge v0, v3, :cond_1

    .line 85
    aget-object v3, v2, v0

    invoke-virtual {v3}, Lkzx;->d()Lkzv;

    move-result-object v3

    .line 86
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Lkzv;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 87
    invoke-virtual {p0, v1, v0}, Lkaz;->a(Landroid/content/Context;I)Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 88
    invoke-virtual {p0, v1, v0}, Lkaz;->b(Landroid/content/Context;I)Lcom/google/android/libraries/social/media/ui/MediaView;

    move-result-object v4

    .line 89
    invoke-virtual {v3}, Lkzv;->e()Ljava/lang/String;

    move-result-object v3

    const/16 v5, 0x100

    invoke-static {v3, v5}, Ljbd;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v3

    sget-object v5, Ljac;->a:Ljac;

    .line 88
    invoke-static {v1, v3, v5}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 92
    :cond_0
    invoke-virtual {p0, v1, v0}, Lkaz;->f(Landroid/content/Context;I)Landroid/view/View;

    .line 93
    invoke-virtual {p0, v1, v0}, Lkaz;->e(Landroid/content/Context;I)Landroid/view/View;

    .line 94
    invoke-virtual {p0, v1, v0}, Lkaz;->c(Landroid/content/Context;I)Landroid/widget/TextView;

    move-result-object v3

    aget-object v4, v2, v0

    invoke-virtual {v4}, Lkzx;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 95
    invoke-virtual {p0, v1, v0}, Lkaz;->d(Landroid/content/Context;I)Landroid/widget/TextView;

    .line 84
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 97
    :cond_1
    iget-object v0, p0, Lkaz;->p:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lkaz;->addView(Landroid/view/View;)V

    .line 98
    return-void
.end method

.method public b(I)V
    .locals 6

    .prologue
    .line 183
    invoke-virtual {p0}, Lkaz;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhtj;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhtj;

    .line 184
    iget-object v1, p0, Lkaz;->b:Lkzw;

    invoke-virtual {v1}, Lkzw;->j()[Lkzx;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lkzx;->d()Lkzv;

    move-result-object v3

    .line 185
    iget-object v1, p0, Lkaz;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->l()Lizu;

    move-result-object v1

    .line 186
    invoke-virtual {v3}, Lkzv;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3}, Lkzv;->f()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lkaz;->b:Lkzw;

    .line 187
    invoke-virtual {v5}, Lkzw;->e()Ljava/lang/String;

    move-result-object v5

    .line 185
    invoke-interface/range {v0 .. v5}, Lhtj;->a(Lizu;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V

    .line 188
    return-void
.end method

.method abstract c()I
.end method
