.class public final Lkaq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljxz;
.implements Llnx;
.implements Llrg;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lhoc;

.field private c:Ljxt;


# direct methods
.method constructor <init>(Llqr;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 31
    return-void
.end method

.method constructor <init>(Llqr;B)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 35
    return-void
.end method

.method private a()Ljxt;
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lkaq;->c:Ljxt;

    if-nez v0, :cond_0

    .line 91
    iget-object v0, p0, Lkaq;->a:Landroid/content/Context;

    const-class v1, Ljxt;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxt;

    iput-object v0, p0, Lkaq;->c:Ljxt;

    .line 93
    :cond_0
    iget-object v0, p0, Lkaq;->c:Ljxt;

    return-object v0
.end method

.method private b()Lhoc;
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lkaq;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 98
    const/4 v0, 0x0

    .line 104
    :goto_0
    return-object v0

    .line 101
    :cond_0
    iget-object v0, p0, Lkaq;->b:Lhoc;

    if-nez v0, :cond_1

    .line 102
    iget-object v0, p0, Lkaq;->a:Landroid/content/Context;

    const-class v1, Lhoc;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Lkaq;->b:Lhoc;

    .line 104
    :cond_1
    iget-object v0, p0, Lkaq;->b:Lhoc;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lkaq;->a:Landroid/content/Context;

    .line 40
    return-void
.end method

.method public a(Lhob;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Lkaq;->b()Lhoc;

    move-result-object v0

    .line 74
    if-nez v0, :cond_0

    .line 78
    :goto_0
    return-void

    .line 77
    :cond_0
    invoke-virtual {v0, p1}, Lhoc;->a(Lhob;)Lhoc;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lkzw;I)V
    .locals 3

    .prologue
    .line 63
    invoke-direct {p0}, Lkaq;->b()Lhoc;

    move-result-object v0

    .line 64
    if-nez v0, :cond_0

    .line 69
    :goto_0
    return-void

    .line 67
    :cond_0
    new-instance v1, Lkab;

    iget-object v2, p0, Lkaq;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, p3, p1, p2}, Lkab;-><init>(Landroid/content/Context;ILjava/lang/String;Lkzw;)V

    invoke-virtual {v0, v1}, Lhoc;->b(Lhny;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Lkzw;IZ)V
    .locals 8

    .prologue
    .line 45
    iget-object v0, p0, Lkaq;->a:Landroid/content/Context;

    const-class v1, Lhee;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 46
    invoke-virtual {p0, p1, p2, v1}, Lkaq;->a(Ljava/lang/String;Lkzw;I)V

    .line 47
    invoke-direct {p0}, Lkaq;->a()Ljxt;

    move-result-object v0

    invoke-virtual {p2}, Lkzw;->c()Ljava/lang/String;

    move-result-object v3

    .line 48
    invoke-virtual {p2}, Lkzw;->g()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lkzw;->f()I

    move-result v5

    move-object v2, p1

    move v6, p3

    move v7, p4

    .line 47
    invoke-interface/range {v0 .. v7}, Ljxt;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)V

    .line 50
    return-void
.end method

.method public b(Lhob;)V
    .locals 1

    .prologue
    .line 82
    invoke-direct {p0}, Lkaq;->b()Lhoc;

    move-result-object v0

    .line 83
    if-nez v0, :cond_0

    .line 87
    :goto_0
    return-void

    .line 86
    :cond_0
    invoke-virtual {v0, p1}, Lhoc;->b(Lhob;)Lhoc;

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Lkzw;IZ)V
    .locals 7

    .prologue
    .line 55
    iget-object v0, p0, Lkaq;->a:Landroid/content/Context;

    const-class v1, Lhee;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v1

    .line 56
    invoke-virtual {p0, p1, p2, v1}, Lkaq;->a(Ljava/lang/String;Lkzw;I)V

    .line 57
    invoke-direct {p0}, Lkaq;->a()Ljxt;

    move-result-object v0

    invoke-virtual {p2}, Lkzw;->c()Ljava/lang/String;

    move-result-object v3

    .line 58
    invoke-virtual {p2}, Lkzw;->f()I

    move-result v4

    move-object v2, p1

    move v5, p3

    move v6, p4

    .line 57
    invoke-interface/range {v0 .. v6}, Ljxt;->a(ILjava/lang/String;Ljava/lang/String;IIZ)V

    .line 59
    return-void
.end method
