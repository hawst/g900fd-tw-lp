.class public final Limt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lgst;
.implements Lgsu;
.implements Lgsv;
.implements Limp;


# instance fields
.field private a:Lgsp;

.field private b:Limr;

.field private c:Limq;

.field private d:Lims;


# direct methods
.method public constructor <init>(Lgsp;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-object p1, p0, Limt;->a:Lgsp;

    .line 33
    return-void
.end method


# virtual methods
.method public a()Limu;
    .locals 2

    .prologue
    .line 37
    new-instance v0, Limv;

    iget-object v1, p0, Limt;->a:Lgsp;

    invoke-virtual {v1}, Lgsp;->e()Lgsx;

    move-result-object v1

    invoke-direct {v0, v1}, Limv;-><init>(Lgsx;)V

    return-object v0
.end method

.method public a(Ling;DIII)Line;
    .locals 6

    .prologue
    .line 52
    new-instance v0, Lcom/google/android/gms/maps/model/CircleOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/CircleOptions;-><init>()V

    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, p1, Ling;->a:D

    iget-wide v4, p1, Ling;->b:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 53
    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/CircleOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/CircleOptions;

    move-result-object v0

    .line 54
    invoke-virtual {v0, p2, p3}, Lcom/google/android/gms/maps/model/CircleOptions;->a(D)Lcom/google/android/gms/maps/model/CircleOptions;

    move-result-object v0

    int-to-float v1, p4

    .line 55
    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/CircleOptions;->a(F)Lcom/google/android/gms/maps/model/CircleOptions;

    move-result-object v0

    .line 56
    invoke-virtual {v0, p5}, Lcom/google/android/gms/maps/model/CircleOptions;->a(I)Lcom/google/android/gms/maps/model/CircleOptions;

    move-result-object v0

    invoke-virtual {v0, p6}, Lcom/google/android/gms/maps/model/CircleOptions;->b(I)Lcom/google/android/gms/maps/model/CircleOptions;

    move-result-object v0

    .line 58
    new-instance v1, Linf;

    iget-object v2, p0, Limt;->a:Lgsp;

    invoke-virtual {v2, v0}, Lgsp;->a(Lcom/google/android/gms/maps/model/CircleOptions;)Lgwr;

    move-result-object v0

    invoke-direct {v1, v0}, Linf;-><init>(Lgwr;)V

    return-object v1
.end method

.method public a(Ling;Landroid/graphics/Bitmap;FF)Linl;
    .locals 8

    .prologue
    .line 41
    iget-object v0, p0, Limt;->a:Lgsp;

    new-instance v1, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    new-instance v2, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, p1, Ling;->a:D

    iget-wide v6, p1, Ling;->b:D

    invoke-direct {v2, v4, v5, v6, v7}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 42
    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    .line 43
    invoke-static {p2}, Lgwp;->a(Landroid/graphics/Bitmap;)Lgwo;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lgwo;)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    .line 44
    invoke-virtual {v1, p3, p4}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(FF)Lcom/google/android/gms/maps/model/MarkerOptions;

    move-result-object v1

    .line 41
    invoke-virtual {v0, v1}, Lgsp;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lgwt;

    move-result-object v0

    .line 46
    new-instance v1, Linm;

    invoke-direct {v1, v0}, Linm;-><init>(Lgwt;)V

    return-object v1
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Limt;->a:Lgsp;

    invoke-virtual {v0, p1}, Lgsp;->a(I)V

    .line 107
    return-void
.end method

.method public a(Lcom/google/android/gms/maps/model/CameraPosition;)V
    .locals 2

    .prologue
    .line 136
    iget-object v0, p0, Limt;->c:Limq;

    if-eqz v0, :cond_0

    .line 137
    iget-object v0, p0, Limt;->c:Limq;

    new-instance v1, Lind;

    invoke-direct {v1, p1}, Lind;-><init>(Lcom/google/android/gms/maps/model/CameraPosition;)V

    invoke-interface {v0, v1}, Limq;->a(Linc;)V

    .line 139
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 6

    .prologue
    .line 129
    iget-object v0, p0, Limt;->b:Limr;

    if-eqz v0, :cond_0

    .line 130
    iget-object v0, p0, Limt;->b:Limr;

    new-instance v1, Ling;

    iget-wide v2, p1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v4, p1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-direct {v1, v2, v3, v4, v5}, Ling;-><init>(DD)V

    invoke-interface {v0}, Limr;->a()V

    .line 132
    :cond_0
    return-void
.end method

.method public a(Limq;)V
    .locals 1

    .prologue
    .line 117
    iput-object p1, p0, Limt;->c:Limq;

    .line 118
    iget-object v0, p0, Limt;->a:Lgsp;

    invoke-virtual {v0, p0}, Lgsp;->a(Lgst;)V

    .line 119
    return-void
.end method

.method public a(Limr;)V
    .locals 1

    .prologue
    .line 111
    iput-object p1, p0, Limt;->b:Limr;

    .line 112
    iget-object v0, p0, Limt;->a:Lgsp;

    invoke-virtual {v0, p0}, Lgsp;->a(Lgsu;)V

    .line 113
    return-void
.end method

.method public a(Lims;)V
    .locals 1

    .prologue
    .line 123
    iput-object p1, p0, Limt;->d:Lims;

    .line 124
    iget-object v0, p0, Limt;->a:Lgsp;

    invoke-virtual {v0, p0}, Lgsp;->a(Lgsv;)V

    .line 125
    return-void
.end method

.method public a(Ling;)V
    .locals 6

    .prologue
    .line 80
    iget-object v0, p0, Limt;->a:Lgsp;

    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, p1, Ling;->a:D

    iget-wide v4, p1, Ling;->b:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 81
    invoke-static {v1}, Lgso;->a(Lcom/google/android/gms/maps/model/LatLng;)Lgsn;

    move-result-object v1

    .line 80
    invoke-virtual {v0, v1}, Lgsp;->b(Lgsn;)V

    .line 82
    return-void
.end method

.method public a(Ling;I)V
    .locals 6

    .prologue
    .line 63
    iget-object v0, p0, Limt;->a:Lgsp;

    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, p1, Ling;->a:D

    iget-wide v4, p1, Ling;->b:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    int-to-float v2, p2

    .line 64
    invoke-static {v1, v2}, Lgso;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lgsn;

    move-result-object v1

    .line 63
    invoke-virtual {v0, v1}, Lgsp;->a(Lgsn;)V

    .line 65
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Limt;->a:Lgsp;

    invoke-virtual {v0, p1}, Lgsp;->a(Z)V

    .line 92
    return-void
.end method

.method public a(Lgwt;)Z
    .locals 2

    .prologue
    .line 143
    iget-object v0, p0, Limt;->d:Lims;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Limt;->d:Lims;

    new-instance v1, Linm;

    invoke-direct {v1, p1}, Linm;-><init>(Lgwt;)V

    invoke-interface {v0, v1}, Lims;->a(Linl;)Z

    move-result v0

    .line 146
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lina;
    .locals 2

    .prologue
    .line 86
    new-instance v0, Linb;

    iget-object v1, p0, Limt;->a:Lgsp;

    invoke-virtual {v1}, Lgsp;->d()Lgtb;

    move-result-object v1

    invoke-direct {v0, v1}, Linb;-><init>(Lgtb;)V

    return-object v0
.end method

.method public b(Ling;I)V
    .locals 6

    .prologue
    .line 74
    iget-object v0, p0, Limt;->a:Lgsp;

    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, p1, Ling;->a:D

    iget-wide v4, p1, Ling;->b:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    int-to-float v2, p2

    .line 75
    invoke-static {v1, v2}, Lgso;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lgsn;

    move-result-object v1

    .line 74
    invoke-virtual {v0, v1}, Lgsp;->b(Lgsn;)V

    .line 76
    return-void
.end method

.method public c()Linc;
    .locals 2

    .prologue
    .line 96
    new-instance v0, Lind;

    iget-object v1, p0, Limt;->a:Lgsp;

    invoke-virtual {v1}, Lgsp;->b()Lcom/google/android/gms/maps/model/CameraPosition;

    move-result-object v1

    invoke-direct {v0, v1}, Lind;-><init>(Lcom/google/android/gms/maps/model/CameraPosition;)V

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Limt;->a:Lgsp;

    invoke-virtual {v0}, Lgsp;->c()I

    move-result v0

    return v0
.end method
