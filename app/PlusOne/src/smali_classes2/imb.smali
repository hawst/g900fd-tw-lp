.class public final Limb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lily;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lijk;Landroid/accounts/Account;)Lijq;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lijk;",
            "Landroid/accounts/Account;",
            ")",
            "Lijq",
            "<",
            "Limf;",
            ">;"
        }
    .end annotation

    .prologue
    .line 26
    check-cast p1, Lijh;

    .line 27
    sget-object v0, Lgse;->b:Lgsb;

    .line 28
    invoke-virtual {p1}, Lijh;->e()Lgfz;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lgsb;->a(Lgfz;Landroid/accounts/Account;)Lggd;

    move-result-object v0

    .line 30
    new-instance v1, Lijr;

    new-instance v2, Limc;

    invoke-direct {v2}, Limc;-><init>()V

    invoke-direct {v1, v0, v2}, Lijr;-><init>(Lggd;Likg;)V

    return-object v1
.end method

.method public a(Lijk;Limj;)Lijq;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lijk;",
            "Limj;",
            ")",
            "Lijq",
            "<",
            "Limh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 42
    check-cast p1, Lijh;

    .line 43
    check-cast p2, Limm;

    .line 44
    sget-object v0, Lgse;->b:Lgsb;

    .line 45
    invoke-virtual {p1}, Lijh;->e()Lgfz;

    move-result-object v1

    invoke-virtual {p2}, Limm;->a()Lcom/google/android/gms/location/reporting/UploadRequest;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lgsb;->a(Lgfz;Lcom/google/android/gms/location/reporting/UploadRequest;)Lggd;

    move-result-object v0

    .line 47
    new-instance v1, Lijr;

    new-instance v2, Limd;

    invoke-direct {v2}, Limd;-><init>()V

    invoke-direct {v1, v0, v2}, Lijr;-><init>(Lggd;Likg;)V

    return-object v1
.end method

.method public b(Lijk;Landroid/accounts/Account;)Lijq;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lijk;",
            "Landroid/accounts/Account;",
            ")",
            "Lijq",
            "<",
            "Lika;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    check-cast p1, Lijh;

    .line 59
    sget-object v0, Lgse;->b:Lgsb;

    .line 60
    invoke-virtual {p1}, Lijh;->e()Lgfz;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lgsb;->b(Lgfz;Landroid/accounts/Account;)Lggd;

    move-result-object v0

    .line 62
    new-instance v1, Lijr;

    new-instance v2, Lime;

    invoke-direct {v2}, Lime;-><init>()V

    invoke-direct {v1, v0, v2}, Lijr;-><init>(Lggd;Likg;)V

    return-object v1
.end method
