.class public Lbiu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbij;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbij",
        "<",
        "Lboo;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Landroid/net/Uri;

.field private final b:Landroid/media/MediaMetadataRetriever;

.field private final c:Ljfb;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lbiu;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Ljfb;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const-string v0, "context"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 44
    const-string v0, "uri"

    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lbiu;->a:Landroid/net/Uri;

    .line 46
    new-instance v0, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v0}, Landroid/media/MediaMetadataRetriever;-><init>()V

    iput-object v0, p0, Lbiu;->b:Landroid/media/MediaMetadataRetriever;

    .line 47
    iget-object v0, p0, Lbiu;->b:Landroid/media/MediaMetadataRetriever;

    iget-object v1, p0, Lbiu;->a:Landroid/net/Uri;

    invoke-static {v0, v1, p1}, Lbqd;->a(Landroid/media/MediaMetadataRetriever;Landroid/net/Uri;Landroid/content/Context;)V

    .line 48
    const-string v0, "movieMakerProvider"

    invoke-static {p3, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfb;

    iput-object v0, p0, Lbiu;->c:Ljfb;

    .line 49
    return-void
.end method

.method private c()J
    .locals 8

    .prologue
    const-wide/16 v6, 0x3e8

    const-wide/16 v0, -0x1

    .line 72
    iget-object v2, p0, Lbiu;->c:Ljfb;

    iget-object v3, p0, Lbiu;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljfb;->a(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 73
    iget-object v2, p0, Lbiu;->c:Ljfb;

    iget-object v3, p0, Lbiu;->a:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljfb;->f(Landroid/net/Uri;)J

    move-result-wide v2

    .line 74
    cmp-long v4, v2, v0

    if-eqz v4, :cond_1

    .line 75
    mul-long v0, v2, v6

    .line 92
    :cond_0
    :goto_0
    return-wide v0

    .line 81
    :cond_1
    iget-object v2, p0, Lbiu;->b:Landroid/media/MediaMetadataRetriever;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v2

    .line 83
    if-eqz v2, :cond_0

    .line 84
    new-instance v3, Ljava/text/SimpleDateFormat;

    const-string v4, "yyyyMMdd\'T\'HHmmss.SSS\'Z\'"

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v3, v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 86
    :try_start_0
    invoke-virtual {v3, v2}, Ljava/text/DateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    mul-long/2addr v0, v6

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lbiu;->b:Landroid/media/MediaMetadataRetriever;

    invoke-virtual {v0}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 54
    return-void
.end method

.method public b()Lboo;
    .locals 12

    .prologue
    const/4 v0, 0x0

    const/4 v6, -0x1

    .line 58
    new-instance v1, Lboo;

    .line 59
    invoke-direct {p0}, Lbiu;->c()J

    move-result-wide v2

    .line 60
    iget-object v4, p0, Lbiu;->b:Landroid/media/MediaMetadataRetriever;

    const/16 v5, 0x18

    invoke-virtual {v4, v5}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    move v4, v0

    .line 61
    :goto_0
    iget-object v5, p0, Lbiu;->b:Landroid/media/MediaMetadataRetriever;

    const/16 v7, 0x12

    invoke-virtual {v5, v7}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v5

    if-nez v5, :cond_1

    move v5, v6

    .line 62
    :goto_1
    iget-object v7, p0, Lbiu;->b:Landroid/media/MediaMetadataRetriever;

    const/16 v8, 0x13

    invoke-virtual {v7, v8}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v7

    if-nez v7, :cond_2

    .line 63
    :goto_2
    iget-object v7, p0, Lbiu;->b:Landroid/media/MediaMetadataRetriever;

    const/16 v8, 0x10

    invoke-virtual {v7, v8}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_3

    const-string v8, "yes"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v7, 0x1

    .line 64
    :goto_3
    iget-object v0, p0, Lbiu;->b:Landroid/media/MediaMetadataRetriever;

    const/16 v8, 0x9

    invoke-virtual {v0, v8}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    :goto_4
    invoke-direct/range {v1 .. v9}, Lboo;-><init>(JIIIZJ)V

    return-object v1

    .line 60
    :cond_0
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    goto :goto_0

    .line 61
    :cond_1
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    goto :goto_1

    .line 62
    :cond_2
    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    goto :goto_2

    :cond_3
    move v7, v0

    .line 63
    goto :goto_3

    .line 64
    :cond_4
    const-wide/16 v8, -0x1

    goto :goto_4
.end method

.method public synthetic i()Lbmu;
    .locals 1

    .prologue
    .line 31
    invoke-virtual {p0}, Lbiu;->b()Lboo;

    move-result-object v0

    return-object v0
.end method
