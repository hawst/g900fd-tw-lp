.class public final Lbue;
.super Loxu;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field public c:[Lbuf;

.field public d:[Lbuf;

.field public e:[Lbuf;

.field public f:I

.field public g:Lbun;

.field public h:Ljava/lang/String;

.field public i:[Lbty;

.field public j:[Lbut;

.field public k:Lbua;

.field public l:Z

.field public m:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 9
    invoke-direct {p0}, Loxu;-><init>()V

    .line 12
    iput v1, p0, Lbue;->a:I

    .line 15
    iput v1, p0, Lbue;->b:I

    .line 18
    sget-object v0, Lbuf;->a:[Lbuf;

    iput-object v0, p0, Lbue;->c:[Lbuf;

    .line 21
    sget-object v0, Lbuf;->a:[Lbuf;

    iput-object v0, p0, Lbue;->d:[Lbuf;

    .line 24
    sget-object v0, Lbuf;->a:[Lbuf;

    iput-object v0, p0, Lbue;->e:[Lbuf;

    .line 27
    iput v1, p0, Lbue;->f:I

    .line 30
    iput-object v2, p0, Lbue;->g:Lbun;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lbue;->h:Ljava/lang/String;

    .line 36
    sget-object v0, Lbty;->a:[Lbty;

    iput-object v0, p0, Lbue;->i:[Lbty;

    .line 39
    sget-object v0, Lbut;->a:[Lbut;

    iput-object v0, p0, Lbue;->j:[Lbut;

    .line 42
    iput-object v2, p0, Lbue;->k:Lbua;

    .line 45
    iput-boolean v1, p0, Lbue;->l:Z

    .line 48
    iput-boolean v1, p0, Lbue;->m:Z

    .line 9
    return-void
.end method

.method public static a([B)Lbue;
    .locals 1

    .prologue
    .line 388
    new-instance v0, Lbue;

    invoke-direct {v0}, Lbue;-><init>()V

    invoke-static {v0, p0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lbue;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 134
    .line 135
    iget v0, p0, Lbue;->a:I

    if-eqz v0, :cond_11

    .line 136
    const/4 v0, 0x1

    iget v2, p0, Lbue;->a:I

    .line 137
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 139
    :goto_0
    iget v2, p0, Lbue;->b:I

    if-eqz v2, :cond_0

    .line 140
    const/4 v2, 0x2

    iget v3, p0, Lbue;->b:I

    .line 141
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 143
    :cond_0
    iget-object v2, p0, Lbue;->c:[Lbuf;

    if-eqz v2, :cond_2

    .line 144
    iget-object v3, p0, Lbue;->c:[Lbuf;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v5, v3, v2

    .line 145
    if-eqz v5, :cond_1

    .line 146
    const/4 v6, 0x3

    .line 147
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 144
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 151
    :cond_2
    iget v2, p0, Lbue;->f:I

    if-eqz v2, :cond_3

    .line 152
    const/4 v2, 0x4

    iget v3, p0, Lbue;->f:I

    .line 153
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 155
    :cond_3
    iget-object v2, p0, Lbue;->g:Lbun;

    if-eqz v2, :cond_4

    .line 156
    const/4 v2, 0x5

    iget-object v3, p0, Lbue;->g:Lbun;

    .line 157
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 159
    :cond_4
    iget-object v2, p0, Lbue;->h:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 160
    const/4 v2, 0x6

    iget-object v3, p0, Lbue;->h:Ljava/lang/String;

    .line 161
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 163
    :cond_5
    iget-object v2, p0, Lbue;->i:[Lbty;

    if-eqz v2, :cond_7

    .line 164
    iget-object v3, p0, Lbue;->i:[Lbty;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_7

    aget-object v5, v3, v2

    .line 165
    if-eqz v5, :cond_6

    .line 166
    const/4 v6, 0x7

    .line 167
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 164
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 171
    :cond_7
    iget-object v2, p0, Lbue;->j:[Lbut;

    if-eqz v2, :cond_9

    .line 172
    iget-object v3, p0, Lbue;->j:[Lbut;

    array-length v4, v3

    move v2, v1

    :goto_3
    if-ge v2, v4, :cond_9

    aget-object v5, v3, v2

    .line 173
    if-eqz v5, :cond_8

    .line 174
    const/16 v6, 0x8

    .line 175
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 172
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 179
    :cond_9
    iget-object v2, p0, Lbue;->k:Lbua;

    if-eqz v2, :cond_a

    .line 180
    const/16 v2, 0x9

    iget-object v3, p0, Lbue;->k:Lbua;

    .line 181
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 183
    :cond_a
    iget-boolean v2, p0, Lbue;->l:Z

    if-eqz v2, :cond_b

    .line 184
    const/16 v2, 0xa

    iget-boolean v3, p0, Lbue;->l:Z

    .line 185
    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 187
    :cond_b
    iget-boolean v2, p0, Lbue;->m:Z

    if-eqz v2, :cond_c

    .line 188
    const/16 v2, 0xb

    iget-boolean v3, p0, Lbue;->m:Z

    .line 189
    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 191
    :cond_c
    iget-object v2, p0, Lbue;->d:[Lbuf;

    if-eqz v2, :cond_e

    .line 192
    iget-object v3, p0, Lbue;->d:[Lbuf;

    array-length v4, v3

    move v2, v1

    :goto_4
    if-ge v2, v4, :cond_e

    aget-object v5, v3, v2

    .line 193
    if-eqz v5, :cond_d

    .line 194
    const/16 v6, 0xc

    .line 195
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 192
    :cond_d
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 199
    :cond_e
    iget-object v2, p0, Lbue;->e:[Lbuf;

    if-eqz v2, :cond_10

    .line 200
    iget-object v2, p0, Lbue;->e:[Lbuf;

    array-length v3, v2

    :goto_5
    if-ge v1, v3, :cond_10

    aget-object v4, v2, v1

    .line 201
    if-eqz v4, :cond_f

    .line 202
    const/16 v5, 0xd

    .line 203
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 200
    :cond_f
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 207
    :cond_10
    iput v0, p0, Lbue;->ai:I

    .line 208
    return v0

    :cond_11
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lbue;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 216
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 217
    sparse-switch v0, :sswitch_data_0

    .line 221
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 222
    :sswitch_0
    return-object p0

    .line 227
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    iput v0, p0, Lbue;->a:I

    goto :goto_0

    .line 231
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    iput v0, p0, Lbue;->b:I

    goto :goto_0

    .line 235
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 236
    iget-object v0, p0, Lbue;->c:[Lbuf;

    if-nez v0, :cond_2

    move v0, v1

    .line 237
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lbuf;

    .line 238
    iget-object v3, p0, Lbue;->c:[Lbuf;

    if-eqz v3, :cond_1

    .line 239
    iget-object v3, p0, Lbue;->c:[Lbuf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 241
    :cond_1
    iput-object v2, p0, Lbue;->c:[Lbuf;

    .line 242
    :goto_2
    iget-object v2, p0, Lbue;->c:[Lbuf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_3

    .line 243
    iget-object v2, p0, Lbue;->c:[Lbuf;

    new-instance v3, Lbuf;

    invoke-direct {v3}, Lbuf;-><init>()V

    aput-object v3, v2, v0

    .line 244
    iget-object v2, p0, Lbue;->c:[Lbuf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 245
    invoke-virtual {p1}, Loxn;->a()I

    .line 242
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 236
    :cond_2
    iget-object v0, p0, Lbue;->c:[Lbuf;

    array-length v0, v0

    goto :goto_1

    .line 248
    :cond_3
    iget-object v2, p0, Lbue;->c:[Lbuf;

    new-instance v3, Lbuf;

    invoke-direct {v3}, Lbuf;-><init>()V

    aput-object v3, v2, v0

    .line 249
    iget-object v2, p0, Lbue;->c:[Lbuf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 253
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 254
    if-eqz v0, :cond_4

    const/4 v2, 0x5

    if-eq v0, v2, :cond_4

    const/4 v2, 0x6

    if-eq v0, v2, :cond_4

    const/4 v2, 0x7

    if-eq v0, v2, :cond_4

    const/16 v2, 0x8

    if-eq v0, v2, :cond_4

    const/16 v2, 0xa

    if-eq v0, v2, :cond_4

    const/16 v2, 0xb

    if-eq v0, v2, :cond_4

    const/16 v2, 0xc

    if-eq v0, v2, :cond_4

    const/16 v2, 0xd

    if-eq v0, v2, :cond_4

    const/16 v2, 0xe

    if-eq v0, v2, :cond_4

    const/16 v2, 0xf

    if-eq v0, v2, :cond_4

    const/16 v2, 0x10

    if-eq v0, v2, :cond_4

    const/16 v2, 0x11

    if-eq v0, v2, :cond_4

    const/16 v2, 0x12

    if-eq v0, v2, :cond_4

    const/16 v2, 0x13

    if-eq v0, v2, :cond_4

    const/16 v2, 0x14

    if-eq v0, v2, :cond_4

    const/16 v2, 0x15

    if-eq v0, v2, :cond_4

    const/16 v2, 0x16

    if-eq v0, v2, :cond_4

    const/16 v2, 0x17

    if-eq v0, v2, :cond_4

    const/4 v2, 0x1

    if-eq v0, v2, :cond_4

    const/4 v2, 0x2

    if-eq v0, v2, :cond_4

    const/4 v2, 0x3

    if-eq v0, v2, :cond_4

    const/4 v2, 0x4

    if-eq v0, v2, :cond_4

    const/16 v2, 0x9

    if-ne v0, v2, :cond_5

    .line 278
    :cond_4
    iput v0, p0, Lbue;->f:I

    goto/16 :goto_0

    .line 280
    :cond_5
    iput v1, p0, Lbue;->f:I

    goto/16 :goto_0

    .line 285
    :sswitch_5
    iget-object v0, p0, Lbue;->g:Lbun;

    if-nez v0, :cond_6

    .line 286
    new-instance v0, Lbun;

    invoke-direct {v0}, Lbun;-><init>()V

    iput-object v0, p0, Lbue;->g:Lbun;

    .line 288
    :cond_6
    iget-object v0, p0, Lbue;->g:Lbun;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 292
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbue;->h:Ljava/lang/String;

    goto/16 :goto_0

    .line 296
    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 297
    iget-object v0, p0, Lbue;->i:[Lbty;

    if-nez v0, :cond_8

    move v0, v1

    .line 298
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lbty;

    .line 299
    iget-object v3, p0, Lbue;->i:[Lbty;

    if-eqz v3, :cond_7

    .line 300
    iget-object v3, p0, Lbue;->i:[Lbty;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 302
    :cond_7
    iput-object v2, p0, Lbue;->i:[Lbty;

    .line 303
    :goto_4
    iget-object v2, p0, Lbue;->i:[Lbty;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 304
    iget-object v2, p0, Lbue;->i:[Lbty;

    new-instance v3, Lbty;

    invoke-direct {v3}, Lbty;-><init>()V

    aput-object v3, v2, v0

    .line 305
    iget-object v2, p0, Lbue;->i:[Lbty;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 306
    invoke-virtual {p1}, Loxn;->a()I

    .line 303
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 297
    :cond_8
    iget-object v0, p0, Lbue;->i:[Lbty;

    array-length v0, v0

    goto :goto_3

    .line 309
    :cond_9
    iget-object v2, p0, Lbue;->i:[Lbty;

    new-instance v3, Lbty;

    invoke-direct {v3}, Lbty;-><init>()V

    aput-object v3, v2, v0

    .line 310
    iget-object v2, p0, Lbue;->i:[Lbty;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 314
    :sswitch_8
    const/16 v0, 0x42

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 315
    iget-object v0, p0, Lbue;->j:[Lbut;

    if-nez v0, :cond_b

    move v0, v1

    .line 316
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lbut;

    .line 317
    iget-object v3, p0, Lbue;->j:[Lbut;

    if-eqz v3, :cond_a

    .line 318
    iget-object v3, p0, Lbue;->j:[Lbut;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 320
    :cond_a
    iput-object v2, p0, Lbue;->j:[Lbut;

    .line 321
    :goto_6
    iget-object v2, p0, Lbue;->j:[Lbut;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_c

    .line 322
    iget-object v2, p0, Lbue;->j:[Lbut;

    new-instance v3, Lbut;

    invoke-direct {v3}, Lbut;-><init>()V

    aput-object v3, v2, v0

    .line 323
    iget-object v2, p0, Lbue;->j:[Lbut;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 324
    invoke-virtual {p1}, Loxn;->a()I

    .line 321
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 315
    :cond_b
    iget-object v0, p0, Lbue;->j:[Lbut;

    array-length v0, v0

    goto :goto_5

    .line 327
    :cond_c
    iget-object v2, p0, Lbue;->j:[Lbut;

    new-instance v3, Lbut;

    invoke-direct {v3}, Lbut;-><init>()V

    aput-object v3, v2, v0

    .line 328
    iget-object v2, p0, Lbue;->j:[Lbut;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 332
    :sswitch_9
    iget-object v0, p0, Lbue;->k:Lbua;

    if-nez v0, :cond_d

    .line 333
    new-instance v0, Lbua;

    invoke-direct {v0}, Lbua;-><init>()V

    iput-object v0, p0, Lbue;->k:Lbua;

    .line 335
    :cond_d
    iget-object v0, p0, Lbue;->k:Lbua;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 339
    :sswitch_a
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    iput-boolean v0, p0, Lbue;->l:Z

    goto/16 :goto_0

    .line 343
    :sswitch_b
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    iput-boolean v0, p0, Lbue;->m:Z

    goto/16 :goto_0

    .line 347
    :sswitch_c
    const/16 v0, 0x62

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 348
    iget-object v0, p0, Lbue;->d:[Lbuf;

    if-nez v0, :cond_f

    move v0, v1

    .line 349
    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Lbuf;

    .line 350
    iget-object v3, p0, Lbue;->d:[Lbuf;

    if-eqz v3, :cond_e

    .line 351
    iget-object v3, p0, Lbue;->d:[Lbuf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 353
    :cond_e
    iput-object v2, p0, Lbue;->d:[Lbuf;

    .line 354
    :goto_8
    iget-object v2, p0, Lbue;->d:[Lbuf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_10

    .line 355
    iget-object v2, p0, Lbue;->d:[Lbuf;

    new-instance v3, Lbuf;

    invoke-direct {v3}, Lbuf;-><init>()V

    aput-object v3, v2, v0

    .line 356
    iget-object v2, p0, Lbue;->d:[Lbuf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 357
    invoke-virtual {p1}, Loxn;->a()I

    .line 354
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 348
    :cond_f
    iget-object v0, p0, Lbue;->d:[Lbuf;

    array-length v0, v0

    goto :goto_7

    .line 360
    :cond_10
    iget-object v2, p0, Lbue;->d:[Lbuf;

    new-instance v3, Lbuf;

    invoke-direct {v3}, Lbuf;-><init>()V

    aput-object v3, v2, v0

    .line 361
    iget-object v2, p0, Lbue;->d:[Lbuf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 365
    :sswitch_d
    const/16 v0, 0x6a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 366
    iget-object v0, p0, Lbue;->e:[Lbuf;

    if-nez v0, :cond_12

    move v0, v1

    .line 367
    :goto_9
    add-int/2addr v2, v0

    new-array v2, v2, [Lbuf;

    .line 368
    iget-object v3, p0, Lbue;->e:[Lbuf;

    if-eqz v3, :cond_11

    .line 369
    iget-object v3, p0, Lbue;->e:[Lbuf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 371
    :cond_11
    iput-object v2, p0, Lbue;->e:[Lbuf;

    .line 372
    :goto_a
    iget-object v2, p0, Lbue;->e:[Lbuf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_13

    .line 373
    iget-object v2, p0, Lbue;->e:[Lbuf;

    new-instance v3, Lbuf;

    invoke-direct {v3}, Lbuf;-><init>()V

    aput-object v3, v2, v0

    .line 374
    iget-object v2, p0, Lbue;->e:[Lbuf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 375
    invoke-virtual {p1}, Loxn;->a()I

    .line 372
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    .line 366
    :cond_12
    iget-object v0, p0, Lbue;->e:[Lbuf;

    array-length v0, v0

    goto :goto_9

    .line 378
    :cond_13
    iget-object v2, p0, Lbue;->e:[Lbuf;

    new-instance v3, Lbuf;

    invoke-direct {v3}, Lbuf;-><init>()V

    aput-object v3, v2, v0

    .line 379
    iget-object v2, p0, Lbue;->e:[Lbuf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 217
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 71
    iget v1, p0, Lbue;->a:I

    if-eqz v1, :cond_0

    .line 72
    const/4 v1, 0x1

    iget v2, p0, Lbue;->a:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 74
    :cond_0
    iget v1, p0, Lbue;->b:I

    if-eqz v1, :cond_1

    .line 75
    const/4 v1, 0x2

    iget v2, p0, Lbue;->b:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 77
    :cond_1
    iget-object v1, p0, Lbue;->c:[Lbuf;

    if-eqz v1, :cond_3

    .line 78
    iget-object v2, p0, Lbue;->c:[Lbuf;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 79
    if-eqz v4, :cond_2

    .line 80
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 78
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 84
    :cond_3
    iget v1, p0, Lbue;->f:I

    if-eqz v1, :cond_4

    .line 85
    const/4 v1, 0x4

    iget v2, p0, Lbue;->f:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 87
    :cond_4
    iget-object v1, p0, Lbue;->g:Lbun;

    if-eqz v1, :cond_5

    .line 88
    const/4 v1, 0x5

    iget-object v2, p0, Lbue;->g:Lbun;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 90
    :cond_5
    iget-object v1, p0, Lbue;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 91
    const/4 v1, 0x6

    iget-object v2, p0, Lbue;->h:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 93
    :cond_6
    iget-object v1, p0, Lbue;->i:[Lbty;

    if-eqz v1, :cond_8

    .line 94
    iget-object v2, p0, Lbue;->i:[Lbty;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 95
    if-eqz v4, :cond_7

    .line 96
    const/4 v5, 0x7

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 94
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 100
    :cond_8
    iget-object v1, p0, Lbue;->j:[Lbut;

    if-eqz v1, :cond_a

    .line 101
    iget-object v2, p0, Lbue;->j:[Lbut;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_a

    aget-object v4, v2, v1

    .line 102
    if-eqz v4, :cond_9

    .line 103
    const/16 v5, 0x8

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 101
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 107
    :cond_a
    iget-object v1, p0, Lbue;->k:Lbua;

    if-eqz v1, :cond_b

    .line 108
    const/16 v1, 0x9

    iget-object v2, p0, Lbue;->k:Lbua;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 110
    :cond_b
    iget-boolean v1, p0, Lbue;->l:Z

    if-eqz v1, :cond_c

    .line 111
    const/16 v1, 0xa

    iget-boolean v2, p0, Lbue;->l:Z

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 113
    :cond_c
    iget-boolean v1, p0, Lbue;->m:Z

    if-eqz v1, :cond_d

    .line 114
    const/16 v1, 0xb

    iget-boolean v2, p0, Lbue;->m:Z

    invoke-virtual {p1, v1, v2}, Loxo;->a(IZ)V

    .line 116
    :cond_d
    iget-object v1, p0, Lbue;->d:[Lbuf;

    if-eqz v1, :cond_f

    .line 117
    iget-object v2, p0, Lbue;->d:[Lbuf;

    array-length v3, v2

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_f

    aget-object v4, v2, v1

    .line 118
    if-eqz v4, :cond_e

    .line 119
    const/16 v5, 0xc

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 117
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 123
    :cond_f
    iget-object v1, p0, Lbue;->e:[Lbuf;

    if-eqz v1, :cond_11

    .line 124
    iget-object v1, p0, Lbue;->e:[Lbuf;

    array-length v2, v1

    :goto_4
    if-ge v0, v2, :cond_11

    aget-object v3, v1, v0

    .line 125
    if-eqz v3, :cond_10

    .line 126
    const/16 v4, 0xd

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 124
    :cond_10
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 130
    :cond_11
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lbue;->a(Loxn;)Lbue;

    move-result-object v0

    return-object v0
.end method
