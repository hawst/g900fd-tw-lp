.class public final Lccz;
.super Lccv;
.source "PG"


# instance fields
.field final b:Lcdd;

.field private final c:Landroid/graphics/RectF;

.field private final d:Landroid/graphics/RectF;

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcgn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcgn",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private g:Z

.field private h:Z

.field private i:Z


# direct methods
.method public constructor <init>(Lcci;)V
    .locals 2

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lccv;-><init>(Lcci;)V

    .line 68
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lccz;->c:Landroid/graphics/RectF;

    .line 74
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lccz;->d:Landroid/graphics/RectF;

    .line 77
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lccz;->e:Ljava/util/List;

    .line 80
    new-instance v0, Lcgn;

    new-instance v1, Lcda;

    invoke-direct {v1, p0}, Lcda;-><init>(Lccz;)V

    invoke-direct {v0, v1}, Lcgn;-><init>(Lcgo;)V

    iput-object v0, p0, Lccz;->f:Lcgn;

    .line 98
    new-instance v0, Lcdb;

    invoke-direct {v0, p0}, Lcdb;-><init>(Lccz;)V

    iput-object v0, p0, Lccz;->b:Lcdd;

    .line 111
    return-void
.end method

.method private static a(Landroid/view/View;Landroid/graphics/RectF;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 220
    instance-of v0, p0, Lcdc;

    if-eqz v0, :cond_0

    .line 221
    check-cast p0, Lcdc;

    invoke-interface {p0, p1}, Lcdc;->a(Landroid/graphics/RectF;)V

    .line 225
    :goto_0
    return-void

    .line 223
    :cond_0
    invoke-virtual {p1, v1, v1, v1, v1}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0
.end method


# virtual methods
.method a()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 149
    iget-object v0, p0, Lccz;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v6

    move v4, v3

    move v5, v2

    .line 154
    :goto_0
    if-ge v4, v6, :cond_3

    .line 155
    iget-object v0, p0, Lccz;->e:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 156
    iget-object v1, p0, Lccz;->c:Landroid/graphics/RectF;

    invoke-static {v0, v1}, Lccz;->a(Landroid/view/View;Landroid/graphics/RectF;)V

    .line 157
    iget-boolean v1, p0, Lccz;->g:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lccz;->c:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    :goto_1
    add-float/2addr v1, v5

    .line 158
    iget-boolean v5, p0, Lccz;->g:Z

    if-eqz v5, :cond_1

    .line 159
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    .line 163
    :goto_2
    iget-boolean v0, p0, Lccz;->g:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lccz;->c:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    :goto_3
    add-float/2addr v1, v0

    .line 154
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move v5, v1

    goto :goto_0

    .line 157
    :cond_0
    iget-object v1, p0, Lccz;->c:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    goto :goto_1

    .line 161
    :cond_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_2

    .line 163
    :cond_2
    iget-object v0, p0, Lccz;->c:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    goto :goto_3

    .line 167
    :cond_3
    iget-boolean v0, p0, Lccz;->i:Z

    if-eqz v0, :cond_b

    .line 169
    iget-object v0, p0, Lccz;->e:Ljava/util/List;

    add-int/lit8 v1, v6, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 170
    iget-object v1, p0, Lccz;->c:Landroid/graphics/RectF;

    invoke-static {v0, v1}, Lccz;->a(Landroid/view/View;Landroid/graphics/RectF;)V

    .line 171
    iget-boolean v1, p0, Lccz;->g:Z

    if-eqz v1, :cond_4

    .line 172
    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, v1

    iget-object v1, p0, Lccz;->c:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->right:F

    add-float/2addr v0, v1

    .line 173
    :goto_4
    add-float v1, v2, v0

    .line 174
    iget-boolean v0, p0, Lccz;->g:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lccz;->d:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    .line 175
    :goto_5
    cmpg-float v4, v1, v0

    if-gez v4, :cond_b

    .line 176
    sub-float/2addr v0, v1

    add-float/2addr v0, v2

    move v1, v0

    .line 179
    :goto_6
    iget-boolean v0, p0, Lccz;->h:Z

    if-eqz v0, :cond_a

    .line 181
    iget-object v0, p0, Lccz;->e:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 182
    iget-object v4, p0, Lccz;->c:Landroid/graphics/RectF;

    invoke-static {v0, v4}, Lccz;->a(Landroid/view/View;Landroid/graphics/RectF;)V

    .line 183
    iget-boolean v4, p0, Lccz;->g:Z

    if-eqz v4, :cond_6

    .line 184
    invoke-virtual {v0}, Landroid/view/View;->getX()F

    move-result v0

    iget-object v4, p0, Lccz;->c:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v4

    .line 185
    :goto_7
    add-float v4, v1, v0

    .line 186
    iget-boolean v0, p0, Lccz;->g:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lccz;->d:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    .line 187
    :goto_8
    cmpl-float v5, v4, v0

    if-lez v5, :cond_a

    .line 188
    sub-float v0, v4, v0

    sub-float/2addr v1, v0

    move v4, v1

    .line 191
    :goto_9
    cmpl-float v0, v4, v2

    if-eqz v0, :cond_9

    .line 193
    iget-object v2, p0, Lccz;->e:Ljava/util/List;

    iget-boolean v5, p0, Lccz;->g:Z

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v6

    move v1, v3

    :goto_a
    if-ge v1, v6, :cond_9

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v5, :cond_8

    invoke-virtual {v0}, Landroid/view/View;->getTranslationX()F

    move-result v3

    add-float/2addr v3, v4

    invoke-virtual {v0, v3}, Landroid/view/View;->setTranslationX(F)V

    :goto_b
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 173
    :cond_4
    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v1

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    int-to-float v0, v0

    add-float/2addr v0, v1

    iget-object v1, p0, Lccz;->c:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, v1

    goto :goto_4

    .line 174
    :cond_5
    iget-object v0, p0, Lccz;->d:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->bottom:F

    goto :goto_5

    .line 185
    :cond_6
    invoke-virtual {v0}, Landroid/view/View;->getY()F

    move-result v0

    iget-object v4, p0, Lccz;->c:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    sub-float/2addr v0, v4

    goto :goto_7

    .line 186
    :cond_7
    iget-object v0, p0, Lccz;->d:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->top:F

    goto :goto_8

    .line 193
    :cond_8
    invoke-virtual {v0}, Landroid/view/View;->getTranslationY()F

    move-result v3

    add-float/2addr v3, v4

    invoke-virtual {v0, v3}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_b

    .line 195
    :cond_9
    return-void

    :cond_a
    move v4, v1

    goto :goto_9

    :cond_b
    move v1, v2

    goto/16 :goto_6
.end method

.method public a(Lccj;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 115
    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 116
    iget-boolean v0, p1, Lccj;->a:Z

    iput-boolean v0, p0, Lccz;->g:Z

    .line 120
    if-lez v3, :cond_0

    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lccz;->h:Z

    .line 121
    if-lez v3, :cond_1

    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    add-int/lit8 v4, v3, -0x1

    .line 122
    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    iget-object v4, p1, Lccj;->h:Landroid/widget/Adapter;

    invoke-interface {v4}, Landroid/widget/Adapter;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v0, v4, :cond_1

    :goto_1
    iput-boolean v1, p0, Lccz;->i:Z

    .line 125
    iget-object v0, p0, Lccz;->d:Landroid/graphics/RectF;

    iget-object v1, p1, Lccj;->d:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 126
    iget-boolean v0, p1, Lccj;->a:Z

    if-eqz v0, :cond_2

    .line 127
    iget-object v0, p0, Lccz;->d:Landroid/graphics/RectF;

    iget v1, p1, Lccj;->i:I

    int-to-float v1, v1

    invoke-virtual {v0, v1, v5}, Landroid/graphics/RectF;->offset(FF)V

    .line 133
    :goto_2
    iget-object v0, p0, Lccz;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 134
    :goto_3
    if-ge v2, v3, :cond_3

    .line 135
    iget-object v0, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    .line 136
    iget-object v1, p0, Lccz;->e:Ljava/util/List;

    iget-object v0, v0, Lccg;->a:Landroid/view/View;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_0
    move v0, v2

    .line 120
    goto :goto_0

    :cond_1
    move v1, v2

    .line 122
    goto :goto_1

    .line 129
    :cond_2
    iget-object v0, p0, Lccz;->d:Landroid/graphics/RectF;

    iget v1, p1, Lccj;->i:I

    int-to-float v1, v1

    invoke-virtual {v0, v5, v1}, Landroid/graphics/RectF;->offset(FF)V

    goto :goto_2

    .line 139
    :cond_3
    iget-object v0, p0, Lccz;->f:Lcgn;

    iget-object v1, p0, Lccz;->e:Ljava/util/List;

    invoke-virtual {v0, v1}, Lcgn;->a(Ljava/util/Collection;)V

    .line 141
    invoke-virtual {p0}, Lccz;->a()V

    .line 142
    return-void
.end method
