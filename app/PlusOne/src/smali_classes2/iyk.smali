.class public final Liyk;
.super Lloj;
.source "PG"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private Q:Landroid/view/LayoutInflater;

.field private R:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Liyl;",
            ">;"
        }
    .end annotation
.end field

.field private S:Liwa;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Lloj;-><init>()V

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Liyk;->R:Ljava/util/List;

    .line 49
    const/4 v0, 0x0

    const v1, 0x7f090066

    invoke-virtual {p0, v0, v1}, Liyk;->a(II)V

    .line 50
    return-void
.end method

.method static synthetic a(Liyk;)Landroid/view/LayoutInflater;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Liyk;->Q:Landroid/view/LayoutInflater;

    return-object v0
.end method

.method public static b(Lae;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 41
    new-instance v0, Liyk;

    invoke-direct {v0}, Liyk;-><init>()V

    .line 42
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 43
    const-string v2, "account_name"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    invoke-virtual {v0, v1}, Liyk;->f(Landroid/os/Bundle;)V

    .line 45
    const-string v1, "oob_select_page"

    invoke-virtual {v0, p0, v1}, Liyk;->a(Lae;Ljava/lang/String;)V

    .line 46
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 99
    iget-object v0, p0, Liyk;->Q:Landroid/view/LayoutInflater;

    const v1, 0x7f0401d1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 100
    const v0, 0x102000a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 101
    new-instance v2, Liym;

    iget-object v3, p0, Liyk;->R:Ljava/util/List;

    invoke-direct {v2, p0, v3}, Liym;-><init>(Liyk;Ljava/util/List;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 102
    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 103
    return-object v1
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-super {p0, p1}, Lloj;->a(Landroid/os/Bundle;)V

    .line 62
    invoke-virtual {p0}, Liyk;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "account_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 63
    invoke-virtual {p0}, Liyk;->n()Lz;

    move-result-object v0

    const-class v3, Lhei;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 64
    invoke-interface {v0, v2}, Lhei;->a(Ljava/lang/String;)I

    move-result v2

    .line 65
    invoke-interface {v0, v2}, Lhei;->a(I)Lhej;

    move-result-object v2

    move v0, v1

    .line 67
    :goto_0
    const-string v3, "page_count"

    invoke-interface {v2, v3, v1}, Lhej;->a(Ljava/lang/String;I)I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 68
    const-string v3, "page"

    .line 69
    invoke-interface {v2, v3}, Lhej;->d(Ljava/lang/String;)Lhej;

    move-result-object v3

    .line 70
    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lhej;->d(Ljava/lang/String;)Lhej;

    move-result-object v3

    .line 71
    new-instance v4, Liyl;

    invoke-direct {v4}, Liyl;-><init>()V

    .line 72
    const-string v5, "gaia_id"

    invoke-interface {v3, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Liyl;->a:Ljava/lang/String;

    .line 73
    const-string v5, "display_name"

    invoke-interface {v3, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Liyl;->b:Ljava/lang/String;

    .line 74
    const-string v5, "profile_photo_url"

    invoke-interface {v3, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Liyl;->c:Ljava/lang/String;

    .line 75
    iget-object v3, p0, Liyk;->R:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 77
    :cond_0
    return-void
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 85
    invoke-super {p0, p1}, Lloj;->c(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    .line 86
    const v1, 0x7f0a01a9

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setTitle(I)V

    .line 87
    return-object v0
.end method

.method c(I)V
    .locals 3

    .prologue
    .line 112
    iget-object v0, p0, Liyk;->R:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liyl;

    .line 113
    invoke-virtual {p0}, Liyk;->k()Landroid/os/Bundle;

    move-result-object v1

    .line 114
    const-string v2, "account_name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 115
    iget-object v0, v0, Liyl;->a:Ljava/lang/String;

    .line 116
    iget-object v2, p0, Liyk;->S:Liwa;

    invoke-interface {v2, v1, v0}, Liwa;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    invoke-virtual {p0}, Liyk;->b()V

    .line 118
    return-void
.end method

.method protected k(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 92
    invoke-super {p0, p1}, Lloj;->k(Landroid/os/Bundle;)V

    .line 93
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Liyk;->n()Lz;

    move-result-object v1

    invoke-virtual {p0}, Liyk;->j_()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Liyk;->Q:Landroid/view/LayoutInflater;

    .line 94
    iget-object v0, p0, Liyk;->O:Llnh;

    const-class v1, Liwa;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwa;

    iput-object v0, p0, Liyk;->S:Liwa;

    .line 95
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 122
    invoke-super {p0, p1}, Lloj;->onCancel(Landroid/content/DialogInterface;)V

    .line 123
    iget-object v0, p0, Liyk;->S:Liwa;

    invoke-interface {v0}, Liwa;->c()V

    .line 124
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 108
    invoke-virtual {p0, p3}, Liyk;->c(I)V

    .line 109
    return-void
.end method
