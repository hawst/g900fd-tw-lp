.class public final Lhdq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:I

.field b:Ljava/lang/Runnable;

.field final c:Landroid/graphics/PointF;

.field private d:Z

.field private final e:[I

.field private final f:[Landroid/graphics/PointF;

.field private g:I

.field private h:J

.field private i:F

.field private j:Z

.field private final k:Ljava/util/Deque;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Deque",
            "<",
            "Lhdu;",
            ">;"
        }
    .end annotation
.end field

.field private l:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lhdq;->e:[I

    .line 38
    new-array v0, v1, [Landroid/graphics/PointF;

    new-instance v1, Landroid/graphics/PointF;

    invoke-direct {v1}, Landroid/graphics/PointF;-><init>()V

    aput-object v1, v0, v3

    const/4 v1, 0x1

    new-instance v2, Landroid/graphics/PointF;

    invoke-direct {v2}, Landroid/graphics/PointF;-><init>()V

    aput-object v2, v0, v1

    iput-object v0, p0, Lhdq;->f:[Landroid/graphics/PointF;

    .line 39
    iput v3, p0, Lhdq;->g:I

    .line 40
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhdq;->h:J

    .line 42
    const/high16 v0, 0x40a00000    # 5.0f

    iput v0, p0, Lhdq;->i:F

    .line 46
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lhdq;->k:Ljava/util/Deque;

    .line 232
    iput v3, p0, Lhdq;->a:I

    .line 234
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lhdq;->c:Landroid/graphics/PointF;

    .line 51
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lhdq;->l:Landroid/os/Handler;

    .line 52
    return-void

    .line 37
    nop

    :array_0
    .array-data 4
        -0x1
        -0x1
    .end array-data
.end method

.method private static a(Landroid/graphics/PointF;Landroid/graphics/PointF;)F
    .locals 4

    .prologue
    .line 357
    iget v0, p1, Landroid/graphics/PointF;->y:F

    iget v1, p0, Landroid/graphics/PointF;->y:F

    sub-float/2addr v0, v1

    float-to-double v0, v0

    iget v2, p1, Landroid/graphics/PointF;->x:F

    iget v3, p0, Landroid/graphics/PointF;->x:F

    sub-float/2addr v2, v3

    float-to-double v2, v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->atan2(DD)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private a(IIFF)V
    .locals 1

    .prologue
    .line 348
    if-ltz p1, :cond_0

    const/4 v0, 0x1

    if-le p1, v0, :cond_1

    .line 349
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0

    .line 352
    :cond_1
    iget-object v0, p0, Lhdq;->e:[I

    aput p2, v0, p1

    .line 353
    iget-object v0, p0, Lhdq;->f:[Landroid/graphics/PointF;

    aget-object v0, v0, p1

    invoke-virtual {v0, p3, p4}, Landroid/graphics/PointF;->set(FF)V

    .line 354
    return-void
.end method

.method private a(I[Landroid/graphics/PointF;)V
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 304
    iget-object v0, p0, Lhdq;->k:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->descendingIterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    .line 306
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 307
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdu;

    .line 308
    invoke-virtual {v0}, Lhdu;->l()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 309
    sget-object v5, Lhds;->a:[I

    add-int/lit8 v6, p1, -0x1

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    move v0, v1

    .line 335
    :goto_1
    if-eqz v0, :cond_6

    .line 342
    :cond_1
    return-void

    .line 314
    :pswitch_0
    aget-object v1, p2, v2

    iget v1, v1, Landroid/graphics/PointF;->x:F

    aget-object v1, p2, v2

    iget v1, v1, Landroid/graphics/PointF;->y:F

    aget-object v1, p2, v3

    iget v1, v1, Landroid/graphics/PointF;->x:F

    aget-object v1, p2, v3

    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 315
    invoke-virtual {v0}, Lhdu;->m()Z

    move-result v1

    if-nez v1, :cond_2

    aget-object v1, p2, v2

    iget v1, v1, Landroid/graphics/PointF;->x:F

    aget-object v1, p2, v3

    iget v1, v1, Landroid/graphics/PointF;->x:F

    aget-object v1, p2, v2

    iget v1, v1, Landroid/graphics/PointF;->y:F

    aget-object v1, p2, v3

    iget v1, v1, Landroid/graphics/PointF;->y:F

    aget-object v1, p2, v2

    iget v1, v1, Landroid/graphics/PointF;->x:F

    aget-object v5, p2, v2

    iget v5, v5, Landroid/graphics/PointF;->y:F

    aget-object v6, p2, v3

    iget v6, v6, Landroid/graphics/PointF;->x:F

    aget-object v7, p2, v3

    iget v7, v7, Landroid/graphics/PointF;->y:F

    .line 319
    invoke-static {v1, v5, v6, v7}, Lhdo;->a(FFFF)F

    move-result v1

    aget-object v5, p2, v2

    aget-object v6, p2, v3

    .line 320
    invoke-static {v5, v6}, Lhdq;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v5

    .line 316
    invoke-virtual {v0, v1, v5}, Lhdu;->e(FF)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v3

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_1

    .line 324
    :pswitch_1
    aget-object v1, p2, v2

    iget v1, v1, Landroid/graphics/PointF;->x:F

    aget-object v1, p2, v2

    iget v1, v1, Landroid/graphics/PointF;->y:F

    aget-object v1, p2, v3

    iget v1, v1, Landroid/graphics/PointF;->x:F

    aget-object v1, p2, v3

    iget v1, v1, Landroid/graphics/PointF;->y:F

    .line 325
    invoke-virtual {v0}, Lhdu;->n()Z

    move-result v1

    if-nez v1, :cond_4

    aget-object v1, p2, v2

    iget v1, v1, Landroid/graphics/PointF;->x:F

    aget-object v1, p2, v3

    iget v1, v1, Landroid/graphics/PointF;->x:F

    aget-object v1, p2, v2

    iget v1, v1, Landroid/graphics/PointF;->y:F

    aget-object v1, p2, v3

    iget v1, v1, Landroid/graphics/PointF;->y:F

    aget-object v1, p2, v2

    iget v1, v1, Landroid/graphics/PointF;->x:F

    aget-object v5, p2, v2

    iget v5, v5, Landroid/graphics/PointF;->y:F

    aget-object v6, p2, v3

    iget v6, v6, Landroid/graphics/PointF;->x:F

    aget-object v7, p2, v3

    iget v7, v7, Landroid/graphics/PointF;->y:F

    .line 329
    invoke-static {v1, v5, v6, v7}, Lhdo;->a(FFFF)F

    move-result v1

    aget-object v5, p2, v2

    aget-object v6, p2, v3

    .line 330
    invoke-static {v5, v6}, Lhdq;->a(Landroid/graphics/PointF;Landroid/graphics/PointF;)F

    move-result v5

    .line 326
    invoke-virtual {v0, v1, v5}, Lhdu;->f(FF)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_4
    move v0, v3

    goto/16 :goto_1

    :cond_5
    move v0, v2

    goto/16 :goto_1

    .line 334
    :pswitch_2
    invoke-virtual {v0}, Lhdu;->h()Z

    move-result v0

    goto/16 :goto_1

    :cond_6
    move v1, v0

    goto/16 :goto_0

    .line 309
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method a(IFF)V
    .locals 5

    .prologue
    .line 263
    iget-object v0, p0, Lhdq;->k:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->descendingIterator()Ljava/util/Iterator;

    move-result-object v2

    .line 264
    const/4 v0, 0x0

    move v1, v0

    .line 265
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 266
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhdu;

    .line 267
    invoke-virtual {v0}, Lhdu;->l()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 268
    sget-object v3, Lhds;->a:[I

    add-int/lit8 v4, p1, -0x1

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    move v0, v1

    .line 290
    :goto_1
    if-eqz v0, :cond_2

    .line 301
    :cond_1
    return-void

    .line 273
    :pswitch_0
    invoke-virtual {v0, p2, p3}, Lhdu;->a(FF)Z

    move-result v0

    goto :goto_1

    .line 277
    :pswitch_1
    invoke-virtual {v0, p2, p3}, Lhdu;->b(FF)Z

    move-result v0

    goto :goto_1

    .line 281
    :pswitch_2
    invoke-virtual {v0, p2, p3}, Lhdu;->c(FF)Z

    move-result v0

    goto :goto_1

    .line 285
    :pswitch_3
    invoke-virtual {v0, p2, p3}, Lhdu;->d(FF)Z

    move-result v0

    goto :goto_1

    .line 289
    :pswitch_4
    invoke-virtual {v0}, Lhdu;->g()Z

    move-result v0

    goto :goto_1

    :cond_2
    move v1, v0

    .line 300
    goto :goto_0

    .line 268
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public a(Lhdu;)V
    .locals 2

    .prologue
    .line 72
    if-nez p1, :cond_0

    .line 73
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 76
    :cond_0
    iget-object v0, p0, Lhdq;->k:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 77
    iget-object v0, p0, Lhdq;->k:Ljava/util/Deque;

    invoke-interface {v0, p1}, Ljava/util/Deque;->add(Ljava/lang/Object;)Z

    .line 79
    :cond_1
    return-void
.end method

.method public a(Landroid/view/MotionEvent;)Z
    .locals 9

    .prologue
    const/4 v4, -0x1

    const/4 v2, 0x0

    const/4 v8, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 90
    iget-object v0, p0, Lhdq;->k:Ljava/util/Deque;

    invoke-interface {v0}, Ljava/util/Deque;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    :goto_0
    return v1

    .line 94
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    move v1, v3

    .line 114
    goto :goto_0

    .line 96
    :pswitch_1
    iput-boolean v1, p0, Lhdq;->d:Z

    iput v3, p0, Lhdq;->g:I

    iput-boolean v3, p0, Lhdq;->j:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, p0, Lhdq;->h:J

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-direct {p0, v1, v0, v2, v4}, Lhdq;->a(IIFF)V

    goto :goto_1

    .line 101
    :pswitch_2
    iget-boolean v0, p0, Lhdq;->j:Z

    if-eqz v0, :cond_5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lhdq;->h:J

    sub-long/2addr v4, v6

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v0

    int-to-long v6, v0

    cmp-long v0, v4, v6

    if-gtz v0, :cond_4

    iget-object v0, p0, Lhdq;->f:[Landroid/graphics/PointF;

    aget-object v0, v0, v1

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lhdq;->f:[Landroid/graphics/PointF;

    aget-object v2, v2, v1

    iget v2, v2, Landroid/graphics/PointF;->y:F

    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lhdq;->b:Ljava/lang/Runnable;

    if-nez v4, :cond_3

    const/4 v4, 0x1

    iput v4, p0, Lhdq;->a:I

    iget-object v4, p0, Lhdq;->c:Landroid/graphics/PointF;

    invoke-virtual {v4, v0, v2}, Landroid/graphics/PointF;->set(FF)V

    :goto_2
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v0, Lhdr;

    invoke-direct {v0, p0}, Lhdr;-><init>(Lhdq;)V

    iput-object v0, p0, Lhdq;->b:Ljava/lang/Runnable;

    iget-object v0, p0, Lhdq;->l:Landroid/os/Handler;

    iget-object v2, p0, Lhdq;->b:Ljava/lang/Runnable;

    invoke-static {}, Landroid/view/ViewConfiguration;->getDoubleTapTimeout()I

    move-result v4

    int-to-long v4, v4

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_2
    :goto_3
    iput-boolean v1, p0, Lhdq;->d:Z

    iput-boolean v1, p0, Lhdq;->j:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lhdq;->h:J

    goto :goto_1

    :cond_3
    :try_start_1
    iget-object v0, p0, Lhdq;->l:Landroid/os/Handler;

    iget-object v2, p0, Lhdq;->b:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget v0, p0, Lhdq;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhdq;->a:I

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_4
    iget-object v0, p0, Lhdq;->f:[Landroid/graphics/PointF;

    aget-object v0, v0, v1

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lhdq;->f:[Landroid/graphics/PointF;

    aget-object v2, v2, v1

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {p0, v8, v0, v2}, Lhdq;->a(IFF)V

    goto :goto_3

    :cond_5
    iget-boolean v0, p0, Lhdq;->d:Z

    if-nez v0, :cond_2

    const/4 v0, 0x5

    iget-object v2, p0, Lhdq;->f:[Landroid/graphics/PointF;

    aget-object v2, v2, v1

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lhdq;->f:[Landroid/graphics/PointF;

    aget-object v4, v4, v1

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {p0, v0, v2, v4}, Lhdq;->a(IFF)V

    goto :goto_3

    .line 105
    :pswitch_3
    iget-boolean v0, p0, Lhdq;->d:Z

    if-nez v0, :cond_1

    iget v0, p0, Lhdq;->g:I

    if-ne v0, v3, :cond_7

    iget-object v0, p0, Lhdq;->e:[I

    aget v0, v0, v1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iget-object v4, p0, Lhdq;->f:[Landroid/graphics/PointF;

    aget-object v4, v4, v1

    iget v4, v4, Landroid/graphics/PointF;->x:F

    sub-float/2addr v4, v2

    float-to-double v4, v4

    iget-object v6, p0, Lhdq;->f:[Landroid/graphics/PointF;

    aget-object v6, v6, v1

    iget v6, v6, Landroid/graphics/PointF;->y:F

    sub-float/2addr v6, v0

    float-to-double v6, v6

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v4

    double-to-float v4, v4

    iget-boolean v5, p0, Lhdq;->j:Z

    if-eqz v5, :cond_6

    iget v5, p0, Lhdq;->i:F

    cmpl-float v4, v4, v5

    if-ltz v4, :cond_6

    iput-boolean v1, p0, Lhdq;->j:Z

    const/4 v4, 0x3

    iget-object v5, p0, Lhdq;->f:[Landroid/graphics/PointF;

    aget-object v5, v5, v1

    iget v5, v5, Landroid/graphics/PointF;->x:F

    iget-object v6, p0, Lhdq;->f:[Landroid/graphics/PointF;

    aget-object v6, v6, v1

    iget v6, v6, Landroid/graphics/PointF;->y:F

    invoke-virtual {p0, v4, v5, v6}, Lhdq;->a(IFF)V

    :cond_6
    iget-boolean v4, p0, Lhdq;->j:Z

    if-nez v4, :cond_1

    iget-object v4, p0, Lhdq;->f:[Landroid/graphics/PointF;

    aget-object v4, v4, v1

    invoke-virtual {v4, v2, v0}, Landroid/graphics/PointF;->set(FF)V

    const/4 v0, 0x4

    iget-object v2, p0, Lhdq;->f:[Landroid/graphics/PointF;

    aget-object v2, v2, v1

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lhdq;->f:[Landroid/graphics/PointF;

    aget-object v1, v4, v1

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-virtual {p0, v0, v2, v1}, Lhdq;->a(IFF)V

    goto/16 :goto_1

    :cond_7
    move v0, v1

    :goto_4
    iget-object v2, p0, Lhdq;->e:[I

    if-ge v0, v8, :cond_b

    iget-object v2, p0, Lhdq;->e:[I

    aget v5, v2, v0

    move v2, v1

    :goto_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v6

    if-ge v2, v6, :cond_a

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v6

    if-ne v6, v5, :cond_9

    :goto_6
    if-eq v2, v4, :cond_8

    iget-object v5, p0, Lhdq;->f:[Landroid/graphics/PointF;

    aget-object v5, v5, v0

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v6

    invoke-virtual {p1, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-virtual {v5, v6, v2}, Landroid/graphics/PointF;->set(FF)V

    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_a
    move v2, v4

    goto :goto_6

    :cond_b
    const/4 v0, 0x7

    iget-object v1, p0, Lhdq;->f:[Landroid/graphics/PointF;

    invoke-direct {p0, v0, v1}, Lhdq;->a(I[Landroid/graphics/PointF;)V

    goto/16 :goto_1

    .line 109
    :pswitch_4
    iget v0, p0, Lhdq;->g:I

    if-ne v0, v3, :cond_1

    iput-boolean v1, p0, Lhdq;->j:Z

    const/4 v0, 0x5

    invoke-virtual {p0, v0, v2, v2}, Lhdq;->a(IFF)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v1

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    invoke-direct {p0, v3, v1, v2, v0}, Lhdq;->a(IIFF)V

    iput v8, p0, Lhdq;->g:I

    const/4 v0, 0x6

    iget-object v1, p0, Lhdq;->f:[Landroid/graphics/PointF;

    invoke-direct {p0, v0, v1}, Lhdq;->a(I[Landroid/graphics/PointF;)V

    goto/16 :goto_1

    .line 113
    :pswitch_5
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v2

    if-gt v2, v8, :cond_d

    const/16 v2, 0x8

    const/4 v4, 0x0

    invoke-direct {p0, v2, v4}, Lhdq;->a(I[Landroid/graphics/PointF;)V

    iput-boolean v3, p0, Lhdq;->d:Z

    iget-object v2, p0, Lhdq;->e:[I

    aget v2, v2, v1

    if-ne v0, v2, :cond_c

    iget-object v0, p0, Lhdq;->e:[I

    aget v0, v0, v3

    iget-object v2, p0, Lhdq;->f:[Landroid/graphics/PointF;

    aget-object v2, v2, v3

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lhdq;->f:[Landroid/graphics/PointF;

    aget-object v4, v4, v3

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-direct {p0, v1, v0, v2, v4}, Lhdq;->a(IIFF)V

    :cond_c
    iput v3, p0, Lhdq;->g:I

    goto/16 :goto_1

    :cond_d
    iget-object v2, p0, Lhdq;->e:[I

    aget v2, v2, v1

    if-eq v0, v2, :cond_e

    iget-object v2, p0, Lhdq;->e:[I

    aget v2, v2, v3

    if-ne v0, v2, :cond_1

    :cond_e
    const/16 v0, 0x8

    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lhdq;->a(I[Landroid/graphics/PointF;)V

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getX(I)F

    move-result v2

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getY(I)F

    move-result v4

    invoke-direct {p0, v1, v0, v2, v4}, Lhdq;->a(IIFF)V

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    invoke-virtual {p1, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    invoke-direct {p0, v3, v0, v1, v2}, Lhdq;->a(IIFF)V

    const/4 v0, 0x6

    iget-object v1, p0, Lhdq;->f:[Landroid/graphics/PointF;

    invoke-direct {p0, v0, v1}, Lhdq;->a(I[Landroid/graphics/PointF;)V

    goto/16 :goto_1

    .line 94
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
