.class final Lalb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:F

.field private b:F

.field private c:J

.field private d:Z


# direct methods
.method public constructor <init>(F)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput p1, p0, Lalb;->a:F

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lalb;->d:Z

    .line 79
    return-void
.end method


# virtual methods
.method public a(FJ)F
    .locals 4

    .prologue
    .line 83
    iget-boolean v0, p0, Lalb;->d:Z

    if-eqz v0, :cond_0

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lalb;->d:Z

    .line 85
    iput p1, p0, Lalb;->b:F

    .line 86
    const-wide/16 v0, 0x1

    sub-long v0, p2, v0

    iput-wide v0, p0, Lalb;->c:J

    .line 89
    :cond_0
    iget-wide v0, p0, Lalb;->c:J

    sub-long v0, p2, v0

    long-to-float v0, v0

    .line 90
    iget v1, p0, Lalb;->a:F

    add-float/2addr v1, v0

    div-float/2addr v0, v1

    .line 91
    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v1, v0

    iget v2, p0, Lalb;->b:F

    mul-float/2addr v1, v2

    mul-float/2addr v0, p1

    add-float/2addr v0, v1

    .line 92
    iput-wide p2, p0, Lalb;->c:J

    .line 93
    iput v0, p0, Lalb;->b:F

    .line 94
    return v0
.end method
