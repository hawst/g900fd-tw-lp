.class public final Lgfo;
.super Ljava/lang/Object;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<O::",
        "Lgfp;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Lgfs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgfs",
            "<*TO;>;"
        }
    .end annotation
.end field

.field private final b:Lgft;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgft",
            "<*>;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lggh;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public varargs constructor <init>(Lgfs;Lgft;[Lggh;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<C::",
            "Lgfr;",
            ">(",
            "Lgfs",
            "<TC;TO;>;",
            "Lgft",
            "<TC;>;[",
            "Lggh;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgfo;->a:Lgfs;

    iput-object p2, p0, Lgfo;->b:Lgft;

    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lgfo;->c:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public a()Lgfs;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lgfs",
            "<*TO;>;"
        }
    .end annotation

    iget-object v0, p0, Lgfo;->a:Lgfs;

    return-object v0
.end method

.method public b()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lggh;",
            ">;"
        }
    .end annotation

    iget-object v0, p0, Lgfo;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public c()Lgft;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lgft",
            "<*>;"
        }
    .end annotation

    iget-object v0, p0, Lgfo;->b:Lgft;

    return-object v0
.end method
