.class public final Lbxz;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Lbor;


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbon;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbol;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmw;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbol;",
            ">;"
        }
    .end annotation
.end field

.field private f:J

.field private g:J

.field private h:I

.field private i:Lbor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    new-instance v0, Lbor;

    invoke-direct {v0}, Lbor;-><init>()V

    sput-object v0, Lbxz;->a:Lbor;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    sget-object v0, Lbxz;->a:Lbor;

    iput-object v0, p0, Lbxz;->i:Lbor;

    return-void
.end method


# virtual methods
.method public a()Lbxy;
    .locals 12

    .prologue
    .line 122
    iget-object v0, p0, Lbxz;->b:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbxz;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lbxz;->d:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lbxz;->d:Ljava/util/List;

    .line 123
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    const-string v1, "At least one of mPhotos and mVideos should be non-empty."

    .line 122
    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 125
    iget-wide v0, p0, Lbxz;->f:J

    const-string v2, "mTargetDuration"

    invoke-static {v0, v1, v2}, Lcgp;->a(JLjava/lang/CharSequence;)J

    .line 126
    iget-wide v0, p0, Lbxz;->g:J

    const-string v2, "mSinglePhotoDurationUs"

    invoke-static {v0, v1, v2}, Lcgp;->a(JLjava/lang/CharSequence;)J

    .line 128
    new-instance v1, Lbxy;

    iget-object v2, p0, Lbxz;->b:Ljava/util/List;

    iget-object v3, p0, Lbxz;->c:Ljava/util/Map;

    iget-object v4, p0, Lbxz;->d:Ljava/util/List;

    iget-object v5, p0, Lbxz;->e:Ljava/util/Map;

    iget-wide v6, p0, Lbxz;->f:J

    iget-wide v8, p0, Lbxz;->g:J

    iget v10, p0, Lbxz;->h:I

    iget-object v11, p0, Lbxz;->i:Lbor;

    invoke-direct/range {v1 .. v11}, Lbxy;-><init>(Ljava/util/List;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;JJILbor;)V

    return-object v1

    .line 123
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)Lbxz;
    .locals 0

    .prologue
    .line 112
    iput p1, p0, Lbxz;->h:I

    .line 113
    return-object p0
.end method

.method public a(J)Lbxz;
    .locals 1

    .prologue
    .line 102
    iput-wide p1, p0, Lbxz;->f:J

    .line 103
    return-object p0
.end method

.method public a(Lbor;)Lbxz;
    .locals 2

    .prologue
    .line 117
    const-string v0, "constraints"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbor;

    iput-object v0, p0, Lbxz;->i:Lbor;

    .line 118
    return-object p0
.end method

.method public a(Ljava/util/List;)Lbxz;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbon;",
            ">;)",
            "Lbxz;"
        }
    .end annotation

    .prologue
    .line 82
    iput-object p1, p0, Lbxz;->b:Ljava/util/List;

    .line 83
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbxz;->c:Ljava/util/Map;

    .line 84
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbon;

    .line 85
    iget-object v2, p0, Lbxz;->c:Ljava/util/Map;

    .line 86
    invoke-virtual {v0}, Lbon;->a()Ljeg;

    move-result-object v3

    invoke-virtual {v0}, Lbon;->c()Lbol;

    move-result-object v0

    .line 85
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 88
    :cond_0
    return-object p0
.end method

.method public b(J)Lbxz;
    .locals 1

    .prologue
    .line 107
    iput-wide p1, p0, Lbxz;->g:J

    .line 108
    return-object p0
.end method

.method public b(Ljava/util/List;)Lbxz;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmw;",
            ">;)",
            "Lbxz;"
        }
    .end annotation

    .prologue
    .line 92
    iput-object p1, p0, Lbxz;->d:Ljava/util/List;

    .line 93
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbxz;->e:Ljava/util/Map;

    .line 94
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmw;

    .line 95
    iget-object v2, p0, Lbxz;->e:Ljava/util/Map;

    .line 96
    invoke-virtual {v0}, Lbmw;->a()Ljeg;

    move-result-object v3

    invoke-virtual {v0}, Lbmw;->c()Lbol;

    move-result-object v0

    .line 95
    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 98
    :cond_0
    return-object p0
.end method
