.class public final Lclw;
.super Lcjy;
.source "PG"

# interfaces
.implements Lhcv;
.implements Lhcw;
.implements Lhdb;


# static fields
.field private static final W:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private X:Lhct;

.field private Y:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

.field private Z:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

.field private aa:Landroid/graphics/Bitmap;

.field private ab:I

.field private ac:I

.field private ad:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

.field private ae:I

.field private af:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

.field private ag:I

.field private ah:I

.field private final ai:Landroid/graphics/PointF;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 54
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 55
    new-array v1, v6, [Ljava/lang/Integer;

    .line 57
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    .line 58
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    .line 59
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    const/4 v2, 0x3

    .line 60
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 55
    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 62
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lclw;->W:Ljava/util/List;

    .line 63
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0}, Lcjy;-><init>()V

    .line 328
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lclw;->ai:Landroid/graphics/PointF;

    return-void
.end method

.method static synthetic a(Lclw;)Lhct;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lclw;->X:Lhct;

    return-object v0
.end method

.method private a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;FF)V
    .locals 4

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 335
    iget-object v0, p0, Lclw;->ai:Landroid/graphics/PointF;

    invoke-virtual {v0, p2, p3}, Landroid/graphics/PointF;->set(FF)V

    .line 336
    iget-object v0, p0, Lclw;->ai:Landroid/graphics/PointF;

    iget v1, p0, Lclw;->ae:I

    invoke-static {v0, v1, v2, v2}, Lhbg;->a(Landroid/graphics/PointF;IFF)V

    .line 338
    iget-object v0, p0, Lclw;->ai:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget v1, p0, Lclw;->ab:I

    add-int/lit8 v1, v1, -0x1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    .line 339
    iget-object v1, p0, Lclw;->ai:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    iget v2, p0, Lclw;->ac:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    .line 342
    const/16 v2, 0x1f5

    iget-object v3, p0, Lclw;->ai:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    invoke-virtual {p1, v2, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterFloat(IF)Z

    .line 343
    const/16 v2, 0x1f6

    iget-object v3, p0, Lclw;->ai:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-virtual {p1, v2, v3}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterFloat(IF)Z

    .line 344
    const/16 v2, 0xc9

    iget-object v3, p0, Lclw;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterInteger(II)Z

    .line 345
    return-void
.end method

.method private a(Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 380
    check-cast p1, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 381
    const/16 v1, 0xcb

    if-eqz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v1, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterInteger(II)Z

    .line 383
    return-void

    .line 381
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected X()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 109
    iget-object v0, p0, Lclw;->ad:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    if-nez v0, :cond_0

    .line 110
    invoke-super {p0}, Lcjy;->X()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lclw;->W:Ljava/util/List;

    goto :goto_0
.end method

.method public a(FF)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 167
    const/16 v0, 0x12c

    invoke-static {v0}, Lham;->a(I)Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    .line 168
    invoke-direct {p0, v0, p1, p2}, Lclw;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;FF)V

    .line 170
    invoke-super {p0}, Lcjy;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->addSubParameters(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    .line 172
    invoke-virtual {p0}, Lclw;->ap()V

    .line 173
    invoke-virtual {p0}, Lclw;->ak()V

    .line 174
    return-object v0
.end method

.method protected a(Lckp;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 132
    const v1, 0x7f020384

    const v2, 0x7f020383

    const v0, 0x7f0a0079

    .line 136
    invoke-virtual {p0, v0}, Lclw;->e_(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lclx;

    invoke-direct {v5, p0}, Lclx;-><init>(Lclw;)V

    move-object v0, p1

    .line 132
    invoke-virtual/range {v0 .. v5}, Lckp;->a(IIILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    move-result-object v0

    iput-object v0, p0, Lclw;->Z:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 145
    iget-object v1, p0, Lclw;->Z:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    iget-object v0, p0, Lclw;->ad:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setEnabled(Z)V

    .line 147
    const v1, 0x7f020382

    const v2, 0x7f020381

    const v0, 0x7f0a0078

    .line 151
    invoke-virtual {p0, v0}, Lclw;->e_(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcly;

    invoke-direct {v5, p0}, Lcly;-><init>(Lclw;)V

    move-object v0, p1

    .line 147
    invoke-virtual/range {v0 .. v5}, Lckp;->a(IIILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    move-result-object v0

    iput-object v0, p0, Lclw;->Y:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 158
    return-void

    :cond_0
    move v0, v3

    .line 145
    goto :goto_0
.end method

.method protected a(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V
    .locals 1

    .prologue
    .line 120
    invoke-super {p0, p1}, Lcjy;->a(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V

    .line 122
    invoke-virtual {p0}, Lclw;->ah()Lchp;

    move-result-object v0

    invoke-virtual {v0}, Lchp;->i()Lcom/google/android/libraries/photoeditor/core/FilterChain;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getPostRotation()I

    move-result v0

    iput v0, p0, Lclw;->ae:I

    .line 124
    new-instance v0, Lhct;

    invoke-direct {v0, p1}, Lhct;-><init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V

    iput-object v0, p0, Lclw;->X:Lhct;

    .line 125
    iget-object v0, p0, Lclw;->X:Lhct;

    invoke-virtual {v0, p0}, Lhct;->a(Lhcv;)V

    .line 126
    iget-object v0, p0, Lclw;->X:Lhct;

    invoke-virtual {v0, p0}, Lhct;->a(Lhcw;)V

    .line 127
    iget-object v0, p0, Lclw;->X:Lhct;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a(Lhdx;)I

    .line 128
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 179
    invoke-super {p0}, Lcjy;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    check-cast p1, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)Z

    .line 180
    iget-object v0, p0, Lclw;->Y:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    iget-object v1, p0, Lclw;->X:Lhct;

    invoke-virtual {v1}, Lhct;->d()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setEnabled(Z)V

    .line 181
    invoke-virtual {p0}, Lclw;->ap()V

    .line 182
    return-void
.end method

.method public a(Ljava/lang/Object;FF)V
    .locals 0

    .prologue
    .line 192
    check-cast p1, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 193
    invoke-direct {p0, p1, p2, p3}, Lclw;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;FF)V

    .line 195
    invoke-virtual {p0}, Lclw;->ak()V

    .line 196
    return-void
.end method

.method public a(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 200
    if-eqz p1, :cond_0

    .line 201
    invoke-direct {p0, p1, v1}, Lclw;->a(Ljava/lang/Object;Z)V

    .line 204
    :cond_0
    if-eqz p2, :cond_1

    .line 205
    invoke-direct {p0, p2, v0}, Lclw;->a(Ljava/lang/Object;Z)V

    .line 208
    :cond_1
    check-cast p2, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    iput-object p2, p0, Lclw;->ad:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 211
    iget-object v2, p0, Lclw;->R:Lhcs;

    invoke-virtual {p0}, Lclw;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v3

    invoke-virtual {p0}, Lclw;->X()Ljava/util/List;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lhcs;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Ljava/util/List;)V

    .line 212
    iget-object v2, p0, Lclw;->U:Lhcz;

    iget-object v3, p0, Lclw;->R:Lhcs;

    invoke-virtual {v2, v3}, Lhcz;->a(Lhdc;)V

    .line 217
    iget-object v2, p0, Lclw;->Y:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    iget-object v3, p0, Lclw;->X:Lhct;

    invoke-virtual {v3}, Lhct;->d()Z

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setEnabled(Z)V

    .line 218
    iget-object v2, p0, Lclw;->Z:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    iget-object v3, p0, Lclw;->ad:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    if-eqz v3, :cond_2

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setEnabled(Z)V

    .line 220
    invoke-virtual {p0}, Lclw;->ap()V

    .line 221
    return-void

    :cond_2
    move v0, v1

    .line 218
    goto :goto_0
.end method

.method protected a(Z)V
    .locals 3

    .prologue
    .line 236
    invoke-super {p0}, Lcjy;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    .line 237
    iget-object v1, p0, Lclw;->ad:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    invoke-super {p0}, Lcjy;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getSubParameters()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    iput v1, p0, Lclw;->ag:I

    .line 238
    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->e()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    iput-object v1, p0, Lclw;->af:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 239
    iget-object v1, p0, Lclw;->X:Lhct;

    invoke-virtual {v1}, Lhct;->f()I

    move-result v1

    iput v1, p0, Lclw;->ah:I

    .line 240
    iget v1, p0, Lclw;->ah:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 241
    iget-object v1, p0, Lclw;->X:Lhct;

    invoke-virtual {v1}, Lhct;->b()Z

    .line 243
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->f()Z

    .line 244
    iget-object v0, p0, Lclw;->X:Lhct;

    invoke-virtual {v0}, Lhct;->a()V

    .line 245
    iget-object v0, p0, Lclw;->S:Lckp;

    if-eqz v0, :cond_1

    .line 246
    iget-object v0, p0, Lclw;->S:Lckp;

    invoke-virtual {v0}, Lckp;->c()Z

    .line 248
    :cond_1
    invoke-virtual {p0}, Lclw;->ak()V

    .line 249
    return-void
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 72
    invoke-super {p0}, Lcjy;->aO_()V

    .line 74
    invoke-virtual {p0}, Lclw;->ah()Lchp;

    move-result-object v0

    invoke-virtual {v0}, Lchp;->g()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lclw;->aa:Landroid/graphics/Bitmap;

    .line 75
    iget-object v0, p0, Lclw;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    iput v0, p0, Lclw;->ab:I

    .line 76
    iget-object v0, p0, Lclw;->aa:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    iput v0, p0, Lclw;->ac:I

    .line 78
    iget-object v0, p0, Lclw;->X:Lhct;

    invoke-virtual {v0}, Lhct;->a()V

    .line 79
    return-void
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 67
    new-instance v0, Lhmk;

    sget-object v1, Lonc;->v:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method protected aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lclw;->ad:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    if-nez v0, :cond_0

    invoke-super {p0}, Lcjy;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lclw;->ad:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    goto :goto_0
.end method

.method public at()I
    .locals 1

    .prologue
    .line 349
    invoke-super {p0}, Lcjy;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getSubParameters()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public au()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 393
    const-string v0, ""

    return-object v0
.end method

.method public av()F
    .locals 1

    .prologue
    .line 398
    const/4 v0, 0x0

    return v0
.end method

.method public b(Ljava/lang/Object;)Landroid/graphics/PointF;
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    .line 359
    check-cast p1, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 360
    new-instance v0, Landroid/graphics/PointF;

    const/16 v1, 0x1f5

    .line 361
    invoke-virtual {p1, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v1

    const/16 v2, 0x1f6

    .line 362
    invoke-virtual {p1, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 363
    iget v1, p0, Lclw;->ae:I

    invoke-static {v0, v1, v3, v3}, Lhbg;->b(Landroid/graphics/PointF;IFF)V

    .line 365
    return-object v0
.end method

.method public b(ILjava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 420
    const/16 v0, 0xca

    if-eq p1, v0, :cond_0

    .line 421
    invoke-super {p0, p1, p2}, Lcjy;->b(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, ""

    goto :goto_0
.end method

.method protected b(Z)V
    .locals 2

    .prologue
    .line 253
    invoke-super {p0}, Lcjy;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    iget-object v1, p0, Lclw;->af:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    .line 254
    iget v0, p0, Lclw;->ag:I

    if-ltz v0, :cond_1

    .line 256
    invoke-super {p0}, Lcjy;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getSubParameters()Ljava/util/List;

    move-result-object v0

    iget v1, p0, Lclw;->ag:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    iput-object v0, p0, Lclw;->ad:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 261
    :goto_0
    iget-object v0, p0, Lclw;->X:Lhct;

    iget v1, p0, Lclw;->ah:I

    invoke-virtual {v0, v1}, Lhct;->a(I)Z

    .line 262
    iget-object v0, p0, Lclw;->X:Lhct;

    invoke-virtual {v0}, Lhct;->a()V

    .line 263
    iget-object v0, p0, Lclw;->S:Lckp;

    if-eqz v0, :cond_0

    .line 264
    iget-object v0, p0, Lclw;->S:Lckp;

    invoke-virtual {v0}, Lckp;->c()Z

    .line 267
    :cond_0
    invoke-virtual {p0}, Lclw;->ak()V

    .line 268
    return-void

    .line 258
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lclw;->ad:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    goto :goto_0
.end method

.method public c(Ljava/lang/Object;)F
    .locals 1

    .prologue
    .line 370
    check-cast p1, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 371
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->i(I)F

    move-result v0

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 97
    const/4 v0, 0x3

    return v0
.end method

.method public c(I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 426
    iget-object v0, p0, Lclw;->R:Lhcs;

    invoke-virtual {v0, p1}, Lhcs;->f(I)I

    move-result v0

    .line 427
    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 433
    :goto_0
    return-void

    .line 431
    :cond_0
    iget-object v0, p0, Lclw;->X:Lhct;

    invoke-virtual {v0, v2}, Lhct;->a(Z)V

    .line 432
    const/16 v0, 0xca

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v2}, Lclw;->a(ILjava/lang/Object;Z)Z

    goto :goto_0
.end method

.method public c(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 461
    iget-object v0, p0, Lclw;->ad:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    if-nez v0, :cond_0

    .line 469
    :goto_0
    return-void

    .line 465
    :cond_0
    iget-object v0, p0, Lclw;->R:Lhcs;

    invoke-virtual {v0, p1}, Lhcs;->f(I)I

    move-result v0

    .line 466
    const/4 v1, 0x1

    invoke-virtual {p0, v0, p2, v1}, Lclw;->a(ILjava/lang/Object;Z)Z

    .line 468
    iget-object v0, p0, Lclw;->X:Lhct;

    invoke-virtual {v0}, Lhct;->e()V

    goto :goto_0
.end method

.method public d(I)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 437
    iget-object v0, p0, Lclw;->R:Lhcs;

    invoke-virtual {v0, p1}, Lhcs;->f(I)I

    move-result v0

    .line 438
    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 444
    :goto_0
    return-void

    .line 442
    :cond_0
    iget-object v0, p0, Lclw;->X:Lhct;

    invoke-virtual {v0, v2}, Lhct;->a(Z)V

    .line 443
    const/16 v0, 0xca

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p0, v0, v1, v2}, Lclw;->a(ILjava/lang/Object;Z)Z

    goto :goto_0
.end method

.method public d(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 387
    check-cast p1, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 388
    const/16 v0, 0xcb

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected e()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcmd;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x64

    const/4 v0, 0x0

    const/16 v10, 0x3e8

    const/high16 v9, 0x3f000000    # 0.5f

    const v8, 0x3e99999a    # 0.3f

    .line 272
    invoke-virtual {p0}, Lclw;->n()Lz;

    move-result-object v1

    .line 273
    if-eqz v1, :cond_0

    iget-object v2, p0, Lclw;->P:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lclw;->Y:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    if-nez v2, :cond_1

    .line 325
    :cond_0
    :goto_0
    return-object v0

    .line 277
    :cond_1
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v2, v1}, Lcmh;->a(Landroid/view/Window;Landroid/content/res/Resources;)Landroid/graphics/Rect;

    move-result-object v1

    .line 278
    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 281
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 284
    invoke-virtual {p0}, Lclw;->o()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0049

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 285
    div-int/lit8 v0, v0, 0x2

    .line 287
    iget-object v2, p0, Lclw;->Y:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    invoke-static {v2}, Lcmp;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v2

    .line 288
    new-instance v3, Landroid/graphics/PointF;

    .line 289
    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v4

    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    invoke-direct {v3, v4, v2}, Landroid/graphics/PointF;-><init>(FF)V

    .line 290
    invoke-virtual {p0}, Lclw;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0c0002

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    shl-int/lit8 v2, v2, 0x1

    int-to-long v4, v2

    .line 292
    int-to-float v2, v0

    invoke-static {v10, v2}, Lcmd;->a(IF)Lcme;

    move-result-object v2

    .line 293
    invoke-virtual {v2, v4, v5}, Lcme;->a(J)Lcme;

    move-result-object v4

    int-to-float v5, v0

    mul-float/2addr v5, v8

    invoke-virtual {v4, v5}, Lcme;->a(F)Lcme;

    move-result-object v4

    iget v5, v3, Landroid/graphics/PointF;->x:F

    iget v3, v3, Landroid/graphics/PointF;->y:F

    .line 294
    invoke-virtual {v4, v5, v3}, Lcme;->a(FF)Lcme;

    .line 295
    invoke-virtual {v2, v12, v13}, Lcme;->b(J)Lcmg;

    move-result-object v3

    int-to-float v4, v0

    invoke-virtual {v3, v4}, Lcmg;->a(F)Lcmg;

    move-result-object v3

    invoke-virtual {v3}, Lcmg;->a()Lcme;

    .line 296
    const-wide/16 v4, 0xaf

    invoke-virtual {v2, v4, v5}, Lcme;->b(J)Lcmg;

    move-result-object v3

    int-to-float v4, v0

    mul-float/2addr v4, v8

    invoke-virtual {v3, v4}, Lcmg;->a(F)Lcmg;

    move-result-object v3

    invoke-virtual {v3}, Lcmg;->a()Lcme;

    .line 297
    invoke-virtual {v2}, Lcme;->a()Lcmd;

    move-result-object v2

    .line 298
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 299
    invoke-virtual {v2}, Lcmd;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x514

    add-long/2addr v2, v4

    .line 301
    iget-object v4, p0, Lclw;->P:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-static {v4}, Lcmp;->a(Landroid/view/View;)Landroid/graphics/Rect;

    move-result-object v4

    .line 302
    iget-object v5, p0, Lclw;->P:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v5}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a()Landroid/graphics/Rect;

    move-result-object v5

    .line 303
    iget v6, v4, Landroid/graphics/Rect;->left:I

    iget v4, v4, Landroid/graphics/Rect;->top:I

    invoke-virtual {v5, v6, v4}, Landroid/graphics/Rect;->offset(II)V

    .line 304
    new-instance v4, Landroid/graphics/PointF;

    .line 305
    invoke-virtual {v5}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v6

    iget v7, v5, Landroid/graphics/Rect;->left:I

    int-to-float v7, v7

    add-float/2addr v6, v7

    mul-float/2addr v6, v9

    .line 306
    invoke-virtual {v5}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v7

    iget v5, v5, Landroid/graphics/Rect;->top:I

    int-to-float v5, v5

    add-float/2addr v5, v7

    mul-float/2addr v5, v9

    invoke-direct {v4, v6, v5}, Landroid/graphics/PointF;-><init>(FF)V

    .line 308
    int-to-float v5, v0

    invoke-static {v10, v5}, Lcmd;->a(IF)Lcme;

    move-result-object v5

    .line 309
    invoke-virtual {v5, v2, v3}, Lcme;->a(J)Lcme;

    move-result-object v2

    int-to-float v3, v0

    mul-float/2addr v3, v8

    invoke-virtual {v2, v3}, Lcme;->a(F)Lcme;

    move-result-object v2

    iget v3, v4, Landroid/graphics/PointF;->x:F

    iget v4, v4, Landroid/graphics/PointF;->y:F

    .line 310
    invoke-virtual {v2, v3, v4}, Lcme;->a(FF)Lcme;

    .line 311
    invoke-virtual {v5, v12, v13}, Lcme;->b(J)Lcmg;

    move-result-object v2

    int-to-float v3, v0

    invoke-virtual {v2, v3}, Lcmg;->a(F)Lcmg;

    move-result-object v2

    invoke-virtual {v2}, Lcmg;->a()Lcme;

    .line 312
    const-wide/16 v2, 0xaf

    invoke-virtual {v5, v2, v3}, Lcme;->b(J)Lcmg;

    move-result-object v2

    int-to-float v0, v0

    mul-float/2addr v0, v8

    invoke-virtual {v2, v0}, Lcmg;->a(F)Lcmg;

    move-result-object v0

    invoke-virtual {v0}, Lcmg;->a()Lcme;

    .line 313
    invoke-virtual {v5}, Lcme;->a()Lcmd;

    move-result-object v0

    .line 314
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 316
    invoke-virtual {v0}, Lcmd;->c()J

    move-result-wide v2

    const-wide/16 v4, 0x12c

    add-long/2addr v2, v4

    .line 318
    invoke-super {p0}, Lcjy;->e()Ljava/util/List;

    move-result-object v0

    .line 319
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmd;

    .line 320
    invoke-virtual {v0}, Lcmd;->a()Lcme;

    move-result-object v5

    .line 321
    invoke-virtual {v0}, Lcmd;->b()J

    move-result-wide v6

    add-long/2addr v6, v2

    invoke-virtual {v5, v6, v7}, Lcme;->a(J)Lcme;

    move-result-object v0

    .line 322
    invoke-virtual {v0}, Lcme;->a()Lcmd;

    move-result-object v0

    .line 323
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 325
    goto/16 :goto_0
.end method

.method public e(I)V
    .locals 2

    .prologue
    .line 448
    iget-object v0, p0, Lclw;->ad:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    if-nez v0, :cond_0

    .line 457
    :goto_0
    return-void

    .line 452
    :cond_0
    invoke-virtual {p0}, Lclw;->as()V

    .line 454
    invoke-virtual {p0}, Lclw;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    iget-object v1, p0, Lclw;->R:Lhcs;

    .line 455
    invoke-virtual {v1, p1}, Lhcs;->f(I)I

    move-result v1

    .line 454
    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setActiveParameterKey(I)V

    .line 456
    invoke-virtual {p0}, Lclw;->ap()V

    goto :goto_0
.end method

.method public i(I)V
    .locals 2

    .prologue
    .line 162
    iget-object v1, p0, Lclw;->Y:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setSelected(Z)V

    .line 163
    return-void

    .line 162
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 354
    invoke-super {p0}, Lcjy;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getSubParameters()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public z()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lclw;->aa:Landroid/graphics/Bitmap;

    .line 84
    iput v2, p0, Lclw;->ab:I

    .line 85
    iput v2, p0, Lclw;->ac:I

    .line 87
    iget-object v0, p0, Lclw;->ad:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    if-eqz v0, :cond_0

    .line 89
    const/16 v0, 0xca

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v2}, Lclw;->a(ILjava/lang/Object;Z)Z

    .line 92
    :cond_0
    invoke-super {p0}, Lcjy;->z()V

    .line 93
    return-void
.end method
