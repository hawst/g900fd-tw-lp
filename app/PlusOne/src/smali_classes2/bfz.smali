.class public Lbfz;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lbfz;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbfz;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    return-void
.end method

.method public static a(ILbgn;Lbhd;ILbgs;)V
    .locals 3

    .prologue
    .line 34
    packed-switch p0, :pswitch_data_0

    .line 45
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unhandled draw mode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 36
    :pswitch_0
    const/4 v0, 0x4

    .line 47
    :goto_0
    :try_start_0
    new-instance v1, Lbga;

    invoke-direct {v1, p3, p4, p2, v0}, Lbga;-><init>(ILbgs;Lbhd;I)V

    invoke-interface {p1, v1}, Lbgn;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    :goto_1
    return-void

    .line 39
    :pswitch_1
    const/4 v0, 0x5

    .line 40
    goto :goto_0

    .line 42
    :pswitch_2
    const/4 v0, 0x6

    .line 43
    goto :goto_0

    .line 47
    :catch_0
    move-exception v0

    sget-object v0, Lbfz;->a:Ljava/lang/String;

    goto :goto_1

    .line 34
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lbgn;[F)V
    .locals 1

    .prologue
    .line 106
    :try_start_0
    new-instance v0, Lbgb;

    invoke-direct {v0, p1}, Lbgb;-><init>([F)V

    invoke-interface {p0, v0}, Lbgn;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :goto_0
    return-void

    .line 114
    :catch_0
    move-exception v0

    sget-object v0, Lbfz;->a:Ljava/lang/String;

    goto :goto_0
.end method
