.class public final Ljyu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljyy;


# instance fields
.field private final a:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljyv;",
            ">;"
        }
    .end annotation
.end field

.field private b:Z

.field private c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljyw;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/lang/String;

.field private e:J

.field private final f:Landroid/content/Context;

.field private g:Z


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljyu;->a:Ljava/util/ArrayList;

    .line 34
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    .line 37
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Ljyu;->e:J

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljyu;->g:Z

    .line 43
    iput-object p1, p0, Ljyu;->f:Landroid/content/Context;

    .line 44
    return-void
.end method

.method private d(I)V
    .locals 2

    .prologue
    .line 375
    iget-object v0, p0, Ljyu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyv;

    .line 376
    invoke-interface {v0, p1}, Ljyv;->a(I)V

    goto :goto_0

    .line 378
    :cond_0
    return-void
.end method

.method private o()V
    .locals 2

    .prologue
    .line 364
    iget-object v0, p0, Ljyu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyv;

    .line 365
    invoke-interface {v0}, Ljyv;->b()V

    goto :goto_0

    .line 367
    :cond_0
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public a(I)Ljyw;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyw;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 399
    if-nez p1, :cond_0

    .line 400
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    .line 401
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    .line 402
    iget-object v1, p0, Ljyu;->c:Ljava/util/ArrayList;

    new-instance v2, Ljyw;

    invoke-direct {v2}, Ljyw;-><init>()V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 401
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 405
    :cond_0
    const-string v0, "OPTIONS"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    .line 406
    const-string v0, "HAS_POLL"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ljyu;->b:Z

    .line 407
    const-string v0, "QUESTION"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljyu;->d:Ljava/lang/String;

    .line 408
    const-string v0, "IMG1_HEADER_CANDIDATE"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ljyu;->g:Z

    .line 411
    :cond_1
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyw;

    .line 412
    if-eqz v0, :cond_2

    .line 413
    invoke-virtual {v0, p0}, Ljyw;->a(Ljyy;)V

    goto :goto_1

    .line 416
    :cond_3
    return-void
.end method

.method public a(Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Ljyu;->d:Ljava/lang/String;

    .line 134
    if-eqz p2, :cond_0

    .line 135
    invoke-direct {p0}, Ljyu;->o()V

    .line 137
    :cond_0
    return-void
.end method

.method public a(Ljyv;)V
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Ljyu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 392
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 124
    iput-boolean p1, p0, Ljyu;->b:Z

    .line 125
    iget-object v0, p0, Ljyu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyv;

    invoke-interface {v0}, Ljyv;->c()V

    goto :goto_0

    .line 126
    :cond_0
    return-void
.end method

.method public b(I)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 179
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v6, :cond_0

    .line 180
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must have at least 2 options"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 182
    :cond_0
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 183
    invoke-virtual {p0}, Ljyu;->j()Z

    move-result v0

    if-nez v0, :cond_1

    if-le v2, v6, :cond_1

    if-nez p1, :cond_1

    .line 186
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyw;

    .line 187
    iget-object v1, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljyw;

    iget-object v3, p0, Ljyu;->f:Landroid/content/Context;

    invoke-virtual {v0}, Ljyw;->a()Lizu;

    move-result-object v4

    .line 188
    invoke-virtual {v0}, Ljyw;->b()Lizu;

    move-result-object v0

    .line 187
    invoke-virtual {v1, v3, v4, v0, v7}, Ljyw;->a(Landroid/content/Context;Lizu;Lizu;Z)V

    .line 190
    :cond_1
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 191
    invoke-virtual {p0}, Ljyu;->a()I

    move-result v0

    if-ne v0, v6, :cond_2

    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyw;

    invoke-virtual {v0}, Ljyw;->a()Lizu;

    move-result-object v0

    if-nez v0, :cond_2

    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    .line 192
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyw;

    invoke-virtual {v0}, Ljyw;->a()Lizu;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 193
    invoke-virtual {p0}, Ljyu;->k()V

    .line 194
    invoke-virtual {p0, v5}, Ljyu;->b(Z)V

    .line 196
    :cond_2
    invoke-direct {p0, v2}, Ljyu;->d(I)V

    .line 197
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 419
    const-string v0, "OPTIONS"

    iget-object v1, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 420
    const-string v0, "HAS_POLL"

    iget-boolean v1, p0, Ljyu;->b:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 421
    const-string v0, "QUESTION"

    iget-object v1, p0, Ljyu;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    const-string v0, "IMG1_HEADER_CANDIDATE"

    iget-boolean v1, p0, Ljyu;->g:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 423
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 147
    iput-boolean p1, p0, Ljyu;->g:Z

    .line 148
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 83
    iget-boolean v0, p0, Ljyu;->b:Z

    return v0
.end method

.method public c()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 90
    iget-object v0, p0, Ljyu;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 101
    :goto_0
    return v0

    .line 93
    :cond_0
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_3

    .line 94
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyw;

    invoke-virtual {v0}, Ljyw;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 95
    goto :goto_0

    .line 97
    :cond_1
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyw;

    invoke-virtual {v0}, Ljyw;->a()Lizu;

    move-result-object v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 98
    goto :goto_0

    .line 93
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_3
    move v0, v2

    .line 101
    goto :goto_0
.end method

.method public c(I)Z
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 307
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_1

    move v0, v1

    .line 308
    :goto_0
    invoke-virtual {p0}, Ljyu;->j()Z

    move-result v5

    invoke-virtual {p0}, Ljyu;->a()I

    move-result v6

    if-ge v6, v9, :cond_2

    :cond_0
    :goto_1
    return v2

    :cond_1
    move v0, v2

    .line 307
    goto :goto_0

    :cond_2
    move v4, v2

    .line 308
    :goto_2
    if-ge v4, v6, :cond_6

    invoke-virtual {p0, v4}, Ljyu;->a(I)Ljyw;

    move-result-object v7

    add-int/lit8 v3, v6, -0x1

    if-ne v4, v3, :cond_4

    move v3, v1

    :goto_3
    invoke-virtual {v7}, Ljyw;->d()Z

    move-result v8

    if-eqz v8, :cond_3

    if-eqz v3, :cond_3

    if-gt v6, v9, :cond_5

    :cond_3
    invoke-virtual {v7, v5}, Ljyw;->a(Z)Z

    move-result v3

    if-nez v3, :cond_5

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljyu;->f:Landroid/content/Context;

    const v1, 0x7f0a0183

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_4
    move v3, v2

    goto :goto_3

    :cond_5
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_2

    :cond_6
    move v2, v1

    goto :goto_1
.end method

.method public d()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 105
    iput-object v6, p0, Ljyu;->d:Ljava/lang/String;

    .line 106
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 107
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    .line 108
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyw;

    invoke-virtual {v0, v6}, Ljyw;->a(Ljava/lang/String;)V

    .line 109
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyw;

    iget-object v5, p0, Ljyu;->f:Landroid/content/Context;

    invoke-virtual {v0, v5, v6, v6, v1}, Ljyw;->a(Landroid/content/Context;Lizu;Lizu;Z)V

    .line 110
    const/4 v0, 0x2

    if-lt v2, v0, :cond_0

    .line 111
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 114
    :cond_1
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_2

    .line 115
    iget-object v1, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 114
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 117
    :cond_2
    return-void
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Ljyu;->d:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljyw;
    .locals 5

    .prologue
    .line 163
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    int-to-long v0, v0

    invoke-virtual {p0}, Ljyu;->h()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    .line 164
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-virtual {p0}, Ljyu;->h()J

    move-result-wide v2

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x2a

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Cannot exceed "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " options"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 166
    :cond_0
    new-instance v0, Ljyw;

    invoke-direct {v0}, Ljyw;-><init>()V

    .line 167
    invoke-virtual {v0, p0}, Ljyw;->a(Ljyy;)V

    .line 168
    iget-object v1, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 169
    iget-object v2, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 170
    invoke-direct {p0, v1}, Ljyu;->d(I)V

    .line 171
    return-object v0
.end method

.method public g()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljyw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    return-object v0
.end method

.method public h()J
    .locals 4

    .prologue
    .line 213
    iget-wide v0, p0, Ljyu;->e:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 214
    iget-object v0, p0, Ljyu;->f:Landroid/content/Context;

    const-class v1, Lieh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 216
    iget-object v1, p0, Ljyu;->f:Landroid/content/Context;

    const-class v2, Lhee;

    invoke-static {v1, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhee;

    .line 217
    sget-object v2, Lkat;->a:Lief;

    .line 218
    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 217
    invoke-interface {v0, v2, v1}, Lieh;->c(Lief;I)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Ljyu;->e:J

    .line 220
    :cond_0
    iget-wide v0, p0, Ljyu;->e:J

    return-wide v0
.end method

.method public i()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 231
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_1

    .line 232
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyw;

    invoke-virtual {v0}, Ljyw;->a()Lizu;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 233
    const/4 v0, 0x1

    .line 236
    :goto_1
    return v0

    .line 231
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move v0, v1

    .line 236
    goto :goto_1
.end method

.method public j()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 246
    iget-boolean v0, p0, Ljyu;->g:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyw;

    invoke-virtual {v0}, Ljyw;->a()Lizu;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 255
    :cond_0
    :goto_0
    return v2

    :cond_1
    move v1, v2

    .line 250
    :goto_1
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 251
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyw;

    invoke-virtual {v0}, Ljyw;->a()Lizu;

    move-result-object v0

    if-nez v0, :cond_0

    .line 250
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move v2, v3

    .line 255
    goto :goto_0
.end method

.method public k()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, -0x1

    const/4 v5, 0x0

    .line 264
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyw;

    invoke-virtual {v0}, Ljyw;->a()Lizu;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 286
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    const/4 v0, 0x1

    move v1, v0

    move v2, v3

    :goto_1
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 269
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyw;

    invoke-virtual {v0}, Ljyw;->a()Lizu;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 272
    if-ne v2, v3, :cond_0

    move v2, v1

    .line 268
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 278
    :cond_3
    if-eq v2, v3, :cond_0

    .line 282
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyw;

    .line 283
    iget-object v1, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljyw;

    iget-object v2, p0, Ljyu;->f:Landroid/content/Context;

    .line 284
    invoke-virtual {v0}, Ljyw;->a()Lizu;

    move-result-object v3

    invoke-virtual {v0}, Ljyw;->b()Lizu;

    move-result-object v4

    .line 283
    invoke-virtual {v1, v2, v3, v4, v5}, Ljyw;->a(Landroid/content/Context;Lizu;Lizu;Z)V

    .line 285
    iget-object v1, p0, Ljyu;->f:Landroid/content/Context;

    invoke-virtual {v0, v1, v6, v6, v5}, Ljyw;->a(Landroid/content/Context;Lizu;Lizu;Z)V

    goto :goto_0
.end method

.method public l()Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 338
    invoke-virtual {p0}, Ljyu;->b()Z

    move-result v2

    if-nez v2, :cond_1

    .line 345
    :cond_0
    :goto_0
    return v0

    .line 340
    :cond_1
    iget-object v2, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    int-to-long v2, v2

    invoke-virtual {p0}, Ljyu;->h()J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 342
    iget-object v0, p0, Ljyu;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x2

    if-le v0, v2, :cond_2

    move v0, v1

    .line 343
    goto :goto_0

    :cond_2
    move v0, v1

    .line 345
    goto :goto_0
.end method

.method public m()V
    .locals 2

    .prologue
    .line 354
    iget-object v0, p0, Ljyu;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyv;

    invoke-interface {v0}, Ljyv;->a()V

    goto :goto_0

    .line 355
    :cond_0
    return-void
.end method

.method public n()V
    .locals 0

    .prologue
    .line 371
    invoke-direct {p0}, Ljyu;->o()V

    .line 372
    return-void
.end method
