.class public final Lieo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lien;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Liek;

.field private final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lieg;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lieo;->c:Ljava/util/ArrayList;

    .line 32
    iput-object p1, p0, Lieo;->a:Landroid/content/Context;

    .line 33
    new-instance v0, Liek;

    invoke-direct {v0, p1}, Liek;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lieo;->b:Liek;

    .line 34
    return-void
.end method

.method private a([Lmxp;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 127
    if-eqz p1, :cond_0

    array-length v1, p1

    if-nez v1, :cond_1

    .line 128
    :cond_0
    const-string v0, ""

    .line 141
    :goto_0
    return-object v0

    .line 131
    :cond_1
    :try_start_0
    array-length v1, p1

    invoke-static {v1}, Loxo;->i(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    move v2, v1

    move v1, v0

    :goto_1
    array-length v3, p1

    if-ge v1, v3, :cond_2

    aget-object v3, p1, v1

    invoke-static {v3}, Loxo;->c(Loxu;)I

    move-result v3

    add-int/2addr v2, v3

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 132
    :cond_2
    new-array v1, v2, [B

    .line 133
    const/4 v2, 0x0

    array-length v3, v1

    invoke-static {v1, v2, v3}, Loxo;->a([BII)Loxo;

    move-result-object v2

    .line 134
    array-length v3, p1

    invoke-virtual {v2, v3}, Loxo;->a(I)V

    .line 135
    :goto_2
    array-length v3, p1

    if-ge v0, v3, :cond_3

    .line 136
    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Loxo;->b(Loxu;)V

    .line 135
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 138
    :cond_3
    const/4 v0, 0x0

    invoke-static {v1, v0}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 139
    :catch_0
    move-exception v0

    .line 140
    const-string v1, "ExperimentLoader"

    const-string v2, "Failed to serialize experiment data"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 141
    const-string v0, ""

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)[Lmxp;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 155
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 172
    :cond_1
    :goto_0
    return-object v0

    .line 159
    :cond_2
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 161
    const/4 v3, 0x0

    array-length v4, v0

    invoke-static {v0, v3, v4}, Loxn;->a([BII)Loxn;

    move-result-object v3

    .line 162
    invoke-virtual {v3}, Loxn;->g()I

    move-result v4

    .line 163
    new-array v0, v4, [Lmxp;

    .line 164
    :goto_1
    if-ge v2, v4, :cond_1

    .line 165
    new-instance v5, Lmxp;

    invoke-direct {v5}, Lmxp;-><init>()V

    .line 166
    invoke-virtual {v3, v5}, Loxn;->a(Loxu;)V

    .line 167
    aput-object v5, v0, v2
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 170
    :catch_0
    move-exception v0

    .line 171
    const-string v2, "ExperimentLoader"

    const-string v3, "Unable to parse experiments"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 172
    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Ljava/util/Map;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 66
    .line 68
    iget-object v0, p0, Lieo;->b:Liek;

    invoke-virtual {v0, p1}, Liek;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 70
    if-eqz v0, :cond_9

    .line 71
    invoke-direct {p0, v0}, Lieo;->b(Ljava/lang/String;)[Lmxp;

    move-result-object v0

    move-object v4, v0

    .line 74
    :goto_0
    if-nez v4, :cond_0

    .line 105
    :goto_1
    return-object v1

    .line 78
    :cond_0
    array-length v5, v4

    .line 79
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2, v5}, Ljava/util/HashMap;-><init>(I)V

    .line 80
    const/4 v0, 0x0

    move v3, v0

    :goto_2
    if-ge v3, v5, :cond_7

    .line 81
    aget-object v6, v4, v3

    .line 83
    iget v0, v6, Lmxp;->c:I

    .line 84
    const/high16 v7, -0x80000000

    if-eq v0, v7, :cond_1

    const/4 v7, 0x1

    if-ne v0, v7, :cond_3

    .line 87
    :cond_1
    const-string v0, "true"

    .line 100
    :goto_3
    if-eqz v0, :cond_2

    .line 101
    iget-object v6, v6, Lmxp;->b:Ljava/lang/String;

    invoke-virtual {v2, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    :cond_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    .line 88
    :cond_3
    const/4 v7, 0x4

    if-ne v0, v7, :cond_5

    .line 89
    iget-object v0, v6, Lmxp;->d:Lmxq;

    if-eqz v0, :cond_4

    iget-object v0, v6, Lmxp;->d:Lmxq;

    iget-object v0, v0, Lmxq;->c:Ljava/lang/String;

    goto :goto_3

    :cond_4
    move-object v0, v1

    goto :goto_3

    .line 90
    :cond_5
    const/4 v7, 0x3

    if-ne v0, v7, :cond_6

    .line 91
    iget-object v0, v6, Lmxp;->d:Lmxq;

    if-eqz v0, :cond_8

    iget-object v0, v6, Lmxp;->d:Lmxq;

    iget-object v0, v0, Lmxq;->b:Ljava/lang/Double;

    if-eqz v0, :cond_8

    .line 92
    iget-object v0, v6, Lmxp;->d:Lmxq;

    iget-object v0, v0, Lmxq;->b:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 94
    :cond_6
    const/4 v7, 0x2

    if-ne v0, v7, :cond_8

    .line 95
    iget-object v0, v6, Lmxp;->d:Lmxq;

    if-eqz v0, :cond_8

    iget-object v0, v6, Lmxp;->d:Lmxq;

    iget-object v0, v0, Lmxq;->a:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 96
    iget-object v0, v6, Lmxp;->d:Lmxq;

    iget-object v0, v0, Lmxq;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_7
    move-object v1, v2

    .line 105
    goto :goto_1

    :cond_8
    move-object v0, v1

    goto :goto_3

    :cond_9
    move-object v4, v1

    goto :goto_0
.end method

.method public a(Lieg;)V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lieo;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    return-void
.end method

.method public a(Ljava/lang/String;[Lmxp;)V
    .locals 2

    .prologue
    .line 59
    invoke-direct {p0, p2}, Lieo;->a([Lmxp;)Ljava/lang/String;

    move-result-object v0

    .line 60
    iget-object v1, p0, Lieo;->b:Liek;

    invoke-virtual {v1, p1, v0}, Liek;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 61
    iget-object v0, p0, Lieo;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieg;

    invoke-interface {v0}, Lieg;->l()V

    goto :goto_0

    .line 62
    :cond_0
    return-void
.end method

.method public a(Ljava/util/List;Ljava/lang/String;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")Z"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lieo;->a:Landroid/content/Context;

    .line 40
    new-instance v1, Lkfo;

    const/4 v2, 0x0

    invoke-direct {v1, p2, v2}, Lkfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    new-instance v2, Lieu;

    invoke-direct {v2, v0, v1, p1}, Lieu;-><init>(Landroid/content/Context;Lkfo;Ljava/util/List;)V

    .line 43
    invoke-virtual {v2}, Lieu;->l()V

    .line 46
    :try_start_0
    const-string v0, "ExperimentLoader"

    invoke-virtual {v2, v0}, Lieu;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    invoke-virtual {v2}, Lieu;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmbz;

    .line 52
    iget-object v0, v0, Lmbz;->a:Lmxo;

    iget-object v0, v0, Lmxo;->a:[Lmxp;

    invoke-virtual {p0, p2, v0}, Lieo;->a(Ljava/lang/String;[Lmxp;)V

    .line 54
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 48
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method
