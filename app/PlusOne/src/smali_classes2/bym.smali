.class public Lbym;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbyh;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lbym;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lbrm;IIJ)J
    .locals 4

    .prologue
    .line 91
    add-int v0, p2, p3

    .line 92
    invoke-virtual {p1}, Lbrm;->a()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 93
    invoke-virtual {p1, v0}, Lbrm;->a(I)J

    move-result-wide v0

    invoke-virtual {p1, p2}, Lbrm;->a(I)J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 95
    :goto_0
    return-wide v0

    :cond_0
    int-to-long v0, p3

    mul-long/2addr v0, p4

    goto :goto_0
.end method


# virtual methods
.method public a(Ljava/util/List;Lbrm;Ljava/util/List;Lbyu;)Ljava/util/List;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbqv;",
            ">;",
            "Lbrm;",
            "Ljava/util/List",
            "<",
            "Lbys;",
            ">;",
            "Lbyu;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 34
    if-nez p3, :cond_1

    .line 35
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 36
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbqv;

    .line 37
    iget-wide v6, v2, Lbqv;->b:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    move-object v2, v3

    .line 86
    :goto_1
    return-object v2

    .line 42
    :cond_1
    const-string v2, "miniThemes"

    const-string v3, "miniThemeTypes cannot be null for FunFactoryBeatMatcher"

    move-object/from16 v0, p3

    invoke-static {v0, v2, v3}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 45
    const/4 v2, 0x0

    .line 46
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v3, v2

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbys;

    .line 47
    invoke-interface {v2}, Lbys;->e()I

    move-result v2

    add-int/2addr v2, v3

    move v3, v2

    .line 48
    goto :goto_2

    .line 50
    :cond_2
    const-string v2, "totalClipsInMiniThemes"

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v4

    const-string v5, "Total clips in mini themes should equal the number of clip constraints."

    invoke-static {v3, v2, v4, v5}, Lcgp;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 54
    move-object/from16 v0, p4

    iget-wide v6, v0, Lbyu;->c:J

    invoke-virtual/range {p2 .. p2}, Lbrm;->a()I

    move-result v2

    const/4 v3, 0x2

    if-lt v2, v3, :cond_3

    invoke-virtual/range {p2 .. p2}, Lbrm;->a()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lbrm;->a(I)J

    move-result-wide v2

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Lbrm;->a(I)J

    move-result-wide v4

    sub-long/2addr v2, v4

    long-to-float v2, v2

    invoke-virtual/range {p2 .. p2}, Lbrm;->a()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    div-float/2addr v2, v3

    float-to-long v6, v2

    .line 56
    :cond_3
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 57
    const/4 v4, 0x0

    .line 58
    const/4 v2, 0x0

    move v9, v2

    :goto_3
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v2

    if-ge v9, v2, :cond_9

    .line 59
    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lbys;

    .line 61
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v9, v2, :cond_4

    sget-object v2, Lbyn;->R:Lbyn;

    if-ne v8, v2, :cond_5

    .line 63
    :cond_4
    invoke-interface/range {p3 .. p3}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v9, v2, :cond_6

    add-int/lit8 v2, v9, 0x1

    .line 64
    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    sget-object v3, Lbyn;->Q:Lbyn;

    if-ne v2, v3, :cond_6

    :cond_5
    const/4 v2, 0x1

    move v10, v2

    .line 67
    :goto_4
    invoke-interface {v8}, Lbys;->f()I

    move-result v5

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    .line 66
    invoke-direct/range {v2 .. v7}, Lbym;->a(Lbrm;IIJ)J

    move-result-wide v14

    .line 68
    invoke-interface {v8}, Lbys;->f()I

    move-result v2

    add-int/2addr v4, v2

    .line 70
    const/4 v2, 0x0

    move v11, v2

    :goto_5
    invoke-interface {v8}, Lbys;->e()I

    move-result v2

    if-ge v11, v2, :cond_8

    .line 72
    invoke-interface {v8, v11}, Lbys;->a(I)D

    move-result-wide v2

    long-to-double v0, v14

    move-wide/from16 v16, v0

    mul-double v2, v2, v16

    double-to-long v0, v2

    move-wide/from16 v16, v0

    .line 74
    if-eqz v10, :cond_7

    invoke-interface {v8}, Lbys;->e()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne v11, v2, :cond_7

    .line 76
    const/4 v5, 0x1

    move-object/from16 v2, p0

    move-object/from16 v3, p2

    invoke-direct/range {v2 .. v7}, Lbym;->a(Lbrm;IIJ)J

    move-result-wide v2

    .line 78
    add-int/lit8 v4, v4, 0x1

    .line 80
    add-long v2, v2, v16

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    :goto_6
    add-int/lit8 v2, v11, 0x1

    move v11, v2

    goto :goto_5

    .line 64
    :cond_6
    const/4 v2, 0x0

    move v10, v2

    goto :goto_4

    .line 82
    :cond_7
    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v12, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 58
    :cond_8
    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto/16 :goto_3

    :cond_9
    move-object v2, v12

    .line 86
    goto/16 :goto_1
.end method
