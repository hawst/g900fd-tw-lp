.class final Lbsz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Livo;


# instance fields
.field public volatile a:J

.field public volatile b:J

.field public volatile c:Lbtw;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Livm;)V
    .locals 6

    .prologue
    .line 896
    const-string v0, "Playback loop at %d us. Last seek was to %d us."

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lbsz;->b:J

    .line 897
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p0, Lbsz;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 896
    invoke-interface {p1, v0, v1}, Livm;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 898
    iget-object v0, p0, Lbsz;->c:Lbtw;

    if-eqz v0, :cond_0

    .line 899
    iget-object v0, p0, Lbsz;->c:Lbtw;

    invoke-virtual {v0, p1}, Lbtw;->a(Livm;)V

    .line 901
    :cond_0
    return-void
.end method
