.class public final Lbrv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lbrv;->a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lbrv;->a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->a(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 100
    :try_start_0
    iget-object v0, p0, Lbrv;->a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->e(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-nez v0, :cond_0

    .line 101
    monitor-exit v1

    .line 111
    :goto_0
    return-void

    .line 104
    :cond_0
    iget-object v0, p0, Lbrv;->a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->b(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 105
    iget-object v0, p0, Lbrv;->a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->a(Lcom/google/android/apps/moviemaker/picker/PickerTileView;Z)V

    .line 108
    :cond_1
    iget-object v0, p0, Lbrv;->a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    iget-object v2, p0, Lbrv;->a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    invoke-static {v2}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->e(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 109
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    iget-object v0, p0, Lbrv;->a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->f(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    goto :goto_0

    .line 109
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
