.class public abstract Lbbi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbbd;


# instance fields
.field private final a:Lbkz;

.field private final b:F

.field private final c:F

.field private final d:Lbaz;


# direct methods
.method public constructor <init>(Lbkz;FFLbaz;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lbbi;->a:Lbkz;

    .line 36
    iput p2, p0, Lbbi;->b:F

    .line 37
    iput p3, p0, Lbbi;->c:F

    .line 38
    iput-object p4, p0, Lbbi;->d:Lbaz;

    .line 39
    return-void
.end method


# virtual methods
.method public a(Lbmr;)Z
    .locals 1

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Lbbi;->c(Lbmr;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lbmr;)F
    .locals 2

    .prologue
    .line 48
    invoke-virtual {p0, p1}, Lbbi;->d(Lbmr;)F

    move-result v0

    .line 50
    iget-object v1, p0, Lbbi;->d:Lbaz;

    if-eqz v1, :cond_0

    .line 51
    iget-object v1, p0, Lbbi;->d:Lbaz;

    invoke-interface {v1, v0}, Lbaz;->a(F)F

    move-result v0

    .line 53
    :cond_0
    iget v1, p0, Lbbi;->b:F

    sub-float/2addr v0, v1

    iget v1, p0, Lbbi;->c:F

    div-float/2addr v0, v1

    return v0
.end method

.method protected c(Lbmr;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 57
    invoke-virtual {p1}, Lbmr;->c()Lbky;

    move-result-object v0

    iget-object v1, p0, Lbbi;->a:Lbkz;

    invoke-virtual {v0, v1}, Lbky;->a(Lbkz;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected abstract d(Lbmr;)F
.end method
