.class public final Lbvk;
.super Loxu;
.source "PG"


# instance fields
.field public a:[F

.field private b:F

.field private c:F

.field private d:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 597
    invoke-direct {p0}, Loxu;-><init>()V

    .line 600
    iput v0, p0, Lbvk;->b:F

    .line 603
    iput v0, p0, Lbvk;->c:F

    .line 606
    iput v0, p0, Lbvk;->d:F

    .line 609
    sget-object v0, Loxx;->c:[F

    iput-object v0, p0, Lbvk;->a:[F

    .line 597
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 641
    const/4 v0, 0x0

    .line 642
    iget v1, p0, Lbvk;->b:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_0

    .line 643
    const/4 v0, 0x1

    iget v1, p0, Lbvk;->b:F

    .line 644
    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x0

    .line 646
    :cond_0
    iget v1, p0, Lbvk;->c:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_1

    .line 647
    const/4 v1, 0x2

    iget v2, p0, Lbvk;->c:F

    .line 648
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 650
    :cond_1
    iget v1, p0, Lbvk;->d:F

    cmpl-float v1, v1, v3

    if-eqz v1, :cond_2

    .line 651
    const/4 v1, 0x3

    iget v2, p0, Lbvk;->d:F

    .line 652
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 654
    :cond_2
    iget-object v1, p0, Lbvk;->a:[F

    if-eqz v1, :cond_3

    iget-object v1, p0, Lbvk;->a:[F

    array-length v1, v1

    if-lez v1, :cond_3

    .line 655
    iget-object v1, p0, Lbvk;->a:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x4

    .line 656
    add-int/2addr v0, v1

    .line 657
    iget-object v1, p0, Lbvk;->a:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 659
    :cond_3
    iput v0, p0, Lbvk;->ai:I

    .line 660
    return v0
.end method

.method public a(Loxn;)Lbvk;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 668
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 669
    sparse-switch v0, :sswitch_data_0

    .line 673
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 674
    :sswitch_0
    return-object p0

    .line 679
    :sswitch_1
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbvk;->b:F

    goto :goto_0

    .line 683
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbvk;->c:F

    goto :goto_0

    .line 687
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbvk;->d:F

    goto :goto_0

    .line 691
    :sswitch_4
    const/16 v0, 0x25

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 692
    iget-object v0, p0, Lbvk;->a:[F

    array-length v0, v0

    .line 693
    add-int/2addr v1, v0

    new-array v1, v1, [F

    .line 694
    iget-object v2, p0, Lbvk;->a:[F

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 695
    iput-object v1, p0, Lbvk;->a:[F

    .line 696
    :goto_1
    iget-object v1, p0, Lbvk;->a:[F

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    .line 697
    iget-object v1, p0, Lbvk;->a:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v2

    aput v2, v1, v0

    .line 698
    invoke-virtual {p1}, Loxn;->a()I

    .line 696
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 701
    :cond_1
    iget-object v1, p0, Lbvk;->a:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 669
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 623
    iget v0, p0, Lbvk;->b:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_0

    .line 624
    const/4 v0, 0x1

    iget v1, p0, Lbvk;->b:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 626
    :cond_0
    iget v0, p0, Lbvk;->c:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_1

    .line 627
    const/4 v0, 0x2

    iget v1, p0, Lbvk;->c:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 629
    :cond_1
    iget v0, p0, Lbvk;->d:F

    cmpl-float v0, v0, v2

    if-eqz v0, :cond_2

    .line 630
    const/4 v0, 0x3

    iget v1, p0, Lbvk;->d:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 632
    :cond_2
    iget-object v0, p0, Lbvk;->a:[F

    if-eqz v0, :cond_3

    .line 633
    iget-object v1, p0, Lbvk;->a:[F

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget v3, v1, v0

    .line 634
    const/4 v4, 0x4

    invoke-virtual {p1, v4, v3}, Loxo;->a(IF)V

    .line 633
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 637
    :cond_3
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 593
    invoke-virtual {p0, p1}, Lbvk;->a(Loxn;)Lbvk;

    move-result-object v0

    return-object v0
.end method
