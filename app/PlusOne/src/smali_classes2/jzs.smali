.class public Ljzs;
.super Lhny;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Ljzs;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ljzs;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 25
    sget-object v0, Ljzs;->a:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 26
    iput-object p1, p0, Ljzs;->b:Landroid/content/Context;

    .line 27
    iput p2, p0, Ljzs;->c:I

    .line 28
    iput-object p3, p0, Ljzs;->d:Ljava/lang/String;

    .line 29
    iput-object p4, p0, Ljzs;->e:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 6

    .prologue
    .line 34
    new-instance v2, Ljzr;

    iget-object v0, p0, Ljzs;->b:Landroid/content/Context;

    iget v1, p0, Ljzs;->c:I

    iget-object v3, p0, Ljzs;->d:Ljava/lang/String;

    iget-object v4, p0, Ljzs;->e:Ljava/lang/String;

    invoke-direct {v2, v0, v1, v3, v4}, Ljzr;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 36
    iget-object v0, p0, Ljzs;->b:Landroid/content/Context;

    const-class v1, Lkfd;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    .line 37
    invoke-interface {v0, v2}, Lkfd;->a(Lkff;)V

    .line 38
    new-instance v1, Lhoz;

    iget v0, v2, Lkff;->i:I

    iget-object v3, v2, Lkff;->k:Ljava/lang/Exception;

    const/4 v4, 0x0

    invoke-direct {v1, v0, v3, v4}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljzr;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmhf;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    iget-object v3, v0, Lmhf;->a:Lnhb;

    invoke-virtual {v2}, Ljzr;->t()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz v3, :cond_1

    iget-object v0, v3, Lnhb;->a:Lnge;

    if-eqz v0, :cond_1

    iget-object v0, v3, Lnhb;->a:Lnge;

    iget-object v0, v0, Lnge;->a:[Lngg;

    if-eqz v0, :cond_1

    iget-object v0, v3, Lnhb;->a:Lnge;

    iget-object v0, v0, Lnge;->a:[Lngg;

    array-length v0, v0

    const/4 v2, 0x2

    if-ge v0, v2, :cond_2

    :cond_1
    move-object v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Ljzs;->b:Landroid/content/Context;

    const-class v2, Ljxn;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxn;

    iget v2, p0, Ljzs;->c:I

    iget-object v4, p0, Ljzs;->d:Ljava/lang/String;

    iget-object v3, v3, Lnhb;->a:Lnge;

    const/4 v5, 0x1

    invoke-interface {v0, v2, v4, v3, v5}, Ljxn;->a(ILjava/lang/String;Lnge;Z)V

    move-object v0, v1

    goto :goto_0
.end method
