.class public Layo;
.super Layd;
.source "PG"


# instance fields
.field private final a:Z

.field private final b:Landroid/graphics/Matrix;

.field private final c:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Layj;FFLandroid/animation/TimeInterpolator;IZ)V
    .locals 1

    .prologue
    .line 30
    invoke-direct/range {p0 .. p5}, Layd;-><init>(Layj;FFLandroid/animation/TimeInterpolator;I)V

    .line 24
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Layo;->b:Landroid/graphics/Matrix;

    .line 25
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Layo;->c:Landroid/graphics/Matrix;

    .line 31
    iput-boolean p6, p0, Layo;->a:Z

    .line 32
    return-void
.end method


# virtual methods
.method public a(Layl;Lbof;)Landroid/graphics/Matrix;
    .locals 3

    .prologue
    const v2, 0x3f933333    # 1.15f

    const/high16 v1, 0x3f000000    # 0.5f

    .line 36
    invoke-super {p0, p1, p2}, Layd;->a(Layl;Lbof;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 37
    invoke-virtual {v0, v2, v2, v1, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 38
    return-object v0
.end method

.method public b(Layl;)Landroid/graphics/Matrix;
    .locals 4

    .prologue
    const v3, 0x3f933333    # 1.15f

    const/high16 v2, 0x3f000000    # 0.5f

    .line 43
    iget-object v0, p1, Layl;->k:Lbmg;

    sget-object v1, Lbmg;->c:Lbmg;

    if-eq v0, v1, :cond_0

    .line 44
    invoke-virtual {p0}, Layo;->h()Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->b(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 48
    :goto_0
    return-object v0

    .line 46
    :cond_0
    invoke-super {p0, p1}, Layd;->b(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 47
    invoke-virtual {v0, v3, v3, v2, v2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    goto :goto_0
.end method

.method public f(Layl;)I
    .locals 1

    .prologue
    .line 59
    const v0, 0x7f020113

    return v0
.end method

.method public g(Layl;)Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Layo;->b:Landroid/graphics/Matrix;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, p1, v1}, Layo;->a(Landroid/graphics/Matrix;Layl;Z)V

    .line 54
    iget-object v0, p0, Layo;->b:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public h(Layl;)F
    .locals 2

    .prologue
    .line 64
    const/high16 v0, 0x3f800000    # 1.0f

    iget v1, p1, Layl;->m:F

    sub-float/2addr v0, v1

    return v0
.end method

.method public i(Layl;)I
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x4

    return v0
.end method

.method public j(Layl;)I
    .locals 2

    .prologue
    .line 84
    iget-boolean v0, p0, Layo;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Layl;->k:Lbmg;

    sget-object v1, Lbmg;->c:Lbmg;

    if-eq v0, v1, :cond_1

    .line 85
    :cond_0
    invoke-virtual {p0}, Layo;->h()Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->j(Layl;)I

    move-result v0

    .line 87
    :goto_0
    return v0

    :cond_1
    const v0, 0x7f020113

    goto :goto_0
.end method

.method public k(Layl;)Landroid/graphics/Matrix;
    .locals 2

    .prologue
    .line 74
    iget-boolean v0, p0, Layo;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Layl;->k:Lbmg;

    sget-object v1, Lbmg;->c:Lbmg;

    if-eq v0, v1, :cond_1

    .line 75
    :cond_0
    invoke-virtual {p0}, Layo;->h()Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->k(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 79
    :goto_0
    return-object v0

    .line 78
    :cond_1
    iget-object v0, p0, Layo;->c:Landroid/graphics/Matrix;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Layo;->a(Landroid/graphics/Matrix;Layl;Z)V

    .line 79
    iget-object v0, p0, Layo;->c:Landroid/graphics/Matrix;

    goto :goto_0
.end method

.method public l(Layl;)F
    .locals 2

    .prologue
    .line 92
    iget-boolean v0, p0, Layo;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Layl;->k:Lbmg;

    sget-object v1, Lbmg;->c:Lbmg;

    if-eq v0, v1, :cond_1

    .line 93
    :cond_0
    invoke-virtual {p0}, Layo;->h()Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->l(Layl;)F

    move-result v0

    .line 95
    :goto_0
    return v0

    :cond_1
    iget v0, p1, Layl;->m:F

    goto :goto_0
.end method

.method public m(Layl;)I
    .locals 2

    .prologue
    .line 100
    iget-boolean v0, p0, Layo;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Layl;->k:Lbmg;

    sget-object v1, Lbmg;->c:Lbmg;

    if-eq v0, v1, :cond_1

    .line 101
    :cond_0
    invoke-virtual {p0}, Layo;->h()Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->m(Layl;)I

    move-result v0

    .line 103
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method
