.class final Laqi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbtq;


# instance fields
.field private a:Landroid/graphics/Bitmap;

.field private synthetic b:Laqc;


# direct methods
.method constructor <init>(Laqc;)V
    .locals 0

    .prologue
    .line 620
    iput-object p1, p0, Laqi;->b:Laqc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 631
    iget-object v0, p0, Laqi;->b:Laqc;

    iget-boolean v0, v0, Lamn;->i:Z

    if-nez v0, :cond_0

    .line 654
    :goto_0
    return-void

    .line 635
    :cond_0
    iget-object v0, p0, Laqi;->b:Laqc;

    invoke-static {v0}, Laqc;->f(Laqc;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    .line 636
    invoke-interface {v0, v4}, Laqv;->b(Z)V

    .line 637
    invoke-interface {v0, v3, v3}, Laqv;->a(ZI)V

    .line 638
    invoke-interface {v0}, Laqv;->s_()V

    .line 639
    invoke-interface {v0, v3}, Laqv;->c(Z)V

    .line 641
    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-static {v1}, Laqc;->q(Laqc;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Laqi;->a:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 643
    iget-object v1, p0, Laqi;->a:Landroid/graphics/Bitmap;

    const/4 v2, 0x2

    invoke-interface {v0, v1, v2}, Laqv;->a(Landroid/graphics/Bitmap;I)V

    .line 645
    const/4 v1, 0x0

    iput-object v1, p0, Laqi;->a:Landroid/graphics/Bitmap;

    .line 649
    :goto_1
    invoke-interface {v0, v3}, Laqv;->d_(Z)V

    .line 651
    iget-object v0, p0, Laqi;->b:Laqc;

    invoke-static {v0}, Laqc;->b(Laqc;)Lawf;

    move-result-object v0

    invoke-interface {v0, v4}, Lawf;->h(Z)V

    .line 653
    iget-object v0, p0, Laqi;->b:Laqc;

    invoke-static {v0, v3}, Laqc;->c(Laqc;Z)Z

    goto :goto_0

    .line 647
    :cond_1
    invoke-interface {v0, v4}, Laqv;->c_(Z)V

    goto :goto_1
.end method

.method public a(IZ)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 708
    iget-object v0, p0, Laqi;->b:Laqc;

    invoke-static {v0}, Laqc;->t(Laqc;)V

    .line 710
    iget-object v0, p0, Laqi;->b:Laqc;

    iget-boolean v0, v0, Lamn;->i:Z

    if-nez v0, :cond_1

    .line 752
    :cond_0
    :goto_0
    return-void

    .line 714
    :cond_1
    iget-object v0, p0, Laqi;->b:Laqc;

    invoke-static {v0}, Laqc;->f(Laqc;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    .line 715
    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-static {v1, v6, v7}, Laqc;->b(Laqc;J)J

    .line 716
    if-eqz p1, :cond_2

    .line 717
    const/4 v1, 0x2

    if-ne p1, v1, :cond_3

    .line 718
    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-static {v1}, Laqc;->u(Laqc;)Laqb;

    move-result-object v1

    invoke-interface {v1}, Laqb;->a()V

    .line 729
    :cond_2
    :goto_1
    invoke-interface {v0, v5}, Laqv;->b(Z)V

    .line 730
    invoke-interface {v0, v4}, Laqv;->d_(Z)V

    .line 731
    invoke-interface {v0, v5}, Laqv;->c(Z)V

    .line 732
    if-eqz p2, :cond_0

    .line 733
    const/4 v1, 0x0

    iget-object v2, p0, Laqi;->b:Laqc;

    invoke-static {v2}, Laqc;->b(Laqc;)Lawf;

    move-result-object v2

    invoke-interface {v2}, Lawf;->W()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Laqv;->a(FJ)V

    .line 734
    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-static {v1}, Laqc;->b(Laqc;)Lawf;

    move-result-object v1

    invoke-interface {v1, v5}, Lawf;->h(Z)V

    .line 735
    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-static {v1}, Laqc;->b(Laqc;)Lawf;

    move-result-object v1

    invoke-interface {v1, v6, v7}, Lawf;->b(J)V

    .line 736
    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-static {v1}, Laqc;->a(Laqc;)Laxp;

    move-result-object v1

    sget-object v2, Laxq;->c:Laxq;

    invoke-virtual {v1, v2}, Laxp;->a(Laxq;)V

    .line 737
    iget-object v1, p0, Laqi;->b:Laqc;

    .line 738
    invoke-static {v1}, Laqc;->a(Laqc;)Laxp;

    move-result-object v1

    iget-object v2, p0, Laqi;->b:Laqc;

    invoke-static {v2}, Laqc;->b(Laqc;)Lawf;

    move-result-object v2

    invoke-interface {v2}, Lawf;->bd()Lawg;

    move-result-object v2

    invoke-virtual {v1, v2}, Laxp;->a(Lawg;)Laxs;

    move-result-object v1

    .line 739
    iget-object v2, p0, Laqi;->b:Laqc;

    invoke-static {v2}, Laqc;->b(Laqc;)Lawf;

    move-result-object v2

    invoke-interface {v2}, Lawf;->bd()Lawg;

    move-result-object v2

    sget-object v3, Lawg;->c:Lawg;

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Laqi;->b:Laqc;

    .line 740
    invoke-static {v2}, Laqc;->b(Laqc;)Lawf;

    move-result-object v2

    invoke-interface {v2}, Lawf;->bi()Z

    move-result v2

    if-nez v2, :cond_5

    .line 741
    invoke-interface {v0, v1}, Laqv;->b(Laxs;)V

    .line 742
    iget-object v0, p0, Laqi;->b:Laqc;

    invoke-static {v0}, Laqc;->b(Laqc;)Lawf;

    move-result-object v0

    invoke-interface {v0, v4}, Lawf;->r(Z)V

    .line 747
    :goto_2
    iget-object v0, p0, Laqi;->b:Laqc;

    invoke-static {v0}, Laqc;->a(Laqc;)Laxp;

    move-result-object v0

    invoke-virtual {v0, v1}, Laxp;->a(Laxs;)V

    .line 748
    iget-object v0, p0, Laqi;->b:Laqc;

    invoke-static {v0}, Laqc;->w(Laqc;)V

    .line 750
    iget-object v0, p0, Laqi;->b:Laqc;

    invoke-static {v0}, Laqc;->o(Laqc;)V

    goto/16 :goto_0

    .line 720
    :cond_3
    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-static {v1}, Laqc;->v(Laqc;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 723
    const/4 v1, 0x3

    if-ne p1, v1, :cond_4

    const v1, 0x7f0a00f7

    :goto_3
    invoke-interface {v0, v4, v1}, Laqv;->a(ZI)V

    goto/16 :goto_1

    :cond_4
    const v1, 0x7f0a00f6

    goto :goto_3

    .line 744
    :cond_5
    iget-object v2, p0, Laqi;->b:Laqc;

    invoke-static {v2}, Laqc;->b(Laqc;)Lawf;

    move-result-object v2

    invoke-interface {v2, v4}, Lawf;->i(Z)V

    .line 745
    invoke-interface {v0, v1}, Laqv;->a(Laxs;)V

    goto :goto_2
.end method

.method public a(J)V
    .locals 2

    .prologue
    .line 756
    iget-object v0, p0, Laqi;->b:Laqc;

    iget-boolean v0, v0, Lamn;->i:Z

    if-nez v0, :cond_0

    .line 761
    :goto_0
    return-void

    .line 760
    :cond_0
    iget-object v0, p0, Laqi;->b:Laqc;

    invoke-static {v0}, Laqc;->f(Laqc;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Laqv;->c(Z)V

    goto :goto_0
.end method

.method public a(JLandroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 789
    iget-object v0, p0, Laqi;->b:Laqc;

    iget-boolean v0, v0, Lamn;->i:Z

    if-nez v0, :cond_0

    .line 795
    :goto_0
    return-void

    .line 792
    :cond_0
    iget-object v0, p0, Laqi;->b:Laqc;

    invoke-static {v0}, Laqc;->b(Laqc;)Lawf;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lawf;->b(J)V

    .line 793
    iget-object v0, p0, Laqi;->b:Laqc;

    invoke-static {v0}, Laqc;->b(Laqc;)Lawf;

    move-result-object v0

    invoke-interface {v0, p3}, Lawf;->a(Landroid/graphics/RectF;)V

    .line 794
    iget-object v0, p0, Laqi;->b:Laqc;

    invoke-static {v0, p1, p2}, Laqc;->c(Laqc;J)V

    goto :goto_0
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 658
    iget-object v0, p0, Laqi;->b:Laqc;

    iget-boolean v0, v0, Lamn;->i:Z

    if-nez v0, :cond_1

    .line 703
    :cond_0
    :goto_0
    return-void

    .line 662
    :cond_1
    iget-object v0, p0, Laqi;->b:Laqc;

    invoke-static {v0}, Laqc;->c(Laqc;)Lcdu;

    move-result-object v0

    invoke-virtual {v0}, Lcdu;->f()V

    .line 664
    iget-object v0, p0, Laqi;->b:Laqc;

    invoke-static {v0}, Laqc;->f(Laqc;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    .line 665
    invoke-interface {v0, v2}, Laqv;->b(Z)V

    .line 667
    if-eqz p1, :cond_4

    .line 668
    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-static {v1}, Laqc;->r(Laqc;)Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-static {v1}, Laqc;->b(Laqc;)Lawf;

    move-result-object v1

    invoke-interface {v1}, Lawf;->aC()Z

    move-result v1

    if-nez v1, :cond_3

    .line 669
    invoke-interface {v0, p1}, Laqv;->b(Landroid/graphics/Bitmap;)V

    .line 670
    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-static {v1, v2}, Laqc;->d(Laqc;Z)Z

    .line 686
    :goto_1
    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-static {v1}, Laqc;->b(Laqc;)Lawf;

    move-result-object v1

    invoke-interface {v1, v2}, Lawf;->h(Z)V

    .line 687
    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-static {v1, v2}, Laqc;->c(Laqc;Z)Z

    .line 688
    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-static {v1, v2}, Laqc;->b(Laqc;Z)Z

    .line 689
    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-static {v1, v2}, Laqc;->e(Laqc;Z)Z

    .line 691
    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-static {v1}, Laqc;->b(Laqc;)Lawf;

    move-result-object v1

    invoke-interface {v1}, Lawf;->aC()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 692
    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-static {v1, v2}, Laqc;->d(Laqc;Z)Z

    .line 693
    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-virtual {v1}, Laqc;->h()V

    .line 694
    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-static {v1}, Laqc;->i(Laqc;)Laqm;

    move-result-object v1

    invoke-interface {v1}, Laqm;->b()V

    .line 700
    :cond_2
    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-static {v1}, Laqc;->b(Laqc;)Lawf;

    move-result-object v1

    invoke-interface {v1}, Lawf;->aZ()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 701
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Laqv;->b(Laxs;)V

    goto :goto_0

    .line 672
    :cond_3
    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-static {v1}, Laqc;->q(Laqc;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 673
    invoke-interface {v0, p1, v3}, Laqv;->a(Landroid/graphics/Bitmap;I)V

    .line 679
    :goto_2
    invoke-interface {v0, v3}, Laqv;->d_(Z)V

    .line 680
    :cond_4
    invoke-interface {v0, v2}, Laqv;->c(Z)V

    goto :goto_1

    .line 674
    :cond_5
    iget-object v1, p0, Laqi;->b:Laqc;

    invoke-static {v1}, Laqc;->s(Laqc;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 675
    const/4 v1, 0x5

    invoke-interface {v0, p1, v1}, Laqv;->a(Landroid/graphics/Bitmap;I)V

    goto :goto_2

    .line 677
    :cond_6
    invoke-interface {v0, p1, v2}, Laqv;->a(Landroid/graphics/Bitmap;I)V

    goto :goto_2
.end method

.method public a(Lcdx;)V
    .locals 1

    .prologue
    .line 799
    iget-object v0, p0, Laqi;->b:Laqc;

    invoke-static {v0}, Laqc;->c(Laqc;)Lcdu;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcdu;->a(Lcdx;)V

    .line 800
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 781
    iget-object v0, p0, Laqi;->b:Laqc;

    iget-boolean v0, v0, Lamn;->i:Z

    if-nez v0, :cond_0

    .line 785
    :goto_0
    return-void

    .line 784
    :cond_0
    iget-object v0, p0, Laqi;->b:Laqc;

    invoke-static {v0}, Laqc;->f(Laqc;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Laqv;->c(Z)V

    goto :goto_0
.end method

.method public b(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 765
    iget-object v0, p0, Laqi;->b:Laqc;

    iget-boolean v0, v0, Lamn;->i:Z

    if-nez v0, :cond_1

    .line 777
    :cond_0
    :goto_0
    return-void

    .line 769
    :cond_1
    if-eqz p1, :cond_0

    .line 772
    iput-object p1, p0, Laqi;->a:Landroid/graphics/Bitmap;

    .line 773
    iget-object v0, p0, Laqi;->b:Laqc;

    invoke-static {v0}, Laqc;->f(Laqc;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    .line 774
    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Laqv;->a(Landroid/graphics/Bitmap;I)V

    .line 775
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Laqv;->d_(Z)V

    goto :goto_0
.end method
