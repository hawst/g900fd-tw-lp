.class public final Lbzj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbzf;


# instance fields
.field private final a:Lbzf;

.field private final b:Lbzk;


# direct methods
.method public constructor <init>(Lbzf;)V
    .locals 2

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Lbzk;

    invoke-direct {v0}, Lbzk;-><init>()V

    iput-object v0, p0, Lbzj;->b:Lbzk;

    .line 29
    const-string v0, "delegate"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzf;

    iput-object v0, p0, Lbzj;->a:Lbzf;

    .line 30
    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;Ljava/util/List;)J
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbon;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmw;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 50
    iget-object v0, p0, Lbzj;->a:Lbzf;

    invoke-interface {v0, p1, p2}, Lbzf;->a(Ljava/util/List;Ljava/util/List;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Ljava/util/List;Ljava/util/List;Ljava/util/Map;Lbor;Lbzg;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbon;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmw;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;",
            "Lbor;",
            "Lbzg;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    invoke-virtual {p4}, Lbor;->e()J

    move-result-wide v0

    const-string v2, "targetDurationUs"

    invoke-static {v0, v1, v2}, Lcec;->a(JLjava/lang/CharSequence;)J

    .line 37
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "photos or videos non-empty"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 39
    iget-object v0, p0, Lbzj;->a:Lbzf;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lbzf;->a(Ljava/util/List;Ljava/util/List;Ljava/util/Map;Lbor;Lbzg;)Ljava/util/List;

    move-result-object v0

    .line 41
    invoke-virtual {p4}, Lbor;->d()Lbox;

    move-result-object v1

    .line 42
    if-eqz v1, :cond_1

    .line 43
    iget-object v2, p0, Lbzj;->b:Lbzk;

    invoke-virtual {v2, p3, v1, v0}, Lbzk;->a(Ljava/util/Map;Lbox;Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 45
    :cond_1
    return-object v0

    .line 37
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 61
    if-ne p0, p1, :cond_0

    .line 62
    const/4 v0, 0x1

    .line 75
    :goto_0
    return v0

    .line 64
    :cond_0
    instance-of v0, p1, Lbzj;

    if-nez v0, :cond_1

    .line 65
    const/4 v0, 0x0

    goto :goto_0

    .line 67
    :cond_1
    check-cast p1, Lbzj;

    .line 75
    iget-object v0, p0, Lbzj;->a:Lbzf;

    iget-object v1, p1, Lbzj;->a:Lbzf;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lbzj;->a:Lbzf;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 80
    const-string v0, "ReorderingClipSelector [delegate=%s]"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lbzj;->a:Lbzf;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
