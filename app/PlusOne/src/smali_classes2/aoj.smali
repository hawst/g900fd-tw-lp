.class public Laoj;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Lbue;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lawx;

.field private final c:Laok;

.field private final d:Ljava/lang/String;

.field private final e:Ljed;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Laoj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Laoj;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Laok;Lawx;Ljava/lang/String;Ljed;)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 43
    iput-object p1, p0, Laoj;->c:Laok;

    .line 44
    const-string v0, "editingSessionStore"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawx;

    iput-object v0, p0, Laoj;->b:Lawx;

    .line 45
    iput-object p3, p0, Laoj;->d:Ljava/lang/String;

    .line 46
    iput-object p4, p0, Laoj;->e:Ljed;

    .line 47
    return-void
.end method


# virtual methods
.method protected varargs a()Lbue;
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Laoj;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Laoj;->b:Lawx;

    iget-object v1, p0, Laoj;->d:Ljava/lang/String;

    invoke-interface {v0, v1}, Lawx;->a(Ljava/lang/String;)Lbue;

    move-result-object v0

    .line 59
    if-eqz v0, :cond_0

    .line 60
    sget-object v1, Laoj;->a:Ljava/lang/String;

    .line 71
    :goto_0
    return-object v0

    .line 64
    :cond_0
    iget-object v0, p0, Laoj;->e:Ljed;

    if-eqz v0, :cond_1

    .line 65
    iget-object v0, p0, Laoj;->b:Lawx;

    iget-object v1, p0, Laoj;->e:Ljed;

    invoke-interface {v0, v1}, Lawx;->a(Ljed;)Lbue;

    move-result-object v0

    .line 66
    if-eqz v0, :cond_1

    .line 67
    sget-object v1, Laoj;->a:Ljava/lang/String;

    goto :goto_0

    .line 71
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lbue;)V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Laoj;->c:Laok;

    if-eqz v0, :cond_0

    .line 80
    iget-object v0, p0, Laoj;->c:Laok;

    invoke-interface {v0, p0, p1}, Laok;->a(Laoj;Lbue;)V

    .line 82
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 17
    invoke-virtual {p0}, Laoj;->a()Lbue;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lbue;

    invoke-virtual {p0, p1}, Laoj;->a(Lbue;)V

    return-void
.end method
