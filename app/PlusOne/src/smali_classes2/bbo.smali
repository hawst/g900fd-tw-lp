.class public final Lbbo;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field

.field private b:Landroid/hardware/Camera$Face;

.field private c:F

.field private d:F

.field private e:F

.field private f:F

.field private g:F

.field private h:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lbbo;->a:Ljava/util/Map;

    .line 148
    return-void
.end method

.method public constructor <init>(Landroid/hardware/Camera$Face;FFFFFF)V
    .locals 1

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lbbo;->a:Ljava/util/Map;

    .line 108
    iput-object p1, p0, Lbbo;->b:Landroid/hardware/Camera$Face;

    .line 109
    iput p2, p0, Lbbo;->c:F

    .line 110
    iput p3, p0, Lbbo;->d:F

    .line 111
    iput p4, p0, Lbbo;->e:F

    .line 112
    iput p5, p0, Lbbo;->f:F

    .line 113
    iput p6, p0, Lbbo;->g:F

    .line 114
    iput p7, p0, Lbbo;->h:F

    .line 115
    return-void
.end method

.method public static a(Lbva;)Lbbo;
    .locals 8

    .prologue
    .line 275
    iget-object v0, p0, Lbva;->b:Lbvb;

    if-nez v0, :cond_0

    const/4 v1, 0x0

    .line 277
    :goto_0
    new-instance v0, Lbbo;

    iget v2, p0, Lbva;->c:F

    iget v3, p0, Lbva;->d:F

    iget v4, p0, Lbva;->f:F

    iget v5, p0, Lbva;->e:F

    iget v6, p0, Lbva;->g:F

    iget v7, p0, Lbva;->h:F

    invoke-direct/range {v0 .. v7}, Lbbo;-><init>(Landroid/hardware/Camera$Face;FFFFFF)V

    .line 282
    return-object v0

    .line 275
    :cond_0
    new-instance v1, Landroid/hardware/Camera$Face;

    invoke-direct {v1}, Landroid/hardware/Camera$Face;-><init>()V

    iget-object v0, p0, Lbva;->b:Lbvb;

    iget-object v2, v0, Lbvb;->a:Lbvd;

    new-instance v3, Landroid/graphics/Rect;

    iget v4, v2, Lbvd;->a:I

    iget v5, v2, Lbvd;->b:I

    iget v6, v2, Lbvd;->c:I

    iget v2, v2, Lbvd;->d:I

    invoke-direct {v3, v4, v5, v6, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v3, v1, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    new-instance v2, Landroid/graphics/Point;

    iget-object v3, v0, Lbvb;->b:Lbvc;

    iget v3, v3, Lbvc;->a:I

    iget-object v4, v0, Lbvb;->b:Lbvc;

    iget v4, v4, Lbvc;->b:I

    invoke-direct {v2, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    iput-object v2, v1, Landroid/hardware/Camera$Face;->leftEye:Landroid/graphics/Point;

    new-instance v2, Landroid/graphics/Point;

    iget-object v3, v0, Lbvb;->c:Lbvc;

    iget v3, v3, Lbvc;->a:I

    iget-object v4, v0, Lbvb;->c:Lbvc;

    iget v4, v4, Lbvc;->b:I

    invoke-direct {v2, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    iput-object v2, v1, Landroid/hardware/Camera$Face;->rightEye:Landroid/graphics/Point;

    new-instance v2, Landroid/graphics/Point;

    iget-object v3, v0, Lbvb;->d:Lbvc;

    iget v3, v3, Lbvc;->a:I

    iget-object v4, v0, Lbvb;->d:Lbvc;

    iget v4, v4, Lbvc;->b:I

    invoke-direct {v2, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    iput-object v2, v1, Landroid/hardware/Camera$Face;->mouth:Landroid/graphics/Point;

    iget v0, v0, Lbvb;->e:I

    iput v0, v1, Landroid/hardware/Camera$Face;->id:I

    iget v0, p0, Lbva;->g:F

    float-to-int v0, v0

    iput v0, v1, Landroid/hardware/Camera$Face;->score:I

    goto :goto_0
.end method

.method private g(F)F
    .locals 10

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 204
    const/high16 v0, 0x41f00000    # 30.0f

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 209
    const/high16 v1, -0x3e100000    # -30.0f

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 210
    invoke-static {v8, v9}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    add-double/2addr v2, v8

    div-double v2, v8, v2

    double-to-float v1, v2

    .line 211
    const-wide v2, -0x4046666666666666L    # -0.1

    float-to-double v4, v0

    const-wide/high16 v6, 0x4024000000000000L    # 10.0

    sub-double/2addr v4, v6

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->exp(D)D

    move-result-wide v2

    add-double/2addr v2, v8

    div-double v2, v8, v2

    double-to-float v0, v2

    sub-float/2addr v0, v1

    return v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)F
    .locals 3

    .prologue
    .line 445
    iget-object v0, p0, Lbbo;->a:Ljava/util/Map;

    const-string v1, "detection_score"

    invoke-virtual {p0}, Lbbo;->e()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lbbo;->a:Ljava/util/Map;

    const-string v1, "eyes_open_score"

    invoke-virtual {p0}, Lbbo;->d()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lbbo;->a:Ljava/util/Map;

    const-string v1, "illumination_score"

    invoke-virtual {p0}, Lbbo;->g()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lbbo;->a:Ljava/util/Map;

    const-string v1, "sharpness_score"

    invoke-virtual {p0}, Lbbo;->f()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lbbo;->a:Ljava/util/Map;

    const-string v1, "smile_score"

    invoke-virtual {p0}, Lbbo;->c()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lbbo;->a:Ljava/util/Map;

    const-string v1, "framing_score"

    invoke-virtual {p0}, Lbbo;->h()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    iget-object v0, p0, Lbbo;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    return v0
.end method

.method public a()Lbva;
    .locals 7

    .prologue
    .line 218
    new-instance v0, Lbva;

    invoke-direct {v0}, Lbva;-><init>()V

    .line 219
    invoke-virtual {p0}, Lbbo;->c()F

    move-result v1

    iput v1, v0, Lbva;->c:F

    .line 220
    invoke-virtual {p0}, Lbbo;->d()F

    move-result v1

    iput v1, v0, Lbva;->d:F

    .line 221
    invoke-virtual {p0}, Lbbo;->f()F

    move-result v1

    iput v1, v0, Lbva;->e:F

    .line 222
    invoke-virtual {p0}, Lbbo;->g()F

    move-result v1

    iput v1, v0, Lbva;->f:F

    .line 223
    invoke-virtual {p0}, Lbbo;->e()F

    move-result v1

    iput v1, v0, Lbva;->g:F

    .line 224
    invoke-virtual {p0}, Lbbo;->h()F

    move-result v1

    iput v1, v0, Lbva;->h:F

    .line 226
    invoke-virtual {p0}, Lbbo;->b()Landroid/hardware/Camera$Face;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v1, Lbvb;

    invoke-direct {v1}, Lbvb;-><init>()V

    new-instance v2, Lbvd;

    invoke-direct {v2}, Lbvd;-><init>()V

    new-instance v3, Lbvc;

    invoke-direct {v3}, Lbvc;-><init>()V

    new-instance v4, Lbvc;

    invoke-direct {v4}, Lbvc;-><init>()V

    new-instance v5, Lbvc;

    invoke-direct {v5}, Lbvc;-><init>()V

    invoke-virtual {p0}, Lbbo;->b()Landroid/hardware/Camera$Face;

    move-result-object v6

    iget-object v6, v6, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    iput v6, v2, Lbvd;->a:I

    invoke-virtual {p0}, Lbbo;->b()Landroid/hardware/Camera$Face;

    move-result-object v6

    iget-object v6, v6, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    iput v6, v2, Lbvd;->b:I

    invoke-virtual {p0}, Lbbo;->b()Landroid/hardware/Camera$Face;

    move-result-object v6

    iget-object v6, v6, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    iput v6, v2, Lbvd;->c:I

    invoke-virtual {p0}, Lbbo;->b()Landroid/hardware/Camera$Face;

    move-result-object v6

    iget-object v6, v6, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    iput v6, v2, Lbvd;->d:I

    invoke-virtual {p0}, Lbbo;->b()Landroid/hardware/Camera$Face;

    move-result-object v6

    iget-object v6, v6, Landroid/hardware/Camera$Face;->leftEye:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    iput v6, v3, Lbvc;->a:I

    invoke-virtual {p0}, Lbbo;->b()Landroid/hardware/Camera$Face;

    move-result-object v6

    iget-object v6, v6, Landroid/hardware/Camera$Face;->leftEye:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    iput v6, v3, Lbvc;->b:I

    invoke-virtual {p0}, Lbbo;->b()Landroid/hardware/Camera$Face;

    move-result-object v6

    iget-object v6, v6, Landroid/hardware/Camera$Face;->rightEye:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    iput v6, v4, Lbvc;->a:I

    invoke-virtual {p0}, Lbbo;->b()Landroid/hardware/Camera$Face;

    move-result-object v6

    iget-object v6, v6, Landroid/hardware/Camera$Face;->rightEye:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    iput v6, v4, Lbvc;->b:I

    invoke-virtual {p0}, Lbbo;->b()Landroid/hardware/Camera$Face;

    move-result-object v6

    iget-object v6, v6, Landroid/hardware/Camera$Face;->mouth:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->x:I

    iput v6, v5, Lbvc;->a:I

    invoke-virtual {p0}, Lbbo;->b()Landroid/hardware/Camera$Face;

    move-result-object v6

    iget-object v6, v6, Landroid/hardware/Camera$Face;->mouth:Landroid/graphics/Point;

    iget v6, v6, Landroid/graphics/Point;->y:I

    iput v6, v5, Lbvc;->b:I

    iput-object v2, v1, Lbvb;->a:Lbvd;

    iput-object v3, v1, Lbvb;->b:Lbvc;

    iput-object v4, v1, Lbvb;->c:Lbvc;

    iput-object v5, v1, Lbvb;->d:Lbvc;

    invoke-virtual {p0}, Lbbo;->b()Landroid/hardware/Camera$Face;

    move-result-object v2

    iget v2, v2, Landroid/hardware/Camera$Face;->id:I

    iput v2, v1, Lbvb;->e:I

    iput-object v1, v0, Lbva;->b:Lbvb;

    .line 227
    :cond_0
    return-object v0
.end method

.method public a(F)V
    .locals 1

    .prologue
    .line 328
    iget v0, p0, Lbbo;->c:F

    mul-float/2addr v0, p1

    iput v0, p0, Lbbo;->c:F

    .line 329
    iget v0, p0, Lbbo;->d:F

    mul-float/2addr v0, p1

    iput v0, p0, Lbbo;->d:F

    .line 330
    iget v0, p0, Lbbo;->e:F

    mul-float/2addr v0, p1

    iput v0, p0, Lbbo;->e:F

    .line 331
    iget v0, p0, Lbbo;->f:F

    mul-float/2addr v0, p1

    iput v0, p0, Lbbo;->f:F

    .line 332
    iget v0, p0, Lbbo;->g:F

    mul-float/2addr v0, p1

    iput v0, p0, Lbbo;->g:F

    .line 333
    iget v0, p0, Lbbo;->h:F

    mul-float/2addr v0, p1

    iput v0, p0, Lbbo;->h:F

    .line 334
    return-void
.end method

.method public a(Landroid/hardware/Camera$Face;)V
    .locals 6

    .prologue
    const/high16 v5, 0x42c80000    # 100.0f

    .line 347
    iput-object p1, p0, Lbbo;->b:Landroid/hardware/Camera$Face;

    .line 348
    iget-object v0, p0, Lbbo;->b:Landroid/hardware/Camera$Face;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbbo;->b:Landroid/hardware/Camera$Face;

    iget-object v0, v0, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    iget-object v1, p0, Lbbo;->b:Landroid/hardware/Camera$Face;

    iget-object v1, v1, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v0, v1

    int-to-float v0, v0

    const v1, 0x3ecccccd    # 0.4f

    mul-float/2addr v0, v1

    const/high16 v1, 0x447a0000    # 1000.0f

    sub-float v0, v1, v0

    neg-float v0, v0

    iget-object v1, p0, Lbbo;->b:Landroid/hardware/Camera$Face;

    iget-object v1, v1, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    sub-float/2addr v0, v1

    iget-object v1, p0, Lbbo;->b:Landroid/hardware/Camera$Face;

    iget-object v1, v1, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    mul-float/2addr v0, v5

    invoke-direct {p0, v0}, Lbbo;->g(F)F

    move-result v0

    iget-object v1, p0, Lbbo;->b:Landroid/hardware/Camera$Face;

    iget-object v1, v1, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/lit16 v1, v1, -0x384

    int-to-float v1, v1

    iget-object v2, p0, Lbbo;->b:Landroid/hardware/Camera$Face;

    iget-object v2, v2, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    mul-float/2addr v1, v5

    invoke-direct {p0, v1}, Lbbo;->g(F)F

    move-result v1

    iget-object v2, p0, Lbbo;->b:Landroid/hardware/Camera$Face;

    iget-object v2, v2, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    rsub-int v2, v2, -0x3e8

    int-to-float v2, v2

    iget-object v3, p0, Lbbo;->b:Landroid/hardware/Camera$Face;

    iget-object v3, v3, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    mul-float/2addr v2, v5

    invoke-direct {p0, v2}, Lbbo;->g(F)F

    move-result v2

    iget-object v3, p0, Lbbo;->b:Landroid/hardware/Camera$Face;

    iget-object v3, v3, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->right:I

    add-int/lit16 v3, v3, -0x3e8

    int-to-float v3, v3

    iget-object v4, p0, Lbbo;->b:Landroid/hardware/Camera$Face;

    iget-object v4, v4, Landroid/hardware/Camera$Face;->rect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    mul-float/2addr v3, v5

    invoke-direct {p0, v3}, Lbbo;->g(F)F

    move-result v3

    add-float/2addr v0, v1

    add-float/2addr v0, v2

    add-float/2addr v0, v3

    neg-float v0, v0

    iput v0, p0, Lbbo;->h:F

    .line 349
    :cond_0
    return-void
.end method

.method public a(Lbbo;)V
    .locals 2

    .prologue
    .line 314
    iget v0, p0, Lbbo;->c:F

    iget v1, p1, Lbbo;->c:F

    add-float/2addr v0, v1

    iput v0, p0, Lbbo;->c:F

    .line 315
    iget v0, p0, Lbbo;->d:F

    iget v1, p1, Lbbo;->d:F

    add-float/2addr v0, v1

    iput v0, p0, Lbbo;->d:F

    .line 316
    iget v0, p0, Lbbo;->e:F

    iget v1, p1, Lbbo;->e:F

    add-float/2addr v0, v1

    iput v0, p0, Lbbo;->e:F

    .line 317
    iget v0, p0, Lbbo;->f:F

    iget v1, p1, Lbbo;->f:F

    add-float/2addr v0, v1

    iput v0, p0, Lbbo;->f:F

    .line 318
    iget v0, p0, Lbbo;->g:F

    iget v1, p1, Lbbo;->g:F

    add-float/2addr v0, v1

    iput v0, p0, Lbbo;->g:F

    .line 319
    iget v0, p0, Lbbo;->h:F

    iget v1, p1, Lbbo;->h:F

    add-float/2addr v0, v1

    iput v0, p0, Lbbo;->h:F

    .line 320
    return-void
.end method

.method public b()Landroid/hardware/Camera$Face;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lbbo;->b:Landroid/hardware/Camera$Face;

    return-object v0
.end method

.method public b(F)V
    .locals 0

    .prologue
    .line 362
    iput p1, p0, Lbbo;->c:F

    .line 363
    return-void
.end method

.method public c()F
    .locals 1

    .prologue
    .line 355
    iget v0, p0, Lbbo;->c:F

    return v0
.end method

.method public c(F)V
    .locals 0

    .prologue
    .line 376
    iput p1, p0, Lbbo;->d:F

    .line 377
    return-void
.end method

.method public d()F
    .locals 1

    .prologue
    .line 369
    iget v0, p0, Lbbo;->d:F

    return v0
.end method

.method public d(F)V
    .locals 0

    .prologue
    .line 390
    iput p1, p0, Lbbo;->g:F

    .line 391
    return-void
.end method

.method public e()F
    .locals 1

    .prologue
    .line 383
    iget v0, p0, Lbbo;->g:F

    return v0
.end method

.method public e(F)V
    .locals 0

    .prologue
    .line 404
    iput p1, p0, Lbbo;->f:F

    .line 405
    return-void
.end method

.method public f()F
    .locals 1

    .prologue
    .line 397
    iget v0, p0, Lbbo;->f:F

    return v0
.end method

.method public f(F)V
    .locals 0

    .prologue
    .line 418
    iput p1, p0, Lbbo;->e:F

    .line 419
    return-void
.end method

.method public g()F
    .locals 1

    .prologue
    .line 411
    iget v0, p0, Lbbo;->e:F

    return v0
.end method

.method public h()F
    .locals 1

    .prologue
    .line 425
    iget v0, p0, Lbbo;->h:F

    return v0
.end method
