.class public Lbxf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbxe;


# instance fields
.field private final a:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lbxf;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput p1, p0, Lbxf;->a:I

    .line 46
    return-void
.end method

.method private a(FFF)F
    .locals 4

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    .line 199
    neg-float v0, p2

    sub-float v1, p1, p3

    mul-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->exp(D)D

    move-result-wide v0

    add-double/2addr v0, v2

    div-double v0, v2, v0

    double-to-float v0, v0

    return v0
.end method

.method private a(Lbok;Lbmp;)J
    .locals 4

    .prologue
    .line 145
    invoke-virtual {p1}, Lbok;->c()J

    move-result-wide v0

    .line 146
    invoke-virtual {p1}, Lbok;->b()Lbmv;

    move-result-object v2

    sget-object v3, Lbmv;->b:Lbmv;

    if-ne v2, v3, :cond_0

    .line 151
    :goto_0
    return-wide v0

    .line 150
    :cond_0
    invoke-virtual {p1}, Lbok;->d()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 151
    iget-wide v2, p2, Lbmp;->b:J

    add-long/2addr v0, v2

    goto :goto_0
.end method

.method private b(Lbok;Lbmp;)J
    .locals 4

    .prologue
    .line 161
    invoke-virtual {p1}, Lbok;->c()J

    move-result-wide v0

    .line 162
    invoke-virtual {p1}, Lbok;->b()Lbmv;

    move-result-object v2

    sget-object v3, Lbmv;->b:Lbmv;

    if-ne v2, v3, :cond_0

    .line 167
    :goto_0
    return-wide v0

    .line 166
    :cond_0
    invoke-virtual {p1}, Lbok;->d()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 167
    iget-wide v2, p2, Lbmp;->c:J

    add-long/2addr v0, v2

    goto :goto_0
.end method


# virtual methods
.method public a(Lbok;Lbmr;Lbok;Lbmr;)F
    .locals 8

    .prologue
    const/high16 v4, 0x3e800000    # 0.25f

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    .line 52
    invoke-virtual {p0, p1, p2, p3, p4}, Lbxf;->c(Lbok;Lbmr;Lbok;Lbmr;)J

    move-result-wide v0

    .line 53
    iget v2, p0, Lbxf;->a:I

    if-nez v2, :cond_1

    .line 54
    long-to-float v0, v0

    const v1, 0x49742400    # 1000000.0f

    div-float/2addr v0, v1

    const/high16 v1, 0x43b40000    # 360.0f

    cmpg-float v1, v0, v1

    if-gtz v1, :cond_0

    const v1, 0x3d75c28f    # 0.06f

    const/high16 v2, 0x42480000    # 50.0f

    invoke-direct {p0, v0, v1, v2}, Lbxf;->a(FFF)F

    move-result v0

    mul-float/2addr v0, v4

    .line 56
    :goto_0
    return v0

    .line 54
    :cond_0
    const/high16 v1, 0x3f400000    # 0.75f

    const v2, 0x3ac49ba6    # 0.0015f

    const/high16 v3, 0x44e10000    # 1800.0f

    invoke-direct {p0, v0, v2, v3}, Lbxf;->a(FFF)F

    move-result v0

    mul-float/2addr v0, v1

    add-float/2addr v0, v4

    goto :goto_0

    .line 55
    :cond_1
    iget v2, p0, Lbxf;->a:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 56
    const-wide/16 v2, 0x0

    long-to-double v0, v0

    const-wide v4, 0x3eb0c6f7a0b5ed8dL    # 1.0E-6

    mul-double/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    add-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    add-double/2addr v0, v6

    invoke-static {v0, v1}, Ljava/lang/Math;->log(D)D

    move-result-wide v0

    double-to-float v0, v0

    goto :goto_0

    .line 58
    :cond_2
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Only Sigmoid Based and Log based scores are supported."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b(Lbok;Lbmr;Lbok;Lbmr;)Z
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 66
    invoke-virtual {p1}, Lbok;->a()Ljeg;

    move-result-object v0

    invoke-virtual {p3}, Lbok;->a()Ljeg;

    move-result-object v1

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 67
    invoke-virtual {p1}, Lbok;->c()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 68
    invoke-virtual {p3}, Lbok;->c()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 73
    :cond_0
    const/4 v0, 0x0

    .line 76
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c(Lbok;Lbmr;Lbok;Lbmr;)J
    .locals 8

    .prologue
    .line 86
    invoke-virtual {p2}, Lbmr;->a()Lbmp;

    move-result-object v0

    .line 87
    invoke-virtual {p4}, Lbmr;->a()Lbmp;

    move-result-object v1

    .line 88
    invoke-virtual {p1}, Lbok;->a()Ljeg;

    move-result-object v2

    invoke-virtual {p3}, Lbok;->a()Ljeg;

    move-result-object v3

    invoke-static {v2, v3}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 92
    invoke-direct {p0, p1, v0}, Lbxf;->a(Lbok;Lbmp;)J

    move-result-wide v2

    invoke-direct {p0, p1, v0}, Lbxf;->b(Lbok;Lbmp;)J

    move-result-wide v4

    invoke-direct {p0, p3, v1}, Lbxf;->a(Lbok;Lbmp;)J

    move-result-wide v6

    invoke-direct {p0, p3, v1}, Lbxf;->b(Lbok;Lbmp;)J

    move-result-wide v0

    invoke-static {v2, v3, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    sub-long v0, v2, v0

    .line 97
    :goto_0
    return-wide v0

    :cond_0
    iget-wide v2, v0, Lbmp;->b:J

    iget-wide v4, v1, Lbmp;->b:J

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iget-wide v4, v0, Lbmp;->c:J

    iget-wide v0, v1, Lbmp;->c:J

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    sub-long v0, v2, v0

    goto :goto_0
.end method
