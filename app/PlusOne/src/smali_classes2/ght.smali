.class final Lght;
.super Ljava/lang/Object;


# instance fields
.field final a:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Lghh",
            "<*>.ghm;>;"
        }
    .end annotation
.end field

.field b:I

.field c:Landroid/os/IBinder;

.field d:Landroid/content/ComponentName;

.field final synthetic e:Lghs;

.field private final f:Ljava/lang/String;

.field private final g:Lghu;

.field private h:Z


# direct methods
.method public constructor <init>(Lghs;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lght;->e:Lghs;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lght;->f:Ljava/lang/String;

    new-instance v0, Lghu;

    invoke-direct {v0, p0}, Lghu;-><init>(Lght;)V

    iput-object v0, p0, Lght;->g:Lghu;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lght;->a:Ljava/util/HashSet;

    const/4 v0, 0x0

    iput v0, p0, Lght;->b:I

    return-void
.end method


# virtual methods
.method public a()Lghu;
    .locals 1

    iget-object v0, p0, Lght;->g:Lghu;

    return-object v0
.end method

.method public a(Lghm;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lghh",
            "<*>.ghm;)V"
        }
    .end annotation

    iget-object v0, p0, Lght;->a:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lght;->h:Z

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lght;->f:Ljava/lang/String;

    return-object v0
.end method

.method public b(Lghm;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lghh",
            "<*>.ghm;)V"
        }
    .end annotation

    iget-object v0, p0, Lght;->a:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lght;->h:Z

    return v0
.end method

.method public c(Lghm;)Z
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lghh",
            "<*>.ghm;)Z"
        }
    .end annotation

    iget-object v0, p0, Lght;->a:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lght;->b:I

    return v0
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Lght;->a:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public f()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lght;->c:Landroid/os/IBinder;

    return-object v0
.end method

.method public g()Landroid/content/ComponentName;
    .locals 1

    iget-object v0, p0, Lght;->d:Landroid/content/ComponentName;

    return-object v0
.end method
