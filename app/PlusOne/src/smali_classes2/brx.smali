.class public Lbrx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbsq;
.implements Lcgq;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lbtt;

.field private final c:Lbrw;

.field private final d:Lcgw;

.field private e:Lbmc;

.field private f:Lbid;

.field private g:Lbhx;

.field private h:Z

.field private i:Lbia;

.field private j:I

.field private k:I

.field private l:Z

.field private m:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lbrx;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbrx;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbtt;Lbrw;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    const-string v0, "listener"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbtt;

    iput-object v0, p0, Lbrx;->b:Lbtt;

    .line 84
    const-string v0, "audioSink"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrw;

    iput-object v0, p0, Lbrx;->c:Lbrw;

    .line 86
    new-instance v0, Lcgw;

    sget-object v1, Lbrx;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcgw;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbrx;->d:Lcgw;

    .line 87
    return-void
.end method

.method private h()V
    .locals 5

    .prologue
    const/4 v3, 0x2

    const/4 v4, 0x0

    .line 177
    iget-object v0, p0, Lbrx;->i:Lbia;

    if-nez v0, :cond_2

    .line 178
    iget-object v0, p0, Lbrx;->d:Lcgw;

    const-string v1, "Poll audio samples"

    invoke-virtual {v0, v1}, Lcgw;->a(Ljava/lang/String;)V

    .line 179
    iget-object v0, p0, Lbrx;->g:Lbhx;

    invoke-virtual {v0}, Lbhx;->b()Lbia;

    move-result-object v0

    iput-object v0, p0, Lbrx;->i:Lbia;

    .line 180
    iget-object v0, p0, Lbrx;->d:Lcgw;

    invoke-virtual {v0}, Lcgw;->a()V

    .line 181
    iget-object v0, p0, Lbrx;->i:Lbia;

    if-nez v0, :cond_1

    .line 218
    :cond_0
    :goto_0
    return-void

    .line 185
    :cond_1
    iget-object v0, p0, Lbrx;->i:Lbia;

    iget v0, v0, Lbia;->c:I

    const-string v1, "mPendingMixerOutput.bytesPerSample"

    invoke-static {v0, v1, v3, v4}, Lcgp;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 187
    iget-object v0, p0, Lbrx;->i:Lbia;

    iget v0, v0, Lbia;->d:I

    const-string v1, "mPendingMixerOutput.sampleRate"

    const v2, 0xac44

    invoke-static {v0, v1, v2, v4}, Lcgp;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 189
    iget-object v0, p0, Lbrx;->i:Lbia;

    iget v0, v0, Lbia;->e:I

    const-string v1, "mPendingMixerOutput.channelCount"

    invoke-static {v0, v1, v3, v4}, Lcgp;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 192
    iget-object v0, p0, Lbrx;->f:Lbid;

    iget-object v1, p0, Lbrx;->i:Lbia;

    invoke-virtual {v0, v1}, Lbid;->a(Lbia;)V

    .line 194
    iget-object v0, p0, Lbrx;->i:Lbia;

    const/4 v0, 0x0

    iput v0, p0, Lbrx;->j:I

    .line 195
    iget-object v0, p0, Lbrx;->i:Lbia;

    iget v0, v0, Lbia;->g:I

    iput v0, p0, Lbrx;->k:I

    .line 198
    :cond_2
    iget-object v0, p0, Lbrx;->d:Lcgw;

    const-string v1, "Write to audio sink"

    invoke-virtual {v0, v1}, Lcgw;->a(Ljava/lang/String;)V

    .line 200
    iget-object v0, p0, Lbrx;->c:Lbrw;

    iget-object v1, p0, Lbrx;->i:Lbia;

    iget-object v1, v1, Lbia;->f:[B

    iget v2, p0, Lbrx;->j:I

    iget v3, p0, Lbrx;->k:I

    invoke-interface {v0, v1, v2, v3}, Lbrw;->a([BII)I

    move-result v0

    .line 205
    iget v1, p0, Lbrx;->j:I

    add-int/2addr v1, v0

    iput v1, p0, Lbrx;->j:I

    .line 206
    iget v1, p0, Lbrx;->k:I

    sub-int v0, v1, v0

    iput v0, p0, Lbrx;->k:I

    .line 208
    iget-object v0, p0, Lbrx;->d:Lcgw;

    invoke-virtual {v0}, Lcgw;->a()V

    .line 210
    iget v0, p0, Lbrx;->k:I

    if-nez v0, :cond_0

    .line 211
    iget-object v0, p0, Lbrx;->i:Lbia;

    iget-boolean v0, v0, Lbia;->b:Z

    .line 212
    iget-object v1, p0, Lbrx;->g:Lbhx;

    iget-object v2, p0, Lbrx;->i:Lbia;

    invoke-virtual {v1, v2}, Lbhx;->a(Lbia;)V

    .line 213
    iput-object v4, p0, Lbrx;->i:Lbia;

    .line 214
    if-eqz v0, :cond_0

    .line 215
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbrx;->h:Z

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lbrx;->c:Lbrw;

    invoke-interface {v0}, Lbrw;->c()V

    .line 153
    return-void
.end method

.method public a(Lbmc;)V
    .locals 6

    .prologue
    .line 106
    iget-object v0, p0, Lbrx;->g:Lbhx;

    const-string v1, "mMixer"

    const-string v2, "initialize must be called before replaceTimeline"

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 107
    const-string v0, "timeline"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 109
    iget-object v0, p0, Lbrx;->e:Lbmc;

    if-eqz v0, :cond_0

    .line 111
    invoke-interface {p1}, Lbmc;->a()Ljava/util/List;

    move-result-object v0

    const-string v1, "timeline.getAudioClipSequences()"

    iget-object v2, p0, Lbrx;->e:Lbmc;

    .line 112
    invoke-interface {v2}, Lbmc;->a()Ljava/util/List;

    move-result-object v2

    .line 111
    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    .line 115
    :cond_0
    iput-object p1, p0, Lbrx;->e:Lbmc;

    .line 116
    new-instance v0, Lbid;

    invoke-interface {p1}, Lbmc;->c()J

    move-result-wide v2

    const-wide/32 v4, 0x1e8480

    invoke-direct {v0, v2, v3, v4, v5}, Lbid;-><init>(JJ)V

    iput-object v0, p0, Lbrx;->f:Lbid;

    .line 117
    return-void
.end method

.method public a(Lbmc;Lbhx;)V
    .locals 3

    .prologue
    .line 90
    iget-object v0, p0, Lbrx;->g:Lbhx;

    const-string v1, "mMixer"

    const-string v2, "already initialized"

    invoke-static {v0, v1, v2}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 91
    const-string v0, "audioMixer"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhx;

    iput-object v0, p0, Lbrx;->g:Lbhx;

    .line 92
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbrx;->h:Z

    .line 93
    invoke-virtual {p0, p1}, Lbrx;->a(Lbmc;)V

    .line 94
    return-void
.end method

.method public a(J)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 121
    iget-object v1, p0, Lbrx;->i:Lbia;

    if-eqz v1, :cond_0

    .line 122
    iget-object v1, p0, Lbrx;->g:Lbhx;

    iget-object v2, p0, Lbrx;->i:Lbia;

    invoke-virtual {v1, v2}, Lbhx;->a(Lbia;)V

    .line 123
    const/4 v1, 0x0

    iput-object v1, p0, Lbrx;->i:Lbia;

    .line 125
    :cond_0
    iput v0, p0, Lbrx;->j:I

    .line 126
    iput v0, p0, Lbrx;->k:I

    .line 127
    iput-boolean v0, p0, Lbrx;->h:Z

    .line 130
    iget-object v1, p0, Lbrx;->c:Lbrw;

    invoke-interface {v1, p1, p2}, Lbrw;->a(J)V

    .line 132
    iget-object v1, p0, Lbrx;->g:Lbhx;

    invoke-virtual {v1, p1, p2}, Lbhx;->a(J)Z

    move-result v1

    if-nez v1, :cond_1

    .line 148
    :goto_0
    return v0

    .line 137
    :cond_1
    iget-object v0, p0, Lbrx;->d:Lcgw;

    const-string v1, "Pre-fill audio sink buffer"

    invoke-virtual {v0, v1}, Lcgw;->a(Ljava/lang/String;)V

    .line 138
    :goto_1
    iget-object v0, p0, Lbrx;->c:Lbrw;

    invoke-interface {v0}, Lbrw;->g()Z

    move-result v0

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lbrx;->h:Z

    if-nez v0, :cond_3

    .line 139
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 140
    new-instance v0, Ljava/lang/InterruptedException;

    const-string v1, "interrupted while seeking audio timeline"

    invoke-direct {v0, v1}, Ljava/lang/InterruptedException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 142
    :cond_2
    invoke-direct {p0}, Lbrx;->h()V

    goto :goto_1

    .line 144
    :cond_3
    iget-object v0, p0, Lbrx;->d:Lcgw;

    invoke-virtual {v0}, Lcgw;->a()V

    .line 148
    iget-object v0, p0, Lbrx;->c:Lbrw;

    invoke-interface {v0}, Lbrw;->g()Z

    move-result v0

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 222
    iget-object v0, p0, Lbrx;->i:Lbia;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lbrx;->g:Lbhx;

    iget-object v1, p0, Lbrx;->i:Lbia;

    invoke-virtual {v0, v1}, Lbhx;->a(Lbia;)V

    .line 224
    iput-object v2, p0, Lbrx;->i:Lbia;

    .line 227
    :cond_0
    iget-object v0, p0, Lbrx;->g:Lbhx;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 228
    iput-object v2, p0, Lbrx;->g:Lbhx;

    .line 230
    iput-object v2, p0, Lbrx;->e:Lbmc;

    .line 231
    iput-object v2, p0, Lbrx;->f:Lbid;

    .line 233
    iget-object v0, p0, Lbrx;->d:Lcgw;

    invoke-virtual {v0}, Lcgw;->b()V

    .line 234
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    .line 156
    iget-boolean v0, p0, Lbrx;->l:Z

    if-eqz v0, :cond_1

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    iget-boolean v0, p0, Lbrx;->h:Z

    if-eqz v0, :cond_2

    .line 161
    iget-object v0, p0, Lbrx;->c:Lbrw;

    invoke-interface {v0}, Lbrw;->e()I

    move-result v0

    if-nez v0, :cond_0

    .line 165
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbrx;->l:Z

    .line 166
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, Lbrx;->m:J

    .line 167
    iget-object v0, p0, Lbrx;->c:Lbrw;

    invoke-interface {v0}, Lbrw;->h()V

    .line 168
    iget-object v0, p0, Lbrx;->b:Lbtt;

    invoke-interface {v0}, Lbtt;->a()V

    goto :goto_0

    .line 173
    :cond_2
    invoke-direct {p0}, Lbrx;->h()V

    goto :goto_0
.end method

.method public d()J
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lbrx;->c:Lbrw;

    invoke-interface {v0}, Lbrw;->d()J

    move-result-wide v0

    return-wide v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lbrx;->c:Lbrw;

    invoke-interface {v0}, Lbrw;->b()V

    .line 244
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lbrx;->c:Lbrw;

    invoke-interface {v0}, Lbrw;->c()V

    .line 249
    return-void
.end method

.method public g()J
    .locals 6

    .prologue
    .line 257
    iget-boolean v0, p0, Lbrx;->l:Z

    if-eqz v0, :cond_0

    .line 259
    const-wide/16 v0, -0x1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    iget-wide v4, p0, Lbrx;->m:J

    sub-long/2addr v2, v4

    mul-long/2addr v0, v2

    .line 261
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lbrx;->e:Lbmc;

    invoke-interface {v0}, Lbmc;->c()J

    move-result-wide v0

    invoke-virtual {p0}, Lbrx;->d()J

    move-result-wide v2

    sub-long/2addr v0, v2

    goto :goto_0
.end method
