.class public final Lgjg;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lgfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgfo",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Lgft;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgft",
            "<",
            "Lglg;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Lgfs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgfs",
            "<",
            "Lglg;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lgft;

    invoke-direct {v0}, Lgft;-><init>()V

    sput-object v0, Lgjg;->b:Lgft;

    new-instance v0, Lgjh;

    invoke-direct {v0}, Lgjh;-><init>()V

    sput-object v0, Lgjg;->c:Lgfs;

    new-instance v0, Lgfo;

    sget-object v1, Lgjg;->c:Lgfs;

    sget-object v2, Lgjg;->b:Lgft;

    const/4 v3, 0x0

    new-array v3, v3, [Lggh;

    invoke-direct {v0, v1, v2, v3}, Lgfo;-><init>(Lgfs;Lgft;[Lggh;)V

    sput-object v0, Lgjg;->a:Lgfo;

    return-void
.end method

.method static synthetic a()Lgft;
    .locals 1

    sget-object v0, Lgjg;->b:Lgft;

    return-object v0
.end method

.method public static a(Lgfz;Lgjl;)Lggd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfz;",
            "Lgjl;",
            ")",
            "Lggd",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    new-instance v0, Lgji;

    invoke-direct {v0, p1}, Lgji;-><init>(Lgjl;)V

    invoke-interface {p0, v0}, Lgfz;->a(Lgfx;)Lgfx;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lgfz;Lgjl;)Lggd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfz;",
            "Lgjl;",
            ")",
            "Lggd",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    new-instance v0, Lgjj;

    invoke-direct {v0, p1}, Lgjj;-><init>(Lgjl;)V

    invoke-interface {p0, v0}, Lgfz;->a(Lgfx;)Lgfx;

    move-result-object v0

    return-object v0
.end method
