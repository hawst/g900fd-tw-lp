.class public final Lixi;
.super Llol;
.source "PG"

# interfaces
.implements Livw;


# instance fields
.field private N:Liwa;

.field private O:Lhei;

.field private P:Liwc;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Llol;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Liwg;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 40
    invoke-virtual {p0}, Lixi;->k()Landroid/os/Bundle;

    move-result-object v5

    .line 41
    if-eqz v5, :cond_2

    const-string v0, "account_filter"

    .line 42
    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Livq;

    move-object v1, v0

    .line 45
    :goto_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 46
    iget-object v0, p0, Lixi;->O:Lhei;

    invoke-interface {v0}, Lhei;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 47
    iget-object v8, p0, Lixi;->O:Lhei;

    invoke-interface {v8, v0}, Lhei;->a(I)Lhej;

    move-result-object v8

    .line 48
    iget-object v9, p0, Lixi;->P:Liwc;

    invoke-interface {v9, p1, v0}, Liwc;->a(Liwg;I)Z

    move-result v9

    if-eqz v9, :cond_0

    if-eqz v1, :cond_1

    .line 49
    invoke-interface {v1, v8}, Livq;->a(Lhej;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 50
    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    move-object v1, v2

    .line 42
    goto :goto_0

    .line 54
    :cond_3
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_6

    .line 55
    iget-object v0, p0, Lixi;->O:Lhei;

    invoke-interface {v0}, Lhei;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_4
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 56
    iget-object v8, p0, Lixi;->O:Lhei;

    invoke-interface {v8, v0}, Lhei;->a(I)Lhej;

    move-result-object v8

    .line 57
    if-eqz v1, :cond_5

    invoke-interface {v1, v8}, Livq;->a(Lhej;)Z

    move-result v9

    if-eqz v9, :cond_4

    :cond_5
    const-string v9, "is_managed_account"

    .line 58
    invoke-interface {v8, v9}, Lhej;->c(Ljava/lang/String;)Z

    move-result v8

    if-nez v8, :cond_4

    .line 59
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 64
    :cond_6
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_a

    .line 65
    if-eqz v5, :cond_7

    const-string v0, "add_account"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    :cond_7
    move v3, v4

    .line 66
    :cond_8
    if-eqz v3, :cond_9

    .line 67
    iget-object v0, p0, Lixi;->N:Liwa;

    invoke-interface {v0}, Liwa;->b()V

    .line 90
    :goto_3
    return-void

    .line 69
    :cond_9
    iget-object v0, p0, Lixi;->N:Liwa;

    invoke-interface {v0}, Liwa;->c()V

    goto :goto_3

    .line 71
    :cond_a
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v4, :cond_b

    .line 72
    invoke-interface {v6, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 73
    iget-object v1, p0, Lixi;->O:Lhei;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 74
    iget-object v1, p0, Lixi;->N:Liwa;

    const-string v2, "account_name"

    .line 75
    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "effective_gaia_id"

    .line 76
    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 74
    invoke-interface {v1, v2, v0}, Liwa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    .line 78
    :cond_b
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [I

    move v1, v3

    .line 79
    :goto_4
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    .line 80
    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v4, v1

    .line 79
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    .line 83
    :cond_c
    if-eqz v5, :cond_d

    const-string v0, "dialog_title"

    .line 84
    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 87
    :cond_d
    invoke-virtual {p0}, Lixi;->q()Lae;

    move-result-object v0

    .line 88
    if-eqz v2, :cond_e

    .line 87
    :goto_5
    invoke-static {v0, v2, v4, v3}, Liwt;->a(Lae;Ljava/lang/String;[IZ)V

    goto :goto_3

    .line 88
    :cond_e
    iget-object v1, p0, Lixi;->at:Llnl;

    const v2, 0x7f0a01b2

    invoke-virtual {v1, v2}, Llnl;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_5
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 32
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 33
    iget-object v0, p0, Lixi;->au:Llnh;

    const-class v1, Liwa;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwa;

    iput-object v0, p0, Lixi;->N:Liwa;

    .line 34
    iget-object v0, p0, Lixi;->au:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lixi;->O:Lhei;

    .line 35
    iget-object v0, p0, Lixi;->au:Llnh;

    const-class v1, Liwc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwc;

    iput-object v0, p0, Lixi;->P:Liwc;

    .line 36
    return-void
.end method
