.class public final Lbms;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Lbms;


# instance fields
.field private final b:[J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 23
    new-instance v0, Lbms;

    const/4 v1, 0x0

    new-array v1, v1, [J

    invoke-direct {v0, v1}, Lbms;-><init>([J)V

    sput-object v0, Lbms;->a:Lbms;

    return-void
.end method

.method constructor <init>([J)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const-string v0, "keyFrameTimestampsUs"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    iput-object v0, p0, Lbms;->b:[J

    .line 31
    return-void
.end method


# virtual methods
.method public a(J)Landroid/util/Pair;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 53
    iget-object v0, p0, Lbms;->b:[J

    array-length v0, v0

    if-nez v0, :cond_0

    .line 54
    invoke-static {v1, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 64
    :goto_0
    return-object v0

    .line 56
    :cond_0
    iget-object v0, p0, Lbms;->b:[J

    invoke-static {p1, p2, v0}, Lcgv;->b(J[J)I

    move-result v0

    .line 57
    iget-object v2, p0, Lbms;->b:[J

    invoke-static {p1, p2, v2}, Lcgv;->a(J[J)I

    move-result v2

    .line 58
    if-ltz v0, :cond_2

    iget-object v3, p0, Lbms;->b:[J

    aget-wide v4, v3, v0

    .line 59
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 61
    :goto_1
    iget-object v3, p0, Lbms;->b:[J

    array-length v3, v3

    if-ge v2, v3, :cond_1

    iget-object v1, p0, Lbms;->b:[J

    aget-wide v2, v1, v2

    .line 62
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    .line 64
    :cond_1
    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 59
    goto :goto_1
.end method

.method public a(Lbmp;)Lbms;
    .locals 5

    .prologue
    .line 153
    const/4 v0, 0x0

    iget-wide v2, p1, Lbmp;->b:J

    iget-object v1, p0, Lbms;->b:[J

    .line 155
    invoke-static {v2, v3, v1}, Lcgv;->b(J[J)I

    move-result v1

    .line 153
    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 156
    iget-object v0, p0, Lbms;->b:[J

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iget-wide v2, p1, Lbmp;->c:J

    iget-object v4, p0, Lbms;->b:[J

    .line 158
    invoke-static {v2, v3, v4}, Lcgv;->a(J[J)I

    move-result v2

    .line 156
    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 159
    if-nez v1, :cond_0

    iget-object v0, p0, Lbms;->b:[J

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    if-ne v2, v0, :cond_0

    .line 164
    :goto_0
    return-object p0

    .line 163
    :cond_0
    new-instance v0, Lbms;

    iget-object v3, p0, Lbms;->b:[J

    add-int/lit8 v2, v2, 0x1

    .line 164
    invoke-static {v3, v1, v2}, Ljava/util/Arrays;->copyOfRange([JII)[J

    move-result-object v1

    invoke-direct {v0, v1}, Lbms;-><init>([J)V

    move-object p0, v0

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lbms;->b:[J

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(J)J
    .locals 9

    .prologue
    .line 71
    iget-object v0, p0, Lbms;->b:[J

    array-length v0, v0

    if-nez v0, :cond_1

    .line 72
    const-wide/16 v0, 0x0

    .line 88
    :cond_0
    :goto_0
    return-wide v0

    .line 75
    :cond_1
    iget-object v0, p0, Lbms;->b:[J

    .line 76
    invoke-static {p1, p2, v0}, Lcgv;->b(J[J)I

    move-result v0

    .line 77
    iget-object v1, p0, Lbms;->b:[J

    invoke-static {p1, p2, v1}, Lcgv;->a(J[J)I

    move-result v2

    .line 78
    if-gez v0, :cond_2

    .line 79
    iget-object v0, p0, Lbms;->b:[J

    aget-wide v0, v0, v2

    goto :goto_0

    .line 80
    :cond_2
    iget-object v1, p0, Lbms;->b:[J

    array-length v1, v1

    if-lt v2, v1, :cond_3

    .line 81
    iget-object v1, p0, Lbms;->b:[J

    aget-wide v0, v1, v0

    goto :goto_0

    .line 83
    :cond_3
    iget-object v1, p0, Lbms;->b:[J

    aget-wide v0, v1, v0

    .line 84
    iget-object v3, p0, Lbms;->b:[J

    aget-wide v2, v3, v2

    .line 85
    sub-long v4, p1, v0

    sub-long v6, v2, p1

    cmp-long v4, v4, v6

    if-ltz v4, :cond_0

    move-wide v0, v2

    .line 88
    goto :goto_0
.end method

.method public c(J)J
    .locals 3

    .prologue
    const-wide/16 v0, 0x0

    .line 97
    iget-object v2, p0, Lbms;->b:[J

    array-length v2, v2

    if-nez v2, :cond_1

    .line 103
    :cond_0
    :goto_0
    return-wide v0

    .line 101
    :cond_1
    iget-object v2, p0, Lbms;->b:[J

    .line 102
    invoke-static {p1, p2, v2}, Lcgv;->b(J[J)I

    move-result v2

    .line 103
    if-ltz v2, :cond_0

    iget-object v0, p0, Lbms;->b:[J

    aget-wide v0, v0, v2

    goto :goto_0
.end method

.method public d(J)Ljava/lang/Long;
    .locals 3

    .prologue
    .line 136
    iget-object v0, p0, Lbms;->b:[J

    invoke-static {p1, p2, v0}, Lcgv;->a(J[J)I

    move-result v0

    .line 137
    iget-object v1, p0, Lbms;->b:[J

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lbms;->b:[J

    aget-wide v0, v1, v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
