.class public final Lbgq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:I

.field private b:Z


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput p1, p0, Lbgq;->a:I

    .line 47
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbgq;->b:Z

    .line 48
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    iput v0, p0, Lbgq;->a:I

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbgq;->b:Z

    .line 38
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 57
    instance-of v1, p1, Lbgq;

    if-nez v1, :cond_1

    .line 61
    :cond_0
    :goto_0
    return v0

    .line 60
    :cond_1
    check-cast p1, Lbgq;

    .line 61
    iget v1, p0, Lbgq;->a:I

    iget v2, p1, Lbgq;->a:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lbgq;->b:Z

    iget-boolean v2, p1, Lbgq;->b:Z

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lbgq;->a:I

    return v0
.end method
