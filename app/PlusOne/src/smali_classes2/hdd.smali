.class public final Lhdd;
.super Lhdx;
.source "PG"


# instance fields
.field private final a:Landroid/graphics/Rect;

.field private b:Landroid/graphics/Bitmap;

.field private final c:Landroid/graphics/Paint;

.field private d:F

.field private e:Landroid/graphics/Paint;

.field private g:F

.field private final h:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lhdx;-><init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V

    .line 27
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lhdd;->a:Landroid/graphics/Rect;

    .line 29
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lhdd;->c:Landroid/graphics/Paint;

    .line 30
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lhdd;->d:F

    .line 87
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lhdd;->h:Landroid/graphics/Matrix;

    .line 39
    iget-object v0, p0, Lhdd;->c:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 40
    return-void
.end method

.method private getPreviewAlpha()F
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 149
    iget v0, p0, Lhdd;->d:F

    return v0
.end method

.method private setPreviewAlpha(F)V
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 140
    iput p1, p0, Lhdd;->d:F

    .line 141
    iget-object v0, p0, Lhdd;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    .line 142
    return-void
.end method


# virtual methods
.method public a(F)V
    .locals 1

    .prologue
    .line 43
    iput p1, p0, Lhdd;->g:F

    .line 45
    iget-object v0, p0, Lhdd;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    .line 46
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 55
    const/high16 v0, -0x1000000

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    .line 56
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lhdd;->e:Landroid/graphics/Paint;

    .line 57
    iget-object v0, p0, Lhdd;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setColor(I)V

    .line 62
    :goto_0
    iget-object v0, p0, Lhdd;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    .line 63
    return-void

    .line 59
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lhdd;->e:Landroid/graphics/Paint;

    goto :goto_0
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 66
    iput-object p1, p0, Lhdd;->b:Landroid/graphics/Bitmap;

    .line 68
    if-eqz p1, :cond_0

    .line 69
    iget-object v0, p0, Lhdd;->a:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 72
    :cond_0
    iget-object v0, p0, Lhdd;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    .line 73
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 9

    .prologue
    const/high16 v8, 0x437f0000    # 255.0f

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 91
    iget-object v0, p0, Lhdd;->b:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhdd;->e:Landroid/graphics/Paint;

    if-nez v0, :cond_1

    .line 132
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    iget-object v0, p0, Lhdd;->e:Landroid/graphics/Paint;

    iget v2, p0, Lhdd;->d:F

    mul-float/2addr v2, v8

    float-to-int v2, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 97
    iget-object v0, p0, Lhdd;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b()Landroid/graphics/RectF;

    move-result-object v6

    .line 98
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 99
    :cond_2
    iget-object v0, p0, Lhdd;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    iget-object v0, p0, Lhdd;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lhdd;->e:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_0

    .line 103
    :cond_3
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    .line 104
    sget-object v0, Landroid/graphics/Region$Op;->DIFFERENCE:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v6, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;Landroid/graphics/Region$Op;)Z

    .line 105
    iget-object v0, p0, Lhdd;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    iget-object v0, p0, Lhdd;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lhdd;->e:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 106
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 108
    iget-object v0, p0, Lhdd;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    .line 109
    iget-object v0, p0, Lhdd;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    .line 111
    int-to-float v2, v1

    div-float/2addr v2, v7

    .line 112
    int-to-float v0, v0

    div-float v3, v0, v7

    .line 114
    iget-object v0, p0, Lhdd;->h:Landroid/graphics/Matrix;

    neg-float v4, v2

    neg-float v5, v3

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 115
    iget-object v0, p0, Lhdd;->h:Landroid/graphics/Matrix;

    iget v4, p0, Lhdd;->g:F

    invoke-virtual {v0, v4}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 117
    iget v0, p0, Lhdd;->g:F

    const/high16 v4, 0x42b40000    # 90.0f

    cmpl-float v0, v0, v4

    if-eqz v0, :cond_4

    iget v0, p0, Lhdd;->g:F

    const/high16 v4, 0x43870000    # 270.0f

    cmpl-float v0, v0, v4

    if-nez v0, :cond_5

    :cond_4
    const/4 v0, 0x1

    .line 119
    :goto_1
    if-eqz v0, :cond_6

    .line 120
    iget-object v4, p0, Lhdd;->h:Landroid/graphics/Matrix;

    invoke-virtual {v4, v3, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 125
    :goto_2
    iget-object v2, p0, Lhdd;->h:Landroid/graphics/Matrix;

    iget v3, v6, Landroid/graphics/RectF;->left:F

    iget v4, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 126
    if-eqz v0, :cond_7

    .line 127
    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    .line 128
    :goto_3
    iget-object v1, p0, Lhdd;->h:Landroid/graphics/Matrix;

    iget v2, v6, Landroid/graphics/RectF;->left:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {v1, v0, v0, v2, v3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 130
    iget-object v0, p0, Lhdd;->c:Landroid/graphics/Paint;

    iget v1, p0, Lhdd;->d:F

    mul-float/2addr v1, v8

    float-to-int v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 131
    iget-object v0, p0, Lhdd;->b:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lhdd;->h:Landroid/graphics/Matrix;

    iget-object v2, p0, Lhdd;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto/16 :goto_0

    .line 117
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    .line 122
    :cond_6
    iget-object v4, p0, Lhdd;->h:Landroid/graphics/Matrix;

    invoke-virtual {v4, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_2

    .line 127
    :cond_7
    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    goto :goto_3
.end method

.method public a(Z)V
    .locals 4

    .prologue
    .line 76
    if-eqz p1, :cond_0

    .line 77
    const-string v0, "previewAlpha"

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-static {p0, v0, v1}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 78
    new-instance v1, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 79
    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setDuration(J)Landroid/animation/Animator;

    .line 80
    new-instance v1, Lhde;

    invoke-direct {v1, p0}, Lhde;-><init>(Lhdd;)V

    invoke-virtual {v0, v1}, Landroid/animation/Animator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 81
    invoke-virtual {v0}, Landroid/animation/Animator;->start()V

    .line 85
    :goto_0
    return-void

    .line 83
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhdd;->a(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 77
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method
