.class public final Lcbd;
.super Lcba;
.source "PG"


# instance fields
.field final d:Landroid/graphics/Point;

.field final e:Landroid/graphics/Point;

.field final f:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

.field final g:I

.field h:Z

.field i:F


# direct methods
.method public constructor <init>(Lcam;Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;Lcao;)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 53
    invoke-direct {p0, p1}, Lcba;-><init>(Lcam;)V

    .line 21
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcbd;->d:Landroid/graphics/Point;

    .line 27
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, v1, v1}, Landroid/graphics/Point;-><init>(II)V

    iput-object v0, p0, Lcbd;->e:Landroid/graphics/Point;

    .line 54
    iput-object p2, p0, Lcbd;->f:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

    .line 55
    invoke-virtual {p2}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0090

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcbd;->g:I

    .line 57
    new-instance v0, Lcbe;

    invoke-direct {v0, p0}, Lcbe;-><init>(Lcbd;)V

    invoke-interface {p1, v0}, Lcam;->a(Lcan;)V

    .line 79
    new-instance v0, Lcah;

    new-instance v1, Lcbf;

    invoke-direct {v1, p0, p1}, Lcbf;-><init>(Lcbd;Lcam;)V

    invoke-direct {v0, v1}, Lcah;-><init>(Lcaj;)V

    .line 89
    iget-object v1, p0, Lcbd;->f:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

    new-instance v2, Lcbg;

    invoke-direct {v2, p0, v0}, Lcbg;-><init>(Lcbd;Lcah;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a(Lcap;)V

    .line 121
    iget-object v1, p0, Lcbd;->f:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

    new-instance v2, Lcbh;

    invoke-direct {v2, p0, p1, v0, p3}, Lcbh;-><init>(Lcbd;Lcam;Lcah;Lcao;)V

    invoke-virtual {v1, v2}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a(Lcao;)V

    .line 157
    return-void
.end method
