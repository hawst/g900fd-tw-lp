.class public final Lhna;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkfi;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Lkff;)Lkff;
    .locals 5

    .prologue
    .line 39
    const-string v0, "oauth2:https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.circles.write https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/plus.photos.readwrite https://www.googleapis.com/auth/plus.native"

    invoke-virtual {p2, v0}, Lkff;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    instance-of v0, p2, Lhnd;

    if-nez v0, :cond_1

    .line 41
    iget-object v1, p2, Lkff;->g:Lkfo;

    invoke-virtual {v1}, Lkfo;->a()Ljava/lang/String;

    move-result-object v2

    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, v2}, Lhei;->a(Ljava/lang/String;)I

    move-result v0

    const/4 v2, -0x1

    if-eq v0, v2, :cond_2

    invoke-static {p1, v0}, Lhnb;->b(Landroid/content/Context;I)Ljava/util/List;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    const-string v0, "LogAppender"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x27

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "ClientOzEvents piggybacked: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    new-instance v0, Lhnd;

    invoke-direct {v0, p1, v1, v2}, Lhnd;-><init>(Landroid/content/Context;Lkfo;Ljava/util/List;)V

    move-object v1, v0

    .line 42
    :goto_0
    if-eqz v1, :cond_1

    .line 43
    iget-object v0, p2, Lkff;->g:Lkfo;

    invoke-static {p1, v0}, Lkgi;->a(Landroid/content/Context;Lkfo;)Lkfu;

    move-result-object v0

    .line 44
    invoke-virtual {v0, p2}, Lkfu;->a(Lkff;)V

    .line 45
    invoke-virtual {v0, v1}, Lkfu;->a(Lkff;)V

    move-object p2, v0

    .line 50
    :cond_1
    return-object p2

    .line 41
    :cond_2
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0
.end method
