.class public final Ljzv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljxt;
.implements Llnx;
.implements Llrg;


# instance fields
.field private a:Landroid/content/Context;

.field private b:Lhoc;


# direct methods
.method constructor <init>(Llqr;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 27
    return-void
.end method

.method constructor <init>(Llqr;B)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-virtual {p1, p0}, Llqr;->a(Llrg;)Llrg;

    .line 31
    return-void
.end method

.method private a()Lhoc;
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Ljzv;->b:Lhoc;

    if-nez v0, :cond_0

    .line 72
    iget-object v0, p0, Ljzv;->a:Landroid/content/Context;

    const-class v1, Lhoc;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Ljzv;->b:Lhoc;

    .line 74
    :cond_0
    iget-object v0, p0, Ljzv;->b:Lhoc;

    return-object v0
.end method


# virtual methods
.method public a(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 56
    new-instance v0, Ljzd;

    iget-object v1, p0, Ljzv;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2, p3}, Ljzd;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-direct {p0}, Ljzv;->a()Lhoc;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    .line 59
    invoke-direct {p0}, Ljzv;->a()Lhoc;

    move-result-object v1

    invoke-virtual {v1}, Lhoc;->d()Lhos;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhos;->a(Lhny;)V

    .line 60
    return-void
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;IIZ)V
    .locals 9

    .prologue
    .line 50
    invoke-direct {p0}, Ljzv;->a()Lhoc;

    move-result-object v8

    new-instance v0, Ljyi;

    iget-object v1, p0, Ljzv;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Ljyi;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;IIZ)V

    invoke-virtual {v8, v0}, Lhoc;->b(Lhny;)V

    .line 52
    return-void
.end method

.method public a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)V
    .locals 10

    .prologue
    .line 41
    new-instance v5, Lngl;

    invoke-direct {v5}, Lngl;-><init>()V

    .line 42
    iput-object p4, v5, Lngl;->a:Ljava/lang/String;

    .line 43
    invoke-direct {p0}, Ljzv;->a()Lhoc;

    move-result-object v9

    new-instance v0, Ljzy;

    iget-object v1, p0, Ljzv;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Ljzy;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lngl;IIZ)V

    invoke-virtual {v9, v0}, Lhoc;->b(Lhny;)V

    .line 45
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Ljzv;->a:Landroid/content/Context;

    .line 36
    return-void
.end method

.method public b(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 64
    new-instance v0, Ljzs;

    iget-object v1, p0, Ljzv;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1, p2, p3}, Ljzs;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-direct {p0}, Ljzv;->a()Lhoc;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    .line 67
    invoke-direct {p0}, Ljzv;->a()Lhoc;

    move-result-object v1

    invoke-virtual {v1}, Lhoc;->d()Lhos;

    move-result-object v1

    invoke-virtual {v1, v0}, Lhos;->a(Lhny;)V

    .line 68
    return-void
.end method
