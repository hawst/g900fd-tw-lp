.class public final Lazf;
.super Layr;
.source "PG"


# static fields
.field private static final a:Landroid/animation/TimeInterpolator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    sput-object v0, Lazf;->a:Landroid/animation/TimeInterpolator;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/32 v4, 0x61a80

    .line 29
    new-instance v0, Lays;

    invoke-direct {v0}, Lays;-><init>()V

    const-wide/32 v2, 0xf4240

    .line 30
    invoke-virtual {v0, v2, v3}, Lays;->a(J)Lays;

    move-result-object v0

    .line 31
    invoke-virtual {v0, v4, v5}, Lays;->b(J)Lays;

    move-result-object v0

    .line 32
    invoke-virtual {v0, v4, v5}, Lays;->c(J)Lays;

    move-result-object v0

    .line 33
    invoke-virtual {v0, v4, v5}, Lays;->d(J)Lays;

    move-result-object v0

    const v1, 0x7f020427

    .line 34
    invoke-virtual {v0, v1}, Lays;->a(I)Lays;

    move-result-object v0

    sget-object v1, Layc;->c:Layc;

    .line 35
    invoke-virtual {v0, v1}, Lays;->a(Layc;)Lays;

    move-result-object v0

    const/4 v1, 0x4

    .line 36
    invoke-virtual {v0, v1}, Lays;->e(I)Lays;

    move-result-object v0

    .line 29
    invoke-direct {p0, v0}, Layr;-><init>(Lays;)V

    .line 38
    return-void
.end method


# virtual methods
.method protected a(Layj;Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 43
    new-instance v0, Lazg;

    new-instance v1, Layu;

    invoke-direct {v1}, Layu;-><init>()V

    .line 44
    invoke-virtual {v1, p1}, Layu;->a(Layj;)Layu;

    move-result-object v1

    const v2, 0x7f0b00a5

    .line 45
    invoke-virtual {p3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Layu;->a(I)Layu;

    move-result-object v1

    const/high16 v2, 0x42100000    # 36.0f

    .line 46
    invoke-virtual {v1, v2}, Layu;->a(F)Layu;

    move-result-object v1

    const/high16 v2, 0x42300000    # 44.0f

    .line 47
    invoke-virtual {v1, v2}, Layu;->b(F)Layu;

    move-result-object v1

    const-string v2, "sans-serif-condensed"

    .line 48
    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Layu;->a(Landroid/graphics/Typeface;)Layu;

    move-result-object v1

    .line 49
    invoke-virtual {v1, v3}, Layu;->a(Z)Layu;

    move-result-object v1

    const/16 v2, 0x19

    .line 50
    invoke-virtual {v1, v2}, Layu;->c(I)Layu;

    move-result-object v1

    const/4 v2, 0x1

    .line 51
    invoke-virtual {v1, v2}, Layu;->b(Z)Layu;

    move-result-object v1

    const v2, 0x7f0b00b3

    .line 52
    invoke-virtual {p3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Layu;->g(I)Layu;

    move-result-object v1

    const v2, 0x3d99999a    # 0.075f

    .line 53
    invoke-virtual {v1, v2}, Layu;->c(F)Layu;

    move-result-object v1

    const-wide/32 v2, 0xf4240

    .line 54
    invoke-virtual {v1, v2, v3}, Layu;->b(J)Layu;

    move-result-object v1

    invoke-direct {v0, v1}, Lazg;-><init>(Layu;)V

    return-object v0
.end method

.method public a(Layl;)Z
    .locals 1

    .prologue
    .line 76
    invoke-static {p1}, Lbag;->d(Layl;)Z

    move-result v0

    return v0
.end method

.method public b(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 94
    invoke-static {p1}, Lbag;->d(Layl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {p0}, Lazf;->a()Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->b(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 97
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Layr;->b(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    goto :goto_0
.end method

.method public c(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 81
    invoke-virtual {p0}, Lazf;->a()Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->c(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method public c(Lbmd;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbmd;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Double;

    const/4 v1, 0x0

    const-wide/high16 v2, -0x3ff0000000000000L    # -4.0

    const-wide/high16 v4, 0x4010000000000000L    # 4.0

    new-instance v6, Ljava/util/Random;

    .line 65
    invoke-virtual {p1}, Lbmd;->hashCode()I

    move-result v7

    int-to-long v8, v7

    invoke-direct {v6, v8, v9}, Ljava/util/Random;-><init>(J)V

    .line 64
    invoke-static {v2, v3, v4, v5, v6}, Lbag;->a(DDLjava/util/Random;)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public d(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 86
    invoke-static {p1}, Lbag;->d(Layl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    invoke-virtual {p0}, Lazf;->a()Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->d(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 89
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Layr;->d(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    goto :goto_0
.end method

.method protected f()Layj;
    .locals 7

    .prologue
    .line 70
    new-instance v0, Layo;

    const v2, 0x3f866666    # 1.05f

    const v3, 0x3f99999a    # 1.2f

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Layo;-><init>(Layj;FFLandroid/animation/TimeInterpolator;IZ)V

    return-object v0
.end method

.method protected g()Layj;
    .locals 1

    .prologue
    .line 59
    new-instance v0, Lbae;

    invoke-direct {v0}, Lbae;-><init>()V

    return-object v0
.end method

.method public j(Layl;)I
    .locals 1

    .prologue
    .line 102
    invoke-static {p1}, Lbag;->d(Layl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {p0}, Lazf;->a()Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->j(Layl;)I

    move-result v0

    .line 105
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 118
    invoke-static {p1}, Lbag;->d(Layl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    invoke-virtual {p0}, Lazf;->a()Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->k(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 121
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Layr;->k(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    goto :goto_0
.end method

.method public l(Layl;)F
    .locals 1

    .prologue
    .line 110
    invoke-static {p1}, Lbag;->d(Layl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 111
    invoke-virtual {p0}, Lazf;->a()Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->l(Layl;)F

    move-result v0

    .line 113
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v(Layl;)F
    .locals 6

    .prologue
    const-wide/32 v4, 0xf4240

    const/high16 v1, 0x3f800000    # 1.0f

    .line 126
    iget-boolean v0, p1, Layl;->w:Z

    if-eqz v0, :cond_0

    .line 127
    const/4 v0, 0x0

    .line 135
    :goto_0
    return v0

    .line 130
    :cond_0
    iget v0, p1, Layl;->f:I

    if-eqz v0, :cond_1

    move v0, v1

    .line 131
    :goto_1
    cmpg-float v2, v0, v1

    if-gez v2, :cond_2

    .line 132
    sget-object v2, Lazf;->a:Landroid/animation/TimeInterpolator;

    sub-float v0, v1, v0

    invoke-interface {v2, v0}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v0

    goto :goto_0

    .line 130
    :cond_1
    iget-wide v2, p1, Layl;->v:J

    sub-long/2addr v2, v4

    long-to-float v0, v2

    const/high16 v2, 0x3e800000    # 0.25f

    mul-float/2addr v0, v2

    float-to-long v2, v0

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    iget-wide v4, p1, Layl;->a:J

    long-to-float v0, v4

    mul-float/2addr v0, v1

    long-to-float v2, v2

    div-float/2addr v0, v2

    invoke-static {v0}, Lcfn;->a(F)F

    move-result v0

    goto :goto_1

    .line 135
    :cond_2
    invoke-super {p0, p1}, Layr;->v(Layl;)F

    move-result v0

    neg-float v0, v0

    goto :goto_0
.end method
