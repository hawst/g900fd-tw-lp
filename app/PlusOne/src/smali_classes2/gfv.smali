.class public abstract Lgfv;
.super Ljava/lang/Object;

# interfaces
.implements Lgfy;
.implements Lggd;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R::",
        "Lggf;",
        ">",
        "Ljava/lang/Object;",
        "Lgfy",
        "<TR;>;",
        "Lggd",
        "<TR;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/concurrent/CountDownLatch;

.field final b:Ljava/lang/Object;

.field public c:Lgfw;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgfw",
            "<TR;>;"
        }
    .end annotation
.end field

.field d:Z

.field private final e:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private f:Lggg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lggg",
            "<TR;>;"
        }
    .end annotation
.end field

.field private volatile g:Lggf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TR;"
        }
    .end annotation
.end field

.field private volatile h:Z

.field private i:Z


# direct methods
.method constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lgfv;->b:Ljava/lang/Object;

    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lgfv;->a:Ljava/util/concurrent/CountDownLatch;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lgfv;->e:Ljava/util/ArrayList;

    return-void
.end method

.method private a()Lggf;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TR;"
        }
    .end annotation

    iget-object v1, p0, Lgfv;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lgfv;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Result has already been consumed."

    invoke-static {v0, v2}, Lgih;->a(ZLjava/lang/Object;)V

    invoke-virtual {p0}, Lgfv;->b()Z

    move-result v0

    const-string v2, "Result is not ready."

    invoke-static {v0, v2}, Lgih;->a(ZLjava/lang/Object;)V

    iget-object v0, p0, Lgfv;->g:Lggf;

    invoke-virtual {p0}, Lgfv;->f()V

    monitor-exit v1

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private b(Lggf;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    iput-object p1, p0, Lgfv;->g:Lggf;

    iget-object v0, p0, Lgfv;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    iget-object v0, p0, Lgfv;->g:Lggf;

    invoke-interface {v0}, Lggf;->a()Lcom/google/android/gms/common/api/Status;

    iget-object v0, p0, Lgfv;->f:Lggg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgfv;->c:Lgfw;

    invoke-virtual {v0}, Lgfw;->a()V

    iget-boolean v0, p0, Lgfv;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lgfv;->c:Lgfw;

    iget-object v1, p0, Lgfv;->f:Lggg;

    invoke-direct {p0}, Lgfv;->a()Lggf;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lgfw;->a(Lggg;Lggf;)V

    :cond_0
    iget-object v0, p0, Lgfv;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lgfv;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method


# virtual methods
.method protected a(Lgfw;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfw",
            "<TR;>;)V"
        }
    .end annotation

    iput-object p1, p0, Lgfv;->c:Lgfw;

    return-void
.end method

.method public final a(Lggf;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)V"
        }
    .end annotation

    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v3, p0, Lgfv;->b:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-boolean v2, p0, Lgfv;->d:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lgfv;->i:Z

    if-eqz v2, :cond_1

    :cond_0
    invoke-static {p1}, Lgfu;->a(Lggf;)V

    monitor-exit v3

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lgfv;->b()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    :goto_1
    const-string v4, "Results have already been set"

    invoke-static {v2, v4}, Lgih;->a(ZLjava/lang/Object;)V

    iget-boolean v2, p0, Lgfv;->h:Z

    if-nez v2, :cond_3

    :goto_2
    const-string v1, "Result has already been consumed"

    invoke-static {v0, v1}, Lgih;->a(ZLjava/lang/Object;)V

    invoke-direct {p0, p1}, Lgfv;->b(Lggf;)V

    monitor-exit v3

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final a(Lggg;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lggg",
            "<TR;>;)V"
        }
    .end annotation

    iget-boolean v0, p0, Lgfv;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Result has already been consumed."

    invoke-static {v0, v1}, Lgih;->a(ZLjava/lang/Object;)V

    iget-object v1, p0, Lgfv;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lgfv;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    monitor-exit v1

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lgfv;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lgfv;->c:Lgfw;

    invoke-direct {p0}, Lgfv;->a()Lggf;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lgfw;->a(Lggg;Lggf;)V

    :goto_2
    monitor-exit v1

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    iput-object p1, p0, Lgfv;->f:Lggg;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method public synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lggf;

    invoke-virtual {p0, p1}, Lgfv;->a(Lggf;)V

    return-void
.end method

.method public abstract b(Lcom/google/android/gms/common/api/Status;)Lggf;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/Status;",
            ")TR;"
        }
    .end annotation
.end method

.method public final b()Z
    .locals 4

    iget-object v0, p0, Lgfv;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Lggf;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TR;"
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "await must not be called on the UI thread"

    invoke-static {v0, v3}, Lgih;->a(ZLjava/lang/Object;)V

    iget-boolean v0, p0, Lgfv;->h:Z

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Result has already been consumed"

    invoke-static {v1, v0}, Lgih;->a(ZLjava/lang/Object;)V

    :try_start_0
    iget-object v0, p0, Lgfv;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_2
    invoke-virtual {p0}, Lgfv;->b()Z

    move-result v0

    const-string v1, "Result is not ready."

    invoke-static {v0, v1}, Lgih;->a(ZLjava/lang/Object;)V

    invoke-direct {p0}, Lgfv;->a()Lggf;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1

    :catch_0
    move-exception v0

    iget-object v1, p0, Lgfv;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    invoke-virtual {p0}, Lgfv;->b()Z

    move-result v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/google/android/gms/common/api/Status;->b:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lgfv;->b(Lcom/google/android/gms/common/api/Status;)Lggf;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgfv;->a(Lggf;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgfv;->d:Z

    :cond_2
    monitor-exit v1

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public d()V
    .locals 2

    iget-object v1, p0, Lgfv;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lgfv;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lgfv;->h:Z

    if-eqz v0, :cond_1

    :cond_0
    monitor-exit v1

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lgfv;->g:Lggf;

    invoke-static {v0}, Lgfu;->a(Lggf;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lgfv;->f:Lggg;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgfv;->i:Z

    sget-object v0, Lcom/google/android/gms/common/api/Status;->e:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lgfv;->b(Lcom/google/android/gms/common/api/Status;)Lggf;

    move-result-object v0

    invoke-direct {p0, v0}, Lgfv;->b(Lggf;)V

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public e()Z
    .locals 2

    iget-object v1, p0, Lgfv;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lgfv;->i:Z

    monitor-exit v1

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected f()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lgfv;->h:Z

    iput-object v1, p0, Lgfv;->g:Lggf;

    iput-object v1, p0, Lgfv;->f:Lggg;

    return-void
.end method
