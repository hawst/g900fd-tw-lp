.class public final Layz;
.super Layr;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 13
    new-instance v0, Lays;

    invoke-direct {v0}, Lays;-><init>()V

    const v1, 0x7f020429

    .line 14
    invoke-virtual {v0, v1}, Lays;->a(I)Lays;

    move-result-object v0

    const-wide/32 v2, 0x493e0

    .line 15
    invoke-virtual {v0, v2, v3}, Lays;->b(J)Lays;

    move-result-object v0

    .line 13
    invoke-direct {p0, v0}, Layr;-><init>(Lays;)V

    .line 17
    return-void
.end method


# virtual methods
.method protected a(Layj;Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 27
    new-instance v0, Laza;

    new-instance v1, Layu;

    invoke-direct {v1}, Layu;-><init>()V

    .line 28
    invoke-virtual {v1, p1}, Layu;->a(Layj;)Layu;

    move-result-object v1

    .line 29
    invoke-virtual {v1, v3}, Layu;->e(I)Layu;

    move-result-object v1

    const/4 v2, 0x2

    .line 30
    invoke-virtual {v1, v2}, Layu;->f(I)Layu;

    move-result-object v1

    const/high16 v2, 0x42300000    # 44.0f

    .line 31
    invoke-virtual {v1, v2}, Layu;->b(F)Layu;

    move-result-object v1

    const v2, 0x7f0b00a7

    .line 32
    invoke-virtual {p3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Layu;->a(I)Layu;

    move-result-object v1

    const/high16 v2, 0x42100000    # 36.0f

    .line 33
    invoke-virtual {v1, v2}, Layu;->a(F)Layu;

    move-result-object v1

    const-string v2, "fonts/RobotoSlab-Bold.ttf"

    .line 34
    invoke-static {p2, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Layu;->a(Landroid/graphics/Typeface;)Layu;

    move-result-object v1

    .line 35
    invoke-virtual {v1, v3}, Layu;->a(Z)Layu;

    move-result-object v1

    const/16 v2, 0xc8

    .line 36
    invoke-virtual {v1, v2}, Layu;->c(I)Layu;

    move-result-object v1

    .line 37
    invoke-virtual {v1, v3}, Layu;->b(Z)Layu;

    move-result-object v1

    const v2, 0x7f0b00b2

    .line 38
    invoke-virtual {p3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Layu;->g(I)Layu;

    move-result-object v1

    const v2, 0x3cf5c28f    # 0.03f

    .line 39
    invoke-virtual {v1, v2}, Layu;->d(F)Layu;

    move-result-object v1

    const v2, 0x3d23d70a    # 0.04f

    .line 40
    invoke-virtual {v1, v2}, Layu;->e(F)Layu;

    move-result-object v1

    const v2, 0x3ca3d70a    # 0.02f

    .line 41
    invoke-virtual {v1, v2}, Layu;->c(F)Layu;

    move-result-object v1

    const-wide/32 v2, 0xf4240

    .line 42
    invoke-virtual {v1, v2, v3}, Layu;->a(J)Layu;

    move-result-object v1

    invoke-direct {v0, v1}, Laza;-><init>(Layu;)V

    return-object v0
.end method

.method protected f()Layj;
    .locals 3

    .prologue
    .line 21
    new-instance v0, Lazt;

    sget-object v1, Lazv;->d:Lazv;

    const v2, 0x3f666666    # 0.9f

    invoke-direct {v0, p0, v1, v2}, Lazt;-><init>(Layj;Lazv;F)V

    return-object v0
.end method
