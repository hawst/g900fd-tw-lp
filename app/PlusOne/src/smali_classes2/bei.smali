.class final Lbei;
.super Landroid/widget/BaseAdapter;
.source "PG"

# interfaces
.implements Lcau;
.implements Lcbs;


# instance fields
.field final synthetic a:Lbeb;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lauq;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/content/Context;

.field private final d:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lauq;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lauq;",
            "Lcbk;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lcbr;

.field private final g:Landroid/view/View;

.field private final h:Landroid/view/View;

.field private final i:Landroid/widget/Switch;

.field private j:Z

.field private k:Z


# direct methods
.method public constructor <init>(Lbeb;Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 322
    iput-object p1, p0, Lbei;->a:Lbeb;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 323
    const-string v0, "context"

    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbei;->c:Landroid/content/Context;

    .line 324
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbei;->b:Ljava/util/List;

    .line 325
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lbei;->d:Ljava/util/Set;

    .line 326
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbei;->e:Ljava/util/HashMap;

    .line 327
    new-instance v0, Lbej;

    invoke-direct {v0, p0}, Lbej;-><init>(Lbei;)V

    iput-object v0, p0, Lbei;->f:Lcbr;

    .line 349
    iget-object v0, p0, Lbei;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 351
    const v1, 0x7f04015d

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbei;->h:Landroid/view/View;

    .line 353
    iget-object v0, p0, Lbei;->h:Landroid/view/View;

    const v1, 0x7f100478

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Switch;

    iput-object v0, p0, Lbei;->i:Landroid/widget/Switch;

    .line 355
    iget-object v0, p0, Lbei;->i:Landroid/widget/Switch;

    new-instance v1, Lbek;

    invoke-direct {v1, p0}, Lbek;-><init>(Lbei;)V

    invoke-virtual {v0, v1}, Landroid/widget/Switch;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 363
    iget-object v0, p0, Lbei;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401e7

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lbei;->g:Landroid/view/View;

    .line 366
    iget-object v0, p0, Lbei;->g:Landroid/view/View;

    const v1, 0x7f1005a8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lbel;

    invoke-direct {v1, p0}, Lbel;-><init>(Lbei;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 374
    iput-boolean v4, p0, Lbei;->j:Z

    .line 375
    iput-boolean v4, p0, Lbei;->k:Z

    .line 376
    return-void
.end method

.method private a()I
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lbei;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410
    invoke-direct {p0}, Lbei;->c()I

    move-result v0

    .line 412
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lbei;->c()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private a(Landroid/view/View;)Lauq;
    .locals 1

    .prologue
    .line 578
    const v0, 0x7f100004

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lauq;

    return-object v0
.end method

.method static synthetic a(Lbei;Landroid/view/View;)Lauq;
    .locals 1

    .prologue
    .line 288
    invoke-direct {p0, p1}, Lbei;->a(Landroid/view/View;)Lauq;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbei;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lbei;->d:Ljava/util/Set;

    return-object v0
.end method

.method private b()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 545
    iget-object v0, p0, Lbei;->a:Lbeb;

    invoke-static {v0}, Lbeb;->b(Lbeb;)Lccc;

    move-result-object v0

    invoke-virtual {v0}, Lccc;->getChildCount()I

    move-result v2

    move v1, v5

    :goto_0
    if-ge v1, v2, :cond_1

    .line 546
    iget-object v0, p0, Lbei;->a:Lbeb;

    invoke-static {v0}, Lbeb;->b(Lbeb;)Lccc;

    move-result-object v0

    invoke-virtual {v0, v1}, Lccc;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lcbk;

    if-eqz v0, :cond_0

    .line 547
    iget-object v0, p0, Lbei;->a:Lbeb;

    .line 548
    invoke-static {v0}, Lbeb;->b(Lbeb;)Lccc;

    move-result-object v0

    invoke-virtual {v0, v1}, Lccc;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcbk;

    .line 549
    invoke-direct {p0, v0}, Lbei;->a(Landroid/view/View;)Lauq;

    move-result-object v6

    .line 550
    iget-object v7, p0, Lbei;->e:Ljava/util/HashMap;

    invoke-virtual {v7, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 545
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 554
    :cond_1
    invoke-virtual {p0}, Lbei;->getCount()I

    move-result v8

    move v7, v5

    :goto_1
    if-ge v7, v8, :cond_8

    .line 555
    invoke-virtual {p0, v7}, Lbei;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lauq;

    .line 556
    if-eqz v0, :cond_2

    .line 557
    iget-object v1, p0, Lbei;->e:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcbk;

    .line 560
    if-eqz v1, :cond_2

    .line 561
    if-lez v7, :cond_3

    add-int/lit8 v2, v7, -0x1

    invoke-virtual {p0, v2}, Lbei;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lauq;

    move-object v6, v2

    .line 564
    :goto_2
    add-int/lit8 v2, v8, -0x1

    if-ge v7, v2, :cond_4

    add-int/lit8 v2, v7, 0x1

    invoke-virtual {p0, v2}, Lbei;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lauq;

    .line 565
    :goto_3
    invoke-static {v0}, Lbeb;->a(Lauq;)Z

    move-result v9

    if-nez v9, :cond_5

    iget-object v9, p0, Lbei;->d:Ljava/util/Set;

    invoke-interface {v9, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v4

    .line 566
    :goto_4
    if-eqz v0, :cond_6

    if-eqz v6, :cond_6

    iget-object v9, p0, Lbei;->d:Ljava/util/Set;

    .line 567
    invoke-interface {v9, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_6

    move v6, v4

    .line 568
    :goto_5
    if-eqz v0, :cond_7

    if-eqz v2, :cond_7

    iget-object v0, p0, Lbei;->d:Ljava/util/Set;

    .line 569
    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v4

    .line 570
    :goto_6
    invoke-virtual {v1, v6}, Lcbk;->a(Z)V

    .line 571
    invoke-virtual {v1, v0}, Lcbk;->b(Z)V

    .line 554
    :cond_2
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    :cond_3
    move-object v6, v3

    .line 561
    goto :goto_2

    :cond_4
    move-object v2, v3

    .line 564
    goto :goto_3

    :cond_5
    move v0, v5

    .line 565
    goto :goto_4

    :cond_6
    move v6, v5

    .line 567
    goto :goto_5

    :cond_7
    move v0, v5

    .line 569
    goto :goto_6

    .line 573
    :cond_8
    iget-object v0, p0, Lbei;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 574
    return-void
.end method

.method static synthetic b(Lbei;)V
    .locals 0

    .prologue
    .line 288
    invoke-direct {p0}, Lbei;->b()V

    return-void
.end method

.method private c()I
    .locals 1

    .prologue
    .line 582
    iget-boolean v0, p0, Lbei;->k:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lbei;)Landroid/widget/Switch;
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lbei;->i:Landroid/widget/Switch;

    return-object v0
.end method


# virtual methods
.method public a(J)I
    .locals 5

    .prologue
    .line 526
    const/4 v0, 0x0

    invoke-virtual {p0}, Lbei;->getCount()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_1

    .line 527
    invoke-virtual {p0, v0}, Lbei;->getItemId(I)J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    .line 531
    :goto_1
    return v0

    .line 526
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 531
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public a(Lboh;)V
    .locals 1

    .prologue
    .line 538
    iget-object v0, p0, Lbei;->a:Lbeb;

    invoke-static {v0}, Lbeb;->c(Lbeb;)Laux;

    move-result-object v0

    invoke-interface {v0, p1}, Laux;->a(Lboh;)V

    .line 539
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lauq;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 511
    iget-object v0, p0, Lbei;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 512
    iget-object v0, p0, Lbei;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 513
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lauq;

    .line 514
    iget-object v2, p0, Lbei;->a:Lbeb;

    invoke-static {v2, v0}, Lbeb;->a(Lbeb;Lauq;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 515
    iget-object v2, p0, Lbei;->d:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 518
    :cond_1
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 385
    iget-object v0, p0, Lbei;->i:Landroid/widget/Switch;

    invoke-virtual {v0, p1}, Landroid/widget/Switch;->setChecked(Z)V

    .line 386
    return-void
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 394
    iput-boolean p1, p0, Lbei;->k:Z

    .line 395
    invoke-virtual {p0}, Lbei;->notifyDataSetChanged()V

    .line 396
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 404
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lbei;->j:Z

    .line 405
    invoke-virtual {p0}, Lbei;->notifyDataSetChanged()V

    .line 406
    return-void

    .line 404
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 436
    iget-object v0, p0, Lbei;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 437
    const/4 v0, 0x0

    .line 446
    :goto_0
    return v0

    .line 440
    :cond_0
    invoke-direct {p0}, Lbei;->c()I

    move-result v0

    .line 442
    iget-boolean v1, p0, Lbei;->j:Z

    if-eqz v1, :cond_1

    .line 443
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 446
    :cond_1
    iget-object v1, p0, Lbei;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 587
    iget-boolean v1, p0, Lbei;->k:Z

    if-eqz v1, :cond_1

    if-nez p1, :cond_1

    .line 594
    :cond_0
    :goto_0
    return-object v0

    .line 590
    :cond_1
    iget-boolean v1, p0, Lbei;->j:Z

    if-eqz v1, :cond_2

    invoke-direct {p0}, Lbei;->a()I

    move-result v1

    if-eq p1, v1, :cond_0

    .line 594
    :cond_2
    iget-object v0, p0, Lbei;->b:Ljava/util/List;

    invoke-direct {p0}, Lbei;->c()I

    move-result v1

    sub-int v1, p1, v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 494
    iget-boolean v0, p0, Lbei;->k:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 495
    const-wide v0, 0x80000000L

    .line 500
    :goto_0
    return-wide v0

    .line 497
    :cond_0
    iget-boolean v0, p0, Lbei;->j:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbei;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {p0}, Lbei;->c()I

    move-result v1

    add-int/2addr v0, v1

    if-ne p1, v0, :cond_1

    .line 498
    const-wide v0, 0x80000001L

    goto :goto_0

    .line 500
    :cond_1
    iget-object v0, p0, Lbei;->b:Ljava/util/List;

    invoke-direct {p0}, Lbei;->c()I

    move-result v1

    sub-int v1, p1, v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lauq;

    invoke-virtual {v0}, Lauq;->hashCode()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 424
    iget-boolean v0, p0, Lbei;->k:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 425
    const/4 v0, 0x0

    .line 430
    :goto_0
    return v0

    .line 427
    :cond_0
    iget-boolean v0, p0, Lbei;->j:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lbei;->a()I

    move-result v0

    if-ne p1, v0, :cond_1

    .line 428
    const/4 v0, 0x2

    goto :goto_0

    .line 430
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    .prologue
    .line 454
    invoke-virtual {p0, p1}, Lbei;->getItemViewType(I)I

    move-result v0

    if-nez v0, :cond_0

    .line 455
    invoke-direct {p0}, Lbei;->b()V

    .line 456
    iget-object v1, p0, Lbei;->h:Landroid/view/View;

    .line 484
    :goto_0
    return-object v1

    .line 459
    :cond_0
    invoke-virtual {p0, p1}, Lbei;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 460
    invoke-direct {p0}, Lbei;->b()V

    .line 461
    iget-object v1, p0, Lbei;->g:Landroid/view/View;

    goto :goto_0

    .line 466
    :cond_1
    iget-object v0, p0, Lbei;->b:Ljava/util/List;

    .line 467
    invoke-direct {p0}, Lbei;->c()I

    move-result v1

    sub-int v1, p1, v1

    .line 466
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lauq;

    .line 469
    instance-of v0, p2, Lcbk;

    if-eqz v0, :cond_2

    .line 470
    check-cast p2, Lcbk;

    move-object v1, p2

    .line 481
    :goto_1
    iget-object v0, p0, Lbei;->a:Lbeb;

    const/4 v3, 0x1

    iget-object v4, p0, Lbei;->d:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    iget-object v6, p0, Lbei;->f:Lcbr;

    move-object v4, p0

    invoke-static/range {v0 .. v6}, Lbeb;->a(Lbeb;Lcbk;Lauq;ZLcbs;ZLcbr;)V

    .line 483
    invoke-direct {p0}, Lbei;->b()V

    goto :goto_0

    .line 479
    :cond_2
    new-instance v1, Lcbk;

    iget-object v0, p0, Lbei;->c:Landroid/content/Context;

    invoke-direct {v1, v0}, Lcbk;-><init>(Landroid/content/Context;)V

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 419
    const/4 v0, 0x3

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 489
    const/4 v0, 0x1

    return v0
.end method
