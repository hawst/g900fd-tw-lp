.class public final Lcfh;
.super Landroid/util/LongSparseArray;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Landroid/util/LongSparseArray",
        "<TT;>;"
    }
.end annotation


# static fields
.field private static final a:Lcfh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcfh",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 11
    new-instance v0, Lcfh;

    invoke-direct {v0}, Lcfh;-><init>()V

    sput-object v0, Lcfh;->a:Lcfh;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 38
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/util/LongSparseArray;-><init>(I)V

    .line 39
    return-void
.end method

.method private constructor <init>(Landroid/util/LongSparseArray;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<+TT;>;)V"
        }
    .end annotation

    .prologue
    .line 43
    invoke-virtual {p1}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    invoke-direct {p0, v0}, Landroid/util/LongSparseArray;-><init>(I)V

    .line 44
    invoke-virtual {p1}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    .line 45
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 46
    invoke-virtual {p1, v0}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v2

    invoke-virtual {p1, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-super {p0, v2, v3, v4}, Landroid/util/LongSparseArray;->append(JLjava/lang/Object;)V

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 48
    :cond_0
    return-void
.end method

.method public static a()Lcfh;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lcfh",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 20
    sget-object v0, Lcfh;->a:Lcfh;

    return-object v0
.end method

.method public static a(Landroid/util/LongSparseArray;)Lcfh;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Landroid/util/LongSparseArray",
            "<+TT;>;)",
            "Lcfh",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 28
    instance-of v0, p0, Lcfh;

    if-eqz v0, :cond_0

    .line 30
    check-cast p0, Lcfh;

    .line 32
    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Lcfh;

    invoke-direct {v0, p0}, Lcfh;-><init>(Landroid/util/LongSparseArray;)V

    move-object p0, v0

    goto :goto_0
.end method


# virtual methods
.method public append(JLjava/lang/Object;)V
    .locals 2

    .prologue
    .line 52
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This array is immutable."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public clear()V
    .locals 2

    .prologue
    .line 57
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This array is immutable."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public delete(J)V
    .locals 2

    .prologue
    .line 62
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This array is immutable."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public put(JLjava/lang/Object;)V
    .locals 2

    .prologue
    .line 67
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This array is immutable."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public remove(J)V
    .locals 2

    .prologue
    .line 72
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This array is immutable."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeAt(I)V
    .locals 2

    .prologue
    .line 77
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This array is immutable."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setValueAt(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 82
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "This array is immutable."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
