.class final Ljyt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private final a:I

.field private synthetic b:Ljym;


# direct methods
.method public constructor <init>(Ljym;)V
    .locals 1

    .prologue
    .line 802
    iput-object p1, p0, Ljyt;->b:Ljym;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 803
    const v0, 0x7fffffff

    iput v0, p0, Ljyt;->a:I

    .line 804
    return-void
.end method

.method public constructor <init>(Ljym;I)V
    .locals 0

    .prologue
    .line 806
    iput-object p1, p0, Ljyt;->b:Ljym;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 807
    iput p2, p0, Ljyt;->a:I

    .line 808
    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    .prologue
    .line 818
    iget-object v0, p0, Ljyt;->b:Ljym;

    invoke-static {v0}, Ljym;->c(Ljym;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 826
    :cond_0
    :goto_0
    return-void

    .line 821
    :cond_1
    iget v0, p0, Ljyt;->a:I

    const v1, 0x7fffffff

    if-ne v0, v1, :cond_2

    .line 822
    iget-object v0, p0, Ljyt;->b:Ljym;

    invoke-static {v0}, Ljym;->a(Ljym;)Ljyu;

    move-result-object v0

    iget-object v1, p0, Ljyt;->b:Ljym;

    invoke-static {v1}, Ljym;->d(Ljym;)Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/social/sharekit/comments/MentionMultiAutoCompleteTextView;->d()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljyu;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 823
    :cond_2
    iget v0, p0, Ljyt;->a:I

    iget-object v1, p0, Ljyt;->b:Ljym;

    invoke-static {v1}, Ljym;->a(Ljym;)Ljyu;

    move-result-object v1

    invoke-virtual {v1}, Ljyu;->a()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 824
    iget-object v0, p0, Ljyt;->b:Ljym;

    invoke-static {v0}, Ljym;->a(Ljym;)Ljyu;

    move-result-object v0

    iget v1, p0, Ljyt;->a:I

    invoke-virtual {v0, v1}, Ljyu;->a(I)Ljyw;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljyw;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 811
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    .prologue
    .line 814
    return-void
.end method
