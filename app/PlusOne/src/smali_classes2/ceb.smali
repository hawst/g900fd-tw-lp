.class public final Lceb;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lcex;

.field private final b:I

.field private final c:I

.field private final d:Z

.field private final e:Z

.field private final f:[[F


# direct methods
.method public constructor <init>(IIZZ)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput p1, p0, Lceb;->b:I

    .line 38
    iput p2, p0, Lceb;->c:I

    .line 39
    iput-boolean p3, p0, Lceb;->d:Z

    .line 40
    iput-boolean p4, p0, Lceb;->e:Z

    .line 41
    iget v0, p0, Lceb;->b:I

    iget v1, p0, Lceb;->c:I

    mul-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x2

    filled-new-array {v0, v0}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Float;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[F

    iput-object v0, p0, Lceb;->f:[[F

    .line 42
    new-instance v0, Lcex;

    iget-object v1, p0, Lceb;->f:[[F

    array-length v1, v1

    invoke-direct {v0, v1}, Lcex;-><init>(I)V

    iput-object v0, p0, Lceb;->a:Lcex;

    .line 43
    return-void
.end method

.method private static a(IIZ)Ljava/util/ArrayList;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIZ)",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 126
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 129
    add-int/lit8 v0, p0, -0x1

    :goto_0
    add-int/lit8 v2, p0, 0x1

    if-gt v0, v2, :cond_1

    .line 130
    invoke-static {v0, p1, p2}, Lceb;->b(IIZ)I

    move-result v2

    .line 131
    if-ltz v2, :cond_0

    .line 132
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 129
    :cond_0
    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    .line 135
    :cond_1
    return-object v1
.end method

.method private static b(IIZ)I
    .locals 0

    .prologue
    .line 162
    if-eqz p2, :cond_1

    rem-int/2addr p0, p1

    :cond_0
    :goto_0
    return p0

    :cond_1
    if-ltz p0, :cond_2

    if-lt p0, p1, :cond_0

    :cond_2
    const/4 p0, -0x1

    goto :goto_0
.end method


# virtual methods
.method public a([F[F)F
    .locals 15

    .prologue
    .line 56
    const-string v1, "hist1"

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 57
    const-string v1, "hist2"

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 58
    move-object/from16 v0, p1

    array-length v1, v0

    iget v2, p0, Lceb;->b:I

    iget v3, p0, Lceb;->c:I

    mul-int/2addr v2, v3

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "histogram dimensions are mismatched."

    invoke-static {v1, v2}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 59
    move-object/from16 v0, p2

    array-length v1, v0

    iget v2, p0, Lceb;->b:I

    iget v3, p0, Lceb;->c:I

    mul-int/2addr v2, v3

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_1
    const-string v2, "histogram dimensions are mismatched."

    invoke-static {v1, v2}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 61
    iget-object v1, p0, Lceb;->f:[[F

    array-length v1, v1

    add-int/lit8 v5, v1, -0x1

    iget-object v2, p0, Lceb;->f:[[F

    array-length v3, v2

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    const/4 v6, 0x0

    invoke-static {v4, v6}, Ljava/util/Arrays;->fill([FF)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 58
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 59
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 61
    :cond_2
    const/4 v1, 0x0

    :goto_3
    iget v2, p0, Lceb;->b:I

    iget v3, p0, Lceb;->c:I

    mul-int/2addr v2, v3

    if-ge v1, v2, :cond_5

    aget v2, p1, v1

    aget v3, p2, v1

    sub-float/2addr v2, v3

    add-int/lit8 v3, v1, 0x1

    const/4 v4, 0x0

    cmpl-float v4, v2, v4

    if-lez v4, :cond_4

    iget-object v4, p0, Lceb;->f:[[F

    const/4 v6, 0x0

    aget-object v4, v4, v6

    aput v2, v4, v3

    :cond_3
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_4
    const/4 v4, 0x0

    cmpg-float v4, v2, v4

    if-gez v4, :cond_3

    iget-object v4, p0, Lceb;->f:[[F

    aget-object v3, v4, v3

    neg-float v2, v2

    aput v2, v3, v5

    goto :goto_4

    :cond_5
    const/4 v1, 0x1

    move v4, v1

    :goto_5
    if-ge v4, v5, :cond_d

    iget-object v1, p0, Lceb;->f:[[F

    const/4 v2, 0x0

    aget-object v1, v1, v2

    aget v1, v1, v4

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_c

    iget v1, p0, Lceb;->b:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_8

    iget v1, p0, Lceb;->c:I

    iget-boolean v2, p0, Lceb;->e:Z

    invoke-static {v4, v1, v2}, Lceb;->a(IIZ)Ljava/util/ArrayList;

    move-result-object v1

    :cond_6
    :goto_6
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    :goto_7
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    iget-object v3, p0, Lceb;->f:[[F

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v6

    aget-object v3, v3, v6

    aget v3, v3, v5

    const/4 v6, 0x0

    cmpl-float v3, v3, v6

    if-lez v3, :cond_7

    iget-object v3, p0, Lceb;->f:[[F

    aget-object v3, v3, v4

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const v6, 0x7f7fffff    # Float.MAX_VALUE

    aput v6, v3, v1

    goto :goto_7

    :cond_8
    iget v1, p0, Lceb;->c:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_9

    iget v1, p0, Lceb;->b:I

    iget-boolean v2, p0, Lceb;->d:Z

    invoke-static {v4, v1, v2}, Lceb;->a(IIZ)Ljava/util/ArrayList;

    move-result-object v1

    goto :goto_6

    :cond_9
    iget v6, p0, Lceb;->b:I

    iget v7, p0, Lceb;->c:I

    iget-boolean v8, p0, Lceb;->d:Z

    iget-boolean v9, p0, Lceb;->e:Z

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    add-int/lit8 v2, v4, -0x1

    rem-int v10, v2, v7

    add-int/lit8 v2, v4, -0x1

    div-int v11, v2, v7

    add-int/lit8 v2, v10, -0x1

    move v3, v2

    :goto_8
    add-int/lit8 v2, v10, 0x1

    if-gt v3, v2, :cond_6

    invoke-static {v3, v7, v9}, Lceb;->b(IIZ)I

    move-result v12

    const/4 v2, -0x1

    if-eq v12, v2, :cond_b

    add-int/lit8 v2, v11, -0x1

    :goto_9
    add-int/lit8 v13, v11, 0x1

    if-gt v2, v13, :cond_b

    invoke-static {v2, v6, v8}, Lceb;->b(IIZ)I

    move-result v13

    const/4 v14, -0x1

    if-eq v13, v14, :cond_a

    mul-int/2addr v13, v7

    add-int/2addr v13, v12

    add-int/lit8 v13, v13, 0x1

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v1, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_a
    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    :cond_b
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_8

    :cond_c
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto/16 :goto_5

    .line 62
    :cond_d
    iget-object v1, p0, Lceb;->a:Lcex;

    iget-object v2, p0, Lceb;->f:[[F

    invoke-virtual {v1, v2}, Lcex;->a([[F)F

    move-result v3

    .line 66
    const/4 v2, 0x0

    .line 67
    const/4 v1, 0x0

    :goto_a
    iget-object v4, p0, Lceb;->f:[[F

    array-length v4, v4

    if-ge v1, v4, :cond_e

    .line 68
    iget-object v4, p0, Lceb;->f:[[F

    const/4 v5, 0x0

    aget-object v4, v4, v5

    aget v4, v4, v1

    add-float/2addr v2, v4

    .line 67
    add-int/lit8 v1, v1, 0x1

    goto :goto_a

    .line 72
    :cond_e
    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v1, v3

    sub-float v1, v2, v1

    return v1
.end method
