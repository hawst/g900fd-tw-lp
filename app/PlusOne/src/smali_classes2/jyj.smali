.class public final Ljyj;
.super Llol;
.source "PG"


# instance fields
.field private N:Ljyu;

.field private O:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Llol;-><init>()V

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 73
    iget-object v1, p0, Ljyj;->O:Landroid/widget/TextView;

    iget-object v0, p0, Ljyj;->N:Ljyu;

    invoke-virtual {v0}, Ljyu;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 74
    return-void

    .line 73
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method static synthetic a(Ljyj;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljyj;->a()V

    return-void
.end method

.method static synthetic b(Ljyj;)Ljyu;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Ljyj;->N:Ljyu;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    .line 56
    const v0, 0x7f04019b

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 58
    const v0, 0x7f1004e1

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ljyj;->O:Landroid/widget/TextView;

    .line 59
    iget-object v0, p0, Ljyj;->O:Landroid/widget/TextView;

    new-instance v2, Lhmk;

    sget-object v3, Lonf;->b:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v2}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 62
    iget-object v0, p0, Ljyj;->O:Landroid/widget/TextView;

    new-instance v2, Lhmi;

    new-instance v3, Ljyl;

    invoke-direct {v3, p0}, Ljyl;-><init>(Ljyj;)V

    invoke-direct {v2, v3}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 68
    invoke-direct {p0}, Ljyj;->a()V

    .line 69
    return-object v1
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 29
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 30
    iget-object v0, p0, Ljyj;->au:Llnh;

    const-class v1, Lkjj;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkjj;

    const-class v1, Ljzz;

    invoke-interface {v0, v1}, Lkjj;->a(Ljava/lang/Class;)Lkji;

    move-result-object v0

    check-cast v0, Ljzz;

    invoke-virtual {v0}, Ljzz;->j()Ljyu;

    move-result-object v0

    iput-object v0, p0, Ljyj;->N:Ljyu;

    .line 31
    iget-object v0, p0, Ljyj;->N:Ljyu;

    new-instance v1, Ljyk;

    invoke-direct {v1, p0}, Ljyk;-><init>(Ljyj;)V

    invoke-virtual {v0, v1}, Ljyu;->a(Ljyv;)V

    .line 52
    return-void
.end method
