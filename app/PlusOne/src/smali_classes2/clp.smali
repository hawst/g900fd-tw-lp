.class final Lclp;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lclm;


# direct methods
.method constructor <init>(Lclm;)V
    .locals 0

    .prologue
    .line 371
    iput-object p1, p0, Lclp;->a:Lclm;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a()Landroid/graphics/Bitmap;
    .locals 14

    .prologue
    .line 374
    iget-object v0, p0, Lclp;->a:Lclm;

    invoke-virtual {v0}, Lclm;->ah()Lchp;

    move-result-object v0

    .line 375
    iget-object v1, p0, Lclp;->a:Lclm;

    invoke-static {v1}, Lclm;->a(Lclm;)Lhdy;

    move-result-object v1

    .line 376
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 377
    :cond_0
    const/4 v0, 0x0

    .line 448
    :goto_0
    return-object v0

    .line 380
    :cond_1
    invoke-virtual {v0}, Lchp;->e()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 381
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    .line 382
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    .line 384
    invoke-virtual {v0}, Lchp;->h()Lcom/google/android/libraries/photoeditor/core/FilterChain;

    move-result-object v5

    .line 386
    iget-object v0, p0, Lclp;->a:Lclm;

    invoke-virtual {v0}, Lclm;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v6

    .line 387
    const/16 v0, 0x26

    .line 388
    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v7

    .line 389
    const/16 v0, 0x26

    .line 390
    invoke-virtual {v6, v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    .line 391
    invoke-virtual {v5}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getRotationAngle()F

    move-result v8

    .line 392
    const/16 v9, 0x26

    invoke-virtual {v6, v9}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v6

    neg-float v6, v6

    .line 395
    add-float/2addr v6, v8

    invoke-static {v6, v7, v0}, Lhcg;->a(FFF)F

    move-result v0

    .line 396
    sub-float v6, v0, v8

    .line 400
    new-instance v7, Landroid/graphics/RectF;

    .line 401
    invoke-virtual {v5}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectX()F

    move-result v8

    mul-float/2addr v8, v3

    .line 402
    invoke-virtual {v5}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectY()F

    move-result v9

    mul-float/2addr v9, v4

    .line 403
    invoke-virtual {v5}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectX()F

    move-result v10

    invoke-virtual {v5}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectWidth()F

    move-result v11

    add-float/2addr v10, v11

    mul-float/2addr v10, v3

    .line 404
    invoke-virtual {v5}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectY()F

    move-result v11

    invoke-virtual {v5}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectHeight()F

    move-result v12

    add-float/2addr v11, v12

    mul-float/2addr v11, v4

    invoke-direct {v7, v8, v9, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 412
    new-instance v8, Landroid/graphics/Matrix;

    invoke-direct {v8}, Landroid/graphics/Matrix;-><init>()V

    .line 413
    const/high16 v9, 0x3f000000    # 0.5f

    mul-float/2addr v9, v3

    const/high16 v10, 0x3f000000    # 0.5f

    mul-float/2addr v10, v4

    invoke-virtual {v8, v6, v9, v10}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 414
    const/4 v9, 0x2

    new-array v9, v9, [F

    const/4 v10, 0x0

    .line 415
    invoke-virtual {v7}, Landroid/graphics/RectF;->centerX()F

    move-result v11

    aput v11, v9, v10

    const/4 v10, 0x1

    invoke-virtual {v7}, Landroid/graphics/RectF;->centerY()F

    move-result v11

    aput v11, v9, v10

    .line 417
    invoke-virtual {v8, v9}, Landroid/graphics/Matrix;->mapPoints([F)V

    .line 421
    invoke-virtual {v1, v6}, Lhdy;->b(F)Landroid/graphics/RectF;

    move-result-object v6

    .line 423
    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v8

    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v10

    mul-float/2addr v8, v10

    .line 425
    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v7

    mul-float/2addr v6, v7

    .line 428
    new-instance v7, Landroid/graphics/RectF;

    const/4 v10, 0x0

    aget v10, v9, v10

    const/high16 v11, 0x3f000000    # 0.5f

    mul-float/2addr v11, v8

    sub-float/2addr v10, v11

    const/4 v11, 0x1

    aget v11, v9, v11

    const/high16 v12, 0x3f000000    # 0.5f

    mul-float/2addr v12, v6

    sub-float/2addr v11, v12

    const/4 v12, 0x0

    aget v12, v9, v12

    const/high16 v13, 0x3f000000    # 0.5f

    mul-float/2addr v8, v13

    add-float/2addr v8, v12

    const/high16 v12, 0x3f800000    # 1.0f

    .line 431
    invoke-static {v8, v12}, Ljava/lang/Math;->max(FF)F

    move-result v8

    const/4 v12, 0x1

    aget v9, v9, v12

    const/high16 v12, 0x3f000000    # 0.5f

    mul-float/2addr v6, v12

    add-float/2addr v6, v9

    const/high16 v9, 0x3f800000    # 1.0f

    .line 432
    invoke-static {v6, v9}, Ljava/lang/Math;->max(FF)F

    move-result v6

    invoke-direct {v7, v10, v11, v8, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 435
    new-instance v6, Landroid/graphics/RectF;

    iget v8, v7, Landroid/graphics/RectF;->left:F

    div-float/2addr v8, v3

    const/4 v9, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    .line 436
    invoke-static {v8, v9, v10}, Lhcg;->a(FFF)F

    move-result v8

    iget v9, v7, Landroid/graphics/RectF;->top:F

    div-float/2addr v9, v4

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    .line 437
    invoke-static {v9, v10, v11}, Lhcg;->a(FFF)F

    move-result v9

    iget v10, v7, Landroid/graphics/RectF;->right:F

    div-float v3, v10, v3

    const/4 v10, 0x0

    const/high16 v11, 0x3f800000    # 1.0f

    .line 438
    invoke-static {v3, v10, v11}, Lhcg;->a(FFF)F

    move-result v3

    iget v7, v7, Landroid/graphics/RectF;->bottom:F

    div-float v4, v7, v4

    const/4 v7, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    .line 439
    invoke-static {v4, v7, v10}, Lhcg;->a(FFF)F

    move-result v4

    invoke-direct {v6, v8, v9, v3, v4}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 442
    invoke-virtual {v1}, Lhdy;->a()F

    move-result v1

    invoke-static {v1}, Lhbg;->a(F)I

    move-result v1

    .line 444
    invoke-virtual {v5, v6}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a(Landroid/graphics/RectF;)V

    .line 445
    invoke-virtual {v5, v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a(F)V

    .line 446
    invoke-virtual {v5, v1}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->b(I)V

    .line 448
    sget-object v0, Lhan;->a:Lhao;

    const/4 v1, 0x3

    const/4 v3, 0x0

    invoke-interface {v0, v2, v5, v1, v3}, Lhao;->renderFilterChain(Landroid/graphics/Bitmap;Lcom/google/android/libraries/photoeditor/core/FilterChain;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_0
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 460
    iget-object v0, p0, Lclp;->a:Lclm;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lclm;->k(Z)V

    .line 461
    iget-object v0, p0, Lclp;->a:Lclm;

    invoke-virtual {v0}, Lclm;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Lhdn;->b(Landroid/app/Activity;)V

    .line 463
    iget-object v0, p0, Lclp;->a:Lclm;

    iget-object v1, p0, Lclp;->a:Lclm;

    invoke-virtual {v1}, Lclm;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lclm;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Landroid/graphics/Bitmap;)V

    .line 464
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 371
    invoke-virtual {p0}, Lclp;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 371
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lclp;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 454
    iget-object v0, p0, Lclp;->a:Lclm;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lclm;->k(Z)V

    .line 455
    iget-object v0, p0, Lclp;->a:Lclm;

    invoke-virtual {v0}, Lclm;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Lhdn;->a(Landroid/app/Activity;)V

    .line 456
    return-void
.end method
