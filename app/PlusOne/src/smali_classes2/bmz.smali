.class public final enum Lbmz;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbmz;",
        ">;"
    }
.end annotation


# static fields
.field private static enum A:Lbmz;

.field private static enum B:Lbmz;

.field private static enum C:Lbmz;

.field private static enum D:Lbmz;

.field private static final synthetic F:[Lbmz;

.field public static final enum a:Lbmz;

.field public static final enum b:Lbmz;

.field public static final enum c:Lbmz;

.field public static final enum d:Lbmz;

.field public static final enum e:Lbmz;

.field public static final enum f:Lbmz;

.field public static final enum g:Lbmz;

.field public static final enum h:Lbmz;

.field public static final enum i:Lbmz;

.field public static final enum j:Lbmz;

.field public static final enum k:Lbmz;

.field public static final enum l:Lbmz;

.field public static final enum m:Lbmz;

.field public static final enum n:Lbmz;

.field public static final enum o:Lbmz;

.field public static final enum p:Lbmz;

.field public static final enum q:Lbmz;

.field public static final enum r:Lbmz;

.field public static final enum s:Lbmz;

.field public static final enum t:Lbmz;

.field public static final enum u:Lbmz;

.field public static final enum v:Lbmz;

.field public static final enum w:Lbmz;

.field private static enum x:Lbmz;

.field private static enum y:Lbmz;

.field private static enum z:Lbmz;


# instance fields
.field private final E:Lbnk;


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 26
    new-instance v0, Lbmz;

    const-string v1, "STABILIZATION"

    new-instance v2, Lbob;

    invoke-direct {v2}, Lbob;-><init>()V

    invoke-direct {v0, v1, v4, v2}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->a:Lbmz;

    .line 27
    new-instance v0, Lbmz;

    const-string v1, "STABILIZATION_COMPENSATION_TRANSFORM"

    new-instance v2, Lbnz;

    invoke-direct {v2}, Lbnz;-><init>()V

    invoke-direct {v0, v1, v5, v2}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->b:Lbmz;

    .line 28
    new-instance v0, Lbmz;

    const-string v1, "CAMERA_MOTION"

    new-instance v2, Lbne;

    invoke-direct {v2}, Lbne;-><init>()V

    invoke-direct {v0, v1, v6, v2}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->c:Lbmz;

    .line 29
    new-instance v0, Lbmz;

    const-string v1, "HUE_DIFFERENCE"

    new-instance v2, Lbnm;

    invoke-direct {v2}, Lbnm;-><init>()V

    invoke-direct {v0, v1, v7, v2}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->x:Lbmz;

    .line 30
    new-instance v0, Lbmz;

    const-string v1, "HUE_RANGE"

    new-instance v2, Lbnn;

    invoke-direct {v2}, Lbnn;-><init>()V

    invoke-direct {v0, v1, v8, v2}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->y:Lbmz;

    .line 31
    new-instance v0, Lbmz;

    const-string v1, "COLOR_CORRELATION"

    const/4 v2, 0x5

    new-instance v3, Lbng;

    invoke-direct {v3}, Lbng;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->d:Lbmz;

    .line 32
    new-instance v0, Lbmz;

    const-string v1, "COLORFULNESS"

    const/4 v2, 0x6

    new-instance v3, Lbnh;

    invoke-direct {v3}, Lbnh;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->e:Lbmz;

    .line 33
    new-instance v0, Lbmz;

    const-string v1, "SHARPNESS"

    const/4 v2, 0x7

    new-instance v3, Lbny;

    invoke-direct {v3}, Lbny;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->f:Lbmz;

    .line 34
    new-instance v0, Lbmz;

    const-string v1, "HORIZONTAL_PAN_DIRECTION"

    const/16 v2, 0x8

    new-instance v3, Lbnl;

    invoke-direct {v3}, Lbnl;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->z:Lbmz;

    .line 35
    new-instance v0, Lbmz;

    const-string v1, "VERTICAL_PAN_DIRECTION"

    const/16 v2, 0x9

    new-instance v3, Lboe;

    invoke-direct {v3}, Lboe;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->A:Lbmz;

    .line 36
    new-instance v0, Lbmz;

    const-string v1, "PHASE_CORRELATE_HORIZONTAL_PAN_DIRECTION"

    const/16 v2, 0xa

    new-instance v3, Lbnt;

    invoke-direct {v3}, Lbnt;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->B:Lbmz;

    .line 37
    new-instance v0, Lbmz;

    const-string v1, "PHASE_CORRELATE_VERTICAL_PAN_DIRECTION"

    const/16 v2, 0xb

    new-instance v3, Lbnu;

    invoke-direct {v3}, Lbnu;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->C:Lbmz;

    .line 38
    new-instance v0, Lbmz;

    const-string v1, "FACE_SCORE"

    const/16 v2, 0xc

    new-instance v3, Lbnj;

    invoke-direct {v3}, Lbnj;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->g:Lbmz;

    .line 39
    new-instance v0, Lbmz;

    const-string v1, "CHROMA_HISTOGRAM"

    const/16 v2, 0xd

    new-instance v3, Lbnf;

    invoke-direct {v3}, Lbnf;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->h:Lbmz;

    .line 40
    new-instance v0, Lbmz;

    const-string v1, "SALIENCY"

    const/16 v2, 0xe

    new-instance v3, Lbnv;

    invoke-direct {v3}, Lbnv;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->i:Lbmz;

    .line 41
    new-instance v0, Lbmz;

    const-string v1, "BADNESS"

    const/16 v2, 0xf

    new-instance v3, Lbnd;

    invoke-direct {v3}, Lbnd;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->D:Lbmz;

    .line 42
    new-instance v0, Lbmz;

    const-string v1, "MOTION_SALIENCY"

    const/16 v2, 0x10

    new-instance v3, Lbnp;

    invoke-direct {v3}, Lbnp;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->j:Lbmz;

    .line 43
    new-instance v0, Lbmz;

    const-string v1, "AUDIO_RMS"

    const/16 v2, 0x11

    new-instance v3, Lbnc;

    invoke-direct {v3}, Lbnc;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->k:Lbmz;

    .line 44
    new-instance v0, Lbmz;

    const-string v1, "NEW_COLORFULNESS"

    const/16 v2, 0x12

    new-instance v3, Lbnr;

    invoke-direct {v3}, Lbnr;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->l:Lbmz;

    .line 45
    new-instance v0, Lbmz;

    const-string v1, "NEW_CHROMA_HISTOGRAM"

    const/16 v2, 0x13

    new-instance v3, Lbnq;

    invoke-direct {v3}, Lbnq;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->m:Lbmz;

    .line 46
    new-instance v0, Lbmz;

    const-string v1, "AUDIO_MFCC"

    const/16 v2, 0x14

    new-instance v3, Lbna;

    invoke-direct {v3}, Lbna;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->n:Lbmz;

    .line 47
    new-instance v0, Lbmz;

    const-string v1, "KEY_FRAME_NUMBER"

    const/16 v2, 0x15

    new-instance v3, Lbno;

    invoke-direct {v3}, Lbno;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->o:Lbmz;

    .line 48
    new-instance v0, Lbmz;

    const-string v1, "STABILIZATION_DISPLACEMENT"

    const/16 v2, 0x16

    new-instance v3, Lboa;

    invoke-direct {v3}, Lboa;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->p:Lbmz;

    .line 49
    new-instance v0, Lbmz;

    const-string v1, "STABILIZATION_INTERFRAME_TRANSFORM"

    const/16 v2, 0x17

    new-instance v3, Lboc;

    invoke-direct {v3}, Lboc;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->q:Lbmz;

    .line 50
    new-instance v0, Lbmz;

    const-string v1, "STABILIZATION_JITTER_TRANSFORM"

    const/16 v2, 0x18

    new-instance v3, Lbod;

    invoke-direct {v3}, Lbod;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->r:Lbmz;

    .line 51
    new-instance v0, Lbmz;

    const-string v1, "DELTA_MFCC"

    const/16 v2, 0x19

    new-instance v3, Lbni;

    invoke-direct {v3}, Lbni;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->s:Lbmz;

    .line 52
    new-instance v0, Lbmz;

    const-string v1, "NEW_MOTION_SALIENCY"

    const/16 v2, 0x1a

    new-instance v3, Lbns;

    invoke-direct {v3}, Lbns;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->t:Lbmz;

    .line 53
    new-instance v0, Lbmz;

    const-string v1, "AUDIO_PEAK_AMPLITUDE"

    const/16 v2, 0x1b

    new-instance v3, Lbnb;

    invoke-direct {v3}, Lbnb;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->u:Lbmz;

    .line 54
    new-instance v0, Lbmz;

    const-string v1, "SALIENCY_MAP_CUMULATIVE_VERTICAL_PROJECTION"

    const/16 v2, 0x1c

    new-instance v3, Lbnx;

    invoke-direct {v3}, Lbnx;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->v:Lbmz;

    .line 55
    new-instance v0, Lbmz;

    const-string v1, "SALIENCY_MAP_CUMULATIVE_HORIZONTAL_PROJECTION"

    const/16 v2, 0x1d

    new-instance v3, Lbnw;

    invoke-direct {v3}, Lbnw;-><init>()V

    invoke-direct {v0, v1, v2, v3}, Lbmz;-><init>(Ljava/lang/String;ILbnk;)V

    sput-object v0, Lbmz;->w:Lbmz;

    .line 24
    const/16 v0, 0x1e

    new-array v0, v0, [Lbmz;

    sget-object v1, Lbmz;->a:Lbmz;

    aput-object v1, v0, v4

    sget-object v1, Lbmz;->b:Lbmz;

    aput-object v1, v0, v5

    sget-object v1, Lbmz;->c:Lbmz;

    aput-object v1, v0, v6

    sget-object v1, Lbmz;->x:Lbmz;

    aput-object v1, v0, v7

    sget-object v1, Lbmz;->y:Lbmz;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lbmz;->d:Lbmz;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lbmz;->e:Lbmz;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lbmz;->f:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lbmz;->z:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lbmz;->A:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lbmz;->B:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lbmz;->C:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lbmz;->g:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lbmz;->h:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lbmz;->i:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lbmz;->D:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lbmz;->j:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lbmz;->k:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lbmz;->l:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lbmz;->m:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lbmz;->n:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lbmz;->o:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lbmz;->p:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lbmz;->q:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lbmz;->r:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lbmz;->s:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lbmz;->t:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lbmz;->u:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lbmz;->v:Lbmz;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lbmz;->w:Lbmz;

    aput-object v2, v0, v1

    sput-object v0, Lbmz;->F:[Lbmz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILbnk;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbnk;",
            ")V"
        }
    .end annotation

    .prologue
    .line 103
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 104
    iput-object p3, p0, Lbmz;->E:Lbnk;

    .line 105
    return-void
.end method

.method static synthetic a(Lbvj;)Lbmn;
    .locals 1

    .prologue
    .line 24
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lbmn;->a(Lbvj;)Lbmn;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(F)Ljava/lang/Float;
    .locals 1

    .prologue
    .line 24
    invoke-static {p0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(I)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 24
    const/4 v0, -0x1

    if-ne p0, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lbmz;
    .locals 1

    .prologue
    .line 24
    const-class v0, Lbmz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbmz;

    return-object v0
.end method

.method public static values()[Lbmz;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lbmz;->F:[Lbmz;

    invoke-virtual {v0}, [Lbmz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbmz;

    return-object v0
.end method


# virtual methods
.method public a(Lbvg;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lbmz;->E:Lbnk;

    invoke-interface {v0, p1}, Lbnk;->a(Lbvg;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/Object;Lbvg;)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lbmz;->E:Lbnk;

    invoke-interface {v0, p1, p2}, Lbnk;->a(Ljava/lang/Object;Lbvg;)V

    .line 66
    return-void
.end method
