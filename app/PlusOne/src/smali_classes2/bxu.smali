.class public Lbxu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbxw;


# instance fields
.field private final a:Lbxr;

.field private final b:Lbxr;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lbxu;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbxr;Lbxr;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const-string v0, "videoSummarizer"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxr;

    iput-object v0, p0, Lbxu;->a:Lbxr;

    .line 32
    const-string v0, "photoSummarizer"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbxr;

    iput-object v0, p0, Lbxu;->b:Lbxr;

    .line 33
    return-void
.end method


# virtual methods
.method public a(Lbxy;)Lbom;
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    .line 41
    iget-object v0, p1, Lbxy;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    iget-wide v0, p1, Lbxy;->c:J

    .line 43
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 71
    :goto_0
    iget-object v3, p1, Lbxy;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 72
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 83
    :goto_1
    new-instance v1, Ljava/util/ArrayList;

    .line 84
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 85
    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 86
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 88
    new-instance v0, Lbom;

    invoke-direct {v0, v1}, Lbom;-><init>(Ljava/util/List;)V

    return-object v0

    .line 48
    :cond_0
    iget-object v0, p1, Lbxy;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    move-wide v0, v4

    .line 52
    :goto_2
    new-instance v2, Lbxt;

    invoke-direct {v2}, Lbxt;-><init>()V

    iget-object v3, p1, Lbxy;->b:Ljava/util/List;

    .line 53
    invoke-virtual {v2, v3}, Lbxt;->a(Ljava/util/List;)Lbxt;

    move-result-object v2

    .line 54
    invoke-virtual {v2, v0, v1}, Lbxt;->a(J)Lbxt;

    move-result-object v0

    iget-object v1, p1, Lbxy;->f:Lbor;

    .line 55
    invoke-virtual {v0, v1}, Lbxt;->a(Lbor;)Lbxt;

    move-result-object v0

    iget-wide v2, p1, Lbxy;->d:J

    .line 56
    invoke-virtual {v0, v2, v3}, Lbxt;->b(J)Lbxt;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Lbxt;->a()Lbxs;

    move-result-object v0

    .line 58
    iget-object v1, p0, Lbxu;->b:Lbxr;

    invoke-interface {v1, v0}, Lbxr;->a(Lbxs;)Ljava/util/List;

    move-result-object v2

    .line 61
    iget-wide v0, p1, Lbxy;->c:J

    .line 62
    invoke-static {v2}, Lbya;->b(Ljava/util/List;)J

    move-result-wide v6

    sub-long/2addr v0, v6

    .line 61
    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    goto :goto_0

    .line 48
    :cond_1
    iget-object v0, p1, Lbxy;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-wide v2, v4

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbon;

    invoke-virtual {v0}, Lbon;->e()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    goto :goto_3

    :cond_2
    iget v0, p1, Lbxy;->e:I

    rsub-int/lit8 v0, v0, 0x64

    iget-wide v6, p1, Lbxy;->c:J

    int-to-long v0, v0

    mul-long/2addr v0, v6

    const-wide/16 v6, 0x64

    div-long/2addr v0, v6

    iget-wide v6, p1, Lbxy;->c:J

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    sub-long v0, v6, v0

    goto :goto_2

    .line 74
    :cond_3
    new-instance v3, Lbxt;

    invoke-direct {v3}, Lbxt;-><init>()V

    iget-object v4, p1, Lbxy;->a:Ljava/util/List;

    .line 75
    invoke-virtual {v3, v4}, Lbxt;->a(Ljava/util/List;)Lbxt;

    move-result-object v3

    .line 76
    invoke-virtual {v3, v0, v1}, Lbxt;->a(J)Lbxt;

    move-result-object v0

    iget-object v1, p1, Lbxy;->f:Lbor;

    .line 77
    invoke-virtual {v0, v1}, Lbxt;->a(Lbor;)Lbxt;

    move-result-object v0

    iget-wide v4, p1, Lbxy;->d:J

    .line 78
    invoke-virtual {v0, v4, v5}, Lbxt;->b(J)Lbxt;

    move-result-object v0

    .line 79
    invoke-virtual {v0}, Lbxt;->a()Lbxs;

    move-result-object v0

    .line 81
    iget-object v1, p0, Lbxu;->a:Lbxr;

    invoke-interface {v1, v0}, Lbxr;->a(Lbxs;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_1
.end method

.method public b(Lbxy;)J
    .locals 6

    .prologue
    const-wide v4, 0x7fffffffffffffffL

    .line 118
    new-instance v0, Lbxt;

    invoke-direct {v0}, Lbxt;-><init>()V

    iget-object v1, p1, Lbxy;->a:Ljava/util/List;

    .line 119
    invoke-virtual {v0, v1}, Lbxt;->a(Ljava/util/List;)Lbxt;

    move-result-object v0

    .line 120
    invoke-virtual {v0, v4, v5}, Lbxt;->a(J)Lbxt;

    move-result-object v0

    iget-object v1, p1, Lbxy;->f:Lbor;

    .line 121
    invoke-virtual {v0, v1}, Lbxt;->a(Lbor;)Lbxt;

    move-result-object v0

    iget-wide v2, p1, Lbxy;->d:J

    .line 122
    invoke-virtual {v0, v2, v3}, Lbxt;->b(J)Lbxt;

    move-result-object v0

    .line 123
    invoke-virtual {v0}, Lbxt;->a()Lbxs;

    move-result-object v0

    .line 125
    new-instance v1, Lbxt;

    invoke-direct {v1}, Lbxt;-><init>()V

    iget-object v2, p1, Lbxy;->b:Ljava/util/List;

    .line 126
    invoke-virtual {v1, v2}, Lbxt;->a(Ljava/util/List;)Lbxt;

    move-result-object v1

    .line 127
    invoke-virtual {v1, v4, v5}, Lbxt;->a(J)Lbxt;

    move-result-object v1

    iget-object v2, p1, Lbxy;->f:Lbor;

    .line 128
    invoke-virtual {v1, v2}, Lbxt;->a(Lbor;)Lbxt;

    move-result-object v1

    iget-wide v2, p1, Lbxy;->d:J

    .line 129
    invoke-virtual {v1, v2, v3}, Lbxt;->b(J)Lbxt;

    move-result-object v1

    .line 130
    invoke-virtual {v1}, Lbxt;->a()Lbxs;

    move-result-object v1

    .line 132
    iget-object v2, p0, Lbxu;->a:Lbxr;

    invoke-interface {v2, v0}, Lbxr;->b(Lbxs;)J

    move-result-wide v2

    iget-object v0, p0, Lbxu;->b:Lbxr;

    .line 133
    invoke-interface {v0, v1}, Lbxr;->b(Lbxs;)J

    move-result-wide v0

    add-long/2addr v0, v2

    return-wide v0
.end method
