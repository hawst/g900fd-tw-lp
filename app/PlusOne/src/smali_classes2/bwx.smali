.class final Lbwx;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbwz;


# instance fields
.field final a:Ljava/lang/Object;

.field final b:Z

.field final c:Ljava/util/concurrent/atomic/AtomicBoolean;

.field d:I

.field e:Lbkr;

.field f:Ljava/lang/Exception;

.field private final g:Ljava/util/concurrent/CountDownLatch;

.field private final h:J

.field private final i:Lbwy;

.field private j:J


# direct methods
.method public constructor <init>(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 154
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 136
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbwx;->a:Ljava/lang/Object;

    .line 155
    iput-boolean p1, p0, Lbwx;->b:Z

    .line 156
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lbwx;->g:Ljava/util/concurrent/CountDownLatch;

    .line 157
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lbwx;->h:J

    .line 158
    new-instance v0, Lbwy;

    invoke-direct {v0, p0}, Lbwy;-><init>(Lbwx;)V

    iput-object v0, p0, Lbwx;->i:Lbwy;

    .line 159
    iput v2, p0, Lbwx;->d:I

    .line 161
    if-eqz p1, :cond_0

    .line 162
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lbwx;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 166
    :goto_0
    return-void

    .line 164
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lbwx;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    goto :goto_0
.end method


# virtual methods
.method public a()Lahy;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lbwx;->i:Lbwy;

    return-object v0
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 185
    const/4 v0, 0x0

    .line 186
    iget-boolean v1, p0, Lbwx;->b:Z

    if-eqz v1, :cond_3

    .line 187
    new-instance v0, Lbvw;

    iget-object v1, p0, Lbwx;->c:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Lbvw;-><init>(Ljava/util/concurrent/atomic/AtomicBoolean;)V

    .line 188
    invoke-virtual {v0}, Lbvw;->a()V

    move-object v1, v0

    .line 192
    :goto_0
    :try_start_0
    iget-object v0, p0, Lbwx;->g:Ljava/util/concurrent/CountDownLatch;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p1, p2, v2}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 193
    iget-object v2, p0, Lbwx;->a:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 194
    const/4 v0, 0x4

    :try_start_1
    iput v0, p0, Lbwx;->d:I

    .line 195
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 196
    :try_start_2
    invoke-virtual {p0}, Lbwx;->g()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 205
    :cond_0
    iget-boolean v0, p0, Lbwx;->b:Z

    if-eqz v0, :cond_1

    .line 206
    invoke-virtual {v1}, Lbvw;->b()V

    .line 209
    :cond_1
    :goto_1
    return-void

    .line 195
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 199
    :catch_0
    move-exception v0

    :try_start_5
    iget-object v2, p0, Lbwx;->a:Ljava/lang/Object;

    monitor-enter v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 200
    const/4 v0, 0x4

    :try_start_6
    iput v0, p0, Lbwx;->d:I

    .line 201
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 202
    :try_start_7
    invoke-virtual {p0}, Lbwx;->g()V

    .line 203
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 205
    iget-boolean v0, p0, Lbwx;->b:Z

    if-eqz v0, :cond_1

    .line 206
    invoke-virtual {v1}, Lbvw;->b()V

    goto :goto_1

    .line 201
    :catchall_1
    move-exception v0

    :try_start_8
    monitor-exit v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :try_start_9
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 205
    :catchall_2
    move-exception v0

    iget-boolean v2, p0, Lbwx;->b:Z

    if-eqz v2, :cond_2

    .line 206
    invoke-virtual {v1}, Lbvw;->b()V

    :cond_2
    throw v0

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public b()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 213
    iget-object v1, p0, Lbwx;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 214
    :try_start_0
    iget v2, p0, Lbwx;->d:I

    if-ne v2, v0, :cond_0

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 215
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()Z
    .locals 3

    .prologue
    .line 220
    iget-object v1, p0, Lbwx;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 221
    :try_start_0
    iget v0, p0, Lbwx;->d:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 222
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()Z
    .locals 3

    .prologue
    .line 227
    iget-object v1, p0, Lbwx;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 228
    :try_start_0
    iget v0, p0, Lbwx;->d:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 229
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public e()Z
    .locals 3

    .prologue
    .line 234
    iget-object v1, p0, Lbwx;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 235
    :try_start_0
    iget v0, p0, Lbwx;->d:I

    const/4 v2, 0x4

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 236
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public f()J
    .locals 6

    .prologue
    .line 259
    iget-object v1, p0, Lbwx;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 260
    :try_start_0
    iget-wide v2, p0, Lbwx;->j:J

    iget-wide v4, p0, Lbwx;->h:J

    sub-long/2addr v2, v4

    monitor-exit v1

    return-wide v2

    .line 261
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method g()V
    .locals 4

    .prologue
    .line 306
    iget-object v1, p0, Lbwx;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 307
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lbwx;->j:J

    .line 308
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 309
    iget-object v0, p0, Lbwx;->g:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 310
    return-void

    .line 308
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 266
    iget-object v1, p0, Lbwx;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 268
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lbwx;->d:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, Lbwx;->h:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-wide v4, p0, Lbwx;->j:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    iget-object v4, p0, Lbwx;->e:Lbkr;

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget-object v4, p0, Lbwx;->f:Ljava/lang/Exception;

    aput-object v4, v2, v3

    .line 267
    invoke-static {v0, v2}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    monitor-exit v1

    return-object v0

    .line 269
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
