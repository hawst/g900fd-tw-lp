.class public final Lcmy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:F

.field public b:F

.field public c:F

.field public d:I

.field public e:J


# direct methods
.method public constructor <init>(Landroid/view/MotionEvent;I)V
    .locals 2

    .prologue
    .line 201
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 202
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcmy;->e:J

    .line 203
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    iput v0, p0, Lcmy;->a:F

    .line 204
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    iput v0, p0, Lcmy;->b:F

    .line 205
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getTouchMinor(I)F

    move-result v0

    iput v0, p0, Lcmy;->c:F

    .line 206
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    iput v0, p0, Lcmy;->d:I

    .line 208
    iget v0, p0, Lcmy;->d:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 209
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    .line 210
    if-ne p2, v0, :cond_0

    .line 211
    const/4 v0, 0x1

    iput v0, p0, Lcmy;->d:I

    .line 214
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Landroid/graphics/Rect;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 217
    invoke-virtual {p0, v0, v0}, Lcmy;->a(II)Landroid/graphics/Rect;

    move-result-object v0

    return-object v0
.end method

.method public a(II)Landroid/graphics/Rect;
    .locals 6

    .prologue
    .line 221
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lcmy;->a:F

    int-to-float v2, p1

    add-float/2addr v1, v2

    iget v2, p0, Lcmy;->c:F

    sub-float/2addr v1, v2

    float-to-double v2, v1

    .line 222
    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v1, v2

    iget v2, p0, Lcmy;->b:F

    int-to-float v3, p2

    add-float/2addr v2, v3

    iget v3, p0, Lcmy;->c:F

    sub-float/2addr v2, v3

    float-to-double v2, v2

    .line 223
    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-int v2, v2

    iget v3, p0, Lcmy;->a:F

    int-to-float v4, p1

    add-float/2addr v3, v4

    iget v4, p0, Lcmy;->c:F

    add-float/2addr v3, v4

    float-to-double v4, v3

    .line 224
    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v3, v4

    iget v4, p0, Lcmy;->b:F

    int-to-float v5, p2

    add-float/2addr v4, v5

    iget v5, p0, Lcmy;->c:F

    add-float/2addr v4, v5

    float-to-double v4, v4

    .line 225
    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    double-to-int v4, v4

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    return-object v0
.end method
