.class public final Lbii;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbih;


# instance fields
.field private final a:Landroid/media/MediaExtractor;


# direct methods
.method public constructor <init>(Landroid/media/MediaExtractor;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lbii;->a:Landroid/media/MediaExtractor;

    .line 17
    return-void
.end method


# virtual methods
.method public a(Ljava/nio/ByteBuffer;I)I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lbii;->a:Landroid/media/MediaExtractor;

    invoke-virtual {v0, p1, p2}, Landroid/media/MediaExtractor;->readSampleData(Ljava/nio/ByteBuffer;I)I

    move-result v0

    return v0
.end method

.method public a(I)Landroid/media/MediaFormat;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lbii;->a:Landroid/media/MediaExtractor;

    invoke-virtual {v0, p1}, Landroid/media/MediaExtractor;->getTrackFormat(I)Landroid/media/MediaFormat;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lbii;->a:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Landroid/media/MediaExtractor;->release()V

    .line 77
    return-void
.end method

.method public a(J)V
    .locals 0

    .prologue
    .line 57
    return-void
.end method

.method public a(JI)V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lbii;->a:Landroid/media/MediaExtractor;

    invoke-virtual {v0, p1, p2, p3}, Landroid/media/MediaExtractor;->seekTo(JI)V

    .line 62
    return-void
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lbii;->a:Landroid/media/MediaExtractor;

    invoke-virtual {v0, p1}, Landroid/media/MediaExtractor;->selectTrack(I)V

    .line 67
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lbii;->a:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Landroid/media/MediaExtractor;->advance()Z

    move-result v0

    return v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lbii;->a:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Landroid/media/MediaExtractor;->getSampleFlags()I

    move-result v0

    return v0
.end method

.method public c(I)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lbii;->a:Landroid/media/MediaExtractor;

    invoke-virtual {v0, p1}, Landroid/media/MediaExtractor;->unselectTrack(I)V

    .line 72
    return-void
.end method

.method public d()J
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lbii;->a:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Landroid/media/MediaExtractor;->getSampleTime()J

    move-result-wide v0

    return-wide v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lbii;->a:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Landroid/media/MediaExtractor;->getSampleTrackIndex()I

    move-result v0

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lbii;->a:Landroid/media/MediaExtractor;

    invoke-virtual {v0}, Landroid/media/MediaExtractor;->getTrackCount()I

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x1

    return v0
.end method

.method public h()V
    .locals 0

    .prologue
    .line 89
    return-void
.end method
