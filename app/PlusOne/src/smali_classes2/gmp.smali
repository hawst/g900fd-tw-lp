.class final Lgmp;
.super Lgsg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lgsg",
        "<",
        "Lcom/google/android/gms/common/api/Status;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Landroid/accounts/Account;


# direct methods
.method constructor <init>(Landroid/accounts/Account;)V
    .locals 0

    iput-object p1, p0, Lgmp;->a:Landroid/accounts/Account;

    invoke-direct {p0}, Lgsg;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Status;
    .locals 0

    return-object p1
.end method

.method protected bridge synthetic a(Lgfr;)V
    .locals 0

    check-cast p1, Lgmm;

    invoke-virtual {p0, p1}, Lgmp;->a(Lgmm;)V

    return-void
.end method

.method protected a(Lgmm;)V
    .locals 2

    iget-object v0, p0, Lgmp;->a:Landroid/accounts/Account;

    invoke-virtual {p1, v0}, Lgmm;->b(Landroid/accounts/Account;)I

    move-result v0

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    packed-switch v0, :pswitch_data_0

    const/16 v0, 0x8

    :goto_0
    invoke-direct {v1, v0}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-virtual {p0, v1}, Lgmp;->a(Lggf;)V

    return-void

    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :pswitch_1
    const/16 v0, 0xdb3

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x5

    goto :goto_0

    :pswitch_3
    const/16 v0, 0xa

    goto :goto_0

    :pswitch_4
    const/16 v0, 0xdac

    goto :goto_0

    :pswitch_5
    const/16 v0, 0xdad

    goto :goto_0

    :pswitch_6
    const/16 v0, 0xdae

    goto :goto_0

    :pswitch_7
    const/16 v0, 0xdaf

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected synthetic b(Lcom/google/android/gms/common/api/Status;)Lggf;
    .locals 1

    invoke-virtual {p0, p1}, Lgmp;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    return-object v0
.end method
