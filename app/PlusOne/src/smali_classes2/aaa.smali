.class Laaa;
.super Lzx;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xf
.end annotation


# instance fields
.field public e:Lada;

.field public f:Landroid/graphics/SurfaceTexture;

.field private g:[F

.field private h:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljavax/microedition/khronos/egl/EGLContext;",
            "Landroid/graphics/SurfaceTexture;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/graphics/SurfaceTexture;",
            "Lacw;",
            ">;"
        }
    .end annotation
.end field

.field private j:Lacm;

.field private k:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;


# direct methods
.method constructor <init>(Lzw;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 265
    invoke-direct {p0}, Lzx;-><init>()V

    .line 277
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Laaa;->g:[F

    .line 280
    iput-object v1, p0, Laaa;->e:Lada;

    .line 281
    iput-object v1, p0, Laaa;->f:Landroid/graphics/SurfaceTexture;

    .line 284
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Laaa;->h:Ljava/util/HashMap;

    .line 288
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Laaa;->i:Ljava/util/HashMap;

    .line 292
    iput-object v1, p0, Laaa;->j:Lacm;

    .line 468
    new-instance v0, Laac;

    invoke-direct {v0, p0}, Laac;-><init>(Laaa;)V

    iput-object v0, p0, Laaa;->k:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;

    return-void
.end method


# virtual methods
.method protected a(Landroid/graphics/SurfaceTexture;)Lacw;
    .locals 2

    .prologue
    .line 395
    iget-object v0, p0, Laaa;->i:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacw;

    .line 396
    if-nez v0, :cond_0

    .line 397
    invoke-static {}, Lacw;->a()Lacw;

    move-result-object v0

    invoke-virtual {v0, p1}, Lacw;->a(Landroid/graphics/SurfaceTexture;)Lacw;

    move-result-object v0

    .line 398
    iget-object v1, p0, Laaa;->i:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 400
    :cond_0
    return-object v0
.end method

.method public declared-synchronized a()V
    .locals 1

    .prologue
    .line 296
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Laaa;->c:Laaf;

    invoke-static {v0}, Laaf;->a(Laaf;)Landroid/hardware/Camera;

    move-result-object v0

    invoke-virtual {p0, v0}, Laaa;->a(Landroid/hardware/Camera;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 297
    monitor-exit p0

    return-void

    .line 296
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Laas;)V
    .locals 5

    .prologue
    .line 335
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lacw;->h()Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v0

    .line 336
    invoke-virtual {p0, v0}, Laaa;->d(Ljavax/microedition/khronos/egl/EGLContext;)Lada;

    move-result-object v1

    .line 337
    invoke-virtual {p0, v0}, Laaa;->e(Ljavax/microedition/khronos/egl/EGLContext;)Lacm;

    move-result-object v2

    .line 338
    invoke-virtual {p0, v0}, Laaa;->f(Ljavax/microedition/khronos/egl/EGLContext;)Landroid/graphics/SurfaceTexture;

    move-result-object v0

    .line 339
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    if-nez v0, :cond_1

    .line 340
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Attempting to grab camera frame from unknown thread: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 345
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 346
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    invoke-virtual {p1, v0}, Laas;->a([I)V

    .line 347
    invoke-virtual {p1}, Laas;->m()Lacw;

    move-result-object v0

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v1, v0, v3, v4}, Lacm;->a(Lada;Lacw;II)V

    .line 352
    iget-object v0, p0, Laaa;->f:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->getTimestamp()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Laas;->a(J)V

    .line 353
    invoke-virtual {p1}, Laas;->h()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 354
    monitor-exit p0

    return-void

    .line 346
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method protected a(Lacm;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    .line 440
    iget-object v0, p0, Laaa;->c:Laaf;

    invoke-static {v0}, Laaf;->b(Laaf;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Laaa;->c:Laaf;

    invoke-static {v0}, Laaf;->c(Laaf;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 441
    const/high16 v0, -0x40800000    # -1.0f

    invoke-virtual {p1, v2, v3, v0, v2}, Lacm;->b(FFFF)V

    .line 445
    :goto_0
    return-void

    .line 443
    :cond_0
    invoke-virtual {p1, v3, v3, v2, v2}, Lacm;->b(FFFF)V

    goto :goto_0
.end method

.method protected a(Landroid/hardware/Camera;)V
    .locals 4

    .prologue
    .line 404
    iget-object v0, p0, Laaa;->e:Lada;

    if-nez v0, :cond_0

    .line 405
    invoke-static {}, Lada;->b()Lada;

    move-result-object v0

    iput-object v0, p0, Laaa;->e:Lada;

    .line 407
    :cond_0
    iget-object v0, p0, Laaa;->f:Landroid/graphics/SurfaceTexture;

    if-nez v0, :cond_1

    .line 408
    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Laaa;->e:Lada;

    invoke-virtual {v1}, Lada;->c()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, Laaa;->f:Landroid/graphics/SurfaceTexture;

    .line 410
    :try_start_0
    iget-object v0, p0, Laaa;->f:Landroid/graphics/SurfaceTexture;

    invoke-virtual {p1, v0}, Landroid/hardware/Camera;->setPreviewTexture(Landroid/graphics/SurfaceTexture;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 415
    iget-object v0, p0, Laaa;->f:Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Laaa;->k:Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V

    .line 417
    :cond_1
    return-void

    .line 411
    :catch_0
    move-exception v0

    .line 412
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not bind camera surface texture: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public declared-synchronized b()V
    .locals 1

    .prologue
    .line 301
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Laaa;->f:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 302
    invoke-virtual {p0}, Laaa;->f()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 303
    monitor-exit p0

    return-void

    .line 301
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected b(Lacm;)V
    .locals 2

    .prologue
    .line 454
    iget-object v0, p0, Laaa;->f:Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Laaa;->g:[F

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->getTransformMatrix([F)V

    .line 455
    iget-object v0, p0, Laaa;->g:[F

    invoke-virtual {p1, v0}, Lacm;->b([F)V

    .line 456
    return-void
.end method

.method public declared-synchronized b(Ljavax/microedition/khronos/egl/EGLContext;)V
    .locals 3

    .prologue
    .line 314
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Laaa;->a(Ljavax/microedition/khronos/egl/EGLContext;)Ljava/util/Set;

    move-result-object v0

    .line 317
    invoke-virtual {p0, p1}, Laaa;->d(Ljavax/microedition/khronos/egl/EGLContext;)Lada;

    .line 318
    invoke-virtual {p0, p1}, Laaa;->e(Ljavax/microedition/khronos/egl/EGLContext;)Lacm;

    .line 319
    invoke-virtual {p0, p1}, Laaa;->f(Ljavax/microedition/khronos/egl/EGLContext;)Landroid/graphics/SurfaceTexture;

    move-result-object v1

    .line 322
    new-instance v2, Laab;

    invoke-direct {v2, v0}, Laab;-><init>(Ljava/util/Set;)V

    invoke-virtual {v1, v2}, Landroid/graphics/SurfaceTexture;->setOnFrameAvailableListener(Landroid/graphics/SurfaceTexture$OnFrameAvailableListener;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 330
    monitor-exit p0

    return-void

    .line 314
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c()V
    .locals 1

    .prologue
    .line 358
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Laaa;->e:Lada;

    if-eqz v0, :cond_0

    .line 359
    iget-object v0, p0, Laaa;->e:Lada;

    invoke-virtual {v0}, Lada;->g()V

    .line 360
    const/4 v0, 0x0

    iput-object v0, p0, Laaa;->e:Lada;

    .line 362
    :cond_0
    iget-object v0, p0, Laaa;->f:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_1

    .line 363
    iget-object v0, p0, Laaa;->f:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 364
    const/4 v0, 0x0

    iput-object v0, p0, Laaa;->f:Landroid/graphics/SurfaceTexture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 366
    :cond_1
    monitor-exit p0

    return-void

    .line 358
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected c(Ljavax/microedition/khronos/egl/EGLContext;)V
    .locals 1

    .prologue
    .line 460
    invoke-super {p0, p1}, Lzx;->c(Ljavax/microedition/khronos/egl/EGLContext;)V

    .line 461
    iget-object v0, p0, Laaa;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/SurfaceTexture;

    .line 462
    if-eqz v0, :cond_0

    .line 463
    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    .line 464
    iget-object v0, p0, Laaa;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 466
    :cond_0
    return-void
.end method

.method protected d()Lacm;
    .locals 2

    .prologue
    .line 370
    new-instance v0, Lacm;

    const-string v1, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method protected e()Lada;
    .locals 1

    .prologue
    .line 375
    invoke-static {}, Lada;->b()Lada;

    move-result-object v0

    return-object v0
.end method

.method protected f(Ljavax/microedition/khronos/egl/EGLContext;)Landroid/graphics/SurfaceTexture;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 427
    iget-object v0, p0, Laaa;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/SurfaceTexture;

    .line 428
    if-nez v0, :cond_0

    .line 429
    invoke-virtual {p0, p1}, Laaa;->d(Ljavax/microedition/khronos/egl/EGLContext;)Lada;

    move-result-object v1

    .line 430
    if-eqz v1, :cond_0

    .line 431
    new-instance v0, Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1}, Lada;->c()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    .line 432
    invoke-virtual {v0, v2, v2}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    .line 433
    iget-object v1, p0, Laaa;->h:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 436
    :cond_0
    return-object v0
.end method

.method protected f()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 379
    invoke-virtual {p0}, Laaa;->g()Lacm;

    move-result-object v0

    invoke-virtual {p0, v0}, Laaa;->b(Lacm;)V

    .line 380
    invoke-virtual {p0}, Laaa;->g()Lacm;

    move-result-object v0

    invoke-virtual {p0, v0}, Laaa;->a(Lacm;)V

    .line 382
    iget-object v0, p0, Laaa;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/SurfaceTexture;

    .line 383
    invoke-virtual {p0, v0}, Laaa;->a(Landroid/graphics/SurfaceTexture;)Lacw;

    move-result-object v0

    .line 384
    invoke-virtual {v0}, Lacw;->d()V

    .line 385
    invoke-virtual {p0}, Laaa;->g()Lacm;

    move-result-object v2

    iget-object v3, p0, Laaa;->e:Lada;

    invoke-virtual {v2, v3, v0, v4, v4}, Lacm;->a(Lada;Lacw;II)V

    .line 389
    const-string v2, "distribute frames"

    invoke-static {v2}, Labi;->a(Ljava/lang/String;)V

    .line 390
    invoke-virtual {v0}, Lacw;->f()V

    goto :goto_0

    .line 392
    :cond_0
    return-void
.end method

.method protected g()Lacm;
    .locals 2

    .prologue
    .line 420
    iget-object v0, p0, Laaa;->j:Lacm;

    if-nez v0, :cond_0

    .line 421
    new-instance v0, Lacm;

    const-string v1, "#extension GL_OES_EGL_image_external : require\nprecision mediump float;\nuniform samplerExternalOES tex_sampler_0;\nvarying vec2 v_texcoord;\nvoid main() {\n  gl_FragColor = texture2D(tex_sampler_0, v_texcoord);\n}\n"

    invoke-direct {v0, v1}, Lacm;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Laaa;->j:Lacm;

    .line 423
    :cond_0
    iget-object v0, p0, Laaa;->j:Lacm;

    return-object v0
.end method
