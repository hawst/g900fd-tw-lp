.class public final Ljhy;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljhh;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljhp;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljho;

.field private final f:Ldr;

.field private final g:Ljhq;

.field private final h:Z

.field private final i:Ljib;

.field private final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljhk;",
            ">;"
        }
    .end annotation
.end field

.field private final k:Llfn;

.field private final l:Ljava/lang/Object;

.field private final m:Ljava/lang/Object;

.field private n:J

.field private o:Z


# direct methods
.method constructor <init>(Landroid/content/Context;ILjava/util/Map;Z)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljhk;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .line 107
    new-instance v5, Ljho;

    invoke-direct {v5, p1}, Ljho;-><init>(Landroid/content/Context;)V

    .line 108
    invoke-static {p1}, Ldr;->a(Landroid/content/Context;)Ldr;

    move-result-object v6

    new-instance v7, Ljhq;

    invoke-direct {v7, p1}, Ljhq;-><init>(Landroid/content/Context;)V

    const-wide/32 v8, 0x1d4c0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    .line 107
    invoke-direct/range {v0 .. v9}, Ljhy;-><init>(Landroid/content/Context;ILjava/util/Map;ZLjho;Ldr;Ljhq;J)V

    .line 110
    return-void
.end method

.method constructor <init>(Landroid/content/Context;ILjava/util/Map;ZLjho;Ldr;Ljhq;J)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljhk;",
            ">;Z",
            "Ljho;",
            "Ldr;",
            "Ljhq;",
            "J)V"
        }
    .end annotation

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Ljhy;->d:Ljava/util/List;

    .line 83
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ljhy;->l:Ljava/lang/Object;

    .line 85
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Ljhy;->m:Ljava/lang/Object;

    .line 132
    iput-object p1, p0, Ljhy;->a:Landroid/content/Context;

    .line 133
    iput p2, p0, Ljhy;->b:I

    .line 134
    iput-object p3, p0, Ljhy;->j:Ljava/util/Map;

    .line 135
    iput-boolean p4, p0, Ljhy;->h:Z

    .line 136
    iput-object p5, p0, Ljhy;->e:Ljho;

    .line 137
    iput-object p6, p0, Ljhy;->f:Ldr;

    .line 138
    iput-object p7, p0, Ljhy;->g:Ljhq;

    .line 139
    iput-wide p8, p0, Ljhy;->n:J

    .line 141
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 142
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 143
    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljhy;->c:Ljava/lang/String;

    .line 145
    const-class v0, Ljib;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljib;

    iput-object v0, p0, Ljhy;->i:Ljib;

    .line 146
    const-class v0, Llfn;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llfn;

    iput-object v0, p0, Ljhy;->k:Llfn;

    .line 147
    return-void
.end method

.method static a(Landroid/content/Context;I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 972
    const-class v0, Lhei;

    invoke-static {p0, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 973
    invoke-interface {v0, p1}, Lhei;->c(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 974
    const-string v0, ""

    .line 984
    :cond_0
    :goto_0
    return-object v0

    .line 976
    :cond_1
    invoke-interface {v0, p1}, Lhei;->a(I)Lhej;

    move-result-object v0

    .line 977
    const-string v1, "display_name"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 978
    const-string v2, "account_name"

    invoke-interface {v0, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 979
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 981
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object v0, v1

    .line 982
    goto :goto_0

    .line 984
    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private a(I)V
    .locals 8

    .prologue
    .line 742
    invoke-direct {p0}, Ljhy;->i()Ljhp;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 743
    :goto_0
    if-nez v0, :cond_1

    .line 747
    :goto_1
    return-void

    .line 742
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 746
    :cond_1
    int-to-long v0, p1

    const-string v2, "NetworkQueue"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-wide/16 v2, 0x3e8

    div-long v2, v0, v2

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x33

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Scheduling process in "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " seconds."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Ljhy;->k:Llfn;

    iget v4, p0, Ljhy;->b:I

    const-wide/16 v6, 0x4

    invoke-interface {v3, v4, v6, v7, v2}, Llfn;->a(IJLjava/lang/String;)V

    :cond_2
    iget-object v2, p0, Ljhy;->a:Landroid/content/Context;

    iget v3, p0, Ljhy;->b:I

    invoke-static {v2, v3, v0, v1}, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueAlarmReceiver;->a(Landroid/content/Context;IJ)V

    goto :goto_1
.end method

.method private static a(Ljhn;Ljhn;)V
    .locals 2

    .prologue
    .line 791
    iget v0, p1, Ljhn;->c:I

    iget v1, p0, Ljhn;->c:I

    add-int/2addr v0, v1

    iput v0, p1, Ljhn;->c:I

    .line 792
    iget v0, p1, Ljhn;->d:I

    iget v1, p0, Ljhn;->d:I

    add-int/2addr v0, v1

    iput v0, p1, Ljhn;->d:I

    .line 793
    iget v0, p1, Ljhn;->a:I

    iget v1, p0, Ljhn;->a:I

    add-int/2addr v0, v1

    iput v0, p1, Ljhn;->a:I

    .line 794
    iget v0, p1, Ljhn;->b:I

    iget v1, p0, Ljhn;->b:I

    add-int/2addr v0, v1

    iput v0, p1, Ljhn;->b:I

    .line 795
    iget v0, p1, Ljhn;->e:I

    iget v1, p0, Ljhn;->e:I

    add-int/2addr v0, v1

    iput v0, p1, Ljhn;->e:I

    .line 796
    return-void
.end method

.method private b(Ljhp;)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    .line 648
    iget-object v1, p0, Ljhy;->l:Ljava/lang/Object;

    monitor-enter v1

    .line 649
    :try_start_0
    iget-object v2, p0, Ljhy;->d:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 650
    monitor-exit v1

    .line 694
    :goto_0
    return v0

    .line 657
    :cond_0
    invoke-virtual {p1}, Ljhp;->c()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 710
    new-instance v0, Ljava/lang/IllegalStateException;

    .line 711
    invoke-virtual {p1}, Ljhp;->c()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x55

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Processed request is in an unknown state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Keeping request in this queue."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 714
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 659
    :pswitch_0
    :try_start_1
    const-string v2, "NetworkQueue"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 660
    iget-object v2, p0, Ljhy;->k:Llfn;

    iget v3, p0, Ljhy;->b:I

    const-wide/16 v4, 0x4

    const-string v6, "Processed request was successfully handled. Removing request from this queue."

    invoke-interface {v2, v3, v4, v5, v6}, Llfn;->a(IJLjava/lang/String;)V

    .line 666
    :cond_1
    iget-object v2, p0, Ljhy;->i:Ljib;

    iget v3, p0, Ljhy;->b:I

    invoke-interface {v2, v3, p1}, Ljib;->a(ILjhp;)V

    .line 667
    iget-object v2, p0, Ljhy;->d:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 668
    monitor-exit v1

    goto :goto_0

    .line 672
    :pswitch_1
    const-string v2, "NetworkQueue"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 673
    iget-object v2, p0, Ljhy;->k:Llfn;

    iget v3, p0, Ljhy;->b:I

    const-wide/16 v4, 0x4

    const-string v6, "Processed request has permanently failed to be handled. Keeping request in this queue."

    invoke-interface {v2, v3, v4, v5, v6}, Llfn;->a(IJLjava/lang/String;)V

    .line 679
    :cond_2
    invoke-direct {p0, p1}, Ljhy;->c(Ljhp;)V

    .line 680
    monitor-exit v1

    goto :goto_0

    .line 686
    :pswitch_2
    const-string v0, "NetworkQueue"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 687
    iget-object v0, p0, Ljhy;->k:Llfn;

    iget v2, p0, Ljhy;->b:I

    const-wide/16 v4, 0x4

    const-string v3, "Processed request remains in a processable state. Keeping request in this queue."

    invoke-interface {v0, v2, v4, v5, v3}, Llfn;->a(IJLjava/lang/String;)V

    .line 693
    :cond_3
    invoke-direct {p0, p1}, Ljhy;->c(Ljhp;)V

    .line 694
    const/4 v0, 0x0

    monitor-exit v1

    goto/16 :goto_0

    .line 698
    :pswitch_3
    const-string v0, "NetworkQueue"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 701
    iget-object v0, p0, Ljhy;->k:Llfn;

    iget v2, p0, Ljhy;->b:I

    const-wide/16 v4, 0x4

    const-string v3, "Processed request is in a pending state, which is invalid. Marking this request as permanently failed and keeping request in this queue."

    invoke-interface {v0, v2, v4, v5, v3}, Llfn;->a(IJLjava/lang/String;)V

    .line 705
    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Processed request is in a pending state, which is invalid. Marking this request as permanently failed and keeping request in this queue."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 657
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private c(Ljhp;)V
    .locals 8

    .prologue
    .line 719
    iget-object v1, p0, Ljhy;->l:Ljava/lang/Object;

    monitor-enter v1

    .line 720
    :try_start_0
    invoke-virtual {p1}, Ljhp;->a()Ljhl;

    move-result-object v0

    invoke-interface {v0}, Ljhl;->a()Ljava/lang/String;

    move-result-object v0

    .line 721
    iget-object v2, p0, Ljhy;->j:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhk;

    .line 722
    if-nez v0, :cond_0

    .line 723
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Attempting to serialize a request in this network queue whose serializer cannot be found."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 734
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 727
    :cond_0
    :try_start_1
    iget-object v2, p0, Ljhy;->k:Llfn;

    iget v3, p0, Ljhy;->b:I

    const-wide/16 v4, 0x4

    const-string v6, "Serializing item."

    invoke-interface {v2, v3, v4, v5, v6}, Llfn;->a(IJLjava/lang/String;)V

    .line 729
    iget-object v2, p0, Ljhy;->i:Ljib;

    iget v3, p0, Ljhy;->b:I

    invoke-interface {v2, v3, p1, v0}, Ljib;->a(ILjhp;Ljhk;)J

    move-result-wide v2

    .line 730
    iget-object v0, p0, Ljhy;->k:Llfn;

    iget v4, p0, Ljhy;->b:I

    const-wide/16 v6, 0x4

    const-string v5, "Serialized item."

    invoke-interface {v0, v4, v6, v7, v5}, Llfn;->a(IJLjava/lang/String;)V

    .line 733
    invoke-virtual {p1, v2, v3}, Ljhp;->a(J)V

    .line 734
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private d(J)Ljhp;
    .locals 7

    .prologue
    .line 286
    iget-object v2, p0, Ljhy;->l:Ljava/lang/Object;

    monitor-enter v2

    .line 287
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Ljhy;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 288
    iget-object v0, p0, Ljhy;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhp;

    .line 289
    invoke-virtual {v0}, Ljhp;->b()J

    move-result-wide v4

    cmp-long v4, v4, p1

    if-nez v4, :cond_0

    .line 290
    monitor-exit v2

    .line 294
    :goto_1
    return-object v0

    .line 287
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 293
    :cond_1
    monitor-exit v2

    .line 294
    const/4 v0, 0x0

    goto :goto_1

    .line 293
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private i()Ljhp;
    .locals 5

    .prologue
    .line 754
    iget-object v2, p0, Ljhy;->l:Ljava/lang/Object;

    monitor-enter v2

    .line 755
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Ljhy;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_2

    .line 756
    iget-object v0, p0, Ljhy;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhp;

    .line 758
    invoke-virtual {v0}, Ljhp;->c()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 775
    invoke-virtual {v0}, Ljhp;->c()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x3c

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Request in network queue is in an unknown state: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 776
    const-string v1, "NetworkQueue"

    const/4 v3, 0x6

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 777
    const-string v1, "NetworkQueue"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 779
    :cond_0
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 782
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 763
    :pswitch_0
    :try_start_1
    monitor-exit v2

    .line 783
    :goto_1
    return-object v0

    .line 767
    :pswitch_1
    const-string v0, "Request in network queue should not be present as it has been successfully handled."

    .line 769
    const-string v1, "NetworkQueue"

    const/4 v3, 0x6

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 770
    const-string v1, "NetworkQueue"

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 772
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 755
    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 782
    :cond_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 783
    const/4 v0, 0x0

    goto :goto_1

    .line 758
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private j()V
    .locals 3

    .prologue
    .line 843
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Ljhy;->a:Landroid/content/Context;

    const-class v2, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 844
    const-string v1, "network_queue_scheduler"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 846
    const-string v1, "account_id"

    iget v2, p0, Ljhy;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 847
    iget-object v1, p0, Ljhy;->a:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 848
    return-void
.end method

.method private k()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x4

    const/4 v4, 0x4

    .line 855
    const-string v0, "NetworkQueue"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 856
    iget-object v0, p0, Ljhy;->k:Llfn;

    iget v1, p0, Ljhy;->b:I

    const-string v2, "Broadcasting change in queue."

    invoke-interface {v0, v1, v6, v7, v2}, Llfn;->a(IJLjava/lang/String;)V

    .line 860
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 861
    const-string v1, "com.google.android.apps.plus.networkqueue_change"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 862
    invoke-virtual {p0}, Ljhy;->e()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 863
    iget-object v1, p0, Ljhy;->f:Ldr;

    invoke-virtual {v1, v0}, Ldr;->a(Landroid/content/Intent;)Z

    .line 865
    iget-boolean v0, p0, Ljhy;->h:Z

    if-eqz v0, :cond_1

    .line 866
    invoke-virtual {p0}, Ljhy;->g()I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Ljhy;->g:Ljhq;

    iget v1, p0, Ljhy;->b:I

    invoke-virtual {p0}, Ljhy;->a()I

    move-result v2

    invoke-direct {p0}, Ljhy;->l()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Ljhq;->a(III)V

    .line 869
    :cond_1
    :goto_0
    const-string v0, "NetworkQueue"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 870
    iget-object v0, p0, Ljhy;->k:Llfn;

    iget v1, p0, Ljhy;->b:I

    const-string v2, "Broadcast completed."

    invoke-interface {v0, v1, v6, v7, v2}, Llfn;->a(IJLjava/lang/String;)V

    .line 873
    :cond_2
    return-void

    .line 866
    :cond_3
    iget-object v0, p0, Ljhy;->g:Ljhq;

    iget v1, p0, Ljhy;->b:I

    invoke-virtual {v0, v1}, Ljhq;->a(I)V

    goto :goto_0
.end method

.method private l()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 934
    .line 935
    iget-object v3, p0, Ljhy;->l:Ljava/lang/Object;

    monitor-enter v3

    .line 936
    :try_start_0
    iget-object v1, p0, Ljhy;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v4, :cond_0

    .line 937
    iget-object v0, p0, Ljhy;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhp;

    .line 938
    invoke-virtual {v0}, Ljhp;->c()I

    move-result v0

    const/4 v5, 0x5

    if-ne v0, v5, :cond_1

    .line 939
    add-int/lit8 v0, v1, 0x1

    .line 936
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 942
    :cond_0
    monitor-exit v3

    .line 943
    return v1

    .line 942
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 327
    .line 328
    iget-object v3, p0, Ljhy;->l:Ljava/lang/Object;

    monitor-enter v3

    .line 329
    :try_start_0
    iget-object v1, p0, Ljhy;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v4, :cond_0

    .line 330
    iget-object v0, p0, Ljhy;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhp;

    .line 331
    invoke-virtual {v0}, Ljhp;->c()I

    move-result v0

    const/4 v5, 0x5

    if-eq v0, v5, :cond_1

    .line 332
    add-int/lit8 v0, v1, 0x1

    .line 329
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 335
    :cond_0
    monitor-exit v3

    .line 336
    return v1

    .line 335
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public a(Ljhl;)J
    .locals 2

    .prologue
    .line 187
    new-instance v0, Ljhp;

    invoke-direct {v0, p1}, Ljhp;-><init>(Ljhl;)V

    invoke-virtual {p0, v0}, Ljhy;->a(Ljhp;)J

    move-result-wide v0

    return-wide v0
.end method

.method a(Ljhp;)J
    .locals 11

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    const-wide/16 v0, -0x1

    const/4 v8, 0x4

    const-wide/16 v6, 0x4

    .line 199
    const-string v2, "NetworkQueue"

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 200
    iget-object v2, p0, Ljhy;->k:Llfn;

    iget v3, p0, Ljhy;->b:I

    const-string v4, "NetworkQueue"

    invoke-interface {v2, v3, v4, v6, v7}, Llfn;->a(ILjava/lang/String;J)V

    .line 204
    iget-object v2, p0, Ljhy;->k:Llfn;

    iget v3, p0, Ljhy;->b:I

    const-string v4, "Adding request."

    invoke-interface {v2, v3, v6, v7, v4}, Llfn;->a(IJLjava/lang/String;)V

    .line 208
    :cond_0
    iget-object v2, p0, Ljhy;->e:Ljho;

    iget v3, p0, Ljhy;->b:I

    invoke-virtual {v2, v3}, Ljho;->a(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 209
    const-string v2, "NetworkQueue"

    invoke-static {v2, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 210
    iget-object v2, p0, Ljhy;->k:Llfn;

    iget v3, p0, Ljhy;->b:I

    const-string v4, "Request not added to queue due to account restriction."

    invoke-interface {v2, v3, v6, v7, v4}, Llfn;->a(IJLjava/lang/String;)V

    .line 214
    iget-object v2, p0, Ljhy;->k:Llfn;

    iget v3, p0, Ljhy;->b:I

    new-array v4, v10, [Ljava/lang/String;

    const-string v5, "NetworkQueue"

    aput-object v5, v4, v9

    invoke-interface {v2, v3, v4}, Llfn;->a(I[Ljava/lang/String;)V

    .line 236
    :cond_1
    :goto_0
    return-wide v0

    .line 218
    :cond_2
    iget-object v2, p0, Ljhy;->l:Ljava/lang/Object;

    monitor-enter v2

    .line 219
    :try_start_0
    invoke-direct {p0, p1}, Ljhy;->c(Ljhp;)V

    .line 220
    invoke-virtual {p1}, Ljhp;->b()J

    move-result-wide v4

    cmp-long v3, v4, v0

    if-nez v3, :cond_3

    .line 221
    monitor-exit v2

    goto :goto_0

    .line 224
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 223
    :cond_3
    :try_start_1
    iget-object v0, p0, Ljhy;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 226
    const-string v0, "NetworkQueue"

    invoke-static {v0, v8}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 227
    iget-object v0, p0, Ljhy;->k:Llfn;

    iget v1, p0, Ljhy;->b:I

    const-string v2, "Request successfully added to the queue."

    invoke-interface {v0, v1, v6, v7, v2}, Llfn;->a(IJLjava/lang/String;)V

    .line 231
    iget-object v0, p0, Ljhy;->k:Llfn;

    iget v1, p0, Ljhy;->b:I

    new-array v2, v10, [Ljava/lang/String;

    const-string v3, "NetworkQueue"

    aput-object v3, v2, v9

    invoke-interface {v0, v1, v2}, Llfn;->a(I[Ljava/lang/String;)V

    .line 234
    :cond_4
    invoke-direct {p0}, Ljhy;->k()V

    .line 235
    invoke-direct {p0}, Ljhy;->j()V

    .line 236
    invoke-virtual {p1}, Ljhp;->b()J

    move-result-wide v0

    goto :goto_0
.end method

.method public a(Lkfp;Landroid/content/SyncResult;)V
    .locals 28

    .prologue
    .line 341
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 343
    const-string v2, "NetworkQueue"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 344
    const-string v2, "Processing queue."

    .line 345
    move-object/from16 v0, p0

    iget-object v3, v0, Ljhy;->k:Llfn;

    move-object/from16 v0, p0

    iget v4, v0, Ljhy;->b:I

    const-string v5, "NetworkQueue"

    const-wide/16 v6, 0x5

    invoke-interface {v3, v4, v5, v6, v7}, Llfn;->a(ILjava/lang/String;J)V

    .line 348
    move-object/from16 v0, p0

    iget-object v3, v0, Ljhy;->k:Llfn;

    move-object/from16 v0, p0

    iget v4, v0, Ljhy;->b:I

    const-wide/16 v6, 0x4

    invoke-interface {v3, v4, v6, v7, v2}, Llfn;->a(IJLjava/lang/String;)V

    .line 352
    :cond_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "NetworkQueue"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Ljhy;->k:Llfn;

    move-object/from16 v0, p0

    iget v3, v0, Ljhy;->b:I

    const-wide/16 v4, 0x4

    const-string v6, "Thread has been interrupted."

    invoke-interface {v2, v3, v4, v5, v6}, Llfn;->a(IJLjava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Ljhy;->k:Llfn;

    move-object/from16 v0, p0

    iget v3, v0, Ljhy;->b:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "NetworkQueue"

    aput-object v6, v4, v5

    invoke-interface {v2, v3, v4}, Llfn;->a(I[Ljava/lang/String;)V

    :cond_1
    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_8

    .line 353
    const v2, 0xea60

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljhy;->a(I)V

    .line 379
    :cond_2
    :goto_1
    return-void

    .line 352
    :cond_3
    if-eqz p1, :cond_5

    invoke-virtual/range {p1 .. p1}, Lkfp;->c()Z

    move-result v2

    if-eqz v2, :cond_5

    const-string v2, "NetworkQueue"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Ljhy;->k:Llfn;

    move-object/from16 v0, p0

    iget v3, v0, Ljhy;->b:I

    const-wide/16 v4, 0x4

    const-string v6, "Sync has been cancelled."

    invoke-interface {v2, v3, v4, v5, v6}, Llfn;->a(IJLjava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Ljhy;->k:Llfn;

    move-object/from16 v0, p0

    iget v3, v0, Ljhy;->b:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "NetworkQueue"

    aput-object v6, v4, v5

    invoke-interface {v2, v3, v4}, Llfn;->a(I[Ljava/lang/String;)V

    :cond_4
    const/4 v2, 0x1

    goto :goto_0

    :cond_5
    move-object/from16 v0, p2

    iget-boolean v2, v0, Landroid/content/SyncResult;->syncAlreadyInProgress:Z

    if-eqz v2, :cond_7

    const-string v2, "NetworkQueue"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Ljhy;->k:Llfn;

    move-object/from16 v0, p0

    iget v3, v0, Ljhy;->b:I

    const-wide/16 v4, 0x4

    const-string v6, "SyncAdapter is already performing a sync operation."

    invoke-interface {v2, v3, v4, v5, v6}, Llfn;->a(IJLjava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Ljhy;->k:Llfn;

    move-object/from16 v0, p0

    iget v3, v0, Ljhy;->b:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "NetworkQueue"

    aput-object v6, v4, v5

    invoke-interface {v2, v3, v4}, Llfn;->a(I[Ljava/lang/String;)V

    :cond_6
    const/4 v2, 0x1

    goto :goto_0

    :cond_7
    const/4 v2, 0x0

    goto :goto_0

    .line 360
    :cond_8
    const/4 v11, 0x0

    const/4 v10, 0x0

    new-instance v16, Ljhn;

    invoke-direct/range {v16 .. v16}, Ljhn;-><init>()V

    const/4 v9, 0x0

    new-instance v17, Ljhn;

    invoke-direct/range {v17 .. v17}, Ljhn;-><init>()V

    const/4 v8, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v18

    move-object/from16 v0, p0

    iget-object v0, v0, Ljhy;->m:Ljava/lang/Object;

    move-object/from16 v20, v0

    monitor-enter v20

    :goto_2
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Ljhy;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1d

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v2, v2, v18

    move-object/from16 v0, p0

    iget-wide v4, v0, Ljhy;->n:J

    sub-long v2, v4, v2

    const-string v4, "NetworkQueue"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_9

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x37

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Remaining queue processing time: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "ms"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Ljhy;->k:Llfn;

    move-object/from16 v0, p0

    iget v6, v0, Ljhy;->b:I

    const-wide/16 v12, 0x4

    invoke-interface {v5, v6, v12, v13, v4}, Llfn;->a(IJLjava/lang/String;)V

    :cond_9
    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gtz v2, :cond_f

    const-string v2, "NetworkQueue"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1d

    move-object/from16 v0, p0

    iget-object v2, v0, Ljhy;->k:Llfn;

    move-object/from16 v0, p0

    iget v3, v0, Ljhy;->b:I

    const-wide/16 v4, 0x4

    const-string v6, "Maximum processing time exceeded. Terminating current processing operation."

    invoke-interface {v2, v3, v4, v5, v6}, Llfn;->a(IJLjava/lang/String;)V

    move v12, v8

    move v13, v9

    move v5, v10

    move v4, v11

    :goto_3
    monitor-exit v20
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v2, v2, v18

    const-string v6, "NetworkQueue"

    const/4 v7, 0x4

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_b

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x31

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Queue processing duration: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    if-lez v4, :cond_a

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "success item count: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x24

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "success item duration: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_a
    if-lez v13, :cond_b

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1f

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "failure item count: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x24

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "failure item duration: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_b
    if-lez v4, :cond_c

    new-instance v2, Lkog;

    move-object/from16 v0, p0

    iget-object v3, v0, Ljhy;->c:Ljava/lang/String;

    move-object/from16 v0, v16

    iget v6, v0, Ljhn;->e:I

    move-object/from16 v0, v16

    iget v7, v0, Ljhn;->c:I

    move-object/from16 v0, v16

    iget v8, v0, Ljhn;->d:I

    move-object/from16 v0, v16

    iget v9, v0, Ljhn;->a:I

    move-object/from16 v0, v16

    iget v10, v0, Ljhn;->b:I

    invoke-direct/range {v2 .. v10}, Lkog;-><init>(Ljava/lang/String;IIIIIII)V

    move-object/from16 v0, p0

    iget-object v3, v0, Ljhy;->a:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lkog;->a(Landroid/content/Context;)V

    :cond_c
    if-lez v13, :cond_d

    new-instance v2, Lkof;

    move-object/from16 v0, p0

    iget-object v3, v0, Ljhy;->c:Ljava/lang/String;

    const/4 v4, 0x0

    move-object/from16 v0, v17

    iget v7, v0, Ljhn;->e:I

    move-object/from16 v0, v17

    iget v8, v0, Ljhn;->c:I

    move-object/from16 v0, v17

    iget v9, v0, Ljhn;->d:I

    move-object/from16 v0, v17

    iget v10, v0, Ljhn;->a:I

    move-object/from16 v0, v17

    iget v11, v0, Ljhn;->b:I

    move v5, v13

    move v6, v12

    invoke-direct/range {v2 .. v11}, Lkof;-><init>(Ljava/lang/String;ZIIIIIII)V

    move-object/from16 v0, p0

    iget-object v3, v0, Ljhy;->a:Landroid/content/Context;

    invoke-virtual {v2, v3}, Lkof;->a(Landroid/content/Context;)V

    .line 362
    :cond_d
    move-object/from16 v0, p2

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_e

    .line 363
    const v2, 0xea60

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Ljhy;->a(I)V

    .line 365
    :cond_e
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 372
    const-string v4, "NetworkQueue"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 373
    sub-long/2addr v2, v14

    .line 374
    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x3b

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Queue processing operation duration: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "ms"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 375
    move-object/from16 v0, p0

    iget-object v2, v0, Ljhy;->k:Llfn;

    move-object/from16 v0, p0

    iget v3, v0, Ljhy;->b:I

    const-wide/16 v4, 0x4

    const-string v6, "Processing completed."

    invoke-interface {v2, v3, v4, v5, v6}, Llfn;->a(IJLjava/lang/String;)V

    .line 377
    move-object/from16 v0, p0

    iget-object v2, v0, Ljhy;->k:Llfn;

    move-object/from16 v0, p0

    iget v3, v0, Ljhy;->b:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "NetworkQueue"

    aput-object v6, v4, v5

    invoke-interface {v2, v3, v4}, Llfn;->a(I[Ljava/lang/String;)V

    goto/16 :goto_1

    .line 360
    :cond_f
    :try_start_1
    invoke-direct/range {p0 .. p0}, Ljhy;->i()Ljhp;

    move-result-object v12

    if-nez v12, :cond_10

    const-string v2, "NetworkQueue"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1d

    move-object/from16 v0, p0

    iget-object v2, v0, Ljhy;->k:Llfn;

    move-object/from16 v0, p0

    iget v3, v0, Ljhy;->b:I

    const-wide/16 v4, 0x4

    const-string v6, "No requests are processable. Terminating current processing operation."

    invoke-interface {v2, v3, v4, v5, v6}, Llfn;->a(IJLjava/lang/String;)V

    move v12, v8

    move v13, v9

    move v5, v10

    move v4, v11

    goto/16 :goto_3

    :cond_10
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Ljhy;->l:Ljava/lang/Object;

    monitor-enter v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v12}, Ljhp;->c()I

    move-result v4

    const/4 v5, 0x3

    if-eq v4, v5, :cond_11

    const/4 v2, 0x2

    invoke-virtual {v12, v2}, Ljhp;->a(I)V

    const/4 v2, 0x1

    :cond_11
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v2, :cond_12

    :try_start_3
    invoke-direct/range {p0 .. p0}, Ljhy;->k()V

    :cond_12
    invoke-virtual {v12}, Ljhp;->a()Ljhl;

    move-result-object v13

    const-string v2, "NetworkQueue"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_13

    const-string v3, "Processing request with handle: "

    invoke-interface {v13}, Ljhl;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_17

    invoke-virtual {v3, v2}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Ljhy;->k:Llfn;

    move-object/from16 v0, p0

    iget v4, v0, Ljhy;->b:I

    const-wide/16 v6, 0x4

    invoke-interface {v3, v4, v6, v7, v2}, Llfn;->a(IJLjava/lang/String;)V

    :cond_13
    invoke-virtual {v12}, Ljhp;->e()Ljhn;

    move-result-object v21

    move-object/from16 v0, v21

    iget v2, v0, Ljhn;->e:I

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, v21

    iput v2, v0, Ljhn;->e:I

    invoke-virtual {v12}, Ljhp;->f()Ljhm;

    move-result-object v22

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v24

    const/4 v2, 0x0

    invoke-virtual {v12}, Ljhp;->c()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1c

    invoke-virtual {v12}, Ljhp;->b()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v2, v0, Ljhy;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v3, v0, Ljhy;->b:I

    const-wide/32 v6, 0x36ee80

    invoke-static/range {v2 .. v7}, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueAlarmReceiver;->a(Landroid/content/Context;IJJ)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    const/4 v2, 0x1

    move v3, v2

    :goto_5
    :try_start_4
    move-object/from16 v0, v21

    move-object/from16 v1, v22

    invoke-interface {v13, v0, v1}, Ljhl;->a(Ljhn;Ljhm;)I
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    move-result v2

    :goto_6
    if-eqz v3, :cond_14

    :try_start_5
    invoke-virtual {v12}, Ljhp;->b()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v3, v0, Ljhy;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v6, v0, Ljhy;->b:I

    invoke-static {v3, v6, v4, v5}, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueAlarmReceiver;->b(Landroid/content/Context;IJ)V

    :cond_14
    const/4 v3, 0x4

    if-ne v2, v3, :cond_19

    move-object/from16 v0, v21

    iget v3, v0, Ljhn;->f:I

    add-int/lit8 v3, v3, 0x1

    move-object/from16 v0, v21

    iput v3, v0, Ljhn;->f:I

    :goto_7
    move-object/from16 v0, v21

    iget v3, v0, Ljhn;->f:I

    const/16 v4, 0xa

    if-ne v3, v4, :cond_15

    const/4 v2, 0x5

    move-object/from16 v0, p2

    iget-object v3, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v4, 0x0

    iput-wide v4, v3, Landroid/content/SyncStats;->numIoExceptions:J

    :cond_15
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v3, v0, Ljhy;->l:Ljava/lang/Object;

    monitor-enter v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :try_start_6
    invoke-virtual {v12, v2}, Ljhp;->a(I)V

    monitor-exit v3
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    invoke-virtual/range {v22 .. v22}, Ljhm;->c()Z

    move-result v3

    if-eqz v3, :cond_1a

    move-object/from16 v0, p2

    iget-object v3, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v3, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v26, 0x1

    add-long v6, v6, v26

    iput-wide v6, v3, Landroid/content/SyncStats;->numIoExceptions:J

    :goto_8
    const-string v3, "NetworkQueue"

    const/4 v6, 0x4

    invoke-static {v3, v6}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_16

    sub-long v4, v4, v24

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v6, 0x2b

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Processing duration: "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "ms"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x22

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Current request state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Ljhy;->k:Llfn;

    move-object/from16 v0, p0

    iget v4, v0, Ljhy;->b:I

    const-wide/16 v6, 0x4

    invoke-interface {v3, v4, v6, v7, v2}, Llfn;->a(IJLjava/lang/String;)V

    invoke-virtual/range {v22 .. v22}, Ljhm;->c()Z

    move-result v2

    if-eqz v2, :cond_16

    move-object/from16 v0, p2

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v2, Landroid/content/SyncStats;->numIoExceptions:J

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x21

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Item errors: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Ljhy;->k:Llfn;

    move-object/from16 v0, p0

    iget v4, v0, Ljhy;->b:I

    const-wide/16 v6, 0x4

    invoke-interface {v3, v4, v6, v7, v2}, Llfn;->a(IJLjava/lang/String;)V

    :cond_16
    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Ljhy;->b(Ljhp;)Z

    move-result v6

    invoke-direct/range {p0 .. p0}, Ljhy;->k()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v12}, Ljhp;->c()I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :goto_9
    move v2, v8

    move v3, v9

    move v5, v10

    move v4, v11

    :goto_a
    if-nez v6, :cond_1b

    move v12, v2

    move v13, v3

    goto/16 :goto_3

    :catchall_0
    move-exception v2

    :try_start_8
    monitor-exit v3
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    throw v2

    :catchall_1
    move-exception v2

    monitor-exit v20
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    throw v2

    :cond_17
    :try_start_a
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_4

    :catch_0
    move-exception v2

    const-string v4, "NetworkQueue"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_18

    invoke-virtual {v2}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x14

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Caught an exception "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_18
    const/4 v2, 0x5

    goto/16 :goto_6

    :cond_19
    const/4 v3, 0x0

    move-object/from16 v0, v21

    iput v3, v0, Ljhn;->f:I
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_7

    :catchall_2
    move-exception v2

    :try_start_b
    monitor-exit v3
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    :try_start_c
    throw v2

    :cond_1a
    move-object/from16 v0, p2

    iget-object v3, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    const-wide/16 v6, 0x0

    iput-wide v6, v3, Landroid/content/SyncStats;->numIoExceptions:J

    goto/16 :goto_8

    :sswitch_0
    add-int/lit8 v11, v11, 0x1

    int-to-long v4, v10

    invoke-virtual {v12}, Ljhp;->d()Ljava/util/Date;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v22

    sub-long v2, v2, v22

    add-long/2addr v2, v4

    long-to-int v10, v2

    invoke-virtual {v12}, Ljhp;->e()Ljhn;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-static {v2, v0}, Ljhy;->a(Ljhn;Ljhn;)V

    move v2, v8

    move v3, v9

    move v5, v10

    move v4, v11

    goto :goto_a

    :sswitch_1
    add-int/lit8 v9, v9, 0x1

    int-to-long v4, v8

    invoke-virtual {v12}, Ljhp;->d()Ljava/util/Date;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v22

    sub-long v2, v2, v22

    add-long/2addr v2, v4

    long-to-int v8, v2

    invoke-virtual {v12}, Ljhp;->e()Ljhn;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-static {v2, v0}, Ljhy;->a(Ljhn;Ljhn;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto/16 :goto_9

    :cond_1b
    move v8, v2

    move v9, v3

    move v10, v5

    move v11, v4

    goto/16 :goto_2

    :cond_1c
    move v3, v2

    goto/16 :goto_5

    :cond_1d
    move v12, v8

    move v13, v9

    move v5, v10

    move v4, v11

    goto/16 :goto_3

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x5 -> :sswitch_1
    .end sparse-switch
.end method

.method public a(J)Z
    .locals 9

    .prologue
    const-wide/16 v6, 0x4

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 241
    const-string v2, "NetworkQueue"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 242
    iget-object v2, p0, Ljhy;->k:Llfn;

    iget v3, p0, Ljhy;->b:I

    const-string v4, "NetworkQueue"

    invoke-interface {v2, v3, v4, v6, v7}, Llfn;->a(ILjava/lang/String;J)V

    .line 246
    iget-object v2, p0, Ljhy;->k:Llfn;

    iget v3, p0, Ljhy;->b:I

    const-string v4, "Removing request."

    invoke-interface {v2, v3, v6, v7, v4}, Llfn;->a(IJLjava/lang/String;)V

    .line 249
    :cond_0
    iget-object v2, p0, Ljhy;->l:Ljava/lang/Object;

    monitor-enter v2

    .line 250
    :try_start_0
    invoke-direct {p0, p1, p2}, Ljhy;->d(J)Ljhp;

    move-result-object v3

    .line 251
    if-nez v3, :cond_1

    .line 252
    monitor-exit v2

    .line 278
    :goto_0
    return v0

    .line 254
    :cond_1
    invoke-virtual {v3}, Ljhp;->c()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 267
    :cond_2
    :goto_1
    iget-object v4, p0, Ljhy;->i:Ljib;

    iget v5, p0, Ljhy;->b:I

    invoke-interface {v4, v5, v3}, Ljib;->a(ILjhp;)V

    .line 268
    iget-object v4, p0, Ljhy;->d:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 270
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 271
    invoke-direct {p0}, Ljhy;->k()V

    .line 272
    invoke-virtual {p0}, Ljhy;->a()I

    move-result v2

    if-lez v2, :cond_3

    .line 273
    invoke-direct {p0}, Ljhy;->j()V

    .line 277
    :goto_2
    iget-object v2, p0, Ljhy;->k:Llfn;

    iget v3, p0, Ljhy;->b:I

    new-array v4, v1, [Ljava/lang/String;

    const-string v5, "NetworkQueue"

    aput-object v5, v4, v0

    invoke-interface {v2, v3, v4}, Llfn;->a(I[Ljava/lang/String;)V

    move v0, v1

    .line 278
    goto :goto_0

    .line 256
    :pswitch_0
    :try_start_1
    monitor-exit v2

    goto :goto_0

    .line 270
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 261
    :pswitch_1
    :try_start_2
    const-string v4, "NetworkQueue"

    const/4 v5, 0x6

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 262
    const-string v4, "NetworkQueue"

    const-string v5, "Removing an item that has already successfully processed; this request should have already been removed from the queue."

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 275
    :cond_3
    iget-object v2, p0, Ljhy;->a:Landroid/content/Context;

    iget v3, p0, Ljhy;->b:I

    invoke-static {v2, v3}, Lcom/google/android/libraries/social/networkqueue/impl/NetworkQueueAlarmReceiver;->a(Landroid/content/Context;I)V

    goto :goto_2

    .line 254
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public b()I
    .locals 1

    .prologue
    .line 395
    iget v0, p0, Ljhy;->b:I

    return v0
.end method

.method public b(J)Ljhn;
    .locals 1

    .prologue
    .line 383
    invoke-direct {p0, p1, p2}, Ljhy;->d(J)Ljhp;

    move-result-object v0

    .line 384
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ljhp;->e()Ljhn;

    move-result-object v0

    goto :goto_0
.end method

.method public c(J)Ljhm;
    .locals 1

    .prologue
    .line 389
    invoke-direct {p0, p1, p2}, Ljhy;->d(J)Ljhp;

    move-result-object v0

    .line 390
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ljhp;->f()Ljhm;

    move-result-object v0

    goto :goto_0
.end method

.method c()V
    .locals 5

    .prologue
    .line 155
    iget-object v1, p0, Ljhy;->l:Ljava/lang/Object;

    monitor-enter v1

    .line 156
    :try_start_0
    iget-boolean v0, p0, Ljhy;->o:Z

    if-eqz v0, :cond_0

    .line 157
    monitor-exit v1

    .line 182
    :goto_0
    return-void

    .line 162
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Ljhy;->o:Z

    .line 164
    iget-object v0, p0, Ljhy;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 165
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Attempting to restore to non-empty in-memory network queue."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 182
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 170
    :cond_1
    :try_start_1
    iget-object v0, p0, Ljhy;->d:Ljava/util/List;

    iget-object v2, p0, Ljhy;->i:Ljib;

    iget v3, p0, Ljhy;->b:I

    iget-object v4, p0, Ljhy;->j:Ljava/util/Map;

    invoke-interface {v2, v3, v4}, Ljib;->a(ILjava/util/Map;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 171
    iget-object v0, p0, Ljhy;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 172
    invoke-direct {p0}, Ljhy;->k()V

    .line 173
    invoke-direct {p0}, Ljhy;->j()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 182
    :cond_2
    :goto_1
    :try_start_2
    monitor-exit v1

    goto :goto_0

    .line 175
    :catch_0
    move-exception v0

    .line 176
    const-string v2, "NetworkQueue"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 177
    const-string v2, "NetworkQueue"

    const-string v3, "An exception occurred when attempting to deserialize all requests in this network queue:"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 178
    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 177
    :goto_2
    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    :cond_3
    invoke-virtual {p0}, Ljhy;->d()V

    goto :goto_1

    .line 178
    :cond_4
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public d()V
    .locals 3

    .prologue
    .line 299
    iget-object v1, p0, Ljhy;->l:Ljava/lang/Object;

    monitor-enter v1

    .line 300
    :try_start_0
    iget-object v0, p0, Ljhy;->i:Ljib;

    iget v2, p0, Ljhy;->b:I

    invoke-interface {v0, v2}, Ljib;->a(I)V

    .line 301
    iget-object v0, p0, Ljhy;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 302
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 303
    invoke-direct {p0}, Ljhy;->k()V

    .line 304
    return-void

    .line 302
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public e()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 877
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 878
    const-string v1, "account_id"

    iget v2, p0, Ljhy;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 879
    const-string v1, "networkqueue_change_displayitems"

    .line 880
    invoke-virtual {p0}, Ljhy;->f()Ljava/util/ArrayList;

    move-result-object v2

    .line 879
    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 881
    return-object v0
.end method

.method f()Ljava/util/ArrayList;
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljhw;",
            ">;"
        }
    .end annotation

    .prologue
    .line 890
    move-object/from16 v0, p0

    iget-object v15, v0, Ljhy;->l:Ljava/lang/Object;

    monitor-enter v15

    .line 892
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Ljhy;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v16

    .line 893
    new-instance v17, Ljava/util/ArrayList;

    move-object/from16 v0, v17

    move/from16 v1, v16

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 894
    const/4 v2, 0x0

    move v14, v2

    :goto_0
    move/from16 v0, v16

    if-ge v14, v0, :cond_0

    .line 895
    move-object/from16 v0, p0

    iget-object v2, v0, Ljhy;->d:Ljava/util/List;

    invoke-interface {v2, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljhp;

    .line 896
    invoke-virtual {v2}, Ljhp;->a()Ljhl;

    move-result-object v10

    .line 897
    new-instance v3, Ljhw;

    move-object/from16 v0, p0

    iget-object v4, v0, Ljhy;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget v5, v0, Ljhy;->b:I

    .line 900
    invoke-virtual {v2}, Ljhp;->d()Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Date;->getTime()J

    move-result-wide v6

    .line 901
    invoke-interface {v10}, Ljhl;->b()I

    move-result v8

    .line 902
    invoke-interface {v10}, Ljhl;->c()Ljava/lang/String;

    move-result-object v9

    .line 903
    invoke-interface {v10}, Ljhl;->d()Ljava/lang/String;

    move-result-object v10

    .line 904
    invoke-virtual {v2}, Ljhp;->c()I

    move-result v11

    .line 905
    invoke-virtual {v2}, Ljhp;->b()J

    move-result-wide v12

    invoke-direct/range {v3 .. v13}, Ljhw;-><init>(Landroid/content/Context;IJILjava/lang/String;Ljava/lang/String;IJ)V

    .line 906
    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 894
    add-int/lit8 v2, v14, 0x1

    move v14, v2

    goto :goto_0

    .line 908
    :cond_0
    monitor-exit v15

    .line 909
    return-object v17

    .line 908
    :catchall_0
    move-exception v2

    monitor-exit v15
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method g()I
    .locals 2

    .prologue
    .line 948
    iget-object v1, p0, Ljhy;->l:Ljava/lang/Object;

    monitor-enter v1

    .line 949
    :try_start_0
    iget-object v0, p0, Ljhy;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    monitor-exit v1

    return v0

    .line 950
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method h()Z
    .locals 1

    .prologue
    .line 955
    invoke-virtual {p0}, Ljhy;->g()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
