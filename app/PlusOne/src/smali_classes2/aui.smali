.class public Laui;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lboh;",
        "Ljava/lang/Void;",
        "Lbmk;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lbrc;

.field private final c:Lauj;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Laui;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Laui;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbrc;Lauj;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 25
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 26
    const-string v0, "musicLibrary"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrc;

    iput-object v0, p0, Laui;->b:Lbrc;

    .line 27
    const-string v0, "listener"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lauj;

    iput-object v0, p0, Laui;->c:Lauj;

    .line 28
    return-void
.end method


# virtual methods
.method protected varargs a([Lboh;)Lbmk;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 32
    sget-object v0, Laui;->a:Ljava/lang/String;

    aget-object v0, p1, v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x26

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Downloading beat times for soundtrack "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 33
    iget-object v0, p0, Laui;->b:Lbrc;

    aget-object v1, p1, v3

    invoke-virtual {v0, v1}, Lbrc;->a(Lboh;)Lbmk;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lbmk;)V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Laui;->c:Lauj;

    invoke-interface {v0, p1}, Lauj;->a(Lbmk;)V

    .line 39
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    check-cast p1, [Lboh;

    invoke-virtual {p0, p1}, Laui;->a([Lboh;)Lbmk;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 14
    check-cast p1, Lbmk;

    invoke-virtual {p0, p1}, Laui;->a(Lbmk;)V

    return-void
.end method
