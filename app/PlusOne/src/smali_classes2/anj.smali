.class public Lanj;
.super Lamn;
.source "PG"


# static fields
.field static final a:Laic;

.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Lahy;

.field private final d:Lant;

.field private final e:Lanq;

.field private final f:Lanr;

.field private final g:Lahx;

.field private final h:Lawb;

.field private final j:Lalg;

.field private final k:Lalo;

.field private final l:Lama;

.field private final m:Lbze;

.field private final n:Lann;

.field private final o:Land;

.field private p:Lavz;

.field private q:Lbmu;

.field private final r:Lasn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lasn",
            "<",
            "Lanl;",
            ">;"
        }
    .end annotation
.end field

.field private final s:Lasn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lasn",
            "<",
            "Lano;",
            ">;"
        }
    .end annotation
.end field

.field private t:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<***>;"
        }
    .end annotation
.end field

.field private u:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 39
    const-class v0, Lanj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lanj;->b:Ljava/lang/String;

    .line 83
    new-instance v0, Laic;

    sget-object v1, Laib;->d:Laib;

    sget-object v2, Lahx;->b:[Lahz;

    invoke-direct {v0, v1, v2}, Laic;-><init>(Laib;[Lahz;)V

    sput-object v0, Lanj;->a:Laic;

    return-void
.end method

.method public constructor <init>(Lahx;Lawb;Lalg;Lalo;Lama;Lbze;Lann;Land;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 139
    invoke-direct {p0}, Lamn;-><init>()V

    .line 86
    new-instance v0, Lanp;

    invoke-direct {v0, p0}, Lanp;-><init>(Lanj;)V

    iput-object v0, p0, Lanj;->c:Lahy;

    .line 87
    new-instance v0, Lans;

    invoke-direct {v0, p0}, Lans;-><init>(Lanj;)V

    iput-object v0, p0, Lanj;->d:Lant;

    .line 88
    new-instance v0, Lanq;

    invoke-direct {v0, p0}, Lanq;-><init>(Lanj;)V

    iput-object v0, p0, Lanj;->e:Lanq;

    .line 89
    new-instance v0, Lanr;

    invoke-direct {v0, p0}, Lanr;-><init>(Lanj;)V

    iput-object v0, p0, Lanj;->f:Lanr;

    .line 106
    new-instance v0, Lank;

    const-class v1, Lanl;

    invoke-direct {v0, p0, v1}, Lank;-><init>(Lanj;Ljava/lang/Class;)V

    iput-object v0, p0, Lanj;->r:Lasn;

    .line 119
    new-instance v0, Lasn;

    const-class v1, Lano;

    invoke-direct {v0, v1}, Lasn;-><init>(Ljava/lang/Class;)V

    iput-object v0, p0, Lanj;->s:Lasn;

    .line 140
    const-string v0, "analyzer"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahx;

    iput-object v0, p0, Lanj;->g:Lahx;

    .line 141
    const-string v0, "state"

    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawb;

    iput-object v0, p0, Lanj;->h:Lawb;

    .line 142
    const-string v0, "asyncTaskRunner"

    invoke-static {p3, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lalg;

    iput-object v0, p0, Lanj;->j:Lalg;

    .line 143
    const-string v0, "checkInputUrisTaskFactory"

    invoke-static {p4, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lalo;

    iput-object v0, p0, Lanj;->k:Lalo;

    .line 145
    const-string v0, "clipIdentifierGenerator"

    invoke-static {p5, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lama;

    iput-object v0, p0, Lanj;->l:Lama;

    .line 147
    const-string v0, "videoSplitter"

    invoke-static {p6, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbze;

    iput-object v0, p0, Lanj;->m:Lbze;

    .line 148
    const-string v0, "display"

    invoke-static {p7, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lann;

    iput-object v0, p0, Lanj;->n:Lann;

    .line 149
    const-string v0, "downloadMediaTaskFactory"

    invoke-static {p8, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Land;

    iput-object v0, p0, Lanj;->o:Land;

    .line 151
    return-void
.end method

.method static synthetic a(Lanj;)Landroid/os/AsyncTask;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lanj;->t:Landroid/os/AsyncTask;

    return-object v0
.end method

.method static synthetic a(Lanj;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lanj;->t:Landroid/os/AsyncTask;

    return-object p1
.end method

.method static synthetic a(Lanj;Lavz;)Lavz;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lanj;->p:Lavz;

    return-object p1
.end method

.method static synthetic a(Lanj;Lbmu;)Lbmu;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lanj;->q:Lbmu;

    return-object p1
.end method

.method private a(Landroid/net/Uri;Lbmu;Lbkr;)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    .line 283
    new-instance v0, Lbmf;

    invoke-direct {v0}, Lbmf;-><init>()V

    iget-object v1, p0, Lanj;->l:Lama;

    invoke-interface {v1}, Lama;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lbmf;->a(I)Lbmf;

    move-result-object v1

    iget-object v0, p2, Lbmu;->e:Lbmv;

    sget-object v2, Lbmv;->a:Lbmv;

    if-ne v0, v2, :cond_0

    sget-object v0, Ljei;->b:Ljei;

    invoke-static {p1, v0}, Ljeg;->a(Landroid/net/Uri;Ljei;)Ljeg;

    move-result-object v0

    new-instance v2, Lbon;

    invoke-virtual {p2}, Lbmu;->a()Lboo;

    move-result-object v3

    invoke-direct {v2, v0, v3, p3, v6}, Lbon;-><init>(Ljeg;Lboo;Lbkr;Z)V

    new-instance v0, Lbmp;

    invoke-interface {p3}, Lbkr;->c()Lbkn;

    move-result-object v3

    invoke-virtual {v3}, Lbkn;->c()J

    move-result-wide v4

    invoke-direct {v0, v8, v9, v4, v5}, Lbmp;-><init>(JJ)V

    invoke-virtual {v1, v0}, Lbmf;->a(Lbmp;)Lbmf;

    move-result-object v0

    iget-object v3, p0, Lanj;->m:Lbze;

    invoke-virtual {v3, v2}, Lbze;->a(Lbon;)Z

    move-result v2

    invoke-virtual {v0, v2}, Lbmf;->b(Z)Lbmf;

    sget-object v0, Lbmg;->a:Lbmg;

    :goto_0
    invoke-virtual {v1, v0}, Lbmf;->a(Lbmg;)Lbmf;

    invoke-static {v0}, Lbpm;->a(Lbmg;)Ljei;

    move-result-object v0

    invoke-static {p1, v0}, Ljeg;->a(Landroid/net/Uri;Ljei;)Ljeg;

    move-result-object v0

    invoke-virtual {v1, v0}, Lbmf;->a(Ljeg;)Lbmf;

    invoke-virtual {v1}, Lbmf;->a()Lbmd;

    move-result-object v0

    .line 284
    invoke-virtual {v0}, Lbmd;->b()J

    move-result-wide v2

    const-wide/32 v4, 0x16e360

    cmp-long v1, v2, v4

    if-gez v1, :cond_2

    .line 285
    sget-object v1, Lanj;->b:Ljava/lang/String;

    const-string v1, "Appended video (%s) is too short: %s us"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v6

    const/4 v3, 0x1

    .line 286
    invoke-virtual {v0}, Lbmd;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v2, v3

    .line 285
    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 287
    iget-object v0, p0, Lanj;->s:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lano;

    invoke-interface {v0}, Lano;->a()V

    .line 291
    :goto_1
    return-void

    .line 283
    :cond_0
    iget-object v0, p2, Lbmu;->e:Lbmv;

    sget-object v2, Lbmv;->b:Lbmv;

    if-ne v0, v2, :cond_1

    new-instance v0, Lbmp;

    const-wide/32 v2, 0x1e8480

    invoke-direct {v0, v8, v9, v2, v3}, Lbmp;-><init>(JJ)V

    invoke-virtual {v1, v0}, Lbmf;->a(Lbmp;)Lbmf;

    sget-object v0, Lbmg;->c:Lbmg;

    goto :goto_0

    :cond_1
    iget-object v0, p2, Lbmu;->e:Lbmv;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x17

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid metadata type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 289
    :cond_2
    iget-object v1, p0, Lanj;->h:Lawb;

    new-instance v2, Lboz;

    invoke-direct {v2, v0}, Lboz;-><init>(Lbmd;)V

    invoke-interface {v1, v2}, Lawb;->a(Lbph;)V

    goto :goto_1
.end method

.method static synthetic a(Lanj;Landroid/net/Uri;Lbmu;Lbkr;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Lanj;->a(Landroid/net/Uri;Lbmu;Lbkr;)V

    return-void
.end method

.method static synthetic a(Lanj;Lanl;)V
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lanj;->e()V

    iget-object v0, p0, Lanj;->d:Lant;

    invoke-interface {p1, v0}, Lanl;->a(Lant;)V

    return-void
.end method

.method private a(Landroid/net/Uri;)Z
    .locals 2

    .prologue
    .line 327
    iget-object v0, p0, Lanj;->h:Lawb;

    invoke-interface {v0, p1}, Lawb;->a(Landroid/net/Uri;)Z

    move-result v0

    .line 328
    if-eqz v0, :cond_0

    .line 329
    iget-object v0, p0, Lanj;->h:Lawb;

    invoke-interface {v0, p1}, Lawb;->c(Landroid/net/Uri;)Lbkr;

    move-result-object v0

    .line 330
    if-eqz v0, :cond_0

    .line 331
    iget-object v1, p0, Lanj;->h:Lawb;

    invoke-interface {v1, p1}, Lawb;->b(Landroid/net/Uri;)Lbmu;

    move-result-object v1

    invoke-direct {p0, p1, v1, v0}, Lanj;->a(Landroid/net/Uri;Lbmu;Lbkr;)V

    .line 332
    const/4 v0, 0x1

    .line 335
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lanj;Landroid/net/Uri;)Z
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lanj;->a(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lanj;)Lavz;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lanj;->p:Lavz;

    return-object v0
.end method

.method static synthetic b(Lanj;Lanl;)V
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lanj;->e()V

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lanl;->a(Lant;)V

    return-void
.end method

.method static synthetic c(Lanj;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lanj;->g()V

    return-void
.end method

.method static synthetic d(Lanj;)Lawb;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lanj;->h:Lawb;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lanj;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lanj;)V
    .locals 5

    .prologue
    .line 36
    invoke-virtual {p0}, Lanj;->e()V

    invoke-direct {p0}, Lanj;->h()V

    iget-object v0, p0, Lanj;->t:Landroid/os/AsyncTask;

    const-string v1, "mCurrentTask"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lanj;->k:Lalo;

    iget-object v1, p0, Lanj;->e:Lanq;

    invoke-interface {v0, v1}, Lalo;->a(Lalp;)Lalm;

    move-result-object v0

    iput-object v0, p0, Lanj;->t:Landroid/os/AsyncTask;

    iget-object v1, p0, Lanj;->j:Lalg;

    const/4 v2, 0x1

    new-array v2, v2, [Lavz;

    const/4 v3, 0x0

    iget-object v4, p0, Lanj;->p:Lavz;

    aput-object v4, v2, v3

    invoke-interface {v1, v0, v2}, Lalg;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic f(Lanj;)Lann;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lanj;->n:Lann;

    return-object v0
.end method

.method private f()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 272
    iget-object v0, p0, Lanj;->t:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 273
    iget-object v0, p0, Lanj;->t:Landroid/os/AsyncTask;

    invoke-virtual {v0, v2}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 274
    iput-object v1, p0, Lanj;->t:Landroid/os/AsyncTask;

    .line 276
    :cond_0
    iget-object v0, p0, Lanj;->g:Lahx;

    invoke-interface {v0}, Lahx;->a()V

    .line 277
    iput-object v1, p0, Lanj;->p:Lavz;

    .line 278
    iput-object v1, p0, Lanj;->q:Lbmu;

    .line 279
    iput-boolean v2, p0, Lanj;->u:Z

    .line 280
    return-void
.end method

.method static synthetic g(Lanj;)Lasn;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lanj;->s:Lasn;

    return-object v0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 339
    iget-object v0, p0, Lanj;->r:Lasn;

    invoke-virtual {v0}, Lasn;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lanj;->r:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lanl;

    invoke-interface {v0}, Lanl;->a()V

    .line 342
    :cond_0
    return-void
.end method

.method static synthetic h(Lanj;)Lasn;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lanj;->r:Lasn;

    return-object v0
.end method

.method private h()V
    .locals 3

    .prologue
    .line 520
    iget-object v0, p0, Lanj;->h:Lawb;

    invoke-interface {v0}, Lawb;->bd()Lawg;

    move-result-object v0

    const-string v1, "running mode"

    sget-object v2, Lawg;->b:Lawg;

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    .line 521
    return-void
.end method

.method static synthetic i(Lanj;)Lbmu;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lanj;->q:Lbmu;

    return-object v0
.end method

.method static synthetic j(Lanj;)Lahy;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lanj;->c:Lahy;

    return-object v0
.end method

.method static synthetic k(Lanj;)Lahx;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lanj;->g:Lahx;

    return-object v0
.end method

.method static synthetic l(Lanj;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lanj;->f()V

    return-void
.end method

.method static synthetic m(Lanj;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lanj;->h()V

    return-void
.end method

.method static synthetic n(Lanj;)Z
    .locals 1

    .prologue
    .line 36
    iget-boolean v0, p0, Lanj;->u:Z

    return v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 155
    invoke-super {p0}, Lamn;->a()V

    .line 159
    iget-object v0, p0, Lanj;->r:Lasn;

    invoke-virtual {v0}, Lasn;->a()V

    .line 160
    iget-object v0, p0, Lanj;->s:Lasn;

    invoke-virtual {v0}, Lasn;->a()V

    .line 161
    return-void
.end method

.method public a(Lanl;)V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lanj;->r:Lasn;

    invoke-virtual {v0, p1}, Lasn;->c(Ljava/lang/Object;)V

    .line 204
    return-void
.end method

.method public a(Lano;)V
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lanj;->s:Lasn;

    invoke-virtual {v0, p1}, Lasn;->c(Ljava/lang/Object;)V

    .line 212
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 168
    invoke-direct {p0}, Lanj;->f()V

    .line 169
    iget-object v0, p0, Lanj;->s:Lasn;

    invoke-virtual {v0}, Lasn;->b()V

    .line 170
    invoke-direct {p0}, Lanj;->g()V

    .line 171
    iget-object v0, p0, Lanj;->r:Lasn;

    invoke-virtual {v0}, Lasn;->b()V

    .line 172
    invoke-super {p0}, Lamn;->b()V

    .line 173
    return-void
.end method

.method public b(Lanl;)V
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lanj;->r:Lasn;

    invoke-virtual {v0, p1}, Lasn;->d(Ljava/lang/Object;)V

    .line 208
    return-void
.end method

.method public b(Lano;)V
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lanj;->s:Lasn;

    invoke-virtual {v0, p1}, Lasn;->d(Ljava/lang/Object;)V

    .line 216
    return-void
.end method

.method public c()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 179
    invoke-virtual {p0}, Lanj;->e()V

    .line 181
    iget-object v0, p0, Lanj;->h:Lawb;

    invoke-interface {v0}, Lawb;->bd()Lawg;

    move-result-object v0

    sget-object v1, Lawg;->b:Lawg;

    if-eq v0, v1, :cond_1

    .line 200
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    iget-object v0, p0, Lanj;->h:Lawb;

    invoke-interface {v0}, Lawb;->aL()Landroid/net/Uri;

    move-result-object v0

    .line 188
    if-eqz v0, :cond_0

    .line 191
    iget-object v1, p0, Lanj;->h:Lawb;

    invoke-interface {v1, v0}, Lawb;->a(Landroid/net/Uri;)Z

    .line 195
    invoke-direct {p0, v0}, Lanj;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 196
    iget-object v0, p0, Lanj;->h:Lawb;

    invoke-interface {v0}, Lawb;->aM()V

    goto :goto_0

    .line 199
    :cond_2
    iget-object v1, p0, Lanj;->t:Landroid/os/AsyncTask;

    const-string v2, "mActiveTask"

    invoke-static {v1, v2, v3}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lanj;->o:Land;

    iget-object v2, p0, Lanj;->f:Lanr;

    invoke-interface {v1, v2}, Land;->a(Lane;)Lana;

    move-result-object v1

    iput-object v1, p0, Lanj;->t:Landroid/os/AsyncTask;

    invoke-static {v0}, Lavz;->b(Landroid/net/Uri;)Lavz;

    move-result-object v0

    iput-object v0, p0, Lanj;->p:Lavz;

    iput-object v3, p0, Lanj;->q:Lbmu;

    iget-object v0, p0, Lanj;->j:Lalg;

    const/4 v2, 0x1

    new-array v2, v2, [Lavz;

    const/4 v3, 0x0

    iget-object v4, p0, Lanj;->p:Lavz;

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lalg;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    goto :goto_0
.end method
