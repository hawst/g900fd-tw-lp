.class public final Lhbr;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 15
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "com.google.android.libraries.photoeditor.filterparameters.AutoEnhanceFilterParameter"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "com.google.android.libraries.photoeditor.filterparameters.CropFilterParameter"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "com.google.android.libraries.photoeditor.filterparameters.FilmFilterParameter"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "com.google.android.libraries.photoeditor.filterparameters.FilterParameter"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "com.google.android.libraries.photoeditor.filterparameters.RetroluxFilterParameter"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "com.google.android.libraries.photoeditor.filterparameters.StraightenFilterParameter"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "com.google.android.libraries.photoeditor.filterparameters.UPointFilterParameter"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "com.google.android.libraries.photoeditor.filterparameters.UPointParameterBase"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "com.google.android.libraries.photoeditor.filterparameters.UPointParameter"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "com.google.android.libraries.photoeditor.filterparameters.Vintage2FilterParameter"

    aput-object v2, v0, v1

    sput-object v0, Lhbr;->a:[Ljava/lang/String;

    return-void
.end method

.method public static a()Ljava/util/List;
    .locals 15
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lhal;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v8, 0x9

    const/4 v14, 0x3

    const/4 v2, 0x7

    const/4 v13, 0x1

    const/4 v5, 0x0

    .line 29
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 31
    new-instance v0, Lhal;

    const-string v1, "BlackAndWhite"

    new-array v3, v2, [I

    fill-array-data v3, :array_0

    const/4 v4, 0x2

    new-array v4, v4, [I

    fill-array-data v4, :array_1

    invoke-direct/range {v0 .. v5}, Lhal;-><init>(Ljava/lang/String;I[I[IZ)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 49
    new-instance v0, Lhal;

    const-string v1, "CenterFocus"

    const/16 v3, 0xb

    new-array v4, v8, [I

    fill-array-data v4, :array_2

    invoke-direct {v0, v1, v3, v4, v5}, Lhal;-><init>(Ljava/lang/String;I[IZ)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 65
    new-instance v0, Lhal;

    const-string v1, "CropAndRotate"

    const/16 v3, 0x14

    new-array v4, v2, [I

    fill-array-data v4, :array_3

    invoke-direct {v0, v1, v3, v4, v5}, Lhal;-><init>(Ljava/lang/String;I[IZ)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 79
    new-instance v0, Lhal;

    const-string v1, "Details"

    const/16 v3, 0xd

    const/4 v4, 0x2

    new-array v4, v4, [I

    fill-array-data v4, :array_4

    invoke-direct {v0, v1, v3, v4, v5}, Lhal;-><init>(Ljava/lang/String;I[IZ)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 88
    new-instance v6, Lhal;

    const-string v7, "Drama"

    new-array v9, v14, [I

    fill-array-data v9, :array_5

    new-array v10, v13, [I

    aput v14, v10, v5

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lhal;-><init>(Ljava/lang/String;I[I[IZ)V

    invoke-interface {v12, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 101
    new-instance v0, Lhal;

    const-string v1, "Empty"

    new-array v3, v5, [I

    invoke-direct {v0, v1, v13, v3, v13}, Lhal;-><init>(Ljava/lang/String;I[IZ)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    new-instance v0, Lhal;

    const-string v1, "FixedFrames"

    const/16 v3, 0x11

    new-array v2, v2, [I

    fill-array-data v2, :array_6

    invoke-direct {v0, v1, v3, v2, v13}, Lhal;-><init>(Ljava/lang/String;I[IZ)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 117
    new-instance v6, Lhal;

    const-string v7, "GradEdit"

    const/16 v0, 0x64

    new-array v9, v8, [I

    fill-array-data v9, :array_7

    new-array v10, v13, [I

    aput v14, v10, v5

    move v8, v0

    move v11, v13

    invoke-direct/range {v6 .. v11}, Lhal;-><init>(Ljava/lang/String;I[I[IZ)V

    invoke-interface {v12, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 136
    new-instance v0, Lhal;

    const-string v1, "TiltAndShift"

    const/16 v2, 0xe

    const/16 v3, 0xc

    new-array v3, v3, [I

    fill-array-data v3, :array_8

    invoke-direct {v0, v1, v2, v3, v5}, Lhal;-><init>(Ljava/lang/String;I[IZ)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    new-instance v0, Lhal;

    const-string v1, "TuneImage"

    const/4 v2, 0x4

    const/4 v3, 0x6

    new-array v3, v3, [I

    fill-array-data v3, :array_9

    invoke-direct {v0, v1, v2, v3, v5}, Lhal;-><init>(Ljava/lang/String;I[IZ)V

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 168
    invoke-static {v12}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 31
    nop

    :array_0
    .array-data 4
        0x0
        0x1
        0xe
        0xc
        0xf1
        0x3
        0xc9
    .end array-data

    :array_1
    .array-data 4
        0x3
        0xf1
    .end array-data

    .line 49
    :array_2
    .array-data 4
        0x13
        0x16
        0x17
        0xc
        0x5
        0x18
        0x19
        0x4
        0x3
    .end array-data

    .line 65
    :array_3
    .array-data 4
        0x267
        0x262
        0x263
        0x264
        0x265
        0x266
        0x268
    .end array-data

    .line 79
    :array_4
    .array-data 4
        0x10
        0xf
    .end array-data

    .line 88
    :array_5
    .array-data 4
        0xc
        0x2
        0x3
    .end array-data

    .line 103
    :array_6
    .array-data 4
        0xdd
        0xe0
        0xdf
        0x9
        0x69
        0x67
        0x71
    .end array-data

    .line 117
    :array_7
    .array-data 4
        0xc
        0x0
        0x2
        0x28a
        0x28e
        0x28b
        0x28c
        0x28d
        0x3
    .end array-data

    .line 136
    :array_8
    .array-data 4
        0x11
        0x13
        0x5
        0x18
        0x19
        0x2
        0x1
        0x0
        0x3
        0x12
        0xc9
        0xca
    .end array-data

    .line 155
    :array_9
    .array-data 4
        0x2
        0x0
        0x14
        0xa
        0x1
        0xb
    .end array-data
.end method
