.class Ljzd;
.super Lhny;
.source "PG"


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Ljzd;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Ljzd;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 35
    sget-object v0, Ljzd;->a:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 36
    iput-object p1, p0, Ljzd;->b:Landroid/content/Context;

    .line 37
    iput p2, p0, Ljzd;->c:I

    .line 38
    iput-object p3, p0, Ljzd;->d:Ljava/lang/String;

    .line 39
    iput-object p4, p0, Ljzd;->e:Ljava/lang/String;

    .line 40
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 14

    .prologue
    .line 44
    new-instance v1, Ljzc;

    iget-object v0, p0, Ljzd;->b:Landroid/content/Context;

    iget v2, p0, Ljzd;->c:I

    iget-object v3, p0, Ljzd;->d:Ljava/lang/String;

    iget-object v4, p0, Ljzd;->e:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3, v4}, Ljzc;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 46
    iget-object v0, p0, Ljzd;->b:Landroid/content/Context;

    const-class v2, Lkfd;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    .line 47
    invoke-interface {v0, v1}, Lkfd;->a(Lkff;)V

    .line 48
    new-instance v2, Lhoz;

    iget v0, v1, Lkff;->i:I

    iget-object v3, v1, Lkff;->k:Ljava/lang/Exception;

    const/4 v4, 0x0

    invoke-direct {v2, v0, v3, v4}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    invoke-virtual {v1}, Ljzc;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmhd;

    if-nez v0, :cond_0

    move-object v0, v2

    :goto_0
    return-object v0

    :cond_0
    iget-object v4, v0, Lmhd;->a:Lngy;

    invoke-virtual {v1}, Ljzc;->t()Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz v4, :cond_1

    iget-object v0, v4, Lngy;->a:Lnge;

    if-eqz v0, :cond_1

    iget-object v0, v4, Lngy;->a:Lnge;

    iget-object v0, v0, Lnge;->a:[Lngg;

    if-eqz v0, :cond_1

    iget-object v0, v4, Lngy;->a:Lnge;

    iget-object v0, v0, Lnge;->a:[Lngg;

    array-length v0, v0

    const/4 v1, 0x2

    if-lt v0, v1, :cond_1

    iget-object v0, v4, Lngy;->b:[Lnha;

    if-eqz v0, :cond_1

    iget-object v0, v4, Lngy;->b:[Lnha;

    array-length v0, v0

    if-nez v0, :cond_2

    :cond_1
    move-object v0, v2

    goto :goto_0

    :cond_2
    iget-object v0, v4, Lngy;->a:Lnge;

    iget-object v0, v0, Lnge;->b:Ljava/lang/Long;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    new-instance v6, Ljava/util/LinkedHashMap;

    iget-object v0, v4, Lngy;->a:Lnge;

    iget-object v0, v0, Lnge;->a:[Lngg;

    array-length v0, v0

    invoke-direct {v6, v0}, Ljava/util/LinkedHashMap;-><init>(I)V

    const/4 v0, 0x0

    iget-object v1, v4, Lngy;->a:Lnge;

    iget-object v1, v1, Lnge;->a:[Lngg;

    array-length v7, v1

    move v3, v0

    :goto_1
    if-ge v3, v7, :cond_5

    iget-object v0, v4, Lngy;->a:Lnge;

    iget-object v0, v0, Lnge;->a:[Lngg;

    aget-object v8, v0, v3

    iget-object v0, v8, Lngg;->d:Lngi;

    if-eqz v0, :cond_3

    iget-object v0, v8, Lngg;->d:Lngi;

    iget-object v0, v0, Lngi;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    :goto_2
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v10

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v12, 0x0

    cmp-long v0, v0, v12

    if-nez v0, :cond_4

    const/4 v0, 0x0

    :goto_3
    new-instance v1, Ljzo;

    iget-object v9, v8, Lngg;->c:Lngh;

    iget-object v9, v9, Lngh;->a:Ljava/lang/String;

    invoke-direct {v1, v9, v0, v10, v11}, Ljzo;-><init>(Ljava/lang/String;FJ)V

    iget-object v0, v8, Lngg;->b:Ljava/lang/String;

    invoke-interface {v6, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_3
    const-wide/16 v0, 0x0

    goto :goto_2

    :cond_4
    long-to-float v0, v10

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    long-to-float v1, v12

    div-float/2addr v0, v1

    goto :goto_3

    :cond_5
    const/4 v1, 0x0

    const/4 v0, 0x0

    iget-object v3, v4, Lngy;->b:[Lnha;

    array-length v5, v3

    move v3, v0

    move v0, v1

    :goto_4
    if-ge v3, v5, :cond_8

    iget-object v1, v4, Lngy;->b:[Lnha;

    aget-object v7, v1, v3

    iget-object v1, v7, Lnha;->c:[Lofv;

    if-eqz v1, :cond_7

    iget-object v1, v7, Lnha;->c:[Lofv;

    array-length v1, v1

    if-eqz v1, :cond_7

    iget-object v1, v7, Lnha;->c:[Lofv;

    array-length v8, v1

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    :goto_5
    if-ge v1, v8, :cond_6

    iget-object v10, v7, Lnha;->c:[Lofv;

    aget-object v10, v10, v1

    new-instance v11, Lkav;

    iget-object v12, v10, Lofv;->c:Ljava/lang/String;

    iget-object v13, v10, Lofv;->b:Ljava/lang/String;

    iget-object v10, v10, Lofv;->d:Ljava/lang/String;

    invoke-direct {v11, v12, v13, v10}, Lkav;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    :cond_6
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/2addr v1, v0

    iget-object v0, v7, Lnha;->b:Ljava/lang/String;

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljzo;

    invoke-virtual {v0, v9}, Ljzo;->a(Ljava/util/ArrayList;)V

    move v0, v1

    :cond_7
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_4

    :cond_8
    invoke-virtual {v2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "poll_option_voter_models"

    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {v6}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v3, "poll_option_voters_count"

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object v0, v2

    goto/16 :goto_0
.end method
