.class public Lhdx;
.super Lhdu;
.source "PG"


# instance fields
.field private a:Z

.field public final f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V
    .locals 2

    .prologue
    .line 228
    invoke-direct {p0}, Lhdu;-><init>()V

    .line 226
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhdx;->a:Z

    .line 229
    if-nez p1, :cond_0

    .line 230
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Target overlay view cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 233
    :cond_0
    iput-object p1, p0, Lhdx;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    .line 234
    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 249
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 237
    iget-boolean v0, p0, Lhdx;->a:Z

    if-ne v0, p1, :cond_0

    .line 243
    :goto_0
    return-void

    .line 241
    :cond_0
    iput-boolean p1, p0, Lhdx;->a:Z

    .line 242
    iget-object v0, p0, Lhdx;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    goto :goto_0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 246
    iget-boolean v0, p0, Lhdx;->a:Z

    return v0
.end method
