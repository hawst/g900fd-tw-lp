.class public Lhcz;
.super Lhdx;
.source "PG"


# instance fields
.field final a:Lhck;

.field b:Lhcm;

.field private c:Lhdc;

.field private d:Lhdb;

.field private e:I

.field private g:F

.field private h:F

.field private i:F

.field private final j:F

.field private k:I

.field private l:F

.field private m:F

.field private n:F

.field private o:Ljava/lang/Number;

.field private final p:Landroid/graphics/Point;

.field private final q:Landroid/graphics/PointF;

.field private final r:Landroid/graphics/PointF;

.field private final s:Landroid/graphics/Rect;

.field private t:F

.field private u:F

.field private v:Z


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lhdx;-><init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V

    .line 39
    sget-object v0, Lhcm;->a:Lhcm;

    iput-object v0, p0, Lhcz;->b:Lhcm;

    .line 48
    const/4 v0, 0x1

    iput v0, p0, Lhcz;->k:I

    .line 55
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lhcz;->p:Landroid/graphics/Point;

    .line 122
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lhcz;->q:Landroid/graphics/PointF;

    .line 123
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lhcz;->r:Landroid/graphics/PointF;

    .line 191
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lhcz;->s:Landroid/graphics/Rect;

    .line 60
    iget-object v0, p0, Lhdx;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 61
    invoke-virtual {p0, v0}, Lhcz;->a(Landroid/content/Context;)Lhck;

    move-result-object v1

    iput-object v1, p0, Lhcz;->a:Lhck;

    .line 63
    invoke-static {v0}, Lhea;->a(Landroid/content/Context;)Lhea;

    move-result-object v0

    invoke-virtual {v0}, Lhea;->a()F

    move-result v0

    .line 64
    mul-float/2addr v0, v0

    iput v0, p0, Lhcz;->j:F

    .line 65
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;)Lhck;
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lhcj;

    invoke-direct {v0, p1}, Lhcj;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method protected a(F)V
    .locals 3

    .prologue
    .line 347
    iget-object v0, p0, Lhcz;->d:Lhdb;

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lhcz;->d:Lhdb;

    iget v1, p0, Lhcz;->e:I

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lhdb;->c(ILjava/lang/Object;)V

    .line 350
    :cond_0
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 112
    invoke-super {p0, p1}, Lhdx;->a(Landroid/graphics/Canvas;)V

    .line 114
    invoke-virtual {p0}, Lhcz;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lhcz;->e:I

    if-ltz v0, :cond_0

    .line 115
    iget-object v0, p0, Lhcz;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lhcz;->p:Landroid/graphics/Point;

    invoke-virtual {p0, v0, v1}, Lhcz;->a(Landroid/graphics/Rect;Landroid/graphics/Point;)V

    .line 117
    iget-object v0, p0, Lhcz;->a:Lhck;

    iget-object v1, p0, Lhcz;->b:Lhcm;

    iget-object v2, p0, Lhcz;->p:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iget-object v3, p0, Lhcz;->p:Landroid/graphics/Point;

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, p1, v1, v2, v3}, Lhck;->a(Landroid/graphics/Canvas;Lhcm;II)V

    .line 120
    :cond_0
    return-void
.end method

.method public a(Landroid/graphics/Rect;)V
    .locals 3

    .prologue
    .line 102
    iget-object v0, p0, Lhcz;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a()Landroid/graphics/Rect;

    move-result-object v0

    .line 103
    if-nez v0, :cond_0

    .line 104
    invoke-virtual {p1}, Landroid/graphics/Rect;->setEmpty()V

    .line 108
    :goto_0
    return-void

    .line 106
    :cond_0
    iget-object v1, p0, Lhcz;->b:Lhcm;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerY()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v2, v0, p1}, Lhcm;->a(FFLandroid/graphics/Rect;)V

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Rect;Landroid/graphics/Point;)V
    .locals 2

    .prologue
    .line 93
    invoke-virtual {p1}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    invoke-virtual {p2, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 94
    return-void
.end method

.method public a(Lhdb;)V
    .locals 0

    .prologue
    .line 77
    iput-object p1, p0, Lhcz;->d:Lhdb;

    .line 78
    return-void
.end method

.method public a(Lhdc;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lhcz;->c:Lhdc;

    .line 74
    return-void
.end method

.method protected b(I)V
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lhcz;->d:Lhdb;

    if-eqz v0, :cond_0

    .line 335
    iget-object v0, p0, Lhcz;->d:Lhdb;

    invoke-interface {v0, p1}, Lhdb;->c(I)V

    .line 337
    :cond_0
    return-void
.end method

.method protected c(I)V
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lhcz;->d:Lhdb;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    .line 341
    iget-object v0, p0, Lhcz;->d:Lhdb;

    invoke-interface {v0, p1}, Lhdb;->d(I)V

    .line 343
    :cond_0
    return-void
.end method

.method public c(FF)Z
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lhcz;->q:Landroid/graphics/PointF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 132
    iget-object v0, p0, Lhcz;->r:Landroid/graphics/PointF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 134
    const/4 v0, 0x0

    return v0
.end method

.method public d(FF)Z
    .locals 10

    .prologue
    const/4 v1, 0x3

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 139
    iget-object v0, p0, Lhcz;->c:Lhdc;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhcz;->c:Lhdc;

    invoke-interface {v0}, Lhdc;->c()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 175
    :goto_0
    return v0

    .line 143
    :cond_1
    iget-object v0, p0, Lhcz;->r:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    sub-float v0, p1, v0

    .line 144
    iget-object v4, p0, Lhcz;->r:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->y:F

    sub-float v5, p2, v4

    .line 146
    iget v4, p0, Lhcz;->k:I

    if-ne v4, v3, :cond_2

    iget-object v4, p0, Lhcz;->r:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    iget-object v6, p0, Lhcz;->r:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    .line 147
    invoke-static {p1, p2, v4, v6}, Lhdo;->b(FFFF)F

    move-result v4

    iget v6, p0, Lhcz;->j:F

    cmpg-float v4, v4, v6

    if-gez v4, :cond_2

    move v0, v2

    .line 149
    goto :goto_0

    .line 152
    :cond_2
    iget v4, p0, Lhcz;->k:I

    if-ne v4, v3, :cond_5

    move v4, v3

    .line 153
    :goto_1
    if-eqz v4, :cond_3

    .line 154
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpl-float v0, v0, v5

    if-lez v0, :cond_6

    const/4 v0, 0x2

    :goto_2
    iput v0, p0, Lhcz;->k:I

    .line 158
    :cond_3
    iget v0, p0, Lhcz;->k:I

    if-ne v0, v1, :cond_a

    .line 159
    if-eqz v4, :cond_7

    .line 160
    iget-object v0, p0, Lhcz;->c:Lhdc;

    invoke-interface {v0}, Lhdc;->d()I

    move-result v0

    iput v0, p0, Lhcz;->e:I

    iget v0, p0, Lhcz;->e:I

    if-ltz v0, :cond_4

    iget-object v0, p0, Lhcz;->a:Lhck;

    iget-object v1, p0, Lhcz;->c:Lhdc;

    invoke-virtual {v0, v1}, Lhck;->a(Lhdc;)Lhcm;

    move-result-object v0

    iput-object v0, p0, Lhcz;->b:Lhcm;

    iget-object v0, p0, Lhcz;->b:Lhcm;

    iget v1, p0, Lhcz;->e:I

    invoke-virtual {v0, v1}, Lhcm;->b(I)F

    move-result v0

    iput v0, p0, Lhcz;->g:F

    iget-object v0, p0, Lhcz;->b:Lhcm;

    invoke-virtual {v0, v2}, Lhcm;->b(I)F

    move-result v0

    iput v0, p0, Lhcz;->h:F

    iget-object v0, p0, Lhcz;->b:Lhcm;

    iget-object v1, p0, Lhcz;->b:Lhcm;

    invoke-virtual {v1}, Lhcm;->b()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lhcm;->b(I)F

    move-result v0

    iput v0, p0, Lhcz;->i:F

    iget-object v0, p0, Lhcz;->b:Lhcm;

    iget v1, p0, Lhcz;->g:F

    invoke-virtual {v0, v1}, Lhcm;->a(F)I

    iget-object v0, p0, Lhcz;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lhcz;->p:Landroid/graphics/Point;

    invoke-virtual {p0, v0, v1}, Lhcz;->a(Landroid/graphics/Rect;Landroid/graphics/Point;)V

    iget-object v0, p0, Lhcz;->b:Lhcm;

    iget-object v1, p0, Lhcz;->p:Landroid/graphics/Point;

    iget v1, v1, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    iget-object v2, p0, Lhcz;->p:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    iget-object v4, p0, Lhcz;->s:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v2, v4}, Lhcm;->a(FFLandroid/graphics/Rect;)V

    iget-object v0, p0, Lhcz;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    .line 172
    :cond_4
    :goto_3
    iget-object v0, p0, Lhcz;->r:Landroid/graphics/PointF;

    iput p1, v0, Landroid/graphics/PointF;->x:F

    .line 173
    iget-object v0, p0, Lhcz;->r:Landroid/graphics/PointF;

    iput p2, v0, Landroid/graphics/PointF;->y:F

    move v0, v3

    .line 175
    goto/16 :goto_0

    :cond_5
    move v4, v2

    .line 152
    goto/16 :goto_1

    :cond_6
    move v0, v1

    .line 154
    goto :goto_2

    .line 162
    :cond_7
    iget v0, p0, Lhcz;->g:F

    iget-object v1, p0, Lhcz;->r:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v0, v1

    sub-float/2addr v0, p2

    iget v1, p0, Lhcz;->h:F

    iget v2, p0, Lhcz;->i:F

    invoke-static {v0, v1, v2}, Lhcg;->a(FFF)F

    move-result v0

    iget v1, p0, Lhcz;->g:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lhcz;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v1}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a()Landroid/graphics/Rect;

    move-result-object v1

    iget-object v2, p0, Lhcz;->p:Landroid/graphics/Point;

    invoke-virtual {p0, v1, v2}, Lhcz;->a(Landroid/graphics/Rect;Landroid/graphics/Point;)V

    iget-object v1, p0, Lhcz;->b:Lhcm;

    iget-object v2, p0, Lhcz;->p:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget-object v4, p0, Lhcz;->p:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->y:I

    int-to-float v4, v4

    iget-object v5, p0, Lhcz;->s:Landroid/graphics/Rect;

    invoke-virtual {v1, v2, v4, v5}, Lhcm;->a(FFLandroid/graphics/Rect;)V

    iget-object v1, p0, Lhcz;->s:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->right:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Landroid/graphics/Rect;->right:I

    iget v1, p0, Lhcz;->g:F

    cmpl-float v1, v1, v0

    if-lez v1, :cond_9

    iget-object v1, p0, Lhcz;->s:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->top:I

    add-int/lit8 v2, v2, -0x2

    iput v2, v1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lhcz;->s:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    int-to-double v4, v2

    iget v2, p0, Lhcz;->g:F

    sub-float/2addr v2, v0

    float-to-double v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    add-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-int v2, v4

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    :goto_4
    iput v0, p0, Lhcz;->g:F

    iget-object v0, p0, Lhcz;->b:Lhcm;

    iget v1, p0, Lhcz;->g:F

    invoke-virtual {v0, v1}, Lhcm;->a(F)I

    move-result v0

    if-ltz v0, :cond_8

    iget v1, p0, Lhcz;->e:I

    if-eq v0, v1, :cond_8

    iput v0, p0, Lhcz;->e:I

    iget-object v0, p0, Lhcz;->d:Lhdb;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lhcz;->d:Lhdb;

    iget v1, p0, Lhcz;->e:I

    invoke-interface {v0, v1}, Lhdb;->e(I)V

    :cond_8
    iget-object v0, p0, Lhcz;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    goto/16 :goto_3

    :cond_9
    iget-object v1, p0, Lhcz;->s:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->top:I

    int-to-double v4, v2

    iget v2, p0, Lhcz;->g:F

    sub-float/2addr v2, v0

    float-to-double v6, v2

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    sub-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-int v2, v4

    iput v2, v1, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lhcz;->s:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v2, v2, 0x2

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_4

    .line 165
    :cond_a
    if-eqz v4, :cond_b

    .line 166
    iget-object v0, p0, Lhcz;->c:Lhdc;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lhcz;->c:Lhdc;

    invoke-interface {v0}, Lhdc;->d()I

    move-result v0

    iput v0, p0, Lhcz;->e:I

    iget v0, p0, Lhcz;->e:I

    if-ltz v0, :cond_4

    iget-object v0, p0, Lhdx;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhea;->b(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lhcz;->v:Z

    iget-object v0, p0, Lhcz;->c:Lhdc;

    iget v1, p0, Lhcz;->e:I

    invoke-interface {v0, v1}, Lhdc;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    iget-object v1, p0, Lhcz;->c:Lhdc;

    iget v2, p0, Lhcz;->e:I

    invoke-interface {v1, v2}, Lhdc;->d(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    iput v0, p0, Lhcz;->m:F

    invoke-virtual {v1}, Ljava/lang/Number;->floatValue()F

    move-result v0

    iput v0, p0, Lhcz;->n:F

    iget v0, p0, Lhcz;->n:F

    iget v1, p0, Lhcz;->m:F

    sub-float/2addr v0, v1

    iget-object v1, p0, Lhcz;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v1}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    const/high16 v2, 0x3f400000    # 0.75f

    mul-float/2addr v1, v2

    div-float/2addr v0, v1

    iput v0, p0, Lhcz;->l:F

    iget-object v0, p0, Lhcz;->c:Lhdc;

    iget v1, p0, Lhcz;->e:I

    invoke-interface {v0, v1}, Lhdc;->e(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Number;

    iput-object v0, p0, Lhcz;->o:Ljava/lang/Number;

    iget-object v0, p0, Lhcz;->o:Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    iput v0, p0, Lhcz;->t:F

    iget-object v0, p0, Lhcz;->q:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iput v0, p0, Lhcz;->u:F

    iget v0, p0, Lhcz;->e:I

    iget v1, p0, Lhcz;->t:F

    iget v1, p0, Lhcz;->m:F

    iget v1, p0, Lhcz;->n:F

    invoke-virtual {p0, v0}, Lhcz;->b(I)V

    goto/16 :goto_3

    .line 168
    :cond_b
    iget v0, p0, Lhcz;->e:I

    if-ltz v0, :cond_4

    iget v0, p0, Lhcz;->u:F

    sub-float v0, p1, v0

    iget v1, p0, Lhcz;->l:F

    mul-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x3dcccccd    # 0.1f

    cmpg-float v1, v1, v2

    if-ltz v1, :cond_4

    iget-boolean v1, p0, Lhcz;->v:Z

    if-eqz v1, :cond_c

    neg-float v0, v0

    :cond_c
    iput p1, p0, Lhcz;->u:F

    iget v1, p0, Lhcz;->t:F

    add-float/2addr v0, v1

    iget v1, p0, Lhcz;->m:F

    iget v2, p0, Lhcz;->n:F

    invoke-static {v0, v1, v2}, Lhcg;->a(FFF)F

    move-result v0

    iget v1, p0, Lhcz;->t:F

    cmpl-float v1, v0, v1

    if-eqz v1, :cond_4

    iput v0, p0, Lhcz;->t:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, p0, Lhcz;->o:Ljava/lang/Number;

    iget v1, p0, Lhcz;->e:I

    iget v1, p0, Lhcz;->m:F

    iget v1, p0, Lhcz;->n:F

    invoke-virtual {p0, v0}, Lhcz;->a(F)V

    goto/16 :goto_3
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 180
    iget v0, p0, Lhcz;->k:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 181
    iget v0, p0, Lhcz;->e:I

    invoke-virtual {p0, v0}, Lhcz;->c(I)V

    .line 184
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lhcz;->k:I

    .line 186
    iget-object v0, p0, Lhcz;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    .line 188
    const/4 v0, 0x0

    return v0
.end method

.method protected final k()Z
    .locals 2

    .prologue
    .line 126
    iget v0, p0, Lhcz;->k:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
