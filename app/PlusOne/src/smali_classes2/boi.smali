.class public Lboi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbmc;
.implements Lbop;


# instance fields
.field private final a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbma;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmh;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcac;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Layj;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Lbmp;

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:J

.field private final n:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lboi;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Ljava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Layj;Ljava/lang/String;Ljava/lang/String;Lbmp;IIII)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbma;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmh;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcac;",
            ">;",
            "Layj;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lbmp;",
            "IIII)V"
        }
    .end annotation

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    const-string v2, "inputDisplayables"

    const/4 v3, 0x0

    invoke-static {p1, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map;

    iput-object v2, p0, Lboi;->a:Ljava/util/Map;

    .line 83
    const-string v2, "videoClips"

    invoke-static {p2, v2}, Lcec;->a(Ljava/util/Collection;Ljava/lang/CharSequence;)Ljava/util/Collection;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    iput-object v2, p0, Lboi;->b:Ljava/util/List;

    .line 84
    const-string v2, "audioClipSequences"

    invoke-static {p3, v2}, Lcec;->a(Ljava/util/Collection;Ljava/lang/CharSequence;)Ljava/util/Collection;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    iput-object v2, p0, Lboi;->c:Ljava/util/List;

    .line 85
    const-string v2, "playbackMetrics"

    invoke-static {p4, v2}, Lcec;->a(Ljava/util/Collection;Ljava/lang/CharSequence;)Ljava/util/Collection;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    iput-object v2, p0, Lboi;->d:Ljava/util/List;

    .line 86
    const-string v2, "movieWidth"

    move/from16 v0, p10

    invoke-static {v0, v2}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v2

    iput v2, p0, Lboi;->j:I

    .line 87
    const-string v2, "movieHeight"

    move/from16 v0, p11

    invoke-static {v0, v2}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v2

    iput v2, p0, Lboi;->k:I

    .line 88
    const-string v2, "maxInputVideoWidth"

    move/from16 v0, p12

    invoke-static {v0, v2}, Lcec;->a(ILjava/lang/CharSequence;)I

    .line 89
    const-string v2, "maxInputVideoHeight"

    move/from16 v0, p13

    invoke-static {v0, v2}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v2

    iput v2, p0, Lboi;->l:I

    .line 90
    iget-object v2, p0, Lboi;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    const-string v3, "mPlaybackMetrics.size()"

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    const-string v5, "playback metrics should have the same length as clips"

    invoke-static {v2, v3, v4, v5}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 93
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbmd;

    .line 94
    iget-object v4, v2, Lbmd;->d:Lbmg;

    sget-object v5, Lbmg;->a:Lbmg;

    if-eq v4, v5, :cond_0

    iget-object v4, v2, Lbmd;->d:Lbmg;

    sget-object v5, Lbmg;->c:Lbmg;

    if-eq v4, v5, :cond_0

    iget-object v4, v2, Lbmd;->d:Lbmg;

    sget-object v5, Lbmg;->d:Lbmg;

    if-eq v4, v5, :cond_0

    iget-object v4, v2, Lbmd;->d:Lbmg;

    sget-object v5, Lbmg;->e:Lbmg;

    if-eq v4, v5, :cond_0

    .line 98
    iget-object v2, v2, Lbmd;->d:Lbmg;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2c

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "videoClips should not contain clip of type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v2

    throw v2

    .line 102
    :cond_1
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbma;

    .line 103
    const/4 v3, 0x0

    :goto_0
    invoke-interface {v2}, Lbma;->a()I

    move-result v5

    if-ge v3, v5, :cond_2

    .line 104
    invoke-interface {v2, v3}, Lbma;->a(I)Lbmd;

    move-result-object v5

    .line 105
    iget-object v6, v5, Lbmd;->d:Lbmg;

    sget-object v7, Lbmg;->b:Lbmg;

    if-eq v6, v7, :cond_3

    .line 106
    iget-object v2, v5, Lbmd;->d:Lbmg;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x34

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "audioClipSequences should not contain clip of type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v2

    throw v2

    .line 103
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 112
    :cond_4
    const-string v2, "transitions"

    const/4 v3, 0x0

    invoke-static {p5, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 113
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_5

    .line 114
    invoke-interface {p5}, Ljava/util/List;->size()I

    move-result v2

    const-string v3, "transitions.size()"

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    const-string v5, "transitions should be one element shorter than clips"

    invoke-static {v2, v3, v4, v5}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 117
    :cond_5
    iput-object p5, p0, Lboi;->e:Ljava/util/List;

    .line 118
    invoke-static {p2, p5}, Lboi;->a(Ljava/util/List;Ljava/util/List;)J

    move-result-wide v2

    iput-wide v2, p0, Lboi;->m:J

    .line 119
    const-string v2, "globalEffect"

    const/4 v3, 0x0

    invoke-static {p6, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Layj;

    iput-object v2, p0, Lboi;->f:Layj;

    .line 120
    const-string v2, "title"

    const/4 v3, 0x0

    invoke-static {p7, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lboi;->g:Ljava/lang/String;

    .line 121
    const-string v2, "subtitle"

    const/4 v3, 0x0

    move-object/from16 v0, p8

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lboi;->h:Ljava/lang/String;

    .line 122
    const-string v2, "titleDisplayInterval"

    const/4 v3, 0x0

    move-object/from16 v0, p9

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbmp;

    iput-object v2, p0, Lboi;->i:Lbmp;

    .line 126
    const-wide/16 v4, -0x1

    .line 127
    const/4 v2, 0x0

    move v3, v2

    :goto_1
    iget-object v2, p0, Lboi;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_8

    .line 128
    iget-object v2, p0, Lboi;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbma;

    .line 129
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-eqz v6, :cond_6

    invoke-interface {v2}, Lbma;->b()J

    move-result-wide v6

    cmp-long v6, v6, v4

    if-gez v6, :cond_7

    .line 130
    :cond_6
    invoke-interface {v2}, Lbma;->b()J

    move-result-wide v4

    .line 127
    :cond_7
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    .line 133
    :cond_8
    iput-wide v4, p0, Lboi;->n:J

    .line 134
    return-void
.end method

.method public static a(Lbop;Lbop;)I
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 688
    if-nez p1, :cond_1

    move v2, v3

    .line 727
    :cond_0
    :goto_0
    return v2

    .line 693
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-interface {p0}, Lbop;->f()Ljava/util/List;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 694
    new-instance v5, Ljava/util/ArrayList;

    invoke-interface {p1}, Lbop;->f()Ljava/util/List;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 695
    new-instance v0, Ljava/util/ArrayList;

    .line 696
    invoke-interface {p0}, Lbop;->h()Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 697
    new-instance v1, Ljava/util/ArrayList;

    .line 698
    invoke-interface {p1}, Lbop;->h()Ljava/util/List;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 700
    invoke-interface {p0}, Lbop;->m()Z

    move-result v6

    .line 701
    invoke-interface {p1}, Lbop;->m()Z

    move-result v7

    .line 703
    if-eqz v6, :cond_2

    .line 704
    invoke-interface {v4, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 705
    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 707
    :cond_2
    if-eqz v7, :cond_3

    .line 708
    invoke-interface {v5, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 709
    invoke-interface {v1, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 711
    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_4

    move v2, v3

    .line 712
    goto :goto_0

    :cond_4
    move v1, v2

    .line 714
    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    .line 715
    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-object v8, v0, Lbmd;->e:Ljeg;

    .line 716
    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-object v0, v0, Lbmd;->e:Ljeg;

    .line 715
    invoke-virtual {v8, v0}, Ljeg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    move v2, v3

    .line 717
    goto :goto_0

    .line 714
    :cond_5
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 722
    :cond_6
    if-eqz v6, :cond_7

    if-nez v7, :cond_7

    .line 723
    const/4 v2, 0x2

    goto :goto_0

    .line 724
    :cond_7
    if-nez v6, :cond_0

    if-eqz v7, :cond_0

    .line 725
    const/4 v2, 0x4

    goto :goto_0
.end method

.method public static final a(Ljava/util/List;Ljava/util/List;)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcac;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 670
    const-wide/16 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    invoke-virtual {v0}, Lbmd;->b()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    goto :goto_0

    .line 671
    :cond_0
    if-eqz p1, :cond_1

    .line 672
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcac;

    .line 673
    invoke-interface {v0}, Lcac;->b()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 674
    goto :goto_1

    .line 676
    :cond_1
    return-wide v2
.end method


# virtual methods
.method public a(JZ)I
    .locals 7

    .prologue
    .line 308
    invoke-virtual {p0}, Lboi;->e()I

    move-result v3

    .line 309
    const/4 v2, 0x0

    :goto_0
    if-ge v2, v3, :cond_3

    .line 310
    invoke-virtual {p0, v2}, Lboi;->b(I)Lbmd;

    move-result-object v0

    .line 311
    invoke-virtual {v0}, Lbmd;->b()J

    move-result-wide v4

    cmp-long v1, p1, v4

    if-gez v1, :cond_1

    if-nez p3, :cond_0

    iget-object v1, v0, Lbmd;->d:Lbmg;

    sget-object v4, Lbmg;->d:Lbmg;

    if-eq v1, v4, :cond_1

    :cond_0
    move v0, v2

    .line 320
    :goto_1
    return v0

    .line 316
    :cond_1
    invoke-virtual {v0}, Lbmd;->b()J

    move-result-wide v0

    sub-long v4, p1, v0

    .line 317
    add-int/lit8 v0, v3, -0x1

    if-ge v2, v0, :cond_2

    .line 318
    invoke-virtual {p0, v2}, Lboi;->d(I)Lcac;

    move-result-object v0

    invoke-interface {v0}, Lcac;->b()J

    move-result-wide v0

    :goto_2
    add-long p1, v4, v0

    .line 309
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 318
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_2

    :cond_3
    move v0, v3

    .line 320
    goto :goto_1
.end method

.method public a(JI)J
    .locals 5

    .prologue
    .line 337
    invoke-virtual {p0, p3}, Lboi;->f(I)J

    move-result-wide v0

    sub-long v0, p1, v0

    .line 338
    invoke-virtual {p0, p3}, Lboi;->b(I)Lbmd;

    move-result-object v2

    iget-object v2, v2, Lbmd;->f:Lbmp;

    iget-wide v2, v2, Lbmp;->b:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public a(Lboi;J)J
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 600
    const-string v0, "oldStoryboard"

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 603
    const-string v0, "oldTimestampUs"

    .line 604
    invoke-static {p2, p3, v0}, Lcec;->b(JLjava/lang/CharSequence;)J

    move-result-wide v2

    move v0, v1

    .line 606
    :goto_0
    invoke-virtual {p1}, Lboi;->e()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 607
    invoke-virtual {p1, v0}, Lboi;->b(I)Lbmd;

    move-result-object v4

    invoke-virtual {v4}, Lbmd;->b()J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-ltz v4, :cond_1

    .line 608
    invoke-virtual {p1, v0}, Lboi;->b(I)Lbmd;

    move-result-object v4

    invoke-virtual {v4}, Lbmd;->b()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 612
    invoke-virtual {p1}, Lboi;->e()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_0

    .line 614
    invoke-virtual {p1, v0}, Lboi;->d(I)Lcac;

    move-result-object v4

    invoke-interface {v4}, Lcac;->b()J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 606
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 619
    :cond_1
    invoke-virtual {p1, v1}, Lboi;->b(I)Lbmd;

    move-result-object v4

    iget-object v4, v4, Lbmd;->d:Lbmg;

    sget-object v5, Lbmg;->d:Lbmg;

    if-ne v4, v5, :cond_3

    .line 620
    invoke-virtual {p0, v1}, Lboi;->b(I)Lbmd;

    move-result-object v4

    iget-object v4, v4, Lbmd;->d:Lbmg;

    sget-object v5, Lbmg;->d:Lbmg;

    if-eq v4, v5, :cond_3

    const/4 v4, 0x1

    .line 621
    :goto_1
    if-eqz v4, :cond_4

    if-nez v0, :cond_4

    .line 623
    const-wide/16 v0, 0x0

    .line 648
    :cond_2
    return-wide v0

    :cond_3
    move v4, v1

    .line 620
    goto :goto_1

    .line 628
    :cond_4
    if-eqz v4, :cond_6

    .line 629
    add-int/lit8 v4, v0, -0x1

    .line 631
    :goto_2
    invoke-virtual {p1, v1}, Lboi;->b(I)Lbmd;

    move-result-object v5

    iget-object v5, v5, Lbmd;->d:Lbmg;

    sget-object v6, Lbmg;->d:Lbmg;

    if-eq v5, v6, :cond_5

    .line 632
    invoke-virtual {p0, v1}, Lboi;->b(I)Lbmd;

    move-result-object v5

    iget-object v5, v5, Lbmd;->d:Lbmg;

    sget-object v6, Lbmg;->d:Lbmg;

    if-ne v5, v6, :cond_5

    .line 633
    add-int/lit8 v4, v4, 0x1

    .line 637
    :cond_5
    const-string v5, "newClipIndex"

    invoke-virtual {p0}, Lboi;->f()Ljava/util/List;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcgp;->a(ILjava/lang/CharSequence;Ljava/util/Collection;)I

    .line 638
    invoke-virtual {p1, v0}, Lboi;->b(I)Lbmd;

    move-result-object v0

    iget-object v0, v0, Lbmd;->e:Ljeg;

    const-string v5, "oldStoryboard.getVideoClip(clipIndex).uri"

    .line 640
    invoke-virtual {p0, v4}, Lboi;->b(I)Lbmd;

    move-result-object v6

    iget-object v6, v6, Lbmd;->e:Ljeg;

    .line 638
    invoke-static {v0, v5, v6}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    move v8, v1

    move-wide v0, v2

    move v2, v8

    .line 644
    :goto_3
    if-ge v2, v4, :cond_2

    .line 645
    invoke-virtual {p0, v2}, Lboi;->b(I)Lbmd;

    move-result-object v3

    invoke-virtual {v3}, Lbmd;->b()J

    move-result-wide v6

    add-long/2addr v0, v6

    .line 646
    invoke-virtual {p0, v2}, Lboi;->d(I)Lcac;

    move-result-object v3

    invoke-interface {v3}, Lcac;->b()J

    move-result-wide v6

    sub-long v6, v0, v6

    .line 644
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move-wide v0, v6

    goto :goto_3

    :cond_6
    move v4, v0

    goto :goto_2
.end method

.method public a(I)Lbma;
    .locals 3

    .prologue
    .line 208
    iget-object v0, p0, Lboi;->c:Ljava/util/List;

    const-string v1, "n"

    iget-object v2, p0, Lboi;->c:Ljava/util/List;

    invoke-static {p1, v1, v2}, Lcec;->a(ILjava/lang/CharSequence;Ljava/util/Collection;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbma;

    return-object v0
.end method

.method public a(Ljeg;)Lbon;
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Lboi;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbml;

    .line 264
    instance-of v1, v0, Lbon;

    if-eqz v1, :cond_0

    .line 265
    check-cast v0, Lbon;

    .line 267
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbma;",
            ">;"
        }
    .end annotation

    .prologue
    .line 218
    iget-object v0, p0, Lboi;->c:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lboi;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public b(I)Lbmd;
    .locals 3

    .prologue
    .line 149
    iget-object v0, p0, Lboi;->b:Ljava/util/List;

    const-string v1, "n"

    iget-object v2, p0, Lboi;->b:Ljava/util/List;

    invoke-static {p1, v1, v2}, Lcec;->a(ILjava/lang/CharSequence;Ljava/util/Collection;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    return-object v0
.end method

.method public b(JZ)Lbmd;
    .locals 3

    .prologue
    .line 351
    invoke-virtual {p0}, Lboi;->e()I

    move-result v1

    .line 352
    invoke-virtual {p0, p1, p2, p3}, Lboi;->a(JZ)I

    move-result v0

    .line 353
    if-ne v0, v1, :cond_0

    .line 354
    add-int/lit8 v0, v0, -0x1

    .line 356
    if-nez p3, :cond_0

    .line 359
    :goto_0
    if-ltz v0, :cond_0

    invoke-virtual {p0, v0}, Lboi;->b(I)Lbmd;

    move-result-object v1

    iget-object v1, v1, Lbmd;->d:Lbmg;

    sget-object v2, Lbmg;->d:Lbmg;

    if-ne v1, v2, :cond_0

    .line 360
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 364
    :cond_0
    if-ltz v0, :cond_1

    invoke-virtual {p0, v0}, Lboi;->b(I)Lbmd;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public c()J
    .locals 2

    .prologue
    .line 223
    iget-wide v0, p0, Lboi;->n:J

    return-wide v0
.end method

.method public c(I)Lbmh;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lboi;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmh;

    return-object v0
.end method

.method public d()J
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 139
    iget-object v0, p0, Lboi;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbma;

    invoke-interface {v0, v1}, Lbma;->a(I)Lbmd;

    move-result-object v0

    iget-object v0, v0, Lbmd;->f:Lbmp;

    iget-wide v0, v0, Lbmp;->c:J

    return-wide v0
.end method

.method public d(I)Lcac;
    .locals 3

    .prologue
    .line 173
    iget-object v0, p0, Lboi;->e:Ljava/util/List;

    const-string v1, "from"

    iget-object v2, p0, Lboi;->e:Ljava/util/List;

    invoke-static {p1, v1, v2}, Lcec;->a(ILjava/lang/CharSequence;Ljava/util/Collection;)I

    move-result v1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcac;

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lboi;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public e(I)Lbmm;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 233
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lboi;->e()I

    move-result v1

    if-lt p1, v1, :cond_1

    .line 252
    :cond_0
    :goto_0
    return-object v0

    .line 237
    :cond_1
    invoke-virtual {p0, p1}, Lboi;->b(I)Lbmd;

    move-result-object v2

    .line 238
    iget-object v1, v2, Lbmd;->d:Lbmg;

    sget-object v3, Lbmg;->c:Lbmg;

    if-eq v1, v3, :cond_2

    iget-object v1, v2, Lbmd;->d:Lbmg;

    sget-object v3, Lbmg;->a:Lbmg;

    if-ne v1, v3, :cond_0

    .line 242
    :cond_2
    iget-object v1, p0, Lboi;->a:Ljava/util/Map;

    iget-object v3, v2, Lbmd;->e:Ljeg;

    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 243
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2a

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "no Displayable found for PHOTO/VIDEO clip "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 246
    :cond_3
    iget-object v1, p0, Lboi;->a:Ljava/util/Map;

    iget-object v3, v2, Lbmd;->e:Ljeg;

    .line 247
    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x21

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "displayable for index "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 246
    invoke-static {v1, v3, v0}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbml;

    .line 249
    iget-object v1, v2, Lbmd;->d:Lbmg;

    sget-object v3, Lbmg;->c:Lbmg;

    if-ne v1, v3, :cond_4

    .line 250
    new-instance v1, Lbmm;

    check-cast v0, Lbmw;

    invoke-direct {v1, v0}, Lbmm;-><init>(Lbmw;)V

    move-object v0, v1

    goto :goto_0

    .line 251
    :cond_4
    iget-object v1, v2, Lbmd;->d:Lbmg;

    sget-object v3, Lbmg;->a:Lbmg;

    if-ne v1, v3, :cond_5

    .line 252
    new-instance v1, Lbmm;

    check-cast v0, Lbon;

    iget-object v2, v2, Lbmd;->f:Lbmp;

    iget-wide v2, v2, Lbmp;->b:J

    invoke-direct {v1, v0, v2, v3}, Lbmm;-><init>(Lbon;J)V

    move-object v0, v1

    goto/16 :goto_0

    .line 254
    :cond_5
    const-string v0, "unexpected clip type"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method public f(I)J
    .locals 6

    .prologue
    .line 325
    const-string v0, "index"

    invoke-virtual {p0}, Lboi;->f()Ljava/util/List;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcec;->a(ILjava/lang/CharSequence;Ljava/util/Collection;)I

    .line 326
    const-wide/16 v2, 0x0

    .line 327
    const/4 v0, 0x0

    :goto_0
    if-ge v0, p1, :cond_0

    .line 328
    invoke-virtual {p0, v0}, Lboi;->b(I)Lbmd;

    move-result-object v1

    invoke-virtual {v1}, Lbmd;->b()J

    move-result-wide v4

    add-long/2addr v2, v4

    .line 329
    invoke-virtual {p0, v0}, Lboi;->d(I)Lcac;

    move-result-object v1

    invoke-interface {v1}, Lcac;->b()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 327
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 332
    :cond_0
    return-wide v2
.end method

.method public f()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154
    iget-object v0, p0, Lboi;->b:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public g()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;"
        }
    .end annotation

    .prologue
    .line 158
    iget-object v0, p0, Lboi;->a:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 168
    iget-object v0, p0, Lboi;->e:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public i()Layj;
    .locals 1

    .prologue
    .line 178
    iget-object v0, p0, Lboi;->f:Layj;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lboi;->g:Ljava/lang/String;

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lboi;->h:Ljava/lang/String;

    return-object v0
.end method

.method public l()Lbmp;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lboi;->i:Lbmp;

    return-object v0
.end method

.method public m()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 198
    iget-object v0, p0, Lboi;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lboi;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-object v0, v0, Lbmd;->d:Lbmg;

    sget-object v2, Lbmg;->d:Lbmg;

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public n()J
    .locals 2

    .prologue
    .line 203
    iget-wide v0, p0, Lboi;->m:J

    return-wide v0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 276
    iget v0, p0, Lboi;->j:I

    return v0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 285
    iget v0, p0, Lboi;->k:I

    return v0
.end method

.method public q()I
    .locals 1

    .prologue
    .line 303
    iget v0, p0, Lboi;->l:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 369
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lboi;->b:Ljava/util/List;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lboi;->c:Ljava/util/List;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lboi;->e:Ljava/util/List;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lboi;->f:Layj;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lboi;->g:Ljava/lang/String;

    .line 374
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "\""

    const-string v5, "\\\""

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x2

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "\""

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\""

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 369
    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
