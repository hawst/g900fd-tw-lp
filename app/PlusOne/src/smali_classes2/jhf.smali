.class public final Ljhf;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lieg;
.implements Ljhd;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljhg;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Ljhf;->a:Landroid/content/Context;

    .line 43
    new-instance v0, Ljhg;

    invoke-direct {v0}, Ljhg;-><init>()V

    iput-object v0, p0, Ljhf;->b:Ljhg;

    .line 44
    iget-object v0, p0, Ljhf;->a:Landroid/content/Context;

    const-class v1, Lieh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    .line 45
    invoke-interface {v0, p0}, Lieh;->a(Lieg;)V

    .line 46
    return-void
.end method


# virtual methods
.method public a()V
    .locals 13

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 113
    iget-object v10, p0, Ljhf;->b:Ljhg;

    monitor-enter v10

    .line 114
    :try_start_0
    iget-object v11, p0, Ljhf;->a:Landroid/content/Context;

    iget-object v1, p0, Ljhf;->b:Ljhg;

    invoke-virtual {v1}, Ljhg;->c()Ljava/lang/String;

    move-result-object v12

    iget-object v1, p0, Ljhf;->b:Ljhg;

    invoke-virtual {v1}, Ljhg;->a()[Lqbk;

    move-result-object v5

    iget-object v1, p0, Ljhf;->b:Ljhg;

    .line 115
    invoke-virtual {v1}, Ljhg;->b()[Ljava/lang/Long;

    move-result-object v6

    .line 114
    const-class v1, Ljgn;

    invoke-static {v11, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljgn;

    move-object v4, v0

    new-instance v1, Lkoi;

    invoke-static {v11}, Ljgr;->a(Landroid/content/Context;)I

    move-result v2

    invoke-interface {v4}, Ljgn;->k()I

    move-result v3

    int-to-float v3, v3

    invoke-interface {v4}, Ljgn;->l()I

    move-result v4

    if-ne v4, v8, :cond_1

    move v4, v7

    :goto_0
    invoke-direct/range {v1 .. v6}, Lkoi;-><init>(IFI[Lqbk;[Ljava/lang/Long;)V

    invoke-virtual {v1, v12}, Lkoi;->a(Ljava/lang/String;)Lkoh;

    move-result-object v1

    invoke-virtual {v1, v11}, Lkoh;->a(Landroid/content/Context;)V

    .line 116
    const-string v1, "LogSink"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 117
    iget-object v1, p0, Ljhf;->b:Ljhg;

    invoke-virtual {v1}, Ljhg;->a()[Lqbk;

    move-result-object v1

    array-length v1, v1

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Flushed "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " requests."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 119
    :cond_0
    iget-object v1, p0, Ljhf;->b:Ljhg;

    invoke-virtual {v1}, Ljhg;->e()V

    .line 120
    monitor-exit v10

    return-void

    .line 114
    :cond_1
    if-ne v4, v7, :cond_2

    move v4, v8

    goto :goto_0

    :cond_2
    const/4 v7, -0x1

    if-ne v4, v7, :cond_3

    move v4, v9

    goto :goto_0

    :cond_3
    const/4 v7, -0x2

    if-ne v4, v7, :cond_4

    const/4 v4, 0x4

    goto :goto_0

    :cond_4
    const/4 v4, 0x0

    goto :goto_0

    .line 120
    :catchall_0
    move-exception v1

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method

.method public declared-synchronized a(I[Ljava/lang/String;Ljava/lang/String;JJJJJILjava/lang/String;)V
    .locals 8

    .prologue
    .line 59
    monitor-enter p0

    const/16 v2, 0xc8

    move/from16 v0, p14

    if-eq v0, v2, :cond_1

    .line 90
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 66
    :cond_1
    :try_start_0
    iget-object v2, p0, Ljhf;->a:Landroid/content/Context;

    const-class v3, Lkbu;

    invoke-static {v2, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lkbu;

    invoke-interface {v2}, Lkbu;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 69
    iget-object v2, p0, Ljhf;->b:Ljhg;

    invoke-virtual {v2, p3}, Ljhg;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 70
    invoke-virtual {p0}, Ljhf;->a()V

    .line 72
    :cond_2
    iget-object v3, p0, Ljhf;->b:Ljhg;

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 73
    :try_start_1
    new-instance v4, Lqbk;

    invoke-direct {v4}, Lqbk;-><init>()V

    .line 74
    invoke-static/range {p14 .. p14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v4, Lqbk;->f:Ljava/lang/Integer;

    .line 75
    iput-object p2, v4, Lqbk;->c:[Ljava/lang/String;

    .line 76
    move-wide/from16 v0, p10

    long-to-int v2, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v4, Lqbk;->h:Ljava/lang/Integer;

    .line 77
    move-wide/from16 v0, p12

    long-to-int v2, v0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v4, Lqbk;->g:Ljava/lang/Integer;

    .line 78
    sub-long v6, p8, p4

    long-to-int v2, v6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v4, Lqbk;->d:Ljava/lang/Integer;

    .line 79
    sub-long v6, p6, p4

    long-to-int v2, v6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v4, Lqbk;->e:Ljava/lang/Integer;

    .line 80
    if-eqz p15, :cond_a

    const-string v2, ""

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    const-string v2, "http/1.1"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v2, 0x1

    :goto_1
    iput v2, v4, Lqbk;->i:I

    .line 81
    iput p1, v4, Lqbk;->b:I

    .line 82
    iget-object v2, p0, Ljhf;->b:Ljhg;

    invoke-virtual {v2, v4, p3}, Ljhg;->a(Lqbk;Ljava/lang/String;)V

    .line 83
    iget-object v2, p0, Ljhf;->b:Ljhg;

    iget-object v5, p0, Ljhf;->a:Landroid/content/Context;

    invoke-virtual {v2, v5}, Ljhg;->a(Landroid/content/Context;)Z

    .line 84
    const-string v2, "LogSink"

    const/4 v5, 0x3

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 85
    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0xe

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Added request:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 87
    :cond_3
    iget-object v2, p0, Ljhf;->b:Ljhg;

    invoke-virtual {v2}, Ljhg;->f()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 88
    invoke-virtual {p0}, Ljhf;->a()V

    .line 90
    :cond_4
    monitor-exit v3

    goto/16 :goto_0

    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 59
    :catchall_1
    move-exception v2

    monitor-exit p0

    throw v2

    .line 80
    :cond_5
    :try_start_3
    const-string v2, "spdy/2"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v2, 0x2

    goto :goto_1

    :cond_6
    const-string v2, "spdy/3"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    const/4 v2, 0x3

    goto :goto_1

    :cond_7
    const-string v2, "spdy/3.1"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    const/4 v2, 0x4

    goto :goto_1

    :cond_8
    const-string v2, "h2-14"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    const/4 v2, 0x5

    goto :goto_1

    :cond_9
    const-string v2, "quic/1+spdy/3"

    move-object/from16 v0, p15

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v2

    if-eqz v2, :cond_a

    const/4 v2, 0x6

    goto/16 :goto_1

    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method public l()V
    .locals 1

    .prologue
    .line 155
    iget-object v0, p0, Ljhf;->b:Ljhg;

    invoke-virtual {v0}, Ljhg;->d()V

    .line 156
    invoke-virtual {p0}, Ljhf;->a()V

    .line 157
    return-void
.end method
