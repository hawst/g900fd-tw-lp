.class public Ljzz;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkji;


# instance fields
.field private a:Landroid/content/Context;

.field private final b:Ljyu;

.field private final c:Ljye;

.field private d:Lkjg;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    const/4 v0, 0x0

    iput-object v0, p0, Ljzz;->a:Landroid/content/Context;

    .line 42
    iput-object p1, p0, Ljzz;->a:Landroid/content/Context;

    .line 43
    new-instance v0, Ljyu;

    invoke-direct {v0, p1}, Ljyu;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ljzz;->b:Ljyu;

    .line 44
    new-instance v0, Ljye;

    invoke-direct {v0, p1}, Ljye;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Ljzz;->c:Ljye;

    .line 45
    return-void
.end method

.method static synthetic a(Ljzz;)Lkjg;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Ljzz;->d:Lkjg;

    return-object v0
.end method


# virtual methods
.method public a(I)Lu;
    .locals 1

    .prologue
    .line 109
    packed-switch p1, :pswitch_data_0

    .line 115
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 111
    :pswitch_0
    new-instance v0, Ljym;

    invoke-direct {v0}, Ljym;-><init>()V

    goto :goto_0

    .line 113
    :pswitch_1
    new-instance v0, Ljyj;

    invoke-direct {v0}, Ljyj;-><init>()V

    goto :goto_0

    .line 109
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a()V
    .locals 2

    .prologue
    .line 57
    iget-object v0, p0, Ljzz;->b:Ljyu;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljyu;->a(Z)V

    .line 58
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Ljzz;->b:Ljyu;

    invoke-virtual {v0, p1}, Ljyu;->a(Landroid/os/Bundle;)V

    .line 69
    iget-object v0, p0, Ljzz;->c:Ljye;

    invoke-virtual {v0, p1}, Ljye;->a(Landroid/os/Bundle;)V

    .line 71
    iget-object v0, p0, Ljzz;->a:Landroid/content/Context;

    const-class v1, Lkjg;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkjg;

    iput-object v0, p0, Ljzz;->d:Lkjg;

    .line 73
    iget-object v0, p0, Ljzz;->b:Ljyu;

    new-instance v1, Lkaa;

    invoke-direct {v1, p0}, Lkaa;-><init>(Ljzz;)V

    invoke-virtual {v0, v1}, Ljyu;->a(Ljyv;)V

    .line 94
    return-void
.end method

.method public a(Lodg;)V
    .locals 4

    .prologue
    .line 136
    const/4 v0, 0x0

    iput-object v0, p1, Lodg;->i:Lofq;

    .line 137
    iget-object v0, p1, Lodg;->o:Lohc;

    if-nez v0, :cond_0

    .line 138
    new-instance v0, Lohc;

    invoke-direct {v0}, Lohc;-><init>()V

    iput-object v0, p1, Lodg;->o:Lohc;

    .line 140
    :cond_0
    iget-object v0, p1, Lodg;->o:Lohc;

    const/16 v1, 0x1a

    iput v1, v0, Lohc;->a:I

    .line 141
    iget-object v0, p0, Ljzz;->b:Ljyu;

    invoke-virtual {v0}, Ljyu;->e()Ljava/lang/String;

    move-result-object v0

    .line 142
    if-nez v0, :cond_1

    const-string v0, ""

    :cond_1
    iput-object v0, p1, Lodg;->a:Ljava/lang/String;

    .line 143
    new-instance v0, Ljza;

    iget-object v1, p0, Ljzz;->a:Landroid/content/Context;

    iget-object v2, p0, Ljzz;->b:Ljyu;

    iget-object v3, p0, Ljzz;->c:Ljye;

    invoke-direct {v0, v1, v2, v3}, Ljza;-><init>(Landroid/content/Context;Ljyu;Ljye;)V

    invoke-virtual {v0}, Ljza;->a()Loya;

    move-result-object v0

    iput-object v0, p1, Lodg;->q:Loya;

    .line 144
    return-void
.end method

.method public a(Landroid/widget/ImageButton;)Z
    .locals 2

    .prologue
    .line 176
    new-instance v0, Lhmk;

    sget-object v1, Lonj;->e:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    invoke-static {p1, v0}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    .line 178
    const/4 v0, 0x1

    return v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Ljzz;->b:Ljyu;

    invoke-virtual {v0}, Ljyu;->d()V

    .line 63
    iget-object v0, p0, Ljzz;->b:Ljyu;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljyu;->a(Z)V

    .line 64
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Ljzz;->b:Ljyu;

    invoke-virtual {v0, p1}, Ljyu;->b(Landroid/os/Bundle;)V

    .line 99
    iget-object v0, p0, Ljzz;->c:Ljye;

    invoke-virtual {v0, p1}, Ljye;->b(Landroid/os/Bundle;)V

    .line 100
    return-void
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Ljzz;->b:Ljyu;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljyu;->c(I)Z

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Ljzz;->b:Ljyu;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljyu;->c(I)Z

    move-result v0

    return v0
.end method

.method public e()Lkjf;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 148
    new-instance v0, Lkjf;

    invoke-direct {v0}, Lkjf;-><init>()V

    .line 149
    iput-boolean v1, v0, Lkjf;->b:Z

    .line 150
    iput-boolean v1, v0, Lkjf;->a:Z

    .line 151
    return-object v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Ljzz;->b:Ljyu;

    invoke-virtual {v0}, Ljyu;->c()Z

    move-result v0

    return v0
.end method

.method public g()Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 166
    iget-object v0, p0, Ljzz;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02053b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Ljzz;->a:Landroid/content/Context;

    const v1, 0x7f0a0187

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 183
    iget-object v0, p0, Ljzz;->a:Landroid/content/Context;

    const v1, 0x7f0a018e

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Ljyu;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Ljzz;->b:Ljyu;

    return-object v0
.end method

.method public k()Ljye;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Ljzz;->c:Ljye;

    return-object v0
.end method

.method public l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 202
    const-string v0, "polls"

    return-object v0
.end method
