.class public final enum Lawg;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lawg;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lawg;

.field public static final enum b:Lawg;

.field public static final enum c:Lawg;

.field private static final synthetic d:[Lawg;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 7
    new-instance v0, Lawg;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v2}, Lawg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lawg;->a:Lawg;

    .line 14
    new-instance v0, Lawg;

    const-string v1, "LOCAL"

    invoke-direct {v0, v1, v3}, Lawg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lawg;->b:Lawg;

    .line 21
    new-instance v0, Lawg;

    const-string v1, "CLOUD"

    invoke-direct {v0, v1, v4}, Lawg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lawg;->c:Lawg;

    .line 4
    const/4 v0, 0x3

    new-array v0, v0, [Lawg;

    sget-object v1, Lawg;->a:Lawg;

    aput-object v1, v0, v2

    sget-object v1, Lawg;->b:Lawg;

    aput-object v1, v0, v3

    sget-object v1, Lawg;->c:Lawg;

    aput-object v1, v0, v4

    sput-object v0, Lawg;->d:[Lawg;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 4
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lawg;
    .locals 1

    .prologue
    .line 4
    const-class v0, Lawg;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lawg;

    return-object v0
.end method

.method public static values()[Lawg;
    .locals 1

    .prologue
    .line 4
    sget-object v0, Lawg;->d:[Lawg;

    invoke-virtual {v0}, [Lawg;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lawg;

    return-object v0
.end method
