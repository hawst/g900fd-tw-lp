.class final Lbey;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcca;


# instance fields
.field final synthetic a:Lbem;


# direct methods
.method constructor <init>(Lbem;)V
    .locals 0

    .prologue
    .line 552
    iput-object p1, p0, Lbey;->a:Lbem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/view/View;Lbml;Lbmd;)V
    .locals 8

    .prologue
    .line 560
    iget-object v0, p4, Lbmd;->e:Ljeg;

    const-string v1, "clip.mediaIdentifier"

    .line 561
    invoke-interface {p3}, Lbml;->a()Ljeg;

    move-result-object v2

    const-string v3, "clip\'s media ID must match the displayable"

    .line 560
    invoke-static {v0, v1, v2, v3}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 563
    const v0, 0x7f1005e6

    .line 564
    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;

    .line 565
    iget-object v1, p0, Lbey;->a:Lbem;

    invoke-static {v1}, Lbem;->j(Lbem;)Lbqo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->a(Lbqo;)V

    .line 567
    iget-object v1, p0, Lbey;->a:Lbem;

    invoke-static {v1}, Lbem;->i(Lbem;)Lccc;

    move-result-object v1

    invoke-virtual {v1}, Lccc;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    .line 569
    :goto_0
    iget-object v2, p0, Lbey;->a:Lbem;

    invoke-static {v2}, Lbem;->i(Lbem;)Lccc;

    move-result-object v2

    invoke-virtual {v2}, Lccc;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lbey;->a:Lbem;

    .line 570
    invoke-static {v2}, Lbem;->i(Lbem;)Lccc;

    move-result-object v2

    invoke-virtual {v2}, Lccc;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 571
    :goto_1
    sget-object v3, Lbep;->a:[I

    invoke-interface {p3}, Lbml;->b()Lbmu;

    move-result-object v4

    iget-object v4, v4, Lbmu;->e:Lbmv;

    invoke-virtual {v4}, Lbmv;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 595
    const-string v0, "posters can only be shown for PHOTO/VIDEO clips"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 567
    :cond_0
    iget-object v1, p0, Lbey;->a:Lbem;

    .line 568
    invoke-static {v1}, Lbem;->i(Lbem;)Lccc;

    move-result-object v1

    invoke-virtual {v1}, Lccc;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_0

    .line 570
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 574
    :pswitch_0
    invoke-virtual {p4}, Lbmd;->b()J

    move-result-wide v4

    iget-object v3, p0, Lbey;->a:Lbem;

    invoke-virtual {v3}, Lbem;->o()Landroid/content/res/Resources;

    move-result-object v3

    .line 573
    invoke-static {v4, v5, v3}, Lchf;->a(JLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v3

    .line 578
    iget-object v4, p4, Lbmd;->d:Lbmg;

    sget-object v5, Lbmg;->e:Lbmg;

    if-ne v4, v5, :cond_3

    iget-object v4, p4, Lbmd;->f:Lbmp;

    iget-wide v4, v4, Lbmp;->b:J

    iget-object v6, p4, Lbmd;->f:Lbmp;

    iget-wide v6, v6, Lbmp;->c:J

    add-long/2addr v4, v6

    const-wide/16 v6, 0x2

    div-long/2addr v4, v6

    .line 581
    :goto_2
    iget-object v6, p0, Lbey;->a:Lbem;

    invoke-static {v6}, Lbem;->k(Lbem;)Ljava/util/concurrent/Executor;

    move-result-object v6

    new-instance v7, Lbmm;

    check-cast p3, Lbon;

    invoke-direct {v7, p3, v4, v5}, Lbmm;-><init>(Lbon;J)V

    invoke-virtual {v0, v6, v7, v1, v2}, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->a(Ljava/util/concurrent/Executor;Lbmm;II)V

    .line 585
    invoke-virtual {v0, v3}, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->a(Ljava/lang/String;)V

    .line 598
    :goto_3
    new-instance v1, Lbez;

    invoke-direct {v1, p0, p2, p4}, Lbez;-><init>(Lbey;Landroid/view/View;Lbmd;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 611
    new-instance v1, Lbfa;

    invoke-direct {v1, p0, p2}, Lbfa;-><init>(Lbey;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    .line 619
    const v1, 0x7f10017d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 620
    iget-boolean v2, p4, Lbmd;->h:Z

    if-eqz v2, :cond_4

    .line 621
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 622
    new-instance v2, Lbfb;

    invoke-direct {v2, p0, p2, p4}, Lbfb;-><init>(Lbey;Landroid/view/View;Lbmd;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 632
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->a(Z)V

    .line 639
    :goto_4
    iget-object v1, p0, Lbey;->a:Lbem;

    invoke-static {v1}, Lbem;->l(Lbem;)Lbmd;

    move-result-object v1

    if-eqz v1, :cond_2

    iget v1, p4, Lbmd;->b:I

    iget-object v2, p0, Lbey;->a:Lbem;

    invoke-static {v2}, Lbem;->l(Lbem;)Lbmd;

    move-result-object v2

    iget v2, v2, Lbmd;->b:I

    if-ne v1, v2, :cond_2

    .line 640
    iget-object v1, p0, Lbey;->a:Lbem;

    invoke-static {v1}, Lbem;->m(Lbem;)Landroid/graphics/RectF;

    move-result-object v1

    const/16 v2, 0x1f4

    const/16 v3, 0x2ee

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->a(Landroid/graphics/RectF;II)V

    .line 642
    iget-object v0, p0, Lbey;->a:Lbem;

    invoke-static {v0}, Lbem;->i(Lbem;)Lccc;

    move-result-object v0

    const-class v1, Lcdg;

    invoke-virtual {v0, v1}, Lccc;->a(Ljava/lang/Class;)Lcch;

    move-result-object v0

    check-cast v0, Lcdg;

    const/16 v1, 0x1f4

    const/16 v2, 0x2ee

    iget-object v3, p0, Lbey;->a:Lbem;

    .line 644
    invoke-static {v3}, Lbem;->n(Lbem;)Landroid/graphics/Rect;

    move-result-object v3

    .line 642
    invoke-virtual {v0, v1, v2, p1, v3}, Lcdg;->a(IIILandroid/graphics/Rect;)V

    .line 645
    iget-object v0, p0, Lbey;->a:Lbem;

    invoke-static {v0}, Lbem;->o(Lbem;)V

    .line 647
    :cond_2
    const/4 v0, 0x1

    invoke-static {p2, v0}, Lbem;->a(Landroid/view/View;Z)V

    .line 648
    return-void

    .line 578
    :cond_3
    iget-object v4, p4, Lbmd;->f:Lbmp;

    iget-wide v4, v4, Lbmp;->b:J

    goto/16 :goto_2

    .line 588
    :pswitch_1
    iget-object v3, p0, Lbey;->a:Lbem;

    invoke-static {v3}, Lbem;->k(Lbem;)Ljava/util/concurrent/Executor;

    move-result-object v3

    new-instance v4, Lbmm;

    check-cast p3, Lbmw;

    invoke-direct {v4, p3}, Lbmm;-><init>(Lbmw;)V

    invoke-virtual {v0, v3, v4, v1, v2}, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->a(Ljava/util/concurrent/Executor;Lbmm;II)V

    .line 592
    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->a()V

    goto/16 :goto_3

    .line 634
    :cond_4
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 635
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 636
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/StoryboardVideoPosterView;->a(Z)V

    goto :goto_4

    .line 571
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcbx;)V
    .locals 1

    .prologue
    .line 652
    invoke-virtual {p1}, Lcbx;->a()V

    .line 653
    new-instance v0, Lbfc;

    invoke-direct {v0, p0}, Lbfc;-><init>(Lbey;)V

    invoke-virtual {p1, v0}, Lcbx;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 660
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lbem;->a(Landroid/view/View;Z)V

    .line 661
    return-void
.end method

.method public a(Lcbx;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 666
    invoke-virtual {p1}, Lcbx;->b()V

    .line 668
    iget-object v1, p0, Lbey;->a:Lbem;

    const v0, 0x7f10060b

    invoke-virtual {p1, v0}, Lcbx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-static {v1, v0}, Lbem;->a(Lbem;Landroid/widget/LinearLayout;)Landroid/widget/LinearLayout;

    .line 669
    iget-object v1, p0, Lbey;->a:Lbem;

    const v0, 0x7f10038c

    invoke-virtual {p1, v0}, Lcbx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    invoke-static {v1, v0}, Lbem;->a(Lbem;Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;)Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    .line 670
    iget-object v0, p0, Lbey;->a:Lbem;

    const v1, 0x7f10017d

    invoke-virtual {p1, v1}, Lcbx;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lbem;->a(Lbem;Landroid/view/View;)Landroid/view/View;

    .line 671
    iget-object v1, p0, Lbey;->a:Lbem;

    iget-object v0, p0, Lbey;->a:Lbem;

    .line 672
    invoke-static {v0}, Lbem;->c(Lbem;)Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v2, "input_method"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 671
    invoke-static {v1, v0}, Lbem;->a(Lbem;Landroid/view/inputmethod/InputMethodManager;)Landroid/view/inputmethod/InputMethodManager;

    .line 674
    iget-object v0, p0, Lbey;->a:Lbem;

    invoke-static {v0}, Lbem;->b(Lbem;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lbey;->a:Lbem;

    invoke-static {v1}, Lbem;->p(Lbem;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 675
    iget-object v0, p0, Lbey;->a:Lbem;

    invoke-static {v0}, Lbem;->c(Lbem;)Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    move-result-object v0

    iget-object v1, p0, Lbey;->a:Lbem;

    invoke-static {v1}, Lbem;->q(Lbem;)Landroid/widget/TextView$OnEditorActionListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 676
    iget-object v0, p0, Lbey;->a:Lbem;

    invoke-static {v0}, Lbem;->c(Lbem;)Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    move-result-object v0

    iget-object v1, p0, Lbey;->a:Lbem;

    invoke-static {v1}, Lbem;->r(Lbem;)Landroid/view/View$OnClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 677
    iget-object v0, p0, Lbey;->a:Lbem;

    invoke-static {v0}, Lbem;->c(Lbem;)Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    move-result-object v0

    iget-object v1, p0, Lbey;->a:Lbem;

    invoke-static {v1}, Lbem;->s(Lbem;)Lcal;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;->a(Lcal;)V

    .line 681
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 682
    iget-object v0, p0, Lbey;->a:Lbem;

    invoke-static {v0}, Lbem;->c(Lbem;)Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;->setText(Ljava/lang/CharSequence;)V

    .line 683
    iget-object v0, p0, Lbey;->a:Lbem;

    invoke-static {v0}, Lbem;->a(Lbem;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lbey;->a:Lbem;

    invoke-static {v1}, Lbem;->b(Lbem;)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lbem;->b(Landroid/view/View;Landroid/view/View;)V

    .line 691
    :goto_0
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p1, v0}, Lcbx;->setAlpha(F)V

    .line 692
    invoke-virtual {p1, v3}, Lcbx;->setTranslationX(F)V

    .line 693
    invoke-virtual {p1, v3}, Lcbx;->setTranslationY(F)V

    .line 694
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lbem;->a(Landroid/view/View;Z)V

    .line 695
    return-void

    .line 685
    :cond_0
    iget-object v0, p0, Lbey;->a:Lbem;

    invoke-static {v0}, Lbem;->c(Lbem;)Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/apps/moviemaker/ui/ImeDismissalReportingEditText;->setText(Ljava/lang/CharSequence;)V

    .line 686
    iget-object v0, p0, Lbey;->a:Lbem;

    invoke-static {v0}, Lbem;->a(Lbem;)Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v1, p0, Lbey;->a:Lbem;

    invoke-static {v1}, Lbem;->b(Lbem;)Landroid/view/View;

    move-result-object v1

    invoke-static {v0, v1}, Lbem;->a(Landroid/view/View;Landroid/view/View;)V

    goto :goto_0
.end method

.method public b(Lcbx;)V
    .locals 1

    .prologue
    .line 699
    invoke-virtual {p1}, Lcbx;->d()V

    .line 700
    new-instance v0, Lbfd;

    invoke-direct {v0, p0}, Lbfd;-><init>(Lbey;)V

    invoke-virtual {p1, v0}, Lcbx;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 707
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lbem;->a(Landroid/view/View;Z)V

    .line 708
    return-void
.end method

.method public c(Lcbx;)V
    .locals 1

    .prologue
    .line 712
    invoke-virtual {p1}, Lcbx;->c()V

    .line 713
    new-instance v0, Lbfe;

    invoke-direct {v0, p0}, Lbfe;-><init>(Lbey;)V

    invoke-virtual {p1, v0}, Lcbx;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 719
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lbem;->a(Landroid/view/View;Z)V

    .line 720
    return-void
.end method
