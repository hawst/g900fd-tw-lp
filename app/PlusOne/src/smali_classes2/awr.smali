.class final Lawr;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<[J",
        "Ljava/lang/Void;",
        "Ljava/lang/Exception;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lawq;


# direct methods
.method constructor <init>(Lawq;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lawr;->a:Lawq;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([[J)Ljava/lang/Exception;
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 176
    iget-object v2, p0, Lawr;->a:Lawq;

    invoke-static {v2}, Lawq;->c(Lawq;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 178
    const/4 v3, 0x0

    :try_start_0
    aget-object v3, p1, v3

    .line 179
    array-length v4, v3

    :goto_0
    if-ge v1, v4, :cond_2

    aget-wide v6, v3, v1

    .line 180
    invoke-virtual {p0}, Lawr;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 183
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    :goto_1
    return-object v0

    .line 186
    :cond_0
    :try_start_1
    iget-object v5, p0, Lawr;->a:Lawq;

    invoke-static {v5}, Lawq;->g(Lawq;)Lcff;

    move-result-object v5

    const-string v8, "httpFetcher"

    const/4 v9, 0x0

    invoke-static {v5, v8, v9}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    iget-object v5, p0, Lawr;->a:Lawq;

    invoke-static {v5}, Lawq;->h(Lawq;)Laws;

    move-result-object v5

    invoke-virtual {v5, v6, v7}, Laws;->b(J)Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v8

    cmp-long v8, v8, v10

    if-nez v8, :cond_1

    iget-object v8, p0, Lawr;->a:Lawq;

    invoke-static {v8, v6, v7}, Lawq;->a(Lawq;J)Ljava/lang/String;

    move-result-object v8

    iget-object v9, p0, Lawr;->a:Lawq;

    invoke-static {v9}, Lawq;->g(Lawq;)Lcff;

    move-result-object v9

    invoke-interface {v9, v8, v5}, Lcff;->a(Ljava/lang/String;Ljava/io/File;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v8

    const-string v9, "file should exist"

    invoke-static {v8, v9}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v8

    cmp-long v8, v8, v10

    if-nez v8, :cond_1

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Downloaded file had length 0"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 187
    :catch_0
    move-exception v0

    .line 188
    :try_start_2
    invoke-static {}, Lawq;->d()Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v3, 0x2d

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Failed downloading asset "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 189
    monitor-exit v2

    goto :goto_1

    .line 193
    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 179
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 192
    :cond_2
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method protected a(Ljava/lang/Exception;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 198
    iget-object v0, p0, Lawr;->a:Lawq;

    invoke-static {v0}, Lawq;->d(Lawq;)V

    .line 199
    if-nez p1, :cond_0

    .line 201
    iget-object v0, p0, Lawr;->a:Lawq;

    invoke-static {v0}, Lawq;->f(Lawq;)Lcfj;

    move-result-object v0

    iget-object v1, p0, Lawr;->a:Lawq;

    invoke-static {v1}, Lawq;->e(Lawq;)Lcfj;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcfj;->a(Lcfj;)Z

    .line 202
    iget-object v0, p0, Lawr;->a:Lawq;

    invoke-static {v0}, Lawq;->e(Lawq;)Lcfj;

    move-result-object v0

    invoke-virtual {v0}, Lcfj;->c()V

    .line 203
    iget-object v0, p0, Lawr;->a:Lawq;

    invoke-static {v0}, Lawq;->b(Lawq;)Lavu;

    move-result-object v0

    invoke-interface {v0, v2}, Lavu;->t(Z)V

    .line 207
    :goto_0
    return-void

    .line 205
    :cond_0
    iget-object v0, p0, Lawr;->a:Lawq;

    invoke-static {v0}, Lawq;->b(Lawq;)Lavu;

    move-result-object v0

    invoke-interface {v0, v2}, Lavu;->u(Z)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 169
    check-cast p1, [[J

    invoke-virtual {p0, p1}, Lawr;->a([[J)Ljava/lang/Exception;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 169
    check-cast p1, Ljava/lang/Exception;

    invoke-virtual {p0, p1}, Lawr;->a(Ljava/lang/Exception;)V

    return-void
.end method
