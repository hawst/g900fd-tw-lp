.class public final Lboo;
.super Lbmu;
.source "PG"


# instance fields
.field public final f:J

.field public final g:Z

.field private volatile h:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "creationTimeUs"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "rotation"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "width"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "height"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "hasAudio"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "duration"

    aput-object v2, v0, v1

    return-void
.end method

.method public constructor <init>()V
    .locals 10

    .prologue
    const-wide/16 v2, -0x1

    const/4 v4, -0x1

    .line 39
    const/4 v7, 0x0

    move-object v1, p0

    move v5, v4

    move v6, v4

    move-wide v8, v2

    invoke-direct/range {v1 .. v9}, Lboo;-><init>(JIIIZJ)V

    .line 41
    return-void
.end method

.method public constructor <init>(JIIIZJ)V
    .locals 11

    .prologue
    .line 32
    sget-object v9, Lbmv;->a:Lbmv;

    move-object v3, p0

    move-wide v4, p1

    move v6, p3

    move v7, p4

    move/from16 v8, p5

    invoke-direct/range {v3 .. v9}, Lbmu;-><init>(JIIILbmv;)V

    .line 34
    move/from16 v0, p6

    iput-boolean v0, p0, Lboo;->g:Z

    .line 35
    move-wide/from16 v0, p7

    iput-wide v0, p0, Lboo;->f:J

    .line 36
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 99
    if-ne p1, p0, :cond_1

    .line 106
    :cond_0
    :goto_0
    return v0

    .line 102
    :cond_1
    instance-of v2, p1, Lboo;

    if-nez v2, :cond_2

    move v0, v1

    .line 103
    goto :goto_0

    .line 105
    :cond_2
    check-cast p1, Lboo;

    .line 106
    iget v2, p0, Lboo;->a:I

    iget v3, p1, Lboo;->a:I

    if-ne v2, v3, :cond_3

    iget-wide v2, p0, Lboo;->b:J

    iget-wide v4, p1, Lboo;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget v2, p0, Lboo;->c:I

    iget v3, p1, Lboo;->c:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lboo;->d:I

    iget v3, p1, Lboo;->d:I

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lboo;->g:Z

    iget-boolean v3, p1, Lboo;->g:Z

    if-ne v2, v3, :cond_3

    iget-wide v2, p0, Lboo;->f:J

    iget-wide v4, p1, Lboo;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 9

    .prologue
    .line 113
    iget v0, p0, Lboo;->h:I

    if-nez v0, :cond_0

    .line 114
    iget v0, p0, Lboo;->a:I

    iget-wide v2, p0, Lboo;->b:J

    iget v1, p0, Lboo;->c:I

    iget v4, p0, Lboo;->d:I

    iget-boolean v5, p0, Lboo;->g:Z

    iget-wide v6, p0, Lboo;->f:J

    const/16 v8, 0x11

    .line 119
    invoke-static {v6, v7, v8}, Lbqh;->a(JI)I

    move-result v6

    .line 118
    invoke-static {v5, v6}, Lbqh;->a(ZI)I

    move-result v5

    .line 117
    mul-int/lit8 v5, v5, 0x1f

    add-int/2addr v4, v5

    .line 116
    mul-int/lit8 v4, v4, 0x1f

    add-int/2addr v1, v4

    .line 115
    invoke-static {v2, v3, v1}, Lbqh;->a(JI)I

    move-result v1

    .line 114
    mul-int/lit8 v1, v1, 0x1f

    add-int/2addr v0, v1

    iput v0, p0, Lboo;->h:I

    .line 121
    :cond_0
    iget v0, p0, Lboo;->h:I

    return v0
.end method
