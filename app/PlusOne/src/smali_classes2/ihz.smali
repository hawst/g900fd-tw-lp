.class final Lihz;
.super Lgdt;
.source "PG"


# instance fields
.field private synthetic a:Lihw;


# direct methods
.method constructor <init>(Lihw;)V
    .locals 0

    .prologue
    .line 182
    iput-object p1, p0, Lihz;->a:Lihw;

    invoke-direct {p0}, Lgdt;-><init>()V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 3

    .prologue
    .line 186
    const-string v0, "BaseGcoreCastClientImpl"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    const-string v1, "CastClientListener.onApplicationDisconnected error="

    .line 188
    sparse-switch p1, :sswitch_data_0

    const-string v0, "UNKNOWN_ERROR"

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 187
    :cond_0
    :goto_1
    iget-object v0, p0, Lihz;->a:Lihw;

    iget-object v0, v0, Lihw;->d:Lihs;

    invoke-interface {v0}, Lihs;->b()V

    .line 192
    return-void

    .line 188
    :sswitch_0
    const-string v0, "APPLICATION_NOT_FOUND"

    goto :goto_0

    :sswitch_1
    const-string v0, "APPLICATION_NOT_RUNNING"

    goto :goto_0

    :sswitch_2
    const-string v0, "AUTHENTICATION_FAILED"

    goto :goto_0

    :sswitch_3
    const-string v0, "CANCELED"

    goto :goto_0

    :sswitch_4
    const-string v0, "INVALID_REQUEST"

    goto :goto_0

    :sswitch_5
    const-string v0, "NETWORK_ERROR"

    goto :goto_0

    :sswitch_6
    const-string v0, "NOT_ALLOWED"

    goto :goto_0

    :sswitch_7
    const-string v0, "TIMEOUT"

    goto :goto_0

    :sswitch_8
    const-string v0, "UNKNOWN_ERROR"

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_5
        0xd -> :sswitch_8
        0xf -> :sswitch_7
        0x7d0 -> :sswitch_2
        0x7d1 -> :sswitch_4
        0x7d2 -> :sswitch_3
        0x7d3 -> :sswitch_6
        0x7d4 -> :sswitch_0
        0x7d5 -> :sswitch_1
    .end sparse-switch
.end method
