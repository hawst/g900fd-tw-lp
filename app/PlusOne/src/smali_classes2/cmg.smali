.class public final Lcmg;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Landroid/animation/TimeInterpolator;


# instance fields
.field private final b:Lcme;

.field private final c:Landroid/graphics/Path;

.field private final d:F

.field private e:F

.field private f:J

.field private g:Landroid/animation/TimeInterpolator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 389
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    sput-object v0, Lcmg;->a:Landroid/animation/TimeInterpolator;

    return-void
.end method

.method constructor <init>(Lcme;Landroid/graphics/Path;JF)V
    .locals 1

    .prologue
    .line 398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 396
    sget-object v0, Lcmg;->a:Landroid/animation/TimeInterpolator;

    iput-object v0, p0, Lcmg;->g:Landroid/animation/TimeInterpolator;

    .line 399
    iput-object p1, p0, Lcmg;->b:Lcme;

    .line 400
    iput-object p2, p0, Lcmg;->c:Landroid/graphics/Path;

    .line 401
    iput-wide p3, p0, Lcmg;->f:J

    .line 402
    iput p5, p0, Lcmg;->d:F

    .line 403
    iput p5, p0, Lcmg;->e:F

    .line 404
    return-void
.end method


# virtual methods
.method public a()Lcme;
    .locals 7

    .prologue
    .line 417
    iget-object v0, p0, Lcmg;->b:Lcme;

    iget-object v1, p0, Lcmg;->c:Landroid/graphics/Path;

    iget-wide v2, p0, Lcmg;->f:J

    iget-object v4, p0, Lcmg;->g:Landroid/animation/TimeInterpolator;

    iget v5, p0, Lcmg;->d:F

    iget v6, p0, Lcmg;->e:F

    invoke-virtual/range {v0 .. v6}, Lcme;->a(Landroid/graphics/Path;JLandroid/animation/TimeInterpolator;FF)Lcme;

    move-result-object v0

    return-object v0
.end method

.method public a(F)Lcmg;
    .locals 0

    .prologue
    .line 407
    iput p1, p0, Lcmg;->e:F

    .line 408
    return-object p0
.end method

.method public a(Landroid/animation/TimeInterpolator;)Lcmg;
    .locals 0

    .prologue
    .line 412
    iput-object p1, p0, Lcmg;->g:Landroid/animation/TimeInterpolator;

    .line 413
    return-object p0
.end method
