.class public final Lbvl;
.super Loxu;
.source "PG"


# instance fields
.field public a:[F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 725
    invoke-direct {p0}, Loxu;-><init>()V

    .line 728
    sget-object v0, Loxx;->c:[F

    iput-object v0, p0, Lbvl;->a:[F

    .line 725
    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 748
    const/4 v0, 0x0

    .line 749
    iget-object v1, p0, Lbvl;->a:[F

    if-eqz v1, :cond_0

    iget-object v1, p0, Lbvl;->a:[F

    array-length v1, v1

    if-lez v1, :cond_0

    .line 750
    iget-object v0, p0, Lbvl;->a:[F

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x4

    .line 752
    iget-object v1, p0, Lbvl;->a:[F

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 754
    :cond_0
    iput v0, p0, Lbvl;->ai:I

    .line 755
    return v0
.end method

.method public a(Loxn;)Lbvl;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 763
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 764
    sparse-switch v0, :sswitch_data_0

    .line 768
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 769
    :sswitch_0
    return-object p0

    .line 774
    :sswitch_1
    const/16 v0, 0xd

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 775
    iget-object v0, p0, Lbvl;->a:[F

    array-length v0, v0

    .line 776
    add-int/2addr v1, v0

    new-array v1, v1, [F

    .line 777
    iget-object v2, p0, Lbvl;->a:[F

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 778
    iput-object v1, p0, Lbvl;->a:[F

    .line 779
    :goto_1
    iget-object v1, p0, Lbvl;->a:[F

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    .line 780
    iget-object v1, p0, Lbvl;->a:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v2

    aput v2, v1, v0

    .line 781
    invoke-virtual {p1}, Loxn;->a()I

    .line 779
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 784
    :cond_1
    iget-object v1, p0, Lbvl;->a:[F

    invoke-virtual {p1}, Loxn;->d()F

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 764
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 739
    iget-object v0, p0, Lbvl;->a:[F

    if-eqz v0, :cond_0

    .line 740
    iget-object v1, p0, Lbvl;->a:[F

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 741
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->a(IF)V

    .line 740
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 744
    :cond_0
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 721
    invoke-virtual {p0, p1}, Lbvl;->a(Loxn;)Lbvl;

    move-result-object v0

    return-object v0
.end method
