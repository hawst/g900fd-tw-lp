.class final Lgoh;
.super Lgnm;


# instance fields
.field private final a:Lgfy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgfy",
            "<",
            "Lggf;",
            ">;"
        }
    .end annotation
.end field

.field private synthetic b:Lgny;


# direct methods
.method public constructor <init>(Lgny;Lgfy;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfy",
            "<",
            "Lggf;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lgoh;->b:Lgny;

    invoke-direct {p0}, Lgnm;-><init>()V

    iput-object p2, p0, Lgoh;->a:Lgfy;

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 5

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Bundle callback: status="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nresolution="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nbundle="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v0, 0x0

    invoke-static {p1, v0, p2}, Lgny;->a(ILjava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    iget-object v1, p0, Lgoh;->b:Lgny;

    new-instance v2, Lgoi;

    iget-object v3, p0, Lgoh;->b:Lgny;

    iget-object v4, p0, Lgoh;->a:Lgfy;

    invoke-direct {v2, v3, v4, v0}, Lgoi;-><init>(Lgny;Lgfy;Lcom/google/android/gms/common/api/Status;)V

    invoke-virtual {v1, v2}, Lgny;->b(Lghj;)V

    return-void
.end method
