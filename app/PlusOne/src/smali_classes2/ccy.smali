.class public abstract Lccy;
.super Lccv;
.source "PG"


# instance fields
.field b:Z

.field private final c:F

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:Z

.field private h:Landroid/view/VelocityTracker;

.field private i:Z

.field private j:I


# direct methods
.method public constructor <init>(Lcci;Z)V
    .locals 1

    .prologue
    .line 60
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-direct {p0, p1, p2, v0}, Lccy;-><init>(Lcci;ZF)V

    .line 61
    return-void
.end method

.method public constructor <init>(Lcci;ZF)V
    .locals 4

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lccv;-><init>(Lcci;)V

    .line 74
    const-string v0, "slopModifier"

    invoke-static {p3, v0}, Lcec;->a(FLjava/lang/CharSequence;)F

    .line 75
    invoke-interface {p1}, Lcci;->f()Landroid/content/Context;

    move-result-object v0

    .line 77
    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, p3

    float-to-int v1, v1

    iput v1, p0, Lccy;->d:I

    .line 78
    const/high16 v1, 0x3f800000    # 1.0f

    div-float/2addr v1, p3

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->atan(D)D

    move-result-wide v2

    double-to-float v1, v2

    iput v1, p0, Lccy;->c:F

    .line 79
    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v1

    iput v1, p0, Lccy;->e:I

    .line 80
    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Lccy;->f:I

    .line 81
    iput-boolean p2, p0, Lccy;->g:Z

    .line 82
    return-void
.end method

.method private a(Lccj;Z)V
    .locals 1

    .prologue
    .line 232
    iget-boolean v0, p0, Lccy;->b:Z

    if-eq v0, p2, :cond_1

    .line 233
    if-eqz p2, :cond_2

    .line 234
    iput-object p0, p1, Lccj;->g:Lcch;

    .line 238
    :cond_0
    :goto_0
    iput-boolean p2, p0, Lccy;->b:Z

    .line 240
    :cond_1
    return-void

    .line 235
    :cond_2
    iget-object v0, p1, Lccj;->g:Lcch;

    if-ne v0, p0, :cond_0

    .line 236
    const/4 v0, 0x0

    iput-object v0, p1, Lccj;->g:Lcch;

    goto :goto_0
.end method

.method private b(Lccj;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 216
    iget-boolean v0, p0, Lccy;->b:Z

    if-eqz v0, :cond_0

    .line 217
    invoke-direct {p0, p1, v1}, Lccy;->a(Lccj;Z)V

    .line 218
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v1, v0}, Lccy;->a(Lccj;IZ)V

    .line 220
    :cond_0
    iput-boolean v1, p0, Lccy;->i:Z

    .line 221
    iget-object v0, p0, Lccy;->h:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_1

    .line 222
    iget-object v0, p0, Lccy;->h:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->recycle()V

    .line 223
    const/4 v0, 0x0

    iput-object v0, p0, Lccy;->h:Landroid/view/VelocityTracker;

    .line 225
    :cond_1
    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method protected abstract a(Lccj;IZ)V
.end method

.method public final a(Lccj;Landroid/view/MotionEvent;)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 90
    iget-boolean v0, p0, Lccy;->g:Z

    iget-boolean v4, p1, Lccj;->a:Z

    xor-int/2addr v0, v4

    if-eqz v0, :cond_2

    .line 91
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    :goto_0
    float-to-int v0, v0

    .line 93
    iget-object v4, p0, Lccy;->h:Landroid/view/VelocityTracker;

    if-eqz v4, :cond_0

    .line 94
    iget-object v4, p0, Lccy;->h:Landroid/view/VelocityTracker;

    invoke-virtual {v4, p2}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 99
    :cond_0
    iget-object v4, p1, Lccj;->g:Lcch;

    if-eqz v4, :cond_3

    iget-object v4, p1, Lccj;->g:Lcch;

    if-eq v4, p0, :cond_3

    .line 101
    invoke-direct {p0, p1}, Lccy;->b(Lccj;)V

    .line 166
    :cond_1
    :goto_1
    return v2

    .line 91
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    goto :goto_0

    .line 105
    :cond_3
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    goto :goto_1

    .line 107
    :pswitch_0
    iget-boolean v3, p0, Lccy;->i:Z

    if-nez v3, :cond_1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {p0, p1, v3, v4}, Lccy;->b(Lccj;II)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 108
    iput v0, p0, Lccy;->j:I

    .line 109
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Lccy;->h:Landroid/view/VelocityTracker;

    .line 110
    iput-boolean v1, p0, Lccy;->i:Z

    goto :goto_1

    .line 115
    :pswitch_1
    iget-boolean v3, p0, Lccy;->i:Z

    if-eqz v3, :cond_1

    .line 116
    iget v3, p0, Lccy;->j:I

    sub-int v3, v0, v3

    .line 117
    iget-boolean v4, p0, Lccy;->b:Z

    if-nez v4, :cond_4

    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v3

    iget v4, p0, Lccy;->d:I

    if-le v3, v4, :cond_4

    .line 118
    invoke-virtual {p0}, Lccy;->a()V

    .line 119
    invoke-direct {p0, p1, v1}, Lccy;->a(Lccj;Z)V

    .line 121
    :cond_4
    iget-boolean v3, p0, Lccy;->b:Z

    if-eqz v3, :cond_a

    .line 122
    iget v2, p0, Lccy;->j:I

    sub-int v2, v0, v2

    invoke-virtual {p0, p1, v2}, Lccy;->c(Lccj;I)V

    .line 123
    iput v0, p0, Lccy;->j:I

    move v0, v1

    :goto_2
    move v2, v0

    .line 126
    goto :goto_1

    .line 130
    :pswitch_2
    iget-boolean v0, p0, Lccy;->i:Z

    if-eqz v0, :cond_1

    .line 131
    iget-boolean v5, p0, Lccy;->b:Z

    .line 134
    iget-object v0, p0, Lccy;->h:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    iget v4, p0, Lccy;->f:I

    int-to-float v4, v4

    invoke-virtual {v0, v1, v4}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 135
    iget-object v0, p0, Lccy;->h:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v0

    .line 136
    iget-object v1, p0, Lccy;->h:Landroid/view/VelocityTracker;

    invoke-virtual {v1}, Landroid/view/VelocityTracker;->getYVelocity()F

    move-result v1

    .line 137
    iget-object v4, p0, Lccy;->h:Landroid/view/VelocityTracker;

    invoke-virtual {v4}, Landroid/view/VelocityTracker;->recycle()V

    .line 138
    const/4 v4, 0x0

    iput-object v4, p0, Lccy;->h:Landroid/view/VelocityTracker;

    .line 140
    iget-boolean v4, p0, Lccy;->g:Z

    iget-boolean v6, p1, Lccj;->a:Z

    xor-int/2addr v6, v4

    .line 141
    if-eqz v6, :cond_6

    move v4, v1

    .line 142
    :goto_3
    if-eqz v6, :cond_7

    .line 143
    :goto_4
    cmpl-float v1, v4, v3

    if-eqz v1, :cond_8

    div-float/2addr v0, v4

    float-to-double v0, v0

    .line 144
    invoke-static {v0, v1}, Ljava/lang/Math;->atan(D)D

    move-result-wide v0

    double-to-float v0, v0

    .line 148
    :goto_5
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v1

    iget v3, p0, Lccy;->e:I

    int-to-float v3, v3

    cmpl-float v1, v1, v3

    if-lez v1, :cond_9

    .line 149
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lccy;->c:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_9

    .line 150
    float-to-int v0, v4

    invoke-virtual {p0, p1, v0}, Lccy;->b(Lccj;I)V

    .line 155
    :cond_5
    :goto_6
    invoke-direct {p0, p1, v2}, Lccy;->a(Lccj;Z)V

    .line 156
    iget-boolean v0, p0, Lccy;->b:Z

    or-int/2addr v0, v5

    .line 157
    iput-boolean v2, p0, Lccy;->i:Z

    move v2, v0

    .line 158
    goto/16 :goto_1

    :cond_6
    move v4, v0

    .line 141
    goto :goto_3

    :cond_7
    move v0, v1

    .line 142
    goto :goto_4

    :cond_8
    move v0, v3

    .line 144
    goto :goto_5

    .line 151
    :cond_9
    if-eqz v5, :cond_5

    .line 152
    float-to-int v0, v4

    invoke-virtual {p0, p1, v0, v2}, Lccy;->a(Lccj;IZ)V

    goto :goto_6

    .line 162
    :pswitch_3
    invoke-direct {p0, p1}, Lccy;->b(Lccj;)V

    goto/16 :goto_1

    :cond_a
    move v0, v2

    goto :goto_2

    .line 105
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method protected abstract b(Lccj;I)V
.end method

.method protected abstract b(Lccj;II)Z
.end method

.method protected abstract c(Lccj;I)V
.end method
