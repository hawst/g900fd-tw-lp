.class public final enum Layc;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Layc;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Layc;

.field public static final enum b:Layc;

.field public static final enum c:Layc;

.field public static final enum d:Layc;

.field public static final enum e:Layc;

.field private static final synthetic i:[Layc;


# instance fields
.field private final f:I

.field private final g:F

.field private final h:F


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 7
    new-instance v0, Layc;

    const-string v1, "NO_BLUR"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Layc;-><init>(Ljava/lang/String;IIFF)V

    sput-object v0, Layc;->a:Layc;

    .line 8
    new-instance v0, Layc;

    const-string v1, "SOFT_SKIN"

    const/4 v2, 0x1

    const/4 v3, 0x7

    const/high16 v4, 0x40400000    # 3.0f

    const v5, 0x3e4ccccd    # 0.2f

    invoke-direct/range {v0 .. v5}, Layc;-><init>(Ljava/lang/String;IIFF)V

    sput-object v0, Layc;->b:Layc;

    .line 9
    new-instance v0, Layc;

    const-string v1, "SOFT_EDGES"

    const/4 v2, 0x2

    const/16 v3, 0x8

    const/high16 v4, 0x40800000    # 4.0f

    const v5, 0x3e99999a    # 0.3f

    invoke-direct/range {v0 .. v5}, Layc;-><init>(Ljava/lang/String;IIFF)V

    sput-object v0, Layc;->c:Layc;

    .line 10
    new-instance v0, Layc;

    const-string v1, "BLUR_OUT"

    const/4 v2, 0x3

    const/16 v3, 0x8

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct/range {v0 .. v5}, Layc;-><init>(Ljava/lang/String;IIFF)V

    sput-object v0, Layc;->d:Layc;

    .line 12
    new-instance v0, Layc;

    const-string v1, "SHARPEN"

    const/4 v2, 0x4

    const/4 v3, 0x3

    const/high16 v4, 0x40800000    # 4.0f

    const v5, -0x41666666    # -0.3f

    invoke-direct/range {v0 .. v5}, Layc;-><init>(Ljava/lang/String;IIFF)V

    sput-object v0, Layc;->e:Layc;

    .line 6
    const/4 v0, 0x5

    new-array v0, v0, [Layc;

    const/4 v1, 0x0

    sget-object v2, Layc;->a:Layc;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Layc;->b:Layc;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Layc;->c:Layc;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Layc;->d:Layc;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Layc;->e:Layc;

    aput-object v2, v0, v1

    sput-object v0, Layc;->i:[Layc;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIFF)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IFF)V"
        }
    .end annotation

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 19
    iput p3, p0, Layc;->f:I

    .line 20
    iput p4, p0, Layc;->g:F

    .line 21
    iput p5, p0, Layc;->h:F

    .line 22
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Layc;
    .locals 1

    .prologue
    .line 6
    const-class v0, Layc;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Layc;

    return-object v0
.end method

.method public static values()[Layc;
    .locals 1

    .prologue
    .line 6
    sget-object v0, Layc;->i:[Layc;

    invoke-virtual {v0}, [Layc;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Layc;

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Layc;->f:I

    return v0
.end method

.method public b()F
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Layc;->h:F

    return v0
.end method

.method public c()F
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Layc;->g:F

    return v0
.end method
