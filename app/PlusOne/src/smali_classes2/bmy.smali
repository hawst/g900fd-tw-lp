.class public final Lbmy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lbms;

.field private final b:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Lbxi;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<[",
            "Lbbo;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lbbs;

.field private final e:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<[J>;"
        }
    .end annotation
.end field

.field private final f:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<[J>;"
        }
    .end annotation
.end field


# direct methods
.method protected constructor <init>(Landroid/util/LongSparseArray;Landroid/util/LongSparseArray;Lbbs;Lbms;Landroid/util/LongSparseArray;Landroid/util/LongSparseArray;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Lbxi;",
            ">;",
            "Landroid/util/LongSparseArray",
            "<[",
            "Lbbo;",
            ">;",
            "Lbbs;",
            "Lbms;",
            "Landroid/util/LongSparseArray",
            "<[J>;",
            "Landroid/util/LongSparseArray",
            "<[J>;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const-string v0, "stabilizationMetrics"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/LongSparseArray;

    iput-object v0, p0, Lbmy;->b:Landroid/util/LongSparseArray;

    .line 43
    const-string v0, "faceLocations"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/LongSparseArray;

    iput-object v0, p0, Lbmy;->c:Landroid/util/LongSparseArray;

    .line 44
    iput-object p3, p0, Lbmy;->d:Lbbs;

    .line 45
    const-string v0, "keyFrameMetrics"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbms;

    iput-object v0, p0, Lbmy;->a:Lbms;

    .line 46
    const-string v0, "saliencyMapVerticalProjection"

    invoke-static {p5, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/LongSparseArray;

    iput-object v0, p0, Lbmy;->e:Landroid/util/LongSparseArray;

    .line 48
    const-string v0, "saliencyMapHorizontalProjection"

    invoke-static {p6, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/LongSparseArray;

    iput-object v0, p0, Lbmy;->f:Landroid/util/LongSparseArray;

    .line 50
    return-void
.end method

.method public static a(Lbkr;)Lbmy;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 91
    const-string v0, "videoMetrics"

    invoke-static {p0, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 93
    invoke-interface {p0}, Lbkr;->d()Lbku;

    move-result-object v0

    const-string v1, "videoMetrics.getPointMetrics()"

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lbku;

    .line 95
    invoke-interface {p0}, Lbkr;->c()Lbkn;

    move-result-object v0

    const-string v1, "generalMetrics.getGeneralMetrics()"

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Lbkn;

    .line 100
    invoke-static {v4}, Lbkx;->a(Lbku;)Lbmz;

    move-result-object v0

    .line 102
    if-nez v0, :cond_0

    .line 103
    invoke-static {}, Lcfh;->a()Lcfh;

    move-result-object v1

    .line 109
    :goto_0
    sget-object v0, Lbmz;->g:Lbmz;

    .line 110
    invoke-virtual {v4, v0}, Lbku;->b(Lbmz;)Landroid/util/LongSparseArray;

    move-result-object v2

    .line 112
    sget-object v0, Lbmz;->v:Lbmz;

    .line 113
    invoke-virtual {v4, v0}, Lbku;->b(Lbmz;)Landroid/util/LongSparseArray;

    move-result-object v5

    .line 116
    sget-object v0, Lbmz;->w:Lbmz;

    .line 117
    invoke-virtual {v4, v0}, Lbku;->b(Lbmz;)Landroid/util/LongSparseArray;

    move-result-object v6

    .line 120
    new-instance v0, Lbmy;

    .line 123
    invoke-virtual {v3}, Lbkn;->d()Lbbs;

    move-result-object v3

    .line 124
    invoke-virtual {v4}, Lbku;->d()Lbms;

    move-result-object v4

    invoke-direct/range {v0 .. v6}, Lbmy;-><init>(Landroid/util/LongSparseArray;Landroid/util/LongSparseArray;Lbbs;Lbms;Landroid/util/LongSparseArray;Landroid/util/LongSparseArray;)V

    return-object v0

    .line 105
    :cond_0
    invoke-virtual {v4, v0}, Lbku;->b(Lbmz;)Landroid/util/LongSparseArray;

    move-result-object v1

    goto :goto_0
.end method


# virtual methods
.method public a()Lbms;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lbmy;->a:Lbms;

    return-object v0
.end method

.method public b()Landroid/util/LongSparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LongSparseArray",
            "<",
            "Lbxi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    iget-object v0, p0, Lbmy;->b:Landroid/util/LongSparseArray;

    return-object v0
.end method

.method public c()Landroid/util/LongSparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LongSparseArray",
            "<[",
            "Lbbo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 65
    iget-object v0, p0, Lbmy;->c:Landroid/util/LongSparseArray;

    return-object v0
.end method

.method public d()Lbbs;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lbmy;->d:Lbbs;

    return-object v0
.end method

.method public e()Landroid/util/LongSparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LongSparseArray",
            "<[J>;"
        }
    .end annotation

    .prologue
    .line 76
    iget-object v0, p0, Lbmy;->e:Landroid/util/LongSparseArray;

    return-object v0
.end method

.method public f()Landroid/util/LongSparseArray;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Landroid/util/LongSparseArray",
            "<[J>;"
        }
    .end annotation

    .prologue
    .line 82
    iget-object v0, p0, Lbmy;->f:Landroid/util/LongSparseArray;

    return-object v0
.end method
