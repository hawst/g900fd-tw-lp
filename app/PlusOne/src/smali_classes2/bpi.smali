.class public final Lbpi;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lbuu;)Lbpk;
    .locals 5

    .prologue
    .line 133
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lbuu;->b:[Lbty;

    array-length v0, v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 134
    iget-object v2, p0, Lbuu;->b:[Lbty;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 135
    invoke-static {v4}, Lbpm;->a(Lbty;)Lbmd;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 134
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 137
    :cond_0
    new-instance v0, Lbpk;

    iget v2, p0, Lbuu;->a:I

    invoke-direct {v0, v2, v1}, Lbpk;-><init>(ILjava/util/Collection;)V

    return-object v0
.end method

.method public static a(Lbor;Ljava/util/List;Ljava/util/List;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbor;",
            "Ljava/util/List",
            "<",
            "Lbph;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 76
    const-string v0, "resultSet"

    invoke-static {p0, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 77
    const-string v0, "userEdits"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 78
    const-string v0, "clipsBeforeUserEdits"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 80
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 81
    invoke-static {p0, p2, p1}, Lbpj;->a(Lbor;Ljava/util/List;Ljava/util/List;)V

    .line 85
    :cond_0
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
