.class public final Lbuf;
.super Loxu;
.source "PG"


# static fields
.field public static final a:[Lbuf;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 8
    const/4 v0, 0x0

    new-array v0, v0, [Lbuf;

    sput-object v0, Lbuf;->a:[Lbuf;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Loxu;-><init>()V

    .line 12
    const-string v0, ""

    iput-object v0, p0, Lbuf;->b:Ljava/lang/String;

    .line 15
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbuf;->c:Z

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 37
    const/4 v0, 0x0

    .line 38
    iget-object v1, p0, Lbuf;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 39
    iget-object v0, p0, Lbuf;->b:Ljava/lang/String;

    .line 40
    invoke-static {v3, v0}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 42
    :cond_0
    iget-boolean v1, p0, Lbuf;->c:Z

    if-eq v1, v3, :cond_1

    .line 43
    const/4 v1, 0x2

    iget-boolean v2, p0, Lbuf;->c:Z

    .line 44
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 46
    :cond_1
    iput v0, p0, Lbuf;->ai:I

    .line 47
    return v0
.end method

.method public a(Loxn;)Lbuf;
    .locals 1

    .prologue
    .line 55
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 56
    sparse-switch v0, :sswitch_data_0

    .line 60
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    :sswitch_0
    return-object p0

    .line 66
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbuf;->b:Ljava/lang/String;

    goto :goto_0

    .line 70
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    iput-boolean v0, p0, Lbuf;->c:Z

    goto :goto_0

    .line 56
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 27
    iget-object v0, p0, Lbuf;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 28
    iget-object v0, p0, Lbuf;->b:Ljava/lang/String;

    invoke-virtual {p1, v2, v0}, Loxo;->a(ILjava/lang/String;)V

    .line 30
    :cond_0
    iget-boolean v0, p0, Lbuf;->c:Z

    if-eq v0, v2, :cond_1

    .line 31
    const/4 v0, 0x2

    iget-boolean v1, p0, Lbuf;->c:Z

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 33
    :cond_1
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lbuf;->a(Loxn;)Lbuf;

    move-result-object v0

    return-object v0
.end method
