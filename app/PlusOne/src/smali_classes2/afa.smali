.class final Lafa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field private synthetic a:Laez;


# direct methods
.method constructor <init>(Laez;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lafa;->a:Laez;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 172
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "service connected, binder: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 174
    :try_start_0
    invoke-interface {p2}, Landroid/os/IBinder;->getInterfaceDescriptor()Ljava/lang/String;

    move-result-object v0

    .line 177
    const-string v1, "com.google.android.gms.analytics.internal.IAnalyticsService"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lafa;->a:Laez;

    invoke-static {p2}, Lgcy;->a(Landroid/os/IBinder;)Lgcx;

    move-result-object v1

    iput-object v1, v0, Laez;->e:Lgcx;

    .line 181
    iget-object v0, p0, Lafa;->a:Laez;

    iget-object v0, v0, Laez;->b:Lafb;

    invoke-interface {v0}, Lafb;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    :goto_0
    return-void

    :catch_0
    move-exception v0

    .line 189
    :cond_0
    iget-object v0, p0, Lafa;->a:Laez;

    iget-object v0, v0, Laez;->d:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 190
    iget-object v0, p0, Lafa;->a:Laez;

    const/4 v1, 0x0

    iput-object v1, v0, Laez;->a:Landroid/content/ServiceConnection;

    .line 191
    iget-object v0, p0, Lafa;->a:Laez;

    iget-object v0, v0, Laez;->c:Lafc;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lafc;->a(I)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 196
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "service disconnected: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 197
    iget-object v0, p0, Lafa;->a:Laez;

    const/4 v1, 0x0

    iput-object v1, v0, Laez;->a:Landroid/content/ServiceConnection;

    .line 198
    iget-object v0, p0, Lafa;->a:Laez;

    iget-object v0, v0, Laez;->b:Lafb;

    invoke-interface {v0}, Lafb;->b()V

    .line 199
    return-void
.end method
