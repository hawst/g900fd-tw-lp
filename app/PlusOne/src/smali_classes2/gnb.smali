.class final Lgnb;
.super Lgne;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lgne",
        "<",
        "Lgyq;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Landroid/net/Uri;

.field private synthetic e:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Landroid/net/Uri;Landroid/os/Bundle;)V
    .locals 0

    iput-object p1, p0, Lgnb;->a:Landroid/net/Uri;

    iput-object p2, p0, Lgnb;->e:Landroid/os/Bundle;

    invoke-direct {p0}, Lgne;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/google/android/gms/common/api/Status;)Lgyq;
    .locals 3

    new-instance v0, Lgmt;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lgmt;-><init>(Lcom/google/android/gms/common/api/Status;Landroid/content/Intent;I)V

    return-object v0
.end method

.method protected a(Landroid/content/Context;Lgmx;)V
    .locals 5

    const/4 v4, 0x1

    new-instance v0, Lgnd;

    invoke-direct {v0, p0}, Lgnd;-><init>(Lgfy;)V

    iget-object v1, p0, Lgnb;->a:Landroid/net/Uri;

    iget-object v2, p0, Lgnb;->e:Landroid/os/Bundle;

    const-string v3, "com.google.android.gms"

    invoke-virtual {p1, v3, v1, v4}, Landroid/content/Context;->grantUriPermission(Ljava/lang/String;Landroid/net/Uri;I)V

    new-instance v3, Lgnc;

    invoke-direct {v3, p1, v1, v0}, Lgnc;-><init>(Landroid/content/Context;Landroid/net/Uri;Lgmu;)V

    const/4 v0, 0x1

    :try_start_0
    invoke-interface {p2, v3, v1, v2, v0}, Lgmx;->a(Lgmu;Landroid/net/Uri;Landroid/os/Bundle;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {p1, v1, v4}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    throw v0

    :catch_1
    move-exception v0

    invoke-virtual {p1, v1, v4}, Landroid/content/Context;->revokeUriPermission(Landroid/net/Uri;I)V

    throw v0
.end method

.method protected synthetic b(Lcom/google/android/gms/common/api/Status;)Lggf;
    .locals 1

    invoke-virtual {p0, p1}, Lgnb;->a(Lcom/google/android/gms/common/api/Status;)Lgyq;

    move-result-object v0

    return-object v0
.end method
