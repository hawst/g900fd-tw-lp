.class public final Laky;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lajw;


# instance fields
.field private final b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Laky;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-boolean p1, p0, Laky;->b:Z

    .line 81
    return-void
.end method

.method static a(Landroid/util/LongSparseArray;IZ)Landroid/util/LongSparseArray;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;IZ)",
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 222
    new-instance v3, Landroid/util/LongSparseArray;

    invoke-direct {v3}, Landroid/util/LongSparseArray;-><init>()V

    .line 224
    invoke-virtual {p0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-le p1, v0, :cond_0

    move-object v0, v3

    .line 257
    :goto_0
    return-object v0

    .line 228
    :cond_0
    div-int/lit8 v4, p1, 0x2

    .line 229
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 231
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    add-int/lit8 v0, p1, -0x1

    if-ge v1, v0, :cond_1

    .line 232
    invoke-virtual {p0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 231
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 235
    :cond_1
    add-int/lit8 v0, p1, -0x1

    move v2, v0

    :goto_2
    invoke-virtual {p0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_3

    .line 236
    invoke-virtual {p0, v2}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 238
    sub-int v0, v2, v4

    .line 239
    invoke-virtual {p0, v0}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v6

    .line 240
    invoke-virtual {p0, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    .line 242
    invoke-static {v5}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 243
    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Float;

    .line 247
    if-eqz p2, :cond_2

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v8

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v9

    cmpl-float v8, v8, v9

    if-lez v8, :cond_2

    .line 248
    invoke-virtual {v3, v6, v7, v0}, Landroid/util/LongSparseArray;->append(JLjava/lang/Object;)V

    .line 254
    :goto_3
    sub-int v0, v2, p1

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 235
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 250
    :cond_2
    invoke-virtual {v3, v6, v7, v1}, Landroid/util/LongSparseArray;->append(JLjava/lang/Object;)V

    goto :goto_3

    :cond_3
    move-object v0, v3

    .line 257
    goto :goto_0
.end method

.method private static a([JLjava/util/List;)Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([J",
            "Ljava/util/List",
            "<",
            "Lakz;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lbmp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 431
    invoke-static {p1}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 432
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 433
    array-length v0, p0

    new-array v3, v0, [Z

    .line 435
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakz;

    .line 436
    invoke-virtual {v0}, Lakz;->a()I

    move-result v1

    aget-boolean v1, v3, v1

    if-nez v1, :cond_0

    .line 438
    invoke-virtual {v0}, Lakz;->a()I

    move-result v1

    aget-wide v6, p0, v1

    .line 439
    invoke-virtual {v0}, Lakz;->b()I

    move-result v1

    aget-wide v8, p0, v1

    .line 440
    new-instance v1, Lbmp;

    invoke-direct {v1, v6, v7, v8, v9}, Lbmp;-><init>(JJ)V

    .line 445
    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 448
    const-wide/32 v10, 0x2dc6c0

    sub-long/2addr v6, v10

    const-wide/32 v10, 0x7a120

    sub-long/2addr v6, v10

    .line 449
    invoke-virtual {v0}, Lakz;->b()I

    move-result v1

    .line 450
    :goto_0
    if-ltz v1, :cond_1

    aget-wide v10, p0, v1

    cmp-long v5, v10, v6

    if-ltz v5, :cond_1

    .line 451
    const/4 v5, 0x1

    aput-boolean v5, v3, v1

    .line 450
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 453
    :cond_1
    const-wide/32 v6, 0x7a120

    add-long/2addr v6, v8

    .line 454
    invoke-virtual {v0}, Lakz;->b()I

    move-result v0

    .line 455
    :goto_1
    array-length v1, p0

    if-ge v0, v1, :cond_0

    aget-wide v8, p0, v0

    cmp-long v1, v8, v6

    if-gtz v1, :cond_0

    .line 456
    const/4 v1, 0x1

    aput-boolean v1, v3, v0

    .line 455
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 460
    :cond_2
    return-object v2
.end method

.method static a([J[JJLjava/util/List;)Ljava/util/List;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([J[JJ",
            "Ljava/util/List",
            "<",
            "Lbmp;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lbmp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 480
    new-instance v0, Lbmp;

    const/4 v1, 0x0

    aget-wide v2, p1, v1

    invoke-direct {v0, v2, v3, p2, p3}, Lbmp;-><init>(JJ)V

    .line 481
    invoke-static {v0, p4}, Lbpx;->a(Lbmp;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v1

    .line 484
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 486
    const/4 v0, 0x0

    .line 487
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmp;

    move v2, v1

    .line 490
    :goto_0
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    if-ge v2, v1, :cond_1

    aget-wide v6, p1, v2

    iget-wide v8, v0, Lbmp;->b:J

    cmp-long v1, v6, v8

    if-gez v1, :cond_1

    .line 492
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_1
    move v1, v2

    .line 495
    :goto_1
    array-length v3, p1

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_2

    aget-wide v6, p1, v1

    iget-wide v8, v0, Lbmp;->c:J

    cmp-long v3, v6, v8

    if-gez v3, :cond_2

    .line 497
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_2
    move v3, v2

    move v0, v2

    .line 502
    :goto_2
    if-ge v3, v1, :cond_0

    if-ge v0, v1, :cond_0

    move v2, v0

    .line 505
    :goto_3
    if-ge v2, v1, :cond_3

    aget-wide v6, p1, v2

    aget-wide v8, p1, v3

    sub-long/2addr v6, v8

    const-wide/32 v8, 0x2dc6c0

    cmp-long v0, v6, v8

    if-gez v0, :cond_3

    .line 507
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    .line 510
    :cond_3
    aget-wide v6, p1, v2

    aget-wide v8, p1, v3

    sub-long/2addr v6, v8

    const-wide/32 v8, 0xf4240

    cmp-long v0, v6, v8

    if-ltz v0, :cond_5

    const/4 v0, 0x1

    .line 512
    :goto_4
    if-eqz v0, :cond_4

    .line 513
    aget-wide v6, p0, v2

    aget-wide v8, p0, v3

    sub-long/2addr v6, v8

    long-to-float v0, v6

    sub-int v6, v2, v3

    int-to-float v6, v6

    div-float/2addr v0, v6

    .line 515
    new-instance v6, Lakz;

    invoke-direct {v6, v0, v3, v2}, Lakz;-><init>(FII)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 502
    :cond_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v0, v2

    goto :goto_2

    .line 510
    :cond_5
    const/4 v0, 0x0

    goto :goto_4

    .line 520
    :cond_6
    invoke-static {p1, v4}, Laky;->a([JLjava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 524
    return-object v0
.end method

.method private a(Lbku;Lbkq;Ljava/util/List;I)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbku;",
            "Lbkq;",
            "Ljava/util/List",
            "<",
            "Lbmp;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 129
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 130
    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmp;

    .line 136
    new-instance v3, Lbmr;

    .line 138
    invoke-static {p1, v0}, Lbky;->a(Lbku;Lbmp;)Lbky;

    move-result-object v4

    invoke-direct {v3, v0, v4, p4}, Lbmr;-><init>(Lbmp;Lbky;I)V

    .line 140
    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 142
    :cond_0
    invoke-virtual {p2, p4, v1}, Lbkq;->a(ILjava/util/List;)Lbkq;

    .line 144
    return-void
.end method

.method private static a(Ljava/util/Map;Lbku;Lbmz;Lcfz;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lbmz;",
            "Lcfv",
            "<",
            "Ljava/lang/Object;",
            ">;>;",
            "Lbku;",
            "Lbmz;",
            "Lcfz",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 197
    invoke-virtual {p1, p2}, Lbku;->b(Lbmz;)Landroid/util/LongSparseArray;

    move-result-object v0

    .line 204
    new-instance v1, Lcfv;

    invoke-direct {v1, v0, p3, p4}, Lcfv;-><init>(Landroid/util/LongSparseArray;Lcfz;Ljava/lang/Object;)V

    .line 207
    invoke-interface {p0, p2, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    return-void
.end method

.method private static a(Ljava/util/Map;Lbku;Lcfz;Ljava/lang/Object;)V
    .locals 18
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Lbmz;",
            "Lcfv",
            "<",
            "Ljava/lang/Object;",
            ">;>;",
            "Lbku;",
            "Lcfz",
            "<",
            "Ljava/lang/Object;",
            ">;",
            "Ljava/lang/Object;",
            ")V"
        }
    .end annotation

    .prologue
    .line 317
    sget-object v5, Lbmz;->t:Lbmz;

    .line 321
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Lbku;->b(Lbmz;)Landroid/util/LongSparseArray;

    move-result-object v6

    .line 327
    const/4 v2, 0x5

    const/4 v3, 0x0

    .line 328
    invoke-static {v6, v2, v3}, Laky;->a(Landroid/util/LongSparseArray;IZ)Landroid/util/LongSparseArray;

    move-result-object v7

    .line 331
    const/4 v2, 0x5

    const/4 v3, 0x1

    .line 332
    invoke-static {v6, v2, v3}, Laky;->a(Landroid/util/LongSparseArray;IZ)Landroid/util/LongSparseArray;

    move-result-object v8

    .line 335
    new-instance v9, Landroid/util/LongSparseArray;

    invoke-direct {v9}, Landroid/util/LongSparseArray;-><init>()V

    new-instance v10, Ljava/util/ArrayDeque;

    invoke-direct {v10}, Ljava/util/ArrayDeque;-><init>()V

    const/4 v3, 0x0

    const/4 v2, 0x0

    move v4, v3

    move v3, v2

    :goto_0
    invoke-virtual {v7}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    if-ge v3, v2, :cond_3

    invoke-virtual {v7, v3}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v12

    :goto_1
    invoke-virtual {v10}, Ljava/util/ArrayDeque;->size()I

    move-result v2

    if-lez v2, :cond_1

    invoke-virtual {v10}, Ljava/util/ArrayDeque;->peekFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v2, v3, :cond_1

    invoke-virtual {v10}, Ljava/util/ArrayDeque;->removeFirst()Ljava/lang/Object;

    goto :goto_1

    :cond_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v10, v2}, Ljava/util/ArrayDeque;->addLast(Ljava/lang/Object;)V

    add-int/lit8 v4, v4, 0x1

    :cond_1
    invoke-virtual {v7}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    if-ge v4, v2, :cond_2

    invoke-virtual {v7, v4}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v14

    const-wide/32 v16, 0xb71b0

    add-long v16, v16, v12

    cmp-long v2, v14, v16

    if-gez v2, :cond_2

    invoke-virtual {v7, v4}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v11

    :goto_2
    invoke-virtual {v10}, Ljava/util/ArrayDeque;->size()I

    move-result v2

    if-lez v2, :cond_0

    invoke-virtual {v10}, Ljava/util/ArrayDeque;->peekLast()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v7, v2}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    cmpg-float v2, v2, v11

    if-gez v2, :cond_0

    invoke-virtual {v10}, Ljava/util/ArrayDeque;->removeLast()Ljava/lang/Object;

    goto :goto_2

    :cond_2
    invoke-virtual {v10}, Ljava/util/ArrayDeque;->peekFirst()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v7, v2}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v9, v12, v13, v2}, Landroid/util/LongSparseArray;->append(JLjava/lang/Object;)V

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto/16 :goto_0

    .line 337
    :cond_3
    new-instance v4, Landroid/util/LongSparseArray;

    invoke-direct {v4}, Landroid/util/LongSparseArray;-><init>()V

    .line 338
    const/4 v2, 0x0

    move v3, v2

    :goto_3
    invoke-virtual {v7}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    if-ge v3, v2, :cond_5

    .line 339
    invoke-virtual {v7, v3}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v10

    .line 341
    invoke-virtual {v8, v10, v11}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v12

    .line 344
    const v2, 0x3e4ccccd    # 0.2f

    cmpl-float v2, v12, v2

    if-lez v2, :cond_4

    const/4 v2, 0x0

    .line 347
    :goto_4
    add-float/2addr v2, v12

    .line 348
    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v4, v10, v11, v2}, Landroid/util/LongSparseArray;->append(JLjava/lang/Object;)V

    .line 338
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_3

    .line 345
    :cond_4
    invoke-virtual {v9, v10, v11}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v13

    invoke-virtual {v7, v10, v11}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    sub-float v2, v13, v2

    goto :goto_4

    .line 351
    :cond_5
    invoke-virtual {v7}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    if-nez v2, :cond_6

    new-instance v2, Lcfv;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-direct {v2, v6, v0, v1}, Lcfv;-><init>(Landroid/util/LongSparseArray;Lcfz;Ljava/lang/Object;)V

    .line 354
    :goto_5
    move-object/from16 v0, p0

    invoke-interface {v0, v5, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 355
    return-void

    .line 351
    :cond_6
    new-instance v2, Lcfv;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-direct {v2, v4, v0, v1}, Lcfv;-><init>(Landroid/util/LongSparseArray;Lcfz;Ljava/lang/Object;)V

    goto :goto_5
.end method


# virtual methods
.method public a(Lbkn;Lbku;Lbkq;)V
    .locals 22

    .prologue
    .line 90
    invoke-virtual/range {p3 .. p3}, Lbkq;->a()Ljava/util/List;

    move-result-object v10

    .line 95
    invoke-virtual/range {p2 .. p2}, Lbku;->b()[J

    move-result-object v11

    array-length v4, v11

    if-nez v4, :cond_1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v4

    .line 101
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v5, v0, Laky;->b:Z

    if-eqz v5, :cond_0

    .line 103
    invoke-interface {v10, v4}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 104
    new-instance v5, Lbmp;

    const-wide/16 v6, 0x0

    .line 105
    invoke-virtual/range {p1 .. p1}, Lbkn;->c()J

    move-result-wide v8

    invoke-direct {v5, v6, v7, v8, v9}, Lbmp;-><init>(JJ)V

    .line 104
    invoke-static {v5, v10}, Lbpx;->a(Lbmp;Ljava/util/Collection;)Ljava/util/List;

    move-result-object v5

    .line 111
    const/4 v6, 0x6

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2, v5, v6}, Laky;->a(Lbku;Lbkq;Ljava/util/List;I)V

    .line 114
    :cond_0
    const/4 v5, 0x2

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v0, v1, v2, v4, v5}, Laky;->a(Lbku;Lbkq;Ljava/util/List;I)V

    .line 116
    return-void

    .line 95
    :cond_1
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    new-instance v13, Ljava/util/HashMap;

    invoke-direct {v13}, Ljava/util/HashMap;-><init>()V

    sget-object v4, Lbmz;->g:Lbmz;

    const/high16 v5, 0x40400000    # 3.0f

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-interface {v13, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lbmz;->f:Lbmz;

    const/high16 v5, 0x41200000    # 10.0f

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-interface {v13, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v4, Lbmz;->l:Lbmz;

    const/high16 v5, 0x40a00000    # 5.0f

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    invoke-interface {v13, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p2 .. p2}, Lbkx;->b(Lbku;)Lbmz;

    move-result-object v5

    sget-object v4, Lbmz;->a:Lbmz;

    if-ne v5, v4, :cond_2

    const/4 v4, 0x0

    const/4 v6, 0x0

    const/high16 v7, 0x3f800000    # 1.0f

    invoke-static {v4, v6, v7}, Lbxi;->a(FFF)Lbxi;

    move-result-object v4

    :goto_1
    const/high16 v6, 0x41200000    # 10.0f

    invoke-static {v6}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    invoke-interface {v13, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v6, Lbmz;->t:Lbmz;

    const/high16 v7, 0x3fc00000    # 1.5f

    invoke-static {v7}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v7

    invoke-interface {v13, v6, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v6, Lcfw;

    invoke-direct {v6}, Lcfw;-><init>()V

    new-instance v7, Lcfy;

    invoke-direct {v7}, Lcfy;-><init>()V

    sget-object v8, Lbmz;->f:Lbmz;

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    move-object/from16 v0, p2

    invoke-static {v12, v0, v8, v6, v9}, Laky;->a(Ljava/util/Map;Lbku;Lbmz;Lcfz;Ljava/lang/Object;)V

    sget-object v8, Lbmz;->g:Lbmz;

    const/4 v9, 0x0

    new-array v9, v9, [Lbbo;

    move-object/from16 v0, p2

    invoke-static {v12, v0, v8, v7, v9}, Laky;->a(Ljava/util/Map;Lbku;Lbmz;Lcfz;Ljava/lang/Object;)V

    sget-object v8, Lbmz;->l:Lbmz;

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v9

    move-object/from16 v0, p2

    invoke-static {v12, v0, v8, v6, v9}, Laky;->a(Ljava/util/Map;Lbku;Lbmz;Lcfz;Ljava/lang/Object;)V

    move-object/from16 v0, p2

    invoke-static {v12, v0, v5, v7, v4}, Laky;->a(Ljava/util/Map;Lbku;Lbmz;Lcfz;Ljava/lang/Object;)V

    const/4 v4, 0x0

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-static {v12, v0, v6, v4}, Laky;->a(Ljava/util/Map;Lbku;Lcfz;Ljava/lang/Object;)V

    array-length v4, v11

    new-array v14, v4, [J

    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    move-wide v8, v6

    move v6, v4

    :goto_2
    array-length v4, v11

    if-ge v6, v4, :cond_d

    const/4 v4, 0x0

    invoke-interface {v13}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v15

    move v7, v4

    :goto_3
    invoke-interface {v15}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface {v15}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v16

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lbmz;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v12, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcfv;

    aget-wide v18, v11, v6

    move-wide/from16 v0, v18

    invoke-virtual {v4, v0, v1}, Lcfv;->a(J)Ljava/lang/Object;

    move-result-object v4

    sget-object v17, Lbmz;->g:Lbmz;

    move-object/from16 v0, v17

    if-ne v5, v0, :cond_6

    check-cast v4, [Lbbo;

    array-length v4, v4

    if-nez v4, :cond_5

    const/high16 v4, 0x3f800000    # 1.0f

    :goto_4
    mul-float v4, v4, v16

    add-float/2addr v4, v7

    move v7, v4

    goto :goto_3

    :cond_2
    sget-object v4, Lbmz;->b:Lbmz;

    if-ne v5, v4, :cond_3

    invoke-static {}, Lbxi;->a()[F

    move-result-object v4

    invoke-static {v4}, Lbxi;->a([F)Lbxi;

    move-result-object v4

    goto/16 :goto_1

    :cond_3
    sget-object v4, Lbmz;->r:Lbmz;

    if-ne v5, v4, :cond_4

    invoke-static {}, Lbxi;->a()[F

    move-result-object v4

    invoke-static {v4}, Lbxi;->a([F)Lbxi;

    move-result-object v4

    goto/16 :goto_1

    :cond_4
    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x17

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "unexpected field type: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v4

    throw v4

    :cond_5
    const/4 v4, 0x0

    goto :goto_4

    :cond_6
    sget-object v17, Lbmz;->f:Lbmz;

    move-object/from16 v0, v17

    if-ne v5, v0, :cond_7

    const/high16 v5, 0x41700000    # 15.0f

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    div-float v4, v5, v4

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    mul-float/2addr v4, v4

    goto :goto_4

    :cond_7
    sget-object v17, Lbmz;->l:Lbmz;

    move-object/from16 v0, v17

    if-ne v5, v0, :cond_8

    const-wide/high16 v18, 0x4018000000000000L    # 6.0

    invoke-static/range {v18 .. v19}, Ljava/lang/Math;->log(D)D

    move-result-wide v18

    const-wide/high16 v20, 0x4000000000000000L    # 2.0

    invoke-static/range {v20 .. v21}, Ljava/lang/Math;->log(D)D

    move-result-wide v20

    div-double v18, v18, v20

    move-wide/from16 v0, v18

    double-to-float v5, v0

    const/high16 v17, 0x3f800000    # 1.0f

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    div-float/2addr v4, v5

    sub-float v4, v17, v4

    mul-float/2addr v4, v4

    goto/16 :goto_4

    :cond_8
    sget-object v17, Lbmz;->a:Lbmz;

    move-object/from16 v0, v17

    if-eq v5, v0, :cond_9

    sget-object v17, Lbmz;->r:Lbmz;

    move-object/from16 v0, v17

    if-ne v5, v0, :cond_a

    :cond_9
    check-cast v4, Lbxi;

    invoke-virtual {v4}, Lbxi;->d()F

    move-result v5

    invoke-virtual {v4}, Lbxi;->e()F

    move-result v4

    mul-float/2addr v5, v5

    mul-float/2addr v4, v4

    add-float/2addr v4, v5

    const/high16 v5, 0x42480000    # 50.0f

    mul-float/2addr v4, v5

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-static {v4, v5}, Ljava/lang/Math;->min(FF)F

    move-result v4

    goto/16 :goto_4

    :cond_a
    sget-object v17, Lbmz;->t:Lbmz;

    move-object/from16 v0, v17

    if-ne v5, v0, :cond_b

    const/high16 v5, 0x3f800000    # 1.0f

    check-cast v4, Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    sub-float v4, v5, v4

    mul-float/2addr v4, v4

    goto/16 :goto_4

    :cond_b
    new-instance v4, Ljava/lang/RuntimeException;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x13

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Unknown field type "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4

    :cond_c
    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v4, v7

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v8, v4

    aput-wide v8, v14, v6

    add-int/lit8 v4, v6, 0x1

    move v6, v4

    goto/16 :goto_2

    :cond_d
    invoke-virtual/range {p1 .. p1}, Lbkn;->a()J

    move-result-wide v4

    invoke-static {v14, v11, v4, v5, v10}, Laky;->a([J[JJLjava/util/List;)Ljava/util/List;

    move-result-object v4

    goto/16 :goto_0
.end method
