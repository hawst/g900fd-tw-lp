.class public final Lbxy;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbon;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmw;",
            ">;"
        }
    .end annotation
.end field

.field public final c:J

.field public final d:J

.field public final e:I

.field public final f:Lbor;


# direct methods
.method constructor <init>(Ljava/util/List;Ljava/util/Map;Ljava/util/List;Ljava/util/Map;JJILbor;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbon;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbol;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmw;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbol;",
            ">;JJI",
            "Lbor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    if-nez p1, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 47
    :goto_0
    iput-object v0, p0, Lbxy;->a:Ljava/util/List;

    .line 48
    if-nez p2, :cond_1

    .line 50
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    .line 52
    :goto_1
    if-nez p3, :cond_2

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 53
    :goto_2
    iput-object v0, p0, Lbxy;->b:Ljava/util/List;

    .line 54
    if-nez p4, :cond_3

    .line 56
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    .line 58
    :goto_3
    iput-wide p5, p0, Lbxy;->c:J

    .line 59
    iput-wide p7, p0, Lbxy;->d:J

    .line 60
    iput p9, p0, Lbxy;->e:I

    .line 61
    const-string v0, "constraints"

    const/4 v1, 0x0

    invoke-static {p10, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbor;

    iput-object v0, p0, Lbxy;->f:Lbor;

    .line 62
    return-void

    .line 47
    :cond_0
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 51
    :cond_1
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    goto :goto_1

    .line 53
    :cond_2
    invoke-static {p3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_2

    .line 57
    :cond_3
    invoke-static {p4}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    goto :goto_3
.end method
