.class final Lzs;
.super Lzr;
.source "PG"


# instance fields
.field private d:Landroid/graphics/Bitmap;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 487
    invoke-direct {p0}, Lzr;-><init>()V

    .line 489
    const/4 v0, 0x0

    iput-object v0, p0, Lzs;->d:Landroid/graphics/Bitmap;

    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 576
    iget-object v0, p0, Lzs;->a:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    iget-object v1, p0, Lzs;->a:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lzs;->d:Landroid/graphics/Bitmap;

    .line 577
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 498
    const/16 v0, 0x10

    return v0
.end method

.method public a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 539
    iget-object v0, p0, Lzs;->d:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public a(Labf;)V
    .locals 0

    .prologue
    .line 493
    invoke-virtual {p0, p1}, Lzs;->b(Labf;)V

    .line 494
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 572
    check-cast p1, Landroid/graphics/Bitmap;

    iput-object p1, p0, Lzs;->d:Landroid/graphics/Bitmap;

    .line 573
    return-void
.end method

.method public a(Lzr;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 508
    invoke-virtual {p1}, Lzr;->a()I

    move-result v0

    .line 509
    and-int/lit8 v1, v0, 0x10

    if-eqz v1, :cond_0

    .line 510
    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Lzr;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lzs;->d:Landroid/graphics/Bitmap;

    .line 527
    :goto_0
    invoke-virtual {p1}, Lzr;->d()V

    .line 528
    iput-boolean v4, p0, Lzs;->b:Z

    .line 529
    return-void

    .line 511
    :cond_0
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_1

    .line 512
    invoke-direct {p0}, Lzs;->i()V

    .line 513
    invoke-virtual {p1, v5}, Lzr;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 514
    iget-object v1, p0, Lzs;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    .line 515
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    goto :goto_0

    .line 516
    :cond_1
    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_2

    .line 517
    invoke-direct {p0}, Lzs;->i()V

    .line 518
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lzr;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacw;

    .line 519
    iget-object v1, p0, Lzs;->d:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lzs;->a:[I

    aget v2, v2, v4

    iget-object v3, p0, Lzs;->a:[I

    aget v3, v3, v5

    invoke-virtual {v0, v2, v3}, Lacw;->b(II)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/graphics/Bitmap;->copyPixelsFromBuffer(Ljava/nio/Buffer;)V

    goto :goto_0

    .line 521
    :cond_2
    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_3

    invoke-static {}, Lzq;->i()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 522
    invoke-direct {p0}, Lzs;->i()V

    .line 523
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lzr;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/renderscript/Allocation;

    iget-object v1, p0, Lzs;->d:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/renderscript/Allocation;->copyTo(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 525
    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot sync bytebuffer backing!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 503
    const/16 v0, 0x10

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 554
    const/4 v0, 0x0

    return v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 544
    const/4 v0, 0x4

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 549
    const/4 v0, 0x0

    return v0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 561
    const/4 v0, 0x0

    iput-object v0, p0, Lzs;->d:Landroid/graphics/Bitmap;

    .line 562
    return-void
.end method

.method public h()I
    .locals 3

    .prologue
    .line 566
    iget-object v0, p0, Lzs;->a:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    mul-int/lit8 v0, v0, 0x4

    iget-object v1, p0, Lzs;->a:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    mul-int/2addr v0, v1

    return v0
.end method
