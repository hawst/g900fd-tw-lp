.class final Lgof;
.super Lgnm;


# instance fields
.field private final a:Lgfy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgfy",
            "<",
            "Lgyv;",
            ">;"
        }
    .end annotation
.end field

.field private synthetic b:Lgny;


# direct methods
.method public constructor <init>(Lgny;Lgfy;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfy",
            "<",
            "Lgyv;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lgof;->b:Lgny;

    invoke-direct {p0}, Lgnm;-><init>()V

    iput-object p2, p0, Lgof;->a:Lgfy;

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 6

    const/4 v0, 0x0

    const-string v1, "PeopleService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Circles callback: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nresolution="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nholder="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {p1, v0, p2}, Lgny;->a(ILjava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    if-nez p3, :cond_1

    :goto_0
    iget-object v2, p0, Lgof;->b:Lgny;

    new-instance v3, Lgob;

    iget-object v4, p0, Lgof;->b:Lgny;

    iget-object v5, p0, Lgof;->a:Lgfy;

    invoke-direct {v3, v4, v5, v1, v0}, Lgob;-><init>(Lgny;Lgfy;Lcom/google/android/gms/common/api/Status;Lgzs;)V

    invoke-virtual {v2, v3}, Lgny;->b(Lghj;)V

    return-void

    :cond_1
    new-instance v0, Lgzs;

    invoke-direct {v0, p3}, Lgzs;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method
