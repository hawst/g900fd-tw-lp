.class public final Lgzx;
.super Lgzt;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lgzt",
        "<",
        "Lgzw;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:Lgpa;

.field private final c:Lgoz;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;Lgpa;Lgoz;)V
    .locals 0

    invoke-direct {p0, p1}, Lgzt;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    iput-object p2, p0, Lgzx;->b:Lgpa;

    iput-object p3, p0, Lgzx;->c:Lgoz;

    return-void
.end method


# virtual methods
.method public synthetic a(I)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lgzx;->b(I)Lgzw;

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lgzw;
    .locals 3

    new-instance v0, Lgow;

    iget-object v1, p0, Lgzx;->a:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {p0}, Lgzx;->d()Landroid/os/Bundle;

    iget-object v2, p0, Lgzx;->b:Lgpa;

    iget-object v2, p0, Lgzx;->c:Lgoz;

    invoke-direct {v0, v1, p1}, Lgow;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "People:size="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lgzx;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
