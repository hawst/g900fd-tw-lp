.class public Lbhn;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lbhn;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbhn;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 421
    return-void
.end method

.method public static a(Lbmc;Lbjp;Lbig;)Lbhx;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbmc;",
            "Lbjp",
            "<",
            "Lbhk;",
            ">;",
            "Lbig;",
            ")",
            "Lbhx;"
        }
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 69
    new-instance v4, Ljava/util/ArrayList;

    .line 70
    invoke-interface {p0}, Lbmc;->b()I

    move-result v1

    invoke-direct {v4, v1}, Ljava/util/ArrayList;-><init>(I)V

    move v1, v0

    .line 71
    :goto_0
    invoke-interface {p0}, Lbmc;->b()I

    move-result v2

    if-ge v1, v2, :cond_2

    .line 72
    invoke-interface {p0, v1}, Lbmc;->a(I)Lbma;

    move-result-object v2

    .line 73
    :try_start_0
    new-instance v3, Lbhh;

    .line 76
    invoke-interface {p0, v1}, Lbmc;->a(I)Lbma;

    move-result-object v5

    invoke-direct {v3, v5}, Lbhh;-><init>(Lbma;)V

    .line 75
    invoke-static {v2, v3, p1, p2}, Lbhn;->a(Lbma;Lbhh;Lbjp;Lbig;)Lbib;

    move-result-object v2

    .line 78
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 84
    :catchall_0
    move-exception v2

    move v3, v0

    :goto_1
    if-ge v3, v1, :cond_1

    .line 87
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbib;

    .line 88
    iget-object v5, v0, Lbib;->a:Lbht;

    invoke-static {v5}, Lcgl;->a(Lcgk;)V

    .line 89
    iget-object v0, v0, Lbib;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;

    .line 90
    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    goto :goto_2

    .line 85
    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_1
    throw v2

    .line 96
    :cond_2
    new-instance v0, Lbhx;

    const v1, 0xac44

    const/4 v2, 0x2

    invoke-direct {v0, v4, v1, v2}, Lbhx;-><init>(Ljava/util/List;II)V

    return-object v0
.end method

.method private static a(Lbma;Lbhh;Lbjp;Lbig;)Lbib;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbma;",
            "Lbhh;",
            "Lbjp",
            "<",
            "Lbhk;",
            ">;",
            "Lbig;",
            ")",
            "Lbib;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 226
    invoke-interface {p0}, Lbma;->a()I

    move-result v2

    new-array v3, v2, [Lbiy;

    move v2, v0

    .line 227
    :goto_0
    :try_start_0
    invoke-interface {p0}, Lbma;->a()I

    move-result v4

    if-ge v2, v4, :cond_3

    invoke-interface {p0, v2}, Lbma;->a(I)Lbmd;

    move-result-object v4

    new-instance v5, Lbiv;

    invoke-direct {v5, p3, v4}, Lbiv;-><init>(Lbig;Lbmd;)V

    aput-object v5, v3, v2

    invoke-virtual {v5}, Lbiv;->b()V

    invoke-virtual {v5}, Lbiv;->d()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1, v2, v5}, Lbhh;->a(ILbiv;)V

    invoke-virtual {v5}, Lbiv;->e()Landroid/media/MediaFormat;

    move-result-object v4

    const-string v5, "channel-count"

    invoke-virtual {v4, v5}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x2

    if-le v4, v5, :cond_1

    const-string v2, "Cannot handle audio with more than two channels"

    invoke-static {v2}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v2

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v2

    :goto_1
    array-length v4, v3

    if-ge v0, v4, :cond_2

    aget-object v4, v3, v0

    invoke-static {v4}, Lcgl;->a(Lcgk;)V

    aput-object v1, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    const/4 v4, 0x0

    :try_start_1
    aput-object v4, v3, v2

    invoke-virtual {v5}, Lbiv;->a()V

    sget-object v4, Lbhn;->a:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x32

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Omitting an audio source that has no sample data: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_2
    throw v2

    .line 229
    :cond_3
    new-instance v4, Ljava/util/ArrayList;

    array-length v2, v3

    invoke-direct {v4, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 231
    array-length v5, v3

    move v2, v0

    :goto_2
    if-ge v2, v5, :cond_6

    aget-object v6, v3, v2

    .line 232
    if-nez v6, :cond_5

    move-object v0, v1

    .line 234
    :goto_3
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 237
    if-eqz v0, :cond_4

    .line 238
    invoke-virtual {p1, v0}, Lbhh;->a(Lcgk;)V

    .line 231
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 232
    :cond_5
    new-instance v0, Lbjd;

    invoke-direct {v0, p2, v6}, Lbjd;-><init>(Lbjp;Lbiy;)V

    goto :goto_3

    .line 242
    :cond_6
    new-instance v0, Ljava/util/ArrayList;

    .line 243
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 244
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 245
    invoke-static {v0, v1, p0, v3, v4}, Lbhn;->a(Ljava/util/List;Ljava/util/List;Lbma;[Lbiy;Ljava/util/List;)V

    .line 248
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {p0}, Lbma;->a()I

    move-result v2

    if-eqz v2, :cond_7

    .line 249
    new-instance v0, Lbhp;

    invoke-direct {v0}, Lbhp;-><init>()V

    throw v0

    .line 252
    :cond_7
    new-instance v2, Lbib;

    new-instance v3, Lbht;

    invoke-direct {v3, v0}, Lbht;-><init>(Ljava/util/List;)V

    invoke-direct {v2, v3, v1, p1, p0}, Lbib;-><init>(Lbht;Ljava/util/List;Lbhh;Lbma;)V

    return-object v2
.end method

.method public static a(Lbop;Lbjp;Lbhq;Lbig;Lbhv;)Ljava/util/List;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbop;",
            "Lbjp",
            "<",
            "Lbhl;",
            ">;",
            "Lbhq;",
            "Lbig;",
            "Lbhv;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbhr",
            "<",
            "Lbhl;",
            ">;>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 125
    .line 126
    invoke-interface {p0}, Lbop;->e()I

    move-result v1

    new-array v5, v1, [Lbiz;

    .line 127
    invoke-interface {p0}, Lbop;->e()I

    move-result v1

    new-array v3, v1, [Lbmd;

    .line 128
    invoke-interface {p0}, Lbop;->e()I

    move-result v1

    new-array v4, v1, [Lbmd;

    move v1, v0

    .line 129
    :goto_0
    :try_start_0
    invoke-interface {p0}, Lbop;->e()I

    move-result v6

    if-ge v1, v6, :cond_6

    .line 132
    invoke-interface {p0, v1}, Lbop;->b(I)Lbmd;

    move-result-object v6

    .line 133
    iget-object v7, v6, Lbmd;->d:Lbmg;

    sget-object v8, Lbmg;->a:Lbmg;

    if-eq v7, v8, :cond_0

    iget-object v7, v6, Lbmd;->d:Lbmg;

    sget-object v8, Lbmg;->e:Lbmg;

    if-ne v7, v8, :cond_2

    .line 134
    :cond_0
    new-instance v7, Lbiv;

    invoke-direct {v7, p3, v6}, Lbiv;-><init>(Lbig;Lbmd;)V

    .line 136
    aput-object v7, v5, v1

    .line 137
    invoke-virtual {v7}, Lbiv;->c()V

    .line 138
    invoke-virtual {v7}, Lbiv;->d()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 139
    invoke-virtual {p4, v7, v1}, Lbhv;->a(Lbiv;I)V

    .line 131
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 141
    :cond_1
    const/4 v6, 0x0

    aput-object v6, v5, v1

    .line 142
    invoke-virtual {v7}, Lbiv;->a()V

    .line 143
    sget-object v6, Lbhn;->a:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x31

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Omitting a video source that has no sample data: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 157
    :catchall_0
    move-exception v1

    :goto_2
    array-length v2, v5

    if-ge v0, v2, :cond_5

    .line 158
    aget-object v2, v5, v0

    invoke-static {v2}, Lcgl;->a(Lcgk;)V

    .line 157
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 146
    :cond_2
    :try_start_1
    iget-object v7, v6, Lbmd;->d:Lbmg;

    sget-object v8, Lbmg;->c:Lbmg;

    if-ne v7, v8, :cond_3

    .line 147
    aput-object v6, v3, v1

    goto :goto_1

    .line 148
    :cond_3
    iget-object v7, v6, Lbmd;->d:Lbmg;

    sget-object v8, Lbmg;->d:Lbmg;

    if-ne v7, v8, :cond_4

    .line 149
    aput-object v6, v4, v1

    goto :goto_1

    .line 151
    :cond_4
    iget-object v1, v6, Lbmd;->d:Lbmg;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x16

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unexpected clip type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v1

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 157
    :cond_5
    throw v1

    .line 163
    :cond_6
    invoke-interface {p0}, Lbop;->e()I

    move-result v1

    new-array v6, v1, [Lbjb;

    .line 164
    invoke-static {v6, v3, p0, p2, p4}, Lbhn;->a([Lbjb;[Lbmd;Lbop;Lbhq;Lbhv;)V

    .line 166
    invoke-virtual {p4, p0}, Lbhv;->a(Lbop;)Lbhm;

    move-result-object v7

    .line 169
    new-instance v8, Ljava/util/LinkedList;

    invoke-direct {v8}, Ljava/util/LinkedList;-><init>()V

    move v4, v0

    .line 172
    :goto_3
    :try_start_2
    invoke-interface {p0}, Lbop;->e()I

    move-result v1

    if-ge v4, v1, :cond_d

    .line 173
    aget-object v1, v6, v4

    if-eqz v1, :cond_8

    move v3, v2

    .line 174
    :goto_4
    aget-object v1, v5, v4

    if-eqz v1, :cond_9

    move v1, v2

    .line 175
    :goto_5
    invoke-interface {p0, v4}, Lbop;->b(I)Lbmd;

    move-result-object v9

    iget-object v9, v9, Lbmd;->d:Lbmg;

    sget-object v10, Lbmg;->d:Lbmg;

    if-ne v9, v10, :cond_a

    .line 176
    invoke-interface {v8, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 172
    :cond_7
    :goto_6
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_3

    :cond_8
    move v3, v0

    .line 173
    goto :goto_4

    :cond_9
    move v1, v0

    .line 174
    goto :goto_5

    .line 177
    :cond_a
    if-eqz v3, :cond_b

    if-nez v1, :cond_b

    .line 178
    aget-object v1, v6, v4

    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_6

    .line 200
    :catchall_1
    move-exception v1

    move-object v2, v1

    move v1, v0

    :goto_7
    array-length v3, v5

    if-ge v1, v3, :cond_e

    .line 201
    aget-object v3, v5, v1

    invoke-static {v3}, Lcgl;->a(Lcgk;)V

    .line 200
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 179
    :cond_b
    if-eqz v1, :cond_c

    if-nez v3, :cond_c

    :try_start_3
    aget-object v9, v5, v4

    .line 180
    invoke-virtual {v9}, Lbiz;->d()Z

    move-result v9

    if-eqz v9, :cond_c

    .line 181
    new-instance v1, Lbjd;

    aget-object v3, v5, v4

    invoke-direct {v1, p1, v3}, Lbjd;-><init>(Lbjp;Lbiy;)V

    .line 184
    invoke-virtual {p4, v1}, Lbhv;->a(Lbhw;)V

    .line 185
    invoke-interface {v8, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_6

    .line 186
    :cond_c
    if-eqz v1, :cond_7

    if-eqz v3, :cond_7

    .line 187
    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x3a

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Both video and photo sources available at index"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v1

    throw v1

    .line 194
    :cond_d
    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_10

    .line 195
    new-instance v1, Lbhp;

    invoke-direct {v1}, Lbhp;-><init>()V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 203
    :cond_e
    :goto_8
    array-length v1, v6

    if-ge v0, v1, :cond_f

    .line 204
    aget-object v1, v6, v0

    invoke-static {v1}, Lcgl;->a(Lcgk;)V

    .line 203
    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    .line 206
    :cond_f
    invoke-static {v7}, Lcgl;->a(Lcgk;)V

    throw v2

    .line 210
    :cond_10
    return-object v8
.end method

.method private static a(Ljava/util/List;Ljava/util/List;Lbma;[Lbiy;Ljava/util/List;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbhr",
            "<",
            "Lbhk;",
            ">;>;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;",
            ">;",
            "Lbma;",
            "[",
            "Lbiy;",
            "Ljava/util/List",
            "<",
            "Lbhr",
            "<",
            "Lbhk;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 317
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 318
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    .line 321
    const/4 v1, 0x0

    .line 322
    const/4 v0, 0x0

    move v3, v0

    move-object v2, v1

    :goto_0
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v3, v0, :cond_3

    .line 323
    invoke-interface {p4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    .line 324
    if-eqz v0, :cond_2

    .line 325
    aget-object v1, p3, v3

    .line 326
    invoke-interface {v1}, Lbiy;->e()Landroid/media/MediaFormat;

    move-result-object v1

    invoke-static {v1}, Lbkf;->c(Landroid/media/MediaFormat;)Ljava/lang/Object;

    move-result-object v6

    .line 329
    invoke-interface {v5, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;

    .line 330
    if-ne v2, v1, :cond_1

    if-eqz v1, :cond_1

    .line 331
    invoke-interface {v4, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;

    move-object v2, v1

    move-object v1, v4

    .line 337
    :goto_1
    if-nez v2, :cond_0

    .line 338
    aget-object v2, p3, v3

    invoke-interface {v2}, Lbiy;->e()Landroid/media/MediaFormat;

    move-result-object v2

    const-string v7, "channel-count"

    invoke-virtual {v2, v7}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v7

    .line 340
    new-instance v2, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;

    const v8, 0xac44

    const/4 v9, 0x1

    invoke-direct {v2, v7, v8, v9}, Lcom/google/android/apps/moviemaker/media/audio/AudioResampler;-><init>(IIZ)V

    .line 344
    invoke-interface {v1, v6, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 346
    :cond_0
    invoke-interface {p1, v3, v2}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 349
    invoke-interface {p2, v3}, Lbma;->a(I)Lbmd;

    move-result-object v1

    iget-object v6, v1, Lbmd;->f:Lbmp;

    .line 350
    new-instance v1, Lbic;

    invoke-direct {v1, v0, v6}, Lbic;-><init>(Lbhr;Lbmp;)V

    .line 353
    invoke-interface {p2, v3}, Lbma;->b(I)J

    move-result-wide v6

    invoke-interface {p2, v3}, Lbma;->a(I)Lbmd;

    move-result-object v0

    iget-object v0, v0, Lbmd;->f:Lbmp;

    iget-wide v8, v0, Lbmp;->b:J

    sub-long/2addr v6, v8

    .line 354
    const-wide/16 v8, 0x0

    cmp-long v0, v6, v8

    if-eqz v0, :cond_4

    .line 355
    new-instance v0, Lbhu;

    invoke-direct {v0, v1, v6, v7}, Lbhu;-><init>(Lbhr;J)V

    .line 358
    :goto_2
    invoke-interface {p0, v3, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    move-object v0, v2

    .line 322
    :goto_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move-object v2, v0

    goto :goto_0

    :cond_1
    move-object v2, v1

    move-object v1, v5

    .line 334
    goto :goto_1

    .line 360
    :cond_2
    const/4 v0, 0x0

    invoke-interface {p0, v3, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 361
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 362
    const/4 v0, 0x0

    goto :goto_3

    .line 365
    :cond_3
    return-void

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method private static a([Lbjb;[Lbmd;Lbop;Lbhq;Lbhv;)V
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 375
    const/4 v0, 0x2

    new-array v7, v0, [Lbjb;

    .line 378
    const-wide/16 v2, 0x0

    move v0, v1

    move v4, v1

    .line 379
    :goto_0
    array-length v6, p1

    if-ge v0, v6, :cond_6

    .line 380
    aget-object v6, p1, v0

    if-eqz v6, :cond_2

    .line 383
    if-eqz v0, :cond_0

    add-int/lit8 v6, v0, -0x1

    aget-object v6, p1, v6

    if-eqz v6, :cond_0

    if-nez v4, :cond_4

    :cond_0
    move v6, v5

    .line 384
    :goto_1
    if-eqz v6, :cond_5

    move v4, v1

    .line 385
    :goto_2
    aget-object v8, v7, v4

    if-nez v8, :cond_1

    .line 386
    invoke-virtual {p3}, Lbhq;->a()Lbjb;

    move-result-object v8

    aput-object v8, v7, v4

    .line 388
    :cond_1
    aget-object v8, v7, v4

    aget-object v9, p1, v0

    invoke-virtual {v8, v9, v2, v3}, Lbjb;->a(Lbmd;J)V

    .line 390
    aget-object v4, v7, v4

    aput-object v4, p0, v0

    move v4, v6

    .line 393
    :cond_2
    invoke-interface {p2, v0}, Lbop;->b(I)Lbmd;

    move-result-object v6

    invoke-virtual {v6}, Lbmd;->b()J

    move-result-wide v8

    add-long/2addr v2, v8

    .line 394
    invoke-interface {p2}, Lbop;->e()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    if-ge v0, v6, :cond_3

    .line 395
    invoke-interface {p2, v0}, Lbop;->d(I)Lcac;

    move-result-object v6

    invoke-interface {v6}, Lcac;->b()J

    move-result-wide v8

    sub-long/2addr v2, v8

    .line 379
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v6, v1

    .line 383
    goto :goto_1

    :cond_5
    move v4, v5

    .line 384
    goto :goto_2

    .line 399
    :cond_6
    aget-object v0, v7, v1

    if-eqz v0, :cond_7

    .line 400
    aget-object v0, v7, v1

    invoke-virtual {p4, v0}, Lbhv;->a(Lbjb;)V

    .line 402
    :cond_7
    aget-object v0, v7, v5

    if-eqz v0, :cond_8

    .line 403
    aget-object v0, v7, v5

    invoke-virtual {p4, v0}, Lbhv;->a(Lbjb;)V

    .line 405
    :cond_8
    return-void
.end method
