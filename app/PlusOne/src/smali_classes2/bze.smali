.class public final Lbze;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Lbzf;


# instance fields
.field private final b:Lama;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lbzi;->a:Lbzi;

    sput-object v0, Lbze;->a:Lbzf;

    return-void
.end method

.method public constructor <init>(Lama;)V
    .locals 2

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-string v0, "clipIdentifierGenerator"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lama;

    iput-object v0, p0, Lbze;->b:Lama;

    .line 37
    return-void
.end method


# virtual methods
.method public a(Lbon;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 43
    invoke-virtual {p0, p1}, Lbze;->b(Lbon;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lbon;)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbon;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    .line 53
    invoke-virtual {p1}, Lbon;->a()Ljeg;

    move-result-object v0

    invoke-static {v0, p1}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v3

    .line 54
    new-instance v4, Lbor;

    invoke-direct {v4}, Lbor;-><init>()V

    .line 55
    sget-object v0, Lbze;->a:Lbzf;

    .line 57
    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 58
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    const/4 v5, 0x0

    .line 56
    invoke-interface/range {v0 .. v5}, Lbzf;->a(Ljava/util/List;Ljava/util/List;Ljava/util/Map;Lbor;Lbzg;)Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lbze;->b:Lama;

    .line 55
    invoke-static {v0, v1}, Lbpm;->a(Ljava/util/List;Lama;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
