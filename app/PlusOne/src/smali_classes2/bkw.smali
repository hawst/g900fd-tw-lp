.class public final Lbkw;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lbmz;

.field private synthetic b:Lbkv;


# direct methods
.method public constructor <init>(Lbkv;Lbmz;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lbkw;->b:Lbkv;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    iput-object p2, p0, Lbkw;->a:Lbmz;

    .line 181
    return-void
.end method


# virtual methods
.method public a()I
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lbkw;->b:Lbkv;

    iget-object v0, v0, Lbkv;->a:Ljava/util/Map;

    iget-object v1, p0, Lbkw;->a:Lbmz;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    return v0
.end method

.method public a(I)J
    .locals 3

    .prologue
    .line 188
    const-string v0, "OneMetricIndex"

    const/4 v1, 0x0

    invoke-virtual {p0}, Lbkw;->a()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {p1, v0, v1, v2}, Lcec;->a(ILjava/lang/CharSequence;II)I

    .line 189
    iget-object v0, p0, Lbkw;->b:Lbkv;

    iget-object v0, v0, Lbkv;->a:Ljava/util/Map;

    iget-object v1, p0, Lbkw;->a:Lbmz;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public b(I)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 193
    const-string v0, "OneMetricIndex"

    const/4 v1, 0x0

    invoke-virtual {p0}, Lbkw;->a()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-static {p1, v0, v1, v2}, Lcec;->a(ILjava/lang/CharSequence;II)I

    .line 194
    iget-object v0, p0, Lbkw;->b:Lbkv;

    iget-object v0, v0, Lbkv;->a:Ljava/util/Map;

    iget-object v1, p0, Lbkw;->a:Lbmz;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
