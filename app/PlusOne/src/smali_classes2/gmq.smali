.class final Lgmq;
.super Lgsg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lgsg",
        "<",
        "Lgsd;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lcom/google/android/gms/location/reporting/UploadRequest;


# direct methods
.method constructor <init>(Lcom/google/android/gms/location/reporting/UploadRequest;)V
    .locals 0

    iput-object p1, p0, Lgmq;->a:Lcom/google/android/gms/location/reporting/UploadRequest;

    invoke-direct {p0}, Lgsg;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/common/api/Status;)Lgsd;
    .locals 1

    new-instance v0, Lgms;

    invoke-direct {v0, p1}, Lgms;-><init>(Lcom/google/android/gms/common/api/Status;)V

    return-object v0
.end method

.method protected bridge synthetic a(Lgfr;)V
    .locals 0

    check-cast p1, Lgmm;

    invoke-virtual {p0, p1}, Lgmq;->a(Lgmm;)V

    return-void
.end method

.method protected a(Lgmm;)V
    .locals 4

    iget-object v0, p0, Lgmq;->a:Lcom/google/android/gms/location/reporting/UploadRequest;

    invoke-virtual {p1, v0}, Lgmm;->a(Lcom/google/android/gms/location/reporting/UploadRequest;)Lcom/google/android/gms/location/reporting/UploadRequestResult;

    move-result-object v1

    new-instance v2, Lgms;

    new-instance v3, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v1}, Lcom/google/android/gms/location/reporting/UploadRequestResult;->b()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const/16 v0, 0x8

    :goto_0
    invoke-direct {v3, v0}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-virtual {v1}, Lcom/google/android/gms/location/reporting/UploadRequestResult;->c()J

    invoke-direct {v2, v3}, Lgms;-><init>(Lcom/google/android/gms/common/api/Status;)V

    invoke-virtual {p0, v2}, Lgmq;->a(Lggf;)V

    return-void

    :sswitch_0
    const/4 v0, 0x0

    goto :goto_0

    :sswitch_1
    const/16 v0, 0xdb1

    goto :goto_0

    :sswitch_2
    const/16 v0, 0xdb0

    goto :goto_0

    :sswitch_3
    const/16 v0, 0xdac

    goto :goto_0

    :sswitch_4
    const/16 v0, 0xdb2

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x64 -> :sswitch_4
    .end sparse-switch
.end method

.method public synthetic b(Lcom/google/android/gms/common/api/Status;)Lggf;
    .locals 1

    invoke-virtual {p0, p1}, Lgmq;->a(Lcom/google/android/gms/common/api/Status;)Lgsd;

    move-result-object v0

    return-object v0
.end method
