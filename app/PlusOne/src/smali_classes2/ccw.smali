.class public Lccw;
.super Lccv;
.source "PG"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:I

.field private d:Z

.field private e:Z

.field private f:J

.field private g:I

.field private h:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lccw;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lccw;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcci;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0, p1}, Lccv;-><init>(Lcci;)V

    .line 51
    invoke-interface {p1}, Lcci;->f()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 52
    const v1, 0x7f0d005f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lccw;->c:I

    .line 53
    return-void
.end method

.method private b(Lccj;I)I
    .locals 2

    .prologue
    .line 99
    iget-boolean v0, p1, Lccj;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lccj;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, p2

    iget v1, p1, Lccj;->i:I

    sub-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p1, Lccj;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    add-int/2addr v0, p2

    iget v1, p1, Lccj;->i:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(ILjava/lang/Runnable;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 64
    iput-boolean v0, p0, Lccw;->d:Z

    .line 65
    iput-boolean v0, p0, Lccw;->e:Z

    .line 66
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lccw;->f:J

    .line 67
    const-string v0, "clipPosition"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lccw;->g:I

    .line 68
    iput-object p2, p0, Lccw;->h:Ljava/lang/Runnable;

    .line 69
    iget-object v0, p0, Lccw;->a:Lcci;

    invoke-interface {v0}, Lcci;->d()V

    .line 70
    return-void
.end method

.method public a(JI)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lccw;->d:Z

    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Lccw;->e:Z

    .line 80
    iput-wide p1, p0, Lccw;->f:J

    .line 81
    const-string v0, "clipPosition"

    invoke-static {p3, v0, v1}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lccw;->g:I

    .line 82
    iput-object v1, p0, Lccw;->h:Ljava/lang/Runnable;

    .line 83
    iget-object v0, p0, Lccw;->a:Lcci;

    invoke-interface {v0}, Lcci;->d()V

    .line 84
    return-void
.end method

.method public a(Lccj;)V
    .locals 9

    .prologue
    const/16 v8, 0x4b

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 90
    iget-boolean v0, p0, Lccw;->d:Z

    if-eqz v0, :cond_1

    invoke-virtual {p1, v8}, Lccj;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 91
    iget-boolean v0, p1, Lccj;->f:Z

    if-eqz v0, :cond_0

    sget-object v0, Lccw;->b:Ljava/lang/String;

    iget v0, p0, Lccw;->g:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v5, 0x2a

    invoke-direct {v1, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "playAnimations() KeyPosition = "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_0
    iget-boolean v0, p1, Lccj;->a:Z

    if-eqz v0, :cond_2

    iget-object v0, p1, Lccj;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    :goto_0
    iget v1, p0, Lccw;->c:I

    add-int/2addr v1, v0

    iget-boolean v0, p1, Lccj;->a:Z

    if-eqz v0, :cond_3

    iget-object v0, p1, Lccj;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    :goto_1
    iget v5, p0, Lccw;->c:I

    sub-int v5, v0, v5

    iget v0, p0, Lccw;->g:I

    add-int/lit8 v0, v0, -0x1

    iget-object v6, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v6, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    if-eqz v0, :cond_7

    iget-object v6, v0, Lccg;->a:Landroid/view/View;

    invoke-virtual {p1, v6}, Lccj;->a(Landroid/view/View;)I

    move-result v6

    iget-object v0, v0, Lccg;->a:Landroid/view/View;

    invoke-virtual {p1, v0}, Lccj;->b(Landroid/view/View;)I

    move-result v0

    add-int/2addr v0, v6

    invoke-direct {p0, p1, v0}, Lccw;->b(Lccj;I)I

    move-result v0

    sub-int v0, v1, v0

    move v1, v0

    :goto_2
    iget v0, p0, Lccw;->g:I

    add-int/lit8 v0, v0, 0x1

    iget-object v6, p1, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v6, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    if-eqz v0, :cond_6

    iget-object v0, v0, Lccg;->a:Landroid/view/View;

    invoke-virtual {p1, v0}, Lccj;->a(Landroid/view/View;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lccw;->b(Lccj;I)I

    move-result v0

    sub-int v0, v5, v0

    :goto_3
    new-instance v5, Lccx;

    iget v6, p0, Lccw;->g:I

    invoke-direct {v5, v1, v0, v6}, Lccx;-><init>(III)V

    iget-boolean v0, p0, Lccw;->e:Z

    if-eqz v0, :cond_4

    move v0, v3

    :goto_4
    iget-boolean v1, p0, Lccw;->e:Z

    if-eqz v1, :cond_5

    :goto_5
    invoke-static {}, Lcad;->a()Landroid/animation/ObjectAnimator;

    move-result-object v1

    invoke-virtual {v1, v5}, Landroid/animation/ObjectAnimator;->setProperty(Landroid/util/Property;)V

    invoke-virtual {v1, p1}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    const/4 v3, 0x2

    new-array v3, v3, [F

    aput v0, v3, v2

    const/4 v6, 0x1

    aput v4, v3, v6

    invoke-virtual {v1, v3}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    iget-wide v6, p0, Lccw;->f:J

    invoke-virtual {v1, v6, v7}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    const-wide/16 v6, 0x2ee

    invoke-virtual {v1, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    iget-object v3, p0, Lccw;->h:Ljava/lang/Runnable;

    invoke-virtual {p1, v1, v8, v3}, Lccj;->a(Landroid/animation/Animator;ILjava/lang/Runnable;)V

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v5, p1, v0}, Lccx;->a(Lccj;Ljava/lang/Float;)V

    .line 92
    iput-boolean v2, p0, Lccw;->d:Z

    .line 94
    :cond_1
    return-void

    .line 91
    :cond_2
    iget-object v0, p1, Lccj;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    goto/16 :goto_0

    :cond_3
    iget-object v0, p1, Lccj;->e:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    goto/16 :goto_1

    :cond_4
    move v0, v4

    goto :goto_4

    :cond_5
    move v4, v3

    goto :goto_5

    :cond_6
    move v0, v2

    goto :goto_3

    :cond_7
    move v1, v2

    goto :goto_2
.end method
