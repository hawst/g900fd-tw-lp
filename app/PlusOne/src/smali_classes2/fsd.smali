.class public final Lfsd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field private final a:[Lhng;


# direct methods
.method public constructor <init>([Lhng;)V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    iput-object p1, p0, Lfsd;->a:[Lhng;

    .line 104
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 108
    const/4 v0, 0x0

    .line 109
    iget-object v2, p0, Lfsd;->a:[Lhng;

    array-length v2, v2

    .line 110
    iget-object v3, p0, Lfsd;->a:[Lhng;

    aget-object v3, v3, v1

    iget v3, v3, Lhng;->a:F

    cmpg-float v3, p1, v3

    if-gtz v3, :cond_1

    .line 111
    iget-object v0, p0, Lfsd;->a:[Lhng;

    aget-object v0, v0, v1

    iget v0, v0, Lhng;->b:F

    .line 122
    :cond_0
    :goto_0
    return v0

    .line 112
    :cond_1
    iget-object v3, p0, Lfsd;->a:[Lhng;

    add-int/lit8 v4, v2, -0x1

    aget-object v3, v3, v4

    iget v3, v3, Lhng;->g:F

    cmpl-float v3, p1, v3

    if-ltz v3, :cond_2

    .line 113
    iget-object v0, p0, Lfsd;->a:[Lhng;

    add-int/lit8 v1, v2, -0x1

    aget-object v0, v0, v1

    iget v0, v0, Lhng;->h:F

    goto :goto_0

    .line 115
    :cond_2
    iget-object v2, p0, Lfsd;->a:[Lhng;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 116
    iget v5, v4, Lhng;->a:F

    cmpl-float v5, p1, v5

    if-ltz v5, :cond_3

    iget v5, v4, Lhng;->g:F

    cmpg-float v5, p1, v5

    if-gtz v5, :cond_3

    .line 117
    invoke-virtual {v4, p1}, Lhng;->c(F)F

    move-result v0

    invoke-virtual {v4, v0}, Lhng;->b(F)F

    move-result v0

    goto :goto_0

    .line 115
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method
