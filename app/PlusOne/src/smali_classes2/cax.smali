.class public final Lcax;
.super Landroid/graphics/drawable/Drawable;
.source "PG"


# instance fields
.field private final a:Landroid/graphics/RectF;

.field private final b:Landroid/animation/TimeInterpolator;

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Paint;

.field private synthetic e:Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 154
    iput-object p1, p0, Lcax;->e:Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 148
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcax;->a:Landroid/graphics/RectF;

    .line 149
    sget-object v0, Lcak;->a:Lcak;

    iput-object v0, p0, Lcax;->b:Landroid/animation/TimeInterpolator;

    .line 151
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcax;->c:Landroid/graphics/Paint;

    .line 152
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcax;->d:Landroid/graphics/Paint;

    .line 155
    iget-object v0, p0, Lcax;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 156
    iget-object v0, p0, Lcax;->c:Landroid/graphics/Paint;

    invoke-virtual {p1}, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b004b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 158
    iget-object v0, p0, Lcax;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 160
    iget-object v0, p0, Lcax;->d:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 161
    iget-object v0, p0, Lcax;->d:Landroid/graphics/Paint;

    invoke-virtual {p1}, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b004c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 162
    iget-object v0, p0, Lcax;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 163
    iget-object v0, p0, Lcax;->d:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 164
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 11

    .prologue
    const v7, 0x43b3fffd    # 359.9999f

    const/high16 v6, -0x40800000    # -1.0f

    const/high16 v2, 0x43b40000    # 360.0f

    const/4 v3, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    .line 168
    invoke-virtual {p0}, Lcax;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    .line 169
    iget-object v1, p0, Lcax;->b:Landroid/animation/TimeInterpolator;

    const v4, 0x3f19999a    # 0.6f

    iget-object v8, p0, Lcax;->e:Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;

    .line 171
    invoke-static {v8}, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->a(Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;)F

    move-result v8

    .line 170
    invoke-static {v3, v4, v8}, Lcfn;->c(FFF)F

    move-result v4

    .line 169
    invoke-interface {v1, v4}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v1

    .line 172
    mul-float/2addr v1, v0

    .line 175
    invoke-virtual {p0}, Lcax;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->centerX()I

    move-result v4

    int-to-float v4, v4

    .line 176
    invoke-virtual {p0}, Lcax;->getBounds()Landroid/graphics/Rect;

    move-result-object v8

    invoke-virtual {v8}, Landroid/graphics/Rect;->centerY()I

    move-result v8

    int-to-float v8, v8

    iget-object v9, p0, Lcax;->c:Landroid/graphics/Paint;

    .line 174
    invoke-virtual {p1, v4, v8, v1, v9}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 182
    iget-object v1, p0, Lcax;->e:Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;

    invoke-static {v1}, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->a(Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;)F

    move-result v1

    cmpg-float v1, v1, v5

    if-gez v1, :cond_2

    .line 183
    iget-object v1, p0, Lcax;->b:Landroid/animation/TimeInterpolator;

    const v4, 0x3e4ccccd    # 0.2f

    const v8, 0x3f4ccccd    # 0.8f

    iget-object v9, p0, Lcax;->e:Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;

    .line 184
    invoke-static {v9}, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->a(Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;)F

    move-result v9

    .line 183
    invoke-static {v4, v8, v9}, Lcfn;->c(FFF)F

    move-result v4

    invoke-interface {v1, v4}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v1

    .line 185
    iget-object v4, p0, Lcax;->b:Landroid/animation/TimeInterpolator;

    const v8, 0x3ecccccd    # 0.4f

    iget-object v9, p0, Lcax;->e:Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;

    .line 186
    invoke-static {v9}, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->a(Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;)F

    move-result v9

    .line 185
    invoke-static {v8, v5, v9}, Lcfn;->c(FFF)F

    move-result v8

    invoke-interface {v4, v8}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v4

    .line 187
    const v8, 0x3f333333    # 0.7f

    mul-float/2addr v1, v8

    mul-float v8, v1, v0

    .line 188
    const/high16 v1, 0x3f000000    # 0.5f

    mul-float/2addr v1, v4

    mul-float v4, v1, v0

    .line 195
    sub-float/2addr v0, v8

    sub-float v1, v8, v4

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v1, v9

    add-float/2addr v1, v0

    .line 196
    sub-float v0, v8, v4

    .line 202
    :goto_0
    iget-object v4, p0, Lcax;->a:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcax;->getBounds()Landroid/graphics/Rect;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 203
    iget-object v4, p0, Lcax;->a:Landroid/graphics/RectF;

    invoke-virtual {v4, v1, v1}, Landroid/graphics/RectF;->inset(FF)V

    .line 204
    iget-object v1, p0, Lcax;->d:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 208
    invoke-virtual {p0}, Lcax;->getLevel()I

    move-result v1

    add-int/lit16 v1, v1, 0x2710

    iget-object v4, p0, Lcax;->e:Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;

    invoke-static {v4}, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->b(Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;)I

    move-result v4

    sub-int/2addr v1, v4

    rem-int/lit16 v8, v1, 0x2710

    .line 211
    iget-object v1, p0, Lcax;->e:Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;

    invoke-static {v1}, Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;->a(Lcom/google/android/apps/moviemaker/ui/ProgressSpinner;)F

    move-result v1

    cmpg-float v1, v1, v5

    if-gez v1, :cond_3

    move v5, v2

    .line 246
    :cond_0
    :goto_1
    float-to-double v0, v0

    const-wide v2, 0x3fb999999999999aL    # 0.1

    cmpl-double v0, v0, v2

    if-lez v0, :cond_1

    .line 247
    iget-object v1, p0, Lcax;->a:Landroid/graphics/RectF;

    mul-int/lit16 v0, v8, 0x168

    int-to-float v0, v0

    const v2, 0x38d1b717    # 1.0E-4f

    mul-float/2addr v0, v2

    const/high16 v2, -0x3d4c0000    # -90.0f

    add-float/2addr v2, v0

    const v0, -0x3c4c0003    # -359.9999f

    .line 251
    invoke-static {v5, v0, v7}, Lcfn;->a(FFF)F

    move-result v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcax;->d:Landroid/graphics/Paint;

    move-object v0, p1

    .line 247
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 255
    :cond_1
    return-void

    .line 198
    :cond_2
    invoke-virtual {p0}, Lcax;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    div-int/lit8 v0, v0, 0x5

    int-to-float v1, v0

    .line 199
    invoke-virtual {p0}, Lcax;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    div-int/lit8 v0, v0, 0xa

    int-to-float v0, v0

    goto :goto_0

    .line 216
    :cond_3
    const/16 v1, 0x9c4

    if-gt v8, v1, :cond_4

    move v1, v2

    move v4, v5

    .line 230
    :goto_2
    iget-object v9, p0, Lcax;->b:Landroid/animation/TimeInterpolator;

    int-to-float v10, v8

    sub-float v4, v10, v4

    const v10, 0x451c4000    # 2500.0f

    div-float/2addr v4, v10

    invoke-interface {v9, v4}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v4

    mul-float/2addr v2, v4

    sub-float v1, v2, v1

    .line 234
    cmpl-float v2, v1, v3

    if-ltz v2, :cond_7

    cmpg-float v2, v1, v5

    if-gez v2, :cond_7

    .line 241
    :goto_3
    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v1

    const v2, 0x43b38000    # 359.0f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    move v5, v7

    .line 242
    goto :goto_1

    .line 219
    :cond_4
    const/16 v1, 0x1388

    if-gt v8, v1, :cond_5

    .line 221
    const v1, 0x451c4000    # 2500.0f

    move v4, v1

    move v1, v3

    goto :goto_2

    .line 222
    :cond_5
    const/16 v1, 0x1d4c

    if-gt v8, v1, :cond_6

    .line 224
    const v1, 0x459c4000    # 5000.0f

    move v4, v1

    move v1, v2

    goto :goto_2

    .line 227
    :cond_6
    const v1, 0x45ea6000    # 7500.0f

    move v4, v1

    move v1, v3

    goto :goto_2

    .line 236
    :cond_7
    cmpg-float v2, v1, v3

    if-gez v2, :cond_8

    cmpl-float v2, v1, v6

    if-lez v2, :cond_8

    move v5, v6

    .line 237
    goto :goto_3

    :cond_8
    move v5, v1

    goto :goto_3
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 259
    const/4 v0, -0x3

    return v0
.end method

.method public setAlpha(I)V
    .locals 0

    .prologue
    .line 265
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 270
    return-void
.end method
