.class public Lakd;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/Object;

.field private final c:Lakc;

.field private final d:Labx;

.field private volatile e:Z

.field private f:J

.field private g:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lakd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lakd;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lacs;Lbjf;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 83
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lakd;->b:Ljava/lang/Object;

    .line 104
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lakd;->f:J

    .line 110
    const-string v0, "mffContext"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 111
    const-string v0, "bitmapFactory"

    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 113
    new-instance v0, Lakc;

    invoke-direct {v0, p1, p2}, Lakc;-><init>(Lacs;Lbjf;)V

    iput-object v0, p0, Lakd;->c:Lakc;

    .line 114
    new-instance v0, Labx;

    invoke-direct {v0, p1}, Labx;-><init>(Lacs;)V

    iput-object v0, p0, Lakd;->d:Labx;

    .line 115
    return-void
.end method

.method static synthetic a(Lakd;)J
    .locals 2

    .prologue
    .line 44
    iget-wide v0, p0, Lakd;->f:J

    return-wide v0
.end method

.method static synthetic a(Lakd;J)J
    .locals 1

    .prologue
    .line 44
    iput-wide p1, p0, Lakd;->g:J

    return-wide p1
.end method

.method static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 458
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 461
    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v5

    array-length v6, v5

    move v3, v1

    move v0, v2

    :goto_0
    if-ge v3, v6, :cond_2

    aget-char v7, v5, v3

    .line 462
    const/16 v8, 0x5f

    if-ne v7, v8, :cond_0

    move v0, v1

    .line 461
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 464
    :cond_0
    if-eqz v0, :cond_1

    .line 465
    invoke-static {v7}, Ljava/lang/Character;->toLowerCase(C)C

    move-result v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 467
    :cond_1
    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move v0, v2

    .line 468
    goto :goto_1

    .line 472
    :cond_2
    const-string v0, "Emitter"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Laan;Landroid/net/Uri;Lbkm;Laka;Laki;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laan;",
            "Landroid/net/Uri;",
            "Lbkm;",
            "Laka",
            "<*>;",
            "Laki;",
            ")V"
        }
    .end annotation

    .prologue
    .line 223
    iget-object v7, p0, Lakd;->b:Ljava/lang/Object;

    monitor-enter v7

    .line 224
    :try_start_0
    iget-boolean v8, p0, Lakd;->e:Z

    .line 225
    if-nez v8, :cond_0

    .line 226
    iget-object v0, p0, Lakd;->d:Labx;

    invoke-virtual {p1, v0}, Laan;->a(Labx;)V

    .line 227
    iget-object v9, p0, Lakd;->d:Labx;

    new-instance v0, Lakj;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lakj;-><init>(Lakd;Laan;Landroid/net/Uri;Lbkm;Laka;Laki;)V

    invoke-virtual {v9, v0}, Labx;->a(Lacg;)V

    .line 229
    iget-object v0, p0, Lakd;->d:Labx;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Labx;->a(Z)V

    .line 230
    iget-object v0, p0, Lakd;->d:Labx;

    invoke-virtual {v0, p1}, Labx;->a(Laan;)V

    .line 232
    :cond_0
    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 234
    if-eqz v8, :cond_1

    .line 235
    invoke-interface {p5, p2, p3}, Laki;->b(Landroid/net/Uri;Lbkm;)V

    .line 237
    :cond_1
    return-void

    .line 232
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private a(Laan;Lbkm;Lahz;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Laan;",
            "Lbkm;",
            "Lahz;",
            ")V"
        }
    .end annotation

    .prologue
    .line 371
    invoke-virtual {p3}, Lahz;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lakd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Laan;->a(Ljava/lang/String;)Laak;

    move-result-object v0

    .line 373
    if-nez v0, :cond_0

    .line 375
    sget-object v0, Lakd;->a:Ljava/lang/String;

    invoke-virtual {p3}, Lahz;->name()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 376
    invoke-virtual {p3}, Lahz;->name()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lakd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1c

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unknown filter for output "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 401
    :goto_0
    return-void

    .line 380
    :cond_0
    sget-object v1, Lakg;->a:[I

    iget-object v2, p3, Lahz;->a:Laia;

    invoke-virtual {v2}, Laia;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 400
    sget-object v0, Lakd;->a:Ljava/lang/String;

    iget-object v0, p3, Lahz;->a:Laia;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x15

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unknown output type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 384
    :pswitch_0
    check-cast v0, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter;

    new-instance v1, Lakb;

    iget-object v2, p3, Lahz;->b:Lbmz;

    invoke-direct {v1, p2, v2}, Lakb;-><init>(Lbkm;Lbmz;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BaseEmitterFilter;->a(Lbbp;)V

    goto :goto_0

    .line 391
    :pswitch_1
    check-cast v0, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;

    new-instance v1, Lakk;

    invoke-direct {v1, p0, p2}, Lakk;-><init>(Lakd;Lbkm;)V

    .line 392
    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/stabilizer/StabilizationAnalysisFilter;->a(Lbxj;)V

    goto :goto_0

    .line 395
    :pswitch_2
    check-cast v0, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;

    new-instance v1, Lakh;

    invoke-direct {v1, p2}, Lakh;-><init>(Lbkm;)V

    .line 396
    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/AutoColorCorrectAnalysisFilter;->a(Lbbr;)V

    goto :goto_0

    .line 380
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorAccumulationFilter;Lbku;JZ)V
    .locals 4

    .prologue
    .line 435
    if-eqz p4, :cond_1

    sget-object v0, Lbmz;->v:Lbmz;

    .line 439
    :goto_0
    invoke-virtual {p1, v0}, Lbku;->b(Lbmz;)Landroid/util/LongSparseArray;

    move-result-object v0

    .line 441
    invoke-static {v0, p2, p3}, Lcfk;->b(Landroid/util/LongSparseArray;J)I

    move-result v1

    .line 442
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 444
    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [J

    .line 445
    invoke-virtual {p0, v0}, Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorAccumulationFilter;->a([J)V

    .line 448
    :cond_0
    return-void

    .line 435
    :cond_1
    sget-object v0, Lbmz;->w:Lbmz;

    goto :goto_0
.end method

.method static synthetic b(Lakd;)J
    .locals 2

    .prologue
    .line 44
    iget-wide v0, p0, Lakd;->g:J

    return-wide v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lakd;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lakd;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lakd;->b:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic d(Lakd;)Z
    .locals 1

    .prologue
    .line 44
    iget-boolean v0, p0, Lakd;->e:Z

    return v0
.end method

.method static synthetic e(Lakd;)Labx;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lakd;->d:Labx;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 344
    iget-object v1, p0, Lakd;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 346
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lakd;->e:Z

    .line 347
    iget-object v0, p0, Lakd;->d:Labx;

    invoke-virtual {v0}, Labx;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lakd;->d:Labx;

    invoke-virtual {v0}, Labx;->d()V

    .line 352
    iget-object v0, p0, Lakd;->d:Labx;

    invoke-virtual {v0}, Labx;->c()V

    .line 358
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 217
    iput-wide p1, p0, Lakd;->f:J

    .line 218
    return-void
.end method

.method public a(Landroid/net/Uri;Laic;Lbkr;Laki;)V
    .locals 14

    .prologue
    .line 119
    new-instance v6, Laka;

    invoke-direct {v6}, Laka;-><init>()V

    .line 120
    if-nez p3, :cond_0

    .line 122
    new-instance v7, Lbkm;

    invoke-direct {v7}, Lbkm;-><init>()V

    .line 125
    :goto_0
    new-instance v8, Lakn;

    new-instance v2, Lake;

    move-object v3, p0

    move-object/from16 v4, p4

    move-object v5, p1

    invoke-direct/range {v2 .. v7}, Lake;-><init>(Lakd;Laki;Landroid/net/Uri;Laka;Lbkm;)V

    invoke-direct {v8, v2}, Lakn;-><init>(Lako;)V

    .line 157
    new-instance v3, Lakf;

    move-object/from16 v0, p4

    invoke-direct {v3, v0}, Lakf;-><init>(Laki;)V

    .line 165
    const-wide/16 v4, -0x1

    iput-wide v4, p0, Lakd;->g:J

    .line 166
    const/4 v2, 0x0

    iput-boolean v2, p0, Lakd;->e:Z

    .line 168
    :try_start_0
    iget-object v2, p0, Lakd;->c:Lakc;

    invoke-virtual {v2, p1}, Lakc;->a(Landroid/net/Uri;)Laan;

    move-result-object v9

    sget-object v4, Lahx;->a:[Lahz;

    array-length v5, v4

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v5, :cond_1

    aget-object v10, v4, v2

    iget-object v11, v10, Lahz;->a:Laia;

    iget-object v11, v11, Laia;->f:Ljava/lang/Class;

    invoke-direct {p0, v9, v7, v10}, Lakd;->a(Laan;Lbkm;Lahz;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 123
    :cond_0
    invoke-static/range {p3 .. p3}, Lbkl;->a(Lbkr;)Lbkm;

    move-result-object v7

    goto :goto_0

    .line 168
    :cond_1
    :try_start_1
    const-string v2, "frameNumberEmitter"

    invoke-virtual {v9, v2}, Laan;->a(Ljava/lang/String;)Laak;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/StatisticEmitterFilter;

    invoke-virtual {v2, v6}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/StatisticEmitterFilter;->a(Lbbp;)V

    const-string v2, "frameTimestampEmitter"

    invoke-virtual {v9, v2}, Laan;->a(Ljava/lang/String;)Laak;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/TimestampEmitterFilter;

    invoke-virtual {v2, v8}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/TimestampEmitterFilter;->a(Lbbp;)V

    const-string v2, "frameBitmapEmitter"

    invoke-virtual {v9, v2}, Laan;->a(Ljava/lang/String;)Laak;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BitmapEmitterFilter;

    invoke-virtual {v2, v3}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/BitmapEmitterFilter;->a(Lbbp;)V

    if-eqz p3, :cond_2

    invoke-interface/range {p3 .. p3}, Lbkr;->c()Lbkn;

    move-result-object v2

    invoke-interface/range {p3 .. p3}, Lbkr;->d()Lbku;

    move-result-object v8

    if-eqz v2, :cond_2

    if-eqz v8, :cond_2

    invoke-virtual {v2}, Lbkn;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_2

    sget-object v2, Lbmz;->o:Lbmz;

    invoke-virtual {v8, v2}, Lbku;->b(Lbmz;)Landroid/util/LongSparseArray;

    move-result-object v2

    invoke-virtual {v2}, Landroid/util/LongSparseArray;->size()I

    move-result v3

    if-nez v3, :cond_3

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    move-object v5, v2

    :goto_2
    const-string v2, "frameCounter"

    invoke-virtual {v9, v2}, Laan;->a(Ljava/lang/String;)Laak;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FrameCountFilter;

    iget-object v3, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/google/android/apps/moviemaker/filterpacks/miscellaneous/FrameCountFilter;->a(I)V

    const-string v2, "saliencyMapVerticalProjectionsAccumulationFilter"

    invoke-virtual {v9, v2}, Laan;->a(Ljava/lang/String;)Laak;

    move-result-object v2

    check-cast v2, Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorAccumulationFilter;

    const-string v3, "saliencyMapHorizontalProjectionsAccumulationFilter"

    invoke-virtual {v9, v3}, Laan;->a(Ljava/lang/String;)Laak;

    move-result-object v3

    check-cast v3, Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorAccumulationFilter;

    iget-object v4, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const/4 v4, 0x1

    invoke-static {v2, v8, v10, v11, v4}, Lakd;->a(Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorAccumulationFilter;Lbku;JZ)V

    iget-object v2, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const/4 v2, 0x0

    invoke-static {v3, v8, v10, v11, v2}, Lakd;->a(Lcom/google/android/apps/moviemaker/filterpacks/numeric/VectorAccumulationFilter;Lbku;JZ)V

    const-string v2, "decoderStartTime"

    invoke-virtual {v9, v2}, Laan;->b(Ljava/lang/String;)Landroidx/media/filterpacks/base/VariableSource;

    move-result-object v2

    iget-object v3, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Landroidx/media/filterpacks/base/VariableSource;->a(Ljava/lang/Object;)V

    :cond_2
    move-object/from16 v0, p2

    iget-object v2, v0, Laic;->c:[Lahz;

    array-length v2, v2

    if-eqz v2, :cond_5

    new-instance v3, Lajz;

    invoke-direct {v3, v9}, Lajz;-><init>(Laan;)V

    const-string v2, "decoder"

    invoke-virtual {v3, v2}, Lajz;->a(Ljava/lang/String;)V

    move-object/from16 v0, p2

    iget-object v2, v0, Laic;->c:[Lahz;

    array-length v2, v2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p2

    iget-object v5, v0, Laic;->c:[Lahz;

    array-length v5, v5

    if-ge v2, v5, :cond_4

    move-object/from16 v0, p2

    iget-object v5, v0, Laic;->c:[Lahz;

    aget-object v5, v5, v2

    invoke-virtual {v5}, Lahz;->name()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lakd;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_3
    invoke-virtual {v2}, Landroid/util/LongSparseArray;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    move-object v5, v2

    goto/16 :goto_2

    :cond_4
    invoke-virtual {v3, v4}, Lajz;->a([Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_5
    move-object v8, p0

    move-object v10, p1

    move-object v11, v7

    move-object v12, v6

    move-object/from16 v13, p4

    .line 186
    invoke-direct/range {v8 .. v13}, Lakd;->a(Laan;Landroid/net/Uri;Lbkm;Laka;Laki;)V

    .line 187
    :goto_4
    return-void

    .line 176
    :catch_0
    move-exception v2

    .line 177
    sget-object v3, Lakd;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x18

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Unable to analyze video "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 178
    move-object/from16 v0, p4

    invoke-interface {v0, p1, v2}, Laki;->a(Landroid/net/Uri;Ljava/lang/Exception;)V

    goto :goto_4

    .line 180
    :catch_1
    move-exception v2

    .line 181
    sget-object v3, Lakd;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x18

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Unable to analyze video "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 182
    move-object/from16 v0, p4

    invoke-interface {v0, p1, v2}, Laki;->a(Landroid/net/Uri;Ljava/lang/Exception;)V

    goto :goto_4
.end method

.method public a(Landroid/net/Uri;Lbkm;Laki;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 190
    new-instance v4, Laka;

    invoke-direct {v4}, Laka;-><init>()V

    .line 191
    iput-boolean v0, p0, Lakd;->e:Z

    .line 195
    :try_start_0
    iget-object v1, p0, Lakd;->c:Lakc;

    invoke-virtual {v1, p1}, Lakc;->b(Landroid/net/Uri;)Laan;

    move-result-object v1

    sget-object v2, Lahx;->a:[Lahz;

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v5, v2, v0

    iget-object v6, v5, Lahz;->a:Laia;

    iget-object v6, v6, Laia;->f:Ljava/lang/Class;

    invoke-direct {p0, v1, p2, v5}, Lakd;->a(Laan;Lbkm;Lahz;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 196
    :catch_0
    move-exception v0

    .line 197
    sget-object v1, Lakd;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x18

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unable to analyze photo "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 198
    invoke-interface {p3, p1, v0}, Laki;->a(Landroid/net/Uri;Ljava/lang/Exception;)V

    .line 207
    :goto_1
    return-void

    .line 200
    :catch_1
    move-exception v0

    .line 201
    sget-object v1, Lakd;->a:Ljava/lang/String;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x18

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unable to analyze photo "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 202
    invoke-interface {p3, p1, v0}, Laki;->a(Landroid/net/Uri;Ljava/lang/Exception;)V

    goto :goto_1

    :cond_0
    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    .line 206
    invoke-direct/range {v0 .. v5}, Lakd;->a(Laan;Landroid/net/Uri;Lbkm;Laka;Laki;)V

    goto :goto_1
.end method
