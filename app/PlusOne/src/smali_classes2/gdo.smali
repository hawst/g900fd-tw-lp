.class public final Lgdo;
.super Ljava/lang/Object;

# interfaces
.implements Lgdn;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lgfz;)D
    .locals 2

    sget-object v0, Lgdk;->a:Lgft;

    invoke-interface {p1, v0}, Lgfz;->a(Lgft;)Lgfr;

    move-result-object v0

    check-cast v0, Lgkb;

    invoke-virtual {v0}, Lgkb;->j()D

    move-result-wide v0

    return-wide v0
.end method

.method public a(Lgfz;Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)Lggd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfz;",
            "Ljava/lang/String;",
            "Lcom/google/android/gms/cast/LaunchOptions;",
            ")",
            "Lggd",
            "<",
            "Lgdm;",
            ">;"
        }
    .end annotation

    new-instance v0, Lgdq;

    invoke-direct {v0, p2, p3}, Lgdq;-><init>(Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V

    invoke-interface {p1, v0}, Lgfz;->b(Lgfx;)Lgfx;

    move-result-object v0

    return-object v0
.end method

.method public a(Lgfz;Ljava/lang/String;Ljava/lang/String;)Lggd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfz;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lggd",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    new-instance v0, Lgdp;

    invoke-direct {v0, p2, p3}, Lgdp;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lgfz;->b(Lgfx;)Lgfx;

    move-result-object v0

    return-object v0
.end method

.method public a(Lgfz;Ljava/lang/String;Z)Lggd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfz;",
            "Ljava/lang/String;",
            "Z)",
            "Lggd",
            "<",
            "Lgdm;",
            ">;"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    new-instance v0, Lgea;

    invoke-direct {v0}, Lgea;-><init>()V

    invoke-virtual {v0, p3}, Lgea;->a(Z)Lgea;

    move-result-object v0

    invoke-virtual {v0}, Lgea;->a()Lcom/google/android/gms/cast/LaunchOptions;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lgdo;->a(Lgfz;Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)Lggd;

    move-result-object v0

    return-object v0
.end method

.method public a(Lgfz;D)V
    .locals 2

    :try_start_0
    sget-object v0, Lgdk;->a:Lgft;

    invoke-interface {p1, v0}, Lgfz;->a(Lgft;)Lgfr;

    move-result-object v0

    check-cast v0, Lgkb;

    invoke-virtual {v0, p2, p3}, Lgkb;->a(D)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "service error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lgfz;Ljava/lang/String;)V
    .locals 2

    :try_start_0
    sget-object v0, Lgdk;->a:Lgft;

    invoke-interface {p1, v0}, Lgfz;->a(Lgft;)Lgfr;

    move-result-object v0

    check-cast v0, Lgkb;

    invoke-virtual {v0, p2}, Lgkb;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "service error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Lgfz;Ljava/lang/String;Lgdu;)V
    .locals 2

    :try_start_0
    sget-object v0, Lgdk;->a:Lgft;

    invoke-interface {p1, v0}, Lgfz;->a(Lgft;)Lgfr;

    move-result-object v0

    check-cast v0, Lgkb;

    invoke-virtual {v0, p2, p3}, Lgkb;->a(Ljava/lang/String;Lgdu;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "service error"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
