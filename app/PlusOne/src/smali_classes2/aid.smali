.class public final Laid;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lajb;


# instance fields
.field private final a:Laqc;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private c:J

.field private d:J

.field private e:I


# direct methods
.method public constructor <init>(Laqc;Ljava/util/List;Ljava/util/List;Ljava/util/Map;JI)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laqc;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/Long;",
            ">;JI)V"
        }
    .end annotation

    .prologue
    const-wide/32 v2, 0x493e0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    const/4 v0, -0x1

    iput v0, p0, Laid;->e:I

    .line 59
    int-to-long v0, p7

    mul-long/2addr v0, v2

    add-long/2addr v0, p5

    iput-wide v0, p0, Laid;->c:J

    .line 61
    const-string v0, "playerScreenController"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laqc;

    iput-object v0, p0, Laid;->a:Laqc;

    .line 64
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, p4}, Ljava/util/HashMap;-><init>(Ljava/util/Map;)V

    iput-object v0, p0, Laid;->b:Ljava/util/Map;

    .line 66
    invoke-direct {p0, p2}, Laid;->a(Ljava/util/List;)V

    .line 67
    invoke-direct {p0, p3, v2, v3}, Laid;->a(Ljava/util/List;J)V

    .line 69
    invoke-direct {p0}, Laid;->a()J

    move-result-wide v0

    iput-wide v0, p0, Laid;->d:J

    .line 70
    iget-wide v0, p0, Laid;->d:J

    const-string v2, "mTotalMediaTime"

    invoke-static {v0, v1, v2}, Lcgp;->a(JLjava/lang/CharSequence;)J

    .line 72
    iget-wide v0, p0, Laid;->c:J

    const-wide/16 v2, 0x64

    mul-long/2addr v0, v2

    iget-wide v2, p0, Laid;->d:J

    div-long/2addr v0, v2

    long-to-int v0, v0

    invoke-direct {p0, v0}, Laid;->a(I)V

    .line 73
    return-void
.end method

.method private a()J
    .locals 5

    .prologue
    .line 95
    iget-wide v0, p0, Laid;->c:J

    .line 96
    iget-object v2, p0, Laid;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 97
    add-long/2addr v0, v2

    move-wide v2, v0

    .line 98
    goto :goto_0

    .line 99
    :cond_0
    return-wide v2
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Laid;->e:I

    if-le p1, v0, :cond_0

    .line 104
    iget-object v0, p0, Laid;->a:Laqc;

    invoke-virtual {v0, p1}, Laqc;->b(I)V

    .line 105
    iput p1, p0, Laid;->e:I

    .line 107
    :cond_0
    return-void
.end method

.method private a(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 79
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 80
    iget-object v2, p0, Laid;->b:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x22

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "No approximate duration given for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcec;->a(ZLjava/lang/CharSequence;)V

    goto :goto_0

    .line 83
    :cond_0
    return-void
.end method

.method private a(Ljava/util/List;J)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 89
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 90
    iget-object v2, p0, Laid;->b:Ljava/util/Map;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 92
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Laid;->a:Laqc;

    invoke-virtual {v0, p1}, Laqc;->a(Landroid/graphics/Bitmap;)V

    .line 128
    return-void
.end method

.method public a(Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 132
    iget-object v0, p0, Laid;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, p1, v0, v1}, Laid;->a(Landroid/net/Uri;J)V

    .line 133
    iget-wide v2, p0, Laid;->c:J

    iget-object v0, p0, Laid;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    add-long/2addr v0, v2

    iput-wide v0, p0, Laid;->c:J

    .line 134
    return-void
.end method

.method public a(Landroid/net/Uri;J)V
    .locals 4

    .prologue
    .line 118
    iget-object v0, p0, Laid;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 119
    invoke-static {p2, p3, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 120
    iget-wide v2, p0, Laid;->c:J

    add-long/2addr v0, v2

    const-wide/16 v2, 0x64

    mul-long/2addr v0, v2

    iget-wide v2, p0, Laid;->d:J

    div-long/2addr v0, v2

    long-to-int v0, v0

    .line 122
    invoke-direct {p0, v0}, Laid;->a(I)V

    .line 123
    return-void
.end method
