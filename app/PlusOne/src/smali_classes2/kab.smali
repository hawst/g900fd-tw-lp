.class Lkab;
.super Lhny;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Lkzw;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 15
    const-class v0, Lkab;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "UpdateVote"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lkab;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Lkzw;)V
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lkab;->a:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 24
    iput-object p1, p0, Lkab;->b:Landroid/content/Context;

    .line 25
    iput p2, p0, Lkab;->c:I

    .line 26
    iput-object p3, p0, Lkab;->d:Ljava/lang/String;

    .line 27
    iput-object p4, p0, Lkab;->e:Lkzw;

    .line 28
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 4

    .prologue
    .line 33
    :try_start_0
    iget-object v0, p0, Lkab;->b:Landroid/content/Context;

    const-class v1, Ljxn;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxn;

    iget v1, p0, Lkab;->c:I

    iget-object v2, p0, Lkab;->d:Ljava/lang/String;

    iget-object v3, p0, Lkab;->e:Lkzw;

    invoke-interface {v0, v1, v2, v3}, Ljxn;->a(ILjava/lang/String;Lkzw;)V

    .line 34
    new-instance v0, Lhoz;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lhoz;-><init>(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    :goto_0
    return-object v0

    .line 35
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 36
    new-instance v0, Lhoz;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v2, v1, v3}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method
