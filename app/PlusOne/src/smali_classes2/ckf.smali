.class public final Lckf;
.super Lckg;
.source "PG"


# instance fields
.field private final a:I

.field private final b:[I

.field private c:I

.field private synthetic d:Lcjy;


# direct methods
.method public constructor <init>(Lcjy;I[I)V
    .locals 0

    .prologue
    .line 822
    iput-object p1, p0, Lckf;->d:Lcjy;

    invoke-direct {p0}, Lckg;-><init>()V

    .line 823
    iput p2, p0, Lckf;->a:I

    .line 824
    iput-object p3, p0, Lckf;->b:[I

    .line 825
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 833
    iget-object v0, p0, Lckf;->b:[I

    array-length v0, v0

    div-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 828
    iput p1, p0, Lckf;->c:I

    .line 829
    return-void
.end method

.method public a(Ljava/lang/Integer;)[Ljava/lang/Object;
    .locals 4

    .prologue
    .line 843
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0}, Lckf;->a()I

    move-result v1

    if-lt v0, v1, :cond_1

    .line 844
    :cond_0
    const/4 v0, 0x0

    .line 846
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    iget-object v2, p0, Lckf;->b:[I

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    shl-int/lit8 v3, v3, 0x1

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lckf;->b:[I

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    shl-int/lit8 v3, v3, 0x1

    add-int/lit8 v3, v3, 0x1

    aget v2, v2, v3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    goto :goto_0
.end method

.method public b(I)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 838
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/Integer;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 852
    iget-object v0, p0, Lckf;->d:Lcjy;

    iget v1, p0, Lckf;->a:I

    invoke-virtual {v0, v1, p1}, Lcjy;->b(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/Integer;)Z
    .locals 1

    .prologue
    .line 857
    iget v0, p0, Lckf;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
