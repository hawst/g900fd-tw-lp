.class public final Lamb;
.super Lamn;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lavw;

.field private final c:Laxt;

.field private final d:Laxz;

.field private final e:Laxu;

.field private final f:Lcdu;

.field private final g:Lamd;

.field private final h:Laxw;

.field private final j:Lalg;

.field private final k:Lhob;

.field private final l:Lawk;

.field private final m:Lasn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lasn",
            "<",
            "Lamk;",
            ">;"
        }
    .end annotation
.end field

.field private final n:Lasn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lasn",
            "<",
            "Lami;",
            ">;"
        }
    .end annotation
.end field

.field private final o:Lamj;

.field private p:Landroid/os/AsyncTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/AsyncTask",
            "<***>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lamb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lamb;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lavw;Laxt;Laxz;Laxu;Lcdu;Lamd;Laxw;Lalg;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 114
    invoke-direct {p0}, Lamn;-><init>()V

    .line 94
    new-instance v0, Laml;

    invoke-direct {v0, p0}, Laml;-><init>(Lamb;)V

    iput-object v0, p0, Lamb;->k:Lhob;

    .line 96
    new-instance v0, Lamc;

    invoke-direct {v0, p0}, Lamc;-><init>(Lamb;)V

    iput-object v0, p0, Lamb;->l:Lawk;

    .line 97
    new-instance v0, Lamh;

    invoke-direct {v0, p0}, Lamh;-><init>(Lamb;)V

    iput-object v0, p0, Lamb;->m:Lasn;

    .line 98
    new-instance v0, Lamf;

    invoke-direct {v0, p0}, Lamf;-><init>(Lamb;)V

    iput-object v0, p0, Lamb;->n:Lasn;

    .line 100
    new-instance v0, Lame;

    invoke-direct {v0, p0}, Lame;-><init>(Lamb;)V

    iput-object v0, p0, Lamb;->o:Lamj;

    .line 115
    const-string v0, "state"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavw;

    iput-object v0, p0, Lamb;->b:Lavw;

    .line 116
    const-string v0, "backgroundTaskManagerClient"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxt;

    iput-object v0, p0, Lamb;->c:Laxt;

    .line 118
    const-string v0, "saveCloudStoryboardTaskFactory"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxz;

    iput-object v0, p0, Lamb;->d:Laxz;

    .line 120
    const-string v0, "connectivityChecker"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxu;

    iput-object v0, p0, Lamb;->e:Laxu;

    .line 121
    const-string v0, "analyticsSession"

    invoke-static {p5, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    iput-object v0, p0, Lamb;->f:Lcdu;

    .line 122
    const-string v0, "display"

    invoke-static {p6, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamd;

    iput-object v0, p0, Lamb;->g:Lamd;

    .line 123
    const-string v0, "refreshTaskFactory"

    invoke-static {p7, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxw;

    iput-object v0, p0, Lamb;->h:Laxw;

    .line 124
    const-string v0, "asyncTaskRunner"

    invoke-static {p8, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lalg;

    iput-object v0, p0, Lamb;->j:Lalg;

    .line 125
    return-void
.end method

.method static synthetic a(Lamb;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lamb;->p:Landroid/os/AsyncTask;

    return-object p1
.end method

.method static synthetic a(Lamb;)Lasn;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lamb;->m:Lasn;

    return-object v0
.end method

.method static synthetic a(Lamb;Lamk;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lamb;->c(Lamk;)V

    return-void
.end method

.method static synthetic b(Lamb;)Lcdu;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lamb;->f:Lcdu;

    return-object v0
.end method

.method static synthetic b(Lamb;Lamk;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lamb;->c(Lamk;)V

    return-void
.end method

.method static synthetic c(Lamb;)Lavw;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lamb;->b:Lavw;

    return-object v0
.end method

.method private c(Lamk;)V
    .locals 2

    .prologue
    .line 188
    invoke-virtual {p0}, Lamb;->e()V

    .line 189
    iget-object v0, p0, Lamb;->b:Lavw;

    invoke-interface {v0}, Lavw;->bd()Lawg;

    move-result-object v0

    sget-object v1, Lawg;->c:Lawg;

    if-eq v0, v1, :cond_0

    .line 195
    :goto_0
    return-void

    .line 192
    :cond_0
    iget-object v0, p0, Lamb;->c:Laxt;

    const-string v1, "SaveCloudStoryboardTask"

    .line 193
    invoke-virtual {v0, v1}, Laxt;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lamb;->p:Landroid/os/AsyncTask;

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    .line 192
    :goto_1
    invoke-interface {p1, v0}, Lamk;->a(Z)V

    goto :goto_0

    .line 193
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic d(Lamb;)Laxw;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lamb;->h:Laxw;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lamb;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lamb;)Lalg;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lamb;->j:Lalg;

    return-object v0
.end method

.method static synthetic f(Lamb;)Lamd;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lamb;->g:Lamd;

    return-object v0
.end method

.method static synthetic g(Lamb;)Lamj;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lamb;->o:Lamj;

    return-object v0
.end method

.method static synthetic h(Lamb;)Landroid/os/AsyncTask;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lamb;->p:Landroid/os/AsyncTask;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 129
    invoke-super {p0}, Lamn;->a()V

    .line 130
    iget-object v0, p0, Lamb;->c:Laxt;

    iget-object v1, p0, Lamb;->k:Lhob;

    invoke-virtual {v0, v1}, Laxt;->a(Lhob;)Laxt;

    .line 131
    iget-object v0, p0, Lamb;->b:Lavw;

    iget-object v1, p0, Lamb;->l:Lawk;

    invoke-interface {v0, v1}, Lavw;->k(Lawk;)V

    .line 132
    iget-object v0, p0, Lamb;->b:Lavw;

    iget-object v1, p0, Lamb;->l:Lawk;

    invoke-interface {v0, v1}, Lavw;->J(Lawk;)V

    .line 133
    iget-object v0, p0, Lamb;->m:Lasn;

    invoke-virtual {v0}, Lasn;->a()V

    .line 134
    iget-object v0, p0, Lamb;->n:Lasn;

    invoke-virtual {v0}, Lasn;->a()V

    .line 135
    return-void
.end method

.method public a(Lami;)V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lamb;->n:Lasn;

    invoke-virtual {v0, p1}, Lasn;->c(Ljava/lang/Object;)V

    .line 161
    return-void
.end method

.method public a(Lamk;)V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lamb;->m:Lasn;

    invoke-virtual {v0, p1}, Lasn;->c(Ljava/lang/Object;)V

    .line 153
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lamb;->b:Lavw;

    iget-object v1, p0, Lamb;->l:Lawk;

    invoke-interface {v0, v1}, Lavw;->K(Lawk;)V

    .line 140
    iget-object v0, p0, Lamb;->b:Lavw;

    iget-object v1, p0, Lamb;->l:Lawk;

    invoke-interface {v0, v1}, Lavw;->l(Lawk;)V

    .line 141
    iget-object v0, p0, Lamb;->c:Laxt;

    iget-object v1, p0, Lamb;->k:Lhob;

    invoke-virtual {v0, v1}, Laxt;->b(Lhob;)Laxt;

    .line 142
    iget-object v0, p0, Lamb;->m:Lasn;

    invoke-virtual {v0}, Lasn;->b()V

    .line 143
    iget-object v0, p0, Lamb;->n:Lasn;

    invoke-virtual {v0}, Lasn;->b()V

    .line 144
    iget-object v0, p0, Lamb;->p:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lamb;->p:Landroid/os/AsyncTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lamb;->p:Landroid/os/AsyncTask;

    .line 148
    :cond_0
    invoke-super {p0}, Lamn;->b()V

    .line 149
    return-void
.end method

.method public b(Lami;)V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lamb;->n:Lasn;

    invoke-virtual {v0, p1}, Lasn;->d(Ljava/lang/Object;)V

    .line 165
    return-void
.end method

.method public b(Lamk;)V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lamb;->m:Lasn;

    invoke-virtual {v0, p1}, Lasn;->d(Ljava/lang/Object;)V

    .line 157
    return-void
.end method

.method public c()V
    .locals 7

    .prologue
    .line 169
    iget-object v0, p0, Lamb;->e:Laxu;

    invoke-virtual {v0}, Laxu;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 170
    iget-object v0, p0, Lamb;->m:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lamk;

    invoke-interface {v0}, Lamk;->c()V

    .line 181
    :goto_0
    return-void

    .line 173
    :cond_0
    iget-object v0, p0, Lamb;->m:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lamk;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lamk;->a(Z)V

    .line 174
    iget-object v0, p0, Lamb;->b:Lavw;

    invoke-interface {v0}, Lavw;->bg()Lood;

    move-result-object v2

    .line 175
    iget-object v6, p0, Lamb;->c:Laxt;

    iget-object v0, p0, Lamb;->d:Laxz;

    iget-object v1, p0, Lamb;->b:Lavw;

    .line 176
    invoke-interface {v1}, Lavw;->ba()I

    move-result v1

    .line 178
    invoke-static {v2}, Lcge;->a(Loxu;)[B

    move-result-object v3

    iget-object v4, p0, Lamb;->b:Lavw;

    .line 179
    invoke-interface {v4}, Lavw;->bf()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lamb;->b:Lavw;

    .line 180
    invoke-interface {v5}, Lavw;->aR()Ljdz;

    move-result-object v5

    .line 175
    invoke-virtual/range {v0 .. v5}, Laxz;->a(ILood;[BLjava/lang/String;Ljdz;)Laxy;

    move-result-object v0

    invoke-virtual {v6, v0}, Laxt;->a(Lhny;)V

    goto :goto_0
.end method
