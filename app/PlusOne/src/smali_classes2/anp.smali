.class final Lanp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lahy;


# instance fields
.field private synthetic a:Lanj;


# direct methods
.method constructor <init>(Lanj;)V
    .locals 0

    .prologue
    .line 449
    iput-object p1, p0, Lanp;->a:Lanj;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;)V
    .locals 0

    .prologue
    .line 511
    return-void
.end method

.method public a(Landroid/net/Uri;J)V
    .locals 4

    .prologue
    .line 501
    iget-object v0, p0, Lanp;->a:Lanj;

    invoke-static {v0}, Lanj;->i(Lanj;)Lbmu;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lanp;->a:Lanj;

    invoke-static {v0}, Lanj;->i(Lanj;)Lbmu;

    move-result-object v0

    iget-object v0, v0, Lbmu;->e:Lbmv;

    sget-object v1, Lbmv;->a:Lbmv;

    if-ne v0, v1, :cond_0

    .line 502
    iget-object v0, p0, Lanp;->a:Lanj;

    invoke-static {v0}, Lanj;->i(Lanj;)Lbmu;

    move-result-object v0

    invoke-virtual {v0}, Lbmu;->a()Lboo;

    move-result-object v0

    iget-wide v0, v0, Lboo;->f:J

    .line 503
    long-to-float v2, p2

    long-to-float v0, v0

    div-float v0, v2, v0

    const/high16 v1, 0x42c80000    # 100.0f

    mul-float/2addr v1, v0

    .line 504
    iget-object v0, p0, Lanp;->a:Lanj;

    invoke-static {v0}, Lanj;->h(Lanj;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lanl;

    const/16 v2, 0x64

    float-to-int v1, v1

    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-interface {v0, v1}, Lanl;->a_(I)V

    .line 506
    :cond_0
    return-void
.end method

.method public a(Landroid/net/Uri;Laic;Lbkr;)V
    .locals 3

    .prologue
    .line 456
    iget-object v0, p0, Lanp;->a:Lanj;

    iget-boolean v0, v0, Lamn;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lanp;->a:Lanj;

    invoke-static {v0}, Lanj;->b(Lanj;)Lavz;

    move-result-object v0

    iget-object v0, v0, Lavz;->a:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 466
    :cond_0
    :goto_0
    return-void

    .line 460
    :cond_1
    iget-object v0, p0, Lanp;->a:Lanj;

    invoke-static {v0}, Lanj;->m(Lanj;)V

    .line 461
    iget-object v0, p0, Lanp;->a:Lanj;

    invoke-static {v0}, Lanj;->d(Lanj;)Lawb;

    move-result-object v0

    invoke-static {p1}, Lavz;->b(Landroid/net/Uri;)Lavz;

    move-result-object v1

    iget-object v2, p0, Lanp;->a:Lanj;

    invoke-static {v2}, Lanj;->i(Lanj;)Lbmu;

    move-result-object v2

    invoke-interface {v0, v1, v2, p3}, Lawb;->a(Lavz;Lbmu;Lbkr;)V

    .line 462
    iget-object v0, p0, Lanp;->a:Lanj;

    iget-object v1, p0, Lanp;->a:Lanj;

    invoke-static {v1}, Lanj;->i(Lanj;)Lbmu;

    move-result-object v1

    invoke-static {v0, p1, v1, p3}, Lanj;->a(Lanj;Landroid/net/Uri;Lbmu;Lbkr;)V

    .line 463
    iget-object v0, p0, Lanp;->a:Lanj;

    invoke-static {v0}, Lanj;->d(Lanj;)Lawb;

    move-result-object v0

    invoke-interface {v0}, Lawb;->aM()V

    .line 464
    iget-object v0, p0, Lanp;->a:Lanj;

    invoke-static {v0}, Lanj;->l(Lanj;)V

    .line 465
    iget-object v0, p0, Lanp;->a:Lanj;

    invoke-static {v0}, Lanj;->c(Lanj;)V

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;Laic;Ljava/lang/Exception;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 489
    invoke-static {}, Lanj;->d()Ljava/lang/String;

    const-string v0, "Analysis of %s failed."

    new-array v1, v3, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 490
    iget-object v0, p0, Lanp;->a:Lanj;

    invoke-static {v0}, Lanj;->c(Lanj;)V

    .line 491
    iget-object v0, p0, Lanp;->a:Lanj;

    invoke-static {v0}, Lanj;->d(Lanj;)Lawb;

    move-result-object v0

    invoke-interface {v0}, Lawb;->aM()V

    .line 492
    iget-object v0, p0, Lanp;->a:Lanj;

    invoke-static {v0, v4}, Lanj;->a(Lanj;Lavz;)Lavz;

    .line 493
    iget-object v0, p0, Lanp;->a:Lanj;

    invoke-static {v0, v4}, Lanj;->a(Lanj;Lbmu;)Lbmu;

    .line 494
    iget-object v0, p0, Lanp;->a:Lanj;

    iget-boolean v0, v0, Lamn;->i:Z

    if-eqz v0, :cond_0

    .line 495
    iget-object v0, p0, Lanp;->a:Lanj;

    invoke-static {v0}, Lanj;->f(Lanj;)Lann;

    move-result-object v0

    invoke-interface {v0, v3, v3}, Lann;->a(IZ)V

    .line 497
    :cond_0
    return-void
.end method

.method public a(Landroid/net/Uri;Lbkr;)V
    .locals 0

    .prologue
    .line 516
    return-void
.end method

.method public b(Landroid/net/Uri;Laic;Lbkr;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 478
    iget-object v0, p0, Lanp;->a:Lanj;

    invoke-static {v0}, Lanj;->n(Lanj;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 479
    invoke-static {}, Lanj;->d()Ljava/lang/String;

    .line 480
    iget-object v0, p0, Lanp;->a:Lanj;

    invoke-static {v0}, Lanj;->c(Lanj;)V

    .line 481
    iget-object v0, p0, Lanp;->a:Lanj;

    invoke-static {v0}, Lanj;->d(Lanj;)Lawb;

    move-result-object v0

    invoke-interface {v0}, Lawb;->aM()V

    .line 482
    iget-object v0, p0, Lanp;->a:Lanj;

    invoke-static {v0, v1}, Lanj;->a(Lanj;Lavz;)Lavz;

    .line 483
    iget-object v0, p0, Lanp;->a:Lanj;

    invoke-static {v0, v1}, Lanj;->a(Lanj;Lbmu;)Lbmu;

    .line 485
    :cond_0
    return-void
.end method
