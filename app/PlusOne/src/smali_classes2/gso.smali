.class public final Lgso;
.super Ljava/lang/Object;


# static fields
.field static a:Lgte;


# direct methods
.method public static a(Lcom/google/android/gms/maps/model/LatLng;)Lgsn;
    .locals 2

    :try_start_0
    new-instance v0, Lgsn;

    invoke-static {}, Lgso;->a()Lgte;

    move-result-object v1

    invoke-interface {v1, p0}, Lgte;->a(Lcom/google/android/gms/maps/model/LatLng;)Lgjb;

    move-result-object v1

    invoke-direct {v0, v1}, Lgsn;-><init>(Lgjb;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public static a(Lcom/google/android/gms/maps/model/LatLng;F)Lgsn;
    .locals 2

    :try_start_0
    new-instance v0, Lgsn;

    invoke-static {}, Lgso;->a()Lgte;

    move-result-object v1

    invoke-interface {v1, p0, p1}, Lgte;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lgjb;

    move-result-object v1

    invoke-direct {v0, v1}, Lgsn;-><init>(Lgjb;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method private static a()Lgte;
    .locals 2

    sget-object v0, Lgso;->a:Lgte;

    const-string v1, "CameraUpdateFactory is not initialized"

    invoke-static {v0, v1}, Lgih;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgte;

    return-object v0
.end method
