.class final Lfrr;
.super Landroid/widget/Scroller;
.source "PG"


# instance fields
.field private final a:F

.field private b:[I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 709
    invoke-direct {p0, p1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    .line 710
    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    invoke-virtual {p0, v0}, Lfrr;->setFriction(F)V

    .line 712
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00d8

    .line 713
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    iput v0, p0, Lfrr;->a:F

    .line 714
    return-void
.end method

.method static synthetic a(Lfrr;IZ)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 683
    iget-object v1, p0, Lfrr;->b:[I

    array-length v3, v1

    move v0, v2

    :goto_0
    if-ge v0, v3, :cond_2

    aget v4, v1, v0

    if-eqz p2, :cond_0

    if-gt v4, p1, :cond_1

    :cond_0
    if-nez p2, :cond_3

    if-lt v4, p1, :cond_3

    :cond_1
    if-eqz p2, :cond_2

    sub-int v3, v4, p1

    const/16 v5, 0x3e8

    move-object v0, p0

    move v1, p1

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lfrr;->startScroll(IIIII)V

    :cond_2
    return-void

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a([I)V
    .locals 0

    .prologue
    .line 717
    iput-object p1, p0, Lfrr;->b:[I

    .line 718
    return-void
.end method

.method public fling(IIIIIIII)V
    .locals 11

    .prologue
    .line 723
    iget-object v0, p0, Lfrr;->b:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfrr;->b:[I

    array-length v0, v0

    if-nez v0, :cond_1

    .line 789
    :cond_0
    :goto_0
    return-void

    .line 729
    :cond_1
    const/4 v9, 0x0

    .line 730
    const/4 v2, 0x0

    .line 731
    const/4 v1, 0x0

    .line 732
    iget-object v4, p0, Lfrr;->b:[I

    array-length v5, v4

    const/4 v0, 0x0

    move v3, v0

    move v0, v1

    :goto_1
    if-ge v3, v5, :cond_11

    aget v1, v4, v3

    .line 733
    if-ge v1, p1, :cond_2

    .line 734
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    .line 732
    :goto_2
    add-int/lit8 v3, v3, 0x1

    move-object v9, v1

    goto :goto_1

    .line 735
    :cond_2
    if-ne v1, p1, :cond_3

    .line 736
    const/4 v0, 0x1

    move-object v1, v9

    goto :goto_2

    .line 737
    :cond_3
    if-le v1, p1, :cond_10

    .line 738
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    move-object v10, v1

    .line 745
    :goto_3
    invoke-static {p3}, Ljava/lang/Math;->abs(I)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lfrr;->a:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_4

    const/4 v1, 0x1

    .line 746
    :goto_4
    if-eqz v1, :cond_b

    .line 747
    int-to-float v0, p3

    float-to-int v3, v0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    .line 748
    invoke-super/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 749
    invoke-virtual {p0}, Lfrr;->getFinalX()I

    move-result v6

    .line 751
    const/4 v4, -0x1

    .line 752
    const v1, 0x7fffffff

    .line 753
    iget-object v7, p0, Lfrr;->b:[I

    array-length v8, v7

    const/4 v0, 0x0

    move v5, v0

    :goto_5
    if-ge v5, v8, :cond_5

    aget v2, v7, v5

    .line 754
    sub-int v0, v2, v6

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 755
    if-ge v0, v1, :cond_f

    move v1, v2

    .line 753
    :goto_6
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v4, v1

    move v1, v0

    goto :goto_5

    .line 745
    :cond_4
    const/4 v1, 0x0

    goto :goto_4

    .line 762
    :cond_5
    if-lez v3, :cond_9

    if-eqz v10, :cond_9

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v4, v0, :cond_9

    .line 763
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 768
    :cond_6
    :goto_7
    if-eqz v10, :cond_7

    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v4, v0, :cond_8

    :cond_7
    if-eqz v9, :cond_a

    .line 769
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v4, v0, :cond_a

    .line 770
    :cond_8
    invoke-virtual {p0}, Lfrr;->abortAnimation()V

    .line 771
    sub-int v3, v4, p1

    .line 772
    const/4 v4, 0x0

    const/16 v5, 0x3e8

    move-object v0, p0

    move v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v5}, Lfrr;->startScroll(IIIII)V

    goto/16 :goto_0

    .line 764
    :cond_9
    if-gez v3, :cond_6

    if-eqz v9, :cond_6

    .line 765
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le v4, v0, :cond_6

    .line 766
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto :goto_7

    .line 774
    :cond_a
    invoke-virtual {p0, v4}, Lfrr;->setFinalX(I)V

    goto/16 :goto_0

    .line 776
    :cond_b
    if-nez v0, :cond_0

    .line 777
    if-nez v9, :cond_d

    .line 780
    :cond_c
    :goto_8
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 786
    sub-int v3, v0, p1

    .line 787
    const/4 v4, 0x0

    const/16 v5, 0x3e8

    move-object v0, p0

    move v1, p1

    move v2, p2

    invoke-virtual/range {v0 .. v5}, Lfrr;->startScroll(IIIII)V

    goto/16 :goto_0

    .line 777
    :cond_d
    if-eqz v10, :cond_e

    .line 783
    invoke-virtual {v10}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 784
    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-lt v0, v1, :cond_c

    :cond_e
    move-object v10, v9

    goto :goto_8

    :cond_f
    move v0, v1

    move v1, v4

    goto :goto_6

    :cond_10
    move-object v1, v9

    goto/16 :goto_2

    :cond_11
    move-object v10, v2

    goto/16 :goto_3
.end method
