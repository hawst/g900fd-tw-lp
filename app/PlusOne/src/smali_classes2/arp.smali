.class public final Larp;
.super Lamn;
.source "PG"


# static fields
.field public static final a:J

.field public static final b:J

.field private static c:J


# instance fields
.field private final d:Ljava/util/concurrent/Executor;

.field private final e:Llmg;

.field private final f:Llmg;

.field private final g:Lcez;

.field private final h:Ljava/lang/Object;

.field private final j:Ljava/lang/Runnable;

.field private final k:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 37
    const/high16 v0, 0xc800000

    int-to-long v0, v0

    sput-wide v0, Larp;->a:J

    .line 42
    const/high16 v0, 0x800000

    int-to-long v0, v0

    sput-wide v0, Larp;->b:J

    .line 48
    const/high16 v0, 0x3200000

    int-to-long v0, v0

    sput-wide v0, Larp;->c:J

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Executor;Llmg;Llmg;Lcez;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-direct {p0}, Lamn;-><init>()V

    .line 57
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Larp;->h:Ljava/lang/Object;

    .line 63
    new-instance v0, Larq;

    invoke-direct {v0, p0}, Larq;-><init>(Larp;)V

    iput-object v0, p0, Larp;->j:Ljava/lang/Runnable;

    .line 75
    new-instance v0, Larr;

    invoke-direct {v0, p0}, Larr;-><init>(Larp;)V

    iput-object v0, p0, Larp;->k:Ljava/lang/Runnable;

    .line 89
    const-string v0, "backgroundExecutor"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Larp;->d:Ljava/util/concurrent/Executor;

    .line 90
    const-string v0, "shortTermVideoChunkStore"

    .line 91
    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llmg;

    iput-object v0, p0, Larp;->e:Llmg;

    .line 92
    const-string v0, "firstVideoChunkStore"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llmg;

    iput-object v0, p0, Larp;->f:Llmg;

    .line 93
    const-string v0, "freeSpaceProvider"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcez;

    iput-object v0, p0, Larp;->g:Lcez;

    .line 94
    return-void
.end method

.method static synthetic a(Larp;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Larp;->h:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Larp;)V
    .locals 6

    .prologue
    .line 18
    iget-object v0, p0, Larp;->f:Llmg;

    invoke-virtual {v0}, Llmg;->e()V

    iget-object v0, p0, Larp;->e:Llmg;

    invoke-virtual {v0}, Llmg;->e()V

    iget-object v0, p0, Larp;->g:Lcez;

    invoke-interface {v0}, Lcez;->a()J

    move-result-wide v0

    iget-object v2, p0, Larp;->e:Llmg;

    invoke-virtual {v2}, Llmg;->h()I

    move-result v2

    iget-object v3, p0, Larp;->e:Llmg;

    invoke-virtual {v3}, Llmg;->a()I

    move-result v3

    mul-int/2addr v2, v3

    iget-object v3, p0, Larp;->f:Llmg;

    invoke-virtual {v3}, Llmg;->h()I

    move-result v3

    iget-object v4, p0, Larp;->f:Llmg;

    invoke-virtual {v4}, Llmg;->a()I

    move-result v4

    mul-int/2addr v3, v4

    add-int/2addr v2, v3

    int-to-long v2, v2

    add-long/2addr v0, v2

    const-wide/16 v2, 0x0

    sget-wide v4, Larp;->c:J

    sub-long/2addr v0, v4

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    sget-wide v2, Larp;->b:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    sub-long/2addr v0, v2

    sget-wide v4, Larp;->a:J

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iget-object v4, p0, Larp;->f:Llmg;

    invoke-virtual {v4, v2, v3}, Llmg;->a(J)V

    iget-object v2, p0, Larp;->e:Llmg;

    invoke-virtual {v2, v0, v1}, Llmg;->a(J)V

    return-void
.end method

.method static synthetic c(Larp;)Llmg;
    .locals 1

    .prologue
    .line 18
    iget-object v0, p0, Larp;->e:Llmg;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 98
    invoke-super {p0}, Lamn;->a()V

    .line 99
    iget-object v0, p0, Larp;->d:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Larp;->j:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 100
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Larp;->d:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Larp;->k:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 111
    return-void
.end method

.method public d()Lart;
    .locals 2

    .prologue
    .line 114
    iget-boolean v0, p0, Lamn;->i:Z

    const-string v1, "controller must be initted before calling getVideoPredownloadStatusProvider()"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 116
    new-instance v0, Lars;

    invoke-direct {v0, p0}, Lars;-><init>(Larp;)V

    return-object v0
.end method
