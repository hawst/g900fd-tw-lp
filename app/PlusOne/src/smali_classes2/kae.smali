.class final Lkae;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljxp;


# instance fields
.field private synthetic a:Lkad;


# direct methods
.method constructor <init>(Lkad;)V
    .locals 0

    .prologue
    .line 517
    iput-object p1, p0, Lkae;->a:Lkad;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lhoz;)V
    .locals 4

    .prologue
    .line 520
    invoke-virtual {p1}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 521
    iget-object v0, p0, Lkae;->a:Lkad;

    iget-object v0, v0, Lkad;->a:Landroid/content/Context;

    invoke-static {v0}, Lkbr;->a(Landroid/content/Context;)Lkbr;

    move-result-object v0

    iget-object v1, p0, Lkae;->a:Lkad;

    iget-object v1, v1, Lkad;->a:Landroid/content/Context;

    const v2, 0x7f0a016f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkbr;->a(Ljava/lang/String;)V

    .line 537
    :goto_0
    return-void

    .line 523
    :cond_0
    invoke-virtual {p1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    .line 524
    new-instance v1, Ljzm;

    iget-object v2, p0, Lkae;->a:Lkad;

    iget-object v2, v2, Lkad;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Ljzm;-><init>(Landroid/content/Context;)V

    const-string v2, "poll_option_voter_models"

    .line 526
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 525
    invoke-virtual {v1, v2}, Ljzm;->a(Ljava/util/ArrayList;)Ljzm;

    move-result-object v1

    iget-object v2, p0, Lkae;->a:Lkad;

    .line 527
    iget-object v2, v2, Lkad;->e:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-virtual {v1, v2}, Ljzm;->c(I)Ljzm;

    move-result-object v1

    const-string v2, "poll_option_voters_count"

    .line 528
    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Ljzm;->b(I)Ljzm;

    move-result-object v0

    iget-object v1, p0, Lkae;->a:Lkad;

    .line 529
    iget-object v1, v1, Lkad;->b:Lkzw;

    invoke-virtual {v1}, Lkzw;->h()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljzm;->a(J)Ljzm;

    move-result-object v1

    .line 531
    iget-object v0, p0, Lkae;->a:Lkad;

    iget-object v0, v0, Lkad;->a:Landroid/content/Context;

    invoke-static {v0}, Llsc;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 532
    iget-object v0, p0, Lkae;->a:Lkad;

    .line 533
    invoke-virtual {v0}, Lkad;->c()Ljxw;

    move-result-object v0

    invoke-virtual {v0}, Ljxw;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    .line 532
    invoke-virtual {v1, v0}, Ljzm;->a(I)Ljzm;

    .line 535
    :cond_1
    iget-object v0, p0, Lkae;->a:Lkad;

    iget-object v0, v0, Lkad;->a:Landroid/content/Context;

    invoke-virtual {v1}, Ljzm;->a()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
