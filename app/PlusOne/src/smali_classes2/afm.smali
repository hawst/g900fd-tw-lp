.class public final Lafm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lagx;


# static fields
.field private static final a:Ljava/lang/Object;

.field private static m:Lafm;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Lafd;

.field private volatile d:Laff;

.field private e:I

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:Lafe;

.field private j:Landroid/os/Handler;

.field private k:Lafl;

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lafm;->a:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const/16 v0, 0x708

    iput v0, p0, Lafm;->e:I

    .line 45
    iput-boolean v1, p0, Lafm;->f:Z

    .line 48
    iput-boolean v1, p0, Lafm;->g:Z

    .line 52
    iput-boolean v1, p0, Lafm;->h:Z

    .line 54
    new-instance v0, Lafn;

    invoke-direct {v0, p0}, Lafn;-><init>(Lafm;)V

    iput-object v0, p0, Lafm;->i:Lafe;

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lafm;->l:Z

    .line 80
    return-void
.end method

.method public static a()Lafm;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lafm;->m:Lafm;

    if-nez v0, :cond_0

    .line 74
    new-instance v0, Lafm;

    invoke-direct {v0}, Lafm;-><init>()V

    sput-object v0, Lafm;->m:Lafm;

    .line 76
    :cond_0
    sget-object v0, Lafm;->m:Lafm;

    return-object v0
.end method

.method static synthetic a(Lafm;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lafm;->g:Z

    return v0
.end method

.method static synthetic b(Lafm;)I
    .locals 1

    .prologue
    .line 25
    iget v0, p0, Lafm;->e:I

    return v0
.end method

.method static synthetic c(Lafm;)Z
    .locals 1

    .prologue
    .line 25
    iget-boolean v0, p0, Lafm;->l:Z

    return v0
.end method

.method static synthetic d(Lafm;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lafm;->j:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lafm;->a:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method declared-synchronized a(Landroid/content/Context;Laff;)V
    .locals 1

    .prologue
    .line 132
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lafm;->b:Landroid/content/Context;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 143
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 135
    :cond_1
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lafm;->b:Landroid/content/Context;

    .line 137
    iget-object v0, p0, Lafm;->d:Laff;

    if-nez v0, :cond_0

    .line 138
    iput-object p2, p0, Lafm;->d:Laff;

    .line 139
    iget-boolean v0, p0, Lafm;->f:Z

    if-eqz v0, :cond_0

    .line 140
    invoke-interface {p2}, Laff;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 227
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lafm;->l:Z

    invoke-virtual {p0, v0, p1}, Lafm;->a(ZZ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    monitor-exit p0

    return-void

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method declared-synchronized a(ZZ)V
    .locals 4

    .prologue
    .line 209
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lafm;->l:Z

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lafm;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, p2, :cond_0

    .line 223
    :goto_0
    monitor-exit p0

    return-void

    .line 212
    :cond_0
    if-nez p1, :cond_1

    if-nez p2, :cond_2

    :cond_1
    :try_start_1
    iget v0, p0, Lafm;->e:I

    if-lez v0, :cond_2

    .line 213
    iget-object v0, p0, Lafm;->j:Landroid/os/Handler;

    const/4 v1, 0x1

    sget-object v2, Lafm;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 215
    :cond_2
    if-nez p1, :cond_3

    if-eqz p2, :cond_3

    iget v0, p0, Lafm;->e:I

    if-lez v0, :cond_3

    .line 216
    iget-object v0, p0, Lafm;->j:Landroid/os/Handler;

    iget-object v1, p0, Lafm;->j:Landroid/os/Handler;

    const/4 v2, 0x1

    sget-object v3, Lafm;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget v2, p0, Lafm;->e:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 219
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "PowerSaveMode "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez p1, :cond_4

    if-nez p2, :cond_5

    :cond_4
    const-string v0, "initiated."

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    iput-boolean p1, p0, Lafm;->l:Z

    .line 222
    iput-boolean p2, p0, Lafm;->g:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 209
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 219
    :cond_5
    :try_start_2
    const-string v0, "terminated."
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method declared-synchronized b()Lafd;
    .locals 4

    .prologue
    .line 155
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lafm;->c:Lafd;

    if-nez v0, :cond_1

    .line 156
    iget-object v0, p0, Lafm;->b:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 159
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cant get a store unless we have a context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 161
    :cond_0
    :try_start_1
    new-instance v0, Lagt;

    iget-object v1, p0, Lafm;->i:Lafe;

    iget-object v2, p0, Lafm;->b:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lagt;-><init>(Lafe;Landroid/content/Context;)V

    iput-object v0, p0, Lafm;->c:Lafd;

    .line 163
    :cond_1
    iget-object v0, p0, Lafm;->j:Landroid/os/Handler;

    if-nez v0, :cond_2

    .line 165
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lafm;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    new-instance v2, Lafo;

    invoke-direct {v2, p0}, Lafo;-><init>(Lafm;)V

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lafm;->j:Landroid/os/Handler;

    iget v0, p0, Lafm;->e:I

    if-lez v0, :cond_2

    iget-object v0, p0, Lafm;->j:Landroid/os/Handler;

    iget-object v1, p0, Lafm;->j:Landroid/os/Handler;

    const/4 v2, 0x1

    sget-object v3, Lafm;->a:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget v2, p0, Lafm;->e:I

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 167
    :cond_2
    iget-object v0, p0, Lafm;->k:Lafl;

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lafm;->h:Z

    if-eqz v0, :cond_3

    .line 168
    new-instance v0, Lafl;

    invoke-direct {v0, p0}, Lafl;-><init>(Lagx;)V

    iput-object v0, p0, Lafm;->k:Lafl;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lafm;->b:Landroid/content/Context;

    iget-object v2, p0, Lafm;->k:Lafl;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 170
    :cond_3
    iget-object v0, p0, Lafm;->c:Lafd;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method

.method public declared-synchronized c()V
    .locals 2

    .prologue
    .line 177
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lafm;->d:Laff;

    if-nez v0, :cond_0

    .line 178
    const-string v0, "dispatch call queued.  Need to call GAServiceManager.getInstance().initialize()."

    invoke-static {v0}, Lagm;->c(Ljava/lang/String;)I

    .line 179
    const/4 v0, 0x1

    iput-boolean v0, p0, Lafm;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 185
    :goto_0
    monitor-exit p0

    return-void

    .line 183
    :cond_0
    :try_start_1
    invoke-static {}, Lagd;->a()Lagd;

    move-result-object v0

    sget-object v1, Lage;->e:Lage;

    invoke-virtual {v0, v1}, Lagd;->a(Lage;)V

    .line 184
    iget-object v0, p0, Lafm;->d:Laff;

    invoke-interface {v0}, Laff;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 177
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
