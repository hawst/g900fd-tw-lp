.class public final Lbir;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:[B

.field private b:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-array v0, p1, [B

    iput-object v0, p0, Lbir;->a:[B

    .line 24
    return-void
.end method

.method private static b([BII)I
    .locals 3

    .prologue
    .line 157
    const-string v0, "offset"

    invoke-static {p1, v0, p0}, Lcec;->a(ILjava/lang/CharSequence;[B)I

    .line 158
    add-int v0, p1, p2

    add-int/lit8 v0, v0, -0x1

    const-string v1, "offset + length - 1"

    invoke-static {v0, v1, p0}, Lcec;->a(ILjava/lang/CharSequence;[B)I

    .line 159
    aget-byte v0, p0, p1

    and-int/lit16 v1, v0, 0xff

    .line 160
    add-int/lit8 v0, p1, 0x1

    :goto_0
    add-int v2, p1, p2

    if-ge v0, v2, :cond_0

    .line 161
    shl-int/lit8 v1, v1, 0x8

    .line 162
    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v1, v2

    .line 160
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 164
    :cond_0
    return v1
.end method


# virtual methods
.method public a(I)V
    .locals 2

    .prologue
    .line 52
    iget-object v0, p0, Lbir;->a:[B

    array-length v0, v0

    if-eq p1, v0, :cond_0

    .line 53
    const-string v0, "position"

    iget-object v1, p0, Lbir;->a:[B

    invoke-static {p1, v0, v1}, Lcec;->a(ILjava/lang/CharSequence;[B)I

    .line 55
    :cond_0
    iput p1, p0, Lbir;->b:I

    .line 56
    return-void
.end method

.method public a([BII)V
    .locals 2

    .prologue
    .line 73
    iget-object v0, p0, Lbir;->a:[B

    iget v1, p0, Lbir;->b:I

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 74
    invoke-virtual {p0, p3}, Lbir;->b(I)V

    .line 75
    return-void
.end method

.method public a()[B
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lbir;->a:[B

    return-object v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lbir;->a:[B

    array-length v0, v0

    return v0
.end method

.method public b(I)V
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lbir;->b:I

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lbir;->a(I)V

    .line 65
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 41
    iget v0, p0, Lbir;->b:I

    return v0
.end method

.method public d()I
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 89
    iget-object v0, p0, Lbir;->a:[B

    iget v1, p0, Lbir;->b:I

    invoke-static {v0, v1, v2}, Lbir;->b([BII)I

    move-result v0

    .line 90
    invoke-virtual {p0, v2}, Lbir;->b(I)V

    .line 91
    return v0
.end method

.method public e()I
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 96
    iget-object v0, p0, Lbir;->a:[B

    iget v1, p0, Lbir;->b:I

    invoke-static {v0, v1, v2}, Lbir;->b([BII)I

    move-result v0

    .line 97
    invoke-virtual {p0, v2}, Lbir;->b(I)V

    .line 98
    return v0
.end method

.method public f()I
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 110
    iget-object v0, p0, Lbir;->a:[B

    iget v1, p0, Lbir;->b:I

    invoke-static {v0, v1, v2}, Lbir;->b([BII)I

    move-result v0

    .line 111
    invoke-virtual {p0, v2}, Lbir;->b(I)V

    .line 112
    return v0
.end method

.method public g()J
    .locals 9

    .prologue
    const/16 v8, 0x8

    .line 117
    iget-object v1, p0, Lbir;->a:[B

    iget v4, p0, Lbir;->b:I

    const-string v0, "offset"

    invoke-static {v4, v0, v1}, Lcec;->a(ILjava/lang/CharSequence;[B)I

    add-int/lit8 v0, v4, 0x8

    add-int/lit8 v0, v0, -0x1

    const-string v2, "offset + length - 1"

    invoke-static {v0, v2, v1}, Lcec;->a(ILjava/lang/CharSequence;[B)I

    aget-byte v0, v1, v4

    and-int/lit16 v0, v0, 0xff

    int-to-long v2, v0

    add-int/lit8 v0, v4, 0x1

    :goto_0
    add-int/lit8 v5, v4, 0x8

    if-ge v0, v5, :cond_0

    shl-long/2addr v2, v8

    aget-byte v5, v1, v0

    and-int/lit16 v5, v5, 0xff

    int-to-long v6, v5

    or-long/2addr v2, v6

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 118
    :cond_0
    invoke-virtual {p0, v8}, Lbir;->b(I)V

    .line 119
    return-wide v2
.end method

.method public h()I
    .locals 3

    .prologue
    .line 124
    iget-object v0, p0, Lbir;->a:[B

    iget v1, p0, Lbir;->b:I

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lbir;->b([BII)I

    move-result v0

    .line 125
    const/4 v1, 0x4

    invoke-virtual {p0, v1}, Lbir;->b(I)V

    .line 126
    return v0
.end method
