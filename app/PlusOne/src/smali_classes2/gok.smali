.class final Lgok;
.super Lgnm;


# instance fields
.field private final a:Lgfy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgfy",
            "<",
            "Lgyz;",
            ">;"
        }
    .end annotation
.end field

.field private synthetic b:Lgny;


# direct methods
.method public constructor <init>(Lgny;Lgfy;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfy",
            "<",
            "Lgyz;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lgok;->b:Lgny;

    invoke-direct {p0}, Lgnm;-><init>()V

    iput-object p2, p0, Lgok;->a:Lgfy;

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 6

    const-string v0, "PeopleService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "People callback: status="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nresolution="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nholder="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v0, 0x0

    invoke-static {p1, v0, p2}, Lgny;->a(ILjava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    iget-object v1, p0, Lgok;->b:Lgny;

    invoke-static {v1, p3}, Lgny;->a(Lgny;Lcom/google/android/gms/common/data/DataHolder;)Lgzx;

    move-result-object v1

    iget-object v2, p0, Lgok;->b:Lgny;

    new-instance v3, Lgoa;

    iget-object v4, p0, Lgok;->b:Lgny;

    iget-object v5, p0, Lgok;->a:Lgfy;

    invoke-direct {v3, v4, v5, v0, v1}, Lgoa;-><init>(Lgny;Lgfy;Lcom/google/android/gms/common/api/Status;Lgzx;)V

    invoke-virtual {v2, v3}, Lgny;->b(Lghj;)V

    return-void
.end method
