.class final Lakm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laki;


# instance fields
.field private final a:Laic;

.field private final b:Lahy;

.field private final c:[Lajj;

.field private final d:[Lajw;

.field private synthetic e:Lakl;


# direct methods
.method private constructor <init>(Lakl;Laic;Lahy;[Lajj;[Lajw;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 109
    iput-object p1, p0, Lakm;->e:Lakl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    const-string v0, "spec"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laic;

    iput-object v0, p0, Lakm;->a:Laic;

    .line 111
    const-string v0, "callback"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lahy;

    iput-object v0, p0, Lakm;->b:Lahy;

    .line 112
    const-string v0, "postProcessors"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lajj;

    iput-object v0, p0, Lakm;->c:[Lajj;

    .line 113
    const-string v0, "segmenters"

    invoke-static {p5, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lajw;

    iput-object v0, p0, Lakm;->d:[Lajw;

    .line 114
    return-void
.end method

.method synthetic constructor <init>(Lakl;Laic;Lahy;[Lajj;[Lajw;B)V
    .locals 0

    .prologue
    .line 98
    invoke-direct/range {p0 .. p5}, Lakm;-><init>(Lakl;Laic;Lahy;[Lajj;[Lajw;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 146
    iget-object v0, p0, Lakm;->b:Lahy;

    invoke-interface {v0, p1}, Lahy;->a(Landroid/graphics/Bitmap;)V

    .line 147
    return-void
.end method

.method public a(Landroid/net/Uri;J)V
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lakm;->b:Lahy;

    invoke-interface {v0, p1, p2, p3}, Lahy;->a(Landroid/net/Uri;J)V

    .line 142
    return-void
.end method

.method public a(Landroid/net/Uri;Lbkm;)V
    .locals 4

    .prologue
    .line 118
    invoke-virtual {p2}, Lbkm;->a()Lbko;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lbko;->a(Z)Lbko;

    .line 119
    iget-object v0, p0, Lakm;->c:[Lajj;

    iget-object v1, p0, Lakm;->d:[Lajw;

    invoke-virtual {p2, v0, v1}, Lbkm;->a([Lajj;[Lajw;)Lbkr;

    move-result-object v0

    .line 120
    iget-object v1, p0, Lakm;->e:Lakl;

    invoke-static {v1}, Lakl;->b(Lakl;)Lakp;

    move-result-object v1

    invoke-interface {v0}, Lbkr;->c()Lbkn;

    move-result-object v2

    iget-object v3, p0, Lakm;->e:Lakl;

    .line 121
    invoke-static {v3}, Lakl;->a(Lakl;)Lcdu;

    move-result-object v3

    .line 120
    invoke-virtual {v1, v2, v3}, Lakp;->a(Lbkn;Lcdu;)V

    .line 122
    iget-object v1, p0, Lakm;->b:Lahy;

    iget-object v2, p0, Lakm;->a:Laic;

    invoke-interface {v1, p1, v2, v0}, Lahy;->a(Landroid/net/Uri;Laic;Lbkr;)V

    .line 123
    return-void
.end method

.method public a(Landroid/net/Uri;Lbkr;)V
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lakm;->b:Lahy;

    invoke-interface {v0, p1, p2}, Lahy;->a(Landroid/net/Uri;Lbkr;)V

    .line 152
    return-void
.end method

.method public a(Landroid/net/Uri;Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lakm;->e:Lakl;

    invoke-static {v0}, Lakl;->b(Lakl;)Lakp;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lakp;->a(Lbkn;)V

    .line 136
    iget-object v0, p0, Lakm;->b:Lahy;

    iget-object v1, p0, Lakm;->a:Laic;

    invoke-interface {v0, p1, v1, p2}, Lahy;->a(Landroid/net/Uri;Laic;Ljava/lang/Exception;)V

    .line 137
    return-void
.end method

.method public b(Landroid/net/Uri;Lbkm;)V
    .locals 3

    .prologue
    .line 127
    invoke-virtual {p2}, Lbkm;->a()Lbko;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lbko;->a(Z)Lbko;

    .line 128
    invoke-virtual {p2}, Lbkm;->c()Lbkr;

    move-result-object v0

    .line 129
    iget-object v1, p0, Lakm;->e:Lakl;

    invoke-static {v1}, Lakl;->b(Lakl;)Lakp;

    move-result-object v1

    invoke-interface {v0}, Lbkr;->c()Lbkn;

    move-result-object v2

    invoke-virtual {v1, v2}, Lakp;->a(Lbkn;)V

    .line 130
    iget-object v1, p0, Lakm;->b:Lahy;

    iget-object v2, p0, Lakm;->a:Laic;

    invoke-interface {v1, p1, v2, v0}, Lahy;->b(Landroid/net/Uri;Laic;Lbkr;)V

    .line 131
    return-void
.end method
