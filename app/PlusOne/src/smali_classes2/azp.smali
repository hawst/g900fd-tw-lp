.class public final Lazp;
.super Layt;
.source "PG"


# instance fields
.field private final b:I

.field private final c:Z

.field private final d:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Layu;IZ)V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0, p1}, Layt;-><init>(Layu;)V

    .line 19
    iput p2, p0, Lazp;->b:I

    .line 20
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lazp;->d:Landroid/graphics/Matrix;

    .line 21
    iput-boolean p3, p0, Lazp;->c:Z

    .line 22
    return-void
.end method


# virtual methods
.method public f(Layl;)I
    .locals 1

    .prologue
    .line 26
    iget v0, p0, Lazp;->b:I

    return v0
.end method

.method public g(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lazp;->d:Landroid/graphics/Matrix;

    return-object v0
.end method

.method public h(Layl;)F
    .locals 1

    .prologue
    .line 36
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method public i(Layl;)I
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x4

    return v0
.end method

.method public j(Layl;)I
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lazp;->c:Z

    if-eqz v0, :cond_0

    .line 47
    invoke-super {p0, p1}, Layt;->f(Layl;)I

    move-result v0

    .line 49
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Layt;->j(Layl;)I

    move-result v0

    goto :goto_0
.end method

.method public k(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 54
    iget-boolean v0, p0, Lazp;->c:Z

    if-eqz v0, :cond_0

    .line 55
    invoke-super {p0, p1}, Layt;->g(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 57
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Layt;->k(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    goto :goto_0
.end method

.method public l(Layl;)F
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lazp;->c:Z

    if-eqz v0, :cond_0

    .line 63
    invoke-super {p0, p1}, Layt;->h(Layl;)F

    move-result v0

    .line 65
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Layt;->l(Layl;)F

    move-result v0

    goto :goto_0
.end method

.method public m(Layl;)I
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lazp;->c:Z

    if-eqz v0, :cond_0

    .line 71
    invoke-super {p0, p1}, Layt;->i(Layl;)I

    move-result v0

    .line 73
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Layt;->m(Layl;)I

    move-result v0

    goto :goto_0
.end method
