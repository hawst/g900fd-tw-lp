.class public final Libl;
.super Lkfu;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Libb;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 35
    new-instance v0, Lkfo;

    invoke-direct {v0, p1, p2}, Lkfo;-><init>(Landroid/content/Context;I)V

    const-string v2, "plusi"

    const-string v3, "oauth2:https://www.googleapis.com/auth/plus.me https://www.googleapis.com/auth/plus.stream.read https://www.googleapis.com/auth/plus.stream.write https://www.googleapis.com/auth/plus.circles.write https://www.googleapis.com/auth/plus.circles.read https://www.googleapis.com/auth/plus.photos.readwrite https://www.googleapis.com/auth/plus.native"

    invoke-direct {p0, p1, v0, v2, v3}, Lkfu;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    if-eqz p3, :cond_1

    .line 39
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v2, 0x32

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v4

    move v2, v1

    move v3, v1

    .line 40
    :goto_0
    if-ge v2, v4, :cond_1

    .line 41
    invoke-virtual {p3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Libb;

    .line 42
    new-instance v5, Libk;

    invoke-direct {v5, p1, p2, v0}, Libk;-><init>(Landroid/content/Context;ILibb;)V

    .line 44
    invoke-virtual {v5}, Libk;->P_()[B

    move-result-object v0

    .line 45
    if-nez v0, :cond_0

    move v0, v1

    .line 46
    :goto_1
    add-int v6, v3, v0

    const v7, 0xc800

    if-ge v6, v7, :cond_1

    .line 47
    invoke-virtual {p0, v5}, Libl;->a(Lkff;)V

    .line 48
    add-int/2addr v3, v0

    .line 40
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 45
    :cond_0
    array-length v0, v0

    goto :goto_1

    .line 54
    :cond_1
    return-void
.end method


# virtual methods
.method public i()Ljava/util/ArrayList;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Libb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 60
    invoke-virtual {p0}, Libl;->j()Ljava/util/ArrayList;

    move-result-object v3

    .line 61
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 62
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 63
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 64
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    .line 65
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Libk;

    .line 66
    invoke-virtual {v0}, Libk;->t()Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, v0, Lkff;->k:Ljava/lang/Exception;

    if-nez v5, :cond_0

    .line 67
    invoke-virtual {v0}, Libk;->i()Libb;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 64
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 72
    :goto_1
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method
