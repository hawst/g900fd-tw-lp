.class public final Lgri;
.super Ljava/lang/Object;

# interfaces
.implements Lgff;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Lglv;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgfg;Lgfh;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lglv;

    const-string v1, "location"

    invoke-direct {v0, p1, p2, p3, v1}, Lglv;-><init>(Landroid/content/Context;Lgfg;Lgfh;Ljava/lang/String;)V

    iput-object v0, p0, Lgri;->a:Lglv;

    return-void
.end method


# virtual methods
.method public a()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lgri;->a:Lglv;

    invoke-virtual {v0}, Lglv;->j()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/location/LocationRequest;Lgrj;)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lgri;->a:Lglv;

    invoke-static {p1}, Lcom/google/android/gms/internal/nk;->a(Lcom/google/android/gms/location/LocationRequest;)Lcom/google/android/gms/internal/nk;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lglv;->a(Lcom/google/android/gms/internal/nk;Lgrj;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lgri;->a:Lglv;

    invoke-virtual {v0}, Lglv;->a()V

    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lgri;->a:Lglv;

    invoke-virtual {v0}, Lglv;->b()V

    return-void
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lgri;->a:Lglv;

    invoke-virtual {v0}, Lglv;->c()Z

    move-result v0

    return v0
.end method
