.class public final Laas;
.super Laar;
.source "PG"


# direct methods
.method constructor <init>(Lzp;)V
    .locals 0

    .prologue
    .line 109
    invoke-direct {p0, p1}, Laar;-><init>(Lzp;)V

    .line 110
    return-void
.end method


# virtual methods
.method public a(Laas;Landroid/graphics/RectF;Landroid/graphics/RectF;)V
    .locals 7

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 96
    invoke-static {}, Labx;->a()Labx;

    move-result-object v0

    invoke-virtual {v0}, Labx;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 97
    invoke-static {}, Lacw;->a()Lacw;

    move-result-object v0

    invoke-virtual {v0}, Lacw;->j()Lacm;

    move-result-object v0

    invoke-virtual {v0, p2}, Lacm;->a(Landroid/graphics/RectF;)V

    invoke-virtual {v0, p3}, Lacm;->b(Landroid/graphics/RectF;)V

    invoke-virtual {v0, p0, p1}, Lacm;->a(Laas;Laas;)V

    invoke-virtual {v0, v1, v1, v2, v2}, Lacm;->a(FFFF)V

    invoke-virtual {v0, v1, v1, v2, v2}, Lacm;->b(FFFF)V

    .line 101
    :goto_0
    return-void

    .line 99
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    iget v1, p2, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    invoke-virtual {p0}, Laas;->j()I

    move-result v2

    mul-int/2addr v1, v2

    iget v2, p2, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    invoke-virtual {p0}, Laas;->k()I

    move-result v3

    mul-int/2addr v2, v3

    iget v3, p2, Landroid/graphics/RectF;->right:F

    float-to-int v3, v3

    invoke-virtual {p0}, Laas;->j()I

    move-result v4

    mul-int/2addr v3, v4

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    float-to-int v4, v4

    invoke-virtual {p0}, Laas;->k()I

    move-result v5

    mul-int/2addr v4, v5

    invoke-direct {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    new-instance v1, Landroid/graphics/Rect;

    iget v2, p3, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    invoke-virtual {p1}, Laas;->j()I

    move-result v3

    mul-int/2addr v2, v3

    iget v3, p3, Landroid/graphics/RectF;->top:F

    float-to-int v3, v3

    invoke-virtual {p1}, Laas;->k()I

    move-result v4

    mul-int/2addr v3, v4

    iget v4, p3, Landroid/graphics/RectF;->right:F

    float-to-int v4, v4

    invoke-virtual {p1}, Laas;->j()I

    move-result v5

    mul-int/2addr v4, v5

    iget v5, p3, Landroid/graphics/RectF;->bottom:F

    float-to-int v5, v5

    invoke-virtual {p1}, Laas;->k()I

    move-result v6

    mul-int/2addr v5, v6

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {p1}, Laas;->j()I

    move-result v3

    invoke-virtual {p1}, Laas;->k()I

    move-result v4

    invoke-static {v3, v4, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v2

    new-instance v3, Landroid/graphics/Canvas;

    invoke-direct {v3, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    invoke-virtual {p0}, Laas;->n()Landroid/graphics/Bitmap;

    move-result-object v5

    invoke-virtual {v3, v5, v0, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    invoke-virtual {p1, v2}, Laas;->a(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 61
    iget-object v0, p0, Laas;->a:Lzp;

    invoke-virtual {v0}, Lzp;->a()Labf;

    move-result-object v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    invoke-virtual {v0}, Labf;->e()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported frame type \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\' for bitmap assignment!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    if-eq v1, v0, :cond_0

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {p1, v0, v4}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not convert bitmap to frame-type RGBA8888!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 62
    :cond_0
    iget-object v0, p0, Laas;->a:Lzp;

    invoke-virtual {v0}, Lzp;->e()[I

    move-result-object v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    aget v2, v0, v4

    if-ne v1, v2, :cond_1

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    aget v2, v0, v5

    if-eq v1, v2, :cond_2

    :cond_1
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot assign bitmap of size "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to frame of size "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, v0, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v0, v0, v5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "!"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 63
    :cond_2
    iget-object v0, p0, Laas;->a:Lzp;

    const/4 v1, 0x2

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Lzp;->b(II)Lzr;

    move-result-object v0

    .line 64
    invoke-virtual {v0, p1}, Lzr;->a(Ljava/lang/Object;)V

    .line 65
    iget-object v0, p0, Laas;->a:Lzp;

    invoke-virtual {v0}, Lzp;->b()Z

    .line 66
    return-void

    .line 61
    nop

    :pswitch_data_0
    .packed-switch 0x12d
        :pswitch_0
    .end packed-switch
.end method

.method public l()Lada;
    .locals 3

    .prologue
    .line 36
    iget-object v0, p0, Laas;->a:Lzp;

    const/4 v1, 0x1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lzp;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lada;

    return-object v0
.end method

.method public m()Lacw;
    .locals 3

    .prologue
    .line 47
    iget-object v0, p0, Laas;->a:Lzp;

    const/4 v1, 0x2

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Lzp;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacw;

    return-object v0
.end method

.method public n()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 74
    iget-object v0, p0, Laas;->a:Lzp;

    const/4 v1, 0x1

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Lzp;->a(II)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 75
    iget-object v1, p0, Laas;->a:Lzp;

    invoke-virtual {v1}, Lzp;->b()Z

    .line 76
    return-object v0
.end method
