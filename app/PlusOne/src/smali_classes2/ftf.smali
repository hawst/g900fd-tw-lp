.class public final Lftf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Z

.field final b:Landroid/animation/ObjectAnimator;

.field final synthetic c:Lcom/google/android/apps/plus/stories/views/StoryLayout;

.field private final d:Landroid/content/SharedPreferences;

.field private e:I

.field private f:J


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/stories/views/StoryLayout;Lfrq;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 1308
    iput-object p1, p0, Lftf;->c:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1303
    iput-boolean v4, p0, Lftf;->a:Z

    .line 1304
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lftf;->f:J

    .line 1309
    const-string v0, "xOffset"

    const/4 v1, 0x2

    new-array v1, v1, [I

    aput v4, v1, v4

    const/4 v2, 0x1

    aput p3, v1, v2

    invoke-static {p2, v0, v1}, Landroid/animation/ObjectAnimator;->ofInt(Ljava/lang/Object;Ljava/lang/String;[I)Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lftf;->b:Landroid/animation/ObjectAnimator;

    .line 1310
    iget-object v0, p0, Lftf;->b:Landroid/animation/ObjectAnimator;

    new-instance v1, Lfth;

    invoke-direct {v1, p0}, Lfth;-><init>(Lftf;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 1311
    iget-object v0, p0, Lftf;->b:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x320

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 1313
    invoke-virtual {p1}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "bouncer"

    invoke-virtual {v0, v1, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lftf;->d:Landroid/content/SharedPreferences;

    .line 1314
    iget-object v0, p0, Lftf;->d:Landroid/content/SharedPreferences;

    const-string v1, "abort_count"

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lftf;->e:I

    .line 1315
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 1321
    iget v0, p0, Lftf;->e:I

    const/4 v1, 0x2

    if-lt v0, v1, :cond_0

    const-wide/16 v0, 0x2710

    :goto_0
    invoke-virtual {p0, v0, v1}, Lftf;->a(J)V

    .line 1323
    return-void

    .line 1321
    :cond_0
    const-wide/16 v0, 0x7d0

    goto :goto_0
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 1329
    iget-boolean v0, p0, Lftf;->a:Z

    if-eqz v0, :cond_0

    .line 1347
    :goto_0
    return-void

    .line 1333
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lftf;->a:Z

    .line 1334
    iget-object v0, p0, Lftf;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p1, p2}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 1335
    iget-object v0, p0, Lftf;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 1336
    invoke-virtual {p0}, Lftf;->d()V

    .line 1337
    iget-object v0, p0, Lftf;->b:Landroid/animation/ObjectAnimator;

    new-instance v1, Lftg;

    invoke-direct {v1, p0}, Lftg;-><init>(Lftf;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    goto :goto_0
.end method

.method public b()V
    .locals 3

    .prologue
    .line 1353
    iget-boolean v0, p0, Lftf;->a:Z

    if-eqz v0, :cond_0

    .line 1354
    iget-object v0, p0, Lftf;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 1355
    const-string v1, "abort_count"

    iget v2, p0, Lftf;->e:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lftf;->e:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 1356
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1358
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lftf;->a:Z

    .line 1359
    iget-object v0, p0, Lftf;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 1360
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lftf;->f:J

    .line 1361
    return-void
.end method

.method public c()J
    .locals 4

    .prologue
    const-wide/16 v0, -0x1

    .line 1367
    iget-wide v2, p0, Lftf;->f:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_0

    iget-wide v0, p0, Lftf;->f:J

    .line 1368
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    :cond_0
    return-wide v0
.end method

.method d()V
    .locals 4

    .prologue
    .line 1373
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lftf;->b:Landroid/animation/ObjectAnimator;

    invoke-virtual {v2}, Landroid/animation/ObjectAnimator;->getStartDelay()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lftf;->f:J

    .line 1374
    return-void
.end method
