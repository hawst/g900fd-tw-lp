.class public final Lbdw;
.super Lloj;
.source "PG"

# interfaces
.implements Larl;
.implements Lcdw;


# static fields
.field private static final Q:Larm;

.field private static final R:[I


# instance fields
.field private S:Laqy;

.field private T:Larm;

.field private U:[Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Larm;

    .line 40
    invoke-static {v0}, Lcgf;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larm;

    sput-object v0, Lbdw;->Q:Larm;

    .line 45
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lbdw;->R:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f100559
        0x7f100558
        0x7f100554
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Lloj;-><init>()V

    .line 75
    sget-object v0, Lbdw;->Q:Larm;

    iput-object v0, p0, Lbdw;->T:Larm;

    .line 250
    return-void
.end method

.method static synthetic a(Lbdw;)Latu;
    .locals 4

    .prologue
    .line 30
    invoke-virtual {p0}, Lbdw;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "sizes"

    invoke-static {v0, v1}, Latu;->a(Landroid/os/Bundle;Ljava/lang/String;)[Latu;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    iget-object v3, p0, Lbdw;->U:[Landroid/view/View;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Landroid/view/View;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    aget-object v0, v1, v0

    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string v0, "one size must be selected"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method public static a([Latu;)Lbdw;
    .locals 4

    .prologue
    .line 65
    array-length v0, p0

    const-string v1, "sizes.length"

    const/4 v2, 0x1

    const/4 v3, 0x3

    invoke-static {v0, v1, v2, v3}, Lcec;->a(ILjava/lang/CharSequence;II)I

    .line 66
    new-instance v0, Lbdw;

    invoke-direct {v0}, Lbdw;-><init>()V

    .line 67
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 68
    const-string v2, "sizes"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 69
    invoke-virtual {v0, v1}, Lbdw;->f(Landroid/os/Bundle;)V

    .line 70
    return-object v0
.end method

.method private a(Landroid/view/View;Latu;III)V
    .locals 4

    .prologue
    .line 236
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 237
    const v0, 0x7f100556

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 238
    const v1, 0x7f100555

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 239
    const v2, 0x7f100557

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 241
    invoke-virtual {p0, p3}, Lbdw;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    invoke-virtual {p0, p4}, Lbdw;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 243
    invoke-virtual {p0, p5}, Lbdw;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 244
    new-instance v0, Lbea;

    invoke-direct {v0, p0, p2}, Lbea;-><init>(Lbdw;Latu;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 245
    return-void
.end method

.method private a(Latu;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 136
    invoke-virtual {p0}, Lbdw;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "sizes"

    invoke-static {v0, v2}, Latu;->a(Landroid/os/Bundle;Ljava/lang/String;)[Latu;

    move-result-object v3

    .line 137
    array-length v4, v3

    move v2, v1

    .line 138
    :goto_0
    if-ge v2, v4, :cond_1

    .line 139
    iget-object v0, p0, Lbdw;->U:[Landroid/view/View;

    aget-object v5, v0, v2

    aget-object v0, v3, v2

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v5, v0}, Landroid/view/View;->setSelected(Z)V

    .line 138
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_0
    move v0, v1

    .line 139
    goto :goto_1

    .line 141
    :cond_1
    return-void
.end method

.method static synthetic a(Lbdw;Latu;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lbdw;->a(Latu;)V

    return-void
.end method

.method static synthetic b(Lbdw;)Larm;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lbdw;->T:Larm;

    return-object v0
.end method


# virtual methods
.method public a(Larm;)V
    .locals 0

    .prologue
    .line 102
    if-nez p1, :cond_0

    sget-object p1, Lbdw;->Q:Larm;

    :cond_0
    iput-object p1, p0, Lbdw;->T:Larm;

    .line 103
    return-void
.end method

.method public ae_()V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lbdw;->S:Laqy;

    invoke-virtual {v0, p0}, Laqy;->b(Larl;)V

    .line 96
    const/4 v0, 0x0

    iput-object v0, p0, Lbdw;->S:Laqy;

    .line 97
    invoke-super {p0}, Lloj;->ae_()V

    .line 98
    return-void
.end method

.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10

    .prologue
    .line 107
    invoke-virtual {p0}, Lbdw;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401ce

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    invoke-virtual {p0}, Lbdw;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "sizes"

    invoke-static {v0, v1}, Latu;->a(Landroid/os/Bundle;Ljava/lang/String;)[Latu;

    move-result-object v8

    array-length v9, v8

    new-array v0, v9, [Landroid/view/View;

    iput-object v0, p0, Lbdw;->U:[Landroid/view/View;

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v9, :cond_3

    iget-object v0, p0, Lbdw;->U:[Landroid/view/View;

    sget-object v1, Lbdw;->R:[I

    aget v1, v1, v6

    invoke-virtual {v7, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v6

    sget-object v0, Lbdz;->a:[I

    aget-object v1, v8, v6

    invoke-virtual {v1}, Latu;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v1, "Unknown size: "

    aget-object v0, v8, v6

    iget-object v0, v0, Latu;->d:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    :pswitch_0
    iget-object v0, p0, Lbdw;->U:[Landroid/view/View;

    aget-object v1, v0, v6

    sget-object v2, Latu;->a:Latu;

    const v3, 0x7f0a0126

    const v4, 0x7f0a0120

    const v5, 0x7f0a0123

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lbdw;->a(Landroid/view/View;Latu;III)V

    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lbdw;->U:[Landroid/view/View;

    aget-object v1, v0, v6

    if-nez v6, :cond_0

    const/4 v0, 0x1

    :goto_3
    if-eqz v0, :cond_1

    sget-object v2, Latu;->b:Latu;

    const v3, 0x7f0a0127

    const v4, 0x7f0a0122

    const v5, 0x7f0a0125

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lbdw;->a(Landroid/view/View;Latu;III)V

    goto :goto_2

    :cond_0
    const/4 v0, 0x0

    goto :goto_3

    :cond_1
    sget-object v2, Latu;->b:Latu;

    const v3, 0x7f0a0127

    const v4, 0x7f0a0121

    const v5, 0x7f0a0124

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lbdw;->a(Landroid/view/View;Latu;III)V

    goto :goto_2

    :pswitch_2
    iget-object v0, p0, Lbdw;->U:[Landroid/view/View;

    aget-object v1, v0, v6

    sget-object v2, Latu;->c:Latu;

    const v3, 0x7f0a0128

    const v4, 0x7f0a0122

    const v5, 0x7f0a0125

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lbdw;->a(Landroid/view/View;Latu;III)V

    goto :goto_2

    :cond_2
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    aget-object v0, v8, v0

    invoke-direct {p0, v0}, Lbdw;->a(Latu;)V

    .line 108
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lbdw;->n()Lz;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 109
    invoke-virtual {v0, v7}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x1

    .line 110
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a011f

    new-instance v2, Lbdy;

    invoke-direct {v2, p0}, Lbdy;-><init>(Lbdw;)V

    .line 111
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/high16 v1, 0x1040000

    new-instance v2, Lbdx;

    invoke-direct {v2, p0}, Lbdx;-><init>(Lbdw;)V

    .line 118
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 124
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 125
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 126
    return-object v0

    .line 107
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public d()Lcdz;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcdz;->c:Lcdz;

    return-object v0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 87
    invoke-super {p0, p1}, Lloj;->d(Landroid/os/Bundle;)V

    .line 89
    invoke-static {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->b(Lu;)Lcom/google/android/apps/moviemaker/MovieMakerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o()Laol;

    move-result-object v0

    invoke-virtual {v0}, Laol;->h()Laqy;

    move-result-object v0

    iput-object v0, p0, Lbdw;->S:Laqy;

    .line 90
    iget-object v0, p0, Lbdw;->S:Laqy;

    invoke-virtual {v0, p0}, Laqy;->a(Larl;)V

    .line 91
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lbdw;->T:Larm;

    invoke-interface {v0}, Larm;->a()V

    .line 132
    invoke-super {p0, p1}, Lloj;->onCancel(Landroid/content/DialogInterface;)V

    .line 133
    return-void
.end method
