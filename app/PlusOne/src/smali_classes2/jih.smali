.class public final Ljih;
.super Lt;
.source "PG"


# instance fields
.field private N:I

.field private O:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lt;-><init>()V

    return-void
.end method

.method static synthetic a(Ljih;)I
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Ljih;->N:I

    return v0
.end method

.method static synthetic b(Ljih;)J
    .locals 2

    .prologue
    .line 63
    iget-wide v0, p0, Ljih;->O:J

    return-wide v0
.end method


# virtual methods
.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 76
    invoke-virtual {p0}, Ljih;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ljih;->N:I

    .line 77
    invoke-virtual {p0}, Ljih;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "item_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Ljih;->O:J

    .line 79
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Ljih;->n()Lz;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 80
    invoke-virtual {p0}, Ljih;->n()Lz;

    move-result-object v1

    const v2, 0x7f0a019d

    invoke-virtual {v1, v2}, Lz;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a019e

    new-instance v2, Ljij;

    invoke-direct {v2, p0}, Ljij;-><init>(Ljih;)V

    .line 81
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0a019f

    new-instance v2, Ljii;

    invoke-direct {v2}, Ljii;-><init>()V

    .line 89
    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 94
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 69
    invoke-super {p0, p1}, Lt;->e(Landroid/os/Bundle;)V

    .line 70
    const-string v0, "account_id"

    iget v1, p0, Ljih;->N:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 71
    const-string v0, "item_id"

    iget-wide v2, p0, Ljih;->O:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 72
    return-void
.end method
