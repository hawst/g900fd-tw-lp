.class public final Lkbe;
.super Lkba;
.source "PG"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lkba;-><init>(Landroid/content/Context;)V

    .line 34
    return-void
.end method


# virtual methods
.method public bridge synthetic a(I)V
    .locals 0

    .prologue
    .line 30
    invoke-super {p0, p1}, Lkba;->a(I)V

    return-void
.end method

.method public b(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 176
    const/4 v1, -0x1

    .line 177
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lkbe;->r:I

    if-ge v0, v2, :cond_2

    .line 178
    iget-object v2, p0, Lkbe;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eq p1, v2, :cond_0

    iget-object v2, p0, Lkbe;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-ne p1, v2, :cond_1

    .line 183
    :cond_0
    :goto_1
    return v0

    .line 177
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public bridge synthetic b()V
    .locals 0

    .prologue
    .line 30
    invoke-super {p0}, Lkba;->b()V

    return-void
.end method

.method public bridge synthetic d()V
    .locals 0

    .prologue
    .line 30
    invoke-super {p0}, Lkba;->d()V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 11

    .prologue
    .line 104
    const/4 v2, 0x0

    .line 105
    sget-object v0, Lkbe;->a:Llct;

    iget v5, v0, Llct;->m:I

    .line 108
    iget-object v0, p0, Lkbe;->e:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 109
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v6

    .line 110
    const/4 v1, 0x0

    .line 113
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredWidth()I

    move-result v3

    add-int/lit8 v3, v3, 0x0

    .line 114
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v4

    add-int/2addr v4, v5

    .line 110
    invoke-virtual {v0, v1, v5, v3, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->layout(IIII)V

    .line 117
    iget-object v1, p0, Lkbe;->h:Landroid/view/View;

    const/4 v3, 0x0

    add-int v4, v5, v6

    iget-object v7, p0, Lkbe;->h:Landroid/view/View;

    .line 120
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    sub-int/2addr v4, v7

    iget-object v7, p0, Lkbe;->h:Landroid/view/View;

    .line 121
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/lit8 v7, v7, 0x0

    add-int v8, v5, v6

    .line 117
    invoke-virtual {v1, v3, v4, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 124
    iget-object v1, p0, Lkbe;->b:Lkzw;

    invoke-virtual {v1}, Lkzw;->f()I

    move-result v7

    .line 125
    const/4 v1, 0x0

    move v3, v1

    move v4, v2

    :goto_0
    iget v1, p0, Lkbe;->r:I

    if-ge v3, v1, :cond_1

    .line 126
    iget-object v1, p0, Lkbe;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 127
    add-int v2, v5, v6

    .line 128
    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v8

    sub-int/2addr v2, v8

    sget v8, Lkbe;->i:I

    sub-int v8, v2, v8

    .line 130
    iget-object v2, p0, Lkbe;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 131
    sget v9, Lkbe;->i:I

    add-int/2addr v4, v9

    .line 133
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v9, v4

    .line 135
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    add-int/2addr v10, v8

    .line 138
    invoke-virtual {v2, v4, v8, v9, v10}, Landroid/view/View;->layout(IIII)V

    .line 145
    iget-object v2, p0, Lkbe;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 149
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v9, v4

    .line 150
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    add-int/2addr v10, v8

    .line 146
    invoke-virtual {v2, v4, v8, v9, v10}, Landroid/view/View;->layout(IIII)V

    .line 153
    invoke-virtual {p0, v3}, Lkbe;->c(I)V

    .line 156
    invoke-virtual {p0}, Lkbe;->m()Z

    move-result v2

    if-nez v2, :cond_0

    .line 160
    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v4

    .line 161
    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v8

    .line 157
    invoke-virtual {v1, v4, v8, v2, v9}, Landroid/widget/TextView;->layout(IIII)V

    .line 165
    :goto_1
    iget v1, p0, Lkbe;->c:I

    div-int/lit8 v2, v1, 0x2

    .line 125
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v4, v2

    goto :goto_0

    .line 163
    :cond_0
    invoke-virtual {p0, v3}, Lkbe;->d(I)V

    goto :goto_1

    .line 167
    :cond_1
    const/4 v1, -0x1

    if-eq v7, v1, :cond_2

    .line 168
    invoke-virtual {p0, v7}, Lkbe;->f(I)V

    .line 170
    :cond_2
    invoke-virtual {p0}, Lkbe;->d()V

    .line 171
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float v1, v0, v1

    iget-object v0, p0, Lkbe;->n:Ljava/util/ArrayList;

    const/4 v2, 0x0

    .line 172
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v1, v0

    iput v0, p0, Lkbe;->t:F

    .line 173
    return-void
.end method

.method protected onMeasure(II)V
    .locals 11

    .prologue
    const/4 v10, -0x2

    const/high16 v9, 0x40000000    # 2.0f

    const/4 v3, 0x0

    .line 46
    iget-object v0, p0, Lkbe;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, v10, v10}, Landroid/widget/ImageView;->measure(II)V

    .line 47
    iget v0, p0, Lkbe;->c:I

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 51
    iget-object v0, p0, Lkbe;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 52
    iget v2, p0, Lkbe;->d:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->measure(II)V

    .line 55
    iget-object v2, p0, Lkbe;->h:Landroid/view/View;

    iget v4, p0, Lkbe;->g:I

    invoke-virtual {v2, v1, v4}, Landroid/view/View;->measure(II)V

    .line 57
    iget v1, p0, Lkbe;->c:I

    div-int/lit8 v1, v1, 0x2

    sget v2, Lkbe;->i:I

    shl-int/lit8 v2, v2, 0x1

    sub-int v6, v1, v2

    .line 58
    invoke-static {v6, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    move v2, v3

    move v4, v3

    .line 62
    :goto_0
    iget v1, p0, Lkbe;->r:I

    if-ge v2, v1, :cond_2

    .line 63
    iget v1, p0, Lkbe;->q:I

    if-ne v2, v1, :cond_1

    iget-object v1, p0, Lkbe;->p:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v1

    move v5, v1

    .line 65
    :goto_1
    iget-object v1, p0, Lkbe;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 67
    sub-int v5, v6, v5

    sget v8, Lkbe;->k:I

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v5, v8

    invoke-static {v5, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 72
    invoke-virtual {v1, v5, v10}, Landroid/widget/TextView;->measure(II)V

    .line 74
    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    if-le v5, v4, :cond_0

    .line 75
    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    .line 62
    :cond_0
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_1
    move v5, v3

    .line 63
    goto :goto_1

    .line 79
    :cond_2
    :goto_2
    iget v1, p0, Lkbe;->r:I

    if-ge v3, v1, :cond_3

    .line 80
    iget-object v1, p0, Lkbe;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 82
    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    invoke-static {v2, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 83
    invoke-static {v4, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 87
    invoke-virtual {v1, v2, v5}, Landroid/widget/TextView;->measure(II)V

    .line 91
    iget-object v1, p0, Lkbe;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, v7, v5}, Landroid/view/View;->measure(II)V

    .line 95
    iget-object v1, p0, Lkbe;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, v10, v5}, Landroid/view/View;->measure(II)V

    .line 79
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 98
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v0

    sget-object v1, Lkbe;->a:Llct;

    iget v1, v1, Llct;->m:I

    add-int/2addr v0, v1

    .line 99
    iget v1, p0, Lkbe;->c:I

    invoke-virtual {p0, v1, v0}, Lkbe;->setMeasuredDimension(II)V

    .line 100
    return-void
.end method
