.class public final Lcfn;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:Landroid/graphics/Matrix;

.field public static final b:Landroid/graphics/Matrix;

.field public static final c:Landroid/graphics/Matrix;

.field private static final d:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[F>;"
        }
    .end annotation
.end field

.field private static final e:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[F>;"
        }
    .end annotation
.end field

.field private static final f:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<[F>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v1, 0x9

    const/high16 v3, 0x3f000000    # 0.5f

    .line 12
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, Lcfn;->a:Landroid/graphics/Matrix;

    .line 13
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, Lcfn;->b:Landroid/graphics/Matrix;

    .line 14
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    sput-object v0, Lcfn;->c:Landroid/graphics/Matrix;

    .line 16
    invoke-static {v1}, Lcfn;->a(I)Ljava/lang/ThreadLocal;

    move-result-object v0

    sput-object v0, Lcfn;->d:Ljava/lang/ThreadLocal;

    .line 17
    invoke-static {v1}, Lcfn;->a(I)Ljava/lang/ThreadLocal;

    move-result-object v0

    sput-object v0, Lcfn;->e:Ljava/lang/ThreadLocal;

    .line 18
    invoke-static {v1}, Lcfn;->a(I)Ljava/lang/ThreadLocal;

    move-result-object v0

    sput-object v0, Lcfn;->f:Ljava/lang/ThreadLocal;

    .line 21
    sget-object v0, Lcfn;->b:Landroid/graphics/Matrix;

    const/high16 v1, 0x3f800000    # 1.0f

    const/high16 v2, -0x40800000    # -1.0f

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/graphics/Matrix;->setScale(FFFF)V

    .line 22
    sget-object v0, Lcfn;->c:Landroid/graphics/Matrix;

    const/high16 v1, 0x43870000    # 270.0f

    invoke-virtual {v0, v1, v3, v3}, Landroid/graphics/Matrix;->setRotate(FFF)V

    .line 23
    return-void
.end method

.method public static a(DDD)D
    .locals 2

    .prologue
    .line 29
    cmpg-double v0, p0, p2

    if-gez v0, :cond_0

    .line 35
    :goto_0
    return-wide p2

    .line 32
    :cond_0
    cmpl-double v0, p0, p4

    if-lez v0, :cond_1

    move-wide p2, p4

    .line 33
    goto :goto_0

    :cond_1
    move-wide p2, p0

    .line 35
    goto :goto_0
.end method

.method public static a(Ljava/util/List;)D
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;)D"
        }
    .end annotation

    .prologue
    .line 156
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const-string v1, "values.size()"

    invoke-static {v0, v1}, Lcec;->a(ILjava/lang/CharSequence;)I

    .line 157
    const-wide/16 v0, 0x0

    .line 158
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 159
    long-to-double v0, v0

    add-double/2addr v0, v2

    move-wide v2, v0

    .line 160
    goto :goto_0

    .line 161
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    int-to-double v0, v0

    div-double v0, v2, v0

    return-wide v0
.end method

.method public static a(Ljava/util/List;D)D
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;D)D"
        }
    .end annotation

    .prologue
    .line 143
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    const-string v1, "values.size()"

    invoke-static {v0, v1}, Lcec;->a(ILjava/lang/CharSequence;)I

    .line 144
    const-wide/16 v0, 0x0

    .line 145
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 146
    long-to-double v0, v0

    sub-double/2addr v0, p1

    .line 147
    mul-double/2addr v0, v0

    add-double/2addr v0, v2

    move-wide v2, v0

    .line 148
    goto :goto_0

    .line 149
    :cond_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    int-to-double v0, v0

    div-double v0, v2, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static a(F)F
    .locals 2

    .prologue
    .line 72
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {p0, v0, v1}, Lcfn;->a(FFF)F

    move-result v0

    return v0
.end method

.method public static a(FF)F
    .locals 2

    .prologue
    .line 191
    mul-float v0, p0, p0

    mul-float v1, p1, p1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method public static a(FFF)F
    .locals 1

    .prologue
    .line 39
    cmpg-float v0, p0, p1

    if-gez v0, :cond_0

    .line 45
    :goto_0
    return p1

    .line 42
    :cond_0
    cmpl-float v0, p0, p2

    if-lez v0, :cond_1

    move p1, p2

    .line 43
    goto :goto_0

    :cond_1
    move p1, p0

    .line 45
    goto :goto_0
.end method

.method public static a(II)I
    .locals 1

    .prologue
    .line 130
    if-lez p0, :cond_0

    if-lez p1, :cond_0

    .line 131
    add-int v0, p0, p1

    add-int/lit8 v0, v0, -0x1

    div-int/2addr v0, p1

    .line 136
    :goto_0
    return v0

    .line 133
    :cond_0
    if-gez p0, :cond_1

    if-gez p1, :cond_1

    .line 134
    add-int v0, p0, p1

    add-int/lit8 v0, v0, 0x1

    div-int/2addr v0, p1

    goto :goto_0

    .line 136
    :cond_1
    div-int v0, p0, p1

    goto :goto_0
.end method

.method public static a(IIF)I
    .locals 1

    .prologue
    .line 103
    sub-int v0, p1, p0

    int-to-float v0, v0

    mul-float/2addr v0, p2

    float-to-int v0, v0

    add-int/2addr v0, p0

    return v0
.end method

.method public static a(III)I
    .locals 0

    .prologue
    .line 49
    if-ge p0, p1, :cond_0

    .line 55
    :goto_0
    return p1

    .line 52
    :cond_0
    if-le p0, p2, :cond_1

    move p1, p2

    .line 53
    goto :goto_0

    :cond_1
    move p1, p0

    .line 55
    goto :goto_0
.end method

.method public static a(JJJ)J
    .locals 2

    .prologue
    .line 59
    cmp-long v0, p0, p2

    if-gez v0, :cond_0

    .line 65
    :goto_0
    return-wide p2

    .line 62
    :cond_0
    cmp-long v0, p0, p4

    if-lez v0, :cond_1

    move-wide p2, p4

    .line 63
    goto :goto_0

    :cond_1
    move-wide p2, p0

    .line 65
    goto :goto_0
.end method

.method private static a(I)Ljava/lang/ThreadLocal;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/lang/ThreadLocal",
            "<[F>;"
        }
    .end annotation

    .prologue
    .line 196
    new-instance v0, Lcfo;

    invoke-direct {v0, p0}, Lcfo;-><init>(I)V

    return-object v0
.end method

.method public static a(Landroid/graphics/Matrix;Landroid/graphics/Matrix;FLandroid/graphics/Matrix;)V
    .locals 7

    .prologue
    .line 115
    sget-object v0, Lcfn;->d:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [F

    .line 116
    sget-object v1, Lcfn;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v1}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [F

    .line 117
    sget-object v2, Lcfn;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v2}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [F

    .line 118
    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->getValues([F)V

    .line 119
    invoke-virtual {p1, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 120
    const/4 v3, 0x0

    :goto_0
    const/16 v4, 0x9

    if-ge v3, v4, :cond_0

    .line 121
    aget v4, v0, v3

    aget v5, v1, v3

    aget v6, v0, v3

    sub-float/2addr v5, v6

    mul-float/2addr v5, p2

    add-float/2addr v4, v5

    aput v4, v2, v3

    .line 120
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 123
    :cond_0
    invoke-virtual {p3, v2}, Landroid/graphics/Matrix;->setValues([F)V

    .line 124
    return-void
.end method

.method public static a(Landroid/graphics/Matrix;[F)V
    .locals 7

    .prologue
    const/4 v6, 0x5

    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 178
    const/16 v0, 0x9

    new-array v0, v0, [F

    aget v1, p1, v2

    aput v1, v0, v2

    aget v1, p1, v5

    aput v1, v0, v3

    const/4 v1, 0x2

    const/16 v2, 0xc

    aget v2, p1, v2

    aput v2, v0, v1

    aget v1, p1, v3

    aput v1, v0, v4

    aget v1, p1, v6

    aput v1, v0, v5

    const/16 v1, 0xd

    aget v1, p1, v1

    aput v1, v0, v6

    const/4 v1, 0x6

    aget v2, p1, v4

    aput v2, v0, v1

    const/4 v1, 0x7

    const/4 v2, 0x7

    aget v2, p1, v2

    aput v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0xf

    aget v2, p1, v2

    aput v2, v0, v1

    .line 184
    invoke-virtual {p0, v0}, Landroid/graphics/Matrix;->setValues([F)V

    .line 185
    return-void
.end method

.method public static b(FFF)F
    .locals 2

    .prologue
    .line 82
    cmpl-float v0, p0, p1

    if-eqz v0, :cond_0

    sub-float v0, p2, p0

    sub-float v1, p1, p0

    div-float/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(FFF)F
    .locals 3

    .prologue
    .line 89
    invoke-static {p0, p1, p2}, Lcfn;->b(FFF)F

    move-result v0

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v0, v1, v2}, Lcfn;->a(FFF)F

    move-result v0

    return v0
.end method
