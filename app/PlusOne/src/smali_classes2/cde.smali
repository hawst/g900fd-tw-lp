.class public Lcde;
.super Lccv;
.source "PG"


# static fields
.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Lcfl;

.field private final d:Lcfl;

.field private e:Z

.field private f:Lcdf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 44
    const-class v0, Lcde;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcde;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcci;)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lccv;-><init>(Lcci;)V

    .line 63
    new-instance v0, Lcfl;

    invoke-direct {v0}, Lcfl;-><init>()V

    iput-object v0, p0, Lcde;->c:Lcfl;

    .line 64
    new-instance v0, Lcfl;

    invoke-direct {v0}, Lcfl;-><init>()V

    iput-object v0, p0, Lcde;->d:Lcfl;

    .line 65
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcde;->e:Z

    .line 66
    return-void
.end method

.method private a(Lcfl;Landroid/widget/Adapter;I)I
    .locals 7

    .prologue
    const/high16 v6, -0x80000000

    .line 245
    invoke-interface {p2, p3}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v0

    .line 246
    invoke-virtual {p1, v0, v1, v6}, Lcfl;->b(JI)I

    move-result v0

    .line 247
    if-ne v0, v6, :cond_0

    .line 248
    iget-object v1, p0, Lcde;->f:Lcdf;

    if-eqz v1, :cond_0

    .line 249
    iget-object v1, p0, Lcde;->f:Lcdf;

    invoke-interface {v1, p3}, Lcdf;->a(I)J

    move-result-wide v2

    .line 250
    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 251
    invoke-virtual {p1, v2, v3, v6}, Lcfl;->b(JI)I

    move-result v0

    .line 255
    :cond_0
    return v0
.end method

.method private static b(Lccj;)V
    .locals 4

    .prologue
    .line 230
    const/4 v0, 0x0

    iget-object v1, p0, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 231
    iget-object v0, p0, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    .line 232
    rsub-int v3, v1, 0x7fff

    int-to-short v3, v3

    iput-short v3, v0, Lccg;->d:S

    .line 230
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 234
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lccj;)V
    .locals 20

    .prologue
    .line 79
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcde;->e:Z

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcde;->c:Lcfl;

    move-object v3, v2

    .line 80
    :goto_0
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcde;->e:Z

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcde;->d:Lcfl;

    move-object v4, v2

    .line 81
    :goto_1
    invoke-virtual {v4}, Lcfl;->b()V

    const/4 v2, 0x0

    move-object/from16 v0, p1

    iget-object v5, v0, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v6

    move v5, v2

    :goto_2
    if-ge v5, v6, :cond_3

    move-object/from16 v0, p1

    iget-object v2, v0, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, v5}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v7

    move-object/from16 v0, p1

    iget-object v2, v0, Lccj;->h:Landroid/widget/Adapter;

    invoke-interface {v2, v7}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v8

    move-object/from16 v0, p1

    iget-object v2, v0, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lccg;

    iget-object v2, v2, Lccg;->a:Landroid/view/View;

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lccj;->a(Landroid/view/View;)I

    move-result v2

    invoke-virtual {v4, v8, v9, v2}, Lcfl;->a(JI)V

    move-object/from16 v0, p1

    iget-boolean v8, v0, Lccj;->f:Z

    if-eqz v8, :cond_0

    sget-object v8, Lcde;->b:Ljava/lang/String;

    const-string v8, "Snapshot %s: Position %s = %s"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v9, v10

    const/4 v7, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v9, v7

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    :cond_0
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_2

    .line 79
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcde;->d:Lcfl;

    move-object v3, v2

    goto :goto_0

    .line 80
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcde;->c:Lcfl;

    move-object v4, v2

    goto :goto_1

    .line 82
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcde;->e:Z

    if-nez v2, :cond_5

    const/4 v2, 0x1

    :goto_3
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcde;->e:Z

    .line 84
    const/16 v2, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lccj;->a(I)Z

    move-result v2

    if-nez v2, :cond_6

    .line 175
    :cond_4
    :goto_4
    return-void

    .line 82
    :cond_5
    const/4 v2, 0x0

    goto :goto_3

    .line 88
    :cond_6
    move-object/from16 v0, p1

    iget-object v2, v0, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v14

    .line 91
    const v8, 0x7fffffff

    .line 92
    const/high16 v7, -0x80000000

    .line 93
    const/4 v5, 0x0

    .line 94
    const/4 v4, 0x0

    .line 96
    const/4 v6, 0x0

    :goto_5
    if-ge v6, v14, :cond_8

    .line 97
    move-object/from16 v0, p1

    iget-object v2, v0, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, v6}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v9

    .line 98
    move-object/from16 v0, p1

    iget-object v2, v0, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, v6}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lccg;

    iget-object v2, v2, Lccg;->a:Landroid/view/View;

    .line 99
    move-object/from16 v0, p1

    iget-object v10, v0, Lccj;->h:Landroid/widget/Adapter;

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v10, v9}, Lcde;->a(Lcfl;Landroid/widget/Adapter;I)I

    move-result v9

    .line 102
    const/high16 v10, -0x80000000

    if-eq v9, v10, :cond_19

    .line 103
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lccj;->a(Landroid/view/View;)I

    move-result v2

    sub-int v2, v9, v2

    .line 104
    if-ge v6, v8, :cond_7

    move v5, v2

    move v8, v6

    .line 108
    :cond_7
    if-le v6, v7, :cond_19

    move v4, v5

    move v7, v8

    move v5, v6

    .line 96
    :goto_6
    add-int/lit8 v6, v6, 0x1

    move v8, v7

    move v7, v5

    move v5, v4

    move v4, v2

    goto :goto_5

    .line 115
    :cond_8
    if-gt v8, v7, :cond_4

    .line 119
    const/4 v11, 0x0

    .line 120
    const/4 v10, 0x0

    .line 121
    const/4 v9, 0x0

    .line 122
    const/4 v6, 0x0

    .line 124
    const/4 v2, 0x0

    move v13, v2

    :goto_7
    if-ge v13, v14, :cond_12

    .line 125
    move-object/from16 v0, p1

    iget-object v2, v0, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, v13}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v12

    .line 126
    move-object/from16 v0, p1

    iget-object v2, v0, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, v13}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lccg;

    .line 127
    iget-object v15, v2, Lccg;->a:Landroid/view/View;

    .line 128
    move-object/from16 v0, p1

    iget-object v0, v0, Lccj;->h:Landroid/widget/Adapter;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v3, v1, v12}, Lcde;->a(Lcfl;Landroid/widget/Adapter;I)I

    move-result v16

    .line 130
    if-lt v13, v8, :cond_b

    if-gt v13, v7, :cond_b

    const/4 v12, 0x1

    .line 132
    :goto_8
    const/high16 v17, -0x80000000

    move/from16 v0, v16

    move/from16 v1, v17

    if-ne v0, v1, :cond_c

    if-eqz v12, :cond_c

    .line 134
    move-object/from16 v0, p1

    iget-boolean v12, v0, Lccj;->f:Z

    if-eqz v12, :cond_9

    .line 135
    sget-object v12, Lcde;->b:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/String;->length()I

    move-result v16

    add-int/lit8 v16, v16, 0xf

    move/from16 v0, v16

    invoke-direct {v12, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v16, "Fading in item "

    move-object/from16 v0, v16

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    :cond_9
    invoke-virtual/range {p1 .. p1}, Lccj;->c()V

    .line 143
    const/4 v2, 0x0

    invoke-virtual {v15, v2}, Landroid/view/View;->setAlpha(F)V

    .line 144
    if-nez v10, :cond_a

    .line 145
    new-instance v10, Landroid/animation/AnimatorSet;

    invoke-direct {v10}, Landroid/animation/AnimatorSet;-><init>()V

    .line 147
    :cond_a
    sget-object v2, Landroid/view/View;->ALPHA:Landroid/util/Property;

    const/4 v12, 0x2

    new-array v12, v12, [F

    fill-array-data v12, :array_0

    .line 148
    invoke-static {v15, v2, v12}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 147
    invoke-static {v10, v6, v2}, Lcad;->a(Landroid/animation/AnimatorSet;Landroid/animation/AnimatorSet$Builder;Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    move-object v6, v9

    move-object v9, v10

    move-object v10, v11

    .line 124
    :goto_9
    add-int/lit8 v11, v13, 0x1

    move v13, v11

    move-object v11, v10

    move-object v10, v9

    move-object v9, v6

    move-object v6, v2

    goto/16 :goto_7

    .line 130
    :cond_b
    const/4 v12, 0x0

    goto :goto_8

    .line 151
    :cond_c
    if-gt v13, v8, :cond_f

    move v12, v5

    .line 158
    :goto_a
    if-eqz v12, :cond_18

    .line 160
    move-object/from16 v0, p1

    iget-boolean v0, v0, Lccj;->f:Z

    move/from16 v16, v0

    if-eqz v16, :cond_d

    .line 161
    sget-object v16, Lcde;->b:Ljava/lang/String;

    const-string v16, "Moving item %s by %s"

    const/16 v17, 0x2

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    aput-object v2, v17, v18

    const/4 v2, 0x1

    .line 162
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lccj;->a(Landroid/view/View;)I

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v17, v2

    .line 161
    invoke-static/range {v16 .. v17}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 164
    :cond_d
    if-nez v11, :cond_e

    .line 165
    new-instance v11, Landroid/animation/AnimatorSet;

    invoke-direct {v11}, Landroid/animation/AnimatorSet;-><init>()V

    .line 167
    :cond_e
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lccj;->a:Z

    if-eqz v2, :cond_11

    sget-object v2, Landroid/view/View;->TRANSLATION_X:Landroid/util/Property;

    :goto_b
    const/16 v16, 0x2

    move/from16 v0, v16

    new-array v0, v0, [F

    move-object/from16 v16, v0

    const/16 v17, 0x0

    int-to-float v12, v12

    aput v12, v16, v17

    const/4 v12, 0x1

    const/16 v17, 0x0

    aput v17, v16, v12

    .line 168
    move-object/from16 v0, v16

    invoke-static {v15, v2, v0}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Landroid/util/Property;[F)Landroid/animation/ObjectAnimator;

    move-result-object v2

    .line 167
    invoke-static {v11, v9, v2}, Lcad;->a(Landroid/animation/AnimatorSet;Landroid/animation/AnimatorSet$Builder;Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v2

    move-object v9, v10

    move-object v10, v11

    move-object/from16 v19, v6

    move-object v6, v2

    move-object/from16 v2, v19

    goto :goto_9

    .line 153
    :cond_f
    if-lt v13, v7, :cond_10

    move v12, v4

    .line 154
    goto :goto_a

    .line 156
    :cond_10
    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Lccj;->a(Landroid/view/View;)I

    move-result v12

    sub-int v12, v16, v12

    goto :goto_a

    .line 167
    :cond_11
    sget-object v2, Landroid/view/View;->TRANSLATION_Y:Landroid/util/Property;

    goto :goto_b

    .line 174
    :cond_12
    if-nez v11, :cond_13

    if-eqz v10, :cond_4

    :cond_13
    if-eqz v11, :cond_14

    const-wide/16 v2, 0xfa

    invoke-virtual {v11, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    invoke-static/range {p1 .. p1}, Lcde;->b(Lccj;)V

    :cond_14
    if-eqz v10, :cond_15

    const-wide/16 v2, 0xfa

    invoke-virtual {v10, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    :cond_15
    if-eqz v11, :cond_16

    if-eqz v10, :cond_16

    const-wide/16 v2, 0xfa

    invoke-virtual {v11, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    const-wide/16 v2, 0xfa

    invoke-virtual {v10, v2, v3}, Landroid/animation/AnimatorSet;->setStartDelay(J)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v10, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    new-instance v2, Landroid/animation/AnimatorSet;

    invoke-direct {v2}, Landroid/animation/AnimatorSet;-><init>()V

    sget-object v3, Lcak;->a:Lcak;

    invoke-virtual {v2, v3}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    invoke-virtual {v2, v11}, Landroid/animation/AnimatorSet;->play(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    move-result-object v3

    invoke-virtual {v3, v10}, Landroid/animation/AnimatorSet$Builder;->with(Landroid/animation/Animator;)Landroid/animation/AnimatorSet$Builder;

    const/16 v3, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Lccj;->a(Landroid/animation/Animator;I)V

    goto/16 :goto_4

    :cond_16
    if-eqz v11, :cond_17

    sget-object v2, Lcak;->a:Lcak;

    invoke-virtual {v11, v2}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v11, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    const/16 v2, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v11, v2}, Lccj;->a(Landroid/animation/Animator;I)V

    goto/16 :goto_4

    :cond_17
    if-eqz v10, :cond_4

    sget-object v2, Lcak;->a:Lcak;

    invoke-virtual {v10, v2}, Landroid/animation/AnimatorSet;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    const-wide/16 v2, 0xfa

    invoke-virtual {v10, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    const/16 v2, 0x19

    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v2}, Lccj;->a(Landroid/animation/Animator;I)V

    goto/16 :goto_4

    :cond_18
    move-object v2, v6

    move-object v6, v9

    move-object v9, v10

    move-object v10, v11

    goto/16 :goto_9

    :cond_19
    move v2, v4

    move v4, v5

    move v5, v7

    move v7, v8

    goto/16 :goto_6

    .line 147
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public a(Lcdf;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcde;->f:Lcdf;

    .line 72
    return-void
.end method
