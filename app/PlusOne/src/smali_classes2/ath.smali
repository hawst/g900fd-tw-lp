.class public final Lath;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lath;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lath;->a:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 179
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 180
    return-void
.end method

.method public static a()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public static a(Z)I
    .locals 1

    .prologue
    .line 86
    if-eqz p0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(III)Landroid/media/MediaFormat;
    .locals 3

    .prologue
    .line 188
    const-string v0, "video/avc"

    invoke-static {v0, p0, p1}, Landroid/media/MediaFormat;->createVideoFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v0

    .line 189
    const-string v1, "color-format"

    const v2, 0x7f000789

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 194
    const-string v1, "bitrate"

    invoke-virtual {v0, v1, p2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 195
    const-string v1, "frame-rate"

    const/16 v2, 0x1e

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 196
    const-string v1, "i-frame-interval"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 197
    return-object v0
.end method

.method private static a(Landroid/media/CamcorderProfile;)Landroid/media/MediaFormat;
    .locals 3

    .prologue
    .line 183
    iget v0, p0, Landroid/media/CamcorderProfile;->videoFrameWidth:I

    iget v1, p0, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    iget v2, p0, Landroid/media/CamcorderProfile;->videoBitRate:I

    invoke-static {v0, v1, v2}, Lath;->a(III)Landroid/media/MediaFormat;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/media/MediaFormat;)Landroid/media/MediaFormat;
    .locals 3

    .prologue
    const/16 v2, 0x2d0

    .line 91
    const-string v0, "decodingVideoFormat"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 92
    const-string v0, "width"

    invoke-virtual {p0, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    .line 93
    const-string v1, "height"

    invoke-virtual {p0, v1}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    .line 94
    if-ge v1, v2, :cond_0

    .line 95
    const v2, 0x4c4b40

    invoke-static {v0, v1, v2}, Lath;->a(III)Landroid/media/MediaFormat;

    move-result-object v0

    .line 99
    :goto_0
    return-object v0

    .line 96
    :cond_0
    if-le v1, v2, :cond_1

    .line 97
    const v2, 0xb71b00

    invoke-static {v0, v1, v2}, Lath;->a(III)Landroid/media/MediaFormat;

    move-result-object v0

    goto :goto_0

    .line 99
    :cond_1
    const v2, 0x7a1200

    invoke-static {v0, v1, v2}, Lath;->a(III)Landroid/media/MediaFormat;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Latu;Z)Landroid/media/MediaFormat;
    .locals 6

    .prologue
    const/16 v5, 0x2d0

    const/16 v3, 0x40

    .line 111
    const-string v0, "resolution"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 113
    if-eqz p1, :cond_0

    .line 115
    :try_start_0
    iget v0, p0, Latu;->e:I

    invoke-static {v0}, Landroid/media/CamcorderProfile;->hasProfile(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Latu;->e:I

    invoke-static {v0}, Landroid/media/CamcorderProfile;->get(I)Landroid/media/CamcorderProfile;

    move-result-object v0

    .line 119
    :goto_0
    sget-object v1, Lati;->a:[I

    invoke-virtual {p0}, Latu;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 137
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x16

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "unexpected resolution "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 142
    :catch_0
    move-exception v0

    sget-object v0, Lath;->a:Ljava/lang/String;

    .line 147
    :cond_0
    :goto_1
    sget-object v0, Lath;->a:Ljava/lang/String;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "using default video format for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 148
    sget-object v0, Lati;->a:[I

    invoke-virtual {p0}, Latu;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 156
    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x16

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "unexpected resolution "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 115
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    iget v1, p0, Latu;->e:I

    invoke-static {v0, v1}, Landroid/media/CamcorderProfile;->hasProfile(II)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    iget v1, p0, Latu;->e:I

    invoke-static {v0, v1}, Landroid/media/CamcorderProfile;->get(II)Landroid/media/CamcorderProfile;

    move-result-object v0

    goto/16 :goto_0

    :cond_2
    iget v0, p0, Latu;->e:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    const/4 v0, 0x0

    invoke-static {v0}, Landroid/media/CamcorderProfile;->get(I)Landroid/media/CamcorderProfile;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x1

    invoke-static {v0}, Landroid/media/CamcorderProfile;->get(I)Landroid/media/CamcorderProfile;

    move-result-object v0

    goto/16 :goto_0

    .line 122
    :pswitch_0
    iget v1, v0, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    add-int/lit16 v1, v1, -0x1e0

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-gt v1, v3, :cond_4

    .line 123
    invoke-static {v0}, Lath;->a(Landroid/media/CamcorderProfile;)Landroid/media/MediaFormat;

    move-result-object v0

    .line 154
    :goto_2
    return-object v0

    .line 127
    :pswitch_1
    iget v1, v0, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    add-int/lit16 v1, v1, -0x2d0

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-gt v1, v3, :cond_4

    .line 128
    invoke-static {v0}, Lath;->a(Landroid/media/CamcorderProfile;)Landroid/media/MediaFormat;

    move-result-object v0

    goto :goto_2

    .line 132
    :pswitch_2
    iget v1, v0, Landroid/media/CamcorderProfile;->videoFrameHeight:I

    add-int/lit16 v1, v1, -0x438

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-gt v1, v3, :cond_4

    .line 133
    invoke-static {v0}, Lath;->a(Landroid/media/CamcorderProfile;)Landroid/media/MediaFormat;

    move-result-object v0

    goto :goto_2

    .line 139
    :cond_4
    sget-object v1, Lath;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x18

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v3, v4

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "unsuitable profile "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " for "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_1

    .line 150
    :pswitch_3
    const/16 v0, 0x1e0

    const v1, 0x4c4b40

    invoke-static {v5, v0, v1}, Lath;->a(III)Landroid/media/MediaFormat;

    move-result-object v0

    goto :goto_2

    .line 152
    :pswitch_4
    const/16 v0, 0x500

    const v1, 0x7a1200

    invoke-static {v0, v5, v1}, Lath;->a(III)Landroid/media/MediaFormat;

    move-result-object v0

    goto :goto_2

    .line 154
    :pswitch_5
    const/16 v0, 0x780

    const/16 v1, 0x438

    const v2, 0xb71b00

    invoke-static {v0, v1, v2}, Lath;->a(III)Landroid/media/MediaFormat;

    move-result-object v0

    goto :goto_2

    .line 119
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 148
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static b(Z)Landroid/media/MediaFormat;
    .locals 4

    .prologue
    const/4 v0, 0x2

    .line 162
    const-string v1, "audio/mp4a-latm"

    const v2, 0xac44

    invoke-static {v1, v2, v0}, Landroid/media/MediaFormat;->createAudioFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v1

    .line 165
    const-string v2, "bitrate"

    const/high16 v3, 0x20000

    invoke-virtual {v1, v2, v3}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 166
    const-string v2, "aac-profile"

    if-eqz p0, :cond_0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 168
    return-object v1

    .line 166
    :cond_0
    const/4 v0, 0x5

    goto :goto_0
.end method

.method public static b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    const-string v0, "video/avc"

    return-object v0
.end method

.method public static c()I
    .locals 1

    .prologue
    .line 176
    const/high16 v0, 0x200000

    return v0
.end method
