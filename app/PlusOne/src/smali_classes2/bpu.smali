.class public final Lbpu;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Lbmp;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/lang/Iterable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Iterable",
            "<",
            "Lbmp;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Landroid/util/LongSparseArray;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<",
            "Lbmp;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lbpu;->b:Ljava/lang/Iterable;

    .line 41
    iput-object p1, p0, Lbpu;->a:Landroid/util/LongSparseArray;

    .line 42
    return-void
.end method

.method public static a()Lbpu;
    .locals 2

    .prologue
    .line 32
    new-instance v0, Lbpu;

    new-instance v1, Landroid/util/LongSparseArray;

    invoke-direct {v1}, Landroid/util/LongSparseArray;-><init>()V

    invoke-direct {v0, v1}, Lbpu;-><init>(Landroid/util/LongSparseArray;)V

    return-object v0
.end method

.method private a(JJ)V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 237
    iget-object v0, p0, Lbpu;->a:Landroid/util/LongSparseArray;

    invoke-static {v0, p1, p2}, Lcfk;->c(Landroid/util/LongSparseArray;J)I

    move-result v1

    .line 238
    if-ne v1, v4, :cond_1

    .line 257
    :cond_0
    :goto_0
    return-void

    .line 242
    :cond_1
    iget-object v0, p0, Lbpu;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v2

    .line 243
    cmp-long v0, v2, p3

    if-gez v0, :cond_0

    .line 247
    iget-object v0, p0, Lbpu;->a:Landroid/util/LongSparseArray;

    invoke-static {v0, p3, p4}, Lcfk;->b(Landroid/util/LongSparseArray;J)I

    move-result v0

    .line 248
    const-string v2, "indexBelowUB"

    const/4 v3, 0x0

    invoke-static {v0, v2, v4, v3}, Lcgp;->b(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)I

    .line 251
    :goto_1
    if-lt v0, v1, :cond_2

    .line 252
    iget-object v2, p0, Lbpu;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v2, v0}, Landroid/util/LongSparseArray;->removeAt(I)V

    .line 251
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 256
    :cond_2
    iget-object v0, p0, Lbpu;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    goto :goto_0
.end method

.method private d(Lbmp;)V
    .locals 4

    .prologue
    .line 260
    iget-object v0, p0, Lbpu;->a:Landroid/util/LongSparseArray;

    iget-wide v2, p1, Lbmp;->b:J

    invoke-virtual {v0, v2, v3, p1}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 261
    return-void
.end method


# virtual methods
.method public a(Lbmp;)V
    .locals 9

    .prologue
    const/4 v8, -0x1

    .line 70
    const-string v0, "intervalToAdd"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 71
    invoke-virtual {p1}, Lbmp;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 112
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    iget-wide v2, p1, Lbmp;->b:J

    .line 78
    iget-wide v4, p1, Lbmp;->c:J

    .line 80
    iget-object v0, p0, Lbpu;->a:Landroid/util/LongSparseArray;

    invoke-static {v0, v2, v3}, Lcfk;->a(Landroid/util/LongSparseArray;J)I

    move-result v0

    .line 81
    if-eq v0, v8, :cond_2

    .line 83
    iget-object v1, p0, Lbpu;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v1, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmp;

    .line 84
    iget-wide v6, v0, Lbmp;->c:J

    cmp-long v1, v6, v2

    if-ltz v1, :cond_2

    .line 86
    iget-wide v2, v0, Lbmp;->c:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 90
    iget-wide v0, v0, Lbmp;->b:J

    move-wide v2, v0

    .line 94
    :cond_2
    iget-object v0, p0, Lbpu;->a:Landroid/util/LongSparseArray;

    invoke-static {v0, v4, v5}, Lcfk;->a(Landroid/util/LongSparseArray;J)I

    move-result v0

    .line 95
    if-eq v0, v8, :cond_5

    .line 97
    iget-object v1, p0, Lbpu;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v1, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmp;

    .line 98
    iget-wide v6, v0, Lbmp;->c:J

    cmp-long v1, v6, v4

    if-ltz v1, :cond_5

    .line 100
    iget-wide v0, v0, Lbmp;->c:J

    .line 104
    :goto_1
    invoke-direct {p0, v2, v3, v0, v1}, Lbpu;->a(JJ)V

    .line 106
    iget-wide v4, p1, Lbmp;->b:J

    cmp-long v4, v2, v4

    if-nez v4, :cond_3

    iget-wide v4, p1, Lbmp;->c:J

    cmp-long v4, v0, v4

    if-eqz v4, :cond_4

    .line 108
    :cond_3
    new-instance v4, Lbmp;

    invoke-direct {v4, v2, v3, v0, v1}, Lbmp;-><init>(JJ)V

    invoke-direct {p0, v4}, Lbpu;->d(Lbmp;)V

    goto :goto_0

    .line 110
    :cond_4
    invoke-direct {p0, p1}, Lbpu;->d(Lbmp;)V

    goto :goto_0

    :cond_5
    move-wide v0, v4

    goto :goto_1
.end method

.method public a(Lbpu;)V
    .locals 2

    .prologue
    .line 176
    iget-object v0, p1, Lbpu;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 177
    iget-object v0, p1, Lbpu;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmp;

    invoke-virtual {p0, v0}, Lbpu;->b(Lbmp;)V

    .line 176
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 179
    :cond_0
    return-void
.end method

.method public b()Ljava/lang/Iterable;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/lang/Iterable",
            "<",
            "Lbmp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 52
    iget-object v0, p0, Lbpu;->b:Ljava/lang/Iterable;

    if-nez v0, :cond_0

    .line 53
    new-instance v0, Lbpv;

    invoke-direct {v0, p0}, Lbpv;-><init>(Lbpu;)V

    iput-object v0, p0, Lbpu;->b:Ljava/lang/Iterable;

    .line 60
    :cond_0
    iget-object v0, p0, Lbpu;->b:Ljava/lang/Iterable;

    return-object v0
.end method

.method public b(Lbmp;)V
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 129
    const-string v0, "intervalToRemove"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 131
    invoke-virtual {p1}, Lbmp;->a()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 168
    :goto_0
    return-void

    .line 138
    :cond_0
    iget-object v0, p0, Lbpu;->a:Landroid/util/LongSparseArray;

    iget-wide v2, p1, Lbmp;->b:J

    .line 139
    invoke-static {v0, v2, v3}, Lcfk;->a(Landroid/util/LongSparseArray;J)I

    move-result v0

    .line 140
    if-eq v0, v6, :cond_2

    .line 142
    iget-object v1, p0, Lbpu;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v1, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmp;

    .line 143
    iget-wide v2, v0, Lbmp;->c:J

    iget-wide v4, p1, Lbmp;->b:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_2

    .line 145
    iget-wide v2, v0, Lbmp;->c:J

    iget-wide v4, p1, Lbmp;->c:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 147
    new-instance v1, Lbmp;

    iget-wide v2, p1, Lbmp;->c:J

    iget-wide v4, v0, Lbmp;->c:J

    invoke-direct {v1, v2, v3, v4, v5}, Lbmp;-><init>(JJ)V

    invoke-direct {p0, v1}, Lbpu;->d(Lbmp;)V

    .line 150
    :cond_1
    new-instance v1, Lbmp;

    iget-wide v2, v0, Lbmp;->b:J

    iget-wide v4, p1, Lbmp;->b:J

    invoke-direct {v1, v2, v3, v4, v5}, Lbmp;-><init>(JJ)V

    invoke-direct {p0, v1}, Lbpu;->d(Lbmp;)V

    .line 155
    :cond_2
    iget-object v0, p0, Lbpu;->a:Landroid/util/LongSparseArray;

    iget-wide v2, p1, Lbmp;->c:J

    .line 156
    invoke-static {v0, v2, v3}, Lcfk;->a(Landroid/util/LongSparseArray;J)I

    move-result v0

    .line 157
    if-eq v0, v6, :cond_3

    .line 159
    iget-object v1, p0, Lbpu;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v1, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmp;

    .line 160
    iget-wide v2, v0, Lbmp;->c:J

    iget-wide v4, p1, Lbmp;->c:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_3

    .line 162
    new-instance v1, Lbmp;

    iget-wide v2, p1, Lbmp;->c:J

    iget-wide v4, v0, Lbmp;->c:J

    invoke-direct {v1, v2, v3, v4, v5}, Lbmp;-><init>(JJ)V

    invoke-direct {p0, v1}, Lbpu;->d(Lbmp;)V

    .line 167
    :cond_3
    iget-wide v0, p1, Lbmp;->b:J

    iget-wide v2, p1, Lbmp;->c:J

    invoke-direct {p0, v0, v1, v2, v3}, Lbpu;->a(JJ)V

    goto :goto_0
.end method

.method public c(Lbmp;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 188
    const-string v0, "interval"

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 189
    iget-object v0, p0, Lbpu;->a:Landroid/util/LongSparseArray;

    iget-wide v2, p1, Lbmp;->b:J

    invoke-static {v0, v2, v3}, Lcfk;->a(Landroid/util/LongSparseArray;J)I

    move-result v0

    .line 190
    const/4 v2, -0x1

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 195
    :goto_0
    return v0

    .line 194
    :cond_0
    iget-object v2, p0, Lbpu;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v2, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmp;

    .line 195
    iget-wide v2, v0, Lbmp;->c:J

    iget-wide v4, p1, Lbmp;->c:J

    cmp-long v0, v2, v4

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 219
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 220
    const-string v0, "["

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    iget-object v0, p0, Lbpu;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v3

    .line 222
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 223
    iget-object v0, p0, Lbpu;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmp;

    invoke-virtual {v0}, Lbmp;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 224
    add-int/lit8 v0, v3, -0x1

    if-eq v1, v0, :cond_0

    .line 225
    const-string v0, ", "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 222
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 228
    :cond_1
    const-string v0, "]"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 229
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
