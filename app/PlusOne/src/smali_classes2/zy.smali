.class final Lzy;
.super Lzx;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x9
.end annotation


# instance fields
.field private e:Landroid/view/SurfaceView;

.field private f:[B

.field private g:[B

.field private h:Z

.field private i:[F

.field private j:Ljava/lang/Object;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Landroid/hardware/Camera$PreviewCallback;


# direct methods
.method constructor <init>(Lzw;)V
    .locals 1

    .prologue
    .line 479
    invoke-direct {p0}, Lzx;-><init>()V

    .line 484
    const/4 v0, 0x1

    iput-boolean v0, p0, Lzy;->h:Z

    .line 485
    const/16 v0, 0x8

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lzy;->i:[F

    .line 486
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lzy;->j:Ljava/lang/Object;

    .line 488
    const-string v0, "precision mediump float;\n\nuniform sampler2D tex_sampler_0;\nvarying vec2 v_y_texcoord;\nvarying vec2 v_vu_texcoord;\nvarying vec2 v_pixcoord;\n\nvec3 select(vec4 yyyy, vec4 vuvu, int s) {\n  if (s == 0) {\n    return vec3(yyyy.r, vuvu.g, vuvu.r);\n  } else if (s == 1) {\n    return vec3(yyyy.g, vuvu.g, vuvu.r);\n } else if (s == 2) {\n    return vec3(yyyy.b, vuvu.a, vuvu.b);\n  } else  {\n    return vec3(yyyy.a, vuvu.a, vuvu.b);\n  }\n}\n\nvec3 yuv2rgb(vec3 yuv) {\n  mat4 conversion = mat4(1.0,  0.0,    1.402, -0.701,\n                         1.0, -0.344, -0.714,  0.529,\n                         1.0,  1.772,  0.0,   -0.886,\n                         0, 0, 0, 0);  return (vec4(yuv, 1.0) * conversion).rgb;\n}\n\nvoid main() {\n  vec4 yyyy = texture2D(tex_sampler_0, v_y_texcoord);\n  vec4 vuvu = texture2D(tex_sampler_0, v_vu_texcoord);\n  int s = int(mod(floor(v_pixcoord.x), 4.0));\n  vec3 yuv = select(yyyy, vuvu, s);\n  vec3 rgb = yuv2rgb(yuv);\n  gl_FragColor = vec4(rgb, 1.0);\n}"

    iput-object v0, p0, Lzy;->k:Ljava/lang/String;

    .line 525
    const-string v0, "attribute vec4 a_position;\nattribute vec2 a_y_texcoord;\nattribute vec2 a_vu_texcoord;\nattribute vec2 a_pixcoord;\nvarying vec2 v_y_texcoord;\nvarying vec2 v_vu_texcoord;\nvarying vec2 v_pixcoord;\nvoid main() {\n  gl_Position = a_position;\n  v_y_texcoord = a_y_texcoord;\n  v_vu_texcoord = a_vu_texcoord;\n  v_pixcoord = a_pixcoord;\n}\n"

    iput-object v0, p0, Lzy;->l:Ljava/lang/String;

    .line 558
    new-instance v0, Lzz;

    invoke-direct {v0, p0}, Lzz;-><init>(Lzy;)V

    iput-object v0, p0, Lzy;->m:Landroid/hardware/Camera$PreviewCallback;

    return-void

    .line 485
    :array_0
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private h()[B
    .locals 2

    .prologue
    .line 541
    iget-object v1, p0, Lzy;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 542
    :try_start_0
    iget-boolean v0, p0, Lzy;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lzy;->f:[B

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    iget-object v0, p0, Lzy;->g:[B

    goto :goto_0

    .line 543
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 571
    iget v0, p0, Lzy;->a:I

    rem-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Camera width must be a multiple of 4!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Lzy;->b:I

    rem-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Camera height must be a multiple of 2!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 572
    :cond_1
    iget-object v0, p0, Lzy;->c:Laaf;

    invoke-static {v0}, Laaf;->a(Laaf;)Landroid/hardware/Camera;

    move-result-object v0

    .line 573
    iget v1, p0, Lzy;->a:I

    iget v2, p0, Lzy;->b:I

    iget v3, p0, Lzy;->b:I

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    mul-int/2addr v1, v2

    .line 574
    new-array v2, v1, [B

    iput-object v2, p0, Lzy;->f:[B

    .line 575
    new-array v1, v1, [B

    iput-object v1, p0, Lzy;->g:[B

    .line 576
    invoke-virtual {p0}, Lzy;->f()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->addCallbackBuffer([B)V

    .line 577
    iget-object v1, p0, Lzy;->m:Landroid/hardware/Camera$PreviewCallback;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewCallbackWithBuffer(Landroid/hardware/Camera$PreviewCallback;)V

    .line 578
    iget-object v1, p0, Lzy;->e:Landroid/view/SurfaceView;

    if-nez v1, :cond_2

    iget-object v1, p0, Lzy;->c:Laaf;

    invoke-virtual {v1}, Laaf;->a()Lacs;

    move-result-object v1

    invoke-virtual {v1}, Lacs;->d()Landroid/view/SurfaceView;

    move-result-object v1

    iput-object v1, p0, Lzy;->e:Landroid/view/SurfaceView;

    :cond_2
    iget-object v1, p0, Lzy;->e:Landroid/view/SurfaceView;

    .line 579
    if-eqz v1, :cond_3

    .line 581
    :try_start_0
    invoke-virtual {v1}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 587
    :cond_3
    return-void

    .line 583
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not start camera with given preview display!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Laas;)V
    .locals 10

    .prologue
    const/16 v9, 0x2600

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 605
    invoke-static {}, Lacw;->h()Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v0

    .line 608
    invoke-virtual {p0, v0}, Lzy;->d(Ljavax/microedition/khronos/egl/EGLContext;)Lada;

    move-result-object v1

    .line 609
    iget v2, p0, Lzy;->a:I

    div-int/lit8 v2, v2, 0x4

    .line 610
    iget v3, p0, Lzy;->b:I

    iget v4, p0, Lzy;->b:I

    div-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    .line 611
    iget-object v4, p0, Lzy;->j:Ljava/lang/Object;

    monitor-enter v4

    .line 612
    :try_start_0
    invoke-direct {p0}, Lzy;->h()[B

    move-result-object v5

    invoke-static {v5}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v5

    .line 613
    invoke-virtual {v1, v5, v2, v3}, Lada;->a(Ljava/nio/ByteBuffer;II)V

    .line 614
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 615
    const/16 v2, 0x2800

    invoke-virtual {v1, v2, v9}, Lada;->b(II)V

    .line 616
    const/16 v2, 0x2801

    invoke-virtual {v1, v2, v9}, Lada;->b(II)V

    .line 619
    invoke-virtual {p0, v0}, Lzy;->e(Ljavax/microedition/khronos/egl/EGLContext;)Lacm;

    move-result-object v0

    .line 620
    iget-object v2, p0, Lzy;->i:[F

    invoke-virtual {v0, v2}, Lacm;->c([F)V

    .line 621
    const/16 v2, 0x8

    new-array v2, v2, [F

    aput v6, v2, v7

    const/4 v3, 0x1

    aput v6, v2, v3

    iget v3, p0, Lzy;->a:I

    int-to-float v3, v3

    aput v3, v2, v8

    const/4 v3, 0x3

    aput v6, v2, v3

    const/4 v3, 0x4

    aput v6, v2, v3

    const/4 v3, 0x5

    iget v4, p0, Lzy;->b:I

    int-to-float v4, v4

    aput v4, v2, v3

    const/4 v3, 0x6

    iget v4, p0, Lzy;->a:I

    int-to-float v4, v4

    aput v4, v2, v3

    const/4 v3, 0x7

    iget v4, p0, Lzy;->b:I

    int-to-float v4, v4

    aput v4, v2, v3

    const-string v3, "a_pixcoord"

    invoke-virtual {v0, v3, v2, v8}, Lacm;->a(Ljava/lang/String;[FI)V

    .line 624
    new-array v2, v8, [I

    fill-array-data v2, :array_0

    invoke-virtual {p1, v2}, Laas;->a([I)V

    .line 625
    invoke-virtual {p1}, Laas;->m()Lacw;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v7, v7}, Lacm;->a(Lada;Lacw;II)V

    .line 629
    invoke-virtual {p1}, Laas;->h()V

    .line 630
    return-void

    .line 614
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 624
    nop

    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method public b()V
    .locals 3

    .prologue
    .line 600
    iget-object v1, p0, Lzy;->d:Ljava/util/Vector;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lzy;->d:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laai;

    invoke-interface {v0}, Laai;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 670
    iput-object v0, p0, Lzy;->g:[B

    .line 671
    iput-object v0, p0, Lzy;->f:[B

    .line 672
    return-void
.end method

.method protected d()Lacm;
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v4, 0x2

    .line 681
    new-instance v0, Lacm;

    iget-object v1, p0, Lzy;->l:Ljava/lang/String;

    iget-object v2, p0, Lzy;->k:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lacm;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    new-array v1, v3, [F

    fill-array-data v1, :array_0

    .line 688
    new-array v2, v3, [F

    fill-array-data v2, :array_1

    .line 693
    const-string v3, "a_y_texcoord"

    invoke-virtual {v0, v3, v1, v4}, Lacm;->a(Ljava/lang/String;[FI)V

    .line 694
    const-string v1, "a_vu_texcoord"

    invoke-virtual {v0, v1, v2, v4}, Lacm;->a(Ljava/lang/String;[FI)V

    .line 695
    return-object v0

    .line 683
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f2aaaab
        0x3f800000    # 1.0f
        0x3f2aaaab
    .end array-data

    .line 688
    :array_1
    .array-data 4
        0x0
        0x3f2aaaab
        0x3f800000    # 1.0f
        0x3f2aaaab
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method protected e()Lada;
    .locals 3

    .prologue
    const/16 v2, 0x2600

    .line 700
    invoke-static {}, Lada;->a()Lada;

    move-result-object v0

    .line 701
    const/16 v1, 0x2800

    invoke-virtual {v0, v1, v2}, Lada;->b(II)V

    .line 702
    const/16 v1, 0x2801

    invoke-virtual {v0, v1, v2}, Lada;->b(II)V

    .line 703
    return-object v0
.end method

.method f()[B
    .locals 2

    .prologue
    .line 547
    iget-object v1, p0, Lzy;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 548
    :try_start_0
    iget-boolean v0, p0, Lzy;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lzy;->g:[B

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    iget-object v0, p0, Lzy;->f:[B

    goto :goto_0

    .line 549
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method declared-synchronized g()V
    .locals 2

    .prologue
    .line 553
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lzy;->j:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 554
    :try_start_1
    iget-boolean v0, p0, Lzy;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lzy;->h:Z

    .line 555
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    .line 554
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 555
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 553
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method
