.class public final Lccq;
.super Landroid/animation/AnimatorListenerAdapter;
.source "PG"


# instance fields
.field private synthetic a:Lcom/google/android/apps/moviemaker/ui/ThemeItemView;

.field private synthetic b:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;Lcom/google/android/apps/moviemaker/ui/ThemeItemView;)V
    .locals 0

    .prologue
    .line 212
    iput-object p1, p0, Lccq;->b:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    iput-object p2, p0, Lccq;->a:Lcom/google/android/apps/moviemaker/ui/ThemeItemView;

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 215
    iget-object v0, p0, Lccq;->a:Lcom/google/android/apps/moviemaker/ui/ThemeItemView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->setScaleX(F)V

    .line 216
    iget-object v0, p0, Lccq;->a:Lcom/google/android/apps/moviemaker/ui/ThemeItemView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->setScaleY(F)V

    .line 217
    iget-object v0, p0, Lccq;->a:Lcom/google/android/apps/moviemaker/ui/ThemeItemView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->setAlpha(F)V

    .line 218
    iget-object v0, p0, Lccq;->a:Lcom/google/android/apps/moviemaker/ui/ThemeItemView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->a(F)V

    .line 219
    iget-object v0, p0, Lccq;->b:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->f(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 220
    iget-object v0, p0, Lccq;->b:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->setAlpha(F)V

    .line 222
    :cond_0
    iget-object v0, p0, Lccq;->b:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->d(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;I)I

    .line 223
    return-void
.end method
