.class public final Lbgs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcgk;


# static fields
.field private static final a:Lbgt;


# instance fields
.field private final b:[Ljava/lang/Object;

.field private c:Lbgx;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lbgt;

    invoke-direct {v0}, Lbgt;-><init>()V

    sput-object v0, Lbgs;->a:Lbgt;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 329
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/16 v0, 0x40

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lbgs;->b:[Ljava/lang/Object;

    .line 331
    return-void
.end method

.method public static a(Lbgx;)Lbgs;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 45
    const-string v0, "shaderProgram"

    invoke-static {p0, v0, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 46
    sget-object v0, Lbgs;->a:Lbgt;

    invoke-virtual {v0}, Lbgt;->a()Lbgs;

    move-result-object v0

    .line 47
    iget-object v1, v0, Lbgs;->c:Lbgx;

    const-string v2, "ShaderInstance was re-initialized before being released"

    invoke-static {v1, v2, v3}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    iput-object p0, v0, Lbgs;->c:Lbgx;

    .line 48
    return-object v0
.end method

.method private a(Ljava/lang/String;FZ)Lbgs;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 271
    invoke-direct {p0}, Lbgs;->d()V

    .line 272
    invoke-direct {p0, p1, p3}, Lbgs;->a(Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    .line 273
    if-eqz v0, :cond_0

    .line 274
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v2, v2}, Lbgs;->a(III)V

    .line 275
    sget-object v1, Lbgs;->a:Lbgt;

    invoke-virtual {v1, v2}, Lbgt;->a(I)[F

    move-result-object v1

    .line 276
    const/4 v2, 0x0

    aput p2, v1, v2

    .line 277
    iget-object v2, p0, Lbgs;->b:[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput-object v1, v2, v0

    .line 279
    :cond_0
    return-object p0
.end method

.method private a(Ljava/lang/String;IZ)Lbgs;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 295
    invoke-direct {p0}, Lbgs;->d()V

    .line 296
    invoke-direct {p0, p1, p3}, Lbgs;->a(Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    .line 297
    if-eqz v0, :cond_0

    .line 298
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x2

    invoke-direct {p0, v1, v2, v3}, Lbgs;->a(III)V

    .line 299
    sget-object v1, Lbgs;->a:Lbgt;

    invoke-virtual {v1, v3}, Lbgt;->b(I)[I

    move-result-object v1

    .line 300
    const/4 v2, 0x0

    aput p2, v1, v2

    .line 301
    iget-object v2, p0, Lbgs;->b:[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput-object v1, v2, v0

    .line 303
    :cond_0
    return-object p0
.end method

.method private a(Ljava/lang/String;Landroid/graphics/Matrix;Z)Lbgs;
    .locals 11

    .prologue
    const/4 v10, 0x6

    const/4 v9, 0x5

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 233
    invoke-direct {p0}, Lbgs;->d()V

    .line 234
    invoke-direct {p0, p1, p3}, Lbgs;->a(Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    .line 235
    if-eqz v0, :cond_0

    .line 236
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v2, 0x9

    invoke-direct {p0, v1, v6, v2}, Lbgs;->a(III)V

    .line 237
    sget-object v1, Lbgs;->a:Lbgt;

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Lbgt;->a(I)[F

    move-result-object v1

    .line 238
    invoke-virtual {p2, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 243
    aget v2, v1, v6

    .line 244
    aget v3, v1, v7

    .line 245
    aget v4, v1, v9

    .line 246
    aget v5, v1, v8

    aput v5, v1, v6

    .line 247
    aget v5, v1, v10

    aput v5, v1, v7

    .line 248
    const/4 v5, 0x7

    aget v5, v1, v5

    aput v5, v1, v9

    .line 249
    aput v2, v1, v8

    .line 250
    aput v3, v1, v10

    .line 251
    const/4 v2, 0x7

    aput v4, v1, v2

    .line 253
    iget-object v2, p0, Lbgs;->b:[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput-object v1, v2, v0

    .line 255
    :cond_0
    return-object p0
.end method

.method private a(Ljava/lang/String;Z)Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 334
    iget-object v0, p0, Lbgs;->c:Lbgx;

    iget-object v0, v0, Lbgx;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 335
    if-nez p2, :cond_1

    if-nez v0, :cond_1

    .line 336
    const-string v1, "Unknown uniform with name: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 338
    :cond_1
    return-object v0
.end method

.method private a(III)V
    .locals 3

    .prologue
    .line 342
    iget-object v0, p0, Lbgs;->c:Lbgx;

    iget-object v0, v0, Lbgx;->a:[Lbgz;

    aget-object v0, v0, p1

    .line 343
    iget-object v1, p0, Lbgs;->b:[Ljava/lang/Object;

    aget-object v1, v1, p1

    if-eqz v1, :cond_0

    .line 344
    iget-object v0, v0, Lbgz;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x18

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Uniform \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\' already added"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 346
    :cond_0
    iget-object v1, v0, Lbgz;->a:Ljava/lang/String;

    iget v2, v0, Lbgz;->c:I

    iget v0, v0, Lbgz;->d:I

    invoke-static {v1, p2, p3, v2, v0}, Lbhc;->a(Ljava/lang/String;IIII)V

    .line 348
    return-void
.end method

.method private d()V
    .locals 3

    .prologue
    .line 351
    iget-object v0, p0, Lbgs;->c:Lbgx;

    const-string v1, "Attempting to use a ShaderProgram after it has been released"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 353
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;F)Lbgs;
    .locals 1

    .prologue
    .line 126
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lbgs;->a(Ljava/lang/String;FZ)Lbgs;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;I)Lbgs;
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lbgs;->a(Ljava/lang/String;IZ)Lbgs;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Landroid/graphics/Matrix;)Lbgs;
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lbgs;->a(Ljava/lang/String;Landroid/graphics/Matrix;Z)Lbgs;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lbgd;Z)Lbgs;
    .locals 4

    .prologue
    const/16 v3, 0x10

    .line 259
    invoke-direct {p0}, Lbgs;->d()V

    .line 260
    invoke-direct {p0, p1, p3}, Lbgs;->a(Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    .line 261
    if-eqz v0, :cond_0

    .line 262
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2, v3}, Lbgs;->a(III)V

    .line 263
    sget-object v1, Lbgs;->a:Lbgt;

    invoke-virtual {v1, v3}, Lbgt;->a(I)[F

    move-result-object v1

    .line 264
    invoke-virtual {p2, v1}, Lbgd;->a([F)V

    .line 265
    iget-object v2, p0, Lbgs;->b:[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput-object v1, v2, v0

    .line 267
    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/String;Lbhb;)Lbgs;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 202
    invoke-direct {p0}, Lbgs;->d()V

    .line 203
    invoke-direct {p0, p1, v3}, Lbgs;->a(Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    .line 204
    if-eqz v0, :cond_0

    .line 205
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x1

    invoke-direct {p0, v1, v3, v2}, Lbgs;->a(III)V

    .line 206
    iget-object v1, p0, Lbgs;->b:[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput-object p2, v1, v0

    .line 208
    :cond_0
    return-object p0
.end method

.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 60
    iget-object v0, p0, Lbgs;->c:Lbgx;

    if-nez v0, :cond_0

    .line 61
    const-string v0, "ShaderInstance released twice"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 64
    :cond_0
    iget-object v0, p0, Lbgs;->c:Lbgx;

    iget-object v2, v0, Lbgx;->a:[Lbgz;

    .line 65
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_1

    .line 66
    aget-object v0, v2, v1

    iget v0, v0, Lbgz;->c:I

    invoke-static {v0}, Lbhc;->c(I)I

    move-result v3

    .line 67
    iget-object v0, p0, Lbgs;->b:[Ljava/lang/Object;

    aget-object v0, v0, v1

    .line 68
    packed-switch v3, :pswitch_data_0

    .line 76
    :goto_1
    iget-object v0, p0, Lbgs;->b:[Ljava/lang/Object;

    aput-object v4, v0, v1

    .line 65
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 70
    :pswitch_0
    sget-object v3, Lbgs;->a:Lbgt;

    check-cast v0, [F

    invoke-virtual {v3, v0}, Lbgt;->a([F)V

    goto :goto_1

    .line 73
    :pswitch_1
    sget-object v3, Lbgs;->a:Lbgt;

    check-cast v0, [I

    invoke-virtual {v3, v0}, Lbgt;->a([I)V

    goto :goto_1

    .line 78
    :cond_1
    iput-object v4, p0, Lbgs;->c:Lbgx;

    .line 80
    sget-object v0, Lbgs;->a:Lbgt;

    invoke-virtual {v0, p0}, Lbgt;->a(Lbgs;)V

    .line 81
    return-void

    .line 68
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public b(Ljava/lang/String;F)Lbgs;
    .locals 1

    .prologue
    .line 136
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lbgs;->a(Ljava/lang/String;FZ)Lbgs;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;I)Lbgs;
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lbgs;->a(Ljava/lang/String;IZ)Lbgs;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;Landroid/graphics/Matrix;)Lbgs;
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lbgs;->a(Ljava/lang/String;Landroid/graphics/Matrix;Z)Lbgs;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;Lbhb;)Lbgs;
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 218
    invoke-direct {p0}, Lbgs;->d()V

    .line 219
    invoke-direct {p0, p1, v3}, Lbgs;->a(Ljava/lang/String;Z)Ljava/lang/Integer;

    move-result-object v0

    .line 220
    if-eqz v0, :cond_0

    .line 221
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2, v3}, Lbgs;->a(III)V

    .line 222
    iget-object v1, p0, Lbgs;->b:[Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput-object p2, v1, v0

    .line 224
    :cond_0
    return-object p0
.end method

.method public b()Lbgx;
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Lbgs;->d()V

    .line 88
    iget-object v0, p0, Lbgs;->c:Lbgx;

    return-object v0
.end method

.method public c()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Lbgs;->d()V

    .line 98
    iget-object v0, p0, Lbgs;->b:[Ljava/lang/Object;

    return-object v0
.end method

.method protected finalize()V
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Lbgs;->c:Lbgx;

    if-eqz v0, :cond_0

    .line 321
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "ShaderInstance was not released before being finalized"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 323
    :cond_0
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 324
    return-void
.end method
