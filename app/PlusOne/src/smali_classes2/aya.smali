.class public final Laya;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method private static a(Ljava/lang/StringBuilder;FFFLjava/lang/String;)V
    .locals 4

    .prologue
    .line 434
    cmpg-float v0, p1, p2

    if-ltz v0, :cond_0

    cmpg-float v0, p3, p1

    if-gez v0, :cond_1

    .line 435
    :cond_0
    const-string v0, "\'%s\' must be between %f and %f, but is %f"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p4, v1, v2

    const/4 v2, 0x1

    .line 436
    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    invoke-static {p1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    .line 435
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 438
    :cond_1
    return-void
.end method

.method private static a(Ljava/lang/StringBuilder;JLjava/lang/String;)V
    .locals 3

    .prologue
    .line 441
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 442
    const-string v1, "Negative "

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-static {p0, v0}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 444
    :cond_0
    return-void

    .line 442
    :cond_1
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 447
    if-nez p1, :cond_0

    .line 448
    const-string v0, "\'%s\' must not be null"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 450
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/StringBuilder;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 453
    invoke-virtual {p0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 454
    const-string v0, "\n"

    invoke-virtual {p0, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 455
    return-void
.end method

.method private static a(Ljava/lang/StringBuilder;Lonq;Looa;Lood;)V
    .locals 12

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x0

    .line 346
    if-nez p2, :cond_1

    .line 347
    if-eqz p1, :cond_0

    iget-object v0, p1, Lonq;->a:Lonp;

    if-eqz v0, :cond_0

    .line 349
    const-string v0, "soundtrack audio clip sequence present without soundtrackId"

    invoke-static {p0, v0}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 417
    :cond_0
    :goto_0
    return-void

    .line 355
    :cond_1
    iget-object v0, p1, Lonq;->a:Lonp;

    const-string v1, "beatMatchingStyling.soundtrackAudioClipSequence"

    invoke-static {p0, v0, v1}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    .line 357
    iget-object v0, p1, Lonq;->a:Lonp;

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p1, Lonq;->a:Lonp;

    iget-object v0, v0, Lonp;->a:[Lonr;

    const-string v1, "beatMatchingStyling.soundtrackAudioClipSequence.audioClip"

    invoke-static {p0, v0, v1}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    .line 363
    iget-object v0, p1, Lonq;->a:Lonp;

    iget-object v0, v0, Lonp;->a:[Lonr;

    if-eqz v0, :cond_0

    .line 367
    iget-object v0, p1, Lonq;->a:Lonp;

    iget-object v1, v0, Lonp;->a:[Lonr;

    array-length v5, v1

    move v0, v4

    :goto_1
    if-ge v0, v5, :cond_8

    aget-object v6, v1, v0

    .line 368
    if-nez v6, :cond_3

    .line 369
    const-string v6, "null clip in audioClips"

    invoke-static {p0, v6}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 367
    :cond_2
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 370
    :cond_3
    iget-object v7, v6, Lonr;->c:Lonw;

    if-nez v7, :cond_4

    .line 371
    const-string v6, "null mediaId in audioClips"

    invoke-static {p0, v6}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    goto :goto_2

    .line 372
    :cond_4
    iget-object v7, v6, Lonr;->c:Lonw;

    iget-object v7, v7, Lonw;->c:Looa;

    if-nez v7, :cond_5

    .line 373
    const-string v6, "null soundtrackId in audioClips"

    invoke-static {p0, v6}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    goto :goto_2

    .line 374
    :cond_5
    iget-object v7, v6, Lonr;->c:Lonw;

    iget-object v7, v7, Lonw;->c:Looa;

    iget-object v7, v7, Looa;->a:Ljava/lang/Long;

    if-nez v7, :cond_6

    .line 375
    const-string v6, "audio clip soundtrackId is null"

    invoke-static {p0, v6}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    goto :goto_2

    .line 376
    :cond_6
    iget-object v7, p3, Lood;->h:Looe;

    iget-object v7, v7, Looe;->c:Looa;

    iget-object v7, v7, Looa;->a:Ljava/lang/Long;

    if-nez v7, :cond_7

    .line 377
    const-string v6, "styling soundtrackId is null"

    invoke-static {p0, v6}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    goto :goto_2

    .line 378
    :cond_7
    iget-object v7, v6, Lonr;->c:Lonw;

    iget-object v7, v7, Lonw;->c:Looa;

    iget-object v7, v7, Looa;->a:Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iget-object v7, p3, Lood;->h:Looe;

    iget-object v7, v7, Looe;->c:Looa;

    iget-object v7, v7, Looa;->a:Ljava/lang/Long;

    .line 379
    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v7, v8, v10

    if-eqz v7, :cond_2

    .line 380
    iget-object v6, v6, Lonr;->c:Lonw;

    iget-object v6, v6, Lonw;->c:Looa;

    iget-object v6, v6, Looa;->a:Ljava/lang/Long;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p3, Lood;->h:Looe;

    iget-object v7, v7, Looe;->c:Looa;

    iget-object v7, v7, Looa;->a:Ljava/lang/Long;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/lit8 v9, v9, 0x2c

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/String;->length()I

    move-result v10

    add-int/2addr v9, v10

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v9, "mismatched soundtrackId in audioClips: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, " vs. "

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {p0, v6}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 386
    :cond_8
    iget-object v0, p1, Lonq;->a:Lonp;

    if-eqz v0, :cond_0

    .line 388
    iget-object v0, p1, Lonq;->a:Lonp;

    iget-object v6, v0, Lonp;->a:[Lonr;

    array-length v7, v6

    move v5, v4

    move-wide v0, v2

    :goto_3
    if-ge v5, v7, :cond_b

    aget-object v8, v6, v5

    .line 389
    iget v9, v8, Lonr;->b:I

    const/4 v10, 0x6

    if-eq v9, v10, :cond_9

    .line 390
    iget v9, v8, Lonr;->b:I

    new-instance v10, Ljava/lang/StringBuilder;

    const/16 v11, 0x30

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v11, "Unexpected clip type for soundtrack: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {p0, v9}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 392
    :cond_9
    invoke-static {p0, v8, v4}, Laya;->a(Ljava/lang/StringBuilder;Lonr;Z)V

    .line 393
    iget-object v9, v8, Lonr;->d:Lons;

    if-eqz v9, :cond_a

    .line 394
    iget-object v9, v8, Lonr;->d:Lons;

    iget-object v9, v9, Lons;->c:Ljava/lang/Long;

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    iget-object v8, v8, Lonr;->d:Lons;

    iget-object v8, v8, Lons;->b:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long v8, v10, v8

    add-long/2addr v0, v8

    .line 388
    :cond_a
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 398
    :cond_b
    iget-object v6, p3, Lood;->b:[Lonr;

    .line 400
    array-length v7, v6

    move v5, v4

    :goto_4
    if-ge v5, v7, :cond_d

    aget-object v8, v6, v5

    .line 401
    iget-object v9, v8, Lonr;->d:Lons;

    if-eqz v9, :cond_c

    .line 402
    iget-object v9, v8, Lonr;->d:Lons;

    iget-object v9, v9, Lons;->c:Ljava/lang/Long;

    .line 403
    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    iget-object v8, v8, Lonr;->d:Lons;

    iget-object v8, v8, Lons;->b:Ljava/lang/Long;

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long v8, v10, v8

    add-long/2addr v2, v8

    .line 400
    :cond_c
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    .line 407
    :cond_d
    iget-object v5, p3, Lood;->c:[Ljava/lang/Long;

    array-length v8, v5

    move-wide v6, v2

    move v2, v4

    :goto_5
    if-ge v2, v8, :cond_e

    aget-object v3, v5, v2

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 408
    sub-long/2addr v6, v10

    .line 407
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 411
    :cond_e
    cmp-long v2, v0, v6

    if-lez v2, :cond_0

    .line 412
    const-string v2, "Soundtrack too long: %s us for %s us of clips"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    .line 414
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    aput-object v1, v3, v0

    .line 412
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private static a(Ljava/lang/StringBuilder;Lonr;Z)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 137
    iget-object v0, p1, Lonr;->c:Lonw;

    iget v1, p1, Lonr;->b:I

    packed-switch v1, :pswitch_data_0

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0x1e

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Invalid clip type: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 138
    :goto_0
    :pswitch_0
    iget-object v0, p1, Lonr;->d:Lons;

    const-string v1, "clip.interval"

    invoke-static {p0, v0, v1}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    .line 139
    iget-object v0, p1, Lonr;->d:Lons;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p1, Lonr;->d:Lons;

    invoke-static {p0, v0}, Laya;->a(Ljava/lang/StringBuilder;Lons;)V

    .line 142
    :cond_0
    if-eqz p2, :cond_1

    iget v0, p1, Lonr;->b:I

    if-ne v0, v3, :cond_1

    .line 143
    iget-object v0, p1, Lonr;->e:Lont;

    const-string v1, "clip.metadata"

    invoke-static {p0, v0, v1}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    .line 145
    :cond_1
    iget-object v0, p1, Lonr;->e:Lont;

    if-eqz v0, :cond_3

    .line 146
    iget v0, p1, Lonr;->b:I

    if-ne v0, v3, :cond_2

    .line 147
    iget-object v0, p1, Lonr;->e:Lont;

    iget-object v0, v0, Lont;->b:Ljava/lang/Long;

    const-string v1, "clip.metadata.clip_duration_us"

    invoke-static {p0, v0, v1}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    .line 150
    :cond_2
    iget-object v0, p1, Lonr;->e:Lont;

    invoke-static {p0, v0}, Laya;->a(Ljava/lang/StringBuilder;Lont;)V

    .line 152
    :cond_3
    return-void

    .line 137
    :pswitch_1
    const-string v1, "media_id"

    invoke-static {p0, v0, v1}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_4

    iget-object v1, v0, Lonw;->c:Looa;

    const-string v2, "media_id.soundtrack_id"

    invoke-static {p0, v1, v2}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    :cond_4
    :goto_1
    invoke-static {p0, v0}, Laya;->a(Ljava/lang/StringBuilder;Lonw;)V

    goto :goto_0

    :pswitch_2
    const-string v1, "media_id"

    invoke-static {p0, v0, v1}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_4

    iget-object v1, v0, Lonw;->a:Lonz;

    const-string v2, "media_id.photo_id"

    invoke-static {p0, v1, v2}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_3
    const-string v1, "media_id"

    invoke-static {p0, v0, v1}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_4

    iget-object v1, v0, Lonw;->b:Lony;

    const-string v2, "media_id.burst_ids"

    invoke-static {p0, v1, v2}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_4
    const-string v1, "media_id"

    invoke-static {p0, v0, v1}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    if-eqz v0, :cond_4

    iget-object v1, v0, Lonw;->c:Looa;

    const-string v2, "media_id.soundtrack_id"

    invoke-static {p0, v1, v2}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Ljava/lang/StringBuilder;Lons;)V
    .locals 4

    .prologue
    .line 251
    iget-object v0, p1, Lons;->b:Ljava/lang/Long;

    const-string v1, "interval.start_point_us"

    invoke-static {p0, v0, v1}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    .line 252
    iget-object v0, p1, Lons;->c:Ljava/lang/Long;

    const-string v1, "interval.end_point_us"

    invoke-static {p0, v0, v1}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    .line 253
    iget-object v0, p1, Lons;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lons;->c:Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lons;->c:Ljava/lang/Long;

    .line 255
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iget-object v2, p1, Lons;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 256
    const-string v0, "interval invalid: (%s, %s)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p1, Lons;->b:Ljava/lang/Long;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p1, Lons;->c:Ljava/lang/Long;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 260
    :cond_0
    return-void
.end method

.method private static a(Ljava/lang/StringBuilder;Lons;J)V
    .locals 4

    .prologue
    .line 267
    iget-object v0, p1, Lons;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lons;->c:Ljava/lang/Long;

    if-nez v0, :cond_1

    .line 278
    :cond_0
    :goto_0
    return-void

    .line 271
    :cond_1
    iget-object v0, p1, Lons;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_2

    .line 272
    iget-object v0, p1, Lons;->b:Ljava/lang/Long;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x1d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Interval starts before zero: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 274
    :cond_2
    iget-object v0, p1, Lons;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    cmp-long v0, v0, p2

    if-lez v0, :cond_0

    .line 275
    const-string v0, "Interval extends past end of video: %s vs %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p1, Lons;->c:Ljava/lang/Long;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    .line 276
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 275
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/StringBuilder;Lont;)V
    .locals 8

    .prologue
    const/high16 v7, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 155
    iget-object v0, p1, Lont;->a:Lonu;

    if-eqz v0, :cond_4

    .line 156
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p1, Lont;->a:Lonu;

    iget-object v1, v1, Lonu;->a:[Loog;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 157
    iget-object v1, p1, Lont;->a:Lonu;

    iget-object v1, v1, Lonu;->a:[Loog;

    aget-object v5, v1, v0

    .line 158
    iget-object v1, v5, Loog;->c:Ljava/lang/Float;

    if-nez v1, :cond_0

    iget-object v1, v5, Loog;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 159
    :goto_1
    iget-object v3, v5, Loog;->d:Ljava/lang/Float;

    if-nez v3, :cond_1

    iget-object v3, v5, Loog;->d:Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 160
    :goto_2
    iget-object v4, v5, Loog;->e:Ljava/lang/Float;

    if-nez v4, :cond_2

    iget-object v4, v5, Loog;->e:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 161
    :goto_3
    iget-object v6, v5, Loog;->f:Ljava/lang/Float;

    if-nez v6, :cond_3

    iget-object v5, v5, Loog;->f:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    .line 162
    :goto_4
    const-string v6, "croppingParam[].left"

    invoke-static {p0, v1, v2, v7, v6}, Laya;->a(Ljava/lang/StringBuilder;FFFLjava/lang/String;)V

    .line 163
    const-string v6, "croppingParam[].right"

    invoke-static {p0, v3, v2, v7, v6}, Laya;->a(Ljava/lang/StringBuilder;FFFLjava/lang/String;)V

    .line 164
    const-string v6, "croppingParam[].top"

    invoke-static {p0, v4, v2, v7, v6}, Laya;->a(Ljava/lang/StringBuilder;FFFLjava/lang/String;)V

    .line 165
    const-string v6, "croppingParam[].bottom"

    invoke-static {p0, v5, v2, v7, v6}, Laya;->a(Ljava/lang/StringBuilder;FFFLjava/lang/String;)V

    .line 166
    add-float/2addr v1, v3

    const-string v3, "croppingParam[].left + croppingParam[].right"

    invoke-static {p0, v1, v2, v7, v3}, Laya;->a(Ljava/lang/StringBuilder;FFFLjava/lang/String;)V

    .line 168
    add-float v1, v4, v5

    const-string v3, "croppingParam[].top + croppingParam[].bottom"

    invoke-static {p0, v1, v2, v7, v3}, Laya;->a(Ljava/lang/StringBuilder;FFFLjava/lang/String;)V

    .line 156
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v1, v2

    .line 158
    goto :goto_1

    :cond_1
    move v3, v2

    .line 159
    goto :goto_2

    :cond_2
    move v4, v2

    .line 160
    goto :goto_3

    :cond_3
    move v5, v2

    .line 161
    goto :goto_4

    .line 172
    :cond_4
    return-void
.end method

.method private static a(Ljava/lang/StringBuilder;Lonw;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 215
    .line 216
    iget-object v0, p1, Lonw;->a:Lonz;

    if-eqz v0, :cond_6

    .line 218
    iget-object v0, p1, Lonw;->a:Lonz;

    iget-object v0, v0, Lonz;->b:Ljava/lang/String;

    const-string v3, "media_id.photo_id.obfuscated_user_id"

    invoke-static {p0, v0, v3}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    .line 220
    iget-object v0, p1, Lonw;->a:Lonz;

    iget-object v0, v0, Lonz;->c:Ljava/lang/Long;

    const-string v3, "media_id.photo_id.photo_id"

    invoke-static {p0, v0, v3}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    move v0, v1

    .line 222
    :goto_0
    iget-object v3, p1, Lonw;->b:Lony;

    if-eqz v3, :cond_1

    .line 223
    add-int/lit8 v0, v0, 0x1

    .line 224
    iget-object v3, p1, Lonw;->b:Lony;

    iget-object v3, v3, Lony;->a:[Lonz;

    array-length v3, v3

    if-nez v3, :cond_0

    .line 225
    const-string v3, "burst_ids.photo_id is empty"

    invoke-static {p0, v3}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 227
    :cond_0
    iget-object v3, p1, Lonw;->b:Lony;

    iget-object v3, v3, Lony;->a:[Lonz;

    array-length v4, v3

    :goto_1
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 228
    iget-object v6, v5, Lonz;->b:Ljava/lang/String;

    const-string v7, "media_id.burst_ids.photo_id.obfuscated_user_id"

    invoke-static {p0, v6, v7}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    .line 231
    iget-object v5, v5, Lonz;->c:Ljava/lang/Long;

    const-string v6, "media_id.burst_ids.photo_id.photo_id"

    invoke-static {p0, v5, v6}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    .line 227
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 234
    :cond_1
    iget-object v2, p1, Lonw;->c:Looa;

    if-eqz v2, :cond_2

    .line 235
    add-int/lit8 v0, v0, 0x1

    .line 236
    iget-object v2, p1, Lonw;->c:Looa;

    iget-object v2, v2, Looa;->a:Ljava/lang/Long;

    const-string v3, "media_id.soundtrack_id.soundtrack_id"

    invoke-static {p0, v2, v3}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    .line 239
    :cond_2
    iget-object v2, p1, Lonw;->d:Lonx;

    if-eqz v2, :cond_3

    .line 240
    add-int/lit8 v0, v0, 0x1

    .line 241
    iget-object v2, p1, Lonw;->d:Lonx;

    iget-object v2, v2, Lonx;->a:Ljava/lang/Long;

    const-string v3, "media_id.asset_id.asset_id"

    invoke-static {p0, v2, v3}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    .line 243
    :cond_3
    if-nez v0, :cond_5

    .line 244
    const-string v0, "No ID specified in MediaId proto"

    invoke-static {p0, v0}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 248
    :cond_4
    :goto_2
    return-void

    .line 245
    :cond_5
    if-le v0, v1, :cond_4

    .line 246
    const-string v0, "More than one ID specified in MediaId proto"

    invoke-static {p0, v0}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    goto :goto_2

    :cond_6
    move v0, v2

    goto :goto_0
.end method

.method public static a(Ljava/lang/StringBuilder;Lood;Z)V
    .locals 9

    .prologue
    const/4 v6, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 65
    iget-object v0, p1, Lood;->b:[Lonr;

    array-length v0, v0

    iget-object v2, p1, Lood;->c:[Ljava/lang/Long;

    array-length v2, v2

    add-int/lit8 v2, v2, 0x1

    if-eq v0, v2, :cond_1

    iget-object v0, p1, Lood;->b:[Lonr;

    array-length v0, v0

    if-nez v0, :cond_0

    iget-object v0, p1, Lood;->c:[Ljava/lang/Long;

    array-length v0, v0

    if-eqz v0, :cond_1

    .line 68
    :cond_0
    const-string v0, "Invalid lengths for displayableClip and clipOverlapDurationUs: %s, %s"

    new-array v2, v6, [Ljava/lang/Object;

    iget-object v3, p1, Lood;->b:[Lonr;

    array-length v3, v3

    .line 70
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v1

    iget-object v3, p1, Lood;->c:[Ljava/lang/Long;

    array-length v3, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v8

    .line 68
    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 72
    :cond_1
    iget-object v0, p1, Lood;->b:[Lonr;

    iget-object v2, p1, Lood;->d:[Ljava/lang/Boolean;

    const-string v3, "displayableClip"

    const-string v4, "clipWasAddedByTheme"

    invoke-static {p0, v0, v2, v3, v4}, Laya;->a(Ljava/lang/StringBuilder;[Ljava/lang/Object;[Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    iget-object v2, p1, Lood;->d:[Ljava/lang/Boolean;

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_3

    aget-object v4, v2, v0

    .line 76
    if-nez v4, :cond_2

    .line 77
    const-string v4, "clipWasAddedByTheme must be set"

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v4}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 75
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_3
    iget-object v3, p1, Lood;->d:[Ljava/lang/Boolean;

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_1
    if-ge v2, v4, :cond_5

    aget-object v5, v3, v2

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_4

    add-int/lit8 v0, v0, 0x1

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 84
    :cond_5
    iget-object v2, p1, Lood;->e:[Lons;

    array-length v2, v2

    if-eq v2, v0, :cond_6

    .line 85
    const-string v2, "Invalid length for clip_original_interval_us: %s. %s expected."

    new-array v3, v6, [Ljava/lang/Object;

    iget-object v4, p1, Lood;->e:[Lons;

    array-length v4, v4

    .line 87
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v8

    .line 85
    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 90
    :cond_6
    iget-object v2, p1, Lood;->b:[Lonr;

    array-length v3, v2

    move v0, v1

    :goto_2
    if-ge v0, v3, :cond_9

    aget-object v4, v2, v0

    .line 91
    invoke-static {p0, v4, p2}, Laya;->a(Ljava/lang/StringBuilder;Lonr;Z)V

    .line 92
    iget v5, v4, Lonr;->b:I

    const/4 v6, 0x3

    if-eq v5, v6, :cond_7

    iget v5, v4, Lonr;->b:I

    if-eq v5, v8, :cond_7

    iget v5, v4, Lonr;->b:I

    const/4 v6, 0x4

    if-eq v5, v6, :cond_7

    iget v5, v4, Lonr;->b:I

    const/4 v6, 0x7

    if-eq v5, v6, :cond_7

    iget v5, v4, Lonr;->b:I

    new-instance v6, Ljava/lang/StringBuilder;

    const/16 v7, 0x2e

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Unsupported displayable clip type: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p0, v5}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 93
    :cond_7
    iget v5, v4, Lonr;->b:I

    if-ne v5, v8, :cond_8

    iget-object v5, v4, Lonr;->e:Lont;

    if-eqz v5, :cond_8

    .line 94
    iget-object v5, v4, Lonr;->d:Lons;

    iget-object v4, v4, Lonr;->e:Lont;

    iget-object v4, v4, Lont;->b:Ljava/lang/Long;

    .line 95
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 94
    invoke-static {p0, v5, v6, v7}, Laya;->a(Ljava/lang/StringBuilder;Lons;J)V

    .line 90
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 100
    :cond_9
    iget-object v4, p1, Lood;->e:[Lons;

    array-length v5, v4

    move v2, v1

    move v0, v1

    :goto_3
    if-ge v2, v5, :cond_c

    aget-object v3, v4, v2

    .line 101
    :goto_4
    iget-object v6, p1, Lood;->d:[Ljava/lang/Boolean;

    aget-object v6, v6, v0

    invoke-virtual {v6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_a

    .line 102
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 104
    :cond_a
    invoke-static {p0, v3}, Laya;->a(Ljava/lang/StringBuilder;Lons;)V

    .line 105
    iget-object v6, p1, Lood;->b:[Lonr;

    aget-object v6, v6, v0

    .line 106
    iget v7, v6, Lonr;->b:I

    if-ne v7, v8, :cond_b

    iget-object v7, v6, Lonr;->e:Lont;

    if-eqz v7, :cond_b

    .line 107
    iget-object v6, v6, Lonr;->e:Lont;

    iget-object v6, v6, Lont;->b:Ljava/lang/Long;

    .line 108
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 107
    invoke-static {p0, v3, v6, v7}, Laya;->a(Ljava/lang/StringBuilder;Lons;J)V

    .line 110
    :cond_b
    add-int/lit8 v3, v0, 0x1

    .line 100
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto :goto_3

    .line 113
    :cond_c
    iget-object v2, p1, Lood;->c:[Ljava/lang/Long;

    array-length v3, v2

    move v0, v1

    :goto_5
    if-ge v0, v3, :cond_f

    aget-object v1, v2, v0

    .line 114
    if-nez v1, :cond_e

    .line 115
    const-string v1, "Null entry in clipOverlapDurationUs"

    invoke-static {p0, v1}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 113
    :cond_d
    :goto_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 116
    :cond_e
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-gez v4, :cond_d

    .line 117
    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x29

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Negative entry in clipOverlapDurationUs: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    goto :goto_6

    .line 121
    :cond_f
    iget-object v0, p1, Lood;->h:Looe;

    const-string v1, "storyboard.styling"

    invoke-static {p0, v0, v1}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    .line 122
    iget-object v0, p1, Lood;->h:Looe;

    if-eqz v0, :cond_12

    .line 123
    iget-object v0, p1, Lood;->h:Looe;

    iget v1, v0, Looe;->a:I

    if-nez v1, :cond_10

    const-string v1, "styling.theme=UNKOWN"

    invoke-static {p0, v1}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    :cond_10
    iget-object v1, v0, Looe;->b:Looo;

    const-string v2, "storyboard.styling.theme_styling"

    invoke-static {p0, v1, v2}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v0, Looe;->d:Lonq;

    const-string v2, "storyboard.styling.beat_matching_styling"

    invoke-static {p0, v1, v2}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v1, v0, Looe;->b:Looo;

    if-eqz v1, :cond_11

    iget-object v1, v0, Looe;->b:Looo;

    iget-object v2, p1, Lood;->b:[Lonr;

    invoke-static {p0, v1, v2}, Laya;->a(Ljava/lang/StringBuilder;Looo;[Lonr;)V

    :cond_11
    iget-object v1, v0, Looe;->d:Lonq;

    if-eqz v1, :cond_12

    iget-object v1, v0, Looe;->d:Lonq;

    iget-object v0, v0, Looe;->c:Looa;

    invoke-static {p0, v1, v0, p1}, Laya;->a(Ljava/lang/StringBuilder;Lonq;Looa;Lood;)V

    .line 126
    :cond_12
    iget-object v0, p1, Lood;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_13

    iget-object v0, p1, Lood;->i:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gtz v0, :cond_13

    .line 127
    iget-object v0, p1, Lood;->i:Ljava/lang/Integer;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x16

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "invalid output width: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 130
    :cond_13
    iget-object v0, p1, Lood;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_14

    iget-object v0, p1, Lood;->j:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gtz v0, :cond_14

    .line 131
    iget-object v0, p1, Lood;->j:Ljava/lang/Integer;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x17

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "invalid output height: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 133
    :cond_14
    return-void
.end method

.method private static a(Ljava/lang/StringBuilder;Loom;)V
    .locals 3

    .prologue
    .line 329
    iget-object v0, p1, Loom;->b:Ljava/lang/Long;

    const-string v1, "max_overlap_with_????.with_video_us"

    invoke-static {p0, v0, v1}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    .line 330
    iget-object v0, p1, Loom;->a:Ljava/lang/Long;

    const-string v1, "max_overlap_with_????.with_photo_us"

    invoke-static {p0, v0, v1}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    .line 331
    iget-object v0, p1, Loom;->b:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p1, Loom;->b:Ljava/lang/Long;

    .line 333
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-string v2, "max_overlap_with_????.with_video_us"

    .line 332
    invoke-static {p0, v0, v1, v2}, Laya;->a(Ljava/lang/StringBuilder;JLjava/lang/String;)V

    .line 335
    :cond_0
    iget-object v0, p1, Loom;->a:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 336
    iget-object v0, p1, Loom;->a:Ljava/lang/Long;

    .line 337
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-string v2, "max_overlap_with_????.with_photo_us"

    .line 336
    invoke-static {p0, v0, v1, v2}, Laya;->a(Ljava/lang/StringBuilder;JLjava/lang/String;)V

    .line 339
    :cond_1
    return-void
.end method

.method private static a(Ljava/lang/StringBuilder;Looo;[Lonr;)V
    .locals 6

    .prologue
    .line 308
    iget-object v0, p1, Looo;->b:[Look;

    const-string v1, "theme_styling.displayable_clip_param"

    const-string v2, "displayable_clip"

    invoke-static {p0, v0, p2, v1, v2}, Laya;->a(Ljava/lang/StringBuilder;[Ljava/lang/Object;[Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    .line 311
    iget-object v1, p1, Looo;->b:[Look;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 312
    iget-object v4, v3, Look;->c:Loom;

    const-string v5, "max_overlap_with_next"

    invoke-static {p0, v4, v5}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, v3, Look;->d:Loom;

    const-string v5, "max_overlap_with_prev"

    invoke-static {p0, v4, v5}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/Object;Ljava/lang/String;)V

    iget-object v4, v3, Look;->c:Loom;

    if-eqz v4, :cond_0

    iget-object v4, v3, Look;->c:Loom;

    invoke-static {p0, v4}, Laya;->a(Ljava/lang/StringBuilder;Loom;)V

    :cond_0
    iget-object v4, v3, Look;->d:Loom;

    if-eqz v4, :cond_1

    iget-object v3, v3, Look;->d:Loom;

    invoke-static {p0, v3}, Laya;->a(Ljava/lang/StringBuilder;Loom;)V

    .line 311
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 314
    :cond_2
    return-void
.end method

.method private static a(Ljava/lang/StringBuilder;[Ljava/lang/Object;[Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 425
    array-length v0, p1

    array-length v1, p2

    if-eq v0, v1, :cond_0

    .line 426
    const-string v0, "Invalid lengths for %s and %s: %s, %s"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p3, v1, v2

    const/4 v2, 0x1

    aput-object p4, v1, v2

    const/4 v2, 0x2

    array-length v3, p1

    .line 428
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    array-length v3, p2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 426
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Laya;->a(Ljava/lang/StringBuilder;Ljava/lang/String;)V

    .line 430
    :cond_0
    return-void
.end method
