.class public final Lbot;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lbot;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Lbmd;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    new-instance v0, Lbou;

    invoke-direct {v0}, Lbou;-><init>()V

    sput-object v0, Lbot;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lbmd;

    iput-object v0, p0, Lbot;->a:Lbmd;

    .line 31
    return-void
.end method

.method public constructor <init>(Lbmd;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lbot;->a:Lbmd;

    .line 27
    return-void
.end method

.method public static a(Lbub;)Lbot;
    .locals 3

    .prologue
    .line 35
    iget-object v0, p0, Lbub;->b:Lbty;

    const-string v1, "proto.clip"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 36
    new-instance v0, Lbot;

    iget-object v1, p0, Lbub;->b:Lbty;

    invoke-static {v1}, Lbpm;->a(Lbty;)Lbmd;

    move-result-object v1

    invoke-direct {v0, v1}, Lbot;-><init>(Lbmd;)V

    return-object v0
.end method


# virtual methods
.method public a()Lbub;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Lbub;

    invoke-direct {v0}, Lbub;-><init>()V

    .line 42
    iget-object v1, p0, Lbot;->a:Lbmd;

    invoke-static {v1}, Lbpm;->a(Lbmd;)Lbty;

    move-result-object v1

    iput-object v1, v0, Lbub;->b:Lbty;

    .line 43
    return-object v0
.end method

.method public a(Lbmd;)Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 47
    iget-object v2, p0, Lbot;->a:Lbmd;

    iget-object v3, v2, Lbmd;->e:Ljeg;

    iget-object v4, p1, Lbmd;->e:Ljeg;

    invoke-static {v3, v4}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v2, v2, Lbmd;->f:Lbmp;

    iget-object v3, p1, Lbmd;->f:Lbmp;

    iget-wide v4, v2, Lbmp;->c:J

    iget-wide v6, v3, Lbmp;->b:J

    cmp-long v4, v4, v6

    if-ltz v4, :cond_0

    iget-wide v4, v2, Lbmp;->b:J

    iget-wide v2, v3, Lbmp;->c:J

    cmp-long v2, v4, v2

    if-gtz v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    :goto_1
    return v0

    :cond_0
    move v2, v1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 52
    instance-of v0, p1, Lbot;

    if-eqz v0, :cond_0

    .line 53
    check-cast p1, Lbot;

    iget-object v0, p1, Lbot;->a:Lbmd;

    iget-object v1, p0, Lbot;->a:Lbmd;

    invoke-virtual {v0, v1}, Lbmd;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 55
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lbot;->a:Lbmd;

    const/16 v1, 0x11

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lbot;->a:Lbmd;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xb

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Exclusion("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lbot;->a:Lbmd;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 76
    return-void
.end method
