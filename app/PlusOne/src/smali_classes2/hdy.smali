.class public final Lhdy;
.super Landroid/view/View;
.source "PG"


# instance fields
.field private a:F

.field private b:Landroid/graphics/Bitmap;

.field private c:Landroid/graphics/Paint;

.field private d:Landroid/graphics/Paint;

.field private e:Lhci;

.field private f:I

.field private g:I

.field private final h:Landroid/graphics/Matrix;

.field private final i:Landroid/graphics/RectF;

.field private j:F

.field private k:I

.field private final l:Landroid/graphics/Matrix;

.field private m:Z

.field private final n:Landroid/graphics/Matrix;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v0, 0x3

    const/4 v2, 0x1

    .line 62
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 48
    iput v0, p0, Lhdy;->f:I

    .line 49
    iput v0, p0, Lhdy;->g:I

    .line 51
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lhdy;->h:Landroid/graphics/Matrix;

    .line 52
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lhdy;->i:Landroid/graphics/RectF;

    .line 55
    iput v2, p0, Lhdy;->k:I

    .line 57
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lhdy;->l:Landroid/graphics/Matrix;

    .line 293
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lhdy;->n:Landroid/graphics/Matrix;

    .line 64
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lhdy;->setBackgroundColor(I)V

    new-instance v0, Lhci;

    invoke-virtual {p0}, Lhdy;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lhci;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lhdy;->e:Lhci;

    iget-object v0, p0, Lhdy;->e:Lhci;

    invoke-virtual {v0, v2}, Lhci;->a(Z)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lhdy;->c:Landroid/graphics/Paint;

    iget-object v0, p0, Lhdy;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lhdy;->c:Landroid/graphics/Paint;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    new-instance v0, Landroid/graphics/Paint;

    iget-object v1, p0, Lhdy;->c:Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    iput-object v0, p0, Lhdy;->d:Landroid/graphics/Paint;

    iget-object v0, p0, Lhdy;->d:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffColorFilter;

    const/high16 v2, 0x7f000000

    sget-object v3, Landroid/graphics/PorterDuff$Mode;->SRC_OVER:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2, v3}, Landroid/graphics/PorterDuffColorFilter;-><init>(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 65
    return-void
.end method

.method static synthetic a(Lhdy;F)F
    .locals 0

    .prologue
    .line 28
    iput p1, p0, Lhdy;->j:F

    return p1
.end method

.method static synthetic a(Lhdy;Z)Z
    .locals 0

    .prologue
    .line 28
    iput-boolean p1, p0, Lhdy;->m:Z

    return p1
.end method

.method private b(II)V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/high16 v8, 0x40000000    # 2.0f

    const/high16 v0, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    const/high16 v1, -0x40800000    # -1.0f

    .line 337
    iget-object v2, p0, Lhdy;->b:Landroid/graphics/Bitmap;

    if-nez v2, :cond_1

    .line 367
    :cond_0
    :goto_0
    return-void

    .line 341
    :cond_1
    iget-object v2, p0, Lhdy;->e:Lhci;

    invoke-virtual {v2}, Lhci;->a()I

    move-result v2

    .line 342
    invoke-virtual {p0}, Lhdy;->getPaddingLeft()I

    move-result v3

    sub-int v3, p1, v3

    invoke-virtual {p0}, Lhdy;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    sub-int/2addr v3, v2

    .line 343
    invoke-virtual {p0}, Lhdy;->getPaddingTop()I

    move-result v4

    sub-int v4, p2, v4

    invoke-virtual {p0}, Lhdy;->getPaddingBottom()I

    move-result v5

    sub-int/2addr v4, v5

    sub-int v2, v4, v2

    .line 345
    if-lez v3, :cond_2

    if-gtz v2, :cond_3

    .line 346
    :cond_2
    iget-object v0, p0, Lhdy;->i:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->setEmpty()V

    .line 347
    iget-object v0, p0, Lhdy;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    goto :goto_0

    .line 351
    :cond_3
    iget-object v4, p0, Lhdy;->b:Landroid/graphics/Bitmap;

    .line 352
    invoke-virtual {v4}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    iget-object v5, p0, Lhdy;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    invoke-static {v4, v5, v3, v2}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->a(IIII)Landroid/graphics/Point;

    move-result-object v4

    .line 353
    iget-object v5, p0, Lhdy;->b:Landroid/graphics/Bitmap;

    .line 354
    invoke-virtual {v5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    iget-object v6, p0, Lhdy;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-static {v5, v6, v2, v3}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->a(IIII)Landroid/graphics/Point;

    move-result-object v2

    .line 355
    iget v3, v4, Landroid/graphics/Point;->x:I

    iget v5, v2, Landroid/graphics/Point;->x:I

    invoke-static {v3, v5}, Ljava/lang/Math;->min(II)I

    move-result v3

    iput v3, v4, Landroid/graphics/Point;->x:I

    .line 356
    iget v3, v4, Landroid/graphics/Point;->y:I

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, v4, Landroid/graphics/Point;->y:I

    .line 358
    iget-object v2, p0, Lhdy;->i:Landroid/graphics/RectF;

    iget v3, v4, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    iget v5, v4, Landroid/graphics/Point;->y:I

    int-to-float v5, v5

    invoke-virtual {v2, v7, v7, v3, v5}, Landroid/graphics/RectF;->set(FFFF)V

    .line 359
    iget-object v2, p0, Lhdy;->i:Landroid/graphics/RectF;

    iget v3, v4, Landroid/graphics/Point;->x:I

    sub-int v3, p1, v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v5, v4, Landroid/graphics/Point;->y:I

    sub-int v5, p2, v5

    div-int/lit8 v5, v5, 0x2

    int-to-float v5, v5

    invoke-virtual {v2, v3, v5}, Landroid/graphics/RectF;->offsetTo(FF)V

    .line 362
    iget v2, v4, Landroid/graphics/Point;->x:I

    int-to-float v2, v2

    iget-object v3, p0, Lhdy;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    .line 363
    iget-object v3, p0, Lhdy;->h:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 364
    iget-object v2, p0, Lhdy;->h:Landroid/graphics/Matrix;

    iget-object v3, p0, Lhdy;->i:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lhdy;->i:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 366
    iget v2, p0, Lhdy;->k:I

    iget v3, p0, Lhdy;->k:I

    iget-object v2, p0, Lhdy;->b:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lhdy;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    div-float v4, v2, v8

    iget-object v2, p0, Lhdy;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float v5, v2, v8

    iget-object v6, p0, Lhdy;->l:Landroid/graphics/Matrix;

    const/4 v2, 0x2

    if-eq v3, v2, :cond_4

    if-ne v3, v9, :cond_7

    :cond_4
    move v2, v1

    :goto_1
    const/4 v7, 0x3

    if-eq v3, v7, :cond_5

    if-ne v3, v9, :cond_6

    :cond_5
    move v0, v1

    :cond_6
    invoke-virtual {v6, v2, v0, v4, v5}, Landroid/graphics/Matrix;->setScale(FFFF)V

    goto/16 :goto_0

    :cond_7
    move v2, v0

    goto :goto_1
.end method

.method private static c(F)F
    .locals 2

    .prologue
    const/high16 v1, 0x43b40000    # 360.0f

    .line 404
    const/4 v0, 0x0

    cmpg-float v0, p0, v0

    if-gez v0, :cond_0

    .line 405
    div-float v0, p0, v1

    float-to-int v0, v0

    add-int/lit8 v0, v0, 0x1

    mul-int/lit16 v0, v0, 0x168

    int-to-float v0, v0

    add-float/2addr v0, p0

    .line 407
    :goto_0
    return v0

    :cond_0
    rem-float v0, p0, v1

    goto :goto_0
.end method

.method private getIntermediateRotation()F
    .locals 1
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 222
    iget v0, p0, Lhdy;->j:F

    return v0
.end method

.method private setIntermediateRotation(F)V
    .locals 0
    .annotation build Lcom/google/android/apps/common/proguard/UsedByNative;
    .end annotation

    .prologue
    .line 212
    iput p1, p0, Lhdy;->j:F

    .line 213
    invoke-virtual {p0}, Lhdy;->invalidate()V

    .line 214
    return-void
.end method


# virtual methods
.method public a()F
    .locals 1

    .prologue
    .line 192
    iget v0, p0, Lhdy;->j:F

    return v0
.end method

.method protected a(FF)F
    .locals 6

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 328
    cmpg-float v0, p1, v1

    if-gez v0, :cond_0

    float-to-double v0, p1

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Math;->atan(D)D

    move-result-wide v0

    .line 329
    const-wide v2, 0x400921fb54442d18L    # Math.PI

    invoke-static {p2}, Ljava/lang/Math;->abs(F)F

    move-result v4

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v4

    sub-double/2addr v2, v4

    sub-double/2addr v2, v0

    .line 330
    invoke-static {v2, v3}, Ljava/lang/Math;->sin(D)D

    move-result-wide v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    div-double v0, v2, v0

    double-to-float v0, v0

    return v0

    .line 328
    :cond_0
    div-float v0, v1, p1

    float-to-double v0, v0

    goto :goto_0
.end method

.method public a(F)V
    .locals 0

    .prologue
    .line 108
    iput p1, p0, Lhdy;->a:F

    .line 109
    invoke-virtual {p0}, Lhdy;->invalidate()V

    .line 110
    return-void
.end method

.method public a(II)V
    .locals 0

    .prologue
    .line 200
    iput p1, p0, Lhdy;->f:I

    .line 201
    iput p2, p0, Lhdy;->g:I

    .line 203
    invoke-virtual {p0}, Lhdy;->invalidate()V

    .line 204
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 99
    iput-object p1, p0, Lhdy;->b:Landroid/graphics/Bitmap;

    .line 101
    invoke-virtual {p0}, Lhdy;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lhdy;->getHeight()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lhdy;->b(II)V

    .line 102
    return-void
.end method

.method public declared-synchronized a(FZ)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/high16 v5, 0x43b40000    # 360.0f

    const/4 v0, 0x1

    .line 140
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lhdy;->m:Z

    if-nez v2, :cond_0

    iget v2, p0, Lhdy;->j:F
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmpl-float v2, p1, v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 188
    :goto_0
    monitor-exit p0

    return v0

    .line 144
    :cond_1
    const/high16 v1, 0x42b40000    # 90.0f

    rem-float v1, p1, v1

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_2

    .line 145
    :try_start_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Rotation angle must be aligned to 90 degrees"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 148
    :cond_2
    if-nez p2, :cond_3

    .line 149
    :try_start_2
    iput p1, p0, Lhdy;->j:F

    .line 150
    invoke-virtual {p0}, Lhdy;->invalidate()V

    goto :goto_0

    .line 154
    :cond_3
    iget v1, p0, Lhdy;->j:F

    invoke-static {v1}, Lhdy;->c(F)F

    move-result v2

    .line 155
    invoke-static {p1}, Lhdy;->c(F)F

    move-result v1

    .line 156
    sub-float v3, v2, v1

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x43340000    # 180.0f

    cmpl-float v3, v3, v4

    if-lez v3, :cond_4

    .line 157
    cmpg-float v3, v2, v1

    if-gez v3, :cond_5

    .line 158
    add-float/2addr v2, v5

    .line 164
    :cond_4
    :goto_1
    const-string v3, "intermediateRotation"

    const/4 v4, 0x2

    new-array v4, v4, [F

    const/4 v5, 0x0

    aput v2, v4, v5

    const/4 v2, 0x1

    aput v1, v4, v2

    .line 165
    invoke-static {p0, v3, v4}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    .line 166
    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 167
    new-instance v2, Lhdz;

    invoke-direct {v2, p0, p1}, Lhdz;-><init>(Lhdy;F)V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 187
    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 160
    :cond_5
    add-float/2addr v1, v5

    goto :goto_1
.end method

.method public b(F)Landroid/graphics/RectF;
    .locals 6

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    .line 122
    const/high16 v0, 0x3f800000    # 1.0f

    iget-object v1, p0, Lhdy;->b:Landroid/graphics/Bitmap;

    .line 123
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lhdy;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v1, v2

    invoke-virtual {p0, v1, p1}, Lhdy;->a(FF)F

    move-result v1

    div-float/2addr v0, v1

    mul-float/2addr v0, v5

    .line 124
    new-instance v1, Landroid/graphics/RectF;

    sub-float v2, v5, v0

    sub-float v3, v5, v0

    add-float v4, v0, v5

    add-float/2addr v0, v5

    invoke-direct {v1, v2, v3, v4, v0}, Landroid/graphics/RectF;-><init>(FFFF)V

    return-object v1
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 297
    iget-object v0, p0, Lhdy;->b:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhdy;->i:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 321
    :cond_0
    :goto_0
    return-void

    .line 301
    :cond_1
    iget v0, p0, Lhdy;->j:F

    iget-object v1, p0, Lhdy;->i:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iget-object v2, p0, Lhdy;->i:Landroid/graphics/RectF;

    .line 302
    invoke-virtual {v2}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    .line 301
    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 304
    iget-object v0, p0, Lhdy;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float/2addr v0, v3

    .line 305
    iget-object v1, p0, Lhdy;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    .line 306
    iget-object v2, p0, Lhdy;->b:Landroid/graphics/Bitmap;

    .line 307
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, p0, Lhdy;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v2, v3

    iget v3, p0, Lhdy;->a:F

    neg-float v3, v3

    invoke-virtual {p0, v2, v3}, Lhdy;->a(FF)F

    move-result v2

    .line 309
    iget-object v3, p0, Lhdy;->n:Landroid/graphics/Matrix;

    iget-object v4, p0, Lhdy;->l:Landroid/graphics/Matrix;

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 310
    iget-object v3, p0, Lhdy;->n:Landroid/graphics/Matrix;

    invoke-virtual {v3, v2, v2, v0, v1}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 311
    iget-object v2, p0, Lhdy;->n:Landroid/graphics/Matrix;

    iget v3, p0, Lhdy;->a:F

    neg-float v3, v3

    invoke-virtual {v2, v3, v0, v1}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 312
    iget-object v0, p0, Lhdy;->n:Landroid/graphics/Matrix;

    iget-object v1, p0, Lhdy;->h:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 314
    iget-object v0, p0, Lhdy;->b:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lhdy;->n:Landroid/graphics/Matrix;

    iget-object v2, p0, Lhdy;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 315
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 316
    iget-object v0, p0, Lhdy;->i:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 317
    iget-object v0, p0, Lhdy;->b:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lhdy;->n:Landroid/graphics/Matrix;

    iget-object v2, p0, Lhdy;->c:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 318
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 320
    iget-object v0, p0, Lhdy;->e:Lhci;

    iget-object v1, p0, Lhdy;->i:Landroid/graphics/RectF;

    iget v2, p0, Lhdy;->f:I

    iget v3, p0, Lhdy;->g:I

    invoke-virtual {v0, p1, v1, v2, v3}, Lhci;->a(Landroid/graphics/Canvas;Landroid/graphics/RectF;II)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    .prologue
    .line 288
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 290
    sub-int v0, p4, p2

    sub-int v1, p5, p3

    invoke-direct {p0, v0, v1}, Lhdy;->b(II)V

    .line 291
    return-void
.end method
