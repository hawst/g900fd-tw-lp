.class public Lauy;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lauz;

.field private d:Landroid/media/MediaPlayer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lauy;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lauy;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Lauz;

    invoke-direct {v0, p0}, Lauz;-><init>(Lauy;)V

    iput-object v0, p0, Lauy;->c:Lauz;

    .line 31
    const-string v0, "context"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lauy;->b:Landroid/content/Context;

    .line 32
    return-void
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lauy;->a:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lauy;->d:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 60
    iget-object v0, p0, Lauy;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lauy;->d:Landroid/media/MediaPlayer;

    .line 63
    :cond_0
    return-void
.end method

.method public a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 40
    invoke-virtual {p0}, Lauy;->a()V

    .line 42
    :try_start_0
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lauy;->d:Landroid/media/MediaPlayer;

    .line 43
    iget-object v0, p0, Lauy;->d:Landroid/media/MediaPlayer;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 44
    iget-object v0, p0, Lauy;->d:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lauy;->b:Landroid/content/Context;

    invoke-virtual {v0, v1, p1}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V

    .line 45
    iget-object v0, p0, Lauy;->d:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lauy;->c:Lauz;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 46
    iget-object v0, p0, Lauy;->d:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lauy;->c:Lauz;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 47
    iget-object v0, p0, Lauy;->d:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lauy;->c:Lauz;

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 48
    iget-object v0, p0, Lauy;->d:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 53
    :goto_0
    return-void

    .line 49
    :catch_0
    move-exception v0

    .line 51
    sget-object v1, Lauy;->a:Ljava/lang/String;

    const-string v2, "Media player produced an exception."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
