.class final Ljhp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljhl;

.field private b:Ljava/util/Date;

.field private final c:Ljhn;

.field private final d:Ljhm;

.field private e:J

.field private f:I


# direct methods
.method constructor <init>(ILjava/util/Date;JLjhl;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p5, p0, Ljhp;->a:Ljhl;

    .line 44
    iput-wide p3, p0, Ljhp;->e:J

    .line 45
    iput p1, p0, Ljhp;->f:I

    .line 46
    iput-object p2, p0, Ljhp;->b:Ljava/util/Date;

    .line 48
    new-instance v0, Ljhn;

    invoke-direct {v0}, Ljhn;-><init>()V

    iput-object v0, p0, Ljhp;->c:Ljhn;

    .line 49
    new-instance v0, Ljhm;

    invoke-direct {v0}, Ljhm;-><init>()V

    iput-object v0, p0, Ljhp;->d:Ljhm;

    .line 50
    return-void
.end method

.method public constructor <init>(Ljhl;)V
    .locals 7

    .prologue
    .line 57
    const/4 v2, 0x0

    new-instance v3, Ljava/util/Date;

    invoke-direct {v3}, Ljava/util/Date;-><init>()V

    const-wide/16 v4, -0x1

    move-object v1, p0

    move-object v6, p1

    invoke-direct/range {v1 .. v6}, Ljhp;-><init>(ILjava/util/Date;JLjhl;)V

    .line 58
    return-void
.end method


# virtual methods
.method public a()Ljhl;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Ljhp;->a:Ljhl;

    return-object v0
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 91
    iput p1, p0, Ljhp;->f:I

    .line 92
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 75
    iput-wide p1, p0, Ljhp;->e:J

    .line 76
    return-void
.end method

.method public b()J
    .locals 2

    .prologue
    .line 67
    iget-wide v0, p0, Ljhp;->e:J

    return-wide v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 83
    iget v0, p0, Ljhp;->f:I

    return v0
.end method

.method public d()Ljava/util/Date;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Ljhp;->b:Ljava/util/Date;

    return-object v0
.end method

.method public e()Ljhn;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Ljhp;->c:Ljhn;

    return-object v0
.end method

.method public f()Ljhm;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Ljhp;->d:Ljhm;

    return-object v0
.end method
