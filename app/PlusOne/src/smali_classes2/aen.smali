.class public final Laen;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static b:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Laeo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Laen;->b:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p1, p0, Laen;->a:Ljava/lang/String;

    .line 52
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 55
    sget-object v0, Laen;->b:Ljava/util/HashMap;

    iget-object v1, p0, Laen;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeo;

    .line 56
    if-nez v0, :cond_0

    .line 57
    new-instance v0, Laeo;

    invoke-direct {v0}, Laeo;-><init>()V

    .line 58
    sget-object v1, Laen;->b:Ljava/util/HashMap;

    iget-object v2, p0, Laen;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Laeo;->e:J

    .line 61
    return-void
.end method

.method public b()V
    .locals 6

    .prologue
    .line 69
    sget-object v0, Laen;->b:Ljava/util/HashMap;

    iget-object v1, p0, Laen;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeo;

    .line 70
    if-eqz v0, :cond_0

    iget-wide v2, v0, Laeo;->e:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 71
    :cond_0
    const-string v0, "Timing"

    const-string v1, "No begin called."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :goto_0
    return-void

    .line 74
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, v0, Laeo;->e:J

    sub-long/2addr v2, v4

    .line 75
    iget v1, v0, Laeo;->a:I

    if-nez v1, :cond_2

    .line 76
    iput-wide v2, v0, Laeo;->b:J

    .line 77
    iput-wide v2, v0, Laeo;->c:J

    .line 78
    iput-wide v2, v0, Laeo;->d:J

    .line 79
    iput-wide v2, v0, Laeo;->f:J

    .line 80
    const/4 v1, 0x1

    iput v1, v0, Laeo;->a:I

    goto :goto_0

    .line 82
    :cond_2
    iget v1, v0, Laeo;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Laeo;->a:I

    .line 83
    iget-wide v4, v0, Laeo;->d:J

    add-long/2addr v4, v2

    iput-wide v4, v0, Laeo;->d:J

    .line 84
    iget-wide v4, v0, Laeo;->b:J

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v4

    iput-wide v4, v0, Laeo;->b:J

    .line 85
    iget-wide v4, v0, Laeo;->c:J

    invoke-static {v4, v5, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    iput-wide v4, v0, Laeo;->c:J

    .line 86
    iput-wide v2, v0, Laeo;->f:J

    goto :goto_0
.end method

.method public c()V
    .locals 8

    .prologue
    .line 91
    sget-object v0, Laen;->b:Ljava/util/HashMap;

    iget-object v1, p0, Laen;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laeo;

    .line 92
    if-nez v0, :cond_0

    .line 98
    :goto_0
    return-void

    .line 94
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Timing: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Laen;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Average: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "%.2f"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v0}, Laeo;->a()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Maximum: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Laeo;->b()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Latest: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, v0, Laeo;->f:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Laeo;->c()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
