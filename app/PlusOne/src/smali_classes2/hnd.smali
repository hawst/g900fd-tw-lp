.class final Lhnd;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmhk;",
        "Lmhl;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Lmti;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lhne;",
            ">;"
        }
    .end annotation
.end field

.field private c:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkfo;Ljava/util/List;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lkfo;",
            "Ljava/util/List",
            "<",
            "Lhne;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 30
    const-string v3, "postclientlogs"

    new-instance v4, Lmhk;

    invoke-direct {v4}, Lmhk;-><init>()V

    new-instance v5, Lmhl;

    invoke-direct {v5}, Lmhl;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 34
    iput-object p3, p0, Lhnd;->b:Ljava/util/List;

    .line 35
    invoke-static {p1}, Lhmt;->a(Landroid/content/Context;)Lmti;

    move-result-object v0

    iput-object v0, p0, Lhnd;->a:Lmti;

    .line 36
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Lmth;

    .line 37
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 38
    iget-object v0, p0, Lhnd;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhne;

    iget-object v0, v0, Lhne;->b:Lmth;

    aput-object v0, v2, v1

    .line 37
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 40
    :cond_0
    iget-object v0, p0, Lhnd;->a:Lmti;

    iput-object v2, v0, Lmti;->a:[Lmth;

    .line 41
    iput-object p1, p0, Lhnd;->c:Landroid/content/Context;

    .line 42
    return-void
.end method


# virtual methods
.method protected a(Lmhk;)V
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lmxe;

    invoke-direct {v0}, Lmxe;-><init>()V

    iput-object v0, p1, Lmhk;->a:Lmxe;

    .line 52
    iget-object v0, p1, Lmhk;->apiHeader:Llyq;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Llyq;->a:Ljava/lang/Boolean;

    .line 53
    iget-object v0, p1, Lmhk;->a:Lmxe;

    iget-object v1, p0, Lhnd;->a:Lmti;

    iput-object v1, v0, Lmxe;->a:Lmti;

    .line 54
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 21
    check-cast p1, Lmhk;

    invoke-virtual {p0, p1}, Lhnd;->a(Lmhk;)V

    return-void
.end method

.method protected synthetic a_(Loxu;)V
    .locals 0

    .prologue
    .line 21
    invoke-virtual {p0}, Lhnd;->i()V

    return-void
.end method

.method public b([BLjava/lang/String;)V
    .locals 3

    .prologue
    .line 65
    iget-object v0, p0, Lhnd;->c:Landroid/content/Context;

    iget-object v1, p0, Lkff;->g:Lkfo;

    invoke-virtual {v1}, Lkfo;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lhnd;->b:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lhnb;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    .line 66
    invoke-super {p0, p1, p2}, Lkgg;->b([BLjava/lang/String;)V

    .line 67
    return-void
.end method

.method protected i()V
    .locals 3

    .prologue
    .line 59
    iget-object v0, p0, Lhnd;->c:Landroid/content/Context;

    iget-object v1, p0, Lkff;->g:Lkfo;

    invoke-virtual {v1}, Lkfo;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lhnd;->b:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lhnb;->b(Landroid/content/Context;Ljava/lang/String;Ljava/util/List;)V

    .line 60
    return-void
.end method
