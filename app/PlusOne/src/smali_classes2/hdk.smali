.class public final Lhdk;
.super Lhdf;
.source "PG"


# instance fields
.field private g:Lhdl;

.field private h:Lhdm;

.field private i:Z

.field private j:F

.field private k:F

.field private final l:Landroid/graphics/PointF;

.field private m:F

.field private final n:Lhcq;

.field private final o:Landroid/graphics/PointF;

.field private final p:Landroid/graphics/PointF;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lhdf;-><init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V

    .line 24
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lhdk;->l:Landroid/graphics/PointF;

    .line 125
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lhdk;->o:Landroid/graphics/PointF;

    .line 126
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lhdk;->p:Landroid/graphics/PointF;

    .line 33
    new-instance v0, Lhcq;

    invoke-virtual {p1}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lhcq;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lhdk;->n:Lhcq;

    .line 34
    return-void
.end method


# virtual methods
.method public a(F)V
    .locals 0

    .prologue
    .line 47
    iput p1, p0, Lhdk;->m:F

    .line 48
    return-void
.end method

.method protected a(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V
    .locals 17

    .prologue
    .line 130
    const/4 v2, 0x3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/graphics/Canvas;->save(I)I

    .line 132
    invoke-virtual/range {p1 .. p2}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    .line 133
    move-object/from16 v0, p0

    iget-object v2, v0, Lhdk;->b:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v3, v0, Lhdk;->b:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 135
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->width()F

    move-result v2

    .line 136
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/RectF;->height()F

    move-result v3

    .line 138
    move-object/from16 v0, p0

    iget-object v4, v0, Lhdk;->h:Lhdm;

    move-object/from16 v0, p0

    iget-object v5, v0, Lhdk;->p:Landroid/graphics/PointF;

    invoke-interface {v4, v5}, Lhdm;->a(Landroid/graphics/PointF;)V

    .line 139
    move-object/from16 v0, p0

    iget-object v4, v0, Lhdk;->p:Landroid/graphics/PointF;

    iget v5, v4, Landroid/graphics/PointF;->x:F

    const/high16 v6, 0x42c80000    # 100.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/PointF;->x:F

    .line 140
    move-object/from16 v0, p0

    iget-object v4, v0, Lhdk;->p:Landroid/graphics/PointF;

    iget v5, v4, Landroid/graphics/PointF;->y:F

    const/high16 v6, 0x42c80000    # 100.0f

    div-float/2addr v5, v6

    iput v5, v4, Landroid/graphics/PointF;->y:F

    .line 142
    move-object/from16 v0, p0

    iget-object v4, v0, Lhdk;->h:Lhdm;

    invoke-interface {v4}, Lhdm;->at()F

    move-result v4

    move-object/from16 v0, p0

    iget v5, v0, Lhdk;->m:F

    add-float/2addr v4, v5

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->toDegrees(D)D

    move-result-wide v4

    double-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lhdk;->o:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v6, v0, Lhdk;->o:Landroid/graphics/PointF;

    iget v6, v6, Landroid/graphics/PointF;->y:F

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, Landroid/graphics/Canvas;->rotate(FFF)V

    .line 145
    move-object/from16 v0, p0

    iget-object v4, v0, Lhdk;->h:Lhdm;

    invoke-interface {v4}, Lhdm;->au()F

    move-result v13

    .line 146
    move-object/from16 v0, p0

    iget-object v4, v0, Lhdk;->h:Lhdm;

    invoke-interface {v4}, Lhdm;->av()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 147
    move-object/from16 v0, p0

    iget-object v4, v0, Lhdk;->p:Landroid/graphics/PointF;

    iget v8, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v9

    const/high16 v2, 0x40000000    # 2.0f

    div-float v10, v9, v2

    mul-float v11, v8, v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lhdk;->n:Lhcq;

    neg-float v4, v9

    neg-float v5, v11

    const/high16 v3, 0x40000000    # 2.0f

    mul-float v6, v9, v3

    neg-float v7, v11

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Lhcq;->a(Landroid/graphics/Canvas;FFFF)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lhdk;->n:Lhcq;

    neg-float v4, v9

    const/high16 v3, 0x40000000    # 2.0f

    mul-float v6, v9, v3

    move-object/from16 v3, p1

    move v5, v11

    move v7, v11

    invoke-virtual/range {v2 .. v7}, Lhcq;->a(Landroid/graphics/Canvas;FFFF)V

    const/high16 v2, 0x40000000    # 2.0f

    div-float v2, v13, v2

    add-float/2addr v2, v8

    mul-float v8, v2, v10

    move-object/from16 v0, p0

    iget-object v2, v0, Lhdk;->n:Lhcq;

    neg-float v4, v9

    neg-float v5, v8

    const/high16 v3, 0x40000000    # 2.0f

    mul-float v6, v9, v3

    neg-float v7, v8

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Lhcq;->a(Landroid/graphics/Canvas;FFFF)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lhdk;->n:Lhcq;

    neg-float v4, v9

    const/high16 v3, 0x40000000    # 2.0f

    mul-float v6, v9, v3

    move-object/from16 v3, p1

    move v5, v8

    move v7, v8

    invoke-virtual/range {v2 .. v7}, Lhcq;->a(Landroid/graphics/Canvas;FFFF)V

    .line 152
    :goto_0
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 153
    return-void

    .line 149
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lhdk;->p:Landroid/graphics/PointF;

    iget v14, v4, Landroid/graphics/PointF;->x:F

    move-object/from16 v0, p0

    iget-object v4, v0, Lhdk;->p:Landroid/graphics/PointF;

    iget v15, v4, Landroid/graphics/PointF;->y:F

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    const/high16 v3, 0x3f000000    # 0.5f

    mul-float v16, v2, v3

    mul-float v6, v14, v16

    mul-float v12, v15, v16

    move-object/from16 v0, p0

    iget-object v2, v0, Lhdk;->n:Lhcq;

    neg-float v4, v6

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Lhcq;->a(Landroid/graphics/Canvas;FFFF)V

    move-object/from16 v0, p0

    iget-object v7, v0, Lhdk;->n:Lhcq;

    const/4 v9, 0x0

    neg-float v10, v12

    const/4 v11, 0x0

    move-object/from16 v8, p1

    invoke-virtual/range {v7 .. v12}, Lhcq;->a(Landroid/graphics/Canvas;FFFF)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lhdk;->n:Lhcq;

    neg-float v4, v6

    neg-float v5, v12

    move-object/from16 v3, p1

    move v7, v12

    invoke-virtual/range {v2 .. v7}, Lhcq;->b(Landroid/graphics/Canvas;FFFF)V

    float-to-double v2, v14

    float-to-double v4, v15

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->hypot(DD)D

    move-result-wide v2

    double-to-float v2, v2

    div-float v3, v14, v2

    div-float v4, v15, v2

    cmpl-float v2, v3, v4

    if-ltz v2, :cond_1

    move v2, v3

    :goto_1
    mul-float/2addr v3, v13

    mul-float/2addr v3, v2

    add-float/2addr v3, v14

    mul-float v6, v3, v16

    mul-float v3, v13, v4

    mul-float/2addr v2, v3

    add-float/2addr v2, v15

    mul-float v7, v2, v16

    move-object/from16 v0, p0

    iget-object v2, v0, Lhdk;->n:Lhcq;

    neg-float v4, v6

    neg-float v5, v7

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Lhcq;->b(Landroid/graphics/Canvas;FFFF)V

    goto :goto_0

    :cond_1
    move v2, v4

    goto :goto_1
.end method

.method public a(Lhdl;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lhdk;->g:Lhdl;

    .line 39
    iput-object p1, p0, Lhdf;->a:Lhdg;

    .line 40
    return-void
.end method

.method public a(Lhdm;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lhdk;->h:Lhdm;

    .line 44
    return-void
.end method

.method public e(FF)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 52
    iget-object v1, p0, Lhdk;->h:Lhdm;

    if-nez v1, :cond_0

    .line 53
    const/4 v0, 0x0

    .line 69
    :goto_0
    return v0

    .line 56
    :cond_0
    float-to-double v2, p2

    invoke-static {v2, v3}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v2

    double-to-float v1, v2

    iget v2, p0, Lhdk;->m:F

    add-float/2addr v1, v2

    .line 57
    iput-boolean v0, p0, Lhdk;->i:Z

    .line 59
    iget-object v2, p0, Lhdk;->h:Lhdm;

    invoke-interface {v2}, Lhdm;->at()F

    move-result v2

    .line 60
    sub-float/2addr v1, v2

    iput v1, p0, Lhdk;->j:F

    .line 61
    iget v1, p0, Lhdk;->j:F

    const v2, 0x40490fdb    # (float)Math.PI

    rem-float/2addr v1, v2

    iput v1, p0, Lhdk;->j:F

    .line 63
    iput p1, p0, Lhdk;->k:F

    .line 64
    iget-object v1, p0, Lhdk;->h:Lhdm;

    iget-object v2, p0, Lhdk;->l:Landroid/graphics/PointF;

    invoke-interface {v1, v2}, Lhdm;->a(Landroid/graphics/PointF;)V

    .line 66
    iput-boolean v0, p0, Lhdk;->e:Z

    .line 67
    invoke-virtual {p0, v0}, Lhdk;->b(Z)V

    goto :goto_0
.end method

.method public f(FF)Z
    .locals 9

    .prologue
    const/4 v3, 0x0

    const v2, 0x40490fdb    # (float)Math.PI

    const/high16 v8, 0x42820000    # 65.0f

    const/high16 v7, 0x3f800000    # 1.0f

    .line 74
    iget-boolean v0, p0, Lhdk;->i:Z

    if-nez v0, :cond_0

    move v0, v3

    .line 112
    :goto_0
    return v0

    .line 78
    :cond_0
    float-to-double v0, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->toRadians(D)D

    move-result-wide v0

    double-to-float v0, v0

    iget v1, p0, Lhdk;->m:F

    add-float/2addr v0, v1

    .line 80
    iget v1, p0, Lhdk;->j:F

    sub-float/2addr v0, v1

    .line 81
    rem-float/2addr v0, v2

    .line 83
    const/4 v1, 0x0

    cmpg-float v1, v0, v1

    if-gez v1, :cond_1

    .line 84
    add-float/2addr v0, v2

    .line 87
    :cond_1
    iget v1, p0, Lhdk;->k:F

    div-float v1, p1, v1

    .line 91
    cmpl-float v2, v1, v7

    if-eqz v2, :cond_2

    .line 92
    iget v2, p0, Lhdk;->j:F

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    float-to-double v4, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->sin(D)D

    move-result-wide v4

    double-to-float v4, v4

    .line 93
    sub-float v2, v7, v4

    .line 95
    cmpl-float v5, v1, v7

    if-lez v5, :cond_3

    .line 96
    iget-object v5, p0, Lhdk;->l:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    sub-float v6, v1, v7

    mul-float/2addr v2, v6

    add-float/2addr v2, v7

    mul-float/2addr v2, v8

    add-float/2addr v2, v5

    sub-float/2addr v2, v8

    float-to-int v2, v2

    .line 98
    iget-object v5, p0, Lhdk;->l:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v7

    mul-float/2addr v1, v4

    add-float/2addr v1, v7

    mul-float/2addr v1, v8

    add-float/2addr v1, v5

    sub-float/2addr v1, v8

    float-to-int v1, v1

    .line 107
    :goto_1
    iget-object v4, p0, Lhdk;->g:Lhdl;

    invoke-interface {v4, v0, v2, v1}, Lhdl;->a(FII)V

    .line 109
    invoke-virtual {p0, v3}, Lhdk;->b(Z)V

    .line 112
    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    .line 101
    :cond_3
    iget-object v5, p0, Lhdk;->l:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->x:F

    sub-float v6, v7, v1

    mul-float/2addr v2, v6

    sub-float v2, v7, v2

    div-float v2, v8, v2

    sub-float v2, v5, v2

    add-float/2addr v2, v8

    float-to-int v2, v2

    .line 103
    iget-object v5, p0, Lhdk;->l:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    sub-float v1, v7, v1

    mul-float/2addr v1, v4

    sub-float v1, v7, v1

    div-float v1, v8, v1

    sub-float v1, v5, v1

    add-float/2addr v1, v8

    float-to-int v1, v1

    goto :goto_1
.end method

.method public h()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 117
    iput-boolean v0, p0, Lhdk;->i:Z

    .line 119
    iput-boolean v0, p0, Lhdk;->e:Z

    .line 120
    invoke-virtual {p0, v1}, Lhdk;->b(Z)V

    .line 122
    return v1
.end method
