.class final Lash;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lasm;


# instance fields
.field private synthetic a:Lase;


# direct methods
.method constructor <init>(Lase;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lash;->a:Lase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 232
    iget-object v1, p0, Lash;->a:Lase;

    iget-object v0, p0, Lash;->a:Lase;

    iget-object v0, v0, Lase;->o:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lasl;

    iget-object v2, p0, Lash;->a:Lase;

    iget-wide v2, v2, Lase;->l:J

    invoke-static {v1, v0, v2, v3}, Lase;->a(Lase;Lasl;J)V

    .line 233
    iget-object v0, p0, Lash;->a:Lase;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lase;->f:Z

    .line 234
    return-void
.end method

.method public a(IZ)V
    .locals 6

    .prologue
    .line 220
    iget-object v0, p0, Lash;->a:Lase;

    .line 221
    iget v1, v0, Lase;->j:I

    iget-object v0, p0, Lash;->a:Lase;

    iget-wide v2, v0, Lase;->a:J

    iget-object v0, p0, Lash;->a:Lase;

    iget-wide v4, v0, Lase;->k:J

    move v0, p1

    .line 220
    invoke-static/range {v0 .. v5}, Lchf;->a(IIJJ)J

    move-result-wide v2

    .line 222
    iget-object v0, p0, Lash;->a:Lase;

    iget-object v0, v0, Lase;->o:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lasl;

    invoke-interface {v0, v2, v3}, Lasl;->a(J)V

    .line 223
    if-eqz p2, :cond_0

    .line 224
    iget-object v0, p0, Lash;->a:Lase;

    invoke-virtual {v0, v2, v3}, Lase;->a(J)V

    .line 225
    iget-object v1, p0, Lash;->a:Lase;

    iget-object v0, p0, Lash;->a:Lase;

    iget-object v0, v0, Lase;->o:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lasl;

    invoke-virtual {v1, v0, v2, v3}, Lase;->a(Lasl;J)V

    .line 227
    :cond_0
    iget-object v0, p0, Lash;->a:Lase;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lase;->e:Z

    .line 228
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 238
    iget-object v1, p0, Lash;->a:Lase;

    iget-object v0, p0, Lash;->a:Lase;

    iget-object v0, v0, Lase;->o:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lasl;

    iget-object v2, p0, Lash;->a:Lase;

    iget-wide v2, v2, Lase;->m:J

    invoke-static {v1, v0, v2, v3}, Lase;->a(Lase;Lasl;J)V

    .line 239
    iget-object v0, p0, Lash;->a:Lase;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lase;->f:Z

    .line 240
    return-void
.end method

.method public c()V
    .locals 4

    .prologue
    .line 244
    iget-object v1, p0, Lash;->a:Lase;

    iget-object v0, p0, Lash;->a:Lase;

    iget-object v0, v0, Lase;->o:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lasl;

    iget-object v2, p0, Lash;->a:Lase;

    iget-wide v2, v2, Lase;->n:J

    invoke-static {v1, v0, v2, v3}, Lase;->a(Lase;Lasl;J)V

    .line 245
    iget-object v0, p0, Lash;->a:Lase;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lase;->f:Z

    .line 246
    return-void
.end method

.method public d()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 250
    iget-object v0, p0, Lash;->a:Lase;

    iget-boolean v0, v0, Lase;->d:Z

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lash;->a:Lase;

    iget-object v0, v0, Lase;->c:Lawn;

    invoke-interface {v0}, Lawn;->ac()Z

    .line 252
    iget-object v0, p0, Lash;->a:Lase;

    iget-object v0, v0, Lase;->c:Lawn;

    iget-object v1, p0, Lash;->a:Lase;

    iget-wide v2, v1, Lase;->g:J

    invoke-interface {v0, v2, v3}, Lawn;->c(J)V

    .line 253
    iget-object v0, p0, Lash;->a:Lase;

    iput-boolean v8, v0, Lase;->d:Z

    .line 255
    :cond_0
    iget-object v0, p0, Lash;->a:Lase;

    iget-object v1, v0, Lase;->b:Lcdu;

    iget-object v0, p0, Lash;->a:Lase;

    iget-wide v2, v0, Lase;->g:J

    iget-object v0, p0, Lash;->a:Lase;

    iget-wide v4, v0, Lase;->h:J

    iget-object v0, p0, Lash;->a:Lase;

    .line 256
    iget-boolean v6, v0, Lase;->e:Z

    iget-object v0, p0, Lash;->a:Lase;

    iget-boolean v7, v0, Lase;->f:Z

    .line 255
    invoke-virtual/range {v1 .. v7}, Lcdu;->a(JJZZ)V

    .line 257
    iget-object v0, p0, Lash;->a:Lase;

    iput-boolean v8, v0, Lase;->e:Z

    .line 258
    iget-object v0, p0, Lash;->a:Lase;

    iput-boolean v8, v0, Lase;->f:Z

    .line 259
    return-void
.end method
