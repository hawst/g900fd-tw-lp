.class public final Lcfj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lcfj;->a:Landroid/util/LongSparseArray;

    .line 18
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcfj;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    return v0
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 27
    iget-object v0, p0, Lcfj;->a:Landroid/util/LongSparseArray;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Landroid/util/LongSparseArray;->append(JLjava/lang/Object;)V

    .line 28
    return-void
.end method

.method public a(Lcfj;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-virtual {p1}, Lcfj;->a()I

    move-result v2

    move v1, v0

    .line 40
    :goto_0
    if-ge v1, v2, :cond_1

    .line 41
    iget-object v3, p1, Lcfj;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v3, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    .line 42
    iget-object v3, p0, Lcfj;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v3, v4, v5}, Landroid/util/LongSparseArray;->indexOfKey(J)I

    move-result v3

    if-gez v3, :cond_0

    .line 43
    const/4 v0, 0x1

    .line 44
    iget-object v3, p0, Lcfj;->a:Landroid/util/LongSparseArray;

    const/4 v6, 0x0

    invoke-virtual {v3, v4, v5, v6}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 40
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 47
    :cond_1
    return v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcfj;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(J)Z
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcfj;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->indexOfKey(J)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcfj;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    .line 70
    return-void
.end method

.method public d()[J
    .locals 8

    .prologue
    .line 87
    invoke-virtual {p0}, Lcfj;->a()I

    move-result v1

    .line 88
    new-array v2, v1, [J

    .line 89
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 90
    iget-object v3, p0, Lcfj;->a:Landroid/util/LongSparseArray;

    invoke-virtual {v3, v0}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    .line 91
    aput-wide v4, v2, v0

    .line 92
    if-lez v0, :cond_0

    add-int/lit8 v3, v0, -0x1

    aget-wide v6, v2, v3

    cmp-long v3, v4, v6

    if-gez v3, :cond_0

    .line 93
    const-string v0, "LongSparseArray not sorted"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 89
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 96
    :cond_1
    return-object v2
.end method
