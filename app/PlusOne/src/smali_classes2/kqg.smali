.class public final Lkqg;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheg;
.implements Llnx;
.implements Llpu;
.implements Llqz;
.implements Llrd;
.implements Llre;
.implements Llrg;


# static fields
.field private static final a:Llpa;


# instance fields
.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lkqi;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Landroid/app/Activity;

.field private d:Lhlw;

.field private e:Z

.field private f:I

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 52
    new-instance v0, Llpa;

    const-string v1, "debug.launch_instr.dogfood"

    invoke-direct {v0, v1}, Llpa;-><init>(Ljava/lang/String;)V

    sput-object v0, Lkqg;->a:Llpa;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 2

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkqg;->b:Ljava/util/List;

    .line 82
    const/4 v0, 0x1

    iput v0, p0, Lkqg;->f:I

    .line 83
    const/4 v0, -0x1

    iput v0, p0, Lkqg;->g:I

    .line 86
    iput-object p1, p0, Lkqg;->c:Landroid/app/Activity;

    .line 87
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 89
    iget-object v0, p0, Lkqg;->b:Ljava/util/List;

    new-instance v1, Lkqj;

    invoke-direct {v1}, Lkqj;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    iget-object v0, p0, Lkqg;->b:Ljava/util/List;

    new-instance v1, Lkqk;

    invoke-direct {v1}, Lkqk;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 91
    iget-object v0, p0, Lkqg;->b:Ljava/util/List;

    new-instance v1, Lkqm;

    invoke-direct {v1}, Lkqm;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    iget-object v0, p0, Lkqg;->b:Ljava/util/List;

    new-instance v1, Lkql;

    invoke-direct {v1}, Lkql;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    return-void
.end method

.method static synthetic b()Llpa;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lkqg;->a:Llpa;

    return-object v0
.end method

.method private c()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 151
    iget-object v1, p0, Lkqg;->c:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 153
    iget-boolean v3, p0, Lkqg;->e:Z

    if-nez v3, :cond_3

    iget v3, p0, Lkqg;->f:I

    if-eq v3, v2, :cond_3

    iget-object v3, p0, Lkqg;->c:Landroid/app/Activity;

    .line 155
    invoke-virtual {v3}, Landroid/app/Activity;->isTaskRoot()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0, v1}, Lkqg;->b(Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    move v1, v2

    .line 157
    :goto_0
    if-eqz v1, :cond_2

    .line 158
    iput-boolean v2, p0, Lkqg;->e:Z

    .line 159
    iget-object v1, p0, Lkqg;->c:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    new-instance v4, Lhml;

    invoke-direct {v4}, Lhml;-><init>()V

    const/4 v2, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lkqg;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_6

    iget-object v0, p0, Lkqg;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkqi;

    invoke-interface {v0, v3}, Lkqi;->a(Landroid/content/Intent;)Z

    move-result v5

    if-eqz v5, :cond_4

    invoke-interface {v0, v3}, Lkqi;->b(Landroid/content/Intent;)Lhmk;

    move-result-object v0

    :goto_2
    if-eqz v0, :cond_5

    :goto_3
    invoke-virtual {v4, v0}, Lhml;->a(Lhmk;)Lhml;

    iget-object v0, v0, Lhmk;->a:Lhmn;

    iget-boolean v0, v0, Lhmn;->b:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lkqg;->c:Landroid/app/Activity;

    invoke-virtual {v4, v0}, Lhml;->a(Landroid/content/Context;)Lhml;

    :cond_1
    new-instance v0, Lhly;

    const/4 v1, 0x4

    invoke-direct {v0, v1, v4}, Lhly;-><init>(ILhml;)V

    iget-object v1, p0, Lkqg;->d:Lhlw;

    iget-object v2, p0, Lkqg;->c:Landroid/app/Activity;

    invoke-interface {v1, v2, v0}, Lhlw;->a(Landroid/content/Context;Lhlv;)V

    .line 161
    :cond_2
    return-void

    :cond_3
    move v1, v0

    .line 155
    goto :goto_0

    .line 159
    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_5
    new-instance v0, Lhmk;

    sget-object v1, Lomw;->c:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    goto :goto_3

    :cond_6
    move-object v0, v2

    goto :goto_2
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0}, Lkqg;->c()V

    .line 131
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 97
    const-class v0, Lhlw;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlw;

    iput-object v0, p0, Lkqg;->d:Lhlw;

    .line 99
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    .line 100
    invoke-interface {v0, p0}, Lhee;->a(Lheg;)Lhee;

    .line 101
    return-void
.end method

.method public a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 135
    invoke-virtual {p0, p1}, Lkqg;->b(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 136
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkqg;->e:Z

    .line 138
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 105
    if-eqz p1, :cond_0

    .line 106
    const-string v0, "state_event_logged"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lkqg;->e:Z

    .line 107
    const-string v0, "state_account_handler_state"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 108
    invoke-static {}, Lhef;->a()[I

    move-result-object v1

    aget v0, v1, v0

    iput v0, p0, Lkqg;->f:I

    .line 109
    const-string v0, "state_account_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lkqg;->g:I

    .line 111
    :cond_0
    return-void
.end method

.method public a(ZIIII)V
    .locals 0

    .prologue
    .line 123
    iput p3, p0, Lkqg;->f:I

    .line 124
    iput p5, p0, Lkqg;->g:I

    .line 125
    invoke-direct {p0}, Lkqg;->c()V

    .line 126
    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 115
    const-string v0, "state_event_logged"

    iget-boolean v1, p0, Lkqg;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 116
    const-string v0, "state_account_id"

    iget v1, p0, Lkqg;->g:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 117
    const-string v0, "state_account_handler_state"

    iget v1, p0, Lkqg;->f:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 118
    return-void
.end method

.method b(Landroid/content/Intent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 141
    move v1, v2

    :goto_0
    iget-object v0, p0, Lkqg;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 142
    iget-object v0, p0, Lkqg;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkqi;

    .line 143
    invoke-interface {v0, p1}, Lkqi;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    const/4 v2, 0x1

    .line 147
    :cond_0
    return v2

    .line 141
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
