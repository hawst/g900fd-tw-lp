.class public final Limv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Limu;


# instance fields
.field private a:Lgsx;


# direct methods
.method public constructor <init>(Lgsx;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Limv;->a:Lgsx;

    .line 21
    return-void
.end method


# virtual methods
.method public a(Ling;)Landroid/graphics/Point;
    .locals 6

    .prologue
    .line 25
    iget-object v0, p0, Limv;->a:Lgsx;

    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, p1, Ling;->a:D

    iget-wide v4, p1, Ling;->b:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v0, v1}, Lgsx;->a(Lcom/google/android/gms/maps/model/LatLng;)Landroid/graphics/Point;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/graphics/Point;)Ling;
    .locals 6

    .prologue
    .line 30
    iget-object v0, p0, Limv;->a:Lgsx;

    invoke-virtual {v0, p1}, Lgsx;->a(Landroid/graphics/Point;)Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    .line 31
    new-instance v1, Ling;

    iget-wide v2, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-direct {v1, v2, v3, v4, v5}, Ling;-><init>(DD)V

    return-object v1
.end method

.method public a()Linn;
    .locals 10

    .prologue
    .line 36
    iget-object v0, p0, Limv;->a:Lgsx;

    invoke-virtual {v0}, Lgsx;->a()Lcom/google/android/gms/maps/model/VisibleRegion;

    move-result-object v0

    .line 37
    new-instance v1, Ling;

    iget-object v2, v0, Lcom/google/android/gms/maps/model/VisibleRegion;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-object v4, v0, Lcom/google/android/gms/maps/model/VisibleRegion;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, v4, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-direct {v1, v2, v3, v4, v5}, Ling;-><init>(DD)V

    .line 39
    new-instance v2, Ling;

    iget-object v3, v0, Lcom/google/android/gms/maps/model/VisibleRegion;->b:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, v3, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-object v3, v0, Lcom/google/android/gms/maps/model/VisibleRegion;->b:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v6, v3, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-direct {v2, v4, v5, v6, v7}, Ling;-><init>(DD)V

    .line 41
    new-instance v3, Ling;

    iget-object v4, v0, Lcom/google/android/gms/maps/model/VisibleRegion;->c:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v4, v4, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-object v6, v0, Lcom/google/android/gms/maps/model/VisibleRegion;->c:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v6, v6, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-direct {v3, v4, v5, v6, v7}, Ling;-><init>(DD)V

    .line 43
    new-instance v4, Ling;

    iget-object v5, v0, Lcom/google/android/gms/maps/model/VisibleRegion;->d:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v6, v5, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-object v5, v0, Lcom/google/android/gms/maps/model/VisibleRegion;->d:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v8, v5, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-direct {v4, v6, v7, v8, v9}, Ling;-><init>(DD)V

    .line 46
    new-instance v5, Link;

    iget-object v0, v0, Lcom/google/android/gms/maps/model/VisibleRegion;->e:Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-direct {v5, v0}, Link;-><init>(Lcom/google/android/gms/maps/model/LatLngBounds;)V

    .line 48
    new-instance v0, Linn;

    invoke-direct {v0, v1, v2, v3, v4}, Linn;-><init>(Ling;Ling;Ling;Ling;)V

    return-object v0
.end method
