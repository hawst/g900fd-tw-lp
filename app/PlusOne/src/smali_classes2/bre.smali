.class public final Lbre;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmo;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Lboh;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lbmo;",
            "Ljava/util/List",
            "<",
            "Lboh;",
            ">;>;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lbza;",
            "Ljava/util/List",
            "<",
            "Lboh;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-class v0, Lbre;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbre;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lbre;->d:Landroid/util/LongSparseArray;

    .line 58
    const-string v0, ""

    iput-object v0, p0, Lbre;->b:Ljava/lang/String;

    .line 59
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbre;->c:Ljava/util/List;

    .line 60
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lbre;->e:Ljava/util/Map;

    .line 61
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lbre;->f:Ljava/util/Map;

    .line 62
    return-void
.end method

.method public constructor <init>(Lbuk;Ljava/lang/String;)V
    .locals 18

    .prologue
    .line 66
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 42
    new-instance v2, Landroid/util/LongSparseArray;

    invoke-direct {v2}, Landroid/util/LongSparseArray;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lbre;->d:Landroid/util/LongSparseArray;

    .line 67
    const-string v2, "defaultBaseUrl"

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 70
    move-object/from16 v0, p1

    iget-object v2, v0, Lbuk;->e:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 71
    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lbre;->b:Ljava/lang/String;

    .line 76
    :goto_0
    move-object/from16 v0, p1

    iget v11, v0, Lbuk;->c:I

    .line 78
    if-eqz v11, :cond_0

    const/4 v2, 0x1

    if-ne v11, v2, :cond_2

    :cond_0
    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_3

    .line 79
    sget-object v2, Lbre;->a:Ljava/lang/String;

    const-string v3, "Track database encryption method not supported."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    new-instance v2, Lbrf;

    invoke-direct {v2, v11}, Lbrf;-><init>(I)V

    throw v2

    .line 73
    :cond_1
    move-object/from16 v0, p1

    iget-object v2, v0, Lbuk;->e:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v2, v0, Lbre;->b:Ljava/lang/String;

    goto :goto_0

    .line 78
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 83
    :cond_3
    move-object/from16 v0, p1

    iget-object v3, v0, Lbuk;->b:[Lbuj;

    .line 84
    new-instance v12, Ljava/util/ArrayList;

    array-length v2, v3

    invoke-direct {v12, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 85
    array-length v2, v3

    .line 86
    invoke-static {v2}, Lcfm;->a(I)Ljava/util/HashMap;

    move-result-object v13

    .line 87
    new-instance v14, Landroid/util/SparseArray;

    array-length v2, v3

    invoke-direct {v14, v2}, Landroid/util/SparseArray;-><init>(I)V

    .line 90
    array-length v4, v3

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    .line 91
    new-instance v6, Lbmo;

    iget-object v7, v5, Lbuj;->c:Ljava/lang/String;

    invoke-direct {v6, v7}, Lbmo;-><init>(Ljava/lang/String;)V

    .line 92
    iget v5, v5, Lbuj;->b:I

    invoke-virtual {v14, v5, v6}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 93
    invoke-virtual {v12, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 94
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v13, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 90
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 98
    :cond_4
    move-object/from16 v0, p1

    iget-object v15, v0, Lbuk;->a:[Lbum;

    array-length v0, v15

    move/from16 v16, v0

    const/4 v2, 0x0

    move v10, v2

    :goto_3
    move/from16 v0, v16

    if-ge v10, v0, :cond_6

    aget-object v17, v15, v10

    .line 99
    move-object/from16 v0, v17

    iget-wide v2, v0, Lbum;->d:J

    invoke-static {v11, v2, v3}, Lbre;->a(IJ)J

    move-result-wide v6

    .line 100
    new-instance v3, Lboh;

    move-object/from16 v0, v17

    iget-object v4, v0, Lbum;->c:Ljava/lang/String;

    move-object/from16 v0, v17

    iget-object v5, v0, Lbum;->b:Ljava/lang/String;

    move-object/from16 v0, v17

    iget v2, v0, Lbum;->e:I

    int-to-long v8, v2

    invoke-direct/range {v3 .. v9}, Lboh;-><init>(Ljava/lang/String;Ljava/lang/String;JJ)V

    .line 103
    move-object/from16 v0, v17

    iget v2, v0, Lbum;->e:I

    invoke-virtual {v14, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbmo;

    .line 104
    if-nez v2, :cond_5

    .line 105
    const-string v2, "genreId"

    move-object/from16 v0, v17

    iget v4, v0, Lbum;->e:I

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0xb

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 106
    invoke-virtual {v3}, Lboh;->toString()Ljava/lang/String;

    move-result-object v3

    .line 105
    invoke-static {v2, v4, v3}, Lbrg;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lbrg;

    move-result-object v2

    throw v2

    .line 108
    :cond_5
    invoke-interface {v13, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 109
    move-object/from16 v0, p0

    iget-object v2, v0, Lbre;->d:Landroid/util/LongSparseArray;

    invoke-virtual {v2, v6, v7, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    .line 98
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_3

    .line 113
    :cond_6
    new-instance v5, Ljava/util/EnumMap;

    const-class v2, Lbza;

    invoke-direct {v5, v2}, Ljava/util/EnumMap;-><init>(Ljava/lang/Class;)V

    .line 115
    move-object/from16 v0, p1

    iget-object v6, v0, Lbuk;->d:[Lbul;

    .line 116
    array-length v7, v6

    const/4 v2, 0x0

    move v4, v2

    :goto_4
    if-ge v4, v7, :cond_a

    aget-object v8, v6, v4

    .line 117
    iget-object v2, v8, Lbul;->b:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 118
    new-instance v2, Lbrg;

    const-string v3, "Missing theme type"

    invoke-direct {v2, v3}, Lbrg;-><init>(Ljava/lang/String;)V

    throw v2

    .line 126
    :cond_7
    :try_start_0
    iget-object v2, v8, Lbul;->b:Ljava/lang/String;

    invoke-static {v2}, Lbza;->valueOf(Ljava/lang/String;)Lbza;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    move-object v3, v2

    .line 134
    :goto_5
    if-eqz v3, :cond_9

    .line 135
    new-instance v9, Ljava/util/ArrayList;

    iget-object v2, v8, Lbul;->c:[J

    array-length v2, v2

    invoke-direct {v9, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 138
    iget-object v8, v8, Lbul;->c:[J

    array-length v10, v8

    const/4 v2, 0x0

    :goto_6
    if-ge v2, v10, :cond_8

    aget-wide v14, v8, v2

    .line 139
    invoke-static {v11, v14, v15}, Lbre;->a(IJ)J

    move-result-wide v14

    .line 140
    move-object/from16 v0, p0

    iget-object v0, v0, Lbre;->d:Landroid/util/LongSparseArray;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v14, v15}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v14

    invoke-interface {v9, v14}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 138
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 128
    :catch_0
    move-exception v2

    const/4 v2, 0x0

    move-object v3, v2

    goto :goto_5

    .line 142
    :cond_8
    invoke-static {v9}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v5, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    :cond_9
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_4

    .line 148
    :cond_a
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Lcfm;->a(I)Ljava/util/HashMap;

    move-result-object v4

    .line 149
    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_7
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lbmo;

    .line 151
    invoke-interface {v13, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 150
    invoke-interface {v4, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_7

    .line 153
    :cond_b
    invoke-static {v12}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lbre;->c:Ljava/util/List;

    .line 154
    invoke-static {v4}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lbre;->e:Ljava/util/Map;

    .line 155
    invoke-static {v5}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lbre;->f:Ljava/util/Map;

    .line 163
    return-void
.end method

.method private static a(IJ)J
    .locals 3

    .prologue
    .line 240
    if-nez p0, :cond_0

    .line 243
    :goto_0
    return-wide p1

    .line 242
    :cond_0
    const/4 v0, 0x1

    if-ne p0, v0, :cond_1

    .line 243
    const-wide v0, 0x2ba9057979c4c891L    # 2.2879301890676542E-98

    mul-long/2addr p1, v0

    goto :goto_0

    .line 245
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x27

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "Unsupported encryption type "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public a(J)Lboh;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lbre;->d:Landroid/util/LongSparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboh;

    return-object v0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lbre;->b:Ljava/lang/String;

    return-object v0
.end method

.method public a(Lbmo;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbmo;",
            ")",
            "Ljava/util/List",
            "<",
            "Lboh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 192
    iget-object v0, p0, Lbre;->e:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    return-object v0
.end method

.method public a(Lbza;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbza;",
            ")",
            "Ljava/util/List",
            "<",
            "Lboh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 206
    iget-object v0, p0, Lbre;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 207
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lbre;->d:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbmo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 199
    iget-object v0, p0, Lbre;->c:Ljava/util/List;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 212
    if-ne p1, p0, :cond_1

    .line 222
    :cond_0
    :goto_0
    return v0

    .line 215
    :cond_1
    instance-of v2, p1, Lbre;

    if-nez v2, :cond_2

    move v0, v1

    .line 216
    goto :goto_0

    .line 218
    :cond_2
    check-cast p1, Lbre;

    .line 221
    iget-object v2, p0, Lbre;->e:Ljava/util/Map;

    iget-object v3, p1, Lbre;->e:Ljava/util/Map;

    invoke-static {v2, v3}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lbre;->f:Ljava/util/Map;

    iget-object v3, p1, Lbre;->f:Ljava/util/Map;

    .line 222
    invoke-static {v2, v3}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 229
    iget-object v0, p0, Lbre;->e:Ljava/util/Map;

    iget-object v1, p0, Lbre;->f:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method
