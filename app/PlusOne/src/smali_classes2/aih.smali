.class final Laih;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lahy;


# instance fields
.field private final a:Ljava/util/concurrent/CountDownLatch;

.field private final b:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Lbkr;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/concurrent/atomic/AtomicReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/atomic/AtomicReference",
            "<",
            "Ljava/lang/Exception;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final e:Landroid/net/Uri;

.field private final f:Lail;


# direct methods
.method constructor <init>(Landroid/net/Uri;Lail;)V
    .locals 2

    .prologue
    .line 436
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 425
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Laih;->a:Ljava/util/concurrent/CountDownLatch;

    .line 426
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Laih;->b:Ljava/util/concurrent/atomic/AtomicReference;

    .line 428
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Laih;->c:Ljava/util/concurrent/atomic/AtomicReference;

    .line 430
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Laih;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 437
    iput-object p1, p0, Laih;->e:Landroid/net/Uri;

    .line 438
    iput-object p2, p0, Laih;->f:Lail;

    .line 439
    return-void
.end method


# virtual methods
.method public a()Lbkr;
    .locals 3

    .prologue
    .line 489
    iget-object v0, p0, Laih;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "mMetrics"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 490
    iget-object v0, p0, Laih;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbkr;

    return-object v0
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Laih;->f:Lail;

    if-eqz v0, :cond_0

    .line 473
    iget-object v0, p0, Laih;->f:Lail;

    invoke-interface {v0, p1}, Lail;->a(Landroid/graphics/Bitmap;)V

    .line 475
    :cond_0
    return-void
.end method

.method public a(Landroid/net/Uri;J)V
    .locals 2

    .prologue
    .line 465
    iget-object v0, p0, Laih;->f:Lail;

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Laih;->f:Lail;

    invoke-interface {v0, p2, p3}, Lail;->a(J)V

    .line 468
    :cond_0
    return-void
.end method

.method public a(Landroid/net/Uri;Laic;Lbkr;)V
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Laih;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 445
    iget-object v0, p0, Laih;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 446
    return-void
.end method

.method public a(Landroid/net/Uri;Laic;Ljava/lang/Exception;)V
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Laih;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 460
    iget-object v0, p0, Laih;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 461
    return-void
.end method

.method public a(Landroid/net/Uri;Lbkr;)V
    .locals 2

    .prologue
    .line 479
    iget-object v0, p0, Laih;->f:Lail;

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Laih;->f:Lail;

    iget-object v1, p0, Laih;->e:Landroid/net/Uri;

    invoke-interface {v0, v1, p2}, Lail;->a(Landroid/net/Uri;Lbkr;)V

    .line 482
    :cond_0
    return-void
.end method

.method public b(Landroid/net/Uri;Laic;Lbkr;)V
    .locals 2

    .prologue
    .line 451
    iget-object v0, p0, Laih;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 452
    iget-object v0, p0, Laih;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 453
    iget-object v0, p0, Laih;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 454
    return-void
.end method

.method public b()Z
    .locals 3

    .prologue
    .line 499
    iget-object v0, p0, Laih;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    const-string v1, "mMetrics"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 500
    iget-object v0, p0, Laih;->d:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 510
    :try_start_0
    iget-object v0, p0, Laih;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 518
    iget-object v0, p0, Laih;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 519
    new-instance v1, Ljava/util/concurrent/ExecutionException;

    iget-object v0, p0, Laih;->c:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    invoke-direct {v1, v0}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 511
    :catch_0
    move-exception v0

    .line 514
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    .line 515
    new-instance v1, Ljava/util/concurrent/ExecutionException;

    invoke-direct {v1, v0}, Ljava/util/concurrent/ExecutionException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 521
    :cond_0
    return-void
.end method
