.class public final Lfrq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lfrt;


# instance fields
.field private a:I

.field private b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lfrs;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Lfrr;

.field private final d:[[Lhng;

.field private final e:[[I

.field private final f:[I

.field private final g:[I

.field private final h:[I

.field private final i:[I

.field private final j:[[Lhng;

.field private final k:I

.field private final l:I

.field private final m:[Landroid/graphics/Point;

.field private n:I

.field private o:I

.field private p:I

.field private final q:I

.field private r:Z

.field private s:I

.field private t:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private u:I

.field private v:I

.field private w:I

.field private x:J

.field private y:I


# direct methods
.method public constructor <init>(Landroid/content/Context;[[F[[Landroid/graphics/Point;[[Lhng;II)V
    .locals 8

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lfrq;->a:I

    .line 100
    const/4 v0, -0x1

    iput v0, p0, Lfrq;->s:I

    .line 104
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfrq;->t:Ljava/util/Set;

    .line 118
    const/4 v0, 0x0

    iput v0, p0, Lfrq;->y:I

    .line 153
    array-length v0, p2

    array-length v1, p3

    if-ne v0, v1, :cond_0

    array-length v0, p2

    array-length v1, p4

    if-eq v0, v1, :cond_1

    .line 155
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Parameters velocityScaleFactors, velocityScaleFactors, and planeScrollPositions must represent the same number of planes."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_1
    array-length v0, p3

    iput v0, p0, Lfrq;->k:I

    .line 160
    const/4 v0, 0x0

    aget-object v0, p3, v0

    array-length v0, v0

    iput v0, p0, Lfrq;->l:I

    .line 162
    iput-object p4, p0, Lfrq;->d:[[Lhng;

    .line 163
    iput p5, p0, Lfrq;->v:I

    .line 164
    iput p6, p0, Lfrq;->w:I

    .line 165
    new-instance v0, Lfrr;

    invoke-direct {v0, p1}, Lfrr;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfrq;->c:Lfrr;

    .line 167
    iget v0, p0, Lfrq;->v:I

    int-to-float v0, v0

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    float-to-int v2, v0

    .line 168
    iget v0, p0, Lfrq;->k:I

    iget v1, p0, Lfrq;->l:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, p0, Lfrq;->e:[[I

    .line 169
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lfrq;->k:I

    if-ge v0, v1, :cond_3

    .line 170
    const/4 v1, 0x0

    :goto_1
    iget v3, p0, Lfrq;->l:I

    if-ge v1, v3, :cond_2

    .line 175
    iget-object v3, p0, Lfrq;->e:[[I

    aget-object v3, v3, v0

    aget-object v4, p3, v0

    aget-object v4, v4, v1

    iget v4, v4, Landroid/graphics/Point;->x:I

    sub-int/2addr v4, v2

    aput v4, v3, v1

    .line 170
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 169
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 180
    :cond_3
    iget v0, p0, Lfrq;->l:I

    new-array v0, v0, [I

    iput-object v0, p0, Lfrq;->f:[I

    .line 181
    iget-object v0, p0, Lfrq;->f:[I

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput v2, v0, v1

    .line 182
    iget v0, p0, Lfrq;->k:I

    iget v1, p0, Lfrq;->l:I

    filled-new-array {v0, v1}, [I

    move-result-object v0

    const-class v1, Lhng;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Lhng;

    iput-object v0, p0, Lfrq;->j:[[Lhng;

    .line 183
    const/4 v0, 0x0

    :goto_2
    iget v1, p0, Lfrq;->l:I

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_7

    .line 188
    iget v1, p0, Lfrq;->k:I

    new-array v3, v1, [I

    .line 189
    const/4 v2, 0x0

    .line 190
    const/4 v1, 0x0

    :goto_3
    iget v4, p0, Lfrq;->k:I

    if-ge v1, v4, :cond_5

    .line 191
    aget-object v4, p3, v1

    add-int/lit8 v5, v0, 0x1

    aget-object v4, v4, v5

    iget v4, v4, Landroid/graphics/Point;->x:I

    aget-object v5, p3, v1

    aget-object v5, v5, v0

    iget v5, v5, Landroid/graphics/Point;->x:I

    sub-int/2addr v4, v5

    .line 193
    int-to-float v4, v4

    aget-object v5, p2, v1

    aget v5, v5, v0

    div-float/2addr v4, v5

    .line 194
    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    aput v4, v3, v1

    .line 195
    aget v4, v3, v1

    if-le v4, v2, :cond_4

    .line 196
    aget v2, v3, v1

    .line 190
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 200
    :cond_5
    const/4 v1, 0x0

    :goto_4
    iget v4, p0, Lfrq;->k:I

    if-ge v1, v4, :cond_6

    .line 217
    aget v4, v3, v1

    int-to-float v4, v4

    int-to-float v5, v2

    div-float/2addr v4, v5

    .line 218
    aget-object v5, p2, v1

    aget v5, v5, v0

    mul-float/2addr v5, v4

    .line 219
    new-instance v6, Lhng;

    invoke-direct {v6}, Lhng;-><init>()V

    .line 220
    const/4 v7, 0x0

    iput v7, v6, Lhng;->a:F

    .line 221
    const/4 v7, 0x0

    iput v7, v6, Lhng;->b:F

    .line 222
    iput v4, v6, Lhng;->c:F

    .line 223
    iput v5, v6, Lhng;->d:F

    .line 224
    iput v4, v6, Lhng;->e:F

    .line 225
    iput v5, v6, Lhng;->f:F

    .line 226
    const/high16 v4, 0x3f800000    # 1.0f

    iput v4, v6, Lhng;->g:F

    .line 227
    iput v5, v6, Lhng;->h:F

    .line 228
    iget-object v4, p0, Lfrq;->j:[[Lhng;

    aget-object v4, v4, v1

    aput-object v6, v4, v0

    .line 200
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 232
    :cond_6
    iget-object v1, p0, Lfrq;->f:[I

    add-int/lit8 v3, v0, 0x1

    iget-object v4, p0, Lfrq;->f:[I

    aget v4, v4, v0

    add-int/2addr v2, v4

    aput v2, v1, v3

    .line 183
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 235
    :cond_7
    iget-object v0, p0, Lfrq;->f:[I

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Lfrq;->g:[I

    .line 236
    iget-object v0, p0, Lfrq;->f:[I

    array-length v0, v0

    new-array v0, v0, [I

    iput-object v0, p0, Lfrq;->h:[I

    .line 238
    iget-object v0, p0, Lfrq;->g:[I

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput v2, v0, v1

    .line 239
    const/4 v0, 0x0

    :goto_5
    iget-object v1, p0, Lfrq;->f:[I

    array-length v1, v1

    if-ge v0, v1, :cond_a

    .line 240
    if-eqz v0, :cond_8

    .line 241
    iget-object v1, p0, Lfrq;->g:[I

    iget-object v2, p0, Lfrq;->f:[I

    aget v2, v2, v0

    int-to-float v2, v2

    const v3, 0x3f19999a    # 0.6f

    iget-object v4, p0, Lfrq;->f:[I

    aget v4, v4, v0

    iget-object v5, p0, Lfrq;->f:[I

    add-int/lit8 v6, v0, -0x1

    aget v5, v5, v6

    sub-int/2addr v4, v5

    int-to-float v4, v4

    mul-float/2addr v3, v4

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    aput v2, v1, v0

    .line 245
    :cond_8
    iget-object v1, p0, Lfrq;->f:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-eq v0, v1, :cond_9

    .line 246
    iget-object v1, p0, Lfrq;->h:[I

    iget-object v2, p0, Lfrq;->f:[I

    aget v2, v2, v0

    int-to-float v2, v2

    const v3, 0x3f19999a    # 0.6f

    iget-object v4, p0, Lfrq;->f:[I

    add-int/lit8 v5, v0, 0x1

    aget v4, v4, v5

    iget-object v5, p0, Lfrq;->f:[I

    aget v5, v5, v0

    sub-int/2addr v4, v5

    int-to-float v4, v4

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v2

    aput v2, v1, v0

    .line 239
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 250
    :cond_a
    iget-object v0, p0, Lfrq;->h:[I

    iget-object v1, p0, Lfrq;->f:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    iget-object v2, p0, Lfrq;->f:[I

    iget-object v3, p0, Lfrq;->f:[I

    array-length v3, v3

    add-int/lit8 v3, v3, -0x1

    aget v2, v2, v3

    aput v2, v0, v1

    .line 253
    iget-object v0, p0, Lfrq;->f:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    new-array v0, v0, [I

    iput-object v0, p0, Lfrq;->i:[I

    .line 254
    const/4 v0, 0x0

    :goto_6
    iget-object v1, p0, Lfrq;->i:[I

    array-length v1, v1

    if-ge v0, v1, :cond_b

    .line 255
    iget-object v1, p0, Lfrq;->i:[I

    iget-object v2, p0, Lfrq;->f:[I

    aget v2, v2, v0

    iget-object v3, p0, Lfrq;->f:[I

    add-int/lit8 v4, v0, 0x1

    aget v3, v3, v4

    iget-object v4, p0, Lfrq;->f:[I

    aget v4, v4, v0

    sub-int/2addr v3, v4

    div-int/lit8 v3, v3, 0x2

    add-int/2addr v2, v3

    aput v2, v1, v0

    .line 254
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 260
    :cond_b
    iget-object v0, p0, Lfrq;->f:[I

    iget v1, p0, Lfrq;->l:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    iput v0, p0, Lfrq;->q:I

    .line 262
    iget-object v0, p0, Lfrq;->c:Lfrr;

    iget-object v1, p0, Lfrq;->f:[I

    invoke-virtual {v0, v1}, Lfrr;->a([I)V

    .line 264
    iget v0, p0, Lfrq;->k:I

    new-array v0, v0, [Landroid/graphics/Point;

    iput-object v0, p0, Lfrq;->m:[Landroid/graphics/Point;

    .line 265
    const/4 v0, 0x0

    :goto_7
    iget v1, p0, Lfrq;->k:I

    if-ge v0, v1, :cond_c

    .line 266
    iget-object v1, p0, Lfrq;->m:[Landroid/graphics/Point;

    new-instance v2, Landroid/graphics/Point;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Landroid/graphics/Point;-><init>(II)V

    aput-object v2, v1, v0

    .line 265
    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    .line 269
    :cond_c
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lfrq;->b:Ljava/util/Set;

    .line 270
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfrq;->r:Z

    .line 272
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lfrq;->e(I)Z

    .line 273
    return-void
.end method

.method private e(I)Z
    .locals 13

    .prologue
    const/high16 v12, 0x40000000    # 2.0f

    const-wide v10, 0x3e012e0be826d695L    # 5.0E-10

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 521
    .line 522
    iput p1, p0, Lfrq;->n:I

    .line 524
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    .line 525
    iget v0, p0, Lfrq;->n:I

    iget v3, p0, Lfrq;->o:I

    if-eq v0, v3, :cond_a

    .line 527
    iget v0, p0, Lfrq;->n:I

    iget v3, p0, Lfrq;->o:I

    sub-int/2addr v0, v3

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    int-to-float v0, v0

    iget v3, p0, Lfrq;->v:I

    int-to-float v3, v3

    div-float/2addr v0, v3

    iget-wide v4, p0, Lfrq;->x:J

    sub-long v4, v6, v4

    long-to-float v3, v4

    div-float v5, v0, v3

    .line 529
    iget-object v0, p0, Lfrq;->t:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget v0, p0, Lfrq;->n:I

    iget-object v8, p0, Lfrq;->g:[I

    aget v8, v8, v4

    if-lt v0, v8, :cond_1

    iget v0, p0, Lfrq;->n:I

    iget-object v8, p0, Lfrq;->h:[I

    aget v8, v8, v4

    if-le v0, v8, :cond_0

    :cond_1
    iget-object v0, p0, Lfrq;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrs;

    invoke-interface {v0, v4}, Lfrs;->b(I)V

    goto :goto_1

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_3
    iget v0, p0, Lfrq;->n:I

    iget v3, p0, Lfrq;->o:I

    sub-int/2addr v0, v3

    if-lez v0, :cond_b

    move v0, v1

    :goto_2
    move v3, v2

    :goto_3
    iget-object v4, p0, Lfrq;->f:[I

    array-length v4, v4

    if-ge v3, v4, :cond_1d

    if-eqz v0, :cond_c

    move v4, v3

    :goto_4
    if-eqz v0, :cond_4

    iget v8, p0, Lfrq;->n:I

    iget-object v9, p0, Lfrq;->g:[I

    aget v9, v9, v4

    if-lt v8, v9, :cond_d

    iget v8, p0, Lfrq;->n:I

    iget-object v9, p0, Lfrq;->f:[I

    aget v9, v9, v4

    if-gt v8, v9, :cond_d

    :cond_4
    if-nez v0, :cond_5

    iget v8, p0, Lfrq;->n:I

    iget-object v9, p0, Lfrq;->h:[I

    aget v9, v9, v4

    if-gt v8, v9, :cond_d

    iget v8, p0, Lfrq;->n:I

    iget-object v9, p0, Lfrq;->f:[I

    aget v9, v9, v4

    if-lt v8, v9, :cond_d

    :cond_5
    iget-boolean v8, p0, Lfrq;->r:Z

    if-eqz v8, :cond_6

    iget v8, p0, Lfrq;->s:I

    if-eq v8, v4, :cond_d

    :cond_6
    float-to-double v8, v5

    cmpg-double v0, v8, v10

    if-gez v0, :cond_7

    invoke-virtual {p0}, Lfrq;->f()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-direct {p0, v4}, Lfrq;->f(I)V

    :cond_7
    move v0, v1

    :goto_5
    if-nez v0, :cond_8

    const/4 v0, -0x1

    iput v0, p0, Lfrq;->s:I

    iput-boolean v2, p0, Lfrq;->r:Z

    :cond_8
    move v0, v2

    .line 530
    :goto_6
    iget-object v3, p0, Lfrq;->f:[I

    array-length v3, v3

    if-ge v0, v3, :cond_1c

    float-to-double v8, v5

    cmpg-double v3, v8, v10

    if-gez v3, :cond_e

    iget v3, p0, Lfrq;->n:I

    iget-object v4, p0, Lfrq;->f:[I

    aget v4, v4, v0

    if-ne v3, v4, :cond_e

    iget-object v3, p0, Lfrq;->f:[I

    aget v0, v3, v0

    iput v0, p0, Lfrq;->u:I

    move v0, v1

    :goto_7
    if-eqz v0, :cond_f

    move v0, v2

    move-object v3, p0

    :goto_8
    iput v0, v3, Lfrq;->y:I

    .line 532
    :cond_9
    iput-wide v6, p0, Lfrq;->x:J

    .line 533
    iget v0, p0, Lfrq;->n:I

    iput v0, p0, Lfrq;->o:I

    .line 536
    :cond_a
    :goto_9
    iget v0, p0, Lfrq;->p:I

    iget-object v3, p0, Lfrq;->i:[I

    array-length v3, v3

    if-ge v0, v3, :cond_11

    iget-object v0, p0, Lfrq;->i:[I

    iget v3, p0, Lfrq;->p:I

    aget v0, v0, v3

    if-lt p1, v0, :cond_11

    .line 538
    iget v0, p0, Lfrq;->p:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lfrq;->p:I

    goto :goto_9

    :cond_b
    move v0, v2

    .line 529
    goto/16 :goto_2

    :cond_c
    iget-object v4, p0, Lfrq;->f:[I

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v4, v3

    goto/16 :goto_4

    :cond_d
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_3

    .line 530
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_f
    iget v0, p0, Lfrq;->y:I

    if-nez v0, :cond_9

    iget v0, p0, Lfrq;->n:I

    iget v3, p0, Lfrq;->u:I

    if-le v0, v3, :cond_10

    move v0, v1

    move-object v3, p0

    goto :goto_8

    :cond_10
    const/4 v0, 0x2

    move-object v3, p0

    goto :goto_8

    .line 541
    :cond_11
    :goto_a
    iget v0, p0, Lfrq;->p:I

    if-lez v0, :cond_12

    iget-object v0, p0, Lfrq;->i:[I

    iget v3, p0, Lfrq;->p:I

    add-int/lit8 v3, v3, -0x1

    aget v0, v0, v3

    if-ge p1, v0, :cond_12

    .line 543
    iget v0, p0, Lfrq;->p:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lfrq;->p:I

    goto :goto_a

    :cond_12
    move v0, v2

    move v3, v2

    .line 546
    :goto_b
    iget v4, p0, Lfrq;->k:I

    if-ge v0, v4, :cond_1a

    .line 547
    iget-object v4, p0, Lfrq;->e:[[I

    aget-object v6, v4, v0

    move v4, v2

    move v5, v2

    :goto_c
    iget-object v7, p0, Lfrq;->f:[I

    array-length v7, v7

    if-ge v4, v7, :cond_14

    iget-object v7, p0, Lfrq;->f:[I

    aget v7, v7, v4

    if-le p1, v7, :cond_13

    move v5, v4

    :cond_13
    add-int/lit8 v4, v4, 0x1

    goto :goto_c

    :cond_14
    add-int/lit8 v4, v5, 0x1

    iget-object v7, p0, Lfrq;->f:[I

    aget v7, v7, v5

    iget-object v8, p0, Lfrq;->f:[I

    aget v8, v8, v4

    sub-int v9, v8, v7

    int-to-float v9, v9

    iget-object v10, p0, Lfrq;->j:[[Lhng;

    aget-object v10, v10, v0

    aget-object v10, v10, v5

    iget v11, p0, Lfrq;->y:I

    packed-switch v11, :pswitch_data_0

    sub-int v4, p1, v7

    int-to-float v4, v4

    div-float/2addr v4, v9

    invoke-virtual {v10, v4}, Lhng;->c(F)F

    move-result v4

    invoke-virtual {v10, v4}, Lhng;->b(F)F

    move-result v4

    aget v5, v6, v5

    int-to-float v5, v5

    mul-float/2addr v4, v9

    add-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    .line 548
    :goto_d
    iget-object v5, p0, Lfrq;->m:[Landroid/graphics/Point;

    aget-object v5, v5, v0

    iget v5, v5, Landroid/graphics/Point;->x:I

    if-eq v4, v5, :cond_16

    .line 549
    iget-object v3, p0, Lfrq;->m:[Landroid/graphics/Point;

    aget-object v3, v3, v0

    iput v4, v3, Landroid/graphics/Point;->x:I

    .line 550
    iget-object v3, p0, Lfrq;->m:[Landroid/graphics/Point;

    aget-object v5, v3, v0

    iget-object v3, p0, Lfrq;->m:[Landroid/graphics/Point;

    aget-object v3, v3, v0

    iget v3, v3, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    iget v4, p0, Lfrq;->v:I

    int-to-float v4, v4

    div-float/2addr v4, v12

    add-float v6, v3, v4

    const/4 v3, 0x0

    iget-object v4, p0, Lfrq;->d:[[Lhng;

    aget-object v4, v4, v0

    array-length v4, v4

    iget-object v7, p0, Lfrq;->d:[[Lhng;

    aget-object v7, v7, v0

    aget-object v7, v7, v2

    iget v7, v7, Lhng;->a:F

    cmpg-float v7, v6, v7

    if-gtz v7, :cond_17

    iget-object v3, p0, Lfrq;->d:[[Lhng;

    aget-object v3, v3, v0

    aget-object v3, v3, v2

    iget v3, v3, Lhng;->b:F

    :cond_15
    :goto_e
    iget v4, p0, Lfrq;->w:I

    int-to-float v4, v4

    div-float/2addr v4, v12

    sub-float/2addr v3, v4

    float-to-int v3, v3

    iput v3, v5, Landroid/graphics/Point;->y:I

    move v3, v1

    .line 546
    :cond_16
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_b

    .line 547
    :pswitch_0
    sub-int v5, v8, p1

    int-to-float v5, v5

    div-float/2addr v5, v9

    invoke-virtual {v10, v5}, Lhng;->c(F)F

    move-result v5

    invoke-virtual {v10, v5}, Lhng;->b(F)F

    move-result v5

    aget v4, v6, v4

    int-to-float v4, v4

    mul-float/2addr v5, v9

    sub-float/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Math;->round(F)I

    move-result v4

    goto :goto_d

    .line 550
    :cond_17
    iget-object v7, p0, Lfrq;->d:[[Lhng;

    aget-object v7, v7, v0

    add-int/lit8 v8, v4, -0x1

    aget-object v7, v7, v8

    iget v7, v7, Lhng;->g:F

    cmpl-float v7, v6, v7

    if-ltz v7, :cond_18

    iget-object v3, p0, Lfrq;->d:[[Lhng;

    aget-object v3, v3, v0

    add-int/lit8 v4, v4, -0x1

    aget-object v3, v3, v4

    iget v3, v3, Lhng;->h:F

    goto :goto_e

    :cond_18
    iget-object v4, p0, Lfrq;->d:[[Lhng;

    aget-object v7, v4, v0

    array-length v8, v7

    move v4, v2

    :goto_f
    if-ge v4, v8, :cond_15

    aget-object v9, v7, v4

    iget v10, v9, Lhng;->a:F

    cmpl-float v10, v6, v10

    if-ltz v10, :cond_19

    iget v10, v9, Lhng;->g:F

    cmpg-float v10, v6, v10

    if-gtz v10, :cond_19

    invoke-virtual {v9, v6}, Lhng;->c(F)F

    move-result v3

    invoke-virtual {v9, v3}, Lhng;->b(F)F

    move-result v3

    goto :goto_e

    :cond_19
    add-int/lit8 v4, v4, 0x1

    goto :goto_f

    .line 555
    :cond_1a
    if-eqz v3, :cond_1b

    iget-object v0, p0, Lfrq;->b:Ljava/util/Set;

    if-eqz v0, :cond_1b

    .line 556
    iget-object v0, p0, Lfrq;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_10
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrs;

    .line 557
    invoke-interface {v0, p0}, Lfrs;->a(Lfrt;)V

    goto :goto_10

    .line 561
    :cond_1b
    return v3

    :cond_1c
    move v0, v2

    goto/16 :goto_7

    :cond_1d
    move v0, v2

    goto/16 :goto_5

    .line 547
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private f(I)V
    .locals 2

    .prologue
    .line 617
    iget-object v0, p0, Lfrq;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 618
    iget-object v0, p0, Lfrq;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfrs;

    .line 619
    invoke-interface {v0, p1}, Lfrs;->a(I)V

    goto :goto_0

    .line 622
    :cond_0
    iput p1, p0, Lfrq;->s:I

    .line 623
    iget-object v0, p0, Lfrq;->t:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 624
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfrq;->r:Z

    .line 625
    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lfrq;->m:[Landroid/graphics/Point;

    aget-object v0, v0, p1

    iget v0, v0, Landroid/graphics/Point;->y:I

    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 276
    const/4 v0, 0x0

    iput v0, p0, Lfrq;->a:I

    .line 277
    return-void
.end method

.method public a(II)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/high16 v2, 0x40000000    # 2.0f

    .line 323
    iget v0, p0, Lfrq;->v:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lfrq;->w:I

    if-ne p2, v0, :cond_0

    .line 344
    :goto_0
    return-void

    .line 327
    :cond_0
    iget v0, p0, Lfrq;->v:I

    int-to-float v0, v0

    div-float/2addr v0, v2

    float-to-int v3, v0

    .line 328
    iput p1, p0, Lfrq;->v:I

    .line 329
    iput p2, p0, Lfrq;->w:I

    .line 330
    iget v0, p0, Lfrq;->v:I

    int-to-float v0, v0

    div-float/2addr v0, v2

    float-to-int v4, v0

    move v0, v1

    .line 332
    :goto_1
    iget v2, p0, Lfrq;->k:I

    if-ge v0, v2, :cond_2

    move v2, v1

    .line 333
    :goto_2
    iget v5, p0, Lfrq;->l:I

    if-ge v2, v5, :cond_1

    .line 338
    iget-object v5, p0, Lfrq;->e:[[I

    aget-object v5, v5, v0

    iget-object v6, p0, Lfrq;->e:[[I

    aget-object v6, v6, v0

    aget v6, v6, v2

    add-int/2addr v6, v3

    sub-int/2addr v6, v4

    aput v6, v5, v2

    .line 333
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 332
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 343
    :cond_2
    iget v0, p0, Lfrq;->n:I

    invoke-direct {p0, v0}, Lfrq;->e(I)Z

    goto :goto_0
.end method

.method public a(IIIIIIII)V
    .locals 9

    .prologue
    .line 436
    iget-object v0, p0, Lfrq;->c:Lfrr;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-virtual/range {v0 .. v8}, Lfrr;->fling(IIIIIIII)V

    .line 437
    invoke-virtual {p0}, Lfrq;->c()V

    .line 438
    return-void
.end method

.method public a(Lfrs;)V
    .locals 1

    .prologue
    .line 315
    iget-object v0, p0, Lfrq;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 316
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 445
    iget-object v0, p0, Lfrq;->c:Lfrr;

    iget v1, p0, Lfrq;->n:I

    invoke-static {v0, v1, p1}, Lfrr;->a(Lfrr;IZ)V

    .line 446
    invoke-virtual {p0}, Lfrq;->c()V

    .line 447
    return-void
.end method

.method public b(I)I
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lfrq;->m:[Landroid/graphics/Point;

    aget-object v0, v0, p1

    iget v0, v0, Landroid/graphics/Point;->x:I

    return v0
.end method

.method public b(Z)V
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lfrq;->f:[I

    array-length v0, v0

    if-nez v0, :cond_0

    .line 459
    :goto_0
    return-void

    .line 457
    :cond_0
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 458
    :goto_1
    invoke-direct {p0, v0}, Lfrq;->f(I)V

    goto :goto_0

    .line 457
    :cond_1
    iget-object v0, p0, Lfrq;->f:[I

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 280
    iget v0, p0, Lfrq;->a:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 284
    const/4 v0, 0x3

    iput v0, p0, Lfrq;->a:I

    .line 285
    return-void
.end method

.method public c(I)V
    .locals 0

    .prologue
    .line 408
    invoke-direct {p0, p1}, Lfrq;->e(I)Z

    .line 409
    return-void
.end method

.method public d(I)V
    .locals 1

    .prologue
    .line 412
    iget-object v0, p0, Lfrq;->f:[I

    aget v0, v0, p1

    invoke-virtual {p0, v0}, Lfrq;->c(I)V

    .line 413
    return-void
.end method

.method public d()Z
    .locals 2

    .prologue
    .line 288
    iget v0, p0, Lfrq;->a:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 292
    const/4 v0, 0x2

    iput v0, p0, Lfrq;->a:I

    .line 293
    return-void
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 296
    iget v0, p0, Lfrq;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 301
    invoke-virtual {p0}, Lfrq;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 302
    invoke-virtual {p0}, Lfrq;->h()Z

    .line 304
    invoke-virtual {p0}, Lfrq;->n()Z

    move-result v0

    if-nez v0, :cond_0

    .line 305
    invoke-virtual {p0}, Lfrq;->a()V

    .line 307
    :cond_0
    const/4 v0, 0x1

    .line 311
    :goto_0
    return v0

    .line 308
    :cond_1
    invoke-virtual {p0}, Lfrq;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 309
    invoke-virtual {p0}, Lfrq;->a()V

    .line 311
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lfrq;->c:Lfrr;

    invoke-virtual {v0}, Lfrr;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lfrq;->c:Lfrr;

    invoke-virtual {v0}, Lfrr;->getCurrX()I

    move-result v0

    invoke-direct {p0, v0}, Lfrq;->e(I)Z

    move-result v0

    .line 358
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 373
    iget v0, p0, Lfrq;->k:I

    return v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 382
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lfrq;->b(I)I

    move-result v0

    return v0
.end method

.method public k()I
    .locals 1

    .prologue
    .line 391
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lfrq;->a(I)I

    move-result v0

    return v0
.end method

.method public l()I
    .locals 1

    .prologue
    .line 400
    iget v0, p0, Lfrq;->n:I

    return v0
.end method

.method public m()V
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lfrq;->c:Lfrr;

    invoke-virtual {v0}, Lfrr;->abortAnimation()V

    .line 466
    return-void
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lfrq;->c:Lfrr;

    invoke-virtual {v0}, Lfrr;->isFinished()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()I
    .locals 1

    .prologue
    .line 479
    iget v0, p0, Lfrq;->q:I

    return v0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 483
    iget v0, p0, Lfrq;->p:I

    return v0
.end method
