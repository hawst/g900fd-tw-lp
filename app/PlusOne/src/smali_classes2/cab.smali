.class public final Lcab;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcac;


# instance fields
.field private final a:J

.field private final b:F

.field private final c:F


# direct methods
.method public constructor <init>(J)V
    .locals 3

    .prologue
    .line 28
    const/4 v0, 0x0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, p1, p2, v0, v1}, Lcab;-><init>(JFF)V

    .line 29
    return-void
.end method

.method public constructor <init>(JFF)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    iput-wide p1, p0, Lcab;->a:J

    .line 33
    iput p3, p0, Lcab;->b:F

    .line 34
    iput p4, p0, Lcab;->c:F

    .line 35
    return-void
.end method


# virtual methods
.method public a(J)F
    .locals 5

    .prologue
    .line 54
    iget v0, p0, Lcab;->b:F

    long-to-float v1, p1

    iget v2, p0, Lcab;->c:F

    iget v3, p0, Lcab;->b:F

    sub-float/2addr v2, v3

    mul-float/2addr v1, v2

    iget-wide v2, p0, Lcab;->a:J

    long-to-float v2, v2

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    return v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 39
    iget-wide v0, p0, Lcab;->a:J

    return-wide v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 68
    if-ne p1, p0, :cond_1

    .line 75
    :cond_0
    :goto_0
    return v0

    .line 71
    :cond_1
    instance-of v2, p1, Lcab;

    if-nez v2, :cond_2

    move v0, v1

    .line 72
    goto :goto_0

    .line 74
    :cond_2
    check-cast p1, Lcab;

    .line 75
    iget v2, p0, Lcab;->b:F

    iget v3, p1, Lcab;->b:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lcab;->c:F

    iget v3, p1, Lcab;->c:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget-wide v2, p0, Lcab;->a:J

    iget-wide v4, p1, Lcab;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 82
    iget v0, p0, Lcab;->b:F

    iget v1, p0, Lcab;->c:F

    iget-wide v2, p0, Lcab;->a:J

    const/16 v4, 0x11

    .line 83
    invoke-static {v2, v3, v4}, Lbqh;->a(JI)I

    move-result v2

    invoke-static {v1, v2}, Lbqh;->a(FI)I

    move-result v1

    .line 82
    invoke-static {v0, v1}, Lbqh;->a(FI)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 88
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcab;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcab;->b:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcab;->c:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
