.class public final Lcju;
.super Lcjy;
.source "PG"


# static fields
.field private static final W:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final X:[I


# instance fields
.field private final Y:Lckf;

.field private final Z:Lcjx;

.field private aa:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 37
    const/16 v0, 0x2e

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcju;->X:[I

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 67
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/16 v3, 0xdd

    .line 69
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 67
    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 71
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcju;->W:Ljava/util/List;

    .line 72
    return-void

    .line 37
    :array_0
    .array-data 4
        0x7f0201fa
        0x7f0201f9
        0x7f0201fe
        0x7f0201fd
        0x7f020202
        0x7f020201
        0x7f0201e4
        0x7f0201e3
        0x7f0201f8
        0x7f0201f7
        0x7f0201e6
        0x7f0201e5
        0x7f0201ea
        0x7f0201e9
        0x7f0201f0
        0x7f0201ef
        0x7f0201dc
        0x7f0201db
        0x7f0201f4
        0x7f0201f3
        0x7f0201da
        0x7f0201d9
        0x7f0201fc
        0x7f0201fb
        0x7f020200
        0x7f0201ff
        0x7f020204
        0x7f020203
        0x7f0201e0
        0x7f0201df
        0x7f0201e2
        0x7f0201e1
        0x7f0201d8
        0x7f0201d7
        0x7f0201e8
        0x7f0201e7
        0x7f0201ec
        0x7f0201eb
        0x7f0201ee
        0x7f0201ed
        0x7f0201f2
        0x7f0201f1
        0x7f0201f6
        0x7f0201f5
        0x7f0201de
        0x7f0201dd
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 31
    invoke-direct {p0}, Lcjy;-><init>()V

    .line 74
    new-instance v0, Lckf;

    const/16 v1, 0xdf

    sget-object v2, Lcju;->X:[I

    invoke-direct {v0, p0, v1, v2}, Lckf;-><init>(Lcjy;I[I)V

    iput-object v0, p0, Lcju;->Y:Lckf;

    .line 76
    new-instance v0, Lcjx;

    invoke-direct {v0, p0}, Lcjx;-><init>(Lcju;)V

    iput-object v0, p0, Lcju;->Z:Lcjx;

    .line 264
    return-void
.end method

.method static synthetic a(Lcju;)Lckf;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcju;->Y:Lckf;

    return-object v0
.end method

.method static synthetic b(Lcju;)Lcjx;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcju;->Z:Lcjx;

    return-object v0
.end method

.method static synthetic c(Lcju;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcju;->aa:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    return-object v0
.end method


# virtual methods
.method protected X()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    sget-object v0, Lcju;->W:Ljava/util/List;

    return-object v0
.end method

.method public a(ILjava/lang/Object;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 157
    sparse-switch p1, :sswitch_data_0

    .line 167
    invoke-super {p0, p1, p2}, Lcjy;->b(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 159
    :sswitch_0
    const-string v0, "%s %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const v2, 0x7f0a0070

    .line 160
    invoke-virtual {p0, v2}, Lcju;->e_(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x1

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 159
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 163
    :sswitch_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0a0073

    invoke-virtual {p0, v0}, Lcju;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const v0, 0x7f0a0072

    .line 164
    invoke-virtual {p0, v0}, Lcju;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 157
    nop

    :sswitch_data_0
    .sparse-switch
        0x9 -> :sswitch_1
        0xdf -> :sswitch_0
    .end sparse-switch
.end method

.method protected a(Lckp;)V
    .locals 10

    .prologue
    const/16 v9, 0x9

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 98
    invoke-virtual {p0}, Lcju;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v7

    .line 99
    invoke-virtual {v7, v9}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 101
    const v0, 0x7f020392

    const v1, 0x7f0a0070

    .line 103
    invoke-virtual {p0, v1}, Lcju;->e_(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcjv;

    invoke-direct {v2, p0, v7}, Lcjv;-><init>(Lcju;Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;)V

    .line 101
    invoke-virtual {p1, v0, v1, v2}, Lckp;->a(ILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 114
    const v1, 0x7f020380

    const v2, 0x7f02037f

    const v0, 0x7f0a0071

    .line 118
    invoke-virtual {p0, v0}, Lcju;->e_(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcjw;

    invoke-direct {v5, p0, v7, v8}, Lcjw;-><init>(Lcju;Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;I)V

    move-object v0, p1

    .line 114
    invoke-virtual/range {v0 .. v5}, Lckp;->a(IIILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    move-result-object v0

    iput-object v0, p0, Lcju;->aa:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 130
    iget-object v1, p0, Lcju;->aa:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 131
    invoke-virtual {v7, v9}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    if-ne v0, v8, :cond_0

    move v0, v6

    .line 130
    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setSelected(Z)V

    .line 132
    iget-object v0, p0, Lcju;->aa:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    const/16 v1, 0xdf

    .line 133
    invoke-virtual {v7, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v1

    const/16 v2, 0xb

    if-ge v1, v2, :cond_1

    .line 132
    :goto_1
    invoke-virtual {v0, v6}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setEnabled(Z)V

    .line 136
    return-void

    :cond_0
    move v0, v3

    .line 131
    goto :goto_0

    :cond_1
    move v6, v3

    .line 133
    goto :goto_1
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 83
    new-instance v0, Lhmk;

    sget-object v1, Lonc;->o:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method public b(ILjava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 140
    const/16 v0, 0xdf

    if-ne p1, v0, :cond_0

    .line 141
    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcju;->g(I)Ljava/lang/String;

    move-result-object v0

    .line 143
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcjy;->b(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 88
    const/16 v0, 0x11

    return v0
.end method

.method protected e()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcmd;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x96

    const/4 v0, 0x0

    const v11, 0x3e99999a    # 0.3f

    const/4 v10, 0x0

    .line 183
    invoke-virtual {p0}, Lcju;->n()Lz;

    move-result-object v1

    .line 184
    if-nez v1, :cond_1

    .line 227
    :cond_0
    :goto_0
    return-object v0

    .line 188
    :cond_1
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v2, v1}, Lcmh;->a(Landroid/view/Window;Landroid/content/res/Resources;)Landroid/graphics/Rect;

    move-result-object v1

    .line 189
    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 194
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 195
    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 199
    invoke-virtual {p0}, Lcju;->o()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0d004b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 200
    invoke-virtual {p0}, Lcju;->o()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0d0049

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 201
    div-int/lit8 v5, v4, 0x2

    .line 204
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v6

    if-le v6, v3, :cond_2

    .line 205
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v6

    sub-int/2addr v6, v3

    div-int/lit8 v6, v6, 0x2

    invoke-virtual {v1, v6, v10}, Landroid/graphics/Rect;->inset(II)V

    .line 207
    :cond_2
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v6

    if-le v6, v3, :cond_3

    .line 208
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v6

    sub-int v3, v6, v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v1, v10, v3}, Landroid/graphics/Rect;->inset(II)V

    .line 211
    :cond_3
    invoke-virtual {p0}, Lcju;->o()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0c0002

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    .line 212
    iget v6, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v7

    add-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    .line 214
    const/16 v7, 0x3e8

    int-to-float v8, v5

    invoke-static {v7, v8}, Lcmd;->a(IF)Lcme;

    move-result-object v7

    .line 215
    int-to-long v8, v3

    invoke-virtual {v7, v8, v9}, Lcme;->a(J)Lcme;

    move-result-object v3

    int-to-float v8, v5

    mul-float/2addr v8, v11

    invoke-virtual {v3, v8}, Lcme;->a(F)Lcme;

    move-result-object v3

    iget v8, v1, Landroid/graphics/Rect;->left:I

    int-to-float v8, v8

    int-to-float v9, v6

    .line 216
    invoke-virtual {v3, v8, v9}, Lcme;->a(FF)Lcme;

    .line 217
    invoke-virtual {v7, v12, v13}, Lcme;->b(J)Lcmg;

    move-result-object v3

    int-to-float v8, v5

    invoke-virtual {v3, v8}, Lcmg;->a(F)Lcmg;

    move-result-object v3

    invoke-virtual {v3}, Lcmg;->a()Lcme;

    .line 218
    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    int-to-float v3, v6

    const-wide/16 v8, 0x4b0

    invoke-virtual {v7, v1, v3, v8, v9}, Lcme;->b(FFJ)Lcmg;

    move-result-object v1

    .line 219
    invoke-virtual {v1, v0}, Lcmg;->a(Landroid/animation/TimeInterpolator;)Lcmg;

    move-result-object v0

    invoke-virtual {v0}, Lcmg;->a()Lcme;

    .line 220
    const-wide/16 v0, 0x12c

    invoke-virtual {v7, v0, v1}, Lcme;->b(J)Lcmg;

    move-result-object v0

    int-to-float v1, v5

    const v3, 0x3f666666    # 0.9f

    mul-float/2addr v1, v3

    invoke-virtual {v0, v1}, Lcmg;->a(F)Lcmg;

    move-result-object v0

    invoke-virtual {v0}, Lcmg;->a()Lcme;

    .line 221
    const-wide/16 v0, 0x64

    invoke-virtual {v7, v0, v1}, Lcme;->b(J)Lcmg;

    move-result-object v0

    invoke-virtual {v0}, Lcmg;->a()Lcme;

    .line 222
    const-wide/16 v0, 0x32

    invoke-virtual {v7, v0, v1}, Lcme;->b(J)Lcmg;

    move-result-object v0

    int-to-float v1, v5

    const v3, 0x3f6b851f    # 0.92f

    mul-float/2addr v1, v3

    invoke-virtual {v0, v1}, Lcmg;->a(F)Lcmg;

    move-result-object v0

    invoke-virtual {v0}, Lcmg;->a()Lcme;

    .line 223
    const/high16 v0, -0x40400000    # -1.5f

    int-to-float v1, v4

    mul-float/2addr v0, v1

    const/4 v1, 0x0

    const-wide/16 v8, 0x2d0

    invoke-virtual {v7, v0, v1, v8, v9}, Lcme;->a(FFJ)Lcmg;

    move-result-object v0

    .line 224
    invoke-virtual {v0, v2}, Lcmg;->a(Landroid/animation/TimeInterpolator;)Lcmg;

    move-result-object v0

    int-to-float v1, v5

    invoke-virtual {v0, v1}, Lcmg;->a(F)Lcmg;

    move-result-object v0

    invoke-virtual {v0}, Lcmg;->a()Lcme;

    .line 225
    invoke-virtual {v7, v12, v13}, Lcme;->b(J)Lcmg;

    move-result-object v0

    int-to-float v1, v5

    mul-float/2addr v1, v11

    invoke-virtual {v0, v1}, Lcmg;->a(F)Lcmg;

    move-result-object v0

    invoke-virtual {v0}, Lcmg;->a()Lcme;

    .line 227
    const/4 v0, 0x1

    new-array v0, v0, [Lcmd;

    invoke-virtual {v7}, Lcme;->a()Lcmd;

    move-result-object v1

    aput-object v1, v0, v10

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public f(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    const/16 v0, 0xdd

    if-ne p1, v0, :cond_0

    .line 150
    const v0, 0x7f0a0093

    invoke-virtual {p0, v0}, Lcju;->e_(I)Ljava/lang/String;

    move-result-object v0

    .line 152
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcjy;->f(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
