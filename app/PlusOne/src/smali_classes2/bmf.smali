.class public final Lbmf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:I

.field private b:I

.field private c:Lbmg;

.field private d:Lbmp;

.field private e:Z

.field private f:Z

.field private g:[B

.field private h:Ljeg;

.field private i:I

.field private j:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 343
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 328
    iput v0, p0, Lbmf;->a:I

    .line 329
    iput v0, p0, Lbmf;->b:I

    .line 334
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbmf;->e:Z

    .line 344
    return-void
.end method

.method constructor <init>(Lbmd;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 346
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 328
    iput v0, p0, Lbmf;->a:I

    .line 329
    iput v0, p0, Lbmf;->b:I

    .line 334
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbmf;->e:Z

    .line 347
    iget v0, p1, Lbmd;->b:I

    iput v0, p0, Lbmf;->a:I

    .line 348
    iget v0, p1, Lbmd;->c:I

    iput v0, p0, Lbmf;->b:I

    .line 349
    iget-object v0, p1, Lbmd;->d:Lbmg;

    iput-object v0, p0, Lbmf;->c:Lbmg;

    .line 350
    iget-object v0, p1, Lbmd;->f:Lbmp;

    iput-object v0, p0, Lbmf;->d:Lbmp;

    .line 351
    iget-boolean v0, p1, Lbmd;->g:Z

    iput-boolean v0, p0, Lbmf;->e:Z

    .line 352
    iget-boolean v0, p1, Lbmd;->h:Z

    iput-boolean v0, p0, Lbmf;->f:Z

    .line 353
    invoke-static {p1}, Lbmd;->b(Lbmd;)[B

    move-result-object v0

    iput-object v0, p0, Lbmf;->g:[B

    .line 354
    iget-object v0, p1, Lbmd;->e:Ljeg;

    iput-object v0, p0, Lbmf;->h:Ljeg;

    .line 355
    iget v0, p1, Lbmd;->i:I

    iput v0, p0, Lbmf;->i:I

    .line 356
    iget-boolean v0, p1, Lbmd;->j:Z

    iput-boolean v0, p0, Lbmf;->j:Z

    .line 357
    return-void
.end method


# virtual methods
.method public a()Lbmd;
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 366
    iget-object v0, p0, Lbmf;->c:Lbmg;

    const-string v1, "mType"

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbmg;

    .line 367
    iget-object v0, p0, Lbmf;->d:Lbmp;

    const-string v1, "mInterval"

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lbmp;

    .line 368
    iget-object v0, p0, Lbmf;->h:Ljeg;

    const-string v1, "mMediaIdentifier"

    .line 369
    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljeg;

    .line 370
    new-instance v0, Lbmd;

    iget v1, p0, Lbmf;->a:I

    iget v2, p0, Lbmf;->b:I

    iget-boolean v6, p0, Lbmf;->e:Z

    iget-boolean v7, p0, Lbmf;->f:Z

    iget-object v8, p0, Lbmf;->g:[B

    iget v9, p0, Lbmf;->i:I

    iget-boolean v10, p0, Lbmf;->j:Z

    invoke-direct/range {v0 .. v10}, Lbmd;-><init>(IILbmg;Ljeg;Lbmp;ZZ[BIZ)V

    return-object v0
.end method

.method public a(I)Lbmf;
    .locals 0

    .prologue
    .line 377
    iput p1, p0, Lbmf;->a:I

    .line 378
    return-object p0
.end method

.method public a(Lbmg;)Lbmf;
    .locals 2

    .prologue
    .line 387
    const-string v0, "type"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmg;

    iput-object v0, p0, Lbmf;->c:Lbmg;

    .line 388
    return-object p0
.end method

.method public a(Lbmp;)Lbmf;
    .locals 2

    .prologue
    .line 392
    const-string v0, "interval"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmp;

    iput-object v0, p0, Lbmf;->d:Lbmp;

    .line 393
    return-object p0
.end method

.method public a(Ljeg;)Lbmf;
    .locals 0

    .prologue
    .line 417
    iput-object p1, p0, Lbmf;->h:Ljeg;

    .line 418
    return-object p0
.end method

.method public a(Look;)Lbmf;
    .locals 1

    .prologue
    .line 408
    if-nez p1, :cond_0

    .line 409
    const/4 v0, 0x0

    iput-object v0, p0, Lbmf;->g:[B

    .line 413
    :goto_0
    return-object p0

    .line 411
    :cond_0
    invoke-static {p1}, Loxu;->a(Loxu;)[B

    move-result-object v0

    iput-object v0, p0, Lbmf;->g:[B

    goto :goto_0
.end method

.method public a(Z)Lbmf;
    .locals 0

    .prologue
    .line 397
    iput-boolean p1, p0, Lbmf;->e:Z

    .line 398
    return-object p0
.end method

.method public b(I)Lbmf;
    .locals 0

    .prologue
    .line 382
    iput p1, p0, Lbmf;->b:I

    .line 383
    return-object p0
.end method

.method public b(Z)Lbmf;
    .locals 0

    .prologue
    .line 402
    iput-boolean p1, p0, Lbmf;->f:Z

    .line 403
    return-object p0
.end method

.method public c(I)Lbmf;
    .locals 0

    .prologue
    .line 422
    iput p1, p0, Lbmf;->i:I

    .line 423
    return-object p0
.end method

.method public c(Z)Lbmf;
    .locals 0

    .prologue
    .line 427
    iput-boolean p1, p0, Lbmf;->j:Z

    .line 428
    return-object p0
.end method
