.class public final Lbiv;
.super Lbja;
.source "PG"


# instance fields
.field private a:Lbmp;

.field private b:Z


# direct methods
.method public constructor <init>(Lbig;Lbmd;)V
    .locals 2

    .prologue
    .line 34
    const-string v0, "clip"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-object v0, v0, Lbmd;->e:Ljeg;

    invoke-direct {p0, p1, v0}, Lbja;-><init>(Lbig;Ljeg;)V

    .line 35
    iget-object v0, p2, Lbmd;->f:Lbmp;

    iput-object v0, p0, Lbiv;->a:Lbmp;

    .line 36
    return-void
.end method


# virtual methods
.method public a(Ljava/nio/ByteBuffer;)Lbix;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 81
    invoke-super {p0, p1}, Lbja;->a(Ljava/nio/ByteBuffer;)Lbix;

    move-result-object v0

    .line 82
    if-nez v0, :cond_1

    .line 83
    const/4 v0, 0x0

    .line 92
    :cond_0
    :goto_0
    return-object v0

    .line 86
    :cond_1
    invoke-virtual {p0}, Lbiv;->f()J

    move-result-wide v2

    iget-object v1, p0, Lbiv;->a:Lbmp;

    iget-wide v4, v1, Lbmp;->c:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 87
    iput-boolean v6, p0, Lbiv;->b:Z

    .line 88
    iput-boolean v6, v0, Lbix;->f:Z

    .line 89
    invoke-virtual {p0}, Lbiv;->g()V

    goto :goto_0
.end method

.method public bridge synthetic a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    check-cast p1, Ljava/nio/ByteBuffer;

    invoke-virtual {p0, p1}, Lbiv;->a(Ljava/nio/ByteBuffer;)Lbix;

    move-result-object v0

    return-object v0
.end method

.method public a(J)Z
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 66
    const-string v0, "timestampUs"

    invoke-static {p1, p2, v0}, Lcec;->b(JLjava/lang/CharSequence;)J

    .line 68
    iget-object v0, p0, Lbiv;->a:Lbmp;

    iget-wide v0, v0, Lbmp;->c:J

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    move v0, v6

    :goto_0
    iput-boolean v0, p0, Lbiv;->b:Z

    .line 69
    iget-object v0, p0, Lbiv;->a:Lbmp;

    iget-wide v2, v0, Lbmp;->b:J

    iget-object v0, p0, Lbiv;->a:Lbmp;

    iget-wide v4, v0, Lbmp;->c:J

    move-wide v0, p1

    invoke-static/range {v0 .. v5}, Lcfn;->a(JJJ)J

    move-result-wide v0

    invoke-super {p0, v0, v1}, Lbja;->a(J)Z

    .line 71
    iget-boolean v0, p0, Lbiv;->b:Z

    if-nez v0, :cond_1

    :goto_1
    return v6

    :cond_0
    move v0, v7

    .line 68
    goto :goto_0

    :cond_1
    move v6, v7

    .line 71
    goto :goto_1
.end method

.method public a(Lbmp;)Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 43
    invoke-virtual {p0}, Lbiv;->f()J

    move-result-wide v0

    iget-wide v4, p1, Lbmp;->c:J

    cmp-long v0, v0, v4

    if-ltz v0, :cond_1

    move v1, v2

    .line 44
    :goto_0
    const-string v0, "interval"

    const/4 v4, 0x0

    invoke-static {p1, v0, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmp;

    iput-object v0, p0, Lbiv;->a:Lbmp;

    .line 45
    if-eqz v1, :cond_0

    .line 46
    iput-boolean v2, p0, Lbiv;->b:Z

    .line 47
    invoke-virtual {p0}, Lbiv;->g()V

    .line 49
    :cond_0
    if-nez v1, :cond_2

    :goto_1
    return v2

    :cond_1
    move v1, v3

    .line 43
    goto :goto_0

    :cond_2
    move v2, v3

    .line 49
    goto :goto_1
.end method

.method public b()V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0}, Lbja;->b()V

    .line 55
    iget-object v0, p0, Lbiv;->a:Lbmp;

    iget-wide v0, v0, Lbmp;->b:J

    invoke-virtual {p0, v0, v1}, Lbiv;->a(J)Z

    .line 56
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 60
    invoke-super {p0}, Lbja;->c()V

    .line 61
    iget-object v0, p0, Lbiv;->a:Lbmp;

    iget-wide v0, v0, Lbmp;->b:J

    invoke-virtual {p0, v0, v1}, Lbiv;->a(J)Z

    .line 62
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 76
    iget-boolean v0, p0, Lbiv;->b:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Lbja;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 97
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s from %d to %d"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    .line 98
    invoke-super {p0}, Lbja;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lbiv;->a:Lbmp;

    iget-wide v4, v4, Lbmp;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lbiv;->a:Lbmp;

    iget-wide v4, v4, Lbmp;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    .line 97
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
