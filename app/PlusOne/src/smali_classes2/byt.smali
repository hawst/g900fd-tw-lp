.class public final Lbyt;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Lbyh;

.field private b:Lbyu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lbyt;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Lbyk;

    invoke-direct {v0}, Lbyk;-><init>()V

    iput-object v0, p0, Lbyt;->a:Lbyh;

    .line 49
    sget-object v0, Lbyu;->a:Lbyu;

    iput-object v0, p0, Lbyt;->b:Lbyu;

    .line 50
    return-void
.end method

.method public constructor <init>(Lbyh;Lbyu;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lbyt;->a:Lbyh;

    .line 55
    iput-object p2, p0, Lbyt;->b:Lbyu;

    .line 56
    return-void
.end method

.method private static a(Ljava/util/List;Ljava/util/List;)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcac;",
            ">;)J"
        }
    .end annotation

    .prologue
    const-wide/16 v2, 0x0

    .line 197
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 209
    :cond_0
    return-wide v2

    .line 200
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v1, "should be 1 more clip than there are transitions"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 203
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 204
    invoke-virtual {v0}, Lbmd;->b()J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    .line 205
    goto :goto_1

    .line 200
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 206
    :cond_3
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcac;

    .line 207
    invoke-interface {v0}, Lcac;->b()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 208
    goto :goto_2
.end method

.method private static a(Ljava/util/List;Lboj;[J)Ljava/util/List;
    .locals 16
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;",
            "Lboj;",
            "[J)",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 166
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lboj;->d()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v2, v3, :cond_2

    const/4 v2, 0x1

    :goto_0
    const-string v3, "clip counts should match for trimmedLengthUs"

    invoke-static {v2, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 168
    if-eqz p2, :cond_0

    .line 169
    move-object/from16 v0, p2

    array-length v2, v0

    invoke-virtual/range {p1 .. p1}, Lboj;->d()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ne v2, v3, :cond_3

    const/4 v2, 0x1

    :goto_1
    const-string v3, "clip counts should match for trimmedLengthOffsets"

    invoke-static {v2, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 172
    :cond_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v6, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 173
    invoke-virtual/range {p1 .. p1}, Lboj;->d()Ljava/util/List;

    move-result-object v7

    .line 174
    invoke-virtual/range {p1 .. p1}, Lboj;->a()Ljava/util/List;

    move-result-object v8

    .line 176
    const/4 v2, 0x0

    move v3, v2

    :goto_2
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v2

    if-ge v3, v2, :cond_4

    .line 177
    move-object/from16 v0, p0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 178
    if-eqz v8, :cond_1

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v3, v2, :cond_1

    .line 179
    invoke-interface {v8, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcac;

    invoke-interface {v2}, Lcac;->b()J

    move-result-wide v10

    add-long/2addr v4, v10

    .line 181
    :cond_1
    invoke-interface {v7, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbmd;

    .line 182
    invoke-virtual {v2}, Lbmd;->a()Lbmf;

    move-result-object v9

    new-instance v10, Lbmp;

    iget-object v11, v2, Lbmd;->f:Lbmp;

    iget-wide v12, v11, Lbmp;->b:J

    iget-object v2, v2, Lbmd;->f:Lbmp;

    iget-wide v14, v2, Lbmp;->b:J

    add-long/2addr v4, v14

    invoke-direct {v10, v12, v13, v4, v5}, Lbmp;-><init>(JJ)V

    .line 183
    invoke-virtual {v9, v10}, Lbmf;->a(Lbmp;)Lbmf;

    move-result-object v2

    .line 186
    invoke-virtual {v2}, Lbmf;->a()Lbmd;

    move-result-object v2

    .line 182
    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 176
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 166
    :cond_2
    const/4 v2, 0x0

    goto :goto_0

    .line 169
    :cond_3
    const/4 v2, 0x0

    goto :goto_1

    .line 188
    :cond_4
    return-object v6
.end method

.method private a(Lboj;Lboj;Lbmk;Ljava/util/Map;Ljava/util/List;)V
    .locals 19
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lboj;",
            "Lboj;",
            "Lbmk;",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbys;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 317
    .line 318
    invoke-virtual/range {p1 .. p1}, Lboj;->d()Ljava/util/List;

    move-result-object v7

    .line 319
    invoke-virtual/range {p1 .. p1}, Lboj;->a()Ljava/util/List;

    move-result-object v8

    .line 320
    invoke-virtual/range {p1 .. p1}, Lboj;->b()Layj;

    move-result-object v2

    invoke-interface {v2}, Layj;->c()I

    move-result v9

    .line 317
    if-nez v9, :cond_2

    const/4 v2, 0x0

    move-object v15, v2

    .line 322
    :goto_0
    if-eqz v15, :cond_0

    .line 323
    array-length v2, v15

    const-string v3, "beatMatchingOffsets.size()"

    .line 324
    invoke-virtual/range {p1 .. p1}, Lboj;->d()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    const-string v5, "Beat alignment should produce the same sized array as was input"

    .line 323
    invoke-static {v2, v3, v4, v5}, Lcgp;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 330
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lboj;->a()Ljava/util/List;

    move-result-object v16

    invoke-virtual/range {p1 .. p1}, Lboj;->d()Ljava/util/List;

    move-result-object v17

    new-instance v18, Ljava/util/ArrayList;

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v2

    move-object/from16 v0, v18

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    if-eqz v15, :cond_1

    array-length v2, v15

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v3

    if-ne v2, v3, :cond_7

    const/4 v2, 0x1

    :goto_1
    const-string v3, "clip counts should match for beatMatchingOffsets"

    invoke-static {v2, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    :cond_1
    const/4 v2, 0x0

    move v14, v2

    :goto_2
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v2

    if-ge v14, v2, :cond_e

    move-object/from16 v0, v17

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbmd;

    if-eqz v16, :cond_8

    invoke-interface/range {v17 .. v17}, Ljava/util/List;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge v14, v3, :cond_8

    move-object/from16 v0, v16

    invoke-interface {v0, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcac;

    invoke-interface {v3}, Lcac;->b()J

    move-result-wide v4

    :goto_3
    invoke-virtual {v2}, Lbmd;->b()J

    move-result-wide v6

    sub-long v4, v6, v4

    iget-boolean v3, v2, Lbmd;->g:Z

    if-nez v3, :cond_9

    new-instance v3, Lbqv;

    move-wide v6, v4

    move-wide v8, v4

    invoke-direct/range {v3 .. v9}, Lbqv;-><init>(JJJ)V

    move-object v7, v3

    :goto_4
    move-object/from16 v0, v18

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v14, 0x1

    move v14, v2

    goto :goto_2

    .line 317
    :cond_2
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v2

    new-array v6, v2, [J

    const/4 v2, 0x0

    move v5, v2

    :goto_5
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v2

    if-ge v5, v2, :cond_5

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcac;

    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbmd;

    add-int/lit8 v4, v5, 0x1

    invoke-interface {v7, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbmd;

    iget-object v3, v3, Lbmd;->d:Lbmg;

    sget-object v10, Lbmg;->c:Lbmg;

    if-ne v3, v10, :cond_4

    iget-object v3, v4, Lbmd;->d:Lbmg;

    sget-object v4, Lbmg;->c:Lbmg;

    if-ne v3, v4, :cond_4

    if-eqz v2, :cond_4

    invoke-interface {v2}, Lcac;->b()J

    move-result-wide v2

    const-wide/16 v10, 0x0

    cmp-long v2, v2, v10

    if-lez v2, :cond_4

    int-to-long v2, v9

    aput-wide v2, v6, v5

    if-lez v5, :cond_3

    add-int/lit8 v2, v5, -0x1

    aget-wide v2, v6, v2

    const-wide/16 v10, 0x0

    cmp-long v2, v2, v10

    if-eqz v2, :cond_3

    aget-wide v2, v6, v5

    add-int/lit8 v4, v5, -0x1

    aget-wide v10, v6, v4

    sub-long/2addr v2, v10

    aput-wide v2, v6, v5

    :cond_3
    :goto_6
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_5

    :cond_4
    const-wide/16 v2, 0x0

    aput-wide v2, v6, v5

    goto :goto_6

    :cond_5
    if-lez v5, :cond_6

    add-int/lit8 v2, v5, -0x1

    aget-wide v2, v6, v2

    neg-long v2, v2

    aput-wide v2, v6, v5

    :cond_6
    move-object v15, v6

    goto/16 :goto_0

    .line 330
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_8
    const-wide/16 v4, 0x0

    goto/16 :goto_3

    :cond_9
    iget-object v3, v2, Lbmd;->d:Lbmg;

    sget-object v6, Lbmg;->d:Lbmg;

    if-ne v3, v6, :cond_b

    if-nez v14, :cond_a

    new-instance v7, Lbqv;

    const-wide/32 v8, 0xf4240

    move-wide v10, v4

    move-wide v12, v4

    invoke-direct/range {v7 .. v13}, Lbqv;-><init>(JJJ)V

    goto/16 :goto_4

    :cond_a
    new-instance v3, Lbqv;

    move-wide v6, v4

    move-wide v8, v4

    invoke-direct/range {v3 .. v9}, Lbqv;-><init>(JJJ)V

    move-object v7, v3

    goto/16 :goto_4

    :cond_b
    iget-object v3, v2, Lbmd;->e:Ljeg;

    move-object/from16 v0, p4

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbml;

    const-string v6, "displayable"

    const/4 v7, 0x0

    invoke-static {v3, v6, v7}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    instance-of v6, v3, Lbon;

    if-eqz v6, :cond_c

    invoke-interface {v3}, Lbml;->e()J

    move-result-wide v6

    iget-object v2, v2, Lbmd;->f:Lbmp;

    iget-wide v2, v2, Lbmp;->c:J

    sub-long v2, v6, v2

    new-instance v7, Lbqv;

    const-wide/32 v8, 0xf4240

    move-object/from16 v0, p0

    iget-object v6, v0, Lbyt;->b:Lbyu;

    iget-wide v10, v6, Lbyu;->f:J

    invoke-static {v2, v3, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    add-long v12, v4, v2

    move-wide v10, v4

    invoke-direct/range {v7 .. v13}, Lbqv;-><init>(JJJ)V

    goto/16 :goto_4

    :cond_c
    instance-of v2, v3, Lbmw;

    if-eqz v2, :cond_d

    new-instance v7, Lbqv;

    const-wide/32 v8, 0xf4240

    move-object/from16 v0, p0

    iget-object v2, v0, Lbyt;->b:Lbyu;

    iget-wide v2, v2, Lbyu;->g:J

    add-long v12, v4, v2

    move-wide v10, v4

    invoke-direct/range {v7 .. v13}, Lbqv;-><init>(JJJ)V

    goto/16 :goto_4

    :cond_d
    const-string v2, "displayable should be a photo or a video"

    invoke-static {v2}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v2

    throw v2

    .line 334
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyt;->a:Lbyh;

    .line 335
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-virtual {v0, v1}, Lbyt;->a(Lbmk;)Lbrm;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lbyt;->b:Lbyu;

    .line 334
    move-object/from16 v0, v18

    move-object/from16 v1, p5

    invoke-interface {v2, v0, v3, v1, v4}, Lbyh;->a(Ljava/util/List;Lbrm;Ljava/util/List;Lbyu;)Ljava/util/List;

    move-result-object v2

    .line 337
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    const-string v4, "trimmedLengths.size()"

    .line 338
    invoke-virtual/range {p1 .. p1}, Lboj;->d()Ljava/util/List;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    const-string v6, "Beat alignment should produce the same sized array as was input"

    .line 337
    invoke-static {v3, v4, v5, v6}, Lcgp;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 342
    move-object/from16 v0, p1

    invoke-static {v2, v0, v15}, Lbyt;->a(Ljava/util/List;Lboj;[J)Ljava/util/List;

    move-result-object v3

    .line 347
    invoke-virtual/range {p1 .. p1}, Lboj;->a()Ljava/util/List;

    move-result-object v2

    invoke-static {v3, v2}, Lbyt;->a(Ljava/util/List;Ljava/util/List;)J

    move-result-wide v6

    .line 349
    new-instance v10, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v2, v0, Lbyt;->b:Lbyu;

    iget v2, v2, Lbyu;->j:I

    invoke-direct {v10, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 351
    const/4 v2, 0x0

    move-wide v4, v6

    :goto_7
    move-object/from16 v0, p0

    iget-object v8, v0, Lbyt;->b:Lbyu;

    iget v8, v8, Lbyu;->j:I

    if-ge v2, v8, :cond_10

    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-lez v8, :cond_10

    .line 354
    if-nez v2, :cond_f

    .line 355
    invoke-static/range {p3 .. p3}, Lbrj;->a(Lbmk;)J

    move-result-wide v8

    .line 357
    :goto_8
    invoke-virtual/range {p3 .. p3}, Lbmk;->d()J

    move-result-wide v12

    add-long v14, v8, v4

    .line 356
    invoke-static {v12, v13, v14, v15}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v12

    .line 359
    new-instance v11, Lbmd;

    sget-object v14, Lbmg;->b:Lbmg;

    .line 362
    invoke-virtual/range {p3 .. p3}, Lbmk;->a()Landroid/net/Uri;

    move-result-object v15

    sget-object v16, Ljei;->c:Ljei;

    .line 361
    invoke-static/range {v15 .. v16}, Ljeg;->a(Landroid/net/Uri;Ljei;)Ljeg;

    move-result-object v15

    new-instance v16, Lbmp;

    move-object/from16 v0, v16

    invoke-direct {v0, v8, v9, v12, v13}, Lbmp;-><init>(JJ)V

    move-object/from16 v0, v16

    invoke-direct {v11, v14, v15, v0}, Lbmd;-><init>(Lbmg;Ljeg;Lbmp;)V

    .line 359
    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 364
    sub-long v8, v12, v8

    sub-long/2addr v4, v8

    .line 351
    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    .line 355
    :cond_f
    const-wide/16 v8, 0x0

    goto :goto_8

    .line 366
    :cond_10
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Lboj;->a(Ljava/util/List;)Lboj;

    .line 367
    invoke-static {v10, v6, v7}, Lblz;->a(Ljava/util/List;J)Lblz;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lboj;->a(Lbma;)Lboj;

    .line 369
    return-void
.end method


# virtual methods
.method public a(Lboj;Lbmk;Ljava/util/Map;Ljava/util/Map;ZLood;Ljava/util/List;)Lboj;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lboj;",
            "Lbmk;",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbmb;",
            ">;Z",
            "Lood;",
            "Ljava/util/List",
            "<",
            "Lbys;",
            ">;)",
            "Lboj;"
        }
    .end annotation

    .prologue
    .line 81
    if-nez p2, :cond_1

    .line 82
    if-eqz p5, :cond_0

    .line 112
    :goto_0
    return-object p1

    .line 86
    :cond_0
    invoke-virtual {p1}, Lboj;->d()Ljava/util/List;

    move-result-object v2

    invoke-virtual {p1}, Lboj;->a()Ljava/util/List;

    move-result-object v3

    .line 85
    invoke-static {v2, v3}, Lbyt;->a(Ljava/util/List;Ljava/util/List;)J

    move-result-wide v2

    .line 87
    new-instance v4, Lboj;

    invoke-direct {v4, p1}, Lboj;-><init>(Lboj;)V

    .line 89
    invoke-static {v2, v3}, Lblz;->b(J)Lblz;

    move-result-object v2

    .line 88
    invoke-virtual {v4, v2}, Lboj;->a(Lbma;)Lboj;

    move-result-object p1

    goto :goto_0

    .line 93
    :cond_1
    new-instance v4, Lboj;

    invoke-direct {v4, p1}, Lboj;-><init>(Lboj;)V

    .line 97
    if-eqz p6, :cond_5

    .line 98
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p6

    iget-object v2, v0, Lood;->b:[Lonr;

    array-length v2, v2

    const-string v3, "cloud storyboard clip count"

    invoke-virtual {p1}, Lboj;->d()Ljava/util/List;

    move-result-object v7

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    const/4 v8, 0x0

    invoke-static {v2, v3, v7, v8}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    const/4 v2, 0x0

    move v3, v2

    :goto_1
    move-object/from16 v0, p6

    iget-object v2, v0, Lood;->b:[Lonr;

    array-length v2, v2

    if-ge v3, v2, :cond_2

    move-object/from16 v0, p6

    iget-object v2, v0, Lood;->b:[Lonr;

    aget-object v2, v2, v3

    iget-object v2, v2, Lonr;->d:Lons;

    invoke-static {v2}, Lbpx;->a(Lons;)Lbmp;

    move-result-object v7

    invoke-virtual {p1}, Lboj;->d()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbmd;

    invoke-virtual {v2}, Lbmd;->a()Lbmf;

    move-result-object v2

    invoke-virtual {v2, v7}, Lbmf;->a(Lbmp;)Lbmf;

    move-result-object v2

    invoke-virtual {v2}, Lbmf;->a()Lbmd;

    move-result-object v2

    invoke-interface {v6, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_1

    :cond_2
    move-object/from16 v0, p6

    iget-object v2, v0, Lood;->h:Looe;

    iget-object v2, v2, Looe;->d:Lonq;

    iget-object v2, v2, Lonq;->a:Lonp;

    iget-object v3, v2, Lonp;->a:[Lonr;

    array-length v7, v3

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v7, :cond_3

    aget-object v8, v3, v2

    new-instance v9, Lbmd;

    sget-object v10, Lbmg;->b:Lbmg;

    invoke-virtual {p2}, Lbmk;->a()Landroid/net/Uri;

    move-result-object v11

    sget-object v12, Ljei;->c:Ljei;

    invoke-static {v11, v12}, Ljeg;->a(Landroid/net/Uri;Ljei;)Ljeg;

    move-result-object v11

    iget-object v8, v8, Lonr;->d:Lons;

    invoke-static {v8}, Lbpx;->a(Lons;)Lbmp;

    move-result-object v8

    invoke-direct {v9, v10, v11, v8}, Lbmd;-><init>(Lbmg;Ljeg;Lbmp;)V

    invoke-interface {v5, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Lboj;->a()Ljava/util/List;

    move-result-object v2

    invoke-static {v6, v2}, Lbyt;->a(Ljava/util/List;Ljava/util/List;)J

    move-result-wide v2

    invoke-virtual {v4, v6}, Lboj;->a(Ljava/util/List;)Lboj;

    invoke-static {v5, v2, v3}, Lblz;->a(Ljava/util/List;J)Lblz;

    move-result-object v2

    invoke-virtual {v4, v2}, Lboj;->a(Lbma;)Lboj;

    .line 105
    :goto_3
    if-eqz p5, :cond_4

    .line 106
    invoke-virtual {v4}, Lboj;->d()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v4}, Lboj;->a()Ljava/util/List;

    move-result-object v3

    move-object/from16 v0, p3

    move-object/from16 v1, p4

    invoke-static {v0, v1, v2, v3}, Lbzr;->a(Ljava/util/Map;Ljava/util/Map;Ljava/util/List;Ljava/util/List;)Landroid/util/Pair;

    move-result-object v3

    iget-object v2, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Lbma;

    invoke-virtual {v4, v2}, Lboj;->a(Lbma;)Lboj;

    iget-object v2, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v2, :cond_4

    iget-object v2, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lbma;

    invoke-virtual {v4, v2}, Lboj;->a(Lbma;)Lboj;

    :cond_4
    move-object p1, v4

    .line 112
    goto/16 :goto_0

    :cond_5
    move-object v2, p0

    move-object v3, p1

    move-object v5, p2

    move-object/from16 v6, p3

    move-object/from16 v7, p7

    .line 102
    invoke-direct/range {v2 .. v7}, Lbyt;->a(Lboj;Lboj;Lbmk;Ljava/util/Map;Ljava/util/List;)V

    goto :goto_3
.end method

.method public a(Lbmk;)Lbrm;
    .locals 14

    .prologue
    .line 120
    invoke-virtual {p0, p1}, Lbyt;->b(Lbmk;)I

    move-result v7

    .line 121
    invoke-static {p1}, Lbrj;->a(Lbmk;)J

    move-result-wide v4

    .line 123
    new-instance v8, Lbrn;

    invoke-direct {v8}, Lbrn;-><init>()V

    .line 124
    invoke-virtual {p1}, Lbmk;->c()[J

    move-result-object v9

    .line 125
    invoke-static {v9, v4, v5}, Lbrj;->a([JJ)I

    move-result v0

    rem-int v10, v0, v7

    .line 127
    const-wide/16 v2, 0x0

    .line 128
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lbyt;->b:Lbyu;

    iget v1, v1, Lbyu;->j:I

    if-ge v0, v1, :cond_5

    .line 129
    const/4 v1, 0x0

    :goto_1
    array-length v6, v9

    if-ge v1, v6, :cond_4

    .line 130
    aget-wide v12, v9, v1

    cmp-long v6, v12, v4

    if-ltz v6, :cond_1

    .line 131
    const/high16 v6, -0x40800000    # -1.0f

    .line 132
    rem-int v11, v1, v7

    if-ne v11, v10, :cond_2

    .line 133
    iget-object v6, p0, Lbyt;->b:Lbyu;

    iget v6, v6, Lbyu;->h:F

    .line 144
    :cond_0
    :goto_2
    const/4 v11, 0x0

    cmpl-float v11, v6, v11

    if-ltz v11, :cond_1

    .line 145
    aget-wide v12, v9, v1

    sub-long/2addr v12, v4

    add-long/2addr v12, v2

    invoke-virtual {v8, v12, v13, v6}, Lbrn;->a(JF)Lbrn;

    .line 129
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 134
    :cond_2
    rem-int/lit8 v11, v7, 0x2

    if-nez v11, :cond_3

    div-int/lit8 v11, v7, 0x2

    add-int/2addr v11, v1

    rem-int/2addr v11, v7

    if-ne v11, v10, :cond_3

    iget-object v11, p0, Lbyt;->b:Lbyu;

    iget-boolean v11, v11, Lbyu;->l:Z

    if-eqz v11, :cond_3

    .line 137
    iget-object v6, p0, Lbyt;->b:Lbyu;

    iget v6, v6, Lbyu;->i:F

    goto :goto_2

    .line 138
    :cond_3
    iget-object v11, p0, Lbyt;->b:Lbyu;

    iget-boolean v11, v11, Lbyu;->m:Z

    if-eqz v11, :cond_0

    .line 142
    const/4 v6, 0x0

    goto :goto_2

    .line 152
    :cond_4
    invoke-virtual {p1}, Lbmk;->d()J

    move-result-wide v12

    sub-long v4, v12, v4

    add-long/2addr v2, v4

    .line 153
    const-wide/16 v4, 0x0

    .line 128
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 156
    :cond_5
    invoke-virtual {v8}, Lbrn;->a()Lbrm;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbyh;)V
    .locals 0

    .prologue
    .line 514
    iput-object p1, p0, Lbyt;->a:Lbyh;

    .line 515
    return-void
.end method

.method public a(Lbyu;)V
    .locals 0

    .prologue
    .line 518
    iput-object p1, p0, Lbyt;->b:Lbyu;

    .line 519
    return-void
.end method

.method public b(Lbmk;)I
    .locals 10

    .prologue
    .line 225
    invoke-virtual {p1}, Lbmk;->g()I

    move-result v0

    .line 226
    invoke-static {p1}, Lbrj;->b(Lbmk;)F

    move-result v1

    float-to-long v2, v1

    .line 227
    int-to-long v4, v0

    mul-long/2addr v2, v4

    .line 229
    iget-object v1, p0, Lbyt;->b:Lbyu;

    iget-wide v4, v1, Lbyu;->c:J

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 231
    const/4 v1, 0x1

    shl-long v4, v2, v1

    .line 232
    iget-object v1, p0, Lbyt;->b:Lbyu;

    iget-wide v6, v1, Lbyu;->c:J

    sub-long v6, v4, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    .line 233
    iget-object v1, p0, Lbyt;->b:Lbyu;

    iget-wide v8, v1, Lbyu;->c:J

    sub-long/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    .line 234
    iget-object v1, p0, Lbyt;->b:Lbyu;

    iget-wide v8, v1, Lbyu;->e:J

    cmp-long v1, v4, v8

    if-gez v1, :cond_0

    cmp-long v1, v6, v2

    if-gez v1, :cond_0

    .line 235
    shl-int/lit8 v0, v0, 0x1

    .line 253
    :cond_0
    :goto_0
    return v0

    .line 238
    :cond_1
    iget-object v1, p0, Lbyt;->b:Lbyu;

    iget-wide v4, v1, Lbyu;->c:J

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    rem-int/lit8 v1, v0, 0x2

    if-nez v1, :cond_0

    .line 240
    const-wide/16 v4, 0x2

    div-long v4, v2, v4

    .line 241
    iget-object v1, p0, Lbyt;->b:Lbyu;

    iget-wide v6, v1, Lbyu;->c:J

    sub-long v6, v4, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    .line 242
    iget-object v1, p0, Lbyt;->b:Lbyu;

    iget-wide v8, v1, Lbyu;->c:J

    sub-long/2addr v2, v8

    invoke-static {v2, v3}, Ljava/lang/Math;->abs(J)J

    move-result-wide v2

    .line 243
    iget-object v1, p0, Lbyt;->b:Lbyu;

    iget-wide v8, v1, Lbyu;->d:J

    cmp-long v1, v4, v8

    if-lez v1, :cond_0

    cmp-long v1, v6, v2

    if-gez v1, :cond_0

    .line 244
    div-int/lit8 v0, v0, 0x2

    goto :goto_0
.end method
