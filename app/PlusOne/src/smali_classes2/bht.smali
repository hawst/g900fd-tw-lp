.class public final Lbht;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbhr;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lbhj;",
        ">",
        "Ljava/lang/Object;",
        "Lbhr",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbhr",
            "<TT;>;>;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbhr",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const-string v0, "sources"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lbht;->a:Ljava/util/List;

    .line 21
    iget-object v0, p0, Lbht;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 22
    const/4 v0, -0x1

    iput v0, p0, Lbht;->b:I

    .line 26
    :goto_0
    return-void

    .line 24
    :cond_0
    const/4 v0, 0x0

    iput v0, p0, Lbht;->b:I

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Lbht;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    .line 50
    invoke-interface {v0}, Lbhr;->a()V

    goto :goto_0

    .line 52
    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lbht;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne p1, v0, :cond_0

    .line 56
    const/4 v0, -0x1

    iput v0, p0, Lbht;->b:I

    .line 60
    :goto_0
    return-void

    .line 58
    :cond_0
    const-string v0, "sourceIndex"

    iget-object v1, p0, Lbht;->a:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lcec;->a(ILjava/lang/CharSequence;Ljava/util/Collection;)I

    move-result v0

    iput v0, p0, Lbht;->b:I

    goto :goto_0
.end method

.method public a(Lbhj;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 38
    iget-object v0, p0, Lbht;->a:Ljava/util/List;

    iget v1, p0, Lbht;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    invoke-interface {v0, p1}, Lbhr;->a(Ljava/lang/Object;)V

    .line 39
    iget-boolean v0, p1, Lbhj;->b:Z

    if-eqz v0, :cond_0

    .line 40
    iget v0, p0, Lbht;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbht;->b:I

    .line 41
    iget v0, p0, Lbht;->b:I

    iget-object v1, p0, Lbht;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 42
    const/4 v0, -0x1

    iput v0, p0, Lbht;->b:I

    .line 45
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 11
    check-cast p1, Lbhj;

    invoke-virtual {p0, p1}, Lbht;->a(Lbhj;)V

    return-void
.end method

.method public b()Lbhj;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 30
    iget v0, p0, Lbht;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 31
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbht;->a:Ljava/util/List;

    iget v1, p0, Lbht;->b:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhr;

    invoke-interface {v0}, Lbhr;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhj;

    goto :goto_0
.end method

.method public synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 11
    invoke-virtual {p0}, Lbht;->b()Lbhj;

    move-result-object v0

    return-object v0
.end method
