.class final Lafp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lafb;
.implements Lafc;
.implements Lagy;


# instance fields
.field volatile a:J

.field volatile b:I

.field final c:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Lafw;",
            ">;"
        }
    .end annotation
.end field

.field volatile d:Ljava/util/Timer;

.field e:Lafh;

.field f:J

.field private volatile g:Laey;

.field private h:Lafd;

.field private i:Lafd;

.field private final j:Laff;

.field private final k:Landroid/content/Context;

.field private volatile l:I

.field private volatile m:Ljava/util/Timer;

.field private volatile n:Ljava/util/Timer;

.field private o:Z

.field private p:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Laff;)V
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lafp;-><init>(Landroid/content/Context;Laff;Lafd;)V

    .line 83
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Laff;Lafd;)V
    .locals 2

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lafp;->c:Ljava/util/Queue;

    .line 64
    const-wide/32 v0, 0x493e0

    iput-wide v0, p0, Lafp;->f:J

    .line 68
    iput-object p3, p0, Lafp;->i:Lafd;

    .line 69
    iput-object p1, p0, Lafp;->k:Landroid/content/Context;

    .line 70
    iput-object p2, p0, Lafp;->j:Laff;

    .line 71
    new-instance v0, Lafq;

    invoke-direct {v0}, Lafq;-><init>()V

    iput-object v0, p0, Lafp;->e:Lafh;

    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lafp;->l:I

    .line 78
    const/4 v0, 0x7

    iput v0, p0, Lafp;->b:I

    .line 79
    return-void
.end method

.method private a(Ljava/util/Timer;)Ljava/util/Timer;
    .locals 1

    .prologue
    .line 141
    if-eqz p1, :cond_0

    .line 142
    invoke-virtual {p1}, Ljava/util/Timer;->cancel()V

    .line 144
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lafp;->m:Ljava/util/Timer;

    invoke-direct {p0, v0}, Lafp;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Lafp;->m:Ljava/util/Timer;

    .line 149
    iget-object v0, p0, Lafp;->n:Ljava/util/Timer;

    invoke-direct {p0, v0}, Lafp;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Lafp;->n:Ljava/util/Timer;

    .line 150
    iget-object v0, p0, Lafp;->d:Ljava/util/Timer;

    invoke-direct {p0, v0}, Lafp;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Lafp;->d:Ljava/util/Timer;

    .line 151
    return-void
.end method

.method private k()V
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Lafp;->h:Lafd;

    invoke-interface {v0}, Lafd;->a()V

    .line 246
    const/4 v0, 0x0

    iput-boolean v0, p0, Lafp;->o:Z

    .line 247
    return-void
.end method

.method private l()V
    .locals 4

    .prologue
    .line 339
    iget-object v0, p0, Lafp;->m:Ljava/util/Timer;

    invoke-direct {p0, v0}, Lafp;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Lafp;->m:Ljava/util/Timer;

    .line 340
    new-instance v0, Ljava/util/Timer;

    const-string v1, "Service Reconnect"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lafp;->m:Ljava/util/Timer;

    .line 341
    iget-object v0, p0, Lafp;->m:Ljava/util/Timer;

    new-instance v1, Lafx;

    invoke-direct {v1, p0}, Lafx;-><init>(Lafp;)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 342
    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 4

    .prologue
    .line 299
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lafp;->n:Ljava/util/Timer;

    invoke-direct {p0, v0}, Lafp;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Lafp;->n:Ljava/util/Timer;

    .line 300
    const/4 v0, 0x0

    iput v0, p0, Lafp;->l:I

    .line 301
    const/4 v0, 0x2

    iput v0, p0, Lafp;->b:I

    .line 303
    invoke-virtual {p0}, Lafp;->f()V

    .line 304
    iget-object v0, p0, Lafp;->d:Ljava/util/Timer;

    invoke-direct {p0, v0}, Lafp;->a(Ljava/util/Timer;)Ljava/util/Timer;

    move-result-object v0

    iput-object v0, p0, Lafp;->d:Ljava/util/Timer;

    .line 305
    new-instance v0, Ljava/util/Timer;

    const-string v1, "disconnect check"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lafp;->d:Ljava/util/Timer;

    .line 306
    iget-object v0, p0, Lafp;->d:Ljava/util/Timer;

    new-instance v1, Lafu;

    invoke-direct {v1, p0}, Lafu;-><init>(Lafp;)V

    iget-wide v2, p0, Lafp;->f:J

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 307
    monitor-exit p0

    return-void

    .line 299
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(I)V
    .locals 2

    .prologue
    .line 328
    monitor-enter p0

    const/4 v0, 0x5

    :try_start_0
    iput v0, p0, Lafp;->b:I

    .line 329
    iget v0, p0, Lafp;->l:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_0

    .line 330
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Service unavailable (code="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), will retry."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lagm;->c(Ljava/lang/String;)I

    .line 331
    invoke-direct {p0}, Lafp;->l()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 336
    :goto_0
    monitor-exit p0

    return-void

    .line 333
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Service unavailable (code="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "), using local store."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lagm;->c(Ljava/lang/String;)I

    .line 334
    invoke-virtual {p0}, Lafp;->g()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 328
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;J",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/gms/analytics/internal/Command;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 95
    iget-object v6, p0, Lafp;->c:Ljava/util/Queue;

    new-instance v0, Lafw;

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lafw;-><init>(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V

    invoke-interface {v6, v0}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 98
    invoke-virtual {p0}, Lafp;->f()V

    .line 99
    return-void
.end method

.method public declared-synchronized b()V
    .locals 2

    .prologue
    .line 311
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lafp;->b:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_0

    .line 312
    invoke-direct {p0}, Lafp;->j()V

    .line 314
    const/4 v0, 0x7

    iput v0, p0, Lafp;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 324
    :goto_0
    monitor-exit p0

    return-void

    .line 316
    :cond_0
    const/4 v0, 0x5

    :try_start_1
    iput v0, p0, Lafp;->b:I

    .line 318
    iget v0, p0, Lafp;->l:I

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 319
    invoke-direct {p0}, Lafp;->l()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 311
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 321
    :cond_1
    :try_start_2
    invoke-virtual {p0}, Lafp;->g()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public c()V
    .locals 2

    .prologue
    .line 103
    sget-object v0, Lafs;->a:[I

    iget v1, p0, Lafp;->b:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lafp;->o:Z

    .line 113
    :goto_0
    :pswitch_0
    return-void

    .line 105
    :pswitch_1
    invoke-direct {p0}, Lafp;->k()V

    goto :goto_0

    .line 103
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public d()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 117
    iget-object v0, p0, Lafp;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 119
    sget-object v0, Lafs;->a:[I

    iget v1, p0, Lafp;->b:I

    add-int/lit8 v1, v1, -0x1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lafp;->p:Z

    .line 132
    :goto_0
    return-void

    .line 121
    :pswitch_0
    iget-object v0, p0, Lafp;->h:Lafd;

    const-wide/16 v2, 0x0

    invoke-interface {v0, v2, v3}, Lafd;->a(J)V

    .line 122
    iput-boolean v4, p0, Lafp;->p:Z

    goto :goto_0

    .line 125
    :pswitch_1
    iget-object v0, p0, Lafp;->g:Laey;

    invoke-interface {v0}, Laey;->a()V

    .line 126
    iput-boolean v4, p0, Lafp;->p:Z

    goto :goto_0

    .line 119
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public e()V
    .locals 2

    .prologue
    .line 159
    iget-object v0, p0, Lafp;->g:Laey;

    if-eqz v0, :cond_0

    .line 164
    :goto_0
    return-void

    .line 162
    :cond_0
    new-instance v0, Laez;

    iget-object v1, p0, Lafp;->k:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p0}, Laez;-><init>(Landroid/content/Context;Lafb;Lafc;)V

    iput-object v0, p0, Lafp;->g:Laey;

    .line 163
    invoke-virtual {p0}, Lafp;->h()V

    goto :goto_0
.end method

.method declared-synchronized f()V
    .locals 8

    .prologue
    .line 194
    monitor-enter p0

    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    iget-object v3, p0, Lafp;->j:Laff;

    invoke-interface {v3}, Laff;->c()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 195
    iget-object v2, p0, Lafp;->j:Laff;

    invoke-interface {v2}, Laff;->b()Ljava/util/concurrent/LinkedBlockingQueue;

    move-result-object v2

    new-instance v3, Lafr;

    invoke-direct {v3, p0}, Lafr;-><init>(Lafp;)V

    invoke-virtual {v2, v3}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 239
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 203
    :cond_1
    :try_start_1
    iget-boolean v2, p0, Lafp;->p:Z

    if-eqz v2, :cond_2

    .line 204
    invoke-virtual {p0}, Lafp;->d()V

    .line 206
    :cond_2
    sget-object v2, Lafs;->a:[I

    iget v3, p0, Lafp;->b:I

    add-int/lit8 v3, v3, -0x1

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 208
    :goto_1
    :pswitch_0
    iget-object v2, p0, Lafp;->c:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    .line 209
    iget-object v2, p0, Lafp;->c:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lafw;

    move-object v7, v0

    .line 210
    iget-object v2, p0, Lafp;->h:Lafd;

    invoke-virtual {v7}, Lafw;->a()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v7}, Lafw;->b()J

    move-result-wide v4

    invoke-virtual {v7}, Lafw;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7}, Lafw;->d()Ljava/util/List;

    move-result-object v7

    invoke-interface/range {v2 .. v7}, Lafd;->a(Ljava/util/Map;JLjava/lang/String;Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 194
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 215
    :cond_3
    :try_start_2
    iget-boolean v2, p0, Lafp;->o:Z

    if-eqz v2, :cond_0

    .line 216
    invoke-direct {p0}, Lafp;->k()V

    goto :goto_0

    .line 220
    :goto_2
    :pswitch_1
    iget-object v2, p0, Lafp;->c:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    .line 221
    iget-object v2, p0, Lafp;->c:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lafw;

    move-object v7, v0

    .line 222
    iget-object v2, p0, Lafp;->g:Laey;

    invoke-virtual {v7}, Lafw;->a()Ljava/util/Map;

    move-result-object v3

    invoke-virtual {v7}, Lafw;->b()J

    move-result-wide v4

    invoke-virtual {v7}, Lafw;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v7}, Lafw;->d()Ljava/util/List;

    move-result-object v7

    invoke-interface/range {v2 .. v7}, Laey;->a(Ljava/util/Map;JLjava/lang/String;Ljava/util/List;)V

    .line 225
    iget-object v2, p0, Lafp;->c:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    goto :goto_2

    .line 227
    :cond_4
    iget-object v2, p0, Lafp;->e:Lafh;

    invoke-interface {v2}, Lafh;->a()J

    move-result-wide v2

    iput-wide v2, p0, Lafp;->a:J

    goto :goto_0

    .line 230
    :pswitch_2
    iget-object v2, p0, Lafp;->c:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 232
    invoke-virtual {p0}, Lafp;->h()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 206
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method declared-synchronized g()V
    .locals 3

    .prologue
    const/4 v1, 0x3

    .line 253
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lafp;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    .line 268
    :goto_0
    monitor-exit p0

    return-void

    .line 257
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lafp;->j()V

    .line 258
    iget-object v0, p0, Lafp;->i:Lafd;

    if-eqz v0, :cond_1

    .line 260
    iget-object v0, p0, Lafp;->i:Lafd;

    iput-object v0, p0, Lafp;->h:Lafd;

    .line 266
    :goto_1
    const/4 v0, 0x3

    iput v0, p0, Lafp;->b:I

    .line 267
    invoke-virtual {p0}, Lafp;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 253
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 262
    :cond_1
    :try_start_2
    invoke-static {}, Lafm;->a()Lafm;

    move-result-object v0

    .line 263
    iget-object v1, p0, Lafp;->k:Landroid/content/Context;

    iget-object v2, p0, Lafp;->j:Laff;

    invoke-virtual {v0, v1, v2}, Lafm;->a(Landroid/content/Context;Laff;)V

    .line 264
    invoke-virtual {v0}, Lafm;->b()Lafd;

    move-result-object v0

    iput-object v0, p0, Lafp;->h:Lafd;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method declared-synchronized h()V
    .locals 4

    .prologue
    .line 271
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lafp;->g:Laey;

    if-eqz v0, :cond_0

    iget v0, p0, Lafp;->b:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 273
    :try_start_1
    iget v0, p0, Lafp;->l:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lafp;->l:I

    .line 274
    iget-object v0, p0, Lafp;->n:Ljava/util/Timer;

    invoke-direct {p0, v0}, Lafp;->a(Ljava/util/Timer;)Ljava/util/Timer;

    .line 275
    const/4 v0, 0x1

    iput v0, p0, Lafp;->b:I

    .line 276
    new-instance v0, Ljava/util/Timer;

    const-string v1, "Failed Connect"

    invoke-direct {v0, v1}, Ljava/util/Timer;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lafp;->n:Ljava/util/Timer;

    .line 277
    iget-object v0, p0, Lafp;->n:Ljava/util/Timer;

    new-instance v1, Lafv;

    invoke-direct {v1, p0}, Lafv;-><init>(Lafp;)V

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Timer;->schedule(Ljava/util/TimerTask;J)V

    .line 278
    iget-object v0, p0, Lafp;->g:Laey;

    invoke-interface {v0}, Laey;->b()V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 288
    :goto_0
    monitor-exit p0

    return-void

    .line 281
    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "security exception on connectToService"

    invoke-static {v0}, Lagm;->c(Ljava/lang/String;)I

    .line 282
    invoke-virtual {p0}, Lafp;->g()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 271
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 285
    :cond_0
    :try_start_3
    const-string v0, "client not initialized."

    invoke-static {v0}, Lagm;->c(Ljava/lang/String;)I

    .line 286
    invoke-virtual {p0}, Lafp;->g()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method declared-synchronized i()V
    .locals 2

    .prologue
    .line 291
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lafp;->g:Laey;

    if-eqz v0, :cond_0

    iget v0, p0, Lafp;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 292
    const/4 v0, 0x6

    iput v0, p0, Lafp;->b:I

    .line 293
    iget-object v0, p0, Lafp;->g:Laey;

    invoke-interface {v0}, Laey;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 295
    :cond_0
    monitor-exit p0

    return-void

    .line 291
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
