.class final Lavk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Comparable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/lang/Comparable",
        "<",
        "Lavk;",
        ">;"
    }
.end annotation


# instance fields
.field public a:J

.field public b:I


# direct methods
.method public constructor <init>(J)V
    .locals 1

    .prologue
    .line 243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244
    iput-wide p1, p0, Lavk;->a:J

    .line 245
    return-void
.end method


# virtual methods
.method public a(Lavk;)I
    .locals 2

    .prologue
    .line 249
    iget v0, p1, Lavk;->b:I

    iget v1, p0, Lavk;->b:I

    if-ge v0, v1, :cond_0

    .line 250
    const/4 v0, -0x1

    .line 254
    :goto_0
    return v0

    .line 251
    :cond_0
    iget v0, p1, Lavk;->b:I

    iget v1, p0, Lavk;->b:I

    if-ne v0, v1, :cond_1

    .line 252
    const/4 v0, 0x0

    goto :goto_0

    .line 254
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 239
    check-cast p1, Lavk;

    invoke-virtual {p0, p1}, Lavk;->a(Lavk;)I

    move-result v0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 260
    if-nez p1, :cond_1

    .line 267
    :cond_0
    :goto_0
    return v0

    .line 263
    :cond_1
    instance-of v1, p1, Lavk;

    if-eqz v1, :cond_0

    .line 266
    check-cast p1, Lavk;

    .line 267
    iget-wide v2, p0, Lavk;->a:J

    iget-wide v4, p1, Lavk;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    iget v1, p0, Lavk;->b:I

    iget v2, p1, Lavk;->b:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 272
    iget-wide v0, p0, Lavk;->a:J

    iget v2, p0, Lavk;->b:I

    invoke-static {v0, v1, v2}, Lbqh;->a(JI)I

    move-result v0

    return v0
.end method
