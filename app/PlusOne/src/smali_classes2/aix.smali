.class final Laix;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Laic;

.field public final b:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public final c:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public final d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lajc;",
            ">;"
        }
    .end annotation
.end field

.field public final e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lajc;",
            ">;"
        }
    .end annotation
.end field

.field public final f:Laiz;

.field public final g:Lajb;

.field public final h:Lahy;

.field public i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lajc;",
            ">;"
        }
    .end annotation
.end field

.field public j:Landroid/net/Uri;


# direct methods
.method constructor <init>(Ljava/util/List;Ljava/util/List;Laic;Laiz;Lajb;Laiy;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Laic;",
            "Laiz;",
            "Lajb;",
            "Laiy;",
            ")V"
        }
    .end annotation

    .prologue
    .line 353
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 354
    iput-object p3, p0, Laix;->a:Laic;

    .line 355
    iput-object p4, p0, Laix;->f:Laiz;

    .line 356
    iput-object p5, p0, Laix;->g:Lajb;

    .line 357
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0, p1}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Laix;->b:Ljava/util/Queue;

    .line 358
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0, p2}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Laix;->c:Ljava/util/Queue;

    .line 359
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Laix;->d:Ljava/util/List;

    .line 360
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Laix;->e:Ljava/util/List;

    .line 361
    iput-object p6, p0, Laix;->h:Lahy;

    .line 362
    iget-object v0, p0, Laix;->d:Ljava/util/List;

    iput-object v0, p0, Laix;->i:Ljava/util/List;

    .line 363
    return-void
.end method
