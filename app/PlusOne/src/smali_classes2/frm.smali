.class final Lfrm;
.super Ldf;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldf",
        "<",
        "Landroid/content/Intent;",
        ">;"
    }
.end annotation


# instance fields
.field private final b:I

.field private final c:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0, p1}, Ldf;-><init>(Landroid/content/Context;)V

    .line 97
    iput p2, p0, Lfrm;->b:I

    .line 98
    iput-object p3, p0, Lfrm;->c:Landroid/content/Intent;

    .line 99
    return-void
.end method


# virtual methods
.method public synthetic d()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0}, Lfrm;->f()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public f()Landroid/content/Intent;
    .locals 6

    .prologue
    .line 103
    invoke-virtual {p0}, Lfrm;->n()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iget v1, p0, Lfrm;->b:I

    .line 104
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "account_name"

    .line 105
    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 106
    new-instance v1, Lfsr;

    iget-object v2, p0, Lfrm;->c:Landroid/content/Intent;

    const-string v3, "android.intent.extra.TEXT"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lfsr;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lfsr;->a()Ljava/lang/String;

    move-result-object v1

    .line 107
    new-instance v2, Lfnl;

    .line 108
    invoke-virtual {p0}, Lfrm;->n()Landroid/content/Context;

    move-result-object v3

    new-instance v4, Lkfo;

    const/4 v5, 0x0

    invoke-direct {v4, v0, v5}, Lkfo;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v2, v3, v4, v1}, Lfnl;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;)V

    .line 109
    invoke-virtual {p0}, Lfrm;->n()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lkfd;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    invoke-interface {v0, v2}, Lkfd;->a(Lkff;)V

    .line 110
    invoke-virtual {v2}, Lfnl;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 111
    iget-object v0, p0, Lfrm;->c:Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v3, "authkey"

    .line 113
    invoke-virtual {v2}, Lfnl;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmdf;

    iget-object v0, v0, Lmdf;->a:Lmlp;

    iget-object v0, v0, Lmlp;->a:Ljava/lang/String;

    .line 112
    invoke-virtual {v1, v3, v0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 114
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 115
    iget-object v1, p0, Lfrm;->c:Landroid/content/Intent;

    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 117
    :cond_0
    iget-object v0, p0, Lfrm;->c:Landroid/content/Intent;

    return-object v0
.end method

.method public g()V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lfrm;->c:Landroid/content/Intent;

    const-string v1, "android.intent.extra.TEXT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 123
    const-string v1, "authkey"

    invoke-virtual {v0, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lfrm;->c:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lfrm;->b(Ljava/lang/Object;)V

    .line 128
    :goto_0
    return-void

    .line 126
    :cond_0
    invoke-virtual {p0}, Lfrm;->t()V

    goto :goto_0
.end method
