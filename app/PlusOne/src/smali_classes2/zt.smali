.class final Lzt;
.super Lzr;
.source "PG"


# instance fields
.field private d:Ljava/nio/ByteBuffer;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 694
    invoke-direct {p0}, Lzr;-><init>()V

    .line 696
    const/4 v0, 0x0

    iput-object v0, p0, Lzt;->d:Ljava/nio/ByteBuffer;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 709
    const/4 v0, 0x1

    return v0
.end method

.method public a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 765
    iget-object v0, p0, Lzt;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    move-result-object v0

    return-object v0
.end method

.method public a(Labf;)V
    .locals 5

    .prologue
    .line 700
    invoke-virtual {p1}, Labf;->f()I

    move-result v1

    .line 701
    iget-object v2, p0, Lzt;->a:[I

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 702
    mul-int/2addr v1, v4

    .line 701
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 704
    :cond_0
    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lzt;->d:Ljava/nio/ByteBuffer;

    .line 705
    return-void
.end method

.method public a(Lzr;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 724
    invoke-virtual {p1}, Lzr;->a()I

    move-result v0

    .line 725
    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_0

    .line 726
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lzr;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacw;

    .line 727
    iget-object v1, p0, Lzt;->d:Ljava/nio/ByteBuffer;

    iget-object v2, p0, Lzt;->a:[I

    aget v2, v2, v4

    iget-object v3, p0, Lzt;->a:[I

    aget v3, v3, v5

    invoke-static {v0, v1, v2, v3}, Labi;->a(Lacw;Ljava/nio/ByteBuffer;II)V

    .line 741
    :goto_0
    invoke-virtual {p1}, Lzr;->d()V

    .line 742
    iget-object v0, p0, Lzt;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 743
    iput-boolean v4, p0, Lzt;->b:Z

    .line 744
    return-void

    .line 728
    :cond_0
    and-int/lit8 v1, v0, 0x10

    if-eqz v1, :cond_1

    .line 729
    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Lzr;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 730
    iget-object v1, p0, Lzt;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    .line 731
    iget-object v0, p0, Lzt;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    goto :goto_0

    .line 732
    :cond_1
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_2

    .line 733
    invoke-virtual {p1, v5}, Lzr;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 734
    iget-object v1, p0, Lzt;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 735
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    goto :goto_0

    .line 736
    :cond_2
    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_5

    invoke-static {}, Lzq;->i()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 737
    const/16 v0, 0x20

    invoke-virtual {p1, v0}, Lzr;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/renderscript/Allocation;

    invoke-virtual {p0}, Lzt;->l()I

    move-result v1

    const/16 v2, 0x12d

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lzt;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/renderscript/Allocation;->copyTo([B)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lzt;->l()I

    move-result v1

    const/16 v2, 0xc8

    if-ne v1, v2, :cond_4

    invoke-virtual {p0}, Lzt;->h()I

    move-result v1

    div-int/lit8 v1, v1, 0x4

    new-array v1, v1, [F

    invoke-virtual {v0, v1}, Landroid/renderscript/Allocation;->copyTo([F)V

    iget-object v0, p0, Lzt;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to sync to an allocation with an unsupported element id: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lzt;->l()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 739
    :cond_5
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot sync bytebuffer backing!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 714
    const/4 v0, 0x1

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 719
    const/4 v0, 0x0

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 770
    iget-object v0, p0, Lzt;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 771
    return-void
.end method

.method public e()I
    .locals 1

    .prologue
    .line 775
    const/4 v0, 0x1

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 780
    const/4 v0, 0x1

    return v0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 785
    const/4 v0, 0x0

    iput-object v0, p0, Lzt;->d:Ljava/nio/ByteBuffer;

    .line 786
    return-void
.end method

.method public h()I
    .locals 1

    .prologue
    .line 790
    iget-object v0, p0, Lzt;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    return v0
.end method
