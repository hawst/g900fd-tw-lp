.class public final Lcic;
.super Lcjy;
.source "PG"


# static fields
.field private static final W:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final X:[I

.field private static final Y:[I

.field private static final Z:[I


# instance fields
.field private final aa:Lckf;

.field private ab:Lckj;

.field private ac:Lckg;

.field private final ad:Lcmv;

.field private final ae:Lcmv;

.field private final af:Lcig;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 30
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcic;->X:[I

    .line 38
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcic;->Y:[I

    .line 46
    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcic;->Z:[I

    .line 56
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 57
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Integer;

    .line 59
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    .line 60
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const/16 v3, 0xe

    .line 61
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 57
    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 63
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcic;->W:Ljava/util/List;

    .line 64
    return-void

    .line 30
    :array_0
    .array-data 4
        0x7f020208
        0x7f020207
        0x7f02020c
        0x7f02020b
        0x7f02020a
        0x7f020209
        0x7f02020e
        0x7f02020d
        0x7f020206
        0x7f020205
    .end array-data

    .line 38
    :array_1
    .array-data 4
        0x7f0a0056
        0x7f0a0057
        0x7f0a0058
        0x7f0a0059
        0x7f0a005a
    .end array-data

    .line 46
    :array_2
    .array-data 4
        0x7f0a005b
        0x7f0a005c
        0x7f0a005d
        0x7f0a005e
        0x7f0a005f
        0x7f0a0060
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 27
    invoke-direct {p0}, Lcjy;-><init>()V

    .line 66
    new-instance v0, Lckf;

    const/16 v1, 0xf1

    sget-object v2, Lcic;->X:[I

    invoke-direct {v0, p0, v1, v2}, Lckf;-><init>(Lcjy;I[I)V

    iput-object v0, p0, Lcic;->aa:Lckf;

    .line 73
    new-instance v0, Lcih;

    invoke-direct {v0, p0}, Lcih;-><init>(Lcic;)V

    iput-object v0, p0, Lcic;->ad:Lcmv;

    .line 75
    new-instance v0, Lcii;

    invoke-direct {v0, p0}, Lcii;-><init>(Lcic;)V

    iput-object v0, p0, Lcic;->ae:Lcmv;

    .line 77
    new-instance v0, Lcig;

    invoke-direct {v0, p0}, Lcig;-><init>(Lcic;)V

    iput-object v0, p0, Lcic;->af:Lcig;

    .line 237
    return-void
.end method

.method static synthetic a(Lcic;)Lcig;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcic;->af:Lcig;

    return-object v0
.end method

.method static synthetic b(Lcic;)Lckf;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcic;->aa:Lckf;

    return-object v0
.end method

.method static synthetic c(Lcic;)Lckj;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcic;->ab:Lckj;

    return-object v0
.end method

.method static synthetic d(Lcic;)Lckg;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcic;->ac:Lckg;

    return-object v0
.end method


# virtual methods
.method protected A_()V
    .locals 3

    .prologue
    .line 170
    iget-object v0, p0, Lcic;->ab:Lckj;

    .line 171
    invoke-virtual {p0}, Lcic;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v1

    .line 170
    invoke-virtual {v0, v1}, Lckj;->a(I)V

    .line 172
    iget-object v0, p0, Lcic;->ab:Lckj;

    iget-object v1, p0, Lcic;->ae:Lcmv;

    invoke-virtual {p0, v0, v1}, Lcic;->a(Lcmu;Lcmv;)V

    .line 174
    iget-object v0, p0, Lcic;->ab:Lckj;

    iput-object v0, p0, Lcic;->ac:Lckg;

    .line 175
    return-void
.end method

.method protected X()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 91
    sget-object v0, Lcic;->W:Ljava/util/List;

    return-object v0
.end method

.method protected Z()V
    .locals 3

    .prologue
    .line 162
    iget-object v0, p0, Lcic;->aa:Lckf;

    .line 163
    invoke-virtual {p0}, Lcic;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    const/16 v2, 0xf1

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v1

    .line 162
    invoke-virtual {v0, v1}, Lckf;->a(I)V

    .line 164
    iget-object v0, p0, Lcic;->aa:Lckf;

    iget-object v1, p0, Lcic;->ad:Lcmv;

    invoke-virtual {p0, v0, v1}, Lcic;->a(Lcmu;Lcmv;)V

    .line 166
    iget-object v0, p0, Lcic;->aa:Lckf;

    iput-object v0, p0, Lcic;->ac:Lckg;

    .line 167
    return-void
.end method

.method protected a(Lckp;)V
    .locals 3

    .prologue
    .line 96
    const v0, 0x7f0203a3

    const v1, 0x7f0a009b

    .line 98
    invoke-virtual {p0, v1}, Lcic;->e_(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcid;

    invoke-direct {v2, p0}, Lcid;-><init>(Lcic;)V

    .line 96
    invoke-virtual {p1, v0, v1, v2}, Lckp;->a(ILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 107
    const v0, 0x7f02037e

    const v1, 0x7f0a0055

    .line 109
    invoke-virtual {p0, v1}, Lcic;->e_(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcie;

    invoke-direct {v2, p0}, Lcie;-><init>(Lcic;)V

    .line 107
    invoke-virtual {p1, v0, v1, v2}, Lckp;->a(ILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 117
    return-void
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 81
    new-instance v0, Lhmk;

    sget-object v1, Lonc;->c:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method public b(ILjava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 179
    sparse-switch p1, :sswitch_data_0

    .line 187
    invoke-super {p0, p1, p2}, Lcjy;->b(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 181
    :sswitch_0
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sget-object v1, Lcic;->Z:[I

    aget v0, v1, v0

    invoke-virtual {p0, v0}, Lcic;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 184
    :sswitch_1
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_0

    sget-object v1, Lcic;->Y:[I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    sget-object v1, Lcic;->Y:[I

    aget v0, v1, v0

    invoke-virtual {p0, v0}, Lcic;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    const-string v0, "*UNKNOWN*"

    goto :goto_0

    .line 179
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0xf1 -> :sswitch_1
    .end sparse-switch
.end method

.method public c()I
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x7

    return v0
.end method

.method protected y_()V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 121
    invoke-super {p0}, Lcjy;->y_()V

    .line 123
    iget-object v0, p0, Lcic;->ab:Lckj;

    if-eqz v0, :cond_0

    .line 159
    :goto_0
    return-void

    .line 127
    :cond_0
    invoke-virtual {p0}, Lcic;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 128
    const v1, 0x7f0d0047

    .line 129
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 133
    invoke-virtual {p0}, Lcic;->ah()Lchp;

    move-result-object v0

    invoke-virtual {v0}, Lchp;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    move v2, v1

    move v4, v3

    move v5, v3

    .line 132
    invoke-static/range {v0 .. v5}, Lcom/google/android/libraries/photoeditor/util/BitmapHelper;->createCenterCropBitmap(Landroid/graphics/Bitmap;IIIII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 134
    new-instance v2, Lckj;

    invoke-virtual {p0}, Lcic;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v3

    const/4 v4, 0x3

    invoke-direct {v2, p0, v3, v4, v0}, Lckj;-><init>(Lcjy;Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;ILandroid/graphics/Bitmap;)V

    iput-object v2, p0, Lcic;->ab:Lckj;

    .line 137
    invoke-virtual {p0}, Lcic;->ah()Lchp;

    move-result-object v0

    invoke-virtual {v0}, Lchp;->d()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcic;->a(ILandroid/graphics/Bitmap;)Landroid/graphics/Rect;

    move-result-object v1

    .line 139
    invoke-virtual {p0}, Lcic;->an()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lhap;

    .line 141
    new-instance v2, Lcif;

    invoke-direct {v2, p0, v1}, Lcif;-><init>(Lcic;Landroid/graphics/Rect;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v2, v1}, Lhap;->a(Lhbl;Z)V

    goto :goto_0
.end method
