.class public final Lcme;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:I

.field private final b:F

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcmf;",
            ">;"
        }
    .end annotation
.end field

.field private d:J

.field private e:F

.field private f:Landroid/graphics/PointF;


# direct methods
.method constructor <init>(IF)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 267
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 268
    cmpg-float v0, p2, v1

    if-gtz v0, :cond_0

    .line 269
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "defaultRadius should be a positive value."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 271
    :cond_0
    iput p1, p0, Lcme;->a:I

    .line 272
    iput p2, p0, Lcme;->b:F

    .line 273
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcme;->c:Ljava/util/List;

    .line 274
    iput p2, p0, Lcme;->e:F

    .line 275
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v1, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lcme;->f:Landroid/graphics/PointF;

    .line 276
    return-void
.end method

.method constructor <init>(Lcmd;)V
    .locals 4

    .prologue
    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279
    iget v0, p1, Lcmd;->b:I

    iput v0, p0, Lcme;->a:I

    .line 280
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p1, Lcmd;->a:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcme;->c:Ljava/util/List;

    .line 281
    iget-object v0, p0, Lcme;->c:Ljava/util/List;

    iget-object v1, p0, Lcme;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmf;

    .line 282
    iget v1, v0, Lcmf;->c:F

    iput v1, p0, Lcme;->b:F

    .line 283
    iget-wide v2, v0, Lcmf;->e:J

    iput-wide v2, p0, Lcme;->d:J

    .line 284
    iget v1, v0, Lcmf;->b:F

    iput v1, p0, Lcme;->e:F

    .line 285
    iget v1, v0, Lcmf;->a:F

    invoke-virtual {v0, v1}, Lcmf;->a(F)Landroid/graphics/PointF;

    move-result-object v0

    iput-object v0, p0, Lcme;->f:Landroid/graphics/PointF;

    .line 286
    return-void
.end method


# virtual methods
.method public a()Lcmd;
    .locals 3

    .prologue
    .line 380
    new-instance v0, Lcmd;

    iget v1, p0, Lcme;->a:I

    iget-object v2, p0, Lcme;->c:Ljava/util/List;

    invoke-direct {v0, v1, v2}, Lcmd;-><init>(ILjava/util/List;)V

    return-object v0
.end method

.method public a(F)Lcme;
    .locals 2

    .prologue
    .line 340
    iget-object v0, p0, Lcme;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot set initial position after the finger has already moved."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 344
    :cond_0
    iput p1, p0, Lcme;->e:F

    .line 345
    return-object p0
.end method

.method public a(FF)Lcme;
    .locals 2

    .prologue
    .line 331
    iget-object v0, p0, Lcme;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot set initial position after the finger has already moved."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 335
    :cond_0
    iget-object v0, p0, Lcme;->f:Landroid/graphics/PointF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/PointF;->set(FF)V

    .line 336
    return-object p0
.end method

.method public a(J)Lcme;
    .locals 5

    .prologue
    .line 312
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 313
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Start time must be at the beginning of the gesture or in the future."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 317
    :cond_0
    iget-object v0, p0, Lcme;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 318
    iget-object v0, p0, Lcme;->c:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmf;

    iget-wide v0, v0, Lcmf;->d:J

    sub-long v2, p1, v0

    .line 319
    iget-object v0, p0, Lcme;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    .line 320
    iget-object v4, p0, Lcme;->c:Ljava/util/List;

    iget-object v0, p0, Lcme;->c:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmf;

    invoke-virtual {v0, v2, v3}, Lcmf;->a(J)Lcmf;

    move-result-object v0

    invoke-interface {v4, v1, v0}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 319
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 322
    :cond_1
    iget-object v0, p0, Lcme;->c:Ljava/util/List;

    iget-object v1, p0, Lcme;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmf;

    iget-wide v0, v0, Lcmf;->e:J

    iput-wide v0, p0, Lcme;->d:J

    .line 327
    :goto_1
    return-object p0

    .line 324
    :cond_2
    iput-wide p1, p0, Lcme;->d:J

    goto :goto_1
.end method

.method a(Landroid/graphics/Path;JLandroid/animation/TimeInterpolator;FF)Lcme;
    .locals 14

    .prologue
    .line 370
    new-instance v3, Lcmf;

    iget-wide v6, p0, Lcme;->d:J

    iget v12, p0, Lcme;->b:F

    move-object/from16 v4, p4

    move-object v5, p1

    move-wide/from16 v8, p2

    move/from16 v10, p5

    move/from16 v11, p6

    invoke-direct/range {v3 .. v12}, Lcmf;-><init>(Landroid/animation/TimeInterpolator;Landroid/graphics/Path;JJFFF)V

    .line 372
    iget-object v2, p0, Lcme;->c:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 373
    iget-wide v4, p0, Lcme;->d:J

    add-long v4, v4, p2

    iput-wide v4, p0, Lcme;->d:J

    .line 374
    move/from16 v0, p6

    iput v0, p0, Lcme;->e:F

    .line 375
    iget v2, v3, Lcmf;->a:F

    invoke-virtual {v3, v2}, Lcmf;->a(F)Landroid/graphics/PointF;

    move-result-object v2

    iput-object v2, p0, Lcme;->f:Landroid/graphics/PointF;

    .line 376
    return-object p0
.end method

.method public a(FFJ)Lcmg;
    .locals 3

    .prologue
    .line 349
    iget-object v0, p0, Lcme;->f:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    add-float/2addr v0, p1

    iget-object v1, p0, Lcme;->f:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    add-float/2addr v1, p2

    invoke-virtual {p0, v0, v1, p3, p4}, Lcme;->b(FFJ)Lcmg;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/graphics/Path;J)Lcmg;
    .locals 8

    .prologue
    .line 365
    new-instance v1, Lcmg;

    iget v6, p0, Lcme;->e:F

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    invoke-direct/range {v1 .. v6}, Lcmg;-><init>(Lcme;Landroid/graphics/Path;JF)V

    return-object v1
.end method

.method public b(FFJ)Lcmg;
    .locals 3

    .prologue
    .line 353
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 354
    iget-object v1, p0, Lcme;->f:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcme;->f:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    .line 355
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Path;->lineTo(FF)V

    .line 356
    invoke-virtual {p0, v0, p3, p4}, Lcme;->a(Landroid/graphics/Path;J)Lcmg;

    move-result-object v0

    return-object v0
.end method

.method public b(J)Lcmg;
    .locals 5

    .prologue
    const v4, 0x3a83126f    # 0.001f

    .line 360
    iget-object v0, p0, Lcme;->f:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcme;->f:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->moveTo(FF)V

    add-float v3, v0, v4

    add-float/2addr v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {p0, v2, p1, p2}, Lcme;->a(Landroid/graphics/Path;J)Lcmg;

    move-result-object v0

    return-object v0
.end method
