.class public final Lael;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# instance fields
.field private synthetic a:Landroidx/media/filterpacks/video/MediaPlayerSource;


# direct methods
.method public constructor <init>(Landroidx/media/filterpacks/video/MediaPlayerSource;)V
    .locals 0

    .prologue
    .line 372
    iput-object p1, p0, Lael;->a:Landroidx/media/filterpacks/video/MediaPlayerSource;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 3

    .prologue
    .line 375
    iget-object v0, p0, Lael;->a:Landroidx/media/filterpacks/video/MediaPlayerSource;

    const-string v1, "MediaPlayer has completed playback"

    invoke-static {v0, v1}, Landroidx/media/filterpacks/video/MediaPlayerSource;->a(Landroidx/media/filterpacks/video/MediaPlayerSource;Ljava/lang/String;)V

    .line 376
    iget-object v0, p0, Lael;->a:Landroidx/media/filterpacks/video/MediaPlayerSource;

    invoke-static {v0}, Landroidx/media/filterpacks/video/MediaPlayerSource;->b(Landroidx/media/filterpacks/video/MediaPlayerSource;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 377
    :try_start_0
    iget-object v0, p0, Lael;->a:Landroidx/media/filterpacks/video/MediaPlayerSource;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Landroidx/media/filterpacks/video/MediaPlayerSource;->a(Landroidx/media/filterpacks/video/MediaPlayerSource;Z)Z

    .line 378
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 379
    iget-object v0, p0, Lael;->a:Landroidx/media/filterpacks/video/MediaPlayerSource;

    invoke-static {v0}, Landroidx/media/filterpacks/video/MediaPlayerSource;->c(Landroidx/media/filterpacks/video/MediaPlayerSource;)V

    .line 380
    return-void

    .line 378
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
