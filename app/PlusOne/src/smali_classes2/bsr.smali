.class public Lbsr;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/media/AudioTrack;

.field private final c:I

.field private d:I

.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lbsr;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbsr;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(IIIIII)V
    .locals 7

    .prologue
    .line 33
    new-instance v0, Landroid/media/AudioTrack;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    invoke-direct {p0, v0, p5}, Lbsr;-><init>(Landroid/media/AudioTrack;I)V

    .line 36
    return-void
.end method

.method constructor <init>(Landroid/media/AudioTrack;I)V
    .locals 2

    .prologue
    const/4 v0, 0x2

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    if-nez p1, :cond_0

    .line 41
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "audioTrack cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 43
    :cond_0
    iput-object p1, p0, Lbsr;->b:Landroid/media/AudioTrack;

    .line 44
    iput p2, p0, Lbsr;->c:I

    .line 45
    const/4 v1, 0x0

    iput v1, p0, Lbsr;->d:I

    .line 46
    invoke-virtual {p1}, Landroid/media/AudioTrack;->getAudioFormat()I

    move-result v1

    if-ne v1, v0, :cond_1

    .line 47
    :goto_0
    invoke-virtual {p1}, Landroid/media/AudioTrack;->getChannelCount()I

    move-result v1

    mul-int/2addr v0, v1

    iput v0, p0, Lbsr;->e:I

    .line 48
    return-void

    .line 46
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a([BII)I
    .locals 6

    .prologue
    .line 59
    invoke-virtual {p0}, Lbsr;->e()I

    move-result v0

    .line 60
    iget v1, p0, Lbsr;->c:I

    sub-int v0, v1, v0

    .line 61
    if-lez v0, :cond_2

    .line 62
    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 63
    iget-object v0, p0, Lbsr;->b:Landroid/media/AudioTrack;

    invoke-virtual {v0, p1, p2, v1}, Landroid/media/AudioTrack;->write([BII)I

    move-result v0

    .line 64
    if-gez v0, :cond_0

    .line 65
    sget-object v2, Lbsr;->a:Ljava/lang/String;

    array-length v3, p1

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x75

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Error writing bytes: error code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", audioData size "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", offset "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", bytesToWrite "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Error writing bytes to the underlying AudioTrack"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_0
    if-eq v0, v1, :cond_1

    .line 71
    sget-object v2, Lbsr;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x38

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Expected to write "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " but only wrote "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 73
    :cond_1
    iget v1, p0, Lbsr;->d:I

    add-int/2addr v1, v0

    iput v1, p0, Lbsr;->d:I

    .line 76
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lbsr;->b:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V

    .line 91
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lbsr;->b:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->pause()V

    .line 98
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lbsr;->b:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->release()V

    .line 105
    return-void
.end method

.method public d()J
    .locals 4

    .prologue
    .line 112
    iget-object v0, p0, Lbsr;->b:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getPlaybackHeadPosition()I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0xf4240

    mul-long/2addr v0, v2

    iget-object v2, p0, Lbsr;->b:Landroid/media/AudioTrack;

    invoke-virtual {v2}, Landroid/media/AudioTrack;->getSampleRate()I

    move-result v2

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public e()I
    .locals 3

    .prologue
    .line 120
    iget v0, p0, Lbsr;->d:I

    iget-object v1, p0, Lbsr;->b:Landroid/media/AudioTrack;

    invoke-virtual {v1}, Landroid/media/AudioTrack;->getPlaybackHeadPosition()I

    move-result v1

    iget v2, p0, Lbsr;->e:I

    mul-int/2addr v1, v2

    sub-int/2addr v0, v1

    return v0
.end method

.method public f()J
    .locals 4

    .prologue
    .line 124
    invoke-virtual {p0}, Lbsr;->e()I

    move-result v0

    int-to-long v0, v0

    const-wide/32 v2, 0xf4240

    mul-long/2addr v0, v2

    iget v2, p0, Lbsr;->e:I

    iget-object v3, p0, Lbsr;->b:Landroid/media/AudioTrack;

    invoke-virtual {v3}, Landroid/media/AudioTrack;->getSampleRate()I

    move-result v3

    mul-int/2addr v2, v3

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lbsr;->c:I

    return v0
.end method
