.class public final Lilv;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lilu;


# instance fields
.field private a:Lgrp;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private f()V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lilv;->a:Lgrp;

    if-nez v0, :cond_0

    .line 27
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must call populate before accessing member variables."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_0
    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lilv;->f()V

    .line 39
    iget-object v0, p0, Lilv;->a:Lgrp;

    invoke-virtual {v0}, Lgrp;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 33
    invoke-static {p2, p1}, Lgry;->a(Landroid/content/Intent;Landroid/content/Context;)Lgrp;

    move-result-object v0

    iput-object v0, p0, Lilv;->a:Lgrp;

    .line 34
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Lilv;->f()V

    .line 45
    iget-object v0, p0, Lilv;->a:Lgrp;

    invoke-virtual {v0}, Lgrp;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0}, Lilv;->f()V

    .line 57
    iget-object v0, p0, Lilv;->a:Lgrp;

    invoke-virtual {v0}, Lgrp;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d()Ling;
    .locals 6

    .prologue
    .line 62
    invoke-direct {p0}, Lilv;->f()V

    .line 63
    iget-object v0, p0, Lilv;->a:Lgrp;

    invoke-virtual {v0}, Lgrp;->e()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v0

    .line 64
    new-instance v1, Ling;

    iget-wide v2, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v4, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-direct {v1, v2, v3, v4, v5}, Ling;-><init>(DD)V

    return-object v1
.end method

.method public e()Z
    .locals 4

    .prologue
    .line 93
    invoke-direct {p0}, Lilv;->f()V

    .line 94
    iget-object v0, p0, Lilv;->a:Lgrp;

    invoke-virtual {v0}, Lgrp;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/PlaceType;

    .line 95
    sget-object v2, Lcom/google/android/gms/location/places/PlaceType;->a:Lcom/google/android/gms/location/places/PlaceType;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/google/android/gms/location/places/PlaceType;->b:Lcom/google/android/gms/location/places/PlaceType;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/google/android/gms/location/places/PlaceType;->c:Lcom/google/android/gms/location/places/PlaceType;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/google/android/gms/location/places/PlaceType;->f:Lcom/google/android/gms/location/places/PlaceType;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/google/android/gms/location/places/PlaceType;->l:Lcom/google/android/gms/location/places/PlaceType;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/google/android/gms/location/places/PlaceType;->m:Lcom/google/android/gms/location/places/PlaceType;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/google/android/gms/location/places/PlaceType;->n:Lcom/google/android/gms/location/places/PlaceType;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/google/android/gms/location/places/PlaceType;->o:Lcom/google/android/gms/location/places/PlaceType;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/google/android/gms/location/places/PlaceType;->p:Lcom/google/android/gms/location/places/PlaceType;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/google/android/gms/location/places/PlaceType;->q:Lcom/google/android/gms/location/places/PlaceType;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/google/android/gms/location/places/PlaceType;->d:Lcom/google/android/gms/location/places/PlaceType;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/google/android/gms/location/places/PlaceType;->g:Lcom/google/android/gms/location/places/PlaceType;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/google/android/gms/location/places/PlaceType;->j:Lcom/google/android/gms/location/places/PlaceType;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/google/android/gms/location/places/PlaceType;->k:Lcom/google/android/gms/location/places/PlaceType;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/google/android/gms/location/places/PlaceType;->h:Lcom/google/android/gms/location/places/PlaceType;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/google/android/gms/location/places/PlaceType;->e:Lcom/google/android/gms/location/places/PlaceType;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/google/android/gms/location/places/PlaceType;->r:Lcom/google/android/gms/location/places/PlaceType;

    if-eq v0, v2, :cond_1

    sget-object v2, Lcom/google/android/gms/location/places/PlaceType;->i:Lcom/google/android/gms/location/places/PlaceType;

    if-eq v0, v2, :cond_1

    .line 113
    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceType;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "premise"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 114
    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceType;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "synthetic_geocode"

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 115
    invoke-virtual {v0}, Lcom/google/android/gms/location/places/PlaceType;->a()Ljava/lang/String;

    move-result-object v0

    const-string v2, "country"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    :cond_1
    const/4 v0, 0x1

    .line 119
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
