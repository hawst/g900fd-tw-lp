.class public Lgmh;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/util/Locale;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lgmh;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    iput-object v0, p0, Lgmh;->a:Ljava/util/Locale;

    new-instance v0, Lcom/google/android/gms/internal/nw;

    iget-object v1, p0, Lgmh;->a:Ljava/util/Locale;

    invoke-direct {v0, p1, v1, p2}, Lcom/google/android/gms/internal/nw;-><init>(Ljava/lang/String;Ljava/util/Locale;Ljava/lang/String;)V

    return-void
.end method
