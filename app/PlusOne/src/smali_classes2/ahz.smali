.class public final enum Lahz;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lahz;",
        ">;"
    }
.end annotation


# static fields
.field private static enum c:Lahz;

.field private static enum d:Lahz;

.field private static enum e:Lahz;

.field private static enum f:Lahz;

.field private static enum g:Lahz;

.field private static enum h:Lahz;

.field private static enum i:Lahz;

.field private static enum j:Lahz;

.field private static enum k:Lahz;

.field private static enum l:Lahz;

.field private static enum m:Lahz;

.field private static enum n:Lahz;

.field private static enum o:Lahz;

.field private static enum p:Lahz;

.field private static enum q:Lahz;

.field private static enum r:Lahz;

.field private static enum s:Lahz;

.field private static final synthetic t:[Lahz;


# instance fields
.field public final a:Laia;

.field public final b:Lbmz;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 83
    new-instance v0, Lahz;

    const-string v1, "SHARPNESS"

    sget-object v2, Laia;->a:Laia;

    sget-object v3, Lbmz;->f:Lbmz;

    invoke-direct {v0, v1, v5, v2, v3}, Lahz;-><init>(Ljava/lang/String;ILaia;Lbmz;)V

    sput-object v0, Lahz;->c:Lahz;

    .line 86
    new-instance v0, Lahz;

    const-string v1, "FACE_SCORE"

    sget-object v2, Laia;->a:Laia;

    sget-object v3, Lbmz;->g:Lbmz;

    invoke-direct {v0, v1, v6, v2, v3}, Lahz;-><init>(Ljava/lang/String;ILaia;Lbmz;)V

    sput-object v0, Lahz;->d:Lahz;

    .line 89
    new-instance v0, Lahz;

    const-string v1, "SALIENCY"

    sget-object v2, Laia;->a:Laia;

    sget-object v3, Lbmz;->i:Lbmz;

    invoke-direct {v0, v1, v7, v2, v3}, Lahz;-><init>(Ljava/lang/String;ILaia;Lbmz;)V

    sput-object v0, Lahz;->e:Lahz;

    .line 92
    new-instance v0, Lahz;

    const-string v1, "SALIENCY_MAP_CUMULATIVE_VERTICAL_PROJECTION"

    sget-object v2, Laia;->a:Laia;

    sget-object v3, Lbmz;->v:Lbmz;

    invoke-direct {v0, v1, v8, v2, v3}, Lahz;-><init>(Ljava/lang/String;ILaia;Lbmz;)V

    sput-object v0, Lahz;->f:Lahz;

    .line 96
    new-instance v0, Lahz;

    const-string v1, "SALIENCY_MAP_CUMULATIVE_HORIZONTAL_PROJECTION"

    sget-object v2, Laia;->a:Laia;

    sget-object v3, Lbmz;->w:Lbmz;

    invoke-direct {v0, v1, v9, v2, v3}, Lahz;-><init>(Ljava/lang/String;ILaia;Lbmz;)V

    sput-object v0, Lahz;->g:Lahz;

    .line 100
    new-instance v0, Lahz;

    const-string v1, "STABILIZATION"

    const/4 v2, 0x5

    sget-object v3, Laia;->c:Laia;

    sget-object v4, Lbmz;->a:Lbmz;

    invoke-direct {v0, v1, v2, v3, v4}, Lahz;-><init>(Ljava/lang/String;ILaia;Lbmz;)V

    sput-object v0, Lahz;->h:Lahz;

    .line 103
    new-instance v0, Lahz;

    const-string v1, "AUTO_CORRECT"

    const/4 v2, 0x6

    sget-object v3, Laia;->d:Laia;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lahz;-><init>(Ljava/lang/String;ILaia;Lbmz;)V

    sput-object v0, Lahz;->i:Lahz;

    .line 106
    new-instance v0, Lahz;

    const-string v1, "AUDIO_RMS"

    const/4 v2, 0x7

    sget-object v3, Laia;->a:Laia;

    sget-object v4, Lbmz;->k:Lbmz;

    invoke-direct {v0, v1, v2, v3, v4}, Lahz;-><init>(Ljava/lang/String;ILaia;Lbmz;)V

    sput-object v0, Lahz;->j:Lahz;

    .line 109
    new-instance v0, Lahz;

    const-string v1, "NEW_CHROMA_HISTOGRAM"

    const/16 v2, 0x8

    sget-object v3, Laia;->b:Laia;

    sget-object v4, Lbmz;->m:Lbmz;

    invoke-direct {v0, v1, v2, v3, v4}, Lahz;-><init>(Ljava/lang/String;ILaia;Lbmz;)V

    sput-object v0, Lahz;->k:Lahz;

    .line 112
    new-instance v0, Lahz;

    const-string v1, "NEW_COLORFULNESS"

    const/16 v2, 0x9

    sget-object v3, Laia;->a:Laia;

    sget-object v4, Lbmz;->l:Lbmz;

    invoke-direct {v0, v1, v2, v3, v4}, Lahz;-><init>(Ljava/lang/String;ILaia;Lbmz;)V

    sput-object v0, Lahz;->l:Lahz;

    .line 115
    new-instance v0, Lahz;

    const-string v1, "AUDIO_MFCC"

    const/16 v2, 0xa

    sget-object v3, Laia;->b:Laia;

    sget-object v4, Lbmz;->n:Lbmz;

    invoke-direct {v0, v1, v2, v3, v4}, Lahz;-><init>(Ljava/lang/String;ILaia;Lbmz;)V

    sput-object v0, Lahz;->m:Lahz;

    .line 118
    new-instance v0, Lahz;

    const-string v1, "KEY_FRAME_NUMBER"

    const/16 v2, 0xb

    sget-object v3, Laia;->e:Laia;

    sget-object v4, Lbmz;->o:Lbmz;

    invoke-direct {v0, v1, v2, v3, v4}, Lahz;-><init>(Ljava/lang/String;ILaia;Lbmz;)V

    sput-object v0, Lahz;->n:Lahz;

    .line 121
    new-instance v0, Lahz;

    const-string v1, "STABILIZATION_DISPLACEMENT"

    const/16 v2, 0xc

    sget-object v3, Laia;->a:Laia;

    sget-object v4, Lbmz;->p:Lbmz;

    invoke-direct {v0, v1, v2, v3, v4}, Lahz;-><init>(Ljava/lang/String;ILaia;Lbmz;)V

    sput-object v0, Lahz;->o:Lahz;

    .line 124
    new-instance v0, Lahz;

    const-string v1, "STABILIZATION_INTERFRAME_TRANSFORM"

    const/16 v2, 0xd

    sget-object v3, Laia;->a:Laia;

    sget-object v4, Lbmz;->q:Lbmz;

    invoke-direct {v0, v1, v2, v3, v4}, Lahz;-><init>(Ljava/lang/String;ILaia;Lbmz;)V

    sput-object v0, Lahz;->p:Lahz;

    .line 128
    new-instance v0, Lahz;

    const-string v1, "AUDIO_DELTA_MFCC"

    const/16 v2, 0xe

    sget-object v3, Laia;->b:Laia;

    sget-object v4, Lbmz;->s:Lbmz;

    invoke-direct {v0, v1, v2, v3, v4}, Lahz;-><init>(Ljava/lang/String;ILaia;Lbmz;)V

    sput-object v0, Lahz;->q:Lahz;

    .line 131
    new-instance v0, Lahz;

    const-string v1, "NEW_MOTION_SALIENCY"

    const/16 v2, 0xf

    sget-object v3, Laia;->a:Laia;

    sget-object v4, Lbmz;->t:Lbmz;

    invoke-direct {v0, v1, v2, v3, v4}, Lahz;-><init>(Ljava/lang/String;ILaia;Lbmz;)V

    sput-object v0, Lahz;->r:Lahz;

    .line 134
    new-instance v0, Lahz;

    const-string v1, "AUDIO_PEAK_AMPLITUDE"

    const/16 v2, 0x10

    sget-object v3, Laia;->a:Laia;

    sget-object v4, Lbmz;->u:Lbmz;

    invoke-direct {v0, v1, v2, v3, v4}, Lahz;-><init>(Ljava/lang/String;ILaia;Lbmz;)V

    sput-object v0, Lahz;->s:Lahz;

    .line 81
    const/16 v0, 0x11

    new-array v0, v0, [Lahz;

    sget-object v1, Lahz;->c:Lahz;

    aput-object v1, v0, v5

    sget-object v1, Lahz;->d:Lahz;

    aput-object v1, v0, v6

    sget-object v1, Lahz;->e:Lahz;

    aput-object v1, v0, v7

    sget-object v1, Lahz;->f:Lahz;

    aput-object v1, v0, v8

    sget-object v1, Lahz;->g:Lahz;

    aput-object v1, v0, v9

    const/4 v1, 0x5

    sget-object v2, Lahz;->h:Lahz;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lahz;->i:Lahz;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lahz;->j:Lahz;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lahz;->k:Lahz;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lahz;->l:Lahz;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lahz;->m:Lahz;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lahz;->n:Lahz;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lahz;->o:Lahz;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lahz;->p:Lahz;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lahz;->q:Lahz;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lahz;->r:Lahz;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lahz;->s:Lahz;

    aput-object v2, v0, v1

    sput-object v0, Lahz;->t:[Lahz;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILaia;Lbmz;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laia;",
            "Lbmz;",
            ")V"
        }
    .end annotation

    .prologue
    .line 145
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 146
    iput-object p3, p0, Lahz;->a:Laia;

    .line 147
    iput-object p4, p0, Lahz;->b:Lbmz;

    .line 148
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lahz;
    .locals 1

    .prologue
    .line 81
    const-class v0, Lahz;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lahz;

    return-object v0
.end method

.method public static values()[Lahz;
    .locals 1

    .prologue
    .line 81
    sget-object v0, Lahz;->t:[Lahz;

    invoke-virtual {v0}, [Lahz;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lahz;

    return-object v0
.end method
