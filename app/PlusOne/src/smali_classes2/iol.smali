.class public final Liol;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Liok;


# static fields
.field private static final a:Likg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Likg",
            "<",
            "Linu;",
            "Lgzb;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Likg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Likg",
            "<",
            "Lijs;",
            "Lggf;",
            ">;"
        }
    .end annotation
.end field

.field private static final c:Likg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Likg",
            "<",
            "Lijs;",
            "Lggf;",
            ">;"
        }
    .end annotation
.end field

.field private static final d:Likg;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Likg",
            "<",
            "Lipp;",
            "Lgzc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Liom;

    invoke-direct {v0}, Liom;-><init>()V

    sput-object v0, Liol;->a:Likg;

    .line 31
    new-instance v0, Lion;

    invoke-direct {v0}, Lion;-><init>()V

    sput-object v0, Liol;->b:Likg;

    .line 40
    new-instance v0, Lioo;

    invoke-direct {v0}, Lioo;-><init>()V

    sput-object v0, Liol;->c:Likg;

    .line 50
    new-instance v0, Liop;

    invoke-direct {v0}, Liop;-><init>()V

    sput-object v0, Liol;->d:Likg;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Lipj;)Lgfz;
    .locals 1

    .prologue
    .line 108
    instance-of v0, p0, Lipk;

    if-eqz v0, :cond_0

    check-cast p0, Lipk;

    .line 109
    invoke-virtual {p0}, Lipk;->e()Lgfz;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lipj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lijq;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lipj;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lijq",
            "<",
            "Lijs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 83
    sget-object v0, Lgzf;->d:Lgza;

    .line 84
    invoke-static {p1}, Liol;->a(Lipj;)Lgfz;

    move-result-object v1

    .line 83
    invoke-interface {v0, v1, p2, p3, p4}, Lgza;->a(Lgfz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lggd;

    move-result-object v0

    .line 86
    new-instance v1, Lijr;

    sget-object v2, Liol;->c:Likg;

    invoke-direct {v1, v0, v2}, Lijr;-><init>(Lggd;Likg;)V

    return-object v1
.end method

.method public a(Lipj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lijq;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lipj;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lijq",
            "<",
            "Linu;",
            ">;"
        }
    .end annotation

    .prologue
    .line 61
    sget-object v0, Lgzf;->d:Lgza;

    .line 62
    invoke-static {p1}, Liol;->a(Lipj;)Lgfz;

    move-result-object v1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 61
    invoke-interface/range {v0 .. v5}, Lgza;->a(Lgfz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lggd;

    move-result-object v0

    .line 64
    new-instance v1, Lijr;

    sget-object v2, Liol;->a:Likg;

    invoke-direct {v1, v0, v2}, Lijr;-><init>(Lggd;Likg;)V

    return-object v1
.end method

.method public a(Lipj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)Lijq;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lipj;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            ")",
            "Lijq",
            "<",
            "Lijs;",
            ">;"
        }
    .end annotation

    .prologue
    .line 72
    sget-object v0, Lgzf;->d:Lgza;

    .line 73
    invoke-static {p1}, Liol;->a(Lipj;)Lgfz;

    move-result-object v1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    .line 72
    invoke-interface/range {v0 .. v7}, Lgza;->a(Lgfz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)Lggd;

    move-result-object v0

    .line 76
    new-instance v1, Lijr;

    sget-object v2, Liol;->b:Likg;

    invoke-direct {v1, v0, v2}, Lijr;-><init>(Lggd;Likg;)V

    return-object v1
.end method

.method public a(Lipj;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lijq;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lipj;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lijq",
            "<",
            "Lipp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    sget-object v0, Lgzf;->d:Lgza;

    .line 95
    invoke-static {p1}, Liol;->a(Lipj;)Lgfz;

    move-result-object v1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-interface/range {v0 .. v6}, Lgza;->a(Lgfz;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lggd;

    move-result-object v0

    .line 98
    new-instance v1, Lijr;

    sget-object v2, Liol;->d:Likg;

    invoke-direct {v1, v0, v2}, Lijr;-><init>(Lggd;Likg;)V

    return-object v1
.end method
