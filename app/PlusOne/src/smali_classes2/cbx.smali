.class public final Lcbx;
.super Landroid/widget/RelativeLayout;
.source "PG"


# instance fields
.field private a:Landroid/view/View;

.field private b:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcbx;->b:I

    .line 30
    const/16 v0, 0x11

    invoke-virtual {p0, v0}, Lcbx;->setGravity(I)V

    .line 31
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 38
    iget v0, p0, Lcbx;->b:I

    if-ne v0, v1, :cond_0

    .line 46
    :goto_0
    return-void

    .line 41
    :cond_0
    iput v1, p0, Lcbx;->b:I

    .line 43
    invoke-virtual {p0}, Lcbx;->removeAllViews()V

    .line 44
    invoke-virtual {p0}, Lcbx;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04003a

    invoke-static {v0, v1, p0}, Lcbx;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 45
    const v0, 0x7f10017d

    invoke-virtual {p0, v0}, Lcbx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcbx;->a:Landroid/view/View;

    goto :goto_0
.end method

.method public b()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 53
    iget v0, p0, Lcbx;->b:I

    if-ne v0, v1, :cond_0

    .line 61
    :goto_0
    return-void

    .line 56
    :cond_0
    iput v1, p0, Lcbx;->b:I

    .line 58
    invoke-virtual {p0}, Lcbx;->removeAllViews()V

    .line 59
    invoke-virtual {p0}, Lcbx;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040224

    invoke-static {v0, v1, p0}, Lcbx;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 60
    const v0, 0x7f10017d

    invoke-virtual {p0, v0}, Lcbx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcbx;->a:Landroid/view/View;

    goto :goto_0
.end method

.method public c()V
    .locals 7

    .prologue
    const/4 v1, 0x4

    const/4 v6, 0x1

    .line 68
    iget v0, p0, Lcbx;->b:I

    if-ne v0, v1, :cond_0

    .line 80
    :goto_0
    return-void

    .line 71
    :cond_0
    iput v1, p0, Lcbx;->b:I

    .line 73
    invoke-virtual {p0}, Lcbx;->removeAllViews()V

    .line 74
    invoke-virtual {p0}, Lcbx;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f04022b

    invoke-static {v0, v1, p0}, Lcbx;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 75
    const v1, 0x7f10061e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 76
    invoke-virtual {p0}, Lcbx;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f110001

    new-array v3, v6, [Ljava/lang/Object;

    const/4 v4, 0x0

    .line 77
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 76
    invoke-virtual {v1, v2, v6, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    const v0, 0x7f10017d

    invoke-virtual {p0, v0}, Lcbx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcbx;->a:Landroid/view/View;

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 86
    iget v0, p0, Lcbx;->b:I

    if-ne v0, v1, :cond_0

    .line 94
    :goto_0
    return-void

    .line 89
    :cond_0
    iput v1, p0, Lcbx;->b:I

    .line 91
    invoke-virtual {p0}, Lcbx;->removeAllViews()V

    .line 92
    invoke-virtual {p0}, Lcbx;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f040229

    invoke-static {v0, v1, p0}, Lcbx;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 93
    const v0, 0x7f10017d

    invoke-virtual {p0, v0}, Lcbx;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcbx;->a:Landroid/view/View;

    goto :goto_0
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcbx;->a:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 99
    return-void
.end method
