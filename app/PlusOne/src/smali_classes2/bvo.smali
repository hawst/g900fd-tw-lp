.class public final Lbvo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbvr;


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbvp;",
            ">;"
        }
    .end annotation
.end field

.field private final b:F


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;F)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbbd;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Float;",
            ">;F)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const-string v0, "features"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 37
    const-string v0, "weights"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 38
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const-string v1, "features.size()"

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    const-string v3, "weights.size()"

    invoke-static {v0, v1, v2, v3}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 39
    iput p3, p0, Lbvo;->b:F

    .line 40
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 41
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 42
    new-instance v3, Lbvp;

    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbbd;

    invoke-direct {v3, v4, v0}, Lbvp;-><init>(FLbbd;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 41
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 44
    :cond_0
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbvo;->a:Ljava/util/List;

    .line 45
    return-void
.end method


# virtual methods
.method public a(Lbmr;)F
    .locals 4

    .prologue
    .line 49
    iget v0, p0, Lbvo;->b:F

    .line 50
    iget-object v1, p0, Lbvo;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvp;

    .line 51
    iget-object v3, v0, Lbvp;->b:Lbbd;

    invoke-interface {v3, p1}, Lbbd;->a(Lbmr;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 52
    iget v3, v0, Lbvp;->a:F

    iget-object v0, v0, Lbvp;->b:Lbbd;

    invoke-interface {v0, p1}, Lbbd;->b(Lbmr;)F

    move-result v0

    mul-float/2addr v0, v3

    add-float/2addr v0, v1

    :goto_1
    move v1, v0

    .line 54
    goto :goto_0

    .line 55
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lbvo;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
