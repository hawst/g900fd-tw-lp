.class public final Lhlu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhlf;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lhlh;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lhlu;->a:Landroid/content/Context;

    .line 26
    const-class v0, Lhlh;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlh;

    iput-object v0, p0, Lhlu;->b:Lhlh;

    .line 27
    return-void
.end method

.method private a(I)Lhlo;
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lhlo;

    iget-object v1, p0, Lhlu;->a:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lhlo;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method


# virtual methods
.method public a(ILjava/lang/String;Ljava/util/Collection;Lhle;)J
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lhle;",
            ")J"
        }
    .end annotation

    .prologue
    .line 37
    invoke-direct {p0, p1}, Lhlu;->a(I)Lhlo;

    move-result-object v0

    .line 38
    invoke-virtual {v0, p2, p3}, Lhlo;->a(Ljava/lang/String;Ljava/util/Collection;)J

    move-result-wide v0

    .line 39
    invoke-static {p1, p2, v0, v1}, Lhkv;->a(ILjava/lang/String;J)Lhkv;

    move-result-object v2

    .line 40
    if-eqz p4, :cond_0

    .line 41
    invoke-virtual {p0, p4, v2}, Lhlu;->a(Lhle;Lhkv;)V

    .line 43
    :cond_0
    iget-object v3, p0, Lhlu;->b:Lhlh;

    invoke-virtual {v3, v2}, Lhlh;->a(Lhkv;)V

    .line 44
    iget-object v2, p0, Lhlu;->a:Landroid/content/Context;

    invoke-static {v2, p1}, Lcom/google/android/libraries/social/albumupload/impl/UploadSchedulerService;->a(Landroid/content/Context;I)V

    .line 45
    return-wide v0
.end method

.method public a(Lhkv;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lhkv;",
            ")",
            "Ljava/util/List",
            "<",
            "Lhla;",
            ">;"
        }
    .end annotation

    .prologue
    .line 63
    invoke-virtual {p1}, Lhkv;->a()I

    move-result v0

    invoke-direct {p0, v0}, Lhlu;->a(I)Lhlo;

    move-result-object v0

    .line 64
    invoke-virtual {v0, p1}, Lhlo;->a(Lhkv;)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public a(Lhle;Lhkv;)V
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lhlu;->b:Lhlh;

    invoke-virtual {v0, p1, p2}, Lhlh;->a(Lhle;Lhkv;)V

    .line 70
    return-void
.end method
