.class final enum Lavs;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lavs;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lavs;

.field public static final enum b:Lavs;

.field public static final enum c:Lavs;

.field public static final enum d:Lavs;

.field public static final enum e:Lavs;

.field public static final enum f:Lavs;

.field public static final enum g:Lavs;

.field public static final enum h:Lavs;

.field public static final enum i:Lavs;

.field public static final enum j:Lavs;

.field public static final enum k:Lavs;

.field public static final enum l:Lavs;

.field public static final enum m:Lavs;

.field public static final enum n:Lavs;

.field public static final enum o:Lavs;

.field public static final enum p:Lavs;

.field public static final enum q:Lavs;

.field public static final enum r:Lavs;

.field public static final enum s:Lavs;

.field public static final enum t:Lavs;

.field private static final synthetic u:[Lavs;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 736
    new-instance v0, Lavs;

    const-string v1, "CONSTRAINTS"

    invoke-direct {v0, v1, v3}, Lavs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavs;->a:Lavs;

    .line 737
    new-instance v0, Lavs;

    const-string v1, "USER_EDITS"

    invoke-direct {v0, v1, v4}, Lavs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavs;->b:Lavs;

    .line 738
    new-instance v0, Lavs;

    const-string v1, "STORYBOARD"

    invoke-direct {v0, v1, v5}, Lavs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavs;->c:Lavs;

    .line 739
    new-instance v0, Lavs;

    const-string v1, "SOUNDTRACK"

    invoke-direct {v0, v1, v6}, Lavs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavs;->d:Lavs;

    .line 740
    new-instance v0, Lavs;

    const-string v1, "SOUNDTRACK_READINESS"

    invoke-direct {v0, v1, v7}, Lavs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavs;->e:Lavs;

    .line 741
    new-instance v0, Lavs;

    const-string v1, "MUSIC_LIBRARY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lavs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavs;->f:Lavs;

    .line 742
    new-instance v0, Lavs;

    const-string v1, "TITLE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lavs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavs;->g:Lavs;

    .line 743
    new-instance v0, Lavs;

    const-string v1, "KEEP_ORIGINAL_AUDIO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lavs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavs;->h:Lavs;

    .line 744
    new-instance v0, Lavs;

    const-string v1, "VIDEO_AND_PHOTO_READINESS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lavs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavs;->i:Lavs;

    .line 745
    new-instance v0, Lavs;

    const-string v1, "THEME"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lavs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavs;->j:Lavs;

    .line 746
    new-instance v0, Lavs;

    const-string v1, "MAXIMUM_TARGET_DURATION"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lavs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavs;->k:Lavs;

    .line 747
    new-instance v0, Lavs;

    const-string v1, "FORCED_CLIP_SELECTION"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lavs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavs;->l:Lavs;

    .line 748
    new-instance v0, Lavs;

    const-string v1, "SAVING"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lavs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavs;->m:Lavs;

    .line 749
    new-instance v0, Lavs;

    const-string v1, "SHOW_SHARE_PROMO"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lavs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavs;->n:Lavs;

    .line 750
    new-instance v0, Lavs;

    const-string v1, "ACTIONS_AVAILABLE"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lavs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavs;->o:Lavs;

    .line 751
    new-instance v0, Lavs;

    const-string v1, "RUNNING_MODE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lavs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavs;->p:Lavs;

    .line 752
    new-instance v0, Lavs;

    const-string v1, "CLOUD_GET_STORYBOARD_RESULT"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lavs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavs;->q:Lavs;

    .line 753
    new-instance v0, Lavs;

    const-string v1, "CLOUD_MODIFIED_STORYBOARD"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lavs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavs;->r:Lavs;

    .line 754
    new-instance v0, Lavs;

    const-string v1, "ASSET_READINESS"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lavs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavs;->s:Lavs;

    .line 755
    new-instance v0, Lavs;

    const-string v1, "CLOUD_STORYBOARD_MODIFICATION_CHANGE"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lavs;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lavs;->t:Lavs;

    .line 735
    const/16 v0, 0x14

    new-array v0, v0, [Lavs;

    sget-object v1, Lavs;->a:Lavs;

    aput-object v1, v0, v3

    sget-object v1, Lavs;->b:Lavs;

    aput-object v1, v0, v4

    sget-object v1, Lavs;->c:Lavs;

    aput-object v1, v0, v5

    sget-object v1, Lavs;->d:Lavs;

    aput-object v1, v0, v6

    sget-object v1, Lavs;->e:Lavs;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lavs;->f:Lavs;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lavs;->g:Lavs;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lavs;->h:Lavs;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lavs;->i:Lavs;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lavs;->j:Lavs;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lavs;->k:Lavs;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lavs;->l:Lavs;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lavs;->m:Lavs;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lavs;->n:Lavs;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lavs;->o:Lavs;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lavs;->p:Lavs;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lavs;->q:Lavs;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lavs;->r:Lavs;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lavs;->s:Lavs;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lavs;->t:Lavs;

    aput-object v2, v0, v1

    sput-object v0, Lavs;->u:[Lavs;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 735
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lavs;
    .locals 1

    .prologue
    .line 735
    const-class v0, Lavs;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lavs;

    return-object v0
.end method

.method public static values()[Lavs;
    .locals 1

    .prologue
    .line 735
    sget-object v0, Lavs;->u:[Lavs;

    invoke-virtual {v0}, [Lavs;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lavs;

    return-object v0
.end method
