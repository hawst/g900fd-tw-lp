.class public final Lbqu;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final a:[F

.field private static final b:[F

.field private static final c:[F

.field private static final d:[F

.field private static final e:[F


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/16 v1, 0x10

    .line 12
    new-array v0, v1, [F

    fill-array-data v0, :array_0

    sput-object v0, Lbqu;->a:[F

    .line 19
    new-array v0, v1, [F

    fill-array-data v0, :array_1

    sput-object v0, Lbqu;->b:[F

    .line 26
    new-array v0, v1, [F

    fill-array-data v0, :array_2

    sput-object v0, Lbqu;->c:[F

    .line 33
    new-array v0, v1, [F

    fill-array-data v0, :array_3

    sput-object v0, Lbqu;->d:[F

    .line 40
    new-array v0, v1, [F

    fill-array-data v0, :array_4

    sput-object v0, Lbqu;->e:[F

    return-void

    .line 12
    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 19
    :array_1
    .array-data 4
        -0x40800000    # -1.0f
        0x0
        0x0
        0x0
        0x0
        -0x40800000    # -1.0f
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 26
    :array_2
    .array-data 4
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        -0x40800000    # -1.0f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 33
    :array_3
    .array-data 4
        0x0
        -0x40800000    # -1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 40
    :array_4
    .array-data 4
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x0
        -0x40800000    # -1.0f
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public static a(III)F
    .locals 2

    .prologue
    .line 115
    const-string v0, "rotationDegress"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    .line 116
    rem-int/lit8 v0, p0, 0x5a

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "rotationDegrees must be a multiple of 90."

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 118
    rem-int/lit16 v0, p0, 0xb4

    const/16 v1, 0x5a

    if-ne v0, v1, :cond_1

    int-to-float v0, p2

    int-to-float v1, p1

    div-float/2addr v0, v1

    :goto_1
    return v0

    .line 116
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 118
    :cond_1
    int-to-float v0, p1

    int-to-float v1, p2

    div-float/2addr v0, v1

    goto :goto_1
.end method

.method public static a(Lbml;)I
    .locals 1

    .prologue
    .line 58
    invoke-static {p0}, Lbqu;->c(Lbml;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 59
    invoke-interface {p0}, Lbml;->b()Lbmu;

    move-result-object v0

    iget v0, v0, Lbmu;->d:I

    .line 60
    :goto_0
    return v0

    :cond_0
    invoke-interface {p0}, Lbml;->b()Lbmu;

    move-result-object v0

    iget v0, v0, Lbmu;->c:I

    goto :goto_0
.end method

.method public static a([F)V
    .locals 1

    .prologue
    .line 96
    sget-object v0, Lbqu;->a:[F

    invoke-static {p0, v0}, Lbqu;->a([F[F)V

    .line 97
    return-void
.end method

.method private static a([F[F)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 122
    array-length v0, p0

    const-string v1, "destination.length"

    array-length v2, p1

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 123
    array-length v0, p1

    invoke-static {p1, v4, p0, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 124
    return-void
.end method

.method public static a(I)[F
    .locals 1

    .prologue
    .line 81
    if-eqz p0, :cond_0

    const/4 v0, -0x1

    if-ne p0, v0, :cond_1

    .line 82
    :cond_0
    sget-object v0, Lbqu;->a:[F

    .line 88
    :goto_0
    return-object v0

    .line 83
    :cond_1
    const/16 v0, 0xb4

    if-ne p0, v0, :cond_2

    .line 84
    sget-object v0, Lbqu;->b:[F

    goto :goto_0

    .line 85
    :cond_2
    const/16 v0, 0x5a

    if-ne p0, v0, :cond_3

    .line 86
    sget-object v0, Lbqu;->c:[F

    goto :goto_0

    .line 87
    :cond_3
    const/16 v0, 0x10e

    if-ne p0, v0, :cond_4

    .line 88
    sget-object v0, Lbqu;->d:[F

    goto :goto_0

    .line 90
    :cond_4
    const-string v0, "rotationDegree must be either 0, 90, 180 or 270"

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0
.end method

.method public static a(Lbmu;)[F
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lbmu;->a:I

    invoke-static {v0}, Lbqu;->a(I)[F

    move-result-object v0

    return-object v0
.end method

.method public static b(Lbml;)I
    .locals 1

    .prologue
    .line 65
    invoke-static {p0}, Lbqu;->c(Lbml;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    invoke-interface {p0}, Lbml;->b()Lbmu;

    move-result-object v0

    iget v0, v0, Lbmu;->c:I

    .line 67
    :goto_0
    return v0

    :cond_0
    invoke-interface {p0}, Lbml;->b()Lbmu;

    move-result-object v0

    iget v0, v0, Lbmu;->d:I

    goto :goto_0
.end method

.method public static b([F)V
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lbqu;->e:[F

    invoke-static {p0, v0}, Lbqu;->a([F[F)V

    .line 102
    return-void
.end method

.method public static c(Lbml;)Z
    .locals 2

    .prologue
    .line 72
    invoke-interface {p0}, Lbml;->b()Lbmu;

    move-result-object v0

    iget v0, v0, Lbmu;->a:I

    rem-int/lit16 v0, v0, 0xb4

    const/16 v1, 0x5a

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
