.class abstract Lkas;
.super Lhny;
.source "PG"


# instance fields
.field private a:Z

.field public final b:Landroid/content/Context;

.field public final c:I

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field private f:I

.field private h:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;IIZ)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 30
    iput-object p1, p0, Lkas;->b:Landroid/content/Context;

    .line 31
    iput p3, p0, Lkas;->c:I

    .line 32
    iput-object p4, p0, Lkas;->d:Ljava/lang/String;

    .line 33
    iput-object p5, p0, Lkas;->e:Ljava/lang/String;

    .line 34
    iput p6, p0, Lkas;->f:I

    .line 35
    iput p7, p0, Lkas;->h:I

    .line 36
    iput-boolean p8, p0, Lkas;->a:Z

    .line 37
    return-void
.end method


# virtual methods
.method protected a(ILjava/lang/Exception;ZLnge;)Lhoz;
    .locals 5

    .prologue
    .line 41
    new-instance v1, Lhoz;

    const/4 v0, 0x0

    invoke-direct {v1, p1, p2, v0}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    .line 42
    if-nez p3, :cond_0

    .line 43
    iget-object v0, p0, Lkas;->b:Landroid/content/Context;

    const-class v2, Ljxn;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxn;

    iget v2, p0, Lkas;->c:I

    iget-object v3, p0, Lkas;->d:Ljava/lang/String;

    iget-boolean v4, p0, Lkas;->a:Z

    invoke-interface {v0, v2, v3, p4, v4}, Ljxn;->a(ILjava/lang/String;Lnge;Z)V

    .line 50
    :goto_0
    return-object v1

    .line 46
    :cond_0
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "activity_id"

    iget-object v3, p0, Lkas;->d:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "voted_option_index"

    iget v3, p0, Lkas;->f:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 48
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "previous_voted_option_index"

    iget v3, p0, Lkas;->h:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method
