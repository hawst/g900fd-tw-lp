.class public final Lkbc;
.super Lkaz;
.source "PG"


# static fields
.field private static A:I

.field private static g:Z

.field private static h:I

.field private static u:I

.field private static v:I

.field private static w:I

.field private static x:I

.field private static y:I

.field private static z:I


# instance fields
.field private B:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lkaz;-><init>(Landroid/content/Context;)V

    .line 72
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 311
    const/4 v1, -0x1

    .line 312
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lkbc;->r:I

    if-ge v0, v2, :cond_2

    .line 313
    iget-object v2, p0, Lkbc;->e:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eq p1, v2, :cond_0

    iget-object v2, p0, Lkbc;->f:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-ne p1, v2, :cond_1

    .line 318
    :cond_0
    :goto_1
    return v0

    .line 312
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public a()V
    .locals 2

    .prologue
    .line 283
    invoke-super {p0}, Lkaz;->a()V

    .line 284
    iget-object v0, p0, Lkbc;->B:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 285
    iget-object v0, p0, Lkbc;->B:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 287
    :cond_0
    return-void
.end method

.method a(II)V
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Lkbc;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lkbc;->z:I

    invoke-virtual {v0, p2, v1}, Landroid/widget/TextView;->measure(II)V

    .line 307
    return-void
.end method

.method protected a(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 118
    invoke-super {p0, p1}, Lkaz;->a(Landroid/content/Context;)V

    .line 119
    sget-boolean v0, Lkbc;->g:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00a8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lkbc;->h:I

    const v1, 0x7f0d00a9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    const v2, 0x7f0d00b1

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    const v3, 0x7f0b00ba

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sput v3, Lkbc;->u:I

    const v3, 0x7f0b00bb

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lkbc;->v:I

    sget v0, Lkbc;->h:I

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    sput v0, Lkbc;->w:I

    sget v0, Lkbc;->h:I

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    sput v0, Lkbc;->x:I

    sget v0, Lkbc;->h:I

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    sput v0, Lkbc;->y:I

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    sput v0, Lkbc;->z:I

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    sput v0, Lkbc;->A:I

    const/4 v0, 0x1

    sput-boolean v0, Lkbc;->g:Z

    .line 120
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkbc;->B:Ljava/util/ArrayList;

    .line 121
    return-void
.end method

.method public b()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 125
    invoke-virtual {p0}, Lkbc;->getContext()Landroid/content/Context;

    move-result-object v2

    .line 126
    iget-object v0, p0, Lkbc;->b:Lkzw;

    invoke-virtual {v0}, Lkzw;->j()[Lkzx;

    move-result-object v3

    move v0, v1

    .line 127
    :goto_0
    iget v4, p0, Lkbc;->r:I

    if-ge v0, v4, :cond_1

    .line 128
    aget-object v4, v3, v0

    invoke-virtual {v4}, Lkzx;->d()Lkzv;

    move-result-object v4

    .line 129
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Lkzv;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 130
    invoke-virtual {p0, v2, v0}, Lkbc;->a(Landroid/content/Context;I)Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 131
    invoke-virtual {p0, v2, v0}, Lkbc;->b(Landroid/content/Context;I)Lcom/google/android/libraries/social/media/ui/MediaView;

    move-result-object v4

    sget v5, Lkbc;->v:I

    invoke-virtual {v4, v5}, Lcom/google/android/libraries/social/media/ui/MediaView;->setBackgroundColor(I)V

    .line 133
    :cond_0
    invoke-virtual {p0, v2, v0}, Lkbc;->f(Landroid/content/Context;I)Landroid/view/View;

    .line 134
    invoke-virtual {p0, v2, v0}, Lkbc;->e(Landroid/content/Context;I)Landroid/view/View;

    .line 135
    invoke-virtual {p0, v2, v0}, Lkbc;->c(Landroid/content/Context;I)Landroid/widget/TextView;

    move-result-object v4

    aget-object v5, v3, v0

    invoke-virtual {v5}, Lkzx;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 136
    invoke-virtual {p0, v2, v0}, Lkbc;->d(Landroid/content/Context;I)Landroid/widget/TextView;

    .line 127
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 138
    :cond_1
    iget-object v0, p0, Lkbc;->p:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lkbc;->addView(Landroid/view/View;)V

    .line 139
    :goto_1
    iget v0, p0, Lkbc;->r:I

    if-ge v1, v0, :cond_3

    .line 140
    invoke-virtual {p0}, Lkbc;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v0, p0, Lkbc;->B:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lkbc;->B:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    :goto_2
    invoke-virtual {p0, v0}, Lkbc;->addView(Landroid/view/View;)V

    .line 139
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 140
    :cond_2
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, v2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    sget v2, Lkbc;->u:I

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v2, p0, Lkbc;->B:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 142
    :cond_3
    return-void
.end method

.method c()I
    .locals 1

    .prologue
    .line 323
    sget v0, Lkbc;->h:I

    return v0
.end method

.method c(I)V
    .locals 4

    .prologue
    .line 291
    invoke-super {p0, p1}, Lkaz;->c(I)V

    .line 292
    iget-object v0, p0, Lkbc;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 293
    const/4 v1, -0x2

    sget v2, Lkbc;->z:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->measure(II)V

    .line 296
    iget-object v1, p0, Lkbc;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 298
    sget v2, Lkbc;->k:I

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lkbc;->b(Landroid/view/View;Landroid/view/View;II)V

    .line 301
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 10

    .prologue
    .line 203
    sget-object v0, Lkbc;->a:Llct;

    iget v1, v0, Llct;->m:I

    .line 204
    iget-object v0, p0, Lkbc;->b:Lkzw;

    invoke-virtual {v0}, Lkzw;->f()I

    move-result v5

    .line 207
    const/4 v0, 0x0

    move v3, v0

    move v4, v1

    :goto_0
    iget v0, p0, Lkbc;->r:I

    if-ge v3, v0, :cond_1

    .line 209
    iget-object v0, p0, Lkbc;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 210
    const/4 v1, 0x0

    .line 213
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredWidth()I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    .line 214
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v4

    .line 210
    invoke-virtual {v0, v1, v4, v2, v6}, Lcom/google/android/libraries/social/media/ui/MediaView;->layout(IIII)V

    .line 217
    iget-object v1, p0, Lkbc;->B:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 218
    const/4 v2, 0x0

    .line 220
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v6

    add-int/2addr v6, v4

    .line 221
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    add-int/lit8 v7, v7, 0x0

    .line 222
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v4

    .line 223
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v8, v9

    .line 218
    invoke-virtual {v1, v2, v6, v7, v8}, Landroid/view/View;->layout(IIII)V

    .line 226
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredWidth()I

    move-result v6

    .line 227
    iget-object v1, p0, Lkbc;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 232
    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v6

    .line 233
    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v4

    .line 228
    invoke-virtual {v1, v6, v4, v2, v7}, Lcom/google/android/libraries/social/media/ui/MediaView;->layout(IIII)V

    .line 235
    iget-object v2, p0, Lkbc;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 237
    invoke-virtual {v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v1

    .line 238
    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v7

    sub-int/2addr v1, v7

    div-int/lit8 v1, v1, 0x2

    add-int v7, v4, v1

    .line 240
    sget v1, Lkbc;->i:I

    add-int/2addr v6, v1

    .line 243
    iget-object v1, p0, Lkbc;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 247
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v8, v6

    .line 248
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v7

    .line 244
    invoke-virtual {v1, v6, v7, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 251
    iget-object v1, p0, Lkbc;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 255
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v8, v6

    .line 256
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v7

    .line 252
    invoke-virtual {v1, v6, v7, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 259
    invoke-virtual {p0, v3}, Lkbc;->c(I)V

    .line 262
    invoke-virtual {p0}, Lkbc;->m()Z

    move-result v1

    if-nez v1, :cond_0

    .line 266
    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v6

    .line 267
    invoke-virtual {v2}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v8

    add-int/2addr v8, v7

    .line 263
    invoke-virtual {v2, v6, v7, v1, v8}, Landroid/widget/TextView;->layout(IIII)V

    .line 271
    :goto_1
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v0

    add-int v1, v4, v0

    .line 207
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v4, v1

    goto/16 :goto_0

    .line 269
    :cond_0
    invoke-virtual {p0, v3}, Lkbc;->d(I)V

    goto :goto_1

    .line 273
    :cond_1
    const/4 v0, -0x1

    if-eq v5, v0, :cond_2

    .line 274
    invoke-virtual {p0, v5}, Lkbc;->f(I)V

    .line 276
    :cond_2
    invoke-virtual {p0}, Lkbc;->d()V

    .line 277
    iget-object v0, p0, Lkbc;->f:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredWidth()I

    move-result v0

    int-to-float v1, v0

    iget-object v0, p0, Lkbc;->n:Ljava/util/ArrayList;

    const/4 v2, 0x0

    .line 278
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v1, v0

    iput v0, p0, Lkbc;->t:F

    .line 279
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/high16 v3, 0x40000000    # 2.0f

    const/4 v8, -0x2

    .line 159
    iget-object v1, p0, Lkbc;->p:Landroid/widget/ImageView;

    invoke-virtual {v1, v8, v8}, Landroid/widget/ImageView;->measure(II)V

    .line 160
    iget v1, p0, Lkbc;->c:I

    sget v2, Lkbc;->h:I

    sub-int/2addr v1, v2

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 162
    iget v1, p0, Lkbc;->c:I

    sget v2, Lkbc;->h:I

    sub-int/2addr v1, v2

    sget v2, Lkbc;->i:I

    shl-int/lit8 v2, v2, 0x1

    sub-int/2addr v1, v2

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 166
    iget v1, p0, Lkbc;->c:I

    invoke-static {v1, v3}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    move v2, v0

    move v3, v0

    .line 170
    :goto_0
    iget v0, p0, Lkbc;->r:I

    if-ge v2, v0, :cond_0

    .line 172
    iget-object v0, p0, Lkbc;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 173
    sget v1, Lkbc;->w:I

    sget v7, Lkbc;->x:I

    invoke-virtual {v0, v1, v7}, Lcom/google/android/libraries/social/media/ui/MediaView;->measure(II)V

    .line 177
    iget-object v1, p0, Lkbc;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/media/ui/MediaView;

    sget v7, Lkbc;->y:I

    invoke-virtual {v1, v4, v7}, Lcom/google/android/libraries/social/media/ui/MediaView;->measure(II)V

    .line 181
    iget-object v1, p0, Lkbc;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v7, Lkbc;->z:I

    invoke-virtual {v1, v5, v7}, Landroid/widget/TextView;->measure(II)V

    .line 185
    iget-object v1, p0, Lkbc;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    sget v7, Lkbc;->z:I

    invoke-virtual {v1, v5, v7}, Landroid/view/View;->measure(II)V

    .line 189
    iget-object v1, p0, Lkbc;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    sget v7, Lkbc;->z:I

    invoke-virtual {v1, v8, v7}, Landroid/view/View;->measure(II)V

    .line 193
    iget-object v1, p0, Lkbc;->B:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    sget v7, Lkbc;->A:I

    invoke-virtual {v1, v6, v7}, Landroid/view/View;->measure(II)V

    .line 195
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v0

    add-int v1, v3, v0

    .line 170
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v3, v1

    goto :goto_0

    .line 197
    :cond_0
    sget-object v0, Lkbc;->a:Llct;

    iget v0, v0, Llct;->m:I

    add-int/2addr v0, v3

    .line 198
    iget v1, p0, Lkbc;->c:I

    invoke-virtual {p0, v1, v0}, Lkbc;->setMeasuredDimension(II)V

    .line 199
    return-void
.end method
