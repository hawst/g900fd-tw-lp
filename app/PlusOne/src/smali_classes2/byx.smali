.class public Lbyx;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Lbyz;


# instance fields
.field private final c:Lbyt;

.field private final d:Lama;

.field private final e:Lbzx;

.field private final f:Ljava/lang/Object;

.field private final g:Ljava/lang/Object;

.field private h:I

.field private i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbon;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmw;",
            ">;"
        }
    .end annotation
.end field

.field private k:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbon;",
            ">;"
        }
    .end annotation
.end field

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmw;",
            ">;"
        }
    .end annotation
.end field

.field private m:Ljava/lang/String;

.field private n:Lbzf;

.field private o:Lbza;

.field private p:Lbzg;

.field private q:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation
.end field

.field private r:Lbzp;

.field private s:Z

.field private t:Lood;

.field private final u:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbph;",
            ">;"
        }
    .end annotation
.end field

.field private final v:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbph;",
            ">;"
        }
    .end annotation
.end field

.field private final w:Lbor;

.field private x:Lbmk;

.field private final y:Lbyy;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 55
    const-class v0, Lbyx;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbyx;->a:Ljava/lang/String;

    .line 88
    new-instance v0, Lbyz;

    .line 89
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v3, v1, v3, v2}, Lbyz;-><init>(Lboi;Ljava/util/List;Lood;Z)V

    sput-object v0, Lbyx;->b:Lbyz;

    .line 88
    return-void
.end method

.method public constructor <init>(Lbyt;Lama;Lbzx;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbyx;->f:Ljava/lang/Object;

    .line 120
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbyx;->g:Ljava/lang/Object;

    .line 122
    const/4 v0, 0x0

    iput v0, p0, Lbyx;->h:I

    .line 125
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbyx;->i:Ljava/util/List;

    .line 127
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbyx;->j:Ljava/util/List;

    .line 129
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbyx;->k:Ljava/util/List;

    .line 131
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbyx;->l:Ljava/util/List;

    .line 132
    const-string v0, ""

    iput-object v0, p0, Lbyx;->m:Ljava/lang/String;

    .line 155
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbyx;->u:Ljava/util/List;

    .line 165
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbyx;->v:Ljava/util/List;

    .line 175
    new-instance v0, Lbyy;

    invoke-direct {v0}, Lbyy;-><init>()V

    iput-object v0, p0, Lbyx;->y:Lbyy;

    .line 183
    const-string v0, "beatMatcher"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbyt;

    iput-object v0, p0, Lbyx;->c:Lbyt;

    .line 184
    const-string v0, "clipIdentifierGenerator"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lama;

    iput-object v0, p0, Lbyx;->d:Lama;

    .line 186
    const-string v0, "themeApplier"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzx;

    iput-object v0, p0, Lbyx;->e:Lbzx;

    .line 187
    new-instance v0, Lbor;

    invoke-direct {v0}, Lbor;-><init>()V

    iput-object v0, p0, Lbyx;->w:Lbor;

    .line 188
    return-void
.end method

.method private static a(Ljava/util/List;Landroid/util/SparseArray;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Landroid/util/SparseArray",
            "<",
            "Lbmh;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lbmh;",
            ">;"
        }
    .end annotation

    .prologue
    .line 879
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 880
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 883
    iget v0, v0, Lbmd;->b:I

    sget-object v3, Lbmh;->a:Lbmh;

    invoke-virtual {p1, v0, v3}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 886
    :cond_0
    return-object v1
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 911
    iget v0, p0, Lbyx;->h:I

    if-gt v0, p1, :cond_0

    .line 923
    :goto_0
    return-void

    .line 918
    :cond_0
    iget v0, p0, Lbyx;->h:I

    if-gtz v0, :cond_1

    .line 919
    iget-object v0, p0, Lbyx;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const-string v1, "user edits should be converted first"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 920
    iget-object v0, p0, Lbyx;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    const-string v1, "user edits should be converted first"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 922
    :cond_1
    iput p1, p0, Lbyx;->h:I

    goto :goto_0
.end method

.method private static a(Landroid/util/SparseArray;Ljava/util/List;Lood;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lbmh;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Lood;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 854
    invoke-virtual {p0}, Landroid/util/SparseArray;->clear()V

    .line 855
    iget-object v0, p2, Lood;->j:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/16 v0, 0x2d0

    move v1, v0

    .line 857
    :goto_0
    iget-object v0, p2, Lood;->i:Ljava/lang/Integer;

    if-nez v0, :cond_1

    const/16 v0, 0x500

    move v2, v0

    :goto_1
    move v4, v3

    .line 862
    :goto_2
    iget-object v0, p2, Lood;->b:[Lonr;

    array-length v0, v0

    if-ge v4, v0, :cond_2

    .line 863
    iget-object v0, p2, Lood;->d:[Ljava/lang/Boolean;

    aget-object v0, v0, v4

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    .line 864
    iget-object v0, p2, Lood;->b:[Lonr;

    aget-object v0, v0, v4

    iget-object v5, v0, Lonr;->e:Lont;

    .line 866
    invoke-interface {p1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 867
    iget v6, v0, Lbmd;->b:I

    .line 869
    invoke-static {v0, v5, v2, v1}, Lbmh;->a(Lbmd;Lont;II)Lbmh;

    move-result-object v0

    .line 867
    invoke-virtual {p0, v6, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 871
    add-int/lit8 v0, v3, 0x1

    .line 873
    :goto_3
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v0

    goto :goto_2

    .line 855
    :cond_0
    iget-object v0, p2, Lood;->j:Ljava/lang/Integer;

    .line 856
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move v1, v0

    goto :goto_0

    .line 857
    :cond_1
    iget-object v0, p2, Lood;->i:Ljava/lang/Integer;

    .line 858
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move v2, v0

    goto :goto_1

    .line 875
    :cond_2
    return-void

    :cond_3
    move v0, v3

    goto :goto_3
.end method

.method private a(Lboi;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 946
    iget-object v0, p0, Lbyx;->y:Lbyy;

    iget-object v4, v0, Lbyy;->h:Lcfj;

    .line 948
    invoke-virtual {v4}, Lcfj;->c()V

    .line 949
    invoke-virtual {p1}, Lboi;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 950
    iget v1, v0, Lbmd;->b:I

    const/4 v6, -0x1

    if-eq v1, v6, :cond_0

    move v1, v2

    :goto_1
    const-string v6, "Clip with no identifier"

    invoke-static {v1, v6}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 951
    iget v1, v0, Lbmd;->b:I

    int-to-long v6, v1

    invoke-virtual {v4, v6, v7}, Lcfj;->b(J)Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    :goto_2
    const-string v6, "Clip identifier used twice"

    invoke-static {v1, v6}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 952
    iget v0, v0, Lbmd;->b:I

    int-to-long v0, v0

    invoke-virtual {v4, v0, v1}, Lcfj;->a(J)V

    goto :goto_0

    :cond_0
    move v1, v3

    .line 950
    goto :goto_1

    :cond_1
    move v1, v3

    .line 951
    goto :goto_2

    .line 954
    :cond_2
    return-void
.end method

.method private c()Lbyz;
    .locals 28

    .prologue
    .line 462
    new-instance v6, Lbor;

    invoke-direct {v6}, Lbor;-><init>()V

    .line 479
    move-object/from16 v0, p0

    iget-object v8, v0, Lbyx;->f:Ljava/lang/Object;

    monitor-enter v8

    .line 481
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->k:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 485
    sget-object v2, Lbyx;->b:Lbyz;

    monitor-exit v8

    .line 822
    :goto_0
    return-object v2

    .line 489
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->t:Lood;

    if-nez v2, :cond_1

    .line 490
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->w:Lbor;

    invoke-virtual {v2}, Lbor;->e()J

    move-result-wide v2

    const-string v4, "targetDurationUs"

    invoke-static {v2, v3, v4}, Lcec;->a(JLjava/lang/CharSequence;)J

    .line 493
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->n:Lbzf;

    const-string v3, "mClipSelector"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbzf;

    .line 494
    move-object/from16 v0, p0

    iget-object v3, v0, Lbyx;->p:Lbzg;

    const-string v4, "mClipSelectorLimits"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lbzg;

    .line 496
    move-object/from16 v0, p0

    iget-object v12, v0, Lbyx;->q:Ljava/util/List;

    .line 497
    move-object/from16 v0, p0

    iget-object v3, v0, Lbyx;->r:Lbzp;

    const-string v4, "mEffectApplier"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Lbzp;

    move-object/from16 v18, v0

    .line 501
    move-object/from16 v0, p0

    iget-object v3, v0, Lbyx;->k:Ljava/util/List;

    .line 502
    move-object/from16 v0, p0

    iget-object v4, v0, Lbyx;->l:Ljava/util/List;

    .line 504
    move-object/from16 v0, p0

    iget-object v5, v0, Lbyx;->w:Lbor;

    invoke-virtual {v6, v5}, Lbor;->a(Lbor;)Z

    .line 505
    new-instance v17, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbyx;->u:Ljava/util/List;

    move-object/from16 v0, v17

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 506
    new-instance v21, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbyx;->v:Ljava/util/List;

    move-object/from16 v0, v21

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 507
    move-object/from16 v0, p0

    iget v14, v0, Lbyx;->h:I

    .line 508
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lbyx;->s:Z

    move/from16 v19, v0

    .line 509
    move-object/from16 v0, p0

    iget-object v5, v0, Lbyx;->j:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v9, v0, Lbyx;->i:Ljava/util/List;

    .line 510
    invoke-static {v5, v9}, Lbps;->a(Ljava/util/Collection;Ljava/util/Collection;)Ljava/util/Map;

    move-result-object v5

    .line 511
    move-object/from16 v0, p0

    iget-object v9, v0, Lbyx;->i:Ljava/util/List;

    invoke-static {v9}, Lbmb;->a(Ljava/util/List;)Ljava/util/Map;

    move-result-object v13

    .line 512
    move-object/from16 v0, p0

    iget-object v0, v0, Lbyx;->t:Lood;

    move-object/from16 v23, v0

    .line 513
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 514
    move-object/from16 v0, p0

    iget-object v0, v0, Lbyx;->m:Ljava/lang/String;

    move-object/from16 v22, v0

    .line 515
    move-object/from16 v0, p0

    iget-object v9, v0, Lbyx;->o:Lbza;

    const-string v11, "mThemeForThemeApplier"

    const/4 v15, 0x0

    invoke-static {v9, v11, v15}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lbza;

    .line 517
    move-object/from16 v0, p0

    iget-object v0, v0, Lbyx;->x:Lbmk;

    move-object/from16 v25, v0

    .line 521
    const/4 v11, 0x7

    move-object/from16 v0, p0

    iput v11, v0, Lbyx;->h:I

    .line 522
    move-object/from16 v0, p0

    iget-object v11, v0, Lbyx;->u:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v15, v0, Lbyx;->v:Ljava/util/List;

    invoke-interface {v11, v15}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 523
    move-object/from16 v0, p0

    iget-object v11, v0, Lbyx;->v:Ljava/util/List;

    invoke-interface {v11}, Ljava/util/List;->clear()V

    .line 524
    monitor-exit v8
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 529
    const/16 v16, 0x0

    .line 530
    const/4 v15, 0x1

    .line 531
    if-eqz v23, :cond_24

    .line 532
    const/4 v8, 0x0

    .line 533
    if-eqz v25, :cond_2

    .line 534
    invoke-virtual/range {v25 .. v25}, Lbmk;->b()Lboh;

    move-result-object v8

    invoke-virtual {v8}, Lboh;->c()J

    move-result-wide v26

    invoke-static/range {v26 .. v27}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    .line 536
    :cond_2
    const/4 v11, 0x0

    .line 537
    move-object/from16 v0, v23

    iget-object v0, v0, Lood;->h:Looe;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Looe;->c:Looa;

    move-object/from16 v20, v0

    if-eqz v20, :cond_3

    .line 538
    move-object/from16 v0, v23

    iget-object v11, v0, Lood;->h:Looe;

    iget-object v11, v11, Looe;->c:Looa;

    iget-object v11, v11, Looa;->a:Ljava/lang/Long;

    .line 540
    :cond_3
    invoke-static {v8, v11}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v20

    .line 541
    move-object/from16 v0, v23

    iget-object v8, v0, Lood;->h:Looe;

    iget v8, v8, Looe;->a:I

    .line 542
    invoke-static {v8}, Lbza;->a(I)Lbza;

    move-result-object v8

    .line 541
    invoke-virtual {v9, v8}, Lbza;->equals(Ljava/lang/Object;)Z

    move-result v24

    .line 543
    if-nez v22, :cond_5

    const-string v8, ""

    .line 544
    :goto_1
    move-object/from16 v0, v23

    iget-object v11, v0, Lood;->f:Ljava/lang/String;

    if-nez v11, :cond_6

    const-string v11, ""

    .line 545
    :goto_2
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->isEmpty()Z

    move-result v26

    if-eqz v26, :cond_24

    .line 546
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->isEmpty()Z

    move-result v26

    if-eqz v26, :cond_24

    .line 547
    invoke-virtual {v8}, Ljava/lang/String;->isEmpty()Z

    move-result v26

    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v27

    move/from16 v0, v26

    move/from16 v1, v27

    if-ne v0, v1, :cond_24

    if-eqz v24, :cond_24

    if-eqz v20, :cond_24

    .line 550
    const/16 v16, 0x1

    .line 551
    invoke-virtual {v8, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_24

    .line 552
    const/4 v8, 0x0

    move/from16 v24, v8

    move/from16 v20, v16

    .line 559
    :goto_3
    if-eqz v23, :cond_23

    sget-object v8, Lbza;->l:Lbza;

    if-ne v9, v8, :cond_23

    const/4 v8, 0x4

    if-eq v14, v8, :cond_4

    const/4 v8, 0x5

    if-ne v14, v8, :cond_23

    .line 562
    :cond_4
    const/4 v8, 0x3

    .line 566
    :goto_4
    if-nez v8, :cond_22

    .line 568
    if-nez v12, :cond_7

    .line 569
    invoke-interface/range {v2 .. v7}, Lbzf;->a(Ljava/util/List;Ljava/util/List;Ljava/util/Map;Lbor;Lbzg;)Ljava/util/List;

    move-result-object v2

    .line 577
    :goto_5
    const-wide/32 v14, 0x16e360

    invoke-static {v2, v14, v15}, Lbpm;->b(Ljava/util/List;J)V

    .line 578
    move-object/from16 v0, p0

    iget-object v3, v0, Lbyx;->y:Lbyy;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbyx;->d:Lama;

    .line 579
    invoke-static {v2, v4}, Lbpm;->a(Ljava/util/List;Lama;)Ljava/util/List;

    move-result-object v2

    .line 578
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, v3, Lbyy;->b:Ljava/util/List;

    .line 581
    if-eqz v23, :cond_8

    .line 586
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    .line 587
    iget-object v2, v2, Lbyy;->a:Landroid/util/SparseArray;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbyx;->y:Lbyy;

    .line 588
    iget-object v3, v3, Lbyy;->b:Ljava/util/List;

    .line 586
    move-object/from16 v0, v23

    invoke-static {v2, v3, v0}, Lbyx;->a(Landroid/util/SparseArray;Ljava/util/List;Lood;)V

    .line 600
    :goto_6
    const/4 v2, 0x1

    .line 604
    :goto_7
    const/4 v3, 0x1

    if-ne v2, v3, :cond_a

    .line 605
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    iget-object v2, v2, Lbyy;->b:Ljava/util/List;

    const-string v3, "mProductionState.clipsFromSelector"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 607
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    new-instance v3, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbyx;->y:Lbyy;

    .line 609
    iget-object v4, v4, Lbyy;->b:Ljava/util/List;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 607
    iput-object v3, v2, Lbyy;->d:Ljava/util/List;

    .line 610
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_8
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbph;

    .line 611
    move-object/from16 v0, p0

    iget-object v4, v0, Lbyx;->y:Lbyy;

    iget-object v4, v4, Lbyy;->d:Ljava/util/List;

    invoke-interface {v2, v4}, Lbph;->a(Ljava/util/List;)V

    goto :goto_8

    .line 524
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2

    :cond_5
    move-object/from16 v8, v22

    .line 543
    goto/16 :goto_1

    .line 544
    :cond_6
    move-object/from16 v0, v23

    iget-object v11, v0, Lood;->f:Ljava/lang/String;

    goto/16 :goto_2

    :cond_7
    move-object v2, v12

    .line 575
    goto :goto_5

    .line 591
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    iget-object v2, v2, Lbyy;->a:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    goto :goto_6

    .line 620
    :cond_9
    const/4 v2, 0x2

    .line 625
    :cond_a
    const/4 v3, 0x2

    if-ne v2, v3, :cond_d

    .line 626
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    iget-object v2, v2, Lbyy;->d:Ljava/util/List;

    const-string v3, "mProductionState.clipsAfterUserEdits"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 628
    invoke-interface/range {v21 .. v21}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_9
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbph;

    .line 629
    move-object/from16 v0, p0

    iget-object v4, v0, Lbyx;->y:Lbyy;

    iget-object v4, v4, Lbyy;->d:Ljava/util/List;

    invoke-interface {v2, v4}, Lbph;->a(Ljava/util/List;)V

    goto :goto_9

    .line 632
    :cond_b
    if-nez v23, :cond_11

    .line 636
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbyx;->y:Lbyy;

    .line 637
    iget-object v3, v3, Lbyy;->d:Ljava/util/List;

    .line 636
    invoke-static {v3, v5, v7}, Lbpm;->a(Ljava/util/List;Ljava/util/Map;Lbzg;)Ljava/util/List;

    move-result-object v3

    iput-object v3, v2, Lbyy;->d:Ljava/util/List;

    .line 660
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    iget-object v2, v2, Lbyy;->d:Ljava/util/List;

    const-wide/32 v6, 0x16e360

    invoke-static {v2, v6, v7}, Lbpm;->b(Ljava/util/List;J)V

    .line 662
    const/4 v2, 0x3

    .line 668
    :cond_d
    const/4 v3, 0x3

    if-ne v2, v3, :cond_f

    .line 669
    sget-object v2, Lbyx;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    iget-object v2, v2, Lbyy;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x19

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " selector size"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 670
    sget-object v2, Lbyx;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    iget-object v2, v2, Lbyy;->d:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x20

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " after user edit size"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 671
    move-object/from16 v0, p0

    iget-object v6, v0, Lbyx;->e:Lbzx;

    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    .line 673
    iget-object v8, v2, Lbyy;->d:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    .line 676
    iget-object v11, v2, Lbyy;->a:Landroid/util/SparseArray;

    if-eqz v20, :cond_e

    .line 679
    invoke-interface/range {v17 .. v17}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_e

    invoke-interface/range {v21 .. v21}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_13

    :cond_e
    const/4 v12, 0x1

    :goto_a
    move-object/from16 v7, v23

    .line 671
    invoke-interface/range {v6 .. v12}, Lbzx;->a(Lood;Ljava/util/List;Lbza;Ljava/util/List;Landroid/util/SparseArray;Z)Lbzy;

    move-result-object v2

    .line 680
    iget-object v3, v2, Lbzy;->b:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const-string v4, "themedClips.size()"

    move-object/from16 v0, p0

    iget-object v6, v0, Lbyx;->y:Lbyy;

    .line 681
    iget-object v6, v6, Lbyy;->d:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    .line 680
    const/4 v7, 0x0

    invoke-static {v3, v4, v6, v7}, Lcgp;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 682
    iget-object v3, v2, Lbzy;->b:Ljava/util/List;

    const-wide/32 v6, 0x16e360

    invoke-static {v3, v6, v7}, Lbpm;->b(Ljava/util/List;J)V

    .line 683
    move-object/from16 v0, p0

    iget-object v3, v0, Lbyx;->y:Lbyy;

    iput-object v2, v3, Lbyy;->c:Lbzy;

    .line 685
    const/4 v2, 0x4

    .line 690
    :cond_f
    const/4 v3, 0x4

    if-ne v2, v3, :cond_15

    .line 691
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    iget-object v2, v2, Lbyy;->c:Lbzy;

    iget-object v2, v2, Lbzy;->b:Ljava/util/List;

    const-string v3, "mProductionState.clipsAfterUserEdits"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 694
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 698
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    iget-object v2, v2, Lbyy;->c:Lbzy;

    iget-object v2, v2, Lbzy;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_b
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_14

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbmd;

    .line 699
    invoke-virtual {v2}, Lbmd;->d()Z

    move-result v4

    if-nez v4, :cond_10

    .line 700
    move-object/from16 v0, p0

    iget-object v4, v0, Lbyx;->e:Lbzx;

    invoke-interface {v4, v2, v9}, Lbzx;->a(Lbmd;Lbza;)Lbmd;

    move-result-object v2

    .line 702
    :cond_10
    invoke-interface {v14, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_b

    .line 641
    :cond_11
    invoke-interface {v5}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_12
    :goto_c
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbml;

    .line 642
    invoke-interface {v2}, Lbml;->b()Lbmu;

    move-result-object v2

    .line 643
    iget-object v4, v2, Lbmu;->e:Lbmv;

    sget-object v6, Lbmv;->a:Lbmv;

    if-ne v4, v6, :cond_12

    .line 644
    iget v4, v2, Lbmu;->c:I

    iget v6, v2, Lbmu;->d:I

    mul-int/2addr v4, v6

    const v6, 0xe1000

    if-le v4, v6, :cond_12

    .line 647
    sget-object v4, Lbyx;->a:Ljava/lang/String;

    iget v6, v2, Lbmu;->c:I

    iget v2, v2, Lbmu;->d:I

    new-instance v7, Ljava/lang/StringBuilder;

    const/16 v8, 0x3b

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Cloud displayable has dimensions "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " by "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c

    .line 679
    :cond_13
    const/4 v12, 0x0

    goto/16 :goto_a

    .line 705
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    .line 709
    invoke-virtual/range {v22 .. v22}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1b

    const/4 v15, 0x1

    :goto_d
    move-object/from16 v0, p0

    iget-object v3, v0, Lbyx;->y:Lbyy;

    .line 710
    iget-object v3, v3, Lbyy;->c:Lbzy;

    iget-object v0, v3, Lbzy;->c:Lbmd;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lbyx;->y:Lbyy;

    .line 711
    iget-object v3, v3, Lbyy;->c:Lbzy;

    iget-object v0, v3, Lbzy;->d:Lbmd;

    move-object/from16 v17, v0

    move-object/from16 v11, v18

    move-object v12, v5

    .line 705
    invoke-interface/range {v11 .. v17}, Lbzp;->a(Ljava/util/Map;Ljava/util/Map;Ljava/util/List;ZLbmd;Lbmd;)Lboj;

    move-result-object v3

    iput-object v3, v2, Lbyy;->e:Lboj;

    .line 722
    const/4 v2, 0x5

    .line 726
    :cond_15
    const/4 v3, 0x5

    if-ne v2, v3, :cond_16

    .line 729
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    iget-object v2, v2, Lbyy;->e:Lboj;

    if-nez v2, :cond_1c

    .line 730
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    const/4 v3, 0x0

    iput-object v3, v2, Lbyy;->f:Lboj;

    .line 744
    :goto_e
    const/4 v2, 0x6

    .line 748
    :cond_16
    const/4 v3, 0x6

    if-ne v2, v3, :cond_17

    .line 749
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    iget-object v2, v2, Lbyy;->f:Lboj;

    if-nez v2, :cond_1e

    .line 750
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    const/4 v3, 0x0

    iput-object v3, v2, Lbyy;->g:Lboi;

    .line 780
    :cond_17
    :goto_f
    move-object/from16 v0, p0

    iget-object v3, v0, Lbyx;->f:Ljava/lang/Object;

    monitor-enter v3

    .line 784
    :try_start_2
    move-object/from16 v0, p0

    iget v2, v0, Lbyx;->h:I

    const/4 v4, 0x7

    if-eq v2, v4, :cond_18

    .line 785
    sget-object v2, Lbyx;->a:Ljava/lang/String;

    .line 788
    :cond_18
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 797
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    iget-object v2, v2, Lbyy;->g:Lboi;

    if-eqz v2, :cond_19

    .line 798
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    iget-object v2, v2, Lbyy;->g:Lboi;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lbyx;->a(Lboi;)V

    .line 803
    :cond_19
    const/4 v2, 0x0

    .line 804
    if-eqz v23, :cond_1a

    move-object/from16 v0, p0

    iget-object v3, v0, Lbyx;->y:Lbyy;

    .line 805
    iget-object v3, v3, Lbyy;->g:Lboi;

    if-eqz v3, :cond_1a

    .line 806
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    .line 808
    iget-object v2, v2, Lbyy;->c:Lbzy;

    iget-object v10, v2, Lbzy;->a:Loon;

    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    .line 809
    iget-object v11, v2, Lbyy;->d:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    .line 810
    iget-object v12, v2, Lbyy;->g:Lboi;

    const/4 v15, 0x0

    move-object/from16 v13, v25

    move/from16 v14, v19

    .line 806
    invoke-static/range {v9 .. v15}, Lbyw;->a(Lbza;Loon;Ljava/util/List;Lboi;Lbmk;ZZ)Lood;

    move-result-object v2

    .line 820
    :cond_1a
    new-instance v3, Lbyz;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbyx;->y:Lbyy;

    .line 821
    iget-object v4, v4, Lbyy;->g:Lboi;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbyx;->y:Lbyy;

    .line 822
    iget-object v5, v5, Lbyy;->b:Ljava/util/List;

    move/from16 v0, v24

    invoke-direct {v3, v4, v5, v2, v0}, Lbyz;-><init>(Lboi;Ljava/util/List;Lood;Z)V

    move-object v2, v3

    goto/16 :goto_0

    .line 709
    :cond_1b
    const/4 v15, 0x0

    goto/16 :goto_d

    .line 732
    :cond_1c
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    move-object/from16 v0, p0

    iget-object v14, v0, Lbyx;->c:Lbyt;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbyx;->y:Lbyy;

    .line 733
    iget-object v15, v3, Lbyy;->e:Lboj;

    if-eqz v20, :cond_1d

    move-object/from16 v20, v23

    :goto_10
    move-object/from16 v16, v25

    move-object/from16 v17, v5

    move-object/from16 v18, v13

    move-object/from16 v21, v10

    .line 732
    invoke-virtual/range {v14 .. v21}, Lbyt;->a(Lboj;Lbmk;Ljava/util/Map;Ljava/util/Map;ZLood;Ljava/util/List;)Lboj;

    move-result-object v3

    iput-object v3, v2, Lbyy;->f:Lboj;

    goto/16 :goto_e

    .line 733
    :cond_1d
    const/16 v20, 0x0

    goto :goto_10

    .line 752
    :cond_1e
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    iget-object v2, v2, Lbyy;->f:Lboj;

    move-object/from16 v0, v22

    invoke-virtual {v2, v0}, Lboj;->a(Ljava/lang/String;)Lboj;

    .line 753
    if-eqz v23, :cond_1f

    move-object/from16 v0, v23

    iget-object v2, v0, Lood;->g:Ljava/lang/String;

    if-eqz v2, :cond_1f

    .line 754
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    iget-object v2, v2, Lbyy;->f:Lboj;

    move-object/from16 v0, v23

    iget-object v3, v0, Lood;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lboj;->b(Ljava/lang/String;)Lboj;

    .line 756
    :cond_1f
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    iget-object v2, v2, Lbyy;->f:Lboj;

    invoke-virtual {v2}, Lboj;->d()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_20

    .line 757
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    .line 758
    iget-object v2, v2, Lbyy;->f:Lboj;

    invoke-virtual {v2}, Lboj;->d()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbmd;

    .line 759
    iget-object v3, v2, Lbmd;->d:Lbmg;

    sget-object v4, Lbmg;->d:Lbmg;

    if-ne v3, v4, :cond_20

    .line 760
    move-object/from16 v0, p0

    iget-object v3, v0, Lbyx;->y:Lbyy;

    iget-object v3, v3, Lbyy;->f:Lboj;

    iget-object v2, v2, Lbmd;->f:Lbmp;

    invoke-virtual {v3, v2}, Lboj;->a(Lbmp;)Lboj;

    .line 764
    :cond_20
    if-eqz v23, :cond_21

    .line 765
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    iget-object v2, v2, Lbyy;->f:Lboj;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbyx;->y:Lbyy;

    .line 767
    iget-object v3, v3, Lbyy;->f:Lboj;

    invoke-virtual {v3}, Lboj;->d()Ljava/util/List;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lbyx;->y:Lbyy;

    .line 768
    iget-object v4, v4, Lbyy;->a:Landroid/util/SparseArray;

    .line 766
    invoke-static {v3, v4}, Lbyx;->a(Ljava/util/List;Landroid/util/SparseArray;)Ljava/util/List;

    move-result-object v3

    .line 765
    invoke-virtual {v2, v3}, Lboj;->b(Ljava/util/List;)Lboj;

    .line 772
    :cond_21
    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbyx;->y:Lbyy;

    .line 773
    iget-object v3, v3, Lbyy;->f:Lboj;

    .line 774
    invoke-virtual {v3}, Lboj;->c()Lboi;

    move-result-object v3

    .line 772
    iput-object v3, v2, Lbyy;->g:Lboi;
    :try_end_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_f

    .line 775
    :catch_0
    move-exception v2

    .line 776
    sget-object v3, Lbyx;->a:Ljava/lang/String;

    const-string v4, "Invalid storyboard generated."

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 777
    move-object/from16 v0, p0

    iget-object v2, v0, Lbyx;->y:Lbyy;

    const/4 v3, 0x0

    iput-object v3, v2, Lbyy;->g:Lboi;

    goto/16 :goto_f

    .line 788
    :catchall_1
    move-exception v2

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2

    :cond_22
    move v2, v8

    goto/16 :goto_7

    :cond_23
    move v8, v14

    goto/16 :goto_4

    :cond_24
    move/from16 v24, v15

    move/from16 v20, v16

    goto/16 :goto_3
.end method

.method private static c(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lbml;",
            ">(",
            "Ljava/util/List",
            "<TT;>;)",
            "Ljava/util/List",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 927
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 928
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 937
    :goto_0
    return-object v0

    .line 931
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 932
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbml;

    .line 933
    invoke-interface {v0}, Lbml;->f()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 934
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 937
    :cond_3
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()Lbyz;
    .locals 4

    .prologue
    .line 413
    iget-object v1, p0, Lbyx;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 415
    :try_start_0
    invoke-direct {p0}, Lbyx;->c()Lbyz;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    monitor-exit v1

    return-object v0

    .line 416
    :catch_0
    move-exception v0

    .line 420
    iget-object v2, p0, Lbyx;->f:Ljava/lang/Object;

    monitor-enter v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 421
    const/4 v3, 0x0

    :try_start_2
    invoke-direct {p0, v3}, Lbyx;->a(I)V

    .line 422
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 423
    :try_start_3
    throw v0

    .line 425
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 422
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method public a(Lbmk;)V
    .locals 2

    .prologue
    .line 367
    iget-object v1, p0, Lbyx;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 368
    :try_start_0
    iget-object v0, p0, Lbyx;->x:Lbmk;

    if-eq v0, p1, :cond_0

    .line 369
    iput-object p1, p0, Lbyx;->x:Lbmk;

    .line 370
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lbyx;->a(I)V

    .line 372
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lbor;)V
    .locals 4

    .prologue
    .line 194
    iget-object v1, p0, Lbyx;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 195
    :try_start_0
    iget-object v2, p0, Lbyx;->w:Lbor;

    const-string v0, "constraints"

    const/4 v3, 0x0

    invoke-static {p1, v0, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbor;

    invoke-virtual {v2, v0}, Lbor;->a(Lbor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbyx;->a(I)V

    .line 198
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lbza;)V
    .locals 2

    .prologue
    .line 258
    const-string v0, "theme"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 259
    iget-object v1, p0, Lbyx;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 260
    :try_start_0
    iget-object v0, p0, Lbyx;->o:Lbza;

    invoke-virtual {p1, v0}, Lbza;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 261
    iput-object p1, p0, Lbyx;->o:Lbza;

    .line 262
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lbyx;->a(I)V

    .line 264
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lbzf;)V
    .locals 2

    .prologue
    .line 207
    const-string v0, "clipSelector"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 208
    iget-object v1, p0, Lbyx;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 209
    :try_start_0
    iget-object v0, p0, Lbyx;->n:Lbzf;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 210
    iput-object p1, p0, Lbyx;->n:Lbzf;

    .line 211
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbyx;->a(I)V

    .line 213
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lbzg;)V
    .locals 2

    .prologue
    .line 220
    const-string v0, "clipSelectorLimits"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 221
    iget-object v1, p0, Lbyx;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 222
    :try_start_0
    iget-object v0, p0, Lbyx;->p:Lbzg;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 223
    iput-object p1, p0, Lbyx;->p:Lbzg;

    .line 224
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbyx;->a(I)V

    .line 226
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lbzp;)V
    .locals 2

    .prologue
    .line 295
    const-string v0, "effectApplier"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 296
    iget-object v1, p0, Lbyx;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 297
    :try_start_0
    iget-object v0, p0, Lbyx;->r:Lbzp;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 298
    iput-object p1, p0, Lbyx;->r:Lbzp;

    .line 299
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lbyx;->a(I)V

    .line 301
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 339
    const-string v0, "title"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 340
    iget-object v1, p0, Lbyx;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 341
    :try_start_0
    iget-object v0, p0, Lbyx;->m:Ljava/lang/String;

    invoke-static {p1, v0}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342
    monitor-exit v1

    .line 351
    :goto_0
    return-void

    .line 348
    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    iget-object v2, p0, Lbyx;->m:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eq v0, v2, :cond_1

    const/4 v0, 0x4

    .line 349
    :goto_1
    iput-object p1, p0, Lbyx;->m:Ljava/lang/String;

    .line 350
    invoke-direct {p0, v0}, Lbyx;->a(I)V

    .line 351
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 348
    :cond_1
    const/4 v0, 0x6

    goto :goto_1
.end method

.method public a(Ljava/util/List;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 244
    if-nez p1, :cond_1

    const/4 v0, 0x0

    .line 246
    :goto_0
    iget-object v1, p0, Lbyx;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 247
    :try_start_0
    iget-object v2, p0, Lbyx;->q:Ljava/util/List;

    invoke-static {p1, v2}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 248
    iput-object v0, p0, Lbyx;->q:Ljava/util/List;

    .line 249
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbyx;->a(I)V

    .line 251
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 245
    :cond_1
    invoke-static {p1}, Lcfi;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 251
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbon;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmw;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 316
    invoke-static {p1}, Lbyx;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    .line 317
    invoke-static {p2}, Lbyx;->c(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    .line 318
    iget-object v3, p0, Lbyx;->f:Ljava/lang/Object;

    monitor-enter v3

    .line 319
    if-nez p1, :cond_2

    .line 320
    :try_start_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 321
    :goto_0
    iput-object v0, p0, Lbyx;->i:Ljava/util/List;

    .line 323
    if-nez p2, :cond_3

    .line 324
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 325
    :goto_1
    iput-object v0, p0, Lbyx;->j:Ljava/util/List;

    .line 327
    iget-object v0, p0, Lbyx;->k:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 328
    iput-object v1, p0, Lbyx;->k:Ljava/util/List;

    .line 329
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbyx;->a(I)V

    .line 331
    :cond_0
    iget-object v0, p0, Lbyx;->l:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 332
    iput-object v2, p0, Lbyx;->l:Ljava/util/List;

    .line 333
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lbyx;->a(I)V

    .line 335
    :cond_1
    monitor-exit v3

    return-void

    .line 321
    :cond_2
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 325
    :cond_3
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_1

    .line 335
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lood;)V
    .locals 3

    .prologue
    .line 271
    iget-object v1, p0, Lbyx;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 272
    :try_start_0
    iget-object v0, p0, Lbyx;->t:Lood;

    invoke-static {v0, p1}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 273
    if-nez p1, :cond_1

    .line 274
    const/4 v0, 0x0

    iput-object v0, p0, Lbyx;->t:Lood;

    .line 285
    :goto_0
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lbyx;->a(I)V

    .line 287
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 278
    :cond_1
    :try_start_1
    new-instance v0, Lood;

    invoke-direct {v0}, Lood;-><init>()V

    .line 280
    invoke-static {p1}, Loxu;->a(Loxu;)[B

    move-result-object v2

    .line 278
    invoke-static {v0, v2}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lood;

    iput-object v0, p0, Lbyx;->t:Lood;
    :try_end_1
    .catch Loxt; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 281
    :catch_0
    move-exception v0

    .line 282
    :try_start_2
    const-string v2, "Couldn\'t copy cloud storyboard proto"

    invoke-static {v2, v0}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/Throwable;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 287
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 379
    iget-object v1, p0, Lbyx;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 380
    :try_start_0
    iput-boolean p1, p0, Lbyx;->s:Z

    .line 381
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lbyx;->a(I)V

    .line 382
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()J
    .locals 4

    .prologue
    .line 436
    iget-object v2, p0, Lbyx;->f:Ljava/lang/Object;

    monitor-enter v2

    .line 437
    :try_start_0
    iget-object v0, p0, Lbyx;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbyx;->j:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 438
    const-wide/16 v0, 0x0

    monitor-exit v2

    .line 448
    :goto_0
    return-wide v0

    .line 441
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lbyx;->i:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 442
    new-instance v1, Ljava/util/ArrayList;

    iget-object v3, p0, Lbyx;->j:Ljava/util/List;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 443
    iget-object v3, p0, Lbyx;->n:Lbzf;

    .line 444
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 448
    invoke-interface {v3, v0, v1}, Lbzf;->a(Ljava/util/List;Ljava/util/List;)J

    move-result-wide v0

    goto :goto_0

    .line 444
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b(Lbza;)V
    .locals 3

    .prologue
    .line 305
    iget-object v1, p0, Lbyx;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 306
    :try_start_0
    iget-object v0, p0, Lbyx;->c:Lbyt;

    iget-object v2, p1, Lbza;->t:Lbyh;

    invoke-virtual {v0, v2}, Lbyt;->a(Lbyh;)V

    .line 307
    iget-object v0, p0, Lbyx;->c:Lbyt;

    iget-object v2, p1, Lbza;->u:Lbyu;

    invoke-virtual {v0, v2}, Lbyt;->a(Lbyu;)V

    .line 308
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lbyx;->a(I)V

    .line 309
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lbph;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 386
    iget-object v1, p0, Lbyx;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 388
    :try_start_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 389
    iget-object v2, p0, Lbyx;->u:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    .line 391
    if-lt v0, v2, :cond_0

    iget-object v3, p0, Lbyx;->u:Ljava/util/List;

    const/4 v4, 0x0

    .line 392
    invoke-interface {p1, v4, v2}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 393
    :cond_0
    iget-object v0, p0, Lbyx;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lbyx;->v:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lbyx;->v:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lbyx;->a(I)V

    .line 394
    monitor-exit v1

    .line 404
    :goto_0
    return-void

    .line 398
    :cond_1
    invoke-interface {p1, v2, v0}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v0

    .line 399
    iget-object v2, p0, Lbyx;->v:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 400
    iget-object v2, p0, Lbyx;->v:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->clear()V

    iget-object v2, p0, Lbyx;->v:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lbyx;->a(I)V

    .line 401
    monitor-exit v1

    goto :goto_0

    .line 404
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method
