.class public final Lazd;
.super Layr;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    new-instance v0, Lays;

    invoke-direct {v0}, Lays;-><init>()V

    const v1, 0x7f020426

    .line 20
    invoke-virtual {v0, v1}, Lays;->a(I)Lays;

    move-result-object v0

    const v1, 0x7f020408

    .line 21
    invoke-virtual {v0, v1}, Lays;->b(I)Lays;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 22
    invoke-virtual {v0, v1}, Lays;->a(F)Lays;

    move-result-object v0

    const/4 v1, 0x1

    .line 23
    invoke-virtual {v0, v1}, Lays;->c(I)Lays;

    move-result-object v0

    const/4 v1, 0x5

    .line 24
    invoke-virtual {v0, v1}, Lays;->f(I)Lays;

    move-result-object v0

    .line 19
    invoke-direct {p0, v0}, Layr;-><init>(Lays;)V

    .line 25
    return-void
.end method


# virtual methods
.method protected a(Layj;Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 44
    new-instance v0, Layu;

    invoke-direct {v0}, Layu;-><init>()V

    .line 45
    invoke-virtual {v0, p1}, Layu;->a(Layj;)Layu;

    move-result-object v0

    .line 46
    invoke-virtual {v0, v4}, Layu;->e(I)Layu;

    move-result-object v0

    const/4 v1, 0x2

    .line 47
    invoke-virtual {v0, v1}, Layu;->f(I)Layu;

    move-result-object v0

    const/high16 v1, 0x42300000    # 44.0f

    .line 48
    invoke-virtual {v0, v1}, Layu;->b(F)Layu;

    move-result-object v0

    const v1, 0x7f0b00a4

    .line 49
    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Layu;->a(I)Layu;

    move-result-object v0

    const/high16 v1, 0x42100000    # 36.0f

    .line 50
    invoke-virtual {v0, v1}, Layu;->a(F)Layu;

    move-result-object v0

    const-string v1, "sans-serif"

    .line 51
    invoke-static {v1, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Layu;->a(Landroid/graphics/Typeface;)Layu;

    move-result-object v0

    .line 52
    invoke-virtual {v0, v4}, Layu;->a(Z)Layu;

    move-result-object v0

    const/16 v1, 0x64

    .line 53
    invoke-virtual {v0, v1}, Layu;->c(I)Layu;

    move-result-object v0

    const/4 v1, 0x0

    .line 54
    invoke-virtual {v0, v1}, Layu;->b(Z)Layu;

    move-result-object v0

    const-wide/32 v2, 0xf4240

    .line 55
    invoke-virtual {v0, v2, v3}, Layu;->b(J)Layu;

    move-result-object v0

    .line 56
    new-instance v1, Lazp;

    const v2, 0x7f02041f

    invoke-direct {v1, v0, v2, v4}, Lazp;-><init>(Layu;IZ)V

    return-object v1
.end method

.method protected f()Layj;
    .locals 1

    .prologue
    .line 63
    new-instance v0, Laze;

    invoke-direct {v0, p0}, Laze;-><init>(Layj;)V

    return-object v0
.end method

.method public g(Layl;)Landroid/graphics/Matrix;
    .locals 6

    .prologue
    const v5, 0x3ba3d70a    # 0.005f

    const v4, -0x445c28f6    # -0.005f

    .line 29
    const v0, -0x40cccccd    # -0.7f

    const v1, 0x3f333333    # 0.7f

    iget-wide v2, p1, Layl;->a:J

    long-to-int v2, v2

    invoke-static {v0, v1, v2}, Lbag;->a(FFI)F

    move-result v0

    .line 31
    iget-wide v2, p1, Layl;->a:J

    long-to-int v1, v2

    add-int/lit8 v1, v1, 0x1

    invoke-static {v4, v5, v1}, Lbag;->a(FFI)F

    move-result v1

    .line 33
    iget-wide v2, p1, Layl;->a:J

    long-to-int v2, v2

    add-int/lit8 v2, v2, 0x2

    invoke-static {v4, v5, v2}, Lbag;->a(FFI)F

    move-result v2

    .line 35
    invoke-super {p0, p1}, Layr;->g(Layl;)Landroid/graphics/Matrix;

    move-result-object v3

    .line 36
    invoke-virtual {v3, v1, v2}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 37
    iget v1, p1, Layl;->u:F

    invoke-static {v3, v0, v1}, Lbag;->a(Landroid/graphics/Matrix;FF)V

    .line 38
    return-object v3
.end method
