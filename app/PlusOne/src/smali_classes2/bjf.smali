.class public Lbjf;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lbjf;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbjf;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    const/16 v0, 0x1000

    invoke-direct {p0, v0}, Lbjf;-><init>(I)V

    .line 43
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput p1, p0, Lbjf;->b:I

    .line 51
    return-void
.end method

.method private static a(Lbjg;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 164
    :try_start_0
    invoke-interface {p0, p1}, Lbjg;->a(Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 165
    iget-object v1, p1, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-eq v1, v0, :cond_0

    iget-object v1, p1, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 166
    sget-object v1, Lbjf;->a:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    :cond_0
    :goto_0
    return-object v0

    .line 169
    :catch_0
    move-exception v0

    .line 170
    iget-object v1, p1, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 171
    sget-object v1, Lbjf;->a:Ljava/lang/String;

    const-string v2, "IllegalArgumentException while decoding to reusable bitmap"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 175
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 173
    :cond_1
    sget-object v1, Lbjf;->a:Ljava/lang/String;

    const-string v2, "IllegalArgumentException while decoding bitmap"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private static a(Lbjg;)Landroid/graphics/BitmapFactory$Options;
    .locals 2

    .prologue
    .line 150
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 151
    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 152
    invoke-interface {p0, v0}, Lbjg;->a(Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 153
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 154
    return-object v0
.end method

.method private a(Landroid/graphics/BitmapFactory$Options;I)V
    .locals 10

    .prologue
    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    .line 139
    iget v0, p0, Lbjf;->b:I

    iget v1, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    iget v2, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    mul-int/2addr v1, v2

    int-to-float v1, v1

    int-to-float v2, p2

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v2

    double-to-int v1, v2

    iget v2, p1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v3, p1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    int-to-float v2, v2

    int-to-float v0, v0

    div-float v0, v2, v0

    const/4 v2, 0x1

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    invoke-static {v8, v9}, Ljava/lang/Math;->log(D)D

    move-result-wide v6

    div-double/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v4

    invoke-static {v8, v9, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    double-to-int v0, v4

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p1, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 141
    return-void
.end method


# virtual methods
.method public a(Landroid/net/Uri;Landroid/content/ContentResolver;I)Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 86
    const-string v0, "decodeSampledBitmapFromUri"

    invoke-static {v0}, Laep;->a(Ljava/lang/String;)V

    .line 87
    const/4 v1, 0x0

    .line 89
    :try_start_0
    const-string v0, "r"

    invoke-virtual {p2, p1, v0}, Landroid/content/ContentResolver;->openFileDescriptor(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    .line 90
    new-instance v0, Lbjh;

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-direct {v0, v2}, Lbjh;-><init>(Ljava/io/FileDescriptor;)V

    .line 91
    invoke-static {v0}, Lbjf;->a(Lbjg;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v2

    .line 92
    invoke-direct {p0, v2, p3}, Lbjf;->a(Landroid/graphics/BitmapFactory$Options;I)V

    .line 93
    invoke-static {v0, v2}, Lbjf;->a(Lbjg;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 95
    if-eqz v1, :cond_0

    .line 96
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 98
    :cond_0
    invoke-static {}, Laep;->a()V

    return-object v0

    .line 95
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_1

    .line 96
    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 98
    :cond_1
    invoke-static {}, Laep;->a()V

    throw v0
.end method

.method public a(Ljava/io/FileDescriptor;ILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 75
    new-instance v3, Lbjh;

    invoke-direct {v3, p1}, Lbjh;-><init>(Ljava/io/FileDescriptor;)V

    invoke-static {v3}, Lbjf;->a(Lbjg;)Landroid/graphics/BitmapFactory$Options;

    move-result-object v4

    iput-boolean v1, v4, Landroid/graphics/BitmapFactory$Options;->inMutable:Z

    invoke-direct {p0, v4, p2}, Lbjf;->a(Landroid/graphics/BitmapFactory$Options;I)V

    if-eqz p3, :cond_2

    move v0, v1

    :goto_0
    invoke-static {}, Lceh;->a()Z

    move-result v5

    if-nez v5, :cond_0

    iget v5, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    if-ne v5, v1, :cond_3

    if-eqz p3, :cond_3

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    iget v6, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-ne v5, v6, :cond_3

    invoke-virtual {p3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    iget v6, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    if-ne v5, v6, :cond_3

    :goto_1
    and-int/2addr v0, v1

    :cond_0
    if-eqz v0, :cond_1

    iput-object p3, v4, Landroid/graphics/BitmapFactory$Options;->inBitmap:Landroid/graphics/Bitmap;

    :cond_1
    invoke-static {v3, v4}, Lbjf;->a(Lbjg;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 57
    iput p1, p0, Lbjf;->b:I

    .line 58
    return-void
.end method
