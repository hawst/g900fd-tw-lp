.class public Lbqw;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lbra;

.field private final c:I

.field private final d:Lbqz;

.field private final e:Ljava/io/File;

.field private final f:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lbqw;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbqw;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(ILbqz;Ljava/io/File;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lbqw;->f:Ljava/lang/Object;

    .line 69
    const-string v0, "capacity"

    invoke-static {p1, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lbqw;->c:I

    .line 70
    new-instance v0, Lbra;

    invoke-direct {v0, p1}, Lbra;-><init>(I)V

    iput-object v0, p0, Lbqw;->b:Lbra;

    .line 71
    const-string v0, "trackFetcher"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqz;

    iput-object v0, p0, Lbqw;->d:Lbqz;

    .line 72
    const-string v0, "cacheDirectory"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, Lbqw;->e:Ljava/io/File;

    .line 74
    invoke-direct {p0}, Lbqw;->a()V

    .line 75
    return-void
.end method

.method private a()V
    .locals 8

    .prologue
    .line 176
    iget-object v0, p0, Lbqw;->e:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 178
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 179
    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v1, v0

    .line 180
    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v6

    long-to-int v5, v6

    .line 181
    if-nez v5, :cond_0

    .line 183
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    .line 179
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 185
    :cond_0
    new-instance v6, Lbqy;

    invoke-direct {v6}, Lbqy;-><init>()V

    .line 186
    iput v5, v6, Lbqy;->a:I

    .line 187
    iput-object v4, v6, Lbqy;->b:Ljava/io/File;

    .line 188
    invoke-virtual {v2, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 192
    :cond_1
    new-instance v0, Lbqx;

    invoke-direct {v0}, Lbqx;-><init>()V

    invoke-static {v2, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 198
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqy;

    .line 199
    iget-object v2, p0, Lbqw;->b:Lbra;

    iget-object v3, v0, Lbqy;->b:Ljava/io/File;

    invoke-virtual {v3}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/math/BigInteger;

    const/16 v5, 0x10

    invoke-direct {v4, v3, v5}, Ljava/math/BigInteger;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v4}, Ljava/math/BigInteger;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lbra;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 201
    :cond_2
    return-void
.end method

.method private b(JI)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 107
    iget v0, p0, Lbqw;->c:I

    if-le p3, v0, :cond_0

    .line 108
    sget-object v0, Lbqw;->a:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x4a

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Requested track byte count "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was greater than the cache capacity"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v7

    .line 157
    :goto_0
    return v0

    .line 113
    :cond_0
    iget-object v0, p0, Lbqw;->b:Lbra;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Lbra;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqy;

    .line 116
    if-eqz v0, :cond_3

    .line 117
    iget v1, v0, Lbqy;->a:I

    if-lt v1, p3, :cond_1

    .line 124
    iget-object v0, v0, Lbqy;->b:Ljava/io/File;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/io/File;->setLastModified(J)Z

    move v0, v8

    .line 125
    goto :goto_0

    .line 127
    :cond_1
    iget-object v1, v0, Lbqy;->b:Ljava/io/File;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/io/File;->setLastModified(J)Z

    .line 135
    :goto_1
    iget v1, v0, Lbqy;->a:I

    sub-int v1, p3, v1

    .line 139
    iget-object v2, p0, Lbqw;->b:Lbra;

    iget v3, p0, Lbqw;->c:I

    sub-int v1, v3, v1

    invoke-virtual {v2, v1}, Lbra;->trimToSize(I)V

    .line 142
    if-lez p3, :cond_2

    .line 143
    :try_start_0
    iget-object v1, p0, Lbqw;->d:Lbqz;

    iget-object v4, v0, Lbqy;->b:Ljava/io/File;

    iget v5, v0, Lbqy;->a:I

    move-wide v2, p1

    move v6, p3

    invoke-interface/range {v1 .. v6}, Lbqz;->a(JLjava/io/File;II)V

    .line 148
    :cond_2
    iput p3, v0, Lbqy;->a:I

    .line 152
    iget-object v1, p0, Lbqw;->b:Lbra;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lbra;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v8

    .line 153
    goto :goto_0

    .line 130
    :cond_3
    new-instance v0, Lbqy;

    invoke-direct {v0}, Lbqy;-><init>()V

    .line 131
    iput v7, v0, Lbqy;->a:I

    .line 132
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lbqw;->e:Ljava/io/File;

    invoke-static {p1, p2}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    iput-object v1, v0, Lbqy;->b:Ljava/io/File;

    .line 133
    iget-object v1, v0, Lbqy;->b:Ljava/io/File;

    invoke-virtual {v1}, Ljava/io/File;->length()J

    move-result-wide v2

    const-string v1, "entry.file.length()"

    const-wide/16 v4, 0x0

    invoke-static {v2, v3, v1, v4, v5}, Lcgp;->a(JLjava/lang/CharSequence;J)V

    goto :goto_1

    .line 154
    :catch_0
    move-exception v1

    .line 155
    sget-object v2, Lbqw;->a:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x31

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Failure while fetching track "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 156
    iget-object v0, v0, Lbqy;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move v0, v7

    .line 157
    goto/16 :goto_0
.end method


# virtual methods
.method public a(J)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 78
    iget-object v0, p0, Lbqw;->e:Ljava/io/File;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public a(JI)Z
    .locals 3

    .prologue
    .line 97
    iget-object v1, p0, Lbqw;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 98
    :try_start_0
    invoke-direct {p0, p1, p2, p3}, Lbqw;->b(JI)Z

    move-result v0

    monitor-exit v1

    return v0

    .line 99
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
