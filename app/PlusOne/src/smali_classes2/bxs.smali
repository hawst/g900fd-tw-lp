.class public final Lbxs;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbok;",
            ">;"
        }
    .end annotation
.end field

.field public final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbol;",
            ">;"
        }
    .end annotation
.end field

.field public final c:J

.field public final d:Lbor;

.field public final e:J


# direct methods
.method constructor <init>(Lbxt;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Lbor;

    invoke-direct {v0}, Lbor;-><init>()V

    iput-object v0, p0, Lbxs;->d:Lbor;

    .line 31
    iget-object v0, p1, Lbxt;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbxs;->a:Ljava/util/List;

    .line 33
    iget-object v0, p1, Lbxt;->b:Ljava/util/Map;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lbxs;->b:Ljava/util/Map;

    .line 34
    iget-wide v0, p1, Lbxt;->d:J

    iput-wide v0, p0, Lbxs;->c:J

    .line 35
    iget-object v0, p0, Lbxs;->d:Lbor;

    iget-object v1, p1, Lbxt;->c:Lbor;

    invoke-virtual {v0, v1}, Lbor;->a(Lbor;)Z

    .line 36
    iget-wide v0, p1, Lbxt;->e:J

    iput-wide v0, p0, Lbxs;->e:J

    .line 37
    return-void
.end method
