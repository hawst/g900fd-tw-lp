.class public final Lbul;
.super Loxu;
.source "PG"


# static fields
.field public static final a:[Lbul;


# instance fields
.field public b:Ljava/lang/String;

.field public c:[J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 306
    const/4 v0, 0x0

    new-array v0, v0, [Lbul;

    sput-object v0, Lbul;->a:[Lbul;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 307
    invoke-direct {p0}, Loxu;-><init>()V

    .line 310
    const-string v0, ""

    iput-object v0, p0, Lbul;->b:Ljava/lang/String;

    .line 313
    sget-object v0, Loxx;->b:[J

    iput-object v0, p0, Lbul;->c:[J

    .line 307
    return-void
.end method


# virtual methods
.method public a()I
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 337
    .line 338
    iget-object v0, p0, Lbul;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 339
    const/4 v0, 0x1

    iget-object v2, p0, Lbul;->b:Ljava/lang/String;

    .line 340
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 342
    :goto_0
    iget-object v2, p0, Lbul;->c:[J

    if-eqz v2, :cond_1

    iget-object v2, p0, Lbul;->c:[J

    array-length v2, v2

    if-lez v2, :cond_1

    .line 344
    iget-object v3, p0, Lbul;->c:[J

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_0

    aget-wide v6, v3, v1

    .line 346
    invoke-static {v6, v7}, Loxo;->f(J)I

    move-result v5

    add-int/2addr v2, v5

    .line 344
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 348
    :cond_0
    add-int/2addr v0, v2

    .line 349
    iget-object v1, p0, Lbul;->c:[J

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 351
    :cond_1
    iput v0, p0, Lbul;->ai:I

    .line 352
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lbul;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 360
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 361
    sparse-switch v0, :sswitch_data_0

    .line 365
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 366
    :sswitch_0
    return-object p0

    .line 371
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbul;->b:Ljava/lang/String;

    goto :goto_0

    .line 375
    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 376
    iget-object v0, p0, Lbul;->c:[J

    array-length v0, v0

    .line 377
    add-int/2addr v1, v0

    new-array v1, v1, [J

    .line 378
    iget-object v2, p0, Lbul;->c:[J

    invoke-static {v2, v4, v1, v4, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 379
    iput-object v1, p0, Lbul;->c:[J

    .line 380
    :goto_1
    iget-object v1, p0, Lbul;->c:[J

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    .line 381
    iget-object v1, p0, Lbul;->c:[J

    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v2

    aput-wide v2, v1, v0

    .line 382
    invoke-virtual {p1}, Loxn;->a()I

    .line 380
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 385
    :cond_1
    iget-object v1, p0, Lbul;->c:[J

    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v2

    aput-wide v2, v1, v0

    goto :goto_0

    .line 361
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    .line 325
    iget-object v0, p0, Lbul;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 326
    const/4 v0, 0x1

    iget-object v1, p0, Lbul;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 328
    :cond_0
    iget-object v0, p0, Lbul;->c:[J

    if-eqz v0, :cond_1

    .line 329
    iget-object v1, p0, Lbul;->c:[J

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-wide v4, v1, v0

    .line 330
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v4, v5}, Loxo;->a(IJ)V

    .line 329
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 333
    :cond_1
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 303
    invoke-virtual {p0, p1}, Lbul;->a(Loxn;)Lbul;

    move-result-object v0

    return-object v0
.end method
