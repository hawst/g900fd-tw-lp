.class public final Lgny;
.super Lghh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lghh",
        "<",
        "Lgns;",
        ">;"
    }
.end annotation


# static fields
.field private static volatile g:Landroid/os/Bundle;

.field private static volatile h:Landroid/os/Bundle;


# instance fields
.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private final f:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lgzn;",
            "Lgog;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lggb;Lggc;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3, p4}, Lghh;-><init>(Landroid/content/Context;Landroid/os/Looper;Lggb;Lggc;)V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lgny;->f:Ljava/util/HashMap;

    iput-object p5, p0, Lgny;->d:Ljava/lang/String;

    iput-object p6, p0, Lgny;->e:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(ILjava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;
    .locals 2

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    return-object v1

    :cond_0
    const-string v0, "pendingIntent"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    goto :goto_0
.end method

.method private a(Lgzn;)Lgog;
    .locals 3

    iget-object v1, p0, Lgny;->f:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lgny;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgny;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgog;

    monitor-exit v1

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lgog;

    invoke-direct {v0, p0, p1}, Lgog;-><init>(Lgny;Lgzn;)V

    iget-object v2, p0, Lgny;->f:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic a(Lgny;Lcom/google/android/gms/common/data/DataHolder;)Lgzx;
    .locals 4

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lgzx;

    new-instance v1, Lgpa;

    sget-object v2, Lgny;->h:Landroid/os/Bundle;

    invoke-direct {v1, v2}, Lgpa;-><init>(Landroid/os/Bundle;)V

    new-instance v2, Lgoz;

    sget-object v3, Lgny;->g:Landroid/os/Bundle;

    invoke-direct {v2, v3}, Lgoz;-><init>(Landroid/os/Bundle;)V

    invoke-direct {v0, p1, v1, v2}, Lgzx;-><init>(Lcom/google/android/gms/common/data/DataHolder;Lgpa;Lgoz;)V

    goto :goto_0
.end method

.method static synthetic a(Lgny;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lgny;->f:Ljava/util/HashMap;

    return-object v0
.end method


# virtual methods
.method protected synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, Lgny;->b(Landroid/os/IBinder;)Lgns;

    move-result-object v0

    return-object v0
.end method

.method protected a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 1

    if-nez p1, :cond_0

    if-eqz p3, :cond_0

    const-string v0, "post_init_configuration"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lgny;->a(Landroid/os/Bundle;)V

    :cond_0
    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-super {p0, p1, p2, v0}, Lghh;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    return-void

    :cond_1
    const-string v0, "post_init_resolution"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public declared-synchronized a(Landroid/os/Bundle;)V
    .locals 2

    monitor-enter p0

    if-nez p1, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_0
    const-string v0, "use_contactables_api"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Lgox;->a(Z)V

    sget-object v0, Lgnx;->a:Lgnx;

    invoke-virtual {v0, p1}, Lgnx;->a(Landroid/os/Bundle;)V

    const-string v0, "config.email_type_map"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    sput-object v0, Lgny;->g:Landroid/os/Bundle;

    const-string v0, "config.phone_type_map"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    sput-object v0, Lgny;->h:Landroid/os/Bundle;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Lgfy;Ljava/lang/String;Ljava/lang/String;Lgyy;)V
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfy",
            "<",
            "Lgyz;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lgyy;",
            ")V"
        }
    .end annotation

    if-nez p4, :cond_0

    sget-object p4, Lgyy;->a:Lgyy;

    :cond_0
    invoke-virtual/range {p4 .. p4}, Lgyy;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p4 .. p4}, Lgyy;->b()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual/range {p4 .. p4}, Lgyy;->c()I

    move-result v6

    invoke-virtual/range {p4 .. p4}, Lgyy;->d()Z

    move-result v7

    invoke-virtual/range {p4 .. p4}, Lgyy;->e()J

    move-result-wide v8

    invoke-virtual/range {p4 .. p4}, Lgyy;->f()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p4 .. p4}, Lgyy;->g()I

    move-result v11

    invoke-virtual/range {p4 .. p4}, Lgyy;->h()I

    move-result v12

    invoke-virtual/range {p4 .. p4}, Lgyy;->i()I

    move-result v13

    invoke-virtual {p0}, Lgny;->k()V

    new-instance v1, Lgok;

    invoke-direct {v1, p0, p1}, Lgok;-><init>(Lgny;Lgfy;)V

    :try_start_0
    invoke-virtual {p0}, Lgny;->j()Lgns;

    move-result-object v0

    if-nez v2, :cond_1

    const/4 v5, 0x0

    :goto_0
    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-interface/range {v0 .. v13}, Lgns;->a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;III)V

    :goto_1
    return-void

    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lgok;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_1
.end method

.method public a(Lgfy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfy",
            "<",
            "Lggf;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-virtual {p0}, Lgny;->k()V

    new-instance v0, Lgoh;

    invoke-direct {v0, p0, p1}, Lgoh;-><init>(Lgny;Lgfy;)V

    :try_start_0
    invoke-virtual {p0}, Lgny;->j()Lgns;

    move-result-object v1

    invoke-interface {v1, v0, p2, p3, p4}, Lgns;->a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2, v2}, Lgoh;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public a(Lgfy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfy",
            "<",
            "Lgyv;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    invoke-virtual {p0}, Lgny;->k()V

    new-instance v1, Lgof;

    invoke-direct {v1, p0, p1}, Lgof;-><init>(Lgny;Lgfy;)V

    :try_start_0
    invoke-virtual {p0}, Lgny;->j()Lgns;

    move-result-object v0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    move v7, p7

    invoke-interface/range {v0 .. v7}, Lgns;->a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lgof;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method

.method public a(Lgfy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfy",
            "<",
            "Lgzb;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v6, 0x0

    invoke-virtual {p0}, Lgny;->k()V

    new-instance v1, Lgod;

    invoke-direct {v1, p0, p1}, Lgod;-><init>(Lgny;Lgfy;)V

    :try_start_0
    invoke-virtual {p0}, Lgny;->j()Lgns;

    move-result-object v0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lgns;->a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v6, v6}, Lgod;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public a(Lgfy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfy",
            "<",
            "Lggf;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    invoke-virtual {p0}, Lgny;->k()V

    new-instance v1, Lgoh;

    invoke-direct {v1, p0, p1}, Lgoh;-><init>(Lgny;Lgfy;)V

    :try_start_0
    invoke-virtual {p0}, Lgny;->j()Lgns;

    move-result-object v0

    if-nez p6, :cond_0

    const/4 v6, 0x0

    :goto_0
    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v7, p7

    invoke-interface/range {v0 .. v7}, Lgns;->a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    :goto_1
    return-void

    :cond_0
    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_1

    const/4 v6, 0x2

    goto :goto_0

    :cond_1
    const/4 v6, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lgoh;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method public a(Lgfy;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/internal/jn;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfy",
            "<",
            "Lgzc;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/gms/internal/jn;",
            ")V"
        }
    .end annotation

    invoke-virtual {p0}, Lgny;->k()V

    new-instance v1, Lgol;

    invoke-direct {v1, p0, p1}, Lgol;-><init>(Lgny;Lgfy;)V

    :try_start_0
    invoke-virtual {p0}, Lgny;->j()Lgns;

    move-result-object v0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-interface/range {v0 .. v7}, Lgns;->a(Lgnp;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/internal/jn;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lgol;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public a(Lgfy;Ljava/lang/String;Z[Ljava/lang/String;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfy",
            "<",
            "Lggf;",
            ">;",
            "Ljava/lang/String;",
            "Z[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    const/4 v2, 0x0

    invoke-virtual {p0}, Lgny;->k()V

    new-instance v0, Lgoh;

    invoke-direct {v0, p0, p1}, Lgoh;-><init>(Lgny;Lgfy;)V

    :try_start_0
    invoke-virtual {p0}, Lgny;->j()Lgns;

    move-result-object v1

    invoke-interface {v1, v0, p2, p3, p4}, Lgns;->a(Lgnp;Ljava/lang/String;Z[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v2, v2}, Lgoh;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public a(Lgfy;ZZLjava/lang/String;Ljava/lang/String;I)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfy",
            "<",
            "Lgyx;",
            ">;ZZ",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    const/4 v7, 0x0

    invoke-virtual {p0}, Lgny;->k()V

    new-instance v1, Lgoj;

    invoke-direct {v1, p0, p1}, Lgoj;-><init>(Lgny;Lgfy;)V

    :try_start_0
    invoke-virtual {p0}, Lgny;->j()Lgns;

    move-result-object v0

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lgns;->a(Lgnp;ZZLjava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v7, v7}, Lgoj;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method

.method protected a(Lgic;Lghl;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "social_client_application_id"

    iget-object v2, p0, Lgny;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "real_client_package_name"

    iget-object v2, p0, Lgny;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x5e3530

    iget-object v2, p0, Lghh;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lgic;->b(Lghz;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Z)V
    .locals 1

    invoke-virtual {p0}, Lgny;->k()V

    invoke-virtual {p0}, Lgny;->j()Lgns;

    move-result-object v0

    invoke-interface {v0, p1}, Lgns;->a(Z)V

    return-void
.end method

.method public a(Lgzn;Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 8

    const/4 v6, 0x1

    invoke-virtual {p0}, Lgny;->k()V

    iget-object v7, p0, Lgny;->f:Ljava/util/HashMap;

    monitor-enter v7

    :try_start_0
    invoke-direct {p0, p1}, Lgny;->a(Lgzn;)Lgog;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :try_start_1
    invoke-virtual {p0}, Lgny;->j()Lgns;

    move-result-object v0

    const/4 v2, 0x1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lgns;->a(Lgnp;ZLjava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    monitor-exit v7

    move v0, v6

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "PeopleClient"

    const-string v2, "Failed to register listener"

    invoke-static {v1, v2, v0}, Lgon;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    monitor-exit v7

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;JZZ)Z
    .locals 9

    invoke-virtual {p0}, Lgny;->k()V

    :try_start_0
    invoke-virtual {p0}, Lgny;->j()Lgns;

    move-result-object v1

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    move v7, p6

    invoke-interface/range {v1 .. v7}, Lgns;->a(Ljava/lang/String;Ljava/lang/String;JZZ)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const-string v1, "PeopleClient"

    const-string v2, "Service call failed."

    invoke-static {v1, v2, v0}, Lgon;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(Landroid/os/IBinder;)Lgns;
    .locals 1

    invoke-static {p1}, Lgnt;->a(Landroid/os/IBinder;)Lgns;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 8

    iget-object v6, p0, Lgny;->f:Ljava/util/HashMap;

    monitor-enter v6

    :try_start_0
    invoke-virtual {p0}, Lgny;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgny;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lgog;

    invoke-virtual {p0}, Lgny;->j()Lgns;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lgns;->a(Lgnp;ZLjava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_0
    :goto_1
    :try_start_1
    iget-object v0, p0, Lgny;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-super {p0}, Lghh;->b()V

    return-void

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public b(Lghj;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lghh",
            "<",
            "Lgns;",
            ">.ghj<*>;)V"
        }
    .end annotation

    invoke-super {p0, p1}, Lghh;->a(Lghj;)V

    return-void
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.people.service.START"

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.people.internal.IPeopleService"

    return-object v0
.end method

.method protected j()Lgns;
    .locals 1

    invoke-super {p0}, Lghh;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lgns;

    return-object v0
.end method

.method protected k()V
    .locals 0

    invoke-super {p0}, Lghh;->h()V

    return-void
.end method
