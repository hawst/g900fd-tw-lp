.class public final Latq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Latm;

.field private final b:Lbjs;

.field private final c:Latr;

.field private final d:Lchl;

.field private e:I

.field private f:Landroid/media/MediaFormat;

.field private g:J

.field private h:Lats;


# direct methods
.method public constructor <init>(Latm;Lbjs;Latr;Lchl;)V
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const/4 v0, -0x1

    iput v0, p0, Latq;->e:I

    .line 87
    iput-object p1, p0, Latq;->a:Latm;

    .line 88
    iput-object p2, p0, Latq;->b:Lbjs;

    .line 89
    iput-object p3, p0, Latq;->c:Latr;

    .line 90
    iput-object p4, p0, Latq;->d:Lchl;

    .line 91
    return-void
.end method


# virtual methods
.method public a()Lato;
    .locals 11

    .prologue
    .line 126
    new-instance v1, Lato;

    iget-object v2, p0, Latq;->a:Latm;

    iget-object v3, p0, Latq;->b:Lbjs;

    iget-object v4, p0, Latq;->c:Latr;

    iget-object v5, p0, Latq;->d:Lchl;

    iget v6, p0, Latq;->e:I

    iget-object v7, p0, Latq;->f:Landroid/media/MediaFormat;

    iget-wide v8, p0, Latq;->g:J

    iget-object v10, p0, Latq;->h:Lats;

    invoke-direct/range {v1 .. v10}, Lato;-><init>(Latm;Lbjs;Latr;Lchl;ILandroid/media/MediaFormat;JLats;)V

    return-object v1
.end method

.method public a(ILandroid/media/MediaFormat;)Latq;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 100
    const-string v0, "trackIndex"

    invoke-static {p1, v0, v1}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Latq;->e:I

    .line 101
    const-string v0, "mediaFormat"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaFormat;

    iput-object v0, p0, Latq;->f:Landroid/media/MediaFormat;

    .line 102
    return-object p0
.end method

.method public a(J)Latq;
    .locals 1

    .prologue
    .line 113
    iput-wide p1, p0, Latq;->g:J

    .line 114
    return-object p0
.end method

.method public a(Lats;)Latq;
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Latq;->h:Lats;

    .line 122
    return-object p0
.end method
