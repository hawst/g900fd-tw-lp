.class final Lbfi;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field private a:Z

.field private synthetic b:Lbff;


# direct methods
.method constructor <init>(Lbff;)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lbfi;->b:Lbff;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Landroid/widget/SeekBar;)V
    .locals 3

    .prologue
    .line 214
    iget-object v0, p0, Lbfi;->b:Lbff;

    invoke-static {v0}, Lbff;->a(Lbff;)Lasm;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lbfi;->b:Lbff;

    invoke-static {v0}, Lbff;->a(Lbff;)Lasm;

    move-result-object v1

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    iget-boolean v0, p0, Lbfi;->a:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v2, v0}, Lasm;->a(IZ)V

    .line 217
    :cond_0
    return-void

    .line 215
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1

    .prologue
    .line 199
    iget-object v0, p0, Lbfi;->b:Lbff;

    invoke-static {v0}, Lbff;->e(Lbff;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-direct {p0, v0}, Lbfi;->a(Landroid/widget/SeekBar;)V

    .line 200
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbfi;->a:Z

    .line 205
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 1

    .prologue
    .line 209
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbfi;->a:Z

    .line 210
    iget-object v0, p0, Lbfi;->b:Lbff;

    invoke-static {v0}, Lbff;->e(Lbff;)Landroid/widget/SeekBar;

    move-result-object v0

    invoke-direct {p0, v0}, Lbfi;->a(Landroid/widget/SeekBar;)V

    .line 211
    return-void
.end method
