.class public final Lbkt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lcei;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcei",
        "<",
        "Lbvf;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:[Lbvg;

.field private static final b:[Lbve;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 22
    new-array v0, v1, [Lbvg;

    sput-object v0, Lbkt;->a:[Lbvg;

    .line 23
    new-array v0, v1, [Lbve;

    sput-object v0, Lbkt;->b:[Lbve;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lbvf;Lcgm;Loxo;)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbvf;",
            "Lcgm",
            "<",
            "Lbvf;",
            ">;",
            "Loxo;",
            ")I"
        }
    .end annotation

    .prologue
    .line 97
    if-eqz p3, :cond_0

    .line 98
    invoke-virtual {p3}, Loxo;->a()I

    move-result v0

    .line 99
    invoke-interface {p2, p1, p3}, Lcgm;->a(Loxu;Loxo;)V

    .line 100
    invoke-virtual {p3}, Loxo;->a()I

    move-result v1

    sub-int/2addr v0, v1

    .line 102
    :goto_0
    return v0

    :cond_0
    invoke-interface {p2, p1}, Lcgm;->a(Loxu;)I

    move-result v0

    goto :goto_0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 111
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->isInterrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    new-instance v0, Ljava/lang/InterruptedException;

    invoke-direct {v0}, Ljava/lang/InterruptedException;-><init>()V

    throw v0

    .line 114
    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lbvf;Loxo;Lcgm;)I
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbvf;",
            "Loxo;",
            "Lcgm",
            "<",
            "Lbvf;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 36
    const-string v0, "message"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 37
    const-string v0, "serializer"

    invoke-static {p3, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 39
    iget-object v4, p1, Lbvf;->a:Ljava/lang/String;

    .line 40
    iget-object v5, p1, Lbvf;->c:[Lbvg;

    .line 41
    iget-object v6, p1, Lbvf;->d:[Lbve;

    .line 45
    :try_start_0
    sget-object v0, Lbkt;->b:[Lbve;

    iput-object v0, p1, Lbvf;->d:[Lbve;

    .line 46
    invoke-direct {p0}, Lbkt;->a()V

    .line 47
    array-length v0, v5

    .line 48
    add-int/lit16 v0, v0, 0x708

    add-int/lit8 v0, v0, -0x1

    div-int/lit16 v7, v0, 0x708

    move v3, v1

    move v2, v1

    .line 49
    :goto_0
    if-ge v3, v7, :cond_0

    .line 50
    mul-int/lit16 v0, v3, 0x708

    add-int/lit8 v8, v3, 0x1

    mul-int/lit16 v8, v8, 0x708

    array-length v9, v5

    .line 53
    invoke-static {v8, v9}, Ljava/lang/Math;->min(II)I

    move-result v8

    .line 50
    invoke-static {v5, v0, v8}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbvg;

    iput-object v0, p1, Lbvf;->c:[Lbvg;

    .line 56
    invoke-direct {p0}, Lbkt;->a()V

    .line 57
    invoke-direct {p0, p1, p3, p2}, Lbkt;->a(Lbvf;Lcgm;Loxo;)I

    move-result v0

    add-int/2addr v2, v0

    .line 58
    const-string v0, ""

    iput-object v0, p1, Lbvf;->a:Ljava/lang/String;

    .line 49
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 61
    :cond_0
    sget-object v0, Lbkt;->a:[Lbvg;

    iput-object v0, p1, Lbvf;->c:[Lbvg;

    .line 62
    array-length v0, v6

    .line 63
    add-int/lit8 v0, v0, 0x5

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v3, v0, 0x5

    move v10, v1

    move v1, v2

    move v2, v10

    .line 64
    :goto_1
    if-ge v2, v3, :cond_1

    .line 65
    mul-int/lit8 v0, v2, 0x5

    add-int/lit8 v7, v2, 0x1

    mul-int/lit8 v7, v7, 0x5

    array-length v8, v6

    .line 68
    invoke-static {v7, v8}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 65
    invoke-static {v6, v0, v7}, Ljava/util/Arrays;->copyOfRange([Ljava/lang/Object;II)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbve;

    iput-object v0, p1, Lbvf;->d:[Lbve;

    .line 71
    invoke-direct {p0}, Lbkt;->a()V

    .line 72
    invoke-direct {p0, p1, p3, p2}, Lbkt;->a(Lbvf;Lcgm;Loxo;)I

    move-result v0

    add-int/2addr v1, v0

    .line 73
    const-string v0, ""

    iput-object v0, p1, Lbvf;->a:Ljava/lang/String;

    .line 64
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 75
    :cond_1
    invoke-direct {p0}, Lbkt;->a()V

    .line 76
    iget-object v0, p1, Lbvf;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 79
    invoke-direct {p0, p1, p3, p2}, Lbkt;->a(Lbvf;Lcgm;Loxo;)I

    move-result v0

    add-int/2addr v0, v1

    .line 80
    sget-object v1, Lbkt;->a:[Lbvg;

    iput-object v1, p1, Lbvf;->c:[Lbvg;

    .line 81
    sget-object v1, Lbkt;->b:[Lbve;

    iput-object v1, p1, Lbvf;->d:[Lbve;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    :goto_2
    iput-object v4, p1, Lbvf;->a:Ljava/lang/String;

    .line 86
    iput-object v5, p1, Lbvf;->c:[Lbvg;

    .line 87
    iput-object v6, p1, Lbvf;->d:[Lbve;

    .line 89
    return v0

    .line 85
    :catchall_0
    move-exception v0

    iput-object v4, p1, Lbvf;->a:Ljava/lang/String;

    .line 86
    iput-object v5, p1, Lbvf;->c:[Lbvg;

    .line 87
    iput-object v6, p1, Lbvf;->d:[Lbve;

    throw v0

    :cond_2
    move v0, v1

    goto :goto_2
.end method

.method public bridge synthetic a(Loxu;Loxo;Lcgm;)I
    .locals 1

    .prologue
    .line 20
    check-cast p1, Lbvf;

    invoke-virtual {p0, p1, p2, p3}, Lbkt;->a(Lbvf;Loxo;Lcgm;)I

    move-result v0

    return v0
.end method
