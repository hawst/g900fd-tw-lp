.class public final Laks;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Livo;


# instance fields
.field private final a:Landroid/net/Uri;

.field private final b:Laic;

.field private final c:Lald;

.field private final d:Lamy;

.field private e:Lakt;

.field private f:Lakt;

.field private g:Lakv;

.field private h:Z

.field private i:J

.field private j:J


# direct methods
.method public constructor <init>(Landroid/net/Uri;Laic;Lald;Lamy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-string v0, "uri"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Laks;->a:Landroid/net/Uri;

    .line 36
    const-string v0, "spec"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laic;

    iput-object v0, p0, Laks;->b:Laic;

    .line 37
    const-string v0, "appInfo"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lald;

    iput-object v0, p0, Laks;->c:Lald;

    .line 38
    const-string v0, "deviceInfo"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lamy;

    iput-object v0, p0, Laks;->d:Lamy;

    .line 39
    return-void
.end method


# virtual methods
.method public a()Laib;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Laks;->b:Laic;

    iget-object v0, v0, Laic;->a:Laib;

    return-object v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 61
    iput-wide p1, p0, Laks;->j:J

    .line 62
    return-void
.end method

.method public a(Lakq;)V
    .locals 1

    .prologue
    .line 101
    invoke-virtual {p0}, Laks;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lakq;->a(Ljava/lang/String;)V

    .line 102
    return-void
.end method

.method public a(Lakt;)V
    .locals 2

    .prologue
    .line 42
    const-string v0, "startCounters"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakt;

    iput-object v0, p0, Laks;->e:Lakt;

    .line 43
    return-void
.end method

.method public a(Lakv;)V
    .locals 2

    .prologue
    .line 50
    const-string v0, "analyzedVideoInfo"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakv;

    iput-object v0, p0, Laks;->g:Lakv;

    .line 51
    return-void
.end method

.method public a(Livm;)V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 121
    iget-object v0, p0, Laks;->f:Lakt;

    if-nez v0, :cond_0

    .line 122
    const-string v0, "Ongoing analysis of URI:%s with spec:%s started %d ms ago"

    new-array v1, v7, [Ljava/lang/Object;

    iget-object v2, p0, Laks;->a:Landroid/net/Uri;

    aput-object v2, v1, v3

    iget-object v2, p0, Laks;->b:Laic;

    aput-object v2, v1, v4

    .line 125
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Laks;->e:Lakt;

    iget-wide v4, v4, Lakt;->a:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    .line 122
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Livm;->a(Ljava/lang/String;)V

    .line 137
    :goto_0
    return-void

    .line 126
    :cond_0
    iget-boolean v0, p0, Laks;->h:Z

    if-eqz v0, :cond_1

    .line 127
    const-string v0, "Completed analysis of URI:%s with spec:%s ended %d ms ago"

    new-array v1, v7, [Ljava/lang/Object;

    iget-object v2, p0, Laks;->a:Landroid/net/Uri;

    aput-object v2, v1, v3

    iget-object v2, p0, Laks;->b:Laic;

    aput-object v2, v1, v4

    .line 130
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Laks;->f:Lakt;

    iget-wide v4, v4, Lakt;->a:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    .line 127
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Livm;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 132
    :cond_1
    const-string v0, "Canceled analysis of URI:%s with spec:%s ended %d ms ago; analyzed up to %d"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Laks;->a:Landroid/net/Uri;

    aput-object v2, v1, v3

    iget-object v2, p0, Laks;->b:Laic;

    aput-object v2, v1, v4

    .line 134
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Laks;->f:Lakt;

    iget-wide v4, v4, Lakt;->a:J

    sub-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    iget-wide v2, p0, Laks;->j:J

    .line 135
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v7

    .line 132
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Livm;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 54
    iput-boolean p1, p0, Laks;->h:Z

    .line 55
    return-void
.end method

.method public b()J
    .locals 4

    .prologue
    .line 76
    iget-wide v0, p0, Laks;->j:J

    iget-wide v2, p0, Laks;->i:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public b(J)V
    .locals 1

    .prologue
    .line 68
    iput-wide p1, p0, Laks;->i:J

    .line 69
    return-void
.end method

.method public b(Lakt;)V
    .locals 2

    .prologue
    .line 46
    const-string v0, "endCounters"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lakt;

    iput-object v0, p0, Laks;->f:Lakt;

    .line 47
    return-void
.end method

.method public c()F
    .locals 4

    .prologue
    .line 93
    iget-object v0, p0, Laks;->f:Lakt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laks;->e:Lakt;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Laks;->j:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 94
    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    .line 97
    :goto_0
    return v0

    .line 96
    :cond_1
    iget-object v0, p0, Laks;->f:Lakt;

    iget-wide v0, v0, Lakt;->a:J

    iget-object v2, p0, Laks;->e:Lakt;

    iget-wide v2, v2, Lakt;->a:J

    sub-long/2addr v0, v2

    long-to-float v0, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v1

    .line 97
    invoke-virtual {p0}, Laks;->b()J

    move-result-wide v2

    long-to-float v1, v2

    div-float/2addr v0, v1

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 106
    .line 107
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Laks;->a:Landroid/net/Uri;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Laks;->b:Laic;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Laks;->c:Lald;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Laks;->d:Lamy;

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Laks;->e:Lakt;

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Laks;->f:Lakt;

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget-object v3, p0, Laks;->g:Lakv;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-boolean v3, p0, Laks;->h:Z

    .line 115
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-wide v4, p0, Laks;->j:J

    .line 116
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    .line 106
    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
