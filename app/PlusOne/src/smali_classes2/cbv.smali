.class public final Lcbv;
.super Landroid/graphics/drawable/Drawable;
.source "PG"


# instance fields
.field private final a:Landroid/graphics/RectF;

.field private final b:Landroid/animation/TimeInterpolator;

.field private final c:Landroid/graphics/Paint;

.field private synthetic d:Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;)V
    .locals 3

    .prologue
    .line 135
    iput-object p1, p0, Lcbv;->d:Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 129
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcbv;->a:Landroid/graphics/RectF;

    .line 130
    sget-object v0, Lcak;->a:Lcak;

    iput-object v0, p0, Lcbv;->b:Landroid/animation/TimeInterpolator;

    .line 133
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcbv;->c:Landroid/graphics/Paint;

    .line 136
    iget-object v0, p0, Lcbv;->c:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 137
    iget-object v0, p0, Lcbv;->c:Landroid/graphics/Paint;

    invoke-virtual {p1}, Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0070

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 138
    iget-object v0, p0, Lcbv;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 139
    iget-object v0, p0, Lcbv;->c:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    .line 141
    iget-object v0, p0, Lcbv;->c:Landroid/graphics/Paint;

    .line 142
    invoke-virtual {p1}, Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v2, 0x40400000    # 3.0f

    mul-float/2addr v1, v2

    .line 141
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 143
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const v6, 0x43b3fffd    # 359.9999f

    const/high16 v5, -0x40800000    # -1.0f

    const/high16 v1, 0x43b40000    # 360.0f

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 148
    .line 149
    invoke-virtual {p0}, Lcbv;->getBounds()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcbv;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    const v7, 0x3f59999a    # 0.85f

    mul-float/2addr v3, v7

    iget-object v7, p0, Lcbv;->d:Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;

    invoke-static {v7}, Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;->a(Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;)F

    move-result v7

    mul-float/2addr v3, v7

    sub-float/2addr v0, v3

    .line 151
    iget-object v3, p0, Lcbv;->a:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcbv;->getBounds()Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 152
    iget-object v3, p0, Lcbv;->a:Landroid/graphics/RectF;

    invoke-virtual {v3, v0, v0}, Landroid/graphics/RectF;->inset(FF)V

    .line 156
    invoke-virtual {p0}, Lcbv;->getLevel()I

    move-result v0

    add-int/lit16 v0, v0, 0x2710

    iget-object v3, p0, Lcbv;->d:Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;

    invoke-static {v3}, Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;->b(Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;)I

    move-result v3

    sub-int/2addr v0, v3

    rem-int/lit16 v7, v0, 0x2710

    .line 159
    iget-object v0, p0, Lcbv;->d:Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;->a(Lcom/google/android/apps/moviemaker/ui/SoundtrackProgressSpinner;)F

    move-result v0

    cmpg-float v0, v0, v4

    if-gez v0, :cond_1

    move v4, v1

    .line 194
    :cond_0
    :goto_0
    iget-object v1, p0, Lcbv;->a:Landroid/graphics/RectF;

    mul-int/lit16 v0, v7, 0x168

    int-to-float v0, v0

    const v2, 0x38d1b717    # 1.0E-4f

    mul-float/2addr v0, v2

    const/high16 v2, -0x3d4c0000    # -90.0f

    add-float/2addr v2, v0

    const v0, -0x3c4c0003    # -359.9999f

    .line 198
    invoke-static {v4, v0, v6}, Lcfn;->a(FFF)F

    move-result v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcbv;->c:Landroid/graphics/Paint;

    move-object v0, p1

    .line 194
    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawArc(Landroid/graphics/RectF;FFZLandroid/graphics/Paint;)V

    .line 201
    return-void

    .line 164
    :cond_1
    const/16 v0, 0x9c4

    if-gt v7, v0, :cond_2

    move v0, v1

    move v3, v4

    .line 178
    :goto_1
    iget-object v8, p0, Lcbv;->b:Landroid/animation/TimeInterpolator;

    int-to-float v9, v7

    sub-float v3, v9, v3

    const v9, 0x451c4000    # 2500.0f

    div-float/2addr v3, v9

    invoke-interface {v8, v3}, Landroid/animation/TimeInterpolator;->getInterpolation(F)F

    move-result v3

    mul-float/2addr v1, v3

    sub-float v0, v1, v0

    .line 182
    cmpl-float v1, v0, v2

    if-ltz v1, :cond_5

    cmpg-float v1, v0, v4

    if-gez v1, :cond_5

    .line 189
    :goto_2
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x43b38000    # 359.0f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    move v4, v6

    .line 190
    goto :goto_0

    .line 167
    :cond_2
    const/16 v0, 0x1388

    if-gt v7, v0, :cond_3

    .line 169
    const v0, 0x451c4000    # 2500.0f

    move v3, v0

    move v0, v2

    goto :goto_1

    .line 170
    :cond_3
    const/16 v0, 0x1d4c

    if-gt v7, v0, :cond_4

    .line 172
    const v0, 0x459c4000    # 5000.0f

    move v3, v0

    move v0, v1

    goto :goto_1

    .line 175
    :cond_4
    const v0, 0x45ea6000    # 7500.0f

    move v3, v0

    move v0, v2

    goto :goto_1

    .line 184
    :cond_5
    cmpg-float v1, v0, v2

    if-gez v1, :cond_6

    cmpl-float v1, v0, v5

    if-lez v1, :cond_6

    move v4, v5

    .line 185
    goto :goto_2

    :cond_6
    move v4, v0

    goto :goto_2
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 205
    const/4 v0, -0x3

    return v0
.end method

.method public setAlpha(I)V
    .locals 0

    .prologue
    .line 211
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 216
    return-void
.end method
