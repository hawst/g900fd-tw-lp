.class public final Lhnu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhnt;


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Z

.field private c:Z

.field private d:Z

.field private e:I

.field private f:I

.field private g:Lkoe;

.field private h:Lkoe;

.field private i:Lkoe;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-boolean v0, p0, Lhnu;->b:Z

    .line 18
    iput-boolean v0, p0, Lhnu;->c:Z

    .line 19
    iput-boolean v0, p0, Lhnu;->d:Z

    .line 20
    iput v0, p0, Lhnu;->e:I

    .line 21
    iput v0, p0, Lhnu;->f:I

    .line 27
    iput-object p1, p0, Lhnu;->a:Landroid/content/Context;

    .line 28
    return-void
.end method


# virtual methods
.method public declared-synchronized a(J)V
    .locals 3

    .prologue
    .line 33
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lhnu;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhnu;->e:I

    .line 34
    iget-boolean v0, p0, Lhnu;->c:Z

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lkoe;

    const/4 v1, 0x1

    invoke-direct {v0, v1, p1, p2}, Lkoe;-><init>(IJ)V

    iput-object v0, p0, Lhnu;->g:Lkoe;

    .line 36
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhnu;->c:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    :cond_0
    monitor-exit p0

    return-void

    .line 33
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;J)V
    .locals 6

    .prologue
    .line 60
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lhnu;->b:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lhnu;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lhnu;->f:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhnu;->d:Z

    if-nez v0, :cond_0

    .line 61
    iget-object v0, p0, Lhnu;->g:Lkoe;

    invoke-virtual {v0, p1}, Lkoe;->a(Ljava/lang/String;)Lkoe;

    move-result-object v0

    iget-object v1, p0, Lhnu;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 62
    iget-object v0, p0, Lhnu;->h:Lkoe;

    invoke-virtual {v0, p1}, Lkoe;->a(Ljava/lang/String;)Lkoe;

    move-result-object v0

    iget-object v1, p0, Lhnu;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 63
    iget-object v0, p0, Lhnu;->i:Lkoe;

    invoke-virtual {v0, p1}, Lkoe;->a(Ljava/lang/String;)Lkoe;

    move-result-object v0

    iget-object v1, p0, Lhnu;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 64
    new-instance v0, Lkoe;

    const/16 v1, 0x10

    invoke-direct {v0, v1, p2, p3}, Lkoe;-><init>(IJ)V

    invoke-virtual {v0, p1}, Lkoe;->a(Ljava/lang/String;)Lkoe;

    move-result-object v0

    iget-object v1, p0, Lhnu;->a:Landroid/content/Context;

    .line 65
    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 67
    iget-object v0, p0, Lhnu;->i:Lkoe;

    iget-wide v0, v0, Lkoe;->a:J

    sub-long v0, p2, v0

    iget-object v2, p0, Lhnu;->h:Lkoe;

    iget-wide v2, v2, Lkoe;->a:J

    iget-object v4, p0, Lhnu;->g:Lkoe;

    iget-wide v4, v4, Lkoe;->a:J

    sub-long/2addr v2, v4

    add-long/2addr v0, v2

    .line 69
    new-instance v2, Lkoe;

    const/16 v3, 0x3d

    sub-long v0, p2, v0

    invoke-direct {v2, v3, v0, v1}, Lkoe;-><init>(IJ)V

    .line 70
    invoke-virtual {v2, p1}, Lkoe;->a(Ljava/lang/String;)Lkoe;

    move-result-object v0

    iget-object v1, p0, Lhnu;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 71
    new-instance v0, Lkoe;

    const/16 v1, 0x3e

    invoke-direct {v0, v1, p2, p3}, Lkoe;-><init>(IJ)V

    .line 72
    invoke-virtual {v0, p1}, Lkoe;->a(Ljava/lang/String;)Lkoe;

    move-result-object v0

    iget-object v1, p0, Lhnu;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 74
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhnu;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    :cond_0
    monitor-exit p0

    return-void

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized b(J)V
    .locals 3

    .prologue
    .line 42
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lhnu;->c:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lhnu;->e:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lhnu;->e:I

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lkoe;

    const/4 v1, 0x2

    invoke-direct {v0, v1, p1, p2}, Lkoe;-><init>(IJ)V

    iput-object v0, p0, Lhnu;->h:Lkoe;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    :cond_0
    monitor-exit p0

    return-void

    .line 42
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized c(J)V
    .locals 3

    .prologue
    .line 50
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lhnu;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lhnu;->f:I

    .line 51
    iget-boolean v0, p0, Lhnu;->b:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhnu;->c:Z

    if-eqz v0, :cond_0

    .line 52
    new-instance v0, Lkoe;

    const/16 v1, 0xf

    invoke-direct {v0, v1, p1, p2}, Lkoe;-><init>(IJ)V

    iput-object v0, p0, Lhnu;->i:Lkoe;

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lhnu;->b:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 56
    :cond_0
    monitor-exit p0

    return-void

    .line 50
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
