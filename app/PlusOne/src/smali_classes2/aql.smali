.class final Laql;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Landroid/net/Uri;",
        "Landroid/graphics/Bitmap;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Laqc;


# direct methods
.method constructor <init>(Laqc;)V
    .locals 0

    .prologue
    .line 1624
    iput-object p1, p0, Laql;->a:Laqc;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Landroid/net/Uri;)Ljava/lang/Void;
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v8, 0x0

    .line 1633
    .line 1635
    array-length v9, p1

    move v7, v0

    move-object v6, v8

    :goto_0
    if-ge v7, v9, :cond_0

    aget-object v1, p1, v7

    .line 1636
    invoke-virtual {p0}, Laql;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1653
    :cond_0
    return-object v8

    .line 1640
    :cond_1
    :try_start_0
    iget-object v0, p0, Laql;->a:Laqc;

    invoke-static {v0}, Laqc;->E(Laqc;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v1

    .line 1642
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1643
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1644
    iget-object v2, p0, Laql;->a:Laqc;

    .line 1645
    invoke-static {v2}, Laqc;->E(Laqc;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    int-to-long v4, v0

    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 1644
    invoke-static {v2, v4, v5, v0, v3}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1647
    const/4 v2, 0x1

    new-array v2, v2, [Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p0, v2}, Laql;->publishProgress([Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1650
    :cond_2
    invoke-static {v1}, Lcek;->a(Ljava/io/Closeable;)V

    .line 1635
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    move-object v6, v1

    goto :goto_0

    .line 1650
    :catchall_0
    move-exception v0

    :goto_1
    invoke-static {v1}, Lcek;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    move-object v1, v6

    goto :goto_1
.end method

.method protected a()V
    .locals 2

    .prologue
    .line 1668
    iget-object v0, p0, Laql;->a:Laqc;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Laqc;->a(Laqc;Laql;)Laql;

    .line 1669
    return-void
.end method

.method protected varargs a([Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 1658
    const/4 v0, 0x0

    aget-object v1, p1, v0

    .line 1659
    iget-object v0, p0, Laql;->a:Laqc;

    invoke-static {v0}, Laqc;->j(Laqc;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 1660
    iget-object v0, p0, Laql;->a:Laqc;

    invoke-static {v0}, Laqc;->j(Laqc;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->size()I

    move-result v0

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 1662
    iget-object v0, p0, Laql;->a:Laqc;

    invoke-static {v0}, Laqc;->f(Laqc;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    invoke-interface {v0, v1}, Laqv;->a(Landroid/graphics/Bitmap;)V

    .line 1664
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1624
    check-cast p1, [Landroid/net/Uri;

    invoke-virtual {p0, p1}, Laql;->a([Landroid/net/Uri;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1624
    invoke-virtual {p0}, Laql;->a()V

    return-void
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 1628
    iget-object v0, p0, Laql;->a:Laqc;

    invoke-static {v0}, Laqc;->j(Laqc;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 1629
    return-void
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1624
    check-cast p1, [Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Laql;->a([Landroid/graphics/Bitmap;)V

    return-void
.end method
