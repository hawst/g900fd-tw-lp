.class public final Lbif;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbig;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljfb;

.field private final c:Lllx;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljfb;Lllx;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const-string v0, "context"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbif;->a:Landroid/content/Context;

    .line 32
    const-string v0, "movieMakerProvider"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfb;

    iput-object v0, p0, Lbif;->b:Ljfb;

    .line 33
    const-string v0, "chunkStore"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lllx;

    iput-object v0, p0, Lbif;->c:Lllx;

    .line 34
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Lbih;
    .locals 3

    .prologue
    .line 93
    const-string v0, "path"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 94
    new-instance v1, Landroid/media/MediaExtractor;

    invoke-direct {v1}, Landroid/media/MediaExtractor;-><init>()V

    .line 97
    :try_start_0
    invoke-virtual {v1, p1}, Landroid/media/MediaExtractor;->setDataSource(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 106
    new-instance v0, Lbii;

    invoke-direct {v0, v1}, Lbii;-><init>(Landroid/media/MediaExtractor;)V

    return-object v0

    .line 103
    :catch_0
    move-exception v0

    .line 100
    :try_start_1
    new-instance v2, Lbpr;

    invoke-direct {v2, p1, v0}, Lbpr;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 102
    :catchall_0
    move-exception v0

    .line 103
    invoke-virtual {v1}, Landroid/media/MediaExtractor;->release()V

    throw v0
.end method

.method public a(Landroid/net/Uri;)Lbij;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lbij",
            "<",
            "Lboo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    new-instance v0, Lbiu;

    iget-object v1, p0, Lbif;->a:Landroid/content/Context;

    iget-object v2, p0, Lbif;->b:Ljfb;

    invoke-direct {v0, v1, p1, v2}, Lbiu;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljfb;)V

    return-object v0
.end method

.method public a(Ljeg;)Lbij;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljeg;",
            ")",
            "Lbij",
            "<",
            "Lboo;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    const-string v0, "mediaIdentifier"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 43
    :try_start_0
    new-instance v0, Lbin;

    iget-object v1, p0, Lbif;->b:Ljfb;

    iget-object v2, p1, Ljeg;->a:Ljdx;

    iget-object v2, v2, Ljdx;->a:Ljdz;

    iget-object v3, p0, Lbif;->c:Lllx;

    invoke-virtual {v1, v2, v3}, Ljfb;->a(Ljdz;Lllx;)Lllt;

    move-result-object v1

    invoke-direct {v0, v1}, Lbin;-><init>(Lllt;)V
    :try_end_0
    .catch Ljfg; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 45
    :catch_0
    move-exception v0

    .line 46
    new-instance v1, Ljava/io/IOException;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x1f

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Error getting video stream for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public b(Ljeg;)Lbih;
    .locals 4

    .prologue
    .line 112
    iget-object v0, p1, Ljeg;->a:Ljdx;

    if-eqz v0, :cond_0

    iget-object v0, p1, Ljeg;->a:Ljdx;

    iget-object v0, v0, Ljdx;->a:Ljdz;

    if-eqz v0, :cond_0

    .line 115
    :try_start_0
    new-instance v0, Lbin;

    iget-object v1, p0, Lbif;->b:Ljfb;

    iget-object v2, p1, Ljeg;->a:Ljdx;

    iget-object v2, v2, Ljdx;->a:Ljdz;

    iget-object v3, p0, Lbif;->c:Lllx;

    invoke-virtual {v1, v2, v3}, Ljfb;->a(Ljdz;Lllx;)Lllt;

    move-result-object v1

    invoke-direct {v0, v1}, Lbin;-><init>(Lllt;)V
    :try_end_0
    .catch Ljfg; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :goto_0
    return-object v0

    .line 117
    :catch_0
    move-exception v0

    .line 118
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Couldn\'t get cloud video stream."

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 121
    :cond_0
    iget-object v0, p1, Ljeg;->b:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lbif;->c(Landroid/net/Uri;)Lbih;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Landroid/net/Uri;)Lbij;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            ")",
            "Lbij",
            "<",
            "Lbmx;",
            ">;"
        }
    .end annotation

    .prologue
    .line 58
    new-instance v0, Lbis;

    iget-object v1, p0, Lbif;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lbif;->b:Ljfb;

    invoke-direct {v0, v1, p1, v2}, Lbis;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;Ljfb;)V

    return-object v0
.end method

.method public c(Landroid/net/Uri;)Lbih;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 74
    const-string v0, "uri"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 75
    new-instance v1, Landroid/media/MediaExtractor;

    invoke-direct {v1}, Landroid/media/MediaExtractor;-><init>()V

    .line 78
    :try_start_0
    iget-object v0, p0, Lbif;->a:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, p1, v2}, Landroid/media/MediaExtractor;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    new-instance v0, Lbii;

    invoke-direct {v0, v1}, Lbii;-><init>(Landroid/media/MediaExtractor;)V

    return-object v0

    .line 84
    :catch_0
    move-exception v0

    .line 81
    :try_start_1
    new-instance v2, Lbpr;

    invoke-direct {v2, p1, v0}, Lbpr;-><init>(Landroid/net/Uri;Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 83
    :catchall_0
    move-exception v0

    .line 84
    invoke-virtual {v1}, Landroid/media/MediaExtractor;->release()V

    throw v0
.end method
