.class public final Layg;
.super Layr;
.source "PG"


# instance fields
.field private final a:J


# direct methods
.method public constructor <init>(IIJ)V
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 12
    new-instance v0, Lays;

    invoke-direct {v0}, Lays;-><init>()V

    .line 13
    invoke-virtual {v0, p1}, Lays;->b(I)Lays;

    move-result-object v0

    .line 14
    invoke-virtual {v0, v1}, Lays;->c(I)Lays;

    move-result-object v0

    .line 15
    invoke-virtual {v0, p2}, Lays;->d(I)Lays;

    move-result-object v0

    .line 16
    invoke-virtual {v0, v1}, Lays;->e(I)Lays;

    move-result-object v0

    .line 12
    invoke-direct {p0, v0}, Layr;-><init>(Lays;)V

    .line 18
    iput-wide p3, p0, Layg;->a:J

    .line 19
    return-void
.end method


# virtual methods
.method public h(Layl;)F
    .locals 4

    .prologue
    .line 23
    iget-wide v0, p1, Layl;->a:J

    long-to-double v0, v0

    iget-wide v2, p0, Layg;->a:J

    long-to-double v2, v2

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->sin(D)D

    move-result-wide v0

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    add-double/2addr v0, v2

    const-wide/high16 v2, 0x4000000000000000L    # 2.0

    div-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method

.method public l(Layl;)F
    .locals 2

    .prologue
    .line 29
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, p1}, Layg;->h(Layl;)F

    move-result v1

    sub-float/2addr v0, v1

    return v0
.end method
