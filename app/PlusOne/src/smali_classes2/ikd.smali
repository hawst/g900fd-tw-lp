.class public final Likd;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lijm;",
            "Lggb;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lijn;",
            "Lggc;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Lgf;

    invoke-direct {v0}, Lgf;-><init>()V

    iput-object v0, p0, Likd;->a:Ljava/util/Map;

    .line 33
    new-instance v0, Lgf;

    invoke-direct {v0}, Lgf;-><init>()V

    iput-object v0, p0, Likd;->b:Ljava/util/Map;

    .line 36
    return-void
.end method


# virtual methods
.method public a(Lijj;)Lgfo;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<O::",
            "Lijj;",
            ">()",
            "Lcom/google/android/gms/common/api/Api;"
        }
    .end annotation

    .prologue
    .line 39
    instance-of v0, p1, Lijg;

    if-eqz v0, :cond_0

    .line 40
    check-cast p1, Lijg;

    invoke-interface {p1}, Lijg;->a()Lgfo;

    move-result-object v0

    .line 42
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lijk;)Lgfz;
    .locals 1

    .prologue
    .line 130
    instance-of v0, p1, Lijo;

    if-eqz v0, :cond_0

    .line 131
    check-cast p1, Lijo;

    invoke-virtual {p1}, Lijo;->e()Lgfz;

    move-result-object v0

    .line 133
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lijm;)Lggb;
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Likd;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Likd;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggb;

    .line 108
    :goto_0
    return-object v0

    .line 95
    :cond_0
    new-instance v0, Like;

    invoke-direct {v0, p1}, Like;-><init>(Lijm;)V

    .line 107
    iget-object v1, p0, Likd;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Lijn;)Lggc;
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Likd;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Likd;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lggc;

    .line 125
    :goto_0
    return-object v0

    .line 117
    :cond_0
    new-instance v0, Likf;

    invoke-direct {v0, p0, p1}, Likf;-><init>(Likd;Lijn;)V

    .line 124
    iget-object v1, p0, Likd;->b:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Lgfd;)Liiw;
    .locals 1

    .prologue
    .line 146
    new-instance v0, Liiu;

    invoke-direct {v0, p1}, Liiu;-><init>(Lgfd;)V

    return-object v0
.end method

.method public a(Lggd;)Lijq;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lggd",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;)",
            "Lijq",
            "<",
            "Lika;",
            ">;"
        }
    .end annotation

    .prologue
    .line 150
    new-instance v0, Lijr;

    sget-object v1, Likb;->a:Likg;

    invoke-direct {v0, p1, v1}, Lijr;-><init>(Lggd;Likg;)V

    return-object v0
.end method
