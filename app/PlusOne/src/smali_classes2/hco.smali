.class public final Lhco;
.super Lhck;
.source "PG"


# static fields
.field private static final a:Landroid/graphics/PointF;


# instance fields
.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:Lhcn;

.field private final f:Landroid/graphics/drawable/Drawable;

.field private final g:Landroid/graphics/Paint;

.field private final h:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 24
    new-instance v0, Landroid/graphics/PointF;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/graphics/PointF;-><init>(FF)V

    sput-object v0, Lhco;->a:Landroid/graphics/PointF;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 37
    invoke-direct {p0}, Lhck;-><init>()V

    .line 73
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lhco;->h:Landroid/graphics/Rect;

    .line 38
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 40
    new-instance v1, Lhcn;

    invoke-direct {v1, v0}, Lhcn;-><init>(Landroid/content/res/Resources;)V

    iput-object v1, p0, Lhco;->e:Lhcn;

    .line 42
    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 43
    const v2, 0x7f0d0006

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    .line 45
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    iput-object v3, p0, Lhco;->g:Landroid/graphics/Paint;

    .line 46
    iget-object v3, p0, Lhco;->g:Landroid/graphics/Paint;

    const v4, 0x7f0b001c

    .line 47
    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    .line 46
    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 48
    iget-object v3, p0, Lhco;->g:Landroid/graphics/Paint;

    invoke-virtual {v3, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 49
    iget-object v3, p0, Lhco;->g:Landroid/graphics/Paint;

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 50
    iget-object v2, p0, Lhco;->g:Landroid/graphics/Paint;

    const/high16 v3, 0x40000000    # 2.0f

    .line 51
    invoke-static {v6, v3, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v3

    sget-object v4, Lhco;->a:Landroid/graphics/PointF;

    iget v4, v4, Landroid/graphics/PointF;->x:F

    .line 53
    invoke-static {v6, v4, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v4

    sget-object v5, Lhco;->a:Landroid/graphics/PointF;

    iget v5, v5, Landroid/graphics/PointF;->y:F

    .line 55
    invoke-static {v6, v5, v1}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v1

    const/high16 v5, -0x1000000

    .line 50
    invoke-virtual {v2, v3, v4, v1, v5}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    .line 59
    iget-object v1, p0, Lhco;->g:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 62
    const v1, 0x7f020486

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lhco;->f:Landroid/graphics/drawable/Drawable;

    .line 65
    const v1, 0x7f0d001b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lhco;->b:I

    .line 66
    const v1, 0x7f0d001a

    .line 67
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lhco;->c:I

    .line 69
    const v1, 0x7f0d001c

    .line 70
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lhco;->d:I

    .line 71
    return-void
.end method


# virtual methods
.method protected a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Lhcl;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 77
    iget-object v0, p0, Lhco;->g:Landroid/graphics/Paint;

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    iget-object v3, p0, Lhco;->h:Landroid/graphics/Rect;

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 79
    new-instance v0, Lhcl;

    iget-object v1, p0, Lhco;->h:Landroid/graphics/Rect;

    invoke-direct {v0, p1, v1}, Lhcl;-><init>(Ljava/lang/CharSequence;Landroid/graphics/Rect;)V

    .line 80
    iget-object v1, v0, Lhcl;->a:Landroid/graphics/Rect;

    iget-object v2, p0, Lhco;->e:Lhcn;

    .line 81
    invoke-virtual {v2}, Lhcn;->a()I

    move-result v2

    iget-object v3, p0, Lhco;->e:Lhcn;

    invoke-virtual {v3}, Lhcn;->b()I

    move-result v3

    .line 80
    invoke-virtual {v1, v4, v4, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 83
    return-object v0
.end method

.method protected a(Ljava/util/List;)Lhcm;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lhcl;",
            ">;)",
            "Lhcm;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 88
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 89
    :cond_0
    sget-object v0, Lhcm;->a:Lhcm;

    .line 119
    :goto_0
    return-object v0

    .line 93
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhcl;

    .line 94
    iget-object v0, v0, Lhcl;->c:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    .line 95
    goto :goto_1

    .line 97
    :cond_2
    iget-object v0, p0, Lhco;->e:Lhcn;

    invoke-virtual {v0}, Lhcn;->a()I

    move-result v0

    .line 98
    iget-object v3, p0, Lhco;->e:Lhcn;

    invoke-virtual {v3}, Lhcn;->b()I

    move-result v4

    .line 100
    new-instance v3, Lhcp;

    invoke-direct {v3, p1}, Lhcp;-><init>(Ljava/util/List;)V

    .line 101
    iget-object v5, v3, Lhcp;->f:Landroid/graphics/Rect;

    iget v6, p0, Lhco;->c:I

    shl-int/lit8 v6, v6, 0x1

    add-int/2addr v6, v0

    iget v7, p0, Lhco;->b:I

    add-int/2addr v7, v4

    .line 105
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v8

    mul-int/2addr v7, v8

    iget v8, p0, Lhco;->b:I

    sub-int/2addr v7, v8

    iget v8, p0, Lhco;->c:I

    shl-int/lit8 v8, v8, 0x1

    add-int/2addr v7, v8

    .line 101
    invoke-virtual {v5, v2, v2, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 107
    iget-object v5, v3, Lhcp;->c:Landroid/graphics/Rect;

    iget-object v6, v3, Lhcp;->f:Landroid/graphics/Rect;

    invoke-virtual {v5, v6}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 108
    iget-object v5, v3, Lhcp;->f:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->centerX()I

    move-result v5

    int-to-float v5, v5

    iput v5, v3, Lhcp;->d:F

    .line 109
    iget-object v5, v3, Lhcp;->c:Landroid/graphics/Rect;

    iget-object v6, v3, Lhcp;->c:Landroid/graphics/Rect;

    invoke-virtual {v6}, Landroid/graphics/Rect;->centerX()I

    move-result v6

    iget v7, p0, Lhco;->d:I

    add-int/2addr v6, v7

    add-int/2addr v1, v6

    iget v6, p0, Lhco;->c:I

    add-int/2addr v1, v6

    iput v1, v5, Landroid/graphics/Rect;->right:I

    .line 112
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1, v2, v2, v0, v4}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 113
    iget v0, p0, Lhco;->c:I

    iget v5, p0, Lhco;->c:I

    invoke-virtual {v1, v0, v5}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 114
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhcl;

    .line 115
    iget-object v0, v0, Lhcl;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 116
    iget v0, p0, Lhco;->b:I

    add-int/2addr v0, v4

    invoke-virtual {v1, v2, v0}, Landroid/graphics/Rect;->offset(II)V

    goto :goto_2

    :cond_3
    move-object v0, v3

    .line 119
    goto/16 :goto_0
.end method

.method public a(Landroid/graphics/Canvas;Lhcm;II)V
    .locals 11

    .prologue
    const/4 v7, 0x1

    const/4 v10, 0x0

    const/4 v5, 0x0

    .line 125
    invoke-virtual {p2}, Lhcm;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    :goto_0
    return-void

    .line 129
    :cond_0
    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->save(I)I

    move-object v0, p2

    .line 131
    check-cast v0, Lhcp;

    .line 135
    int-to-float v1, p3

    iget v2, v0, Lhcp;->d:F

    sub-float/2addr v1, v2

    iget-object v2, v0, Lhcp;->f:Landroid/graphics/Rect;

    .line 137
    invoke-virtual {v2}, Landroid/graphics/Rect;->centerY()I

    move-result v2

    sub-int v2, p4, v2

    int-to-float v2, v2

    iget v3, p2, Lhcm;->e:F

    sub-float/2addr v2, v3

    .line 135
    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->translate(FF)V

    .line 140
    iget-object v1, p0, Lhco;->f:Landroid/graphics/drawable/Drawable;

    iget-object v0, v0, Lhcp;->f:Landroid/graphics/Rect;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 141
    iget-object v0, p0, Lhco;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 143
    const/4 v9, 0x0

    move v6, v10

    .line 144
    :goto_1
    iget-object v0, p2, Lhcm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v6, v0, :cond_2

    .line 145
    iget-object v0, p2, Lhcm;->b:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lhcl;

    .line 146
    invoke-virtual {p2, v6}, Lhcm;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 144
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    move-object v9, v1

    goto :goto_1

    .line 151
    :cond_1
    iget-object v0, p0, Lhco;->e:Lhcn;

    iget-object v2, v1, Lhcl;->a:Landroid/graphics/Rect;

    .line 152
    invoke-virtual {v2}, Landroid/graphics/Rect;->centerX()I

    move-result v2

    int-to-float v2, v2

    iget-object v3, v1, Lhcl;->a:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->centerY()I

    move-result v3

    int-to-float v3, v3

    iget-object v4, v1, Lhcl;->b:Ljava/lang/CharSequence;

    move-object v1, p1

    .line 151
    invoke-virtual/range {v0 .. v5}, Lhcn;->a(Landroid/graphics/Canvas;FFLjava/lang/CharSequence;F)V

    move-object v1, v9

    goto :goto_2

    .line 156
    :cond_2
    if-eqz v9, :cond_3

    .line 159
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 160
    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->save(I)I

    .line 162
    int-to-float v0, p3

    int-to-float v1, p4

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 164
    iget-object v3, p0, Lhco;->e:Lhcn;

    iget-object v7, v9, Lhcl;->b:Ljava/lang/CharSequence;

    move-object v4, p1

    move v6, v5

    move v8, v5

    invoke-virtual/range {v3 .. v8}, Lhcn;->b(Landroid/graphics/Canvas;FFLjava/lang/CharSequence;F)V

    .line 165
    iget-object v1, v9, Lhcl;->b:Ljava/lang/CharSequence;

    iget-object v0, v9, Lhcl;->b:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    iget v0, p0, Lhco;->d:I

    int-to-float v4, v0

    iget v0, v9, Lhcl;->d:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float v5, v0, v2

    iget-object v6, p0, Lhco;->g:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v10

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/CharSequence;IIFFLandroid/graphics/Paint;)V

    .line 169
    :cond_3
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0
.end method
