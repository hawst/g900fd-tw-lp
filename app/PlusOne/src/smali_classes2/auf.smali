.class final Lauf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:I

.field public final b:Landroid/media/MediaFormat;

.field public final c:I

.field public final d:Landroid/media/MediaFormat;

.field public final e:J


# direct methods
.method public constructor <init>(ILandroid/media/MediaFormat;ILandroid/media/MediaFormat;J)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 771
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 772
    const/4 v2, -0x1

    if-ne p1, v2, :cond_0

    move v3, v0

    :goto_0
    if-nez p2, :cond_1

    move v2, v0

    :goto_1
    xor-int/2addr v2, v3

    if-nez v2, :cond_2

    :goto_2
    const-string v1, "audioTrackIndex and audioTrackFormat must be either both set or both unset"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 776
    iput p1, p0, Lauf;->a:I

    .line 777
    iput-object p2, p0, Lauf;->b:Landroid/media/MediaFormat;

    .line 778
    const-string v0, "videoTrackIndex"

    invoke-static {p3, v0, v4}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    move-result v0

    iput v0, p0, Lauf;->c:I

    .line 779
    const-string v0, "videoTrackFormat"

    invoke-static {p4, v0, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaFormat;

    iput-object v0, p0, Lauf;->d:Landroid/media/MediaFormat;

    .line 780
    const-string v0, "resumePresentationTimeUs"

    .line 781
    invoke-static {p5, p6, v0}, Lcec;->a(JLjava/lang/CharSequence;)J

    move-result-wide v0

    iput-wide v0, p0, Lauf;->e:J

    .line 782
    return-void

    :cond_0
    move v3, v1

    .line 772
    goto :goto_0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_2
.end method
