.class public final Lfsq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lfsq;->a:Ljava/lang/String;

    .line 26
    iput-object p2, p0, Lfsq;->b:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public a()Landroid/net/Uri;
    .locals 3

    .prologue
    .line 46
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 47
    const-string v1, "https"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 48
    const-string v1, "plus.google.com"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 49
    iget-object v1, p0, Lfsq;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 50
    const-string v1, "stories"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 51
    iget-object v1, p0, Lfsq;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 52
    iget-object v1, p0, Lfsq;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 53
    const-string v1, "authkey"

    iget-object v2, p0, Lfsq;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 57
    :cond_0
    :goto_0
    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 54
    :cond_1
    iget-object v1, p0, Lfsq;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 55
    const-string v1, "gpinv"

    iget-object v2, p0, Lfsq;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lfsq;
    .locals 0

    .prologue
    .line 33
    iput-object p1, p0, Lfsq;->c:Ljava/lang/String;

    .line 34
    return-object p0
.end method

.method public b(Ljava/lang/String;)Lfsq;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lfsq;->d:Ljava/lang/String;

    .line 42
    return-object p0
.end method
