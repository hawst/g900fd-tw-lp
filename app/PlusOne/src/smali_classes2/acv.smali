.class public final Lacv;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field a:Z

.field private b:Laak;

.field private c:Ljava/lang/String;

.field private d:Lacy;

.field private e:Labc;

.field private f:Labb;

.field private g:Lacp;


# direct methods
.method constructor <init>(Laak;Ljava/lang/String;Lacy;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object v1, p0, Lacv;->e:Labc;

    .line 39
    iput-object v1, p0, Lacv;->f:Labb;

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lacv;->a:Z

    .line 41
    iput-object v1, p0, Lacv;->g:Lacp;

    .line 146
    iput-object p1, p0, Lacv;->b:Laak;

    .line 147
    iput-object p2, p0, Lacv;->c:Ljava/lang/String;

    .line 148
    iput-object p3, p0, Lacv;->d:Lacy;

    .line 149
    return-void
.end method


# virtual methods
.method public a([I)Laap;
    .locals 2

    .prologue
    .line 71
    invoke-virtual {p0}, Lacv;->e()Labb;

    move-result-object v0

    invoke-virtual {v0, p1}, Labb;->a([I)Laap;

    move-result-object v0

    .line 72
    if-eqz v0, :cond_0

    .line 74
    iget-object v1, p0, Lacv;->b:Laak;

    invoke-virtual {v1, v0}, Laak;->a(Laap;)V

    .line 76
    :cond_0
    return-object v0
.end method

.method public a(Laap;)V
    .locals 4

    .prologue
    .line 94
    iget-object v0, p1, Laap;->a:Lzp;

    invoke-virtual {v0}, Lzp;->g()J

    move-result-wide v0

    .line 95
    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 96
    iget-object v0, p0, Lacv;->b:Laak;

    iget-wide v0, v0, Laak;->mCurrentTimestamp:J

    invoke-virtual {p1, v0, v1}, Laap;->a(J)V

    .line 97
    :cond_0
    invoke-virtual {p0}, Lacv;->e()Labb;

    move-result-object v0

    invoke-virtual {v0, p1}, Labb;->a(Laap;)V

    .line 98
    return-void
.end method

.method a(Labb;)V
    .locals 1

    .prologue
    .line 169
    iput-object p1, p0, Lacv;->f:Labb;

    .line 170
    const/4 v0, 0x0

    iput-object v0, p0, Lacv;->e:Labc;

    .line 171
    return-void
.end method

.method a(Labc;)V
    .locals 2

    .prologue
    .line 174
    iput-object p1, p0, Lacv;->e:Labc;

    .line 175
    iget-object v0, p0, Lacv;->e:Labc;

    iget-object v1, p0, Lacv;->d:Lacy;

    iget-object v1, v1, Lacy;->a:Labf;

    invoke-virtual {v0, v1}, Labc;->a(Labf;)V

    .line 176
    iget-object v0, p0, Lacv;->b:Laak;

    .line 177
    return-void
.end method

.method a(Lacp;)V
    .locals 0

    .prologue
    .line 152
    iput-object p1, p0, Lacv;->g:Lacp;

    .line 153
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 109
    iput-boolean p1, p0, Lacv;->a:Z

    .line 110
    return-void
.end method

.method public a()Z
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lacv;->f:Labb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lacv;->f:Labb;

    invoke-virtual {v0}, Labb;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lacv;->c:Ljava/lang/String;

    return-object v0
.end method

.method public c()Laak;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lacv;->b:Laak;

    return-object v0
.end method

.method public d()Lacp;
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lacv;->g:Lacp;

    return-object v0
.end method

.method e()Labb;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lacv;->f:Labb;

    return-object v0
.end method

.method f()V
    .locals 1

    .prologue
    .line 188
    iget-object v0, p0, Lacv;->f:Labb;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lacv;->f:Labb;

    invoke-virtual {v0}, Labb;->e()V

    .line 191
    :cond_0
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 142
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lacv;->b:Laak;

    invoke-virtual {v1}, Laak;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lacv;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
