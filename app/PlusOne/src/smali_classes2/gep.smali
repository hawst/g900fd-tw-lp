.class final Lgep;
.super Ljava/lang/Object;

# interfaces
.implements Lgkt;


# instance fields
.field final synthetic a:Lgeh;

.field private b:Lgfz;

.field private c:J


# direct methods
.method public constructor <init>(Lgeh;)V
    .locals 2

    iput-object p1, p0, Lgep;->a:Lgeh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lgep;->c:J

    return-void
.end method


# virtual methods
.method public a()J
    .locals 4

    iget-wide v0, p0, Lgep;->c:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lgep;->c:J

    return-wide v0
.end method

.method public a(Lgfz;)V
    .locals 0

    iput-object p1, p0, Lgep;->b:Lgfz;

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3

    iget-object v0, p0, Lgep;->b:Lgfz;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "No GoogleApiClient available"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    sget-object v0, Lgdk;->c:Lgdn;

    iget-object v1, p0, Lgep;->b:Lgfz;

    invoke-interface {v0, v1, p1, p2}, Lgdn;->a(Lgfz;Ljava/lang/String;Ljava/lang/String;)Lggd;

    move-result-object v0

    new-instance v1, Lgeq;

    invoke-direct {v1, p0, p3, p4}, Lgeq;-><init>(Lgep;J)V

    invoke-interface {v0, v1}, Lggd;->a(Lggg;)V

    return-void
.end method
