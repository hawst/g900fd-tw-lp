.class public Lbja;
.super Lbiz;
.source "PG"


# instance fields
.field private final a:Lbig;

.field private final b:Ljeg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lbja;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbig;Ljeg;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Lbiz;-><init>()V

    .line 34
    const-string v0, "mediaExtractorFactory"

    .line 35
    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbig;

    iput-object v0, p0, Lbja;->a:Lbig;

    .line 36
    const-string v0, "mediaIdentifier"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljeg;

    iput-object v0, p0, Lbja;->b:Ljeg;

    .line 37
    return-void
.end method

.method private a(Ljava/lang/String;Ljeg;)V
    .locals 5

    .prologue
    const/4 v2, -0x1

    .line 64
    const/4 v1, 0x0

    .line 65
    :try_start_0
    iget-object v0, p0, Lbja;->a:Lbig;

    invoke-interface {v0, p2}, Lbig;->b(Ljeg;)Lbih;

    move-result-object v1

    .line 69
    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1}, Lbih;->f()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 70
    invoke-interface {v1, v0}, Lbih;->a(I)Landroid/media/MediaFormat;

    move-result-object v3

    .line 76
    const-string v4, "mime"

    invoke-virtual {v3, v4}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 82
    :goto_1
    if-ne v0, v2, :cond_1

    .line 83
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x22

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "No track with MIME type starting "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    :catch_0
    move-exception v0

    .line 91
    :try_start_1
    const-string v2, "Error reading media."

    new-instance v3, Ljava/lang/IllegalArgumentException;

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v3, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 93
    :catchall_0
    move-exception v0

    .line 94
    invoke-static {v1}, Lcgl;->a(Lcgk;)V

    throw v0

    .line 69
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 87
    :cond_1
    :try_start_2
    invoke-interface {v1, v0}, Lbih;->b(I)V

    .line 88
    invoke-virtual {p0, v1}, Lbja;->a(Lbih;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 89
    return-void

    :cond_2
    move v0, v2

    goto :goto_1
.end method


# virtual methods
.method public b()V
    .locals 2

    .prologue
    .line 43
    const-string v0, "audio/"

    iget-object v1, p0, Lbja;->b:Ljeg;

    invoke-direct {p0, v0, v1}, Lbja;->a(Ljava/lang/String;Ljeg;)V

    .line 44
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 50
    const-string v0, "video/"

    iget-object v1, p0, Lbja;->b:Ljeg;

    invoke-direct {p0, v0, v1}, Lbja;->a(Ljava/lang/String;Ljeg;)V

    .line 52
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lbja;->b:Ljeg;

    invoke-virtual {v0}, Ljeg;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
