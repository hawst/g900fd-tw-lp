.class public final Lfua;
.super Landroid/view/ViewGroup;
.source "PG"

# interfaces
.implements Lljh;


# instance fields
.field private a:Lmnk;

.field private b:Lfqu;

.field private c:Lfrj;

.field private d:Landroid/graphics/Rect;

.field private e:Landroid/graphics/Rect;

.field private f:Landroid/graphics/Rect;

.field private g:Landroid/graphics/Paint;

.field private h:Landroid/graphics/Paint;

.field private i:Landroid/graphics/drawable/Drawable;

.field private j:Z

.field private k:Z

.field private l:I

.field private m:Lfrd;

.field private n:Landroid/graphics/RectF;

.field private o:Landroid/graphics/Paint;

.field private p:Landroid/graphics/Path;

.field private q:Landroid/util/SparseIntArray;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lfua;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lfua;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 88
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfua;->d:Landroid/graphics/Rect;

    .line 64
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfua;->e:Landroid/graphics/Rect;

    .line 65
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lfua;->f:Landroid/graphics/Rect;

    .line 66
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lfua;->g:Landroid/graphics/Paint;

    .line 67
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lfua;->h:Landroid/graphics/Paint;

    .line 71
    iput v1, p0, Lfua;->l:I

    .line 73
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lfua;->n:Landroid/graphics/RectF;

    .line 74
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v2}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lfua;->o:Landroid/graphics/Paint;

    .line 77
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lfua;->q:Landroid/util/SparseIntArray;

    .line 89
    invoke-virtual {p0, v1}, Lfua;->setWillNotDraw(Z)V

    .line 90
    invoke-virtual {p0, v1}, Lfua;->setClipToPadding(Z)V

    .line 91
    invoke-virtual {p0, v1}, Lfua;->setClipChildren(Z)V

    .line 92
    return-void
.end method

.method public static a(Landroid/view/View;II)V
    .locals 14

    .prologue
    .line 468
    .line 469
    invoke-static {p0}, Lfrc;->a(Landroid/view/View;)Lfrc;

    move-result-object v3

    .line 470
    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v4

    .line 471
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v5

    .line 472
    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    .line 473
    invoke-virtual {p0}, Landroid/view/View;->getPaddingBottom()I

    move-result v6

    .line 474
    invoke-virtual {v3}, Lfrc;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v3}, Lfrc;->f()I

    move-result v0

    .line 475
    :goto_0
    invoke-virtual {v3}, Lfrc;->g()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v3}, Lfrc;->h()I

    move-result v1

    .line 476
    :goto_1
    invoke-virtual {v3}, Lfrc;->q()Z

    move-result v7

    if-eqz v7, :cond_2

    .line 480
    check-cast p0, Landroid/view/ViewGroup;

    .line 481
    invoke-virtual {v3}, Lfrc;->r()I

    move-result v2

    invoke-virtual {p0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 482
    invoke-virtual {v6}, Landroid/view/View;->getPaddingTop()I

    move-result v7

    .line 483
    invoke-virtual {v6}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    .line 484
    invoke-virtual {v6}, Landroid/view/View;->getPaddingRight()I

    move-result v8

    .line 485
    invoke-virtual {v6}, Landroid/view/View;->getPaddingBottom()I

    move-result v9

    .line 487
    invoke-static {v6}, Lfrc;->a(Landroid/view/View;)Lfrc;

    move-result-object v10

    .line 488
    int-to-float v0, v0

    invoke-virtual {v10}, Lfrc;->i()I

    move-result v11

    int-to-float v11, v11

    .line 489
    invoke-virtual {v3}, Lfrc;->o()F

    move-result v12

    .line 490
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v13

    sub-int v2, v13, v2

    sub-int/2addr v2, v8

    int-to-float v2, v2

    mul-float/2addr v2, v12

    add-float/2addr v2, v11

    sub-float/2addr v0, v2

    float-to-int v2, v0

    .line 491
    int-to-float v0, v1

    invoke-virtual {v10}, Lfrc;->j()I

    move-result v1

    int-to-float v1, v1

    .line 492
    invoke-virtual {v3}, Lfrc;->p()F

    move-result v8

    .line 493
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    sub-int/2addr v6, v7

    sub-int/2addr v6, v9

    int-to-float v6, v6

    mul-float/2addr v6, v8

    add-float/2addr v1, v6

    sub-float/2addr v0, v1

    float-to-int v0, v0

    move v1, v2

    .line 518
    :goto_2
    sub-int/2addr v1, v5

    add-int/2addr v1, p1

    sub-int/2addr v0, v4

    add-int v0, v0, p2

    invoke-virtual {v3, v1, v0}, Lfrc;->a(II)V

    .line 519
    return-void

    .line 474
    :cond_0
    invoke-virtual {v3}, Lfrc;->v()I

    move-result v0

    goto :goto_0

    .line 475
    :cond_1
    invoke-virtual {v3}, Lfrc;->u()I

    move-result v1

    goto :goto_1

    .line 495
    :cond_2
    int-to-float v0, v0

    invoke-virtual {v3}, Lfrc;->o()F

    move-result v7

    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    sub-int/2addr v8, v5

    sub-int v2, v8, v2

    int-to-float v2, v2

    mul-float/2addr v2, v7

    sub-float/2addr v0, v2

    float-to-int v2, v0

    .line 496
    int-to-float v0, v1

    invoke-virtual {v3}, Lfrc;->p()F

    move-result v1

    .line 497
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    sub-int/2addr v7, v4

    sub-int v6, v7, v6

    int-to-float v6, v6

    mul-float/2addr v1, v6

    sub-float/2addr v0, v1

    float-to-int v0, v0

    .line 498
    invoke-virtual {v3}, Lfrc;->D()I

    move-result v1

    .line 499
    invoke-virtual {v3}, Lfrc;->m()Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-static {v1}, Landroid/view/Gravity;->isVertical(I)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 500
    invoke-virtual {v3}, Lfrc;->n()I

    move-result v6

    .line 501
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    if-le v6, v7, :cond_3

    .line 502
    and-int/lit8 v7, v1, 0x70

    packed-switch v7, :pswitch_data_0

    .line 509
    :cond_3
    :goto_3
    invoke-virtual {v3}, Lfrc;->k()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-static {v1}, Landroid/view/Gravity;->isHorizontal(I)Z

    move-result v6

    if-eqz v6, :cond_4

    .line 510
    invoke-virtual {v3}, Lfrc;->l()I

    move-result v6

    .line 511
    and-int/lit8 v1, v1, 0x7

    packed-switch v1, :pswitch_data_1

    :cond_4
    move v1, v2

    goto :goto_2

    .line 504
    :pswitch_0
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    sub-int/2addr v6, v7

    add-int/2addr v0, v6

    goto :goto_3

    .line 513
    :pswitch_1
    invoke-virtual {p0}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int v1, v6, v1

    div-int/lit8 v1, v1, 0x2

    add-int/2addr v1, v2

    goto :goto_2

    .line 502
    :pswitch_data_0
    .packed-switch 0x50
        :pswitch_0
    .end packed-switch

    .line 511
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method

.method private c()Z
    .locals 2

    .prologue
    .line 427
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_0

    invoke-virtual {p0}, Lfua;->isHardwareAccelerated()Z

    move-result v0

    if-eqz v0, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 432
    iput-object v1, p0, Lfua;->i:Landroid/graphics/drawable/Drawable;

    .line 433
    iput-object v1, p0, Lfua;->m:Lfrd;

    .line 434
    iget-object v0, p0, Lfua;->o:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->reset()V

    .line 435
    iput-object v1, p0, Lfua;->p:Landroid/graphics/Path;

    .line 436
    iput v2, p0, Lfua;->l:I

    .line 437
    iput-boolean v2, p0, Lfua;->k:Z

    .line 439
    invoke-virtual {p0}, Lfua;->getChildCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 440
    invoke-virtual {p0, v0}, Lfua;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 441
    iget-object v2, p0, Lfua;->a:Lmnk;

    iget-object v2, v2, Lmnk;->e:[Lmnk;

    iget-object v3, p0, Lfua;->q:Landroid/util/SparseIntArray;

    .line 442
    invoke-virtual {v3, v0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v3

    aget-object v2, v2, v3

    .line 443
    iget-object v3, p0, Lfua;->c:Lfrj;

    iget-object v4, p0, Lfua;->b:Lfqu;

    invoke-virtual {v4, v2}, Lfqu;->a(Lmnk;)I

    move-result v2

    invoke-virtual {v3, v1, v2}, Lfrj;->a(Landroid/view/View;I)V

    .line 439
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 445
    :cond_0
    invoke-virtual {p0}, Lfua;->removeAllViews()V

    .line 446
    iget-object v0, p0, Lfua;->q:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    .line 447
    return-void
.end method

.method public a(Lfqu;)V
    .locals 1

    .prologue
    .line 99
    iput-object p1, p0, Lfua;->b:Lfqu;

    .line 100
    invoke-virtual {p1}, Lfqu;->a()Lfrj;

    move-result-object v0

    iput-object v0, p0, Lfua;->c:Lfrj;

    .line 101
    return-void
.end method

.method public a(Lmnk;)V
    .locals 9

    .prologue
    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 109
    iput-object p1, p0, Lfua;->a:Lmnk;

    .line 110
    invoke-static {p0}, Lfrc;->a(Landroid/view/View;)Lfrc;

    move-result-object v2

    .line 111
    invoke-virtual {v2}, Lfrc;->C()I

    move-result v3

    .line 112
    invoke-virtual {v2}, Lfrc;->E()Z

    move-result v4

    .line 113
    invoke-virtual {v2}, Lfrc;->N()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 114
    invoke-virtual {v2}, Lfrc;->O()I

    move-result v0

    iput v0, p0, Lfua;->l:I

    .line 115
    iget v0, p0, Lfua;->l:I

    if-ne v0, v7, :cond_0

    invoke-virtual {v2}, Lfrc;->P()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    iput-boolean v7, p0, Lfua;->k:Z

    .line 119
    iget-object v0, p0, Lfua;->h:Landroid/graphics/Paint;

    invoke-virtual {v2}, Lfrc;->Q()I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 120
    iget-object v0, p0, Lfua;->h:Landroid/graphics/Paint;

    invoke-virtual {v2}, Lfrc;->R()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 121
    iget-object v0, p0, Lfua;->h:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 124
    :cond_0
    if-eqz v4, :cond_1

    .line 125
    invoke-virtual {p0}, Lfua;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v5, Lfru;

    invoke-static {v0, v5}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfru;

    .line 126
    invoke-virtual {v2}, Lfrc;->F()I

    move-result v5

    invoke-virtual {v2}, Lfrc;->I()I

    move-result v6

    .line 125
    invoke-virtual {v0, v5, v6}, Lfru;->a(II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lfua;->i:Landroid/graphics/drawable/Drawable;

    .line 128
    :cond_1
    iget v0, p0, Lfua;->l:I

    if-eq v0, v7, :cond_2

    if-eqz v4, :cond_4

    .line 129
    :cond_2
    invoke-virtual {p0, v1}, Lfua;->setBackgroundColor(I)V

    .line 130
    iget-object v0, p0, Lfua;->g:Landroid/graphics/Paint;

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 131
    iput-boolean v7, p0, Lfua;->j:Z

    .line 137
    :goto_0
    invoke-virtual {v2}, Lfrc;->S()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 138
    invoke-virtual {v2}, Lfrc;->T()Lfrd;

    move-result-object v0

    iput-object v0, p0, Lfua;->m:Lfrd;

    .line 139
    iget-object v0, p0, Lfua;->o:Landroid/graphics/Paint;

    iget-object v2, p0, Lfua;->m:Lfrd;

    iget v2, v2, Lfrd;->b:I

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 140
    iget-object v0, p0, Lfua;->o:Landroid/graphics/Paint;

    iget-object v2, p0, Lfua;->m:Lfrd;

    iget v2, v2, Lfrd;->a:I

    int-to-float v2, v2

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 141
    iget-object v0, p0, Lfua;->o:Landroid/graphics/Paint;

    sget-object v2, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 146
    :cond_3
    iget-object v3, p1, Lmnk;->e:[Lmnk;

    array-length v4, v3

    move v0, v1

    move v2, v1

    :goto_1
    if-ge v0, v4, :cond_5

    aget-object v5, v3, v0

    .line 149
    iget-object v6, p0, Lfua;->b:Lfqu;

    iget-object v7, p0, Lfua;->c:Lfrj;

    iget-object v8, p0, Lfua;->b:Lfqu;

    .line 150
    invoke-virtual {v8, v5}, Lfqu;->a(Lmnk;)I

    move-result v8

    invoke-virtual {v7, v8}, Lfrj;->a(I)Landroid/view/View;

    move-result-object v7

    .line 149
    invoke-virtual {v6, v5, v7, p0}, Lfqu;->a(Lmnk;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 151
    iget-object v6, p0, Lfua;->q:Landroid/util/SparseIntArray;

    invoke-virtual {v6, v2, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 152
    invoke-virtual {p0, v5}, Lfua;->addView(Landroid/view/View;)V

    .line 153
    add-int/lit8 v2, v2, 0x1

    .line 154
    add-int/lit8 v1, v1, 0x1

    .line 146
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 133
    :cond_4
    invoke-virtual {p0, v3}, Lfua;->setBackgroundColor(I)V

    .line 134
    iput-boolean v1, p0, Lfua;->j:Z

    goto :goto_0

    .line 156
    :cond_5
    return-void
.end method

.method public b()Z
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 356
    invoke-virtual {p0}, Lfua;->getChildCount()I

    move-result v7

    .line 357
    invoke-virtual {p0}, Lfua;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v2

    :goto_0
    move v6, v3

    move v5, v0

    .line 358
    :goto_1
    if-ge v6, v7, :cond_5

    .line 359
    invoke-virtual {p0, v6}, Lfua;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 360
    instance-of v0, v1, Lfua;

    if-eqz v0, :cond_2

    move-object v0, v1

    check-cast v0, Lfua;

    .line 361
    invoke-virtual {v0}, Lfua;->b()Z

    move-result v0

    if-nez v0, :cond_2

    move v4, v2

    .line 362
    :goto_2
    const v0, 0x7f100040

    invoke-virtual {v1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    .line 363
    if-eqz v0, :cond_0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    if-nez v4, :cond_3

    move v4, v2

    .line 365
    :goto_3
    if-eqz v4, :cond_4

    move v0, v3

    :goto_4
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 366
    if-eqz v4, :cond_6

    move v0, v2

    .line 358
    :goto_5
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    move v5, v0

    goto :goto_1

    :cond_1
    move v0, v3

    .line 357
    goto :goto_0

    :cond_2
    move v4, v3

    .line 361
    goto :goto_2

    :cond_3
    move v4, v3

    .line 363
    goto :goto_3

    .line 365
    :cond_4
    const/4 v0, 0x4

    goto :goto_4

    .line 370
    :cond_5
    return v5

    :cond_6
    move v0, v5

    goto :goto_5
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 391
    iget-object v0, p0, Lfua;->p:Landroid/graphics/Path;

    if-eqz v0, :cond_0

    .line 392
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->save(I)I

    .line 393
    invoke-direct {p0}, Lfua;->c()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 394
    iget-object v0, p0, Lfua;->p:Landroid/graphics/Path;

    sget-object v1, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;Landroid/graphics/Region$Op;)Z

    .line 399
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 401
    iget-boolean v0, p0, Lfua;->k:Z

    if-eqz v0, :cond_1

    .line 402
    iget-object v0, p0, Lfua;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lfua;->f:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lfua;->f:Landroid/graphics/Rect;

    .line 403
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget-object v3, p0, Lfua;->f:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    iget-object v3, p0, Lfua;->h:Landroid/graphics/Paint;

    .line 402
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 407
    :cond_1
    iget-object v0, p0, Lfua;->p:Landroid/graphics/Path;

    if-eqz v0, :cond_2

    .line 408
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 415
    :cond_2
    iget-object v0, p0, Lfua;->m:Lfrd;

    if-eqz v0, :cond_3

    .line 416
    invoke-direct {p0}, Lfua;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lfua;->m:Lfrd;

    iget v0, v0, Lfrd;->c:I

    .line 417
    :goto_1
    iget-object v1, p0, Lfua;->n:Landroid/graphics/RectF;

    int-to-float v2, v0

    int-to-float v0, v0

    iget-object v3, p0, Lfua;->o:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 419
    :cond_3
    return-void

    .line 396
    :cond_4
    iget-object v0, p0, Lfua;->n:Landroid/graphics/RectF;

    sget-object v1, Landroid/graphics/Region$Op;->INTERSECT:Landroid/graphics/Region$Op;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;Landroid/graphics/Region$Op;)Z

    goto :goto_0

    .line 416
    :cond_5
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 375
    iget-object v0, p0, Lfua;->i:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Lfua;->i:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lfua;->d:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 377
    iget-object v0, p0, Lfua;->i:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 379
    :cond_0
    iget-boolean v0, p0, Lfua;->j:Z

    if-eqz v0, :cond_1

    .line 380
    iget v0, p0, Lfua;->l:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 381
    iget-object v0, p0, Lfua;->e:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->centerX()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p0, Lfua;->e:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lfua;->e:Landroid/graphics/Rect;

    .line 382
    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    iget-object v3, p0, Lfua;->e:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->height()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    int-to-float v2, v2

    const/high16 v3, 0x40000000    # 2.0f

    div-float/2addr v2, v3

    iget-object v3, p0, Lfua;->g:Landroid/graphics/Paint;

    .line 381
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 387
    :cond_1
    :goto_0
    return-void

    .line 384
    :cond_2
    iget-object v0, p0, Lfua;->e:Landroid/graphics/Rect;

    iget-object v1, p0, Lfua;->g:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    .line 340
    invoke-virtual {p0}, Lfua;->getChildCount()I

    move-result v1

    .line 341
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 342
    invoke-virtual {p0, v0}, Lfua;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 344
    invoke-static {v2}, Lfrc;->a(Landroid/view/View;)Lfrc;

    move-result-object v3

    .line 345
    invoke-virtual {v3}, Lfrc;->i()I

    move-result v4

    .line 346
    invoke-virtual {v3}, Lfrc;->j()I

    move-result v3

    .line 347
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    .line 348
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 349
    add-int/2addr v5, v4

    add-int/2addr v6, v3

    invoke-virtual {v2, v4, v3, v5, v6}, Landroid/view/View;->layout(IIII)V

    .line 341
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 351
    :cond_0
    invoke-virtual {p0}, Lfua;->b()Z

    .line 352
    return-void
.end method

.method protected onMeasure(II)V
    .locals 30

    .prologue
    .line 160
    .line 161
    invoke-static/range {p0 .. p0}, Lfrc;->a(Landroid/view/View;)Lfrc;

    move-result-object v4

    .line 162
    invoke-virtual {v4}, Lfrc;->y()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    move v3, v2

    .line 167
    :goto_0
    invoke-virtual {v4}, Lfrc;->W()Lfrh;

    move-result-object v19

    .line 168
    invoke-virtual {v4}, Lfrc;->S()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v4}, Lfrc;->T()Lfrd;

    move-result-object v2

    iget v2, v2, Lfrd;->a:I

    .line 169
    :goto_1
    move-object/from16 v0, v19

    iget v4, v0, Lfrh;->c:I

    add-int v11, v2, v4

    .line 170
    move-object/from16 v0, v19

    iget v4, v0, Lfrh;->b:I

    add-int v16, v2, v4

    .line 171
    move-object/from16 v0, v19

    iget v4, v0, Lfrh;->d:I

    add-int v14, v2, v4

    .line 172
    move-object/from16 v0, v19

    iget v4, v0, Lfrh;->e:I

    add-int v12, v2, v4

    .line 177
    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v20

    .line 178
    if-eqz v3, :cond_3

    const/4 v2, 0x0

    move/from16 v18, v2

    .line 179
    :goto_2
    add-int v2, v16, v14

    sub-int v21, v20, v2

    .line 180
    if-eqz v3, :cond_4

    const/4 v2, 0x0

    .line 183
    :goto_3
    const/4 v6, 0x0

    .line 184
    const/4 v4, 0x0

    invoke-virtual/range {p0 .. p0}, Lfua;->getChildCount()I

    move-result v22

    move v10, v4

    move v7, v11

    move v8, v2

    move v9, v12

    move v13, v14

    move/from16 v15, v16

    move/from16 v17, v11

    :goto_4
    move/from16 v0, v22

    if-ge v10, v0, :cond_c

    .line 185
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Lfua;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 187
    invoke-static {v2}, Lfrc;->a(Landroid/view/View;)Lfrc;

    move-result-object v23

    .line 188
    invoke-virtual/range {v23 .. v23}, Lfrc;->v()I

    move-result v5

    invoke-virtual/range {v23 .. v23}, Lfrc;->x()I

    move-result v24

    invoke-virtual/range {v23 .. v23}, Lfrc;->u()I

    move-result v25

    invoke-virtual/range {v23 .. v23}, Lfrc;->w()I

    move-result v26

    invoke-virtual/range {v23 .. v23}, Lfrc;->W()Lfrh;

    move-result-object v27

    invoke-virtual/range {v23 .. v23}, Lfrc;->S()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual/range {v23 .. v23}, Lfrc;->T()Lfrd;

    move-result-object v4

    iget v4, v4, Lfrd;->a:I

    :goto_5
    invoke-virtual/range {v23 .. v23}, Lfrc;->k()Z

    move-result v28

    if-eqz v28, :cond_6

    invoke-virtual/range {v23 .. v23}, Lfrc;->l()I

    move-result v5

    :goto_6
    move-object/from16 v0, v27

    iget v0, v0, Lfrh;->b:I

    move/from16 v24, v0

    add-int v5, v5, v24

    move-object/from16 v0, v27

    iget v0, v0, Lfrh;->d:I

    move/from16 v24, v0

    add-int v5, v5, v24

    mul-int/lit8 v24, v4, 0x2

    add-int v24, v24, v5

    if-eqz v28, :cond_7

    const/high16 v5, 0x40000000    # 2.0f

    :goto_7
    move/from16 v0, v24

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v24

    invoke-virtual/range {v23 .. v23}, Lfrc;->m()Z

    move-result v28

    if-eqz v28, :cond_8

    invoke-virtual/range {v23 .. v23}, Lfrc;->n()I

    move-result v5

    :goto_8
    move-object/from16 v0, v27

    iget v0, v0, Lfrh;->c:I

    move/from16 v25, v0

    add-int v5, v5, v25

    move-object/from16 v0, v27

    iget v0, v0, Lfrh;->e:I

    move/from16 v25, v0

    add-int v5, v5, v25

    mul-int/lit8 v4, v4, 0x2

    add-int/2addr v5, v4

    if-eqz v28, :cond_9

    const/high16 v4, 0x40000000    # 2.0f

    :goto_9
    invoke-static {v5, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    move/from16 v0, v24

    invoke-virtual {v2, v0, v4}, Landroid/view/View;->measure(II)V

    .line 189
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v24

    .line 190
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    .line 195
    if-eqz v3, :cond_b

    .line 196
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    invoke-virtual {v2}, Landroid/view/View;->getPaddingTop()I

    move-result v25

    invoke-virtual {v2}, Landroid/view/View;->getPaddingLeft()I

    move-result v26

    invoke-virtual {v2}, Landroid/view/View;->getPaddingRight()I

    move-result v27

    invoke-virtual {v2}, Landroid/view/View;->getPaddingBottom()I

    move-result v28

    add-int v27, v27, v26

    sub-int v27, v4, v27

    add-int v4, v25, v28

    sub-int/2addr v8, v4

    invoke-virtual/range {v23 .. v23}, Lfrc;->u()I

    move-result v4

    add-int/2addr v4, v7

    sub-int v7, v4, v25

    invoke-virtual/range {v23 .. v23}, Lfrc;->v()I

    move-result v4

    add-int v4, v4, v16

    sub-int v4, v4, v26

    instance-of v0, v2, Landroid/widget/TextView;

    move/from16 v26, v0

    if-eqz v26, :cond_0

    move/from16 v0, v27

    move/from16 v1, v21

    if-ge v0, v1, :cond_0

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getGravity()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    move v2, v4

    :goto_a
    move-object/from16 v0, v23

    invoke-virtual {v0, v2, v7}, Lfrc;->a(II)V

    add-int v2, v7, v25

    add-int/2addr v2, v8

    invoke-virtual/range {v23 .. v23}, Lfrc;->w()I

    move-result v4

    add-int/2addr v2, v4

    int-to-float v2, v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v7

    .line 198
    invoke-virtual/range {v23 .. v23}, Lfrc;->j()I

    move-result v4

    .line 199
    invoke-virtual/range {v23 .. v23}, Lfrc;->i()I

    move-result v2

    .line 200
    add-int/2addr v5, v4

    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 201
    sub-int v6, v7, v11

    move v8, v9

    move/from16 v29, v6

    move v6, v7

    move/from16 v7, v29

    .line 210
    :goto_b
    const/4 v9, 0x0

    invoke-static {v9, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    move/from16 v0, v17

    invoke-static {v0, v4}, Ljava/lang/Math;->max(II)I

    move-result v17

    .line 211
    const/4 v4, 0x0

    invoke-static {v4, v2}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Math;->abs(I)I

    move-result v4

    invoke-static {v15, v4}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 212
    const/4 v4, 0x0

    add-int v2, v2, v24

    sub-int v2, v2, v20

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v13, v2}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 184
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    move v13, v4

    move v15, v9

    move v9, v8

    move v8, v7

    move v7, v6

    move v6, v5

    goto/16 :goto_4

    .line 162
    :cond_1
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_0

    .line 168
    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 178
    :cond_3
    invoke-static/range {p2 .. p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    move/from16 v18, v2

    goto/16 :goto_2

    .line 180
    :cond_4
    add-int v2, v11, v12

    sub-int v2, v18, v2

    goto/16 :goto_3

    .line 188
    :cond_5
    const/4 v4, 0x0

    goto/16 :goto_5

    :cond_6
    add-int v5, v5, v24

    sub-int v5, v21, v5

    goto/16 :goto_6

    :cond_7
    const/high16 v5, -0x80000000

    goto/16 :goto_7

    :cond_8
    add-int v5, v25, v26

    sub-int v5, v8, v5

    goto/16 :goto_8

    :cond_9
    if-eqz v3, :cond_a

    const/4 v4, 0x0

    goto/16 :goto_9

    :cond_a
    const/high16 v4, -0x80000000

    goto/16 :goto_9

    :sswitch_0
    move v2, v4

    .line 196
    goto/16 :goto_a

    :sswitch_1
    sub-int v2, v21, v27

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v2, v4

    goto/16 :goto_a

    :sswitch_2
    sub-int v2, v21, v27

    add-int/2addr v2, v4

    goto/16 :goto_a

    .line 203
    :cond_b
    move/from16 v0, v16

    invoke-static {v2, v0, v11}, Lfua;->a(Landroid/view/View;II)V

    .line 204
    invoke-virtual/range {v23 .. v23}, Lfrc;->j()I

    move-result v4

    .line 205
    invoke-virtual/range {v23 .. v23}, Lfrc;->i()I

    move-result v2

    .line 206
    const/16 v23, 0x0

    add-int/2addr v5, v4

    sub-int v5, v5, v18

    .line 207
    move/from16 v0, v23

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    invoke-static {v9, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    move/from16 v29, v6

    move v6, v7

    move v7, v8

    move v8, v5

    move/from16 v5, v29

    goto/16 :goto_b

    .line 214
    :cond_c
    if-eqz v3, :cond_d

    .line 215
    const/4 v2, 0x0

    sub-int v4, v6, v8

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v9, v2}, Ljava/lang/Math;->max(II)I

    move-result v9

    .line 218
    :cond_d
    const/4 v2, 0x0

    sub-int v4, v17, v11

    invoke-static {v2, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 219
    const/4 v2, 0x0

    sub-int v5, v15, v16

    invoke-static {v2, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 220
    const/4 v2, 0x0

    sub-int v6, v13, v14

    invoke-static {v2, v6}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 221
    const/4 v6, 0x0

    sub-int v7, v9, v12

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 222
    add-int v7, v20, v5

    add-int/2addr v7, v2

    if-eqz v3, :cond_f

    add-int v2, v8, v17

    add-int/2addr v2, v9

    :goto_c
    move-object/from16 v0, p0

    invoke-virtual {v0, v7, v2}, Lfua;->setMeasuredDimension(II)V

    .line 226
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v15, v1, v13, v9}, Lfua;->setPadding(IIII)V

    .line 229
    if-gtz v4, :cond_e

    if-lez v5, :cond_10

    .line 230
    :cond_e
    const/4 v2, 0x0

    invoke-virtual/range {p0 .. p0}, Lfua;->getChildCount()I

    move-result v6

    :goto_d
    if-ge v2, v6, :cond_10

    .line 232
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lfua;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-static {v7}, Lfrc;->a(Landroid/view/View;)Lfrc;

    move-result-object v7

    .line 233
    invoke-virtual {v7}, Lfrc;->i()I

    move-result v9

    add-int/2addr v9, v5

    .line 234
    invoke-virtual {v7}, Lfrc;->j()I

    move-result v10

    add-int/2addr v10, v4

    .line 233
    invoke-virtual {v7, v9, v10}, Lfrc;->a(II)V

    .line 230
    add-int/lit8 v2, v2, 0x1

    goto :goto_d

    .line 222
    :cond_f
    add-int v2, v18, v4

    add-int/2addr v2, v6

    goto :goto_c

    .line 237
    :cond_10
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lfua;->j:Z

    if-eqz v2, :cond_11

    .line 238
    move-object/from16 v0, p0

    iget-object v2, v0, Lfua;->d:Landroid/graphics/Rect;

    add-int v6, v5, v20

    add-int v7, v4, v18

    invoke-virtual {v2, v5, v4, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 240
    move-object/from16 v0, p0

    iget-object v2, v0, Lfua;->e:Landroid/graphics/Rect;

    add-int v6, v15, v21

    add-int v7, v17, v8

    move/from16 v0, v17

    invoke-virtual {v2, v15, v0, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    .line 242
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lfua;->k:Z

    if-eqz v2, :cond_11

    .line 245
    move-object/from16 v0, p0

    iget-object v2, v0, Lfua;->h:Landroid/graphics/Paint;

    invoke-virtual {v2}, Landroid/graphics/Paint;->getStrokeWidth()F

    move-result v2

    float-to-int v2, v2

    div-int/lit8 v2, v2, 0x2

    .line 246
    move-object/from16 v0, p0

    iget-object v6, v0, Lfua;->f:Landroid/graphics/Rect;

    move-object/from16 v0, p0

    iget-object v7, v0, Lfua;->e:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    add-int/2addr v7, v2

    move-object/from16 v0, p0

    iget-object v9, v0, Lfua;->e:Landroid/graphics/Rect;

    iget v9, v9, Landroid/graphics/Rect;->top:I

    add-int/2addr v9, v2

    move-object/from16 v0, p0

    iget-object v10, v0, Lfua;->e:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->right:I

    sub-int/2addr v10, v2

    move-object/from16 v0, p0

    iget-object v11, v0, Lfua;->e:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->bottom:I

    sub-int v2, v11, v2

    invoke-virtual {v6, v7, v9, v10, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 253
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lfua;->m:Lfrd;

    if-eqz v2, :cond_12

    .line 254
    move-object/from16 v0, p0

    iget-object v2, v0, Lfua;->m:Lfrd;

    iget v2, v2, Lfrd;->a:I

    div-int/lit8 v2, v2, 0x2

    .line 255
    move-object/from16 v0, p0

    iget-object v6, v0, Lfua;->n:Landroid/graphics/RectF;

    move-object/from16 v0, v19

    iget v7, v0, Lfrh;->b:I

    add-int/2addr v7, v5

    add-int/2addr v7, v2

    int-to-float v7, v7

    move-object/from16 v0, v19

    iget v9, v0, Lfrh;->c:I

    add-int/2addr v9, v4

    add-int/2addr v9, v2

    int-to-float v9, v9

    add-int v5, v5, v20

    move-object/from16 v0, v19

    iget v10, v0, Lfrh;->d:I

    sub-int/2addr v5, v10

    sub-int/2addr v5, v2

    int-to-float v5, v5

    if-eqz v3, :cond_13

    add-int v3, v17, v8

    add-int/2addr v2, v3

    int-to-float v2, v2

    :goto_e
    invoke-virtual {v6, v7, v9, v5, v2}, Landroid/graphics/RectF;->set(FFFF)V

    .line 261
    move-object/from16 v0, p0

    iget-object v2, v0, Lfua;->p:Landroid/graphics/Path;

    if-nez v2, :cond_14

    .line 262
    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lfua;->p:Landroid/graphics/Path;

    .line 266
    :goto_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lfua;->p:Landroid/graphics/Path;

    move-object/from16 v0, p0

    iget-object v3, v0, Lfua;->n:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v4, v0, Lfua;->m:Lfrd;

    iget v4, v4, Lfrd;->c:I

    int-to-float v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lfua;->m:Lfrd;

    iget v5, v5, Lfrd;->c:I

    int-to-float v5, v5

    sget-object v6, Landroid/graphics/Path$Direction;->CW:Landroid/graphics/Path$Direction;

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/Path;->addRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Path$Direction;)V

    .line 269
    :cond_12
    return-void

    .line 255
    :cond_13
    add-int v3, v4, v18

    move-object/from16 v0, v19

    iget v4, v0, Lfrh;->e:I

    sub-int/2addr v3, v4

    sub-int v2, v3, v2

    int-to-float v2, v2

    goto :goto_e

    .line 264
    :cond_14
    move-object/from16 v0, p0

    iget-object v2, v0, Lfua;->p:Landroid/graphics/Path;

    invoke-virtual {v2}, Landroid/graphics/Path;->reset()V

    goto :goto_f

    .line 196
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x5 -> :sswitch_2
        0x11 -> :sswitch_1
    .end sparse-switch
.end method
