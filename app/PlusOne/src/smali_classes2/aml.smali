.class final Laml;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhob;


# instance fields
.field private synthetic a:Lamb;


# direct methods
.method constructor <init>(Lamb;)V
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Laml;->a:Lamb;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a([B)Lnbq;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 299
    if-nez p1, :cond_0

    .line 300
    new-instance v0, Lche;

    const-string v1, "No response bytes"

    invoke-direct {v0, v1}, Lche;-><init>(Ljava/lang/String;)V

    throw v0

    .line 305
    :cond_0
    :try_start_0
    new-instance v0, Lnbq;

    invoke-direct {v0}, Lnbq;-><init>()V

    invoke-static {v0, p1}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnbq;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 310
    iget-object v1, v0, Lnbq;->c:Lnbr;

    .line 311
    if-nez v1, :cond_1

    .line 312
    new-instance v0, Lche;

    const-string v1, "No result"

    invoke-direct {v0, v1}, Lche;-><init>(Ljava/lang/String;)V

    throw v0

    .line 306
    :catch_0
    move-exception v0

    .line 307
    new-instance v1, Lche;

    invoke-direct {v1, v0}, Lche;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 314
    :cond_1
    iget-object v2, v0, Lnbq;->b:Lnwv;

    if-nez v2, :cond_2

    .line 315
    new-instance v0, Lche;

    const-string v1, "No filters"

    invoke-direct {v0, v1}, Lche;-><init>(Ljava/lang/String;)V

    throw v0

    .line 317
    :cond_2
    iget-object v2, v0, Lnbq;->b:Lnwv;

    iget-object v2, v2, Lnwv;->a:Lnwy;

    if-nez v2, :cond_3

    .line 318
    new-instance v0, Lche;

    const-string v1, "No photoRef"

    invoke-direct {v0, v1}, Lche;-><init>(Ljava/lang/String;)V

    throw v0

    .line 320
    :cond_3
    iget-object v2, v0, Lnbq;->a:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 321
    new-instance v0, Lche;

    const-string v1, "No versionId"

    invoke-direct {v0, v1}, Lche;-><init>(Ljava/lang/String;)V

    throw v0

    .line 323
    :cond_4
    iget v2, v1, Lnbr;->a:I

    if-eq v2, v4, :cond_5

    iget v2, v1, Lnbr;->a:I

    const/4 v3, 0x2

    if-eq v2, v3, :cond_5

    .line 327
    new-instance v0, Lche;

    iget v1, v1, Lnbr;->a:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x23

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unkown mutation status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lche;-><init>(Ljava/lang/String;)V

    throw v0

    .line 330
    :cond_5
    iget v1, v1, Lnbr;->a:I

    if-ne v1, v4, :cond_9

    .line 332
    iget-object v1, v0, Lnbq;->b:Lnwv;

    iget-object v1, v1, Lnwv;->b:Lopf;

    if-nez v1, :cond_6

    .line 333
    new-instance v0, Lche;

    const-string v1, "Mutation applied. No renderParams"

    invoke-direct {v0, v1}, Lche;-><init>(Ljava/lang/String;)V

    throw v0

    .line 335
    :cond_6
    iget-object v1, v0, Lnbq;->b:Lnwv;

    iget-object v1, v1, Lnwv;->b:Lopf;

    iget v1, v1, Lopf;->b:I

    const/16 v2, 0x8

    if-eq v1, v2, :cond_7

    .line 336
    new-instance v1, Lche;

    iget-object v0, v0, Lnbq;->b:Lnwv;

    iget-object v0, v0, Lnwv;->b:Lopf;

    iget v0, v0, Lopf;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x2d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "mutation applied but renderType = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lche;-><init>(Ljava/lang/String;)V

    throw v1

    .line 339
    :cond_7
    iget-object v1, v0, Lnbq;->b:Lnwv;

    iget-object v1, v1, Lnwv;->b:Lopf;

    iget-object v1, v1, Lopf;->c:Lpxr;

    if-eqz v1, :cond_8

    iget-object v1, v0, Lnbq;->b:Lnwv;

    iget-object v1, v1, Lnwv;->b:Lopf;

    iget-object v1, v1, Lopf;->c:Lpxr;

    iget-object v1, v1, Lpxr;->a:Lood;

    if-nez v1, :cond_9

    .line 341
    :cond_8
    new-instance v0, Lche;

    const-string v1, "mutation applied but missing storyboard"

    invoke-direct {v0, v1}, Lche;-><init>(Ljava/lang/String;)V

    throw v0

    .line 345
    :cond_9
    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 5

    .prologue
    .line 209
    iget-object v0, p0, Laml;->a:Lamb;

    iget-boolean v0, v0, Lamn;->i:Z

    if-nez v0, :cond_1

    .line 217
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    const-string v0, "SaveCloudStoryboardTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {}, Lamb;->d()Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x20

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Failed to save cloud storyboard "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Lhoz;->b()Ljava/lang/Exception;

    iget-object v0, p0, Laml;->a:Lamb;

    invoke-static {v0}, Lamb;->a(Lamb;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lamk;

    invoke-interface {v0}, Lamk;->b()V

    .line 216
    :cond_2
    :goto_1
    iget-object v1, p0, Laml;->a:Lamb;

    iget-object v0, p0, Laml;->a:Lamb;

    invoke-static {v0}, Lamb;->a(Lamb;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lamk;

    invoke-static {v1, v0}, Lamb;->a(Lamb;Lamk;)V

    goto :goto_0

    .line 215
    :cond_3
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "mutate_filters_response"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    const-string v2, "storyboard_token"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    const-string v2, "storyboardToken"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    :try_start_0
    invoke-direct {p0, v1}, Laml;->a([B)Lnbq;
    :try_end_0
    .catch Lche; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    iget-object v2, p0, Laml;->a:Lamb;

    invoke-static {v2}, Lamb;->c(Lamb;)Lavw;

    move-result-object v2

    invoke-interface {v2}, Lavw;->aR()Ljdz;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v3, v2, Ljdz;->a:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v1, Lnbq;->b:Lnwv;

    iget-object v4, v4, Lnwv;->a:Lnwy;

    iget-object v4, v4, Lnwy;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v2, v2, Ljdz;->b:Ljava/lang/String;

    iget-object v3, v1, Lnbq;->b:Lnwv;

    iget-object v3, v3, Lnwv;->a:Lnwy;

    iget-object v3, v3, Lnwy;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v1, Lnbq;->c:Lnbr;

    iget v2, v2, Lnbr;->a:I

    packed-switch v2, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :catch_0
    move-exception v1

    iget-object v0, p0, Laml;->a:Lamb;

    invoke-static {v0}, Lamb;->a(Lamb;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lamk;

    invoke-interface {v0}, Lamk;->b()V

    iget-object v0, p0, Laml;->a:Lamb;

    invoke-static {v0}, Lamb;->b(Lamb;)Lcdu;

    move-result-object v0

    new-instance v2, Lcdx;

    invoke-direct {v2, v1}, Lcdx;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v0, v2}, Lcdu;->b(Lcdx;)V

    invoke-static {}, Lamb;->d()Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_0
    iget-object v2, p0, Laml;->a:Lamb;

    invoke-static {v2}, Lamb;->c(Lamb;)Lavw;

    move-result-object v2

    iget-object v1, v1, Lnbq;->a:Ljava/lang/String;

    invoke-interface {v2, v1, v0}, Lavw;->a(Ljava/lang/String;[B)V

    iget-object v0, p0, Laml;->a:Lamb;

    invoke-static {v0}, Lamb;->c(Lamb;)Lavw;

    move-result-object v0

    invoke-interface {v0}, Lavw;->bb()V

    iget-object v0, p0, Laml;->a:Lamb;

    invoke-static {v0}, Lamb;->d(Lamb;)Laxw;

    move-result-object v0

    new-instance v1, Lamg;

    iget-object v2, p0, Laml;->a:Lamb;

    invoke-direct {v1, v2}, Lamg;-><init>(Lamb;)V

    invoke-virtual {v0, v1}, Laxw;->a(Laxx;)Laxv;

    move-result-object v0

    iget-object v1, p0, Laml;->a:Lamb;

    invoke-static {v1, v0}, Lamb;->a(Lamb;Landroid/os/AsyncTask;)Landroid/os/AsyncTask;

    iget-object v1, p0, Laml;->a:Lamb;

    invoke-static {v1}, Lamb;->e(Lamb;)Lalg;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljdz;

    const/4 v3, 0x0

    iget-object v4, p0, Laml;->a:Lamb;

    invoke-static {v4}, Lamb;->c(Lamb;)Lavw;

    move-result-object v4

    invoke-interface {v4}, Lavw;->aR()Ljdz;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v1, v0, v2}, Lalg;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    goto/16 :goto_1

    :pswitch_1
    iget-object v0, p0, Laml;->a:Lamb;

    invoke-static {v0}, Lamb;->c(Lamb;)Lavw;

    move-result-object v0

    invoke-interface {v0}, Lavw;->bf()Ljava/lang/String;

    move-result-object v0

    iget-object v1, v1, Lnbq;->a:Ljava/lang/String;

    if-eqz v1, :cond_4

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    invoke-static {}, Lamb;->d()Ljava/lang/String;

    iget-object v0, p0, Laml;->a:Lamb;

    invoke-static {v0}, Lamb;->c(Lamb;)Lavw;

    move-result-object v0

    invoke-interface {v0, v1}, Lavw;->f(Ljava/lang/String;)V

    iget-object v0, p0, Laml;->a:Lamb;

    invoke-static {v0}, Lamb;->f(Lamb;)Lamd;

    move-result-object v0

    invoke-interface {v0}, Lamd;->a()V

    goto/16 :goto_1

    :cond_4
    invoke-static {}, Lamb;->d()Ljava/lang/String;

    iget-object v0, p0, Laml;->a:Lamb;

    invoke-static {v0}, Lamb;->a(Lamb;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lamk;

    invoke-interface {v0}, Lamk;->b()V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
