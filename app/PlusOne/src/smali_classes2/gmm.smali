.class public final Lgmm;
.super Lghh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lghh",
        "<",
        "Lgmj;",
        ">;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lggb;Lggc;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lghh;-><init>(Landroid/content/Context;Landroid/os/Looper;Lggb;Lggc;)V

    return-void
.end method


# virtual methods
.method protected synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, Lgmm;->b(Landroid/os/IBinder;)Lgmj;

    move-result-object v0

    return-object v0
.end method

.method a(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;
    .locals 1

    invoke-virtual {p0}, Lgmm;->h()V

    invoke-virtual {p0}, Lgmm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lgmj;

    invoke-interface {v0, p1}, Lgmj;->a(Landroid/accounts/Account;)Lcom/google/android/gms/location/reporting/ReportingState;

    move-result-object v0

    return-object v0
.end method

.method a(Lcom/google/android/gms/location/reporting/UploadRequest;)Lcom/google/android/gms/location/reporting/UploadRequestResult;
    .locals 1

    invoke-virtual {p0}, Lgmm;->h()V

    invoke-virtual {p1}, Lcom/google/android/gms/location/reporting/UploadRequest;->b()Landroid/accounts/Account;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p0}, Lgmm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lgmj;

    invoke-interface {v0, p1}, Lgmj;->a(Lcom/google/android/gms/location/reporting/UploadRequest;)Lcom/google/android/gms/location/reporting/UploadRequestResult;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lgic;Lghl;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const v1, 0x5e3530

    iget-object v2, p0, Lghh;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lgic;->c(Lghz;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method b(Landroid/accounts/Account;)I
    .locals 1

    invoke-virtual {p0}, Lgmm;->h()V

    invoke-virtual {p0}, Lgmm;->i()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lgmj;

    invoke-interface {v0, p1}, Lgmj;->b(Landroid/accounts/Account;)I

    move-result v0

    invoke-static {v0}, Lgsa;->a(I)I

    move-result v0

    return v0
.end method

.method protected b(Landroid/os/IBinder;)Lgmj;
    .locals 1

    invoke-static {p1}, Lgmk;->a(Landroid/os/IBinder;)Lgmj;

    move-result-object v0

    return-object v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.location.reporting.service.START"

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.location.reporting.internal.IReportingService"

    return-object v0
.end method
