.class public abstract Lixo;
.super Landroid/widget/LinearLayout;
.source "PG"


# instance fields
.field public a:Lnqa;

.field public b:Liyf;

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 57
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 48
    return-void
.end method


# virtual methods
.method public a()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 63
    iget v0, p0, Lixo;->c:I

    invoke-virtual {p0, v0}, Lixo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    return-object v0
.end method

.method public a(Lnqa;ILiyf;)V
    .locals 3

    .prologue
    .line 82
    const v0, 0x7f100138

    invoke-virtual {p0, v0}, Lixo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 83
    if-eqz v0, :cond_0

    .line 84
    iput p2, p0, Lixo;->c:I

    .line 85
    iget v1, p0, Lixo;->c:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 86
    add-int/lit8 p2, p2, 0x1

    .line 89
    :cond_0
    const v0, 0x7f10046d

    invoke-virtual {p0, v0}, Lixo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 90
    if-eqz v0, :cond_1

    .line 91
    iput p2, p0, Lixo;->d:I

    .line 92
    iget v1, p0, Lixo;->d:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setId(I)V

    .line 93
    :cond_1
    iput-object p1, p0, Lixo;->a:Lnqa;

    .line 97
    iput-object p3, p0, Lixo;->b:Liyf;

    .line 98
    iget-object v0, p0, Lixo;->a:Lnqa;

    iget-object v0, v0, Lnqa;->c:Lnqe;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lixo;->a:Lnqa;

    iget-object v0, v0, Lnqa;->c:Lnqe;

    iget-object v0, v0, Lnqe;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lixo;->a:Lnqa;

    iget-object v0, v0, Lnqa;->c:Lnqe;

    iget-object v0, v0, Lnqe;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 99
    invoke-virtual {p0}, Lixo;->a()Landroid/widget/TextView;

    move-result-object v0

    instance-of v1, v0, Landroid/widget/TextView;

    if-eqz v1, :cond_2

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lixo;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f090075

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    .line 101
    :cond_2
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 183
    return-void
.end method

.method public b()Landroid/view/View;
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lixo;->d:I

    invoke-virtual {p0, v0}, Lixo;->findViewById(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public c()Lnqa;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lixo;->a:Lnqa;

    return-object v0
.end method

.method public d()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 114
    iget-object v1, p0, Lixo;->a:Lnqa;

    iget-object v1, v1, Lnqa;->c:Lnqe;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lixo;->a:Lnqa;

    iget-object v1, v1, Lnqa;->c:Lnqe;

    iget v1, v1, Lnqe;->c:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    .line 118
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lixo;->a:Lnqa;

    iget-object v1, v1, Lnqa;->c:Lnqe;

    iget-object v1, v1, Lnqe;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lixo;->l()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    invoke-virtual {p0}, Lixo;->h()Lnqc;

    move-result-object v0

    .line 126
    if-eqz v0, :cond_0

    iget-object v0, v0, Lnqc;->a:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Lnpv;
    .locals 1

    .prologue
    .line 133
    invoke-virtual {p0}, Lixo;->h()Lnqc;

    move-result-object v0

    .line 134
    if-eqz v0, :cond_0

    iget-object v0, v0, Lnqc;->c:Lnpv;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Ljava/lang/Boolean;
    .locals 1

    .prologue
    .line 141
    invoke-virtual {p0}, Lixo;->h()Lnqc;

    move-result-object v0

    .line 142
    if-eqz v0, :cond_0

    iget-object v0, v0, Lnqc;->b:Ljava/lang/Boolean;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Lnqc;
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lixo;->a:Lnqa;

    iget-object v0, v0, Lnqa;->c:Lnqe;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lixo;->a:Lnqa;

    iget-object v0, v0, Lnqa;->c:Lnqe;

    iget-object v0, v0, Lnqe;->f:Lnqc;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lixo;->a:Lnqa;

    iget-object v0, v0, Lnqa;->f:Lnqd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lixo;->a:Lnqa;

    iget-object v0, v0, Lnqa;->f:Lnqd;

    iget v0, v0, Lnqd;->a:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lixo;->a:Lnqa;

    iget-object v0, v0, Lnqa;->e:Lnpx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lixo;->a:Lnqa;

    iget-object v0, v0, Lnqa;->e:Lnpx;

    iget v0, v0, Lnpx;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public abstract k()Lnqe;
.end method

.method public abstract l()Z
.end method
