.class public final Lbkb;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbjs;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/media/MediaCodec;

.field private final c:Landroid/media/MediaCodecInfo;

.field private final d:Ljava/lang/String;

.field private e:Z

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lbkb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbkb;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/media/MediaCodec;Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    const-string v0, "codec"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaCodec;

    iput-object v0, p0, Lbkb;->b:Landroid/media/MediaCodec;

    .line 36
    const-string v0, "mimeType"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lbkb;->d:Ljava/lang/String;

    .line 38
    iget-object v0, p0, Lbkb;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getCodecInfo()Landroid/media/MediaCodecInfo;

    move-result-object v0

    iput-object v0, p0, Lbkb;->c:Landroid/media/MediaCodecInfo;

    .line 39
    return-void
.end method

.method private a(Ljava/lang/RuntimeException;)V
    .locals 2

    .prologue
    .line 222
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbkb;->f:Z

    .line 225
    sget-object v0, Lbkb;->a:Ljava/lang/String;

    const-string v1, "Codec threw an exception"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 226
    const-string v0, "Codec threw an exception"

    invoke-static {v0, p1}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/Throwable;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public a(Landroid/media/MediaCodec$BufferInfo;)I
    .locals 4

    .prologue
    .line 134
    :try_start_0
    iget-object v0, p0, Lbkb;->b:Landroid/media/MediaCodec;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, p1, v2, v3}, Landroid/media/MediaCodec;->dequeueOutputBuffer(Landroid/media/MediaCodec$BufferInfo;J)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 135
    :catch_0
    move-exception v0

    .line 136
    invoke-direct {p0, v0}, Lbkb;->a(Ljava/lang/RuntimeException;)V

    .line 137
    throw v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 214
    :try_start_0
    iget-object v0, p0, Lbkb;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->release()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    return-void

    .line 215
    :catch_0
    move-exception v0

    .line 216
    invoke-direct {p0, v0}, Lbkb;->a(Ljava/lang/RuntimeException;)V

    .line 217
    throw v0
.end method

.method public a(IIIJI)V
    .locals 8

    .prologue
    .line 124
    :try_start_0
    iget-object v0, p0, Lbkb;->b:Landroid/media/MediaCodec;

    move v1, p1

    move v2, p2

    move v3, p3

    move-wide v4, p4

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    return-void

    .line 125
    :catch_0
    move-exception v0

    .line 126
    invoke-direct {p0, v0}, Lbkb;->a(Ljava/lang/RuntimeException;)V

    .line 127
    throw v0
.end method

.method public a(IZ)V
    .locals 1

    .prologue
    .line 144
    :try_start_0
    iget-object v0, p0, Lbkb;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0, p1, p2}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 148
    return-void

    .line 145
    :catch_0
    move-exception v0

    .line 146
    invoke-direct {p0, v0}, Lbkb;->a(Ljava/lang/RuntimeException;)V

    .line 147
    throw v0
.end method

.method public a(Landroid/media/MediaFormat;)V
    .locals 4

    .prologue
    .line 73
    :try_start_0
    iget-object v0, p0, Lbkb;->b:Landroid/media/MediaCodec;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    return-void

    .line 74
    :catch_0
    move-exception v0

    .line 75
    invoke-direct {p0, v0}, Lbkb;->a(Ljava/lang/RuntimeException;)V

    .line 76
    throw v0
.end method

.method public a(Landroid/media/MediaFormat;Landroid/view/Surface;)V
    .locals 3

    .prologue
    .line 63
    :try_start_0
    iget-object v0, p0, Lbkb;->b:Landroid/media/MediaCodec;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p1, p2, v1, v2}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    return-void

    .line 64
    :catch_0
    move-exception v0

    .line 65
    invoke-direct {p0, v0}, Lbkb;->a(Ljava/lang/RuntimeException;)V

    .line 66
    throw v0
.end method

.method public b(Landroid/media/MediaFormat;)V
    .locals 4

    .prologue
    .line 83
    :try_start_0
    iget-object v0, p0, Lbkb;->b:Landroid/media/MediaCodec;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, p1, v1, v2, v3}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    return-void

    .line 84
    :catch_0
    move-exception v0

    .line 85
    invoke-direct {p0, v0}, Lbkb;->a(Ljava/lang/RuntimeException;)V

    .line 86
    throw v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lbkb;->e:Z

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lbkb;->f:Z

    return v0
.end method

.method public d()Landroid/view/Surface;
    .locals 1

    .prologue
    .line 93
    :try_start_0
    iget-object v0, p0, Lbkb;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->createInputSurface()Landroid/view/Surface;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 94
    :catch_0
    move-exception v0

    .line 95
    invoke-direct {p0, v0}, Lbkb;->a(Ljava/lang/RuntimeException;)V

    .line 96
    throw v0
.end method

.method public e()V
    .locals 1

    .prologue
    .line 103
    :try_start_0
    iget-object v0, p0, Lbkb;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->start()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    return-void

    .line 104
    :catch_0
    move-exception v0

    .line 105
    invoke-direct {p0, v0}, Lbkb;->a(Ljava/lang/RuntimeException;)V

    .line 106
    throw v0
.end method

.method public f()I
    .locals 4

    .prologue
    .line 113
    :try_start_0
    iget-object v0, p0, Lbkb;->b:Landroid/media/MediaCodec;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 114
    :catch_0
    move-exception v0

    .line 115
    invoke-direct {p0, v0}, Lbkb;->a(Ljava/lang/RuntimeException;)V

    .line 116
    throw v0
.end method

.method public g()[Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 154
    :try_start_0
    iget-object v0, p0, Lbkb;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 155
    :catch_0
    move-exception v0

    .line 156
    invoke-direct {p0, v0}, Lbkb;->a(Ljava/lang/RuntimeException;)V

    .line 157
    throw v0
.end method

.method public h()[Ljava/nio/ByteBuffer;
    .locals 1

    .prologue
    .line 164
    :try_start_0
    iget-object v0, p0, Lbkb;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputBuffers()[Ljava/nio/ByteBuffer;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 165
    :catch_0
    move-exception v0

    .line 166
    invoke-direct {p0, v0}, Lbkb;->a(Ljava/lang/RuntimeException;)V

    .line 167
    throw v0
.end method

.method public i()Landroid/media/MediaFormat;
    .locals 1

    .prologue
    .line 174
    :try_start_0
    iget-object v0, p0, Lbkb;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 175
    :catch_0
    move-exception v0

    .line 176
    invoke-direct {p0, v0}, Lbkb;->a(Ljava/lang/RuntimeException;)V

    .line 177
    throw v0
.end method

.method public j()V
    .locals 1

    .prologue
    .line 184
    :try_start_0
    iget-object v0, p0, Lbkb;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->signalEndOfInputStream()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    return-void

    .line 185
    :catch_0
    move-exception v0

    .line 186
    invoke-direct {p0, v0}, Lbkb;->a(Ljava/lang/RuntimeException;)V

    .line 187
    throw v0
.end method

.method public k()V
    .locals 1

    .prologue
    .line 194
    :try_start_0
    iget-object v0, p0, Lbkb;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->flush()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    return-void

    .line 195
    :catch_0
    move-exception v0

    .line 196
    invoke-direct {p0, v0}, Lbkb;->a(Ljava/lang/RuntimeException;)V

    .line 197
    throw v0
.end method

.method public l()V
    .locals 1

    .prologue
    .line 204
    :try_start_0
    iget-object v0, p0, Lbkb;->b:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->stop()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    return-void

    .line 205
    :catch_0
    move-exception v0

    .line 206
    invoke-direct {p0, v0}, Lbkb;->a(Ljava/lang/RuntimeException;)V

    .line 207
    throw v0
.end method

.method public m()Z
    .locals 2

    .prologue
    .line 43
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 44
    iget-object v0, p0, Lbkb;->c:Landroid/media/MediaCodecInfo;

    iget-object v1, p0, Lbkb;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/media/MediaCodecInfo;->getCapabilitiesForType(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;

    move-result-object v0

    const-string v1, "adaptive-playback"

    invoke-virtual {v0, v1}, Landroid/media/MediaCodecInfo$CodecCapabilities;->isFeatureSupported(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lbkb;->e:Z

    iget-boolean v0, p0, Lbkb;->e:Z

    .line 46
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
