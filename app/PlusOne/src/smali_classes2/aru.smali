.class public final Laru;
.super Lamn;
.source "PG"


# instance fields
.field final a:Lawl;

.field final b:Lbze;

.field final c:Lcdu;

.field final d:Lasa;

.field final e:Lapl;

.field final f:Lasd;

.field final g:Lasn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lasn",
            "<",
            "Lasc;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Larz;

.field private final j:Lbqo;

.field private final k:Ljava/util/concurrent/Executor;

.field private final l:Lawk;

.field private final m:Lawk;

.field private final n:Lawk;


# direct methods
.method public constructor <init>(Lbqo;Ljava/util/concurrent/Executor;Lapl;Lcdu;Lawl;Larz;Lbze;Lasa;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 249
    invoke-direct {p0}, Lamn;-><init>()V

    .line 191
    new-instance v0, Lasb;

    invoke-direct {v0, p0}, Lasb;-><init>(Laru;)V

    iput-object v0, p0, Laru;->f:Lasd;

    .line 192
    new-instance v0, Larv;

    const-class v1, Lasc;

    invoke-direct {v0, p0, v1}, Larv;-><init>(Laru;Ljava/lang/Class;)V

    iput-object v0, p0, Laru;->g:Lasn;

    .line 204
    new-instance v0, Larw;

    invoke-direct {v0, p0}, Larw;-><init>(Laru;)V

    iput-object v0, p0, Laru;->l:Lawk;

    .line 216
    new-instance v0, Larx;

    invoke-direct {v0, p0}, Larx;-><init>(Laru;)V

    iput-object v0, p0, Laru;->m:Lawk;

    .line 223
    new-instance v0, Lary;

    invoke-direct {v0, p0}, Lary;-><init>(Laru;)V

    iput-object v0, p0, Laru;->n:Lawk;

    .line 250
    const-string v0, "posterStore"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqo;

    iput-object v0, p0, Laru;->j:Lbqo;

    .line 251
    const-string v0, "posterExecutor"

    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Laru;->k:Ljava/util/concurrent/Executor;

    .line 252
    const-string v0, "mediaUriFetcher"

    invoke-static {p3, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lapl;

    iput-object v0, p0, Laru;->e:Lapl;

    .line 253
    const-string v0, "analyticsSession"

    invoke-static {p4, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    iput-object v0, p0, Laru;->c:Lcdu;

    .line 254
    const-string v0, "state"

    invoke-static {p5, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawl;

    iput-object v0, p0, Laru;->a:Lawl;

    .line 255
    const-string v0, "display"

    invoke-static {p6, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Larz;

    iput-object v0, p0, Laru;->h:Larz;

    .line 256
    const-string v0, "videoSplitter"

    invoke-static {p7, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbze;

    iput-object v0, p0, Laru;->b:Lbze;

    .line 257
    const-string v0, "listener"

    invoke-static {p8, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lasa;

    iput-object v0, p0, Laru;->d:Lasa;

    .line 260
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 286
    invoke-super {p0}, Lamn;->a()V

    .line 288
    iget-object v0, p0, Laru;->g:Lasn;

    invoke-virtual {v0}, Lasn;->a()V

    .line 290
    iget-object v0, p0, Laru;->a:Lawl;

    iget-object v1, p0, Laru;->l:Lawk;

    invoke-interface {v0, v1}, Lawl;->i(Lawk;)V

    .line 291
    iget-object v0, p0, Laru;->a:Lawl;

    iget-object v1, p0, Laru;->m:Lawk;

    invoke-interface {v0, v1}, Lawl;->t(Lawk;)V

    .line 292
    iget-object v0, p0, Laru;->a:Lawl;

    iget-object v1, p0, Laru;->n:Lawk;

    invoke-interface {v0, v1}, Lawl;->J(Lawk;)V

    .line 293
    return-void
.end method

.method public a(I)V
    .locals 1

    .prologue
    .line 313
    iget-object v0, p0, Laru;->g:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Lasc;

    invoke-interface {v0, p1}, Lasc;->b(I)V

    .line 314
    return-void
.end method

.method public a(Lasc;)V
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Laru;->g:Lasn;

    invoke-virtual {v0, p1}, Lasn;->c(Ljava/lang/Object;)V

    .line 264
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 298
    iget-object v0, p0, Laru;->a:Lawl;

    iget-object v1, p0, Laru;->l:Lawk;

    invoke-interface {v0, v1}, Lawl;->j(Lawk;)V

    .line 299
    iget-object v0, p0, Laru;->a:Lawl;

    iget-object v1, p0, Laru;->m:Lawk;

    invoke-interface {v0, v1}, Lawl;->u(Lawk;)V

    .line 300
    iget-object v0, p0, Laru;->a:Lawl;

    iget-object v1, p0, Laru;->n:Lawk;

    invoke-interface {v0, v1}, Lawl;->K(Lawk;)V

    .line 301
    iget-object v0, p0, Laru;->g:Lasn;

    invoke-virtual {v0}, Lasn;->b()V

    .line 302
    invoke-super {p0}, Lamn;->b()V

    .line 303
    return-void
.end method

.method public b(Lasc;)V
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Laru;->g:Lasn;

    invoke-virtual {v0, p1}, Lasn;->d(Ljava/lang/Object;)V

    .line 268
    return-void
.end method

.method c()V
    .locals 1

    .prologue
    .line 341
    iget-object v0, p0, Laru;->h:Larz;

    invoke-interface {v0}, Larz;->r()V

    .line 342
    return-void
.end method

.method c(Lasc;)V
    .locals 4

    .prologue
    .line 320
    iget-object v0, p0, Laru;->a:Lawl;

    invoke-interface {v0}, Lawl;->F()Lboi;

    move-result-object v2

    .line 322
    if-eqz v2, :cond_1

    .line 323
    invoke-virtual {v2}, Lboi;->f()Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    .line 326
    :goto_0
    if-eqz v2, :cond_2

    .line 327
    invoke-virtual {v2}, Lboi;->g()Ljava/util/Map;

    move-result-object v0

    .line 330
    :goto_1
    iget-object v2, p0, Laru;->j:Lbqo;

    iget-object v3, p0, Laru;->k:Ljava/util/concurrent/Executor;

    invoke-interface {p1, v2, v3, v1, v0}, Lasc;->a(Lbqo;Ljava/util/concurrent/Executor;Ljava/util/List;Ljava/util/Map;)V

    .line 331
    iget-object v0, p0, Laru;->a:Lawl;

    invoke-interface {v0}, Lawl;->P()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 332
    iget-object v0, p0, Laru;->a:Lawl;

    invoke-interface {v0}, Lawl;->Q()I

    move-result v0

    invoke-interface {p1, v0}, Lasc;->d_(I)V

    .line 334
    :cond_0
    return-void

    .line 324
    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 328
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    goto :goto_1
.end method

.method d(Lasc;)V
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Laru;->a:Lawl;

    invoke-interface {v0}, Lawl;->A()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lasc;->a(Ljava/lang/String;)V

    .line 338
    return-void
.end method

.method e(Lasc;)V
    .locals 2

    .prologue
    .line 345
    iget-object v0, p0, Laru;->a:Lawl;

    invoke-interface {v0}, Lawl;->bd()Lawg;

    move-result-object v0

    sget-object v1, Lawg;->c:Lawg;

    if-ne v0, v1, :cond_0

    .line 347
    invoke-interface {p1}, Lasc;->w_()V

    .line 351
    :goto_0
    return-void

    .line 349
    :cond_0
    invoke-interface {p1}, Lasc;->b()V

    goto :goto_0
.end method
