.class public final Lbgz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:I

.field public final d:I


# direct methods
.method public constructor <init>(II)V
    .locals 11

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    new-array v0, v2, [I

    .line 106
    const v1, 0x8b87

    invoke-static {p1, v1, v0, v4}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    .line 108
    new-array v7, v2, [I

    .line 109
    new-array v5, v2, [I

    .line 110
    aget v1, v0, v4

    new-array v9, v1, [B

    .line 111
    new-array v3, v2, [I

    .line 113
    aget v2, v0, v4

    move v0, p1

    move v1, p2

    move v6, v4

    move v8, v4

    move v10, v4

    invoke-static/range {v0 .. v10}, Landroid/opengl/GLES20;->glGetActiveUniform(III[II[II[II[BI)V

    .line 114
    new-instance v0, Ljava/lang/String;

    invoke-static {v9}, Lbgx;->a([B)I

    move-result v1

    invoke-direct {v0, v9, v4, v1}, Ljava/lang/String;-><init>([BII)V

    iput-object v0, p0, Lbgz;->a:Ljava/lang/String;

    .line 115
    iget-object v0, p0, Lbgz;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lbgz;->b:I

    .line 116
    aget v0, v7, v4

    iput v0, p0, Lbgz;->c:I

    .line 117
    aget v0, v5, v4

    iput v0, p0, Lbgz;->d:I

    .line 118
    const-string v0, "Initializing uniform"

    invoke-static {v0}, Lbgc;->a(Ljava/lang/String;)V

    .line 119
    return-void
.end method
