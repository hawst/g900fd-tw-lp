.class final Laqe;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laqw;


# instance fields
.field private synthetic a:Laqc;


# direct methods
.method constructor <init>(Laqc;)V
    .locals 0

    .prologue
    .line 394
    iput-object p1, p0, Laqe;->a:Laqc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 406
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->b(Laqc;)Lawf;

    move-result-object v0

    invoke-interface {v0, v2}, Lawf;->g(Z)V

    .line 409
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->d(Laqc;)Laqx;

    move-result-object v0

    const/16 v1, 0x1e

    invoke-virtual {v0, v1}, Laqx;->b(I)V

    .line 410
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->e(Laqc;)V

    .line 411
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->f(Laqc;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    invoke-interface {v0, v2, v2}, Laqv;->a(ZI)V

    .line 412
    return-void
.end method

.method public a(F)V
    .locals 3

    .prologue
    .line 434
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->b(Laqc;)Lawf;

    move-result-object v0

    invoke-interface {v0}, Lawf;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->b(Laqc;)Lawf;

    move-result-object v0

    invoke-interface {v0}, Lawf;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 435
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->b(Laqc;)Lawf;

    move-result-object v0

    invoke-interface {v0}, Lawf;->F()Lboi;

    move-result-object v0

    invoke-virtual {v0}, Lboi;->n()J

    move-result-wide v0

    long-to-float v0, v0

    mul-float/2addr v0, p1

    float-to-long v0, v0

    .line 436
    iget-object v2, p0, Laqe;->a:Laqc;

    invoke-static {v2, v0, v1}, Laqc;->a(Laqc;J)V

    .line 437
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->h(Laqc;)Lbtp;

    move-result-object v0

    invoke-interface {v0}, Lbtp;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 438
    iget-object v0, p0, Laqe;->a:Laqc;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Laqc;->b(Laqc;Z)Z

    .line 439
    invoke-virtual {p0}, Laqe;->b()V

    .line 442
    :cond_0
    return-void
.end method

.method public a(Lbza;)V
    .locals 2

    .prologue
    .line 398
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->a(Laqc;)Laxp;

    move-result-object v0

    sget-object v1, Laxq;->e:Laxq;

    invoke-virtual {v0, v1}, Laxp;->a(Laxq;)V

    .line 399
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->b(Laqc;)Lawf;

    move-result-object v0

    invoke-interface {v0, p1}, Lawf;->a(Lbza;)V

    .line 400
    iget-object v0, p0, Laqe;->a:Laqc;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Laqc;->a(Laqc;Z)Z

    .line 401
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->c(Laqc;)Lcdu;

    move-result-object v0

    invoke-virtual {v0}, Lcdu;->j()V

    .line 402
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 422
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 423
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->a(Laqc;)Laxp;

    move-result-object v0

    sget-object v1, Laxq;->d:Laxq;

    invoke-virtual {v0, v1}, Laxp;->a(Laxq;)V

    .line 424
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->c(Laqc;)Lcdu;

    move-result-object v0

    invoke-virtual {v0}, Lcdu;->k()V

    .line 426
    :cond_0
    iget-object v0, p0, Laqe;->a:Laqc;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Laqc;->a(Laqc;Z)Z

    .line 427
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->b(Laqc;)Lawf;

    move-result-object v0

    invoke-interface {v0, p1}, Lawf;->a(Ljava/lang/String;)V

    .line 428
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->g(Laqc;)V

    .line 417
    return-void
.end method

.method public c()Z
    .locals 2

    .prologue
    .line 446
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->b(Laqc;)Lawf;

    move-result-object v0

    invoke-interface {v0}, Lawf;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laqe;->a:Laqc;

    .line 447
    invoke-static {v0}, Laqc;->d(Laqc;)Laqx;

    move-result-object v0

    invoke-virtual {v0}, Laqx;->b()I

    move-result v0

    const/16 v1, 0x1e

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 452
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->b(Laqc;)Lawf;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lawf;->g(Z)V

    .line 453
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->d(Laqc;)Laqx;

    move-result-object v0

    iget-object v1, p0, Laqe;->a:Laqc;

    .line 454
    invoke-static {v1}, Laqc;->d(Laqc;)Laqx;

    move-result-object v1

    invoke-virtual {v1}, Laqx;->b()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    .line 453
    invoke-virtual {v0, v1}, Laqx;->b(I)V

    .line 455
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->i(Laqc;)Laqm;

    move-result-object v0

    invoke-interface {v0}, Laqm;->a()V

    .line 460
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 464
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->e(Laqc;)V

    .line 465
    return-void
.end method

.method public g()V
    .locals 2

    .prologue
    .line 469
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->a(Laqc;)Laxp;

    move-result-object v0

    sget-object v1, Laxq;->f:Laxq;

    invoke-virtual {v0, v1}, Laxp;->a(Laxq;)V

    .line 470
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->i(Laqc;)Laqm;

    move-result-object v0

    invoke-interface {v0}, Laqm;->c()V

    .line 471
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->h(Laqc;)Lbtp;

    move-result-object v0

    invoke-interface {v0}, Lbtp;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 473
    invoke-virtual {p0}, Laqe;->b()V

    .line 475
    :cond_0
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->h(Laqc;)Lbtp;

    move-result-object v0

    invoke-interface {v0}, Lbtp;->d()V

    .line 480
    return-void
.end method

.method public i()V
    .locals 3

    .prologue
    .line 484
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->f(Laqc;)Lasn;

    move-result-object v0

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laqv;

    .line 486
    iget-object v1, p0, Laqe;->a:Laqc;

    invoke-static {v1}, Laqc;->j(Laqc;)Ljava/util/Queue;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    .line 487
    iget-object v1, p0, Laqe;->a:Laqc;

    invoke-static {v1}, Laqc;->j(Laqc;)Ljava/util/Queue;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Queue;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 490
    iget-object v1, p0, Laqe;->a:Laqc;

    invoke-static {v1}, Laqc;->j(Laqc;)Ljava/util/Queue;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-interface {v0, v1}, Laqv;->a(Landroid/graphics/Bitmap;)V

    .line 496
    :cond_0
    :goto_0
    return-void

    .line 491
    :cond_1
    iget-object v1, p0, Laqe;->a:Laqc;

    invoke-static {v1}, Laqc;->k(Laqc;)Laql;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Laqe;->a:Laqc;

    .line 492
    invoke-static {v1}, Laqc;->l(Laqc;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 493
    iget-object v1, p0, Laqe;->a:Laqc;

    invoke-static {v1}, Laqc;->l(Laqc;)Landroid/graphics/Bitmap;

    move-result-object v1

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Laqv;->a(Landroid/graphics/Bitmap;I)V

    .line 494
    iget-object v0, p0, Laqe;->a:Laqc;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Laqc;->a(Laqc;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public j()V
    .locals 1

    .prologue
    .line 500
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->m(Laqc;)V

    .line 501
    return-void
.end method

.method public k()V
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->n(Laqc;)V

    .line 506
    return-void
.end method

.method public l()V
    .locals 2

    .prologue
    .line 510
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->b(Laqc;)Lawf;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lawf;->r(Z)V

    .line 511
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->o(Laqc;)V

    .line 512
    return-void
.end method

.method public m()V
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->b(Laqc;)Lawf;

    move-result-object v0

    invoke-interface {v0}, Lawf;->aW()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 519
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->o(Laqc;)V

    .line 521
    :cond_0
    return-void
.end method

.method public n()Z
    .locals 1

    .prologue
    .line 525
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->b(Laqc;)Lawf;

    move-result-object v0

    invoke-interface {v0}, Lawf;->bi()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 526
    iget-object v0, p0, Laqe;->a:Laqc;

    invoke-static {v0}, Laqc;->p(Laqc;)Laqt;

    move-result-object v0

    invoke-interface {v0}, Laqt;->n()V

    .line 527
    const/4 v0, 0x1

    .line 529
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
