.class public Laxb;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Lavy;

.field private final c:Lawx;

.field private d:Lbue;

.field private e:Ljava/lang/String;

.field private f:Ljed;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Laxb;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Laxb;->a:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Lavy;Lawx;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 52
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 44
    iput-object v0, p0, Laxb;->d:Lbue;

    .line 46
    iput-object v0, p0, Laxb;->e:Ljava/lang/String;

    .line 48
    iput-object v0, p0, Laxb;->f:Ljed;

    .line 53
    iput-object p1, p0, Laxb;->b:Lavy;

    .line 54
    iput-object p2, p0, Laxb;->c:Lawx;

    .line 55
    return-void
.end method


# virtual methods
.method protected varargs a()Ljava/lang/Void;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 67
    :try_start_0
    invoke-virtual {p0}, Laxb;->isCancelled()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 68
    iput-object v3, p0, Laxb;->d:Lbue;

    .line 83
    iput-object v3, p0, Laxb;->f:Ljed;

    .line 85
    :goto_0
    return-object v3

    .line 70
    :cond_0
    :try_start_1
    iget-object v0, p0, Laxb;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 71
    iget-object v0, p0, Laxb;->c:Lawx;

    iget-object v1, p0, Laxb;->e:Ljava/lang/String;

    iget-object v2, p0, Laxb;->d:Lbue;

    invoke-interface {v0, v1, v2}, Lawx;->a(Ljava/lang/String;Lbue;)V

    .line 73
    :cond_1
    invoke-virtual {p0}, Laxb;->isCancelled()Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 74
    iput-object v3, p0, Laxb;->d:Lbue;

    .line 83
    iput-object v3, p0, Laxb;->f:Ljed;

    goto :goto_0

    .line 76
    :cond_2
    :try_start_2
    iget-object v0, p0, Laxb;->f:Ljed;

    if-eqz v0, :cond_3

    .line 77
    iget-object v0, p0, Laxb;->c:Lawx;

    iget-object v1, p0, Laxb;->f:Ljed;

    iget-object v2, p0, Laxb;->d:Lbue;

    invoke-interface {v0, v1, v2}, Lawx;->a(Ljed;Lbue;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 82
    :cond_3
    iput-object v3, p0, Laxb;->d:Lbue;

    .line 83
    iput-object v3, p0, Laxb;->f:Ljed;

    goto :goto_0

    .line 80
    :catch_0
    move-exception v0

    :try_start_3
    sget-object v0, Laxb;->a:Ljava/lang/String;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 82
    iput-object v3, p0, Laxb;->d:Lbue;

    .line 83
    iput-object v3, p0, Laxb;->f:Ljed;

    goto :goto_0

    .line 82
    :catchall_0
    move-exception v0

    iput-object v3, p0, Laxb;->d:Lbue;

    .line 83
    iput-object v3, p0, Laxb;->f:Ljed;

    throw v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Laxb;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected onPreExecute()V
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Laxb;->b:Lavy;

    invoke-interface {v0}, Lavy;->aP()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Laxb;->e:Ljava/lang/String;

    .line 60
    iget-object v0, p0, Laxb;->b:Lavy;

    invoke-interface {v0}, Lavy;->aO()Ljed;

    move-result-object v0

    iput-object v0, p0, Laxb;->f:Ljed;

    .line 61
    iget-object v0, p0, Laxb;->b:Lavy;

    invoke-interface {v0}, Lavy;->a()Lbue;

    move-result-object v0

    iput-object v0, p0, Laxb;->d:Lbue;

    .line 62
    return-void
.end method
