.class public final Lbie;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:[B

.field private static final b:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lbie;->a:[B

    .line 23
    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lbie;->b:[I

    return-void

    .line 20
    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data

    .line 23
    :array_1
    .array-data 4
        0x17700
        0x15888
        0xfa00
        0xbb80
        0xac44
        0x7d00
        0x5dc0
        0x5622
        0x3e80
        0x2ee0
        0x2b11
        0x1f40
        0x1cb6
    .end array-data
.end method

.method public static a([B)Landroid/util/Pair;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([B)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 36
    aget-byte v0, p0, v1

    shr-int/lit8 v0, v0, 0x3

    and-int/lit8 v0, v0, 0x1f

    .line 37
    const/4 v2, 0x5

    if-eq v0, v2, :cond_0

    const/16 v2, 0x1d

    if-ne v0, v2, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 38
    :goto_0
    aget-byte v2, p0, v0

    and-int/lit8 v2, v2, 0x7

    shl-int/lit8 v2, v2, 0x1

    add-int/lit8 v3, v0, 0x1

    aget-byte v3, p0, v3

    shr-int/lit8 v3, v3, 0x7

    and-int/lit8 v3, v3, 0x1

    or-int/2addr v2, v3

    .line 40
    const-string v3, "frequencyIndex"

    sget-object v4, Lbie;->b:[I

    const/16 v4, 0xc

    const/4 v5, 0x0

    invoke-static {v2, v3, v1, v4, v5}, Lcec;->a(ILjava/lang/CharSequence;IILjava/lang/CharSequence;)I

    .line 42
    sget-object v1, Lbie;->b:[I

    aget v1, v1, v2

    .line 43
    add-int/lit8 v0, v0, 0x1

    aget-byte v0, p0, v0

    shr-int/lit8 v0, v0, 0x3

    and-int/lit8 v0, v0, 0xf

    .line 44
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    :cond_1
    move v0, v1

    .line 37
    goto :goto_0
.end method

.method public static a([BII)[B
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 56
    sget-object v0, Lbie;->a:[B

    add-int/lit8 v0, p2, 0x4

    new-array v0, v0, [B

    .line 57
    sget-object v1, Lbie;->a:[B

    sget-object v2, Lbie;->a:[B

    invoke-static {v1, v3, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 58
    sget-object v1, Lbie;->a:[B

    invoke-static {p0, p1, v0, v4, p2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 59
    return-object v0
.end method
