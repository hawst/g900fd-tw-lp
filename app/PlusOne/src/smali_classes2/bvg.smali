.class public final Lbvg;
.super Loxu;
.source "PG"


# static fields
.field public static final a:[Lbvg;


# instance fields
.field public A:I

.field public B:Lbvk;

.field public C:Lbvl;

.field public D:Lbvl;

.field public E:Lbvm;

.field private F:F

.field private G:F

.field private H:F

.field private I:[Lbva;

.field private J:I

.field private K:Lbvj;

.field public b:J

.field public c:Lbvn;

.field public d:F

.field public e:F

.field public f:Lbvj;

.field public g:F

.field public h:F

.field public i:F

.field public j:F

.field public k:Lbvj;

.field public l:F

.field public m:F

.field public n:F

.field public o:Lbvi;

.field public p:F

.field public q:Lbvj;

.field public r:F

.field public s:Lbvj;

.field public t:F

.field public u:F

.field public v:I

.field public w:Lbvh;

.field public x:Lbvj;

.field public y:Lbvj;

.field public z:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 490
    const/4 v0, 0x0

    new-array v0, v0, [Lbvg;

    sput-object v0, Lbvg;->a:[Lbvg;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x0

    const/high16 v2, 0x7fc00000    # NaNf

    .line 491
    invoke-direct {p0}, Loxu;-><init>()V

    .line 1206
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbvg;->b:J

    .line 1209
    iput-object v3, p0, Lbvg;->c:Lbvn;

    .line 1212
    iput v2, p0, Lbvg;->d:F

    .line 1215
    iput v2, p0, Lbvg;->F:F

    .line 1218
    iput v2, p0, Lbvg;->G:F

    .line 1221
    iput v2, p0, Lbvg;->e:F

    .line 1224
    iput v2, p0, Lbvg;->H:F

    .line 1227
    iput-object v3, p0, Lbvg;->f:Lbvj;

    .line 1230
    iput v2, p0, Lbvg;->g:F

    .line 1233
    iput v2, p0, Lbvg;->h:F

    .line 1236
    iput v2, p0, Lbvg;->i:F

    .line 1239
    iput v2, p0, Lbvg;->j:F

    .line 1242
    sget-object v0, Lbva;->a:[Lbva;

    iput-object v0, p0, Lbvg;->I:[Lbva;

    .line 1245
    iput-object v3, p0, Lbvg;->k:Lbvj;

    .line 1248
    iput v4, p0, Lbvg;->J:I

    .line 1251
    iput v2, p0, Lbvg;->l:F

    .line 1254
    iput v2, p0, Lbvg;->m:F

    .line 1257
    iput v2, p0, Lbvg;->n:F

    .line 1260
    iput-object v3, p0, Lbvg;->K:Lbvj;

    .line 1263
    iput-object v3, p0, Lbvg;->o:Lbvi;

    .line 1266
    iput v2, p0, Lbvg;->p:F

    .line 1269
    iput-object v3, p0, Lbvg;->q:Lbvj;

    .line 1272
    iput v2, p0, Lbvg;->r:F

    .line 1275
    iput-object v3, p0, Lbvg;->s:Lbvj;

    .line 1278
    iput v2, p0, Lbvg;->t:F

    .line 1281
    iput v2, p0, Lbvg;->u:F

    .line 1284
    iput v4, p0, Lbvg;->v:I

    .line 1287
    iput-object v3, p0, Lbvg;->w:Lbvh;

    .line 1290
    iput-object v3, p0, Lbvg;->x:Lbvj;

    .line 1293
    iput-object v3, p0, Lbvg;->y:Lbvj;

    .line 1296
    iput v2, p0, Lbvg;->z:F

    .line 1299
    iput v4, p0, Lbvg;->A:I

    .line 1302
    iput-object v3, p0, Lbvg;->B:Lbvk;

    .line 1305
    iput-object v3, p0, Lbvg;->C:Lbvl;

    .line 1308
    iput-object v3, p0, Lbvg;->D:Lbvl;

    .line 1311
    iput-object v3, p0, Lbvg;->E:Lbvm;

    .line 491
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 1471
    const/4 v0, 0x1

    iget-wide v2, p0, Lbvg;->b:J

    .line 1473
    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1474
    iget-object v1, p0, Lbvg;->c:Lbvn;

    if-eqz v1, :cond_0

    .line 1475
    const/4 v1, 0x2

    iget-object v2, p0, Lbvg;->c:Lbvn;

    .line 1476
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1478
    :cond_0
    iget v1, p0, Lbvg;->d:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1479
    const/4 v1, 0x3

    iget v2, p0, Lbvg;->d:F

    .line 1480
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1482
    :cond_1
    iget v1, p0, Lbvg;->F:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1483
    const/4 v1, 0x4

    iget v2, p0, Lbvg;->F:F

    .line 1484
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1486
    :cond_2
    iget v1, p0, Lbvg;->G:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1487
    const/4 v1, 0x5

    iget v2, p0, Lbvg;->G:F

    .line 1488
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1490
    :cond_3
    iget v1, p0, Lbvg;->e:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1491
    const/4 v1, 0x6

    iget v2, p0, Lbvg;->e:F

    .line 1492
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1494
    :cond_4
    iget v1, p0, Lbvg;->H:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1495
    const/4 v1, 0x7

    iget v2, p0, Lbvg;->H:F

    .line 1496
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1498
    :cond_5
    iget-object v1, p0, Lbvg;->f:Lbvj;

    if-eqz v1, :cond_6

    .line 1499
    const/16 v1, 0x8

    iget-object v2, p0, Lbvg;->f:Lbvj;

    .line 1500
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1502
    :cond_6
    iget v1, p0, Lbvg;->g:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1503
    const/16 v1, 0x9

    iget v2, p0, Lbvg;->g:F

    .line 1504
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1506
    :cond_7
    iget v1, p0, Lbvg;->h:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_8

    .line 1507
    const/16 v1, 0xa

    iget v2, p0, Lbvg;->h:F

    .line 1508
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1510
    :cond_8
    iget v1, p0, Lbvg;->i:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1511
    const/16 v1, 0xb

    iget v2, p0, Lbvg;->i:F

    .line 1512
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1514
    :cond_9
    iget v1, p0, Lbvg;->j:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_a

    .line 1515
    const/16 v1, 0xc

    iget v2, p0, Lbvg;->j:F

    .line 1516
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1518
    :cond_a
    iget-object v1, p0, Lbvg;->I:[Lbva;

    if-eqz v1, :cond_c

    .line 1519
    iget-object v2, p0, Lbvg;->I:[Lbva;

    array-length v3, v2

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v3, :cond_c

    aget-object v4, v2, v1

    .line 1520
    if-eqz v4, :cond_b

    .line 1521
    const/16 v5, 0xd

    .line 1522
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1519
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1526
    :cond_c
    iget-object v1, p0, Lbvg;->k:Lbvj;

    if-eqz v1, :cond_d

    .line 1527
    const/16 v1, 0xe

    iget-object v2, p0, Lbvg;->k:Lbvj;

    .line 1528
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1530
    :cond_d
    iget v1, p0, Lbvg;->J:I

    if-eq v1, v6, :cond_e

    .line 1531
    const/16 v1, 0xf

    iget v2, p0, Lbvg;->J:I

    .line 1532
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1534
    :cond_e
    iget v1, p0, Lbvg;->l:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_f

    .line 1535
    const/16 v1, 0x10

    iget v2, p0, Lbvg;->l:F

    .line 1536
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1538
    :cond_f
    iget v1, p0, Lbvg;->m:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_10

    .line 1539
    const/16 v1, 0x11

    iget v2, p0, Lbvg;->m:F

    .line 1540
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1542
    :cond_10
    iget v1, p0, Lbvg;->n:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_11

    .line 1543
    const/16 v1, 0x12

    iget v2, p0, Lbvg;->n:F

    .line 1544
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1546
    :cond_11
    iget-object v1, p0, Lbvg;->K:Lbvj;

    if-eqz v1, :cond_12

    .line 1547
    const/16 v1, 0x13

    iget-object v2, p0, Lbvg;->K:Lbvj;

    .line 1548
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1550
    :cond_12
    iget-object v1, p0, Lbvg;->o:Lbvi;

    if-eqz v1, :cond_13

    .line 1551
    const/16 v1, 0x14

    iget-object v2, p0, Lbvg;->o:Lbvi;

    .line 1552
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1554
    :cond_13
    iget v1, p0, Lbvg;->p:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_14

    .line 1555
    const/16 v1, 0x15

    iget v2, p0, Lbvg;->p:F

    .line 1556
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1558
    :cond_14
    iget-object v1, p0, Lbvg;->q:Lbvj;

    if-eqz v1, :cond_15

    .line 1559
    const/16 v1, 0x16

    iget-object v2, p0, Lbvg;->q:Lbvj;

    .line 1560
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1562
    :cond_15
    iget v1, p0, Lbvg;->r:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_16

    .line 1563
    const/16 v1, 0x17

    iget v2, p0, Lbvg;->r:F

    .line 1564
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1566
    :cond_16
    iget-object v1, p0, Lbvg;->s:Lbvj;

    if-eqz v1, :cond_17

    .line 1567
    const/16 v1, 0x18

    iget-object v2, p0, Lbvg;->s:Lbvj;

    .line 1568
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1570
    :cond_17
    iget v1, p0, Lbvg;->t:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_18

    .line 1571
    const/16 v1, 0x19

    iget v2, p0, Lbvg;->t:F

    .line 1572
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1574
    :cond_18
    iget v1, p0, Lbvg;->u:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_19

    .line 1575
    const/16 v1, 0x1a

    iget v2, p0, Lbvg;->u:F

    .line 1576
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1578
    :cond_19
    iget v1, p0, Lbvg;->v:I

    if-eq v1, v6, :cond_1a

    .line 1579
    const/16 v1, 0x1b

    iget v2, p0, Lbvg;->v:I

    .line 1580
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1582
    :cond_1a
    iget-object v1, p0, Lbvg;->w:Lbvh;

    if-eqz v1, :cond_1b

    .line 1583
    const/16 v1, 0x1c

    iget-object v2, p0, Lbvg;->w:Lbvh;

    .line 1584
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1586
    :cond_1b
    iget-object v1, p0, Lbvg;->x:Lbvj;

    if-eqz v1, :cond_1c

    .line 1587
    const/16 v1, 0x1d

    iget-object v2, p0, Lbvg;->x:Lbvj;

    .line 1588
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1590
    :cond_1c
    iget-object v1, p0, Lbvg;->y:Lbvj;

    if-eqz v1, :cond_1d

    .line 1591
    const/16 v1, 0x1e

    iget-object v2, p0, Lbvg;->y:Lbvj;

    .line 1592
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1594
    :cond_1d
    iget v1, p0, Lbvg;->z:F

    invoke-static {v1}, Ljava/lang/Float;->isNaN(F)Z

    move-result v1

    if-nez v1, :cond_1e

    .line 1595
    const/16 v1, 0x1f

    iget v2, p0, Lbvg;->z:F

    .line 1596
    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 1598
    :cond_1e
    iget v1, p0, Lbvg;->A:I

    if-eq v1, v6, :cond_1f

    .line 1599
    const/16 v1, 0x20

    iget v2, p0, Lbvg;->A:I

    .line 1600
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1602
    :cond_1f
    iget-object v1, p0, Lbvg;->B:Lbvk;

    if-eqz v1, :cond_20

    .line 1603
    const/16 v1, 0x21

    iget-object v2, p0, Lbvg;->B:Lbvk;

    .line 1604
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1606
    :cond_20
    iget-object v1, p0, Lbvg;->C:Lbvl;

    if-eqz v1, :cond_21

    .line 1607
    const/16 v1, 0x22

    iget-object v2, p0, Lbvg;->C:Lbvl;

    .line 1608
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1610
    :cond_21
    iget-object v1, p0, Lbvg;->D:Lbvl;

    if-eqz v1, :cond_22

    .line 1611
    const/16 v1, 0x23

    iget-object v2, p0, Lbvg;->D:Lbvl;

    .line 1612
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1614
    :cond_22
    iget-object v1, p0, Lbvg;->E:Lbvm;

    if-eqz v1, :cond_23

    .line 1615
    const/16 v1, 0x24

    iget-object v2, p0, Lbvg;->E:Lbvm;

    .line 1616
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1618
    :cond_23
    iput v0, p0, Lbvg;->ai:I

    .line 1619
    return v0
.end method

.method public a(Loxn;)Lbvg;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1627
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1628
    sparse-switch v0, :sswitch_data_0

    .line 1632
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1633
    :sswitch_0
    return-object p0

    .line 1638
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v2

    iput-wide v2, p0, Lbvg;->b:J

    goto :goto_0

    .line 1642
    :sswitch_2
    iget-object v0, p0, Lbvg;->c:Lbvn;

    if-nez v0, :cond_1

    .line 1643
    new-instance v0, Lbvn;

    invoke-direct {v0}, Lbvn;-><init>()V

    iput-object v0, p0, Lbvg;->c:Lbvn;

    .line 1645
    :cond_1
    iget-object v0, p0, Lbvg;->c:Lbvn;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1649
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbvg;->d:F

    goto :goto_0

    .line 1653
    :sswitch_4
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbvg;->F:F

    goto :goto_0

    .line 1657
    :sswitch_5
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbvg;->G:F

    goto :goto_0

    .line 1661
    :sswitch_6
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbvg;->e:F

    goto :goto_0

    .line 1665
    :sswitch_7
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbvg;->H:F

    goto :goto_0

    .line 1669
    :sswitch_8
    iget-object v0, p0, Lbvg;->f:Lbvj;

    if-nez v0, :cond_2

    .line 1670
    new-instance v0, Lbvj;

    invoke-direct {v0}, Lbvj;-><init>()V

    iput-object v0, p0, Lbvg;->f:Lbvj;

    .line 1672
    :cond_2
    iget-object v0, p0, Lbvg;->f:Lbvj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1676
    :sswitch_9
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbvg;->g:F

    goto :goto_0

    .line 1680
    :sswitch_a
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbvg;->h:F

    goto :goto_0

    .line 1684
    :sswitch_b
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbvg;->i:F

    goto :goto_0

    .line 1688
    :sswitch_c
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbvg;->j:F

    goto :goto_0

    .line 1692
    :sswitch_d
    const/16 v0, 0x6a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1693
    iget-object v0, p0, Lbvg;->I:[Lbva;

    if-nez v0, :cond_4

    move v0, v1

    .line 1694
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lbva;

    .line 1695
    iget-object v3, p0, Lbvg;->I:[Lbva;

    if-eqz v3, :cond_3

    .line 1696
    iget-object v3, p0, Lbvg;->I:[Lbva;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1698
    :cond_3
    iput-object v2, p0, Lbvg;->I:[Lbva;

    .line 1699
    :goto_2
    iget-object v2, p0, Lbvg;->I:[Lbva;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 1700
    iget-object v2, p0, Lbvg;->I:[Lbva;

    new-instance v3, Lbva;

    invoke-direct {v3}, Lbva;-><init>()V

    aput-object v3, v2, v0

    .line 1701
    iget-object v2, p0, Lbvg;->I:[Lbva;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1702
    invoke-virtual {p1}, Loxn;->a()I

    .line 1699
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1693
    :cond_4
    iget-object v0, p0, Lbvg;->I:[Lbva;

    array-length v0, v0

    goto :goto_1

    .line 1705
    :cond_5
    iget-object v2, p0, Lbvg;->I:[Lbva;

    new-instance v3, Lbva;

    invoke-direct {v3}, Lbva;-><init>()V

    aput-object v3, v2, v0

    .line 1706
    iget-object v2, p0, Lbvg;->I:[Lbva;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1710
    :sswitch_e
    iget-object v0, p0, Lbvg;->k:Lbvj;

    if-nez v0, :cond_6

    .line 1711
    new-instance v0, Lbvj;

    invoke-direct {v0}, Lbvj;-><init>()V

    iput-object v0, p0, Lbvg;->k:Lbvj;

    .line 1713
    :cond_6
    iget-object v0, p0, Lbvg;->k:Lbvj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1717
    :sswitch_f
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    iput v0, p0, Lbvg;->J:I

    goto/16 :goto_0

    .line 1721
    :sswitch_10
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbvg;->l:F

    goto/16 :goto_0

    .line 1725
    :sswitch_11
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbvg;->m:F

    goto/16 :goto_0

    .line 1729
    :sswitch_12
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbvg;->n:F

    goto/16 :goto_0

    .line 1733
    :sswitch_13
    iget-object v0, p0, Lbvg;->K:Lbvj;

    if-nez v0, :cond_7

    .line 1734
    new-instance v0, Lbvj;

    invoke-direct {v0}, Lbvj;-><init>()V

    iput-object v0, p0, Lbvg;->K:Lbvj;

    .line 1736
    :cond_7
    iget-object v0, p0, Lbvg;->K:Lbvj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1740
    :sswitch_14
    iget-object v0, p0, Lbvg;->o:Lbvi;

    if-nez v0, :cond_8

    .line 1741
    new-instance v0, Lbvi;

    invoke-direct {v0}, Lbvi;-><init>()V

    iput-object v0, p0, Lbvg;->o:Lbvi;

    .line 1743
    :cond_8
    iget-object v0, p0, Lbvg;->o:Lbvi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1747
    :sswitch_15
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbvg;->p:F

    goto/16 :goto_0

    .line 1751
    :sswitch_16
    iget-object v0, p0, Lbvg;->q:Lbvj;

    if-nez v0, :cond_9

    .line 1752
    new-instance v0, Lbvj;

    invoke-direct {v0}, Lbvj;-><init>()V

    iput-object v0, p0, Lbvg;->q:Lbvj;

    .line 1754
    :cond_9
    iget-object v0, p0, Lbvg;->q:Lbvj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1758
    :sswitch_17
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbvg;->r:F

    goto/16 :goto_0

    .line 1762
    :sswitch_18
    iget-object v0, p0, Lbvg;->s:Lbvj;

    if-nez v0, :cond_a

    .line 1763
    new-instance v0, Lbvj;

    invoke-direct {v0}, Lbvj;-><init>()V

    iput-object v0, p0, Lbvg;->s:Lbvj;

    .line 1765
    :cond_a
    iget-object v0, p0, Lbvg;->s:Lbvj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1769
    :sswitch_19
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbvg;->t:F

    goto/16 :goto_0

    .line 1773
    :sswitch_1a
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbvg;->u:F

    goto/16 :goto_0

    .line 1777
    :sswitch_1b
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    iput v0, p0, Lbvg;->v:I

    goto/16 :goto_0

    .line 1781
    :sswitch_1c
    iget-object v0, p0, Lbvg;->w:Lbvh;

    if-nez v0, :cond_b

    .line 1782
    new-instance v0, Lbvh;

    invoke-direct {v0}, Lbvh;-><init>()V

    iput-object v0, p0, Lbvg;->w:Lbvh;

    .line 1784
    :cond_b
    iget-object v0, p0, Lbvg;->w:Lbvh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1788
    :sswitch_1d
    iget-object v0, p0, Lbvg;->x:Lbvj;

    if-nez v0, :cond_c

    .line 1789
    new-instance v0, Lbvj;

    invoke-direct {v0}, Lbvj;-><init>()V

    iput-object v0, p0, Lbvg;->x:Lbvj;

    .line 1791
    :cond_c
    iget-object v0, p0, Lbvg;->x:Lbvj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1795
    :sswitch_1e
    iget-object v0, p0, Lbvg;->y:Lbvj;

    if-nez v0, :cond_d

    .line 1796
    new-instance v0, Lbvj;

    invoke-direct {v0}, Lbvj;-><init>()V

    iput-object v0, p0, Lbvg;->y:Lbvj;

    .line 1798
    :cond_d
    iget-object v0, p0, Lbvg;->y:Lbvj;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1802
    :sswitch_1f
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    iput v0, p0, Lbvg;->z:F

    goto/16 :goto_0

    .line 1806
    :sswitch_20
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    iput v0, p0, Lbvg;->A:I

    goto/16 :goto_0

    .line 1810
    :sswitch_21
    iget-object v0, p0, Lbvg;->B:Lbvk;

    if-nez v0, :cond_e

    .line 1811
    new-instance v0, Lbvk;

    invoke-direct {v0}, Lbvk;-><init>()V

    iput-object v0, p0, Lbvg;->B:Lbvk;

    .line 1813
    :cond_e
    iget-object v0, p0, Lbvg;->B:Lbvk;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1817
    :sswitch_22
    iget-object v0, p0, Lbvg;->C:Lbvl;

    if-nez v0, :cond_f

    .line 1818
    new-instance v0, Lbvl;

    invoke-direct {v0}, Lbvl;-><init>()V

    iput-object v0, p0, Lbvg;->C:Lbvl;

    .line 1820
    :cond_f
    iget-object v0, p0, Lbvg;->C:Lbvl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1824
    :sswitch_23
    iget-object v0, p0, Lbvg;->D:Lbvl;

    if-nez v0, :cond_10

    .line 1825
    new-instance v0, Lbvl;

    invoke-direct {v0}, Lbvl;-><init>()V

    iput-object v0, p0, Lbvg;->D:Lbvl;

    .line 1827
    :cond_10
    iget-object v0, p0, Lbvg;->D:Lbvl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1831
    :sswitch_24
    iget-object v0, p0, Lbvg;->E:Lbvm;

    if-nez v0, :cond_11

    .line 1832
    new-instance v0, Lbvm;

    invoke-direct {v0}, Lbvm;-><init>()V

    iput-object v0, p0, Lbvg;->E:Lbvm;

    .line 1834
    :cond_11
    iget-object v0, p0, Lbvg;->E:Lbvm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1628
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1d -> :sswitch_3
        0x25 -> :sswitch_4
        0x2d -> :sswitch_5
        0x35 -> :sswitch_6
        0x3d -> :sswitch_7
        0x42 -> :sswitch_8
        0x4d -> :sswitch_9
        0x55 -> :sswitch_a
        0x5d -> :sswitch_b
        0x65 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x78 -> :sswitch_f
        0x85 -> :sswitch_10
        0x8d -> :sswitch_11
        0x95 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xad -> :sswitch_15
        0xb2 -> :sswitch_16
        0xbd -> :sswitch_17
        0xc2 -> :sswitch_18
        0xcd -> :sswitch_19
        0xd5 -> :sswitch_1a
        0xd8 -> :sswitch_1b
        0xe2 -> :sswitch_1c
        0xea -> :sswitch_1d
        0xf2 -> :sswitch_1e
        0xfd -> :sswitch_1f
        0x100 -> :sswitch_20
        0x10a -> :sswitch_21
        0x112 -> :sswitch_22
        0x11a -> :sswitch_23
        0x122 -> :sswitch_24
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 1357
    const/4 v0, 0x1

    iget-wide v2, p0, Lbvg;->b:J

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 1358
    iget-object v0, p0, Lbvg;->c:Lbvn;

    if-eqz v0, :cond_0

    .line 1359
    const/4 v0, 0x2

    iget-object v1, p0, Lbvg;->c:Lbvn;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1361
    :cond_0
    iget v0, p0, Lbvg;->d:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1362
    const/4 v0, 0x3

    iget v1, p0, Lbvg;->d:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1364
    :cond_1
    iget v0, p0, Lbvg;->F:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1365
    const/4 v0, 0x4

    iget v1, p0, Lbvg;->F:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1367
    :cond_2
    iget v0, p0, Lbvg;->G:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1368
    const/4 v0, 0x5

    iget v1, p0, Lbvg;->G:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1370
    :cond_3
    iget v0, p0, Lbvg;->e:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1371
    const/4 v0, 0x6

    iget v1, p0, Lbvg;->e:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1373
    :cond_4
    iget v0, p0, Lbvg;->H:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1374
    const/4 v0, 0x7

    iget v1, p0, Lbvg;->H:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1376
    :cond_5
    iget-object v0, p0, Lbvg;->f:Lbvj;

    if-eqz v0, :cond_6

    .line 1377
    const/16 v0, 0x8

    iget-object v1, p0, Lbvg;->f:Lbvj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1379
    :cond_6
    iget v0, p0, Lbvg;->g:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1380
    const/16 v0, 0x9

    iget v1, p0, Lbvg;->g:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1382
    :cond_7
    iget v0, p0, Lbvg;->h:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1383
    const/16 v0, 0xa

    iget v1, p0, Lbvg;->h:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1385
    :cond_8
    iget v0, p0, Lbvg;->i:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1386
    const/16 v0, 0xb

    iget v1, p0, Lbvg;->i:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1388
    :cond_9
    iget v0, p0, Lbvg;->j:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1389
    const/16 v0, 0xc

    iget v1, p0, Lbvg;->j:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1391
    :cond_a
    iget-object v0, p0, Lbvg;->I:[Lbva;

    if-eqz v0, :cond_c

    .line 1392
    iget-object v1, p0, Lbvg;->I:[Lbva;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_c

    aget-object v3, v1, v0

    .line 1393
    if-eqz v3, :cond_b

    .line 1394
    const/16 v4, 0xd

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 1392
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1398
    :cond_c
    iget-object v0, p0, Lbvg;->k:Lbvj;

    if-eqz v0, :cond_d

    .line 1399
    const/16 v0, 0xe

    iget-object v1, p0, Lbvg;->k:Lbvj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1401
    :cond_d
    iget v0, p0, Lbvg;->J:I

    if-eq v0, v5, :cond_e

    .line 1402
    const/16 v0, 0xf

    iget v1, p0, Lbvg;->J:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1404
    :cond_e
    iget v0, p0, Lbvg;->l:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_f

    .line 1405
    const/16 v0, 0x10

    iget v1, p0, Lbvg;->l:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1407
    :cond_f
    iget v0, p0, Lbvg;->m:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_10

    .line 1408
    const/16 v0, 0x11

    iget v1, p0, Lbvg;->m:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1410
    :cond_10
    iget v0, p0, Lbvg;->n:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_11

    .line 1411
    const/16 v0, 0x12

    iget v1, p0, Lbvg;->n:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1413
    :cond_11
    iget-object v0, p0, Lbvg;->K:Lbvj;

    if-eqz v0, :cond_12

    .line 1414
    const/16 v0, 0x13

    iget-object v1, p0, Lbvg;->K:Lbvj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1416
    :cond_12
    iget-object v0, p0, Lbvg;->o:Lbvi;

    if-eqz v0, :cond_13

    .line 1417
    const/16 v0, 0x14

    iget-object v1, p0, Lbvg;->o:Lbvi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1419
    :cond_13
    iget v0, p0, Lbvg;->p:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_14

    .line 1420
    const/16 v0, 0x15

    iget v1, p0, Lbvg;->p:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1422
    :cond_14
    iget-object v0, p0, Lbvg;->q:Lbvj;

    if-eqz v0, :cond_15

    .line 1423
    const/16 v0, 0x16

    iget-object v1, p0, Lbvg;->q:Lbvj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1425
    :cond_15
    iget v0, p0, Lbvg;->r:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_16

    .line 1426
    const/16 v0, 0x17

    iget v1, p0, Lbvg;->r:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1428
    :cond_16
    iget-object v0, p0, Lbvg;->s:Lbvj;

    if-eqz v0, :cond_17

    .line 1429
    const/16 v0, 0x18

    iget-object v1, p0, Lbvg;->s:Lbvj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1431
    :cond_17
    iget v0, p0, Lbvg;->t:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_18

    .line 1432
    const/16 v0, 0x19

    iget v1, p0, Lbvg;->t:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1434
    :cond_18
    iget v0, p0, Lbvg;->u:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_19

    .line 1435
    const/16 v0, 0x1a

    iget v1, p0, Lbvg;->u:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1437
    :cond_19
    iget v0, p0, Lbvg;->v:I

    if-eq v0, v5, :cond_1a

    .line 1438
    const/16 v0, 0x1b

    iget v1, p0, Lbvg;->v:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1440
    :cond_1a
    iget-object v0, p0, Lbvg;->w:Lbvh;

    if-eqz v0, :cond_1b

    .line 1441
    const/16 v0, 0x1c

    iget-object v1, p0, Lbvg;->w:Lbvh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1443
    :cond_1b
    iget-object v0, p0, Lbvg;->x:Lbvj;

    if-eqz v0, :cond_1c

    .line 1444
    const/16 v0, 0x1d

    iget-object v1, p0, Lbvg;->x:Lbvj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1446
    :cond_1c
    iget-object v0, p0, Lbvg;->y:Lbvj;

    if-eqz v0, :cond_1d

    .line 1447
    const/16 v0, 0x1e

    iget-object v1, p0, Lbvg;->y:Lbvj;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1449
    :cond_1d
    iget v0, p0, Lbvg;->z:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_1e

    .line 1450
    const/16 v0, 0x1f

    iget v1, p0, Lbvg;->z:F

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 1452
    :cond_1e
    iget v0, p0, Lbvg;->A:I

    if-eq v0, v5, :cond_1f

    .line 1453
    const/16 v0, 0x20

    iget v1, p0, Lbvg;->A:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1455
    :cond_1f
    iget-object v0, p0, Lbvg;->B:Lbvk;

    if-eqz v0, :cond_20

    .line 1456
    const/16 v0, 0x21

    iget-object v1, p0, Lbvg;->B:Lbvk;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1458
    :cond_20
    iget-object v0, p0, Lbvg;->C:Lbvl;

    if-eqz v0, :cond_21

    .line 1459
    const/16 v0, 0x22

    iget-object v1, p0, Lbvg;->C:Lbvl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1461
    :cond_21
    iget-object v0, p0, Lbvg;->D:Lbvl;

    if-eqz v0, :cond_22

    .line 1462
    const/16 v0, 0x23

    iget-object v1, p0, Lbvg;->D:Lbvl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1464
    :cond_22
    iget-object v0, p0, Lbvg;->E:Lbvm;

    if-eqz v0, :cond_23

    .line 1465
    const/16 v0, 0x24

    iget-object v1, p0, Lbvg;->E:Lbvm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1467
    :cond_23
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 487
    invoke-virtual {p0, p1}, Lbvg;->a(Loxn;)Lbvg;

    move-result-object v0

    return-object v0
.end method
