.class public Lazi;
.super Layb;
.source "PG"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Layj;",
            ">;"
        }
    .end annotation
.end field

.field private final b:[I

.field private final c:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Layj;",
            ">;"
        }
    .end annotation
.end field

.field private d:J

.field private e:Layj;


# direct methods
.method public constructor <init>([ILjava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I",
            "Ljava/util/List",
            "<",
            "Layj;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Layb;-><init>()V

    .line 37
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    const-string v2, "size of effectList"

    invoke-static {v0, v2}, Lcec;->a(ILjava/lang/CharSequence;)I

    .line 38
    array-length v0, p1

    const-string v2, "Effect index count"

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v3

    const-string v4, "Effect index array and effect list must have the same size."

    invoke-static {v0, v2, v3, v4}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 40
    invoke-static {p2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lazi;->a:Ljava/util/List;

    .line 41
    iput-object p1, p0, Lazi;->b:[I

    .line 42
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lazi;->c:Landroid/util/SparseArray;

    move v0, v1

    .line 43
    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 44
    iget-object v2, p0, Lazi;->c:Landroid/util/SparseArray;

    aget v3, p1, v0

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    const-string v3, "Repeated effect index found."

    invoke-static {v2, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 46
    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    const-string v3, "effectList member"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 47
    iget-object v2, p0, Lazi;->c:Landroid/util/SparseArray;

    aget v3, p1, v0

    invoke-interface {p2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 43
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move v2, v1

    .line 44
    goto :goto_1

    .line 49
    :cond_1
    return-void
.end method

.method public varargs constructor <init>([I[Layj;)V
    .locals 1

    .prologue
    .line 57
    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lazi;-><init>([ILjava/util/List;)V

    .line 58
    return-void
.end method

.method private B(Layl;)Layj;
    .locals 4

    .prologue
    .line 287
    iget-object v0, p0, Lazi;->e:Layj;

    if-eqz v0, :cond_0

    iget-wide v0, p1, Layl;->a:J

    iget-wide v2, p0, Lazi;->d:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 288
    :cond_0
    invoke-virtual {p0, p1}, Lazi;->A(Layl;)Layj;

    move-result-object v0

    iput-object v0, p0, Lazi;->e:Layj;

    .line 289
    iget-wide v0, p1, Layl;->a:J

    iput-wide v0, p0, Lazi;->d:J

    .line 291
    :cond_1
    iget-object v0, p0, Lazi;->e:Layj;

    return-object v0
.end method

.method private d(Lbmd;)I
    .locals 4

    .prologue
    .line 295
    iget-object v0, p0, Lazi;->b:[I

    new-instance v1, Ljava/util/Random;

    invoke-virtual {p1}, Lbmd;->hashCode()I

    move-result v2

    int-to-long v2, v2

    invoke-direct {v1, v2, v3}, Ljava/util/Random;-><init>(J)V

    iget-object v2, p0, Lazi;->b:[I

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    aget v0, v0, v1

    return v0
.end method

.method private e(Lbmd;)Layj;
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Lazi;->c:Landroid/util/SparseArray;

    invoke-direct {p0, p1}, Lazi;->d(Lbmd;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Layj;

    return-object v0
.end method


# virtual methods
.method protected A(Layl;)Layj;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 265
    iget-object v1, p1, Layl;->x:Look;

    if-eqz v1, :cond_2

    .line 267
    iget-object v1, p1, Layl;->x:Look;

    iget-object v2, v1, Look;->b:[Ljava/lang/Integer;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 268
    iget-object v4, p0, Lazi;->c:Landroid/util/SparseArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Layj;

    .line 269
    if-eqz v0, :cond_0

    .line 283
    :goto_1
    return-object v0

    .line 267
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 274
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No effect enabled in styling for MultiEffect"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 278
    :cond_2
    iget v1, p1, Layl;->f:I

    if-nez v1, :cond_3

    .line 279
    iget-object v1, p0, Lazi;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Layj;

    goto :goto_1

    .line 281
    :cond_3
    iget v1, p1, Layl;->f:I

    iget-wide v2, p1, Layl;->e:J

    long-to-int v2, v2

    add-int/2addr v1, v2

    .line 282
    iget-object v2, p0, Lazi;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-static {v0, v2, v1}, Lbag;->a(III)I

    move-result v0

    .line 283
    iget-object v1, p0, Lazi;->a:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Layj;

    goto :goto_1
.end method

.method public a(Lbmg;Lbmg;)J
    .locals 2

    .prologue
    .line 62
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public a(Landroid/content/Context;Layl;II)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 133
    invoke-direct {p0, p2}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Layj;->a(Landroid/content/Context;Layl;II)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public a(Layl;Lbof;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 180
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    .line 181
    invoke-interface {v0, p1, p2}, Layj;->a(Layl;Lbof;)Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method protected a(I)Layj;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lazi;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Layj;

    return-object v0
.end method

.method public a(Lbmd;)Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbmd;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 67
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 68
    invoke-direct {p0, p1}, Lazi;->d(Lbmd;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 69
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public a(Layl;)Z
    .locals 1

    .prologue
    .line 218
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->a(Layl;)Z

    move-result v0

    return v0
.end method

.method public b(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 186
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    .line 187
    invoke-interface {v0, p1}, Layj;->b(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method public b(Lbmd;)Lool;
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lazi;->e(Lbmd;)Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->b(Lbmd;)Lool;

    move-result-object v0

    return-object v0
.end method

.method public c(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 223
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    .line 224
    invoke-interface {v0, p1}, Layj;->c(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method public c(Lbmd;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbmd;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Double;",
            ">;"
        }
    .end annotation

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lazi;->e(Lbmd;)Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->c(Lbmd;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public d(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 229
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    .line 230
    invoke-interface {v0, p1}, Layj;->d(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method public e(Layl;)I
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->e(Layl;)I

    move-result v0

    return v0
.end method

.method public f(Layl;)I
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->f(Layl;)I

    move-result v0

    return v0
.end method

.method public f_()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 236
    iget-object v0, p0, Lazi;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Layj;

    .line 237
    invoke-interface {v0}, Layj;->f_()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 238
    invoke-interface {v0}, Layj;->f_()Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 241
    :cond_1
    return-object v1
.end method

.method public g(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    .line 96
    invoke-interface {v0, p1}, Layj;->g(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method public g_()Layc;
    .locals 1

    .prologue
    .line 208
    sget-object v0, Layc;->a:Layc;

    return-object v0
.end method

.method public h(Layl;)F
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->h(Layl;)F

    move-result v0

    return v0
.end method

.method public i(Layl;)I
    .locals 1

    .prologue
    .line 106
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->i(Layl;)I

    move-result v0

    return v0
.end method

.method public j(Layl;)I
    .locals 1

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->j(Layl;)I

    move-result v0

    return v0
.end method

.method public k(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 116
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    .line 117
    invoke-interface {v0, p1}, Layj;->k(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method public l(Layl;)F
    .locals 1

    .prologue
    .line 122
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->l(Layl;)F

    move-result v0

    return v0
.end method

.method public m(Layl;)I
    .locals 1

    .prologue
    .line 127
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->m(Layl;)I

    move-result v0

    return v0
.end method

.method public n(Layl;)I
    .locals 1

    .prologue
    .line 139
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->n(Layl;)I

    move-result v0

    return v0
.end method

.method public o(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->o(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method public p(Layl;)F
    .locals 1

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->p(Layl;)F

    move-result v0

    return v0
.end method

.method public q(Layl;)I
    .locals 1

    .prologue
    .line 155
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->q(Layl;)I

    move-result v0

    return v0
.end method

.method public r(Layl;)I
    .locals 1

    .prologue
    .line 160
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->r(Layl;)I

    move-result v0

    return v0
.end method

.method public s(Layl;)J
    .locals 2

    .prologue
    .line 165
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public t(Layl;)F
    .locals 1

    .prologue
    .line 170
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->t(Layl;)F

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 247
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lazi;->a:Ljava/util/List;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public u(Layl;)F
    .locals 1

    .prologue
    .line 213
    invoke-virtual {p0}, Lazi;->g_()Layc;

    move-result-object v0

    invoke-virtual {v0}, Layc;->b()F

    move-result v0

    return v0
.end method

.method public v(Layl;)F
    .locals 1

    .prologue
    .line 175
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->v(Layl;)F

    move-result v0

    return v0
.end method

.method public w(Layl;)F
    .locals 1

    .prologue
    .line 192
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    .line 193
    invoke-interface {v0, p1}, Layj;->w(Layl;)F

    move-result v0

    return v0
.end method

.method public x(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 198
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->x(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method public y(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 203
    invoke-direct {p0, p1}, Lazi;->B(Layl;)Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->y(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method public z(Layl;)V
    .locals 0

    .prologue
    .line 252
    return-void
.end method
