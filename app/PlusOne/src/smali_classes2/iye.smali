.class public final Liye;
.super Lt;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lt;-><init>()V

    return-void
.end method

.method private U()Lnql;
    .locals 2

    .prologue
    .line 45
    invoke-virtual {p0}, Liye;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "oob_resp"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhyt;

    .line 46
    new-instance v1, Lnql;

    invoke-direct {v1}, Lnql;-><init>()V

    .line 47
    invoke-virtual {v0, v1}, Lhyt;->a(Loxu;)Loxu;

    move-result-object v0

    check-cast v0, Lnql;

    return-object v0
.end method

.method public static a(Liyf;Ljava/lang/String;Lnql;)V
    .locals 5

    .prologue
    .line 35
    new-instance v0, Liye;

    invoke-direct {v0}, Liye;-><init>()V

    .line 36
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Liye;->a(Lu;I)V

    .line 37
    invoke-virtual {p0}, Liyf;->p()Lae;

    move-result-object v1

    .line 38
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    .line 39
    const-string v3, "oob_resp"

    new-instance v4, Lhyt;

    invoke-direct {v4, p2}, Lhyt;-><init>(Loxu;)V

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 40
    invoke-virtual {v0, v2}, Liye;->f(Landroid/os/Bundle;)V

    .line 41
    invoke-virtual {v0, v1, p1}, Liye;->a(Lae;Ljava/lang/String;)V

    .line 42
    return-void
.end method


# virtual methods
.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 52
    invoke-virtual {p0}, Liye;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 53
    invoke-direct {p0}, Liye;->U()Lnql;

    move-result-object v0

    .line 54
    iget-object v0, v0, Lnql;->a:Lnqg;

    iget-object v1, v0, Lnqg;->a:Lnpy;

    .line 55
    new-instance v5, Landroid/app/AlertDialog$Builder;

    new-instance v0, Landroid/view/ContextThemeWrapper;

    .line 56
    invoke-virtual {p0}, Liye;->n()Lz;

    move-result-object v2

    const v6, 0x7f090065

    invoke-direct {v0, v2, v6}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v5, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 57
    invoke-virtual {v5, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 58
    iget-object v0, v1, Lnpy;->b:Ljava/lang/String;

    invoke-virtual {v5, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 59
    const v0, 0x7f040151

    const/4 v2, 0x0

    invoke-virtual {v4, v0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 60
    invoke-virtual {v5, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 62
    iget-object v0, v1, Lnpy;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 63
    const v0, 0x7f100139

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 64
    iget-object v6, v1, Lnpy;->c:Ljava/lang/String;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    :cond_0
    iget-object v0, v1, Lnpy;->a:[Lnpx;

    if-eqz v0, :cond_1

    .line 68
    const v0, 0x7f10013a

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 69
    iget-object v6, v1, Lnpy;->a:[Lnpx;

    array-length v7, v6

    move v2, v3

    :goto_0
    if-ge v2, v7, :cond_1

    aget-object v8, v6, v2

    .line 70
    const v1, 0x7f040152

    invoke-virtual {v4, v1, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 71
    iget-object v9, v8, Lnpx;->c:Ljava/lang/String;

    invoke-virtual {v1, v9}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 72
    invoke-virtual {v1, v8}, Landroid/widget/Button;->setTag(Ljava/lang/Object;)V

    .line 73
    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 74
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 69
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    .line 78
    :cond_1
    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 103
    invoke-super {p0, p1}, Lt;->onCancel(Landroid/content/DialogInterface;)V

    .line 104
    invoke-virtual {p0}, Liye;->u_()Lu;

    move-result-object v0

    check-cast v0, Liyf;

    invoke-virtual {v0}, Liyf;->U()V

    .line 105
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 83
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnpx;

    .line 84
    iget v1, v0, Lnpx;->b:I

    packed-switch v1, :pswitch_data_0

    .line 96
    :pswitch_0
    invoke-virtual {p0}, Liye;->b()V

    .line 97
    invoke-virtual {p0}, Liye;->u_()Lu;

    move-result-object v1

    check-cast v1, Liyf;

    invoke-direct {p0}, Liye;->U()Lnql;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Liyf;->a(Lnql;Lnpx;)V

    .line 99
    :goto_0
    return-void

    .line 87
    :pswitch_1
    invoke-virtual {p0}, Liye;->b()V

    .line 88
    invoke-virtual {p0}, Liye;->u_()Lu;

    move-result-object v0

    check-cast v0, Liyf;

    invoke-virtual {v0}, Liyf;->U()V

    goto :goto_0

    .line 92
    :pswitch_2
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    iget-object v0, v0, Lnpx;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v1}, Liye;->a(Landroid/content/Intent;)V

    goto :goto_0

    .line 84
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
