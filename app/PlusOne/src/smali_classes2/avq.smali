.class final Lavq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lavq;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final A:Ljava/util/concurrent/atomic/AtomicInteger;

.field public B:Z

.field public C:Z

.field public D:Z

.field public E:Z

.field public F:Z

.field public G:Latu;

.field public H:J

.field public I:Ljava/lang/String;

.field public J:Ljava/lang/String;

.field public K:Ljava/lang/String;

.field public L:Z

.field public M:Z

.field public N:Ljava/lang/String;

.field public O:Ljed;

.field public P:Ljdz;

.field public Q:Landroid/net/Uri;

.field public R:Ljava/lang/String;

.field public S:J

.field public T:Z

.field public U:Landroid/content/Intent;

.field public V:Z

.field private W:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation
.end field

.field private X:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation
.end field

.field public a:Lawg;

.field public b:Z

.field public c:I

.field public d:Ljava/lang/String;

.field public e:I

.field public f:Z

.field public g:Z

.field public h:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field public final i:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lavz;",
            ">;"
        }
    .end annotation
.end field

.field public final j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljdx;",
            "Ljej;",
            ">;"
        }
    .end annotation
.end field

.field public final k:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/net/Uri;",
            "Ljdx;",
            ">;"
        }
    .end annotation
.end field

.field public l:Ljfd;

.field public m:[B

.field public n:Ljava/lang/String;

.field public o:Ljava/lang/String;

.field public p:Z

.field public q:Landroid/net/Uri;

.field public r:Z

.field public s:Z

.field public t:Lbza;

.field public u:J

.field public v:Ljava/lang/String;

.field public w:Z

.field public final x:Lbor;

.field public final y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbph;",
            ">;"
        }
    .end annotation
.end field

.field public z:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 525
    new-instance v0, Lavr;

    invoke-direct {v0}, Lavr;-><init>()V

    sput-object v0, Lavq;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 313
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    sget-object v0, Lawg;->a:Lawg;

    iput-object v0, p0, Lavq;->a:Lawg;

    .line 143
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lavq;->h:Ljava/util/List;

    .line 148
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lavq;->i:Ljava/util/List;

    .line 153
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lavq;->j:Ljava/util/Map;

    .line 162
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lavq;->k:Ljava/util/Map;

    .line 314
    new-instance v0, Lbor;

    invoke-direct {v0}, Lbor;-><init>()V

    iput-object v0, p0, Lavq;->x:Lbor;

    .line 315
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lavq;->y:Ljava/util/List;

    .line 316
    const/4 v0, 0x1

    iput-boolean v0, p0, Lavq;->s:Z

    .line 317
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lavq;->A:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 318
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lavq;->a(Z)V

    .line 319
    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    sget-object v0, Lawg;->a:Lawg;

    iput-object v0, p0, Lavq;->a:Lawg;

    .line 143
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lavq;->h:Ljava/util/List;

    .line 148
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lavq;->i:Ljava/util/List;

    .line 153
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lavq;->j:Ljava/util/Map;

    .line 162
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lavq;->k:Ljava/util/Map;

    .line 323
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v5

    .line 324
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v4

    array-length v6, v4

    move v1, v3

    :goto_0
    if-ge v1, v6, :cond_0

    aget-object v0, v4, v1

    .line 325
    iget-object v7, p0, Lavq;->h:Ljava/util/List;

    check-cast v0, Landroid/net/Uri;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 324
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 327
    :cond_0
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readParcelableArray(Ljava/lang/ClassLoader;)[Landroid/os/Parcelable;

    move-result-object v4

    array-length v6, v4

    move v1, v3

    :goto_1
    if-ge v1, v6, :cond_1

    aget-object v0, v4, v1

    .line 328
    iget-object v7, p0, Lavq;->i:Ljava/util/List;

    check-cast v0, Lavz;

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 327
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 330
    :cond_1
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_3

    move v0, v2

    .line 331
    :goto_2
    if-eqz v0, :cond_2

    .line 332
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lavq;->q:Landroid/net/Uri;

    .line 334
    :cond_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    :goto_3
    iput-boolean v0, p0, Lavq;->r:Z

    .line 335
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    :goto_4
    iput-boolean v0, p0, Lavq;->V:Z

    .line 336
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_6

    move v0, v2

    :goto_5
    iput-boolean v0, p0, Lavq;->s:Z

    .line 337
    invoke-static {}, Lbza;->values()[Lbza;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    aget-object v0, v0, v1

    iput-object v0, p0, Lavq;->t:Lbza;

    .line 338
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lavq;->u:J

    .line 339
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavq;->v:Ljava/lang/String;

    .line 340
    iget-object v0, p0, Lavq;->v:Ljava/lang/String;

    if-nez v0, :cond_7

    const-string v0, ""

    :goto_6
    iput-object v0, p0, Lavq;->v:Ljava/lang/String;

    .line 341
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_8

    move v0, v2

    :goto_7
    iput-boolean v0, p0, Lavq;->w:Z

    .line 342
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lbor;

    iput-object v0, p0, Lavq;->x:Lbor;

    .line 343
    const-class v0, Lbph;

    invoke-static {p1, v0}, Lawe;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lavq;->y:Ljava/util/List;

    .line 344
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lavq;->z:I

    .line 345
    const-class v0, Lbmd;

    invoke-static {p1, v0}, Lawe;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lavq;->W:Ljava/util/List;

    .line 346
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_9

    move v0, v2

    .line 347
    :goto_8
    if-eqz v0, :cond_a

    .line 348
    const-class v0, Lbmd;

    invoke-static {p1, v0}, Lawe;->a(Landroid/os/Parcel;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lavq;->X:Ljava/util/List;

    .line 352
    :goto_9
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lavq;->A:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 353
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_b

    move v0, v2

    :goto_a
    iput-boolean v0, p0, Lavq;->b:Z

    .line 354
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_c

    move v0, v2

    :goto_b
    iput-boolean v0, p0, Lavq;->g:Z

    .line 355
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_d

    move v0, v2

    :goto_c
    iput-boolean v0, p0, Lavq;->B:Z

    .line 356
    iget-boolean v0, p0, Lavq;->B:Z

    if-eqz v0, :cond_11

    .line 357
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_e

    move v0, v2

    :goto_d
    iput-boolean v0, p0, Lavq;->C:Z

    .line 358
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_f

    move v0, v2

    :goto_e
    iput-boolean v0, p0, Lavq;->D:Z

    .line 359
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_10

    move v0, v2

    :goto_f
    iput-boolean v0, p0, Lavq;->E:Z

    .line 360
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Latu;

    iput-object v0, p0, Lavq;->G:Latu;

    .line 361
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lavq;->H:J

    .line 362
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavq;->I:Ljava/lang/String;

    .line 363
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavq;->J:Ljava/lang/String;

    .line 364
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavq;->K:Ljava/lang/String;

    .line 368
    :goto_10
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_12

    move v0, v2

    :goto_11
    iput-boolean v0, p0, Lavq;->L:Z

    .line 369
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavq;->N:Ljava/lang/String;

    .line 370
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ljed;

    iput-object v0, p0, Lavq;->O:Ljed;

    .line 371
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_13

    move v0, v2

    .line 372
    :goto_12
    if-eqz v0, :cond_14

    .line 373
    sget-object v0, Ljdz;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljdz;

    iput-object v0, p0, Lavq;->P:Ljdz;

    .line 377
    :goto_13
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_15

    move v0, v2

    .line 378
    :goto_14
    if-eqz v0, :cond_16

    .line 379
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lavq;->Q:Landroid/net/Uri;

    .line 383
    :goto_15
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_17

    move v0, v2

    .line 384
    :goto_16
    if-eqz v0, :cond_18

    .line 385
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavq;->R:Ljava/lang/String;

    .line 389
    :goto_17
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_19

    move v0, v2

    .line 390
    :goto_18
    if-eqz v0, :cond_1a

    .line 391
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Ljfd;

    iput-object v0, p0, Lavq;->l:Ljfd;

    .line 395
    :goto_19
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1b

    move v0, v2

    .line 396
    :goto_1a
    if-eqz v0, :cond_1c

    .line 397
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lavq;->m:[B

    .line 401
    :goto_1b
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1d

    move v0, v2

    .line 402
    :goto_1c
    if-eqz v0, :cond_1e

    .line 403
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavq;->n:Ljava/lang/String;

    .line 408
    :goto_1d
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1f

    move v0, v2

    .line 409
    :goto_1e
    if-eqz v0, :cond_20

    .line 410
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavq;->o:Ljava/lang/String;

    .line 415
    :goto_1f
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_21

    move v0, v2

    :goto_20
    iput-boolean v0, p0, Lavq;->p:Z

    .line 416
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v6

    move v4, v3

    .line 417
    :goto_21
    if-ge v4, v6, :cond_22

    .line 418
    sget-object v0, Ljdx;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljdx;

    .line 419
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Ljej;

    .line 420
    iget-object v7, p0, Lavq;->j:Ljava/util/Map;

    invoke-interface {v7, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 421
    iget-object v7, p0, Lavq;->k:Ljava/util/Map;

    iget-object v1, v1, Ljej;->a:Landroid/net/Uri;

    invoke-interface {v7, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 417
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_21

    :cond_3
    move v0, v3

    .line 330
    goto/16 :goto_2

    :cond_4
    move v0, v3

    .line 334
    goto/16 :goto_3

    :cond_5
    move v0, v3

    .line 335
    goto/16 :goto_4

    :cond_6
    move v0, v3

    .line 336
    goto/16 :goto_5

    .line 340
    :cond_7
    iget-object v0, p0, Lavq;->v:Ljava/lang/String;

    goto/16 :goto_6

    :cond_8
    move v0, v3

    .line 341
    goto/16 :goto_7

    :cond_9
    move v0, v3

    .line 346
    goto/16 :goto_8

    .line 350
    :cond_a
    iput-object v8, p0, Lavq;->X:Ljava/util/List;

    goto/16 :goto_9

    :cond_b
    move v0, v3

    .line 353
    goto/16 :goto_a

    :cond_c
    move v0, v3

    .line 354
    goto/16 :goto_b

    :cond_d
    move v0, v3

    .line 355
    goto/16 :goto_c

    :cond_e
    move v0, v3

    .line 357
    goto/16 :goto_d

    :cond_f
    move v0, v3

    .line 358
    goto/16 :goto_e

    :cond_10
    move v0, v3

    .line 359
    goto/16 :goto_f

    .line 366
    :cond_11
    invoke-virtual {p0, v3}, Lavq;->b(Z)V

    goto/16 :goto_10

    :cond_12
    move v0, v3

    .line 368
    goto/16 :goto_11

    :cond_13
    move v0, v3

    .line 371
    goto/16 :goto_12

    .line 375
    :cond_14
    iput-object v8, p0, Lavq;->P:Ljdz;

    goto/16 :goto_13

    :cond_15
    move v0, v3

    .line 377
    goto/16 :goto_14

    .line 381
    :cond_16
    iput-object v8, p0, Lavq;->Q:Landroid/net/Uri;

    goto/16 :goto_15

    :cond_17
    move v0, v3

    .line 383
    goto/16 :goto_16

    .line 387
    :cond_18
    iput-object v8, p0, Lavq;->R:Ljava/lang/String;

    goto/16 :goto_17

    :cond_19
    move v0, v3

    .line 389
    goto/16 :goto_18

    .line 393
    :cond_1a
    iput-object v8, p0, Lavq;->l:Ljfd;

    goto/16 :goto_19

    :cond_1b
    move v0, v3

    .line 395
    goto/16 :goto_1a

    .line 399
    :cond_1c
    iput-object v8, p0, Lavq;->m:[B

    goto/16 :goto_1b

    :cond_1d
    move v0, v3

    .line 401
    goto/16 :goto_1c

    .line 405
    :cond_1e
    iput-object v8, p0, Lavq;->n:Ljava/lang/String;

    goto/16 :goto_1d

    :cond_1f
    move v0, v3

    .line 408
    goto/16 :goto_1e

    .line 412
    :cond_20
    iput-object v8, p0, Lavq;->o:Ljava/lang/String;

    goto/16 :goto_1f

    :cond_21
    move v0, v3

    .line 415
    goto/16 :goto_20

    .line 423
    :cond_22
    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lavq;->S:J

    .line 424
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_23

    move v0, v2

    :goto_22
    iput-boolean v0, p0, Lavq;->T:Z

    .line 425
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lavq;->c:I

    .line 426
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lavq;->d:Ljava/lang/String;

    .line 427
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lavq;->e:I

    .line 428
    invoke-virtual {p1, v5}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lavq;->U:Landroid/content/Intent;

    .line 429
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_24

    move v0, v2

    :goto_23
    iput-boolean v0, p0, Lavq;->f:Z

    .line 430
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_25

    :goto_24
    iput-boolean v2, p0, Lavq;->F:Z

    .line 431
    return-void

    :cond_23
    move v0, v3

    .line 424
    goto :goto_22

    :cond_24
    move v0, v3

    .line 429
    goto :goto_23

    :cond_25
    move v2, v3

    .line 430
    goto :goto_24
.end method

.method static synthetic a(Lavq;)Ljava/util/List;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lavq;->W:Ljava/util/List;

    return-object v0
.end method

.method static synthetic a(Lavq;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lavq;->W:Ljava/util/List;

    return-object p1
.end method

.method static synthetic b(Lavq;)Ljava/util/List;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Lavq;->X:Ljava/util/List;

    return-object v0
.end method

.method static synthetic b(Lavq;Ljava/util/List;)Ljava/util/List;
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lavq;->X:Ljava/util/List;

    return-object p1
.end method


# virtual methods
.method a(Z)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 546
    if-nez p1, :cond_0

    .line 547
    iget-object v0, p0, Lavq;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 548
    iget-object v0, p0, Lavq;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 549
    iput-object v1, p0, Lavq;->N:Ljava/lang/String;

    .line 550
    iput-object v1, p0, Lavq;->O:Ljed;

    .line 551
    iput-object v1, p0, Lavq;->P:Ljdz;

    .line 552
    iput-object v1, p0, Lavq;->Q:Landroid/net/Uri;

    .line 553
    iput-object v1, p0, Lavq;->R:Ljava/lang/String;

    .line 554
    sget-object v0, Lawg;->a:Lawg;

    iput-object v0, p0, Lavq;->a:Lawg;

    .line 555
    iget-object v0, p0, Lavq;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 556
    iget-object v0, p0, Lavq;->k:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 557
    iput-object v1, p0, Lavq;->l:Ljfd;

    .line 558
    iput-object v1, p0, Lavq;->m:[B

    .line 559
    iput-object v1, p0, Lavq;->n:Ljava/lang/String;

    .line 560
    iput-boolean v2, p0, Lavq;->V:Z

    .line 562
    :cond_0
    iget-object v0, p0, Lavq;->x:Lbor;

    invoke-virtual {v0}, Lbor;->j()V

    .line 563
    iget-object v0, p0, Lavq;->y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 564
    iput v2, p0, Lavq;->z:I

    .line 565
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lavq;->W:Ljava/util/List;

    .line 566
    iput-object v1, p0, Lavq;->o:Ljava/lang/String;

    .line 567
    iput-boolean v2, p0, Lavq;->p:Z

    .line 568
    iput-object v1, p0, Lavq;->X:Ljava/util/List;

    .line 569
    iput-boolean v2, p0, Lavq;->w:Z

    .line 570
    iput-wide v4, p0, Lavq;->u:J

    .line 571
    sget-object v0, Lbza;->a:Lbza;

    iput-object v0, p0, Lavq;->t:Lbza;

    .line 572
    const-string v0, ""

    iput-object v0, p0, Lavq;->v:Ljava/lang/String;

    .line 573
    const/4 v0, 0x1

    iput-boolean v0, p0, Lavq;->L:Z

    .line 574
    iput-boolean v2, p0, Lavq;->M:Z

    .line 575
    invoke-virtual {p0, v2}, Lavq;->b(Z)V

    .line 576
    iput-wide v4, p0, Lavq;->S:J

    .line 577
    iput-boolean v2, p0, Lavq;->F:Z

    .line 578
    return-void
.end method

.method b(Z)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 581
    iput-boolean p1, p0, Lavq;->B:Z

    .line 583
    iput-boolean v0, p0, Lavq;->C:Z

    .line 584
    iput-boolean v0, p0, Lavq;->D:Z

    .line 585
    iput-boolean v0, p0, Lavq;->E:Z

    .line 586
    iput-object v2, p0, Lavq;->G:Latu;

    .line 587
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lavq;->H:J

    .line 588
    iput-object v2, p0, Lavq;->I:Ljava/lang/String;

    .line 589
    iput-object v2, p0, Lavq;->J:Ljava/lang/String;

    .line 590
    iput-object v2, p0, Lavq;->K:Ljava/lang/String;

    .line 591
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 435
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 440
    iget-object v0, p0, Lavq;->h:Ljava/util/List;

    invoke-static {}, Lavo;->bm()[Landroid/net/Uri;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 441
    iget-object v0, p0, Lavq;->i:Ljava/util/List;

    invoke-static {}, Lavo;->bn()[Lavz;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->writeParcelableArray([Landroid/os/Parcelable;I)V

    .line 442
    iget-object v0, p0, Lavq;->q:Landroid/net/Uri;

    if-eqz v0, :cond_9

    .line 443
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeByte(B)V

    .line 444
    iget-object v0, p0, Lavq;->q:Landroid/net/Uri;

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 448
    :goto_0
    iget-boolean v0, p0, Lavq;->r:Z

    if-eqz v0, :cond_a

    move v0, v2

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 449
    iget-boolean v0, p0, Lavq;->V:Z

    if-eqz v0, :cond_b

    move v0, v2

    :goto_2
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 450
    iget-boolean v0, p0, Lavq;->s:Z

    if-eqz v0, :cond_c

    move v0, v2

    :goto_3
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 451
    iget-object v0, p0, Lavq;->t:Lbza;

    invoke-virtual {v0}, Lbza;->ordinal()I

    move-result v0

    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 452
    iget-wide v0, p0, Lavq;->u:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 453
    iget-object v0, p0, Lavq;->v:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 454
    iget-boolean v0, p0, Lavq;->w:Z

    if-eqz v0, :cond_d

    move v0, v2

    :goto_4
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 455
    iget-object v0, p0, Lavq;->x:Lbor;

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 456
    iget-object v0, p0, Lavq;->y:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 457
    iget v0, p0, Lavq;->z:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 458
    iget-object v0, p0, Lavq;->W:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 459
    iget-object v0, p0, Lavq;->X:Ljava/util/List;

    if-eqz v0, :cond_e

    move v0, v2

    :goto_5
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 460
    iget-object v0, p0, Lavq;->X:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 461
    iget-object v0, p0, Lavq;->X:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 463
    :cond_0
    iget-object v0, p0, Lavq;->A:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 464
    iget-boolean v0, p0, Lavq;->b:Z

    if-eqz v0, :cond_f

    move v0, v2

    :goto_6
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 465
    iget-boolean v0, p0, Lavq;->g:Z

    if-eqz v0, :cond_10

    move v0, v2

    :goto_7
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 466
    iget-boolean v0, p0, Lavq;->B:Z

    if-eqz v0, :cond_11

    move v0, v2

    :goto_8
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 467
    iget-boolean v0, p0, Lavq;->B:Z

    if-eqz v0, :cond_1

    .line 468
    iget-boolean v0, p0, Lavq;->C:Z

    if-eqz v0, :cond_12

    move v0, v2

    :goto_9
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 469
    iget-boolean v0, p0, Lavq;->D:Z

    if-eqz v0, :cond_13

    move v0, v2

    :goto_a
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 470
    iget-boolean v0, p0, Lavq;->E:Z

    if-eqz v0, :cond_14

    move v0, v2

    :goto_b
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 471
    iget-object v0, p0, Lavq;->G:Latu;

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 472
    iget-wide v0, p0, Lavq;->H:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 473
    iget-object v0, p0, Lavq;->I:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 474
    iget-object v0, p0, Lavq;->J:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 475
    iget-object v0, p0, Lavq;->K:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 477
    :cond_1
    iget-boolean v0, p0, Lavq;->L:Z

    if-eqz v0, :cond_15

    move v0, v2

    :goto_c
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 478
    iget-object v0, p0, Lavq;->N:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 479
    iget-object v0, p0, Lavq;->O:Ljed;

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 480
    iget-object v0, p0, Lavq;->P:Ljdz;

    if-eqz v0, :cond_16

    move v0, v2

    :goto_d
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 481
    iget-object v0, p0, Lavq;->P:Ljdz;

    if-eqz v0, :cond_2

    .line 482
    iget-object v0, p0, Lavq;->P:Ljdz;

    invoke-virtual {v0, p1, v3}, Ljdz;->writeToParcel(Landroid/os/Parcel;I)V

    .line 484
    :cond_2
    iget-object v0, p0, Lavq;->Q:Landroid/net/Uri;

    if-eqz v0, :cond_17

    move v0, v2

    :goto_e
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 485
    iget-object v0, p0, Lavq;->Q:Landroid/net/Uri;

    if-eqz v0, :cond_3

    .line 486
    iget-object v0, p0, Lavq;->Q:Landroid/net/Uri;

    invoke-virtual {v0, p1, v3}, Landroid/net/Uri;->writeToParcel(Landroid/os/Parcel;I)V

    .line 488
    :cond_3
    iget-object v0, p0, Lavq;->R:Ljava/lang/String;

    if-eqz v0, :cond_18

    move v0, v2

    :goto_f
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 489
    iget-object v0, p0, Lavq;->R:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 490
    iget-object v0, p0, Lavq;->R:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 492
    :cond_4
    iget-object v0, p0, Lavq;->l:Ljfd;

    if-eqz v0, :cond_19

    move v0, v2

    :goto_10
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 493
    iget-object v0, p0, Lavq;->l:Ljfd;

    if-eqz v0, :cond_5

    .line 494
    iget-object v0, p0, Lavq;->l:Ljfd;

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 496
    :cond_5
    iget-object v0, p0, Lavq;->m:[B

    if-eqz v0, :cond_1a

    move v0, v2

    :goto_11
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 497
    iget-object v0, p0, Lavq;->m:[B

    if-eqz v0, :cond_6

    .line 498
    iget-object v0, p0, Lavq;->m:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 500
    :cond_6
    iget-object v0, p0, Lavq;->n:Ljava/lang/String;

    if-eqz v0, :cond_1b

    move v0, v2

    :goto_12
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 501
    iget-object v0, p0, Lavq;->n:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 502
    iget-object v0, p0, Lavq;->n:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 504
    :cond_7
    iget-object v0, p0, Lavq;->o:Ljava/lang/String;

    if-eqz v0, :cond_1c

    move v0, v2

    :goto_13
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 505
    iget-object v0, p0, Lavq;->o:Ljava/lang/String;

    if-eqz v0, :cond_8

    .line 506
    iget-object v0, p0, Lavq;->o:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 508
    :cond_8
    iget-boolean v0, p0, Lavq;->p:Z

    if-eqz v0, :cond_1d

    move v0, v2

    :goto_14
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 509
    iget-object v0, p0, Lavq;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 510
    iget-object v0, p0, Lavq;->j:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_15
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 511
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljdx;

    invoke-virtual {v1, p1, v3}, Ljdx;->writeToParcel(Landroid/os/Parcel;I)V

    .line 512
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    goto :goto_15

    .line 446
    :cond_9
    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeByte(B)V

    goto/16 :goto_0

    :cond_a
    move v0, v3

    .line 448
    goto/16 :goto_1

    :cond_b
    move v0, v3

    .line 449
    goto/16 :goto_2

    :cond_c
    move v0, v3

    .line 450
    goto/16 :goto_3

    :cond_d
    move v0, v3

    .line 454
    goto/16 :goto_4

    :cond_e
    move v0, v3

    .line 459
    goto/16 :goto_5

    :cond_f
    move v0, v3

    .line 464
    goto/16 :goto_6

    :cond_10
    move v0, v3

    .line 465
    goto/16 :goto_7

    :cond_11
    move v0, v3

    .line 466
    goto/16 :goto_8

    :cond_12
    move v0, v3

    .line 468
    goto/16 :goto_9

    :cond_13
    move v0, v3

    .line 469
    goto/16 :goto_a

    :cond_14
    move v0, v3

    .line 470
    goto/16 :goto_b

    :cond_15
    move v0, v3

    .line 477
    goto/16 :goto_c

    :cond_16
    move v0, v3

    .line 480
    goto/16 :goto_d

    :cond_17
    move v0, v3

    .line 484
    goto/16 :goto_e

    :cond_18
    move v0, v3

    .line 488
    goto/16 :goto_f

    :cond_19
    move v0, v3

    .line 492
    goto/16 :goto_10

    :cond_1a
    move v0, v3

    .line 496
    goto/16 :goto_11

    :cond_1b
    move v0, v3

    .line 500
    goto/16 :goto_12

    :cond_1c
    move v0, v3

    .line 504
    goto/16 :goto_13

    :cond_1d
    move v0, v3

    .line 508
    goto :goto_14

    .line 514
    :cond_1e
    iget-wide v0, p0, Lavq;->S:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 515
    iget-boolean v0, p0, Lavq;->T:Z

    if-eqz v0, :cond_1f

    move v0, v2

    :goto_16
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 516
    iget v0, p0, Lavq;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 517
    iget-object v0, p0, Lavq;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 518
    iget v0, p0, Lavq;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 519
    iget-object v0, p0, Lavq;->U:Landroid/content/Intent;

    invoke-virtual {p1, v0, v3}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 520
    iget-boolean v0, p0, Lavq;->f:Z

    if-eqz v0, :cond_20

    move v0, v2

    :goto_17
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 521
    iget-boolean v0, p0, Lavq;->F:Z

    if-eqz v0, :cond_21

    :goto_18
    int-to-byte v0, v2

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 522
    return-void

    :cond_1f
    move v0, v3

    .line 515
    goto :goto_16

    :cond_20
    move v0, v3

    .line 520
    goto :goto_17

    :cond_21
    move v2, v3

    .line 521
    goto :goto_18
.end method
