.class public final enum Lbmv;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbmv;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Lbmv;

.field public static final enum b:Lbmv;

.field private static final synthetic c:[Lbmv;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    new-instance v0, Lbmv;

    const-string v1, "VIDEO"

    invoke-direct {v0, v1, v2}, Lbmv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbmv;->a:Lbmv;

    new-instance v0, Lbmv;

    const-string v1, "PHOTO"

    invoke-direct {v0, v1, v3}, Lbmv;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lbmv;->b:Lbmv;

    .line 19
    const/4 v0, 0x2

    new-array v0, v0, [Lbmv;

    sget-object v1, Lbmv;->a:Lbmv;

    aput-object v1, v0, v2

    sget-object v1, Lbmv;->b:Lbmv;

    aput-object v1, v0, v3

    sput-object v0, Lbmv;->c:[Lbmv;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lbmv;
    .locals 1

    .prologue
    .line 19
    const-class v0, Lbmv;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbmv;

    return-object v0
.end method

.method public static values()[Lbmv;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lbmv;->c:[Lbmv;

    invoke-virtual {v0}, [Lbmv;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbmv;

    return-object v0
.end method
