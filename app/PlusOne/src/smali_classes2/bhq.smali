.class public final Lbhq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lbgf;

.field private final b:Ljava/util/concurrent/Executor;

.field private final c:Lbjf;

.field private final d:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lbgf;Ljava/util/concurrent/Executor;Lbjf;Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 431
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 432
    const-string v0, "renderContext"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgf;

    iput-object v0, p0, Lbhq;->a:Lbgf;

    .line 433
    const-string v0, "backgroundExecutor"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lbhq;->b:Ljava/util/concurrent/Executor;

    .line 434
    const-string v0, "bitmapFactory"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjf;

    iput-object v0, p0, Lbhq;->c:Lbjf;

    .line 435
    const-string v0, "context"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbhq;->d:Landroid/content/Context;

    .line 436
    return-void
.end method


# virtual methods
.method public a()Lbjb;
    .locals 5

    .prologue
    .line 439
    new-instance v0, Lbjb;

    iget-object v1, p0, Lbhq;->a:Lbgf;

    iget-object v2, p0, Lbhq;->d:Landroid/content/Context;

    iget-object v3, p0, Lbhq;->b:Ljava/util/concurrent/Executor;

    iget-object v4, p0, Lbhq;->c:Lbjf;

    invoke-direct {v0, v1, v2, v3, v4}, Lbjb;-><init>(Lbgf;Landroid/content/Context;Ljava/util/concurrent/Executor;Lbjf;)V

    return-object v0
.end method
