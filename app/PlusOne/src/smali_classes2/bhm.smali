.class public final Lbhm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbhr;
.implements Lbhs;
.implements Lcgk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbhr",
        "<",
        "Lbhl;",
        ">;",
        "Lbhs;",
        "Lcgk;"
    }
.end annotation


# instance fields
.field private final a:Lbgf;

.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lbhl;

.field private e:J

.field private f:I


# direct methods
.method public constructor <init>(Lbgf;)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbhm;->b:Ljava/util/List;

    .line 24
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbhm;->c:Ljava/util/List;

    .line 34
    const-string v0, "renderContext"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgf;

    iput-object v0, p0, Lbhm;->a:Lbgf;

    .line 35
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lbhm;->d:Lbhl;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lbhm;->d:Lbhl;

    iget-object v0, v0, Lbhl;->c:Lbhb;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lbhm;->d:Lbhl;

    .line 112
    :cond_0
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 18
    return-void
.end method

.method public a(J)Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 39
    const-string v0, "timestampUs"

    invoke-static {p1, p2, v0}, Lcec;->b(JLjava/lang/CharSequence;)J

    .line 41
    iput v2, p0, Lbhm;->f:I

    :goto_0
    iget v0, p0, Lbhm;->f:I

    iget-object v1, p0, Lbhm;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 42
    iget-object v0, p0, Lbhm;->b:Ljava/util/List;

    iget v1, p0, Lbhm;->f:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 43
    iget-object v1, p0, Lbhm;->c:Ljava/util/List;

    iget v3, p0, Lbhm;->f:I

    .line 44
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long v4, p1, v4

    .line 45
    invoke-virtual {v0}, Lbmd;->b()J

    move-result-wide v6

    cmp-long v1, v4, v6

    if-gez v1, :cond_0

    .line 48
    const-wide/16 v2, 0x0

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iget-object v0, v0, Lbmd;->f:Lbmp;

    iget-wide v0, v0, Lbmp;->b:J

    add-long/2addr v0, v2

    iput-wide v0, p0, Lbhm;->e:J

    .line 49
    const/4 v0, 0x1

    .line 53
    :goto_1
    return v0

    .line 41
    :cond_0
    iget v0, p0, Lbhm;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbhm;->f:I

    goto :goto_0

    :cond_1
    move v0, v2

    .line 53
    goto :goto_1
.end method

.method public a(Lbop;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 63
    const-string v0, "timeline"

    const/4 v2, 0x0

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 65
    iget-object v0, p0, Lbhm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 66
    iget-object v0, p0, Lbhm;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    move v0, v1

    .line 67
    :goto_0
    invoke-interface {p1}, Lbop;->e()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 68
    invoke-interface {p1, v0}, Lbop;->b(I)Lbmd;

    move-result-object v2

    .line 69
    iget-object v3, v2, Lbmd;->d:Lbmg;

    sget-object v4, Lbmg;->d:Lbmg;

    if-ne v3, v4, :cond_0

    .line 70
    iget-object v3, p0, Lbhm;->b:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 71
    iget-object v2, p0, Lbhm;->c:Ljava/util/List;

    invoke-interface {p1, v0}, Lbop;->f(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 67
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 74
    :cond_1
    iget-object v0, p0, Lbhm;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v1, 0x1

    :cond_2
    return v1
.end method

.method public b()Lbhl;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 79
    iget v0, p0, Lbhm;->f:I

    const-string v1, "mCurrentIndex"

    iget-object v2, p0, Lbhm;->b:Ljava/util/List;

    invoke-static {v0, v1, v2}, Lcgp;->a(ILjava/lang/CharSequence;Ljava/util/Collection;)I

    .line 81
    iget-object v0, p0, Lbhm;->d:Lbhl;

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Lbhl;

    iget-object v1, p0, Lbhm;->a:Lbgf;

    invoke-virtual {v1, v4, v4}, Lbgf;->b(II)Lbfw;

    move-result-object v1

    invoke-direct {v0, v1}, Lbhl;-><init>(Lbhb;)V

    iput-object v0, p0, Lbhm;->d:Lbhl;

    .line 83
    iget-object v0, p0, Lbhm;->d:Lbhl;

    iget-object v0, v0, Lbhl;->d:[F

    invoke-static {v0}, Lbqu;->a([F)V

    .line 85
    :cond_0
    iget-object v0, p0, Lbhm;->d:Lbhl;

    iget-wide v2, p0, Lbhm;->e:J

    iput-wide v2, v0, Lbhl;->a:J

    .line 86
    iget-wide v0, p0, Lbhm;->e:J

    const-wide/32 v2, 0x80e8

    add-long/2addr v0, v2

    iput-wide v0, p0, Lbhm;->e:J

    .line 88
    iget-wide v2, p0, Lbhm;->e:J

    iget-object v0, p0, Lbhm;->b:Ljava/util/List;

    iget v1, p0, Lbhm;->f:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-object v0, v0, Lbmd;->f:Lbmp;

    iget-wide v0, v0, Lbmp;->c:J

    cmp-long v0, v2, v0

    if-ltz v0, :cond_2

    .line 89
    iget-object v0, p0, Lbhm;->d:Lbhl;

    iput-boolean v4, v0, Lbhl;->b:Z

    .line 90
    iget v0, p0, Lbhm;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbhm;->f:I

    .line 91
    iget v0, p0, Lbhm;->f:I

    iget-object v1, p0, Lbhm;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 92
    iget-object v0, p0, Lbhm;->b:Ljava/util/List;

    iget v1, p0, Lbhm;->f:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-object v0, v0, Lbmd;->f:Lbmp;

    iget-wide v0, v0, Lbmp;->b:J

    iput-wide v0, p0, Lbhm;->e:J

    .line 98
    :cond_1
    :goto_0
    iget-object v0, p0, Lbhm;->d:Lbhl;

    return-object v0

    .line 95
    :cond_2
    iget-object v0, p0, Lbhm;->d:Lbhl;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lbhl;->b:Z

    goto :goto_0
.end method

.method public synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 18
    invoke-virtual {p0}, Lbhm;->b()Lbhl;

    move-result-object v0

    return-object v0
.end method
