.class final Lgod;
.super Lgnm;


# instance fields
.field private final a:Lgfy;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgfy",
            "<",
            "Lgzb;",
            ">;"
        }
    .end annotation
.end field

.field private synthetic b:Lgny;


# direct methods
.method public constructor <init>(Lgny;Lgfy;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfy",
            "<",
            "Lgzb;",
            ">;)V"
        }
    .end annotation

    iput-object p1, p0, Lgod;->b:Lgny;

    invoke-direct {p0}, Lgnm;-><init>()V

    iput-object p2, p0, Lgod;->a:Lgfy;

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 6

    const/4 v0, 0x0

    const-string v1, "PeopleService"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bundle callback: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nresolution="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nbundle="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-static {p1, v0, p2}, Lgny;->a(ILjava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    if-nez p3, :cond_2

    :goto_0
    if-eqz p3, :cond_1

    const-string v2, "circle_name"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    :cond_1
    iget-object v2, p0, Lgod;->b:Lgny;

    new-instance v3, Lgoe;

    iget-object v4, p0, Lgod;->b:Lgny;

    iget-object v5, p0, Lgod;->a:Lgfy;

    invoke-direct {v3, v4, v5, v1, v0}, Lgoe;-><init>(Lgny;Lgfy;Lcom/google/android/gms/common/api/Status;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Lgny;->b(Lghj;)V

    return-void

    :cond_2
    const-string v0, "circle_id"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
