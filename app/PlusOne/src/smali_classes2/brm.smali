.class public final Lbrm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:[J

.field private final b:[F


# direct methods
.method constructor <init>([J[F)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    array-length v0, p1

    array-length v1, p2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Array length should match"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 33
    iput-object p1, p0, Lbrm;->a:[J

    .line 34
    iput-object p2, p0, Lbrm;->b:[F

    .line 35
    return-void

    .line 31
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lbrm;->a:[J

    array-length v0, v0

    return v0
.end method

.method public a(J)I
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lbrm;->a:[J

    invoke-static {p1, p2, v0}, Lcgv;->a(J[J)I

    move-result v0

    return v0
.end method

.method public a(I)J
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lbrm;->a:[J

    aget-wide v0, v0, p1

    return-wide v0
.end method

.method public b(I)F
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lbrm;->b:[F

    aget v0, v0, p1

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 64
    if-ne p1, p0, :cond_1

    .line 70
    :cond_0
    :goto_0
    return v0

    .line 66
    :cond_1
    instance-of v2, p1, Lbrm;

    if-nez v2, :cond_2

    move v0, v1

    .line 67
    goto :goto_0

    .line 69
    :cond_2
    check-cast p1, Lbrm;

    .line 70
    iget-object v2, p0, Lbrm;->b:[F

    iget-object v3, p1, Lbrm;->b:[F

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([F[F)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lbrm;->a:[J

    iget-object v3, p1, Lbrm;->a:[J

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([J[J)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 75
    iget-object v0, p0, Lbrm;->b:[F

    array-length v1, v0

    .line 76
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lbrm;->b:[F

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 77
    iget-object v2, p0, Lbrm;->b:[F

    aget v2, v2, v0

    invoke-static {v2, v1}, Lbqh;->a(FI)I

    move-result v1

    .line 78
    iget-object v2, p0, Lbrm;->a:[J

    aget-wide v2, v2, v0

    invoke-static {v2, v3, v1}, Lbqh;->a(JI)I

    move-result v1

    .line 76
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80
    :cond_0
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 7

    .prologue
    .line 55
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 56
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lbrm;->a:[J

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 57
    iget-object v2, p0, Lbrm;->a:[J

    aget-wide v2, v2, v0

    iget-object v4, p0, Lbrm;->b:[F

    aget v4, v4, v0

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x25

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 56
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 59
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
