.class public final Layf;
.super Layr;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/32 v4, 0x61a80

    .line 24
    new-instance v0, Lays;

    invoke-direct {v0}, Lays;-><init>()V

    const-wide/32 v2, 0xf4240

    .line 25
    invoke-virtual {v0, v2, v3}, Lays;->a(J)Lays;

    move-result-object v0

    .line 26
    invoke-virtual {v0, v4, v5}, Lays;->b(J)Lays;

    move-result-object v0

    .line 27
    invoke-virtual {v0, v4, v5}, Lays;->c(J)Lays;

    move-result-object v0

    .line 28
    invoke-virtual {v0, v4, v5}, Lays;->d(J)Lays;

    move-result-object v0

    const v1, 0x7f020421

    .line 29
    invoke-virtual {v0, v1}, Lays;->a(I)Lays;

    move-result-object v0

    const v1, 0x7f02040a

    .line 30
    invoke-virtual {v0, v1}, Lays;->b(I)Lays;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 31
    invoke-virtual {v0, v1}, Lays;->a(F)Lays;

    move-result-object v0

    const/4 v1, 0x1

    .line 32
    invoke-virtual {v0, v1}, Lays;->c(I)Lays;

    move-result-object v0

    .line 24
    invoke-direct {p0, v0}, Layr;-><init>(Lays;)V

    .line 34
    return-void
.end method


# virtual methods
.method protected a(Layj;Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 39
    new-instance v0, Layu;

    invoke-direct {v0}, Layu;-><init>()V

    .line 40
    invoke-virtual {v0, p1}, Layu;->a(Layj;)Layu;

    move-result-object v0

    .line 41
    invoke-virtual {v0, v2}, Layu;->e(I)Layu;

    move-result-object v0

    const/4 v1, 0x2

    .line 42
    invoke-virtual {v0, v1}, Layu;->f(I)Layu;

    move-result-object v0

    const/high16 v1, 0x42300000    # 44.0f

    .line 43
    invoke-virtual {v0, v1}, Layu;->b(F)Layu;

    move-result-object v0

    const v1, 0x7f0b009c

    .line 44
    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Layu;->a(I)Layu;

    move-result-object v0

    const/high16 v1, 0x42100000    # 36.0f

    .line 45
    invoke-virtual {v0, v1}, Layu;->a(F)Layu;

    move-result-object v0

    const-string v1, "sans-serif-condensed"

    .line 46
    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Layu;->a(Landroid/graphics/Typeface;)Layu;

    move-result-object v0

    .line 47
    invoke-virtual {v0, v2}, Layu;->a(Z)Layu;

    move-result-object v0

    const/16 v1, 0x1f4

    .line 48
    invoke-virtual {v0, v1}, Layu;->c(I)Layu;

    move-result-object v0

    const v1, 0x7f0b009d

    .line 49
    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Layu;->h(I)Layu;

    move-result-object v0

    const/4 v1, 0x0

    .line 50
    invoke-virtual {v0, v1}, Layu;->b(Z)Layu;

    move-result-object v0

    sget-object v1, Layc;->d:Layc;

    .line 51
    invoke-virtual {v0, v1}, Layu;->a(Layc;)Layu;

    move-result-object v0

    const-wide/32 v2, 0xf4240

    .line 52
    invoke-virtual {v0, v2, v3}, Layu;->b(J)Layu;

    move-result-object v0

    .line 53
    invoke-virtual {v0}, Layu;->a()Layt;

    move-result-object v0

    return-object v0
.end method

.method public b(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 73
    invoke-static {p1}, Lbag;->d(Layl;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    invoke-virtual {p0}, Layf;->a()Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->b(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    .line 76
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Layr;->b(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    goto :goto_0
.end method

.method public b(Lbmd;)Lool;
    .locals 9

    .prologue
    .line 59
    iget-object v0, p1, Lbmd;->d:Lbmg;

    sget-object v1, Lbmg;->c:Lbmg;

    if-eq v0, v1, :cond_0

    .line 60
    const/4 v0, 0x0

    .line 62
    :goto_0
    return-object v0

    :cond_0
    const-wide v0, 0x3ff0cccccccccccdL    # 1.05

    const-wide v2, 0x3f947ae147ae147bL    # 0.02

    const-wide v4, 0x3fa999999999999aL    # 0.05

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    move-object v8, p1

    invoke-static/range {v0 .. v8}, Lbah;->a(DDDDLbmd;)Lool;

    move-result-object v0

    goto :goto_0
.end method

.method protected f()Layj;
    .locals 1

    .prologue
    .line 68
    new-instance v0, Lazc;

    invoke-direct {v0, p0}, Lazc;-><init>(Layj;)V

    return-object v0
.end method
