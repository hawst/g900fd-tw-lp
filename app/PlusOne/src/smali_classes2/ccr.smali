.class public final Lccr;
.super Lip;
.source "PG"


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbza;",
            ">;"
        }
    .end annotation
.end field

.field private synthetic b:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbza;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 235
    iput-object p1, p0, Lccr;->b:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-direct {p0}, Lip;-><init>()V

    .line 236
    iput-object p2, p0, Lccr;->a:Ljava/util/List;

    .line 237
    return-void
.end method


# virtual methods
.method public a(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 246
    iget-object v0, p0, Lccr;->b:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 249
    const v1, 0x7f04021f

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    .line 250
    const v0, 0x7f100603

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;

    .line 251
    iget-object v1, p0, Lccr;->a:Ljava/util/List;

    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbza;

    iget v1, v1, Lbza;->n:I

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->setText(I)V

    .line 252
    iget-object v1, p0, Lccr;->b:Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;

    invoke-virtual {v1}, Lcom/google/android/apps/moviemaker/ui/ThemeSelectorView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-object v1, p0, Lccr;->a:Ljava/util/List;

    .line 253
    invoke-interface {v1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbza;

    iget v1, v1, Lbza;->o:I

    .line 252
    invoke-virtual {v3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->setBackgroundColor(I)V

    .line 254
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/ThemeItemView;->setTag(Ljava/lang/Object;)V

    .line 255
    invoke-virtual {p1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 256
    return-object v2
.end method

.method public a(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    .prologue
    .line 261
    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 262
    return-void
.end method

.method public a(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 266
    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lccr;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
