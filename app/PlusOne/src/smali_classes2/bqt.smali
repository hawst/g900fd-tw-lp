.class public final Lbqt;
.super Lbqs;
.source "PG"


# instance fields
.field private b:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0}, Lbqs;-><init>()V

    .line 17
    const-string v0, "bitmap"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lbqt;->b:Landroid/graphics/Bitmap;

    .line 18
    return-void
.end method


# virtual methods
.method protected c()V
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lbqt;->b:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 28
    return-void
.end method

.method public final d()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lbqs;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v0

    if-gtz v0, :cond_0

    new-instance v0, Lbqr;

    invoke-direct {v0}, Lbqr;-><init>()V

    throw v0

    .line 22
    :cond_0
    iget-object v0, p0, Lbqt;->b:Landroid/graphics/Bitmap;

    return-object v0
.end method
