.class final Laqu;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lboi;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Laqc;


# direct methods
.method constructor <init>(Laqc;)V
    .locals 0

    .prologue
    .line 1570
    iput-object p1, p0, Laqu;->a:Laqc;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Lboi;)Ljava/lang/Void;
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, 0x0

    .line 1574
    aget-object v1, p1, v0

    .line 1575
    invoke-virtual {v1}, Lboi;->f()Ljava/util/List;

    move-result-object v2

    .line 1577
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 1578
    iget-object v4, v0, Lbmd;->d:Lbmg;

    sget-object v5, Lbmg;->c:Lbmg;

    if-ne v4, v5, :cond_4

    iget-object v0, v0, Lbmd;->e:Ljeg;

    iget-object v0, v0, Ljeg;->b:Landroid/net/Uri;

    iget-object v4, p0, Laqu;->a:Laqc;

    .line 1580
    invoke-static {v4}, Laqc;->E(Laqc;)Landroid/content/Context;

    move-result-object v4

    .line 1579
    invoke-static {v0, v4}, Lbje;->b(Landroid/net/Uri;Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1581
    add-int/lit8 v1, v1, 0x1

    move v0, v1

    :goto_1
    move v1, v0

    .line 1583
    goto :goto_0

    .line 1584
    :cond_0
    if-nez v1, :cond_2

    .line 1599
    :cond_1
    return-object v6

    .line 1588
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 1589
    invoke-virtual {p0}, Laqu;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1592
    iget-object v2, v0, Lbmd;->d:Lbmg;

    sget-object v3, Lbmg;->c:Lbmg;

    if-ne v2, v3, :cond_3

    iget-object v2, v0, Lbmd;->e:Ljeg;

    iget-object v2, v2, Ljeg;->b:Landroid/net/Uri;

    iget-object v3, p0, Laqu;->a:Laqc;

    .line 1594
    invoke-static {v3}, Laqc;->E(Laqc;)Landroid/content/Context;

    move-result-object v3

    .line 1593
    invoke-static {v2, v3}, Lbje;->b(Landroid/net/Uri;Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1595
    iget-object v0, v0, Lbmd;->e:Ljeg;

    iget-object v0, v0, Ljeg;->b:Landroid/net/Uri;

    iget-object v2, p0, Laqu;->a:Laqc;

    .line 1596
    invoke-static {v2}, Laqc;->F(Laqc;)Lbjf;

    move-result-object v2

    iget-object v3, p0, Laqu;->a:Laqc;

    invoke-static {v3}, Laqc;->E(Laqc;)Landroid/content/Context;

    move-result-object v3

    .line 1595
    invoke-static {v0, v2, v3}, Lbje;->a(Landroid/net/Uri;Lbjf;Landroid/content/Context;)V

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method protected a()V
    .locals 2

    .prologue
    .line 1604
    iget-object v0, p0, Laqu;->a:Laqc;

    invoke-static {v0}, Laqc;->b(Laqc;)Lawf;

    move-result-object v0

    invoke-interface {v0}, Lawf;->G()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1605
    iget-object v0, p0, Laqu;->a:Laqc;

    invoke-static {v0}, Laqc;->A(Laqc;)V

    .line 1607
    :cond_0
    iget-object v0, p0, Laqu;->a:Laqc;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Laqc;->a(Laqc;Laqu;)Laqu;

    .line 1608
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1570
    check-cast p1, [Lboi;

    invoke-virtual {p0, p1}, Laqu;->a([Lboi;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 1570
    invoke-virtual {p0}, Laqu;->a()V

    return-void
.end method
