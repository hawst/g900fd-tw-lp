.class public abstract Lbji;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbhr;
.implements Lcgk;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Lbhj;",
        ">",
        "Ljava/lang/Object;",
        "Lbhr",
        "<TT;>;",
        "Lcgk;"
    }
.end annotation


# static fields
.field private static final e:Ljava/lang/String;


# instance fields
.field public final a:Lbjs;

.field public b:Landroid/media/MediaFormat;

.field public c:Landroid/media/MediaFormat;

.field d:I

.field private final f:Ljava/util/concurrent/Executor;

.field private final g:Ljava/util/concurrent/FutureTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/FutureTask",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Landroid/media/MediaCodec$BufferInfo;

.field private i:I

.field private j:Lbhi;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbhi",
            "<",
            "Ljava/nio/ByteBuffer;",
            "Lbix;",
            ">;"
        }
    .end annotation
.end field

.field private k:[Ljava/nio/ByteBuffer;

.field private l:I

.field private m:[Ljava/nio/ByteBuffer;

.field private n:J

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lbji;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbji;->e:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lbjs;Ljava/util/concurrent/Executor;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 135
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, Lbjj;

    invoke-direct {v1, p0}, Lbjj;-><init>(Lbji;)V

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/lang/Runnable;Ljava/lang/Object;)V

    iput-object v0, p0, Lbji;->g:Ljava/util/concurrent/FutureTask;

    .line 86
    const/4 v0, 0x0

    iput v0, p0, Lbji;->i:I

    .line 95
    const/4 v0, -0x1

    iput v0, p0, Lbji;->l:I

    .line 100
    const/16 v0, -0xff

    iput v0, p0, Lbji;->d:I

    .line 106
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbji;->n:J

    .line 136
    const-string v0, "codec"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjs;

    iput-object v0, p0, Lbji;->a:Lbjs;

    .line 137
    const-string v0, "backgroundExecutor"

    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lbji;->f:Ljava/util/concurrent/Executor;

    .line 139
    new-instance v0, Landroid/media/MediaCodec$BufferInfo;

    invoke-direct {v0}, Landroid/media/MediaCodec$BufferInfo;-><init>()V

    iput-object v0, p0, Lbji;->h:Landroid/media/MediaCodec$BufferInfo;

    .line 140
    return-void
.end method

.method private e()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 259
    iget v2, p0, Lbji;->i:I

    packed-switch v2, :pswitch_data_0

    .line 294
    iget v0, p0, Lbji;->i:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x27

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unhandled MediaCodec state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 261
    :pswitch_0
    iput v1, p0, Lbji;->i:I

    .line 262
    iget-object v1, p0, Lbji;->f:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Lbji;->g:Ljava/util/concurrent/FutureTask;

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 290
    :cond_0
    :goto_0
    return v0

    .line 265
    :pswitch_1
    iget-object v2, p0, Lbji;->g:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v2}, Ljava/util/concurrent/FutureTask;->isDone()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 271
    :try_start_0
    iget-object v2, p0, Lbji;->g:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v2}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    .line 272
    iget-object v2, p0, Lbji;->a:Lbjs;

    invoke-interface {v2}, Lbjs;->g()[Ljava/nio/ByteBuffer;

    move-result-object v2

    iput-object v2, p0, Lbji;->k:[Ljava/nio/ByteBuffer;

    .line 273
    iget-object v2, p0, Lbji;->a:Lbjs;

    invoke-interface {v2}, Lbjs;->h()[Ljava/nio/ByteBuffer;

    move-result-object v2

    iput-object v2, p0, Lbji;->m:[Ljava/nio/ByteBuffer;

    .line 275
    const/16 v2, -0xff

    iput v2, p0, Lbji;->d:I

    .line 276
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lbji;->n:J

    .line 277
    const/4 v2, 0x1

    iput-boolean v2, p0, Lbji;->o:Z

    .line 278
    const/4 v2, 0x2

    iput v2, p0, Lbji;->i:I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    :pswitch_2
    move v0, v1

    .line 290
    goto :goto_0

    .line 280
    :catch_0
    move-exception v1

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 282
    :catch_1
    move-exception v0

    .line 283
    const-string v1, "Exception while starting MediaCodec"

    invoke-static {v1, v0}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/Throwable;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 292
    :pswitch_3
    const-string v0, "Tried to use a MediaCodec that was released."

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 259
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method protected abstract a(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;Z)Lbhj;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/nio/ByteBuffer;",
            "Landroid/media/MediaCodec$BufferInfo;",
            "Z)TT;"
        }
    .end annotation
.end method

.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 184
    iget v0, p0, Lbji;->i:I

    const-string v1, "mMediaCodecState"

    const/4 v2, 0x0

    invoke-static {v0, v1, v3, v2}, Lcgp;->b(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)I

    .line 187
    :try_start_0
    iget v0, p0, Lbji;->i:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lbji;->i:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lbji;->i:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 190
    :cond_0
    iget-object v0, p0, Lbji;->g:Ljava/util/concurrent/FutureTask;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    .line 191
    iget-object v0, p0, Lbji;->a:Lbjs;

    invoke-interface {v0}, Lbjs;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 192
    iget-object v0, p0, Lbji;->a:Lbjs;

    invoke-interface {v0}, Lbjs;->l()V
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    :cond_1
    iget-object v0, p0, Lbji;->a:Lbjs;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 201
    iput v3, p0, Lbji;->i:I

    .line 202
    :goto_0
    return-void

    .line 200
    :catch_0
    move-exception v0

    iget-object v0, p0, Lbji;->a:Lbjs;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 201
    iput v3, p0, Lbji;->i:I

    goto :goto_0

    .line 198
    :catch_1
    move-exception v0

    :try_start_1
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 200
    iget-object v0, p0, Lbji;->a:Lbjs;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 201
    iput v3, p0, Lbji;->i:I

    goto :goto_0

    .line 200
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lbji;->a:Lbjs;

    invoke-static {v1}, Lcgl;->a(Lcgk;)V

    .line 201
    iput v3, p0, Lbji;->i:I

    throw v0
.end method

.method public a(Lbiy;)V
    .locals 2

    .prologue
    .line 147
    iget v0, p0, Lbji;->i:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 148
    invoke-virtual {p0}, Lbji;->d()V

    .line 151
    :cond_0
    const-string v0, "source"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhi;

    iput-object v0, p0, Lbji;->j:Lbhi;

    .line 152
    invoke-interface {p1}, Lbiy;->e()Landroid/media/MediaFormat;

    move-result-object v0

    iput-object v0, p0, Lbji;->b:Landroid/media/MediaFormat;

    .line 153
    return-void
.end method

.method protected final a(Z)V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lbji;->a:Lbjs;

    iget v1, p0, Lbji;->d:I

    invoke-interface {v0, v1, p1}, Lbjs;->a(IZ)V

    .line 211
    return-void
.end method

.method public a(Landroid/media/MediaFormat;)Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x1

    .line 156
    const-string v1, "newFormat"

    invoke-static {p1, v1, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 157
    iget-object v1, p0, Lbji;->b:Landroid/media/MediaFormat;

    const-string v2, "mInputFormat"

    invoke-static {v1, v2, v3}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 159
    iget-object v1, p0, Lbji;->b:Landroid/media/MediaFormat;

    invoke-static {p1, v1}, Lbkf;->a(Landroid/media/MediaFormat;Landroid/media/MediaFormat;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 170
    :cond_0
    :goto_0
    return v0

    .line 163
    :cond_1
    iget-object v1, p0, Lbji;->a:Lbjs;

    invoke-interface {v1}, Lbjs;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "mime"

    invoke-virtual {p1, v1}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lbji;->b:Landroid/media/MediaFormat;

    const-string v3, "mime"

    .line 164
    invoke-virtual {v2, v3}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 163
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 170
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lbhj;
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    const/4 v10, 0x3

    const/4 v7, 0x1

    const/4 v2, 0x0

    const/4 v8, 0x0

    const/4 v9, -0x1

    .line 176
    iget-object v0, p0, Lbji;->j:Lbhi;

    const-string v1, "mSource"

    const-string v3, "set data source before polling"

    invoke-static {v0, v1, v3}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 178
    iget v0, p0, Lbji;->l:I

    if-ne v0, v9, :cond_0

    invoke-direct {p0}, Lbji;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lbji;->n:J

    const-wide/16 v4, -0x1

    cmp-long v0, v0, v4

    if-nez v0, :cond_0

    iget-object v0, p0, Lbji;->a:Lbjs;

    invoke-interface {v0}, Lbjs;->f()I

    move-result v0

    iput v0, p0, Lbji;->l:I

    :cond_0
    iget v0, p0, Lbji;->l:I

    if-eq v0, v9, :cond_3

    iput v10, p0, Lbji;->i:I

    iget-object v0, p0, Lbji;->j:Lbhi;

    iget-object v1, p0, Lbji;->k:[Ljava/nio/ByteBuffer;

    iget v3, p0, Lbji;->l:I

    aget-object v1, v1, v3

    invoke-interface {v0, v1}, Lbhi;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v4, v0

    check-cast v4, Lbix;

    if-eqz v4, :cond_3

    iget v0, p0, Lbji;->l:I

    const-string v1, "mCurrentInputBufferIndex"

    invoke-static {v0, v1, v9, v8}, Lcgp;->b(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)I

    iget v0, v4, Lbix;->c:I

    const-string v1, "encodedMedia.size"

    invoke-static {v0, v1, v8}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    iget-object v0, v4, Lbix;->b:Ljava/nio/ByteBuffer;

    const-string v1, "encodedMedia.buffer"

    iget-object v3, p0, Lbji;->k:[Ljava/nio/ByteBuffer;

    iget v5, p0, Lbji;->l:I

    aget-object v3, v3, v5

    const-string v5, "encodedMedia input buffer did not match last returned input buffer"

    invoke-static {v0, v1, v3, v5}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/lang/CharSequence;)V

    iget-boolean v0, v4, Lbix;->e:Z

    if-eqz v0, :cond_1

    move v0, v7

    :goto_0
    iget-boolean v1, v4, Lbix;->f:Z

    if-eqz v1, :cond_c

    or-int/lit8 v6, v0, 0x4

    iget-wide v0, v4, Lbix;->d:J

    iput-wide v0, p0, Lbji;->n:J

    :goto_1
    iget-boolean v0, p0, Lbji;->o:Z

    if-eqz v0, :cond_2

    iput-boolean v2, p0, Lbji;->o:Z

    iget-boolean v0, v4, Lbix;->e:Z

    if-nez v0, :cond_2

    const-string v0, "Sample synchronization flag not set when required."

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lbji;->a:Lbjs;

    iget v1, p0, Lbji;->l:I

    iget v3, v4, Lbix;->c:I

    iget-wide v4, v4, Lbix;->d:J

    invoke-interface/range {v0 .. v6}, Lbjs;->a(IIIJI)V

    iput v9, p0, Lbji;->l:I

    .line 179
    :cond_3
    :goto_2
    iget v0, p0, Lbji;->i:I

    if-eq v0, v10, :cond_4

    move-object v0, v8

    :goto_3
    return-object v0

    :cond_4
    iget v0, p0, Lbji;->d:I

    const/16 v1, -0xff

    if-ne v0, v1, :cond_5

    iget-object v0, p0, Lbji;->a:Lbjs;

    iget-object v1, p0, Lbji;->h:Landroid/media/MediaCodec$BufferInfo;

    invoke-interface {v0, v1}, Lbjs;->a(Landroid/media/MediaCodec$BufferInfo;)I

    move-result v0

    :cond_5
    const/4 v1, -0x3

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lbji;->a:Lbjs;

    invoke-interface {v0}, Lbjs;->h()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lbji;->m:[Ljava/nio/ByteBuffer;

    goto :goto_2

    :cond_6
    const/4 v1, -0x2

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lbji;->a:Lbjs;

    invoke-interface {v0}, Lbjs;->i()Landroid/media/MediaFormat;

    move-result-object v0

    iput-object v0, p0, Lbji;->c:Landroid/media/MediaFormat;

    goto :goto_2

    :cond_7
    if-ltz v0, :cond_a

    iget-object v1, p0, Lbji;->h:Landroid/media/MediaCodec$BufferInfo;

    iget v1, v1, Landroid/media/MediaCodec$BufferInfo;->flags:I

    and-int/lit8 v1, v1, 0x4

    if-eqz v1, :cond_8

    iget-object v1, p0, Lbji;->h:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v4, p0, Lbji;->n:J

    iput-wide v4, v1, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    :cond_8
    iput v0, p0, Lbji;->d:I

    iget-object v0, p0, Lbji;->m:[Ljava/nio/ByteBuffer;

    iget v1, p0, Lbji;->d:I

    aget-object v0, v0, v1

    iget-object v1, p0, Lbji;->h:Landroid/media/MediaCodec$BufferInfo;

    iget-object v3, p0, Lbji;->h:Landroid/media/MediaCodec$BufferInfo;

    iget-wide v4, v3, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iget-wide v8, p0, Lbji;->n:J

    cmp-long v3, v4, v8

    if-nez v3, :cond_9

    :goto_4
    invoke-virtual {p0, v0, v1, v7}, Lbji;->a(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;Z)Lbhj;

    move-result-object v0

    goto :goto_3

    :cond_9
    move v7, v2

    goto :goto_4

    :cond_a
    if-ne v0, v9, :cond_b

    move-object v0, v8

    goto :goto_3

    :cond_b
    sget-object v0, Lbji;->e:Ljava/lang/String;

    iget v0, p0, Lbji;->d:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x3b

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "unexpected output buffer index from MediaCodec: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-object v0, v8

    goto :goto_3

    :cond_c
    move v6, v0

    goto/16 :goto_1
.end method

.method public synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 34
    invoke-virtual {p0}, Lbji;->b()Lbhj;

    move-result-object v0

    return-object v0
.end method

.method public d()V
    .locals 7

    .prologue
    const/16 v6, -0xff

    .line 225
    invoke-direct {p0}, Lbji;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    :goto_0
    return-void

    .line 230
    :cond_0
    iget v0, p0, Lbji;->i:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_4

    .line 233
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 234
    :cond_1
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, v0

    const-wide/16 v4, 0x1f4

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    iget-object v2, p0, Lbji;->c:Landroid/media/MediaFormat;

    if-nez v2, :cond_2

    .line 236
    invoke-virtual {p0}, Lbji;->b()Lbhj;

    move-result-object v2

    .line 237
    if-eqz v2, :cond_1

    .line 238
    invoke-virtual {p0, v2}, Lbji;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 242
    :cond_2
    iget-object v0, p0, Lbji;->c:Landroid/media/MediaFormat;

    if-nez v0, :cond_3

    .line 243
    sget-object v0, Lbji;->e:Ljava/lang/String;

    .line 246
    :cond_3
    iget-object v0, p0, Lbji;->a:Lbjs;

    invoke-interface {v0}, Lbjs;->k()V

    .line 247
    iput v6, p0, Lbji;->d:I

    .line 248
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lbji;->n:J

    .line 249
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbji;->o:Z

    .line 250
    const/4 v0, 0x2

    iput v0, p0, Lbji;->i:I

    goto :goto_0

    .line 253
    :cond_4
    iget v0, p0, Lbji;->d:I

    const-string v1, "mPendingOutputBufferIndex"

    const/4 v2, 0x0

    invoke-static {v0, v1, v6, v2}, Lcgp;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    goto :goto_0
.end method
