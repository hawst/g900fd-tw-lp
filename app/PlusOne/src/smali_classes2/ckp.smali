.class public final Lckp;
.super Lu;
.source "PG"


# instance fields
.field private N:Landroid/view/View;

.field private O:Landroid/widget/TextView;

.field private P:Landroid/widget/TextView;

.field private Q:Lcom/google/android/apps/photoeditor/views/ItemSelectorView;

.field private R:I

.field private final S:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

.field private T:Lckq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lu;-><init>()V

    .line 27
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    iput-object v0, p0, Lckp;->S:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 132
    return-void
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 34
    const v0, 0x7f0400a6

    invoke-virtual {p1, v0, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lckp;->N:Landroid/view/View;

    .line 36
    iget-object v0, p0, Lckp;->N:Landroid/view/View;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to inflate the parameter panel fragment"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 40
    :cond_0
    iget-object v0, p0, Lckp;->N:Landroid/view/View;

    const v1, 0x7f10028f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lckp;->O:Landroid/widget/TextView;

    .line 41
    iget-object v0, p0, Lckp;->N:Landroid/view/View;

    const v1, 0x7f10028e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lckp;->P:Landroid/widget/TextView;

    .line 42
    iget-object v0, p0, Lckp;->N:Landroid/view/View;

    const v1, 0x7f100293

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/photoeditor/views/ItemSelectorView;

    iput-object v0, p0, Lckp;->Q:Lcom/google/android/apps/photoeditor/views/ItemSelectorView;

    .line 44
    iget-object v1, p0, Lckp;->S:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    iget-object v0, p0, Lckp;->N:Landroid/view/View;

    const v2, 0x7f100290

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    aput-object v0, v1, v3

    .line 45
    iget-object v1, p0, Lckp;->S:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    const/4 v2, 0x1

    iget-object v0, p0, Lckp;->N:Landroid/view/View;

    const v3, 0x7f100291

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    aput-object v0, v1, v2

    .line 46
    iget-object v1, p0, Lckp;->S:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    const/4 v2, 0x2

    iget-object v0, p0, Lckp;->N:Landroid/view/View;

    const v3, 0x7f100292

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    aput-object v0, v1, v2

    .line 48
    iget-object v0, p0, Lckp;->N:Landroid/view/View;

    return-object v0
.end method

.method public a(IIILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;
    .locals 3

    .prologue
    .line 77
    iget v0, p0, Lckp;->R:I

    iget-object v1, p0, Lckp;->S:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    .line 78
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "There are no available tool button slots"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 81
    :cond_0
    iget-object v0, p0, Lckp;->S:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    iget v1, p0, Lckp;->R:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lckp;->R:I

    aget-object v0, v0, v1

    .line 82
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->a(III)V

    .line 83
    invoke-virtual {v0, p4}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setText(Ljava/lang/CharSequence;)V

    .line 84
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setEnabled(Z)V

    .line 86
    invoke-virtual {v0, p5}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 87
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setVisibility(I)V

    .line 89
    return-object v0
.end method

.method public a(ILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 72
    move-object v0, p0

    move v1, p1

    move v3, v2

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lckp;->a(IIILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x4

    .line 63
    iput v1, p0, Lckp;->R:I

    .line 65
    iget-object v0, p0, Lckp;->S:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    aget-object v0, v0, v1

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setVisibility(I)V

    .line 66
    iget-object v0, p0, Lckp;->S:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setVisibility(I)V

    .line 67
    iget-object v0, p0, Lckp;->S:[Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setVisibility(I)V

    .line 68
    return-void
.end method

.method public a(Lckq;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lckp;->T:Lckq;

    .line 60
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lckp;->O:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lckp;->P:Landroid/widget/TextView;

    if-nez v0, :cond_1

    .line 94
    :cond_0
    const/4 v0, 0x0

    .line 105
    :goto_0
    return v0

    .line 97
    :cond_1
    if-eqz p1, :cond_2

    .line 98
    iget-object v0, p0, Lckp;->O:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    :cond_2
    if-eqz p2, :cond_3

    .line 102
    iget-object v0, p0, Lckp;->P:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public aO_()V
    .locals 0

    .prologue
    .line 53
    invoke-super {p0}, Lu;->aO_()V

    .line 55
    invoke-virtual {p0}, Lckp;->c()Z

    .line 56
    return-void
.end method

.method public b()Lcom/google/android/apps/photoeditor/views/ItemSelectorView;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lckp;->Q:Lcom/google/android/apps/photoeditor/views/ItemSelectorView;

    return-object v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lckp;->N:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lckp;->T:Lckq;

    if-nez v0, :cond_1

    .line 120
    :cond_0
    const/4 v0, 0x0

    .line 125
    :goto_0
    return v0

    .line 123
    :cond_1
    iget-object v0, p0, Lckp;->T:Lckq;

    invoke-interface {v0}, Lckq;->ao()V

    .line 125
    const/4 v0, 0x1

    goto :goto_0
.end method
