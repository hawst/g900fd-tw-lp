.class public final Lazx;
.super Layr;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 16
    new-instance v0, Lays;

    invoke-direct {v0}, Lays;-><init>()V

    const v1, 0x7f020430

    .line 17
    invoke-virtual {v0, v1}, Lays;->a(I)Lays;

    move-result-object v0

    const-wide/32 v2, 0xc3500

    .line 18
    invoke-virtual {v0, v2, v3}, Lays;->b(J)Lays;

    move-result-object v0

    const v1, 0x7f02040a

    .line 19
    invoke-virtual {v0, v1}, Lays;->b(I)Lays;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    .line 20
    invoke-virtual {v0, v1}, Lays;->a(F)Lays;

    move-result-object v0

    const/4 v1, 0x1

    .line 21
    invoke-virtual {v0, v1}, Lays;->c(I)Lays;

    move-result-object v0

    .line 16
    invoke-direct {p0, v0}, Layr;-><init>(Lays;)V

    .line 23
    return-void
.end method


# virtual methods
.method protected a(Layj;Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;
    .locals 6

    .prologue
    const-wide/32 v4, 0xf4240

    const/4 v3, 0x0

    .line 28
    new-instance v0, Lazy;

    new-instance v1, Layu;

    invoke-direct {v1}, Layu;-><init>()V

    .line 29
    invoke-virtual {v1, p1}, Layu;->a(Layj;)Layu;

    move-result-object v1

    const v2, 0x7f0b00ae

    .line 30
    invoke-virtual {p3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Layu;->a(I)Layu;

    move-result-object v1

    const/high16 v2, 0x42100000    # 36.0f

    .line 31
    invoke-virtual {v1, v2}, Layu;->a(F)Layu;

    move-result-object v1

    const/high16 v2, 0x42300000    # 44.0f

    .line 32
    invoke-virtual {v1, v2}, Layu;->b(F)Layu;

    move-result-object v1

    const-string v2, "sans-serif"

    .line 33
    invoke-static {v2, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Layu;->a(Landroid/graphics/Typeface;)Layu;

    move-result-object v1

    .line 34
    invoke-virtual {v1, v3}, Layu;->a(Z)Layu;

    move-result-object v1

    const/16 v2, 0x32

    .line 35
    invoke-virtual {v1, v2}, Layu;->c(I)Layu;

    move-result-object v1

    const/4 v2, 0x1

    .line 36
    invoke-virtual {v1, v2}, Layu;->b(Z)Layu;

    move-result-object v1

    const v2, 0x7f0b00b2

    .line 37
    invoke-virtual {p3, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Layu;->g(I)Layu;

    move-result-object v1

    .line 38
    invoke-virtual {v1, v4, v5}, Layu;->a(J)Layu;

    move-result-object v1

    .line 39
    invoke-virtual {v1, v4, v5}, Layu;->b(J)Layu;

    move-result-object v1

    invoke-direct {v0, v1}, Lazy;-><init>(Layu;)V

    return-object v0
.end method

.method protected f()Layj;
    .locals 3

    .prologue
    .line 44
    new-instance v0, Lazt;

    sget-object v1, Lazv;->a:Lazv;

    const v2, 0x3f6e147b    # 0.93f

    invoke-direct {v0, p0, v1, v2}, Lazt;-><init>(Layj;Lazv;F)V

    return-object v0
.end method
