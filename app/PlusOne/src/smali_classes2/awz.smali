.class public Lawz;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lbyx;",
        "Ljava/lang/Void;",
        "Lbyz;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Laxa;

.field private c:Ljava/lang/RuntimeException;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    const-class v0, Lawz;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lawz;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Laxa;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 18
    iput-object v1, p0, Lawz;->c:Ljava/lang/RuntimeException;

    .line 33
    const-string v0, "listener"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laxa;

    iput-object v0, p0, Lawz;->b:Laxa;

    .line 34
    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 72
    iget-object v0, p0, Lawz;->c:Ljava/lang/RuntimeException;

    if-eqz v0, :cond_0

    .line 73
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "exception while producing storyboard"

    iget-object v2, p0, Lawz;->c:Ljava/lang/RuntimeException;

    invoke-direct {v0, v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 76
    :cond_0
    return-void
.end method


# virtual methods
.method protected varargs a([Lbyx;)Lbyz;
    .locals 6

    .prologue
    .line 39
    const/4 v0, 0x0

    aget-object v0, p1, v0

    .line 41
    :try_start_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    .line 42
    invoke-virtual {v0}, Lbyx;->a()Lbyz;

    move-result-object v0

    .line 43
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 45
    const-wide/32 v4, 0x2faf080

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 46
    sget-object v1, Lawz;->a:Ljava/lang/String;

    const-wide/32 v4, 0xf4240

    div-long/2addr v2, v4

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v4, 0x34

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Took "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ms to produce a storyboard"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    :cond_0
    return-object v0

    .line 49
    :catch_0
    move-exception v0

    .line 55
    iput-object v0, p0, Lawz;->c:Ljava/lang/RuntimeException;

    .line 56
    throw v0
.end method

.method protected a()V
    .locals 0

    .prologue
    .line 62
    invoke-direct {p0}, Lawz;->b()V

    .line 63
    return-void
.end method

.method protected a(Lbyz;)V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lawz;->b()V

    .line 68
    iget-object v0, p0, Lawz;->b:Laxa;

    invoke-interface {v0, p1}, Laxa;->a(Lbyz;)V

    .line 69
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 12
    check-cast p1, [Lbyx;

    invoke-virtual {p0, p1}, Lawz;->a([Lbyx;)Lbyz;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 12
    invoke-virtual {p0}, Lawz;->a()V

    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 12
    check-cast p1, Lbyz;

    invoke-virtual {p0, p1}, Lawz;->a(Lbyz;)V

    return-void
.end method
