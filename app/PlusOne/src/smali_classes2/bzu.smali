.class public final Lbzu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbzx;


# instance fields
.field private final a:Lama;


# direct methods
.method public constructor <init>(Lama;)V
    .locals 2

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const-string v0, "clipIdentifierGenerator"

    .line 33
    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lama;

    iput-object v0, p0, Lbzu;->a:Lama;

    .line 34
    return-void
.end method

.method private static a(Layj;Lbmg;Lbmg;Lbmd;)J
    .locals 6

    .prologue
    .line 141
    invoke-interface {p0, p1, p2}, Layj;->a(Lbmg;Lbmg;)J

    move-result-wide v0

    .line 142
    invoke-virtual {p3}, Lbmd;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x3

    div-long/2addr v2, v4

    .line 141
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public a(Lbmd;Lbza;)Lbmd;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 84
    new-instance v1, Look;

    invoke-direct {v1}, Look;-><init>()V

    .line 86
    iget-object v0, p2, Lbza;->r:Layj;

    new-instance v2, Loom;

    invoke-direct {v2}, Loom;-><init>()V

    iput-object v2, v1, Look;->d:Loom;

    new-instance v2, Loom;

    invoke-direct {v2}, Loom;-><init>()V

    iput-object v2, v1, Look;->c:Loom;

    iget-object v2, v1, Look;->d:Loom;

    sget-object v3, Lbmg;->c:Lbmg;

    iget-object v4, p1, Lbmd;->d:Lbmg;

    invoke-static {v0, v3, v4, p1}, Lbzu;->a(Layj;Lbmg;Lbmg;Lbmd;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v2, Loom;->a:Ljava/lang/Long;

    iget-object v2, v1, Look;->d:Loom;

    sget-object v3, Lbmg;->a:Lbmg;

    iget-object v4, p1, Lbmd;->d:Lbmg;

    invoke-static {v0, v3, v4, p1}, Lbzu;->a(Layj;Lbmg;Lbmg;Lbmd;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v2, Loom;->b:Ljava/lang/Long;

    iget-object v2, v1, Look;->c:Loom;

    iget-object v3, p1, Lbmd;->d:Lbmg;

    sget-object v4, Lbmg;->c:Lbmg;

    invoke-static {v0, v3, v4, p1}, Lbzu;->a(Layj;Lbmg;Lbmg;Lbmd;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v2, Loom;->a:Ljava/lang/Long;

    iget-object v2, v1, Look;->c:Loom;

    iget-object v3, p1, Lbmd;->d:Lbmg;

    sget-object v4, Lbmg;->a:Lbmg;

    invoke-static {v0, v3, v4, p1}, Lbzu;->a(Layj;Lbmg;Lbmg;Lbmd;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Loom;->b:Ljava/lang/Long;

    .line 88
    iget-object v0, p2, Lbza;->r:Layj;

    .line 89
    invoke-interface {v0, p1}, Layj;->a(Lbmd;)Ljava/util/List;

    move-result-object v0

    new-array v2, v6, [Ljava/lang/Integer;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Integer;

    iput-object v0, v1, Look;->b:[Ljava/lang/Integer;

    .line 91
    iget-object v0, p2, Lbza;->r:Layj;

    invoke-interface {v0, p1}, Layj;->b(Lbmd;)Lool;

    move-result-object v0

    iput-object v0, v1, Look;->e:Lool;

    .line 93
    iget-object v0, p2, Lbza;->r:Layj;

    invoke-interface {v0, p1}, Layj;->c(Lbmd;)Ljava/util/List;

    move-result-object v0

    new-array v2, v6, [Ljava/lang/Double;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Double;

    iput-object v0, v1, Look;->f:[Ljava/lang/Double;

    .line 95
    invoke-virtual {p1}, Lbmd;->a()Lbmf;

    move-result-object v0

    invoke-virtual {v0, v1}, Lbmf;->a(Look;)Lbmf;

    move-result-object v0

    invoke-virtual {v0}, Lbmf;->a()Lbmd;

    move-result-object v0

    return-object v0
.end method

.method public a(Lood;Ljava/util/List;Lbza;Ljava/util/List;Landroid/util/SparseArray;Z)Lbzy;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lood;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Lbza;",
            "Ljava/util/List",
            "<",
            "Lbys;",
            ">;",
            "Landroid/util/SparseArray",
            "<",
            "Lbmh;",
            ">;Z)",
            "Lbzy;"
        }
    .end annotation

    .prologue
    .line 45
    sget-object v0, Lbza;->l:Lbza;

    if-ne p3, v0, :cond_0

    .line 46
    new-instance v0, Lbzv;

    iget-object v1, p0, Lbzu;->a:Lama;

    invoke-direct {v0, v1}, Lbzv;-><init>(Lama;)V

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move/from16 v6, p6

    .line 47
    invoke-virtual/range {v0 .. v6}, Lbzv;->a(Lood;Ljava/util/List;Lbza;Ljava/util/List;Landroid/util/SparseArray;Z)Lbzy;

    move-result-object v0

    .line 75
    :goto_0
    return-object v0

    .line 50
    :cond_0
    new-instance v2, Loon;

    invoke-direct {v2}, Loon;-><init>()V

    .line 51
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 53
    if-eqz p1, :cond_1

    iget-object v0, p1, Lood;->h:Looe;

    iget v0, v0, Looe;->a:I

    .line 56
    invoke-static {v0}, Lbza;->a(I)Lbza;

    move-result-object v0

    if-ne v0, p3, :cond_1

    .line 59
    iget-object v0, p0, Lbzu;->a:Lama;

    invoke-static {p1, p2, p3, v0}, Lbzz;->a(Lood;Ljava/util/List;Lbza;Lama;)Lbzy;

    move-result-object v0

    goto :goto_0

    .line 63
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 64
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    invoke-virtual {p0, v0, p3}, Lbzu;->a(Lbmd;Lbza;)Lbmd;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 63
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 66
    :cond_2
    new-instance v0, Look;

    invoke-direct {v0}, Look;-><init>()V

    new-instance v1, Loom;

    invoke-direct {v1}, Loom;-><init>()V

    iput-object v1, v0, Look;->c:Loom;

    iget-object v1, v0, Look;->c:Loom;

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v1, Loom;->a:Ljava/lang/Long;

    iget-object v1, v0, Look;->c:Loom;

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v1, Loom;->b:Ljava/lang/Long;

    new-instance v1, Loom;

    invoke-direct {v1}, Loom;-><init>()V

    iput-object v1, v0, Look;->d:Loom;

    iget-object v1, v0, Look;->d:Loom;

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v1, Loom;->a:Ljava/lang/Long;

    iget-object v1, v0, Look;->d:Loom;

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iput-object v4, v1, Loom;->b:Ljava/lang/Long;

    new-instance v1, Lbmf;

    invoke-direct {v1}, Lbmf;-><init>()V

    iget-object v4, p0, Lbzu;->a:Lama;

    invoke-interface {v4}, Lama;->a()I

    move-result v4

    invoke-virtual {v1, v4}, Lbmf;->a(I)Lbmf;

    move-result-object v1

    sget-object v4, Lbmg;->d:Lbmg;

    invoke-virtual {v1, v4}, Lbmf;->a(Lbmg;)Lbmf;

    move-result-object v1

    sget-object v4, Lbmd;->a:Ljeg;

    invoke-virtual {v1, v4}, Lbmf;->a(Ljeg;)Lbmf;

    move-result-object v1

    new-instance v4, Lbmp;

    const-wide/16 v6, 0x0

    const-wide/32 v8, 0xf4240

    invoke-direct {v4, v6, v7, v8, v9}, Lbmp;-><init>(JJ)V

    invoke-virtual {v1, v4}, Lbmf;->a(Lbmp;)Lbmf;

    move-result-object v1

    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Lbmf;->a(Z)Lbmf;

    move-result-object v1

    invoke-virtual {v1, v0}, Lbmf;->a(Look;)Lbmf;

    move-result-object v0

    invoke-virtual {v0}, Lbmf;->a()Lbmd;

    move-result-object v1

    .line 73
    new-instance v0, Lbzy;

    iget-object v4, p0, Lbzu;->a:Lama;

    .line 75
    invoke-static {p3, v4}, Lbzz;->a(Lbza;Lama;)Lbmd;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4, v1}, Lbzy;-><init>(Loon;Ljava/util/List;Lbmd;Lbmd;)V

    goto/16 :goto_0
.end method
