.class public final Lbyw;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lbza;Loon;Ljava/util/List;Lboi;Lbmk;ZZ)Lood;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbza;",
            "Loon;",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;",
            "Lboi;",
            "Lbmk;",
            "ZZ)",
            "Lood;"
        }
    .end annotation

    .prologue
    .line 41
    const-string v0, "theme"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 42
    const-string v0, "storyboard"

    const/4 v1, 0x0

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 43
    const-string v0, "globalThemeParams"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 44
    const-string v0, "clipsAfterUserEdits"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 46
    new-instance v2, Lood;

    invoke-direct {v2}, Lood;-><init>()V

    .line 47
    sget-object v0, Lbza;->l:Lbza;

    if-ne p0, v0, :cond_3

    const/4 v0, 0x3

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Lood;->a:Ljava/lang/Integer;

    .line 50
    invoke-virtual {p3}, Lboi;->p()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Lood;->j:Ljava/lang/Integer;

    .line 51
    invoke-virtual {p3}, Lboi;->o()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Lood;->i:Ljava/lang/Integer;

    .line 52
    invoke-virtual {p3}, Lboi;->j()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 53
    invoke-virtual {p3}, Lboi;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lood;->f:Ljava/lang/String;

    .line 55
    :cond_0
    invoke-virtual {p3}, Lboi;->k()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 56
    invoke-virtual {p3}, Lboi;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lood;->g:Ljava/lang/String;

    .line 58
    :cond_1
    new-instance v0, Looe;

    invoke-direct {v0}, Looe;-><init>()V

    iput-object v0, v2, Lood;->h:Looe;

    .line 60
    invoke-virtual {p3}, Lboi;->e()I

    move-result v3

    .line 61
    iget-object v0, v2, Lood;->h:Looe;

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Looe;->e:Ljava/lang/Boolean;

    .line 62
    iget-object v0, v2, Lood;->h:Looe;

    iget v1, p0, Lbza;->p:I

    iput v1, v0, Looe;->a:I

    .line 63
    iget-object v0, v2, Lood;->h:Looe;

    new-instance v1, Looo;

    invoke-direct {v1}, Looo;-><init>()V

    iput-object v1, v0, Looe;->b:Looo;

    .line 64
    iget-object v0, v2, Lood;->h:Looe;

    iget-object v0, v0, Looe;->b:Looo;

    iput-object p1, v0, Looo;->a:Loon;

    .line 65
    iget-object v0, v2, Lood;->h:Looe;

    iget-object v0, v0, Looe;->b:Looo;

    new-array v1, v3, [Look;

    iput-object v1, v0, Looo;->b:[Look;

    .line 67
    new-array v0, v3, [Lonr;

    iput-object v0, v2, Lood;->b:[Lonr;

    .line 68
    new-array v0, v3, [Ljava/lang/Boolean;

    iput-object v0, v2, Lood;->d:[Ljava/lang/Boolean;

    .line 69
    iget-object v0, v2, Lood;->h:Looe;

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Looe;->e:Ljava/lang/Boolean;

    .line 71
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_6

    .line 72
    invoke-virtual {p3, v1}, Lboi;->b(I)Lbmd;

    move-result-object v0

    .line 73
    iget-object v4, v2, Lood;->h:Looe;

    iget-object v4, v4, Looe;->b:Looo;

    iget-object v4, v4, Looo;->b:[Look;

    invoke-virtual {v0}, Lbmd;->c()Look;

    move-result-object v5

    aput-object v5, v4, v1

    .line 74
    iget-object v4, v2, Lood;->b:[Lonr;

    new-instance v5, Lonr;

    invoke-direct {v5}, Lonr;-><init>()V

    aput-object v5, v4, v1

    .line 75
    iget-object v4, v0, Lbmd;->e:Ljeg;

    iget-object v4, v4, Ljeg;->a:Ljdx;

    if-eqz v4, :cond_4

    .line 76
    iget-object v4, v2, Lood;->b:[Lonr;

    aget-object v4, v4, v1

    iget-object v5, v0, Lbmd;->e:Ljeg;

    iget-object v5, v5, Ljeg;->a:Ljdx;

    .line 77
    invoke-virtual {v5}, Ljdx;->a()Lonw;

    move-result-object v5

    iput-object v5, v4, Lonr;->c:Lonw;

    .line 81
    :cond_2
    :goto_2
    iget-object v4, v2, Lood;->b:[Lonr;

    aget-object v4, v4, v1

    iget-object v5, v0, Lbmd;->f:Lbmp;

    invoke-static {v5}, Lbpx;->a(Lbmp;)Lons;

    move-result-object v5

    iput-object v5, v4, Lonr;->d:Lons;

    .line 83
    iget-object v4, v2, Lood;->b:[Lonr;

    aget-object v4, v4, v1

    iget-object v5, v0, Lbmd;->d:Lbmg;

    invoke-virtual {v5}, Lbmg;->a()I

    move-result v5

    iput v5, v4, Lonr;->b:I

    .line 85
    iget-object v4, v2, Lood;->d:[Ljava/lang/Boolean;

    iget-object v0, v0, Lbmd;->d:Lbmg;

    sget-object v5, Lbmg;->d:Lbmg;

    if-ne v0, v5, :cond_5

    const/4 v0, 0x1

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    aput-object v0, v4, v1

    .line 71
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 47
    :cond_3
    const/4 v0, 0x2

    goto/16 :goto_0

    .line 78
    :cond_4
    if-nez p6, :cond_2

    .line 79
    iget-object v4, v0, Lbmd;->d:Lbmg;

    const-string v5, "clip.type"

    sget-object v6, Lbmg;->d:Lbmg;

    invoke-static {v4, v5, v6}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    goto :goto_2

    .line 85
    :cond_5
    const/4 v0, 0x0

    goto :goto_3

    .line 89
    :cond_6
    add-int/lit8 v0, v3, -0x1

    new-array v0, v0, [Ljava/lang/Long;

    iput-object v0, v2, Lood;->c:[Ljava/lang/Long;

    .line 90
    const/4 v0, 0x0

    :goto_4
    add-int/lit8 v1, v3, -0x1

    if-ge v0, v1, :cond_8

    .line 91
    invoke-virtual {p3}, Lboi;->h()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_7

    .line 92
    iget-object v1, v2, Lood;->c:[Ljava/lang/Long;

    .line 93
    invoke-virtual {p3, v0}, Lboi;->d(I)Lcac;

    move-result-object v4

    invoke-interface {v4}, Lcac;->b()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v0

    .line 90
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 95
    :cond_7
    iget-object v1, v2, Lood;->c:[Ljava/lang/Long;

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v1, v0

    goto :goto_5

    .line 100
    :cond_8
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lons;

    iput-object v0, v2, Lood;->e:[Lons;

    .line 101
    const/4 v0, 0x0

    move v1, v0

    :goto_6
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_9

    .line 102
    iget-object v3, v2, Lood;->e:[Lons;

    .line 103
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    iget-object v0, v0, Lbmd;->f:Lbmp;

    .line 102
    invoke-static {v0}, Lbpx;->a(Lbmp;)Lons;

    move-result-object v0

    aput-object v0, v3, v1

    .line 101
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 107
    :cond_9
    iget-object v0, v2, Lood;->h:Looe;

    new-instance v1, Lonq;

    invoke-direct {v1}, Lonq;-><init>()V

    iput-object v1, v0, Looe;->d:Lonq;

    .line 108
    if-eqz p4, :cond_b

    .line 109
    iget-object v0, v2, Lood;->h:Looe;

    new-instance v1, Looa;

    invoke-direct {v1}, Looa;-><init>()V

    iput-object v1, v0, Looe;->c:Looa;

    .line 110
    iget-object v0, v2, Lood;->h:Looe;

    iget-object v0, v0, Looe;->c:Looa;

    .line 111
    invoke-virtual {p4}, Lbmk;->b()Lboh;

    move-result-object v1

    invoke-virtual {v1}, Lboh;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Looa;->a:Ljava/lang/Long;

    .line 112
    iget-object v0, v2, Lood;->h:Looe;

    iget-object v1, v0, Looe;->d:Lonq;

    const/4 v0, 0x0

    .line 113
    invoke-virtual {p3, v0}, Lboi;->a(I)Lbma;

    move-result-object v3

    new-instance v4, Lonp;

    invoke-direct {v4}, Lonp;-><init>()V

    invoke-interface {v3}, Lbma;->a()I

    move-result v0

    new-array v0, v0, [Lonr;

    iput-object v0, v4, Lonp;->a:[Lonr;

    invoke-interface {v3}, Lbma;->a()I

    move-result v5

    const/4 v0, 0x0

    :goto_7
    if-ge v0, v5, :cond_a

    invoke-interface {v3, v0}, Lbma;->a(I)Lbmd;

    move-result-object v6

    iget-object v7, v4, Lonp;->a:[Lonr;

    new-instance v8, Lonr;

    invoke-direct {v8}, Lonr;-><init>()V

    aput-object v8, v7, v0

    iget-object v7, v4, Lonp;->a:[Lonr;

    aget-object v7, v7, v0

    iget-object v6, v6, Lbmd;->f:Lbmp;

    invoke-static {v6}, Lbpx;->a(Lbmp;)Lons;

    move-result-object v6

    iput-object v6, v7, Lonr;->d:Lons;

    iget-object v6, v4, Lonp;->a:[Lonr;

    aget-object v6, v6, v0

    new-instance v7, Lonw;

    invoke-direct {v7}, Lonw;-><init>()V

    iput-object v7, v6, Lonr;->c:Lonw;

    iget-object v6, v4, Lonp;->a:[Lonr;

    aget-object v6, v6, v0

    iget-object v6, v6, Lonr;->c:Lonw;

    new-instance v7, Looa;

    invoke-direct {v7}, Looa;-><init>()V

    iput-object v7, v6, Lonw;->c:Looa;

    iget-object v6, v4, Lonp;->a:[Lonr;

    aget-object v6, v6, v0

    iget-object v6, v6, Lonr;->c:Lonw;

    iget-object v6, v6, Lonw;->c:Looa;

    invoke-virtual {p4}, Lbmk;->b()Lboh;

    move-result-object v7

    invoke-virtual {v7}, Lboh;->c()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    iput-object v7, v6, Looa;->a:Ljava/lang/Long;

    iget-object v6, v4, Lonp;->a:[Lonr;

    aget-object v6, v6, v0

    const/4 v7, 0x6

    iput v7, v6, Lonr;->b:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_a
    iput-object v4, v1, Lonq;->a:Lonp;

    .line 116
    :cond_b
    return-object v2
.end method
