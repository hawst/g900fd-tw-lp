.class public final Lbmd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation
.end field

.field public static final a:Ljeg;


# instance fields
.field public final b:I

.field public final c:I

.field public final d:Lbmg;

.field public final e:Ljeg;

.field public final f:Lbmp;

.field public final g:Z

.field public final h:Z

.field public final i:I

.field public final j:Z

.field private final k:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 75
    const-string v0, "empty:"

    .line 76
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sget-object v1, Ljei;->b:Ljei;

    invoke-static {v0, v1}, Ljeg;->a(Landroid/net/Uri;Ljei;)Ljeg;

    move-result-object v0

    sput-object v0, Lbmd;->a:Ljeg;

    .line 307
    new-instance v0, Lbme;

    invoke-direct {v0}, Lbme;-><init>()V

    sput-object v0, Lbmd;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IILbmg;Ljeg;Lbmp;ZZ[BIZ)V
    .locals 3

    .prologue
    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 146
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    if-ltz p1, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "invalid clip identifier"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 147
    iput p1, p0, Lbmd;->b:I

    .line 148
    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    if-ltz p2, :cond_4

    :cond_1
    const/4 v0, 0x1

    :goto_1
    const-string v1, "invalid clip parent identifer"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 150
    iput p2, p0, Lbmd;->c:I

    .line 151
    const-string v0, "type"

    const/4 v1, 0x0

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmg;

    iput-object v0, p0, Lbmd;->d:Lbmg;

    .line 152
    const-string v0, "mediaIdentifier"

    const/4 v1, 0x0

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljeg;

    iput-object v0, p0, Lbmd;->e:Ljeg;

    .line 153
    sget-object v0, Lbmg;->d:Lbmg;

    if-ne p3, v0, :cond_2

    sget-object v0, Lbmd;->a:Ljeg;

    .line 154
    invoke-virtual {p4, v0}, Ljeg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_2
    const/4 v0, 0x1

    :goto_2
    const-string v1, "if the clip\'s type is EMPTY_VIDEO, its media ID has to be EMPTY_VIDEO_MEDIA_IDENTIFIER"

    .line 153
    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 157
    const-string v0, "interval"

    const/4 v1, 0x0

    invoke-static {p5, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmp;

    iput-object v0, p0, Lbmd;->f:Lbmp;

    .line 158
    iget-wide v0, p5, Lbmp;->b:J

    const-string v2, "interval.startPointUs"

    invoke-static {v0, v1, v2}, Lcec;->b(JLjava/lang/CharSequence;)J

    .line 159
    iget-wide v0, p5, Lbmp;->c:J

    const-string v2, "interval.endPointUs"

    invoke-static {v0, v1, v2}, Lcec;->b(JLjava/lang/CharSequence;)J

    .line 160
    iput-boolean p6, p0, Lbmd;->g:Z

    .line 161
    iput-boolean p7, p0, Lbmd;->h:Z

    .line 162
    iput-object p8, p0, Lbmd;->k:[B

    .line 163
    iput p9, p0, Lbmd;->i:I

    .line 164
    iput-boolean p10, p0, Lbmd;->j:Z

    .line 165
    return-void

    .line 146
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 148
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 154
    :cond_5
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 173
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 174
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lbmd;->b:I

    .line 175
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lbmd;->c:I

    .line 176
    invoke-static {}, Lbmg;->values()[Lbmg;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    aget-object v0, v0, v3

    iput-object v0, p0, Lbmd;->d:Lbmg;

    .line 177
    sget-object v0, Ljeg;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljeg;

    iput-object v0, p0, Lbmd;->e:Ljeg;

    .line 178
    sget-object v0, Lbmp;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmp;

    iput-object v0, p0, Lbmd;->f:Lbmp;

    .line 179
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v3

    .line 180
    and-int/lit8 v0, v3, 0x1

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lbmd;->g:Z

    .line 181
    and-int/lit8 v0, v3, 0x2

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lbmd;->h:Z

    .line 182
    and-int/lit8 v0, v3, 0x4

    if-eqz v0, :cond_2

    .line 183
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    iput-object v0, p0, Lbmd;->k:[B

    .line 187
    :goto_2
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lbmd;->i:I

    .line 188
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_3

    :goto_3
    iput-boolean v1, p0, Lbmd;->j:Z

    .line 189
    return-void

    :cond_0
    move v0, v2

    .line 180
    goto :goto_0

    :cond_1
    move v0, v2

    .line 181
    goto :goto_1

    .line 185
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lbmd;->k:[B

    goto :goto_2

    :cond_3
    move v1, v2

    .line 188
    goto :goto_3
.end method

.method public constructor <init>(Lbmg;Ljeg;Lbmp;)V
    .locals 11

    .prologue
    const/4 v1, -0x1

    const/4 v7, 0x0

    .line 169
    const/4 v6, 0x1

    const/4 v8, 0x0

    move-object v0, p0

    move v2, v1

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v9, v7

    move v10, v7

    invoke-direct/range {v0 .. v10}, Lbmd;-><init>(IILbmg;Ljeg;Lbmp;ZZ[BIZ)V

    .line 171
    return-void
.end method

.method static synthetic b(Lbmd;)[B
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lbmd;->k:[B

    return-object v0
.end method


# virtual methods
.method public a()Lbmf;
    .locals 1

    .prologue
    .line 212
    new-instance v0, Lbmf;

    invoke-direct {v0, p0}, Lbmf;-><init>(Lbmd;)V

    return-object v0
.end method

.method public a(Lbmd;)Z
    .locals 2

    .prologue
    .line 295
    iget-object v0, p0, Lbmd;->d:Lbmg;

    iget-object v1, p1, Lbmd;->d:Lbmg;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbmd;->f:Lbmp;

    iget-object v1, p1, Lbmd;->f:Lbmp;

    .line 296
    invoke-virtual {v0, v1}, Lbmp;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lbmd;->g:Z

    iget-boolean v1, p1, Lbmd;->g:Z

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lbmd;->h:Z

    iget-boolean v1, p1, Lbmd;->h:Z

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lbmd;->e:Ljeg;

    iget-object v1, p1, Lbmd;->e:Ljeg;

    .line 299
    invoke-virtual {v0, v1}, Ljeg;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lbmd;->f:Lbmp;

    invoke-virtual {v0}, Lbmp;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public c()Look;
    .locals 2

    .prologue
    .line 225
    iget-object v0, p0, Lbmd;->k:[B

    if-nez v0, :cond_0

    .line 226
    const/4 v0, 0x0

    .line 231
    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    new-instance v0, Look;

    invoke-direct {v0}, Look;-><init>()V

    iget-object v1, p0, Lbmd;->k:[B

    invoke-static {v0, v1}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Look;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 234
    :catch_0
    move-exception v0

    .line 235
    const-string v1, "Protobuf copying failed"

    invoke-static {v1, v0}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/Throwable;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 244
    iget-object v0, p0, Lbmd;->k:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 304
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 249
    if-ne p0, p1, :cond_1

    .line 259
    :cond_0
    :goto_0
    return v0

    .line 252
    :cond_1
    instance-of v2, p1, Lbmd;

    if-nez v2, :cond_2

    move v0, v1

    .line 253
    goto :goto_0

    .line 255
    :cond_2
    check-cast p1, Lbmd;

    .line 256
    iget v2, p0, Lbmd;->b:I

    iget v3, p1, Lbmd;->b:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lbmd;->c:I

    iget v3, p1, Lbmd;->c:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lbmd;->k:[B

    iget-object v3, p1, Lbmd;->k:[B

    .line 258
    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 259
    invoke-virtual {p0, p1}, Lbmd;->a(Lbmd;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 12

    .prologue
    .line 265
    iget v3, p0, Lbmd;->b:I

    iget v4, p0, Lbmd;->c:I

    iget-object v5, p0, Lbmd;->d:Lbmg;

    iget-object v6, p0, Lbmd;->f:Lbmp;

    iget-boolean v7, p0, Lbmd;->g:Z

    iget-boolean v8, p0, Lbmd;->h:Z

    iget-object v9, p0, Lbmd;->k:[B

    iget-object v0, p0, Lbmd;->e:Ljeg;

    const/16 v1, 0x11

    .line 272
    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v1

    .line 271
    if-nez v9, :cond_1

    mul-int/lit8 v0, v1, 0x1f

    .line 270
    :cond_0
    invoke-static {v8, v0}, Lbqh;->a(ZI)I

    move-result v0

    .line 269
    invoke-static {v7, v0}, Lbqh;->a(ZI)I

    move-result v0

    .line 268
    invoke-static {v6, v0}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v0

    .line 267
    invoke-static {v5, v0}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v0

    .line 266
    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, v4

    .line 265
    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, v3

    return v0

    .line 271
    :cond_1
    const/4 v0, 0x0

    array-length v10, v9

    move v11, v0

    move v0, v1

    move v1, v11

    :goto_0
    if-ge v1, v10, :cond_0

    aget-byte v2, v9, v1

    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 277
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Clip [identifier="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 278
    iget v1, p0, Lbmd;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", parentIdentifier="

    .line 279
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lbmd;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type="

    .line 280
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbmd;->d:Lbmg;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", interval="

    .line 281
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbmd;->f:Lbmp;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", shouldBeatMatch="

    .line 282
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lbmd;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", canBeSplitIntoScenes="

    .line 283
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lbmd;->h:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", themingParams present="

    .line 284
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Lbmd;->k:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mediaId="

    .line 285
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lbmd;->e:Ljeg;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    .line 286
    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 287
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 284
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 193
    iget v0, p0, Lbmd;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 194
    iget v0, p0, Lbmd;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 195
    iget-object v0, p0, Lbmd;->d:Lbmg;

    invoke-virtual {v0}, Lbmg;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 196
    iget-object v0, p0, Lbmd;->e:Ljeg;

    invoke-virtual {v0, p1, v2}, Ljeg;->writeToParcel(Landroid/os/Parcel;I)V

    .line 197
    iget-object v0, p0, Lbmd;->f:Lbmp;

    invoke-virtual {v0, p1, v2}, Lbmp;->writeToParcel(Landroid/os/Parcel;I)V

    .line 198
    iget-boolean v0, p0, Lbmd;->g:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iget-boolean v3, p0, Lbmd;->h:Z

    if-eqz v3, :cond_2

    const/4 v3, 0x2

    :goto_1
    or-int/2addr v3, v0

    iget-object v0, p0, Lbmd;->k:[B

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    :goto_2
    or-int/2addr v0, v3

    .line 202
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 203
    iget-object v0, p0, Lbmd;->k:[B

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lbmd;->k:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 206
    :cond_0
    iget v0, p0, Lbmd;->i:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 207
    iget-boolean v0, p0, Lbmd;->j:Z

    if-eqz v0, :cond_4

    :goto_3
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 208
    return-void

    :cond_1
    move v0, v2

    .line 198
    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v1, v2

    .line 207
    goto :goto_3
.end method
