.class public final Lkqd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheg;
.implements Llnx;
.implements Llrb;
.implements Llrc;
.implements Llrf;
.implements Llrg;


# instance fields
.field private a:Landroid/app/Activity;

.field private b:Z

.field private c:Lkqf;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Llqr;)V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    iput-object p1, p0, Lkqd;->a:Landroid/app/Activity;

    .line 42
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 43
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lkqf;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkqf;

    iput-object v0, p0, Lkqd;->c:Lkqf;

    .line 48
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->b(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    .line 49
    if-eqz v0, :cond_0

    .line 50
    invoke-interface {v0, p0}, Lhee;->a(Lheg;)Lhee;

    .line 52
    :cond_0
    return-void
.end method

.method public a(ZIIII)V
    .locals 1

    .prologue
    .line 66
    if-eq p5, p4, :cond_0

    .line 67
    iget-object v0, p0, Lkqd;->c:Lkqf;

    invoke-virtual {v0, p5}, Lkqf;->a(I)V

    .line 69
    :cond_0
    return-void
.end method

.method public aP_()V
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lkqd;->b:Z

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lkqd;->c:Lkqf;

    invoke-virtual {v0}, Lkqf;->b()V

    .line 83
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkqd;->b:Z

    .line 85
    :cond_0
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lkqd;->b:Z

    if-nez v0, :cond_0

    .line 57
    iget-object v0, p0, Lkqd;->c:Lkqf;

    invoke-virtual {v0}, Lkqf;->a()V

    .line 58
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkqd;->b:Z

    .line 60
    :cond_0
    return-void
.end method

.method public c()V
    .locals 2

    .prologue
    .line 73
    iget-boolean v0, p0, Lkqd;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkqd;->a:Landroid/app/Activity;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lkqd;->c:Lkqf;

    invoke-virtual {v0}, Lkqf;->b()V

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkqd;->b:Z

    .line 77
    :cond_0
    return-void

    .line 73
    :cond_1
    iget-object v0, p0, Lkqd;->a:Landroid/app/Activity;

    const-string v1, "keyguard"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    goto :goto_0
.end method
