.class public final Lcfk;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Landroid/util/LongSparseArray;J)I
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<*>;J)I"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 28
    invoke-virtual {p0}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 43
    :cond_0
    :goto_0
    return v0

    .line 31
    :cond_1
    const/4 v2, 0x0

    .line 32
    invoke-virtual {p0}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v3, v2

    move v2, v1

    .line 33
    :goto_1
    if-gt v3, v2, :cond_4

    .line 34
    add-int v1, v3, v2

    ushr-int/lit8 v1, v1, 0x1

    .line 35
    invoke-virtual {p0, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    .line 36
    cmp-long v6, v4, p1

    if-lez v6, :cond_2

    .line 37
    add-int/lit8 v1, v1, -0x1

    move v2, v1

    goto :goto_1

    .line 38
    :cond_2
    cmp-long v3, v4, p1

    if-gez v3, :cond_3

    .line 39
    add-int/lit8 v1, v1, 0x1

    move v3, v1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 41
    goto :goto_0

    .line 43
    :cond_4
    if-lez v3, :cond_0

    add-int/lit8 v0, v3, -0x1

    goto :goto_0
.end method

.method public static a(Lbmp;Landroid/util/LongSparseArray;)Landroid/util/LongSparseArray;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lbmp;",
            "Landroid/util/LongSparseArray",
            "<TT;>;)",
            "Landroid/util/LongSparseArray",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v3, -0x1

    .line 118
    new-instance v2, Landroid/util/LongSparseArray;

    invoke-direct {v2}, Landroid/util/LongSparseArray;-><init>()V

    .line 120
    iget-wide v0, p0, Lbmp;->b:J

    invoke-static {p1, v0, v1}, Lcfk;->a(Landroid/util/LongSparseArray;J)I

    move-result v0

    .line 121
    if-ne v0, v3, :cond_0

    .line 122
    const/4 v0, 0x0

    .line 125
    :cond_0
    iget-wide v4, p0, Lbmp;->c:J

    invoke-static {p1, v4, v5}, Lcfk;->c(Landroid/util/LongSparseArray;J)I

    move-result v1

    .line 126
    if-ne v1, v3, :cond_1

    .line 127
    invoke-virtual {p1}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    .line 130
    :cond_1
    :goto_0
    if-gt v0, v1, :cond_2

    .line 131
    invoke-virtual {p1, v0}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v4

    invoke-virtual {p1, v0}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v4, v5, v3}, Landroid/util/LongSparseArray;->append(JLjava/lang/Object;)V

    .line 130
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134
    :cond_2
    return-object v2
.end method

.method public static b(Landroid/util/LongSparseArray;J)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<*>;J)I"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 54
    invoke-static {p0, p1, p2}, Lcfk;->a(Landroid/util/LongSparseArray;J)I

    move-result v1

    .line 55
    if-eq v1, v0, :cond_1

    invoke-virtual {p0, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_1

    .line 56
    if-lez v1, :cond_0

    add-int/lit8 v0, v1, -0x1

    .line 58
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public static c(Landroid/util/LongSparseArray;J)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/LongSparseArray",
            "<*>;J)I"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 67
    invoke-virtual {p0}, Landroid/util/LongSparseArray;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 83
    :cond_0
    :goto_0
    return v0

    .line 70
    :cond_1
    invoke-static {p0, p1, p2}, Lcfk;->a(Landroid/util/LongSparseArray;J)I

    move-result v1

    .line 71
    if-ne v1, v0, :cond_2

    .line 73
    const/4 v0, 0x0

    goto :goto_0

    .line 75
    :cond_2
    invoke-virtual {p0, v1}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_3

    move v0, v1

    .line 76
    goto :goto_0

    .line 78
    :cond_3
    invoke-virtual {p0}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-eq v1, v2, :cond_0

    .line 83
    add-int/lit8 v0, v1, 0x1

    goto :goto_0
.end method
