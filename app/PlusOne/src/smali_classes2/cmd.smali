.class public final Lcmd;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcmf;",
            ">;"
        }
    .end annotation
.end field

.field final b:I


# direct methods
.method constructor <init>(ILjava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Lcmf;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot create an empty motion."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :cond_0
    iput p1, p0, Lcmd;->b:I

    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcmd;->a:Ljava/util/List;

    .line 48
    return-void
.end method

.method public static a(IF)Lcme;
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcme;

    invoke-direct {v0, p0, p1}, Lcme;-><init>(IF)V

    return-object v0
.end method


# virtual methods
.method a(J)Landroid/view/MotionEvent$PointerCoords;
    .locals 7

    .prologue
    .line 116
    const/4 v1, 0x0

    .line 117
    invoke-virtual {p0}, Lcmd;->b()J

    move-result-wide v2

    cmp-long v0, p1, v2

    if-gtz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcmd;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmf;

    .line 130
    :goto_0
    invoke-virtual {v0, p1, p2}, Lcmf;->b(J)Landroid/view/MotionEvent$PointerCoords;

    move-result-object v0

    return-object v0

    .line 119
    :cond_0
    invoke-virtual {p0}, Lcmd;->c()J

    move-result-wide v2

    cmp-long v0, p1, v2

    if-ltz v0, :cond_1

    .line 120
    iget-object v0, p0, Lcmd;->a:Ljava/util/List;

    iget-object v1, p0, Lcmd;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmf;

    goto :goto_0

    .line 122
    :cond_1
    iget-object v0, p0, Lcmd;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmf;

    .line 123
    iget-wide v4, v0, Lcmf;->d:J

    cmp-long v3, v4, p1

    if-gtz v3, :cond_2

    iget-wide v4, v0, Lcmf;->e:J

    cmp-long v3, p1, v4

    if-gtz v3, :cond_2

    goto :goto_0

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method public a()Lcme;
    .locals 1

    .prologue
    .line 75
    new-instance v0, Lcme;

    invoke-direct {v0, p0}, Lcme;-><init>(Lcmd;)V

    return-object v0
.end method

.method public b()J
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcmd;->a:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmf;

    iget-wide v0, v0, Lcmf;->d:J

    return-wide v0
.end method

.method public c()J
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcmd;->a:Ljava/util/List;

    iget-object v1, p0, Lcmd;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcmf;

    iget-wide v0, v0, Lcmf;->e:J

    return-wide v0
.end method

.method d()Landroid/view/MotionEvent$PointerProperties;
    .locals 2

    .prologue
    .line 108
    new-instance v0, Landroid/view/MotionEvent$PointerProperties;

    invoke-direct {v0}, Landroid/view/MotionEvent$PointerProperties;-><init>()V

    .line 109
    const/4 v1, 0x1

    iput v1, v0, Landroid/view/MotionEvent$PointerProperties;->toolType:I

    .line 110
    iget v1, p0, Lcmd;->b:I

    iput v1, v0, Landroid/view/MotionEvent$PointerProperties;->id:I

    .line 111
    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 88
    if-ne p0, p1, :cond_1

    .line 97
    :cond_0
    :goto_0
    return v0

    .line 91
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    .line 92
    goto :goto_0

    .line 95
    :cond_3
    check-cast p1, Lcmd;

    .line 97
    iget v2, p0, Lcmd;->b:I

    iget v3, p1, Lcmd;->b:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcmd;->a:Ljava/util/List;

    iget-object v3, p1, Lcmd;->a:Ljava/util/List;

    invoke-interface {v2, v3}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcmd;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->hashCode()I

    move-result v0

    .line 103
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcmd;->b:I

    add-int/2addr v0, v1

    .line 104
    return v0
.end method
