.class public final Lceg;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(JIII)I
    .locals 6

    .prologue
    .line 16
    mul-int v0, p2, p3

    .line 17
    int-to-long v2, p4

    mul-long/2addr v2, p0

    const-wide/32 v4, 0xf4240

    div-long/2addr v2, v4

    long-to-int v1, v2

    mul-int/2addr v0, v1

    return v0
.end method

.method public static a(IIII)J
    .locals 4

    .prologue
    .line 27
    if-nez p0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/32 v0, 0xf4240

    int-to-long v2, p0

    mul-long/2addr v0, v2

    mul-int v2, p1, p2

    mul-int/2addr v2, p3

    int-to-long v2, v2

    div-long/2addr v0, v2

    goto :goto_0
.end method

.method public static a([BII)S
    .locals 2

    .prologue
    .line 95
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 96
    aget-byte v0, p0, p1

    int-to-short v0, v0

    .line 98
    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, p1, 0x1

    aget-byte v0, p0, v0

    and-int/lit16 v0, v0, 0xff

    shl-int/lit8 v0, v0, 0x8

    aget-byte v1, p0, p1

    and-int/lit16 v1, v1, 0xff

    or-int/2addr v0, v1

    int-to-short v0, v0

    goto :goto_0
.end method
