.class final Ladf;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>(Landroid/media/MediaFormat;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 242
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 243
    const-string v0, "color-format"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ladf;->a:I

    .line 244
    const-string v0, "width"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ladf;->b:I

    .line 245
    const-string v0, "height"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Ladf;->c:I

    .line 246
    const-string v0, "crop-left"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "crop-left"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    :goto_0
    iput v0, p0, Ladf;->d:I

    .line 247
    const-string v0, "crop-right"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "crop-right"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    :goto_1
    iput v0, p0, Ladf;->e:I

    .line 249
    const-string v0, "crop-top"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "crop-top"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    :goto_2
    iput v0, p0, Ladf;->f:I

    .line 250
    const-string v0, "crop-bottom"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "crop-bottom"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    :goto_3
    iput v0, p0, Ladf;->g:I

    .line 252
    const-string v0, "stride"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "stride"

    invoke-virtual {p1, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    :cond_0
    iput v1, p0, Ladf;->h:I

    .line 253
    iget v0, p0, Ladf;->b:I

    iget v1, p0, Ladf;->c:I

    mul-int/2addr v0, v1

    shl-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Ladf;->i:Ljava/nio/ByteBuffer;

    .line 254
    return-void

    :cond_1
    move v0, v1

    .line 246
    goto :goto_0

    .line 247
    :cond_2
    iget v0, p0, Ladf;->b:I

    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_3
    move v0, v1

    .line 249
    goto :goto_2

    .line 250
    :cond_4
    iget v0, p0, Ladf;->c:I

    add-int/lit8 v0, v0, -0x1

    goto :goto_3
.end method

.method static synthetic a(Ladf;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V
    .locals 8

    .prologue
    .line 222
    iget v0, p0, Ladf;->a:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported color format: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Ladf;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    iget v0, p0, Ladf;->h:I

    if-eqz v0, :cond_0

    iget v0, p0, Ladf;->h:I

    :goto_0
    iget-object v1, p0, Ladf;->i:Ljava/nio/ByteBuffer;

    iget v2, p0, Ladf;->b:I

    iget v3, p0, Ladf;->c:I

    invoke-static {p1, v1, v2, v3, v0}, Landroidx/media/filterfw/ColorSpace;->b(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;III)V

    :goto_1
    iget-object v0, p0, Ladf;->i:Ljava/nio/ByteBuffer;

    iget v2, p0, Ladf;->b:I

    iget v3, p0, Ladf;->c:I

    iget v4, p0, Ladf;->d:I

    iget v5, p0, Ladf;->f:I

    iget v6, p0, Ladf;->e:I

    iget v7, p0, Ladf;->g:I

    move-object v1, p2

    invoke-static/range {v0 .. v7}, Landroidx/media/filterfw/ColorSpace;->a(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;IIIIII)V

    return-void

    :cond_0
    iget v0, p0, Ladf;->b:I

    shl-int/lit8 v0, v0, 0x2

    goto :goto_0

    :pswitch_2
    iget v0, p0, Ladf;->h:I

    if-eqz v0, :cond_1

    iget v0, p0, Ladf;->h:I

    :goto_2
    iget-object v1, p0, Ladf;->i:Ljava/nio/ByteBuffer;

    iget v2, p0, Ladf;->b:I

    iget v3, p0, Ladf;->c:I

    invoke-static {p1, v1, v2, v3, v0}, Landroidx/media/filterfw/ColorSpace;->a(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;III)V

    goto :goto_1

    :cond_1
    iget v0, p0, Ladf;->b:I

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
