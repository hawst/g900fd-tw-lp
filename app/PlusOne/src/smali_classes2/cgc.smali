.class public Lcgc;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public a:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field private b:I


# direct methods
.method public constructor <init>([Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    const-string v0, "objects"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcgc;->a:[Ljava/lang/Object;

    .line 26
    const/4 v0, 0x0

    iput v0, p0, Lcgc;->b:I

    .line 27
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .line 35
    iget v0, p0, Lcgc;->b:I

    iget-object v1, p0, Lcgc;->a:[Ljava/lang/Object;

    array-length v1, v1

    if-ne v0, v1, :cond_0

    .line 36
    invoke-virtual {p0}, Lcgc;->b()V

    .line 38
    :cond_0
    iget-object v0, p0, Lcgc;->a:[Ljava/lang/Object;

    iget v1, p0, Lcgc;->b:I

    aget-object v0, v0, v1

    .line 39
    iget-object v1, p0, Lcgc;->a:[Ljava/lang/Object;

    iget v2, p0, Lcgc;->b:I

    const/4 v3, 0x0

    aput-object v3, v1, v2

    .line 40
    iget v1, p0, Lcgc;->b:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcgc;->b:I

    .line 41
    return-object v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 51
    iget v0, p0, Lcgc;->b:I

    if-gtz v0, :cond_0

    .line 52
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Pool has had more releases than acquires"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_0
    iget-object v0, p0, Lcgc;->a:[Ljava/lang/Object;

    iget v1, p0, Lcgc;->b:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcgc;->b:I

    aput-object p1, v0, v1

    .line 56
    return-void
.end method

.method protected b()V
    .locals 2

    .prologue
    .line 62
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Pool out of objects"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
