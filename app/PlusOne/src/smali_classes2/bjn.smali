.class public final Lbjn;
.super Lbjl;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbjl",
        "<",
        "Lbhl;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lbgf;

.field private d:Lbjr;


# direct methods
.method public constructor <init>(Lbgf;Lbiw;Ljava/util/concurrent/Executor;)V
    .locals 2

    .prologue
    .line 87
    invoke-direct {p0, p2, p3}, Lbjl;-><init>(Lbiw;Ljava/util/concurrent/Executor;)V

    .line 89
    const-string v0, "renderContext"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgf;

    iput-object v0, p0, Lbjn;->c:Lbgf;

    .line 90
    return-void
.end method


# virtual methods
.method public a(Landroid/media/MediaFormat;)Lbji;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/media/MediaFormat;",
            ")",
            "Lbji",
            "<",
            "Lbhl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 94
    invoke-static {p1}, Lbkf;->b(Landroid/media/MediaFormat;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 95
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x35

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "invalid media format for clip editor decoder factory "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 100
    :cond_0
    iget-object v0, p0, Lbjn;->d:Lbjr;

    if-nez v0, :cond_1

    .line 101
    iget-object v0, p0, Lbjn;->a:Lbiw;

    invoke-interface {v0}, Lbiw;->l()Z

    move-result v0

    if-nez v0, :cond_2

    .line 102
    invoke-static {}, Lbjt;->b()Lbjr;

    move-result-object v0

    iput-object v0, p0, Lbjn;->d:Lbjr;

    .line 110
    :cond_1
    :goto_0
    iget-object v0, p0, Lbjn;->d:Lbjr;

    .line 111
    invoke-interface {v0, p1}, Lbjr;->a(Landroid/media/MediaFormat;)Lbjs;

    move-result-object v0

    iget-object v1, p0, Lbjn;->c:Lbgf;

    .line 112
    invoke-virtual {v1}, Lbgf;->e()Lbfw;

    move-result-object v1

    iget-object v2, p0, Lbjn;->b:Ljava/util/concurrent/Executor;

    .line 110
    invoke-static {v0, v1, p1, v2}, Lbkc;->a(Lbjs;Lbhb;Landroid/media/MediaFormat;Ljava/util/concurrent/Executor;)Lbkc;

    move-result-object v0

    return-object v0

    .line 103
    :cond_2
    iget-object v0, p0, Lbjn;->a:Lbiw;

    invoke-interface {v0}, Lbiw;->q()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 104
    invoke-static {}, Lbjt;->a()Lbjr;

    move-result-object v0

    iput-object v0, p0, Lbjn;->d:Lbjr;

    goto :goto_0

    .line 106
    :cond_3
    invoke-static {}, Lbjt;->c()Lbjr;

    move-result-object v0

    iput-object v0, p0, Lbjn;->d:Lbjr;

    goto :goto_0
.end method
