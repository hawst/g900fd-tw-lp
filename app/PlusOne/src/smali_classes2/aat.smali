.class public final Laat;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Labx;

.field private b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Lzr;",
            ">;"
        }
    .end annotation
.end field

.field private c:Laau;

.field private d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Laay;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Labx;I)V
    .locals 3

    .prologue
    .line 392
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Laat;->b:Ljava/util/Set;

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Laat;->d:Ljava/util/Map;

    .line 393
    iput-object p1, p0, Laat;->a:Labx;

    .line 394
    packed-switch p2, :pswitch_data_0

    .line 405
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown cache-type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 396
    :pswitch_0
    new-instance v0, Laax;

    invoke-direct {v0}, Laax;-><init>()V

    iput-object v0, p0, Laat;->c:Laau;

    .line 403
    :goto_0
    return-void

    .line 399
    :pswitch_1
    new-instance v0, Laaw;

    invoke-direct {v0}, Laaw;-><init>()V

    iput-object v0, p0, Laat;->c:Laau;

    goto :goto_0

    .line 402
    :pswitch_2
    new-instance v0, Laav;

    invoke-direct {v0}, Laav;-><init>()V

    iput-object v0, p0, Laat;->c:Laau;

    goto :goto_0

    .line 394
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a()Laat;
    .locals 1

    .prologue
    .line 264
    invoke-static {}, Labx;->a()Labx;

    move-result-object v0

    .line 265
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Labx;->k()Laat;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 2

    .prologue
    .line 466
    iget-object v0, p0, Laat;->a:Labx;

    invoke-virtual {v0}, Labx;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Labx;->a()Labx;

    move-result-object v0

    iget-object v1, p0, Laat;->a:Labx;

    if-eq v0, v1, :cond_1

    .line 467
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempting to access FrameManager Frame data outside of graph run-loop!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 470
    :cond_1
    return-void
.end method


# virtual methods
.method public a(Laap;)Laap;
    .locals 3

    .prologue
    .line 318
    iget-boolean v0, p1, Laap;->b:Z

    if-nez v0, :cond_0

    .line 319
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Frame "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " must be read-only to import into another FrameManager!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 322
    :cond_0
    invoke-virtual {p1, p0}, Laap;->a(Laat;)Laap;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Laap;
    .locals 1

    .prologue
    .line 372
    invoke-direct {p0}, Laat;->e()V

    .line 373
    invoke-virtual {p0, p1}, Laat;->b(Ljava/lang/String;)Laay;

    move-result-object v0

    invoke-virtual {v0}, Laay;->c()Laap;

    move-result-object v0

    return-object v0
.end method

.method a(II[II)Lzr;
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Laat;->c:Laau;

    invoke-virtual {v0, p1, p2, p3, p4}, Laau;->a(II[II)Lzr;

    move-result-object v0

    return-object v0
.end method

.method public a(Laap;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 364
    invoke-direct {p0}, Laat;->e()V

    .line 365
    invoke-virtual {p0, p2}, Laat;->b(Ljava/lang/String;)Laay;

    move-result-object v0

    invoke-virtual {v0, p1}, Laay;->a(Laap;)V

    .line 366
    return-void
.end method

.method a(Lzr;)V
    .locals 1

    .prologue
    .line 414
    if-eqz p1, :cond_0

    .line 415
    iget-object v0, p0, Laat;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 418
    :cond_0
    return-void
.end method

.method public b(Ljava/lang/String;)Laay;
    .locals 3

    .prologue
    .line 440
    iget-object v0, p0, Laat;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laay;

    .line 441
    if-nez v0, :cond_0

    .line 442
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown frame slot \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 444
    :cond_0
    return-object v0
.end method

.method public b()Lacs;
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Laat;->a:Labx;

    invoke-virtual {v0}, Labx;->b()Lacs;

    move-result-object v0

    return-object v0
.end method

.method b(Lzr;)V
    .locals 1

    .prologue
    .line 421
    invoke-virtual {p1}, Lzr;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Laat;->c:Laau;

    invoke-virtual {v0, p1}, Laau;->a(Lzr;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 422
    :cond_0
    invoke-virtual {p1}, Lzr;->g()V

    .line 423
    iget-object v0, p0, Laat;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 426
    :cond_1
    return-void
.end method

.method public c()Labx;
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Laat;->a:Labx;

    return-object v0
.end method

.method d()V
    .locals 2

    .prologue
    .line 432
    iget-object v0, p0, Laat;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzr;

    .line 433
    invoke-virtual {v0}, Lzr;->g()V

    goto :goto_0

    .line 435
    :cond_0
    iget-object v0, p0, Laat;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 436
    iget-object v0, p0, Laat;->c:Laau;

    invoke-virtual {v0}, Laau;->a()V

    .line 437
    return-void
.end method
