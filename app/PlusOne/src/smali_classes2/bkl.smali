.class public final Lbkl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbkr;


# instance fields
.field private final a:[B

.field private final b:Lbkn;

.field private final c:Lbku;

.field private final d:Lbkp;


# direct methods
.method public constructor <init>([BLbkn;Lbku;Lbkp;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lbkl;->a:[B

    .line 35
    const-string v0, "generalMetrics"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbkn;

    iput-object v0, p0, Lbkl;->b:Lbkn;

    .line 36
    const-string v0, "pointMetrics"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbku;

    iput-object v0, p0, Lbkl;->c:Lbku;

    .line 37
    iput-object p4, p0, Lbkl;->d:Lbkp;

    .line 38
    return-void
.end method

.method public static a(Lbkr;)Lbkm;
    .locals 1

    .prologue
    .line 83
    new-instance v0, Lbkm;

    invoke-direct {v0, p0}, Lbkm;-><init>(Lbkr;)V

    return-object v0
.end method


# virtual methods
.method public a()Z
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lbkl;->a:[B

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()[B
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lbkl;->a:[B

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lbkl;->a:[B

    iget-object v1, p0, Lbkl;->a:[B

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v0

    goto :goto_0
.end method

.method public c()Lbkn;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lbkl;->b:Lbkn;

    return-object v0
.end method

.method public d()Lbku;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lbkl;->c:Lbku;

    return-object v0
.end method

.method public e()Lbkp;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lbkl;->d:Lbkp;

    return-object v0
.end method
