.class final Lakj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lacg;


# instance fields
.field private final a:Laan;

.field private final b:Landroid/net/Uri;

.field private final c:Lbkm;

.field private final d:Laka;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Laka",
            "<*>;"
        }
    .end annotation
.end field

.field private final e:Laki;

.field private synthetic f:Lakd;


# direct methods
.method constructor <init>(Lakd;Laan;Landroid/net/Uri;Lbkm;Laka;Laki;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Laan;",
            "Landroid/net/Uri;",
            "Lbkm;",
            "Laka",
            "<*>;",
            "Laki;",
            ")V"
        }
    .end annotation

    .prologue
    .line 484
    iput-object p1, p0, Lakj;->f:Lakd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 485
    iput-object p2, p0, Lakj;->a:Laan;

    .line 486
    iput-object p3, p0, Lakj;->b:Landroid/net/Uri;

    .line 487
    iput-object p4, p0, Lakj;->c:Lbkm;

    .line 488
    iput-object p5, p0, Lakj;->d:Laka;

    .line 489
    iput-object p6, p0, Lakj;->e:Laki;

    .line 490
    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 523
    iget-object v0, p0, Lakj;->a:Laan;

    invoke-virtual {v0}, Laan;->a()V

    .line 524
    iget-object v0, p0, Lakj;->f:Lakd;

    invoke-static {v0}, Lakd;->e(Lakd;)Labx;

    move-result-object v0

    invoke-virtual {v0}, Labx;->m()V

    .line 525
    iget-object v0, p0, Lakj;->f:Lakd;

    invoke-static {v0}, Lakd;->e(Lakd;)Labx;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Labx;->a(Lacg;)V

    .line 526
    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    .prologue
    .line 494
    iget-object v0, p0, Lakj;->f:Lakd;

    invoke-static {v0}, Lakd;->c(Lakd;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 495
    :try_start_0
    invoke-direct {p0}, Lakj;->b()V

    .line 496
    iget-object v0, p0, Lakj;->d:Laka;

    invoke-virtual {v0}, Laka;->a()J

    move-result-wide v2

    .line 497
    iget-object v0, p0, Lakj;->c:Lbkm;

    invoke-virtual {v0}, Lbkm;->a()Lbko;

    move-result-object v0

    .line 498
    iget-object v4, p0, Lakj;->c:Lbkm;

    invoke-virtual {v4}, Lbkm;->b()Lbkv;

    move-result-object v4

    .line 499
    invoke-virtual {v0, v2, v3}, Lbko;->c(J)Lbko;

    .line 501
    iget-object v5, p0, Lakj;->f:Lakd;

    invoke-static {v5}, Lakd;->d(Lakd;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 502
    invoke-static {}, Lakd;->b()Ljava/lang/String;

    move-result-object v0

    const-string v5, "Cancel received while running detection graph"

    invoke-static {v0, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 503
    invoke-virtual {v4, v2, v3}, Lbkv;->a(J)Lbkv;

    .line 504
    iget-object v0, p0, Lakj;->e:Laki;

    iget-object v2, p0, Lakj;->b:Landroid/net/Uri;

    iget-object v3, p0, Lakj;->c:Lbkm;

    invoke-interface {v0, v2, v3}, Laki;->b(Landroid/net/Uri;Lbkm;)V

    .line 509
    :goto_0
    monitor-exit v1

    return-void

    .line 506
    :cond_0
    invoke-virtual {v0, v2, v3}, Lbko;->b(J)Lbko;

    .line 507
    iget-object v0, p0, Lakj;->e:Laki;

    iget-object v2, p0, Lakj;->b:Landroid/net/Uri;

    iget-object v3, p0, Lakj;->c:Lbkm;

    invoke-interface {v0, v2, v3}, Laki;->a(Landroid/net/Uri;Lbkm;)V

    goto :goto_0

    .line 509
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Ljava/lang/Exception;)V
    .locals 2

    .prologue
    .line 514
    invoke-static {}, Lakd;->b()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Error while running detection graph"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 515
    iget-object v0, p0, Lakj;->f:Lakd;

    invoke-static {v0}, Lakd;->c(Lakd;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 516
    :try_start_0
    invoke-direct {p0}, Lakj;->b()V

    .line 517
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 518
    iget-object v0, p0, Lakj;->e:Laki;

    iget-object v1, p0, Lakj;->b:Landroid/net/Uri;

    invoke-interface {v0, v1, p1}, Laki;->a(Landroid/net/Uri;Ljava/lang/Exception;)V

    .line 519
    return-void

    .line 517
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
