.class public final Ljim;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:J

.field private final c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;IJ)V
    .locals 1

    .prologue
    .line 105
    const-string v0, "RemoveItemTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 106
    iput-object p1, p0, Ljim;->a:Landroid/content/Context;

    .line 107
    iput p2, p0, Ljim;->c:I

    .line 108
    iput-wide p3, p0, Ljim;->b:J

    .line 109
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 4

    .prologue
    .line 113
    new-instance v1, Lhoz;

    const/4 v0, 0x1

    invoke-direct {v1, v0}, Lhoz;-><init>(Z)V

    .line 114
    iget-object v0, p0, Ljim;->a:Landroid/content/Context;

    const-class v2, Ljhi;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljhi;

    iget v2, p0, Ljim;->c:I

    .line 115
    invoke-interface {v0, v2}, Ljhi;->a(I)Ljhh;

    move-result-object v0

    .line 116
    iget-wide v2, p0, Ljim;->b:J

    invoke-interface {v0, v2, v3}, Ljhh;->a(J)Z

    .line 117
    return-object v1
.end method
