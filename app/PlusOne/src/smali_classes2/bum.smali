.class public final Lbum;
.super Loxu;
.source "PG"


# static fields
.field public static final a:[Lbum;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:J

.field public e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 408
    const/4 v0, 0x0

    new-array v0, v0, [Lbum;

    sput-object v0, Lbum;->a:[Lbum;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 409
    invoke-direct {p0}, Loxu;-><init>()V

    .line 412
    const-string v0, ""

    iput-object v0, p0, Lbum;->b:Ljava/lang/String;

    .line 415
    const-string v0, ""

    iput-object v0, p0, Lbum;->c:Ljava/lang/String;

    .line 418
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lbum;->d:J

    .line 421
    const/4 v0, 0x0

    iput v0, p0, Lbum;->e:I

    .line 409
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    .line 451
    const/4 v0, 0x0

    .line 452
    iget-object v1, p0, Lbum;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 453
    const/4 v0, 0x1

    iget-object v1, p0, Lbum;->b:Ljava/lang/String;

    .line 454
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 456
    :cond_0
    iget-object v1, p0, Lbum;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 457
    const/4 v1, 0x2

    iget-object v2, p0, Lbum;->c:Ljava/lang/String;

    .line 458
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 460
    :cond_1
    iget-wide v2, p0, Lbum;->d:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 461
    const/4 v1, 0x3

    iget-wide v2, p0, Lbum;->d:J

    .line 462
    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 464
    :cond_2
    iget v1, p0, Lbum;->e:I

    if-eqz v1, :cond_3

    .line 465
    const/4 v1, 0x4

    iget v2, p0, Lbum;->e:I

    .line 466
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 468
    :cond_3
    iput v0, p0, Lbum;->ai:I

    .line 469
    return v0
.end method

.method public a(Loxn;)Lbum;
    .locals 2

    .prologue
    .line 477
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 478
    sparse-switch v0, :sswitch_data_0

    .line 482
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 483
    :sswitch_0
    return-object p0

    .line 488
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbum;->b:Ljava/lang/String;

    goto :goto_0

    .line 492
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbum;->c:Ljava/lang/String;

    goto :goto_0

    .line 496
    :sswitch_3
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    iput-wide v0, p0, Lbum;->d:J

    goto :goto_0

    .line 500
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    iput v0, p0, Lbum;->e:I

    goto :goto_0

    .line 478
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 435
    iget-object v0, p0, Lbum;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 436
    const/4 v0, 0x1

    iget-object v1, p0, Lbum;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 438
    :cond_0
    iget-object v0, p0, Lbum;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 439
    const/4 v0, 0x2

    iget-object v1, p0, Lbum;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 441
    :cond_1
    iget-wide v0, p0, Lbum;->d:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 442
    const/4 v0, 0x3

    iget-wide v2, p0, Lbum;->d:J

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 444
    :cond_2
    iget v0, p0, Lbum;->e:I

    if-eqz v0, :cond_3

    .line 445
    const/4 v0, 0x4

    iget v1, p0, Lbum;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 447
    :cond_3
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 405
    invoke-virtual {p0, p1}, Lbum;->a(Loxn;)Lbum;

    move-result-object v0

    return-object v0
.end method
