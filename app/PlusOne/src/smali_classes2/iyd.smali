.class final Liyd;
.super Lhny;
.source "PG"


# instance fields
.field private final a:Lkfd;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Lnpx;

.field private final e:[Lnqe;

.field private final f:Z

.field private final h:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lnpx;[Lnqe;ZZ)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 34
    const-class v0, Lkfd;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    iput-object v0, p0, Liyd;->a:Lkfd;

    .line 35
    iput-object p3, p0, Liyd;->b:Ljava/lang/String;

    .line 36
    iput-object p4, p0, Liyd;->c:Ljava/lang/String;

    .line 37
    iput-object p5, p0, Liyd;->d:Lnpx;

    .line 38
    iput-object p6, p0, Liyd;->e:[Lnqe;

    .line 39
    iput-boolean p7, p0, Liyd;->f:Z

    .line 40
    iput-boolean p8, p0, Liyd;->h:Z

    .line 41
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 9

    .prologue
    .line 46
    new-instance v0, Lixx;

    .line 47
    invoke-virtual {p0}, Liyd;->f()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Liyd;->b:Ljava/lang/String;

    iget-object v3, p0, Liyd;->c:Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Liyd;->d:Lnpx;

    iget-object v6, p0, Liyd;->e:[Lnqe;

    iget-boolean v7, p0, Liyd;->f:Z

    iget-boolean v8, p0, Liyd;->h:Z

    invoke-direct/range {v0 .. v8}, Lixx;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILnpx;[Lnqe;ZZ)V

    .line 49
    iget-object v1, p0, Liyd;->a:Lkfd;

    invoke-interface {v1, v0}, Lkfd;->a(Lkff;)V

    .line 51
    invoke-virtual {v0}, Lixx;->t()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 52
    new-instance v1, Lhoz;

    iget v2, v0, Lkff;->i:I

    iget-object v3, v0, Lkff;->k:Ljava/lang/Exception;

    .line 53
    iget-object v0, v0, Lkff;->j:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v0}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    move-object v0, v1

    .line 71
    :goto_0
    return-object v0

    .line 56
    :cond_0
    const/4 v1, 0x0

    .line 57
    invoke-virtual {v0}, Lixx;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmez;

    iget-object v0, v0, Lmez;->a:Lnql;

    .line 58
    iget-object v2, v0, Lnql;->b:Ljava/lang/Boolean;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 62
    :goto_1
    new-instance v1, Lhoz;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lhoz;-><init>(Z)V

    .line 63
    invoke-virtual {v1}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v2

    .line 65
    if-eqz v0, :cond_1

    .line 66
    const-string v3, "oob_response"

    new-instance v4, Lhyt;

    invoke-direct {v4, v0}, Lhyt;-><init>(Loxu;)V

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 69
    :cond_1
    const-string v0, "allow_non_google_accounts"

    iget-boolean v3, p0, Liyd;->f:Z

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    move-object v0, v1

    .line 71
    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method
