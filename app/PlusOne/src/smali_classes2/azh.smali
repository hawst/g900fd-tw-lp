.class public final Lazh;
.super Layr;
.source "PG"


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/32 v2, 0x493e0

    .line 17
    new-instance v0, Lays;

    invoke-direct {v0}, Lays;-><init>()V

    const v1, 0x7f020428

    .line 18
    invoke-virtual {v0, v1}, Lays;->a(I)Lays;

    move-result-object v0

    .line 19
    invoke-virtual {v0, v2, v3}, Lays;->b(J)Lays;

    move-result-object v0

    .line 20
    invoke-virtual {v0, v2, v3}, Lays;->c(J)Lays;

    move-result-object v0

    .line 21
    invoke-virtual {v0, v2, v3}, Lays;->d(J)Lays;

    move-result-object v0

    const v1, 0x7f020409

    .line 22
    invoke-virtual {v0, v1}, Lays;->b(I)Lays;

    move-result-object v0

    const/4 v1, 0x4

    .line 23
    invoke-virtual {v0, v1}, Lays;->c(I)Lays;

    move-result-object v0

    .line 17
    invoke-direct {p0, v0}, Layr;-><init>(Lays;)V

    .line 25
    return-void
.end method


# virtual methods
.method protected a(Layj;Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 30
    new-instance v0, Layu;

    invoke-direct {v0}, Layu;-><init>()V

    .line 31
    invoke-virtual {v0, p1}, Layu;->a(Layj;)Layu;

    move-result-object v0

    const v1, 0x7f0b00a6

    .line 32
    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Layu;->a(I)Layu;

    move-result-object v0

    const/high16 v1, 0x42100000    # 36.0f

    .line 33
    invoke-virtual {v0, v1}, Layu;->a(F)Layu;

    move-result-object v0

    const/high16 v1, 0x42300000    # 44.0f

    .line 34
    invoke-virtual {v0, v1}, Layu;->b(F)Layu;

    move-result-object v0

    const-string v1, "sans-serif"

    .line 35
    invoke-static {v1, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Layu;->a(Landroid/graphics/Typeface;)Layu;

    move-result-object v0

    .line 36
    invoke-virtual {v0, v2}, Layu;->a(Z)Layu;

    move-result-object v0

    const/16 v1, 0x19

    .line 37
    invoke-virtual {v0, v1}, Layu;->c(I)Layu;

    move-result-object v0

    .line 38
    invoke-virtual {v0, v2}, Layu;->b(Z)Layu;

    move-result-object v0

    const v1, 0x7f0b00b3

    .line 39
    invoke-virtual {p3, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Layu;->g(I)Layu;

    move-result-object v0

    .line 40
    invoke-virtual {v0}, Layu;->a()Layt;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbmd;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbmd;",
            ")",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 54
    invoke-virtual {p0}, Lazh;->a()Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->a(Lbmd;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public b(Layl;)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 60
    invoke-virtual {p0}, Lazh;->a()Layj;

    move-result-object v0

    invoke-interface {v0, p1}, Layj;->b(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    return-object v0
.end method

.method protected f()Layj;
    .locals 7

    .prologue
    const/4 v2, 0x4

    const/high16 v6, 0x3f800000    # 1.0f

    .line 45
    new-instance v0, Lazi;

    new-array v1, v2, [I

    fill-array-data v1, :array_0

    new-array v2, v2, [Layj;

    const/4 v3, 0x0

    new-instance v4, Lazt;

    sget-object v5, Lazv;->d:Lazv;

    invoke-direct {v4, p0, v5, v6}, Lazt;-><init>(Layj;Lazv;F)V

    aput-object v4, v2, v3

    const/4 v3, 0x1

    new-instance v4, Lazt;

    sget-object v5, Lazv;->c:Lazv;

    invoke-direct {v4, p0, v5, v6}, Lazt;-><init>(Layj;Lazv;F)V

    aput-object v4, v2, v3

    const/4 v3, 0x2

    new-instance v4, Lazt;

    sget-object v5, Lazv;->b:Lazv;

    invoke-direct {v4, p0, v5, v6}, Lazt;-><init>(Layj;Lazv;F)V

    aput-object v4, v2, v3

    const/4 v3, 0x3

    new-instance v4, Lazt;

    sget-object v5, Lazv;->a:Lazv;

    invoke-direct {v4, p0, v5, v6}, Lazt;-><init>(Layj;Lazv;F)V

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lazi;-><init>([I[Layj;)V

    return-object v0

    :array_0
    .array-data 4
        0x1
        0x2
        0x3
        0x4
    .end array-data
.end method
