.class public final Lbru;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private synthetic a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)V
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lbru;->a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    .line 65
    iget-object v0, p0, Lbru;->a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->a(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 67
    :try_start_0
    iget-object v0, p0, Lbru;->a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->b(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)Z

    move-result v2

    .line 68
    iget-object v0, p0, Lbru;->a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->c(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)J

    move-result-wide v4

    .line 69
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    if-eqz v2, :cond_1

    .line 73
    iget-object v0, p0, Lbru;->a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    .line 74
    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 73
    invoke-static {v0, v4, v5, v3, v6}, Landroid/provider/MediaStore$Video$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 83
    :goto_0
    iget-object v1, p0, Lbru;->a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    invoke-static {v1}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->a(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 85
    :try_start_1
    iget-object v3, p0, Lbru;->a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    invoke-static {v3}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->b(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)Z

    move-result v3

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lbru;->a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    invoke-static {v2}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->c(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)J

    move-result-wide v2

    cmp-long v2, v4, v2

    if-nez v2, :cond_0

    .line 86
    iget-object v2, p0, Lbru;->a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    invoke-static {v2, v0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->a(Lcom/google/android/apps/moviemaker/picker/PickerTileView;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    .line 87
    iget-object v0, p0, Lbru;->a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    iget-object v2, p0, Lbru;->a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    invoke-static {v2}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->d(Lcom/google/android/apps/moviemaker/picker/PickerTileView;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->post(Ljava/lang/Runnable;)Z

    .line 89
    :cond_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    return-void

    .line 69
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 78
    :cond_1
    iget-object v0, p0, Lbru;->a:Lcom/google/android/apps/moviemaker/picker/PickerTileView;

    .line 79
    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/picker/PickerTileView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 78
    invoke-static {v0, v4, v5, v3, v6}, Landroid/provider/MediaStore$Images$Thumbnails;->getThumbnail(Landroid/content/ContentResolver;JILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    .line 89
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method
