.class final Lbiq;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lbiq;->a:[B

    return-void

    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data
.end method

.method public static a(Lbir;)I
    .locals 1

    .prologue
    .line 22
    invoke-virtual {p0}, Lbir;->f()I

    move-result v0

    .line 23
    shr-int/lit8 v0, v0, 0x18

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method public static a(Ljava/nio/ByteBuffer;I)V
    .locals 5

    .prologue
    .line 54
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    sub-int v3, v0, p1

    move v2, v3

    .line 56
    :goto_0
    add-int v0, v3, p1

    if-ge v2, v0, :cond_1

    .line 57
    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 58
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    and-int/lit16 v1, v0, 0xff

    const/4 v0, 0x1

    :goto_1
    const/4 v4, 0x4

    if-ge v0, v4, :cond_0

    shl-int/lit8 v1, v1, 0x8

    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    or-int/2addr v1, v4

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    const-string v0, "result"

    const-string v4, "Non-zero top bit"

    invoke-static {v1, v0, v4}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    .line 59
    invoke-virtual {p0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 60
    sget-object v0, Lbiq;->a:[B

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 61
    add-int/lit8 v0, v1, 0x4

    add-int/2addr v0, v2

    move v2, v0

    .line 62
    goto :goto_0

    .line 63
    :cond_1
    add-int v0, v3, p1

    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 64
    return-void
.end method
