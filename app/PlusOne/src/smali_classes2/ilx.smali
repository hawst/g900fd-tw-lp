.class public final Lilx;
.super Lilt;
.source "PG"


# instance fields
.field private a:Lgrz;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lilt;-><init>()V

    .line 18
    new-instance v0, Lgrz;

    invoke-direct {v0}, Lgrz;-><init>()V

    iput-object v0, p0, Lilx;->a:Lgrz;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 37
    :try_start_0
    iget-object v0, p0, Lilx;->a:Lgrz;

    invoke-virtual {v0, p1}, Lgrz;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 40
    new-instance v2, Lgrz;

    invoke-direct {v2}, Lgrz;-><init>()V

    iput-object v2, p0, Lilx;->a:Lgrz;
    :try_end_0
    .catch Lgfj; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lgfi; {:try_start_0 .. :try_end_0} :catch_1

    .line 48
    :goto_0
    return-object v0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    const-string v2, "GcorePlacePickerIntentBuilderImpl"

    const-string v3, "unable to build PlacePicker intent"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 45
    goto :goto_0

    .line 46
    :catch_1
    move-exception v0

    .line 47
    const-string v2, "GcorePlacePickerIntentBuilderImpl"

    const-string v3, "unable to build PlacePicker intent"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 48
    goto :goto_0
.end method

.method public a(Linh;)Lilw;
    .locals 6

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-interface {p1}, Linh;->a()Ling;

    move-result-object v1

    iget-wide v2, v1, Ling;->a:D

    invoke-interface {p1}, Linh;->a()Ling;

    move-result-object v1

    iget-wide v4, v1, Ling;->b:D

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 29
    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    invoke-interface {p1}, Linh;->b()Ling;

    move-result-object v2

    iget-wide v2, v2, Ling;->a:D

    invoke-interface {p1}, Linh;->b()Ling;

    move-result-object v4

    iget-wide v4, v4, Ling;->b:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    .line 30
    iget-object v2, p0, Lilx;->a:Lgrz;

    new-instance v3, Lcom/google/android/gms/maps/model/LatLngBounds;

    invoke-direct {v3, v0, v1}, Lcom/google/android/gms/maps/model/LatLngBounds;-><init>(Lcom/google/android/gms/maps/model/LatLng;Lcom/google/android/gms/maps/model/LatLng;)V

    invoke-virtual {v2, v3}, Lgrz;->a(Lcom/google/android/gms/maps/model/LatLngBounds;)Lgrz;

    .line 31
    return-object p0
.end method
