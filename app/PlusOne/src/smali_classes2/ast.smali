.class final Last;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation
.end field

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljeo;

.field private d:I

.field private synthetic e:Laso;


# direct methods
.method constructor <init>(Laso;)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Last;->e:Laso;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Integer;)Ljava/lang/Void;
    .locals 3

    .prologue
    .line 257
    iget v0, p0, Last;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 259
    iget-object v0, p0, Last;->c:Ljeo;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v0, Ljeo;->d:I

    .line 262
    iget-object v0, p0, Last;->c:Ljeo;

    iget-object v0, v0, Ljeo;->b:Ljew;

    iget-object v0, v0, Ljew;->c:Ljava/util/List;

    iget-object v1, p0, Last;->e:Laso;

    iget-object v2, p0, Last;->a:Ljava/util/List;

    invoke-static {v1, v2}, Laso;->c(Laso;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 265
    iget-object v0, p0, Last;->c:Ljeo;

    iget-object v0, v0, Ljeo;->b:Ljew;

    iget-object v0, v0, Ljew;->a:Ljava/util/List;

    iget-object v1, p0, Last;->e:Laso;

    iget-object v2, p0, Last;->b:Ljava/util/List;

    invoke-static {v1, v2}, Laso;->c(Laso;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 270
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a()V
    .locals 3

    .prologue
    .line 275
    iget v0, p0, Last;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 276
    iget-object v0, p0, Last;->e:Laso;

    invoke-static {v0}, Laso;->d(Laso;)Ljfb;

    move-result-object v0

    iget v1, p0, Last;->d:I

    iget-object v2, p0, Last;->c:Ljeo;

    invoke-virtual {v0, v1, v2}, Ljfb;->a(ILjeo;)V

    .line 278
    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 227
    check-cast p1, [Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Last;->a([Ljava/lang/Integer;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 227
    invoke-virtual {p0}, Last;->a()V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 237
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Last;->a:Ljava/util/List;

    .line 238
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Last;->b:Ljava/util/List;

    .line 239
    iget-object v0, p0, Last;->b:Ljava/util/List;

    iget-object v1, p0, Last;->e:Laso;

    iget-object v2, p0, Last;->e:Laso;

    invoke-static {v2}, Laso;->a(Laso;)Lawp;

    move-result-object v2

    invoke-interface {v2}, Lawp;->b()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Laso;->a(Laso;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 240
    iget-object v0, p0, Last;->b:Ljava/util/List;

    iget-object v1, p0, Last;->e:Laso;

    iget-object v2, p0, Last;->e:Laso;

    invoke-static {v2}, Laso;->a(Laso;)Lawp;

    move-result-object v2

    invoke-interface {v2}, Lawp;->c()Ljava/util/List;

    move-result-object v2

    invoke-static {v1, v2}, Laso;->b(Laso;Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 242
    iget-object v0, p0, Last;->e:Laso;

    invoke-static {v0}, Laso;->a(Laso;)Lawp;

    move-result-object v0

    invoke-interface {v0}, Lawp;->F()Lboi;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Last;->a:Ljava/util/List;

    iget-object v1, p0, Last;->e:Laso;

    invoke-static {v1}, Laso;->a(Laso;)Lawp;

    move-result-object v1

    invoke-interface {v1}, Lawp;->F()Lboi;

    move-result-object v1

    invoke-virtual {v1}, Lboi;->f()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 247
    :cond_0
    iget-object v0, p0, Last;->e:Laso;

    invoke-static {v0}, Laso;->b(Laso;)Ljeo;

    move-result-object v0

    iput-object v0, p0, Last;->c:Ljeo;

    .line 248
    iget-object v0, p0, Last;->c:Ljeo;

    invoke-static {v0}, Laso;->a(Ljeo;)V

    .line 249
    iget-object v0, p0, Last;->e:Laso;

    iget-object v1, p0, Last;->e:Laso;

    invoke-static {v1}, Laso;->c(Laso;)Ljeo;

    move-result-object v1

    invoke-static {v0, v1}, Laso;->a(Laso;Ljeo;)Ljeo;

    .line 252
    iget-object v0, p0, Last;->e:Laso;

    invoke-static {v0}, Laso;->a(Laso;)Lawp;

    move-result-object v0

    invoke-interface {v0}, Lawp;->ba()I

    move-result v0

    iput v0, p0, Last;->d:I

    .line 253
    return-void
.end method
