.class public final Lkgl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkgj;


# instance fields
.field private a:Lkgm;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Lkgm;

    invoke-direct {v0}, Lkgm;-><init>()V

    iput-object v0, p0, Lkgl;->a:Lkgm;

    .line 82
    return-void
.end method


# virtual methods
.method public declared-synchronized a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    const/16 v8, 0xa

    .line 37
    monitor-enter p0

    :try_start_0
    const-string v3, "(^(http|https)://[^?]+)(\\?.*|)"

    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    :goto_0
    if-nez v3, :cond_1

    .line 38
    :goto_1
    if-nez v0, :cond_2

    move v0, v1

    .line 46
    :goto_2
    monitor-exit p0

    return v0

    :cond_0
    move-object v3, v0

    .line 37
    goto :goto_0

    :cond_1
    :try_start_1
    new-instance v0, Lkgk;

    invoke-direct {v0, v3, p4, p5}, Lkgk;-><init>(Ljava/lang/String;ILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 41
    :cond_2
    :try_start_2
    iget-object v3, p0, Lkgl;->a:Lkgm;

    invoke-virtual {v3, v0}, Lkgm;->a(Lkgk;)I

    move-result v3

    if-ne v3, v8, :cond_4

    .line 42
    iget-object v3, p0, Lkgl;->a:Lkgm;

    const/16 v4, 0xa

    invoke-virtual {v3, v0, v4}, Lkgm;->a(Lkgk;I)Ljava/util/List;

    move-result-object v4

    .line 43
    const-class v0, Ljgn;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljgn;

    const/16 v3, 0xa

    new-array v5, v3, [Lqbk;

    move v3, v1

    :goto_3
    if-ge v3, v8, :cond_3

    new-instance v6, Lqbk;

    invoke-direct {v6}, Lqbk;-><init>()V

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkgk;

    const/4 v7, 0x5

    iput v7, v6, Lqbk;->b:I

    invoke-virtual {v1}, Lkgk;->b()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iput-object v7, v6, Lqbk;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Lkgk;->d()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    iput-object v7, v6, Lqbk;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Lkgk;->c()[Ljava/lang/String;

    move-result-object v1

    iput-object v1, v6, Lqbk;->c:[Ljava/lang/String;

    aput-object v6, v5, v3

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    :cond_3
    new-instance v1, Lkok;

    invoke-static {p1}, Ljgr;->a(Landroid/content/Context;)I

    move-result v3

    invoke-interface {v0}, Ljgn;->k()I

    move-result v0

    int-to-float v0, v0

    invoke-direct {v1, p2, v3, v0, v5}, Lkok;-><init>(Ljava/lang/String;IF[Lqbk;)V

    invoke-virtual {v1, p1}, Lkok;->a(Landroid/content/Context;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v2

    .line 44
    goto :goto_2

    :cond_4
    move v0, v1

    .line 46
    goto :goto_2
.end method
