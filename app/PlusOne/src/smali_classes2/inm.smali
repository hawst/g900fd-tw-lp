.class public final Linm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Linl;


# instance fields
.field private a:Lgwt;


# direct methods
.method public constructor <init>(Lgwt;)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Linm;->a:Lgwt;

    .line 18
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Linm;->a:Lgwt;

    invoke-virtual {v0}, Lgwt;->a()V

    .line 34
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 22
    invoke-static {p1}, Lgwp;->a(Landroid/graphics/Bitmap;)Lgwo;

    move-result-object v0

    .line 23
    iget-object v1, p0, Linm;->a:Lgwt;

    invoke-virtual {v1, v0}, Lgwt;->a(Lgwo;)V

    .line 24
    return-void
.end method

.method public a(Ling;)V
    .locals 6

    .prologue
    .line 28
    iget-object v0, p0, Linm;->a:Lgwt;

    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, p1, Ling;->a:D

    iget-wide v4, p1, Ling;->b:D

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v0, v1}, Lgwt;->a(Lcom/google/android/gms/maps/model/LatLng;)V

    .line 29
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 38
    if-ne p0, p1, :cond_0

    .line 39
    const/4 v0, 0x1

    .line 44
    :goto_0
    return v0

    .line 40
    :cond_0
    instance-of v0, p1, Linm;

    if-nez v0, :cond_1

    .line 41
    const/4 v0, 0x0

    goto :goto_0

    .line 43
    :cond_1
    check-cast p1, Linm;

    .line 44
    iget-object v0, p0, Linm;->a:Lgwt;

    iget-object v1, p1, Linm;->a:Lgwt;

    invoke-virtual {v0, v1}, Lgwt;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Linm;->a:Lgwt;

    invoke-virtual {v0}, Lgwt;->hashCode()I

    move-result v0

    return v0
.end method
