.class public Lbry;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbrw;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private b:Lbsr;

.field private c:Z

.field private d:J

.field private e:I

.field private f:I

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lbry;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbry;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a([BII)I
    .locals 8

    .prologue
    .line 74
    iget-object v0, p0, Lbry;->b:Lbsr;

    const-string v1, "mAudioTrack"

    const-string v2, "must call initialize() first"

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 75
    iget-boolean v0, p0, Lbry;->c:Z

    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {p0}, Lbry;->f()J

    move-result-wide v0

    .line 77
    const-wide/16 v2, 0x7530

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 78
    sget-object v2, Lbry;->a:Ljava/lang/String;

    sget-object v2, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v3, "Audio sink buffer running low: %d ms (%d bytes)"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-wide/16 v6, 0x3e8

    div-long/2addr v0, v6

    .line 80
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    invoke-virtual {p0}, Lbry;->e()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    .line 78
    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    .line 83
    :cond_0
    iget-object v0, p0, Lbry;->b:Lbsr;

    invoke-virtual {v0, p1, p2, p3}, Lbsr;->a([BII)I

    move-result v0

    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lbry;->b:Lbsr;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lbry;->b:Lbsr;

    invoke-virtual {v0}, Lbsr;->c()V

    .line 52
    const/4 v0, 0x0

    iput-object v0, p0, Lbry;->b:Lbsr;

    .line 54
    :cond_0
    return-void
.end method

.method public a(III)V
    .locals 3

    .prologue
    .line 40
    iget-object v0, p0, Lbry;->b:Lbsr;

    const-string v1, "mAudioTrack"

    const-string v2, "sink can only be initialized once"

    invoke-static {v0, v1, v2}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 41
    iput p1, p0, Lbry;->e:I

    .line 42
    iput p2, p0, Lbry;->f:I

    .line 43
    iput p3, p0, Lbry;->g:I

    .line 45
    const-wide/16 v0, 0x0

    invoke-virtual {p0, v0, v1}, Lbry;->a(J)V

    .line 46
    return-void
.end method

.method public a(J)V
    .locals 11

    .prologue
    const/4 v1, 0x3

    .line 121
    const-string v0, "presentationTimeUs"

    .line 122
    invoke-static {p1, p2, v0}, Lcec;->b(JLjava/lang/CharSequence;)J

    move-result-wide v2

    iput-wide v2, p0, Lbry;->d:J

    .line 123
    iget-boolean v0, p0, Lbry;->c:Z

    const-string v2, "mIsPlaying"

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcgp;->a(ZLjava/lang/CharSequence;Z)V

    .line 127
    invoke-virtual {p0}, Lbry;->a()V

    .line 128
    iget v0, p0, Lbry;->e:I

    iget v2, p0, Lbry;->f:I

    iget v5, p0, Lbry;->g:I

    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x29

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "unsupported bytes per sample: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    :pswitch_0
    move v4, v1

    :goto_0
    packed-switch v5, :pswitch_data_1

    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0x26

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "unsupported channel count: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    :pswitch_2
    const/4 v4, 0x2

    goto :goto_0

    :pswitch_3
    const/4 v3, 0x4

    :goto_1
    invoke-static {v2, v3, v4}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v6

    const-wide/32 v8, 0xf4240

    invoke-static {v8, v9, v0, v5, v2}, Lceg;->a(JIII)I

    move-result v0

    shl-int/lit8 v5, v6, 0x3

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v5

    new-instance v0, Lbsr;

    const/4 v6, 0x1

    invoke-direct/range {v0 .. v6}, Lbsr;-><init>(IIIIII)V

    iput-object v0, p0, Lbry;->b:Lbsr;

    .line 129
    return-void

    .line 128
    :pswitch_4
    const/16 v3, 0xc

    goto :goto_1

    :pswitch_5
    const/16 v3, 0xfc

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_5
    .end packed-switch
.end method

.method public b()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lbry;->b:Lbsr;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lbry;->b:Lbsr;

    invoke-virtual {v0}, Lbsr;->b()V

    .line 69
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lbry;->c:Z

    .line 70
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lbry;->b:Lbsr;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lbry;->b:Lbsr;

    invoke-virtual {v0}, Lbsr;->a()V

    .line 61
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbry;->c:Z

    .line 62
    return-void
.end method

.method public d()J
    .locals 4

    .prologue
    .line 88
    iget-object v0, p0, Lbry;->b:Lbsr;

    const-string v1, "mAudioTrack"

    const-string v2, "must call initialize() first"

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 89
    iget-object v0, p0, Lbry;->b:Lbsr;

    invoke-virtual {v0}, Lbsr;->d()J

    move-result-wide v0

    iget-wide v2, p0, Lbry;->d:J

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public e()I
    .locals 3

    .prologue
    .line 94
    iget-object v0, p0, Lbry;->b:Lbsr;

    const-string v1, "mAudioTrack"

    const-string v2, "must call initialize() first"

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 95
    iget-object v0, p0, Lbry;->b:Lbsr;

    invoke-virtual {v0}, Lbsr;->e()I

    move-result v0

    return v0
.end method

.method public f()J
    .locals 3

    .prologue
    .line 100
    iget-object v0, p0, Lbry;->b:Lbsr;

    const-string v1, "mAudioTrack"

    const-string v2, "must call initialize() first"

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 101
    iget-object v0, p0, Lbry;->b:Lbsr;

    invoke-virtual {v0}, Lbsr;->f()J

    move-result-wide v0

    return-wide v0
.end method

.method public g()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 106
    iget-object v1, p0, Lbry;->b:Lbsr;

    if-nez v1, :cond_1

    .line 110
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lbry;->b:Lbsr;

    invoke-virtual {v1}, Lbsr;->g()I

    move-result v1

    iget-object v2, p0, Lbry;->b:Lbsr;

    invoke-virtual {v2}, Lbsr;->e()I

    move-result v2

    sub-int/2addr v1, v2

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public h()V
    .locals 0

    .prologue
    .line 117
    return-void
.end method
