.class public final Lkoo;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkon;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lkop;",
            "Lkoq;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lkoo;->b:Ljava/util/Map;

    .line 72
    iput-object p1, p0, Lkoo;->a:Landroid/content/Context;

    .line 73
    return-void
.end method

.method static synthetic a(Lkoo;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lkoo;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method a(I)Lkoq;
    .locals 4

    .prologue
    .line 119
    sget-object v0, Lkop;->a:Lkop;

    sparse-switch p1, :sswitch_data_0

    move-object v1, v0

    .line 120
    :goto_0
    iget-object v0, p0, Lkoo;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkoq;

    .line 121
    if-nez v0, :cond_1

    .line 122
    iget-object v2, p0, Lkoo;->b:Ljava/util/Map;

    monitor-enter v2

    .line 123
    :try_start_0
    iget-object v0, p0, Lkoo;->b:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkoq;

    .line 124
    if-nez v0, :cond_0

    .line 125
    new-instance v0, Lkoq;

    invoke-direct {v0, p0, v1}, Lkoq;-><init>(Lkoo;Lkop;)V

    .line 126
    iget-object v3, p0, Lkoo;->b:Ljava/util/Map;

    invoke-interface {v3, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    :cond_1
    return-object v0

    .line 119
    :sswitch_0
    sget-object v0, Lkop;->b:Lkop;

    move-object v1, v0

    goto :goto_0

    .line 128
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 119
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0xd -> :sswitch_0
        0xa5 -> :sswitch_0
        0xad -> :sswitch_0
    .end sparse-switch
.end method

.method public a(Ljava/lang/String;Lltz;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v3, -0x1

    const/4 v0, 0x0

    .line 78
    iget-object v1, p2, Lltz;->a:Lpec;

    iget v1, v1, Lpec;->b:I

    invoke-virtual {p0, v1}, Lkoo;->a(I)Lkoq;

    move-result-object v6

    .line 79
    monitor-enter v6

    .line 80
    :try_start_0
    iget-object v1, v6, Lkoq;->b:Landroid/os/Handler;

    iget-object v2, v6, Lkoq;->d:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 81
    invoke-virtual {p2}, Lltz;->a()I

    move-result v1

    new-array v2, v1, [B

    .line 82
    const/4 v1, 0x0

    array-length v4, v2

    invoke-static {v2, v1, v4}, Loxo;->a([BII)Loxo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 84
    :try_start_1
    invoke-virtual {p2, v1}, Lltz;->a(Loxo;)V

    .line 86
    invoke-virtual {v6}, Lkoq;->a()Liqx;

    .line 88
    iget-object v1, v6, Lkoq;->c:Liqx;

    invoke-interface {v1}, Liqx;->c()Ljava/lang/String;

    move-result-object v1

    .line 89
    if-nez v1, :cond_0

    if-nez p1, :cond_1

    :cond_0
    if-eqz v1, :cond_3

    .line 90
    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 91
    :cond_1
    iget-object v1, v6, Lkoq;->c:Liqx;

    if-nez p1, :cond_2

    const/4 v0, 0x1

    :cond_2
    invoke-interface {v1, v0}, Liqx;->a(Z)Liqx;

    .line 94
    iget-object v0, v6, Lkoq;->c:Liqx;

    invoke-interface {v0, p1}, Liqx;->a(Ljava/lang/String;)Liqx;

    .line 97
    :cond_3
    if-eqz p3, :cond_4

    const-string v0, "latency_event_code"

    const/4 v1, -0x1

    .line 98
    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 100
    :cond_4
    if-ltz v3, :cond_5

    .line 101
    const-string v0, "event_timestamp"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 102
    iget-object v0, v6, Lkoq;->c:Liqx;

    iget-object v1, p0, Lkoo;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-interface/range {v0 .. v5}, Liqx;->a(Ljava/lang/String;[BIJ)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 112
    :goto_0
    :try_start_2
    iget-object v0, v6, Lkoq;->b:Landroid/os/Handler;

    iget-object v1, v6, Lkoq;->d:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 114
    :goto_1
    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void

    .line 107
    :cond_5
    :try_start_3
    iget-object v0, v6, Lkoq;->c:Liqx;

    iget-object v1, p0, Lkoo;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Liqx;->a(Ljava/lang/String;[B[Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 109
    :catch_0
    move-exception v0

    .line 110
    :try_start_4
    const-string v1, "AnalyticsTransmitter"

    const-string v2, "Cannot send log record"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 112
    :try_start_5
    iget-object v0, v6, Lkoq;->b:Landroid/os/Handler;

    iget-object v1, v6, Lkoq;->d:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1

    .line 114
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    throw v0

    .line 112
    :catchall_1
    move-exception v0

    :try_start_6
    iget-object v1, v6, Lkoq;->b:Landroid/os/Handler;

    iget-object v2, v6, Lkoq;->d:Ljava/lang/Runnable;

    const-wide/16 v4, 0x2710

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method
