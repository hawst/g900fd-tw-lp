.class public final Lbjk;
.super Lbji;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbji",
        "<",
        "Lbhk;",
        ">;"
    }
.end annotation


# instance fields
.field private final e:Lbhk;

.field private f:Z

.field private g:Z

.field private h:Z


# direct methods
.method private constructor <init>(Lbjs;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0, p1, p2}, Lbji;-><init>(Lbjs;Ljava/util/concurrent/Executor;)V

    .line 43
    iput-boolean v0, p0, Lbjk;->f:Z

    .line 49
    iput-boolean v0, p0, Lbjk;->g:Z

    .line 52
    iput-boolean v0, p0, Lbjk;->h:Z

    .line 57
    new-instance v0, Lbhk;

    invoke-direct {v0}, Lbhk;-><init>()V

    iput-object v0, p0, Lbjk;->e:Lbhk;

    .line 58
    return-void
.end method

.method public static a(Lbjs;Ljava/util/concurrent/Executor;)Lbjk;
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lbjk;

    invoke-direct {v0, p0, p1}, Lbjk;-><init>(Lbjs;Ljava/util/concurrent/Executor;)V

    return-object v0
.end method


# virtual methods
.method protected synthetic a(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;Z)Lbhj;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0, p1, p2, p3}, Lbjk;->b(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;Z)Lbhk;

    move-result-object v0

    return-object v0
.end method

.method public a(Lbhk;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 125
    iget-boolean v0, p0, Lbjk;->f:Z

    const-string v1, "releasePolled without buffer ownership"

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 126
    const-string v0, "samples"

    iget-object v1, p0, Lbjk;->e:Lbhk;

    invoke-static {p1, v0, v1, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;Ljava/lang/CharSequence;)V

    .line 127
    iput-boolean v2, p0, Lbjk;->f:Z

    .line 128
    iget-boolean v0, p0, Lbjk;->g:Z

    if-eqz v0, :cond_0

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbjk;->h:Z

    .line 130
    invoke-virtual {p0, v2}, Lbjk;->a(Z)V

    .line 131
    iput-object v3, p1, Lbhk;->f:Ljava/nio/ByteBuffer;

    .line 133
    :cond_0
    return-void
.end method

.method public a(Lbiy;)V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lbjk;->b:Landroid/media/MediaFormat;

    if-nez v0, :cond_0

    .line 63
    iget-object v0, p0, Lbjk;->a:Lbjs;

    invoke-interface {p1}, Lbiy;->e()Landroid/media/MediaFormat;

    move-result-object v1

    invoke-interface {v0, v1}, Lbjs;->a(Landroid/media/MediaFormat;)V

    .line 66
    :cond_0
    invoke-super {p0, p1}, Lbji;->a(Lbiy;)V

    .line 67
    return-void
.end method

.method public bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lbhk;

    invoke-virtual {p0, p1}, Lbjk;->a(Lbhk;)V

    return-void
.end method

.method protected b(Ljava/nio/ByteBuffer;Landroid/media/MediaCodec$BufferInfo;Z)Lbhk;
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    const/16 v3, -0xff

    const/4 v2, 0x0

    .line 73
    iget-boolean v1, p0, Lbjk;->f:Z

    if-eqz v1, :cond_0

    .line 120
    :goto_0
    return-object v0

    .line 77
    :cond_0
    iget-boolean v1, p0, Lbjk;->g:Z

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lbjk;->h:Z

    if-eqz v1, :cond_1

    .line 78
    iput-boolean v2, p0, Lbjk;->g:Z

    .line 79
    iput-boolean v2, p0, Lbjk;->h:Z

    .line 82
    iput v3, p0, Lbji;->d:I

    goto :goto_0

    .line 87
    :cond_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->isDirect()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 88
    iget-object v0, p0, Lbjk;->e:Lbhk;

    iput-object p1, v0, Lbhk;->f:Ljava/nio/ByteBuffer;

    .line 89
    iget-object v0, p0, Lbjk;->e:Lbhk;

    iget v1, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    iput v1, v0, Lbhk;->h:I

    .line 90
    iget-object v0, p0, Lbjk;->e:Lbhk;

    iget v1, p2, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iput v1, v0, Lbhk;->g:I

    .line 92
    iput-boolean v4, p0, Lbjk;->g:Z

    .line 114
    :goto_1
    iget-object v0, p0, Lbjk;->e:Lbhk;

    iput-boolean p3, v0, Lbhk;->b:Z

    .line 115
    iget-object v0, p0, Lbjk;->e:Lbhk;

    iget-wide v2, p2, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iput-wide v2, v0, Lbhk;->a:J

    .line 116
    iget-object v0, p0, Lbjk;->e:Lbhk;

    const/4 v1, 0x2

    iput v1, v0, Lbhk;->c:I

    .line 117
    iget-object v0, p0, Lbjk;->e:Lbhk;

    iget-object v1, p0, Lbjk;->c:Landroid/media/MediaFormat;

    const-string v2, "channel-count"

    invoke-virtual {v1, v2}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lbhk;->e:I

    .line 118
    iget-object v0, p0, Lbjk;->e:Lbhk;

    iget-object v1, p0, Lbjk;->c:Landroid/media/MediaFormat;

    const-string v2, "sample-rate"

    invoke-virtual {v1, v2}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lbhk;->d:I

    .line 119
    iput-boolean v4, p0, Lbjk;->f:Z

    .line 120
    iget-object v0, p0, Lbjk;->e:Lbhk;

    goto :goto_0

    .line 94
    :cond_2
    iget-object v0, p0, Lbjk;->e:Lbhk;

    iget-object v0, v0, Lbhk;->f:Ljava/nio/ByteBuffer;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbjk;->e:Lbhk;

    iget-object v0, v0, Lbhk;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    iget v1, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    if-ge v0, v1, :cond_4

    .line 95
    :cond_3
    iget-object v0, p0, Lbjk;->e:Lbhk;

    iget v1, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, v0, Lbhk;->f:Ljava/nio/ByteBuffer;

    .line 98
    :cond_4
    iget v0, p2, Landroid/media/MediaCodec$BufferInfo;->offset:I

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 99
    iget v0, p2, Landroid/media/MediaCodec$BufferInfo;->offset:I

    iget v1, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    add-int/2addr v0, v1

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 101
    iget-object v0, p0, Lbjk;->e:Lbhk;

    iget-object v0, v0, Lbhk;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 102
    iget-object v0, p0, Lbjk;->e:Lbhk;

    iget-object v0, v0, Lbhk;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 103
    iget-object v0, p0, Lbjk;->e:Lbhk;

    iget-object v0, v0, Lbhk;->f:Ljava/nio/ByteBuffer;

    iget v1, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 104
    iget-object v0, p0, Lbjk;->e:Lbhk;

    iget-object v0, v0, Lbhk;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 105
    invoke-virtual {p0, v2}, Lbjk;->a(Z)V

    .line 106
    iput v3, p0, Lbji;->d:I

    .line 107
    iget-object v0, p0, Lbjk;->e:Lbhk;

    iget-object v0, v0, Lbhk;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 108
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 110
    iget-object v0, p0, Lbjk;->e:Lbhk;

    iget v1, p2, Landroid/media/MediaCodec$BufferInfo;->size:I

    iput v1, v0, Lbhk;->h:I

    .line 111
    iget-object v0, p0, Lbjk;->e:Lbhk;

    iput v2, v0, Lbhk;->g:I

    goto/16 :goto_1
.end method

.method public d()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 137
    invoke-super {p0}, Lbji;->d()V

    .line 139
    iput-boolean v0, p0, Lbjk;->f:Z

    .line 140
    iput-boolean v0, p0, Lbjk;->g:Z

    .line 141
    iput-boolean v0, p0, Lbjk;->h:Z

    .line 142
    return-void
.end method
