.class public final Lbug;
.super Loxu;
.source "PG"


# instance fields
.field public a:J

.field public b:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 9
    invoke-direct {p0}, Loxu;-><init>()V

    .line 12
    iput-wide v0, p0, Lbug;->a:J

    .line 15
    iput-wide v0, p0, Lbug;->b:J

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 37
    const/4 v0, 0x0

    .line 38
    iget-wide v2, p0, Lbug;->a:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 39
    const/4 v0, 0x1

    iget-wide v2, p0, Lbug;->a:J

    .line 40
    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 42
    :cond_0
    iget-wide v2, p0, Lbug;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 43
    const/4 v1, 0x2

    iget-wide v2, p0, Lbug;->b:J

    .line 44
    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 46
    :cond_1
    iput v0, p0, Lbug;->ai:I

    .line 47
    return v0
.end method

.method public a(Loxn;)Lbug;
    .locals 2

    .prologue
    .line 55
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 56
    sparse-switch v0, :sswitch_data_0

    .line 60
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    :sswitch_0
    return-object p0

    .line 66
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    iput-wide v0, p0, Lbug;->a:J

    goto :goto_0

    .line 70
    :sswitch_2
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    iput-wide v0, p0, Lbug;->b:J

    goto :goto_0

    .line 56
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 27
    iget-wide v0, p0, Lbug;->a:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 28
    const/4 v0, 0x1

    iget-wide v2, p0, Lbug;->a:J

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 30
    :cond_0
    iget-wide v0, p0, Lbug;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 31
    const/4 v0, 0x2

    iget-wide v2, p0, Lbug;->b:J

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 33
    :cond_1
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lbug;->a(Loxn;)Lbug;

    move-result-object v0

    return-object v0
.end method
