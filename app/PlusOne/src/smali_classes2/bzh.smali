.class public final Lbzh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbzf;


# instance fields
.field private final a:Ljfd;

.field private final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljdx;",
            "Ljej;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Ljfd;Ljava/util/Map;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljfd;",
            "Ljava/util/Map",
            "<",
            "Ljdx;",
            "Ljej;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const-string v0, "getStoryboardResult"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfd;

    iput-object v0, p0, Lbzh;->a:Ljfd;

    .line 43
    const-string v0, "photoIdToUriMap"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iput-object v0, p0, Lbzh;->b:Ljava/util/Map;

    .line 44
    return-void
.end method


# virtual methods
.method public a(Ljava/util/List;Ljava/util/List;)J
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbon;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmw;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 102
    const-wide/16 v2, 0x0

    .line 103
    iget-object v0, p0, Lbzh;->a:Ljfd;

    iget-object v0, v0, Ljfd;->b:Lood;

    iget-object v1, v0, Lood;->b:[Lonr;

    array-length v4, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, v1, v0

    .line 104
    iget-object v6, v5, Lonr;->d:Lons;

    iget-object v6, v6, Lons;->c:Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    iget-object v5, v5, Lonr;->d:Lons;

    iget-object v5, v5, Lons;->b:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    sub-long/2addr v6, v8

    add-long/2addr v2, v6

    .line 103
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 106
    :cond_0
    return-wide v2
.end method

.method public a()Ljava/util/List;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 61
    iget-object v0, p0, Lbzh;->a:Ljfd;

    iget-object v0, v0, Ljfd;->a:Ljff;

    sget-object v1, Ljff;->a:Ljff;

    if-eq v0, v1, :cond_0

    .line 62
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    .line 97
    :goto_0
    return-object v0

    .line 64
    :cond_0
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 65
    new-instance v5, Lbmf;

    invoke-direct {v5}, Lbmf;-><init>()V

    .line 66
    iget-object v0, p0, Lbzh;->a:Ljfd;

    iget-object v6, v0, Ljfd;->b:Lood;

    move v1, v2

    move v3, v2

    .line 71
    :goto_1
    iget-object v0, v6, Lood;->b:[Lonr;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 72
    iget-object v0, v6, Lood;->b:[Lonr;

    aget-object v7, v0, v1

    .line 73
    iget-object v0, v6, Lood;->d:[Ljava/lang/Boolean;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 78
    if-nez v0, :cond_1

    .line 79
    iget-object v0, v6, Lood;->e:[Lons;

    aget-object v8, v0, v3

    .line 81
    iget-object v0, v7, Lonr;->c:Lonw;

    invoke-static {v0}, Ljdx;->a(Lonw;)Ljdx;

    move-result-object v9

    .line 82
    iget-object v0, p0, Lbzh;->b:Ljava/util/Map;

    invoke-interface {v0, v9}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljej;

    .line 83
    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v11

    add-int/lit8 v11, v11, 0x8

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v11, "uri for "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v0, v9, v10}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 84
    iget-object v9, v7, Lonr;->c:Lonw;

    iget-object v0, v0, Ljej;->a:Landroid/net/Uri;

    iget v10, v7, Lonr;->b:I

    invoke-static {v9, v0, v10}, Ljeg;->a(Lonw;Landroid/net/Uri;I)Ljeg;

    move-result-object v0

    .line 88
    invoke-static {v8}, Lbpx;->a(Lons;)Lbmp;

    move-result-object v8

    invoke-virtual {v5, v8}, Lbmf;->a(Lbmp;)Lbmf;

    move-result-object v8

    .line 89
    invoke-virtual {v8, v0}, Lbmf;->a(Ljeg;)Lbmf;

    move-result-object v0

    iget v8, v7, Lonr;->b:I

    .line 90
    invoke-static {v8}, Lbmg;->a(I)Lbmg;

    move-result-object v8

    invoke-virtual {v0, v8}, Lbmf;->a(Lbmg;)Lbmf;

    move-result-object v8

    iget v0, v7, Lonr;->b:I

    const/4 v7, 0x7

    if-eq v0, v7, :cond_2

    const/4 v0, 0x1

    .line 91
    :goto_2
    invoke-virtual {v8, v0}, Lbmf;->a(Z)Lbmf;

    move-result-object v0

    .line 92
    invoke-virtual {v0}, Lbmf;->a()Lbmd;

    move-result-object v0

    .line 87
    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    add-int/lit8 v3, v3, 0x1

    .line 71
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 90
    goto :goto_2

    :cond_3
    move-object v0, v4

    .line 97
    goto/16 :goto_0
.end method

.method public a(Ljava/util/List;Ljava/util/List;Ljava/util/Map;Lbor;Lbzg;)Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbon;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmw;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;",
            "Lbor;",
            "Lbzg;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 50
    invoke-virtual {p0}, Lbzh;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 111
    instance-of v1, p1, Lbzh;

    if-nez v1, :cond_1

    .line 116
    :cond_0
    :goto_0
    return v0

    .line 114
    :cond_1
    check-cast p1, Lbzh;

    .line 115
    iget-object v1, p1, Lbzh;->a:Ljfd;

    iget-object v2, p0, Lbzh;->a:Ljfd;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lbzh;->b:Ljava/util/Map;

    iget-object v2, p0, Lbzh;->b:Ljava/util/Map;

    .line 116
    invoke-interface {v1, v2}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 121
    iget-object v0, p0, Lbzh;->a:Ljfd;

    iget-object v1, p0, Lbzh;->b:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method
