.class public final Linp;
.super Landroid/os/HandlerThread;
.source "PG"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lino;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Linq;",
            ">;"
        }
    .end annotation
.end field

.field private final c:Ljava/lang/Object;

.field private d:Lgfz;

.field private e:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 93
    const-string v0, "PanoramaClient"

    const/16 v1, 0xa

    invoke-direct {p0, v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Linp;->b:Ljava/util/ArrayList;

    .line 42
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Linp;->c:Ljava/lang/Object;

    .line 94
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Linp;->a:Landroid/content/Context;

    .line 95
    return-void
.end method

.method static synthetic a(Linp;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Linp;->e:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    packed-switch p0, :pswitch_data_0

    const-string v0, "none"

    :goto_0
    return-object v0

    :pswitch_0
    const-string v0, "360 horizontal"

    goto :goto_0

    :pswitch_1
    const-string v0, "partial"

    goto :goto_0

    :pswitch_2
    const-string v0, "full"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private a()Z
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Linp;->d:Lgfz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Linp;->d:Lgfz;

    invoke-interface {v0}, Lgfz;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/net/Uri;Linr;)V
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Linp;->a:Landroid/content/Context;

    invoke-static {v0}, Lgfk;->a(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    invoke-interface {p2}, Linr;->m()V

    .line 116
    :goto_0
    return-void

    .line 105
    :cond_0
    new-instance v0, Linq;

    invoke-direct {v0, p0, p2, p1}, Linq;-><init>(Linp;Linr;Landroid/net/Uri;)V

    .line 107
    iget-object v1, p0, Linp;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 108
    :try_start_0
    iget-object v2, p0, Linp;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    iget-object v0, p0, Linp;->e:Landroid/os/Handler;

    if-nez v0, :cond_1

    .line 111
    invoke-virtual {p0}, Linp;->start()V

    .line 112
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Linp;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Linp;->e:Landroid/os/Handler;

    .line 115
    :cond_1
    iget-object v0, p0, Linp;->e:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 116
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 11

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x3

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 121
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 161
    :goto_0
    return v0

    .line 124
    :pswitch_0
    iget-object v2, p0, Linp;->c:Ljava/lang/Object;

    monitor-enter v2

    .line 126
    :try_start_0
    iget-object v3, p0, Linp;->e:Landroid/os/Handler;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 127
    new-instance v4, Ljava/util/ArrayList;

    iget-object v3, p0, Linp;->b:Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 128
    iget-object v3, p0, Linp;->b:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 129
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 130
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v0

    .line 131
    :goto_1
    if-ge v3, v5, :cond_5

    .line 132
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Linq;

    .line 133
    invoke-direct {p0}, Linp;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v1

    :goto_2
    if-eqz v2, :cond_4

    .line 134
    const-string v2, "PanoramaClient"

    invoke-static {v2, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 135
    iget-object v2, v0, Linq;->a:Landroid/net/Uri;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x26

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v7, "Detecting if the image is a panorama: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    :cond_0
    iget-object v2, p0, Linp;->e:Landroid/os/Handler;

    invoke-virtual {v2, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 139
    sget-object v2, Lgyn;->c:Lgyp;

    iget-object v6, p0, Linp;->d:Lgfz;

    .line 140
    iget-object v7, v0, Linq;->a:Landroid/net/Uri;

    .line 139
    invoke-interface {v2, v6, v7, v10}, Lgyp;->a(Lgfz;Landroid/net/Uri;Landroid/os/Bundle;)Lggd;

    move-result-object v2

    .line 140
    invoke-interface {v2, v0}, Lggd;->a(Lggg;)V

    .line 131
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 129
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 133
    :cond_1
    iget-object v2, p0, Linp;->d:Lgfz;

    if-eqz v2, :cond_2

    iget-object v2, p0, Linp;->d:Lgfz;

    invoke-interface {v2}, Lgfz;->c()V

    :cond_2
    new-instance v2, Lgga;

    iget-object v6, p0, Linp;->a:Landroid/content/Context;

    invoke-direct {v2, v6}, Lgga;-><init>(Landroid/content/Context;)V

    sget-object v6, Lgyn;->b:Lgfo;

    invoke-virtual {v2, v6}, Lgga;->a(Lgfo;)Lgga;

    move-result-object v2

    invoke-virtual {v2}, Lgga;->b()Lgfz;

    move-result-object v2

    iput-object v2, p0, Linp;->d:Lgfz;

    iget-object v2, p0, Linp;->d:Lgfz;

    const-wide/16 v6, 0x1e

    sget-object v8, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v6, v7, v8}, Lgfz;->a(JLjava/util/concurrent/TimeUnit;)Lgfd;

    move-result-object v2

    const-string v6, "PanoramaClient"

    invoke-static {v6, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_3

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x12

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "ConnectionResult: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v2}, Lgfd;->b()Z

    move-result v2

    goto/16 :goto_2

    .line 143
    :cond_4
    invoke-virtual {v0}, Linq;->a()Linr;

    move-result-object v0

    invoke-interface {v0}, Linr;->m()V

    goto :goto_3

    :cond_5
    move v0, v1

    .line 146
    goto/16 :goto_0

    .line 150
    :pswitch_1
    invoke-direct {p0}, Linp;->a()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 151
    iget-object v0, p0, Linp;->d:Lgfz;

    invoke-interface {v0}, Lgfz;->c()V

    .line 155
    iput-object v10, p0, Linp;->d:Lgfz;

    :cond_6
    move v0, v1

    .line 157
    goto/16 :goto_0

    .line 121
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
