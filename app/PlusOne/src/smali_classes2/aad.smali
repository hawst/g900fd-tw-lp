.class final Laad;
.super Laaa;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# direct methods
.method constructor <init>(Lzw;)V
    .locals 0

    .prologue
    .line 198
    invoke-direct {p0, p1}, Laaa;-><init>(Lzw;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Laad;->c:Laaf;

    invoke-static {v0}, Laaf;->a(Laaf;)Landroid/hardware/Camera;

    move-result-object v0

    invoke-virtual {p0, v0}, Laad;->a(Landroid/hardware/Camera;)V

    .line 203
    return-void
.end method

.method public declared-synchronized a(Laas;)V
    .locals 5

    .prologue
    .line 213
    monitor-enter p0

    :try_start_0
    invoke-static {}, Lada;->b()Lada;

    move-result-object v0

    .line 214
    invoke-static {}, Lacw;->h()Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v1

    invoke-virtual {p0, v1}, Laad;->e(Ljavax/microedition/khronos/egl/EGLContext;)Lacm;

    move-result-object v1

    .line 215
    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 216
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Attempting to grab camera frame from unknown thread: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 219
    :cond_1
    :try_start_1
    iget-object v2, p0, Laad;->f:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Lada;->c()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/SurfaceTexture;->attachToGLContext(I)V

    .line 220
    invoke-virtual {p0, v1}, Laad;->b(Lacm;)V

    .line 221
    invoke-virtual {p0, v1}, Laad;->a(Lacm;)V

    .line 222
    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-virtual {p1, v2}, Laas;->a([I)V

    .line 223
    invoke-virtual {p1}, Laas;->m()Lacw;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v1, v0, v2, v3, v4}, Lacm;->a(Lada;Lacw;II)V

    .line 227
    iget-object v1, p0, Laad;->f:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1}, Landroid/graphics/SurfaceTexture;->getTimestamp()J

    move-result-wide v2

    invoke-virtual {p1, v2, v3}, Laas;->a(J)V

    .line 228
    invoke-virtual {p1}, Laas;->h()V

    .line 229
    iget-object v1, p0, Laad;->f:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v1}, Landroid/graphics/SurfaceTexture;->detachFromGLContext()V

    .line 230
    invoke-virtual {v0}, Lada;->g()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 231
    monitor-exit p0

    return-void

    .line 222
    :array_0
    .array-data 4
        0x0
        0x0
    .end array-data
.end method

.method protected a(Lacm;)V
    .locals 4

    .prologue
    const/high16 v3, -0x40800000    # -1.0f

    const/high16 v2, 0x3f800000    # 1.0f

    .line 235
    iget-object v0, p0, Laad;->c:Laaf;

    invoke-static {v0}, Laaf;->b(Laaf;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Laad;->c:Laaf;

    invoke-static {v0}, Laaf;->c(Laaf;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 236
    invoke-virtual {p1, v2, v2, v3, v3}, Lacm;->b(FFFF)V

    .line 240
    :goto_0
    return-void

    .line 238
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0, v2, v2, v3}, Lacm;->b(FFFF)V

    goto :goto_0
.end method

.method protected a(Landroid/hardware/Camera;)V
    .locals 1

    .prologue
    .line 244
    invoke-super {p0, p1}, Laaa;->a(Landroid/hardware/Camera;)V

    .line 245
    iget-object v0, p0, Laad;->f:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->detachFromGLContext()V

    .line 246
    return-void
.end method

.method public declared-synchronized b()V
    .locals 1

    .prologue
    .line 207
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Laad;->h()V

    .line 208
    invoke-virtual {p0}, Laad;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209
    monitor-exit p0

    return-void

    .line 207
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected h()V
    .locals 2

    .prologue
    .line 249
    iget-object v0, p0, Laad;->f:Landroid/graphics/SurfaceTexture;

    iget-object v1, p0, Laad;->e:Lada;

    invoke-virtual {v1}, Lada;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/SurfaceTexture;->attachToGLContext(I)V

    .line 250
    iget-object v0, p0, Laad;->f:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->updateTexImage()V

    .line 251
    iget-object v0, p0, Laad;->f:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->detachFromGLContext()V

    .line 252
    return-void
.end method

.method protected i()V
    .locals 3

    .prologue
    .line 255
    iget-object v1, p0, Laad;->d:Ljava/util/Vector;

    monitor-enter v1

    .line 256
    :try_start_0
    iget-object v0, p0, Laad;->d:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laai;

    .line 257
    invoke-interface {v0}, Laai;->a()V

    goto :goto_0

    .line 259
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
