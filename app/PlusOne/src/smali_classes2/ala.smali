.class public final Lala;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lajw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lala;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    return-void
.end method


# virtual methods
.method public a(Lbkn;Lbku;Lbkq;)V
    .locals 18

    .prologue
    .line 45
    .line 46
    invoke-static/range {p2 .. p2}, Lbkx;->b(Lbku;)Lbmz;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lbku;->b(Lbmz;)Landroid/util/LongSparseArray;

    move-result-object v7

    .line 45
    invoke-virtual {v7}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    if-nez v2, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v2

    .line 47
    :goto_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 48
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbmp;

    .line 53
    new-instance v5, Lbmr;

    .line 54
    move-object/from16 v0, p2

    invoke-static {v0, v2}, Lbky;->a(Lbku;Lbmp;)Lbky;

    move-result-object v6

    const/4 v7, 0x1

    invoke-direct {v5, v2, v6, v7}, Lbmr;-><init>(Lbmp;Lbky;I)V

    .line 56
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 45
    :cond_0
    new-instance v10, Lalb;

    const v2, 0x49742400    # 1000000.0f

    invoke-direct {v10, v2}, Lalb;-><init>(F)V

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    const/4 v2, 0x0

    move v6, v5

    move v5, v4

    move v4, v3

    move v3, v2

    :goto_2
    invoke-virtual {v7}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    if-ge v3, v2, :cond_4

    invoke-virtual {v7, v3}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v14

    invoke-virtual {v7, v14, v15}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbxi;

    invoke-virtual {v2}, Lbxi;->d()F

    move-result v2

    invoke-virtual {v10, v2, v14, v15}, Lalb;->a(FJ)F

    move-result v13

    invoke-static {v13}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-float/2addr v5, v13

    mul-float v2, v13, v13

    add-float/2addr v6, v2

    :goto_3
    invoke-virtual {v7, v4}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v8

    sub-long v8, v14, v8

    const-wide/32 v16, 0x1e8480

    cmp-long v2, v8, v16

    if-lez v2, :cond_1

    invoke-virtual {v11, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    sub-float/2addr v5, v2

    mul-float/2addr v2, v2

    sub-float/2addr v6, v2

    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    :cond_1
    sub-int v2, v3, v4

    int-to-float v2, v2

    div-float v16, v5, v2

    sub-int v2, v3, v4

    int-to-float v2, v2

    div-float v2, v6, v2

    float-to-double v8, v2

    invoke-static {v8, v9}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v8

    double-to-float v0, v8

    move/from16 v17, v0

    invoke-virtual {v12}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    const-wide/32 v8, 0x7a120

    sub-long v8, v14, v8

    :goto_4
    sub-float v2, v13, v16

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    const/high16 v13, 0x40000000    # 2.0f

    mul-float v13, v13, v17

    cmpl-float v2, v2, v13

    if-lez v2, :cond_2

    sub-long v8, v14, v8

    const-wide/32 v16, 0x7a120

    cmp-long v2, v8, v16

    if-ltz v2, :cond_2

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    :cond_3
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    goto :goto_4

    :cond_4
    invoke-virtual {v7}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v7, v2}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Landroid/util/LongSparseArray;->keyAt(I)J

    move-result-wide v2

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-wide v4, v2

    :goto_5
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    new-instance v3, Lbmp;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-direct {v3, v4, v5, v8, v9}, Lbmp;-><init>(JJ)V

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move-wide v4, v2

    goto :goto_5

    :cond_5
    move-object v2, v6

    goto/16 :goto_0

    .line 58
    :cond_6
    const/4 v2, 0x1

    move-object/from16 v0, p3

    invoke-virtual {v0, v2, v3}, Lbkq;->a(ILjava/util/List;)Lbkq;

    .line 60
    return-void
.end method
