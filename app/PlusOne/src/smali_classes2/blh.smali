.class abstract Lblh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lblk;


# instance fields
.field private final a:F


# direct methods
.method public constructor <init>(F)V
    .locals 0

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput p1, p0, Lblh;->a:F

    .line 117
    return-void
.end method


# virtual methods
.method protected abstract a(Lbku;J)Ljava/lang/Float;
.end method

.method public final a(Lbku;[J)Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 121
    .line 122
    const/4 v1, 0x0

    .line 123
    array-length v3, p2

    move v2, v0

    move v6, v1

    move v1, v0

    move v0, v6

    :goto_0
    if-ge v2, v3, :cond_1

    aget-wide v4, p2, v2

    .line 124
    invoke-virtual {p0, p1, v4, v5}, Lblh;->a(Lbku;J)Ljava/lang/Float;

    move-result-object v4

    .line 126
    if-eqz v4, :cond_0

    .line 127
    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    add-float/2addr v0, v4

    .line 128
    add-int/lit8 v1, v1, 0x1

    .line 123
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 131
    :cond_1
    if-nez v1, :cond_2

    .line 132
    iget v0, p0, Lblh;->a:F

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 134
    :goto_1
    return-object v0

    :cond_2
    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    goto :goto_1
.end method
