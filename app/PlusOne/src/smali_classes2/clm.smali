.class public final Lclm;
.super Lcjy;
.source "PG"


# static fields
.field private static final W:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private X:Lhdh;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 61
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    const/16 v3, 0x26

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 63
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lclm;->W:Ljava/util/List;

    .line 64
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Lcjy;-><init>()V

    .line 371
    return-void
.end method

.method static synthetic a(Lclm;)Lhdy;
    .locals 1

    .prologue
    .line 46
    invoke-virtual {p0}, Lclm;->an()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lhdy;

    return-object v0
.end method

.method static synthetic a(Lclm;F)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 46
    invoke-virtual {p0}, Lclm;->an()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lhdy;

    if-nez v0, :cond_1

    const-string v0, "StraightenFragment"

    const-string v1, "Set post rotation failed: straighten view is not ready!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0, p1, v3}, Lhdy;->a(FZ)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x26

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v2}, Lclm;->a(ILjava/lang/Object;Z)Z

    const/16 v0, 0x27

    invoke-static {p1}, Lhbg;->a(F)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, v0, v1, v3}, Lclm;->a(ILjava/lang/Object;Z)Z

    goto :goto_0
.end method


# virtual methods
.method protected B_()Z
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x0

    return v0
.end method

.method protected U()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;
    .locals 3

    .prologue
    .line 92
    invoke-super {p0}, Lcjy;->U()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    .line 94
    invoke-virtual {p0}, Lclm;->ah()Lchp;

    move-result-object v1

    invoke-virtual {v1}, Lchp;->h()Lcom/google/android/libraries/photoeditor/core/FilterChain;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getPostRotation()I

    move-result v1

    .line 95
    const/16 v2, 0x27

    invoke-virtual {v0, v2, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->setParameterInteger(II)Z

    .line 97
    return-object v0
.end method

.method protected V()V
    .locals 2

    .prologue
    .line 236
    new-instance v0, Lclp;

    invoke-direct {v0, p0}, Lclp;-><init>(Lclm;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lclp;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 237
    return-void
.end method

.method protected X()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 87
    sget-object v0, Lclm;->W:Ljava/util/List;

    return-object v0
.end method

.method public a(ILjava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 215
    const/16 v0, 0x27

    if-ne p1, v0, :cond_0

    const-string v0, ""

    .line 216
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcjy;->a(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Lckp;)V
    .locals 3

    .prologue
    .line 180
    const v0, 0x7f02039a

    const v1, 0x7f0a0048

    .line 182
    invoke-virtual {p0, v1}, Lclm;->e_(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcln;

    invoke-direct {v2, p0}, Lcln;-><init>(Lclm;)V

    .line 180
    invoke-virtual {p1, v0, v1, v2}, Lckp;->a(ILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 196
    const v0, 0x7f020399

    const v1, 0x7f0a0049

    .line 198
    invoke-virtual {p0, v1}, Lclm;->e_(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lclo;

    invoke-direct {v2, p0}, Lclo;-><init>(Lclm;)V

    .line 196
    invoke-virtual {p1, v0, v1, v2}, Lckp;->a(ILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 211
    return-void
.end method

.method protected a(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V
    .locals 2

    .prologue
    .line 107
    new-instance v0, Lhdh;

    invoke-direct {v0, p1}, Lhdh;-><init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V

    iput-object v0, p0, Lclm;->X:Lhdh;

    .line 108
    iget-object v0, p0, Lclm;->X:Lhdh;

    new-instance v1, Lclr;

    invoke-direct {v1, p0}, Lclr;-><init>(Lclm;)V

    invoke-virtual {v0, v1}, Lhdh;->a(Lhdj;)V

    .line 109
    iget-object v0, p0, Lclm;->X:Lhdh;

    new-instance v1, Lclq;

    invoke-direct {v1, p0}, Lclq;-><init>(Lclm;)V

    invoke-virtual {v0, v1}, Lhdh;->a(Lhdi;)V

    .line 111
    iget-object v0, p0, Lclm;->X:Lhdh;

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->a(Lhdx;)I

    .line 112
    return-void
.end method

.method protected aa()Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 116
    new-instance v0, Lhdy;

    invoke-virtual {p0}, Lclm;->n()Lz;

    move-result-object v1

    invoke-direct {v0, v1}, Lhdy;-><init>(Landroid/content/Context;)V

    .line 117
    invoke-virtual {v0, v2, v2}, Lhdy;->a(II)V

    .line 119
    return-object v0
.end method

.method protected ab()V
    .locals 0

    .prologue
    .line 143
    return-void
.end method

.method protected ac()V
    .locals 0

    .prologue
    .line 146
    return-void
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 77
    new-instance v0, Lhmk;

    sget-object v1, Lonc;->w:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method protected ad()Lhbn;
    .locals 1

    .prologue
    .line 150
    const/4 v0, 0x0

    return-object v0
.end method

.method protected ae()Lhbp;
    .locals 1

    .prologue
    .line 155
    const/4 v0, 0x0

    return-object v0
.end method

.method protected af()Lhbj;
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    return-object v0
.end method

.method protected ag()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    .line 124
    invoke-virtual {p0}, Lclm;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 125
    const v1, 0x7f0d0023

    .line 126
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 127
    const v2, 0x7f0d0025

    .line 128
    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 130
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 132
    iput v1, v3, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    .line 133
    iput v2, v3, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    .line 135
    invoke-virtual {p0}, Lclm;->an()Landroid/view/View;

    move-result-object v1

    .line 136
    const v2, 0x7f0d0026

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    div-int/lit8 v0, v0, 0x2

    .line 137
    invoke-virtual {v1, v0, v0, v0, v0}, Landroid/view/View;->setPadding(IIII)V

    .line 139
    invoke-virtual {v1, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 140
    return-void
.end method

.method protected ak()V
    .locals 3

    .prologue
    .line 241
    invoke-virtual {p0}, Lclm;->an()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lhdy;

    .line 242
    invoke-virtual {p0}, Lclm;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    const/16 v2, 0x26

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterFloat(I)F

    move-result v1

    .line 241
    invoke-virtual {v0, v1}, Lhdy;->a(F)V

    .line 243
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x5

    return v0
.end method

.method protected e()Ljava/util/List;
    .locals 14
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcmd;",
            ">;"
        }
    .end annotation

    .prologue
    const-wide/16 v12, 0x96

    const/4 v0, 0x0

    const v11, 0x3e99999a    # 0.3f

    const/4 v10, 0x0

    .line 261
    invoke-virtual {p0}, Lclm;->n()Lz;

    move-result-object v1

    .line 262
    if-nez v1, :cond_1

    .line 305
    :cond_0
    :goto_0
    return-object v0

    .line 266
    :cond_1
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v2, v1}, Lcmh;->a(Landroid/view/Window;Landroid/content/res/Resources;)Landroid/graphics/Rect;

    move-result-object v1

    .line 267
    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 272
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 273
    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    .line 276
    invoke-virtual {p0}, Lclm;->o()Landroid/content/res/Resources;

    move-result-object v3

    .line 277
    const v4, 0x7f0d004b

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 278
    const v5, 0x7f0d0049

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 279
    div-int/lit8 v5, v3, 0x2

    .line 282
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v6

    if-le v6, v4, :cond_2

    .line 283
    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v6

    sub-int/2addr v6, v4

    div-int/lit8 v6, v6, 0x2

    invoke-virtual {v1, v6, v10}, Landroid/graphics/Rect;->inset(II)V

    .line 285
    :cond_2
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v6

    if-le v6, v4, :cond_3

    .line 286
    invoke-virtual {v1}, Landroid/graphics/Rect;->height()I

    move-result v6

    sub-int v4, v6, v4

    div-int/lit8 v4, v4, 0x2

    invoke-virtual {v1, v10, v4}, Landroid/graphics/Rect;->inset(II)V

    .line 289
    :cond_3
    invoke-virtual {p0}, Lclm;->o()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0c0002

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    .line 290
    iget v6, v1, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1}, Landroid/graphics/Rect;->centerY()I

    move-result v7

    add-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    .line 292
    const/16 v7, 0x3e8

    int-to-float v8, v5

    invoke-static {v7, v8}, Lcmd;->a(IF)Lcme;

    move-result-object v7

    .line 293
    int-to-long v8, v4

    invoke-virtual {v7, v8, v9}, Lcme;->a(J)Lcme;

    move-result-object v4

    int-to-float v8, v5

    mul-float/2addr v8, v11

    invoke-virtual {v4, v8}, Lcme;->a(F)Lcme;

    move-result-object v4

    iget v8, v1, Landroid/graphics/Rect;->left:I

    int-to-float v8, v8

    int-to-float v9, v6

    .line 294
    invoke-virtual {v4, v8, v9}, Lcme;->a(FF)Lcme;

    .line 295
    invoke-virtual {v7, v12, v13}, Lcme;->b(J)Lcmg;

    move-result-object v4

    int-to-float v8, v5

    invoke-virtual {v4, v8}, Lcmg;->a(F)Lcmg;

    move-result-object v4

    invoke-virtual {v4}, Lcmg;->a()Lcme;

    .line 296
    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    int-to-float v4, v6

    const-wide/16 v8, 0x4b0

    invoke-virtual {v7, v1, v4, v8, v9}, Lcme;->b(FFJ)Lcmg;

    move-result-object v1

    .line 297
    invoke-virtual {v1, v0}, Lcmg;->a(Landroid/animation/TimeInterpolator;)Lcmg;

    move-result-object v0

    invoke-virtual {v0}, Lcmg;->a()Lcme;

    .line 298
    const-wide/16 v0, 0x12c

    invoke-virtual {v7, v0, v1}, Lcme;->b(J)Lcmg;

    move-result-object v0

    int-to-float v1, v5

    const v4, 0x3f666666    # 0.9f

    mul-float/2addr v1, v4

    invoke-virtual {v0, v1}, Lcmg;->a(F)Lcmg;

    move-result-object v0

    invoke-virtual {v0}, Lcmg;->a()Lcme;

    .line 299
    const-wide/16 v0, 0x64

    invoke-virtual {v7, v0, v1}, Lcme;->b(J)Lcmg;

    move-result-object v0

    invoke-virtual {v0}, Lcmg;->a()Lcme;

    .line 300
    const-wide/16 v0, 0x32

    invoke-virtual {v7, v0, v1}, Lcme;->b(J)Lcmg;

    move-result-object v0

    int-to-float v1, v5

    const v4, 0x3f6b851f    # 0.92f

    mul-float/2addr v1, v4

    invoke-virtual {v0, v1}, Lcmg;->a(F)Lcmg;

    move-result-object v0

    invoke-virtual {v0}, Lcmg;->a()Lcme;

    .line 301
    const/high16 v0, -0x40400000    # -1.5f

    int-to-float v1, v3

    mul-float/2addr v0, v1

    const/4 v1, 0x0

    const-wide/16 v8, 0x2d0

    invoke-virtual {v7, v0, v1, v8, v9}, Lcme;->a(FFJ)Lcmg;

    move-result-object v0

    .line 302
    invoke-virtual {v0, v2}, Lcmg;->a(Landroid/animation/TimeInterpolator;)Lcmg;

    move-result-object v0

    int-to-float v1, v5

    invoke-virtual {v0, v1}, Lcmg;->a(F)Lcmg;

    move-result-object v0

    invoke-virtual {v0}, Lcmg;->a()Lcme;

    .line 303
    invoke-virtual {v7, v12, v13}, Lcme;->b(J)Lcmg;

    move-result-object v0

    int-to-float v1, v5

    mul-float/2addr v1, v11

    invoke-virtual {v0, v1}, Lcmg;->a(F)Lcmg;

    move-result-object v0

    invoke-virtual {v0}, Lcmg;->a()Lcme;

    .line 305
    const/4 v0, 0x1

    new-array v0, v0, [Lcmd;

    invoke-virtual {v7}, Lcme;->a()Lcmd;

    move-result-object v1

    aput-object v1, v0, v10

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0
.end method

.method protected y_()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 165
    invoke-virtual {p0}, Lclm;->an()Landroid/view/View;

    move-result-object v0

    check-cast v0, Lhdy;

    .line 167
    invoke-virtual {p0}, Lclm;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    const/16 v2, 0x27

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v1

    .line 166
    invoke-static {v1}, Lhbg;->a(I)F

    move-result v1

    .line 168
    invoke-virtual {v0, v1, v3}, Lhdy;->a(FZ)Z

    .line 170
    invoke-virtual {p0}, Lclm;->ah()Lchp;

    move-result-object v1

    invoke-virtual {v1}, Lchp;->d()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 171
    invoke-virtual {v0, v1}, Lhdy;->a(Landroid/graphics/Bitmap;)V

    .line 172
    invoke-virtual {p0}, Lclm;->ak()V

    .line 174
    invoke-virtual {p0, v3}, Lclm;->k(Z)V

    .line 175
    invoke-virtual {p0}, Lclm;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Lhdn;->b(Landroid/app/Activity;)V

    .line 176
    return-void
.end method

.method public z()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 68
    invoke-super {p0}, Lcjy;->z()V

    .line 70
    iget-object v0, p0, Lclm;->X:Lhdh;

    invoke-virtual {v0, v1}, Lhdh;->a(Lhdj;)V

    .line 71
    iget-object v0, p0, Lclm;->X:Lhdh;

    invoke-virtual {v0, v1}, Lhdh;->a(Lhdi;)V

    .line 72
    iput-object v1, p0, Lclm;->X:Lhdh;

    .line 73
    return-void
.end method
