.class final Lbga;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/util/concurrent/Callable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/concurrent/Callable",
        "<",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:I

.field private synthetic b:Lbgs;

.field private synthetic c:Lbhd;

.field private synthetic d:I


# direct methods
.method constructor <init>(ILbgs;Lbhd;I)V
    .locals 0

    .prologue
    .line 57
    iput p1, p0, Lbga;->a:I

    iput-object p2, p0, Lbga;->b:Lbgs;

    iput-object p3, p0, Lbga;->c:Lbhd;

    iput p4, p0, Lbga;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Void;
    .locals 5

    .prologue
    const/16 v4, 0xbe2

    const/16 v3, 0x302

    const/16 v2, 0x303

    const/4 v1, 0x1

    .line 60
    iget v0, p0, Lbga;->a:I

    if-eqz v0, :cond_0

    .line 61
    invoke-static {v4}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 63
    :cond_0
    iget v0, p0, Lbga;->a:I

    packed-switch v0, :pswitch_data_0

    .line 83
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    iget v1, p0, Lbga;->a:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x25

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unhandled blend mode type:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 65
    :pswitch_1
    invoke-static {v4}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 86
    :goto_0
    iget-object v0, p0, Lbga;->b:Lbgs;

    invoke-virtual {v0}, Lbgs;->b()Lbgx;

    move-result-object v0

    .line 87
    iget-object v1, p0, Lbga;->c:Lbhd;

    iget-object v2, p0, Lbga;->b:Lbgs;

    invoke-virtual {v0, v1, v2}, Lbgx;->a(Lbhd;Lbgs;)V

    .line 88
    iget v0, p0, Lbga;->d:I

    const/4 v1, 0x0

    iget-object v2, p0, Lbga;->c:Lbhd;

    invoke-virtual {v2}, Lbhd;->a()I

    move-result v2

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 89
    iget-object v0, p0, Lbga;->b:Lbgs;

    invoke-virtual {v0}, Lbgs;->a()V

    .line 90
    const-string v0, "glDrawArrays"

    invoke-static {v0}, Lbgc;->a(Ljava/lang/String;)V

    .line 91
    const/4 v0, 0x0

    return-object v0

    .line 68
    :pswitch_2
    invoke-static {v3, v2}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    goto :goto_0

    .line 71
    :pswitch_3
    invoke-static {v3, v1}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    goto :goto_0

    .line 74
    :pswitch_4
    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    goto :goto_0

    .line 77
    :pswitch_5
    const/16 v0, 0x306

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    goto :goto_0

    .line 80
    :pswitch_6
    const/16 v0, 0x301

    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    goto :goto_0

    .line 63
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 57
    invoke-virtual {p0}, Lbga;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
