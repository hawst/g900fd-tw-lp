.class public final Lbas;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbax;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 185
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lbmn;)F
    .locals 13

    .prologue
    const/4 v12, 0x6

    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 192
    invoke-virtual {p1}, Lbmn;->b()I

    move-result v2

    const-string v3, "input.getValueCount()"

    const/16 v4, 0x18

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 193
    new-array v4, v12, [F

    move v2, v0

    .line 195
    :goto_0
    const/16 v3, 0x12

    if-ge v2, v3, :cond_0

    .line 196
    rem-int/lit8 v3, v2, 0x6

    aget v5, v4, v3

    invoke-virtual {p1, v2}, Lbmn;->a(I)F

    move-result v6

    invoke-static {v2}, Lbaq;->a(I)I

    move-result v7

    int-to-float v7, v7

    mul-float/2addr v6, v7

    add-float/2addr v5, v6

    aput v5, v4, v3

    .line 195
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    move v2, v0

    move v3, v1

    .line 198
    :goto_1
    if-ge v2, v12, :cond_1

    aget v5, v4, v2

    add-float/2addr v3, v5

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    cmpl-float v2, v3, v1

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    :goto_2
    if-ge v2, v12, :cond_4

    aget v5, v4, v2

    div-float/2addr v5, v3

    cmpl-float v6, v5, v1

    if-lez v6, :cond_2

    float-to-double v6, v0

    float-to-double v8, v5

    float-to-double v10, v5

    invoke-static {v10, v11}, Ljava/lang/Math;->log(D)D

    move-result-wide v10

    mul-double/2addr v8, v10

    add-double/2addr v6, v8

    double-to-float v0, v6

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    move v0, v1

    :cond_4
    neg-float v0, v0

    return v0
.end method
