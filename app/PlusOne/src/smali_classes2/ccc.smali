.class public Lccc;
.super Landroid/widget/AdapterView;
.source "PG"

# interfaces
.implements Landroid/view/View$OnLongClickListener;
.implements Lcci;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/AdapterView",
        "<",
        "Landroid/widget/Adapter;",
        ">;",
        "Landroid/view/View$OnLongClickListener;",
        "Lcci;"
    }
.end annotation


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Landroid/content/ClipData;


# instance fields
.field private final c:Landroid/database/DataSetObserver;

.field private final d:Lccf;

.field private final e:[Lcch;

.field private final f:Lccj;

.field private final g:Landroid/util/LongSparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LongSparseArray",
            "<",
            "Lccg;",
            ">;"
        }
    .end annotation
.end field

.field private final h:Landroid/util/SparseIntArray;

.field private final i:Ljava/lang/Runnable;

.field private final j:I

.field private final k:I

.field private l:I

.field private m:Landroid/widget/Adapter;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 51
    const-class v0, Lccc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lccc;->a:Ljava/lang/String;

    .line 55
    new-instance v0, Landroid/content/ClipData;

    const-string v1, ""

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "application/octet-stream"

    aput-object v4, v2, v3

    new-instance v3, Landroid/content/ClipData$Item;

    const-string v4, ""

    invoke-direct {v3, v4}, Landroid/content/ClipData$Item;-><init>(Ljava/lang/CharSequence;)V

    invoke-direct {v0, v1, v2, v3}, Landroid/content/ClipData;-><init>(Ljava/lang/CharSequence;[Ljava/lang/String;Landroid/content/ClipData$Item;)V

    sput-object v0, Lccc;->b:Landroid/content/ClipData;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 75
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lccc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 76
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lccc;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 80
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 83
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/AdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 59
    new-instance v0, Lccf;

    invoke-direct {v0}, Lccf;-><init>()V

    iput-object v0, p0, Lccc;->d:Lccf;

    .line 62
    new-instance v0, Landroid/util/LongSparseArray;

    invoke-direct {v0}, Landroid/util/LongSparseArray;-><init>()V

    iput-object v0, p0, Lccc;->g:Landroid/util/LongSparseArray;

    .line 64
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lccc;->h:Landroid/util/SparseIntArray;

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lccc;->m:Landroid/widget/Adapter;

    .line 85
    sget-object v0, Lahj;->e:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    .line 88
    invoke-virtual {v3, v2, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lccc;->j:I

    .line 89
    const/4 v0, -0x1

    invoke-virtual {v3, v1, v0}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    iput v0, p0, Lccc;->k:I

    .line 90
    iget v0, p0, Lccc;->k:I

    if-eqz v0, :cond_0

    iget v0, p0, Lccc;->k:I

    if-ne v0, v2, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    iget v4, p0, Lccc;->k:I

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x3c

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "orientation must be horizontal or vertical, not: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 93
    const/4 v0, 0x2

    invoke-virtual {v3, v0, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v0

    .line 95
    cmpl-float v4, v0, v7

    if-lez v4, :cond_2

    float-to-int v0, v0

    const/high16 v4, -0x80000000

    .line 96
    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 97
    :goto_1
    iput v0, p0, Lccc;->l:I

    .line 99
    invoke-virtual {v3}, Landroid/content/res/TypedArray;->recycle()V

    .line 101
    new-instance v0, Lcce;

    invoke-direct {v0, p0}, Lcce;-><init>(Lccc;)V

    iput-object v0, p0, Lccc;->c:Landroid/database/DataSetObserver;

    .line 102
    new-instance v0, Lccj;

    invoke-virtual {p0}, Lccc;->b()Z

    move-result v3

    iget v4, p0, Lccc;->j:I

    invoke-direct {v0, v3, v4, v1}, Lccj;-><init>(ZIZ)V

    iput-object v0, p0, Lccc;->f:Lccj;

    .line 103
    invoke-virtual {p0}, Lccc;->a()[Lcch;

    move-result-object v0

    iput-object v0, p0, Lccc;->e:[Lcch;

    .line 104
    new-instance v0, Lccd;

    invoke-direct {v0, p0}, Lccd;-><init>(Lccc;)V

    iput-object v0, p0, Lccc;->i:Ljava/lang/Runnable;

    .line 112
    invoke-virtual {p0, v2}, Lccc;->setChildrenDrawingOrderEnabled(Z)V

    .line 113
    invoke-virtual {p0, v1}, Lccc;->setClipToPadding(Z)V

    .line 114
    return-void

    :cond_1
    move v0, v1

    .line 90
    goto :goto_0

    .line 97
    :cond_2
    invoke-static {v1, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    goto :goto_1
.end method

.method private a(Landroid/view/View;)V
    .locals 3

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 454
    invoke-virtual {p0}, Lccc;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 455
    iget v1, p0, Lccc;->l:I

    .line 456
    invoke-virtual {p0}, Lccc;->getHeight()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 462
    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/view/View;->measure(II)V

    .line 463
    return-void

    .line 458
    :cond_0
    invoke-virtual {p0}, Lccc;->getWidth()I

    move-result v0

    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 459
    iget v0, p0, Lccc;->l:I

    goto :goto_0
.end method

.method static synthetic a(Lccc;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lccc;->g()V

    return-void
.end method

.method static synthetic b(Lccc;)V
    .locals 5

    .prologue
    .line 47
    const/4 v0, 0x0

    iget-object v1, p0, Lccc;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v1}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v0, p0, Lccc;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    iget-object v3, p0, Lccc;->d:Lccf;

    iget v4, v0, Lccg;->c:I

    iget-object v0, v0, Lccg;->a:Landroid/view/View;

    invoke-virtual {v3, v4, v0}, Lccf;->a(ILandroid/view/View;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lccc;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    invoke-virtual {p0}, Lccc;->removeAllViewsInLayout()V

    return-void
.end method

.method private g()V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 339
    iget-object v0, p0, Lccc;->i:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lccc;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 340
    iget-object v0, p0, Lccc;->m:Landroid/widget/Adapter;

    if-nez v0, :cond_0

    .line 404
    :goto_0
    return-void

    .line 348
    :cond_0
    const-string v0, "refreshInternal"

    invoke-static {v0}, Laep;->a(Ljava/lang/String;)V

    .line 351
    iget-object v0, p0, Lccc;->f:Lccj;

    iget-object v1, p0, Lccc;->m:Landroid/widget/Adapter;

    iput-object v1, v0, Lccj;->h:Landroid/widget/Adapter;

    .line 352
    iget-object v0, p0, Lccc;->f:Lccj;

    iget-object v0, v0, Lccj;->d:Landroid/graphics/Rect;

    .line 353
    invoke-virtual {p0}, Lccc;->getPaddingLeft()I

    move-result v1

    .line 354
    invoke-virtual {p0}, Lccc;->getPaddingTop()I

    move-result v2

    .line 355
    invoke-virtual {p0}, Lccc;->getWidth()I

    move-result v4

    invoke-virtual {p0}, Lccc;->getPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    .line 356
    invoke-virtual {p0}, Lccc;->getHeight()I

    move-result v5

    invoke-virtual {p0}, Lccc;->getPaddingBottom()I

    move-result v6

    sub-int/2addr v5, v6

    .line 352
    invoke-virtual {v0, v1, v2, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 357
    iget-object v0, p0, Lccc;->f:Lccj;

    iget-object v0, v0, Lccj;->e:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lccc;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    .line 360
    invoke-virtual {p0}, Lccc;->getChildCount()I

    move-result v1

    move v0, v3

    :goto_1
    if-ge v0, v1, :cond_2

    .line 361
    invoke-virtual {p0, v0}, Lccc;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 362
    invoke-virtual {v2}, Landroid/view/View;->isLayoutRequested()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 363
    invoke-direct {p0, v2}, Lccc;->a(Landroid/view/View;)V

    .line 360
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 368
    :cond_2
    iget-object v1, p0, Lccc;->e:[Lcch;

    array-length v2, v1

    move v0, v3

    :goto_2
    if-ge v0, v2, :cond_3

    aget-object v4, v1, v0

    .line 369
    iget-object v5, p0, Lccc;->f:Lccj;

    invoke-interface {v4, v5}, Lcch;->a(Lccj;)V

    .line 368
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 372
    :cond_3
    invoke-virtual {p0}, Lccc;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 373
    iget-object v0, p0, Lccc;->f:Lccj;

    iget v0, v0, Lccj;->i:I

    invoke-virtual {p0, v0}, Lccc;->setScrollX(I)V

    .line 379
    :goto_3
    iget-object v0, p0, Lccc;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->size()I

    move-result v2

    move v1, v3

    .line 380
    :goto_4
    if-ge v1, v2, :cond_5

    .line 381
    iget-object v0, p0, Lccc;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    .line 382
    iget-object v4, p0, Lccc;->f:Lccj;

    iget-object v4, v4, Lccj;->c:Landroid/util/SparseArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->indexOfValue(Ljava/lang/Object;)I

    move-result v4

    if-gez v4, :cond_7

    .line 383
    iget-object v4, p0, Lccc;->g:Landroid/util/LongSparseArray;

    iget-wide v6, v0, Lccg;->b:J

    invoke-virtual {v4, v6, v7}, Landroid/util/LongSparseArray;->delete(J)V

    iget-object v4, p0, Lccc;->d:Lccf;

    iget v5, v0, Lccg;->c:I

    iget-object v6, v0, Lccg;->a:Landroid/view/View;

    invoke-virtual {v4, v5, v6}, Lccf;->a(ILandroid/view/View;)V

    iget-object v0, v0, Lccg;->a:Landroid/view/View;

    invoke-virtual {p0, v0}, Lccc;->removeViewInLayout(Landroid/view/View;)V

    .line 385
    add-int/lit8 v0, v1, -0x1

    .line 386
    add-int/lit8 v1, v2, -0x1

    .line 380
    :goto_5
    add-int/lit8 v0, v0, 0x1

    move v2, v1

    move v1, v0

    goto :goto_4

    .line 375
    :cond_4
    iget-object v0, p0, Lccc;->f:Lccj;

    iget v0, v0, Lccj;->i:I

    invoke-virtual {p0, v0}, Lccc;->setScrollY(I)V

    goto :goto_3

    .line 391
    :cond_5
    iget-object v0, p0, Lccc;->h:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->clear()V

    move v1, v3

    .line 392
    :goto_6
    if-ge v1, v2, :cond_6

    .line 393
    iget-object v0, p0, Lccc;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v1}, Landroid/util/LongSparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    .line 394
    iget-object v3, v0, Lccg;->a:Landroid/view/View;

    invoke-virtual {p0, v3}, Lccc;->indexOfChild(Landroid/view/View;)I

    move-result v3

    .line 397
    iget-short v0, v0, Lccg;->d:S

    shl-int/lit8 v0, v0, 0xf

    sub-int/2addr v0, v3

    .line 398
    iget-object v4, p0, Lccc;->h:Landroid/util/SparseIntArray;

    invoke-virtual {v4, v0, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 392
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    .line 401
    :cond_6
    invoke-virtual {p0}, Lccc;->invalidate()V

    .line 403
    invoke-static {}, Laep;->a()V

    goto/16 :goto_0

    :cond_7
    move v0, v1

    move v1, v2

    goto :goto_5
.end method


# virtual methods
.method public a(Landroid/widget/Adapter;I)Lccg;
    .locals 10

    .prologue
    const/4 v2, -0x2

    const/4 v9, 0x0

    const/4 v1, -0x1

    .line 214
    invoke-interface {p1, p2}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v4

    .line 215
    invoke-interface {p1, p2}, Landroid/widget/Adapter;->getItemViewType(I)I

    move-result v6

    .line 216
    iget-object v0, p0, Lccc;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v4, v5}, Landroid/util/LongSparseArray;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    .line 217
    if-eqz v0, :cond_0

    iget v3, v0, Lccg;->c:I

    if-eq v3, v6, :cond_2

    .line 219
    :cond_0
    new-instance v3, Lccg;

    invoke-interface {p1, p2}, Landroid/widget/Adapter;->getItemViewType(I)I

    move-result v0

    iget-object v7, p0, Lccc;->d:Lccf;

    invoke-virtual {v7, v0}, Lccf;->a(I)Landroid/view/View;

    move-result-object v0

    invoke-interface {p1, p2, v0, p0}, Landroid/widget/Adapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    invoke-virtual {p0, v7}, Lccc;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-eq v0, v1, :cond_1

    sget-object v0, Lccc;->a:Ljava/lang/String;

    :cond_1
    invoke-virtual {p0}, Lccc;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    new-instance v8, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v8, v2, v0}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v7, v1, v8}, Lccc;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)Z

    invoke-virtual {v7, v9}, Landroid/view/View;->setTranslationX(F)V

    invoke-virtual {v7, v9}, Landroid/view/View;->setTranslationY(F)V

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v7, v0}, Landroid/view/View;->setAlpha(F)V

    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Landroid/view/View;->setLongClickable(Z)V

    invoke-virtual {v7, p0}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    invoke-direct {v3, v7, v4, v5, v6}, Lccg;-><init>(Landroid/view/View;JI)V

    .line 223
    iget-object v0, v3, Lccg;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Lccc;->a(Landroid/view/View;)V

    .line 225
    iget-object v0, v3, Lccg;->a:Landroid/view/View;

    .line 226
    invoke-virtual {p0}, Lccc;->getPaddingLeft()I

    move-result v1

    .line 227
    invoke-virtual {p0}, Lccc;->getPaddingTop()I

    move-result v2

    iget-object v6, v3, Lccg;->a:Landroid/view/View;

    .line 228
    invoke-virtual {v6}, Landroid/view/View;->getMeasuredWidth()I

    move-result v6

    invoke-virtual {p0}, Lccc;->getPaddingLeft()I

    move-result v7

    add-int/2addr v6, v7

    iget-object v7, v3, Lccg;->a:Landroid/view/View;

    .line 229
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    invoke-virtual {p0}, Lccc;->getPaddingTop()I

    move-result v8

    add-int/2addr v7, v8

    .line 225
    invoke-virtual {v0, v1, v2, v6, v7}, Landroid/view/View;->layout(IIII)V

    .line 230
    iget-object v0, p0, Lccc;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0, v4, v5, v3}, Landroid/util/LongSparseArray;->put(JLjava/lang/Object;)V

    move-object v0, v3

    .line 233
    :cond_2
    return-object v0

    :cond_3
    move v0, v2

    move v2, v1

    .line 219
    goto :goto_0
.end method

.method public a(Ljava/lang/Class;)Lcch;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T::",
            "Lcch;",
            ">(",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 121
    iget-object v1, p0, Lccc;->e:[Lcch;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 122
    invoke-virtual {p1, v3}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 123
    invoke-virtual {p1, v3}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcch;

    .line 126
    :goto_1
    return-object v0

    .line 121
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 126
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public a(Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 238
    sget-object v0, Lccc;->b:Landroid/content/ClipData;

    const/4 v1, 0x0

    invoke-super {p0, v0, p1, p2, v1}, Landroid/widget/AdapterView;->startDrag(Landroid/content/ClipData;Landroid/view/View$DragShadowBuilder;Ljava/lang/Object;I)Z

    .line 239
    return-void
.end method

.method public a()[Lcch;
    .locals 3

    .prologue
    .line 325
    const/4 v0, 0x2

    new-array v0, v0, [Lcch;

    const/4 v1, 0x0

    new-instance v2, Lcdo;

    invoke-direct {v2, p0}, Lcdo;-><init>(Lcci;)V

    aput-object v2, v0, v1

    const/4 v1, 0x1

    new-instance v2, Lcdm;

    invoke-direct {v2, p0}, Lcdm;-><init>(Lcci;)V

    aput-object v2, v0, v1

    return-object v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lccc;->k:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lccc;->f:Lccj;

    iget-boolean v0, v0, Lccj;->j:Z

    return v0
.end method

.method public d()V
    .locals 1

    .prologue
    .line 194
    invoke-virtual {p0}, Lccc;->getWidth()I

    move-result v0

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lccc;->getHeight()I

    move-result v0

    if-lez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    .line 195
    iget-object v0, p0, Lccc;->i:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lccc;->postOnAnimation(Ljava/lang/Runnable;)V

    .line 200
    :cond_1
    return-void

    .line 194
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public dispatchDragEvent(Landroid/view/DragEvent;)Z
    .locals 5

    .prologue
    .line 306
    iget-object v1, p0, Lccc;->e:[Lcch;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 307
    iget-object v4, p0, Lccc;->f:Lccj;

    invoke-interface {v3, v4, p1}, Lcch;->a(Lccj;Landroid/view/DragEvent;)Z

    .line 306
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 309
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    .line 271
    invoke-super {p0, p1}, Landroid/widget/AdapterView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 272
    iget-object v1, p0, Lccc;->e:[Lcch;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 273
    iget-object v4, p0, Lccc;->f:Lccj;

    invoke-interface {v3, v4, p1}, Lcch;->a(Lccj;Landroid/graphics/Canvas;)V

    .line 272
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 275
    :cond_0
    return-void
.end method

.method public e()V
    .locals 0

    .prologue
    .line 204
    invoke-virtual {p0}, Lccc;->invalidate()V

    .line 205
    return-void
.end method

.method public f()Landroid/content/Context;
    .locals 1

    .prologue
    .line 209
    invoke-virtual {p0}, Lccc;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lccc;->m:Landroid/widget/Adapter;

    return-object v0
.end method

.method protected getChildDrawingOrder(II)I
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lccc;->h:Landroid/util/SparseIntArray;

    invoke-virtual {v0}, Landroid/util/SparseIntArray;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    iget-object v0, p0, Lccc;->h:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseIntArray;->valueAt(I)I

    move-result p2

    :cond_0
    return p2
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 1

    .prologue
    .line 181
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lccc;->f:Lccj;

    invoke-virtual {v0}, Lccj;->d()V

    .line 315
    iget-object v0, p0, Lccc;->i:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lccc;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 316
    invoke-super {p0}, Landroid/widget/AdapterView;->onDetachedFromWindow()V

    .line 317
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 281
    invoke-virtual {p0, p1}, Lccc;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 0

    .prologue
    .line 260
    invoke-super/range {p0 .. p5}, Landroid/widget/AdapterView;->onLayout(ZIIII)V

    .line 261
    invoke-direct {p0}, Lccc;->g()V

    .line 262
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 245
    iget-object v1, p0, Lccc;->f:Lccj;

    invoke-virtual {v1, p1}, Lccj;->d(Landroid/view/View;)I

    move-result v3

    .line 246
    if-ltz v3, :cond_0

    .line 248
    iget-object v4, p0, Lccc;->e:[Lcch;

    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v2, v4, v1

    .line 249
    iget-object v6, p0, Lccc;->f:Lccj;

    invoke-interface {v2, v6, v3}, Lcch;->a(Lccj;I)Z

    move-result v2

    or-int/2addr v2, v0

    .line 248
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    .line 253
    :cond_0
    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 286
    .line 287
    iget-object v2, p0, Lccc;->e:[Lcch;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 288
    iget-object v5, p0, Lccc;->f:Lccj;

    invoke-interface {v4, v5, p1}, Lcch;->a(Lccj;Landroid/view/MotionEvent;)Z

    move-result v4

    or-int/2addr v1, v4

    .line 287
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 290
    :cond_0
    if-eqz v1, :cond_1

    .line 291
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lccc;->requestDisallowInterceptTouchEvent(Z)V

    .line 293
    :cond_1
    return v1
.end method

.method public setAdapter(Landroid/widget/Adapter;)V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lccc;->m:Landroid/widget/Adapter;

    if-eqz v0, :cond_0

    .line 162
    iget-object v0, p0, Lccc;->m:Landroid/widget/Adapter;

    iget-object v1, p0, Lccc;->c:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 165
    :cond_0
    iget-object v0, p0, Lccc;->g:Landroid/util/LongSparseArray;

    invoke-virtual {v0}, Landroid/util/LongSparseArray;->clear()V

    .line 166
    invoke-virtual {p0}, Lccc;->removeAllViewsInLayout()V

    .line 167
    iget-object v0, p0, Lccc;->d:Lccf;

    invoke-virtual {v0}, Lccf;->a()V

    .line 168
    iput-object p1, p0, Lccc;->m:Landroid/widget/Adapter;

    .line 170
    iget-object v0, p0, Lccc;->m:Landroid/widget/Adapter;

    if-eqz v0, :cond_1

    .line 171
    iget-object v0, p0, Lccc;->m:Landroid/widget/Adapter;

    invoke-interface {v0}, Landroid/widget/Adapter;->hasStableIds()Z

    move-result v0

    const-string v1, "StripView currently only works with adapters that have stable ids"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 173
    iget-object v0, p0, Lccc;->m:Landroid/widget/Adapter;

    iget-object v1, p0, Lccc;->c:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 174
    invoke-virtual {p0}, Lccc;->d()V

    .line 176
    :cond_1
    return-void
.end method

.method public setSelection(I)V
    .locals 0

    .prologue
    .line 187
    return-void
.end method
