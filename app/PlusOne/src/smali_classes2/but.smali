.class public final Lbut;
.super Loxu;
.source "PG"


# static fields
.field public static final a:[Lbut;


# instance fields
.field public b:I

.field public c:Lbup;

.field public d:Lbuq;

.field public e:Lbur;

.field public f:Lbus;

.field public g:Lbuu;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lbut;

    sput-object v0, Lbut;->a:[Lbut;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxu;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lbut;->b:I

    .line 25
    iput-object v1, p0, Lbut;->c:Lbup;

    .line 28
    iput-object v1, p0, Lbut;->d:Lbuq;

    .line 31
    iput-object v1, p0, Lbut;->e:Lbur;

    .line 34
    iput-object v1, p0, Lbut;->f:Lbus;

    .line 37
    iput-object v1, p0, Lbut;->g:Lbuu;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 75
    const/4 v0, 0x0

    .line 76
    iget v1, p0, Lbut;->b:I

    if-eqz v1, :cond_0

    .line 77
    const/4 v0, 0x1

    iget v1, p0, Lbut;->b:I

    .line 78
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 80
    :cond_0
    iget-object v1, p0, Lbut;->c:Lbup;

    if-eqz v1, :cond_1

    .line 81
    const/4 v1, 0x2

    iget-object v2, p0, Lbut;->c:Lbup;

    .line 82
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    :cond_1
    iget-object v1, p0, Lbut;->d:Lbuq;

    if-eqz v1, :cond_2

    .line 85
    const/4 v1, 0x3

    iget-object v2, p0, Lbut;->d:Lbuq;

    .line 86
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    :cond_2
    iget-object v1, p0, Lbut;->e:Lbur;

    if-eqz v1, :cond_3

    .line 89
    const/4 v1, 0x4

    iget-object v2, p0, Lbut;->e:Lbur;

    .line 90
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    :cond_3
    iget-object v1, p0, Lbut;->f:Lbus;

    if-eqz v1, :cond_4

    .line 93
    const/4 v1, 0x5

    iget-object v2, p0, Lbut;->f:Lbus;

    .line 94
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    :cond_4
    iget-object v1, p0, Lbut;->g:Lbuu;

    if-eqz v1, :cond_5

    .line 97
    const/4 v1, 0x6

    iget-object v2, p0, Lbut;->g:Lbuu;

    .line 98
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 100
    :cond_5
    iput v0, p0, Lbut;->ai:I

    .line 101
    return v0
.end method

.method public a(Loxn;)Lbut;
    .locals 2

    .prologue
    .line 109
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 110
    sparse-switch v0, :sswitch_data_0

    .line 114
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 115
    :sswitch_0
    return-object p0

    .line 120
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 121
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    const/4 v1, 0x2

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 127
    :cond_1
    iput v0, p0, Lbut;->b:I

    goto :goto_0

    .line 129
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lbut;->b:I

    goto :goto_0

    .line 134
    :sswitch_2
    iget-object v0, p0, Lbut;->c:Lbup;

    if-nez v0, :cond_3

    .line 135
    new-instance v0, Lbup;

    invoke-direct {v0}, Lbup;-><init>()V

    iput-object v0, p0, Lbut;->c:Lbup;

    .line 137
    :cond_3
    iget-object v0, p0, Lbut;->c:Lbup;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 141
    :sswitch_3
    iget-object v0, p0, Lbut;->d:Lbuq;

    if-nez v0, :cond_4

    .line 142
    new-instance v0, Lbuq;

    invoke-direct {v0}, Lbuq;-><init>()V

    iput-object v0, p0, Lbut;->d:Lbuq;

    .line 144
    :cond_4
    iget-object v0, p0, Lbut;->d:Lbuq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 148
    :sswitch_4
    iget-object v0, p0, Lbut;->e:Lbur;

    if-nez v0, :cond_5

    .line 149
    new-instance v0, Lbur;

    invoke-direct {v0}, Lbur;-><init>()V

    iput-object v0, p0, Lbut;->e:Lbur;

    .line 151
    :cond_5
    iget-object v0, p0, Lbut;->e:Lbur;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 155
    :sswitch_5
    iget-object v0, p0, Lbut;->f:Lbus;

    if-nez v0, :cond_6

    .line 156
    new-instance v0, Lbus;

    invoke-direct {v0}, Lbus;-><init>()V

    iput-object v0, p0, Lbut;->f:Lbus;

    .line 158
    :cond_6
    iget-object v0, p0, Lbut;->f:Lbus;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 162
    :sswitch_6
    iget-object v0, p0, Lbut;->g:Lbuu;

    if-nez v0, :cond_7

    .line 163
    new-instance v0, Lbuu;

    invoke-direct {v0}, Lbuu;-><init>()V

    iput-object v0, p0, Lbut;->g:Lbuu;

    .line 165
    :cond_7
    iget-object v0, p0, Lbut;->g:Lbuu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 110
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 53
    iget v0, p0, Lbut;->b:I

    if-eqz v0, :cond_0

    .line 54
    const/4 v0, 0x1

    iget v1, p0, Lbut;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 56
    :cond_0
    iget-object v0, p0, Lbut;->c:Lbup;

    if-eqz v0, :cond_1

    .line 57
    const/4 v0, 0x2

    iget-object v1, p0, Lbut;->c:Lbup;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 59
    :cond_1
    iget-object v0, p0, Lbut;->d:Lbuq;

    if-eqz v0, :cond_2

    .line 60
    const/4 v0, 0x3

    iget-object v1, p0, Lbut;->d:Lbuq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 62
    :cond_2
    iget-object v0, p0, Lbut;->e:Lbur;

    if-eqz v0, :cond_3

    .line 63
    const/4 v0, 0x4

    iget-object v1, p0, Lbut;->e:Lbur;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 65
    :cond_3
    iget-object v0, p0, Lbut;->f:Lbus;

    if-eqz v0, :cond_4

    .line 66
    const/4 v0, 0x5

    iget-object v1, p0, Lbut;->f:Lbus;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 68
    :cond_4
    iget-object v0, p0, Lbut;->g:Lbuu;

    if-eqz v0, :cond_5

    .line 69
    const/4 v0, 0x6

    iget-object v1, p0, Lbut;->g:Lbuu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 71
    :cond_5
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lbut;->a(Loxn;)Lbut;

    move-result-object v0

    return-object v0
.end method
