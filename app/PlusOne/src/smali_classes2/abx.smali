.class public final Labx;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:I

.field private static b:I

.field private static final c:Ljava/lang/String;

.field private static final d:Laca;

.field private static final e:Laca;

.field private static final f:Laca;

.field private static final g:Laca;

.field private static final h:Laca;

.field private static final i:Laca;

.field private static final j:Laca;

.field private static q:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Labx;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final k:Lacs;

.field private l:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Laan;",
            ">;"
        }
    .end annotation
.end field

.field private m:Lacj;

.field private n:Lace;

.field private o:Ljava/lang/Thread;

.field private p:Laat;

.field private r:Lach;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 48
    const/4 v0, -0x1

    sput v0, Labx;->a:I

    .line 49
    const/4 v0, -0x2

    sput v0, Labx;->b:I

    .line 51
    const-class v0, Labx;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Labx;->c:Ljava/lang/String;

    .line 53
    new-instance v0, Laca;

    const/4 v1, 0x2

    invoke-direct {v0, v1, v2}, Laca;-><init>(ILjava/lang/Object;)V

    sput-object v0, Labx;->d:Laca;

    .line 54
    new-instance v0, Laca;

    const/16 v1, 0xa

    invoke-direct {v0, v1, v2}, Laca;-><init>(ILjava/lang/Object;)V

    sput-object v0, Labx;->e:Laca;

    .line 55
    new-instance v0, Laca;

    const/4 v1, 0x7

    invoke-direct {v0, v1, v2}, Laca;-><init>(ILjava/lang/Object;)V

    sput-object v0, Labx;->f:Laca;

    .line 56
    new-instance v0, Laca;

    const/16 v1, 0xc

    invoke-direct {v0, v1, v2}, Laca;-><init>(ILjava/lang/Object;)V

    sput-object v0, Labx;->g:Laca;

    .line 57
    new-instance v0, Laca;

    const/4 v1, 0x6

    invoke-direct {v0, v1, v2}, Laca;-><init>(ILjava/lang/Object;)V

    .line 58
    new-instance v0, Laca;

    const/16 v1, 0xd

    invoke-direct {v0, v1, v2}, Laca;-><init>(ILjava/lang/Object;)V

    sput-object v0, Labx;->h:Laca;

    .line 59
    new-instance v0, Laca;

    const/16 v1, 0x9

    invoke-direct {v0, v1, v2}, Laca;-><init>(ILjava/lang/Object;)V

    .line 60
    new-instance v0, Laca;

    const/16 v1, 0x8

    invoke-direct {v0, v1, v2}, Laca;-><init>(ILjava/lang/Object;)V

    .line 61
    new-instance v0, Laca;

    const/4 v1, 0x3

    invoke-direct {v0, v1, v2}, Laca;-><init>(ILjava/lang/Object;)V

    sput-object v0, Labx;->i:Laca;

    .line 62
    new-instance v0, Laca;

    const/4 v1, 0x4

    invoke-direct {v0, v1, v2}, Laca;-><init>(ILjava/lang/Object;)V

    sput-object v0, Labx;->j:Laca;

    .line 899
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    sput-object v0, Labx;->q:Ljava/lang/ThreadLocal;

    return-void
.end method

.method public constructor <init>(Lacs;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 909
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 888
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Labx;->l:Ljava/util/Set;

    .line 895
    iput-object v1, p0, Labx;->o:Ljava/lang/Thread;

    .line 897
    iput-object v1, p0, Labx;->p:Laat;

    .line 901
    new-instance v0, Lach;

    invoke-direct {v0}, Lach;-><init>()V

    iput-object v0, p0, Labx;->r:Lach;

    .line 910
    iput-object p1, p0, Labx;->k:Lacs;

    .line 911
    new-instance v0, Labz;

    invoke-direct {v0}, Labz;-><init>()V

    new-instance v1, Laat;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2}, Laat;-><init>(Labx;I)V

    iput-object v1, p0, Labx;->p:Laat;

    new-instance v1, Lacb;

    invoke-direct {v1}, Lacb;-><init>()V

    iput-object v1, p0, Labx;->m:Lacj;

    new-instance v1, Lace;

    iget-boolean v2, v0, Labz;->b:Z

    invoke-direct {v1, p0, v2}, Lace;-><init>(Labx;Z)V

    iput-object v1, p0, Labx;->n:Lace;

    new-instance v1, Ljava/lang/Thread;

    iget-object v2, p0, Labx;->n:Lace;

    invoke-direct {v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v1, p0, Labx;->o:Ljava/lang/Thread;

    iget-object v1, p0, Labx;->o:Ljava/lang/Thread;

    iget v0, v0, Labz;->a:I

    invoke-virtual {v1, v0}, Ljava/lang/Thread;->setPriority(I)V

    iget-object v0, p0, Labx;->o:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    iget-object v0, p0, Labx;->k:Lacs;

    invoke-virtual {v0, p0}, Lacs;->a(Labx;)V

    .line 912
    return-void
.end method

.method static synthetic a(Labx;)Laat;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Labx;->p:Laat;

    return-object v0
.end method

.method public static a()Labx;
    .locals 1

    .prologue
    .line 931
    sget-object v0, Labx;->q:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Labx;

    return-object v0
.end method

.method static synthetic a(Labx;Ljava/lang/Exception;Z)V
    .locals 3

    .prologue
    .line 46
    iget-object v1, p0, Labx;->r:Lach;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Labx;->r:Lach;

    iget-object v0, v0, Lach;->a:Lacg;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Labx;->b()Lacs;

    move-result-object v0

    new-instance v2, Laby;

    invoke-direct {v2, p0, p1, p2}, Laby;-><init>(Labx;Ljava/lang/Exception;Z)V

    invoke-virtual {v0, v2}, Lacs;->a(Ljava/lang/Runnable;)V

    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    :cond_1
    if-eqz p1, :cond_0

    const-string v0, "GraphRunner"

    const-string v2, "Uncaught exception during graph execution! Stack Trace: "

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic b(Labx;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Labx;->l:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic c(Labx;)Lacj;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Labx;->m:Lacj;

    return-object v0
.end method

.method static synthetic d(Labx;)Lach;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Labx;->r:Lach;

    return-object v0
.end method

.method static synthetic p()Laca;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Labx;->j:Laca;

    return-object v0
.end method

.method static synthetic q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Labx;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic r()Laca;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Labx;->i:Laca;

    return-object v0
.end method

.method static synthetic s()Ljava/lang/ThreadLocal;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Labx;->q:Ljava/lang/ThreadLocal;

    return-object v0
.end method

.method static synthetic t()Laca;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Labx;->d:Laca;

    return-object v0
.end method

.method static synthetic u()I
    .locals 1

    .prologue
    .line 46
    sget v0, Labx;->a:I

    return v0
.end method

.method static synthetic v()I
    .locals 1

    .prologue
    .line 46
    sget v0, Labx;->b:I

    return v0
.end method


# virtual methods
.method public declared-synchronized a(Laan;)V
    .locals 2

    .prologue
    .line 958
    monitor-enter p0

    :try_start_0
    iget-object v0, p1, Laan;->c:Labx;

    if-eq v0, p0, :cond_0

    .line 959
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Graph must be attached to runner!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 958
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 961
    :cond_0
    :try_start_1
    iget-object v0, p0, Labx;->n:Lace;

    invoke-virtual {v0}, Lace;->a()Landroid/os/ConditionVariable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->close()V

    .line 963
    iget-object v0, p0, Labx;->n:Lace;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Lace;->a(ILjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 964
    monitor-exit p0

    return-void
.end method

.method public a(Laan;Lacl;)V
    .locals 2

    .prologue
    .line 971
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Labx;->o:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    .line 972
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "enterSubGraph must be called from the runner\'s thread!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 974
    :cond_0
    iget-object v0, p0, Labx;->n:Lace;

    invoke-virtual {v0, p1, p2}, Lace;->a(Laan;Lacl;)V

    .line 975
    return-void
.end method

.method public a(Lacg;)V
    .locals 2

    .prologue
    .line 1135
    iget-object v1, p0, Labx;->r:Lach;

    monitor-enter v1

    .line 1136
    :try_start_0
    iget-object v0, p0, Labx;->r:Lach;

    iput-object p1, v0, Lach;->a:Lacg;

    .line 1137
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 1071
    iget-object v1, p0, Labx;->r:Lach;

    monitor-enter v1

    .line 1072
    :try_start_0
    iget-object v0, p0, Labx;->r:Lach;

    iput-boolean p1, v0, Lach;->b:Z

    .line 1073
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()Lacs;
    .locals 1

    .prologue
    .line 950
    iget-object v0, p0, Labx;->k:Lacs;

    return-object v0
.end method

.method b(Laan;)V
    .locals 2

    .prologue
    .line 1192
    iget-object v1, p0, Labx;->l:Ljava/util/Set;

    monitor-enter v1

    .line 1193
    :try_start_0
    iget-object v0, p0, Labx;->l:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1194
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 983
    iget-object v0, p0, Labx;->n:Lace;

    invoke-virtual {v0}, Lace;->a()Landroid/os/ConditionVariable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 984
    return-void
.end method

.method c(Laan;)V
    .locals 2

    .prologue
    .line 1221
    invoke-virtual {p1}, Laan;->c()Labx;

    move-result-object v0

    if-eq v0, p0, :cond_0

    .line 1222
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Attempting to tear down graph with foreign GraphRunner!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1225
    :cond_0
    iget-object v0, p0, Labx;->n:Lace;

    const/16 v1, 0xb

    invoke-virtual {v0, v1, p1}, Lace;->a(ILjava/lang/Object;)V

    .line 1226
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 1004
    iget-object v0, p0, Labx;->n:Lace;

    sget-object v1, Labx;->j:Laca;

    invoke-virtual {v0, v1}, Lace;->b(Laca;)V

    .line 1005
    return-void
.end method

.method public e()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1014
    iget-object v1, p0, Labx;->n:Lace;

    invoke-virtual {v1, v0}, Lace;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    .prologue
    .line 1023
    iget-object v0, p0, Labx;->n:Lace;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lace;->a(I)Z

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 2

    .prologue
    .line 1032
    iget-object v0, p0, Labx;->n:Lace;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lace;->a(I)Z

    move-result v0

    return v0
.end method

.method public h()Z
    .locals 2

    .prologue
    .line 1083
    iget-object v1, p0, Labx;->r:Lach;

    monitor-enter v1

    .line 1084
    :try_start_0
    iget-object v0, p0, Labx;->r:Lach;

    iget-boolean v0, v0, Lach;->b:Z

    monitor-exit v1

    return v0

    .line 1085
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 1096
    iget-object v0, p0, Labx;->n:Lace;

    invoke-virtual {v0}, Lace;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Labx;->k:Lacs;

    iget-boolean v0, v0, Lacs;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()Z
    .locals 2

    .prologue
    .line 1121
    iget-object v1, p0, Labx;->r:Lach;

    monitor-enter v1

    .line 1122
    :try_start_0
    iget-object v0, p0, Labx;->r:Lach;

    iget-boolean v0, v0, Lach;->c:Z

    monitor-exit v1

    return v0

    .line 1123
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public k()Laat;
    .locals 1

    .prologue
    .line 1158
    iget-object v0, p0, Labx;->p:Laat;

    return-object v0
.end method

.method public l()V
    .locals 2

    .prologue
    .line 1168
    iget-object v0, p0, Labx;->n:Lace;

    sget-object v1, Labx;->g:Laca;

    invoke-virtual {v0, v1}, Lace;->b(Laca;)V

    .line 1171
    :try_start_0
    iget-object v0, p0, Labx;->o:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1175
    :goto_0
    return-void

    .line 1173
    :catch_0
    move-exception v0

    const-string v0, "GraphRunner"

    const-string v1, "Error waiting for runner thread to finish!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public m()V
    .locals 2

    .prologue
    .line 1187
    iget-object v0, p0, Labx;->n:Lace;

    sget-object v1, Labx;->h:Laca;

    invoke-virtual {v0, v1}, Lace;->b(Laca;)V

    .line 1188
    return-void
.end method

.method n()V
    .locals 2

    .prologue
    .line 1198
    iget-object v0, p0, Labx;->n:Lace;

    sget-object v1, Labx;->i:Laca;

    invoke-virtual {v0, v1}, Lace;->a(Laca;)V

    .line 1199
    return-void
.end method

.method o()V
    .locals 2

    .prologue
    .line 1240
    iget-object v0, p0, Labx;->n:Lace;

    sget-object v1, Labx;->e:Laca;

    invoke-virtual {v0, v1}, Lace;->b(Laca;)V

    .line 1241
    return-void
.end method
