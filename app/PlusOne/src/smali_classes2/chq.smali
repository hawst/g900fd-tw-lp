.class final Lchq;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Lchs;

.field private synthetic b:Lchp;


# direct methods
.method public constructor <init>(Lchp;Lchs;)V
    .locals 2

    .prologue
    .line 400
    iput-object p1, p0, Lchq;->b:Lchp;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 401
    if-nez p2, :cond_0

    .line 402
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid update filter chain listener"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 405
    :cond_0
    iput-object p2, p0, Lchq;->a:Lchs;

    .line 406
    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Object;)Ljava/lang/Void;
    .locals 2

    .prologue
    .line 410
    iget-object v1, p0, Lchq;->b:Lchp;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    check-cast v0, Lcom/google/android/libraries/photoeditor/core/FilterChain;

    invoke-static {v1, v0}, Lchp;->a(Lchp;Lcom/google/android/libraries/photoeditor/core/FilterChain;)V

    .line 411
    const/4 v0, 0x0

    return-object v0
.end method

.method protected a()V
    .locals 2

    .prologue
    .line 421
    iget-object v0, p0, Lchq;->a:Lchs;

    iget-object v1, p0, Lchq;->b:Lchp;

    invoke-interface {v0}, Lchs;->b()V

    .line 422
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 397
    invoke-virtual {p0, p1}, Lchq;->a([Ljava/lang/Object;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 397
    invoke-virtual {p0}, Lchq;->a()V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 416
    iget-object v0, p0, Lchq;->a:Lchs;

    iget-object v1, p0, Lchq;->b:Lchp;

    invoke-interface {v0}, Lchs;->a()V

    .line 417
    return-void
.end method
