.class public final Lbid;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:J

.field private final b:J


# direct methods
.method public constructor <init>(JJ)V
    .locals 3

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string v0, "endPointUs"

    invoke-static {p1, p2, v0}, Lcec;->a(JLjava/lang/CharSequence;)J

    move-result-wide v0

    iput-wide v0, p0, Lbid;->a:J

    .line 20
    const-string v0, "fadeDurationUs"

    invoke-static {p3, p4, v0}, Lcec;->a(JLjava/lang/CharSequence;)J

    move-result-wide v0

    iput-wide v0, p0, Lbid;->b:J

    .line 21
    return-void
.end method


# virtual methods
.method public a(Lbia;)V
    .locals 20

    .prologue
    .line 28
    move-object/from16 v0, p1

    iget v2, v0, Lbia;->g:I

    move-object/from16 v0, p1

    iget v3, v0, Lbia;->c:I

    move-object/from16 v0, p1

    iget v4, v0, Lbia;->e:I

    move-object/from16 v0, p1

    iget v5, v0, Lbia;->d:I

    invoke-static {v2, v3, v4, v5}, Lceg;->a(IIII)J

    move-result-wide v2

    .line 34
    move-object/from16 v0, p1

    iget-wide v4, v0, Lbia;->a:J

    sub-long/2addr v4, v2

    .line 35
    move-object/from16 v0, p0

    iget-wide v6, v0, Lbid;->a:J

    sub-long v4, v6, v4

    .line 37
    move-object/from16 v0, p0

    iget-wide v6, v0, Lbid;->b:J

    cmp-long v6, v4, v6

    if-gez v6, :cond_7

    .line 38
    long-to-double v6, v4

    move-object/from16 v0, p0

    iget-wide v8, v0, Lbid;->b:J

    long-to-double v8, v8

    div-double v10, v6, v8

    .line 41
    const-wide/16 v6, 0x0

    sub-long v2, v4, v2

    invoke-static {v6, v7, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 44
    long-to-double v2, v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lbid;->b:J

    long-to-double v4, v4

    div-double v12, v2, v4

    .line 47
    move-object/from16 v0, p1

    iget v14, v0, Lbia;->c:I

    move-object/from16 v0, p1

    iget v3, v0, Lbia;->e:I

    move-object/from16 v0, p1

    iget-object v15, v0, Lbia;->f:[B

    move-object/from16 v0, p1

    iget v0, v0, Lbia;->g:I

    move/from16 v16, v0

    const/4 v2, 0x1

    if-eq v14, v2, :cond_0

    const/4 v2, 0x2

    if-ne v14, v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    const-string v4, "only supports 8-bit or 16-bit PCM samples"

    invoke-static {v2, v4}, Lcec;->a(ZLjava/lang/CharSequence;)V

    const-string v2, "channelCount"

    invoke-static {v3, v2}, Lcec;->a(ILjava/lang/CharSequence;)I

    const-string v2, "samples"

    const/4 v4, 0x0

    invoke-static {v15, v2, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    if-eqz v16, :cond_7

    const/4 v2, 0x0

    const-string v4, "offset"

    invoke-static {v2, v4, v15}, Lcec;->a(ILjava/lang/CharSequence;[B)I

    const-string v2, "length"

    const/4 v4, 0x0

    move/from16 v0, v16

    invoke-static {v0, v2, v4}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    add-int/lit8 v2, v16, 0x0

    array-length v4, v15

    if-gt v2, v4, :cond_2

    const/4 v2, 0x1

    :goto_1
    const-string v4, "offset + length must be less than or equal to samples.length"

    invoke-static {v2, v4}, Lcec;->a(ZLjava/lang/CharSequence;)V

    mul-int v17, v14, v3

    const/4 v2, 0x0

    rem-int v2, v2, v17

    if-nez v2, :cond_3

    const/4 v2, 0x1

    :goto_2
    const-string v3, "offset must be a multiple of frame size (bytesPerSample * channelCount)"

    invoke-static {v2, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    rem-int v2, v16, v17

    if-nez v2, :cond_4

    const/4 v2, 0x1

    :goto_3
    const-string v3, "length must be a multiple of frame size (bytesPerSample * channelCount)"

    invoke-static {v2, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    const-string v2, "volumeStart"

    invoke-static {v10, v11, v2}, Lcec;->a(DLjava/lang/CharSequence;)D

    const-string v2, "volumeEnd"

    invoke-static {v12, v13, v2}, Lcec;->a(DLjava/lang/CharSequence;)D

    const/4 v9, 0x0

    :goto_4
    add-int/lit8 v2, v16, 0x0

    if-ge v9, v2, :cond_7

    sub-double v2, v12, v10

    int-to-double v4, v9

    mul-double/2addr v2, v4

    move/from16 v0, v16

    int-to-double v4, v0

    div-double/2addr v2, v4

    add-double v18, v10, v2

    move v8, v9

    :goto_5
    add-int v2, v9, v17

    if-ge v8, v2, :cond_6

    invoke-static {v15, v8, v14}, Lceg;->a([BII)S

    move-result v2

    int-to-double v2, v2

    mul-double v2, v2, v18

    double-to-long v2, v2

    const/4 v4, 0x1

    if-ne v14, v4, :cond_5

    const-wide/16 v4, -0x80

    const-wide/16 v6, 0x7f

    invoke-static/range {v2 .. v7}, Lcfn;->a(JJJ)J

    move-result-wide v2

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v15, v8

    :goto_6
    add-int v2, v8, v14

    move v8, v2

    goto :goto_5

    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    goto :goto_3

    :cond_5
    const-wide/16 v4, -0x8000

    const-wide/16 v6, 0x7fff

    invoke-static/range {v2 .. v7}, Lcfn;->a(JJJ)J

    move-result-wide v2

    const-wide/16 v4, 0xff

    and-long/2addr v4, v2

    long-to-int v4, v4

    int-to-byte v4, v4

    aput-byte v4, v15, v8

    add-int/lit8 v4, v8, 0x1

    const/16 v5, 0x8

    shr-long/2addr v2, v5

    const-wide/16 v6, 0xff

    and-long/2addr v2, v6

    long-to-int v2, v2

    int-to-byte v2, v2

    aput-byte v2, v15, v4

    goto :goto_6

    :cond_6
    add-int v9, v9, v17

    goto :goto_4

    .line 56
    :cond_7
    return-void
.end method
