.class public final Lbzl;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbzf;


# instance fields
.field private final a:Ljava/util/concurrent/Future;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Future",
            "<",
            "Lbvq;",
            ">;"
        }
    .end annotation
.end field

.field private final b:Lbzc;

.field private c:Lbxw;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lbzl;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/util/concurrent/Future;Lbzc;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Future",
            "<",
            "Lbvq;",
            ">;",
            "Lbzc;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    sget-object v0, Lbzc;->a:Lbzc;

    if-ne p2, v0, :cond_0

    .line 60
    iput-object v1, p0, Lbzl;->a:Ljava/util/concurrent/Future;

    .line 64
    :goto_0
    const-string v0, "summarizerType"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzc;

    iput-object v0, p0, Lbzl;->b:Lbzc;

    .line 65
    return-void

    .line 62
    :cond_0
    const-string v0, "modelFuture"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Future;

    iput-object v0, p0, Lbzl;->a:Ljava/util/concurrent/Future;

    goto :goto_0
.end method

.method private b()Lbvq;
    .locals 2

    .prologue
    .line 191
    :try_start_0
    iget-object v0, p0, Lbzl;->a:Ljava/util/concurrent/Future;

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbvq;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 192
    :catch_0
    move-exception v0

    .line 193
    const-string v1, "Failed to get summarizer"

    invoke-static {v1, v0}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/Throwable;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 194
    :catch_1
    move-exception v0

    .line 195
    const-string v1, "Failed to get summarizer"

    invoke-static {v1, v0}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/Throwable;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method


# virtual methods
.method public a(Ljava/util/List;Ljava/util/List;)J
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbon;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmw;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 143
    iget-object v0, p0, Lbzl;->c:Lbxw;

    if-nez v0, :cond_0

    .line 144
    invoke-virtual {p0}, Lbzl;->a()Lbxw;

    move-result-object v0

    iput-object v0, p0, Lbzl;->c:Lbxw;

    .line 146
    :cond_0
    new-instance v0, Lbxz;

    invoke-direct {v0}, Lbxz;-><init>()V

    const-wide/32 v2, 0x1e8480

    .line 147
    invoke-virtual {v0, v2, v3}, Lbxz;->b(J)Lbxz;

    move-result-object v0

    const-wide v2, 0x7fffffffffffffffL

    .line 148
    invoke-virtual {v0, v2, v3}, Lbxz;->a(J)Lbxz;

    move-result-object v0

    .line 149
    invoke-virtual {v0, p2}, Lbxz;->b(Ljava/util/List;)Lbxz;

    move-result-object v0

    .line 150
    invoke-virtual {v0, p1}, Lbxz;->a(Ljava/util/List;)Lbxz;

    move-result-object v0

    .line 151
    invoke-virtual {v0}, Lbxz;->a()Lbxy;

    move-result-object v0

    .line 152
    iget-object v1, p0, Lbzl;->c:Lbxw;

    invoke-interface {v1, v0}, Lbxw;->b(Lbxy;)J

    move-result-wide v0

    return-wide v0
.end method

.method public a()Lbxw;
    .locals 2

    .prologue
    .line 177
    sget-object v0, Lbzm;->a:[I

    iget-object v1, p0, Lbzl;->b:Lbzc;

    invoke-virtual {v1}, Lbzc;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 185
    const-string v0, "Unknown summarizer type"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    .line 179
    :pswitch_0
    new-instance v0, Lbxv;

    invoke-direct {v0}, Lbxv;-><init>()V

    .line 183
    :goto_0
    return-object v0

    .line 181
    :pswitch_1
    invoke-direct {p0}, Lbzl;->b()Lbvq;

    move-result-object v0

    invoke-static {v0}, Lbxx;->b(Lbvq;)Lbxu;

    move-result-object v0

    goto :goto_0

    .line 183
    :pswitch_2
    invoke-direct {p0}, Lbzl;->b()Lbvq;

    move-result-object v0

    invoke-static {v0}, Lbxx;->a(Lbvq;)Lbxu;

    move-result-object v0

    goto :goto_0

    .line 177
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Ljava/util/List;Ljava/util/List;Ljava/util/Map;Lbor;Lbzg;)Ljava/util/List;
    .locals 17
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbon;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lbmw;",
            ">;",
            "Ljava/util/Map",
            "<",
            "Ljeg;",
            "Lbml;",
            ">;",
            "Lbor;",
            "Lbzg;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 71
    invoke-virtual/range {p4 .. p4}, Lbor;->e()J

    move-result-wide v4

    const-string v6, "targetDurationUs"

    invoke-static {v4, v5, v6}, Lcec;->a(JLjava/lang/CharSequence;)J

    .line 72
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface/range {p2 .. p2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    :cond_0
    const/4 v4, 0x1

    :goto_0
    const-string v5, "photos or videos non-empty"

    invoke-static {v4, v5}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 73
    const-string v4, "limits"

    const/4 v5, 0x0

    move-object/from16 v0, p5

    invoke-static {v0, v4, v5}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 80
    move-object/from16 v0, p0

    iget-object v4, v0, Lbzl;->c:Lbxw;

    if-nez v4, :cond_1

    .line 81
    invoke-virtual/range {p0 .. p0}, Lbzl;->a()Lbxw;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lbzl;->c:Lbxw;

    .line 86
    :cond_1
    new-instance v4, Lbxz;

    invoke-direct {v4}, Lbxz;-><init>()V

    .line 87
    invoke-virtual/range {p4 .. p4}, Lbor;->e()J

    move-result-wide v6

    invoke-virtual {v4, v6, v7}, Lbxz;->a(J)Lbxz;

    move-result-object v4

    const-wide/32 v6, 0x1e8480

    .line 88
    invoke-virtual {v4, v6, v7}, Lbxz;->b(J)Lbxz;

    move-result-object v4

    .line 89
    move-object/from16 v0, p4

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-virtual {v0, v1, v2}, Lbor;->a(Ljava/util/List;Ljava/util/List;)I

    move-result v5

    invoke-virtual {v4, v5}, Lbxz;->a(I)Lbxz;

    move-result-object v4

    .line 90
    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Lbxz;->b(Ljava/util/List;)Lbxz;

    move-result-object v4

    .line 91
    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Lbxz;->a(Ljava/util/List;)Lbxz;

    move-result-object v4

    .line 92
    move-object/from16 v0, p4

    invoke-virtual {v4, v0}, Lbxz;->a(Lbor;)Lbxz;

    move-result-object v4

    .line 93
    invoke-virtual {v4}, Lbxz;->a()Lbxy;

    move-result-object v4

    .line 96
    move-object/from16 v0, p0

    iget-object v5, v0, Lbzl;->c:Lbxw;

    invoke-interface {v5, v4}, Lbxw;->a(Lbxy;)Lbom;

    move-result-object v4

    invoke-virtual {v4}, Lbom;->a()Ljava/util/List;

    move-result-object v4

    .line 104
    sget-object v5, Lbmg;->a:Lbmg;

    invoke-static {v4, v5}, Lbpm;->a(Ljava/util/List;Lbmg;)Ljava/util/List;

    move-result-object v9

    .line 105
    sget-object v5, Lbmg;->c:Lbmg;

    invoke-static {v4, v5}, Lbpm;->a(Ljava/util/List;Lbmg;)Ljava/util/List;

    move-result-object v12

    .line 106
    new-instance v4, Lbpo;

    move-object/from16 v0, p3

    invoke-direct {v4, v0}, Lbpo;-><init>(Ljava/util/Map;)V

    .line 107
    invoke-static {v9, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 108
    invoke-static {v12, v4}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 111
    const/4 v4, 0x4

    const-string v5, "preferredGroupSize"

    invoke-static {v4, v5}, Lcec;->a(ILjava/lang/CharSequence;)I

    if-eqz v12, :cond_2

    invoke-interface {v12}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_2
    move-object v4, v9

    .line 115
    :goto_1
    const-wide/32 v6, 0x2dc6c0

    invoke-static {v4, v6, v7}, Lbpm;->a(Ljava/util/List;J)Ljava/util/List;

    move-result-object v4

    .line 123
    move-object/from16 v0, p3

    move-object/from16 v1, p5

    invoke-static {v4, v0, v1}, Lbpm;->a(Ljava/util/List;Ljava/util/Map;Lbzg;)Ljava/util/List;

    move-result-object v4

    .line 131
    const-wide/32 v6, 0x2dc6c0

    invoke-static {v4, v6, v7}, Lbpm;->a(Ljava/util/List;J)Ljava/util/List;

    move-result-object v4

    .line 138
    return-object v4

    .line 72
    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 111
    :cond_4
    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v13

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v14

    new-instance v8, Ljava/util/ArrayList;

    add-int v4, v13, v14

    invoke-direct {v8, v4}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v4, 0x1

    invoke-static {v4, v13}, Ljava/lang/Math;->max(II)I

    move-result v4

    const/4 v5, 0x4

    invoke-static {v14, v5}, Lcfn;->a(II)I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v7

    const/4 v10, 0x0

    const/4 v6, 0x0

    if-ge v7, v13, :cond_6

    const/4 v4, 0x1

    :goto_2
    if-lt v6, v14, :cond_5

    if-ge v10, v13, :cond_a

    :cond_5
    sub-int v11, v13, v10

    if-eqz v4, :cond_7

    const/4 v5, 0x1

    :goto_3
    add-int/2addr v5, v7

    invoke-static {v11, v5}, Lcfn;->a(II)I

    move-result v15

    const/4 v5, 0x0

    move v11, v10

    :goto_4
    if-ge v5, v15, :cond_8

    invoke-interface {v9, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v10, v11, 0x1

    add-int/lit8 v5, v5, 0x1

    move v11, v10

    goto :goto_4

    :cond_6
    const/4 v4, 0x0

    goto :goto_2

    :cond_7
    const/4 v5, 0x0

    goto :goto_3

    :cond_8
    if-lez v7, :cond_b

    sub-int v5, v14, v6

    invoke-static {v5, v7}, Lcfn;->a(II)I

    move-result v15

    const/4 v5, 0x0

    move/from16 v16, v5

    move v5, v6

    move/from16 v6, v16

    :goto_5
    if-ge v6, v15, :cond_9

    invoke-interface {v12, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-interface {v8, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v10, v5, 0x1

    add-int/lit8 v5, v6, 0x1

    move v6, v5

    move v5, v10

    goto :goto_5

    :cond_9
    add-int/lit8 v6, v7, -0x1

    :goto_6
    move v10, v11

    move v7, v6

    move v6, v5

    goto :goto_2

    :cond_a
    move-object v4, v8

    goto/16 :goto_1

    :cond_b
    move v5, v6

    move v6, v7

    goto :goto_6
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 157
    if-ne p1, p0, :cond_1

    .line 164
    :cond_0
    :goto_0
    return v0

    .line 159
    :cond_1
    instance-of v2, p1, Lbzl;

    if-eqz v2, :cond_3

    .line 160
    check-cast p1, Lbzl;

    .line 161
    iget-object v2, p1, Lbzl;->a:Ljava/util/concurrent/Future;

    iget-object v3, p0, Lbzl;->a:Ljava/util/concurrent/Future;

    if-ne v2, v3, :cond_2

    iget-object v2, p1, Lbzl;->b:Lbzc;

    iget-object v3, p0, Lbzl;->b:Lbzc;

    .line 162
    invoke-virtual {v2, v3}, Lbzc;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 164
    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 170
    iget-object v0, p0, Lbzl;->a:Ljava/util/concurrent/Future;

    iget-object v1, p0, Lbzl;->b:Lbzc;

    invoke-virtual {v1}, Lbzc;->hashCode()I

    move-result v1

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v0

    return v0
.end method
