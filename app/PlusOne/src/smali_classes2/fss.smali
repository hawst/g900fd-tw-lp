.class public final Lfss;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Landroid/graphics/Typeface;

.field private static final b:Landroid/graphics/Typeface;

.field private static final c:Landroid/graphics/Typeface;

.field private static final d:Landroid/graphics/Typeface;

.field private static final e:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 70
    const-string v0, "sans-serif"

    .line 71
    invoke-static {v0, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lfss;->a:Landroid/graphics/Typeface;

    .line 72
    const-string v0, "sans-serif-medium"

    .line 73
    invoke-static {v0, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lfss;->b:Landroid/graphics/Typeface;

    .line 74
    const-string v0, "sans-serif"

    .line 75
    invoke-static {v0, v3}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lfss;->c:Landroid/graphics/Typeface;

    .line 76
    const-string v0, "sans-serif-light"

    .line 77
    invoke-static {v0, v2}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    sput-object v0, Lfss;->d:Landroid/graphics/Typeface;

    .line 79
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "data"

    aput-object v1, v0, v2

    const-string v1, "refresh_timestamp"

    aput-object v1, v0, v3

    sput-object v0, Lfss;->e:[Ljava/lang/String;

    return-void
.end method

.method public static a(Lmnk;)F
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 302
    iget-object v1, p0, Lmnk;->c:Lmof;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmnk;->c:Lmof;

    iget-object v1, v1, Lmof;->a:Lmnu;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmnk;->c:Lmof;

    iget-object v1, v1, Lmof;->a:Lmnu;

    iget-object v1, v1, Lmnu;->a:Ljava/lang/Float;

    .line 303
    invoke-static {v1, v0}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    :cond_0
    return v0
.end method

.method public static a(Lmmq;)I
    .locals 14

    .prologue
    const/4 v2, 0x0

    .line 415
    .line 416
    iget-object v5, p0, Lmmq;->e:[Lmma;

    array-length v6, v5

    move v4, v2

    move v0, v2

    :goto_0
    if-ge v4, v6, :cond_3

    aget-object v1, v5, v4

    .line 417
    iget-object v7, v1, Lmma;->b:[Lmmp;

    array-length v8, v7

    move v3, v2

    :goto_1
    if-ge v3, v8, :cond_2

    aget-object v1, v7, v3

    .line 418
    iget-object v9, v1, Lmmp;->b:[Lmml;

    array-length v10, v9

    move v1, v2

    :goto_2
    if-ge v1, v10, :cond_1

    aget-object v11, v9, v1

    .line 419
    invoke-static {v11}, Lfss;->a(Lmml;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 420
    iget v12, v11, Lmml;->c:I

    const/4 v13, 0x1

    if-ne v12, v13, :cond_0

    iget-object v11, v11, Lmml;->f:Ljava/lang/Boolean;

    invoke-static {v11}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v11

    if-nez v11, :cond_0

    .line 423
    add-int/lit8 v0, v0, 0x1

    .line 418
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 417
    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 416
    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    .line 429
    :cond_3
    return v0
.end method

.method public static a(Landroid/content/Context;)Landroid/graphics/Point;
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/high16 v4, 0x3f000000    # 0.5f

    .line 709
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 710
    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    .line 711
    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 712
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 713
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 714
    invoke-virtual {v0, v2}, Landroid/view/Display;->getRealSize(Landroid/graphics/Point;)V

    .line 717
    const/4 v0, 0x0

    cmpl-float v0, v1, v0

    if-nez v0, :cond_0

    .line 718
    const/high16 v0, 0x3f800000    # 1.0f

    .line 720
    :goto_0
    new-instance v1, Landroid/graphics/Point;

    iget v3, v2, Landroid/graphics/Point;->x:I

    int-to-float v3, v3

    div-float/2addr v3, v0

    add-float/2addr v3, v4

    float-to-int v3, v3

    iget v2, v2, Landroid/graphics/Point;->y:I

    int-to-float v2, v2

    div-float v0, v2, v0

    add-float/2addr v0, v4

    float-to-int v0, v0

    invoke-direct {v1, v3, v0}, Landroid/graphics/Point;-><init>(II)V

    return-object v1

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static a(Lmnw;)Landroid/graphics/RectF;
    .locals 6

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 542
    if-eqz p0, :cond_1

    const/4 v0, 0x1

    .line 543
    :goto_0
    new-instance v3, Landroid/graphics/RectF;

    invoke-direct {v3}, Landroid/graphics/RectF;-><init>()V

    .line 544
    if-eqz v0, :cond_2

    iget-object v1, p0, Lmnw;->a:Ljava/lang/Float;

    :goto_1
    invoke-static {v1, v4}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v1

    iput v1, v3, Landroid/graphics/RectF;->left:F

    .line 545
    if-eqz v0, :cond_3

    iget-object v1, p0, Lmnw;->c:Ljava/lang/Float;

    :goto_2
    invoke-static {v1, v4}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v1

    iput v1, v3, Landroid/graphics/RectF;->top:F

    .line 546
    if-eqz v0, :cond_4

    iget-object v1, p0, Lmnw;->b:Ljava/lang/Float;

    :goto_3
    invoke-static {v1, v5}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v1

    iput v1, v3, Landroid/graphics/RectF;->right:F

    .line 547
    if-eqz v0, :cond_0

    iget-object v2, p0, Lmnw;->d:Ljava/lang/Float;

    :cond_0
    invoke-static {v2, v5}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    iput v0, v3, Landroid/graphics/RectF;->bottom:F

    .line 549
    return-object v3

    .line 542
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v1, v2

    .line 544
    goto :goto_1

    :cond_3
    move-object v1, v2

    .line 545
    goto :goto_2

    :cond_4
    move-object v1, v2

    .line 546
    goto :goto_3
.end method

.method public static a(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 518
    :try_start_0
    new-instance v0, Ljava/net/URL;

    new-instance v1, Ljava/net/URL;

    const-string v2, "https://plus.google.com/"

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 522
    new-instance v1, Lfsr;

    invoke-direct {v1, v0}, Lfsr;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lfsr;->a()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 520
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lnzx;Lfod;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 375
    if-nez p0, :cond_0

    .line 386
    :goto_0
    return-object v1

    .line 379
    :cond_0
    if-eqz p1, :cond_3

    .line 380
    iget-object v0, p0, Lnzx;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lfod;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 382
    :goto_1
    if-nez v0, :cond_1

    .line 383
    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {p0, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    .line 384
    if-eqz v0, :cond_2

    iget-object v2, v0, Lnzu;->b:Lnym;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lnzu;->b:Lnym;

    iget-object v2, v2, Lnym;->b:Lnyl;

    if-eqz v2, :cond_2

    iget-object v0, v0, Lnzu;->b:Lnym;

    iget-object v0, v0, Lnym;->b:Lnyl;

    iget-object v0, v0, Lnyl;->b:Ljava/lang/String;

    :cond_1
    :goto_2
    move-object v1, v0

    .line 386
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 384
    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;ILjava/lang/String;)Lmmq;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 681
    .line 682
    invoke-static {p0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 683
    if-nez v0, :cond_0

    .line 699
    :goto_0
    return-object v5

    .line 687
    :cond_0
    const-string v1, "stories"

    sget-object v2, Lfss;->e:[Ljava/lang/String;

    const-string v3, "story_id=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    aput-object p2, v4, v6

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 691
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 692
    new-instance v0, Lmmq;

    invoke-direct {v0}, Lmmq;-><init>()V

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-static {v0, v2}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lmmq;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v5, v0

    .line 697
    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 694
    :catch_0
    move-exception v0

    .line 695
    :try_start_1
    const-string v2, "StoryUtils"

    const-string v3, "Unable to deserialize story"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 697
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Loxu;",
            ">(",
            "Lmmq;",
            "Lmms;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    .line 91
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 92
    :cond_0
    const/4 v0, 0x0

    .line 138
    :goto_0
    return-object v0

    .line 94
    :cond_1
    iget-object v0, p1, Lmms;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lmms;->b:Ljava/lang/Integer;

    .line 95
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lmmq;->e:[Lmma;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    iget-object v0, p0, Lmmq;->e:[Lmma;

    iget-object v1, p1, Lmms;->b:Ljava/lang/Integer;

    .line 96
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aget-object v0, v0, v1

    move-object v1, v0

    .line 97
    :goto_1
    if-eqz v1, :cond_4

    iget-object v0, p1, Lmms;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lmms;->c:Ljava/lang/Integer;

    .line 98
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, v1, Lmma;->b:[Lmmp;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    iget-object v0, v1, Lmma;->b:[Lmmp;

    iget-object v2, p1, Lmms;->c:Ljava/lang/Integer;

    .line 99
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    aget-object v0, v0, v2

    .line 100
    :goto_2
    iget v2, p1, Lmms;->a:I

    packed-switch v2, :pswitch_data_0

    .line 138
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 96
    :cond_3
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_1

    .line 99
    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    .line 102
    :pswitch_0
    const-class v0, Lmmq;

    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p2, p0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loxu;

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 105
    :pswitch_1
    const-class v0, Lmoo;

    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lmmq;->c:Lmoo;

    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loxu;

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    goto :goto_0

    .line 108
    :pswitch_2
    const-class v0, Lmma;

    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p2, v1}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loxu;

    goto :goto_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_0

    .line 111
    :pswitch_3
    const-class v1, Lmmp;

    invoke-virtual {v1, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loxu;

    goto/16 :goto_0

    :cond_8
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 114
    :pswitch_4
    if-nez v0, :cond_9

    .line 115
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 117
    :cond_9
    iget-object v2, v0, Lmmp;->c:[Lmmc;

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v3, :cond_2

    aget-object v4, v2, v1

    .line 118
    iget-object v0, v4, Lmmc;->c:Lmmd;

    iget-object v5, p1, Lmms;->e:Lmmd;

    if-eqz v0, :cond_a

    if-nez v5, :cond_c

    :cond_a
    if-ne v0, v5, :cond_b

    const/4 v0, 0x1

    :goto_4
    if-eqz v0, :cond_11

    .line 120
    const-class v0, Lmmc;

    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {p2, v4}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loxu;

    goto/16 :goto_0

    .line 118
    :cond_b
    const/4 v0, 0x0

    goto :goto_4

    :cond_c
    iget-object v6, v0, Lmmd;->a:Ljava/lang/String;

    if-eqz v6, :cond_d

    iget-object v6, v5, Lmmd;->a:Ljava/lang/String;

    if-nez v6, :cond_f

    :cond_d
    iget-object v0, v0, Lmmd;->a:Ljava/lang/String;

    if-nez v0, :cond_e

    iget-object v0, v5, Lmmd;->a:Ljava/lang/String;

    if-nez v0, :cond_e

    const/4 v0, 0x1

    goto :goto_4

    :cond_e
    const/4 v0, 0x0

    goto :goto_4

    :cond_f
    iget-object v0, v0, Lmmd;->a:Ljava/lang/String;

    iget-object v5, v5, Lmmd;->a:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_4

    .line 120
    :cond_10
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 117
    :cond_11
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 126
    :pswitch_5
    iget-object v4, p0, Lmmq;->e:[Lmma;

    array-length v5, v4

    const/4 v0, 0x0

    move v3, v0

    :goto_5
    if-ge v3, v5, :cond_2

    aget-object v0, v4, v3

    .line 127
    iget-object v6, v0, Lmma;->b:[Lmmp;

    array-length v7, v6

    const/4 v0, 0x0

    move v2, v0

    :goto_6
    if-ge v2, v7, :cond_1b

    aget-object v0, v6, v2

    .line 128
    iget-object v8, v0, Lmmp;->b:[Lmml;

    array-length v9, v8

    const/4 v0, 0x0

    move v1, v0

    :goto_7
    if-ge v1, v9, :cond_1a

    aget-object v10, v8, v1

    .line 129
    iget-object v0, v10, Lmml;->b:Lmmm;

    iget-object v11, p1, Lmms;->d:Lmmm;

    if-eqz v0, :cond_12

    if-nez v11, :cond_14

    :cond_12
    if-ne v0, v11, :cond_13

    const/4 v0, 0x1

    :goto_8
    if-eqz v0, :cond_19

    .line 130
    const-class v0, Lmml;

    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-virtual {p2, v10}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Loxu;

    goto/16 :goto_0

    .line 129
    :cond_13
    const/4 v0, 0x0

    goto :goto_8

    :cond_14
    iget-object v12, v0, Lmmm;->a:Ljava/lang/String;

    if-eqz v12, :cond_15

    iget-object v12, v11, Lmmm;->a:Ljava/lang/String;

    if-nez v12, :cond_17

    :cond_15
    iget-object v0, v0, Lmmm;->a:Ljava/lang/String;

    if-nez v0, :cond_16

    iget-object v0, v11, Lmmm;->a:Ljava/lang/String;

    if-nez v0, :cond_16

    const/4 v0, 0x1

    goto :goto_8

    :cond_16
    const/4 v0, 0x0

    goto :goto_8

    :cond_17
    iget-object v0, v0, Lmmm;->a:Ljava/lang/String;

    iget-object v11, v11, Lmmm;->a:Ljava/lang/String;

    invoke-virtual {v0, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_8

    .line 130
    :cond_18
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 128
    :cond_19
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_7

    .line 127
    :cond_1a
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 126
    :cond_1b
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_5

    .line 100
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Loxu;Loxu;)V
    .locals 7

    .prologue
    .line 658
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getFields()[Ljava/lang/reflect/Field;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 659
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v5

    .line 660
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v0

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    if-ne v0, v6, :cond_1

    .line 661
    invoke-virtual {v4, p1}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    const/high16 v6, -0x80000000

    if-eq v0, v6, :cond_0

    .line 662
    invoke-virtual {v4, p1}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I

    move-result v0

    invoke-virtual {v5, p0, v0}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V

    .line 658
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 664
    :cond_1
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v4, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    array-length v0, v0

    if-gtz v0, :cond_3

    .line 665
    :cond_2
    invoke-virtual {v4}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->isArray()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v4, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 666
    :cond_3
    invoke-virtual {v4, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v5, p0, v0}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    .line 669
    :catch_0
    move-exception v0

    .line 670
    const-string v1, "StoryUtils"

    const-string v2, "IllegalAccessException while merging protos in StoryUtils: "

    .line 671
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 670
    :goto_2
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 676
    :cond_4
    :goto_3
    return-void

    .line 671
    :cond_5
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_2

    .line 672
    :catch_1
    move-exception v0

    .line 673
    const-string v1, "StoryUtils"

    const-string v2, "NoSuchFieldException while merging protos in StoryUtils: "

    .line 674
    invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 673
    :goto_4
    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 674
    :cond_6
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_4
.end method

.method public static a(Lmmc;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 484
    if-eqz p0, :cond_0

    iget-object v2, p0, Lmmc;->c:Lmmd;

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    .line 495
    :cond_1
    :goto_0
    return v0

    .line 487
    :cond_2
    iget v2, p0, Lmmc;->b:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lmmc;->d:Lmme;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lmmc;->d:Lmme;

    iget-object v2, v2, Lmme;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 491
    :cond_3
    iget v2, p0, Lmmc;->b:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lmmc;->e:Lmmj;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lmmc;->e:Lmmj;

    iget-object v2, v2, Lmmj;->a:Lmon;

    if-nez v2, :cond_1

    :cond_4
    move v0, v1

    .line 495
    goto :goto_0
.end method

.method public static a(Lmml;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 499
    if-eqz p0, :cond_0

    iget-object v0, p0, Lmml;->b:Lmmm;

    if-nez v0, :cond_1

    :cond_0
    move v0, v2

    .line 512
    :goto_0
    return v0

    .line 502
    :cond_1
    iget v0, p0, Lmml;->c:I

    if-ne v0, v1, :cond_4

    .line 503
    iget-object v0, p0, Lmml;->d:Lnzx;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lmml;->d:Lnzx;

    sget-object v3, Lnzu;->a:Loxr;

    .line 504
    invoke-virtual {v0, v3}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    .line 505
    :goto_1
    if-eqz v0, :cond_3

    iget-object v3, v0, Lnzu;->b:Lnym;

    if-eqz v3, :cond_3

    iget-object v3, v0, Lnzu;->b:Lnym;

    iget-object v3, v3, Lnym;->b:Lnyl;

    if-eqz v3, :cond_3

    iget-object v0, v0, Lnzu;->b:Lnym;

    iget-object v0, v0, Lnym;->b:Lnyl;

    iget-object v0, v0, Lnyl;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    move v0, v1

    goto :goto_0

    .line 504
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move v0, v2

    .line 505
    goto :goto_0

    .line 508
    :cond_4
    iget v0, p0, Lmml;->c:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_5

    iget-object v0, p0, Lmml;->e:Lmmn;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lmml;->e:Lmmn;

    iget-object v0, v0, Lmmn;->a:Lmmo;

    if-eqz v0, :cond_5

    move v0, v1

    .line 510
    goto :goto_0

    :cond_5
    move v0, v2

    .line 512
    goto :goto_0
.end method

.method public static a(Lmmq;Lmmq;[Lmmr;)Z
    .locals 15

    .prologue
    .line 564
    if-nez p0, :cond_0

    .line 565
    const/4 v1, 0x0

    .line 650
    :goto_0
    return v1

    .line 568
    :cond_0
    move-object/from16 v0, p1

    iget-object v1, v0, Lmmq;->f:Lmoi;

    iput-object v1, p0, Lmmq;->f:Lmoi;

    .line 569
    iget-object v1, p0, Lmmq;->a:Lmok;

    move-object/from16 v0, p1

    iget-object v2, v0, Lmmq;->a:Lmok;

    iget-object v2, v2, Lmok;->b:Lmol;

    iput-object v2, v1, Lmok;->b:Lmol;

    .line 570
    move-object/from16 v0, p2

    array-length v6, v0

    const/4 v1, 0x0

    move v5, v1

    :goto_1
    if-ge v5, v6, :cond_b

    aget-object v2, p2, v5

    .line 572
    iget v1, v2, Lmmr;->b:I

    sparse-switch v1, :sswitch_data_0

    new-instance v1, Ljava/lang/RuntimeException;

    iget v2, v2, Lmmr;->b:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x33

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Unable to apply unrecognised edit type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_0
    iget-object v1, p0, Lmmq;->c:Lmoo;

    iget-object v2, v2, Lmmr;->c:Ljava/lang/String;

    iput-object v2, v1, Lmoo;->a:Ljava/lang/String;

    .line 570
    :cond_1
    :goto_2
    :sswitch_1
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_1

    .line 572
    :sswitch_2
    iget-object v1, v2, Lmmr;->f:Lmms;

    const-class v3, Lmml;

    invoke-static {p0, v1, v3}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v1

    check-cast v1, Lmml;

    iget-object v7, v1, Lmml;->e:Lmmn;

    iget-object v1, v2, Lmmr;->f:Lmms;

    const-class v3, Lmml;

    move-object/from16 v0, p1

    invoke-static {v0, v1, v3}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v1

    check-cast v1, Lmml;

    if-nez v1, :cond_2

    const/4 v1, 0x0

    :goto_3
    if-eqz v1, :cond_3

    iget-object v3, v1, Lmmn;->a:Lmmo;

    if-eqz v3, :cond_3

    iget-object v1, v1, Lmmn;->a:Lmmo;

    :goto_4
    iget-object v8, v7, Lmmn;->b:[Lmmo;

    const/4 v3, 0x1

    const/4 v2, 0x0

    :goto_5
    array-length v4, v8

    if-ge v2, v4, :cond_21

    aget-object v4, v8, v2

    invoke-static {v4, v1}, Lfsp;->a(Lmmo;Lmmo;)Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v2, 0x0

    :goto_6
    if-eqz v2, :cond_7

    array-length v2, v8

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [Lmmo;

    iput-object v2, v7, Lmmn;->b:[Lmmo;

    iget-object v2, v7, Lmmn;->b:[Lmmo;

    const/4 v3, 0x0

    iget-object v4, v7, Lmmn;->a:Lmmo;

    aput-object v4, v2, v3

    const/4 v2, 0x0

    :goto_7
    array-length v3, v8

    if-ge v2, v3, :cond_5

    iget-object v3, v7, Lmmn;->b:[Lmmo;

    add-int/lit8 v4, v2, 0x1

    aget-object v9, v8, v2

    aput-object v9, v3, v4

    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_2
    iget-object v1, v1, Lmml;->e:Lmmn;

    goto :goto_3

    :cond_3
    iget-object v1, v2, Lmmr;->d:Lmmo;

    goto :goto_4

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_5
    iput-object v1, v7, Lmmn;->a:Lmmo;

    :cond_6
    iget-object v1, v7, Lmmn;->b:[Lmmo;

    if-eqz v1, :cond_1

    iget-object v1, v7, Lmmn;->b:[Lmmo;

    array-length v1, v1

    const/16 v2, 0x9

    if-le v1, v2, :cond_1

    const/16 v1, 0x9

    new-array v2, v1, [Lmmo;

    const/4 v1, 0x0

    :goto_8
    const/16 v3, 0x9

    if-ge v1, v3, :cond_a

    iget-object v3, v7, Lmmn;->b:[Lmmo;

    aget-object v3, v3, v1

    aput-object v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_7
    array-length v2, v8

    new-array v2, v2, [Lmmo;

    iput-object v2, v7, Lmmn;->b:[Lmmo;

    iget-object v2, v7, Lmmn;->b:[Lmmo;

    const/4 v3, 0x0

    iget-object v4, v7, Lmmn;->a:Lmmo;

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v2, 0x1

    :goto_9
    array-length v4, v8

    if-ge v3, v4, :cond_6

    aget-object v4, v8, v3

    invoke-static {v4, v1}, Lfsp;->a(Lmmo;Lmmo;)Z

    move-result v4

    if-eqz v4, :cond_8

    aget-object v4, v8, v3

    iput-object v4, v7, Lmmn;->a:Lmmo;

    :goto_a
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    :cond_8
    array-length v4, v8

    if-ge v2, v4, :cond_9

    iget-object v9, v7, Lmmn;->b:[Lmmo;

    add-int/lit8 v4, v2, 0x1

    aget-object v10, v8, v3

    aput-object v10, v9, v2

    move v2, v4

    goto :goto_a

    :cond_9
    const-string v4, "StoryUtils"

    const-string v9, "Server side logic error when rearranging the place locations"

    invoke-static {v4, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    :cond_a
    iput-object v2, v7, Lmmn;->b:[Lmmo;

    goto/16 :goto_2

    .line 574
    :cond_b
    move-object/from16 v0, p1

    iget-object v1, v0, Lmmq;->c:Lmoo;

    iput-object v1, p0, Lmmq;->c:Lmoo;

    .line 575
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 576
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 577
    iget-object v4, p0, Lmmq;->e:[Lmma;

    array-length v7, v4

    const/4 v1, 0x0

    move v3, v1

    :goto_b
    if-ge v3, v7, :cond_11

    aget-object v1, v4, v3

    .line 578
    iget-object v8, v1, Lmma;->b:[Lmmp;

    array-length v9, v8

    const/4 v1, 0x0

    move v2, v1

    :goto_c
    if-ge v2, v9, :cond_10

    aget-object v10, v8, v2

    .line 579
    iget-object v11, v10, Lmmp;->b:[Lmml;

    array-length v12, v11

    const/4 v1, 0x0

    :goto_d
    if-ge v1, v12, :cond_d

    aget-object v13, v11, v1

    .line 580
    iget-object v14, v13, Lmml;->b:Lmmm;

    if-eqz v14, :cond_c

    .line 581
    iget-object v14, v13, Lmml;->b:Lmmm;

    iget-object v14, v14, Lmmm;->a:Ljava/lang/String;

    invoke-interface {v6, v14, v13}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 579
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_d

    .line 584
    :cond_d
    iget-object v10, v10, Lmmp;->c:[Lmmc;

    array-length v11, v10

    const/4 v1, 0x0

    :goto_e
    if-ge v1, v11, :cond_f

    aget-object v12, v10, v1

    .line 585
    iget-object v13, v12, Lmmc;->c:Lmmd;

    if-eqz v13, :cond_e

    .line 586
    iget-object v13, v12, Lmmc;->c:Lmmd;

    iget-object v13, v13, Lmmd;->a:Ljava/lang/String;

    invoke-interface {v5, v13, v12}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 584
    :cond_e
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 578
    :cond_f
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_c

    .line 577
    :cond_10
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_b

    .line 591
    :cond_11
    move-object/from16 v0, p1

    iget-object v1, v0, Lmmq;->e:[Lmma;

    iput-object v1, p0, Lmmq;->e:[Lmma;

    .line 592
    iget-object v7, p0, Lmmq;->e:[Lmma;

    array-length v8, v7

    const/4 v1, 0x0

    move v4, v1

    :goto_f
    if-ge v4, v8, :cond_1a

    aget-object v1, v7, v4

    .line 593
    iget-object v9, v1, Lmma;->b:[Lmmp;

    array-length v10, v9

    const/4 v1, 0x0

    move v3, v1

    :goto_10
    if-ge v3, v10, :cond_19

    aget-object v11, v9, v3

    .line 594
    const/4 v1, 0x0

    move v2, v1

    :goto_11
    iget-object v1, v11, Lmmp;->b:[Lmml;

    array-length v1, v1

    if-ge v2, v1, :cond_15

    .line 595
    iget-object v1, v11, Lmmp;->b:[Lmml;

    aget-object v12, v1, v2

    .line 596
    iget-object v1, v12, Lmml;->b:Lmmm;

    if-nez v1, :cond_12

    .line 597
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 600
    :cond_12
    iget-object v1, v12, Lmml;->b:Lmmm;

    iget-object v1, v1, Lmmm;->a:Ljava/lang/String;

    invoke-interface {v6, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmml;

    .line 601
    if-nez v1, :cond_13

    .line 602
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 606
    :cond_13
    iget v13, v1, Lmml;->c:I

    const/4 v14, 0x2

    if-eq v13, v14, :cond_14

    .line 607
    invoke-static {v1, v12}, Lfss;->a(Loxu;Loxu;)V

    .line 609
    :cond_14
    iget-object v12, v11, Lmmp;->b:[Lmml;

    aput-object v1, v12, v2

    .line 594
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_11

    .line 611
    :cond_15
    const/4 v1, 0x0

    move v2, v1

    :goto_12
    iget-object v1, v11, Lmmp;->c:[Lmmc;

    array-length v1, v1

    if-ge v2, v1, :cond_18

    .line 612
    iget-object v1, v11, Lmmp;->c:[Lmmc;

    aget-object v12, v1, v2

    .line 613
    iget-object v1, v12, Lmmc;->c:Lmmd;

    if-nez v1, :cond_16

    .line 614
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 617
    :cond_16
    iget-object v1, v12, Lmmc;->c:Lmmd;

    iget-object v1, v1, Lmmd;->a:Ljava/lang/String;

    .line 618
    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lmmc;

    .line 619
    if-nez v1, :cond_17

    .line 620
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 623
    :cond_17
    invoke-static {v1, v12}, Lfss;->a(Loxu;Loxu;)V

    .line 624
    iget-object v12, v11, Lmmp;->c:[Lmmc;

    aput-object v1, v12, v2

    .line 611
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_12

    .line 593
    :cond_18
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_10

    .line 592
    :cond_19
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_f

    .line 628
    :cond_1a
    move-object/from16 v0, p1

    iget-object v1, v0, Lmmq;->h:[Lmmt;

    array-length v1, v1

    if-lez v1, :cond_1b

    .line 629
    move-object/from16 v0, p1

    iget-object v1, v0, Lmmq;->h:[Lmmt;

    iput-object v1, p0, Lmmq;->h:[Lmmt;

    .line 631
    :cond_1b
    iget-object v1, p0, Lmmq;->g:Lmlz;

    if-eqz v1, :cond_20

    .line 634
    move-object/from16 v0, p1

    iget-object v1, v0, Lmmq;->g:Lmlz;

    if-eqz v1, :cond_1c

    iget-object v1, p0, Lmmq;->g:Lmlz;

    iget-object v1, v1, Lmlz;->a:Ljava/lang/String;

    if-eqz v1, :cond_1d

    iget-object v1, p0, Lmmq;->g:Lmlz;

    iget-object v1, v1, Lmlz;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v2, v0, Lmmq;->g:Lmlz;

    iget-object v2, v2, Lmlz;->a:Ljava/lang/String;

    .line 635
    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1d

    .line 636
    :cond_1c
    iget-object v1, p0, Lmmq;->g:Lmlz;

    const/4 v2, 0x0

    iput-object v2, v1, Lmlz;->b:Logr;

    .line 639
    :cond_1d
    move-object/from16 v0, p1

    iget-object v1, v0, Lmmq;->g:Lmlz;

    if-eqz v1, :cond_1e

    move-object/from16 v0, p1

    iget-object v1, v0, Lmmq;->g:Lmlz;

    iget-object v1, v1, Lmlz;->c:Loya;

    if-eqz v1, :cond_1e

    move-object/from16 v0, p1

    iget-object v1, v0, Lmmq;->g:Lmlz;

    if-eqz v1, :cond_1f

    move-object/from16 v0, p1

    iget-object v1, v0, Lmmq;->g:Lmlz;

    iget-object v1, v1, Lmlz;->a:Ljava/lang/String;

    if-eqz v1, :cond_1f

    .line 642
    :cond_1e
    iget-object v1, p0, Lmmq;->g:Lmlz;

    const/4 v2, 0x0

    iput-object v2, v1, Lmlz;->c:Loya;

    .line 645
    :cond_1f
    move-object/from16 v0, p1

    iget-object v1, v0, Lmmq;->g:Lmlz;

    if-eqz v1, :cond_20

    move-object/from16 v0, p1

    iget-object v1, v0, Lmmq;->g:Lmlz;

    iget-object v1, v1, Lmlz;->c:Loya;

    if-eqz v1, :cond_20

    .line 646
    iget-object v1, p0, Lmmq;->g:Lmlz;

    move-object/from16 v0, p1

    iget-object v2, v0, Lmmq;->g:Lmlz;

    iget-object v2, v2, Lmlz;->c:Loya;

    iput-object v2, v1, Lmlz;->c:Loya;

    .line 647
    iget-object v1, p0, Lmmq;->g:Lmlz;

    const/4 v2, 0x0

    iput-object v2, v1, Lmlz;->b:Logr;

    .line 650
    :cond_20
    const/4 v1, 0x1

    goto/16 :goto_0

    :cond_21
    move v2, v3

    goto/16 :goto_6

    .line 572
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_1
        0x5 -> :sswitch_0
        0x9 -> :sswitch_2
        0xe -> :sswitch_1
    .end sparse-switch
.end method

.method public static a(Landroid/graphics/Point;)[Landroid/graphics/Point;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 727
    if-nez p0, :cond_0

    .line 728
    new-array v0, v3, [Landroid/graphics/Point;

    .line 733
    :goto_0
    return-object v0

    .line 732
    :cond_0
    new-instance v1, Landroid/graphics/Point;

    iget v0, p0, Landroid/graphics/Point;->y:I

    iget v2, p0, Landroid/graphics/Point;->x:I

    invoke-direct {v1, v0, v2}, Landroid/graphics/Point;-><init>(II)V

    .line 733
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/graphics/Point;

    aput-object p0, v0, v3

    const/4 v2, 0x1

    aput-object v1, v0, v2

    goto :goto_0
.end method

.method public static a([Lmnf;)[Lhng;
    .locals 13

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v8, 0x0

    const/4 v12, 0x0

    .line 453
    if-nez p0, :cond_0

    .line 454
    new-array v0, v1, [Lhng;

    .line 460
    :goto_0
    return-object v0

    .line 456
    :cond_0
    array-length v0, p0

    new-array v10, v0, [Lhng;

    move v0, v1

    .line 457
    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_e

    .line 458
    aget-object v11, p0, v0

    if-nez v11, :cond_1

    new-instance v2, Lhng;

    invoke-direct {v2}, Lhng;-><init>()V

    :goto_2
    aput-object v2, v10, v0

    .line 457
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 458
    :cond_1
    iget-object v2, v11, Lmnf;->b:Lmnu;

    if-eqz v2, :cond_2

    move v2, v3

    :goto_3
    iget-object v4, v11, Lmnf;->c:Lmnu;

    if-eqz v4, :cond_3

    move v4, v3

    :goto_4
    iget-object v5, v11, Lmnf;->d:Lmnu;

    if-eqz v5, :cond_4

    move v5, v3

    :goto_5
    iget-object v6, v11, Lmnf;->e:Lmnu;

    if-eqz v6, :cond_5

    move v6, v3

    :goto_6
    new-instance v9, Lhng;

    invoke-direct {v9}, Lhng;-><init>()V

    if-eqz v2, :cond_6

    iget-object v7, v11, Lmnf;->b:Lmnu;

    iget-object v7, v7, Lmnu;->a:Ljava/lang/Float;

    :goto_7
    invoke-static {v7, v12}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v7

    iput v7, v9, Lhng;->a:F

    if-eqz v2, :cond_7

    iget-object v2, v11, Lmnf;->b:Lmnu;

    iget-object v2, v2, Lmnu;->b:Ljava/lang/Float;

    :goto_8
    invoke-static {v2, v12}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v2

    iput v2, v9, Lhng;->b:F

    if-eqz v4, :cond_8

    iget-object v2, v11, Lmnf;->c:Lmnu;

    iget-object v2, v2, Lmnu;->a:Ljava/lang/Float;

    :goto_9
    invoke-static {v2, v12}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v2

    iput v2, v9, Lhng;->c:F

    if-eqz v4, :cond_9

    iget-object v2, v11, Lmnf;->c:Lmnu;

    iget-object v2, v2, Lmnu;->b:Ljava/lang/Float;

    :goto_a
    invoke-static {v2, v12}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v2

    iput v2, v9, Lhng;->d:F

    if-eqz v5, :cond_a

    iget-object v2, v11, Lmnf;->d:Lmnu;

    iget-object v2, v2, Lmnu;->a:Ljava/lang/Float;

    :goto_b
    invoke-static {v2, v12}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v2

    iput v2, v9, Lhng;->e:F

    if-eqz v5, :cond_b

    iget-object v2, v11, Lmnf;->d:Lmnu;

    iget-object v2, v2, Lmnu;->b:Ljava/lang/Float;

    :goto_c
    invoke-static {v2, v12}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v2

    iput v2, v9, Lhng;->f:F

    if-eqz v6, :cond_c

    iget-object v2, v11, Lmnf;->e:Lmnu;

    iget-object v2, v2, Lmnu;->a:Ljava/lang/Float;

    :goto_d
    invoke-static {v2, v12}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v2

    iput v2, v9, Lhng;->g:F

    if-eqz v6, :cond_d

    iget-object v2, v11, Lmnf;->e:Lmnu;

    iget-object v2, v2, Lmnu;->b:Ljava/lang/Float;

    :goto_e
    invoke-static {v2, v12}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v2

    iput v2, v9, Lhng;->h:F

    move-object v2, v9

    goto :goto_2

    :cond_2
    move v2, v1

    goto :goto_3

    :cond_3
    move v4, v1

    goto :goto_4

    :cond_4
    move v5, v1

    goto :goto_5

    :cond_5
    move v6, v1

    goto :goto_6

    :cond_6
    move-object v7, v8

    goto :goto_7

    :cond_7
    move-object v2, v8

    goto :goto_8

    :cond_8
    move-object v2, v8

    goto :goto_9

    :cond_9
    move-object v2, v8

    goto :goto_a

    :cond_a
    move-object v2, v8

    goto :goto_b

    :cond_b
    move-object v2, v8

    goto :goto_c

    :cond_c
    move-object v2, v8

    goto :goto_d

    :cond_d
    move-object v2, v8

    goto :goto_e

    :cond_e
    move-object v0, v10

    .line 460
    goto/16 :goto_0
.end method

.method public static b(Lmnk;)F
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 307
    iget-object v1, p0, Lmnk;->c:Lmof;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmnk;->c:Lmof;

    iget-object v1, v1, Lmof;->a:Lmnu;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmnk;->c:Lmof;

    iget-object v1, v1, Lmof;->a:Lmnu;

    iget-object v1, v1, Lmnu;->b:Ljava/lang/Float;

    .line 308
    invoke-static {v1, v0}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    :cond_0
    return v0
.end method

.method public static b(Lmmq;Lmms;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(",
            "Lmmq;",
            "Lmms;",
            "Ljava/lang/Class",
            "<TT;>;)TT;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 143
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 223
    :cond_0
    :goto_0
    return-object v1

    .line 146
    :cond_1
    iget v0, p1, Lmms;->f:I

    packed-switch v0, :pswitch_data_0

    .line 222
    :pswitch_0
    iget v0, p1, Lmms;->f:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x23

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unrecognized attribute: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 148
    :pswitch_1
    const-class v0, Lmmc;

    .line 149
    invoke-static {p0, p1, v0}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmmc;

    .line 150
    if-eqz v0, :cond_0

    iget-object v2, v0, Lmmc;->d:Lmme;

    if-eqz v2, :cond_0

    const-class v2, Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Lmmc;->d:Lmme;

    iget-object v0, v0, Lmme;->b:Ljava/lang/String;

    .line 151
    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 154
    :pswitch_2
    const-class v0, Lmmc;

    .line 155
    invoke-static {p0, p1, v0}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmmc;

    .line 156
    if-eqz v0, :cond_0

    iget-object v2, v0, Lmmc;->e:Lmmj;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lmmc;->e:Lmmj;

    iget-object v2, v2, Lmmj;->b:Lmme;

    if-eqz v2, :cond_0

    const-class v2, Ljava/lang/String;

    .line 157
    invoke-virtual {v2, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Lmmc;->e:Lmmj;

    iget-object v0, v0, Lmmj;->b:Lmme;

    iget-object v0, v0, Lmme;->b:Ljava/lang/String;

    .line 158
    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 161
    :pswitch_3
    const-class v0, Lmmc;

    .line 162
    invoke-static {p0, p1, v0}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmmc;

    .line 163
    if-eqz v0, :cond_0

    iget-object v2, v0, Lmmc;->e:Lmmj;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lmmc;->e:Lmmj;

    iget-object v2, v2, Lmmj;->c:Lmme;

    if-eqz v2, :cond_0

    const-class v2, Ljava/lang/String;

    .line 164
    invoke-virtual {v2, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Lmmc;->e:Lmmj;

    iget-object v0, v0, Lmmj;->c:Lmme;

    iget-object v0, v0, Lmme;->b:Ljava/lang/String;

    .line 165
    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/16 :goto_0

    .line 168
    :pswitch_4
    const-class v0, Lmml;

    invoke-static {p0, p1, v0}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmml;

    .line 169
    if-eqz v0, :cond_0

    const-class v2, Lnzx;

    invoke-virtual {v2, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Lmml;->d:Lnzx;

    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/16 :goto_0

    .line 172
    :pswitch_5
    const-class v0, Lmoo;

    invoke-static {p0, p1, v0}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmoo;

    .line 173
    if-eqz v0, :cond_0

    const-class v2, Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Lmoo;->a:Ljava/lang/String;

    .line 174
    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/16 :goto_0

    .line 177
    :pswitch_6
    const-class v0, Lmmc;

    .line 178
    invoke-static {p0, p1, v0}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmmc;

    .line 179
    if-eqz v0, :cond_0

    iget-object v2, v0, Lmmc;->d:Lmme;

    if-eqz v2, :cond_0

    const-class v2, Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Lmmc;->d:Lmme;

    iget-object v0, v0, Lmme;->a:Ljava/lang/String;

    .line 180
    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/16 :goto_0

    .line 183
    :pswitch_7
    const-class v0, Lmmc;

    .line 184
    invoke-static {p0, p1, v0}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmmc;

    .line 185
    if-eqz v0, :cond_0

    iget-object v2, v0, Lmmc;->e:Lmmj;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lmmc;->e:Lmmj;

    iget-object v2, v2, Lmmj;->b:Lmme;

    if-eqz v2, :cond_0

    const-class v2, Ljava/lang/String;

    .line 186
    invoke-virtual {v2, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Lmmc;->e:Lmmj;

    iget-object v0, v0, Lmmj;->b:Lmme;

    iget-object v0, v0, Lmme;->a:Ljava/lang/String;

    .line 187
    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/16 :goto_0

    .line 190
    :pswitch_8
    const-class v0, Lmmc;

    .line 191
    invoke-static {p0, p1, v0}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmmc;

    .line 192
    if-eqz v0, :cond_0

    iget-object v2, v0, Lmmc;->e:Lmmj;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lmmc;->e:Lmmj;

    iget-object v2, v2, Lmmj;->c:Lmme;

    if-eqz v2, :cond_0

    const-class v2, Ljava/lang/String;

    .line 193
    invoke-virtual {v2, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Lmmc;->e:Lmmj;

    iget-object v0, v0, Lmmj;->c:Lmme;

    iget-object v0, v0, Lmme;->a:Ljava/lang/String;

    .line 194
    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/16 :goto_0

    .line 197
    :pswitch_9
    const-class v0, Lmml;

    invoke-static {p0, p1, v0}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmml;

    .line 198
    if-eqz v0, :cond_2

    iget-object v2, v0, Lmml;->e:Lmmn;

    if-eqz v2, :cond_2

    iget-object v0, v0, Lmml;->e:Lmmn;

    iget-object v0, v0, Lmmn;->a:Lmmo;

    .line 200
    :goto_1
    const-class v2, Ljava/lang/String;

    invoke-virtual {v2, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    iget-object v2, v0, Lmmo;->d:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v0, v0, Lmmo;->d:Ljava/lang/String;

    .line 202
    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/16 :goto_0

    :cond_2
    move-object v0, v1

    .line 198
    goto :goto_1

    .line 202
    :cond_3
    iget-object v2, v0, Lmmo;->b:Lofq;

    if-eqz v2, :cond_0

    iget-object v0, v0, Lmmo;->b:Lofq;

    iget-object v0, v0, Lofq;->e:Ljava/lang/String;

    .line 204
    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/16 :goto_0

    .line 208
    :pswitch_a
    const-class v0, Lmml;

    invoke-static {p0, p1, v0}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmml;

    .line 209
    if-eqz v0, :cond_0

    iget-object v2, v0, Lmml;->e:Lmmn;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lmml;->e:Lmmn;

    iget-object v2, v2, Lmmn;->a:Lmmo;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lmml;->e:Lmmn;

    iget-object v2, v2, Lmmn;->a:Lmmo;

    iget-object v2, v2, Lmmo;->b:Lofq;

    if-eqz v2, :cond_0

    const-class v2, Ljava/lang/String;

    .line 210
    invoke-virtual {v2, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Lmml;->e:Lmmn;

    iget-object v0, v0, Lmmn;->a:Lmmo;

    iget-object v0, v0, Lmmo;->b:Lofq;

    iget-object v0, v0, Lofq;->f:Ljava/lang/String;

    .line 211
    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/16 :goto_0

    .line 214
    :pswitch_b
    const-class v0, Lmml;

    invoke-static {p0, p1, v0}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmml;

    .line 215
    if-eqz v0, :cond_0

    iget-object v2, v0, Lmml;->e:Lmmn;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lmml;->e:Lmmn;

    iget-object v2, v2, Lmmn;->a:Lmmo;

    if-eqz v2, :cond_0

    const-class v2, Lodo;

    .line 216
    invoke-virtual {v2, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, v0, Lmml;->e:Lmmn;

    iget-object v0, v0, Lmmn;->a:Lmmo;

    iget-object v0, v0, Lmmo;->e:Lodo;

    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/16 :goto_0

    .line 219
    :pswitch_c
    const-class v0, Lodo;

    invoke-virtual {v0, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmmq;->b:Lodo;

    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    goto/16 :goto_0

    .line 146
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_1
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_7
        :pswitch_c
        :pswitch_b
        :pswitch_3
        :pswitch_8
    .end packed-switch
.end method

.method public static b(Landroid/net/Uri;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 534
    :try_start_0
    new-instance v0, Ljava/net/URL;

    new-instance v1, Ljava/net/URL;

    const-string v2, "https://plus.google.com/"

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 538
    new-instance v1, Lfsr;

    invoke-direct {v1, v0}, Lfsr;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lfsr;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 536
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lnzx;Lfod;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 396
    if-nez p0, :cond_0

    .line 406
    :goto_0
    return-object v1

    .line 400
    :cond_0
    if-eqz p1, :cond_3

    .line 401
    iget-object v0, p0, Lnzx;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Lfod;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 403
    :goto_1
    if-nez v0, :cond_1

    .line 404
    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {p0, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    .line 405
    if-eqz v0, :cond_2

    iget-object v2, v0, Lnzu;->b:Lnym;

    if-eqz v2, :cond_2

    iget-object v0, v0, Lnzu;->b:Lnym;

    iget-object v0, v0, Lnym;->d:Ljava/lang/String;

    :cond_1
    :goto_2
    move-object v1, v0

    .line 406
    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 405
    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_1
.end method

.method public static c(Lmnk;)F
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 312
    iget-object v1, p0, Lmnk;->c:Lmof;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmnk;->c:Lmof;

    iget-object v1, v1, Lmof;->b:Lmoa;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmnk;->c:Lmof;

    iget-object v1, v1, Lmof;->b:Lmoa;

    iget-object v1, v1, Lmoa;->a:Ljava/lang/Float;

    .line 313
    invoke-static {v1, v0}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    :cond_0
    return v0
.end method

.method public static d(Lmnk;)F
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 317
    iget-object v1, p0, Lmnk;->c:Lmof;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmnk;->c:Lmof;

    iget-object v1, v1, Lmof;->b:Lmoa;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmnk;->c:Lmof;

    iget-object v1, v1, Lmof;->b:Lmoa;

    iget-object v1, v1, Lmoa;->b:Ljava/lang/Float;

    .line 318
    invoke-static {v1, v0}, Llsl;->a(Ljava/lang/Float;F)F

    move-result v0

    :cond_0
    return v0
.end method

.method public static e(Lmnk;)I
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->g:Lmne;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->g:Lmne;

    iget-object v0, v0, Lmne;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->g:Lmne;

    iget-object v0, v0, Lmne;->a:Ljava/lang/Integer;

    .line 324
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static f(Lmnk;)I
    .locals 1

    .prologue
    .line 328
    iget-object v0, p0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->h:Lmne;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->h:Lmne;

    iget-object v0, v0, Lmne;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->h:Lmne;

    iget-object v0, v0, Lmne;->a:Ljava/lang/Integer;

    .line 330
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Lmnk;)Landroid/graphics/Typeface;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 334
    iget-object v0, p0, Lmnk;->c:Lmof;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lmnk;->c:Lmof;

    iget-object v0, v0, Lmof;->j:Ljava/lang/String;

    .line 336
    :goto_0
    iget-object v1, p0, Lmnk;->c:Lmof;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lmnk;->c:Lmof;

    iget v1, v1, Lmof;->k:I

    const/high16 v4, -0x80000000

    if-eq v1, v4, :cond_1

    iget-object v1, p0, Lmnk;->c:Lmof;

    iget v1, v1, Lmof;->k:I

    .line 339
    :goto_1
    const-string v4, "Roboto"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 340
    if-ne v1, v3, :cond_2

    sget-object v0, Lfss;->c:Landroid/graphics/Typeface;

    .line 346
    :goto_2
    return-object v0

    .line 334
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    move v1, v2

    .line 336
    goto :goto_1

    .line 340
    :cond_2
    sget-object v0, Lfss;->a:Landroid/graphics/Typeface;

    goto :goto_2

    .line 341
    :cond_3
    const-string v4, "Roboto Light"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 342
    sget-object v0, Lfss;->d:Landroid/graphics/Typeface;

    goto :goto_2

    .line 343
    :cond_4
    const-string v4, "Roboto Medium"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 344
    sget-object v0, Lfss;->b:Landroid/graphics/Typeface;

    goto :goto_2

    .line 346
    :cond_5
    if-ne v1, v3, :cond_6

    move v2, v3

    :cond_6
    invoke-static {v2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_2
.end method

.method public static h(Lmnk;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 355
    iget-object v1, p0, Lmnk;->c:Lmof;

    iget v1, v1, Lmof;->m:I

    sparse-switch v1, :sswitch_data_0

    .line 365
    iget-object v1, p0, Lmnk;->c:Lmof;

    iget v1, v1, Lmof;->m:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x28

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unrecognised text alignment: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 366
    :goto_0
    :sswitch_0
    return v0

    .line 357
    :sswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 359
    :sswitch_2
    const/16 v0, 0x11

    goto :goto_0

    .line 361
    :sswitch_3
    const/4 v0, 0x5

    goto :goto_0

    .line 355
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
    .end sparse-switch
.end method
