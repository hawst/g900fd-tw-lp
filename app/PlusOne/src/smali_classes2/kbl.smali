.class public abstract Lkbl;
.super Ljxw;
.source "PG"


# static fields
.field private static d:Z

.field private static e:I

.field private static f:I

.field private static g:I

.field private static h:I

.field static i:I

.field static j:I

.field static k:I

.field private static u:I

.field private static v:I

.field private static w:Ljava/text/NumberFormat;

.field private static x:[Landroid/text/InputFilter;


# instance fields
.field private A:J

.field private B:Z

.field private C:Landroid/view/View$OnClickListener;

.field private D:Ljya;

.field private E:Ljxv;

.field private F:Lhee;

.field private G:Z

.field l:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field m:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field n:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field o:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field p:Landroid/widget/ImageView;

.field q:I

.field r:I

.field s:Lkbg;

.field t:F

.field private y:Z

.field private z:Lkax;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0, p1}, Ljxw;-><init>(Landroid/content/Context;)V

    .line 97
    return-void
.end method

.method static synthetic a(Lkbl;)V
    .locals 4

    .prologue
    .line 39
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    invoke-virtual {p0}, Lkbl;->e()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    new-instance v1, Lkbn;

    invoke-direct {v1, p0}, Lkbn;-><init>(Lkbl;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    return-void
.end method

.method static synthetic a(Lkbl;Z)Z
    .locals 0

    .prologue
    .line 39
    iput-boolean p1, p0, Lkbl;->B:Z

    return p1
.end method

.method static synthetic b(Lkbl;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lkbl;->c()V

    return-void
.end method

.method static synthetic b(Lkbl;Z)Z
    .locals 0

    .prologue
    .line 39
    iput-boolean p1, p0, Lkbl;->G:Z

    return p1
.end method

.method private c()V
    .locals 5

    .prologue
    .line 553
    invoke-virtual {p0}, Lkbl;->i()Ljava/util/ArrayList;

    move-result-object v0

    .line 554
    iget-object v1, p0, Lkbl;->s:Lkbg;

    iget-object v2, p0, Lkbl;->m:Ljava/util/ArrayList;

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2, v3, v4}, Lkbg;->a(Ljava/util/ArrayList;FF)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 556
    new-instance v1, Landroid/animation/AnimatorSet;

    invoke-direct {v1}, Landroid/animation/AnimatorSet;-><init>()V

    .line 557
    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    .line 558
    const-wide/16 v2, 0x12c

    invoke-virtual {v1, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    .line 559
    new-instance v0, Lkbp;

    invoke-direct {v0, p0}, Lkbp;-><init>(Lkbl;)V

    invoke-virtual {v1, v0}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 566
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    .line 567
    return-void
.end method

.method private n()Lhee;
    .locals 2

    .prologue
    .line 737
    iget-object v0, p0, Lkbl;->F:Lhee;

    if-nez v0, :cond_0

    .line 738
    invoke-virtual {p0}, Lkbl;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lhee;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lkbl;->F:Lhee;

    .line 740
    :cond_0
    iget-object v0, p0, Lkbl;->F:Lhee;

    return-object v0
.end method

.method private o()Z
    .locals 2

    .prologue
    .line 744
    invoke-direct {p0}, Lkbl;->n()Lhee;

    move-result-object v0

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lkbl;->b:Lkzw;

    .line 745
    invoke-virtual {v1}, Lkzw;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private p()Z
    .locals 4

    .prologue
    .line 749
    iget-wide v0, p0, Lkbl;->A:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    invoke-direct {p0}, Lkbl;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lkzw;)Ljxw;
    .locals 2

    .prologue
    .line 158
    invoke-super {p0, p1}, Ljxw;->a(Lkzw;)Ljxw;

    .line 159
    invoke-virtual {p1}, Lkzw;->b()I

    move-result v0

    iput v0, p0, Lkbl;->r:I

    .line 160
    invoke-virtual {p1}, Lkzw;->f()I

    move-result v0

    iput v0, p0, Lkbl;->q:I

    .line 161
    invoke-virtual {p1}, Lkzw;->h()J

    move-result-wide v0

    iput-wide v0, p0, Lkbl;->A:J

    .line 162
    return-object p0
.end method

.method public a()V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v1, 0x0

    .line 251
    invoke-virtual {p0}, Lkbl;->removeAllViews()V

    .line 252
    iget-object v0, p0, Lkbl;->p:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 253
    iget-object v0, p0, Lkbl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    .line 254
    iget-object v0, p0, Lkbl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 253
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 256
    :cond_0
    iget-object v0, p0, Lkbl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_1

    .line 257
    iget-object v0, p0, Lkbl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->clearAnimation()V

    .line 256
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 259
    :cond_1
    iget-object v0, p0, Lkbl;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_2
    if-ge v2, v3, :cond_2

    .line 260
    iget-object v0, p0, Lkbl;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 259
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 262
    :cond_2
    iget-object v0, p0, Lkbl;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_3
    if-ge v1, v2, :cond_3

    .line 263
    iget-object v0, p0, Lkbl;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 262
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 265
    :cond_3
    iget-object v0, p0, Lkbl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v4, :cond_4

    .line 266
    iget-object v0, p0, Lkbl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 268
    :cond_4
    iget-object v0, p0, Lkbl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v4, :cond_5

    .line 269
    iget-object v0, p0, Lkbl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 271
    :cond_5
    iget-object v0, p0, Lkbl;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v4, :cond_6

    .line 272
    iget-object v0, p0, Lkbl;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 274
    :cond_6
    iget-object v0, p0, Lkbl;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v4, :cond_7

    .line 275
    iget-object v0, p0, Lkbl;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 277
    :cond_7
    return-void
.end method

.method a(II)V
    .locals 3

    .prologue
    .line 377
    iget-object v0, p0, Lkbl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 379
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 380
    invoke-virtual {v0, p2, v1}, Landroid/widget/TextView;->measure(II)V

    .line 381
    return-void
.end method

.method protected a(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 144
    sget-boolean v0, Lkbl;->d:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00ac

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lkbl;->e:I

    const v1, 0x7f0d00ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lkbl;->k:I

    const v1, 0x7f0d00aa

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lkbl;->i:I

    const v1, 0x7f0d00ab

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lkbl;->j:I

    const v1, 0x7f0d00ae

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    sput v1, Lkbl;->h:I

    const v1, 0x7f0b00b8

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lkbl;->u:I

    const v1, 0x7f0b00b7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lkbl;->f:I

    const v1, 0x7f0b00b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lkbl;->v:I

    const v1, 0x7f0c0025

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    sput v1, Lkbl;->g:I

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v1}, Ljava/text/NumberFormat;->getPercentInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v1

    sput-object v1, Lkbl;->w:Ljava/text/NumberFormat;

    invoke-virtual {v1, v4}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    new-array v1, v5, [Landroid/text/InputFilter;

    sput-object v1, Lkbl;->x:[Landroid/text/InputFilter;

    new-instance v2, Landroid/text/InputFilter$LengthFilter;

    const v3, 0x7f0c0024

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    invoke-direct {v2, v0}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    aput-object v2, v1, v4

    sput-boolean v5, Lkbl;->d:Z

    .line 145
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkbl;->l:Ljava/util/ArrayList;

    .line 146
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkbl;->n:Ljava/util/ArrayList;

    .line 147
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkbl;->o:Ljava/util/ArrayList;

    .line 148
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkbl;->m:Ljava/util/ArrayList;

    .line 149
    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lkbl;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lkbl;->p:Landroid/widget/ImageView;

    .line 150
    iget-object v0, p0, Lkbl;->p:Landroid/widget/ImageView;

    const v1, 0x7f02016d

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 151
    new-instance v0, Lkbg;

    invoke-direct {v0}, Lkbg;-><init>()V

    iput-object v0, p0, Lkbl;->s:Lkbg;

    .line 153
    invoke-static {}, Lfo;->a()Lfo;

    move-result-object v0

    invoke-virtual {v0}, Lfo;->b()Z

    move-result v0

    iput-boolean v0, p0, Lkbl;->y:Z

    .line 154
    return-void
.end method

.method a(Landroid/view/View;Landroid/view/View;II)V
    .locals 4

    .prologue
    .line 333
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    add-int v2, v0, p4

    .line 334
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int v3, v2, v0

    .line 337
    iget-boolean v0, p0, Lkbl;->y:Z

    if-eqz v0, :cond_0

    .line 338
    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v0

    sub-int/2addr v0, p3

    .line 339
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int v1, v0, v1

    .line 345
    :goto_0
    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/view/View;->layout(IIII)V

    .line 346
    return-void

    .line 341
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    add-int v1, v0, p3

    .line 342
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public a(Ljxu;)V
    .locals 4

    .prologue
    .line 650
    new-instance v0, Lkbq;

    invoke-direct {v0, p0, p1}, Lkbq;-><init>(Lkbl;Ljxu;)V

    const-wide/16 v2, 0x12c

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    .line 666
    return-void
.end method

.method public a(Ljya;)V
    .locals 0

    .prologue
    .line 623
    iput-object p1, p0, Lkbl;->D:Ljya;

    .line 624
    return-void
.end method

.method public a(Z)V
    .locals 0

    .prologue
    .line 669
    iput-boolean p1, p0, Lkbl;->G:Z

    .line 670
    return-void
.end method

.method public b(Landroid/view/View;)I
    .locals 3

    .prologue
    .line 627
    const/4 v1, -0x1

    .line 628
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lkbl;->r:I

    if-ge v0, v2, :cond_2

    .line 629
    iget-object v2, p0, Lkbl;->l:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eq p1, v2, :cond_0

    iget-object v2, p0, Lkbl;->o:Ljava/util/ArrayList;

    .line 630
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eq p1, v2, :cond_0

    iget-object v2, p0, Lkbl;->m:Ljava/util/ArrayList;

    .line 631
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-ne p1, v2, :cond_1

    .line 636
    :cond_0
    :goto_1
    return v0

    .line 628
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public b(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 619
    iput-object p1, p0, Lkbl;->C:Landroid/view/View$OnClickListener;

    .line 620
    return-void
.end method

.method b(Landroid/view/View;Landroid/view/View;II)V
    .locals 4

    .prologue
    .line 361
    invoke-virtual {p2}, Landroid/view/View;->getTop()I

    move-result v0

    add-int v2, v0, p4

    .line 362
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    add-int v3, v2, v0

    .line 365
    iget-boolean v0, p0, Lkbl;->y:Z

    if-eqz v0, :cond_0

    .line 366
    invoke-virtual {p2}, Landroid/view/View;->getLeft()I

    move-result v0

    add-int v1, v0, p3

    .line 367
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    add-int/2addr v0, v1

    .line 373
    :goto_0
    invoke-virtual {p1, v1, v2, v0, v3}, Landroid/view/View;->layout(IIII)V

    .line 374
    return-void

    .line 369
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getRight()I

    move-result v0

    sub-int/2addr v0, p3

    .line 370
    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    sub-int v1, v0, v1

    goto :goto_0
.end method

.method c(Landroid/content/Context;I)Landroid/widget/TextView;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 168
    iget-object v0, p0, Lkbl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 169
    iget-object v0, p0, Lkbl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 170
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 190
    :goto_0
    invoke-virtual {p0, v0}, Lkbl;->addView(Landroid/view/View;)V

    .line 191
    return-object v0

    .line 172
    :cond_0
    const/4 v0, 0x0

    const/16 v1, 0x25

    invoke-static {p1, v2, v0, v1}, Llhx;->a(Landroid/content/Context;Landroid/util/AttributeSet;II)Landroid/widget/TextView;

    move-result-object v1

    .line 174
    const v0, 0x7f0204a0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setBackgroundResource(I)V

    .line 175
    sget v0, Lkbl;->k:I

    sget v2, Lkbl;->e:I

    sget v3, Lkbl;->k:I

    sget v4, Lkbl;->e:I

    invoke-virtual {v1, v0, v2, v3, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 181
    iget-boolean v0, p0, Lkbl;->y:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x5

    .line 182
    :goto_1
    or-int/lit8 v0, v0, 0x10

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 183
    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setClickable(Z)V

    .line 184
    iget-object v0, p0, Lkbl;->C:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 185
    sget v0, Lkbl;->g:I

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    .line 186
    sget-object v0, Lkbl;->x:[Landroid/text/InputFilter;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setFilters([Landroid/text/InputFilter;)V

    .line 187
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 188
    iget-object v0, p0, Lkbl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v0, v1

    goto :goto_0

    .line 181
    :cond_1
    const/4 v0, 0x3

    goto :goto_1
.end method

.method c(I)V
    .locals 4

    .prologue
    .line 285
    const/4 v0, 0x0

    .line 286
    invoke-virtual {p0}, Lkbl;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 287
    sget-object v0, Lkbl;->w:Ljava/text/NumberFormat;

    invoke-virtual {p0, p1}, Lkbl;->e(I)D

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 290
    :goto_0
    iget-object v0, p0, Lkbl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 291
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 292
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 293
    return-void

    :cond_0
    move-object v1, v0

    goto :goto_0
.end method

.method d(Landroid/content/Context;I)Landroid/widget/TextView;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 231
    iget-object v0, p0, Lkbl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 232
    iget-object v0, p0, Lkbl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 241
    :goto_0
    const/16 v1, 0x26

    invoke-static {p1, v0, v1}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 243
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 244
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 245
    invoke-virtual {p0, v0}, Lkbl;->addView(Landroid/view/View;)V

    .line 246
    return-object v0

    .line 234
    :cond_0
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 235
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 236
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setClickable(Z)V

    .line 237
    iget-object v1, p0, Lkbl;->C:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 238
    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 239
    iget-object v1, p0, Lkbl;->m:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public d()V
    .locals 2

    .prologue
    .line 455
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lkbl;->r:I

    if-ge v0, v1, :cond_1

    .line 456
    invoke-virtual {p0}, Lkbl;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 457
    invoke-virtual {p0, v0}, Lkbl;->g(I)V

    .line 455
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 459
    :cond_0
    invoke-virtual {p0, v0}, Lkbl;->j(I)V

    goto :goto_1

    .line 462
    :cond_1
    invoke-virtual {p0}, Lkbl;->j()V

    .line 463
    return-void
.end method

.method d(I)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 308
    iget-object v0, p0, Lkbl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 309
    iget-object v1, p0, Lkbl;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 310
    iget v2, p0, Lkbl;->q:I

    if-ne p1, v2, :cond_0

    iget-object v2, p0, Lkbl;->p:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v2

    move v3, v2

    .line 312
    :goto_0
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int/2addr v2, v3

    sget v5, Lkbl;->k:I

    div-int/lit8 v5, v5, 0x2

    sub-int v5, v2, v5

    iget-object v2, p0, Lkbl;->m:Ljava/util/ArrayList;

    .line 314
    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getWidth()I

    move-result v2

    sub-int v2, v5, v2

    const/high16 v5, 0x40000000    # 2.0f

    .line 311
    invoke-static {v2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 316
    invoke-virtual {p0, p1, v2}, Lkbl;->a(II)V

    .line 317
    invoke-virtual {p0, v0, v1, v3, v4}, Lkbl;->a(Landroid/view/View;Landroid/view/View;II)V

    .line 319
    return-void

    :cond_0
    move v3, v4

    .line 310
    goto :goto_0
.end method

.method e(I)D
    .locals 4

    .prologue
    .line 280
    iget-wide v0, p0, Lkbl;->A:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    .line 281
    :goto_0
    return-wide v0

    .line 280
    :cond_0
    iget-object v0, p0, Lkbl;->b:Lkzw;

    .line 281
    invoke-virtual {v0}, Lkzw;->j()[Lkzx;

    move-result-object v0

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lkzx;->c()J

    move-result-wide v0

    long-to-double v0, v0

    iget-wide v2, p0, Lkbl;->A:J

    long-to-double v2, v2

    div-double/2addr v0, v2

    goto :goto_0
.end method

.method e(Landroid/content/Context;I)Landroid/view/View;
    .locals 3

    .prologue
    .line 196
    iget-object v0, p0, Lkbl;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 197
    iget-object v0, p0, Lkbl;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object v1, v0

    .line 207
    :goto_0
    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 208
    invoke-virtual {p0, v1}, Lkbl;->addView(Landroid/view/View;)V

    .line 209
    return-object v1

    .line 199
    :cond_0
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 200
    const v1, 0x7f0204a0

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 202
    const/4 v1, 0x2

    invoke-static {v0, v1}, Liu;->c(Landroid/view/View;I)V

    .line 204
    iget-object v1, p0, Lkbl;->C:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 205
    iget-object v1, p0, Lkbl;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    goto :goto_0
.end method

.method e()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation

    .prologue
    .line 466
    invoke-virtual {p0}, Lkbl;->g()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method f(Landroid/content/Context;I)Landroid/view/View;
    .locals 3

    .prologue
    .line 214
    iget-object v0, p0, Lkbl;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 215
    iget-object v0, p0, Lkbl;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    move-object v1, v0

    .line 223
    :goto_0
    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    sget v2, Lkbl;->u:I

    .line 224
    invoke-virtual {v0, v2}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 225
    invoke-virtual {p0, v1}, Lkbl;->addView(Landroid/view/View;)V

    .line 226
    return-object v1

    .line 217
    :cond_0
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 218
    const v1, 0x7f02049e

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 219
    const/4 v1, 0x2

    invoke-static {v0, v1}, Liu;->c(Landroid/view/View;I)V

    .line 221
    iget-object v1, p0, Lkbl;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v1, v0

    goto :goto_0
.end method

.method f()Ljava/util/ArrayList;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation

    .prologue
    .line 475
    invoke-virtual {p0}, Lkbl;->h()Ljava/util/ArrayList;

    move-result-object v0

    .line 476
    iget-object v1, p0, Lkbl;->s:Lkbg;

    iget-object v2, p0, Lkbl;->m:Ljava/util/ArrayList;

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lkbg;->a(Ljava/util/ArrayList;FF)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 478
    return-object v0
.end method

.method f(I)V
    .locals 4

    .prologue
    .line 296
    iget-object v0, p0, Lkbl;->p:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 297
    iget-object v0, p0, Lkbl;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 299
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget-object v2, p0, Lkbl;->p:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getMeasuredHeight()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 301
    iget-object v2, p0, Lkbl;->p:Landroid/widget/ImageView;

    sget v3, Lkbl;->h:I

    invoke-virtual {p0, v2, v0, v3, v1}, Lkbl;->a(Landroid/view/View;Landroid/view/View;II)V

    .line 304
    iget-object v1, p0, Lkbl;->p:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setPivotX(F)V

    .line 305
    return-void
.end method

.method g()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation

    .prologue
    .line 470
    iget-object v0, p0, Lkbl;->s:Lkbg;

    iget-object v1, p0, Lkbl;->n:Ljava/util/ArrayList;

    sget v2, Lkbl;->u:I

    sget v3, Lkbl;->f:I

    invoke-virtual {v0, v1, v2, v3}, Lkbg;->a(Ljava/util/ArrayList;II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method g(I)V
    .locals 2

    .prologue
    .line 384
    invoke-virtual {p0, p1}, Lkbl;->h(I)V

    .line 385
    iget-object v0, p0, Lkbl;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setAlpha(F)V

    .line 386
    invoke-virtual {p0, p1}, Lkbl;->i(I)V

    .line 387
    return-void
.end method

.method h()Ljava/util/ArrayList;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation

    .prologue
    .line 482
    iget-object v0, p0, Lkbl;->s:Lkbg;

    iget-object v1, p0, Lkbl;->n:Ljava/util/ArrayList;

    sget v2, Lkbl;->f:I

    sget v3, Lkbl;->u:I

    invoke-virtual {v0, v1, v2, v3}, Lkbg;->a(Ljava/util/ArrayList;II)Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method h(I)V
    .locals 2

    .prologue
    .line 390
    iget-object v0, p0, Lkbl;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    sget v1, Lkbl;->f:I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 392
    return-void
.end method

.method i()Ljava/util/ArrayList;
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/animation/Animator;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 487
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    move v3, v4

    .line 488
    :goto_0
    iget v0, p0, Lkbl;->r:I

    if-ge v3, v0, :cond_1

    .line 489
    iget-object v0, p0, Lkbl;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 490
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-double v8, v1

    .line 491
    invoke-virtual {p0, v3}, Lkbl;->e(I)D

    move-result-wide v10

    mul-double/2addr v8, v10

    double-to-int v7, v8

    .line 493
    iget-object v1, p0, Lkbl;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 494
    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/GradientDrawable;

    iget v5, p0, Lkbl;->q:I

    if-ne v3, v5, :cond_0

    sget v5, Lkbl;->u:I

    .line 495
    :goto_1
    invoke-virtual {v2, v5}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 498
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 501
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    const/high16 v2, 0x40000000    # 2.0f

    .line 500
    invoke-static {v0, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 503
    iget-object v2, p0, Lkbl;->s:Lkbg;

    iget-boolean v5, p0, Lkbl;->y:Z

    invoke-virtual {v2, v1, v0, v7, v5}, Lkbg;->a(Landroid/view/View;IIZ)Landroid/animation/ValueAnimator;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 488
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 494
    :cond_0
    sget v5, Lkbl;->v:I

    goto :goto_1

    .line 506
    :cond_1
    return-object v6
.end method

.method i(I)V
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    const/4 v6, 0x0

    .line 395
    iget-object v0, p0, Lkbl;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 396
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    int-to-double v2, v1

    .line 397
    invoke-virtual {p0, p1}, Lkbl;->e(I)D

    move-result-wide v4

    mul-double/2addr v2, v4

    double-to-int v4, v2

    .line 399
    iget-object v1, p0, Lkbl;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 400
    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/GradientDrawable;

    iget v3, p0, Lkbl;->q:I

    if-ne p1, v3, :cond_0

    sget v3, Lkbl;->u:I

    .line 401
    :goto_0
    invoke-virtual {v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 403
    invoke-static {v4, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 406
    invoke-virtual {v0}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    .line 405
    invoke-static {v3, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 408
    invoke-virtual {v1, v6}, Landroid/view/View;->setVisibility(I)V

    .line 410
    invoke-virtual {v1, v2, v3}, Landroid/view/View;->measure(II)V

    .line 414
    invoke-virtual {p0, v1, v0, v6, v6}, Lkbl;->a(Landroid/view/View;Landroid/view/View;II)V

    .line 416
    return-void

    .line 400
    :cond_0
    sget v3, Lkbl;->v:I

    goto :goto_0
.end method

.method public j()V
    .locals 4

    .prologue
    .line 600
    iget-object v0, p0, Lkbl;->D:Ljya;

    invoke-interface {v0}, Ljya;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 601
    iget-object v0, p0, Lkbl;->D:Ljya;

    invoke-static {}, Llsx;->d()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 602
    iget-object v0, p0, Lkbl;->D:Ljya;

    const-wide/16 v2, 0x1f4

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    .line 604
    :cond_0
    return-void
.end method

.method j(I)V
    .locals 0

    .prologue
    .line 419
    invoke-virtual {p0, p1}, Lkbl;->k(I)V

    .line 420
    invoke-virtual {p0, p1}, Lkbl;->l(I)V

    .line 421
    return-void
.end method

.method k(I)V
    .locals 2

    .prologue
    .line 424
    iget-object v0, p0, Lkbl;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    sget v1, Lkbl;->u:I

    .line 425
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 426
    return-void
.end method

.method public k()Z
    .locals 2

    .prologue
    .line 611
    iget v0, p0, Lkbl;->q:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method l(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 429
    iget-object v0, p0, Lkbl;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 430
    iget-object v1, p0, Lkbl;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 432
    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    .line 434
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 435
    invoke-virtual {v1, v3, v3}, Landroid/view/View;->measure(II)V

    .line 438
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v2

    .line 444
    iget-boolean v3, p0, Lkbl;->y:Z

    if-eqz v3, :cond_0

    .line 445
    invoke-virtual {v0}, Landroid/view/View;->getRight()I

    move-result v0

    .line 451
    :goto_0
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v3

    add-int/2addr v3, v2

    .line 450
    invoke-virtual {v1, v0, v2, v0, v3}, Landroid/view/View;->layout(IIII)V

    .line 452
    return-void

    .line 447
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v0

    goto :goto_0
.end method

.method public l()Z
    .locals 1

    .prologue
    .line 615
    iget-boolean v0, p0, Lkbl;->B:Z

    return v0
.end method

.method public m(I)V
    .locals 10

    .prologue
    const/16 v6, 0x8

    const/4 v1, 0x0

    const-wide/16 v8, 0x1

    .line 570
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkbl;->B:Z

    .line 571
    invoke-virtual {p0, p1}, Lkbl;->n(I)Z

    move-result v2

    .line 572
    invoke-virtual {p0}, Lkbl;->k()Z

    move-result v3

    .line 573
    if-eqz v2, :cond_0

    iget-object v0, p0, Lkbl;->b:Lkzw;

    invoke-virtual {v0}, Lkzw;->j()[Lkzx;

    move-result-object v0

    aget-object v0, v0, p1

    iget-object v4, p0, Lkbl;->b:Lkzw;

    invoke-virtual {v4}, Lkzw;->j()[Lkzx;

    move-result-object v4

    aget-object v4, v4, p1

    invoke-virtual {v4}, Lkzx;->c()J

    move-result-wide v4

    sub-long/2addr v4, v8

    invoke-virtual {v0, v4, v5}, Lkzx;->a(J)V

    iget-wide v4, p0, Lkbl;->A:J

    sub-long/2addr v4, v8

    iput-wide v4, p0, Lkbl;->A:J

    iget-object v0, p0, Lkbl;->b:Lkzw;

    iget-wide v4, p0, Lkbl;->A:J

    invoke-virtual {v0, v4, v5}, Lkzw;->a(J)V

    const/4 v0, -0x1

    iput v0, p0, Lkbl;->q:I

    iget-object v0, p0, Lkbl;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lkbl;->b:Lkzw;

    iget v4, p0, Lkbl;->q:I

    invoke-virtual {v0, v4}, Lkzw;->a(I)V

    move v0, v1

    :goto_1
    iget v4, p0, Lkbl;->r:I

    if-ge v0, v4, :cond_2

    invoke-virtual {p0, v0}, Lkbl;->c(I)V

    invoke-virtual {p0, v0}, Lkbl;->d(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lkbl;->b:Lkzw;

    invoke-virtual {v0}, Lkzw;->j()[Lkzx;

    move-result-object v0

    aget-object v0, v0, p1

    iget-object v4, p0, Lkbl;->b:Lkzw;

    invoke-virtual {v4}, Lkzw;->j()[Lkzx;

    move-result-object v4

    aget-object v4, v4, p1

    invoke-virtual {v4}, Lkzx;->c()J

    move-result-wide v4

    add-long/2addr v4, v8

    invoke-virtual {v0, v4, v5}, Lkzx;->a(J)V

    if-eqz v3, :cond_1

    iget-object v0, p0, Lkbl;->b:Lkzw;

    invoke-virtual {v0}, Lkzw;->j()[Lkzx;

    move-result-object v0

    iget v4, p0, Lkbl;->q:I

    aget-object v0, v0, v4

    iget-object v4, p0, Lkbl;->b:Lkzw;

    invoke-virtual {v4}, Lkzw;->j()[Lkzx;

    move-result-object v4

    iget v5, p0, Lkbl;->q:I

    aget-object v4, v4, v5

    invoke-virtual {v4}, Lkzx;->c()J

    move-result-wide v4

    sub-long/2addr v4, v8

    invoke-virtual {v0, v4, v5}, Lkzx;->a(J)V

    iget-object v0, p0, Lkbl;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_2
    iput p1, p0, Lkbl;->q:I

    invoke-virtual {p0, p1}, Lkbl;->f(I)V

    goto :goto_0

    :cond_1
    iget-wide v4, p0, Lkbl;->A:J

    add-long/2addr v4, v8

    iput-wide v4, p0, Lkbl;->A:J

    iget-object v0, p0, Lkbl;->b:Lkzw;

    iget-wide v4, p0, Lkbl;->A:J

    invoke-virtual {v0, v4, v5}, Lkzw;->a(J)V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lkbl;->E:Ljxv;

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lkbl;->getContext()Landroid/content/Context;

    move-result-object v0

    const-class v4, Ljxv;

    invoke-static {v0, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljxv;

    iput-object v0, p0, Lkbl;->E:Ljxv;

    :cond_3
    iget-object v0, p0, Lkbl;->E:Ljxv;

    invoke-direct {p0}, Lkbl;->n()Lhee;

    move-result-object v4

    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-interface {v0, v4}, Ljxv;->a(I)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lkbl;->z:Lkax;

    if-nez v0, :cond_4

    new-instance v0, Lkax;

    invoke-virtual {p0}, Lkbl;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v4}, Lkax;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lkbl;->z:Lkax;

    iget-object v0, p0, Lkbl;->z:Lkax;

    invoke-virtual {p0, v0}, Lkbl;->addView(Landroid/view/View;)V

    :cond_4
    iget v0, p0, Lkbl;->c:I

    sget-object v4, Lkbl;->a:Llct;

    iget v4, v4, Llct;->m:I

    mul-int/lit8 v4, v4, 0x2

    sub-int/2addr v0, v4

    const/high16 v4, 0x40000000    # 2.0f

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    iget-object v4, p0, Lkbl;->z:Lkax;

    const/4 v5, -0x2

    invoke-virtual {v4, v0, v5}, Lkax;->measure(II)V

    sget-object v0, Lkbl;->a:Llct;

    iget v0, v0, Llct;->m:I

    sget-object v4, Lkbl;->a:Llct;

    iget v4, v4, Llct;->m:I

    iget-object v5, p0, Lkbl;->z:Lkax;

    iget-object v6, p0, Lkbl;->z:Lkax;

    invoke-virtual {v6}, Lkax;->getMeasuredWidth()I

    move-result v6

    add-int/2addr v6, v0

    iget-object v7, p0, Lkbl;->z:Lkax;

    invoke-virtual {v7}, Lkax;->getMeasuredHeight()I

    move-result v7

    add-int/2addr v7, v4

    invoke-virtual {v5, v0, v4, v6, v7}, Lkax;->layout(IIII)V

    iget-object v0, p0, Lkbl;->E:Ljxv;

    invoke-direct {p0}, Lkbl;->n()Lhee;

    move-result-object v4

    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-interface {v0, v4}, Ljxv;->b(I)V

    .line 576
    :cond_5
    if-eqz v2, :cond_8

    .line 577
    invoke-direct {p0}, Lkbl;->p()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 578
    invoke-direct {p0}, Lkbl;->c()V

    .line 597
    :goto_3
    return-void

    .line 580
    :cond_6
    :goto_4
    iget v0, p0, Lkbl;->r:I

    if-ge v1, v0, :cond_7

    invoke-virtual {p0, v1}, Lkbl;->l(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_7
    new-instance v0, Landroid/animation/AnimatorSet;

    invoke-direct {v0}, Landroid/animation/AnimatorSet;-><init>()V

    invoke-virtual {p0}, Lkbl;->f()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->playTogether(Ljava/util/Collection;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v2, v3}, Landroid/animation/AnimatorSet;->setDuration(J)Landroid/animation/AnimatorSet;

    new-instance v1, Lkbo;

    invoke-direct {v1, p0}, Lkbo;-><init>(Lkbl;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_3

    .line 586
    :cond_8
    if-eqz v3, :cond_9

    .line 587
    invoke-direct {p0}, Lkbl;->c()V

    goto :goto_3

    .line 592
    :cond_9
    invoke-direct {p0}, Lkbl;->o()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-wide v0, p0, Lkbl;->A:J

    cmp-long v0, v0, v8

    if-gtz v0, :cond_b

    :cond_a
    iget-boolean v0, p0, Lkbl;->G:Z

    if-eqz v0, :cond_c

    .line 593
    :cond_b
    invoke-direct {p0}, Lkbl;->c()V

    goto :goto_3

    .line 595
    :cond_c
    iget-object v2, p0, Lkbl;->s:Lkbg;

    iget-object v3, p0, Lkbl;->p:Landroid/widget/ImageView;

    iget-object v0, p0, Lkbl;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lkbl;->n:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    iget v4, p0, Lkbl;->t:F

    invoke-virtual {v2, v3, v0, v1, v4}, Lkbg;->a(Landroid/view/View;Landroid/view/View;Landroid/view/View;F)Landroid/animation/AnimatorSet;

    move-result-object v0

    new-instance v1, Lkbm;

    invoke-direct {v1, p0}, Lkbm;-><init>(Lkbl;)V

    invoke-virtual {v0, v1}, Landroid/animation/AnimatorSet;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_3
.end method

.method m()Z
    .locals 1

    .prologue
    .line 753
    invoke-virtual {p0}, Lkbl;->k()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lkbl;->p()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lkbl;->G:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n(I)Z
    .locals 1

    .prologue
    .line 607
    iget v0, p0, Lkbl;->q:I

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
