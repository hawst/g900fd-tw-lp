.class public Lcdo;
.super Lccy;
.source "PG"


# instance fields
.field private final c:Landroid/widget/Scroller;

.field private final d:Landroid/widget/EdgeEffect;

.field private final e:Landroid/widget/EdgeEffect;

.field private final f:I

.field private final g:Landroid/graphics/Matrix;

.field private h:I

.field private i:I

.field private j:J

.field private k:Lcdp;


# direct methods
.method public constructor <init>(Lcci;)V
    .locals 3

    .prologue
    .line 50
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lccy;-><init>(Lcci;Z)V

    .line 40
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcdo;->g:Landroid/graphics/Matrix;

    .line 44
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcdo;->j:J

    .line 51
    invoke-interface {p1}, Lcci;->f()Landroid/content/Context;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0d005a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcdo;->f:I

    .line 53
    new-instance v1, Landroid/widget/Scroller;

    invoke-direct {v1, v0}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcdo;->c:Landroid/widget/Scroller;

    .line 54
    new-instance v1, Landroid/widget/EdgeEffect;

    invoke-direct {v1, v0}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcdo;->d:Landroid/widget/EdgeEffect;

    .line 55
    new-instance v1, Landroid/widget/EdgeEffect;

    invoke-direct {v1, v0}, Landroid/widget/EdgeEffect;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcdo;->e:Landroid/widget/EdgeEffect;

    .line 56
    return-void
.end method

.method private a(Lccj;F)Z
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 209
    cmpl-float v0, p2, v1

    if-eqz v0, :cond_1

    cmpl-float v0, p2, v1

    if-lez v0, :cond_0

    iget-boolean v0, p1, Lccj;->j:Z

    if-eqz v0, :cond_1

    :cond_0
    cmpg-float v0, p2, v1

    if-gez v0, :cond_2

    iget-boolean v0, p1, Lccj;->k:Z

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 4

    .prologue
    .line 180
    iget-object v0, p0, Lcdo;->k:Lcdp;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcdo;->k:Lcdp;

    iget-wide v2, p0, Lcdo;->j:J

    invoke-interface {v0, v2, v3}, Lcdp;->a(J)V

    .line 183
    :cond_0
    return-void
.end method

.method public a(Lccj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 68
    iget v2, p0, Lcdo;->h:I

    .line 69
    iput v1, p0, Lcdo;->h:I

    .line 71
    iget-object v0, p0, Lcdo;->c:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 72
    iget-object v0, p0, Lcdo;->c:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->getCurrX()I

    move-result v3

    .line 73
    iget v0, p0, Lcdo;->i:I

    sub-int v0, v3, v0

    .line 74
    iput v3, p0, Lcdo;->i:I

    .line 77
    int-to-float v3, v0

    invoke-direct {p0, p1, v3}, Lcdo;->a(Lccj;F)Z

    move-result v3

    if-nez v3, :cond_0

    .line 78
    if-lez v0, :cond_2

    iget-object v0, p0, Lcdo;->d:Landroid/widget/EdgeEffect;

    .line 79
    :goto_0
    iget-object v3, p0, Lcdo;->c:Landroid/widget/Scroller;

    invoke-virtual {v3}, Landroid/widget/Scroller;->getCurrVelocity()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v0, v3}, Landroid/widget/EdgeEffect;->onAbsorb(I)V

    .line 80
    iget-object v0, p0, Lcdo;->a:Lcci;

    invoke-interface {v0}, Lcci;->e()V

    .line 81
    iget-object v0, p0, Lcdo;->c:Landroid/widget/Scroller;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/Scroller;->forceFinished(Z)V

    move v0, v1

    .line 85
    :cond_0
    add-int/2addr v0, v2

    .line 88
    :goto_1
    iget v1, p1, Lccj;->i:I

    sub-int v0, v1, v0

    iput v0, p1, Lccj;->i:I

    .line 90
    iget-object v0, p0, Lcdo;->c:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-nez v0, :cond_1

    .line 91
    iget-object v0, p0, Lcdo;->a:Lcci;

    invoke-interface {v0}, Lcci;->d()V

    .line 93
    :cond_1
    return-void

    .line 78
    :cond_2
    iget-object v0, p0, Lcdo;->e:Landroid/widget/EdgeEffect;

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1
.end method

.method protected a(Lccj;IZ)V
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lcdo;->d:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 188
    iget-object v0, p0, Lcdo;->e:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->onRelease()V

    .line 189
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcdo;->j:J

    .line 190
    return-void
.end method

.method public a(Lccj;Landroid/graphics/Canvas;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 97
    const/4 v0, 0x0

    .line 99
    invoke-virtual {p2}, Landroid/graphics/Canvas;->save()I

    .line 100
    iget-object v1, p0, Lcdo;->d:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_1

    .line 101
    iget-object v1, p0, Lcdo;->d:Landroid/widget/EdgeEffect;

    iget-boolean v0, p1, Lccj;->a:Z

    if-eqz v0, :cond_4

    invoke-virtual {p2}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    :goto_0
    iget v2, p0, Lcdo;->f:I

    invoke-virtual {v1, v0, v2}, Landroid/widget/EdgeEffect;->setSize(II)V

    .line 104
    iget-object v0, p0, Lcdo;->g:Landroid/graphics/Matrix;

    invoke-virtual {v0}, Landroid/graphics/Matrix;->reset()V

    .line 105
    iget-boolean v0, p1, Lccj;->a:Z

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcdo;->g:Landroid/graphics/Matrix;

    const/high16 v1, -0x3d4c0000    # -90.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 108
    iget-object v0, p0, Lcdo;->g:Landroid/graphics/Matrix;

    invoke-virtual {p2}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v4, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 110
    :cond_0
    iget-object v0, p0, Lcdo;->g:Landroid/graphics/Matrix;

    iget-object v1, p1, Lccj;->e:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    iget-object v2, p1, Lccj;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 111
    iget-object v0, p0, Lcdo;->g:Landroid/graphics/Matrix;

    invoke-virtual {p2, v0}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 113
    iget-object v0, p0, Lcdo;->d:Landroid/widget/EdgeEffect;

    invoke-virtual {v0, p2}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v0

    .line 115
    :cond_1
    iget-object v1, p0, Lcdo;->e:Landroid/widget/EdgeEffect;

    invoke-virtual {v1}, Landroid/widget/EdgeEffect;->isFinished()Z

    move-result v1

    if-nez v1, :cond_2

    .line 116
    iget-object v2, p0, Lcdo;->e:Landroid/widget/EdgeEffect;

    iget-boolean v1, p1, Lccj;->a:Z

    if-eqz v1, :cond_5

    invoke-virtual {p2}, Landroid/graphics/Canvas;->getHeight()I

    move-result v1

    :goto_1
    iget v3, p0, Lcdo;->f:I

    invoke-virtual {v2, v1, v3}, Landroid/widget/EdgeEffect;->setSize(II)V

    .line 119
    iget-object v1, p0, Lcdo;->g:Landroid/graphics/Matrix;

    invoke-virtual {v1}, Landroid/graphics/Matrix;->reset()V

    .line 120
    iget-boolean v1, p1, Lccj;->a:Z

    if-eqz v1, :cond_6

    .line 122
    iget-object v1, p0, Lcdo;->g:Landroid/graphics/Matrix;

    const/high16 v2, 0x42b40000    # 90.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 123
    iget-object v1, p0, Lcdo;->g:Landroid/graphics/Matrix;

    invoke-virtual {p2}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1, v2, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 129
    :goto_2
    iget-object v1, p0, Lcdo;->g:Landroid/graphics/Matrix;

    iget-object v2, p1, Lccj;->e:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    int-to-float v2, v2

    iget-object v3, p1, Lccj;->e:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->top:I

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 130
    iget-object v1, p0, Lcdo;->g:Landroid/graphics/Matrix;

    invoke-virtual {p2, v1}, Landroid/graphics/Canvas;->setMatrix(Landroid/graphics/Matrix;)V

    .line 132
    iget-object v1, p0, Lcdo;->e:Landroid/widget/EdgeEffect;

    invoke-virtual {v1, p2}, Landroid/widget/EdgeEffect;->draw(Landroid/graphics/Canvas;)Z

    move-result v1

    or-int/2addr v0, v1

    .line 134
    :cond_2
    invoke-virtual {p2}, Landroid/graphics/Canvas;->restore()V

    .line 136
    if-eqz v0, :cond_3

    .line 137
    iget-object v0, p0, Lcdo;->a:Lcci;

    invoke-interface {v0}, Lcci;->e()V

    .line 139
    :cond_3
    return-void

    .line 101
    :cond_4
    invoke-virtual {p2}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    goto/16 :goto_0

    .line 116
    :cond_5
    invoke-virtual {p2}, Landroid/graphics/Canvas;->getWidth()I

    move-result v1

    goto :goto_1

    .line 126
    :cond_6
    iget-object v1, p0, Lcdo;->g:Landroid/graphics/Matrix;

    const/high16 v2, 0x43340000    # 180.0f

    invoke-virtual {v1, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 127
    iget-object v1, p0, Lcdo;->g:Landroid/graphics/Matrix;

    invoke-virtual {p2}, Landroid/graphics/Canvas;->getWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {p2}, Landroid/graphics/Canvas;->getHeight()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_2
.end method

.method public a(Lcdp;)V
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcdo;->k:Lcdp;

    .line 62
    return-void
.end method

.method protected b(Lccj;I)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 159
    iget-object v0, p0, Lcdo;->k:Lcdp;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lccy;->b:Z

    if-nez v0, :cond_0

    .line 160
    iget-object v0, p0, Lcdo;->k:Lcdp;

    iget-wide v2, p0, Lcdo;->j:J

    invoke-interface {v0, v2, v3}, Lcdp;->a(J)V

    .line 162
    :cond_0
    int-to-float v0, p2

    invoke-direct {p0, p1, v0}, Lcdo;->a(Lccj;F)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    iget-object v0, p0, Lcdo;->c:Landroid/widget/Scroller;

    const/high16 v5, -0x80000000

    const v6, 0x7fffffff

    move v2, v1

    move v3, p2

    move v4, v1

    move v7, v1

    move v8, v1

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 165
    :cond_1
    iget-object v0, p0, Lcdo;->a:Lcci;

    invoke-interface {v0}, Lcci;->d()V

    .line 166
    return-void
.end method

.method protected b(Lccj;II)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 145
    iget-object v0, p0, Lcdo;->c:Landroid/widget/Scroller;

    invoke-virtual {v0, v2}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 146
    iget-object v0, p0, Lcdo;->c:Landroid/widget/Scroller;

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->setFinalX(I)V

    .line 147
    iput v1, p0, Lcdo;->i:I

    .line 148
    iget-object v0, p0, Lcdo;->d:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->finish()V

    .line 149
    iget-object v0, p0, Lcdo;->e:Landroid/widget/EdgeEffect;

    invoke-virtual {v0}, Landroid/widget/EdgeEffect;->finish()V

    .line 150
    invoke-virtual {p0, p1, p2, p3}, Lcdo;->a(Lccj;II)I

    move-result v0

    .line 151
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p1, Lccj;->c:Landroid/util/SparseArray;

    .line 152
    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lccg;

    iget-wide v0, v0, Lccg;->b:J

    :goto_0
    iput-wide v0, p0, Lcdo;->j:J

    .line 154
    return v2

    .line 152
    :cond_0
    const-wide/high16 v0, -0x8000000000000000L

    goto :goto_0
.end method

.method protected c(Lccj;I)V
    .locals 2

    .prologue
    .line 170
    int-to-float v0, p2

    invoke-direct {p0, p1, v0}, Lcdo;->a(Lccj;F)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 171
    iget v0, p0, Lcdo;->h:I

    add-int/2addr v0, p2

    iput v0, p0, Lcdo;->h:I

    .line 172
    iget-object v0, p0, Lcdo;->a:Lcci;

    invoke-interface {v0}, Lcci;->d()V

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 174
    :cond_1
    iget-boolean v0, p1, Lccj;->a:Z

    if-eqz v0, :cond_2

    iget-object v0, p1, Lccj;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v0

    :goto_1
    if-eqz p2, :cond_0

    if-eqz v0, :cond_0

    int-to-float v1, p2

    int-to-float v0, v0

    div-float/2addr v1, v0

    if-lez p2, :cond_3

    iget-object v0, p0, Lcdo;->d:Landroid/widget/EdgeEffect;

    :goto_2
    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EdgeEffect;->onPull(F)V

    iget-object v0, p0, Lcdo;->a:Lcci;

    invoke-interface {v0}, Lcci;->e()V

    goto :goto_0

    :cond_2
    iget-object v0, p1, Lccj;->d:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcdo;->e:Landroid/widget/EdgeEffect;

    goto :goto_2
.end method
