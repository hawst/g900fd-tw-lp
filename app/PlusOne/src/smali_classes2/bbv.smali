.class public final Lbbv;
.super Llol;
.source "PG"

# interfaces
.implements Laly;
.implements Lbfr;
.implements Lcdw;


# static fields
.field private static final N:Lalz;


# instance fields
.field private O:Lalz;

.field private P:Lalr;

.field private Q:Landroid/os/Handler;

.field private R:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

.field private S:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

.field private T:Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;

.field private U:Landroid/widget/FrameLayout;

.field private V:Landroid/widget/LinearLayout;

.field private W:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

.field private X:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

.field private Y:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

.field private Z:Landroid/view/TextureView;

.field private aa:Landroid/view/TextureView;

.field private ab:Landroid/widget/TextView;

.field private ac:Lcba;

.field private ad:Ljava/util/concurrent/FutureTask;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/FutureTask",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private ae:F

.field private af:F

.field private ag:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    const-class v0, Lalz;

    .line 82
    invoke-static {v0}, Lcgf;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lalz;

    sput-object v0, Lbbv;->N:Lalz;

    .line 81
    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/high16 v3, -0x40800000    # -1.0f

    .line 151
    invoke-direct {p0}, Llol;-><init>()V

    .line 91
    sget-object v0, Lbbv;->N:Lalz;

    iput-object v0, p0, Lbbv;->O:Lalz;

    .line 136
    new-instance v0, Lhmg;

    sget-object v1, Lomx;->b:Lhmn;

    invoke-direct {v0, v1}, Lhmg;-><init>(Lhmn;)V

    iget-object v1, p0, Lbbv;->au:Llnh;

    invoke-virtual {v0, v1}, Lhmg;->a(Llnh;)Lhmg;

    .line 137
    new-instance v0, Lhmf;

    iget-object v1, p0, Lbbv;->av:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 141
    new-instance v0, Lbbw;

    invoke-direct {v0, p0}, Lbbw;-><init>(Lbbv;)V

    iput-object v0, p0, Lbbv;->ag:Ljava/lang/Runnable;

    .line 153
    iput v3, p0, Lbbv;->ae:F

    .line 154
    iput v3, p0, Lbbv;->af:F

    .line 155
    return-void
.end method

.method private U()I
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lbbv;->W:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lbbv;->V:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    goto :goto_0
.end method

.method private V()I
    .locals 1

    .prologue
    .line 463
    iget-object v0, p0, Lbbv;->W:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lbbv;->V:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iget v0, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    goto :goto_0
.end method

.method private W()V
    .locals 3

    .prologue
    const/high16 v1, -0x40800000    # -1.0f

    .line 577
    iget v0, p0, Lbbv;->ae:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    iget v0, p0, Lbbv;->af:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    .line 581
    :cond_0
    :goto_0
    return-void

    .line 580
    :cond_1
    iget-object v0, p0, Lbbv;->T:Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;

    iget v1, p0, Lbbv;->ae:F

    iget v2, p0, Lbbv;->af:F

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;->a(FF)V

    goto :goto_0
.end method

.method static synthetic a(Lbbv;)Lalz;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lbbv;->O:Lalz;

    return-object v0
.end method

.method static synthetic a(Lbbv;I)Lcom/google/android/apps/moviemaker/ui/VideoPosterView;
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lbbv;->c(I)Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lbbv;Lalw;)V
    .locals 3

    .prologue
    .line 66
    iget v0, p1, Lalw;->a:F

    iget v1, p0, Lbbv;->ae:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    iget v0, p1, Lalw;->b:F

    iget v1, p0, Lbbv;->af:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_1

    :cond_0
    iget v0, p1, Lalw;->a:F

    iput v0, p0, Lbbv;->ae:F

    iget v0, p1, Lalw;->b:F

    iput v0, p0, Lbbv;->af:F

    iget-object v0, p0, Lbbv;->O:Lalz;

    iget v1, p0, Lbbv;->ae:F

    iget v2, p0, Lbbv;->af:F

    invoke-interface {v0, v1, v2}, Lalz;->a(FF)V

    invoke-direct {p0}, Lbbv;->W()V

    :cond_1
    return-void
.end method

.method static synthetic b(Lbbv;)Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lbbv;->T:Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;

    return-object v0
.end method

.method static synthetic c(Lbbv;)F
    .locals 4

    .prologue
    .line 66
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iget-object v2, p0, Lbbv;->W:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

    iget-object v3, p0, Lbbv;->U:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v3, v0}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    iget-object v2, p0, Lbbv;->W:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

    iget-object v3, p0, Lbbv;->U:Landroid/widget/FrameLayout;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->b(Landroid/view/ViewGroup;Landroid/graphics/Rect;)V

    iget-object v2, p0, Lbbv;->W:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    iget v0, v0, Landroid/graphics/Rect;->left:I

    :goto_0
    iget-object v2, p0, Lbbv;->W:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

    invoke-virtual {v2}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    iget v1, v1, Landroid/graphics/Rect;->right:I

    :goto_1
    sub-int v2, v1, v0

    iget-object v1, p0, Lbbv;->W:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->c()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lbbv;->U:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getWidth()I

    move-result v1

    :goto_2
    if-ge v2, v1, :cond_3

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    :goto_3
    iget-object v1, p0, Lbbv;->ac:Lcba;

    invoke-virtual {v1}, Lcba;->a()I

    move-result v1

    int-to-float v0, v0

    int-to-float v1, v1

    div-float/2addr v0, v1

    invoke-static {v0}, Lcfn;->a(F)F

    move-result v0

    return v0

    :cond_0
    iget v0, v0, Landroid/graphics/Rect;->top:I

    goto :goto_0

    :cond_1
    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lbbv;->U:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v1

    goto :goto_2

    :cond_3
    int-to-float v1, v1

    const v2, 0x3e4ccccd    # 0.2f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sub-int/2addr v0, v1

    goto :goto_3
.end method

.method private c(I)Lcom/google/android/apps/moviemaker/ui/VideoPosterView;
    .locals 2

    .prologue
    .line 508
    if-ltz p1, :cond_0

    iget-object v0, p0, Lbbv;->V:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "index"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 509
    iget-object v0, p0, Lbbv;->V:Landroid/widget/LinearLayout;

    invoke-virtual {v0, p1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    return-object v0

    .line 508
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic d(Lbbv;)Lcba;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lbbv;->ac:Lcba;

    return-object v0
.end method

.method static synthetic e(Lbbv;)Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lbbv;->W:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

    return-object v0
.end method

.method private e()Loo;
    .locals 1

    .prologue
    .line 447
    invoke-virtual {p0}, Lbbv;->n()Lz;

    move-result-object v0

    check-cast v0, Los;

    invoke-virtual {v0}, Los;->h()Loo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lbbv;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lbbv;->W()V

    return-void
.end method

.method static synthetic g(Lbbv;)V
    .locals 4

    .prologue
    .line 66
    iget-object v0, p0, Lbbv;->ab:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const v1, 0x3f666666    # 0.9f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void
.end method

.method static synthetic h(Lbbv;)V
    .locals 4

    .prologue
    .line 66
    iget-object v0, p0, Lbbv;->ab:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getAlpha()F

    move-result v0

    const v1, 0x3f666666    # 0.9f

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lbbv;->ab:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v2

    if-eqz v0, :cond_1

    const-wide/16 v0, 0x3e8

    :goto_1
    invoke-virtual {v2, v0, v1}, Landroid/view/ViewPropertyAnimator;->setStartDelay(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->start()V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_1
.end method

.method static synthetic i(Lbbv;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lbbv;->Q:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic j(Lbbv;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lbbv;->ag:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic k(Lbbv;)I
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Lbbv;->U()I

    move-result v0

    return v0
.end method

.method static synthetic l(Lbbv;)I
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Lbbv;->V()I

    move-result v0

    return v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 377
    const/16 v0, 0xa

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    .line 160
    const v0, 0x7f04006c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 161
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lbbv;->Q:Landroid/os/Handler;

    .line 162
    const v0, 0x7f10021b

    .line 163
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    iput-object v0, p0, Lbbv;->R:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    .line 164
    const v0, 0x7f100224

    .line 165
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    iput-object v0, p0, Lbbv;->S:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    .line 166
    const v0, 0x7f10021e

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lbbv;->V:Landroid/widget/LinearLayout;

    .line 167
    const v0, 0x7f10021d

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

    iput-object v0, p0, Lbbv;->W:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

    .line 168
    const v0, 0x7f10021f

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    iput-object v0, p0, Lbbv;->Y:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    .line 170
    const v0, 0x7f100220

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    iput-object v0, p0, Lbbv;->X:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    .line 172
    const v0, 0x7f100221

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    iput-object v0, p0, Lbbv;->Z:Landroid/view/TextureView;

    .line 173
    const v0, 0x7f100222

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/TextureView;

    iput-object v0, p0, Lbbv;->aa:Landroid/view/TextureView;

    .line 174
    const v0, 0x7f100225

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lbbv;->ab:Landroid/widget/TextView;

    .line 175
    const v0, 0x7f100223

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;

    iput-object v0, p0, Lbbv;->T:Lcom/google/android/apps/moviemaker/ui/InOutMiniMapView;

    .line 176
    const v0, 0x7f10021c

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lbbv;->U:Landroid/widget/FrameLayout;

    .line 178
    new-instance v2, Lcbd;

    iget-object v0, p0, Lbbv;->U:Landroid/widget/FrameLayout;

    check-cast v0, Lcam;

    iget-object v3, p0, Lbbv;->W:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

    new-instance v4, Lbbx;

    invoke-direct {v4, p0}, Lbbx;-><init>(Lbbv;)V

    invoke-direct {v2, v0, v3, v4}, Lcbd;-><init>(Lcam;Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;Lcao;)V

    iput-object v2, p0, Lbbv;->ac:Lcba;

    .line 213
    if-eqz p3, :cond_0

    const-string v0, "ClipEditorScreenScrollPosition"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, p0, Lbbv;->ac:Lcba;

    const-string v2, "ClipEditorScreenScrollPosition"

    .line 216
    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getFloat(Ljava/lang/String;)F

    move-result v2

    invoke-virtual {v0, v2}, Lcba;->a(F)V

    .line 231
    :goto_0
    iget-object v0, p0, Lbbv;->V:Landroid/widget/LinearLayout;

    new-instance v2, Lbbz;

    invoke-direct {v2, p0}, Lbbz;-><init>(Lbbv;)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 241
    iget-object v0, p0, Lbbv;->W:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

    new-instance v2, Lbca;

    invoke-direct {v2, p0}, Lbca;-><init>(Lbbv;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a(Lcao;)V

    .line 268
    invoke-direct {p0}, Lbbv;->e()Loo;

    move-result-object v0

    .line 269
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Loo;->a(F)V

    .line 271
    return-object v1

    .line 219
    :cond_0
    iget-object v0, p0, Lbbv;->W:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

    new-instance v2, Lbby;

    invoke-direct {v2, p0}, Lbby;-><init>(Lbbv;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    goto :goto_0
.end method

.method public a(Lbgf;)Lbge;
    .locals 2

    .prologue
    .line 367
    new-instance v0, Lbtr;

    iget-object v1, p0, Lbbv;->Z:Landroid/view/TextureView;

    invoke-direct {v0, p1, v1}, Lbtr;-><init>(Lbgf;Landroid/view/TextureView;)V

    return-object v0
.end method

.method public a(F)V
    .locals 3

    .prologue
    .line 514
    const/4 v0, 0x0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_1

    .line 524
    :cond_0
    return-void

    .line 518
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lbbv;->V:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 519
    iget-object v0, p0, Lbbv;->V:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    const-string v2, "Clip editor timeline should only contain VideoPosterViews"

    invoke-static {v0, v2}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    .line 521
    iget-object v0, p0, Lbbv;->V:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    .line 522
    invoke-virtual {v0, p1}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(F)V

    .line 518
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public a(FF)V
    .locals 2

    .prologue
    .line 352
    iget-object v0, p0, Lbbv;->W:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a(F)Lalw;

    .line 353
    iget-object v0, p0, Lbbv;->W:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

    invoke-virtual {v0, p2}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->b(F)Lalw;

    move-result-object v0

    .line 354
    iget v1, v0, Lalw;->a:F

    iput v1, p0, Lbbv;->ae:F

    .line 355
    iget v0, v0, Lalw;->b:F

    iput v0, p0, Lbbv;->af:F

    .line 356
    invoke-direct {p0}, Lbbv;->W()V

    .line 357
    return-void
.end method

.method public a(J)V
    .locals 3

    .prologue
    .line 361
    invoke-virtual {p0}, Lbbv;->o()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lchf;->a(JLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    .line 362
    iget-object v1, p0, Lbbv;->ab:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 363
    return-void
.end method

.method public a(Lalx;)V
    .locals 1

    .prologue
    .line 557
    iget-object v0, p0, Lbbv;->W:Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/moviemaker/ui/MaskingMarkingLayout;->a(Lalx;)V

    .line 558
    return-void
.end method

.method public a(Lalz;)V
    .locals 1

    .prologue
    .line 320
    if-nez p1, :cond_0

    .line 321
    sget-object v0, Lbbv;->N:Lalz;

    iput-object v0, p0, Lbbv;->O:Lalz;

    .line 325
    :goto_0
    return-void

    .line 323
    :cond_0
    iput-object p1, p0, Lbbv;->O:Lalz;

    goto :goto_0
.end method

.method public a(Lbqo;Ljava/util/concurrent/Executor;Lbmm;Lbmm;)V
    .locals 5

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x0

    .line 330
    if-eqz p3, :cond_0

    .line 331
    iget-object v0, p0, Lbbv;->R:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Lbqo;)V

    .line 332
    iget-object v0, p0, Lbbv;->R:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    .line 333
    invoke-direct {p0}, Lbbv;->U()I

    move-result v1

    invoke-direct {p0}, Lbbv;->V()I

    move-result v2

    .line 332
    invoke-virtual {v0, p2, p3, v1, v2}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Ljava/util/concurrent/Executor;Lbmm;II)V

    .line 334
    iget-object v0, p0, Lbbv;->R:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->setVisibility(I)V

    .line 339
    :goto_0
    if-eqz p4, :cond_1

    .line 340
    iget-object v0, p0, Lbbv;->S:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Lbqo;)V

    .line 341
    iget-object v0, p0, Lbbv;->S:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    .line 342
    invoke-direct {p0}, Lbbv;->U()I

    move-result v1

    invoke-direct {p0}, Lbbv;->V()I

    move-result v2

    .line 341
    invoke-virtual {v0, p2, p4, v1, v2}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Ljava/util/concurrent/Executor;Lbmm;II)V

    .line 343
    iget-object v0, p0, Lbbv;->S:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->setVisibility(I)V

    .line 348
    :goto_1
    return-void

    .line 336
    :cond_0
    iget-object v0, p0, Lbbv;->R:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->d()V

    .line 337
    iget-object v0, p0, Lbbv;->R:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->setVisibility(I)V

    goto :goto_0

    .line 345
    :cond_1
    iget-object v0, p0, Lbbv;->S:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->d()V

    .line 346
    iget-object v0, p0, Lbbv;->S:Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->setVisibility(I)V

    goto :goto_1
.end method

.method public a(Lbqo;Ljava/util/concurrent/Executor;Lbon;[J)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 386
    invoke-virtual {p3}, Lbon;->g()Lboo;

    move-result-object v0

    invoke-virtual {v0}, Lboo;->b()F

    move-result v4

    .line 387
    iget-object v0, p0, Lbbv;->Y:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->a(F)V

    .line 388
    iget-object v0, p0, Lbbv;->X:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    invoke-virtual {v0, v4}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->a(F)V

    .line 391
    iget-object v0, p0, Lbbv;->V:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v0

    array-length v2, p4

    if-ne v0, v2, :cond_0

    move v0, v1

    .line 392
    :goto_0
    array-length v1, p4

    if-ge v0, v1, :cond_1

    .line 393
    invoke-direct {p0, v0}, Lbbv;->c(I)Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    move-result-object v1

    .line 394
    invoke-virtual {v1}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->d()V

    .line 395
    invoke-virtual {v1, v4}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(F)V

    .line 392
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 398
    :cond_0
    iget-object v0, p0, Lbbv;->V:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    .line 399
    invoke-virtual {p0}, Lbbv;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    move v2, v1

    .line 400
    :goto_1
    array-length v0, p4

    if-ge v2, v0, :cond_1

    .line 401
    const v0, 0x7f04006b

    iget-object v5, p0, Lbbv;->V:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0, v5, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;

    .line 403
    invoke-virtual {v0, v4}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(F)V

    .line 404
    invoke-virtual {v0, p1}, Lcom/google/android/apps/moviemaker/ui/VideoPosterView;->a(Lbqo;)V

    .line 405
    iget-object v5, p0, Lbbv;->V:Landroid/widget/LinearLayout;

    invoke-virtual {v5, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 400
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 411
    :cond_1
    new-instance v3, Lbcb;

    invoke-direct {v3, p0, v4, p4}, Lbcb;-><init>(Lbbv;F[J)V

    .line 431
    iget-object v0, p0, Lbbv;->ad:Ljava/util/concurrent/FutureTask;

    if-eqz v0, :cond_2

    .line 432
    iget-object v0, p0, Lbbv;->ad:Ljava/util/concurrent/FutureTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    .line 434
    :cond_2
    new-instance v6, Ljava/util/concurrent/FutureTask;

    new-instance v0, Lbcd;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lbcd;-><init>(Lbbv;Lbqo;Lbqm;Lbon;[J)V

    invoke-direct {v6, v0}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    iput-object v6, p0, Lbbv;->ad:Ljava/util/concurrent/FutureTask;

    .line 443
    iget-object v0, p0, Lbbv;->ad:Ljava/util/concurrent/FutureTask;

    invoke-interface {p2, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 444
    return-void
.end method

.method public a(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 551
    iget-object v3, p0, Lbbv;->Y:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->setVisibility(I)V

    .line 552
    iget-object v0, p0, Lbbv;->X:Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/AspectRatioEnforcingFrameLayout;->setVisibility(I)V

    .line 553
    return-void

    :cond_0
    move v0, v2

    .line 551
    goto :goto_0

    :cond_1
    move v1, v2

    .line 552
    goto :goto_1
.end method

.method public ae_()V
    .locals 2

    .prologue
    .line 312
    iget-object v0, p0, Lbbv;->P:Lalr;

    invoke-virtual {v0, p0}, Lalr;->b(Laly;)V

    .line 313
    invoke-direct {p0}, Lbbv;->e()Loo;

    move-result-object v0

    .line 314
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Loo;->a(Ljava/lang/CharSequence;)V

    .line 315
    invoke-super {p0}, Llol;->ae_()V

    .line 316
    return-void
.end method

.method public b(Lbgf;)Lbge;
    .locals 2

    .prologue
    .line 372
    new-instance v0, Lbtr;

    iget-object v1, p0, Lbbv;->aa:Landroid/view/TextureView;

    invoke-direct {v0, p1, v1}, Lbtr;-><init>(Lbgf;Landroid/view/TextureView;)V

    return-object v0
.end method

.method public d()Lcdz;
    .locals 1

    .prologue
    .line 307
    sget-object v0, Lcdz;->h:Lcdz;

    return-object v0
.end method

.method public d(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 286
    invoke-super {p0, p1}, Llol;->d(Landroid/os/Bundle;)V

    .line 288
    invoke-static {p0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->b(Lu;)Lcom/google/android/apps/moviemaker/MovieMakerActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/MovieMakerActivity;->o()Laol;

    move-result-object v0

    invoke-virtual {v0}, Laol;->g()Lalr;

    move-result-object v0

    iput-object v0, p0, Lbbv;->P:Lalr;

    .line 289
    iget-object v0, p0, Lbbv;->P:Lalr;

    invoke-virtual {v0, p0}, Lalr;->a(Laly;)V

    .line 290
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 294
    iget-object v0, p0, Lbbv;->ac:Lcba;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbbv;->ac:Lcba;

    invoke-virtual {v0}, Lcba;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295
    const-string v0, "ClipEditorScreenScrollPosition"

    iget-object v1, p0, Lbbv;->ac:Lcba;

    .line 296
    invoke-virtual {v1}, Lcba;->c()F

    move-result v1

    .line 295
    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putFloat(Ljava/lang/String;F)V

    .line 298
    :cond_0
    return-void
.end method

.method public h(Z)Landroid/view/animation/Animation;
    .locals 8

    .prologue
    const-wide/16 v6, 0x1f4

    const/4 v2, 0x2

    const/high16 v4, 0x3f800000    # 1.0f

    .line 528
    invoke-static {}, Lcad;->a()Landroid/animation/ObjectAnimator;

    move-result-object v0

    .line 529
    sget-object v1, Landroid/view/View;->ALPHA:Landroid/util/Property;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setProperty(Landroid/util/Property;)V

    .line 531
    if-eqz p1, :cond_0

    .line 532
    new-array v1, v2, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 536
    :goto_0
    invoke-virtual {v0, v6, v7}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    .line 539
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setStartDelay(J)V

    .line 540
    invoke-virtual {p0}, Lbbv;->x()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 541
    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 544
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v4, v4}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 545
    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 546
    return-object v0

    .line 535
    :cond_0
    new-array v1, v2, [F

    fill-array-data v1, :array_1

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    goto :goto_0

    .line 532
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data

    .line 535
    :array_1
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public i_()V
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lbbv;->O:Lalz;

    invoke-interface {v0}, Lalz;->a()V

    .line 303
    return-void
.end method

.method public z()V
    .locals 2

    .prologue
    .line 277
    invoke-super {p0}, Llol;->z()V

    .line 278
    iget-object v0, p0, Lbbv;->ad:Ljava/util/concurrent/FutureTask;

    if-eqz v0, :cond_0

    .line 279
    iget-object v0, p0, Lbbv;->ad:Ljava/util/concurrent/FutureTask;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/FutureTask;->cancel(Z)Z

    .line 280
    const/4 v0, 0x0

    iput-object v0, p0, Lbbv;->ad:Ljava/util/concurrent/FutureTask;

    .line 282
    :cond_0
    return-void
.end method
