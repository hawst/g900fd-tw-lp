.class public final enum Lbza;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lbza;",
        ">;"
    }
.end annotation


# static fields
.field private static enum A:Lbza;

.field private static enum B:Lbza;

.field private static final C:Lcfh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcfh",
            "<",
            "Lbza;",
            ">;"
        }
    .end annotation
.end field

.field private static final D:Lcfh;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcfh",
            "<",
            "Lbza;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic E:[Lbza;

.field public static final enum a:Lbza;

.field public static final enum b:Lbza;

.field public static final enum c:Lbza;

.field public static final enum d:Lbza;

.field public static final enum e:Lbza;

.field public static final enum f:Lbza;

.field public static final enum g:Lbza;

.field public static final enum h:Lbza;

.field public static final enum i:Lbza;

.field public static final enum j:Lbza;

.field public static final enum k:Lbza;

.field public static final enum l:Lbza;

.field private static enum w:Lbza;

.field private static enum x:Lbza;

.field private static enum y:Lbza;

.field private static enum z:Lbza;


# instance fields
.field public final m:I

.field public final n:I

.field public final o:I

.field public final p:I

.field public final q:Z

.field public final r:Layj;

.field public final s:Lbzq;

.field public final t:Lbyh;

.field public final u:Lbyu;

.field public final v:Z


# direct methods
.method static constructor <clinit>()V
    .locals 13

    .prologue
    .line 46
    new-instance v0, Lbza;

    const-string v1, "VANILLA"

    const/4 v2, 0x0

    const/4 v3, 0x5

    const v4, 0x7f0a013e

    const v5, 0x7f0b0066

    const/4 v6, 0x0

    const/16 v7, 0xf

    new-instance v8, Lbab;

    invoke-direct {v8}, Lbab;-><init>()V

    sget-object v9, Lbzn;->a:Lbzq;

    sget-object v10, Lbyk;->a:Lbyk;

    sget-object v11, Lbyu;->a:Lbyu;

    const/4 v12, 0x1

    invoke-direct/range {v0 .. v12}, Lbza;-><init>(Ljava/lang/String;IIIIZILayj;Lbzq;Lbyh;Lbyu;Z)V

    sput-object v0, Lbza;->a:Lbza;

    .line 50
    new-instance v0, Lbza;

    const-string v1, "CINEMASCOPE"

    const/4 v2, 0x1

    const/4 v3, 0x6

    const v4, 0x7f0a013f

    const v5, 0x7f0b008a

    const/4 v6, 0x0

    const/4 v7, 0x1

    new-instance v8, Layf;

    invoke-direct {v8}, Layf;-><init>()V

    sget-object v9, Lbzn;->a:Lbzq;

    sget-object v10, Lbyk;->a:Lbyk;

    sget-object v11, Lbyu;->a:Lbyu;

    const/4 v12, 0x1

    invoke-direct/range {v0 .. v12}, Lbza;-><init>(Ljava/lang/String;IIIIZILayj;Lbzq;Lbyh;Lbyu;Z)V

    sput-object v0, Lbza;->b:Lbza;

    .line 54
    new-instance v0, Lbza;

    const-string v1, "DOCUMENTARY"

    const/4 v2, 0x2

    const/4 v3, 0x7

    const v4, 0x7f0a0140

    const v5, 0x7f0b008c

    const/4 v6, 0x0

    const/4 v7, 0x2

    new-instance v8, Layh;

    invoke-direct {v8}, Layh;-><init>()V

    sget-object v9, Lbzn;->a:Lbzq;

    sget-object v10, Lbyk;->a:Lbyk;

    sget-object v11, Lbyu;->a:Lbyu;

    const/4 v12, 0x1

    invoke-direct/range {v0 .. v12}, Lbza;-><init>(Ljava/lang/String;IIIIZILayj;Lbzq;Lbyh;Lbyu;Z)V

    sput-object v0, Lbza;->c:Lbza;

    .line 58
    new-instance v0, Lbza;

    const-string v1, "FESTIVAL"

    const/4 v2, 0x3

    const/16 v3, 0x8

    const v4, 0x7f0a0141

    const v5, 0x7f0b008b

    const/4 v6, 0x0

    const/4 v7, 0x3

    new-instance v8, Laym;

    invoke-direct {v8}, Laym;-><init>()V

    sget-object v9, Lbzn;->a:Lbzq;

    sget-object v10, Lbyk;->a:Lbyk;

    sget-object v11, Lbyu;->a:Lbyu;

    const/4 v12, 0x1

    invoke-direct/range {v0 .. v12}, Lbza;-><init>(Ljava/lang/String;IIIIZILayj;Lbzq;Lbyh;Lbyu;Z)V

    sput-object v0, Lbza;->d:Lbza;

    .line 62
    new-instance v0, Lbza;

    const-string v1, "GLAMOUR"

    const/4 v2, 0x4

    const/16 v3, 0xa

    const v4, 0x7f0a014d

    const v5, 0x7f0b0097

    const/4 v6, 0x0

    const/4 v7, 0x4

    new-instance v8, Layv;

    invoke-direct {v8}, Layv;-><init>()V

    sget-object v9, Lbzn;->a:Lbzq;

    sget-object v10, Lbyk;->a:Lbyk;

    sget-object v11, Lbyu;->a:Lbyu;

    const/4 v12, 0x1

    invoke-direct/range {v0 .. v12}, Lbza;-><init>(Ljava/lang/String;IIIIZILayj;Lbzq;Lbyh;Lbyu;Z)V

    sput-object v0, Lbza;->e:Lbza;

    .line 66
    new-instance v0, Lbza;

    const-string v1, "HARDCORE"

    const/4 v2, 0x5

    const/16 v3, 0xb

    const v4, 0x7f0a0143

    const v5, 0x7f0b008d

    const/4 v6, 0x0

    const/4 v7, 0x6

    new-instance v8, Lazb;

    invoke-direct {v8}, Lazb;-><init>()V

    sget-object v9, Lbzn;->a:Lbzq;

    sget-object v10, Lbyk;->a:Lbyk;

    sget-object v11, Lbyu;->a:Lbyu;

    const/4 v12, 0x1

    invoke-direct/range {v0 .. v12}, Lbza;-><init>(Ljava/lang/String;IIIIZILayj;Lbzq;Lbyh;Lbyu;Z)V

    sput-object v0, Lbza;->w:Lbza;

    .line 70
    new-instance v0, Lbza;

    const-string v1, "LOMOKINO"

    const/4 v2, 0x6

    const/16 v3, 0xc

    const v4, 0x7f0a0144

    const v5, 0x7f0b008e

    const/4 v6, 0x1

    const/4 v7, 0x7

    new-instance v8, Lazd;

    invoke-direct {v8}, Lazd;-><init>()V

    sget-object v9, Lbzn;->a:Lbzq;

    sget-object v10, Lbyk;->a:Lbyk;

    sget-object v11, Lbyu;->a:Lbyu;

    const/4 v12, 0x1

    invoke-direct/range {v0 .. v12}, Lbza;-><init>(Ljava/lang/String;IIIIZILayj;Lbzq;Lbyh;Lbyu;Z)V

    sput-object v0, Lbza;->x:Lbza;

    .line 74
    new-instance v0, Lbza;

    const-string v1, "MEMORY_LANE"

    const/4 v2, 0x7

    const/16 v3, 0xd

    const v4, 0x7f0a014e

    const v5, 0x7f0b0098

    const/4 v6, 0x0

    const/16 v7, 0x8

    new-instance v8, Lazf;

    invoke-direct {v8}, Lazf;-><init>()V

    sget-object v9, Lbzn;->a:Lbzq;

    sget-object v10, Lbyk;->a:Lbyk;

    sget-object v11, Lbyu;->a:Lbyu;

    const/4 v12, 0x1

    invoke-direct/range {v0 .. v12}, Lbza;-><init>(Ljava/lang/String;IIIIZILayj;Lbzq;Lbyh;Lbyu;Z)V

    sput-object v0, Lbza;->f:Lbza;

    .line 78
    new-instance v0, Lbza;

    const-string v1, "MODERNPOP"

    const/16 v2, 0x8

    const/16 v3, 0xe

    const v4, 0x7f0a0145

    const v5, 0x7f0b008f

    const/4 v6, 0x0

    const/16 v7, 0x9

    new-instance v8, Lazh;

    invoke-direct {v8}, Lazh;-><init>()V

    sget-object v9, Lbzn;->a:Lbzq;

    sget-object v10, Lbyk;->a:Lbyk;

    sget-object v11, Lbyu;->a:Lbyu;

    const/4 v12, 0x1

    invoke-direct/range {v0 .. v12}, Lbza;-><init>(Ljava/lang/String;IIIIZILayj;Lbzq;Lbyh;Lbyu;Z)V

    sput-object v0, Lbza;->g:Lbza;

    .line 82
    new-instance v0, Lbza;

    const-string v1, "GREAT_OUTDOORS"

    const/16 v2, 0x9

    const/16 v3, 0xf

    const v4, 0x7f0a0146

    const v5, 0x7f0b0090

    const/4 v6, 0x0

    const/4 v7, 0x5

    new-instance v8, Layz;

    invoke-direct {v8}, Layz;-><init>()V

    sget-object v9, Lbzn;->a:Lbzq;

    sget-object v10, Lbyk;->a:Lbyk;

    sget-object v11, Lbyu;->a:Lbyu;

    const/4 v12, 0x1

    invoke-direct/range {v0 .. v12}, Lbza;-><init>(Ljava/lang/String;IIIIZILayj;Lbzq;Lbyh;Lbyu;Z)V

    sput-object v0, Lbza;->h:Lbza;

    .line 86
    new-instance v0, Lbza;

    const-string v1, "PUNK"

    const/16 v2, 0xa

    const/16 v3, 0x10

    const v4, 0x7f0a0147

    const v5, 0x7f0b0091

    const/4 v6, 0x0

    const/16 v7, 0xa

    new-instance v8, Lazl;

    invoke-direct {v8}, Lazl;-><init>()V

    sget-object v9, Lbzn;->a:Lbzq;

    sget-object v10, Lbyk;->a:Lbyk;

    sget-object v11, Lbyu;->a:Lbyu;

    const/4 v12, 0x1

    invoke-direct/range {v0 .. v12}, Lbza;-><init>(Ljava/lang/String;IIIIZILayj;Lbzq;Lbyh;Lbyu;Z)V

    sput-object v0, Lbza;->y:Lbza;

    .line 90
    new-instance v0, Lbza;

    const-string v1, "SHOWTIME"

    const/16 v2, 0xb

    const/16 v3, 0x11

    const v4, 0x7f0a0148

    const v5, 0x7f0b0093

    const/4 v6, 0x0

    const/16 v7, 0xb

    new-instance v8, Lazn;

    invoke-direct {v8}, Lazn;-><init>()V

    sget-object v9, Lbzn;->a:Lbzq;

    sget-object v10, Lbyk;->a:Lbyk;

    sget-object v11, Lbyu;->a:Lbyu;

    const/4 v12, 0x1

    invoke-direct/range {v0 .. v12}, Lbza;-><init>(Ljava/lang/String;IIIIZILayj;Lbzq;Lbyh;Lbyu;Z)V

    sput-object v0, Lbza;->i:Lbza;

    .line 94
    new-instance v0, Lbza;

    const-string v1, "SILVER_SCREEN"

    const/16 v2, 0xc

    const/16 v3, 0x12

    const v4, 0x7f0a0149

    const v5, 0x7f0b0092

    const/4 v6, 0x1

    const/16 v7, 0x11

    new-instance v8, Lazo;

    invoke-direct {v8}, Lazo;-><init>()V

    sget-object v9, Lbzn;->a:Lbzq;

    sget-object v10, Lbyk;->a:Lbyk;

    sget-object v11, Lbyu;->a:Lbyu;

    const/4 v12, 0x1

    invoke-direct/range {v0 .. v12}, Lbza;-><init>(Ljava/lang/String;IIIIZILayj;Lbzq;Lbyh;Lbyu;Z)V

    sput-object v0, Lbza;->z:Lbza;

    .line 98
    new-instance v0, Lbza;

    const-string v1, "SUPER_8"

    const/16 v2, 0xd

    const/16 v3, 0x13

    const v4, 0x7f0a014a

    const v5, 0x7f0b0094

    const/4 v6, 0x0

    const/16 v7, 0xc

    new-instance v8, Lazq;

    invoke-direct {v8}, Lazq;-><init>()V

    sget-object v9, Lbzn;->a:Lbzq;

    sget-object v10, Lbyk;->a:Lbyk;

    sget-object v11, Lbyu;->a:Lbyu;

    const/4 v12, 0x1

    invoke-direct/range {v0 .. v12}, Lbza;-><init>(Ljava/lang/String;IIIIZILayj;Lbzq;Lbyh;Lbyu;Z)V

    sput-object v0, Lbza;->j:Lbza;

    .line 102
    new-instance v0, Lbza;

    const-string v1, "TEAL_ORANGE"

    const/16 v2, 0xe

    const/16 v3, 0x14

    const v4, 0x7f0a014b

    const v5, 0x7f0b0095

    const/4 v6, 0x0

    const/16 v7, 0xd

    new-instance v8, Lazx;

    invoke-direct {v8}, Lazx;-><init>()V

    sget-object v9, Lbzn;->a:Lbzq;

    sget-object v10, Lbyk;->a:Lbyk;

    sget-object v11, Lbyu;->a:Lbyu;

    const/4 v12, 0x1

    invoke-direct/range {v0 .. v12}, Lbza;-><init>(Ljava/lang/String;IIIIZILayj;Lbzq;Lbyh;Lbyu;Z)V

    sput-object v0, Lbza;->A:Lbza;

    .line 106
    new-instance v0, Lbza;

    const-string v1, "URBAN_LANDSCAPE"

    const/16 v2, 0xf

    const/16 v3, 0x15

    const v4, 0x7f0a014f

    const v5, 0x7f0b0099

    const/4 v6, 0x0

    const/16 v7, 0xe

    new-instance v8, Lazz;

    invoke-direct {v8}, Lazz;-><init>()V

    sget-object v9, Lbzn;->a:Lbzq;

    sget-object v10, Lbyk;->a:Lbyk;

    sget-object v11, Lbyu;->a:Lbyu;

    const/4 v12, 0x1

    invoke-direct/range {v0 .. v12}, Lbza;-><init>(Ljava/lang/String;IIIIZILayj;Lbzq;Lbyh;Lbyu;Z)V

    sput-object v0, Lbza;->k:Lbza;

    .line 111
    new-instance v0, Lbza;

    const-string v1, "VINTAGE"

    const/16 v2, 0x10

    const/16 v3, 0x16

    const v4, 0x7f0a014c

    const v5, 0x7f0b0096

    const/4 v6, 0x1

    const/16 v7, 0x10

    new-instance v8, Lbac;

    invoke-direct {v8}, Lbac;-><init>()V

    sget-object v9, Lbzn;->a:Lbzq;

    sget-object v10, Lbyk;->a:Lbyk;

    sget-object v11, Lbyu;->a:Lbyu;

    const/4 v12, 0x1

    invoke-direct/range {v0 .. v12}, Lbza;-><init>(Ljava/lang/String;IIIIZILayj;Lbzq;Lbyh;Lbyu;Z)V

    sput-object v0, Lbza;->B:Lbza;

    .line 115
    new-instance v0, Lbza;

    const-string v1, "FUN_FACTORY"

    const/16 v2, 0x11

    const/16 v3, 0x17

    const v4, 0x7f0a0150

    const v5, 0x7f0b009a

    const/4 v6, 0x0

    const/16 v7, 0x12

    new-instance v8, Layp;

    invoke-direct {v8}, Layp;-><init>()V

    new-instance v9, Lbzt;

    invoke-direct {v9}, Lbzt;-><init>()V

    new-instance v10, Lbym;

    invoke-direct {v10}, Lbym;-><init>()V

    sget-object v11, Lbyu;->b:Lbyu;

    const/4 v12, 0x0

    invoke-direct/range {v0 .. v12}, Lbza;-><init>(Ljava/lang/String;IIIIZILayj;Lbzq;Lbyh;Lbyu;Z)V

    sput-object v0, Lbza;->l:Lbza;

    .line 40
    const/16 v0, 0x12

    new-array v0, v0, [Lbza;

    const/4 v1, 0x0

    sget-object v2, Lbza;->a:Lbza;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lbza;->b:Lbza;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lbza;->c:Lbza;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    sget-object v2, Lbza;->d:Lbza;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    sget-object v2, Lbza;->e:Lbza;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    sget-object v2, Lbza;->w:Lbza;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lbza;->x:Lbza;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lbza;->f:Lbza;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lbza;->g:Lbza;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lbza;->h:Lbza;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lbza;->y:Lbza;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lbza;->i:Lbza;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lbza;->z:Lbza;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lbza;->j:Lbza;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lbza;->A:Lbza;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lbza;->k:Lbza;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lbza;->B:Lbza;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lbza;->l:Lbza;

    aput-object v2, v0, v1

    sput-object v0, Lbza;->E:[Lbza;

    .line 123
    new-instance v1, Landroid/util/LongSparseArray;

    invoke-static {}, Lbza;->values()[Lbza;

    move-result-object v0

    array-length v0, v0

    invoke-direct {v1, v0}, Landroid/util/LongSparseArray;-><init>(I)V

    invoke-static {}, Lbza;->values()[Lbza;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    iget v5, v4, Lbza;->p:I

    int-to-long v6, v5

    invoke-virtual {v1, v6, v7, v4}, Landroid/util/LongSparseArray;->append(JLjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v1}, Lcfh;->a(Landroid/util/LongSparseArray;)Lcfh;

    move-result-object v0

    sput-object v0, Lbza;->C:Lcfh;

    .line 125
    invoke-static {}, Lbza;->a()Lcfh;

    move-result-object v0

    sput-object v0, Lbza;->D:Lcfh;

    .line 124
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IIIIZILayj;Lbzq;Lbyh;Lbyu;Z)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(IIIZI",
            "Layj;",
            "Lbzq;",
            "Lbyh;",
            "Lbyu;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 145
    iput p3, p0, Lbza;->m:I

    .line 146
    iput p4, p0, Lbza;->n:I

    .line 147
    iput p5, p0, Lbza;->o:I

    .line 148
    iput-boolean p6, p0, Lbza;->q:Z

    .line 149
    iput p7, p0, Lbza;->p:I

    .line 150
    const-string v0, "effect"

    const/4 v1, 0x0

    invoke-static {p8, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Layj;

    iput-object v0, p0, Lbza;->r:Layj;

    .line 151
    iput-object p9, p0, Lbza;->s:Lbzq;

    .line 152
    iput-object p10, p0, Lbza;->t:Lbyh;

    .line 153
    iput-object p11, p0, Lbza;->u:Lbyu;

    .line 154
    iput-boolean p12, p0, Lbza;->v:Z

    .line 155
    return-void
.end method

.method public static a(I)Lbza;
    .locals 4

    .prologue
    .line 164
    const-string v0, "cloudThemeType"

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {p0, v0, v1, v2}, Lcec;->b(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)I

    .line 166
    sget-object v0, Lbza;->C:Lcfh;

    int-to-long v2, p0

    invoke-virtual {v0, v2, v3}, Lcfh;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbza;

    return-object v0
.end method

.method private static a()Lcfh;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcfh",
            "<",
            "Lbza;",
            ">;"
        }
    .end annotation

    .prologue
    .line 181
    new-instance v1, Landroid/util/LongSparseArray;

    invoke-static {}, Lbza;->values()[Lbza;

    move-result-object v0

    array-length v0, v0

    invoke-direct {v1, v0}, Landroid/util/LongSparseArray;-><init>(I)V

    .line 182
    invoke-static {}, Lbza;->values()[Lbza;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 183
    iget v5, v4, Lbza;->m:I

    int-to-long v6, v5

    invoke-virtual {v1, v6, v7, v4}, Landroid/util/LongSparseArray;->append(JLjava/lang/Object;)V

    .line 182
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 185
    :cond_0
    invoke-static {v1}, Lcfh;->a(Landroid/util/LongSparseArray;)Lcfh;

    move-result-object v0

    return-object v0
.end method

.method public static b(I)Lbza;
    .locals 4

    .prologue
    .line 177
    sget-object v0, Lbza;->D:Lcfh;

    int-to-long v2, p0

    invoke-virtual {v0, v2, v3}, Lcfh;->get(J)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbza;

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lbza;
    .locals 1

    .prologue
    .line 40
    const-class v0, Lbza;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lbza;

    return-object v0
.end method

.method public static values()[Lbza;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lbza;->E:[Lbza;

    invoke-virtual {v0}, [Lbza;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lbza;

    return-object v0
.end method
