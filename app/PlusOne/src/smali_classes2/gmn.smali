.class public final Lgmn;
.super Ljava/lang/Object;

# interfaces
.implements Lgsb;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lgfz;Landroid/accounts/Account;)Lggd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfz;",
            "Landroid/accounts/Account;",
            ")",
            "Lggd",
            "<",
            "Lgsc;",
            ">;"
        }
    .end annotation

    new-instance v0, Lgmo;

    invoke-direct {v0, p2}, Lgmo;-><init>(Landroid/accounts/Account;)V

    invoke-interface {p1, v0}, Lgfz;->a(Lgfx;)Lgfx;

    move-result-object v0

    return-object v0
.end method

.method public a(Lgfz;Lcom/google/android/gms/location/reporting/UploadRequest;)Lggd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfz;",
            "Lcom/google/android/gms/location/reporting/UploadRequest;",
            ")",
            "Lggd",
            "<",
            "Lgsd;",
            ">;"
        }
    .end annotation

    new-instance v0, Lgmq;

    invoke-direct {v0, p2}, Lgmq;-><init>(Lcom/google/android/gms/location/reporting/UploadRequest;)V

    invoke-interface {p1, v0}, Lgfz;->b(Lgfx;)Lgfx;

    move-result-object v0

    return-object v0
.end method

.method public b(Lgfz;Landroid/accounts/Account;)Lggd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfz;",
            "Landroid/accounts/Account;",
            ")",
            "Lggd",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    new-instance v0, Lgmp;

    invoke-direct {v0, p2}, Lgmp;-><init>(Landroid/accounts/Account;)V

    invoke-interface {p1, v0}, Lgfz;->b(Lgfx;)Lgfx;

    move-result-object v0

    return-object v0
.end method
