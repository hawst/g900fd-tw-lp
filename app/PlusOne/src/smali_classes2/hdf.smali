.class public abstract Lhdf;
.super Lhdx;
.source "PG"


# instance fields
.field public a:Lhdg;

.field public final b:Landroid/graphics/PointF;

.field final c:Lhcn;

.field public d:Z

.field public e:Z

.field private g:Landroid/graphics/PointF;

.field private h:F

.field private i:Z

.field private final j:Landroid/graphics/RectF;

.field private final k:Landroid/graphics/Rect;

.field private final l:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V
    .locals 2

    .prologue
    const/high16 v1, 0x3f000000    # 0.5f

    .line 37
    invoke-direct {p0, p1}, Lhdx;-><init>(Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;)V

    .line 21
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0, v1, v1}, Landroid/graphics/PointF;-><init>(FF)V

    iput-object v0, p0, Lhdf;->g:Landroid/graphics/PointF;

    .line 22
    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lhdf;->b:Landroid/graphics/PointF;

    .line 96
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lhdf;->j:Landroid/graphics/RectF;

    .line 97
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lhdf;->k:Landroid/graphics/Rect;

    .line 98
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lhdf;->l:Landroid/graphics/Rect;

    .line 39
    new-instance v0, Lhcn;

    invoke-virtual {p1}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1}, Lhcn;-><init>(Landroid/content/res/Resources;)V

    iput-object v0, p0, Lhdf;->c:Lhcn;

    .line 41
    iget-object v0, p0, Lhdx;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lhea;->a(Landroid/content/Context;)Lhea;

    move-result-object v0

    invoke-virtual {v0}, Lhea;->b()F

    move-result v0

    iput v0, p0, Lhdf;->h:F

    .line 42
    iget v0, p0, Lhdf;->h:F

    iget v1, p0, Lhdf;->h:F

    mul-float/2addr v0, v1

    iput v0, p0, Lhdf;->h:F

    .line 43
    return-void
.end method

.method private a(Landroid/graphics/RectF;)V
    .locals 4

    .prologue
    .line 184
    iget-object v0, p0, Lhdf;->b:Landroid/graphics/PointF;

    iget v1, p1, Landroid/graphics/RectF;->left:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v2

    iget-object v3, p0, Lhdf;->g:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->x:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->x:F

    .line 185
    iget-object v0, p0, Lhdf;->b:Landroid/graphics/PointF;

    iget v1, p1, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v2

    iget-object v3, p0, Lhdf;->g:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v1, v0, Landroid/graphics/PointF;->y:F

    .line 186
    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Canvas;)V
    .locals 3

    .prologue
    .line 151
    iget-object v0, p0, Lhdf;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b()Landroid/graphics/RectF;

    move-result-object v0

    .line 152
    invoke-virtual {v0}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 163
    :goto_0
    return-void

    .line 156
    :cond_0
    invoke-direct {p0, v0}, Lhdf;->a(Landroid/graphics/RectF;)V

    .line 158
    invoke-virtual {p0}, Lhdf;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 159
    invoke-virtual {p0, p1, v0}, Lhdf;->a(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V

    .line 162
    :cond_1
    iget-object v0, p0, Lhdf;->c:Lhcn;

    iget-object v1, p0, Lhdf;->b:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lhdf;->b:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, p1, v1, v2}, Lhcn;->a(Landroid/graphics/Canvas;FF)V

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Canvas;Landroid/graphics/RectF;)V
    .locals 0

    .prologue
    .line 169
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 87
    iput-boolean p1, p0, Lhdf;->d:Z

    .line 89
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lhdf;->b(Z)V

    .line 90
    return-void
.end method

.method protected a()Z
    .locals 1

    .prologue
    .line 93
    iget-boolean v0, p0, Lhdf;->d:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lhdf;->e:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    return v0
.end method

.method protected b()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 172
    iput-boolean v0, p0, Lhdf;->e:Z

    .line 174
    invoke-virtual {p0, v0}, Lhdf;->b(Z)V

    .line 175
    return-void
.end method

.method protected final b(Z)V
    .locals 4

    .prologue
    .line 101
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lhdf;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 102
    iget-object v0, p0, Lhdf;->c:Lhcn;

    iget-object v1, p0, Lhdf;->b:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lhdf;->b:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lhdf;->j:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, v2, v3}, Lhcn;->a(FFLandroid/graphics/RectF;)V

    iget-object v0, p0, Lhdf;->j:Landroid/graphics/RectF;

    iget-object v1, p0, Lhdf;->k:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lhdf;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    iget-object v1, p0, Lhdf;->k:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate(Landroid/graphics/Rect;)V

    .line 119
    :goto_0
    return-void

    .line 106
    :cond_0
    iget-object v0, p0, Lhdf;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b()Landroid/graphics/RectF;

    move-result-object v0

    .line 107
    iget-object v1, p0, Lhdf;->j:Landroid/graphics/RectF;

    invoke-virtual {p0, v0, v1}, Lhdf;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 108
    iget-object v0, p0, Lhdf;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate()V

    goto :goto_0

    .line 112
    :cond_1
    iget-object v0, p0, Lhdf;->j:Landroid/graphics/RectF;

    iget-object v1, p0, Lhdf;->k:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 114
    iget-object v0, p0, Lhdf;->c:Lhcn;

    iget-object v1, p0, Lhdf;->b:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lhdf;->b:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    iget-object v3, p0, Lhdf;->j:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, v2, v3}, Lhcn;->a(FFLandroid/graphics/RectF;)V

    .line 115
    iget-object v0, p0, Lhdf;->j:Landroid/graphics/RectF;

    iget-object v1, p0, Lhdf;->l:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->roundOut(Landroid/graphics/Rect;)V

    .line 116
    iget-object v0, p0, Lhdf;->k:Landroid/graphics/Rect;

    iget-object v1, p0, Lhdf;->l:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->union(Landroid/graphics/Rect;)V

    .line 118
    iget-object v0, p0, Lhdf;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    iget-object v1, p0, Lhdf;->k:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method protected c()V
    .locals 1

    .prologue
    .line 178
    const/4 v0, 0x0

    iput-boolean v0, p0, Lhdf;->e:Z

    .line 180
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lhdf;->b(Z)V

    .line 181
    return-void
.end method

.method public c(FF)Z
    .locals 2

    .prologue
    .line 190
    iget-object v0, p0, Lhdf;->b:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lhdf;->b:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->y:F

    invoke-static {p1, p2, v0, v1}, Lhdo;->b(FFFF)F

    move-result v0

    iget v1, p0, Lhdf;->h:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lhdf;->i:Z

    .line 191
    iget-boolean v0, p0, Lhdf;->i:Z

    if-eqz v0, :cond_0

    .line 192
    invoke-virtual {p0}, Lhdf;->b()V

    .line 195
    :cond_0
    iget-boolean v0, p0, Lhdf;->i:Z

    return v0

    .line 190
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d(FF)Z
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    .line 212
    iget-boolean v0, p0, Lhdf;->i:Z

    if-nez v0, :cond_0

    .line 213
    const/4 v0, 0x0

    .line 218
    :goto_0
    return v0

    .line 216
    :cond_0
    iget-object v0, p0, Lhdf;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v0}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget v1, v0, Landroid/graphics/RectF;->left:F

    sub-float v1, p1, v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v1, v2

    iget v2, v0, Landroid/graphics/RectF;->top:F

    sub-float v2, p2, v2

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    div-float v0, v2, v0

    invoke-static {v1, v3, v4}, Lhcg;->a(FFF)F

    move-result v1

    invoke-static {v0, v3, v4}, Lhcg;->a(FFF)F

    move-result v0

    invoke-virtual {p0, v1, v0}, Lhdf;->g(FF)Z

    .line 218
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public g()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 200
    iget-boolean v1, p0, Lhdf;->i:Z

    if-nez v1, :cond_0

    .line 207
    :goto_0
    return v0

    .line 204
    :cond_0
    iput-boolean v0, p0, Lhdf;->i:Z

    .line 205
    invoke-virtual {p0}, Lhdf;->c()V

    .line 207
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public g(FF)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 65
    iget-object v0, p0, Lhdf;->g:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->x:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lhdf;->g:Landroid/graphics/PointF;

    iget v0, v0, Landroid/graphics/PointF;->y:F

    cmpl-float v0, p2, v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 66
    :goto_0
    iget-object v2, p0, Lhdf;->g:Landroid/graphics/PointF;

    iput p1, v2, Landroid/graphics/PointF;->x:F

    .line 67
    iget-object v2, p0, Lhdf;->g:Landroid/graphics/PointF;

    iput p2, v2, Landroid/graphics/PointF;->y:F

    .line 69
    if-eqz v0, :cond_1

    .line 71
    invoke-virtual {p0, v1}, Lhdf;->b(Z)V

    .line 73
    iget-object v2, p0, Lhdf;->f:Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;

    invoke-virtual {v2}, Lcom/google/android/libraries/snapseed/ui/views/ParameterOverlayView;->b()Landroid/graphics/RectF;

    move-result-object v2

    invoke-direct {p0, v2}, Lhdf;->a(Landroid/graphics/RectF;)V

    .line 75
    invoke-virtual {p0, v1}, Lhdf;->b(Z)V

    .line 77
    iget-object v1, p0, Lhdf;->a:Lhdg;

    if-eqz v1, :cond_1

    .line 78
    iget-object v1, p0, Lhdf;->a:Lhdg;

    iget-object v2, p0, Lhdf;->g:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    iget-object v3, p0, Lhdf;->g:Landroid/graphics/PointF;

    iget v3, v3, Landroid/graphics/PointF;->y:F

    invoke-interface {v1, v2, v3}, Lhdg;->a(FF)V

    .line 83
    :cond_1
    return v0

    :cond_2
    move v0, v1

    .line 65
    goto :goto_0
.end method
