.class final Lace;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Lack;

.field private final b:Z

.field private c:Lacw;

.field private d:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Laca;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/lang/Exception;

.field private f:Z

.field private g:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<[",
            "Laak;",
            ">;"
        }
    .end annotation
.end field

.field private h:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lacl;",
            ">;"
        }
    .end annotation
.end field

.field private i:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Laan;",
            ">;"
        }
    .end annotation
.end field

.field private j:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Laak;",
            "Lacd;",
            ">;"
        }
    .end annotation
.end field

.field private k:J

.field private l:J

.field private m:Landroid/os/ConditionVariable;

.field private final n:Laci;

.field private synthetic o:Labx;


# direct methods
.method public constructor <init>(Labx;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 210
    iput-object p1, p0, Lace;->o:Labx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    new-instance v0, Lack;

    invoke-direct {v0}, Lack;-><init>()V

    iput-object v0, p0, Lace;->a:Lack;

    .line 133
    iput-object v2, p0, Lace;->c:Lacw;

    .line 134
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lace;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 135
    iput-object v2, p0, Lace;->e:Ljava/lang/Exception;

    .line 136
    iput-boolean v1, p0, Lace;->f:Z

    .line 137
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lace;->g:Ljava/util/Stack;

    .line 138
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lace;->h:Ljava/util/Stack;

    .line 139
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lace;->i:Ljava/util/Set;

    .line 140
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lace;->j:Ljava/util/Map;

    .line 143
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lace;->m:Landroid/os/ConditionVariable;

    .line 146
    new-instance v0, Laci;

    iget-object v1, p0, Lace;->o:Labx;

    invoke-direct {v0}, Laci;-><init>()V

    iput-object v0, p0, Lace;->n:Laci;

    .line 211
    iput-boolean p2, p0, Lace;->b:Z

    .line 212
    return-void
.end method

.method private a(Laan;)V
    .locals 3

    .prologue
    .line 423
    invoke-virtual {p1}, Laan;->d()[Laak;

    move-result-object v0

    iget-object v1, p0, Lace;->g:Ljava/util/Stack;

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    iget-object v0, p0, Lace;->i:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 425
    iget-object v1, p0, Lace;->g:Ljava/util/Stack;

    iget-object v0, p0, Lace;->o:Labx;

    invoke-static {v0}, Labx;->c(Labx;)Lacj;

    move-result-object v2

    iget-object v0, p0, Lace;->g:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laak;

    invoke-interface {v2, v0}, Lacj;->a([Laak;)[Laak;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 426
    invoke-static {}, Labx;->t()Laca;

    move-result-object v0

    invoke-virtual {p0, v0}, Lace;->b(Laca;)V

    .line 427
    return-void
.end method

.method private a(Ljava/lang/String;Lacd;Lacd;)V
    .locals 7

    .prologue
    const/high16 v6, 0x42c80000    # 100.0f

    .line 342
    const-string v0, "%dms %.4f%% real, %dms %.4f%% thread (%.4f%%) (x%d) - %s"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p2, Lacd;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-wide v4, p2, Lacd;->b:J

    long-to-float v3, v4

    mul-float/2addr v3, v6

    iget-wide v4, p3, Lacd;->b:J

    long-to-float v4, v4

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-wide v4, p2, Lacd;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-wide v4, p2, Lacd;->a:J

    long-to-float v3, v4

    mul-float/2addr v3, v6

    iget-wide v4, p3, Lacd;->a:J

    long-to-float v4, v4

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-wide v4, p2, Lacd;->a:J

    long-to-float v3, v4

    mul-float/2addr v3, v6

    iget-wide v4, p2, Lacd;->b:J

    long-to-float v4, v4

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget v3, p2, Lacd;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {}, Labx;->q()Ljava/lang/String;

    .line 348
    return-void
.end method

.method private b(Laan;)V
    .locals 8

    .prologue
    const/4 v7, 0x5

    .line 512
    iget-object v0, p0, Lace;->a:Lack;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lack;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 513
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempting to teardown graph while running!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 515
    :cond_0
    invoke-virtual {p1}, Laan;->d()[Laak;

    move-result-object v0

    .line 516
    if-eqz v0, :cond_4

    .line 517
    invoke-virtual {p1}, Laan;->d()[Laak;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    .line 518
    iget-object v4, v3, Laak;->mState:Laal;

    monitor-enter v4

    :try_start_0
    iget-object v5, v3, Laak;->mState:Laal;

    iget v5, v5, Laal;->a:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_1

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Attempting to tear-down filter "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " which is in an open state!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    :try_start_1
    iget-object v5, v3, Laak;->mState:Laal;

    iget v5, v5, Laal;->a:I

    if-eq v5, v7, :cond_2

    iget-object v5, v3, Laak;->mState:Laal;

    iget v5, v5, Laal;->a:I

    const/4 v6, 0x1

    if-eq v5, v6, :cond_2

    invoke-virtual {v3}, Laak;->k()V

    iget-object v3, v3, Laak;->mState:Laal;

    const/4 v5, 0x5

    iput v5, v3, Laal;->a:I

    :cond_2
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 517
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 520
    :cond_3
    invoke-virtual {p1}, Laan;->e()V

    .line 522
    :cond_4
    iget-object v0, p0, Lace;->o:Labx;

    invoke-static {v0}, Labx;->b(Labx;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 523
    return-void
.end method

.method private c()Laca;
    .locals 1

    .prologue
    .line 356
    :try_start_0
    iget-object v0, p0, Lace;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laca;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 359
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Lace;->o:Labx;

    invoke-static {v0}, Labx;->a(Labx;)Laat;

    move-result-object v0

    invoke-virtual {v0}, Laat;->d()V

    .line 392
    iget-object v0, p0, Lace;->c:Lacw;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Lace;->c:Lacw;

    invoke-virtual {v0}, Lacw;->i()V

    .line 394
    const/4 v0, 0x0

    iput-object v0, p0, Lace;->c:Lacw;

    .line 396
    :cond_0
    return-void
.end method

.method private e()V
    .locals 18

    .prologue
    .line 453
    move-object/from16 v0, p0

    iget-object v2, v0, Lace;->a:Lack;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lack;->b(I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 454
    move-object/from16 v0, p0

    iget-object v2, v0, Lace;->o:Labx;

    invoke-virtual {v2}, Labx;->h()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 455
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lace;->k:J

    sub-long v6, v2, v4

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-wide v4, v0, Lace;->l:J

    sub-long v8, v2, v4

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    new-instance v12, Lacd;

    invoke-direct {v12}, Lacd;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lace;->j:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Laak;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lacd;

    new-instance v4, Landroid/util/Pair;

    invoke-direct {v4, v3, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v10, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lacd;

    if-nez v4, :cond_0

    new-instance v4, Lacd;

    invoke-direct {v4}, Lacd;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-interface {v5, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    iget-wide v14, v4, Lacd;->a:J

    iget-wide v0, v2, Lacd;->a:J

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    iput-wide v14, v4, Lacd;->a:J

    iget-wide v14, v4, Lacd;->b:J

    iget-wide v0, v2, Lacd;->b:J

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    iput-wide v14, v4, Lacd;->b:J

    iget v3, v4, Lacd;->c:I

    iget v14, v2, Lacd;->c:I

    add-int/2addr v3, v14

    iput v3, v4, Lacd;->c:I

    iget-wide v14, v12, Lacd;->a:J

    iget-wide v0, v2, Lacd;->a:J

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    iput-wide v14, v12, Lacd;->a:J

    iget-wide v14, v12, Lacd;->b:J

    iget-wide v0, v2, Lacd;->b:J

    move-wide/from16 v16, v0

    add-long v14, v14, v16

    iput-wide v14, v12, Lacd;->b:J

    iget v3, v12, Lacd;->c:I

    iget v2, v2, Lacd;->c:I

    add-int/2addr v2, v3

    iput v2, v12, Lacd;->c:I

    goto :goto_0

    :cond_1
    invoke-interface {v5}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    new-instance v4, Landroid/util/Pair;

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    invoke-direct {v4, v5, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v11, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    new-instance v2, Lacf;

    invoke-direct {v2}, Lacf;-><init>()V

    invoke-static {v10, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-static {v11, v2}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    invoke-static {}, Labx;->q()Ljava/lang/String;

    const-string v2, "Graph time: %dms real, %dms thread (%.4f%%)"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    long-to-float v5, v8

    const/high16 v13, 0x42c80000    # 100.0f

    mul-float/2addr v5, v13

    long-to-float v13, v6

    div-float/2addr v5, v13

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {}, Labx;->q()Ljava/lang/String;

    const-string v2, "Filter totals: %dms real (%.4f%%), %dms thread (%.4f%%)"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v14, v12, Lacd;->b:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-wide v14, v12, Lacd;->b:J

    long-to-float v5, v14

    const/high16 v13, 0x42c80000    # 100.0f

    mul-float/2addr v5, v13

    long-to-float v6, v6

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-wide v6, v12, Lacd;->a:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-wide v6, v12, Lacd;->a:J

    long-to-float v5, v6

    const/high16 v6, 0x42c80000    # 100.0f

    mul-float/2addr v5, v6

    long-to-float v6, v8

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {}, Labx;->q()Ljava/lang/String;

    invoke-static {}, Labx;->q()Ljava/lang/String;

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v4, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lacd;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v2, v12}, Lace;->a(Ljava/lang/String;Lacd;Lacd;)V

    goto :goto_2

    :cond_3
    invoke-static {}, Labx;->q()Ljava/lang/String;

    invoke-interface {v11}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v4, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Lacd;

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v2, v12}, Lace;->a(Ljava/lang/String;Lacd;Lacd;)V

    goto :goto_3

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lace;->j:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 459
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lace;->a:Lack;

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Lack;->b(I)Z

    move-result v2

    if-nez v2, :cond_6

    .line 460
    invoke-direct/range {p0 .. p0}, Lace;->h()V

    .line 462
    :cond_6
    invoke-direct/range {p0 .. p0}, Lace;->f()V

    .line 464
    :cond_7
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 467
    iget-object v0, p0, Lace;->a:Lack;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lack;->a(I)V

    .line 468
    iget-object v0, p0, Lace;->o:Labx;

    invoke-virtual {v0}, Labx;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 469
    invoke-direct {p0}, Lace;->g()V

    .line 471
    :cond_0
    iget-object v0, p0, Lace;->o:Labx;

    invoke-static {v0}, Labx;->c(Labx;)Lacj;

    move-result-object v0

    invoke-interface {v0}, Lacj;->a()V

    .line 472
    iget-object v0, p0, Lace;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 473
    iget-object v0, p0, Lace;->g:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 474
    iget-object v0, p0, Lace;->o:Labx;

    iget-object v1, p0, Lace;->e:Ljava/lang/Exception;

    iget-boolean v2, p0, Lace;->f:Z

    invoke-static {v0, v1, v2}, Labx;->a(Labx;Ljava/lang/Exception;Z)V

    .line 475
    iget-object v0, p0, Lace;->m:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 476
    return-void
.end method

.method private g()V
    .locals 2

    .prologue
    .line 504
    iget-object v0, p0, Lace;->a:Lack;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lack;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lace;->a:Lack;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lack;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 505
    :cond_0
    iget-object v0, p0, Lace;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laan;

    .line 506
    invoke-virtual {v0}, Laan;->f()V

    goto :goto_0

    .line 509
    :cond_1
    return-void
.end method

.method private h()V
    .locals 8

    .prologue
    .line 540
    iget-object v0, p0, Lace;->i:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laan;

    .line 541
    invoke-virtual {v0}, Laan;->d()[Laak;

    move-result-object v2

    iget-object v0, p0, Lace;->o:Labx;

    invoke-virtual {v0}, Labx;->h()Z

    move-result v3

    const/4 v0, 0x0

    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_0

    if-eqz v3, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Closing Filter "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v5, v2, v0

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    aget-object v4, v2, v0

    iget-object v5, v4, Laak;->mState:Laal;

    monitor-enter v5

    :try_start_0
    invoke-virtual {v4}, Laak;->w()V

    iget-object v6, v4, Laak;->mState:Laal;

    iget v6, v6, Laal;->a:I

    const/4 v7, 0x4

    if-ne v6, v7, :cond_2

    iget-object v4, v4, Laak;->mState:Laal;

    const/4 v6, 0x2

    iput v6, v4, Laal;->a:I

    :cond_2
    monitor-exit v5

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 543
    :cond_3
    return-void
.end method


# virtual methods
.method public a()Landroid/os/ConditionVariable;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lace;->m:Landroid/os/ConditionVariable;

    return-object v0
.end method

.method public a(ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 251
    iget-object v0, p0, Lace;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    new-instance v1, Laca;

    invoke-direct {v1, p1, p2}, Laca;-><init>(ILjava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;->offer(Ljava/lang/Object;)Z

    .line 252
    return-void
.end method

.method public a(Laan;Lacl;)V
    .locals 2

    .prologue
    .line 232
    iget-object v0, p0, Lace;->a:Lack;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lack;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    invoke-direct {p0, p1}, Lace;->a(Laan;)V

    .line 234
    iget-object v0, p0, Lace;->h:Ljava/util/Stack;

    invoke-virtual {v0, p2}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 236
    :cond_0
    return-void
.end method

.method public a(Laca;)V
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lace;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    invoke-virtual {p0, p1}, Lace;->b(Laca;)V

    .line 244
    :cond_0
    return-void
.end method

.method public a(I)Z
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lace;->a:Lack;

    invoke-virtual {v0, p1}, Lack;->b(I)Z

    move-result v0

    return v0
.end method

.method public b(Laca;)V
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lace;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->offer(Ljava/lang/Object;)Z

    .line 248
    return-void
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 264
    iget-boolean v0, p0, Lace;->b:Z

    return v0
.end method

.method public run()V
    .locals 14

    .prologue
    .line 217
    :try_start_0
    invoke-static {}, Labx;->s()Ljava/lang/ThreadLocal;

    move-result-object v0

    iget-object v1, p0, Lace;->o:Labx;

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    iget-object v0, p0, Lace;->o:Labx;

    invoke-virtual {v0}, Labx;->b()Lacs;

    move-result-object v0

    iget-boolean v0, v0, Lacs;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lacw;->a(II)Lacw;

    move-result-object v0

    iput-object v0, p0, Lace;->c:Lacw;

    iget-object v0, p0, Lace;->c:Lacw;

    invoke-virtual {v0}, Lacw;->d()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    .line 227
    :cond_0
    const/4 v0, 0x0

    move v6, v0

    :cond_1
    :goto_0
    if-nez v6, :cond_13

    :try_start_1
    invoke-direct {p0}, Lace;->c()Laca;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v1, v0, Laca;->a:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    iget-object v0, v0, Laca;->b:Ljava/lang/Object;

    check-cast v0, Laan;

    iget-object v1, p0, Lace;->a:Lack;

    invoke-virtual {v1}, Lack;->a()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lace;->a:Lack;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lack;->a(I)V

    const/4 v1, 0x0

    iput-object v1, p0, Lace;->e:Ljava/lang/Exception;

    invoke-direct {p0, v0}, Lace;->a(Laan;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lace;->e:Ljava/lang/Exception;

    if-nez v1, :cond_12

    iput-object v0, p0, Lace;->e:Ljava/lang/Exception;

    const/4 v1, 0x1

    iput-boolean v1, p0, Lace;->f:Z

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    invoke-static {}, Labx;->p()Laca;

    move-result-object v0

    invoke-virtual {p0, v0}, Lace;->b(Laca;)V

    goto :goto_0

    .line 218
    :catch_1
    move-exception v0

    .line 219
    iput-object v0, p0, Lace;->e:Ljava/lang/Exception;

    .line 220
    const/4 v0, 0x1

    iput-boolean v0, p0, Lace;->f:Z

    .line 221
    invoke-direct {p0}, Lace;->f()V

    .line 223
    invoke-direct {p0}, Lace;->d()V

    .line 229
    :goto_1
    return-void

    .line 227
    :pswitch_2
    :try_start_2
    iget-object v0, p0, Lace;->a:Lack;

    invoke-virtual {v0}, Lack;->a()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lace;->k:J

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lace;->l:J

    iget-object v0, p0, Lace;->a:Lack;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lack;->a(I)V

    invoke-static {}, Labx;->r()Laca;

    move-result-object v0

    invoke-virtual {p0, v0}, Lace;->b(Laca;)V

    goto :goto_0

    :pswitch_3
    const-string v0, "GraphRunner.onStep()"

    invoke-static {v0}, Laep;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lace;->a:Lack;

    invoke-virtual {v0}, Lack;->a()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_f

    iget-object v0, p0, Lace;->o:Labx;

    invoke-static {v0}, Labx;->c(Labx;)Lacj;

    move-result-object v1

    iget-object v0, p0, Lace;->g:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laak;

    iget-object v2, p0, Lace;->n:Laci;

    invoke-interface {v1, v0, v2}, Lacj;->a([Laak;Laci;)V

    iget-object v0, p0, Lace;->n:Laci;

    iget-wide v0, v0, Laci;->b:J

    invoke-static {}, Labx;->u()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    iget-object v0, p0, Lace;->n:Laci;

    iget-wide v0, v0, Laci;->b:J

    invoke-static {}, Labx;->v()I

    move-result v2

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_5

    iget-object v0, p0, Lace;->g:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    iget-object v0, p0, Lace;->g:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lace;->e()V

    :cond_2
    :goto_2
    invoke-static {}, Laep;->a()V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lace;->h:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacl;

    if-eqz v0, :cond_4

    iget-object v1, p0, Lace;->o:Labx;

    invoke-interface {v0}, Lacl;->a()V

    :cond_4
    iget-object v1, p0, Lace;->g:Ljava/util/Stack;

    iget-object v0, p0, Lace;->o:Labx;

    invoke-static {v0}, Labx;->c(Labx;)Lacj;

    move-result-object v2

    iget-object v0, p0, Lace;->g:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laak;

    invoke-interface {v2, v0}, Lacj;->a([Laak;)[Laak;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Labx;->r()Laca;

    move-result-object v0

    invoke-virtual {p0, v0}, Lace;->b(Laca;)V

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lace;->n:Laci;

    iget-object v7, v0, Laci;->a:Laak;

    invoke-virtual {v7}, Laak;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Laep;->a(Ljava/lang/String;)V

    const-wide/16 v2, 0x0

    const-wide/16 v0, 0x0

    iget-object v4, p0, Lace;->o:Labx;

    invoke-virtual {v4}, Labx;->h()Z

    move-result v4

    if-eqz v4, :cond_14

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v0

    move-wide v4, v2

    move-wide v2, v0

    :goto_3
    iget-object v1, v7, Laak;->mState:Laal;

    monitor-enter v1
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    const/4 v0, 0x0

    :goto_4
    :try_start_3
    iget-object v8, v7, Laak;->mConnectedInputPortArray:[Lacp;

    array-length v8, v8

    if-ge v0, v8, :cond_7

    iget-object v8, v7, Laak;->mConnectedInputPortArray:[Lacp;

    aget-object v8, v8, v0

    invoke-virtual {v8}, Lacp;->d()Z

    move-result v9

    if-eqz v9, :cond_6

    invoke-virtual {v8}, Lacp;->b()Z

    move-result v8

    if-eqz v8, :cond_6

    iget-object v8, v7, Laak;->mConnectedInputPortArray:[Lacp;

    aget-object v8, v8, v0

    invoke-virtual {v8}, Lacp;->c()Laap;

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    iput-wide v8, v7, Laak;->mLastScheduleTime:J

    iget-object v0, v7, Laak;->mState:Laal;

    iget v0, v0, Laal;->a:I

    const/4 v8, 0x1

    if-ne v0, v8, :cond_8

    invoke-virtual {v7}, Laak;->g()V

    iget-object v0, v7, Laak;->mState:Laal;

    const/4 v8, 0x2

    iput v8, v0, Laal;->a:I

    :cond_8
    iget-object v0, v7, Laak;->mState:Laal;

    iget v0, v0, Laal;->a:I

    const/4 v8, 0x2

    if-ne v0, v8, :cond_a

    iget-object v0, v7, Laak;->mConnectedOutputPorts:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacv;

    invoke-virtual {v7, v0}, Laak;->b(Lacv;)V

    goto :goto_5

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0

    :cond_9
    :try_start_5
    invoke-virtual {v7}, Laak;->h()V

    iget-object v0, v7, Laak;->mState:Laal;

    const/4 v8, 0x3

    iput v8, v0, Laal;->a:I

    :cond_a
    iget-object v0, v7, Laak;->mState:Laal;

    iget v0, v0, Laal;->a:I

    const/4 v8, 0x3

    if-ne v0, v8, :cond_b

    invoke-virtual {v7}, Laak;->i()V

    iget v0, v7, Laak;->mRequests:I

    if-eqz v0, :cond_b

    iget v0, v7, Laak;->mRequests:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_b

    invoke-virtual {v7}, Laak;->w()V

    const/4 v0, 0x0

    iput v0, v7, Laak;->mRequests:I

    :cond_b
    monitor-exit v1
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    const/4 v0, 0x0

    move v1, v0

    :goto_6
    :try_start_6
    iget-object v0, v7, Laak;->mAutoReleaseFrames:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_c

    iget-object v0, v7, Laak;->mAutoReleaseFrames:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laap;

    invoke-virtual {v0}, Laap;->f()Laap;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_6

    :cond_c
    iget-object v0, v7, Laak;->mAutoReleaseFrames:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget v0, v7, Laak;->mScheduleCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v7, Laak;->mScheduleCount:I

    iget-object v0, p0, Lace;->o:Labx;

    invoke-virtual {v0}, Labx;->h()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v10

    iget-object v0, p0, Lace;->j:Ljava/util/Map;

    invoke-interface {v0, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacd;

    if-nez v0, :cond_d

    new-instance v0, Lacd;

    invoke-direct {v0}, Lacd;-><init>()V

    iget-object v1, p0, Lace;->j:Ljava/util/Map;

    invoke-interface {v1, v7, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_d
    iget-wide v12, v0, Lacd;->b:J

    sub-long v4, v8, v4

    add-long/2addr v4, v12

    iput-wide v4, v0, Lacd;->b:J

    iget-wide v4, v0, Lacd;->a:J

    sub-long v2, v10, v2

    add-long/2addr v2, v4

    iput-wide v2, v0, Lacd;->a:J

    iget v1, v0, Lacd;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lacd;->c:I

    :cond_e
    invoke-static {}, Laep;->a()V

    invoke-static {}, Labx;->r()Laca;

    move-result-object v0

    invoke-virtual {p0, v0}, Lace;->b(Laca;)V

    goto/16 :goto_2

    :cond_f
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "State is not running! ("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lace;->a:Lack;

    invoke-virtual {v1}, Lack;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :pswitch_4
    invoke-direct {p0}, Lace;->e()V

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p0, Lace;->a:Lack;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lack;->c(I)Z

    goto/16 :goto_0

    :pswitch_6
    iget-object v0, p0, Lace;->a:Lack;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lack;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lace;->a:Lack;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lack;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lace;->h()V

    goto/16 :goto_0

    :pswitch_7
    iget-object v0, p0, Lace;->a:Lack;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lack;->d(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lace;->a:Lack;

    invoke-virtual {v0}, Lack;->a()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    invoke-static {}, Labx;->r()Laca;

    move-result-object v0

    invoke-virtual {p0, v0}, Lace;->b(Laca;)V

    goto/16 :goto_0

    :pswitch_8
    iget-object v0, p0, Lace;->a:Lack;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Lack;->d(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lace;->a:Lack;

    invoke-virtual {v0}, Lack;->a()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    invoke-static {}, Labx;->r()Laca;

    move-result-object v0

    invoke-virtual {p0, v0}, Lace;->b(Laca;)V

    goto/16 :goto_0

    :pswitch_9
    invoke-direct {p0}, Lace;->g()V

    goto/16 :goto_0

    :pswitch_a
    iget-object v0, v0, Laca;->b:Ljava/lang/Object;

    check-cast v0, Laan;

    invoke-direct {p0, v0}, Lace;->b(Laan;)V

    goto/16 :goto_0

    :pswitch_b
    iget-object v0, p0, Lace;->o:Labx;

    invoke-static {v0}, Labx;->b(Labx;)Ljava/util/Set;

    move-result-object v1

    monitor-enter v1
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0

    :try_start_7
    iget-object v0, p0, Lace;->o:Labx;

    invoke-static {v0}, Labx;->b(Labx;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_10

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Attempting to tear down runner with "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lace;->o:Labx;

    invoke-static {v3}, Labx;->b(Labx;)Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " graphs still attached!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :catchall_1
    move-exception v0

    monitor-exit v1
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    :try_start_8
    throw v0
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_0

    :cond_10
    :try_start_9
    monitor-exit v1
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    const/4 v0, 0x1

    move v6, v0

    goto/16 :goto_0

    :pswitch_c
    :try_start_a
    iget-object v0, p0, Lace;->o:Labx;

    invoke-static {v0}, Labx;->b(Labx;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_11

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Attempting to release frames with "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lace;->o:Labx;

    invoke-static {v2}, Labx;->b(Labx;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " graphs still attached!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_11
    iget-object v0, p0, Lace;->o:Labx;

    invoke-static {v0}, Labx;->a(Labx;)Laat;

    move-result-object v0

    invoke-virtual {v0}, Laat;->d()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_0

    goto/16 :goto_0

    :cond_12
    const/4 v0, 0x0

    iput-boolean v0, p0, Lace;->f:Z

    iget-object v0, p0, Lace;->d:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    invoke-direct {p0}, Lace;->f()V

    goto/16 :goto_0

    .line 228
    :cond_13
    invoke-direct {p0}, Lace;->d()V

    goto/16 :goto_1

    :cond_14
    move-wide v4, v2

    move-wide v2, v0

    goto/16 :goto_3

    .line 227
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method
