.class public final Lalr;
.super Lamn;
.source "PG"


# instance fields
.field final a:Lbgf;

.field final b:Lbtu;

.field final c:Lbtu;

.field final d:Lcdu;

.field final e:Lalz;

.field final f:Lavv;

.field final g:Lbzg;

.field final h:Lasn;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lasn",
            "<",
            "Laly;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lbqo;

.field private final k:Ljava/util/concurrent/Executor;


# direct methods
.method public constructor <init>(Lbgf;Lbqo;Lbtu;Lbtu;Lavv;Lbzg;Ljava/util/concurrent/Executor;Lcdu;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 265
    invoke-direct {p0}, Lamn;-><init>()V

    .line 209
    new-instance v0, Lals;

    invoke-direct {v0, p0}, Lals;-><init>(Lalr;)V

    iput-object v0, p0, Lalr;->e:Lalz;

    .line 233
    new-instance v0, Lalt;

    const-class v1, Laly;

    invoke-direct {v0, p0, v1}, Lalt;-><init>(Lalr;Ljava/lang/Class;)V

    iput-object v0, p0, Lalr;->h:Lasn;

    .line 266
    const-string v0, "renderContext"

    invoke-static {p1, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgf;

    iput-object v0, p0, Lalr;->a:Lbgf;

    .line 267
    const-string v0, "posterStore"

    invoke-static {p2, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbqo;

    iput-object v0, p0, Lalr;->j:Lbqo;

    .line 268
    const-string v0, "clipStartVideoFrameViewer"

    .line 269
    invoke-static {p3, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbtu;

    iput-object v0, p0, Lalr;->b:Lbtu;

    .line 270
    const-string v0, "clipEndVideoFrameViewer"

    .line 271
    invoke-static {p4, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbtu;

    iput-object v0, p0, Lalr;->c:Lbtu;

    .line 272
    const-string v0, "clipEditorState"

    invoke-static {p5, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lavv;

    iput-object v0, p0, Lalr;->f:Lavv;

    .line 273
    const-string v0, "clipSelectorLimits"

    invoke-static {p6, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbzg;

    iput-object v0, p0, Lalr;->g:Lbzg;

    .line 274
    const-string v0, "posterExecutor"

    invoke-static {p7, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lalr;->k:Ljava/util/concurrent/Executor;

    .line 275
    const-string v0, "analyticsSession"

    invoke-static {p8, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdu;

    iput-object v0, p0, Lalr;->d:Lcdu;

    .line 276
    return-void
.end method

.method public static a(ILboi;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 560
    invoke-virtual {p1, v0}, Lboi;->b(I)Lbmd;

    move-result-object v1

    iget-object v1, v1, Lbmd;->d:Lbmg;

    sget-object v2, Lbmg;->d:Lbmg;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-eqz v0, :cond_1

    .line 561
    add-int/lit8 p0, p0, 0x1

    .line 563
    :cond_1
    return p0
.end method

.method private b(I)I
    .locals 1

    .prologue
    .line 545
    iget-object v0, p0, Lalr;->f:Lavv;

    invoke-interface {v0}, Lavv;->F()Lboi;

    move-result-object v0

    invoke-static {p1, v0}, Lalr;->a(ILboi;)I

    move-result v0

    return v0
.end method

.method private f()I
    .locals 1

    .prologue
    .line 541
    iget-object v0, p0, Lalr;->f:Lavv;

    invoke-interface {v0}, Lavv;->ay()I

    move-result v0

    invoke-direct {p0, v0}, Lalr;->b(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 294
    invoke-super {p0}, Lamn;->a()V

    .line 295
    iget-object v0, p0, Lalr;->h:Lasn;

    invoke-virtual {v0}, Lasn;->a()V

    .line 296
    return-void
.end method

.method public a(I)V
    .locals 4

    .prologue
    .line 313
    invoke-virtual {p0}, Lalr;->e()V

    .line 314
    iget-object v0, p0, Lalr;->f:Lavv;

    invoke-interface {v0}, Lavv;->F()Lboi;

    move-result-object v0

    const-string v1, "mState.getStoryboard()"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 315
    iget-object v0, p0, Lalr;->f:Lavv;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lavv;->k(Z)V

    .line 316
    iget-object v0, p0, Lalr;->f:Lavv;

    invoke-interface {v0, p1}, Lavv;->g(I)V

    .line 318
    iget-object v0, p0, Lalr;->f:Lavv;

    invoke-interface {v0}, Lavv;->F()Lboi;

    move-result-object v0

    .line 319
    invoke-direct {p0, p1}, Lalr;->b(I)I

    move-result v1

    .line 318
    invoke-virtual {v0, v1}, Lboi;->b(I)Lbmd;

    move-result-object v0

    iget-object v0, v0, Lbmd;->f:Lbmp;

    .line 320
    iget-object v1, p0, Lalr;->f:Lavv;

    iget-wide v2, v0, Lbmp;->b:J

    invoke-interface {v1, v2, v3}, Lavv;->e(J)V

    .line 321
    iget-object v1, p0, Lalr;->f:Lavv;

    iget-wide v2, v0, Lbmp;->c:J

    invoke-interface {v1, v2, v3}, Lavv;->f(J)V

    .line 323
    iget-object v0, p0, Lalr;->h:Lasn;

    iget-object v0, v0, Lasn;->a:Ljava/lang/Object;

    check-cast v0, Laly;

    .line 324
    invoke-virtual {p0, v0}, Lalr;->c(Laly;)V

    .line 325
    invoke-virtual {p0, v0}, Lalr;->d(Laly;)V

    .line 326
    return-void
.end method

.method public a(Laly;)V
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lalr;->h:Lasn;

    invoke-virtual {v0, p1}, Lasn;->c(Ljava/lang/Object;)V

    .line 283
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lalr;->b:Lbtu;

    invoke-interface {v0}, Lbtu;->a()V

    .line 301
    iget-object v0, p0, Lalr;->c:Lbtu;

    invoke-interface {v0}, Lbtu;->a()V

    .line 302
    invoke-virtual {p0}, Lalr;->c()V

    .line 303
    iget-object v0, p0, Lalr;->h:Lasn;

    invoke-virtual {v0}, Lasn;->b()V

    .line 304
    invoke-super {p0}, Lamn;->b()V

    .line 305
    return-void
.end method

.method public b(Laly;)V
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lalr;->h:Lasn;

    invoke-virtual {v0, p1}, Lasn;->d(Ljava/lang/Object;)V

    .line 290
    return-void
.end method

.method c()V
    .locals 14

    .prologue
    .line 418
    invoke-virtual {p0}, Lalr;->e()V

    .line 420
    iget-object v0, p0, Lalr;->f:Lavv;

    invoke-interface {v0}, Lavv;->ax()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 422
    new-instance v1, Lbmp;

    iget-object v0, p0, Lalr;->f:Lavv;

    .line 423
    invoke-interface {v0}, Lavv;->av()J

    move-result-wide v2

    iget-object v0, p0, Lalr;->f:Lavv;

    invoke-interface {v0}, Lavv;->aw()J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lbmp;-><init>(JJ)V

    .line 424
    iget-object v0, p0, Lalr;->f:Lavv;

    invoke-interface {v0}, Lavv;->F()Lboi;

    move-result-object v0

    .line 425
    invoke-direct {p0}, Lalr;->f()I

    move-result v2

    .line 424
    invoke-virtual {v0, v2}, Lboi;->b(I)Lbmd;

    move-result-object v0

    .line 426
    invoke-virtual {v0}, Lbmd;->a()Lbmf;

    move-result-object v2

    iget-object v3, p0, Lalr;->f:Lavv;

    .line 427
    invoke-interface {v3}, Lavv;->C()Lama;

    move-result-object v3

    invoke-interface {v3}, Lama;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Lbmf;->a(I)Lbmf;

    move-result-object v2

    const/4 v3, 0x0

    .line 428
    invoke-virtual {v2, v3}, Lbmf;->a(Z)Lbmf;

    move-result-object v3

    iget-object v2, p0, Lalr;->f:Lavv;

    .line 430
    invoke-interface {v2}, Lavv;->F()Lboi;

    move-result-object v2

    iget-object v0, v0, Lbmd;->e:Ljeg;

    invoke-virtual {v2, v0}, Lboi;->a(Ljeg;)Lbon;

    move-result-object v0

    .line 429
    const-wide/32 v4, 0x16e360

    const-string v2, "minLengthUs"

    invoke-static {v4, v5, v2}, Lcec;->b(JLjava/lang/CharSequence;)J

    invoke-virtual {v0}, Lbon;->e()J

    move-result-wide v4

    const-wide/32 v6, 0x16e360

    cmp-long v0, v4, v6

    if-ltz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v2, "video duration must be greater than minLengthUs"

    invoke-static {v0, v2}, Lcec;->a(ZLjava/lang/CharSequence;)V

    iget-wide v6, v1, Lbmp;->c:J

    iget-wide v8, v1, Lbmp;->b:J

    sub-long/2addr v6, v8

    const-wide/32 v8, 0x16e360

    cmp-long v0, v6, v8

    if-ltz v0, :cond_2

    move-object v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lbmf;->a(Lbmp;)Lbmf;

    move-result-object v0

    .line 434
    invoke-virtual {v0}, Lbmf;->a()Lbmd;

    move-result-object v0

    .line 435
    iget-object v1, p0, Lalr;->f:Lavv;

    .line 436
    invoke-interface {v1}, Lavv;->ay()I

    move-result v1

    .line 435
    new-instance v2, Lbpb;

    invoke-direct {v2, v1, v0}, Lbpb;-><init>(ILbmd;)V

    .line 437
    iget-object v0, p0, Lalr;->f:Lavv;

    invoke-interface {v0, v2}, Lavv;->a(Lbph;)V

    .line 438
    iget-object v0, p0, Lalr;->f:Lavv;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lavv;->k(Z)V

    .line 440
    :cond_0
    return-void

    .line 429
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-wide v8, v1, Lbmp;->b:J

    const-wide/32 v10, 0x16e360

    sub-long/2addr v10, v6

    const-wide/16 v12, 0x2

    div-long/2addr v10, v12

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    iget-wide v10, v1, Lbmp;->c:J

    sub-long v10, v4, v10

    const-wide/32 v12, 0x16e360

    sub-long/2addr v12, v6

    sub-long v8, v12, v8

    invoke-static {v10, v11, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    const-wide/32 v10, 0x16e360

    sub-long v6, v10, v6

    sub-long/2addr v6, v8

    new-instance v2, Lbmp;

    iget-wide v10, v1, Lbmp;->b:J

    sub-long v6, v10, v6

    iget-wide v0, v1, Lbmp;->c:J

    add-long/2addr v0, v8

    invoke-direct {v2, v6, v7, v0, v1}, Lbmp;-><init>(JJ)V

    const-wide/16 v0, 0x0

    iget-wide v6, v2, Lbmp;->b:J

    cmp-long v0, v0, v6

    if-gtz v0, :cond_3

    iget-wide v0, v2, Lbmp;->c:J

    cmp-long v0, v0, v4

    if-gtz v0, :cond_3

    iget-wide v0, v2, Lbmp;->c:J

    iget-wide v4, v2, Lbmp;->b:J

    sub-long/2addr v0, v4

    const-wide/32 v4, 0x16e360

    cmp-long v0, v0, v4

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1d

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "interval "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " failed sanity check"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcgp;->a(ZLjava/lang/CharSequence;)V

    move-object v0, v2

    goto/16 :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2
.end method

.method c(Laly;)V
    .locals 10

    .prologue
    .line 346
    iget-object v0, p0, Lalr;->f:Lavv;

    invoke-interface {v0}, Lavv;->F()Lboi;

    move-result-object v0

    if-nez v0, :cond_0

    .line 412
    :goto_0
    return-void

    .line 350
    :cond_0
    invoke-virtual {p0}, Lalr;->d()Lbon;

    move-result-object v3

    .line 351
    invoke-virtual {p0}, Lalr;->d()Lbon;

    move-result-object v0

    invoke-virtual {v0}, Lbon;->e()J

    move-result-wide v4

    .line 354
    iget-object v0, p0, Lalr;->f:Lavv;

    invoke-interface {v0}, Lavv;->av()J

    move-result-wide v0

    .line 355
    iget-object v2, p0, Lalr;->f:Lavv;

    invoke-interface {v2}, Lavv;->aw()J

    move-result-wide v6

    .line 356
    long-to-float v2, v0

    long-to-float v8, v4

    div-float/2addr v2, v8

    invoke-static {v2}, Lcfn;->a(F)F

    move-result v2

    .line 357
    long-to-float v8, v6

    long-to-float v9, v4

    div-float/2addr v8, v9

    invoke-static {v8}, Lcfn;->a(F)F

    move-result v8

    .line 358
    invoke-interface {p1, v2, v8}, Laly;->a(FF)V

    .line 359
    sub-long v8, v6, v0

    invoke-interface {p1, v8, v9}, Laly;->a(J)V

    .line 360
    iget-object v2, p0, Lalr;->b:Lbtu;

    invoke-interface {v2, v0, v1}, Lbtu;->a(J)V

    .line 361
    iget-object v0, p0, Lalr;->c:Lbtu;

    invoke-interface {v0, v6, v7}, Lbtu;->a(J)V

    .line 363
    const v0, 0x49b71b00    # 1500000.0f

    long-to-float v1, v4

    div-float v6, v0, v1

    .line 364
    new-instance v1, Lalu;

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lalu;-><init>(Lalr;Lbon;JF)V

    invoke-interface {p1, v1}, Laly;->a(Lalx;)V

    goto :goto_0
.end method

.method d()Lbon;
    .locals 3

    .prologue
    .line 536
    iget-object v0, p0, Lalr;->f:Lavv;

    invoke-interface {v0}, Lavv;->F()Lboi;

    move-result-object v0

    iget-object v1, p0, Lalr;->f:Lavv;

    invoke-interface {v1}, Lavv;->F()Lboi;

    move-result-object v1

    .line 537
    invoke-direct {p0}, Lalr;->f()I

    move-result v2

    invoke-virtual {v1, v2}, Lboi;->b(I)Lbmd;

    move-result-object v1

    iget-object v1, v1, Lbmd;->e:Ljeg;

    .line 536
    invoke-virtual {v0, v1}, Lboi;->a(Ljeg;)Lbon;

    move-result-object v0

    return-object v0
.end method

.method d(Laly;)V
    .locals 10

    .prologue
    .line 500
    iget-object v0, p0, Lalr;->f:Lavv;

    invoke-interface {v0}, Lavv;->F()Lboi;

    move-result-object v0

    if-nez v0, :cond_0

    .line 527
    :goto_0
    return-void

    .line 504
    :cond_0
    invoke-direct {p0}, Lalr;->f()I

    move-result v0

    .line 507
    iget-object v1, p0, Lalr;->f:Lavv;

    .line 508
    invoke-interface {v1}, Lavv;->F()Lboi;

    move-result-object v1

    add-int/lit8 v2, v0, -0x1

    invoke-virtual {v1, v2}, Lboi;->e(I)Lbmm;

    move-result-object v1

    .line 509
    iget-object v2, p0, Lalr;->f:Lavv;

    .line 510
    invoke-interface {v2}, Lavv;->F()Lboi;

    move-result-object v2

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v2, v0}, Lboi;->e(I)Lbmm;

    move-result-object v0

    .line 511
    iget-object v2, p0, Lalr;->j:Lbqo;

    iget-object v3, p0, Lalr;->k:Ljava/util/concurrent/Executor;

    invoke-interface {p1, v2, v3, v1, v0}, Laly;->a(Lbqo;Ljava/util/concurrent/Executor;Lbmm;Lbmm;)V

    .line 514
    invoke-virtual {p0}, Lalr;->d()Lbon;

    move-result-object v1

    .line 515
    invoke-virtual {v1}, Lbon;->g()Lboo;

    move-result-object v0

    invoke-virtual {v0}, Lboo;->b()F

    move-result v0

    invoke-interface {p1, v0}, Laly;->a(F)V

    .line 518
    invoke-interface {p1}, Laly;->a()I

    move-result v2

    .line 519
    invoke-virtual {p0}, Lalr;->d()Lbon;

    move-result-object v0

    invoke-virtual {v0}, Lbon;->e()J

    move-result-wide v4

    .line 520
    long-to-float v0, v4

    add-int/lit8 v3, v2, -0x1

    int-to-float v3, v3

    div-float v3, v0, v3

    .line 521
    new-array v6, v2, [J

    .line 522
    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    .line 523
    int-to-float v7, v0

    mul-float/2addr v7, v3

    float-to-long v8, v7

    invoke-static {v4, v5, v8, v9}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v8

    .line 524
    aput-wide v8, v6, v0

    .line 522
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 526
    :cond_1
    iget-object v0, p0, Lalr;->j:Lbqo;

    iget-object v2, p0, Lalr;->k:Ljava/util/concurrent/Executor;

    invoke-interface {p1, v0, v2, v1, v6}, Laly;->a(Lbqo;Ljava/util/concurrent/Executor;Lbon;[J)V

    goto :goto_0
.end method
