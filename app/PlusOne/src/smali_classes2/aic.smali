.class public final Laic;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:Laib;

.field public b:J

.field public final c:[Lahz;


# direct methods
.method public varargs constructor <init>(Laib;J[Lahz;)V
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    const-string v0, "phase"

    invoke-static {p1, v0, v8}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Laib;

    iput-object v0, p0, Laic;->a:Laib;

    .line 196
    const-string v3, "checkpointDurationUs"

    const-wide/16 v4, -0x1

    const-wide v6, 0x7fffffffffffffffL

    move-wide v1, p2

    invoke-static/range {v1 .. v7}, Lcec;->a(JLjava/lang/CharSequence;JJ)J

    move-result-wide v0

    iput-wide v0, p0, Laic;->b:J

    .line 201
    const-string v0, "disabledFilters"

    invoke-static {p4, v0, v8}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lahz;

    iput-object v0, p0, Laic;->c:[Lahz;

    .line 202
    return-void
.end method

.method public varargs constructor <init>(Laib;[Lahz;)V
    .locals 2

    .prologue
    .line 191
    const-wide/16 v0, -0x1

    invoke-direct {p0, p1, v0, v1, p2}, Laic;-><init>(Laib;J[Lahz;)V

    .line 192
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 206
    if-ne p1, p0, :cond_1

    .line 213
    :cond_0
    :goto_0
    return v0

    .line 209
    :cond_1
    instance-of v2, p1, Laic;

    if-nez v2, :cond_2

    move v0, v1

    .line 210
    goto :goto_0

    .line 212
    :cond_2
    check-cast p1, Laic;

    .line 213
    iget-object v2, p0, Laic;->a:Laib;

    iget-object v3, p1, Laic;->a:Laib;

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Laic;->c:[Lahz;

    iget-object v3, p1, Laic;->c:[Lahz;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    .line 218
    iget-object v3, p0, Laic;->a:Laib;

    iget-object v4, p0, Laic;->c:[Lahz;

    const/16 v1, 0x11

    if-nez v4, :cond_1

    const/16 v0, 0x20f

    :cond_0
    invoke-static {v3, v0}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v0

    return v0

    :cond_1
    const/4 v0, 0x0

    array-length v5, v4

    move v6, v0

    move v0, v1

    move v1, v6

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v2, v4, v1

    invoke-static {v2, v0}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v2

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 223
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Laic;->a:Laib;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Laic;->c:[Lahz;

    invoke-static {v3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
