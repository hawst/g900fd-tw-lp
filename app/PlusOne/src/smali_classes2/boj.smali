.class public final Lboj;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Lbmp;


# instance fields
.field private final b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbma;",
            ">;"
        }
    .end annotation
.end field

.field private c:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<+",
            "Lbml;",
            ">;"
        }
    .end annotation
.end field

.field private d:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation
.end field

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmh;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcac;",
            ">;"
        }
    .end annotation
.end field

.field private g:Layj;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lbmp;

.field private k:I

.field private l:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 379
    new-instance v0, Lbmp;

    const-wide/16 v2, 0x0

    const-wide/32 v4, 0x3d0900

    invoke-direct {v0, v2, v3, v4, v5}, Lbmp;-><init>(JJ)V

    sput-object v0, Lboj;->a:Lbmp;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 395
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 381
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lboj;->b:Ljava/util/List;

    .line 389
    const-string v0, ""

    iput-object v0, p0, Lboj;->h:Ljava/lang/String;

    .line 390
    const-string v0, ""

    iput-object v0, p0, Lboj;->i:Ljava/lang/String;

    .line 391
    sget-object v0, Lboj;->a:Lbmp;

    iput-object v0, p0, Lboj;->j:Lbmp;

    .line 396
    return-void
.end method

.method public constructor <init>(Lboj;)V
    .locals 2

    .prologue
    .line 398
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 381
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lboj;->b:Ljava/util/List;

    .line 389
    const-string v0, ""

    iput-object v0, p0, Lboj;->h:Ljava/lang/String;

    .line 390
    const-string v0, ""

    iput-object v0, p0, Lboj;->i:Ljava/lang/String;

    .line 391
    sget-object v0, Lboj;->a:Lbmp;

    iput-object v0, p0, Lboj;->j:Lbmp;

    .line 399
    iget-object v0, p0, Lboj;->b:Ljava/util/List;

    iget-object v1, p0, Lboj;->b:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 400
    iget-object v0, p1, Lboj;->c:Ljava/util/Collection;

    iput-object v0, p0, Lboj;->c:Ljava/util/Collection;

    .line 401
    iget-object v0, p1, Lboj;->d:Ljava/util/List;

    iput-object v0, p0, Lboj;->d:Ljava/util/List;

    .line 402
    iget-object v0, p1, Lboj;->e:Ljava/util/List;

    iput-object v0, p0, Lboj;->e:Ljava/util/List;

    .line 403
    iget-object v0, p1, Lboj;->f:Ljava/util/List;

    iput-object v0, p0, Lboj;->f:Ljava/util/List;

    .line 404
    iget-object v0, p1, Lboj;->g:Layj;

    iput-object v0, p0, Lboj;->g:Layj;

    .line 405
    iget-object v0, p1, Lboj;->h:Ljava/lang/String;

    iput-object v0, p0, Lboj;->h:Ljava/lang/String;

    .line 406
    iget-object v0, p1, Lboj;->i:Ljava/lang/String;

    iput-object v0, p0, Lboj;->i:Ljava/lang/String;

    .line 407
    iget-object v0, p1, Lboj;->j:Lbmp;

    iput-object v0, p0, Lboj;->j:Lbmp;

    .line 408
    iget v0, p1, Lboj;->k:I

    iput v0, p0, Lboj;->k:I

    .line 409
    iget v0, p1, Lboj;->l:I

    iput v0, p0, Lboj;->l:I

    .line 410
    return-void
.end method


# virtual methods
.method public a(II)Lboj;
    .locals 0

    .prologue
    .line 478
    iput p1, p0, Lboj;->k:I

    .line 479
    iput p2, p0, Lboj;->l:I

    .line 480
    return-object p0
.end method

.method public a(Layj;)Lboj;
    .locals 0

    .prologue
    .line 458
    iput-object p1, p0, Lboj;->g:Layj;

    .line 459
    return-object p0
.end method

.method public a(Lbma;)Lboj;
    .locals 1

    .prologue
    .line 423
    iget-object v0, p0, Lboj;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 424
    return-object p0
.end method

.method public a(Lbmp;)Lboj;
    .locals 0

    .prologue
    .line 473
    iput-object p1, p0, Lboj;->j:Lbmp;

    .line 474
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lboj;
    .locals 2

    .prologue
    .line 463
    const-string v0, "title"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lboj;->h:Ljava/lang/String;

    .line 464
    return-object p0
.end method

.method public a(Ljava/util/Collection;)Lboj;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<+",
            "Lbml;",
            ">;)",
            "Lboj;"
        }
    .end annotation

    .prologue
    .line 413
    iput-object p1, p0, Lboj;->c:Ljava/util/Collection;

    .line 414
    return-object p0
.end method

.method public a(Ljava/util/List;)Lboj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)",
            "Lboj;"
        }
    .end annotation

    .prologue
    .line 418
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lboj;->d:Ljava/util/List;

    .line 419
    return-object p0
.end method

.method public a()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcac;",
            ">;"
        }
    .end annotation

    .prologue
    .line 442
    iget-object v0, p0, Lboj;->f:Ljava/util/List;

    return-object v0
.end method

.method public b()Layj;
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Lboj;->g:Layj;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Lboj;
    .locals 2

    .prologue
    .line 468
    const-string v0, "subtitle"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lboj;->i:Ljava/lang/String;

    .line 469
    return-object p0
.end method

.method public b(Ljava/util/List;)Lboj;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmh;",
            ">;)",
            "Lboj;"
        }
    .end annotation

    .prologue
    .line 428
    iput-object p1, p0, Lboj;->e:Ljava/util/List;

    .line 429
    return-object p0
.end method

.method public c()Lboi;
    .locals 14

    .prologue
    .line 484
    iget-object v0, p0, Lboj;->d:Ljava/util/List;

    const-string v1, "mVideoClips"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 485
    iget-object v0, p0, Lboj;->c:Ljava/util/Collection;

    .line 486
    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-static {v1}, Lcfm;->a(I)Ljava/util/HashMap;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbml;

    invoke-interface {v0}, Lbml;->a()Ljeg;

    move-result-object v3

    invoke-interface {v1, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v1

    .line 488
    iget-object v0, p0, Lboj;->d:Ljava/util/List;

    iget v2, p0, Lboj;->k:I

    iget v3, p0, Lboj;->l:I

    invoke-static {v0, v1, v2, v3}, Lbqi;->a(Ljava/util/List;Ljava/util/Map;II)[I

    move-result-object v0

    .line 490
    const/4 v2, 0x0

    aget v10, v0, v2

    .line 491
    const/4 v2, 0x1

    aget v11, v0, v2

    .line 492
    const/4 v2, 0x2

    aget v12, v0, v2

    .line 493
    const/4 v2, 0x3

    aget v13, v0, v2

    .line 495
    iget-object v0, p0, Lboj;->e:Ljava/util/List;

    if-nez v0, :cond_1

    .line 496
    iget-object v0, p0, Lboj;->d:Ljava/util/List;

    invoke-static {v0, v1, v10, v11}, Lbmh;->a(Ljava/util/List;Ljava/util/Map;II)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lboj;->e:Ljava/util/List;

    .line 499
    :cond_1
    new-instance v0, Lboi;

    iget-object v2, p0, Lboj;->d:Ljava/util/List;

    iget-object v3, p0, Lboj;->b:Ljava/util/List;

    iget-object v4, p0, Lboj;->e:Ljava/util/List;

    iget-object v5, p0, Lboj;->f:Ljava/util/List;

    iget-object v6, p0, Lboj;->g:Layj;

    iget-object v7, p0, Lboj;->h:Ljava/lang/String;

    iget-object v8, p0, Lboj;->i:Ljava/lang/String;

    iget-object v9, p0, Lboj;->j:Lbmp;

    invoke-direct/range {v0 .. v13}, Lboi;-><init>(Ljava/util/Map;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Layj;Ljava/lang/String;Ljava/lang/String;Lbmp;IIII)V

    return-object v0
.end method

.method public c(Ljava/util/List;)Lboj;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcac;",
            ">;)",
            "Lboj;"
        }
    .end annotation

    .prologue
    .line 433
    invoke-static {p1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lboj;->f:Ljava/util/List;

    .line 434
    return-object p0
.end method

.method public d()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 506
    iget-object v0, p0, Lboj;->d:Ljava/util/List;

    return-object v0
.end method
