.class public final Laik;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Laie;


# instance fields
.field private final a:Landroid/app/NotificationManager;


# direct methods
.method public constructor <init>(Landroid/app/NotificationManager;)V
    .locals 2

    .prologue
    .line 149
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 150
    const-string v0, "notificationManager"

    .line 151
    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Laik;->a:Landroid/app/NotificationManager;

    .line 152
    return-void
.end method

.method private a(ILandroid/net/Uri;Laic;Landroid/content/Context;Z)V
    .locals 7

    .prologue
    .line 188
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 189
    const-string v1, "content://media/external/video/media/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 190
    const-string v1, "content://media/external/video/media/"

    const-string v2, "external:"

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 193
    :cond_0
    new-instance v1, Landroid/app/Notification$Builder;

    invoke-direct {v1, p4}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 194
    const v2, 0x7f030001

    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    .line 195
    const v2, 0x7f0a00cb

    .line 196
    invoke-virtual {p4, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 195
    invoke-virtual {v1, v2}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 197
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    iget-object v3, p3, Laic;->a:Laib;

    .line 198
    invoke-virtual {v3}, Laib;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p4, p1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 197
    invoke-virtual {v1, v0}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 199
    invoke-virtual {v1, p5}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    .line 201
    iget-object v0, p0, Laik;->a:Landroid/app/NotificationManager;

    .line 202
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p3, Laic;->a:Laib;

    invoke-virtual {v3}, Laib;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ":"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f100006

    .line 204
    invoke-virtual {v1}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    .line 201
    invoke-virtual {v0, v2, v3, v1}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 205
    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/net/Uri;Laic;)V
    .locals 6

    .prologue
    .line 156
    const v1, 0x7f0a00cf

    const/4 v5, 0x1

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Laik;->a(ILandroid/net/Uri;Laic;Landroid/content/Context;Z)V

    .line 159
    return-void
.end method

.method public b(Landroid/content/Context;Landroid/net/Uri;Laic;)V
    .locals 6

    .prologue
    .line 163
    const v1, 0x7f0a00cc

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Laik;->a(ILandroid/net/Uri;Laic;Landroid/content/Context;Z)V

    .line 166
    return-void
.end method

.method public c(Landroid/content/Context;Landroid/net/Uri;Laic;)V
    .locals 6

    .prologue
    .line 170
    const v1, 0x7f0a00cd

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Laik;->a(ILandroid/net/Uri;Laic;Landroid/content/Context;Z)V

    .line 173
    return-void
.end method

.method public d(Landroid/content/Context;Landroid/net/Uri;Laic;)V
    .locals 6

    .prologue
    .line 177
    const v1, 0x7f0a00ce

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Laik;->a(ILandroid/net/Uri;Laic;Landroid/content/Context;Z)V

    .line 180
    return-void
.end method
