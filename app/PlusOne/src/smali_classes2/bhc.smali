.class public final Lbhc;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 15
    sparse-switch p0, :sswitch_data_0

    .line 45
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unhandled gl type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 17
    :sswitch_0
    const-string v0, "sampler2D"

    .line 43
    :goto_0
    return-object v0

    .line 19
    :sswitch_1
    const-string v0, "samplerCUBE"

    goto :goto_0

    .line 21
    :sswitch_2
    const-string v0, "samplerExternalOES"

    goto :goto_0

    .line 23
    :sswitch_3
    const-string v0, "float"

    goto :goto_0

    .line 25
    :sswitch_4
    const-string v0, "int"

    goto :goto_0

    .line 27
    :sswitch_5
    const-string v0, "vec2"

    goto :goto_0

    .line 29
    :sswitch_6
    const-string v0, "ivec2"

    goto :goto_0

    .line 31
    :sswitch_7
    const-string v0, "vec3"

    goto :goto_0

    .line 33
    :sswitch_8
    const-string v0, "ivec3"

    goto :goto_0

    .line 35
    :sswitch_9
    const-string v0, "vec4"

    goto :goto_0

    .line 37
    :sswitch_a
    const-string v0, "ivec4"

    goto :goto_0

    .line 39
    :sswitch_b
    const-string v0, "mat2"

    goto :goto_0

    .line 41
    :sswitch_c
    const-string v0, "mat3"

    goto :goto_0

    .line 43
    :sswitch_d
    const-string v0, "mat4"

    goto :goto_0

    .line 15
    :sswitch_data_0
    .sparse-switch
        0x1404 -> :sswitch_4
        0x1406 -> :sswitch_3
        0x8b50 -> :sswitch_5
        0x8b51 -> :sswitch_7
        0x8b52 -> :sswitch_9
        0x8b53 -> :sswitch_6
        0x8b54 -> :sswitch_8
        0x8b55 -> :sswitch_a
        0x8b5a -> :sswitch_b
        0x8b5b -> :sswitch_c
        0x8b5c -> :sswitch_d
        0x8b5e -> :sswitch_0
        0x8b60 -> :sswitch_1
        0x8d66 -> :sswitch_2
    .end sparse-switch
.end method

.method public static a(Ljava/lang/String;IIII)V
    .locals 7

    .prologue
    .line 178
    invoke-static {p3}, Lbhc;->c(I)I

    move-result v0

    .line 179
    if-eq p1, v0, :cond_0

    .line 180
    new-instance v0, Ljava/lang/IllegalArgumentException;

    .line 181
    invoke-static {p3}, Lbhc;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 182
    invoke-static {p1}, Lbhc;->d(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2d

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "\'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\' expected type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " but was given "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 186
    :cond_0
    invoke-static {p3}, Lbhc;->b(I)I

    move-result v0

    .line 187
    mul-int/2addr v0, p4

    .line 188
    if-eq p2, v0, :cond_1

    .line 189
    new-instance v1, Ljava/lang/IllegalArgumentException;

    .line 190
    invoke-static {p3}, Lbhc;->a(I)Ljava/lang/String;

    move-result-object v2

    .line 192
    invoke-static {p1}, Lbhc;->d(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {p0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x4f

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v5, v6

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "\'"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "\' of type "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " expected an array of length "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " but was given "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "["

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 195
    :cond_1
    return-void
.end method

.method public static b(I)I
    .locals 3

    .prologue
    .line 53
    sparse-switch p0, :sswitch_data_0

    .line 75
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unhandled gl type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :sswitch_0
    const/4 v0, 0x1

    .line 73
    :goto_0
    return v0

    .line 62
    :sswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 65
    :sswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 69
    :sswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 71
    :sswitch_4
    const/16 v0, 0x9

    goto :goto_0

    .line 73
    :sswitch_5
    const/16 v0, 0x10

    goto :goto_0

    .line 53
    :sswitch_data_0
    .sparse-switch
        0x1404 -> :sswitch_0
        0x1406 -> :sswitch_0
        0x8b50 -> :sswitch_1
        0x8b51 -> :sswitch_2
        0x8b52 -> :sswitch_3
        0x8b53 -> :sswitch_1
        0x8b54 -> :sswitch_2
        0x8b55 -> :sswitch_3
        0x8b5a -> :sswitch_3
        0x8b5b -> :sswitch_4
        0x8b5c -> :sswitch_5
        0x8b5e -> :sswitch_0
        0x8b60 -> :sswitch_0
        0x8d66 -> :sswitch_0
    .end sparse-switch
.end method

.method public static c(I)I
    .locals 3

    .prologue
    .line 110
    sparse-switch p0, :sswitch_data_0

    .line 129
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1d

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unhandled gl type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 118
    :sswitch_0
    const/4 v0, 0x1

    .line 127
    :goto_0
    return v0

    .line 123
    :sswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 127
    :sswitch_2
    const/4 v0, 0x0

    goto :goto_0

    .line 110
    :sswitch_data_0
    .sparse-switch
        0x1404 -> :sswitch_1
        0x1406 -> :sswitch_0
        0x8b50 -> :sswitch_0
        0x8b51 -> :sswitch_0
        0x8b52 -> :sswitch_0
        0x8b53 -> :sswitch_1
        0x8b54 -> :sswitch_1
        0x8b55 -> :sswitch_1
        0x8b5a -> :sswitch_0
        0x8b5b -> :sswitch_0
        0x8b5c -> :sswitch_0
        0x8b5e -> :sswitch_2
        0x8b60 -> :sswitch_2
        0x8d66 -> :sswitch_2
    .end sparse-switch
.end method

.method public static d(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 138
    packed-switch p0, :pswitch_data_0

    .line 146
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x1f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unhandled data type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :pswitch_0
    const-string v0, "texture"

    .line 144
    :goto_0
    return-object v0

    .line 142
    :pswitch_1
    const-string v0, "int"

    goto :goto_0

    .line 144
    :pswitch_2
    const-string v0, "float"

    goto :goto_0

    .line 138
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
