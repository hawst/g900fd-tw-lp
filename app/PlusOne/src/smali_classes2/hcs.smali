.class public abstract Lhcs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhdc;


# instance fields
.field private a:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

.field private b:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 29
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterKeys()[I

    move-result-object v2

    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    aget v3, v2, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1, p2}, Ljava/util/HashSet;->containsAll(Ljava/util/Collection;)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Not all the adjustable parameters are present within the filter parameter keys!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 30
    :cond_1
    iput-object p1, p0, Lhcs;->a:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    .line 31
    iput-object p2, p0, Lhcs;->b:Ljava/util/List;

    .line 32
    return-void
.end method

.method public c()I
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lhcs;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public c(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lhcs;->a:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    invoke-virtual {p0, p1}, Lhcs;->f(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lhcs;->a:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    invoke-virtual {v0}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getActiveParameterKey()I

    move-result v0

    invoke-virtual {p0, v0}, Lhcs;->g(I)I

    move-result v0

    return v0
.end method

.method public d(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 55
    iget-object v0, p0, Lhcs;->a:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    invoke-virtual {p0, p1}, Lhcs;->f(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->b(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public e(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lhcs;->a:Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    invoke-virtual {p0, p1}, Lhcs;->f(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterValue(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public f(I)I
    .locals 1

    .prologue
    .line 68
    if-ltz p1, :cond_0

    iget-object v0, p0, Lhcs;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v0, p0, Lhcs;->b:Ljava/util/List;

    .line 69
    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    return v0

    .line 68
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 69
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public g(I)I
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lhcs;->b:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
