.class public final Lcgy;
.super Ljava/lang/Object;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lchc",
            "<TT;>;>;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lchc",
            "<TT;>;>;)V"
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-direct {p0, v0}, Lcgy;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcgy;->a:Ljava/util/List;

    .line 66
    return-void
.end method

.method private a(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lchc",
            "<TT;>;>;)",
            "Ljava/util/List",
            "<",
            "Lchc",
            "<TT;>;>;"
        }
    .end annotation

    .prologue
    .line 69
    const-string v0, "steps"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 70
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 71
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchc;

    .line 72
    iget v2, v0, Lchc;->a:I

    if-le v2, v1, :cond_0

    iget v2, v0, Lchc;->a:I

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-le v2, v3, :cond_1

    .line 73
    :cond_0
    new-instance v2, Lcha;

    iget v0, v0, Lchc;->a:I

    invoke-direct {v2, v1, v0}, Lcha;-><init>(II)V

    throw v2

    .line 70
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 76
    :cond_2
    return-object p1
.end method


# virtual methods
.method public a(Ljava/lang/Object;I)I
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;I)I"
        }
    .end annotation

    .prologue
    .line 50
    .line 51
    :goto_0
    iget-object v0, p0, Lcgy;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 52
    iget-object v0, p0, Lcgy;->a:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchc;

    .line 53
    iget-object v1, v0, Lchc;->b:Lchb;

    invoke-interface {v1, p1}, Lchb;->a(Ljava/lang/Object;)V

    .line 54
    iget p2, v0, Lchc;->a:I

    goto :goto_0

    .line 56
    :cond_0
    return p2
.end method
