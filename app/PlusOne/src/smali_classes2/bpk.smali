.class public final Lbpk;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbph;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lbpk;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final a:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation
.end field

.field private final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lbpl;

    invoke-direct {v0}, Lbpl;-><init>()V

    sput-object v0, Lbpk;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILjava/util/Collection;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Collection",
            "<",
            "Lbmd;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput p1, p0, Lbpk;->b:I

    .line 34
    const-string v0, "newClips"

    invoke-static {p2, v0}, Lcec;->a(Ljava/util/Collection;Ljava/lang/CharSequence;)Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Lcfi;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbpk;->a:Ljava/util/List;

    .line 35
    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lbpk;->b:I

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 40
    const-class v1, Lbmd;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readList(Ljava/util/List;Ljava/lang/ClassLoader;)V

    .line 41
    const-string v1, "newClips"

    invoke-static {v0, v1}, Lcec;->a(Ljava/util/Collection;Ljava/lang/CharSequence;)Ljava/util/Collection;

    .line 42
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lbpk;->a:Ljava/util/List;

    .line 43
    return-void
.end method


# virtual methods
.method public a()Lbut;
    .locals 6

    .prologue
    .line 64
    new-instance v2, Lbut;

    invoke-direct {v2}, Lbut;-><init>()V

    .line 65
    const/4 v0, 0x5

    iput v0, v2, Lbut;->b:I

    .line 66
    new-instance v3, Lbuu;

    invoke-direct {v3}, Lbuu;-><init>()V

    .line 67
    iget v0, p0, Lbpk;->b:I

    iput v0, v3, Lbuu;->a:I

    .line 68
    iget-object v0, p0, Lbpk;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v4

    .line 69
    new-array v0, v4, [Lbty;

    iput-object v0, v3, Lbuu;->b:[Lbty;

    .line 70
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 71
    iget-object v5, v3, Lbuu;->b:[Lbty;

    iget-object v0, p0, Lbpk;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    invoke-static {v0}, Lbpm;->a(Lbmd;)Lbty;

    move-result-object v0

    aput-object v0, v5, v1

    .line 70
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 73
    :cond_0
    iput-object v3, v2, Lbut;->g:Lbuu;

    .line 74
    return-object v2
.end method

.method public a(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 47
    iget v0, p0, Lbpk;->b:I

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    .line 49
    iget-object v1, p0, Lbpk;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    move v3, v2

    :goto_0
    if-ge v3, v4, :cond_0

    .line 50
    iget-object v1, p0, Lbpk;->a:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmd;

    iget-object v1, v1, Lbmd;->e:Ljeg;

    const-string v5, "mediaIdentifier"

    iget-object v6, v0, Lbmd;->e:Ljeg;

    invoke-static {v1, v5, v6}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/Object;)V

    .line 49
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 53
    :cond_0
    iget v1, p0, Lbpk;->b:I

    invoke-interface {p1, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    .line 54
    iget-object v1, p0, Lbpk;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    :goto_1
    if-ge v2, v3, :cond_1

    .line 55
    iget-object v1, p0, Lbpk;->a:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmd;

    invoke-virtual {v1}, Lbmd;->a()Lbmf;

    move-result-object v1

    iget v4, v0, Lbmd;->b:I

    .line 56
    invoke-virtual {v1, v4}, Lbmf;->b(I)Lbmf;

    move-result-object v1

    .line 57
    invoke-virtual {v1}, Lbmf;->a()Lbmd;

    move-result-object v1

    .line 58
    iget v4, p0, Lbpk;->b:I

    add-int/2addr v4, v2

    invoke-interface {p1, v4, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    .line 54
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 60
    :cond_1
    return-void
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lbpk;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 85
    iget-object v0, p0, Lbpk;->a:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeList(Ljava/util/List;)V

    .line 86
    return-void
.end method
