.class final Lgqi;
.super Lghj;

# interfaces
.implements Lhac;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lghh",
        "<",
        "Lgqm;",
        ">.ghj<",
        "Lgfy",
        "<",
        "Lhac;",
        ">;>;",
        "Lhac;"
    }
.end annotation


# instance fields
.field private final a:Lcom/google/android/gms/common/api/Status;

.field private final b:Landroid/app/PendingIntent;


# direct methods
.method constructor <init>(Lgqg;Lgfy;Lcom/google/android/gms/common/api/Status;Landroid/app/PendingIntent;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfy",
            "<",
            "Lhac;",
            ">;",
            "Lcom/google/android/gms/common/api/Status;",
            "Landroid/app/PendingIntent;",
            ")V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Lghj;-><init>(Lghh;Ljava/lang/Object;)V

    iput-object p3, p0, Lgqi;->a:Lcom/google/android/gms/common/api/Status;

    iput-object p4, p0, Lgqi;->b:Landroid/app/PendingIntent;

    return-void
.end method


# virtual methods
.method public a()Lcom/google/android/gms/common/api/Status;
    .locals 1

    iget-object v0, p0, Lgqi;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public a(Landroid/content/Context;)V
    .locals 6

    :try_start_0
    invoke-virtual {p0}, Lgqi;->b()Landroid/app/PendingIntent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/Context;->startIntentSender(Landroid/content/IntentSender;Landroid/content/Intent;III)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/IntentSender$SendIntentException;->printStackTrace()V

    goto :goto_0
.end method

.method protected a(Lgfy;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfy",
            "<",
            "Lhac;",
            ">;)V"
        }
    .end annotation

    invoke-interface {p1, p0}, Lgfy;->a(Ljava/lang/Object;)V

    return-void
.end method

.method protected synthetic a(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lgfy;

    invoke-virtual {p0, p1}, Lgqi;->a(Lgfy;)V

    return-void
.end method

.method public b()Landroid/app/PendingIntent;
    .locals 1

    iget-object v0, p0, Lgqi;->b:Landroid/app/PendingIntent;

    return-object v0
.end method

.method protected c()V
    .locals 0

    return-void
.end method
