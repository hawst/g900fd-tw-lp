.class public final Lbpx;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Lons;)Lbmp;
    .locals 6

    .prologue
    .line 34
    new-instance v0, Lbmp;

    iget-object v1, p0, Lons;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v1, p0, Lons;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lbmp;-><init>(JJ)V

    return-object v0
.end method

.method public static a(Lbmp;Ljava/util/Collection;)Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbmp;",
            "Ljava/util/Collection",
            "<",
            "Lbmp;",
            ">;)",
            "Ljava/util/List",
            "<",
            "Lbmp;",
            ">;"
        }
    .end annotation

    .prologue
    .line 57
    invoke-static {}, Lbpu;->a()Lbpu;

    move-result-object v1

    .line 58
    invoke-virtual {v1, p0}, Lbpu;->a(Lbmp;)V

    .line 59
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmp;

    .line 60
    invoke-virtual {v1, v0}, Lbpu;->b(Lbmp;)V

    goto :goto_0

    .line 63
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 64
    invoke-virtual {v1}, Lbpu;->b()Ljava/lang/Iterable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmp;

    .line 65
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 67
    :cond_1
    return-object v2
.end method

.method public static a(Lbmp;)Lons;
    .locals 4

    .prologue
    .line 42
    new-instance v0, Lons;

    invoke-direct {v0}, Lons;-><init>()V

    .line 43
    iget-wide v2, p0, Lbmp;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lons;->b:Ljava/lang/Long;

    .line 44
    iget-wide v2, p0, Lbmp;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iput-object v1, v0, Lons;->c:Ljava/lang/Long;

    .line 45
    return-object v0
.end method

.method public static a(Lbmp;Lbmp;)Z
    .locals 4

    .prologue
    .line 105
    iget-wide v0, p1, Lbmp;->b:J

    iget-wide v2, p0, Lbmp;->b:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    iget-wide v0, p1, Lbmp;->c:J

    iget-wide v2, p0, Lbmp;->c:J

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
