.class public final Lbjo;
.super Lbjl;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lbjl",
        "<",
        "Lbhl;",
        ">;"
    }
.end annotation


# instance fields
.field private final c:Lbgf;

.field private d:Lbjr;


# direct methods
.method public constructor <init>(Lbgf;Lbiw;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lbjo;-><init>(Lbgf;Lbiw;Ljava/util/concurrent/Executor;Lbjr;)V

    .line 37
    return-void
.end method

.method constructor <init>(Lbgf;Lbiw;Ljava/util/concurrent/Executor;Lbjr;)V
    .locals 2

    .prologue
    .line 45
    invoke-direct {p0, p2, p3}, Lbjl;-><init>(Lbiw;Ljava/util/concurrent/Executor;)V

    .line 47
    const-string v0, "renderContext"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgf;

    iput-object v0, p0, Lbjo;->c:Lbgf;

    .line 48
    iput-object p4, p0, Lbjo;->d:Lbjr;

    .line 49
    return-void
.end method


# virtual methods
.method public a(Landroid/media/MediaFormat;)Lbji;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/media/MediaFormat;",
            ")",
            "Lbji",
            "<",
            "Lbhl;",
            ">;"
        }
    .end annotation

    .prologue
    .line 53
    invoke-static {p1}, Lbkf;->b(Landroid/media/MediaFormat;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x2f

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "invalid media format for video decoder factory "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    .line 58
    :cond_0
    iget-object v0, p0, Lbjo;->d:Lbjr;

    if-nez v0, :cond_1

    .line 59
    iget-object v0, p0, Lbjo;->a:Lbiw;

    .line 60
    invoke-interface {v0}, Lbiw;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 61
    invoke-static {}, Lbjt;->a()Lbjr;

    move-result-object v0

    .line 62
    :goto_0
    iput-object v0, p0, Lbjo;->d:Lbjr;

    .line 65
    :cond_1
    iget-object v0, p0, Lbjo;->d:Lbjr;

    .line 66
    invoke-interface {v0, p1}, Lbjr;->a(Landroid/media/MediaFormat;)Lbjs;

    move-result-object v0

    iget-object v1, p0, Lbjo;->c:Lbgf;

    .line 67
    invoke-virtual {v1}, Lbgf;->e()Lbfw;

    move-result-object v1

    iget-object v2, p0, Lbjo;->b:Ljava/util/concurrent/Executor;

    .line 65
    invoke-static {v0, v1, p1, v2}, Lbkc;->a(Lbjs;Lbhb;Landroid/media/MediaFormat;Ljava/util/concurrent/Executor;)Lbkc;

    move-result-object v0

    return-object v0

    .line 62
    :cond_2
    invoke-static {}, Lbjt;->c()Lbjr;

    move-result-object v0

    goto :goto_0
.end method
