.class public Lbqk;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lbjp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjp",
            "<",
            "Lbhl;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Lbig;

.field private final e:Lbjf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lbqk;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbqk;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lbjp;Lbig;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lbjp",
            "<",
            "Lbhl;",
            ">;",
            "Lbig;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    const-string v0, "context"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbqk;->b:Landroid/content/Context;

    .line 78
    const-string v0, "decoderPool"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjp;

    iput-object v0, p0, Lbqk;->c:Lbjp;

    .line 79
    const-string v0, "mediaExtractorFactory"

    .line 80
    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbig;

    iput-object v0, p0, Lbqk;->d:Lbig;

    .line 82
    new-instance v0, Lbjf;

    invoke-direct {v0}, Lbjf;-><init>()V

    iput-object v0, p0, Lbqk;->e:Lbjf;

    .line 83
    return-void
.end method

.method private a(Lbmw;II)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    .line 186
    const-string v0, "targetWidth"

    invoke-static {p2, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    .line 187
    const-string v0, "targetHeight"

    invoke-static {p3, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    .line 190
    mul-int v0, p2, p3

    int-to-double v0, v0

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    .line 191
    invoke-virtual {p1}, Lbmw;->a()Ljeg;

    move-result-object v1

    iget-object v1, v1, Ljeg;->b:Landroid/net/Uri;

    .line 192
    iget-object v2, p0, Lbqk;->e:Lbjf;

    iget-object v3, p0, Lbqk;->b:Landroid/content/Context;

    .line 193
    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v2, v1, v3, v0}, Lbjf;->a(Landroid/net/Uri;Landroid/content/ContentResolver;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 195
    invoke-virtual {p1}, Lbmw;->g()Lbmx;

    move-result-object v1

    iget v1, v1, Lbmx;->a:I

    .line 196
    if-lez v1, :cond_0

    .line 197
    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    .line 198
    int-to-float v1, v1

    invoke-virtual {v5, v1}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 199
    const/4 v1, 0x0

    const/4 v2, 0x0

    .line 200
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    .line 199
    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 201
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    .line 209
    :cond_0
    :goto_0
    return-object v0

    .line 206
    :catch_0
    move-exception v0

    .line 207
    sget-object v1, Lbqk;->a:Ljava/lang/String;

    const-string v2, "Error decoding bitmap."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 209
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lbqk;)Lbig;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lbqk;->d:Lbig;

    return-object v0
.end method

.method static synthetic a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    sget-object v0, Lbqk;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Landroid/graphics/Matrix;I)V
    .locals 1

    .prologue
    .line 44
    invoke-static {p1}, Lbqu;->a(I)[F

    move-result-object v0

    invoke-static {p0, v0}, Lcfn;->a(Landroid/graphics/Matrix;[F)V

    return-void
.end method

.method static synthetic b(Lbqk;)Lbjp;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lbqk;->c:Lbjp;

    return-object v0
.end method


# virtual methods
.method public a(Lbml;II)F
    .locals 8

    .prologue
    const/4 v3, 0x0

    const/16 v7, 0x400

    const/4 v0, 0x1

    const/4 v1, 0x0

    const/high16 v6, 0x44800000    # 1024.0f

    .line 91
    const-string v2, "targetWidth"

    invoke-static {p2, v2, v3}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    .line 92
    const-string v2, "targetHeight"

    invoke-static {p3, v2, v3}, Lcec;->a(ILjava/lang/CharSequence;Ljava/lang/CharSequence;)I

    .line 93
    if-nez p2, :cond_1

    move v2, v0

    :goto_0
    if-nez p3, :cond_2

    :goto_1
    xor-int/2addr v0, v2

    const-string v1, "one of target width/height must be zero"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 96
    invoke-static {p1}, Lbqu;->a(Lbml;)I

    move-result v0

    int-to-float v1, v0

    .line 97
    invoke-static {p1}, Lbqu;->b(Lbml;)I

    move-result v0

    int-to-float v2, v0

    .line 99
    if-nez p2, :cond_3

    int-to-float v0, p3

    div-float/2addr v0, v2

    .line 102
    :goto_2
    mul-float v3, v0, v1

    float-to-double v4, v3

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v3, v4

    .line 103
    mul-float v4, v0, v2

    float-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-int v4, v4

    .line 104
    if-le v3, v4, :cond_4

    if-le v3, v7, :cond_4

    .line 105
    div-float v0, v6, v1

    .line 107
    mul-float v1, v2, v0

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    .line 119
    :cond_0
    :goto_3
    return v0

    :cond_1
    move v2, v1

    .line 93
    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    .line 99
    :cond_3
    int-to-float v0, p2

    div-float/2addr v0, v1

    goto :goto_2

    .line 108
    :cond_4
    if-le v4, v7, :cond_0

    .line 109
    div-float v0, v6, v2

    .line 111
    mul-float/2addr v1, v0

    float-to-double v2, v1

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    goto :goto_3
.end method

.method public a(Lbgf;Lbqp;)Lbqt;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 126
    :try_start_0
    const-string v0, "renderContext"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 127
    const-string v0, "posterInfo"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 129
    iget-object v1, p2, Lbqp;->a:Lbmm;

    .line 131
    iget-boolean v0, v1, Lbmm;->b:Z

    if-eqz v0, :cond_0

    .line 132
    invoke-virtual {v1}, Lbmm;->a()Lbmw;

    move-result-object v0

    iget v1, p2, Lbqp;->b:I

    iget v2, p2, Lbqp;->c:I

    invoke-direct {p0, v0, v1, v2}, Lbqk;->a(Lbmw;II)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v1, v0

    .line 148
    :goto_0
    if-eqz v1, :cond_1

    new-instance v0, Lbqt;

    invoke-direct {v0, v1}, Lbqt;-><init>(Landroid/graphics/Bitmap;)V

    .line 153
    :goto_1
    return-object v0

    .line 135
    :cond_0
    new-instance v0, Lbqn;

    .line 137
    invoke-virtual {v1}, Lbmm;->b()Lbon;

    move-result-object v3

    const/4 v2, 0x1

    new-array v4, v2, [J

    const/4 v2, 0x0

    iget-wide v6, v1, Lbmm;->c:J

    aput-wide v6, v4, v2

    iget v5, p2, Lbqp;->b:I

    iget v6, p2, Lbqp;->c:I

    new-instance v7, Lbql;

    invoke-direct {v7}, Lbql;-><init>()V

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v7}, Lbqn;-><init>(Lbqk;Lbgf;Lbon;[JIILbqm;)V

    .line 135
    invoke-virtual {p1, v0}, Lbgf;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-object v1, v0

    goto :goto_0

    :cond_1
    move-object v0, v8

    .line 148
    goto :goto_1

    .line 150
    :catch_0
    move-exception v0

    move-object v0, v8

    goto :goto_1

    .line 152
    :catch_1
    move-exception v0

    sget-object v0, Lbqk;->a:Ljava/lang/String;

    move-object v0, v8

    .line 153
    goto :goto_1
.end method

.method public varargs a(Lbgf;Lbqm;Lbon;II[J)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 170
    const-string v0, "renderContext"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 171
    const-string v0, "video"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 172
    const-string v0, "displayWidth"

    invoke-static {p4, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    .line 173
    const-string v0, "displayHeight"

    invoke-static {p5, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    .line 175
    :try_start_0
    new-instance v0, Lbqn;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p6

    move v5, p4

    move v6, p5

    move-object v7, p2

    invoke-direct/range {v0 .. v7}, Lbqn;-><init>(Lbqk;Lbgf;Lbon;[JIILbqm;)V

    invoke-virtual {p1, v0}, Lbgf;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    :goto_0
    return-void

    .line 180
    :catch_0
    move-exception v0

    sget-object v0, Lbqk;->a:Ljava/lang/String;

    goto :goto_0

    .line 181
    :catch_1
    move-exception v0

    goto :goto_0
.end method
