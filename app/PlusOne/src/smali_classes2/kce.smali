.class final Lkce;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheg;
.implements Lhkd;
.implements Lhob;
.implements Llnx;
.implements Llrb;
.implements Llrc;
.implements Llrg;


# instance fields
.field final a:Lz;

.field private b:Lhke;

.field private c:Lhee;

.field private d:Lhei;

.field private e:Lhoc;

.field private f:Lkbz;

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Lz;Llqr;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lkce;->a:Lz;

    .line 61
    invoke-virtual {p2, p0}, Llqr;->a(Llrg;)Llrg;

    .line 62
    return-void
.end method

.method private a()V
    .locals 5

    .prologue
    .line 96
    iget-boolean v0, p0, Lkce;->g:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lkce;->h:Z

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lkce;->c:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 98
    iget-object v1, p0, Lkce;->d:Lhei;

    invoke-interface {v1, v0}, Lhei;->c(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lkce;->f:Lkbz;

    iget-object v2, p0, Lkce;->a:Lz;

    .line 99
    invoke-virtual {v1, v2, v0}, Lkbz;->a(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lkce;->d:Lhei;

    .line 100
    invoke-interface {v1, v0}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v2, "suspension_status_checked"

    invoke-interface {v1, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 101
    iget-object v1, p0, Lkce;->e:Lhoc;

    new-instance v2, Lkcb;

    iget-object v3, p0, Lkce;->a:Lz;

    const-string v4, "GetProfileSuspensionInfoTask"

    invoke-direct {v2, v3, v0, v4}, Lkcb;-><init>(Landroid/content/Context;ILjava/lang/String;)V

    invoke-virtual {v1, v2}, Lhoc;->b(Lhny;)V

    .line 105
    :cond_0
    return-void
.end method


# virtual methods
.method public a(ILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 145
    const v0, 0x7f100007

    if-ne p1, v0, :cond_0

    .line 146
    if-eqz p2, :cond_0

    const-string v0, "name_violation"

    const/4 v1, 0x0

    .line 147
    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    new-instance v0, Lkcf;

    invoke-direct {v0, p0}, Lkcf;-><init>(Lkce;)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    .line 162
    :cond_0
    return-void
.end method

.method public a(Landroid/content/Context;Llnh;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 65
    const-class v0, Lhee;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lkce;->c:Lhee;

    .line 66
    const-class v0, Lhei;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lkce;->d:Lhei;

    .line 67
    const-class v0, Lhoc;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Lkce;->e:Lhoc;

    .line 68
    const-class v0, Lhke;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhke;

    iput-object v0, p0, Lkce;->b:Lhke;

    .line 69
    const-class v0, Lkbz;

    invoke-virtual {p2, v0}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkbz;

    iput-object v0, p0, Lkce;->f:Lkbz;

    .line 70
    iget-object v0, p0, Lkce;->c:Lhee;

    invoke-interface {v0, p0}, Lhee;->a(Lheg;)Lhee;

    .line 71
    iget-object v0, p0, Lkce;->e:Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 72
    iget-object v0, p0, Lkce;->b:Lhke;

    const v1, 0x7f100007

    invoke-virtual {v0, v1, p0}, Lhke;->a(ILhkd;)Lhke;

    .line 73
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 109
    const-string v0, "GetProfileSuspensionInfoTask"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {p2}, Lhoz;->d()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "psi_response"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lhyt;

    .line 113
    iget-object v1, p0, Lkce;->d:Lhei;

    iget-object v2, p0, Lkce;->c:Lhee;

    .line 114
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-interface {v1, v2}, Lhei;->b(I)Lhek;

    move-result-object v1

    const-string v2, "suspension_status_checked"

    const/4 v3, 0x1

    .line 115
    invoke-interface {v1, v2, v3}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v1

    .line 116
    invoke-interface {v1}, Lhek;->c()I

    .line 118
    if-eqz v0, :cond_0

    .line 119
    new-instance v1, Lnkq;

    invoke-direct {v1}, Lnkq;-><init>()V

    invoke-virtual {v0, v1}, Lhyt;->a(Loxu;)Loxu;

    move-result-object v0

    check-cast v0, Lnkq;

    .line 121
    iget-object v1, v0, Lnkq;->a:[Lnkp;

    if-eqz v1, :cond_1

    .line 122
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lkce;->a:Lz;

    const-class v3, Lcom/google/android/libraries/social/profile/suspension/impl/ProfileSuspensionActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 123
    const-string v2, "account_id"

    iget-object v3, p0, Lkce;->c:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 124
    const-string v2, "extra_title"

    iget-object v3, v0, Lnkq;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 125
    const-string v2, "extra_message"

    iget-object v3, v0, Lnkq;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 126
    const-string v2, "extra_profile_suspension_info"

    .line 127
    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    .line 126
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 129
    iget-object v0, p0, Lkce;->b:Lhke;

    const v2, 0x7f100007

    invoke-virtual {v0, v2, v1}, Lhke;->a(ILandroid/content/Intent;)V

    .line 142
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    invoke-virtual {p3, v4}, Lhos;->a(Z)V

    .line 133
    iget-object v1, p0, Lkce;->a:Lz;

    invoke-virtual {v1}, Lz;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 134
    iget-object v2, v0, Lnkq;->b:Ljava/lang/String;

    iget-object v0, v0, Lnkq;->c:Ljava/lang/String;

    const v3, 0x7f0a0583

    .line 135
    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 134
    invoke-static {v2, v0, v1, v5}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    .line 137
    invoke-virtual {v0, v4}, Llgr;->b(Z)V

    .line 138
    iget-object v1, p0, Lkce;->a:Lz;

    invoke-virtual {v1}, Lz;->f()Lae;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Llgr;->a(Lae;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(ZIIII)V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkce;->g:Z

    .line 80
    invoke-direct {p0}, Lkce;->a()V

    .line 81
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkce;->h:Z

    .line 85
    invoke-direct {p0}, Lkce;->a()V

    .line 86
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkce;->h:Z

    .line 90
    return-void
.end method
