.class public final Lzp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Labf;

.field private b:[I

.field private c:J

.field private final d:Laat;

.field private e:Ljava/util/Vector;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Vector",
            "<",
            "Lzr;",
            ">;"
        }
    .end annotation
.end field

.field private f:Z

.field private g:I

.field private h:I

.field private i:Lzr;

.field private j:Lzr;


# direct methods
.method public constructor <init>(Labf;[ILaat;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v0, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lzp;->c:J

    .line 60
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    iput-object v1, p0, Lzp;->e:Ljava/util/Vector;

    .line 62
    iput-boolean v4, p0, Lzp;->f:Z

    .line 63
    iput v4, p0, Lzp;->g:I

    .line 65
    const/4 v1, 0x1

    iput v1, p0, Lzp;->h:I

    .line 68
    iput-object v0, p0, Lzp;->i:Lzr;

    .line 71
    iput-object v0, p0, Lzp;->j:Lzr;

    .line 75
    iput-object p1, p0, Lzp;->a:Labf;

    .line 76
    if-eqz p2, :cond_0

    array-length v0, p2

    invoke-static {p2, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    :cond_0
    iput-object v0, p0, Lzp;->b:[I

    .line 77
    iput-object p3, p0, Lzp;->d:Laat;

    .line 78
    return-void
.end method

.method private a(Lzr;)V
    .locals 1

    .prologue
    .line 293
    iget-object v0, p0, Lzp;->d:Laat;

    invoke-virtual {v0, p1}, Laat;->b(Lzr;)V

    .line 294
    return-void
.end method

.method private a(Lzr;I)V
    .locals 3

    .prologue
    .line 297
    const/4 v0, 0x2

    if-ne p2, v0, :cond_4

    .line 299
    iget v0, p0, Lzp;->g:I

    if-lez v0, :cond_0

    .line 300
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Attempting to write-lock the read-locked frame "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 302
    :cond_0
    iget-boolean v0, p0, Lzp;->f:Z

    if-eqz v0, :cond_1

    .line 303
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Attempting to write-lock the write-locked frame "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 308
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lzp;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 309
    iget-object v0, p0, Lzp;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzr;

    .line 310
    if-eq v0, p1, :cond_2

    .line 311
    invoke-virtual {v0}, Lzr;->n()V

    .line 308
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 314
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lzp;->f:Z

    .line 315
    iput-object p1, p0, Lzp;->i:Lzr;

    .line 322
    :goto_1
    iput-object p1, p0, Lzp;->j:Lzr;

    .line 323
    return-void

    .line 317
    :cond_4
    iget-boolean v0, p0, Lzp;->f:Z

    if-eqz v0, :cond_5

    .line 318
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Attempting to read-lock locked frame "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 320
    :cond_5
    iget v0, p0, Lzp;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lzp;->g:I

    goto :goto_1
.end method

.method private c(II)Lzr;
    .locals 4

    .prologue
    .line 226
    const/4 v0, 0x0

    .line 227
    iget-object v1, p0, Lzp;->a:Labf;

    invoke-virtual {v1}, Labf;->f()I

    move-result v2

    .line 228
    const/16 v1, 0x8

    if-eq p2, v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    .line 229
    iget-object v0, p0, Lzp;->d:Laat;

    iget-object v1, p0, Lzp;->b:[I

    invoke-virtual {v0, p1, p2, v1, v2}, Laat;->a(II[II)Lzr;

    move-result-object v0

    .line 231
    :cond_0
    if-nez v0, :cond_6

    .line 232
    sparse-switch p2, :sswitch_data_0

    .line 255
    :goto_1
    if-nez v0, :cond_4

    .line 256
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not create backing for access type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 228
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 234
    :sswitch_0
    new-instance v0, Lzt;

    invoke-direct {v0}, Lzt;-><init>()V

    goto :goto_1

    .line 238
    :sswitch_1
    new-instance v0, Lzv;

    invoke-direct {v0}, Lzv;-><init>()V

    goto :goto_1

    .line 241
    :sswitch_2
    new-instance v0, Lzu;

    invoke-direct {v0}, Lzu;-><init>()V

    goto :goto_1

    .line 244
    :sswitch_3
    new-instance v0, Lzs;

    invoke-direct {v0}, Lzs;-><init>()V

    goto :goto_1

    .line 247
    :sswitch_4
    invoke-static {}, Lzq;->i()Z

    move-result v0

    if-nez v0, :cond_2

    .line 248
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempted to create an AllocationBacking in context that does not support RenderScript!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 252
    :cond_2
    new-instance v0, Lzq;

    iget-object v1, p0, Lzp;->d:Laat;

    invoke-virtual {v1}, Laat;->b()Lacs;

    move-result-object v1

    iget-object v3, v1, Lacs;->c:Landroid/renderscript/RenderScript;

    if-nez v3, :cond_3

    iget-object v3, v1, Lacs;->a:Landroid/content/Context;

    invoke-static {v3}, Landroid/renderscript/RenderScript;->create(Landroid/content/Context;)Landroid/renderscript/RenderScript;

    move-result-object v3

    iput-object v3, v1, Lacs;->c:Landroid/renderscript/RenderScript;

    :cond_3
    iget-object v1, v1, Lacs;->c:Landroid/renderscript/RenderScript;

    invoke-direct {v0, v1}, Lzq;-><init>(Landroid/renderscript/RenderScript;)V

    goto :goto_1

    .line 259
    :cond_4
    invoke-virtual {v0}, Lzr;->c()Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lzp;->d:Laat;

    invoke-virtual {v1}, Laat;->c()Labx;

    move-result-object v1

    invoke-virtual {v1}, Labx;->i()Z

    move-result v1

    if-nez v1, :cond_5

    .line 260
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot create backing that requires GPU in a runner that does not support OpenGL!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 264
    :cond_5
    iget-object v1, p0, Lzp;->b:[I

    invoke-virtual {v0, v1}, Lzr;->a([I)V

    .line 265
    invoke-virtual {v0, v2}, Lzr;->b(I)V

    .line 266
    iget-object v1, p0, Lzp;->a:Labf;

    invoke-virtual {v1}, Labf;->e()I

    move-result v1

    invoke-virtual {v0, v1}, Lzr;->c(I)V

    .line 267
    iget-object v1, p0, Lzp;->a:Labf;

    invoke-virtual {v0, v1}, Lzr;->a(Labf;)V

    .line 268
    iget-object v1, p0, Lzp;->d:Laat;

    invoke-virtual {v1, v0}, Laat;->a(Lzr;)V

    .line 270
    :cond_6
    return-object v0

    .line 232
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_1
        0x8 -> :sswitch_2
        0x10 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method private h()V
    .locals 2

    .prologue
    .line 285
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lzp;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 286
    iget-object v0, p0, Lzp;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzr;

    invoke-direct {p0, v0}, Lzp;->a(Lzr;)V

    .line 285
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 288
    :cond_0
    iget-object v0, p0, Lzp;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->clear()V

    .line 289
    const/4 v0, 0x0

    iput-object v0, p0, Lzp;->i:Lzr;

    .line 290
    return-void
.end method


# virtual methods
.method public a()Labf;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lzp;->a:Labf;

    return-object v0
.end method

.method public a(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0, p1, p2}, Lzp;->b(II)Lzr;

    move-result-object v0

    invoke-virtual {v0, p2}, Lzr;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 182
    iput-wide p1, p0, Lzp;->c:J

    .line 183
    return-void
.end method

.method public a(Lzp;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 165
    iget-object v0, p1, Lzp;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 166
    iget-object v0, p1, Lzp;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->firstElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzr;

    invoke-virtual {v0}, Lzr;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    invoke-direct {p0, v2, v1}, Lzp;->c(II)Lzr;

    move-result-object v1

    invoke-virtual {v1, v0}, Lzr;->a(Lzr;)V

    iget-object v0, p0, Lzp;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    iput-object v1, p0, Lzp;->i:Lzr;

    .line 168
    :cond_0
    iget-wide v0, p1, Lzp;->c:J

    iput-wide v0, p0, Lzp;->c:J

    .line 169
    return-void

    .line 166
    :cond_1
    invoke-virtual {v0}, Lzr;->a()I

    move-result v1

    goto :goto_0
.end method

.method public a([I)V
    .locals 4

    .prologue
    .line 137
    new-instance v1, Ljava/util/Vector;

    invoke-direct {v1}, Ljava/util/Vector;-><init>()V

    .line 138
    iget-object v0, p0, Lzp;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzr;

    .line 139
    invoke-virtual {v0}, Lzr;->m()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 140
    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 142
    :cond_0
    invoke-direct {p0, v0}, Lzp;->a(Lzr;)V

    goto :goto_0

    .line 145
    :cond_1
    iput-object v1, p0, Lzp;->e:Ljava/util/Vector;

    .line 146
    iput-object p1, p0, Lzp;->b:[I

    .line 147
    return-void
.end method

.method public b(II)Lzr;
    .locals 3

    .prologue
    .line 89
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lzp;->e:Ljava/util/Vector;

    invoke-virtual {v0}, Ljava/util/Vector;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    iget-object v0, p0, Lzp;->e:Ljava/util/Vector;

    invoke-virtual {v0, v1}, Ljava/util/Vector;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lzr;

    const/4 v2, 0x2

    if-ne p1, v2, :cond_3

    invoke-virtual {v0}, Lzr;->b()I

    move-result v2

    :goto_1
    and-int/2addr v2, p2

    if-ne v2, p2, :cond_4

    :goto_2
    if-nez v0, :cond_1

    invoke-direct {p0, p1, p2}, Lzp;->c(II)Lzr;

    move-result-object v0

    iget-object v1, p0, Lzp;->e:Ljava/util/Vector;

    invoke-virtual {v1}, Ljava/util/Vector;->size()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {v0}, Lzr;->n()V

    :cond_0
    iget-object v1, p0, Lzp;->e:Ljava/util/Vector;

    invoke-virtual {v1, v0}, Ljava/util/Vector;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lzr;->o()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lzp;->i:Lzr;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lzp;->i:Lzr;

    invoke-virtual {v0, v1}, Lzr;->a(Lzr;)V

    .line 90
    :cond_2
    if-nez v0, :cond_6

    .line 91
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Could not fetch frame data!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 89
    :cond_3
    invoke-virtual {v0}, Lzr;->a()I

    move-result v2

    goto :goto_1

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_5
    const/4 v0, 0x0

    goto :goto_2

    .line 93
    :cond_6
    invoke-direct {p0, v0, p1}, Lzp;->a(Lzr;I)V

    .line 94
    return-object v0
.end method

.method public b()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 98
    iget-boolean v1, p0, Lzp;->f:Z

    if-eqz v1, :cond_1

    .line 99
    iput-boolean v0, p0, Lzp;->f:Z

    .line 105
    :goto_0
    iget-object v0, p0, Lzp;->j:Lzr;

    invoke-virtual {v0}, Lzr;->d()V

    .line 106
    const/4 v0, 0x0

    iput-object v0, p0, Lzp;->j:Lzr;

    .line 107
    const/4 v0, 0x1

    :cond_0
    return v0

    .line 100
    :cond_1
    iget v1, p0, Lzp;->g:I

    if-lez v1, :cond_0

    .line 101
    iget v0, p0, Lzp;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lzp;->g:I

    goto :goto_0
.end method

.method public c()Lzp;
    .locals 2

    .prologue
    .line 111
    iget v0, p0, Lzp;->h:I

    const/16 v1, 0xa

    if-lt v0, v1, :cond_0

    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "High ref-count of "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lzp;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " on "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "!"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    :cond_0
    iget v0, p0, Lzp;->h:I

    if-gtz v0, :cond_1

    .line 115
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "RETAINING RELEASED"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :cond_1
    iget v0, p0, Lzp;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lzp;->h:I

    .line 118
    return-object p0
.end method

.method public d()Lzp;
    .locals 2

    .prologue
    .line 122
    iget v0, p0, Lzp;->h:I

    if-gtz v0, :cond_0

    .line 123
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "DOUBLE-RELEASE"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_0
    iget v0, p0, Lzp;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lzp;->h:I

    .line 126
    iget v0, p0, Lzp;->h:I

    if-nez v0, :cond_1

    .line 127
    invoke-direct {p0}, Lzp;->h()V

    .line 128
    const/4 p0, 0x0

    .line 130
    :cond_1
    return-object p0
.end method

.method public e()[I
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lzp;->b:[I

    return-object v0
.end method

.method public f()I
    .locals 5

    .prologue
    .line 154
    const/4 v0, 0x1

    .line 155
    iget-object v1, p0, Lzp;->b:[I

    if-eqz v1, :cond_0

    .line 156
    iget-object v3, p0, Lzp;->b:[I

    array-length v4, v3

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_0

    aget v2, v3, v1

    .line 157
    mul-int/2addr v2, v0

    .line 156
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    move v0, v2

    goto :goto_0

    .line 160
    :cond_0
    return v0
.end method

.method public g()J
    .locals 2

    .prologue
    .line 175
    iget-wide v0, p0, Lzp;->c:J

    return-wide v0
.end method
