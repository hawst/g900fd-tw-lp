.class public final Lbxg;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:J

.field public b:F

.field public c:F


# direct methods
.method public constructor <init>(J)V
    .locals 3

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const-string v0, "timestampUs"

    invoke-static {p1, p2, v0}, Lcec;->b(JLjava/lang/CharSequence;)J

    move-result-wide v0

    iput-wide v0, p0, Lbxg;->a:J

    .line 29
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 39
    if-ne p0, p1, :cond_1

    .line 47
    :cond_0
    :goto_0
    return v0

    .line 42
    :cond_1
    instance-of v2, p1, Lbxg;

    if-nez v2, :cond_2

    move v0, v1

    .line 43
    goto :goto_0

    .line 46
    :cond_2
    check-cast p1, Lbxg;

    .line 47
    iget-wide v2, p0, Lbxg;->a:J

    iget-wide v4, p1, Lbxg;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget v2, p0, Lbxg;->b:F

    iget v3, p1, Lbxg;->b:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_3

    iget v2, p0, Lbxg;->c:F

    iget v3, p1, Lbxg;->c:F

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    .line 53
    iget-wide v0, p0, Lbxg;->a:J

    iget v2, p0, Lbxg;->b:F

    iget v3, p0, Lbxg;->c:F

    const/16 v4, 0x11

    .line 54
    invoke-static {v3, v4}, Lbqh;->a(FI)I

    move-result v3

    invoke-static {v2, v3}, Lbqh;->a(FI)I

    move-result v2

    .line 53
    invoke-static {v0, v1, v2}, Lbqh;->a(JI)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 59
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lbxg;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lbxg;->b:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lbxg;->c:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
