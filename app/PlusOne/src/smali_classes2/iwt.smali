.class public final Liwt;
.super Lloj;
.source "PG"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private Q:Liwa;

.field private R:Lhei;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lloj;-><init>()V

    return-void
.end method

.method public static a(Lae;Ljava/lang/String;[IZ)V
    .locals 2

    .prologue
    .line 31
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 32
    const-string v1, "title"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    const-string v1, "account_ids"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 34
    const-string v1, "add_account_enabled"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 35
    new-instance v1, Liwt;

    invoke-direct {v1}, Liwt;-><init>()V

    .line 36
    invoke-virtual {v1, v0}, Liwt;->f(Landroid/os/Bundle;)V

    .line 37
    const-string v0, "account.selector"

    invoke-virtual {v1, p0, v0}, Liwt;->a(Lae;Ljava/lang/String;)V

    .line 38
    return-void
.end method


# virtual methods
.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    .line 49
    new-instance v0, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Liwt;->n()Lz;

    move-result-object v1

    const v2, 0x7f090066

    invoke-direct {v0, v1, v2}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    .line 50
    invoke-virtual {p0}, Liwt;->k()Landroid/os/Bundle;

    move-result-object v1

    .line 51
    const-string v2, "title"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 52
    const-string v3, "account_ids"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v3

    .line 53
    const-string v4, "add_account_enabled"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 54
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 55
    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v4, Liws;

    invoke-direct {v4, v0, v3, v1}, Liws;-><init>(Landroid/content/Context;[IZ)V

    .line 56
    invoke-virtual {v2, v4, p0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const/4 v1, 0x0

    .line 57
    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 58
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method protected k(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 42
    invoke-super {p0, p1}, Lloj;->k(Landroid/os/Bundle;)V

    .line 43
    iget-object v0, p0, Liwt;->O:Llnh;

    const-class v1, Liwa;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwa;

    iput-object v0, p0, Liwt;->Q:Liwa;

    .line 44
    iget-object v0, p0, Liwt;->O:Llnh;

    const-class v1, Lhei;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Liwt;->R:Lhei;

    .line 45
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Liwt;->Q:Liwa;

    invoke-interface {v0}, Liwa;->c()V

    .line 89
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 63
    invoke-virtual {p0}, Liwt;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_ids"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v2

    .line 64
    invoke-virtual {p0}, Liwt;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "add_account_enabled"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 66
    iget-object v0, p0, Liwt;->O:Llnh;

    const-class v3, Liwa;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwa;

    .line 68
    if-eqz v1, :cond_0

    array-length v1, v2

    if-ne p2, v1, :cond_0

    const/4 v1, 0x1

    .line 69
    :goto_0
    if-nez v1, :cond_2

    .line 70
    aget v1, v2, p2

    .line 71
    iget-object v2, p0, Liwt;->R:Lhei;

    invoke-interface {v2, v1}, Lhei;->c(I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 73
    invoke-interface {v0}, Liwa;->c()V

    .line 84
    :goto_1
    return-void

    .line 68
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 77
    :cond_1
    iget-object v2, p0, Liwt;->R:Lhei;

    invoke-interface {v2, v1}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 78
    const-string v2, "account_name"

    .line 79
    invoke-interface {v1, v2}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "effective_gaia_id"

    .line 80
    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 78
    invoke-interface {v0, v2, v1}, Liwa;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 82
    :cond_2
    invoke-interface {v0}, Liwa;->b()V

    goto :goto_1
.end method
