.class public final enum Laib;
.super Ljava/lang/Enum;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Laib;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum a:Laib;

.field public static final enum b:Laib;

.field public static final enum c:Laib;

.field public static final enum d:Laib;

.field public static final enum e:Laib;

.field private static enum g:Laib;

.field private static final synthetic h:[Laib;


# instance fields
.field public final f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 32
    new-instance v0, Laib;

    const-string v1, "POST_CAPTURE"

    invoke-direct {v0, v1, v3, v3}, Laib;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Laib;->a:Laib;

    .line 34
    new-instance v0, Laib;

    const-string v1, "PLUGGED_IN"

    invoke-direct {v0, v1, v4, v3}, Laib;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Laib;->b:Laib;

    .line 36
    new-instance v0, Laib;

    const-string v1, "PRE_PREVIEW"

    invoke-direct {v0, v1, v5, v4}, Laib;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Laib;->c:Laib;

    .line 38
    new-instance v0, Laib;

    const-string v1, "EVALUATION"

    invoke-direct {v0, v1, v6, v4}, Laib;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Laib;->g:Laib;

    .line 40
    new-instance v0, Laib;

    const-string v1, "LATE_ANALYSIS"

    invoke-direct {v0, v1, v7, v4}, Laib;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Laib;->d:Laib;

    .line 42
    new-instance v0, Laib;

    const-string v1, "POST_SYNC"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2, v3}, Laib;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Laib;->e:Laib;

    .line 30
    const/4 v0, 0x6

    new-array v0, v0, [Laib;

    sget-object v1, Laib;->a:Laib;

    aput-object v1, v0, v3

    sget-object v1, Laib;->b:Laib;

    aput-object v1, v0, v4

    sget-object v1, Laib;->c:Laib;

    aput-object v1, v0, v5

    sget-object v1, Laib;->g:Laib;

    aput-object v1, v0, v6

    sget-object v1, Laib;->d:Laib;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Laib;->e:Laib;

    aput-object v2, v0, v1

    sput-object v0, Laib;->h:[Laib;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZZ)V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 51
    iput-boolean p3, p0, Laib;->f:Z

    .line 53
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Laib;
    .locals 1

    .prologue
    .line 30
    const-class v0, Laib;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Laib;

    return-object v0
.end method

.method public static values()[Laib;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Laib;->h:[Laib;

    invoke-virtual {v0}, [Laib;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Laib;

    return-object v0
.end method
