.class final Lcix;
.super Landroid/os/AsyncTask;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lcit;


# direct methods
.method constructor <init>(Lcit;)V
    .locals 0

    .prologue
    .line 296
    iput-object p1, p0, Lcix;->a:Lcit;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a()Landroid/graphics/Bitmap;
    .locals 14

    .prologue
    .line 299
    iget-object v0, p0, Lcix;->a:Lcit;

    invoke-virtual {v0}, Lcit;->ah()Lchp;

    move-result-object v1

    .line 300
    iget-object v0, p0, Lcix;->a:Lcit;

    invoke-static {v0}, Lcit;->a(Lcit;)Lcom/google/android/libraries/snapseed/ui/views/CropImageView;

    move-result-object v2

    .line 301
    if-eqz v1, :cond_0

    if-nez v2, :cond_1

    .line 302
    :cond_0
    const/4 v0, 0x0

    .line 354
    :goto_0
    return-object v0

    .line 305
    :cond_1
    invoke-virtual {v1}, Lchp;->h()Lcom/google/android/libraries/photoeditor/core/FilterChain;

    move-result-object v3

    .line 307
    iget-object v0, p0, Lcix;->a:Lcit;

    invoke-virtual {v0}, Lcit;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    .line 308
    const/16 v4, 0x2a

    .line 309
    invoke-virtual {v0, v4}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    .line 311
    invoke-virtual {v1}, Lchp;->e()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 312
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    .line 313
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    int-to-float v5, v5

    .line 315
    new-instance v6, Landroid/graphics/RectF;

    .line 316
    invoke-virtual {v3}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectX()F

    move-result v7

    .line 317
    invoke-virtual {v3}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectY()F

    move-result v8

    .line 318
    invoke-virtual {v3}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectX()F

    move-result v9

    invoke-virtual {v3}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectWidth()F

    move-result v10

    add-float/2addr v9, v10

    .line 319
    invoke-virtual {v3}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectY()F

    move-result v10

    invoke-virtual {v3}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getCropRectHeight()F

    move-result v11

    add-float/2addr v10, v11

    invoke-direct {v6, v7, v8, v9, v10}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 320
    new-instance v7, Landroid/graphics/RectF;

    iget v8, v6, Landroid/graphics/RectF;->left:F

    mul-float/2addr v8, v4

    iget v9, v6, Landroid/graphics/RectF;->top:F

    mul-float/2addr v9, v5

    iget v10, v6, Landroid/graphics/RectF;->right:F

    mul-float/2addr v10, v4

    iget v6, v6, Landroid/graphics/RectF;->bottom:F

    mul-float/2addr v6, v5

    invoke-direct {v7, v8, v9, v10, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 327
    invoke-virtual {v2}, Lcom/google/android/libraries/snapseed/ui/views/CropImageView;->a()Landroid/graphics/RectF;

    move-result-object v2

    .line 329
    invoke-virtual {v3}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->getPostRotation()I

    move-result v6

    invoke-virtual {v2}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Rectangle should not be empty (left >= right or top >= bottom)"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v8, Landroid/graphics/PointF;

    iget v9, v2, Landroid/graphics/RectF;->left:F

    iget v10, v2, Landroid/graphics/RectF;->top:F

    invoke-direct {v8, v9, v10}, Landroid/graphics/PointF;-><init>(FF)V

    const/high16 v9, 0x3f800000    # 1.0f

    const/high16 v10, 0x3f800000    # 1.0f

    invoke-static {v8, v6, v9, v10}, Lhbg;->a(Landroid/graphics/PointF;IFF)V

    new-instance v9, Landroid/graphics/PointF;

    iget v10, v2, Landroid/graphics/RectF;->right:F

    iget v11, v2, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v9, v10, v11}, Landroid/graphics/PointF;-><init>(FF)V

    const/high16 v10, 0x3f800000    # 1.0f

    const/high16 v11, 0x3f800000    # 1.0f

    invoke-static {v9, v6, v10, v11}, Lhbg;->a(Landroid/graphics/PointF;IFF)V

    iget v6, v8, Landroid/graphics/PointF;->x:F

    iget v10, v9, Landroid/graphics/PointF;->x:F

    invoke-static {v6, v10}, Ljava/lang/Math;->min(FF)F

    move-result v6

    iget v10, v8, Landroid/graphics/PointF;->y:F

    iget v11, v9, Landroid/graphics/PointF;->y:F

    invoke-static {v10, v11}, Ljava/lang/Math;->min(FF)F

    move-result v10

    iget v11, v8, Landroid/graphics/PointF;->x:F

    iget v12, v9, Landroid/graphics/PointF;->x:F

    invoke-static {v11, v12}, Ljava/lang/Math;->max(FF)F

    move-result v11

    iget v8, v8, Landroid/graphics/PointF;->y:F

    iget v9, v9, Landroid/graphics/PointF;->y:F

    invoke-static {v8, v9}, Ljava/lang/Math;->max(FF)F

    move-result v8

    invoke-virtual {v2, v6, v10, v11, v8}, Landroid/graphics/RectF;->set(FFFF)V

    .line 332
    invoke-virtual {v7}, Landroid/graphics/RectF;->width()F

    move-result v6

    .line 333
    invoke-virtual {v7}, Landroid/graphics/RectF;->height()F

    move-result v8

    .line 334
    new-instance v9, Landroid/graphics/RectF;

    iget v10, v7, Landroid/graphics/RectF;->left:F

    iget v11, v2, Landroid/graphics/RectF;->left:F

    mul-float/2addr v11, v6

    add-float/2addr v10, v11

    iget v11, v7, Landroid/graphics/RectF;->top:F

    iget v12, v2, Landroid/graphics/RectF;->top:F

    mul-float/2addr v12, v8

    add-float/2addr v11, v12

    iget v12, v7, Landroid/graphics/RectF;->left:F

    iget v13, v2, Landroid/graphics/RectF;->right:F

    mul-float/2addr v6, v13

    add-float/2addr v6, v12

    iget v7, v7, Landroid/graphics/RectF;->top:F

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    mul-float/2addr v2, v8

    add-float/2addr v2, v7

    invoke-direct {v9, v10, v11, v6, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 340
    new-instance v2, Landroid/graphics/RectF;

    iget v6, v9, Landroid/graphics/RectF;->left:F

    div-float/2addr v6, v4

    const/4 v7, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    .line 341
    invoke-static {v6, v7, v8}, Lhcg;->a(FFF)F

    move-result v6

    iget v7, v9, Landroid/graphics/RectF;->top:F

    div-float/2addr v7, v5

    const/4 v8, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    .line 342
    invoke-static {v7, v8, v10}, Lhcg;->a(FFF)F

    move-result v7

    iget v8, v9, Landroid/graphics/RectF;->right:F

    div-float v4, v8, v4

    const/4 v8, 0x0

    const/high16 v10, 0x3f800000    # 1.0f

    .line 343
    invoke-static {v4, v8, v10}, Lhcg;->a(FFF)F

    move-result v4

    iget v8, v9, Landroid/graphics/RectF;->bottom:F

    div-float v5, v8, v5

    const/4 v8, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    .line 344
    invoke-static {v5, v8, v9}, Lhcg;->a(FFF)F

    move-result v5

    invoke-direct {v2, v6, v7, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    .line 346
    invoke-virtual {v2}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 347
    const/4 v0, 0x0

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v2, v0, v4, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    .line 348
    const/4 v0, 0x0

    .line 351
    :cond_3
    invoke-virtual {v3, v0}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a(I)V

    .line 352
    invoke-virtual {v3, v2}, Lcom/google/android/libraries/photoeditor/core/FilterChain;->a(Landroid/graphics/RectF;)V

    .line 354
    sget-object v0, Lhan;->a:Lhao;

    const/4 v2, 0x3

    const/4 v4, 0x0

    invoke-interface {v0, v1, v3, v2, v4}, Lhao;->renderFilterChain(Landroid/graphics/Bitmap;Lcom/google/android/libraries/photoeditor/core/FilterChain;IZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto/16 :goto_0
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 366
    iget-object v0, p0, Lcix;->a:Lcit;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcit;->k(Z)V

    .line 367
    iget-object v0, p0, Lcix;->a:Lcit;

    invoke-virtual {v0}, Lcit;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Lhdn;->b(Landroid/app/Activity;)V

    .line 369
    iget-object v0, p0, Lcix;->a:Lcit;

    iget-object v1, p0, Lcix;->a:Lcit;

    invoke-virtual {v1}, Lcit;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcit;->a(Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;Landroid/graphics/Bitmap;)V

    .line 370
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 296
    invoke-virtual {p0}, Lcix;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 296
    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcix;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    .prologue
    .line 360
    iget-object v0, p0, Lcix;->a:Lcit;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcit;->k(Z)V

    .line 361
    iget-object v0, p0, Lcix;->a:Lcit;

    invoke-virtual {v0}, Lcit;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Lhdn;->a(Landroid/app/Activity;)V

    .line 362
    return-void
.end method
