.class public Lfqu;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/View$OnSystemUiVisibilityChangeListener;
.implements Landroid/widget/Adapter;


# static fields
.field private static final a:[I

.field private static final d:Lloz;

.field private static final e:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public b:Lmmq;

.field public c:I

.field private final f:Landroid/database/DataSetObservable;

.field private final g:Landroid/content/Context;

.field private final h:Lfrj;

.field private final i:Landroid/view/View$OnClickListener;

.field private final j:Lfra;

.field private k:Lfod;

.field private l:I

.field private m:Lfrb;

.field private n:Z

.field private o:Lfrw;

.field private p:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lfqy;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 147
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lfqu;->a:[I

    .line 158
    new-instance v0, Lloz;

    const-string v1, "debug.stories.color_unknown_els"

    invoke-direct {v0, v1}, Lloz;-><init>(Ljava/lang/String;)V

    sput-object v0, Lfqu;->d:Lloz;

    .line 161
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 164
    sput-object v0, Lfqu;->e:Ljava/util/Set;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 165
    sget-object v0, Lfqu;->e:Ljava/util/Set;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 166
    sget-object v0, Lfqu;->e:Ljava/util/Set;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 167
    return-void

    .line 147
    nop

    :array_0
    .array-data 4
        0x3
        0x2
        0x1
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View$OnClickListener;Lfra;)V
    .locals 2

    .prologue
    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 169
    new-instance v0, Landroid/database/DataSetObservable;

    invoke-direct {v0}, Landroid/database/DataSetObservable;-><init>()V

    iput-object v0, p0, Lfqu;->f:Landroid/database/DataSetObservable;

    .line 171
    new-instance v0, Lfrj;

    const/16 v1, 0xb

    invoke-direct {v0, v1}, Lfrj;-><init>(I)V

    iput-object v0, p0, Lfqu;->h:Lfrj;

    .line 178
    const/4 v0, -0x1

    iput v0, p0, Lfqu;->c:I

    .line 179
    const/4 v0, 0x1

    iput v0, p0, Lfqu;->l:I

    .line 205
    iput-object p1, p0, Lfqu;->g:Landroid/content/Context;

    .line 206
    iput-object p2, p0, Lfqu;->i:Landroid/view/View$OnClickListener;

    .line 207
    iput-object p3, p0, Lfqu;->j:Lfra;

    .line 208
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lfqu;->p:Ljava/util/Map;

    .line 209
    return-void
.end method

.method static synthetic a(Lfqu;)Lfrw;
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lfqu;->o:Lfrw;

    return-object v0
.end method

.method private static a(Landroid/content/Context;Lmmq;Lmnk;Lfrc;FLfod;)Lizu;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 1018
    iget-object v0, p2, Lmnk;->d:Lmnj;

    if-nez v0, :cond_1

    .line 1057
    :cond_0
    :goto_0
    return-object v1

    .line 1022
    :cond_1
    iget-object v0, p2, Lmnk;->d:Lmnj;

    iget v0, v0, Lmnj;->a:I

    sparse-switch v0, :sswitch_data_0

    .line 1054
    iget-object v0, p2, Lmnk;->d:Lmnj;

    iget v0, v0, Lmnj;->a:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x26

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unrecognized content type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 1024
    :sswitch_0
    iget-object v0, p2, Lmnk;->d:Lmnj;

    iget-object v0, v0, Lmnj;->b:Lmms;

    const-class v1, Lmml;

    invoke-static {p1, v0, v1}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmml;

    .line 1026
    iget-object v0, v0, Lmml;->d:Lnzx;

    invoke-static {p0, v0, p5}, Lfqu;->a(Landroid/content/Context;Lnzx;Lfod;)Lizu;

    move-result-object v1

    goto :goto_0

    .line 1028
    :sswitch_1
    iget-object v0, p1, Lmmq;->c:Lmoo;

    iget-object v0, v0, Lmoo;->d:[Lnzx;

    aget-object v0, v0, v3

    invoke-static {p0, v0, p5}, Lfqu;->a(Landroid/content/Context;Lnzx;Lfod;)Lizu;

    move-result-object v1

    goto :goto_0

    .line 1030
    :sswitch_2
    iget-object v0, p2, Lmnk;->d:Lmnj;

    iget-object v0, v0, Lmnj;->b:Lmms;

    iget v0, v0, Lmms;->f:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_2

    .line 1031
    iget-object v0, p2, Lmnk;->d:Lmnj;

    iget-object v0, v0, Lmnj;->b:Lmms;

    const-class v1, Lnzx;

    .line 1032
    invoke-static {p1, v0, v1}, Lfss;->b(Lmmq;Lmms;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzx;

    .line 1031
    invoke-static {p0, v0, p5}, Lfqu;->a(Landroid/content/Context;Lnzx;Lfod;)Lizu;

    move-result-object v1

    goto :goto_0

    .line 1036
    :cond_2
    iget-object v0, p2, Lmnk;->d:Lmnj;

    iget-object v0, v0, Lmnj;->b:Lmms;

    const-class v2, Ljava/lang/String;

    invoke-static {p1, v0, v2}, Lfss;->b(Lmmq;Lmms;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1057
    :goto_1
    if-eqz v0, :cond_0

    sget-object v1, Ljac;->a:Ljac;

    invoke-static {p0, v0, v1}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    goto :goto_0

    .line 1040
    :sswitch_3
    iget-object v0, p2, Lmnk;->d:Lmnj;

    iget-object v0, v0, Lmnj;->b:Lmms;

    const-class v2, Lmml;

    .line 1041
    invoke-static {p1, v0, v2}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmml;

    .line 1042
    if-eqz v0, :cond_3

    iget-object v2, v0, Lmml;->e:Lmmn;

    if-eqz v2, :cond_3

    iget-object v0, v0, Lmml;->e:Lmmn;

    iget-object v0, v0, Lmmn;->a:Lmmo;

    .line 1044
    invoke-static {v0, v3}, Lfsp;->a(Lmmo;Z)Lfof;

    move-result-object v0

    .line 1046
    invoke-virtual {p3}, Lfrc;->l()I

    move-result v2

    invoke-virtual {p3}, Lfrc;->n()I

    move-result v3

    .line 1043
    invoke-static {v0, v2, v3, p4}, Lfsp;->a(Lfof;IIF)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_1

    .line 1050
    :sswitch_4
    iget-object v0, p2, Lmnk;->d:Lmnj;

    iget-object v0, v0, Lmnj;->c:Lmnh;

    if-eqz v0, :cond_4

    iget-object v0, p2, Lmnk;->d:Lmnj;

    iget-object v0, v0, Lmnj;->c:Lmnh;

    iget-object v0, v0, Lmnh;->c:Ljava/lang/String;

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_1

    .line 1022
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_4
        0x5 -> :sswitch_0
        0xa -> :sswitch_1
        0xc -> :sswitch_3
    .end sparse-switch
.end method

.method private static a(Landroid/content/Context;Lnzx;Lfod;)Lizu;
    .locals 2

    .prologue
    .line 1062
    sget-object v0, Lnzu;->a:Loxr;

    invoke-virtual {p1, v0}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    iget-object v0, v0, Lnzu;->b:Lnym;

    .line 1063
    invoke-static {p1, p2}, Lfss;->a(Lnzx;Lfod;)Ljava/lang/String;

    move-result-object v1

    .line 1064
    invoke-static {v0}, Ljvd;->b(Lnym;)Ljac;

    move-result-object v0

    .line 1063
    invoke-static {p0, v1, v0}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lmmq;Lmnk;Lfod;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 990
    iget-object v0, p1, Lmnk;->d:Lmnj;

    if-nez v0, :cond_0

    .line 1010
    :goto_0
    return-object v1

    .line 993
    :cond_0
    iget-object v0, p1, Lmnk;->d:Lmnj;

    iget v0, v0, Lmnj;->a:I

    sparse-switch v0, :sswitch_data_0

    .line 1009
    iget-object v0, p1, Lmnk;->d:Lmnj;

    iget v0, v0, Lmnj;->a:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x26

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unrecognized content type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 996
    :sswitch_0
    iget-object v0, p1, Lmnk;->d:Lmnj;

    iget-object v1, v0, Lmnj;->d:Ljava/lang/String;

    goto :goto_0

    .line 998
    :sswitch_1
    iget-object v0, p1, Lmnk;->d:Lmnj;

    iget-object v0, v0, Lmnj;->b:Lmms;

    iget v0, v0, Lmms;->f:I

    packed-switch v0, :pswitch_data_0

    .line 1005
    iget-object v0, p1, Lmnk;->d:Lmnj;

    iget-object v0, v0, Lmnj;->b:Lmms;

    const-class v1, Ljava/lang/String;

    invoke-static {p0, v0, v1}, Lfss;->b(Lmmq;Lmms;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    goto :goto_0

    .line 1000
    :pswitch_0
    iget-object v0, p1, Lmnk;->d:Lmnj;

    iget-object v0, v0, Lmnj;->b:Lmms;

    const-class v2, Lmml;

    invoke-static {p0, v0, v2}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmml;

    .line 1002
    if-eqz v0, :cond_1

    iget-object v0, v0, Lmml;->d:Lnzx;

    :goto_1
    invoke-static {v0, p2}, Lfss;->b(Lnzx;Lfod;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1

    .line 993
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_1
        0x4 -> :sswitch_0
        0xe -> :sswitch_0
    .end sparse-switch

    .line 998
    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_0
    .end packed-switch
.end method

.method private a(Lmnk;Lfrc;Landroid/view/View;)V
    .locals 8

    .prologue
    const v7, 0x7f100045

    const/4 v0, 0x0

    const v6, 0x7f10003d

    const/4 v5, 0x1

    .line 1321
    .line 1322
    iget-object v1, p1, Lmnk;->i:Lmnl;

    if-eqz v1, :cond_1

    .line 1323
    invoke-virtual {p2}, Lfrc;->a()Lfrc;

    move-result-object v1

    move-object v2, v1

    move-object v1, v0

    .line 1324
    :goto_0
    if-eqz v2, :cond_2

    if-nez v1, :cond_2

    .line 1325
    invoke-virtual {v2}, Lfrc;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1326
    invoke-virtual {v2}, Lfrc;->c()Lmnn;

    move-result-object v1

    goto :goto_0

    .line 1328
    :cond_0
    invoke-virtual {v2}, Lfrc;->a()Lfrc;

    move-result-object v2

    goto :goto_0

    :cond_1
    move-object v1, v0

    .line 1334
    :cond_2
    if-eqz p1, :cond_5

    iget-object v2, p1, Lmnk;->i:Lmnl;

    if-eqz v2, :cond_5

    iget-object v2, p1, Lmnk;->i:Lmnl;

    iget-object v2, v2, Lmnl;->b:Lmnn;

    if-eqz v2, :cond_5

    .line 1335
    invoke-direct {p0}, Lfqu;->d()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1336
    iget-object v0, p1, Lmnk;->i:Lmnl;

    iget-object v0, v0, Lmnl;->b:Lmnn;

    iget-object v0, v0, Lmnn;->a:Lmms;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lmnk;->i:Lmnl;

    iget-object v0, v0, Lmnl;->b:Lmnn;

    iget-object v0, v0, Lmnn;->a:Lmms;

    :goto_1
    move-object v2, v0

    .line 1346
    :goto_2
    if-nez v2, :cond_8

    .line 1385
    :cond_3
    :goto_3
    return-void

    .line 1336
    :cond_4
    iget-object v0, p1, Lmnk;->d:Lmnj;

    iget-object v0, v0, Lmnj;->b:Lmms;

    goto :goto_1

    .line 1339
    :cond_5
    if-eqz v1, :cond_a

    .line 1340
    iget-object v2, v1, Lmnn;->a:Lmms;

    if-eqz v2, :cond_7

    iget-object v0, v1, Lmnn;->a:Lmms;

    :cond_6
    :goto_4
    move-object v2, v0

    goto :goto_2

    :cond_7
    iget-object v1, p1, Lmnk;->d:Lmnj;

    if-eqz v1, :cond_6

    iget-object v1, p1, Lmnk;->d:Lmnj;

    iget-object v1, v1, Lmnj;->b:Lmms;

    if-eqz v1, :cond_6

    iget-object v0, p1, Lmnk;->d:Lmnj;

    iget-object v0, v0, Lmnj;->b:Lmms;

    goto :goto_4

    .line 1350
    :cond_8
    iget v0, v2, Lmms;->a:I

    packed-switch v0, :pswitch_data_0

    goto :goto_3

    .line 1352
    :pswitch_0
    iget-object v0, p0, Lfqu;->b:Lmmq;

    const-class v1, Lmml;

    invoke-static {v0, v2, v1}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmml;

    .line 1353
    if-eqz v0, :cond_9

    iget v1, v0, Lmml;->c:I

    if-ne v1, v5, :cond_9

    .line 1354
    iget-object v1, p0, Lfqu;->j:Lfra;

    invoke-interface {v1, p3}, Lfra;->d(Landroid/view/View;)V

    .line 1355
    invoke-virtual {p3, v6, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1356
    const v1, 0x7f100079

    iget-object v3, v0, Lmml;->d:Lnzx;

    iget-object v3, v3, Lnzx;->b:Ljava/lang/String;

    invoke-virtual {p3, v1, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1357
    const v3, 0x7f10006b

    iget-object v1, v0, Lmml;->d:Lnzx;

    sget-object v4, Lnzu;->a:Loxr;

    .line 1358
    invoke-virtual {v1, v4}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lnzu;

    iget-object v1, v1, Lnzu;->b:Lnym;

    iget-object v1, v1, Lnym;->e:Ljava/lang/String;

    .line 1357
    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p3, v3, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1359
    const v1, 0x7f100046

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {p3, v1, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1360
    iget-object v1, v0, Lmml;->d:Lnzx;

    iget-object v3, p0, Lfqu;->k:Lfod;

    invoke-static {v1, v3}, Lfss;->b(Lnzx;Lfod;)Ljava/lang/String;

    move-result-object v1

    .line 1361
    const v3, 0x7f100047

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {p3, v3, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1362
    const v3, 0x7f100048

    invoke-virtual {p3, v3, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1363
    const v1, 0x7f100049

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {p3, v1, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1365
    :cond_9
    invoke-static {v0}, Lfss;->a(Lmml;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1366
    iget-object v0, p0, Lfqu;->j:Lfra;

    invoke-interface {v0, p3}, Lfra;->d(Landroid/view/View;)V

    .line 1367
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p3, v7, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1368
    invoke-virtual {p3, v6, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto/16 :goto_3

    .line 1373
    :pswitch_1
    iget-object v0, p0, Lfqu;->b:Lmmq;

    const-class v1, Lmmc;

    invoke-static {v0, v2, v1}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmmc;

    .line 1374
    invoke-static {v0}, Lfss;->a(Lmmc;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1375
    iget-object v0, p0, Lfqu;->j:Lfra;

    invoke-interface {v0, p3}, Lfra;->d(Landroid/view/View;)V

    .line 1376
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {p3, v7, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 1377
    invoke-virtual {p3, v6, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto/16 :goto_3

    :cond_a
    move-object v2, v0

    goto/16 :goto_2

    .line 1350
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a([Lmnk;)V
    .locals 18

    .prologue
    .line 279
    move-object/from16 v0, p1

    array-length v6, v0

    const/4 v2, 0x0

    move v5, v2

    :goto_0
    if-ge v5, v6, :cond_6

    aget-object v2, p1, v5

    .line 280
    invoke-static {v2}, Lfqu;->b(Lmnk;)Lmnk;

    move-result-object v7

    .line 281
    if-eqz v7, :cond_5

    iget-object v2, v7, Lmnk;->c:Lmof;

    if-eqz v2, :cond_5

    iget-object v2, v7, Lmnk;->c:Lmof;

    iget-object v2, v2, Lmof;->x:[Lmog;

    if-eqz v2, :cond_5

    .line 283
    iget-object v2, v7, Lmnk;->c:Lmof;

    iget-object v8, v2, Lmof;->x:[Lmog;

    array-length v9, v8

    const/4 v2, 0x0

    move v4, v2

    :goto_1
    if-ge v4, v9, :cond_4

    aget-object v10, v8, v4

    .line 286
    iget-object v2, v10, Lmog;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move-object/from16 v0, p0

    iget-object v11, v0, Lfqu;->m:Lfrb;

    invoke-virtual {v11, v2, v3}, Lfrb;->b(J)Lmmz;

    move-result-object v2

    .line 287
    if-eqz v2, :cond_0

    iget-object v3, v2, Lmmz;->c:[Lmna;

    if-nez v3, :cond_1

    .line 285
    :cond_0
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 291
    :cond_1
    iget-object v3, v10, Lmog;->d:Ljava/lang/Integer;

    invoke-static {v3}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v11

    .line 293
    iget-object v12, v2, Lmmz;->c:[Lmna;

    array-length v13, v12

    const/4 v2, 0x0

    move v3, v2

    :goto_2
    if-ge v3, v13, :cond_0

    aget-object v2, v12, v3

    .line 294
    iget-object v14, v2, Lmna;->c:[B

    if-eqz v14, :cond_3

    .line 295
    new-instance v14, Ljava/lang/String;

    iget-object v15, v2, Lmna;->c:[B

    invoke-direct {v14, v15}, Ljava/lang/String;-><init>([B)V

    .line 300
    new-instance v15, Lfqy;

    invoke-direct {v15}, Lfqy;-><init>()V

    .line 301
    iget-object v0, v2, Lmna;->d:Ljava/lang/Long;

    move-object/from16 v16, v0

    .line 302
    invoke-static/range {v16 .. v16}, Llsl;->a(Ljava/lang/Long;)J

    move-result-wide v16

    move-wide/from16 v0, v16

    iput-wide v0, v15, Lfqy;->a:J

    .line 303
    iget-object v2, v2, Lmna;->b:Ljava/lang/Integer;

    .line 304
    invoke-static {v2}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v2

    int-to-long v0, v2

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    iput-wide v0, v15, Lfqy;->c:J

    .line 305
    int-to-long v0, v11

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    iput-wide v0, v15, Lfqy;->b:J

    .line 306
    iget v2, v10, Lmog;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v2

    iput v2, v15, Lfqy;->d:I

    .line 307
    iget-object v2, v7, Lmnk;->f:Ljava/lang/Integer;

    invoke-static {v2}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v15, Lfqy;->e:Ljava/lang/Object;

    .line 308
    move-object/from16 v0, p0

    iget-object v2, v0, Lfqu;->p:Ljava/util/Map;

    invoke-interface {v2, v14}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 309
    move-object/from16 v0, p0

    iget-object v2, v0, Lfqu;->p:Ljava/util/Map;

    new-instance v16, Ljava/util/ArrayList;

    invoke-direct/range {v16 .. v16}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, v16

    invoke-interface {v2, v14, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lfqu;->p:Ljava/util/Map;

    invoke-interface {v2, v14}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    invoke-interface {v2, v15}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 293
    :cond_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 315
    :cond_4
    iget-object v2, v7, Lmnk;->e:[Lmnk;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lfqu;->a([Lmnk;)V

    .line 279
    :cond_5
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto/16 :goto_0

    .line 317
    :cond_6
    return-void
.end method

.method private static b(Lmnk;)Lmnk;
    .locals 6

    .prologue
    .line 253
    move-object v0, p0

    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, v0, Lmnk;->j:[I

    if-eqz v1, :cond_0

    iget-object v1, v0, Lmnk;->j:[I

    array-length v1, v1

    if-nez v1, :cond_1

    .line 262
    :cond_0
    return-object v0

    .line 257
    :cond_1
    iget-object v2, v0, Lmnk;->j:[I

    array-length v3, v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    aget v4, v2, v1

    .line 258
    sget-object v5, Lfqu;->e:Ljava/util/Set;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v5, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 259
    iget-object v0, v0, Lmnk;->k:Lmnk;

    goto :goto_0

    .line 257
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private static c(Lmnk;)I
    .locals 3

    .prologue
    .line 488
    iget v0, p0, Lmnk;->b:I

    packed-switch v0, :pswitch_data_0

    .line 505
    :cond_0
    :goto_0
    const/4 v0, -0x1

    :goto_1
    return v0

    .line 490
    :pswitch_0
    iget-object v0, p0, Lmnk;->d:Lmnj;

    iget v0, v0, Lmnj;->a:I

    packed-switch v0, :pswitch_data_1

    :pswitch_1
    goto :goto_0

    .line 492
    :pswitch_2
    iget-object v0, p0, Lmnk;->d:Lmnj;

    iget-object v0, v0, Lmnj;->b:Lmms;

    .line 493
    iget v1, v0, Lmms;->a:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    iget v1, v0, Lmms;->f:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 495
    const v0, 0x7f10002c

    goto :goto_1

    .line 496
    :cond_1
    iget v0, v0, Lmms;->f:I

    const/16 v1, 0xb

    if-ne v0, v1, :cond_0

    .line 497
    const v0, 0x7f10002f

    goto :goto_1

    .line 501
    :pswitch_3
    const v0, 0x7f10002e

    goto :goto_1

    .line 488
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch

    .line 490
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private d(I)I
    .locals 7

    .prologue
    const/4 v5, 0x3

    .line 1392
    .line 1393
    const v1, 0x7fffffff

    .line 1394
    sget-object v6, Lfqu;->a:[I

    const/4 v0, 0x0

    move v4, v0

    move v3, v5

    :goto_0
    if-ge v4, v5, :cond_0

    aget v2, v6, v4

    .line 1395
    iget-object v0, p0, Lfqu;->g:Landroid/content/Context;

    invoke-static {v0, v2}, Lhss;->a(Landroid/content/Context;I)I

    move-result v0

    .line 1396
    sub-int/2addr v0, p1

    .line 1397
    if-lez v0, :cond_1

    if-ge v0, v1, :cond_1

    move v1, v2

    .line 1394
    :goto_1
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v3, v1

    move v1, v0

    goto :goto_0

    .line 1402
    :cond_0
    return v3

    :cond_1
    move v0, v1

    move v1, v3

    goto :goto_1
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 933
    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v0, v0, Lmmq;->i:Ljava/lang/Boolean;

    invoke-static {v0}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v0

    return v0
.end method

.method private d(Lmnk;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 938
    invoke-direct {p0}, Lfqu;->d()Z

    move-result v2

    if-nez v2, :cond_0

    .line 954
    :goto_0
    return v0

    .line 941
    :cond_0
    iget-object v2, p1, Lmnk;->d:Lmnj;

    iget v2, v2, Lmnj;->a:I

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 943
    :sswitch_0
    iget-object v2, p1, Lmnk;->d:Lmnj;

    iget-object v2, v2, Lmnj;->b:Lmms;

    iget v2, v2, Lmms;->f:I

    sparse-switch v2, :sswitch_data_1

    goto :goto_0

    :sswitch_1
    move v0, v1

    .line 946
    goto :goto_0

    :sswitch_2
    move v0, v1

    .line 951
    goto :goto_0

    .line 941
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0xe -> :sswitch_2
    .end sparse-switch

    .line 943
    :sswitch_data_1
    .sparse-switch
        0x2 -> :sswitch_1
        0xb -> :sswitch_1
    .end sparse-switch
.end method

.method private static e(Lmnk;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1097
    iget-object v1, p0, Lmnk;->d:Lmnj;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmnk;->d:Lmnj;

    iget-object v1, v1, Lmnj;->c:Lmnh;

    if-nez v1, :cond_1

    .line 1110
    :cond_0
    :goto_0
    return v0

    .line 1100
    :cond_1
    iget-object v1, p0, Lmnk;->d:Lmnj;

    iget-object v1, v1, Lmnj;->c:Lmnh;

    iget v1, v1, Lmnh;->b:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1102
    :pswitch_0
    const v0, 0x7f02036a

    goto :goto_0

    .line 1104
    :pswitch_1
    const v0, 0x7f02032a

    goto :goto_0

    .line 1106
    :pswitch_2
    const v0, 0x7f02027e

    goto :goto_0

    .line 1108
    :pswitch_3
    const v0, 0x7f02036f

    goto :goto_0

    .line 1100
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a(Ljava/util/List;)I
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lfqz;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v0, -0x1

    .line 447
    invoke-virtual {p0, p1}, Lfqu;->b(Ljava/util/List;)I

    move-result v1

    .line 448
    if-eq v1, v0, :cond_1

    .line 449
    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v0, v0, Lmmq;->h:[Lmmt;

    iget v2, p0, Lfqu;->c:I

    aget-object v0, v0, v2

    iget-object v0, v0, Lmmt;->d:[Lmnk;

    aget-object v0, v0, v1

    iget-object v0, v0, Lmnk;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 455
    :cond_0
    :goto_0
    return v0

    .line 450
    :cond_1
    iget-object v1, p0, Lfqu;->b:Lmmq;

    if-eqz v1, :cond_0

    iget v1, p0, Lfqu;->c:I

    if-eq v1, v0, :cond_0

    iget-object v1, p0, Lfqu;->b:Lmmq;

    iget-object v1, v1, Lmmq;->h:[Lmmt;

    iget v2, p0, Lfqu;->c:I

    aget-object v1, v1, v2

    iget-object v1, v1, Lmmt;->d:[Lmnk;

    array-length v1, v1

    if-lez v1, :cond_0

    .line 452
    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v0, v0, Lmmq;->h:[Lmmt;

    iget v1, p0, Lfqu;->c:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lmmt;->d:[Lmnk;

    .line 453
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    iget-object v0, v0, Lmnk;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public a(Lmnk;)I
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/16 v4, 0x2b

    const/16 v0, 0xa

    .line 510
    invoke-static {p1}, Lfqu;->b(Lmnk;)Lmnk;

    move-result-object v2

    .line 511
    if-nez v2, :cond_0

    .line 566
    :goto_0
    return v0

    .line 514
    :cond_0
    iget v3, v2, Lmnk;->b:I

    packed-switch v3, :pswitch_data_0

    .line 563
    iget v1, v2, Lmnk;->b:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unrecognized LayoutElementType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 516
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 518
    :pswitch_1
    iget-object v3, v2, Lmnk;->d:Lmnj;

    iget v3, v3, Lmnj;->a:I

    packed-switch v3, :pswitch_data_1

    .line 558
    :pswitch_2
    iget-object v1, v2, Lmnk;->d:Lmnj;

    iget v1, v1, Lmnj;->a:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unrecognized LayoutContentType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 521
    :pswitch_3
    const/4 v0, 0x1

    goto :goto_0

    .line 523
    :pswitch_4
    const/4 v0, 0x3

    goto :goto_0

    .line 525
    :pswitch_5
    const/16 v0, 0x9

    goto :goto_0

    :pswitch_6
    move v0, v1

    .line 528
    goto :goto_0

    .line 530
    :pswitch_7
    const/4 v0, 0x4

    goto :goto_0

    .line 533
    :pswitch_8
    iget-object v3, v2, Lmnk;->d:Lmnj;

    iget-object v3, v3, Lmnj;->c:Lmnh;

    iget v3, v3, Lmnh;->a:I

    packed-switch v3, :pswitch_data_2

    .line 545
    iget-object v1, v2, Lmnk;->d:Lmnj;

    iget-object v1, v1, Lmnj;->c:Lmnh;

    iget v1, v1, Lmnh;->a:I

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unrecognized ImageResourceType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 536
    :pswitch_9
    invoke-static {v2}, Lfqu;->e(Lmnk;)I

    move-result v1

    if-nez v1, :cond_1

    .line 537
    iget-object v1, v2, Lmnk;->d:Lmnj;

    iget-object v1, v1, Lmnj;->c:Lmnh;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x1d

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Unrecognized image resource: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_0

    .line 541
    :cond_1
    const/4 v0, 0x5

    goto/16 :goto_0

    :pswitch_a
    move v0, v1

    .line 543
    goto/16 :goto_0

    .line 551
    :pswitch_b
    const/4 v0, 0x6

    goto/16 :goto_0

    .line 554
    :pswitch_c
    const/4 v0, 0x7

    goto/16 :goto_0

    .line 556
    :pswitch_d
    const/16 v0, 0x8

    goto/16 :goto_0

    .line 514
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 518
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_6
        :pswitch_8
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_2
        :pswitch_2
        :pswitch_8
        :pswitch_7
        :pswitch_4
        :pswitch_b
        :pswitch_6
        :pswitch_c
        :pswitch_d
        :pswitch_2
        :pswitch_c
    .end packed-switch

    .line 533
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public a(Lmnk;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12

    .prologue
    .line 583
    if-eqz p2, :cond_0

    .line 584
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 585
    iget-object v0, p0, Lfqu;->j:Lfra;

    invoke-interface {v0, p2}, Lfra;->e(Landroid/view/View;)V

    .line 586
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setLongClickable(Z)V

    .line 587
    const v0, 0x7f100046

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 588
    const v0, 0x7f100047

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 589
    const v0, 0x7f100049

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 590
    const v0, 0x7f100045

    const/4 v1, 0x0

    invoke-virtual {p2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 592
    :cond_0
    invoke-static {p1}, Lfqu;->b(Lmnk;)Lmnk;

    move-result-object v2

    .line 593
    if-eqz p3, :cond_1

    .line 594
    invoke-static {p3}, Lfrc;->a(Landroid/view/View;)Lfrc;

    move-result-object v0

    .line 597
    :goto_0
    invoke-virtual {p0, v2, v0}, Lfqu;->a(Lmnk;Lfrc;)Lfrc;

    move-result-object v3

    .line 600
    invoke-virtual {p0, v2}, Lfqu;->a(Lmnk;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 858
    const/4 p2, 0x0

    .line 918
    :goto_1
    return-object p2

    .line 594
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 602
    :pswitch_0
    if-eqz p2, :cond_4

    check-cast p2, Lfua;

    .line 604
    :goto_2
    invoke-virtual {p2, p0}, Lfua;->a(Lfqu;)V

    .line 605
    const v0, 0x7f100031

    invoke-virtual {p2, v0, v3}, Lfua;->setTag(ILjava/lang/Object;)V

    .line 606
    invoke-virtual {p2, v2}, Lfua;->a(Lmnk;)V

    move-object v9, p2

    .line 863
    :goto_3
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xa

    if-le v0, v1, :cond_2

    .line 864
    invoke-virtual {v3}, Lfrc;->A()F

    move-result v0

    .line 865
    invoke-virtual {v9, v0}, Landroid/view/View;->setAlpha(F)V

    .line 866
    const v1, 0x7f100040

    const/4 v4, 0x0

    cmpl-float v0, v0, v4

    if-nez v0, :cond_30

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_4
    invoke-virtual {v9, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 871
    :cond_2
    invoke-virtual {v3}, Lfrc;->d()Ljava/lang/String;

    move-result-object v0

    .line 872
    if-eqz v0, :cond_31

    iget-object v1, p0, Lfqu;->p:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_31

    .line 873
    iget-object v0, p0, Lfqu;->p:Ljava/util/Map;

    invoke-virtual {v3}, Lfrc;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_31

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfqy;

    .line 874
    iget-object v4, p0, Lfqu;->m:Lfrb;

    iget-wide v6, v0, Lfqy;->a:J

    invoke-virtual {v4, v6, v7}, Lfrb;->a(J)Lfsc;

    move-result-object v5

    .line 875
    if-eqz v5, :cond_3

    .line 876
    invoke-virtual {v3}, Lfrc;->t()I

    move-result v7

    .line 881
    iget v4, v0, Lfqy;->d:I

    packed-switch v4, :pswitch_data_1

    .line 909
    iget v0, v0, Lfqy;->d:I

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x20

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unknown TriggerType: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 602
    :cond_4
    new-instance p2, Lfua;

    iget-object v0, p0, Lfqu;->g:Landroid/content/Context;

    invoke-direct {p2, v0}, Lfua;-><init>(Landroid/content/Context;)V

    goto/16 :goto_2

    .line 611
    :pswitch_1
    if-eqz p2, :cond_7

    check-cast p2, Lftz;

    .line 613
    :goto_6
    const v0, 0x7f100031

    invoke-virtual {p2, v0, v3}, Lftz;->setTag(ILjava/lang/Object;)V

    .line 614
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    .line 615
    invoke-virtual {v3}, Lfrc;->B()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 616
    invoke-virtual {v3}, Lfrc;->L()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 617
    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 618
    invoke-virtual {p2, v0}, Lftz;->a(Landroid/graphics/Paint;)V

    .line 619
    invoke-virtual {v3}, Lfrc;->Y()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 621
    invoke-virtual {p0}, Lfqu;->c()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00e7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 623
    invoke-virtual {p0}, Lfqu;->c()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0d00e8

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 620
    invoke-virtual {p2, v0, v1}, Lftz;->a(II)V

    .line 626
    :cond_5
    iget-object v0, v2, Lmnk;->d:Lmnj;

    iget v0, v0, Lmnj;->a:I

    const/16 v1, 0xd

    if-ne v0, v1, :cond_8

    .line 627
    invoke-virtual {v3}, Lfrc;->ah()Lfri;

    move-result-object v0

    invoke-virtual {p2, v0}, Lftz;->a(Lfri;)V

    :cond_6
    :goto_7
    move-object v9, p2

    .line 632
    goto/16 :goto_3

    .line 611
    :cond_7
    new-instance p2, Lftz;

    iget-object v0, p0, Lfqu;->g:Landroid/content/Context;

    invoke-direct {p2, v0}, Lftz;-><init>(Landroid/content/Context;)V

    goto :goto_6

    .line 628
    :cond_8
    iget-object v0, v2, Lmnk;->d:Lmnj;

    iget v0, v0, Lmnj;->a:I

    const/16 v1, 0x10

    if-ne v0, v1, :cond_6

    .line 629
    invoke-virtual {v3}, Lfrc;->X()[Lhng;

    move-result-object v0

    invoke-virtual {p2, v0}, Lftz;->a([Lhng;)V

    goto :goto_7

    .line 635
    :pswitch_2
    if-eqz p2, :cond_b

    check-cast p2, Lftm;

    .line 638
    :goto_8
    const v0, 0x7f100030

    invoke-virtual {p2, v0}, Lftm;->setId(I)V

    .line 639
    const v0, 0x7f100031

    invoke-virtual {p2, v0, v3}, Lftm;->setTag(ILjava/lang/Object;)V

    .line 641
    invoke-virtual {v3}, Lfrc;->D()I

    move-result v0

    and-int/lit8 v0, v0, 0x70

    const/16 v1, 0x50

    if-ne v0, v1, :cond_c

    const/4 v0, 0x1

    .line 640
    :goto_9
    invoke-virtual {p2, v0}, Lftm;->a(Z)V

    .line 642
    invoke-virtual {v3}, Lfrc;->C()I

    move-result v0

    .line 643
    invoke-virtual {p2, v0}, Lftm;->d(I)V

    .line 644
    invoke-virtual {v3}, Lfrc;->af()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 645
    invoke-virtual {v3}, Lfrc;->ag()Lfre;

    move-result-object v0

    invoke-virtual {p2, v0}, Lftm;->a(Lfre;)V

    .line 647
    :cond_9
    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v1, p0, Lfqu;->k:Lfod;

    invoke-static {v0, v2, v1}, Lfqu;->a(Lmmq;Lmnk;Lfod;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lftm;->a(Ljava/lang/String;)V

    .line 648
    invoke-virtual {v3}, Lfrc;->B()I

    move-result v0

    invoke-virtual {p2, v0}, Lftm;->a(I)V

    .line 649
    const/4 v0, 0x0

    invoke-virtual {v3}, Lfrc;->K()F

    move-result v1

    invoke-virtual {p2, v0, v1}, Lftm;->a(IF)V

    .line 650
    invoke-static {v2}, Lfss;->g(Lmnk;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p2, v0}, Lftm;->a(Landroid/graphics/Typeface;)V

    .line 651
    invoke-static {v2}, Lfss;->h(Lmnk;)I

    move-result v0

    invoke-virtual {p2, v0}, Lftm;->b(I)V

    .line 652
    invoke-virtual {v3}, Lfrc;->M()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 654
    invoke-virtual {v3}, Lfrc;->L()I

    move-result v0

    invoke-virtual {p2, v0}, Lftm;->c(I)V

    :cond_a
    move-object v9, p2

    .line 657
    goto/16 :goto_3

    .line 635
    :cond_b
    new-instance p2, Lftm;

    iget-object v0, p0, Lfqu;->g:Landroid/content/Context;

    invoke-direct {p2, v0}, Lftm;-><init>(Landroid/content/Context;)V

    goto :goto_8

    .line 641
    :cond_c
    const/4 v0, 0x0

    goto :goto_9

    .line 660
    :pswitch_3
    if-eqz p2, :cond_d

    check-cast p2, Landroid/widget/TextView;

    .line 662
    :goto_a
    invoke-static {v2}, Lfqu;->c(Lmnk;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setId(I)V

    .line 663
    const v0, 0x7f100031

    invoke-virtual {p2, v0, v3}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    .line 664
    const v0, 0x7f10003c

    invoke-virtual {p2, v0, v2}, Landroid/widget/TextView;->setTag(ILjava/lang/Object;)V

    .line 665
    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v1, p0, Lfqu;->k:Lfod;

    invoke-static {v0, v2, v1}, Lfqu;->a(Lmmq;Lmnk;Lfod;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 667
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setIncludeFontPadding(Z)V

    .line 668
    invoke-virtual {v3}, Lfrc;->C()I

    move-result v4

    .line 669
    if-nez v4, :cond_11

    .line 670
    iget-object v0, v2, Lmnk;->c:Lmof;

    if-eqz v0, :cond_e

    iget-object v0, v2, Lmnk;->c:Lmof;

    iget v0, v0, Lmof;->B:I

    :goto_b
    iget-object v1, v2, Lmnk;->i:Lmnl;

    if-eqz v1, :cond_f

    iget-object v1, v2, Lmnk;->i:Lmnl;

    iget-object v1, v1, Lmnl;->a:Lmnm;

    if-eqz v1, :cond_f

    iget-object v1, v2, Lmnk;->i:Lmnl;

    iget-object v1, v1, Lmnl;->a:Lmnm;

    iget v1, v1, Lmnm;->a:I

    const/4 v5, 0x1

    if-ne v1, v5, :cond_f

    packed-switch v0, :pswitch_data_2

    const v0, 0x7f02007f

    move v1, v0

    .line 671
    :goto_c
    if-eqz v1, :cond_12

    invoke-virtual {p0}, Lfqu;->c()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f0d00d7

    .line 672
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 673
    :goto_d
    if-eqz v4, :cond_13

    .line 674
    invoke-virtual {p2, v4}, Landroid/widget/TextView;->setBackgroundColor(I)V

    .line 678
    :goto_e
    invoke-virtual {p2, v0, v0, v0, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 679
    invoke-virtual {v3}, Lfrc;->B()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 680
    const/4 v0, 0x0

    invoke-virtual {v3}, Lfrc;->K()F

    move-result v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 681
    invoke-static {v2}, Lfss;->g(Lmnk;)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 682
    invoke-static {v2}, Lfss;->h(Lmnk;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setGravity(I)V

    .line 683
    invoke-virtual {v3}, Lfrc;->V()I

    move-result v1

    .line 684
    const v0, 0x7fffffff

    if-ne v1, v0, :cond_14

    const/4 v0, 0x0

    :goto_f
    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 685
    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setMaxLines(I)V

    move-object v9, p2

    .line 687
    goto/16 :goto_3

    .line 660
    :cond_d
    new-instance p2, Landroid/widget/TextView;

    iget-object v0, p0, Lfqu;->g:Landroid/content/Context;

    invoke-direct {p2, v0}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    goto/16 :goto_a

    .line 670
    :cond_e
    const/4 v0, 0x0

    goto :goto_b

    :pswitch_4
    const/4 v0, 0x0

    move v1, v0

    goto :goto_c

    :cond_f
    invoke-direct {p0, v2}, Lfqu;->d(Lmnk;)Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-virtual {p0, v2}, Lfqu;->a(Lmnk;)I

    move-result v1

    const/4 v5, 0x1

    if-ne v1, v5, :cond_10

    packed-switch v0, :pswitch_data_3

    :pswitch_5
    const v0, 0x7f020081

    move v1, v0

    goto :goto_c

    :pswitch_6
    const/4 v0, 0x0

    move v1, v0

    goto :goto_c

    :pswitch_7
    const v0, 0x7f020083

    move v1, v0

    goto :goto_c

    :cond_10
    const/4 v0, 0x0

    move v1, v0

    goto :goto_c

    :cond_11
    const/4 v0, 0x0

    move v1, v0

    goto :goto_c

    .line 672
    :cond_12
    const/4 v0, 0x0

    goto :goto_d

    .line 676
    :cond_13
    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_e

    .line 684
    :cond_14
    sget-object v0, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    goto :goto_f

    .line 690
    :pswitch_8
    if-eqz p2, :cond_15

    check-cast p2, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;

    .line 694
    :goto_10
    const v0, 0x7f100366

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 695
    const v0, 0x7f100031

    invoke-virtual {v6, v0, v3}, Lcom/google/android/libraries/social/media/ui/MediaView;->setTag(ILjava/lang/Object;)V

    .line 696
    iget-object v0, p0, Lfqu;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    .line 697
    const/4 v0, 0x5

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(I)V

    .line 699
    invoke-virtual {p0}, Lfqu;->c()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lfqu;->b:Lmmq;

    iget-object v5, p0, Lfqu;->k:Lfod;

    .line 698
    invoke-static/range {v0 .. v5}, Lfqu;->a(Landroid/content/Context;Lmmq;Lmnk;Lfrc;FLfod;)Lizu;

    move-result-object v0

    .line 700
    invoke-virtual {v6, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 701
    const/4 v1, 0x1

    invoke-virtual {v6, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->f(Z)V

    .line 702
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    iget-object v4, p0, Lfqu;->g:Landroid/content/Context;

    .line 703
    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00c0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v1, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 702
    invoke-virtual {v6, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->b(Landroid/graphics/drawable/Drawable;)V

    .line 704
    const/16 v1, 0x2024

    invoke-virtual {v6, v1}, Lcom/google/android/libraries/social/media/ui/MediaView;->i(I)V

    .line 706
    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v0

    sget-object v1, Ljac;->c:Ljac;

    if-eq v0, v1, :cond_16

    const/4 v0, 0x1

    :goto_11
    invoke-virtual {v6, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->h(Z)V

    .line 707
    iget-object v0, p0, Lfqu;->g:Landroid/content/Context;

    const v1, 0x7f0a01cd

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 709
    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v1, v2, Lmnk;->d:Lmnj;

    iget-object v1, v1, Lmnj;->b:Lmms;

    const-class v4, Lmml;

    invoke-static {v0, v1, v4}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmml;

    .line 711
    iget-object v1, p0, Lfqu;->k:Lfod;

    if-eqz v0, :cond_17

    iget-object v0, v0, Lmml;->d:Lnzx;

    iget-object v0, v0, Lnzx;->b:Ljava/lang/String;

    :goto_12
    iget-boolean v4, p0, Lfqu;->n:Z

    iget-object v5, p0, Lfqu;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {p2, v1, v0, v4, v5}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->a(Lfod;Ljava/lang/String;ZLandroid/view/View$OnClickListener;)V

    .line 713
    invoke-static {v2}, Lfqu;->c(Lmnk;)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->setId(I)V

    .line 714
    const v0, 0x7f100031

    invoke-virtual {p2, v0, v3}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->setTag(ILjava/lang/Object;)V

    move-object v9, p2

    .line 716
    goto/16 :goto_3

    .line 690
    :cond_15
    iget-object v0, p0, Lfqu;->g:Landroid/content/Context;

    const-string v1, "layout_inflater"

    .line 692
    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v1, 0x7f040108

    const/4 v4, 0x0

    .line 693
    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;

    move-object p2, v0

    goto/16 :goto_10

    .line 706
    :cond_16
    const/4 v0, 0x0

    goto :goto_11

    .line 711
    :cond_17
    const/4 v0, 0x0

    goto :goto_12

    .line 719
    :pswitch_9
    if-eqz p2, :cond_1d

    check-cast p2, Lftv;

    .line 721
    :goto_13
    const v0, 0x7f100031

    invoke-virtual {p2, v0, v3}, Lftv;->setTag(ILjava/lang/Object;)V

    .line 722
    iget-object v0, p0, Lfqu;->g:Landroid/content/Context;

    .line 723
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b00c0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 722
    invoke-virtual {p2, v0}, Lftv;->e(I)V

    .line 724
    invoke-virtual {v3}, Lfrc;->O()I

    move-result v0

    invoke-virtual {p2, v0}, Lftv;->a(I)V

    .line 725
    invoke-virtual {v3}, Lfrc;->P()Z

    move-result v0

    .line 726
    invoke-virtual {p2, v0}, Lftv;->a(Z)V

    .line 727
    if-eqz v0, :cond_18

    .line 728
    invoke-virtual {v3}, Lfrc;->Q()I

    move-result v0

    invoke-virtual {v3}, Lfrc;->R()I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p2, v0, v1}, Lftv;->a(IF)V

    .line 730
    :cond_18
    invoke-virtual {v3}, Lfrc;->Z()Z

    move-result v0

    if-eqz v0, :cond_19

    .line 731
    invoke-virtual {v3}, Lfrc;->aa()I

    move-result v0

    invoke-virtual {p2, v0}, Lftv;->b(I)V

    .line 733
    :cond_19
    invoke-virtual {v3}, Lfrc;->ad()Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 734
    invoke-virtual {v3}, Lfrc;->ae()Lfrg;

    move-result-object v0

    invoke-virtual {p2, v0}, Lftv;->a(Lfrg;)V

    .line 736
    :cond_1a
    invoke-virtual {v3}, Lfrc;->ab()Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 737
    invoke-virtual {v3}, Lfrc;->ac()F

    move-result v0

    invoke-virtual {p2, v0}, Lftv;->a(F)V

    .line 739
    :cond_1b
    iget-object v0, p0, Lfqu;->g:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v4, v0, Landroid/util/DisplayMetrics;->density:F

    .line 741
    invoke-virtual {p0}, Lfqu;->c()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lfqu;->b:Lmmq;

    iget-object v5, p0, Lfqu;->k:Lfod;

    .line 740
    invoke-static/range {v0 .. v5}, Lfqu;->a(Landroid/content/Context;Lmmq;Lmnk;Lfrc;FLfod;)Lizu;

    move-result-object v0

    invoke-virtual {p2, v0}, Lftv;->a(Lizu;)V

    .line 742
    invoke-static {v2}, Lfqu;->c(Lmnk;)I

    move-result v0

    invoke-virtual {p2, v0}, Lftv;->setId(I)V

    .line 743
    const/4 v0, 0x5

    invoke-virtual {p2, v0}, Lftv;->c(I)V

    .line 744
    iget-object v0, v2, Lmnk;->d:Lmnj;

    iget v0, v0, Lmnj;->a:I

    .line 745
    const/16 v1, 0xc

    if-ne v0, v1, :cond_1e

    .line 746
    iget-object v0, p0, Lfqu;->g:Landroid/content/Context;

    const v1, 0x7f0a01ce

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lftv;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 754
    :cond_1c
    :goto_14
    const/16 v0, 0x2020

    invoke-virtual {p2, v0}, Lftv;->d(I)V

    .line 757
    invoke-virtual {p2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    .line 758
    iget-object v1, p0, Lfqu;->o:Lfrw;

    invoke-virtual {v3}, Lfrc;->U()I

    move-result v4

    const/16 v5, 0xbb8

    invoke-virtual {v1, v4, v0, v5}, Lfrw;->a(ILjava/lang/String;I)V

    .line 760
    new-instance v1, Lfqw;

    invoke-direct {v1, p0, v3, v0}, Lfqw;-><init>(Lfqu;Lfrc;Ljava/lang/String;)V

    invoke-virtual {p2, v1}, Lftv;->a(Lfty;)V

    move-object v9, p2

    .line 769
    goto/16 :goto_3

    .line 719
    :cond_1d
    new-instance p2, Lftv;

    iget-object v0, p0, Lfqu;->g:Landroid/content/Context;

    invoke-direct {p2, v0}, Lftv;-><init>(Landroid/content/Context;)V

    goto/16 :goto_13

    .line 747
    :cond_1e
    const/4 v1, 0x1

    if-ne v0, v1, :cond_1f

    .line 748
    iget-object v0, p0, Lfqu;->g:Landroid/content/Context;

    const v1, 0x7f0a01cc

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lftv;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_14

    .line 749
    :cond_1f
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1c

    .line 752
    const/4 v0, 0x4

    invoke-virtual {p2, v0}, Lftv;->c(I)V

    goto :goto_14

    .line 773
    :pswitch_a
    new-instance v0, Lkoe;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Lkoe;-><init>(I)V

    invoke-virtual {p0}, Lfqu;->c()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lkoe;->a(Landroid/content/Context;)V

    .line 774
    if-eqz p2, :cond_20

    check-cast p2, Lfto;

    .line 777
    :goto_15
    const v0, 0x7f100031

    invoke-virtual {p2, v0, v3}, Lfto;->setTag(ILjava/lang/Object;)V

    .line 778
    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v0, v0, Lmmq;->c:Lmoo;

    if-eqz v0, :cond_21

    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v0, v0, Lmmq;->c:Lmoo;

    iget-object v0, v0, Lmoo;->d:[Lnzx;

    array-length v0, v0

    :goto_16
    new-array v4, v0, [Lfts;

    .line 780
    array-length v0, v4

    invoke-virtual {v3, v0}, Lfrc;->a(I)[Lfrf;

    move-result-object v5

    .line 781
    const/4 v0, 0x0

    move v1, v0

    :goto_17
    array-length v0, v4

    if-ge v1, v0, :cond_24

    .line 782
    iget-object v0, p0, Lfqu;->g:Landroid/content/Context;

    iget-object v6, p0, Lfqu;->b:Lmmq;

    iget-object v6, v6, Lmmq;->c:Lmoo;

    iget-object v6, v6, Lmoo;->d:[Lnzx;

    aget-object v6, v6, v1

    iget-object v7, p0, Lfqu;->k:Lfod;

    invoke-static {v0, v6, v7}, Lfqu;->a(Landroid/content/Context;Lnzx;Lfod;)Lizu;

    move-result-object v6

    .line 783
    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v0, v0, Lmmq;->c:Lmoo;

    iget-object v0, v0, Lmoo;->d:[Lnzx;

    aget-object v0, v0, v1

    if-eqz v0, :cond_22

    sget-object v7, Lnzu;->a:Loxr;

    invoke-virtual {v0, v7}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    if-eqz v0, :cond_22

    iget-object v0, v0, Lnzu;->b:Lnym;

    if-eqz v0, :cond_22

    iget-object v7, v0, Lnym;->b:Lnyl;

    if-eqz v7, :cond_22

    iget-object v7, v0, Lnym;->b:Lnyl;

    iget-object v7, v7, Lnyl;->c:Ljava/lang/Integer;

    if-eqz v7, :cond_22

    iget-object v0, v0, Lnym;->b:Lnyl;

    iget-object v0, v0, Lnyl;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 784
    :goto_18
    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v0, v0, Lmmq;->c:Lmoo;

    iget-object v0, v0, Lmoo;->d:[Lnzx;

    aget-object v0, v0, v1

    if-eqz v0, :cond_23

    sget-object v7, Lnzu;->a:Loxr;

    invoke-virtual {v0, v7}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    if-eqz v0, :cond_23

    iget-object v0, v0, Lnzu;->b:Lnym;

    if-eqz v0, :cond_23

    iget-object v7, v0, Lnym;->b:Lnyl;

    if-eqz v7, :cond_23

    iget-object v7, v0, Lnym;->b:Lnyl;

    iget-object v7, v7, Lnyl;->d:Ljava/lang/Integer;

    if-eqz v7, :cond_23

    iget-object v0, v0, Lnym;->b:Lnyl;

    iget-object v0, v0, Lnyl;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    .line 785
    :goto_19
    new-instance v0, Lfts;

    aget-object v7, v5, v1

    invoke-direct {v0, v6, v7}, Lfts;-><init>(Lizu;Lfrf;)V

    aput-object v0, v4, v1

    .line 781
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_17

    .line 774
    :cond_20
    new-instance p2, Lfto;

    iget-object v0, p0, Lfqu;->g:Landroid/content/Context;

    invoke-direct {p2, v0}, Lfto;-><init>(Landroid/content/Context;)V

    goto/16 :goto_15

    .line 778
    :cond_21
    const/4 v0, 0x0

    goto/16 :goto_16

    .line 783
    :cond_22
    const-string v0, "StoryLayoutAdapter"

    const-string v7, "Cover image width is missing"

    invoke-static {v0, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_18

    .line 784
    :cond_23
    const-string v0, "StoryLayoutAdapter"

    const-string v7, "Cover image height is missing"

    invoke-static {v0, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_19

    .line 787
    :cond_24
    invoke-virtual {p2, v4}, Lfto;->a([Lfts;)V

    .line 788
    invoke-virtual {v3}, Lfrc;->Z()Z

    move-result v0

    if-eqz v0, :cond_25

    .line 789
    invoke-virtual {v3}, Lfrc;->aa()I

    move-result v0

    invoke-virtual {p2, v0}, Lfto;->b(I)V

    .line 791
    :cond_25
    invoke-virtual {v3}, Lfrc;->ad()Z

    move-result v0

    if-eqz v0, :cond_26

    .line 792
    invoke-virtual {v3}, Lfrc;->ae()Lfrg;

    move-result-object v0

    invoke-virtual {p2, v0}, Lfto;->a(Lfrg;)V

    .line 794
    :cond_26
    iget-object v0, p0, Lfqu;->g:Landroid/content/Context;

    const v1, 0x7f0a01d9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Lfto;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 795
    const/16 v0, 0x2020

    invoke-virtual {p2, v0}, Lfto;->a(I)V

    .line 796
    new-instance v0, Lfqx;

    invoke-direct {v0, p0}, Lfqx;-><init>(Lfqu;)V

    invoke-virtual {p2, v0}, Lfto;->a(Lftt;)V

    move-object v9, p2

    .line 805
    goto/16 :goto_3

    .line 808
    :pswitch_b
    if-eqz p2, :cond_27

    check-cast p2, Lftn;

    .line 811
    :goto_1a
    const v0, 0x7f100031

    invoke-virtual {p2, v0, v3}, Lftn;->setTag(ILjava/lang/Object;)V

    .line 812
    invoke-virtual {v3}, Lfrc;->B()I

    move-result v0

    invoke-virtual {p2, v0}, Lftn;->a(I)V

    .line 813
    invoke-virtual {v3}, Lfrc;->C()I

    move-result v0

    invoke-virtual {p2, v0}, Lftn;->b(I)V

    move-object v9, p2

    .line 815
    goto/16 :goto_3

    .line 808
    :cond_27
    new-instance p2, Lftn;

    iget-object v0, p0, Lfqu;->g:Landroid/content/Context;

    invoke-direct {p2, v0}, Lftn;-><init>(Landroid/content/Context;)V

    goto :goto_1a

    .line 818
    :pswitch_c
    if-eqz p2, :cond_28

    check-cast p2, Landroid/widget/ImageView;

    .line 820
    :goto_1b
    const v0, 0x7f100031

    invoke-virtual {p2, v0, v3}, Landroid/widget/ImageView;->setTag(ILjava/lang/Object;)V

    .line 821
    invoke-static {v2}, Lfqu;->e(Lmnk;)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    move-object v9, p2

    .line 823
    goto/16 :goto_3

    .line 818
    :cond_28
    new-instance p2, Landroid/widget/ImageView;

    iget-object v0, p0, Lfqu;->g:Landroid/content/Context;

    invoke-direct {p2, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    goto :goto_1b

    .line 826
    :pswitch_d
    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v1, v2, Lmnk;->d:Lmnj;

    iget-object v1, v1, Lmnj;->b:Lmms;

    const-class v4, Lodo;

    invoke-static {v0, v1, v4}, Lfss;->b(Lmmq;Lmms;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lodo;

    .line 828
    if-eqz p2, :cond_29

    check-cast p2, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 830
    :goto_1c
    const v1, 0x7f100031

    invoke-virtual {p2, v1, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setTag(ILjava/lang/Object;)V

    .line 831
    if-eqz v0, :cond_2a

    iget-object v1, v0, Lodo;->c:Ljava/lang/String;

    :goto_1d
    if-eqz v0, :cond_2b

    iget-object v0, v0, Lodo;->f:Ljava/lang/String;

    :goto_1e
    invoke-virtual {p2, v1, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 838
    invoke-virtual {v3}, Lfrc;->k()Z

    move-result v0

    if-eqz v0, :cond_2d

    .line 839
    invoke-virtual {v3}, Lfrc;->m()Z

    move-result v0

    if-eqz v0, :cond_2c

    invoke-virtual {v3}, Lfrc;->l()I

    move-result v0

    invoke-virtual {v3}, Lfrc;->n()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 842
    :goto_1f
    invoke-direct {p0, v0}, Lfqu;->d(I)I

    move-result v0

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->b(I)V

    .line 843
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    .line 844
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Z)V

    move-object v9, p2

    .line 846
    goto/16 :goto_3

    .line 828
    :cond_29
    new-instance p2, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iget-object v1, p0, Lfqu;->g:Landroid/content/Context;

    invoke-direct {p2, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    goto :goto_1c

    .line 831
    :cond_2a
    const/4 v1, 0x0

    goto :goto_1d

    :cond_2b
    const/4 v0, 0x0

    goto :goto_1e

    .line 840
    :cond_2c
    invoke-virtual {v3}, Lfrc;->l()I

    move-result v0

    goto :goto_1f

    .line 841
    :cond_2d
    invoke-virtual {v3}, Lfrc;->m()Z

    move-result v0

    if-eqz v0, :cond_2e

    invoke-virtual {v3}, Lfrc;->n()I

    move-result v0

    goto :goto_1f

    :cond_2e
    invoke-virtual {v3}, Lfrc;->J()I

    move-result v0

    goto :goto_1f

    .line 849
    :pswitch_e
    if-eqz p2, :cond_2f

    .line 850
    :goto_20
    const v0, 0x7f100031

    invoke-virtual {p2, v0, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 851
    sget-object v0, Lfqu;->d:Lloz;

    goto/16 :goto_1

    .line 849
    :cond_2f
    new-instance p2, Landroid/view/View;

    iget-object v0, p0, Lfqu;->g:Landroid/content/Context;

    invoke-direct {p2, v0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    goto :goto_20

    .line 866
    :cond_30
    const/4 v0, 0x0

    goto/16 :goto_4

    .line 883
    :pswitch_f
    iget-object v4, p0, Lfqu;->o:Lfrw;

    iget-wide v6, v0, Lfqy;->c:J

    iget-wide v10, v0, Lfqy;->b:J

    add-long/2addr v6, v10

    iget-object v0, v0, Lfqy;->e:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    .line 886
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 883
    invoke-virtual/range {v4 .. v9}, Lfrw;->a(Lfsc;JILandroid/view/View;)V

    goto/16 :goto_5

    .line 891
    :pswitch_10
    iget-object v4, p0, Lfqu;->o:Lfrw;

    const/4 v6, 0x1

    iget-object v0, v0, Lfqy;->e:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    .line 895
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 891
    invoke-virtual/range {v4 .. v9}, Lfrw;->a(Lfsc;ZIILandroid/view/View;)V

    goto/16 :goto_5

    .line 900
    :pswitch_11
    iget-object v4, p0, Lfqu;->o:Lfrw;

    const/4 v6, 0x0

    iget-object v0, v0, Lfqy;->e:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    .line 904
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 900
    invoke-virtual/range {v4 .. v9}, Lfrw;->a(Lfsc;ZIILandroid/view/View;)V

    goto/16 :goto_5

    .line 915
    :cond_31
    invoke-static {v9}, Llhn;->a(Landroid/view/View;)V

    .line 916
    invoke-virtual {p0, v2}, Lfqu;->a(Lmnk;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Landroid/view/View;->setClickable(Z)V

    :cond_32
    :goto_21
    invoke-direct {p0, v2, v3, v9}, Lfqu;->a(Lmnk;Lfrc;Landroid/view/View;)V

    const v0, 0x7f100039

    const/4 v1, 0x0

    invoke-virtual {v9, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    if-eqz v2, :cond_39

    iget-object v0, v2, Lmnk;->i:Lmnl;

    if-eqz v0, :cond_39

    iget-object v0, v2, Lmnk;->i:Lmnl;

    iget-object v0, v0, Lmnl;->a:Lmnm;

    :goto_22
    if-eqz v0, :cond_33

    iget v1, v0, Lmnm;->a:I

    packed-switch v1, :pswitch_data_4

    iget v0, v0, Lmnm;->a:I

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x25

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "Unrecognized action type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    :cond_33
    :goto_23
    move-object p2, v9

    .line 918
    goto/16 :goto_1

    .line 916
    :sswitch_0
    invoke-static {v2}, Lfqu;->c(Lmnk;)I

    move-result v1

    const v0, 0x7f10003d

    iget-object v4, v2, Lmnk;->d:Lmnj;

    iget-object v4, v4, Lmnj;->b:Lmms;

    invoke-virtual {v9, v0, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const v0, 0x7f10002f

    if-ne v1, v0, :cond_34

    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v4, v2, Lmnk;->d:Lmnj;

    iget-object v4, v4, Lmnj;->b:Lmms;

    const-class v5, Lmml;

    invoke-static {v0, v4, v5}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmml;

    const v4, 0x7f100079

    iget-object v5, v0, Lmml;->d:Lnzx;

    iget-object v5, v5, Lnzx;->b:Ljava/lang/String;

    invoke-virtual {v9, v4, v5}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const v4, 0x7f10006b

    iget-object v0, v0, Lmml;->d:Lnzx;

    sget-object v5, Lnzu;->a:Loxr;

    invoke-virtual {v0, v5}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    iget-object v0, v0, Lnzu;->b:Lnym;

    iget-object v0, v0, Lnym;->e:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v9, v4, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    :cond_34
    invoke-direct {p0, v2}, Lfqu;->d(Lmnk;)Z

    move-result v4

    if-nez v4, :cond_35

    const v0, 0x7f10002c

    if-eq v1, v0, :cond_35

    const v0, 0x7f10002f

    if-ne v1, v0, :cond_37

    :cond_35
    const/4 v0, 0x1

    :goto_24
    invoke-virtual {v9, v0}, Landroid/view/View;->setClickable(Z)V

    invoke-virtual {v9}, Landroid/view/View;->isClickable()Z

    move-result v0

    if-eqz v0, :cond_36

    iget-object v0, p0, Lfqu;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v9, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_36
    if-eqz v4, :cond_32

    const v0, 0x7f10002c

    if-ne v1, v0, :cond_38

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/CharSequence;

    const/4 v5, 0x0

    move-object v0, v9

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    iget-object v5, p0, Lfqu;->g:Landroid/content/Context;

    const v6, 0x7f0a01d0

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v1, v4}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_21

    :cond_37
    const/4 v0, 0x0

    goto :goto_24

    :cond_38
    const v0, 0x7f10002f

    if-ne v1, v0, :cond_32

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/CharSequence;

    const/4 v5, 0x0

    move-object v0, v9

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    iget-object v5, p0, Lfqu;->g:Landroid/content/Context;

    const v6, 0x7f0a01cf

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v1, v4}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_21

    :sswitch_1
    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v1, v2, Lmnk;->d:Lmnj;

    iget-object v1, v1, Lmnj;->b:Lmms;

    const-class v4, Lmml;

    invoke-static {v0, v1, v4}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmml;

    iget-object v0, v0, Lmml;->d:Lnzx;

    iget-object v1, v0, Lnzx;->b:Ljava/lang/String;

    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v4, v2, Lmnk;->d:Lmnj;

    iget-object v4, v4, Lmnj;->b:Lmms;

    const-class v5, Lmml;

    invoke-static {v0, v4, v5}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmml;

    iget-object v0, v0, Lmml;->d:Lnzx;

    sget-object v4, Lnzu;->a:Loxr;

    invoke-virtual {v0, v4}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    iget-object v0, v0, Lnzu;->b:Lnym;

    const v4, 0x7f100368

    invoke-virtual {v9, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f100034

    invoke-virtual {v4, v5, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const v5, 0x7f100079

    invoke-virtual {v4, v5, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const v4, 0x7f100367

    invoke-virtual {v9, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f100034

    invoke-virtual {v4, v5, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const v5, 0x7f100079

    invoke-virtual {v4, v5, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const v5, 0x7f100044

    iget-object v6, p0, Lfqu;->k:Lfod;

    invoke-virtual {v6, v1}, Lfod;->e(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v4, v5, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    iget-object v1, p0, Lfqu;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v9, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v1, 0x1

    invoke-virtual {v9, v1}, Landroid/view/View;->setClickable(Z)V

    const v1, 0x7f100034

    invoke-virtual {v9, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto/16 :goto_21

    :sswitch_2
    iget-object v0, p0, Lfqu;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v9, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Landroid/view/View;->setClickable(Z)V

    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v1, v2, Lmnk;->d:Lmnj;

    iget-object v1, v1, Lmnj;->b:Lmms;

    const-class v4, Lmml;

    invoke-static {v0, v1, v4}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmml;

    const v1, 0x7f100079

    iget-object v4, v0, Lmml;->d:Lnzx;

    iget-object v4, v4, Lnzx;->b:Ljava/lang/String;

    invoke-virtual {v9, v1, v4}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const v1, 0x7f10006b

    iget-object v0, v0, Lmml;->d:Lnzx;

    sget-object v4, Lnzu;->a:Loxr;

    invoke-virtual {v0, v4}, Lnzx;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lnzu;

    iget-object v0, v0, Lnzu;->b:Lnym;

    iget-object v0, v0, Lnym;->e:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v9, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/CharSequence;

    const/4 v5, 0x0

    move-object v0, v9

    check-cast v0, Lftm;

    invoke-virtual {v0}, Lftm;->b()Ljava/lang/CharSequence;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    iget-object v5, p0, Lfqu;->g:Landroid/content/Context;

    const v6, 0x7f0a01cf

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v1, v4}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_21

    :cond_39
    const/4 v0, 0x0

    goto/16 :goto_22

    :pswitch_12
    iget-object v1, p0, Lfqu;->b:Lmmq;

    iget-object v0, v0, Lmnm;->b:Lmms;

    const-class v2, Lmml;

    invoke-static {v1, v0, v2}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmml;

    if-eqz v0, :cond_3c

    iget-object v1, v0, Lmml;->e:Lmmn;

    if-eqz v1, :cond_3c

    iget-object v0, v0, Lmml;->e:Lmmn;

    iget-object v0, v0, Lmmn;->a:Lmmo;

    :goto_25
    if-eqz v0, :cond_33

    iget-object v1, v0, Lmmo;->b:Lofq;

    if-eqz v1, :cond_3a

    iget-object v1, v0, Lmmo;->b:Lofq;

    iget-object v1, v1, Lofq;->e:Ljava/lang/String;

    if-eqz v1, :cond_3a

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    invoke-virtual {v9}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, v0, Lmmo;->b:Lofq;

    iget-object v4, v4, Lofq;->e:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lfqu;->g:Landroid/content/Context;

    const v5, 0x7f0a01c9

    invoke-virtual {v4, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v9, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_3a
    iget v1, v0, Lmmo;->c:I

    const/4 v2, 0x4

    if-eq v1, v2, :cond_3b

    iget v1, v0, Lmmo;->c:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_3b

    iget v1, v0, Lmmo;->c:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3d

    :cond_3b
    iget-object v1, v0, Lmmo;->e:Lodo;

    if-eqz v1, :cond_33

    iget-object v1, v0, Lmmo;->e:Lodo;

    iget-object v1, v1, Lodo;->c:Ljava/lang/String;

    if-eqz v1, :cond_33

    const v1, 0x7f100039

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const v1, 0x7f100032

    iget-object v0, v0, Lmmo;->e:Lodo;

    iget-object v0, v0, Lodo;->c:Ljava/lang/String;

    invoke-virtual {v9, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    :goto_26
    iget-object v0, p0, Lfqu;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v9, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x1

    invoke-virtual {v9, v0}, Landroid/view/View;->setClickable(Z)V

    goto/16 :goto_23

    :cond_3c
    const/4 v0, 0x0

    goto :goto_25

    :cond_3d
    iget-object v1, v0, Lmmo;->b:Lofq;

    if-eqz v1, :cond_3f

    iget v0, v0, Lmmo;->c:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_3e

    iget-object v0, v1, Lofq;->i:Ljava/lang/String;

    if-eqz v0, :cond_3e

    iget-object v0, v1, Lofq;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_27
    if-eqz v0, :cond_33

    const v1, 0x7f100039

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const v1, 0x7f10003a

    invoke-virtual {v9, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_26

    :cond_3e
    iget-object v0, v1, Lofq;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_3f

    iget-object v0, v1, Lofq;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_3f

    iget-object v0, v1, Lofq;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v2, v1, Lofq;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lfsp;->a(II)Ling;

    move-result-object v0

    iget-wide v2, v0, Ling;->a:D

    iget-wide v4, v0, Ling;->b:D

    iget-object v1, v1, Lofq;->e:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v7

    add-int/lit8 v7, v7, 0x33

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-wide v2, v0, Ling;->a:D

    iget-wide v4, v0, Ling;->b:D

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x46

    invoke-direct {v0, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "geo:"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "?q="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "&z=16"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_27

    :cond_3f
    const/4 v0, 0x0

    goto/16 :goto_27

    :pswitch_13
    iget-object v1, p0, Lfqu;->b:Lmmq;

    iget-object v0, v0, Lmnm;->b:Lmms;

    const-class v2, Lodo;

    invoke-static {v1, v0, v2}, Lfss;->b(Lmmq;Lmms;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lodo;

    if-eqz v0, :cond_33

    iget-object v1, v0, Lodo;->c:Ljava/lang/String;

    if-eqz v1, :cond_33

    const v1, 0x7f100039

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const v1, 0x7f100032

    iget-object v0, v0, Lodo;->c:Ljava/lang/String;

    invoke-virtual {v9, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto/16 :goto_26

    :pswitch_14
    iget-object v1, p0, Lfqu;->b:Lmmq;

    iget-object v0, v0, Lmnm;->b:Lmms;

    const-class v2, Lmmc;

    invoke-static {v1, v0, v2}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmmc;

    if-eqz v0, :cond_33

    iget-object v1, v0, Lmmc;->d:Lmme;

    if-eqz v1, :cond_33

    iget-object v1, v0, Lmmc;->d:Lmme;

    iget-object v1, v1, Lmme;->a:Ljava/lang/String;

    if-eqz v1, :cond_33

    const v1, 0x7f100039

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const v1, 0x7f10003b

    iget-object v2, v0, Lmmc;->d:Lmme;

    iget-object v2, v2, Lmme;->a:Ljava/lang/String;

    invoke-virtual {v9, v1, v2}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    invoke-virtual {v9}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v0, v0, Lmmc;->d:Lmme;

    iget-object v0, v0, Lmme;->a:Ljava/lang/String;

    aput-object v0, v2, v3

    const/4 v0, 0x2

    iget-object v3, p0, Lfqu;->g:Landroid/content/Context;

    const v4, 0x7f0a01ca

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_26

    :pswitch_15
    iget-object v1, p0, Lfqu;->b:Lmmq;

    iget-object v0, v0, Lmnm;->b:Lmms;

    const-class v2, Lmml;

    invoke-static {v1, v0, v2}, Lfss;->a(Lmmq;Lmms;Ljava/lang/Class;)Loxu;

    move-result-object v0

    check-cast v0, Lmml;

    if-eqz v0, :cond_33

    iget-object v1, v0, Lmml;->e:Lmmn;

    if-eqz v1, :cond_33

    iget-object v1, v0, Lmml;->e:Lmmn;

    invoke-static {v1}, Lfsp;->a(Lmmn;)[Lfof;

    move-result-object v1

    iget-object v0, v0, Lmml;->e:Lmmn;

    iget-object v0, v0, Lmmn;->a:Lmmo;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lfsp;->a(Lmmo;Z)Lfof;

    move-result-object v0

    const v2, 0x7f100039

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const v2, 0x7f100035

    invoke-virtual {v9, v2, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    const v1, 0x7f100038

    invoke-virtual {v9, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    if-eqz v0, :cond_40

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/CharSequence;

    const/4 v3, 0x0

    invoke-virtual {v9}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v0}, Lfof;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    const/4 v0, 0x2

    iget-object v3, p0, Lfqu;->g:Landroid/content/Context;

    const v4, 0x7f0a01cb

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    :cond_40
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_26

    :pswitch_16
    const v0, 0x7f100039

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v9, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/CharSequence;

    const/4 v2, 0x0

    invoke-virtual {v9}, Landroid/view/View;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lfqu;->g:Landroid/content/Context;

    const v4, 0x7f0a01c8

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Llhn;->a(Ljava/lang/StringBuilder;[Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_26

    .line 600
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_1
        :pswitch_2
        :pswitch_8
        :pswitch_e
    .end packed-switch

    .line 881
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_f
        :pswitch_10
        :pswitch_11
    .end packed-switch

    .line 670
    :pswitch_data_2
    .packed-switch 0x4
        :pswitch_4
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x2
        :pswitch_7
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 916
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x8 -> :sswitch_2
        0x9 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_15
        :pswitch_14
        :pswitch_12
        :pswitch_16
        :pswitch_13
    .end packed-switch
.end method

.method protected a(Lmnk;Lfrc;)Lfrc;
    .locals 2

    .prologue
    .line 577
    new-instance v0, Lfrc;

    iget v1, p0, Lfqu;->l:I

    invoke-direct {v0, p1, v1, p2}, Lfrc;-><init>(Lmnk;ILfrc;)V

    return-object v0
.end method

.method public a()Lfrj;
    .locals 1

    .prologue
    .line 928
    iget-object v0, p0, Lfqu;->h:Lfrj;

    return-object v0
.end method

.method public a(I)Lmnk;
    .locals 2

    .prologue
    .line 360
    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v0, v0, Lmmq;->h:[Lmmt;

    iget v1, p0, Lfqu;->c:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lmmt;->d:[Lmnk;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public a(II)V
    .locals 3

    .prologue
    .line 240
    iget v0, p0, Lfqu;->c:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lfqu;->l:I

    if-ne v0, p2, :cond_0

    .line 249
    :goto_0
    return-void

    .line 244
    :cond_0
    iput p1, p0, Lfqu;->c:I

    .line 245
    iput p2, p0, Lfqu;->l:I

    .line 246
    new-instance v0, Lfrb;

    iget-object v1, p0, Lfqu;->b:Lmmq;

    iget-object v1, v1, Lmmq;->h:[Lmmt;

    iget v2, p0, Lfqu;->c:I

    aget-object v1, v1, v2

    iget v2, p0, Lfqu;->l:I

    invoke-direct {v0, v1, v2}, Lfrb;-><init>(Lmmt;I)V

    iput-object v0, p0, Lfqu;->m:Lfrb;

    .line 247
    iget-object v0, p0, Lfqu;->p:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v0, v0, Lmmq;->h:[Lmmt;

    iget v1, p0, Lfqu;->c:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lmmt;->d:[Lmnk;

    invoke-direct {p0, v0}, Lfqu;->a([Lmnk;)V

    .line 248
    iget-object v0, p0, Lfqu;->f:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    goto :goto_0
.end method

.method public a(Lfob;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 215
    .line 217
    if-eqz p1, :cond_2

    iget-object v0, p1, Lfob;->a:Lmmq;

    .line 218
    :goto_0
    iget-object v4, p0, Lfqu;->b:Lmmq;

    if-eq v4, v0, :cond_4

    .line 219
    iput-object v0, p0, Lfqu;->b:Lmmq;

    .line 220
    const/4 v0, -0x1

    iput v0, p0, Lfqu;->c:I

    .line 221
    iput-object v1, p0, Lfqu;->m:Lfrb;

    move v1, v2

    .line 225
    :goto_1
    if-eqz p1, :cond_3

    iget-object v0, p1, Lfob;->b:Lfod;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lfob;->b:Lfod;

    iget-object v4, p0, Lfqu;->k:Lfod;

    .line 226
    invoke-virtual {v0, v4}, Lfod;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 227
    iget-object v0, p1, Lfob;->b:Lfod;

    iput-object v0, p0, Lfqu;->k:Lfod;

    move v0, v2

    .line 231
    :goto_2
    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    .line 232
    :cond_0
    iget-object v0, p0, Lfqu;->f:Landroid/database/DataSetObservable;

    invoke-virtual {v0}, Landroid/database/DataSetObservable;->notifyChanged()V

    .line 234
    :cond_1
    return-void

    :cond_2
    move-object v0, v1

    .line 217
    goto :goto_0

    :cond_3
    move v0, v3

    goto :goto_2

    :cond_4
    move v1, v3

    goto :goto_1
.end method

.method public a(Lfrw;)V
    .locals 2

    .prologue
    .line 325
    iput-object p1, p0, Lfqu;->o:Lfrw;

    .line 327
    iget-object v0, p0, Lfqu;->h:Lfrj;

    new-instance v1, Lfqv;

    invoke-direct {v1, p0}, Lfqv;-><init>(Lfqu;)V

    invoke-virtual {v0, v1}, Lfrj;->a(Lfrk;)V

    .line 333
    return-void
.end method

.method public a(Ljava/lang/String;Landroid/view/View;)V
    .locals 7

    .prologue
    const/16 v6, 0xfa

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 378
    iget-object v0, p0, Lfqu;->k:Lfod;

    invoke-virtual {v0, p1}, Lfod;->e(Ljava/lang/String;)Z

    move-result v3

    .line 379
    iget-object v0, p0, Lfqu;->k:Lfod;

    invoke-virtual {v0, p1}, Lfod;->c(Ljava/lang/String;)I

    move-result v4

    .line 380
    iget-object v5, p0, Lfqu;->k:Lfod;

    if-eqz v3, :cond_0

    add-int/lit8 v0, v4, -0x1

    :goto_0
    invoke-virtual {v5, p1, v0}, Lfod;->a(Ljava/lang/String;I)Z

    .line 381
    iget-object v5, p0, Lfqu;->k:Lfod;

    if-nez v3, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v5, p1, v0}, Lfod;->a(Ljava/lang/String;Z)Z

    .line 384
    if-eqz v3, :cond_2

    .line 385
    const v0, 0x7f02058f

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 386
    invoke-static {p2}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->a(Landroid/view/View;)V

    .line 387
    invoke-virtual {p2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, v6}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 395
    :goto_2
    const v0, 0x7f100044

    if-nez v3, :cond_3

    :goto_3
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 396
    const v1, 0x7f100043

    if-eqz v3, :cond_4

    add-int/lit8 v0, v4, -0x1

    :goto_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p2, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 397
    return-void

    .line 380
    :cond_0
    add-int/lit8 v0, v4, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 381
    goto :goto_1

    .line 390
    :cond_2
    const v0, 0x7f020590

    invoke-virtual {p2, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 391
    invoke-static {p2}, Lcom/google/android/apps/plus/stories/views/StoryMediaFrameLayout;->a(Landroid/view/View;)V

    .line 392
    invoke-virtual {p2}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0, v6}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    goto :goto_2

    :cond_3
    move v1, v2

    .line 395
    goto :goto_3

    .line 396
    :cond_4
    add-int/lit8 v0, v4, 0x1

    goto :goto_4
.end method

.method public b(Ljava/util/List;)I
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lfqz;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v1, -0x1

    .line 464
    iget-object v0, p0, Lfqu;->b:Lmmq;

    if-eqz v0, :cond_1

    iget v0, p0, Lfqu;->c:I

    if-eq v0, v1, :cond_1

    .line 465
    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v0, v0, Lmmq;->h:[Lmmt;

    iget v2, p0, Lfqu;->c:I

    aget-object v0, v0, v2

    iget-object v2, v0, Lmmt;->d:[Lmnk;

    .line 466
    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 467
    aget-object v3, v2, v0

    .line 468
    iget-object v4, v3, Lmnk;->g:[B

    if-eqz v4, :cond_0

    new-instance v4, Lfqz;

    iget-object v3, v3, Lmnk;->g:[B

    invoke-direct {v4, v3}, Lfqz;-><init>([B)V

    .line 469
    invoke-interface {p1, v4}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 474
    :goto_1
    return v0

    .line 466
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 474
    goto :goto_1
.end method

.method public b()Lfrb;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lfqu;->m:Lfrb;

    return-object v0
.end method

.method public b(I)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lfqz;",
            ">;"
        }
    .end annotation

    .prologue
    .line 404
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 405
    iget-object v0, p0, Lfqu;->b:Lmmq;

    if-eqz v0, :cond_1

    iget v0, p0, Lfqu;->c:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    .line 406
    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v0, v0, Lmmq;->h:[Lmmt;

    iget v2, p0, Lfqu;->c:I

    aget-object v0, v0, v2

    iget-object v2, v0, Lmmt;->d:[Lmnk;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 407
    iget-object v5, v4, Lmnk;->g:[B

    if-eqz v5, :cond_0

    .line 408
    iget-object v5, v4, Lmnk;->f:Ljava/lang/Integer;

    invoke-static {v5}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v5

    if-lt v5, p1, :cond_0

    .line 412
    new-instance v5, Lfqz;

    iget-object v4, v4, Lmnk;->g:[B

    invoke-direct {v5, v4}, Lfqz;-><init>([B)V

    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 406
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 416
    :cond_1
    return-object v1
.end method

.method protected c()Landroid/content/Context;
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lfqu;->g:Landroid/content/Context;

    return-object v0
.end method

.method public c(I)Ljava/util/List;
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Lmms;",
            ">;"
        }
    .end annotation

    .prologue
    .line 424
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 425
    iget-object v0, p0, Lfqu;->b:Lmmq;

    if-eqz v0, :cond_1

    iget v0, p0, Lfqu;->c:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_1

    .line 426
    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v0, v0, Lmmq;->h:[Lmmt;

    iget v2, p0, Lfqu;->c:I

    aget-object v0, v0, v2

    iget-object v2, v0, Lmmt;->d:[Lmnk;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 427
    iget-object v5, v4, Lmnk;->g:[B

    if-eqz v5, :cond_0

    .line 428
    iget-object v5, v4, Lmnk;->f:Ljava/lang/Integer;

    invoke-static {v5}, Llsl;->a(Ljava/lang/Integer;)I

    move-result v5

    if-lt v5, p1, :cond_0

    iget-object v5, v4, Lmnk;->d:Lmnj;

    if-eqz v5, :cond_0

    iget-object v5, v4, Lmnk;->d:Lmnj;

    iget-object v5, v5, Lmnj;->b:Lmms;

    if-eqz v5, :cond_0

    .line 433
    iget-object v4, v4, Lmnk;->d:Lmnj;

    iget-object v4, v4, Lmnj;->b:Lmms;

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 426
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 437
    :cond_1
    return-object v1
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 349
    iget-object v0, p0, Lfqu;->b:Lmmq;

    if-eqz v0, :cond_0

    iget v0, p0, Lfqu;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lfqu;->b:Lmmq;

    iget-object v0, v0, Lmmq;->h:[Lmmt;

    iget v1, p0, Lfqu;->c:I

    aget-object v0, v0, v1

    iget-object v0, v0, Lmmt;->d:[Lmnk;

    array-length v0, v0

    goto :goto_0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 122
    invoke-virtual {p0, p1}, Lfqu;->a(I)Lmnk;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 370
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 571
    invoke-virtual {p0, p1}, Lfqu;->a(I)Lmnk;

    move-result-object v0

    invoke-virtual {p0, v0}, Lfqu;->a(Lmnk;)I

    move-result v0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 923
    invoke-virtual {p0, p1}, Lfqu;->a(I)Lmnk;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p3}, Lfqu;->a(Lmnk;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 479
    const/16 v0, 0xb

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 365
    const/4 v0, 0x0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 355
    invoke-virtual {p0}, Lfqu;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onSystemUiVisibilityChange(I)V
    .locals 1

    .prologue
    .line 321
    and-int/lit8 v0, p1, 0x4

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lfqu;->n:Z

    .line 322
    return-void

    .line 321
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 1407
    iget-object v0, p0, Lfqu;->f:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->registerObserver(Ljava/lang/Object;)V

    .line 1408
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 1412
    iget-object v0, p0, Lfqu;->f:Landroid/database/DataSetObservable;

    invoke-virtual {v0, p1}, Landroid/database/DataSetObservable;->unregisterObserver(Ljava/lang/Object;)V

    .line 1413
    return-void
.end method
