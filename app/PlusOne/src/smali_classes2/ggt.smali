.class final Lggt;
.super Ldo;

# interfaces
.implements Lggb;
.implements Lggc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldo",
        "<",
        "Lgfd;",
        ">;",
        "Lggb;",
        "Lggc;"
    }
.end annotation


# instance fields
.field public final b:Lgfz;

.field private c:Z

.field private d:Lgfd;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgfz;)V
    .locals 0

    invoke-direct {p0, p1}, Ldo;-><init>(Landroid/content/Context;)V

    iput-object p2, p0, Lggt;->b:Lgfz;

    return-void
.end method

.method private b(Lgfd;)V
    .locals 1

    iput-object p1, p0, Lggt;->d:Lgfd;

    invoke-virtual {p0}, Lggt;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lggt;->q()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lggt;->b(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lggt;->c:Z

    sget-object v0, Lgfd;->a:Lgfd;

    invoke-direct {p0, v0}, Lggt;->b(Lgfd;)V

    return-void
.end method

.method public a(Lgfd;)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lggt;->c:Z

    invoke-direct {p0, p1}, Lggt;->b(Lgfd;)V

    return-void
.end method

.method public b()V
    .locals 1

    iget-boolean v0, p0, Lggt;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lggt;->c:Z

    invoke-virtual {p0}, Lggt;->p()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lggt;->q()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lggt;->b:Lgfz;

    invoke-interface {v0}, Lgfz;->a()V

    :cond_0
    return-void
.end method

.method protected g()V
    .locals 1

    invoke-super {p0}, Ldo;->g()V

    iget-object v0, p0, Lggt;->b:Lgfz;

    invoke-interface {v0, p0}, Lgfz;->a(Lggb;)V

    iget-object v0, p0, Lggt;->b:Lgfz;

    invoke-interface {v0, p0}, Lgfz;->a(Lggc;)V

    iget-object v0, p0, Lggt;->d:Lgfd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lggt;->d:Lgfd;

    invoke-virtual {p0, v0}, Lggt;->b(Ljava/lang/Object;)V

    :cond_0
    iget-object v0, p0, Lggt;->b:Lgfz;

    invoke-interface {v0}, Lgfz;->d()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lggt;->b:Lgfz;

    invoke-interface {v0}, Lgfz;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lggt;->c:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lggt;->b:Lgfz;

    invoke-interface {v0}, Lgfz;->a()V

    :cond_1
    return-void
.end method

.method protected h()V
    .locals 1

    iget-object v0, p0, Lggt;->b:Lgfz;

    invoke-interface {v0}, Lgfz;->c()V

    return-void
.end method

.method protected i()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lggt;->d:Lgfd;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lggt;->c:Z

    iget-object v0, p0, Lggt;->b:Lgfz;

    invoke-interface {v0, p0}, Lgfz;->b(Lggb;)V

    iget-object v0, p0, Lggt;->b:Lgfz;

    invoke-interface {v0, p0}, Lgfz;->b(Lggc;)V

    iget-object v0, p0, Lggt;->b:Lgfz;

    invoke-interface {v0}, Lgfz;->c()V

    return-void
.end method
