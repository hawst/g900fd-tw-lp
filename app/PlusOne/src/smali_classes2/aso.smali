.class public Laso;
.super Lamn;
.source "PG"


# instance fields
.field private final a:Lali;

.field private final b:Lawp;

.field private final c:Ljfb;

.field private d:Ljeo;

.field private final e:Lass;

.field private final f:Lasr;

.field private final g:Lasq;

.field private final h:Lalg;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const-class v0, Laso;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lawp;Ljfb;Lalg;Lali;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Lamn;-><init>()V

    .line 44
    new-instance v0, Lass;

    invoke-direct {v0, p0}, Lass;-><init>(Laso;)V

    iput-object v0, p0, Laso;->e:Lass;

    .line 46
    new-instance v0, Lasr;

    invoke-direct {v0, p0}, Lasr;-><init>(Laso;)V

    iput-object v0, p0, Laso;->f:Lasr;

    .line 49
    new-instance v0, Lasq;

    invoke-direct {v0, p0}, Lasq;-><init>(Laso;)V

    iput-object v0, p0, Laso;->g:Lasq;

    .line 58
    const-string v0, "state"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lawp;

    iput-object v0, p0, Laso;->b:Lawp;

    .line 59
    const-string v0, "movieMakerProvider"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljfb;

    iput-object v0, p0, Laso;->c:Ljfb;

    .line 60
    const-string v0, "sendTaskRunner"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lalg;

    iput-object v0, p0, Laso;->h:Lalg;

    .line 61
    const-string v0, "cachedMediaResolver"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lali;

    iput-object v0, p0, Laso;->a:Lali;

    .line 62
    invoke-direct {p0}, Laso;->c()Ljeo;

    move-result-object v0

    iput-object v0, p0, Laso;->d:Ljeo;

    .line 63
    return-void
.end method

.method static synthetic a(Laso;)Lawp;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Laso;->b:Lawp;

    return-object v0
.end method

.method static synthetic a(Laso;Ljava/util/List;)Ljava/util/List;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lbmg;->a:Lbmg;

    invoke-direct {p0, p1, v0}, Laso;->a(Ljava/util/List;Lbmg;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/List;Lbmg;)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lbml;",
            ">;",
            "Lbmg;",
            ")",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;"
        }
    .end annotation

    .prologue
    .line 123
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 124
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbml;

    .line 125
    new-instance v3, Lbmp;

    const-wide/16 v4, 0x0

    invoke-interface {v0}, Lbml;->e()J

    move-result-wide v6

    invoke-direct {v3, v4, v5, v6, v7}, Lbmp;-><init>(JJ)V

    .line 126
    new-instance v4, Lbmf;

    invoke-direct {v4}, Lbmf;-><init>()V

    .line 127
    invoke-virtual {v4, p2}, Lbmf;->a(Lbmg;)Lbmf;

    move-result-object v4

    .line 128
    invoke-interface {v0}, Lbml;->a()Ljeg;

    move-result-object v0

    invoke-virtual {v4, v0}, Lbmf;->a(Ljeg;)Lbmf;

    move-result-object v0

    .line 129
    invoke-virtual {v0, v3}, Lbmf;->a(Lbmp;)Lbmf;

    move-result-object v0

    invoke-virtual {v0}, Lbmf;->a()Lbmd;

    move-result-object v0

    .line 130
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 132
    :cond_0
    return-object v1
.end method

.method static synthetic a(Laso;Ljeo;)Ljeo;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Laso;->d:Ljeo;

    return-object p1
.end method

.method static synthetic a(Laso;I)V
    .locals 5

    .prologue
    .line 30
    iget-object v0, p0, Laso;->h:Lalg;

    new-instance v1, Last;

    invoke-direct {v1, p0}, Last;-><init>(Laso;)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Integer;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-interface {v0, v1, v2}, Lalg;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Laso;Ljes;)V
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Laso;->d:Ljeo;

    iget-object v0, v0, Ljeo;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic a(Ljeo;)V
    .locals 6

    .prologue
    .line 30
    iget-object v0, p0, Ljeo;->b:Ljew;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iput-wide v2, v0, Ljew;->d:J

    return-void
.end method

.method static synthetic b(Laso;Ljava/util/List;)Ljava/util/List;
    .locals 1

    .prologue
    .line 30
    sget-object v0, Lbmg;->c:Lbmg;

    invoke-direct {p0, p1, v0}, Laso;->a(Ljava/util/List;Lbmg;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Laso;)Ljeo;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Laso;->d:Ljeo;

    return-object v0
.end method

.method static synthetic c(Laso;Ljava/util/List;)Ljava/util/List;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 30
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    new-instance v4, Ljeq;

    invoke-direct {v4}, Ljeq;-><init>()V

    iget-object v5, v0, Lbmd;->f:Lbmp;

    iget-wide v6, v5, Lbmp;->b:J

    iput-wide v6, v4, Ljeq;->c:J

    iget-object v5, v0, Lbmd;->f:Lbmp;

    iget-wide v6, v5, Lbmp;->c:J

    iput-wide v6, v4, Ljeq;->d:J

    iget-object v5, p0, Laso;->a:Lali;

    iget-object v6, v0, Lbmd;->e:Ljeg;

    iget-object v6, v6, Ljeg;->b:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Lali;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Ljeq;->b:Ljava/lang/String;

    iput v1, v4, Ljeq;->e:I

    sget-object v5, Lasp;->a:[I

    iget-object v0, v0, Lbmd;->d:Lbmg;

    invoke-virtual {v0}, Lbmg;->ordinal()I

    move-result v0

    aget v0, v5, v0

    packed-switch v0, :pswitch_data_0

    move v0, v1

    :goto_1
    iput v0, v4, Ljeq;->a:I

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_1

    :pswitch_1
    const/4 v0, 0x2

    goto :goto_1

    :cond_0
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private c()Ljeo;
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x0

    .line 87
    new-instance v0, Ljeo;

    invoke-direct {v0}, Ljeo;-><init>()V

    .line 88
    new-instance v1, Ljeu;

    invoke-direct {v1}, Ljeu;-><init>()V

    iget-object v2, p0, Laso;->b:Lawp;

    if-eqz v2, :cond_0

    iget-object v2, p0, Laso;->b:Lawp;

    invoke-interface {v2}, Lawp;->aP()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Ljeu;->b:Ljava/lang/String;

    iget-object v2, p0, Laso;->b:Lawp;

    invoke-interface {v2}, Lawp;->aO()Ljed;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, v2, Ljed;->a:Ljava/lang/String;

    iput-object v2, v1, Ljeu;->a:Ljava/lang/String;

    :cond_0
    iput-object v1, v0, Ljeo;->a:Ljeu;

    .line 89
    new-instance v1, Ljew;

    invoke-direct {v1}, Ljew;-><init>()V

    iput-object v1, v0, Ljeo;->b:Ljew;

    .line 90
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Ljeo;->c:Ljava/util/List;

    .line 91
    iput v6, v0, Ljeo;->d:I

    .line 92
    const/4 v1, 0x1

    iput v1, v0, Ljeo;->e:I

    .line 94
    new-instance v1, Ljew;

    invoke-direct {v1}, Ljew;-><init>()V

    iput-object v1, v0, Ljeo;->b:Ljew;

    .line 95
    iget-object v1, v0, Ljeo;->b:Ljew;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Ljew;->a:Ljava/util/List;

    .line 96
    iget-object v1, v0, Ljeo;->b:Ljew;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Ljew;->b:Ljava/util/List;

    .line 97
    iget-object v1, v0, Ljeo;->b:Ljew;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Ljew;->c:Ljava/util/List;

    .line 99
    iget-object v1, v0, Ljeo;->b:Ljew;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    iput-wide v2, v1, Ljew;->d:J

    .line 100
    iget-object v1, v0, Ljeo;->b:Ljew;

    new-instance v2, Ljey;

    invoke-direct {v2}, Ljey;-><init>()V

    iput-object v2, v1, Ljew;->e:Ljey;

    .line 102
    iget-object v1, v0, Ljeo;->b:Ljew;

    iget-object v1, v1, Ljew;->e:Ljey;

    iput-wide v8, v1, Ljey;->a:J

    .line 103
    iget-object v1, v0, Ljeo;->b:Ljew;

    iget-object v1, v1, Ljew;->e:Ljey;

    iput-wide v8, v1, Ljey;->b:J

    .line 104
    iget-object v1, v0, Ljeo;->b:Ljew;

    iget-object v1, v1, Ljew;->e:Ljey;

    iput v6, v1, Ljey;->c:I

    .line 105
    iget-object v1, v0, Ljeo;->b:Ljew;

    iget-object v1, v1, Ljew;->e:Ljey;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Ljey;->d:Ljava/util/List;

    .line 106
    iget-object v1, v0, Ljeo;->b:Ljew;

    iget-object v1, v1, Ljew;->e:Ljey;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v1, Ljey;->e:Ljava/util/List;

    .line 108
    return-object v0
.end method

.method static synthetic c(Laso;)Ljeo;
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0}, Laso;->c()Ljeo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Laso;)Ljfb;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Laso;->c:Ljfb;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    .line 67
    invoke-super {p0}, Lamn;->a()V

    .line 69
    iget-object v0, p0, Laso;->b:Lawp;

    iget-object v1, p0, Laso;->f:Lasr;

    invoke-interface {v0, v1}, Lawp;->n(Lawk;)V

    .line 70
    iget-object v0, p0, Laso;->b:Lawp;

    iget-object v1, p0, Laso;->e:Lass;

    invoke-interface {v0, v1}, Lawp;->e(Lawk;)V

    .line 71
    iget-object v0, p0, Laso;->b:Lawp;

    iget-object v1, p0, Laso;->g:Lasq;

    invoke-interface {v0, v1}, Lawp;->B(Lawk;)V

    .line 72
    return-void
.end method

.method public b()V
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Laso;->b:Lawp;

    iget-object v1, p0, Laso;->g:Lasq;

    invoke-interface {v0, v1}, Lawp;->C(Lawk;)V

    .line 77
    iget-object v0, p0, Laso;->b:Lawp;

    iget-object v1, p0, Laso;->e:Lass;

    invoke-interface {v0, v1}, Lawp;->f(Lawk;)V

    .line 78
    iget-object v0, p0, Laso;->b:Lawp;

    iget-object v1, p0, Laso;->f:Lasr;

    invoke-interface {v0, v1}, Lawp;->o(Lawk;)V

    .line 80
    invoke-super {p0}, Lamn;->b()V

    .line 81
    return-void
.end method
