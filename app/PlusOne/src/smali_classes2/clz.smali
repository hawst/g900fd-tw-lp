.class public final Lclz;
.super Lcjy;
.source "PG"


# static fields
.field private static final W:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final X:[I


# instance fields
.field private final Y:Lckf;

.field private final Z:Lcmc;

.field private aa:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 23
    const/16 v0, 0x18

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lclz;->X:[I

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 40
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Integer;

    .line 42
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x1

    .line 43
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/16 v2, 0x9

    .line 44
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x3

    const/4 v3, 0x6

    .line 45
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 40
    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 47
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lclz;->W:Ljava/util/List;

    .line 48
    return-void

    .line 23
    :array_0
    .array-data 4
        0x7f02023b
        0x7f02023a
        0x7f02023d
        0x7f02023c
        0x7f02023f
        0x7f02023e
        0x7f020241
        0x7f020240
        0x7f020243
        0x7f020242
        0x7f020245
        0x7f020244
        0x7f020247
        0x7f020246
        0x7f020249
        0x7f020248
        0x7f02024b
        0x7f02024a
        0x7f020235
        0x7f020234
        0x7f020237
        0x7f020236
        0x7f020239
        0x7f020238
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 20
    invoke-direct {p0}, Lcjy;-><init>()V

    .line 50
    new-instance v0, Lckf;

    const/16 v1, 0x320

    sget-object v2, Lclz;->X:[I

    invoke-direct {v0, p0, v1, v2}, Lckf;-><init>(Lcjy;I[I)V

    iput-object v0, p0, Lclz;->Y:Lckf;

    .line 52
    new-instance v0, Lcmc;

    invoke-direct {v0, p0}, Lcmc;-><init>(Lclz;)V

    iput-object v0, p0, Lclz;->Z:Lcmc;

    .line 146
    return-void
.end method

.method static synthetic a(Lclz;)Lckf;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lclz;->Y:Lckf;

    return-object v0
.end method

.method static synthetic b(Lclz;)Lcmc;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lclz;->Z:Lcmc;

    return-object v0
.end method

.method static synthetic c(Lclz;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lclz;->aa:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    return-object v0
.end method


# virtual methods
.method protected X()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 69
    sget-object v0, Lclz;->W:Ljava/util/List;

    return-object v0
.end method

.method public a(ILjava/lang/Object;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 116
    const/16 v0, 0x320

    if-ne p1, v0, :cond_0

    .line 117
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 120
    const-string v1, "%s %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-virtual {p0, v4}, Lclz;->f(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 123
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, p2}, Lclz;->b(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Lckp;)V
    .locals 7

    .prologue
    const/16 v6, 0x13

    const/4 v3, 0x0

    .line 74
    invoke-virtual {p0}, Lclz;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    const/16 v1, 0x320

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v0

    .line 75
    iget-object v1, p0, Lclz;->Y:Lckf;

    invoke-virtual {v1, v0}, Lckf;->a(I)V

    .line 77
    const v0, 0x7f0203a3

    const v1, 0x7f0a009b

    .line 79
    invoke-virtual {p0, v1}, Lclz;->e_(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcma;

    invoke-direct {v2, p0}, Lcma;-><init>(Lclz;)V

    .line 77
    invoke-virtual {p1, v0, v1, v2}, Lckp;->a(ILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 89
    invoke-virtual {p0}, Lclz;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 90
    const v1, 0x7f0203a1

    const v2, 0x7f0203a0

    const v4, 0x7f0a0089

    .line 94
    invoke-virtual {p0, v4}, Lclz;->e_(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcmb;

    invoke-direct {v5, p0, v0}, Lcmb;-><init>(Lclz;I)V

    move-object v0, p1

    .line 90
    invoke-virtual/range {v0 .. v5}, Lckp;->a(IIILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    move-result-object v0

    iput-object v0, p0, Lclz;->aa:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 110
    iget-object v0, p0, Lclz;->aa:Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 111
    invoke-virtual {p0}, Lclz;->aj()Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/google/android/libraries/photoeditor/filterparameters/FilterParameter;->getParameterInteger(I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v3, 0x1

    .line 110
    :cond_0
    invoke-virtual {v0, v3}, Lcom/google/android/libraries/snapseed/ui/views/ToolButton;->setSelected(Z)V

    .line 112
    return-void
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 59
    new-instance v0, Lhmk;

    sget-object v1, Lonc;->z:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method public b(ILjava/lang/Object;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 128
    move-object v0, p2

    check-cast v0, Ljava/lang/Integer;

    .line 129
    sparse-switch p1, :sswitch_data_0

    .line 142
    invoke-super {p0, p1, p2}, Lcjy;->b(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 131
    :sswitch_0
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lclz;->g(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 134
    :sswitch_1
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, "0"

    goto :goto_0

    :cond_0
    const-string v1, "%s %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    .line 135
    invoke-virtual {p0, p1}, Lclz;->f(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 138
    :sswitch_2
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f0a007d

    .line 139
    invoke-virtual {p0, v0}, Lclz;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const v0, 0x7f0a007c

    invoke-virtual {p0, v0}, Lclz;->e_(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 129
    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_1
        0x13 -> :sswitch_2
        0x320 -> :sswitch_0
    .end sparse-switch
.end method

.method public c()I
    .locals 1

    .prologue
    .line 64
    const/16 v0, 0xca

    return v0
.end method
