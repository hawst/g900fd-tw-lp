.class public final Lcbk;
.super Landroid/widget/FrameLayout;
.source "PG"

# interfaces
.implements Lcdc;


# static fields
.field private static final a:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Lcbk;",
            "Ljava/lang/Float;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final A:Ljava/util/Random;

.field private B:Lcbr;

.field private final b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/view/View$OnClickListener;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/ImageView;

.field private final g:Landroid/view/ViewGroup;

.field private final h:Landroid/view/View;

.field private final i:Landroid/view/View;

.field private final j:Landroid/view/View;

.field private final k:Landroid/view/View;

.field private final l:Landroid/view/View;

.field private final m:Landroid/view/View;

.field private final n:Landroid/graphics/RectF;

.field private final o:Landroid/graphics/RectF;

.field private final p:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcdd;",
            ">;"
        }
    .end annotation
.end field

.field private q:I

.field private r:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lboh;",
            ">;"
        }
    .end annotation
.end field

.field private s:Lboh;

.field private t:Z

.field private u:Z

.field private v:I

.field private w:Lcbs;

.field private x:Landroid/animation/ObjectAnimator;

.field private final y:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<[F>;"
        }
    .end annotation
.end field

.field private final z:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Lcbq;

    invoke-direct {v0}, Lcbq;-><init>()V

    sput-object v0, Lcbk;->a:Landroid/util/Property;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 137
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcbk;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 138
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 6

    .prologue
    const v3, 0x7f0d005d

    const/4 v1, 0x0

    .line 141
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 145
    invoke-virtual {p0}, Lcbk;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    .line 146
    invoke-virtual {p0}, Lcbk;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 147
    invoke-virtual {p0}, Lcbk;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 146
    invoke-virtual {p0, v0, v1, v2, v1}, Lcbk;->setPadding(IIII)V

    .line 151
    :cond_0
    const v0, 0x7f0400bc

    invoke-static {p1, v0, p0}, Lcbk;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 152
    const v0, 0x7f1002da

    invoke-direct {p0, v0}, Lcbk;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    iput-object v0, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    .line 153
    const v0, 0x7f1002d9

    invoke-direct {p0, v0}, Lcbk;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcbk;->c:Landroid/widget/TextView;

    .line 154
    const v0, 0x7f1002e2

    invoke-direct {p0, v0}, Lcbk;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcbk;->f:Landroid/widget/ImageView;

    .line 155
    iget-object v0, p0, Lcbk;->f:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 156
    const v0, 0x7f1002db

    invoke-direct {p0, v0}, Lcbk;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcbk;->k:Landroid/view/View;

    .line 157
    const v0, 0x7f1002de

    invoke-direct {p0, v0}, Lcbk;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcbk;->m:Landroid/view/View;

    .line 158
    const v0, 0x7f1002df

    invoke-direct {p0, v0}, Lcbk;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcbk;->l:Landroid/view/View;

    .line 161
    new-instance v0, Lcbl;

    invoke-direct {v0, p0}, Lcbl;-><init>(Lcbk;)V

    iput-object v0, p0, Lcbk;->d:Landroid/view/View$OnClickListener;

    .line 177
    const v0, 0x7f1002e1

    invoke-direct {p0, v0}, Lcbk;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcbk;->e:Landroid/widget/TextView;

    .line 178
    const v0, 0x7f1002d8

    invoke-direct {p0, v0}, Lcbk;->b(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcbk;->g:Landroid/view/ViewGroup;

    .line 179
    const v0, 0x7f1002e4

    invoke-virtual {p0, v0}, Lcbk;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcbk;->h:Landroid/view/View;

    .line 180
    const v0, 0x7f1002e3

    invoke-direct {p0, v0}, Lcbk;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcbk;->i:Landroid/view/View;

    .line 181
    const v0, 0x7f1002e0

    invoke-direct {p0, v0}, Lcbk;->b(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcbk;->j:Landroid/view/View;

    .line 184
    iget-object v0, p0, Lcbk;->j:Landroid/view/View;

    new-instance v2, Lcbm;

    invoke-direct {v2, p0}, Lcbm;-><init>(Lcbk;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 192
    iget-object v0, p0, Lcbk;->c:Landroid/widget/TextView;

    new-instance v2, Lcbn;

    invoke-direct {v2, p0}, Lcbn;-><init>(Lcbk;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 201
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/high16 v2, 0x7f0f0000

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->obtainTypedArray(I)Landroid/content/res/TypedArray;

    move-result-object v2

    .line 202
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcbk;->y:Ljava/util/List;

    .line 203
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 205
    :goto_0
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 206
    const/4 v4, 0x3

    new-array v4, v4, [F

    .line 207
    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v5

    invoke-static {v5, v4}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 208
    iget-object v5, p0, Lcbk;->y:Ljava/util/List;

    invoke-interface {v5, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 209
    invoke-virtual {v2, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 211
    :cond_1
    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    .line 213
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcbk;->z:Ljava/util/List;

    .line 215
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcbk;->A:Ljava/util/Random;

    .line 216
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcbk;->n:Landroid/graphics/RectF;

    .line 217
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcbk;->o:Landroid/graphics/RectF;

    .line 218
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcbk;->p:Ljava/util/List;

    .line 221
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcbk;->a(I)V

    .line 222
    invoke-virtual {p0, v1}, Lcbk;->b(Z)V

    .line 223
    return-void
.end method

.method static synthetic a(Lcbk;)Lcbs;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcbk;->w:Lcbs;

    return-object v0
.end method

.method private a(FFFF)V
    .locals 1

    .prologue
    .line 539
    iget-object v0, p0, Lcbk;->n:Landroid/graphics/RectF;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 540
    invoke-direct {p0}, Lcbk;->e()V

    .line 541
    return-void
.end method

.method private a(I)V
    .locals 7

    .prologue
    const/16 v1, 0x8

    const/4 v6, 0x4

    const/4 v2, 0x0

    const/4 v5, 0x2

    .line 569
    iget v0, p0, Lcbk;->q:I

    if-ne p1, v0, :cond_1

    .line 605
    :cond_0
    :goto_0
    return-void

    .line 574
    :cond_1
    iget-object v0, p0, Lcbk;->x:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_2

    .line 575
    iget-object v0, p0, Lcbk;->x:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 576
    const/4 v0, 0x0

    iput-object v0, p0, Lcbk;->x:Landroid/animation/ObjectAnimator;

    .line 579
    :cond_2
    iget v3, p0, Lcbk;->q:I

    .line 580
    iput p1, p0, Lcbk;->q:I

    .line 583
    iget-object v4, p0, Lcbk;->g:Landroid/view/ViewGroup;

    if-ne p1, v6, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {v4, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 584
    iget-object v4, p0, Lcbk;->e:Landroid/widget/TextView;

    if-ne p1, v5, :cond_5

    move v0, v1

    :goto_2
    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 585
    if-ne p1, v5, :cond_6

    const v0, 0x7f02050c

    .line 588
    :goto_3
    iget-object v4, p0, Lcbk;->f:Landroid/widget/ImageView;

    invoke-virtual {v4, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 589
    iget-object v0, p0, Lcbk;->j:Landroid/view/View;

    if-ne p1, v5, :cond_7

    :goto_4
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 591
    if-ne v3, v6, :cond_3

    .line 594
    invoke-direct {p0}, Lcbk;->c()V

    .line 597
    :cond_3
    if-ne p1, v6, :cond_8

    .line 599
    sget-object v0, Lcbk;->a:Landroid/util/Property;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/util/Property;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 600
    invoke-direct {p0}, Lcbk;->d()V

    goto :goto_0

    :cond_4
    move v0, v2

    .line 583
    goto :goto_1

    :cond_5
    move v0, v2

    .line 584
    goto :goto_2

    .line 585
    :cond_6
    const v0, 0x7f02050f

    goto :goto_3

    :cond_7
    move v1, v2

    .line 589
    goto :goto_4

    .line 601
    :cond_8
    if-ne p1, v5, :cond_0

    .line 603
    sget-object v0, Lcbk;->a:Landroid/util/Property;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/util/Property;->set(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method static synthetic a(Lcbk;FFFF)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3, p4}, Lcbk;->a(FFFF)V

    return-void
.end method

.method static synthetic a(Lcbk;I)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcbk;->a(I)V

    return-void
.end method

.method private b(I)Landroid/view/View;
    .locals 4

    .prologue
    .line 666
    invoke-virtual {p0, p1}, Lcbk;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 667
    invoke-virtual {p0}, Lcbk;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getResourceName(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x13

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "View "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " / "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 666
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    return-object v0
.end method

.method static synthetic b(Lcbk;)Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    return-object v0
.end method

.method static synthetic c(Lcbk;)Lcbr;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcbk;->B:Lcbr;

    return-object v0
.end method

.method private c()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    const/16 v10, 0x8

    const/4 v4, 0x0

    .line 466
    iget-object v0, p0, Lcbk;->r:Ljava/util/List;

    if-nez v0, :cond_0

    .line 467
    invoke-direct {p0}, Lcbk;->d()V

    .line 517
    :goto_0
    return-void

    .line 471
    :cond_0
    iget-object v0, p0, Lcbk;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0, v10}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 474
    iget-object v0, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->b()I

    move-result v0

    add-int/2addr v0, v5

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->b()I

    move-result v1

    div-int v2, v0, v1

    .line 476
    iget-object v0, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c()I

    move-result v0

    add-int/2addr v0, v5

    add-int/lit8 v0, v0, -0x1

    iget-object v1, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-virtual {v1}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c()I

    move-result v1

    div-int v3, v0, v1

    .line 477
    iget-object v0, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-virtual {v0, v5}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c(I)V

    .line 478
    iget-object v0, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->b()I

    move-result v0

    mul-int/2addr v0, v2

    move v1, v0

    .line 480
    :goto_1
    iget-object v0, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->getChildCount()I

    move-result v0

    if-ge v0, v1, :cond_3

    .line 481
    iget-object v0, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 482
    iget-object v0, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->b(I)V

    .line 486
    :goto_2
    iput v1, p0, Lcbk;->v:I

    .line 488
    :goto_3
    iget-object v0, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->getChildCount()I

    move-result v0

    if-ge v0, v1, :cond_3

    .line 490
    invoke-virtual {p0}, Lcbk;->getContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f040226

    .line 489
    invoke-static {v0, v2, v11}, Lcbk;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;

    .line 493
    iget-object v2, p0, Lcbk;->d:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 494
    iget-object v2, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-virtual {v2, v0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->addView(Landroid/view/View;)V

    goto :goto_3

    .line 478
    :cond_1
    iget-object v0, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    .line 479
    invoke-virtual {v0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c()I

    move-result v0

    mul-int/2addr v0, v3

    move v1, v0

    goto :goto_1

    .line 484
    :cond_2
    iget-object v0, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a(I)V

    goto :goto_2

    .line 498
    :cond_3
    iget-object v0, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v4}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a(IZ)V

    move v3, v4

    .line 499
    :goto_4
    iget v0, p0, Lcbk;->v:I

    if-ge v3, v0, :cond_5

    .line 500
    iget-object v0, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-virtual {v0, v3}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;

    .line 501
    if-ge v3, v5, :cond_4

    .line 502
    iget-object v1, p0, Lcbk;->r:Ljava/util/List;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lboh;

    .line 503
    invoke-virtual {v0, v4}, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->setVisibility(I)V

    .line 507
    invoke-virtual {v1}, Lboh;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->a(Ljava/lang/String;)V

    .line 508
    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->setTag(Ljava/lang/Object;)V

    .line 509
    iget-object v2, p0, Lcbk;->y:Ljava/util/List;

    invoke-virtual {v1}, Lboh;->d()J

    move-result-wide v6

    iget-object v8, p0, Lcbk;->y:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v8

    int-to-long v8, v8

    rem-long/2addr v6, v8

    long-to-int v6, v6

    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [F

    iget-object v6, p0, Lcbk;->A:Ljava/util/Random;

    invoke-virtual {v1}, Lboh;->c()J

    move-result-wide v8

    invoke-virtual {v6, v8, v9}, Ljava/util/Random;->setSeed(J)V

    const/4 v1, 0x2

    const v6, 0x3f0ccccd    # 0.55f

    iget-object v7, p0, Lcbk;->A:Ljava/util/Random;

    const/16 v8, 0x1e

    invoke-virtual {v7, v8}, Ljava/util/Random;->nextInt(I)I

    move-result v7

    int-to-float v7, v7

    const v8, 0x3c23d70a    # 0.01f

    mul-float/2addr v7, v8

    add-float/2addr v6, v7

    aput v6, v2, v1

    invoke-static {v2}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->a(I)V

    .line 499
    :goto_5
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_4

    .line 511
    :cond_4
    invoke-virtual {v0, v10}, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->setVisibility(I)V

    .line 512
    invoke-virtual {v0, v11}, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->setTag(Ljava/lang/Object;)V

    goto :goto_5

    .line 516
    :cond_5
    iget-object v0, p0, Lcbk;->s:Lboh;

    iget-boolean v1, p0, Lcbk;->t:Z

    iget-boolean v2, p0, Lcbk;->u:Z

    invoke-virtual {p0, v0, v4, v1, v2}, Lcbk;->a(Lboh;ZZZ)V

    goto/16 :goto_0
.end method

.method static synthetic d(Lcbk;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcbk;->e:Landroid/widget/TextView;

    return-object v0
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 526
    iget-object v1, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->c(I)V

    move v1, v0

    .line 527
    :goto_0
    iget v0, p0, Lcbk;->v:I

    if-ge v1, v0, :cond_0

    .line 528
    iget-object v0, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;

    .line 529
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->setVisibility(I)V

    .line 530
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->setTag(Ljava/lang/Object;)V

    .line 527
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 532
    :cond_0
    return-void
.end method

.method static synthetic e(Lcbk;)Landroid/view/View;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcbk;->j:Landroid/view/View;

    return-object v0
.end method

.method private e()V
    .locals 6

    .prologue
    .line 549
    invoke-virtual {p0}, Lcbk;->invalidate()V

    .line 550
    iget-object v0, p0, Lcbk;->o:Landroid/graphics/RectF;

    iget-object v1, p0, Lcbk;->n:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    neg-float v1, v1

    iget-object v2, p0, Lcbk;->n:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    neg-float v2, v2

    iget-object v3, p0, Lcbk;->n:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    .line 553
    invoke-virtual {p0}, Lcbk;->getWidth()I

    move-result v4

    int-to-float v4, v4

    sub-float/2addr v3, v4

    iget-object v4, p0, Lcbk;->n:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    .line 554
    invoke-virtual {p0}, Lcbk;->getHeight()I

    move-result v5

    int-to-float v5, v5

    sub-float/2addr v4, v5

    .line 550
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/RectF;->set(FFFF)V

    .line 555
    const/4 v0, 0x0

    iget-object v1, p0, Lcbk;->p:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 556
    iget-object v0, p0, Lcbk;->p:Ljava/util/List;

    .line 557
    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcdd;

    .line 558
    invoke-interface {v0}, Lcdd;->a()V

    .line 555
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 560
    :cond_0
    return-void
.end method

.method static synthetic f(Lcbk;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcbk;->c:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 342
    iget v0, p0, Lcbk;->q:I

    if-eq v0, v2, :cond_1

    .line 343
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcbk;->a(I)V

    .line 344
    iget-object v0, p0, Lcbk;->x:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcbk;->x:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 347
    :cond_0
    invoke-static {}, Lcad;->a()Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcbk;->x:Landroid/animation/ObjectAnimator;

    .line 348
    iget-object v0, p0, Lcbk;->x:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 349
    iget-object v0, p0, Lcbk;->x:Landroid/animation/ObjectAnimator;

    sget-object v1, Lcbk;->a:Landroid/util/Property;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setProperty(Landroid/util/Property;)V

    .line 350
    iget-object v0, p0, Lcbk;->x:Landroid/animation/ObjectAnimator;

    new-array v1, v2, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 351
    iget-object v0, p0, Lcbk;->x:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 352
    iget-object v0, p0, Lcbk;->x:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcbo;

    invoke-direct {v1, p0}, Lcbo;-><init>(Lcbk;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 360
    :cond_1
    return-void

    .line 350
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public a(J)V
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    const/16 v8, 0xff

    const/4 v7, 0x0

    .line 391
    cmp-long v0, p1, v2

    if-gez v0, :cond_2

    const-wide/16 v0, 0x2

    rem-long v0, p1, v0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcbk;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0055

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 392
    :goto_0
    new-instance v1, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v1}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    .line 393
    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x10100a7

    aput v3, v2, v7

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    .line 394
    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1e

    invoke-static {v4, v7, v8}, Lcfn;->a(III)I

    move-result v4

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v5

    add-int/lit8 v5, v5, 0x1e

    invoke-static {v5, v7, v8}, Lcfn;->a(III)I

    move-result v5

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v6

    add-int/lit8 v6, v6, 0x1e

    invoke-static {v6, v7, v8}, Lcfn;->a(III)I

    move-result v6

    invoke-static {v4, v5, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    .line 393
    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 395
    new-array v2, v7, [I

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v3, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    .line 397
    iget-object v0, p0, Lcbk;->j:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 398
    invoke-virtual {p0}, Lcbk;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    .line 399
    iget-object v0, p0, Lcbk;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 401
    :cond_0
    return-void

    .line 391
    :cond_1
    invoke-virtual {p0}, Lcbk;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0056

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcbk;->z:Ljava/util/List;

    iget-object v1, p0, Lcbk;->y:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    int-to-long v2, v1

    rem-long v2, p1, v2

    long-to-int v1, v2

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public a(Landroid/graphics/RectF;)V
    .locals 1

    .prologue
    .line 442
    iget-object v0, p0, Lcbk;->o:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 443
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 285
    iget-object v0, p0, Lcbk;->l:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 286
    return-void
.end method

.method public a(Lboh;ZZZ)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v3, -0x1

    .line 246
    iput-object p1, p0, Lcbk;->s:Lboh;

    .line 247
    iput-boolean p3, p0, Lcbk;->t:Z

    .line 248
    iput-boolean p4, p0, Lcbk;->u:Z

    .line 252
    if-eqz p1, :cond_2

    move v1, v2

    .line 253
    :goto_0
    iget v0, p0, Lcbk;->v:I

    if-ge v1, v0, :cond_2

    .line 254
    iget-object v0, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lboh;

    .line 255
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lboh;->c()J

    move-result-wide v4

    invoke-virtual {p1}, Lboh;->c()J

    move-result-wide v6

    cmp-long v0, v4, v6

    if-nez v0, :cond_0

    .line 256
    iget-object v0, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;

    invoke-virtual {v0, p4}, Lcom/google/android/apps/moviemaker/ui/SoundtrackItemView;->a(Z)V

    .line 262
    :goto_1
    iget-object v0, p0, Lcbk;->k:Landroid/view/View;

    if-eqz p3, :cond_1

    if-eq v1, v3, :cond_1

    :goto_2
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 264
    iget-object v0, p0, Lcbk;->b:Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/apps/moviemaker/ui/RigidGridLayout;->a(IZ)V

    .line 265
    return-void

    .line 253
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 262
    :cond_1
    const/16 v2, 0x8

    goto :goto_2

    :cond_2
    move v1, v3

    goto :goto_1
.end method

.method public a(Lcbr;)V
    .locals 2

    .prologue
    .line 408
    iput-object p1, p0, Lcbk;->B:Lcbr;

    .line 409
    iget-object v1, p0, Lcbk;->c:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setClickable(Z)V

    .line 410
    return-void

    .line 409
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcbs;)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lcbk;->w:Lcbs;

    .line 319
    return-void
.end method

.method public a(Lcdd;)V
    .locals 2

    .prologue
    .line 447
    const-string v0, "listener"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 448
    iget-object v0, p0, Lcbk;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Already added"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 449
    iget-object v0, p0, Lcbk;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 450
    return-void

    .line 448
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcbk;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 230
    iget-object v0, p0, Lcbk;->e:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    return-void
.end method

.method public a(Ljava/util/List;Lboh;ZZ)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lboh;",
            ">;",
            "Lboh;",
            "ZZ)V"
        }
    .end annotation

    .prologue
    .line 275
    const-string v0, "soundtracks"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    iput-object v0, p0, Lcbk;->r:Ljava/util/List;

    .line 276
    iput-object p2, p0, Lcbk;->s:Lboh;

    .line 277
    iput-boolean p3, p0, Lcbk;->t:Z

    .line 278
    iput-boolean p4, p0, Lcbk;->u:Z

    .line 279
    iget v0, p0, Lcbk;->q:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 280
    invoke-direct {p0}, Lcbk;->c()V

    .line 282
    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Lcbk;->h:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcbk;->h:Landroid/view/View;

    const/16 v1, 0x1f4

    invoke-static {v0, p1, v1}, Lcad;->a(Landroid/view/View;ZI)V

    .line 302
    :cond_0
    return-void
.end method

.method public b()V
    .locals 4

    .prologue
    .line 367
    iget v0, p0, Lcbk;->q:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    .line 368
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcbk;->a(I)V

    .line 369
    iget-object v0, p0, Lcbk;->x:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 370
    iget-object v0, p0, Lcbk;->x:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 372
    :cond_0
    invoke-static {}, Lcad;->a()Landroid/animation/ObjectAnimator;

    move-result-object v0

    iput-object v0, p0, Lcbk;->x:Landroid/animation/ObjectAnimator;

    .line 373
    iget-object v0, p0, Lcbk;->x:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0, p0}, Landroid/animation/ObjectAnimator;->setTarget(Ljava/lang/Object;)V

    .line 374
    iget-object v0, p0, Lcbk;->x:Landroid/animation/ObjectAnimator;

    sget-object v1, Lcbk;->a:Landroid/util/Property;

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setProperty(Landroid/util/Property;)V

    .line 375
    iget-object v0, p0, Lcbk;->x:Landroid/animation/ObjectAnimator;

    const/4 v1, 0x2

    new-array v1, v1, [F

    fill-array-data v1, :array_0

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->setFloatValues([F)V

    .line 376
    iget-object v0, p0, Lcbk;->x:Landroid/animation/ObjectAnimator;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->start()V

    .line 377
    iget-object v0, p0, Lcbk;->x:Landroid/animation/ObjectAnimator;

    new-instance v1, Lcbp;

    invoke-direct {v1, p0}, Lcbp;-><init>(Lcbk;)V

    invoke-virtual {v0, v1}, Landroid/animation/ObjectAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 385
    :cond_1
    return-void

    .line 375
    nop

    :array_0
    .array-data 4
        0x3f800000    # 1.0f
        0x0
    .end array-data
.end method

.method public b(Landroid/view/View$OnClickListener;)V
    .locals 1

    .prologue
    .line 289
    iget-object v0, p0, Lcbk;->m:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 290
    return-void
.end method

.method public b(Lcdd;)V
    .locals 2

    .prologue
    .line 454
    const-string v0, "listener"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 455
    iget-object v0, p0, Lcbk;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Not added"

    invoke-static {v0, v1}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 456
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 311
    iget-object v0, p0, Lcbk;->i:Landroid/view/View;

    const/16 v1, 0x1f4

    invoke-static {v0, p1, v1}, Lcad;->a(Landroid/view/View;ZI)V

    .line 312
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 327
    if-eqz p1, :cond_0

    const/4 v0, 0x4

    :goto_0
    invoke-direct {p0, v0}, Lcbk;->a(I)V

    .line 328
    return-void

    .line 327
    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public d(Z)V
    .locals 2

    .prologue
    .line 331
    iget-object v1, p0, Lcbk;->f:Landroid/widget/ImageView;

    if-eqz p1, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 332
    if-eqz p1, :cond_0

    .line 333
    iget-object v0, p0, Lcbk;->c:Landroid/widget/TextView;

    const v1, 0x7f0b0046

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 335
    :cond_0
    return-void

    .line 331
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 428
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 429
    iget-object v0, p0, Lcbk;->n:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->left:F

    iget-object v1, p0, Lcbk;->n:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->top:F

    iget-object v2, p0, Lcbk;->n:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    iget-object v3, p0, Lcbk;->n:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    .line 434
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 435
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 436
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 416
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 417
    iget-object v0, p0, Lcbk;->j:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    iget-object v1, p0, Lcbk;->j:Landroid/view/View;

    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->topMargin:I

    invoke-virtual {p0}, Lcbk;->getWidth()I

    move-result v4

    iget v5, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    sub-int/2addr v4, v5

    invoke-virtual {p0}, Lcbk;->getHeight()I

    move-result v5

    iget v0, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    sub-int v0, v5, v0

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/view/View;->layout(IIII)V

    .line 419
    iget v0, p0, Lcbk;->q:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcbk;->q:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 420
    :cond_0
    invoke-virtual {p0}, Lcbk;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p0}, Lcbk;->getHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {p0, v6, v6, v0, v1}, Lcbk;->a(FFFF)V

    .line 424
    :goto_0
    return-void

    .line 422
    :cond_1
    invoke-direct {p0}, Lcbk;->e()V

    goto :goto_0
.end method
