.class public final Lckk;
.super Lcjy;
.source "PG"

# interfaces
.implements Lhbm;


# static fields
.field private static final W:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final X:[I

.field private static final Y:[I


# instance fields
.field private final Z:Lckf;

.field private final aa:Lcmv;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 27
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lckk;->X:[I

    .line 34
    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lckk;->Y:[I

    .line 42
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 43
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Integer;

    const/16 v2, 0xc

    .line 45
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    const/4 v2, 0x1

    .line 46
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 47
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    .line 43
    invoke-static {v0, v1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 49
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lckk;->W:Ljava/util/List;

    .line 50
    return-void

    .line 27
    :array_0
    .array-data 4
        0x7f0a0074
        0x7f0a0075
        0x7f0a0076
        0x7f0a0077
    .end array-data

    .line 34
    :array_1
    .array-data 4
        0x7f020210
        0x7f02020f
        0x7f020213
        0x7f020212
        0x7f0201d6
        0x7f0201d5
        0x7f02022f
        0x7f02022e
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 23
    invoke-direct {p0}, Lcjy;-><init>()V

    .line 52
    new-instance v0, Lckf;

    const/4 v1, 0x3

    sget-object v2, Lckk;->Y:[I

    invoke-direct {v0, p0, v1, v2}, Lckf;-><init>(Lcjy;I[I)V

    iput-object v0, p0, Lckk;->Z:Lckf;

    .line 54
    new-instance v0, Lcko;

    invoke-direct {v0, p0}, Lcko;-><init>(Lckk;)V

    iput-object v0, p0, Lckk;->aa:Lcmv;

    .line 125
    return-void
.end method

.method static synthetic a(Lckk;)Lckf;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lckk;->Z:Lckf;

    return-object v0
.end method

.method static synthetic b(Lckk;)Lcmv;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lckk;->aa:Lcmv;

    return-object v0
.end method


# virtual methods
.method protected V()V
    .locals 2

    .prologue
    .line 77
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a:Lcom/google/android/libraries/photoeditor/core/NativeCore;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a(Lhbm;)V

    .line 78
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lckk;->k(Z)V

    .line 79
    invoke-super {p0}, Lcjy;->V()V

    .line 80
    return-void
.end method

.method protected X()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 89
    sget-object v0, Lckk;->W:Ljava/util/List;

    return-object v0
.end method

.method public Y()V
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lckk;->a(Landroid/graphics/Bitmap;)V

    .line 159
    return-void
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 166
    invoke-virtual {p0}, Lckk;->x()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lckn;

    invoke-direct {v1, p0}, Lckn;-><init>(Lckk;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 174
    return-void
.end method

.method protected a(Lckp;)V
    .locals 3

    .prologue
    .line 94
    const v0, 0x7f0203a3

    const v1, 0x7f0a009b

    .line 96
    invoke-virtual {p0, v1}, Lckk;->e_(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lckl;

    invoke-direct {v2, p0}, Lckl;-><init>(Lckk;)V

    .line 94
    invoke-virtual {p1, v0, v1, v2}, Lckp;->a(ILjava/lang/String;Landroid/view/View$OnClickListener;)Lcom/google/android/libraries/snapseed/ui/views/ToolButton;

    .line 106
    return-void
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 64
    invoke-super {p0}, Lcjy;->aO_()V

    .line 65
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a:Lcom/google/android/libraries/photoeditor/core/NativeCore;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a(Lhbm;)V

    .line 66
    return-void
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 59
    new-instance v0, Lhmk;

    sget-object v1, Lonc;->p:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method public b(ILjava/lang/Object;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 110
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    .line 111
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_0

    sget-object v1, Lckk;->X:[I

    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    sget-object v1, Lckk;->X:[I

    aget v0, v1, v0

    invoke-virtual {p0, v0}, Lckk;->e_(I)Ljava/lang/String;

    move-result-object v0

    .line 113
    :goto_0
    return-object v0

    .line 111
    :cond_0
    const-string v0, "*UNKNOWN*"

    goto :goto_0

    .line 113
    :cond_1
    invoke-super {p0, p1, p2}, Lcjy;->b(ILjava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 84
    const/16 v0, 0x64

    return v0
.end method

.method public x_()V
    .locals 2

    .prologue
    .line 144
    invoke-virtual {p0}, Lckk;->x()Landroid/view/View;

    move-result-object v0

    new-instance v1, Lckm;

    invoke-direct {v1, p0}, Lckm;-><init>(Lckk;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 152
    return-void
.end method

.method public z()V
    .locals 2

    .prologue
    .line 70
    sget-object v0, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a:Lcom/google/android/libraries/photoeditor/core/NativeCore;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/photoeditor/core/NativeCore;->a(Lhbm;)V

    .line 71
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lckk;->k(Z)V

    .line 72
    invoke-super {p0}, Lcjy;->z()V

    .line 73
    return-void
.end method
