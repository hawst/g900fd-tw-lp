.class final Lbio;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public final a:J

.field public final b:Lbil;

.field public final c:Landroid/media/MediaFormat;


# direct methods
.method constructor <init>(JLbil;Landroid/media/MediaFormat;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    const-string v0, "timeUnitsPerSecond"

    invoke-static {p1, p2, v0}, Lcec;->a(JLjava/lang/CharSequence;)J

    move-result-wide v0

    iput-wide v0, p0, Lbio;->a:J

    .line 55
    const-string v0, "stblAtom"

    invoke-static {p3, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbil;

    iput-object v0, p0, Lbio;->b:Lbil;

    .line 56
    const-string v0, "mediaFormat"

    invoke-static {p4, v0, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaFormat;

    iput-object v0, p0, Lbio;->c:Landroid/media/MediaFormat;

    .line 57
    return-void
.end method

.method static a(Lbir;)Landroid/media/MediaFormat;
    .locals 15

    .prologue
    const/4 v3, 0x0

    const/4 v14, 0x4

    const/4 v1, 0x0

    .line 84
    invoke-virtual {p0, v14}, Lbir;->a(I)V

    .line 85
    invoke-virtual {p0}, Lbir;->f()I

    move-result v5

    move v4, v1

    move-object v0, v3

    .line 87
    :goto_0
    if-ge v4, v5, :cond_8

    .line 88
    invoke-virtual {p0}, Lbir;->c()I

    move-result v6

    .line 89
    invoke-virtual {p0}, Lbir;->f()I

    move-result v2

    const-string v7, "childAtomSize"

    invoke-static {v2, v7}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v7

    .line 90
    invoke-virtual {p0}, Lbir;->f()I

    move-result v2

    .line 91
    sget v8, Lbik;->l:I

    if-ne v2, v8, :cond_6

    .line 92
    add-int/lit8 v0, v6, 0x8

    invoke-virtual {p0, v0}, Lbir;->a(I)V

    const/16 v0, 0x18

    invoke-virtual {p0, v0}, Lbir;->b(I)V

    invoke-virtual {p0}, Lbir;->e()I

    move-result v8

    invoke-virtual {p0}, Lbir;->e()I

    move-result v9

    const/16 v0, 0x32

    invoke-virtual {p0, v0}, Lbir;->b(I)V

    invoke-virtual {p0}, Lbir;->c()I

    move-result v0

    move v2, v0

    move-object v0, v3

    :goto_1
    sub-int v10, v2, v6

    if-ge v10, v7, :cond_4

    invoke-virtual {p0, v2}, Lbir;->a(I)V

    invoke-virtual {p0}, Lbir;->c()I

    move-result v10

    invoke-virtual {p0}, Lbir;->f()I

    move-result v11

    const-string v12, "childAtomSize"

    invoke-static {v11, v12}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v11

    invoke-virtual {p0}, Lbir;->f()I

    move-result v12

    sget v13, Lbik;->m:I

    if-ne v12, v13, :cond_3

    add-int/lit8 v0, v10, 0xc

    invoke-virtual {p0, v0}, Lbir;->a(I)V

    invoke-virtual {p0}, Lbir;->d()I

    move-result v0

    and-int/lit8 v0, v0, 0x3

    add-int/lit8 v0, v0, 0x1

    if-eq v0, v14, :cond_0

    const-string v0, "Unexpected NAL unit length."

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    :cond_0
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lbir;->d()I

    move-result v0

    and-int/lit8 v12, v0, 0x1f

    move v0, v1

    :goto_2
    if-ge v0, v12, :cond_1

    invoke-static {p0}, Lbio;->b(Lbir;)[B

    move-result-object v13

    invoke-interface {v10, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_1
    invoke-virtual {p0}, Lbir;->d()I

    move-result v12

    move v0, v1

    :goto_3
    if-ge v0, v12, :cond_2

    invoke-static {p0}, Lbio;->b(Lbir;)[B

    move-result-object v13

    invoke-interface {v10, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_2
    new-array v0, v1, [[B

    invoke-interface {v10, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[B

    :cond_3
    add-int/2addr v2, v11

    goto :goto_1

    :cond_4
    const-string v2, "video/avc"

    invoke-static {v2, v8, v9, v0}, Lbkf;->a(Ljava/lang/String;II[[B)Landroid/media/MediaFormat;

    move-result-object v0

    .line 98
    :cond_5
    :goto_4
    add-int v2, v6, v7

    invoke-virtual {p0, v2}, Lbir;->a(I)V

    .line 87
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_0

    .line 93
    :cond_6
    sget v8, Lbik;->o:I

    if-ne v2, v8, :cond_7

    .line 94
    invoke-static {p0, v6, v7}, Lbio;->b(Lbir;II)Landroid/media/MediaFormat;

    move-result-object v0

    goto :goto_4

    .line 95
    :cond_7
    sget v8, Lbik;->n:I

    if-ne v2, v8, :cond_5

    .line 96
    invoke-static {p0, v6, v7}, Lbio;->a(Lbir;II)Landroid/media/MediaFormat;

    move-result-object v0

    goto :goto_4

    .line 100
    :cond_8
    return-object v0
.end method

.method private static a(Lbir;II)Landroid/media/MediaFormat;
    .locals 9

    .prologue
    .line 154
    add-int/lit8 v0, p1, 0x8

    invoke-virtual {p0, v0}, Lbir;->a(I)V

    .line 156
    const/16 v0, 0x18

    invoke-virtual {p0, v0}, Lbir;->b(I)V

    .line 157
    invoke-virtual {p0}, Lbir;->e()I

    move-result v2

    .line 158
    invoke-virtual {p0}, Lbir;->e()I

    move-result v3

    .line 159
    const/16 v0, 0x32

    invoke-virtual {p0, v0}, Lbir;->b(I)V

    .line 161
    const/4 v1, 0x0

    .line 162
    invoke-virtual {p0}, Lbir;->c()I

    move-result v0

    move v8, v0

    move-object v0, v1

    move v1, v8

    .line 163
    :goto_0
    sub-int v4, v1, p1

    if-ge v4, p2, :cond_1

    .line 164
    invoke-virtual {p0, v1}, Lbir;->a(I)V

    .line 165
    invoke-virtual {p0}, Lbir;->c()I

    move-result v4

    .line 166
    invoke-virtual {p0}, Lbir;->f()I

    move-result v5

    const-string v6, "childAtomSize"

    invoke-static {v5, v6}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v5

    .line 167
    invoke-virtual {p0}, Lbir;->f()I

    move-result v6

    .line 168
    sget v7, Lbik;->p:I

    if-ne v6, v7, :cond_0

    .line 169
    invoke-static {p0, v4}, Lbio;->a(Lbir;I)[B

    move-result-object v0

    .line 171
    :cond_0
    add-int/2addr v1, v5

    .line 172
    goto :goto_0

    .line 174
    :cond_1
    const-string v1, "video/mp4v-es"

    const/4 v4, 0x1

    new-array v4, v4, [[B

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-static {v1, v2, v3, v4}, Lbkf;->a(Ljava/lang/String;II[[B)Landroid/media/MediaFormat;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lbir;I)[B
    .locals 5

    .prologue
    const/16 v4, 0x7f

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 214
    add-int/lit8 v0, p1, 0xc

    invoke-virtual {p0, v0}, Lbir;->a(I)V

    .line 216
    invoke-virtual {p0, v2}, Lbir;->b(I)V

    .line 217
    invoke-virtual {p0}, Lbir;->d()I

    move-result v0

    .line 218
    :goto_0
    if-le v0, v4, :cond_0

    .line 219
    invoke-virtual {p0}, Lbir;->d()I

    move-result v0

    goto :goto_0

    .line 221
    :cond_0
    invoke-virtual {p0, v3}, Lbir;->b(I)V

    .line 223
    invoke-virtual {p0}, Lbir;->d()I

    move-result v0

    .line 224
    and-int/lit16 v1, v0, 0x80

    if-eqz v1, :cond_1

    .line 225
    invoke-virtual {p0, v3}, Lbir;->b(I)V

    .line 227
    :cond_1
    and-int/lit8 v1, v0, 0x40

    if-eqz v1, :cond_2

    .line 228
    invoke-virtual {p0}, Lbir;->e()I

    move-result v1

    invoke-virtual {p0, v1}, Lbir;->b(I)V

    .line 230
    :cond_2
    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_3

    .line 231
    invoke-virtual {p0, v3}, Lbir;->b(I)V

    .line 235
    :cond_3
    invoke-virtual {p0, v2}, Lbir;->b(I)V

    .line 236
    invoke-virtual {p0}, Lbir;->d()I

    move-result v0

    .line 237
    :goto_1
    if-le v0, v4, :cond_4

    .line 238
    invoke-virtual {p0}, Lbir;->d()I

    move-result v0

    goto :goto_1

    .line 240
    :cond_4
    const/16 v0, 0xd

    invoke-virtual {p0, v0}, Lbir;->b(I)V

    .line 243
    invoke-virtual {p0, v2}, Lbir;->b(I)V

    .line 244
    invoke-virtual {p0}, Lbir;->d()I

    move-result v1

    .line 245
    and-int/lit8 v0, v1, 0x7f

    .line 246
    :goto_2
    if-le v1, v4, :cond_5

    .line 247
    invoke-virtual {p0}, Lbir;->d()I

    move-result v1

    .line 248
    shl-int/lit8 v0, v0, 0x8

    .line 249
    and-int/lit8 v2, v1, 0x7f

    or-int/2addr v0, v2

    goto :goto_2

    .line 251
    :cond_5
    new-array v1, v0, [B

    .line 252
    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2, v0}, Lbir;->a([BII)V

    .line 253
    return-object v1
.end method

.method private static b(Lbir;II)Landroid/media/MediaFormat;
    .locals 10

    .prologue
    const/4 v4, 0x0

    .line 181
    add-int/lit8 v0, p1, 0x8

    invoke-virtual {p0, v0}, Lbir;->a(I)V

    .line 184
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lbir;->b(I)V

    .line 185
    invoke-virtual {p0}, Lbir;->e()I

    move-result v2

    .line 186
    invoke-virtual {p0}, Lbir;->e()I

    .line 187
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lbir;->b(I)V

    .line 188
    invoke-virtual {p0}, Lbir;->h()I

    move-result v1

    .line 191
    invoke-virtual {p0}, Lbir;->c()I

    move-result v0

    move v3, v0

    move-object v0, v4

    .line 192
    :goto_0
    sub-int v5, v3, p1

    if-ge v5, p2, :cond_1

    .line 193
    invoke-virtual {p0, v3}, Lbir;->a(I)V

    .line 194
    invoke-virtual {p0}, Lbir;->c()I

    move-result v5

    .line 195
    invoke-virtual {p0}, Lbir;->f()I

    move-result v6

    const-string v7, "childAtomSize"

    invoke-static {v6, v7}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v6

    .line 196
    invoke-virtual {p0}, Lbir;->f()I

    move-result v7

    .line 197
    sget v8, Lbik;->p:I

    if-ne v7, v8, :cond_0

    .line 198
    invoke-static {p0, v5}, Lbio;->a(Lbir;I)[B

    move-result-object v1

    .line 202
    invoke-static {v1}, Lbie;->a([B)Landroid/util/Pair;

    move-result-object v5

    .line 203
    iget-object v0, v5, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 204
    iget-object v0, v5, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move-object v9, v1

    move v1, v2

    move v2, v0

    move-object v0, v9

    .line 206
    :cond_0
    add-int/2addr v3, v6

    .line 207
    goto :goto_0

    .line 208
    :cond_1
    const-string v3, "audio/mp4a-latm"

    .line 209
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    .line 208
    const-string v0, "audio/"

    invoke-virtual {v3, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    const-string v6, "mimeType"

    invoke-static {v0, v6}, Lcec;->a(ZLjava/lang/CharSequence;)V

    const-string v0, "initializationData"

    invoke-static {v5, v0, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    new-instance v4, Landroid/media/MediaFormat;

    invoke-direct {v4}, Landroid/media/MediaFormat;-><init>()V

    const-string v0, "mime"

    invoke-virtual {v4, v0, v3}, Landroid/media/MediaFormat;->setString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "channel-count"

    const-string v3, "channelCount"

    invoke-static {v2, v3}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v2

    invoke-virtual {v4, v0, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    const-string v0, "sample-rate"

    const-string v2, "sampleRate"

    invoke-static {v1, v2}, Lcec;->a(ILjava/lang/CharSequence;)I

    move-result v1

    invoke-virtual {v4, v0, v1}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v2, 0xf

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "csd-"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v4, v2, v0}, Landroid/media/MediaFormat;->setByteBuffer(Ljava/lang/String;Ljava/nio/ByteBuffer;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    return-object v4
.end method

.method private static b(Lbir;)[B
    .locals 3

    .prologue
    .line 258
    invoke-virtual {p0}, Lbir;->e()I

    move-result v0

    .line 259
    invoke-virtual {p0}, Lbir;->c()I

    move-result v1

    .line 260
    invoke-virtual {p0, v0}, Lbir;->b(I)V

    .line 261
    invoke-virtual {p0}, Lbir;->a()[B

    move-result-object v2

    invoke-static {v2, v1, v0}, Lbie;->a([BII)[B

    move-result-object v0

    return-object v0
.end method
