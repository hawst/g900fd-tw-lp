.class public final Lglv;
.super Lghh;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lghh",
        "<",
        "Lglp;",
        ">;"
    }
.end annotation


# instance fields
.field private final d:Lglz;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lglz",
            "<",
            "Lglp;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lgls;

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lgfg;Lgfh;Ljava/lang/String;)V
    .locals 3

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lghh;-><init>(Landroid/content/Context;Lgfg;Lgfh;[Ljava/lang/String;)V

    new-instance v0, Lglw;

    invoke-direct {v0, p0}, Lglw;-><init>(Lglv;)V

    iput-object v0, p0, Lglv;->d:Lglz;

    new-instance v0, Lgls;

    iget-object v1, p0, Lglv;->d:Lglz;

    invoke-direct {v0, p1, v1}, Lgls;-><init>(Landroid/content/Context;Lglz;)V

    iput-object v0, p0, Lglv;->e:Lgls;

    iput-object p4, p0, Lglv;->f:Ljava/lang/String;

    new-instance v0, Lgmh;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lglv;->d:Lglz;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lgmh;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lglv;->d:Lglz;

    new-instance v0, Lglk;

    invoke-direct {v0}, Lglk;-><init>()V

    iget-object v0, p0, Lglv;->d:Lglz;

    new-instance v0, Lgjy;

    invoke-direct {v0}, Lgjy;-><init>()V

    return-void
.end method

.method static synthetic a(Lglv;)V
    .locals 0

    invoke-virtual {p0}, Lglv;->h()V

    return-void
.end method


# virtual methods
.method protected synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, Lglv;->b(Landroid/os/IBinder;)Lglp;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/google/android/gms/internal/nk;Lgrj;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lglv;->a(Lcom/google/android/gms/internal/nk;Lgrj;Landroid/os/Looper;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/internal/nk;Lgrj;Landroid/os/Looper;)V
    .locals 2

    iget-object v1, p0, Lglv;->e:Lgls;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lglv;->e:Lgls;

    invoke-virtual {v0, p1, p2, p3}, Lgls;->a(Lcom/google/android/gms/internal/nk;Lgrj;Landroid/os/Looper;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected a(Lgic;Lghl;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v1, "client_name"

    iget-object v2, p0, Lglv;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x5e3530

    iget-object v2, p0, Lghh;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lgic;->e(Lghz;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method protected b(Landroid/os/IBinder;)Lglp;
    .locals 1

    invoke-static {p1}, Lglq;->a(Landroid/os/IBinder;)Lglp;

    move-result-object v0

    return-object v0
.end method

.method public b()V
    .locals 2

    iget-object v1, p0, Lglv;->e:Lgls;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Lglv;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lglv;->e:Lgls;

    invoke-virtual {v0}, Lgls;->b()V

    iget-object v0, p0, Lglv;->e:Lgls;

    invoke-virtual {v0}, Lgls;->c()V

    :cond_0
    invoke-super {p0}, Lghh;->b()V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.location.internal.GoogleLocationManagerService.START"

    return-object v0
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    const-string v0, "com.google.android.gms.location.internal.IGoogleLocationManagerService"

    return-object v0
.end method

.method public j()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lglv;->e:Lgls;

    invoke-virtual {v0}, Lgls;->a()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method
