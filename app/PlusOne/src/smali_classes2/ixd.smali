.class public Lixd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lheh;
.implements Liwc;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Liwd;",
            ">;"
        }
    .end annotation
.end field

.field private final d:Ljava/lang/Object;

.field private final e:Liwx;

.field private final f:Lhfp;

.field private final g:Lhei;

.field private final h:Lkfd;

.field private final i:Liww;

.field private final j:Ljava/util/concurrent/ExecutorService;

.field private k:Lher;

.field private l:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Liwo;",
            ">;"
        }
    .end annotation
.end field

.field private m:Liwn;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lixd;->c:Ljava/util/List;

    .line 57
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lixd;->d:Ljava/lang/Object;

    .line 69
    iput-object p1, p0, Lixd;->b:Landroid/content/Context;

    .line 70
    invoke-static {p1}, Liwx;->a(Landroid/content/Context;)Liwx;

    move-result-object v0

    iput-object v0, p0, Lixd;->e:Liwx;

    .line 71
    const-class v0, Lher;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lher;

    iput-object v0, p0, Lixd;->k:Lher;

    .line 72
    const-class v0, Lhfp;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhfp;

    iput-object v0, p0, Lixd;->f:Lhfp;

    .line 73
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Lixd;->g:Lhei;

    .line 74
    const-class v0, Lkfd;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkfd;

    iput-object v0, p0, Lixd;->h:Lkfd;

    .line 75
    const-class v0, Liwo;

    invoke-static {p1, v0}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lixd;->l:Ljava/util/List;

    .line 76
    const-class v0, Liwn;

    invoke-static {p1, v0}, Llnh;->b(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwn;

    iput-object v0, p0, Lixd;->m:Liwn;

    .line 77
    const-class v0, Liww;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liww;

    iput-object v0, p0, Lixd;->i:Liww;

    .line 78
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v2, 0x0

    const/4 v3, 0x4

    const-wide/16 v4, 0x0

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    invoke-direct/range {v1 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    iput-object v1, p0, Lixd;->j:Ljava/util/concurrent/ExecutorService;

    .line 80
    return-void
.end method

.method static synthetic a(Lixd;Lkff;)Ljava/io/IOException;
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lixd;->a(Lkff;)Ljava/io/IOException;

    move-result-object v0

    return-object v0
.end method

.method private a(Lkff;)Ljava/io/IOException;
    .locals 4

    .prologue
    .line 346
    iget-object v1, p1, Lkff;->k:Ljava/lang/Exception;

    .line 347
    if-eqz v1, :cond_0

    .line 348
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Rpc failed"

    invoke-direct {v0, v2, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 352
    :goto_0
    return-object v0

    .line 349
    :cond_0
    invoke-virtual {p1}, Lkff;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 350
    new-instance v0, Ljava/io/IOException;

    iget v1, p1, Lkff;->i:I

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x20

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "RPC failed with code "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 352
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lixd;)Lkfd;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lixd;->h:Lkfd;

    return-object v0
.end method

.method private a(ILiwe;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 179
    .line 181
    const/4 v1, -0x1

    if-eq p1, v1, :cond_0

    .line 182
    iget-object v1, p0, Lixd;->g:Lhei;

    invoke-interface {v1, p1}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 183
    const-string v2, "LoginManager.last_updated"

    const-wide/16 v4, 0x0

    invoke-interface {v1, v2, v4, v5}, Lhej;->a(Ljava/lang/String;J)J

    move-result-wide v2

    .line 184
    const-string v4, "logged_out"

    invoke-interface {v1, v4}, Lhej;->c(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 185
    invoke-direct {p0, p1}, Lixd;->c(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 195
    :cond_0
    :goto_0
    return v0

    .line 188
    :cond_1
    invoke-virtual {p2}, Liwe;->a()Z

    move-result v1

    if-nez v1, :cond_2

    .line 189
    iget-object v1, p0, Lixd;->e:Liwx;

    invoke-virtual {v1}, Liwx;->a()J

    move-result-wide v4

    sub-long v2, v4, v2

    .line 192
    invoke-virtual {p2}, Liwe;->b()J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Liwg;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 533
    iget-object v0, p0, Lixd;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwd;

    .line 534
    invoke-interface {v0, p1, p2, p3}, Liwd;->a(Liwg;Ljava/lang/String;I)V

    goto :goto_0

    .line 536
    :cond_0
    return-void
.end method

.method private b(Ljava/lang/String;Liwe;)V
    .locals 4

    .prologue
    .line 199
    iget-object v1, p0, Lixd;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 200
    :try_start_0
    invoke-static {}, Llsx;->c()V

    .line 202
    iget-object v0, p0, Lixd;->b:Landroid/content/Context;

    const-class v2, Lhei;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 203
    invoke-interface {v0, p1}, Lhei;->a(Ljava/lang/String;)I

    move-result v0

    .line 204
    invoke-direct {p0, v0, p2}, Lixd;->a(ILiwe;)Z

    move-result v0

    .line 205
    if-eqz v0, :cond_0

    .line 206
    const/4 v0, 0x0

    const/4 v2, 0x0

    invoke-virtual {p2}, Liwe;->c()Z

    move-result v3

    invoke-virtual {p0, p1, v0, v2, v3}, Lixd;->a(Ljava/lang/String;Ljava/lang/String;Liwg;Z)I

    .line 208
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private b(Ljava/util/List;)V
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lkff;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 314
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 315
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 317
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkff;

    .line 318
    new-instance v4, Lixe;

    invoke-direct {v4, p0, v0, v2, v1}, Lixe;-><init>(Lixd;Lkff;Ljava/util/List;Ljava/util/concurrent/CountDownLatch;)V

    .line 329
    iget-object v0, p0, Lixd;->j:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0, v4}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 332
    :cond_0
    :try_start_0
    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 337
    :goto_1
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 338
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Exception;

    .line 339
    const-string v3, "LoginManager"

    const-string v4, "Login HTTP operation error"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2

    .line 333
    :catch_0
    move-exception v0

    .line 334
    new-instance v1, Ljava/io/IOException;

    const-string v3, "Rpc interrupted"

    invoke-direct {v1, v3, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 341
    :cond_1
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/IOException;

    throw v0

    .line 343
    :cond_2
    return-void
.end method

.method private b(Liwe;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 144
    iget-object v0, p0, Lixd;->f:Lhfp;

    invoke-interface {v0}, Lhfp;->a()V

    .line 146
    iget-object v3, p0, Lixd;->d:Ljava/lang/Object;

    monitor-enter v3

    .line 147
    :try_start_0
    invoke-static {}, Llsx;->c()V

    .line 149
    iget-object v0, p0, Lixd;->k:Lher;

    invoke-interface {v0}, Lher;->a()[Ljava/lang/String;

    move-result-object v4

    .line 150
    const/4 v0, 0x1

    move v2, v1

    .line 151
    :goto_0
    array-length v5, v4

    if-ge v2, v5, :cond_0

    .line 152
    aget-object v5, v4, v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    :try_start_1
    invoke-direct {p0, v5, p1}, Lixd;->b(Ljava/lang/String;Liwe;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 156
    :catch_0
    move-exception v0

    .line 158
    :try_start_2
    const-string v5, "LoginManager"

    const-string v6, "Cannot refresh account "

    invoke-static {v5, v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v1

    goto :goto_1

    .line 162
    :cond_0
    monitor-exit v3

    return v0

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method private c()Liwe;
    .locals 4

    .prologue
    .line 127
    new-instance v0, Liwf;

    invoke-direct {v0}, Liwf;-><init>()V

    const/4 v1, 0x1

    .line 128
    invoke-virtual {v0, v1}, Liwf;->a(Z)Liwf;

    move-result-object v0

    const-wide/16 v2, 0x0

    .line 129
    invoke-virtual {v0, v2, v3}, Liwf;->a(J)Liwf;

    move-result-object v0

    const/4 v1, 0x0

    .line 130
    invoke-virtual {v0, v1}, Liwf;->b(Z)Liwf;

    move-result-object v0

    .line 131
    invoke-virtual {v0}, Liwf;->a()Liwe;

    move-result-object v0

    return-object v0
.end method

.method private c(I)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 117
    const/4 v1, -0x1

    if-ne p1, v1, :cond_1

    .line 123
    :cond_0
    :goto_0
    return v0

    .line 121
    :cond_1
    iget-object v1, p0, Lixd;->g:Lhei;

    invoke-interface {v1, p1}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 122
    iget-object v2, p0, Lixd;->i:Liww;

    invoke-virtual {v2}, Liww;->a()Ljava/lang/String;

    move-result-object v2

    .line 123
    const-string v3, "LoginManager.build_version"

    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d(I)V
    .locals 4

    .prologue
    .line 365
    iget-object v0, p0, Lixd;->b:Landroid/content/Context;

    const-class v1, Liwq;

    invoke-static {v0, v1}, Llnh;->c(Landroid/content/Context;Ljava/lang/Class;)Ljava/util/List;

    move-result-object v2

    .line 366
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    .line 368
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 369
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwq;

    invoke-interface {v0, p1}, Liwq;->a(I)V

    .line 368
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 371
    :cond_0
    return-void
.end method


# virtual methods
.method a(Ljava/lang/String;Ljava/lang/String;Liwg;Z)I
    .locals 10

    .prologue
    .line 213
    iget-object v6, p0, Lixd;->d:Ljava/lang/Object;

    monitor-enter v6

    .line 214
    :try_start_0
    invoke-static {}, Llsx;->c()V

    .line 216
    iget-object v0, p0, Lixd;->g:Lhei;

    invoke-interface {v0, p1, p2}, Lhei;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    .line 217
    new-instance v0, Lkfo;

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    invoke-direct/range {v0 .. v5}, Lkfo;-><init>(Ljava/lang/String;Ljava/lang/String;Lkey;ZLkfg;)V

    .line 222
    invoke-direct {p0, v7}, Lixd;->c(I)Z

    move-result v1

    if-nez v1, :cond_0

    if-nez p4, :cond_2

    :cond_0
    const/4 v1, 0x1

    move v5, v1

    .line 224
    :goto_0
    const/4 v1, -0x1

    if-ne v7, v1, :cond_3

    iget-object v1, p0, Lixd;->g:Lhei;

    .line 225
    invoke-interface {v1, p1, p2}, Lhei;->a(Ljava/lang/String;Ljava/lang/String;)Lhek;

    move-result-object v1

    move-object v4, v1

    .line 228
    :goto_1
    new-instance v7, Ljava/util/ArrayList;

    iget-object v1, p0, Lixd;->l:Ljava/util/List;

    invoke-direct {v7, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 231
    :cond_1
    invoke-interface {v7}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_b

    .line 233
    new-instance v8, Ljava/util/IdentityHashMap;

    invoke-direct {v8}, Ljava/util/IdentityHashMap;-><init>()V

    .line 235
    invoke-interface {v7}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Liwo;

    .line 237
    invoke-interface {v1, v4, v5}, Liwo;->a(Lhej;Z)Liwp;

    move-result-object v3

    .line 238
    invoke-interface {v8, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 292
    :catchall_0
    move-exception v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 222
    :cond_2
    const/4 v1, 0x0

    move v5, v1

    goto :goto_0

    .line 225
    :cond_3
    :try_start_1
    iget-object v1, p0, Lixd;->g:Lhei;

    .line 226
    invoke-interface {v1, v7}, Lhei;->b(I)Lhek;

    move-result-object v1

    move-object v4, v1

    goto :goto_1

    .line 241
    :cond_4
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 242
    invoke-interface {v8}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Liwp;

    .line 243
    if-eqz v1, :cond_5

    .line 244
    invoke-interface {v1, v4, v0, v2}, Liwp;->a(Lhej;Lkfo;Ljava/util/List;)V

    goto :goto_3

    .line 248
    :cond_6
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_7

    const/4 v1, 0x0

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkff;

    iget-object v2, p0, Lixd;->h:Lkfd;

    invoke-interface {v2, v1}, Lkfd;->a(Lkff;)V

    invoke-direct {p0, v1}, Lixd;->a(Lkff;)Ljava/io/IOException;

    move-result-object v1

    if-eqz v1, :cond_8

    throw v1

    :cond_7
    invoke-direct {p0, v2}, Lixd;->b(Ljava/util/List;)V

    .line 250
    :cond_8
    invoke-interface {v8}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :cond_9
    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Liwo;

    .line 251
    const/4 v3, 0x0

    .line 252
    invoke-interface {v8, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Liwp;

    .line 253
    if-eqz v2, :cond_10

    .line 254
    invoke-interface {v2, v4}, Liwp;->a(Lhem;)I

    move-result v2

    .line 255
    const/4 v3, 0x2

    if-ne v2, v3, :cond_a

    const/4 v2, 0x1

    .line 258
    :goto_5
    if-nez v2, :cond_9

    .line 259
    invoke-interface {v7, v1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_4

    .line 255
    :cond_a
    const/4 v2, 0x0

    goto :goto_5

    .line 264
    :cond_b
    const-string v0, "LoginManager.last_updated"

    iget-object v1, p0, Lixd;->e:Liwx;

    invoke-virtual {v1}, Liwx;->a()J

    move-result-wide v2

    invoke-interface {v4, v0, v2, v3}, Lhek;->b(Ljava/lang/String;J)Lhek;

    .line 265
    const-string v0, "LoginManager.build_version"

    iget-object v1, p0, Lixd;->i:Liww;

    invoke-virtual {v1}, Liww;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Lhek;->b(Ljava/lang/String;Ljava/lang/String;)Lhek;

    .line 268
    const/4 v0, 0x0

    .line 269
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 271
    iget-object v2, p0, Lixd;->m:Liwn;

    if-eqz v2, :cond_c

    .line 272
    iget-object v2, p0, Lixd;->m:Liwn;

    invoke-interface {v2, p1, v1}, Liwn;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 274
    :cond_c
    iget-object v2, p0, Lixd;->m:Liwn;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lixd;->m:Liwn;

    invoke-interface {v2}, Liwn;->a()Z

    move-result v2

    if-eqz v2, :cond_f

    .line 275
    const/4 v0, 0x1

    .line 276
    iget-object v2, p0, Lixd;->m:Liwn;

    .line 282
    :cond_d
    :goto_6
    if-eqz v0, :cond_e

    .line 284
    invoke-virtual {p0, v1}, Lixd;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 285
    invoke-virtual {p0, v4, v0}, Lixd;->a(Lhek;Ljava/util/List;)Z

    .line 288
    :cond_e
    invoke-interface {v4}, Lhek;->c()I

    move-result v0

    .line 289
    iget-object v1, p0, Lixd;->f:Lhfp;

    invoke-interface {v1, v0}, Lhfp;->a(I)V

    .line 290
    invoke-direct {p0, v0}, Lixd;->d(I)V

    .line 291
    monitor-exit v6

    return v0

    .line 277
    :cond_f
    if-eqz p3, :cond_d

    iget-boolean v2, p3, Liwg;->d:Z

    if-eqz v2, :cond_d

    .line 278
    const/4 v0, 0x1

    .line 279
    iget-object v2, p3, Liwg;->s:Ljava/util/List;

    invoke-interface {v1, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_6

    :cond_10
    move v2, v3

    goto :goto_5
.end method

.method a(Ljava/util/List;)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Class",
            "<+",
            "Liwi;",
            ">;>;)",
            "Ljava/util/List",
            "<",
            "Liwi;",
            ">;"
        }
    .end annotation

    .prologue
    .line 495
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 497
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Class;

    .line 498
    iget-object v3, p0, Lixd;->b:Landroid/content/Context;

    invoke-static {v3, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwi;

    .line 499
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 501
    :cond_0
    return-object v1
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 361
    invoke-virtual {p0, p1}, Lixd;->b(I)V

    .line 362
    return-void
.end method

.method public a(Lae;Liwg;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 84
    invoke-static {p1}, Liwy;->a(Lae;)Liwy;

    move-result-object v0

    .line 85
    invoke-virtual {v0, p2, p3}, Liwy;->a(Liwg;Ljava/lang/String;)V

    .line 86
    return-void
.end method

.method a(Lhek;)V
    .locals 3

    .prologue
    .line 418
    const-string v0, "logged_out"

    invoke-interface {p1, v0}, Lhek;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 419
    invoke-interface {p1}, Lhek;->b()Lhek;

    .line 423
    :cond_0
    const-string v0, "logged_in"

    const/4 v1, 0x1

    .line 424
    invoke-interface {p1, v0, v1}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    const-string v1, "logged_out"

    const/4 v2, 0x0

    .line 425
    invoke-interface {v0, v1, v2}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    .line 426
    return-void
.end method

.method public a(Liwd;)V
    .locals 1

    .prologue
    .line 375
    iget-object v0, p0, Lixd;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 376
    return-void
.end method

.method public a(Liwe;)V
    .locals 0

    .prologue
    .line 140
    invoke-direct {p0, p1}, Lixd;->b(Liwe;)Z

    .line 141
    return-void
.end method

.method a(Liwg;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 529
    const/4 v0, -0x1

    invoke-direct {p0, p1, p2, v0}, Lixd;->b(Liwg;Ljava/lang/String;I)V

    .line 530
    return-void
.end method

.method a(Liwg;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 525
    invoke-direct {p0, p1, p2, p3}, Lixd;->b(Liwg;Ljava/lang/String;I)V

    .line 526
    return-void
.end method

.method public a(Ljava/lang/String;Liwe;)V
    .locals 4

    .prologue
    .line 168
    iget-object v0, p0, Lixd;->g:Lhei;

    invoke-interface {v0, p1}, Lhei;->a(Ljava/lang/String;)I

    move-result v0

    .line 170
    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget-object v1, p0, Lixd;->g:Lhei;

    .line 171
    invoke-interface {v1, v0}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "logged_out"

    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 172
    new-instance v1, Livr;

    const-string v2, "refreshAccount called for a logged out account: "

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v1, v0}, Livr;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 175
    :cond_1
    invoke-direct {p0, p1, p2}, Lixd;->b(Ljava/lang/String;Liwe;)V

    .line 176
    return-void
.end method

.method a()Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 94
    invoke-direct {p0}, Lixd;->c()Liwe;

    move-result-object v3

    .line 95
    new-instance v4, Ljava/util/HashSet;

    iget-object v0, p0, Lixd;->k:Lher;

    invoke-interface {v0}, Lher;->a()[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    .line 97
    iget-object v0, p0, Lixd;->b:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 98
    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 99
    invoke-interface {v0, v1}, Lhei;->a(Ljava/lang/String;)I

    move-result v1

    .line 100
    invoke-direct {p0, v1, v3}, Lixd;->a(ILiwe;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v2

    .line 113
    :goto_0
    return v0

    .line 106
    :cond_1
    invoke-interface {v0}, Lhei;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 107
    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v5, "account_name"

    invoke-interface {v1, v5}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 108
    invoke-interface {v4, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v2

    .line 109
    goto :goto_0

    .line 113
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method a(Lhek;Ljava/util/List;)Z
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lhek;",
            "Ljava/util/List",
            "<",
            "Liwi;",
            ">;)Z"
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 384
    const-string v0, "logged_in"

    invoke-interface {p1, v0}, Lhek;->c(Ljava/lang/String;)Z

    move-result v4

    .line 387
    if-eqz p2, :cond_1

    .line 388
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwi;

    .line 389
    if-eqz v1, :cond_0

    invoke-interface {v0, p1}, Liwi;->a(Lhej;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    :goto_1
    move v1, v0

    .line 390
    goto :goto_0

    :cond_0
    move v0, v3

    .line 389
    goto :goto_1

    :cond_1
    move v1, v2

    .line 394
    :cond_2
    if-nez v1, :cond_3

    .line 402
    :goto_2
    return v3

    .line 398
    :cond_3
    if-nez v4, :cond_4

    .line 399
    invoke-virtual {p0, p1}, Lixd;->a(Lhek;)V

    :cond_4
    move v3, v2

    .line 402
    goto :goto_2
.end method

.method public a(Liwg;I)Z
    .locals 1

    .prologue
    .line 453
    iget-boolean v0, p1, Liwg;->d:Z

    invoke-virtual {p0, p1, p2, v0}, Lixd;->a(Liwg;IZ)Z

    move-result v0

    return v0
.end method

.method public a(Liwg;IZ)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 458
    iget-object v1, p0, Lixd;->g:Lhei;

    invoke-interface {v1, p2}, Lhei;->c(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 470
    :cond_0
    :goto_0
    return v0

    .line 462
    :cond_1
    invoke-virtual {p0, p1, p2}, Lixd;->b(Liwg;I)Liwi;

    move-result-object v1

    if-nez v1, :cond_0

    .line 466
    if-eqz p3, :cond_2

    iget-object v1, p0, Lixd;->g:Lhei;

    invoke-interface {v1, p2}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v2, "logged_in"

    invoke-interface {v1, v2}, Lhej;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 470
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method b(Liwg;I)Liwi;
    .locals 4

    .prologue
    .line 474
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 476
    iget-object v1, p0, Lixd;->g:Lhei;

    invoke-interface {v1, p2}, Lhei;->a(I)Lhej;

    move-result-object v1

    .line 477
    iget-object v2, p0, Lixd;->m:Liwn;

    if-eqz v2, :cond_0

    .line 478
    iget-object v2, p0, Lixd;->m:Liwn;

    const-string v3, "account_name"

    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3, v0}, Liwn;->a(Ljava/lang/String;Ljava/util/List;)V

    .line 481
    :cond_0
    iget-object v2, p1, Liwg;->s:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 483
    invoke-virtual {p0, v0}, Lixd;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 485
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Liwi;

    .line 486
    invoke-interface {v0, v1}, Liwi;->a(Lhej;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 490
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 435
    iget-object v2, p0, Lixd;->d:Ljava/lang/Object;

    monitor-enter v2

    .line 436
    :try_start_0
    iget-object v1, p0, Lixd;->g:Lhei;

    invoke-interface {v1, p1}, Lhei;->d(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 437
    monitor-exit v2

    .line 448
    :goto_0
    return-void

    .line 440
    :cond_0
    iget-object v1, p0, Lixd;->g:Lhei;

    invoke-interface {v1, p1}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v3, "is_managed_account"

    invoke-interface {v1, v3}, Lhej;->c(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "account_name"

    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v1, p0, Lixd;->g:Lhei;

    invoke-interface {v1}, Lhei;->a()Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v1, v0

    :goto_1
    if-ge v1, v5, :cond_2

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v6, p0, Lixd;->g:Lhei;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    invoke-interface {v6, v7}, Lhei;->a(I)Lhej;

    move-result-object v6

    invoke-interface {v6}, Lhej;->a()Z

    move-result v7

    if-eqz v7, :cond_1

    const-string v7, "is_managed_account"

    invoke-interface {v6, v7}, Lhej;->c(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    const-string v7, "account_name"

    invoke-interface {v6, v7}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lixd;->b(I)V

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 442
    :cond_2
    iget-object v0, p0, Lixd;->g:Lhei;

    invoke-interface {v0, p1}, Lhei;->b(I)Lhek;

    move-result-object v0

    const-string v1, "logged_out"

    const/4 v3, 0x1

    .line 443
    invoke-interface {v0, v1, v3}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    const-string v1, "logged_in"

    const/4 v3, 0x0

    .line 444
    invoke-interface {v0, v1, v3}, Lhek;->b(Ljava/lang/String;Z)Lhek;

    move-result-object v0

    .line 445
    invoke-interface {v0}, Lhek;->c()I

    .line 447
    invoke-direct {p0, p1}, Lixd;->d(I)V

    .line 448
    monitor-exit v2

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Liwd;)V
    .locals 1

    .prologue
    .line 380
    iget-object v0, p0, Lixd;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 381
    return-void
.end method

.method b()Z
    .locals 1

    .prologue
    .line 135
    invoke-direct {p0}, Lixd;->c()Liwe;

    move-result-object v0

    invoke-direct {p0, v0}, Lixd;->b(Liwe;)Z

    move-result v0

    return v0
.end method
