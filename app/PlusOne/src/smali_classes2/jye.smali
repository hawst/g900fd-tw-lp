.class public final Ljye;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljdg;


# instance fields
.field final a:Landroid/content/Context;

.field b:Lhoc;

.field final c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lizu;",
            ">;"
        }
    .end annotation
.end field

.field final d:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lizu;",
            "Ljyc;",
            ">;"
        }
    .end annotation
.end field

.field private e:Lhee;

.field private f:Ljct;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Ljye;->c:Ljava/util/ArrayList;

    .line 96
    new-instance v0, Lgf;

    invoke-direct {v0}, Lgf;-><init>()V

    iput-object v0, p0, Ljye;->d:Ljava/util/Map;

    .line 105
    iput-object p1, p0, Ljye;->a:Landroid/content/Context;

    .line 106
    return-void
.end method

.method private a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 448
    iget-object v0, p0, Ljye;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 449
    new-instance v0, Ljyb;

    iget-object v1, p0, Ljye;->a:Landroid/content/Context;

    iget-object v2, p0, Ljye;->e:Lhee;

    .line 450
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {v0, v1, v2}, Ljyb;-><init>(Landroid/content/Context;I)V

    .line 451
    invoke-virtual {v0}, Ljyb;->l()V

    .line 453
    :try_start_0
    const-string v1, "MediaUploadManagerImpl"

    invoke-virtual {v0, v1}, Ljyb;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 458
    invoke-virtual {v0}, Ljyb;->D()Loxu;

    move-result-object v0

    check-cast v0, Llzh;

    iget-object v0, v0, Llzh;->a:Lnff;

    .line 459
    iget-object v0, v0, Lnff;->a:Lnyb;

    iget-object v0, v0, Lnyb;->d:Ljava/lang/String;

    iput-object v0, p0, Ljye;->h:Ljava/lang/String;

    .line 461
    :cond_0
    iget-object v0, p0, Ljye;->h:Ljava/lang/String;

    :goto_0
    return-object v0

    .line 455
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lizu;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 428
    const-string v0, "MediaUploadManagerImpl"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 430
    iget-object v1, p0, Ljye;->d:Ljava/util/Map;

    monitor-enter v1

    .line 431
    :try_start_0
    iget-object v0, p0, Ljye;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyc;

    .line 432
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 433
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1, p2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 434
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lizu;->i()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 435
    const-string v2, "\n  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lizu;->e()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 437
    :cond_0
    if-eqz v0, :cond_1

    .line 438
    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljyc;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 439
    invoke-virtual {v0}, Ljyc;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 440
    const-string v2, "\n  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Ljyc;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 443
    :cond_1
    return-void

    .line 432
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private d(Lizu;)Ljava/lang/Long;
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 288
    if-nez p1, :cond_0

    .line 289
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 306
    :goto_0
    return-object v0

    .line 291
    :cond_0
    invoke-virtual {p1}, Lizu;->c()J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 292
    const-string v0, "Photo already has photoId"

    invoke-direct {p0, p1, v0}, Ljye;->a(Lizu;Ljava/lang/String;)V

    .line 293
    invoke-virtual {p1}, Lizu;->c()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 295
    :cond_1
    invoke-virtual {p1}, Lizu;->e()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_2

    .line 296
    const-string v0, "No local Uri to upload"

    invoke-direct {p0, p1, v0}, Ljye;->a(Lizu;Ljava/lang/String;)V

    .line 297
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 299
    :cond_2
    iget-object v1, p0, Ljye;->d:Ljava/util/Map;

    monitor-enter v1

    .line 300
    :try_start_0
    iget-object v0, p0, Ljye;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyc;

    .line 301
    if-eqz v0, :cond_3

    .line 302
    const-string v2, "Photo already uploaded"

    invoke-direct {p0, p1, v2}, Ljye;->a(Lizu;Ljava/lang/String;)V

    .line 303
    invoke-virtual {v0}, Ljyc;->b()Ljava/lang/Long;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 305
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_3
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 306
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method private e(Lizu;)Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 379
    iget-object v1, p0, Ljye;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 380
    invoke-virtual {p1}, Lizu;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 381
    invoke-virtual {p1}, Lizu;->f()Ljava/lang/String;

    move-result-object v0

    .line 402
    :cond_0
    :goto_0
    return-object v0

    .line 383
    :cond_1
    invoke-virtual {p1}, Lizu;->e()Landroid/net/Uri;

    move-result-object v2

    .line 386
    :try_start_0
    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->openInputStream(Landroid/net/Uri;)Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 387
    :try_start_1
    invoke-static {v2}, Ljbh;->b(Ljava/io/InputStream;)Ljbh;

    move-result-object v1

    .line 388
    invoke-virtual {v1}, Ljbh;->a()Ljava/lang/String;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 394
    if-eqz v2, :cond_0

    .line 396
    :try_start_2
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 397
    :catch_0
    move-exception v1

    .line 398
    const-string v2, "MediaUploadManagerImpl"

    const-string v3, "Unable to close stream"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 389
    :catch_1
    move-exception v1

    move-object v2, v0

    .line 390
    :goto_1
    :try_start_3
    const-string v3, "MediaUploadManagerImpl"

    const-string v4, "Photo url not found"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 394
    if-eqz v2, :cond_0

    .line 396
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    .line 397
    :catch_2
    move-exception v1

    .line 398
    const-string v2, "MediaUploadManagerImpl"

    const-string v3, "Unable to close stream"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 391
    :catch_3
    move-exception v1

    move-object v2, v0

    .line 392
    :goto_2
    :try_start_5
    const-string v3, "MediaUploadManagerImpl"

    const-string v4, "Unable to open photo url"

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 394
    if-eqz v2, :cond_0

    .line 396
    :try_start_6
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    goto :goto_0

    .line 397
    :catch_4
    move-exception v1

    .line 398
    const-string v2, "MediaUploadManagerImpl"

    const-string v3, "Unable to close stream"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 394
    :catchall_0
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    :goto_3
    if-eqz v2, :cond_2

    .line 396
    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 399
    :cond_2
    :goto_4
    throw v0

    .line 397
    :catch_5
    move-exception v1

    .line 398
    const-string v2, "MediaUploadManagerImpl"

    const-string v3, "Unable to close stream"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_4

    .line 394
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 391
    :catch_6
    move-exception v1

    goto :goto_2

    .line 389
    :catch_7
    move-exception v1

    goto :goto_1
.end method


# virtual methods
.method a(Lizu;I)Ljyc;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 235
    const-string v2, "Call to MediaUploader.upload failed"

    .line 237
    :try_start_0
    iget-object v0, p0, Ljye;->f:Ljct;

    .line 238
    invoke-virtual {p1}, Lizu;->e()Landroid/net/Uri;

    move-result-object v3

    invoke-direct {p0}, Ljye;->a()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    .line 237
    invoke-virtual {v0, v3, v4, v5}, Ljct;->a(Landroid/net/Uri;Ljava/lang/String;Z)Ljdb;

    move-result-object v3

    .line 239
    new-instance v0, Ljyc;

    invoke-virtual {v3}, Ljdb;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3}, Ljdb;->b()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v4, v3}, Ljyc;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljda; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljdc; {:try_start_0 .. :try_end_0} :catch_1

    .line 249
    :goto_0
    return-object v0

    .line 240
    :catch_0
    move-exception v0

    .line 241
    const-string v3, "MediaUploadManagerImpl"

    invoke-static {v3, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 242
    goto :goto_0

    .line 243
    :catch_1
    move-exception v0

    .line 244
    const-string v3, "MediaUploadManagerImpl"

    invoke-static {v3, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 245
    const/4 v0, 0x2

    if-lt p2, v0, :cond_0

    move-object v0, v1

    .line 246
    goto :goto_0

    .line 248
    :cond_0
    const-string v0, "Retrying upload."

    invoke-direct {p0, p1, v0}, Ljye;->a(Lizu;Ljava/lang/String;)V

    .line 249
    add-int/lit8 v0, p2, 0x1

    invoke-virtual {p0, p1, v0}, Ljye;->a(Lizu;I)Ljyc;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;JJ)V
    .locals 4

    .prologue
    .line 412
    const-string v0, "MediaUploadManagerImpl"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x43

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "  Uploading "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " upload:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " total:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 416
    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 109
    iget-object v0, p0, Ljye;->a:Landroid/content/Context;

    const-class v1, Lhee;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Ljye;->e:Lhee;

    .line 110
    new-instance v0, Ljct;

    iget-object v1, p0, Ljye;->a:Landroid/content/Context;

    iget-object v2, p0, Ljye;->e:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    invoke-direct {v0, v1, v2, p0}, Ljct;-><init>(Landroid/content/Context;ILjdg;)V

    iput-object v0, p0, Ljye;->f:Ljct;

    .line 111
    iget-object v0, p0, Ljye;->e:Lhee;

    invoke-interface {v0}, Lhee;->g()Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljye;->g:Ljava/lang/String;

    .line 112
    iget-object v0, p0, Ljye;->a:Landroid/content/Context;

    const-class v1, Lhoc;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Ljye;->b:Lhoc;

    .line 114
    if-eqz p1, :cond_1

    .line 115
    const-string v0, "PENDING_LIST"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 116
    iget-object v1, p0, Ljye;->c:Ljava/util/ArrayList;

    monitor-enter v1

    .line 117
    :try_start_0
    iget-object v2, p0, Ljye;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 118
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    const-string v0, "FINISHED_REF_LIST"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 120
    const-string v0, "FINISHED_DATA_LIST"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 122
    iget-object v3, p0, Ljye;->d:Ljava/util/Map;

    monitor-enter v3

    .line 123
    const/4 v0, 0x0

    :goto_0
    :try_start_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 124
    iget-object v4, p0, Ljye;->d:Ljava/util/Map;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-interface {v4, v5, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 123
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 118
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 126
    :cond_0
    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 127
    const-string v0, "temp_album_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljye;->h:Ljava/lang/String;

    .line 129
    :cond_1
    return-void

    .line 126
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0
.end method

.method public a(Lizu;)V
    .locals 2

    .prologue
    .line 168
    iget-object v1, p0, Ljye;->c:Ljava/util/ArrayList;

    monitor-enter v1

    .line 170
    :try_start_0
    iget-object v0, p0, Ljye;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    const-string v0, "asyncUpload skipped, image already pending."

    invoke-direct {p0, p1, v0}, Ljye;->a(Lizu;Ljava/lang/String;)V

    .line 172
    monitor-exit v1

    .line 189
    :goto_0
    return-void

    .line 174
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 175
    iget-object v1, p0, Ljye;->d:Ljava/util/Map;

    monitor-enter v1

    .line 177
    :try_start_1
    iget-object v0, p0, Ljye;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyc;

    .line 178
    if-eqz v0, :cond_1

    .line 179
    const-string v0, "asyncUpload skipped, image already uploaded."

    invoke-direct {p0, p1, v0}, Ljye;->a(Lizu;Ljava/lang/String;)V

    .line 180
    monitor-exit v1

    goto :goto_0

    .line 182
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 174
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0

    .line 182
    :cond_1
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 183
    const-string v0, "asyncUpload started."

    invoke-direct {p0, p1, v0}, Ljye;->a(Lizu;Ljava/lang/String;)V

    .line 184
    new-instance v0, Ljyf;

    invoke-direct {v0, p0, p1}, Ljyf;-><init>(Ljye;Lizu;)V

    invoke-static {v0}, Llsx;->a(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public a([Lizu;)[Ljava/lang/Long;
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 328
    array-length v0, p1

    new-array v2, v0, [Ljava/lang/Long;

    .line 329
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 330
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    move v0, v1

    .line 332
    :goto_0
    array-length v3, p1

    if-ge v0, v3, :cond_2

    .line 333
    aget-object v6, p1, v0

    .line 334
    invoke-direct {p0, v6}, Ljye;->d(Lizu;)Ljava/lang/Long;

    move-result-object v3

    .line 335
    aput-object v3, v2, v0

    .line 338
    if-eqz v6, :cond_1

    invoke-virtual {v6}, Lizu;->e()Landroid/net/Uri;

    move-result-object v7

    if-eqz v7, :cond_1

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    const-wide/16 v10, 0x0

    cmp-long v3, v8, v10

    if-nez v3, :cond_1

    const/4 v3, 0x1

    :goto_1
    if-eqz v3, :cond_0

    .line 339
    invoke-direct {p0, v6}, Ljye;->e(Lizu;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 340
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 332
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v3, v1

    .line 338
    goto :goto_1

    .line 344
    :cond_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v2

    .line 375
    :goto_2
    return-object v0

    .line 348
    :cond_3
    new-instance v3, Ljvu;

    iget-object v0, p0, Ljye;->a:Landroid/content/Context;

    iget-object v6, p0, Ljye;->e:Lhee;

    .line 349
    invoke-interface {v6}, Lhee;->d()I

    move-result v6

    iget-object v7, p0, Ljye;->g:Ljava/lang/String;

    invoke-direct {v3, v0, v6, v7, v4}, Ljvu;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;)V

    .line 350
    invoke-virtual {v3}, Ljvu;->l()V

    .line 352
    invoke-virtual {v3}, Ljvu;->t()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 353
    const-string v0, "MediaUploadManagerImpl"

    .line 354
    iget v1, v3, Lkff;->i:I

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x2f

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "CheckPhotosExistenceOperation error "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 353
    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    .line 355
    goto :goto_2

    .line 358
    :cond_4
    :goto_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 359
    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 361
    :try_start_0
    invoke-virtual {v3, v0}, Ljvu;->c(Ljava/lang/String;)J

    move-result-wide v6

    .line 362
    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 363
    aget-object v8, p1, v0

    .line 364
    iget-object v9, p0, Ljye;->d:Ljava/util/Map;

    monitor-enter v9
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    .line 365
    :try_start_1
    iget-object v10, p0, Ljye;->d:Ljava/util/Map;

    new-instance v11, Ljyc;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v8}, Lizu;->d()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v11, v12, v13}, Ljyc;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    invoke-interface {v10, v8, v11}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 366
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 367
    :try_start_2
    const-string v9, "CheckPhotosExistenceOperation found PhotoID."

    invoke-direct {p0, v8, v9}, Ljye;->a(Lizu;Ljava/lang/String;)V

    .line 368
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v2, v0
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    .line 358
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 366
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v9
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v0
    :try_end_4
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v0

    goto :goto_4

    :cond_5
    move-object v0, v2

    .line 375
    goto :goto_2
.end method

.method public b(Lizu;)Ljyc;
    .locals 4

    .prologue
    .line 200
    invoke-virtual {p0, p1}, Ljye;->a(Lizu;)V

    .line 202
    :goto_0
    iget-object v1, p0, Ljye;->d:Ljava/util/Map;

    monitor-enter v1

    .line 203
    :try_start_0
    iget-object v0, p0, Ljye;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljyc;

    .line 204
    if-eqz v0, :cond_0

    .line 205
    const-string v2, "blockingUpload finished."

    invoke-direct {p0, p1, v2}, Ljye;->a(Lizu;Ljava/lang/String;)V

    .line 206
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 212
    :goto_1
    return-object v0

    .line 209
    :cond_0
    :try_start_1
    iget-object v0, p0, Ljye;->d:Ljava/util/Map;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 214
    :try_start_2
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    .line 210
    :catch_0
    move-exception v0

    .line 211
    :try_start_3
    const-string v2, "MediaUploadManagerImpl"

    const-string v3, "Interrupted while waiting for blocking upload."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 212
    const/4 v0, 0x0

    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 132
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 133
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 135
    iget-object v3, p0, Ljye;->d:Ljava/util/Map;

    monitor-enter v3

    .line 136
    :try_start_0
    iget-object v0, p0, Ljye;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 137
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 142
    iget-object v3, p0, Ljye;->c:Ljava/util/ArrayList;

    monitor-enter v3

    .line 143
    :try_start_2
    const-string v0, "PENDING_LIST"

    iget-object v4, p0, Ljye;->c:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 144
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 145
    const-string v0, "FINISHED_REF_LIST"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 146
    const-string v0, "FINISHED_DATA_LIST"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 147
    const-string v0, "temp_album_id"

    iget-object v1, p0, Ljye;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    return-void

    .line 144
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method c(Lizu;)Ljyc;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 224
    const/4 v0, 0x1

    new-array v0, v0, [Lizu;

    aput-object p1, v0, v6

    .line 225
    invoke-virtual {p0, v0}, Ljye;->a([Lizu;)[Ljava/lang/Long;

    move-result-object v1

    .line 226
    if-eqz v1, :cond_0

    aget-object v0, v1, v6

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    .line 227
    const-string v0, "uploadInternal skipped - checkExistingPhotosOp."

    invoke-direct {p0, p1, v0}, Ljye;->a(Lizu;Ljava/lang/String;)V

    .line 228
    new-instance v0, Ljyc;

    aget-object v1, v1, v6

    invoke-virtual {p1}, Lizu;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljyc;-><init>(Ljava/lang/Long;Ljava/lang/String;)V

    .line 230
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0, p1, v6}, Ljye;->a(Lizu;I)Ljyc;

    move-result-object v0

    goto :goto_0
.end method
