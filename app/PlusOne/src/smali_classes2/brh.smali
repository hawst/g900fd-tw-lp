.class public final Lbrh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Lbri;

.field private final b:Lbrb;

.field private final c:Ljava/io/File;


# direct methods
.method public constructor <init>(Lbri;Lbrb;Ljava/io/File;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-string v0, "settings"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbri;

    iput-object v0, p0, Lbrh;->a:Lbri;

    .line 20
    const-string v0, "cacheFactory"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbrb;

    iput-object v0, p0, Lbrh;->b:Lbrb;

    .line 21
    const-string v0, "cacheFile"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, Lbrh;->c:Ljava/io/File;

    .line 22
    return-void
.end method


# virtual methods
.method public a()Lbrc;
    .locals 4

    .prologue
    .line 25
    iget-object v0, p0, Lbrh;->a:Lbri;

    iget-object v1, p0, Lbrh;->b:Lbrb;

    iget-object v2, p0, Lbrh;->c:Ljava/io/File;

    .line 26
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    .line 25
    invoke-static {v0, v1, v2, v3}, Lbrc;->a(Lbri;Lbrb;Ljava/io/File;Ljava/util/Locale;)Lbrc;

    move-result-object v0

    return-object v0
.end method
