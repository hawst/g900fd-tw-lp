.class public final Ljzm;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 253
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 254
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/libraries/social/poll/impl/PollOptionVoterListPagerActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Ljzm;->a:Landroid/content/Intent;

    .line 255
    return-void
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Ljzm;->a:Landroid/content/Intent;

    return-object v0
.end method

.method public a(I)Ljzm;
    .locals 2

    .prologue
    .line 263
    iget-object v0, p0, Ljzm;->a:Landroid/content/Intent;

    const-string v1, "card_width"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 264
    return-object p0
.end method

.method public a(J)Ljzm;
    .locals 3

    .prologue
    .line 268
    iget-object v0, p0, Ljzm;->a:Landroid/content/Intent;

    const-string v1, "poll_number_of_votes"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 269
    return-object p0
.end method

.method public a(Ljava/util/ArrayList;)Ljzm;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/os/Parcelable;",
            ">;)",
            "Ljzm;"
        }
    .end annotation

    .prologue
    .line 258
    iget-object v0, p0, Ljzm;->a:Landroid/content/Intent;

    const-string v1, "poll_option_voter_models"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 259
    return-object p0
.end method

.method public b(I)Ljzm;
    .locals 2

    .prologue
    .line 273
    iget-object v0, p0, Ljzm;->a:Landroid/content/Intent;

    const-string v1, "poll_option_voters_count"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 274
    return-object p0
.end method

.method public c(I)Ljzm;
    .locals 2

    .prologue
    .line 278
    iget-object v0, p0, Ljzm;->a:Landroid/content/Intent;

    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 279
    return-object p0
.end method
