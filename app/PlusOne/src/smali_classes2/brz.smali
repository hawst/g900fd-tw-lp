.class final Lbrz;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/util/LinkedList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/LinkedList",
            "<",
            "Lbsa;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lbrz;->a:Ljava/util/LinkedList;

    return-void
.end method

.method private a(Lbhl;Lbhr;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbhl;",
            "Lbhr",
            "<",
            "Lbhl;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 90
    if-eqz p2, :cond_0

    .line 91
    invoke-interface {p2, p1}, Lbhr;->a(Ljava/lang/Object;)V

    .line 95
    :goto_0
    return-void

    .line 93
    :cond_0
    invoke-static {p1}, Lcgl;->a(Lcgk;)V

    goto :goto_0
.end method


# virtual methods
.method public a(I)Lbhl;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lbrz;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsa;

    iget-object v0, v0, Lbsa;->a:Lbhl;

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 40
    :goto_0
    iget-object v0, p0, Lbrz;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_1

    .line 41
    iget-object v0, p0, Lbrz;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->pollLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsa;

    .line 42
    iget-object v1, v0, Lbsa;->b:Lbhr;

    if-eqz v1, :cond_0

    .line 43
    iget-object v1, v0, Lbsa;->b:Lbhr;

    iget-object v0, v0, Lbsa;->a:Lbhl;

    invoke-interface {v1, v0}, Lbhr;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 45
    :cond_0
    iget-object v0, v0, Lbsa;->a:Lbhl;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    goto :goto_0

    .line 48
    :cond_1
    return-void
.end method

.method public a(Lbhl;Lbhr;ILbmd;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lbhl;",
            "Lbhr",
            "<",
            "Lbhl;",
            ">;I",
            "Lbmd;",
            ")V"
        }
    .end annotation

    .prologue
    .line 77
    if-eqz p4, :cond_0

    iget-boolean v0, p4, Lbmd;->j:Z

    if-eqz v0, :cond_0

    .line 78
    invoke-direct {p0, p1, p2}, Lbrz;->a(Lbhl;Lbhr;)V

    .line 82
    :goto_0
    if-nez p4, :cond_1

    const/4 v0, 0x0

    move v1, v0

    .line 83
    :goto_1
    iget-object v0, p0, Lbrz;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-le v0, v1, :cond_2

    .line 84
    iget-object v0, p0, Lbrz;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsa;

    .line 85
    iget-object v2, v0, Lbsa;->a:Lbhl;

    iget-object v0, v0, Lbsa;->b:Lbhr;

    invoke-direct {p0, v2, v0}, Lbrz;->a(Lbhl;Lbhr;)V

    goto :goto_1

    .line 80
    :cond_0
    iget-object v0, p0, Lbrz;->a:Ljava/util/LinkedList;

    new-instance v1, Lbsa;

    invoke-direct {v1, p1, p2, p3}, Lbsa;-><init>(Lbhl;Lbhr;I)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    goto :goto_0

    .line 82
    :cond_1
    iget v0, p4, Lbmd;->i:I

    move v1, v0

    goto :goto_1

    .line 87
    :cond_2
    return-void
.end method

.method public b()I
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lbrz;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    return v0
.end method

.method public b(I)I
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lbrz;->a:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbsa;

    iget v0, v0, Lbsa;->c:I

    return v0
.end method
