.class public final Lbin;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbih;
.implements Lbij;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbih;",
        "Lbij",
        "<",
        "Lboo;",
        ">;"
    }
.end annotation


# static fields
.field private static final a:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final b:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final c:Lllt;

.field private final d:Lbir;

.field private final e:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lbil;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final g:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lbio;",
            ">;"
        }
    .end annotation
.end field

.field private h:I

.field private i:J

.field private j:J

.field private k:Z

.field private l:Lbio;

.field private m:Lbip;

.field private n:I

.field private o:Z


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 44
    const-class v0, Lbin;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    .line 53
    const/16 v0, 0xf

    new-array v0, v0, [I

    sget v1, Lbik;->b:I

    aput v1, v0, v3

    sget v1, Lbik;->e:I

    aput v1, v0, v4

    sget v1, Lbik;->f:I

    aput v1, v0, v5

    sget v1, Lbik;->h:I

    aput v1, v0, v6

    sget v1, Lbik;->i:I

    aput v1, v0, v7

    const/4 v1, 0x5

    sget v2, Lbik;->k:I

    aput v2, v0, v1

    const/4 v1, 0x6

    sget v2, Lbik;->l:I

    aput v2, v0, v1

    const/4 v1, 0x7

    sget v2, Lbik;->m:I

    aput v2, v0, v1

    const/16 v1, 0x8

    sget v2, Lbik;->o:I

    aput v2, v0, v1

    const/16 v1, 0x9

    sget v2, Lbik;->p:I

    aput v2, v0, v1

    const/16 v1, 0xa

    sget v2, Lbik;->q:I

    aput v2, v0, v1

    const/16 v1, 0xb

    sget v2, Lbik;->r:I

    aput v2, v0, v1

    const/16 v1, 0xc

    sget v2, Lbik;->s:I

    aput v2, v0, v1

    const/16 v1, 0xd

    sget v2, Lbik;->t:I

    aput v2, v0, v1

    const/16 v1, 0xe

    sget v2, Lbik;->u:I

    aput v2, v0, v1

    invoke-static {v0}, Lbin;->a([I)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lbin;->a:Ljava/util/Set;

    .line 60
    const/4 v0, 0x5

    new-array v0, v0, [I

    sget v1, Lbik;->a:I

    aput v1, v0, v3

    sget v1, Lbik;->c:I

    aput v1, v0, v4

    sget v1, Lbik;->d:I

    aput v1, v0, v5

    sget v1, Lbik;->g:I

    aput v1, v0, v6

    sget v1, Lbik;->j:I

    aput v1, v0, v7

    invoke-static {v0}, Lbin;->a([I)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lbin;->b:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lllt;)V
    .locals 2

    .prologue
    const-wide/16 v0, -0x1

    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    iput-wide v0, p0, Lbin;->i:J

    .line 74
    iput-wide v0, p0, Lbin;->j:J

    .line 86
    const-string v0, "byteRangeFetcher"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lllt;

    iput-object v0, p0, Lbin;->c:Lllt;

    .line 88
    new-instance v0, Lbir;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lbir;-><init>(I)V

    iput-object v0, p0, Lbin;->d:Lbir;

    .line 89
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lbin;->e:Ljava/util/Stack;

    .line 90
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lbin;->f:Ljava/util/Stack;

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lbin;->g:Ljava/util/List;

    .line 92
    return-void
.end method

.method private static varargs a([I)Ljava/util/Set;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([I)",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 442
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 443
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, p0, v0

    .line 444
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 443
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 446
    :cond_0
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method private a(Lbir;)V
    .locals 6

    .prologue
    .line 419
    invoke-virtual {p1}, Lbir;->b()I

    move-result v0

    invoke-virtual {p1}, Lbir;->a()[B

    move-result-object v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    iget-object v2, p0, Lbin;->c:Lllt;

    iget v3, p0, Lbin;->h:I

    int-to-long v4, v3

    invoke-interface {v2, v4, v5, v1}, Lllt;->a(JLjava/nio/ByteBuffer;)I

    move-result v1

    if-eq v1, v0, :cond_0

    new-instance v1, Ljava/io/IOException;

    iget v2, p0, Lbin;->h:I

    iget-object v3, p0, Lbin;->c:Lllt;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/lit8 v5, v5, 0x58

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "Unable to read requested number of bytes "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " at offset "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " with fetcher "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    invoke-direct {p0, v0}, Lbin;->d(I)V

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lbir;->a(I)V

    .line 420
    return-void
.end method

.method private d(I)V
    .locals 1

    .prologue
    .line 437
    iget v0, p0, Lbin;->h:I

    add-int/2addr v0, p1

    iput v0, p0, Lbin;->h:I

    .line 438
    return-void
.end method

.method private k()V
    .locals 8

    .prologue
    .line 303
    :cond_0
    iget-boolean v0, p0, Lbin;->k:Z

    if-nez v0, :cond_10

    .line 304
    iget-object v0, p0, Lbin;->d:Lbir;

    invoke-direct {p0, v0}, Lbin;->a(Lbir;)V

    iget-object v0, p0, Lbin;->d:Lbir;

    invoke-virtual {v0}, Lbir;->f()I

    move-result v0

    iget-object v1, p0, Lbin;->d:Lbir;

    invoke-virtual {v1}, Lbir;->f()I

    move-result v1

    sget v2, Lbik;->v:I

    if-ne v1, v2, :cond_3

    const/4 v0, 0x0

    :goto_0
    if-nez v0, :cond_1

    .line 305
    const/4 v0, 0x1

    iput-boolean v0, p0, Lbin;->k:Z

    .line 306
    iget-object v0, p0, Lbin;->c:Lllt;

    invoke-interface {v0}, Lllt;->a()V

    .line 310
    :cond_1
    :goto_1
    iget-object v0, p0, Lbin;->f:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbin;->f:Ljava/util/Stack;

    .line 311
    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v1, p0, Lbin;->h:I

    if-ne v0, v1, :cond_0

    .line 312
    iget-object v0, p0, Lbin;->f:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 313
    iget-object v0, p0, Lbin;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lbil;

    .line 314
    iget v0, v1, Lbil;->a:I

    sget v2, Lbik;->a:I

    if-ne v0, v2, :cond_f

    .line 315
    iget v0, v1, Lbil;->a:I

    const-string v2, "moov.type"

    sget v3, Lbik;->a:I

    const/4 v4, 0x0

    invoke-static {v0, v2, v3, v4}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    iget-object v0, p0, Lbin;->g:Ljava/util/List;

    const-string v2, "mTracks"

    invoke-static {v0, v2}, Lcgp;->a(Ljava/util/Collection;Ljava/lang/CharSequence;)Ljava/util/Collection;

    sget v0, Lbik;->c:I

    invoke-virtual {v1, v0}, Lbil;->b(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbil;

    const-string v2, "trakAtom"

    const/4 v4, 0x0

    invoke-static {v0, v2, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    sget v2, Lbik;->d:I

    invoke-virtual {v0, v2}, Lbil;->c(I)Lbil;

    move-result-object v0

    sget v2, Lbik;->f:I

    invoke-virtual {v0, v2}, Lbil;->a(I)Lbim;

    move-result-object v2

    iget-object v2, v2, Lbim;->b:Lbir;

    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Lbir;->a(I)V

    invoke-virtual {v2}, Lbir;->f()I

    move-result v2

    const v4, 0x736f756e

    if-eq v2, v4, :cond_7

    const v4, 0x76696465

    if-eq v2, v4, :cond_7

    const/4 v0, 0x0

    :goto_3
    if-eqz v0, :cond_2

    iget-object v2, p0, Lbin;->g:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 304
    :cond_3
    add-int/lit8 v0, v0, -0x8

    sget-object v2, Lbin;->b:Ljava/util/Set;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lbin;->e:Ljava/util/Stack;

    new-instance v3, Lbil;

    invoke-direct {v3, v1}, Lbil;-><init>(I)V

    invoke-virtual {v2, v3}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lbin;->f:Ljava/util/Stack;

    iget v2, p0, Lbin;->h:I

    add-int/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_4
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_5
    sget-object v2, Lbin;->a:Ljava/util/Set;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    new-instance v2, Lbir;

    invoke-direct {v2, v0}, Lbir;-><init>(I)V

    invoke-direct {p0, v2}, Lbin;->a(Lbir;)V

    iget-object v0, p0, Lbin;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lbin;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbil;

    new-instance v3, Lbim;

    invoke-direct {v3, v1, v2}, Lbim;-><init>(ILbir;)V

    invoke-virtual {v0, v3}, Lbil;->a(Lbim;)V

    goto :goto_4

    :cond_6
    invoke-direct {p0, v0}, Lbin;->d(I)V

    goto :goto_4

    .line 315
    :cond_7
    sget v2, Lbik;->g:I

    invoke-virtual {v0, v2}, Lbil;->c(I)Lbil;

    move-result-object v2

    sget v4, Lbik;->j:I

    invoke-virtual {v2, v4}, Lbil;->c(I)Lbil;

    move-result-object v4

    new-instance v2, Lbio;

    sget v5, Lbik;->e:I

    invoke-virtual {v0, v5}, Lbil;->a(I)Lbim;

    move-result-object v0

    iget-object v0, v0, Lbim;->b:Lbir;

    invoke-static {v0}, Lbiq;->a(Lbir;)I

    move-result v5

    if-nez v5, :cond_8

    const/16 v5, 0x8

    invoke-virtual {v0, v5}, Lbir;->b(I)V

    invoke-virtual {v0}, Lbir;->f()I

    move-result v0

    :goto_5
    int-to-long v6, v0

    sget v0, Lbik;->k:I

    invoke-virtual {v4, v0}, Lbil;->a(I)Lbim;

    move-result-object v0

    iget-object v0, v0, Lbim;->b:Lbir;

    invoke-static {v0}, Lbio;->a(Lbir;)Landroid/media/MediaFormat;

    move-result-object v0

    invoke-direct {v2, v6, v7, v4, v0}, Lbio;-><init>(JLbil;Landroid/media/MediaFormat;)V

    move-object v0, v2

    goto/16 :goto_3

    :cond_8
    const/4 v6, 0x1

    if-ne v5, v6, :cond_9

    const/16 v5, 0x10

    invoke-virtual {v0, v5}, Lbir;->b(I)V

    invoke-virtual {v0}, Lbir;->f()I

    move-result v0

    goto :goto_5

    :cond_9
    const-string v0, "Unexpected version for mdhd box"

    invoke-static {v0}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v0

    throw v0

    :cond_a
    sget v0, Lbik;->b:I

    invoke-virtual {v1, v0}, Lbil;->a(I)Lbim;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, v0, Lbim;->b:Lbir;

    invoke-static {v0}, Lbiq;->a(Lbir;)I

    move-result v1

    if-nez v1, :cond_c

    invoke-virtual {v0}, Lbir;->f()I

    move-result v1

    int-to-long v4, v1

    invoke-virtual {v0}, Lbir;->f()I

    invoke-virtual {v0}, Lbir;->f()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0}, Lbir;->f()I

    move-result v0

    int-to-long v0, v0

    :goto_6
    const-wide/32 v6, 0xf4240

    mul-long/2addr v0, v6

    div-long/2addr v0, v2

    iput-wide v0, p0, Lbin;->i:J

    const-wide/32 v0, 0x7c25b080

    cmp-long v0, v4, v0

    if-ltz v0, :cond_b

    const-wide/32 v0, 0x7c25b080

    sub-long/2addr v4, v0

    :cond_b
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-lez v0, :cond_e

    const-wide/16 v0, 0x3e8

    mul-long/2addr v0, v4

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    :goto_7
    iput-wide v0, p0, Lbin;->j:J

    goto/16 :goto_1

    :cond_c
    const/4 v2, 0x1

    if-ne v1, v2, :cond_d

    invoke-virtual {v0}, Lbir;->g()J

    move-result-wide v4

    invoke-virtual {v0}, Lbir;->g()J

    invoke-virtual {v0}, Lbir;->g()J

    move-result-wide v2

    invoke-virtual {v0}, Lbir;->g()J

    move-result-wide v0

    goto :goto_6

    :cond_d
    const-string v0, "Unexpected version for mvhd box"

    invoke-static {v0}, Lcgp;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0

    :cond_e
    const-wide/16 v0, -0x1

    goto :goto_7

    .line 316
    :cond_f
    iget-object v0, p0, Lbin;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 317
    iget-object v0, p0, Lbin;->e:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbil;

    invoke-virtual {v0, v1}, Lbil;->a(Lbil;)V

    goto/16 :goto_1

    .line 321
    :cond_10
    return-void
.end method


# virtual methods
.method public a(Ljava/nio/ByteBuffer;I)I
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 179
    iget-object v0, p0, Lbin;->l:Lbio;

    const-string v1, "mSelectedTrack"

    invoke-static {v0, v1, v3}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 180
    iget v0, p0, Lbin;->n:I

    const-string v1, "mSampleIndex"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2, v3}, Lcgp;->b(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)I

    .line 181
    iget-object v0, p0, Lbin;->m:Lbip;

    iget v1, p0, Lbin;->n:I

    invoke-virtual {v0, v1}, Lbip;->a(I)J

    move-result-wide v0

    .line 182
    iget-object v2, p0, Lbin;->m:Lbip;

    iget v3, p0, Lbin;->n:I

    invoke-virtual {v2, v3}, Lbip;->b(I)I

    move-result v2

    .line 183
    add-int v3, p2, v2

    invoke-virtual {p1, v3}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 184
    invoke-virtual {p1, p2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 185
    iget-object v3, p0, Lbin;->c:Lllt;

    invoke-interface {v3, v0, v1, p1}, Lllt;->a(JLjava/nio/ByteBuffer;)I

    move-result v3

    .line 186
    if-eq v3, v2, :cond_0

    .line 187
    new-instance v4, Ljava/io/IOException;

    new-instance v5, Ljava/lang/StringBuilder;

    const/16 v6, 0x53

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Unable to read "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " bytes at "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ": actually read "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 190
    :cond_0
    iget-boolean v0, p0, Lbin;->o:Z

    if-eqz v0, :cond_1

    .line 191
    invoke-static {p1, v2}, Lbiq;->a(Ljava/nio/ByteBuffer;I)V

    .line 195
    :cond_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 196
    invoke-virtual {p1, p2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 197
    return v2
.end method

.method public a(I)Landroid/media/MediaFormat;
    .locals 2

    .prologue
    .line 172
    invoke-direct {p0}, Lbin;->k()V

    .line 173
    const-string v0, "index"

    iget-object v1, p0, Lbin;->g:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lcec;->a(ILjava/lang/CharSequence;Ljava/util/Collection;)I

    .line 174
    iget-object v0, p0, Lbin;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbio;

    iget-object v0, v0, Lbio;->c:Landroid/media/MediaFormat;

    return-object v0
.end method

.method public a()V
    .locals 2

    .prologue
    .line 278
    :try_start_0
    iget-object v0, p0, Lbin;->c:Lllt;

    invoke-interface {v0}, Lllt;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 281
    return-void

    .line 279
    :catch_0
    move-exception v0

    .line 280
    const-string v1, "Error releasing MP4 extractor."

    invoke-static {v1, v0}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/Throwable;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
.end method

.method public a(J)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v6, -0x1

    .line 243
    iget-object v0, p0, Lbin;->l:Lbio;

    const-string v1, "mSelectedTrack"

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 244
    iget v0, p0, Lbin;->n:I

    const-string v1, "mSampleIndex"

    invoke-static {v0, v1, v6, v2}, Lcgp;->b(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)I

    .line 246
    invoke-virtual {p0}, Lbin;->d()J

    move-result-wide v0

    sub-long v0, p1, v0

    const-string v2, "timestampUs - getSampleTime()"

    .line 245
    invoke-static {v0, v1, v2}, Lcec;->b(JLjava/lang/CharSequence;)J

    .line 247
    iget v1, p0, Lbin;->n:I

    .line 250
    iget-object v0, p0, Lbin;->l:Lbio;

    iget-object v0, v0, Lbio;->c:Landroid/media/MediaFormat;

    const-string v2, "max-input-size"

    invoke-virtual {v0, v2}, Landroid/media/MediaFormat;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lbin;->l:Lbio;

    iget-object v0, v0, Lbio;->c:Landroid/media/MediaFormat;

    const-string v2, "max-input-size"

    invoke-virtual {v0, v2}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    .line 255
    :goto_0
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 256
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    .line 257
    :goto_1
    iget v2, p0, Lbin;->n:I

    if-eq v2, v6, :cond_1

    invoke-virtual {p0}, Lbin;->d()J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-gez v2, :cond_1

    .line 258
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 259
    iget-object v2, p0, Lbin;->m:Lbip;

    iget v3, p0, Lbin;->n:I

    invoke-virtual {v2, v3}, Lbip;->b(I)I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 260
    iget-object v2, p0, Lbin;->c:Lllt;

    iget-object v3, p0, Lbin;->m:Lbip;

    iget v4, p0, Lbin;->n:I

    .line 261
    invoke-virtual {v3, v4}, Lbip;->a(I)J

    move-result-wide v4

    .line 260
    invoke-interface {v2, v4, v5, v0}, Lllt;->a(JLjava/nio/ByteBuffer;)I

    .line 262
    invoke-virtual {p0}, Lbin;->b()Z

    goto :goto_1

    .line 253
    :cond_0
    const/16 v0, 0x2000

    goto :goto_0

    .line 264
    :cond_1
    iput v1, p0, Lbin;->n:I

    .line 272
    iget-object v0, p0, Lbin;->c:Lllt;

    invoke-interface {v0}, Lllt;->a()V

    .line 273
    return-void
.end method

.method public a(JI)V
    .locals 3

    .prologue
    .line 202
    iget-object v0, p0, Lbin;->l:Lbio;

    const-string v1, "mSelectedTrack"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 203
    const/4 v0, 0x2

    if-ne p3, v0, :cond_0

    .line 204
    iget-object v0, p0, Lbin;->m:Lbip;

    invoke-virtual {v0, p1, p2}, Lbip;->c(J)I

    move-result v0

    iput v0, p0, Lbin;->n:I

    .line 209
    :goto_0
    return-void

    .line 205
    :cond_0
    if-nez p3, :cond_1

    .line 209
    iget-object v0, p0, Lbin;->m:Lbip;

    invoke-virtual {v0, p1, p2}, Lbip;->a(J)I

    move-result v0

    iput v0, p0, Lbin;->n:I

    goto :goto_0

    .line 211
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public b(I)V
    .locals 3

    .prologue
    .line 217
    iget-object v0, p0, Lbin;->l:Lbio;

    const-string v1, "mSelectedTrack"

    const-string v2, "This implementation allows only one track to be selected"

    invoke-static {v0, v1, v2}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 219
    invoke-direct {p0}, Lbin;->k()V

    .line 221
    const-string v0, "index"

    iget-object v1, p0, Lbin;->g:Ljava/util/List;

    invoke-static {p1, v0, v1}, Lcec;->a(ILjava/lang/CharSequence;Ljava/util/Collection;)I

    .line 222
    iget-object v0, p0, Lbin;->g:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbio;

    iput-object v0, p0, Lbin;->l:Lbio;

    .line 223
    iget-object v0, p0, Lbin;->l:Lbio;

    invoke-static {v0}, Lbip;->a(Lbio;)Lbip;

    move-result-object v0

    iput-object v0, p0, Lbin;->m:Lbip;

    .line 224
    iget-object v0, p0, Lbin;->m:Lbip;

    invoke-virtual {v0}, Lbip;->a()I

    move-result v0

    if-lez v0, :cond_0

    .line 225
    const/4 v0, 0x0

    iput v0, p0, Lbin;->n:I

    .line 230
    :goto_0
    iget-object v0, p0, Lbin;->l:Lbio;

    iget-object v0, v0, Lbio;->c:Landroid/media/MediaFormat;

    const-string v1, "mime"

    invoke-virtual {v0, v1}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "video/avc"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lbin;->o:Z

    .line 231
    return-void

    .line 227
    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lbin;->n:I

    goto :goto_0
.end method

.method public b()Z
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 124
    iget-object v0, p0, Lbin;->l:Lbio;

    const-string v1, "mSelectedTrack"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 125
    iget v0, p0, Lbin;->n:I

    const-string v1, "mSampleIndex"

    const-string v2, "advance() requires a valid current sample."

    invoke-static {v0, v1, v3, v2}, Lcgp;->b(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)I

    .line 127
    iget v0, p0, Lbin;->n:I

    iget-object v1, p0, Lbin;->m:Lbip;

    invoke-virtual {v1}, Lbip;->a()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ne v0, v1, :cond_0

    .line 128
    iput v3, p0, Lbin;->n:I

    .line 129
    const/4 v0, 0x0

    .line 133
    :goto_0
    return v0

    .line 132
    :cond_0
    iget v0, p0, Lbin;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lbin;->n:I

    .line 133
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 138
    iget-object v0, p0, Lbin;->l:Lbio;

    const-string v1, "mSelectedTrack"

    invoke-static {v0, v1, v3}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 139
    iget v0, p0, Lbin;->n:I

    const-string v1, "mSampleIndex"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2, v3}, Lcgp;->b(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)I

    .line 140
    iget-object v0, p0, Lbin;->m:Lbip;

    iget v1, p0, Lbin;->n:I

    invoke-virtual {v0, v1}, Lbip;->d(I)I

    move-result v0

    return v0
.end method

.method public c(I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 235
    const-string v0, "index"

    iget-object v1, p0, Lbin;->g:Ljava/util/List;

    iget-object v2, p0, Lbin;->l:Lbio;

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-static {p1, v0, v1, v3}, Lcec;->a(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)V

    .line 236
    iput-object v3, p0, Lbin;->l:Lbio;

    .line 237
    iput-object v3, p0, Lbin;->m:Lbip;

    .line 238
    const/4 v0, -0x1

    iput v0, p0, Lbin;->n:I

    .line 239
    return-void
.end method

.method public d()J
    .locals 3

    .prologue
    .line 145
    iget-object v0, p0, Lbin;->l:Lbio;

    const-string v1, "mSelectedTrack"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 146
    iget v0, p0, Lbin;->n:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 147
    iget-object v0, p0, Lbin;->m:Lbip;

    iget v1, p0, Lbin;->n:I

    invoke-virtual {v0, v1}, Lbip;->c(I)J

    move-result-wide v0

    .line 149
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public e()I
    .locals 4

    .prologue
    const/4 v0, -0x1

    .line 155
    iget-object v1, p0, Lbin;->l:Lbio;

    const-string v2, "mSelectedTrack"

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 156
    iget v1, p0, Lbin;->n:I

    if-ne v1, v0, :cond_0

    .line 161
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lbin;->g:Ljava/util/List;

    iget-object v1, p0, Lbin;->l:Lbio;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 166
    invoke-direct {p0}, Lbin;->k()V

    .line 167
    iget-object v0, p0, Lbin;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 286
    iget-object v0, p0, Lbin;->l:Lbio;

    const-string v1, "mSelectedTrack"

    invoke-static {v0, v1, v3}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 287
    iget v0, p0, Lbin;->n:I

    const-string v1, "mSampleIndex"

    const/4 v2, -0x1

    invoke-static {v0, v1, v2, v3}, Lcgp;->b(ILjava/lang/CharSequence;ILjava/lang/CharSequence;)I

    .line 288
    iget-object v0, p0, Lbin;->m:Lbip;

    iget v1, p0, Lbin;->n:I

    invoke-virtual {v0, v1}, Lbip;->a(I)J

    move-result-wide v0

    .line 289
    iget-object v2, p0, Lbin;->m:Lbip;

    iget v3, p0, Lbin;->n:I

    invoke-virtual {v2, v3}, Lbip;->b(I)I

    move-result v2

    .line 290
    iget-object v3, p0, Lbin;->c:Lllt;

    int-to-long v4, v2

    invoke-interface {v3, v0, v1, v4, v5}, Lllt;->a(JJ)Z

    move-result v0

    return v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lbin;->c:Lllt;

    invoke-interface {v0}, Lllt;->a()V

    .line 296
    return-void
.end method

.method public synthetic i()Lbmu;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Lbin;->j()Lboo;

    move-result-object v0

    return-object v0
.end method

.method public j()Lboo;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 97
    invoke-direct {p0}, Lbin;->k()V

    .line 99
    const/4 v7, 0x0

    .line 101
    iget-object v0, p0, Lbin;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-object v6, v1

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbio;

    .line 102
    iget-object v2, v0, Lbio;->c:Landroid/media/MediaFormat;

    .line 103
    iget-object v4, v0, Lbio;->c:Landroid/media/MediaFormat;

    invoke-static {v4}, Lbkf;->b(Landroid/media/MediaFormat;)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object v6, v2

    .line 104
    goto :goto_0

    .line 105
    :cond_1
    iget-object v0, v0, Lbio;->c:Landroid/media/MediaFormat;

    invoke-static {v0}, Lbkf;->a(Landroid/media/MediaFormat;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 106
    const/4 v7, 0x1

    goto :goto_0

    .line 111
    :cond_2
    const-string v0, "videoFormat"

    invoke-static {v6, v0, v1}, Lcgp;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 113
    new-instance v1, Lboo;

    iget-wide v2, p0, Lbin;->j:J

    const/4 v4, -0x1

    const-string v0, "width"

    .line 116
    invoke-virtual {v6, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v5

    const-string v0, "height"

    .line 117
    invoke-virtual {v6, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v6

    iget-wide v8, p0, Lbin;->i:J

    invoke-direct/range {v1 .. v9}, Lboo;-><init>(JIIIZJ)V

    return-object v1
.end method
