.class public final Lade;
.super Lado;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field private final c:I

.field private final d:I

.field private e:J

.field private f:Z

.field private g:Ljava/nio/ByteBuffer;

.field private h:Ljava/nio/ByteBuffer;

.field private i:Ladf;


# direct methods
.method protected constructor <init>(ILandroid/media/MediaFormat;Ladm;)V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Lado;-><init>(ILandroid/media/MediaFormat;Ladm;)V

    .line 46
    const-string v0, "width"

    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lade;->c:I

    .line 47
    const-string v0, "height"

    invoke-virtual {p2, v0}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lade;->d:I

    .line 48
    return-void
.end method

.method private static a([I)Landroid/util/SparseIntArray;
    .locals 3

    .prologue
    .line 215
    new-instance v1, Landroid/util/SparseIntArray;

    invoke-direct {v1}, Landroid/util/SparseIntArray;-><init>()V

    .line 216
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 217
    aget v2, p0, v0

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseIntArray;->append(II)V

    .line 216
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 219
    :cond_0
    return-object v1
.end method


# virtual methods
.method protected a(Landroid/media/MediaFormat;)Landroid/media/MediaCodec;
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 53
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    new-instance v4, Ljava/util/TreeMap;

    invoke-direct {v4}, Ljava/util/TreeMap;-><init>()V

    invoke-static {v0}, Lade;->a([I)Landroid/util/SparseIntArray;

    move-result-object v5

    move v0, v1

    :goto_0
    invoke-static {}, Landroid/media/MediaCodecList;->getCodecCount()I

    move-result v3

    if-ge v0, v3, :cond_2

    invoke-static {v0}, Landroid/media/MediaCodecList;->getCodecInfoAt(I)Landroid/media/MediaCodecInfo;

    move-result-object v6

    invoke-virtual {v6}, Landroid/media/MediaCodecInfo;->isEncoder()Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "mime"

    invoke-virtual {p1, v3}, Landroid/media/MediaFormat;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v6}, Landroid/media/MediaCodecInfo;->getSupportedTypes()[Ljava/lang/String;

    move-result-object v7

    new-instance v8, Ljava/util/HashSet;

    invoke-static {v7}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v7

    invoke-direct {v8, v7}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v8, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v6, v3}, Landroid/media/MediaCodecInfo;->getCapabilitiesForType(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;

    move-result-object v3

    iget-object v7, v3, Landroid/media/MediaCodecInfo$CodecCapabilities;->colorFormats:[I

    array-length v8, v7

    move v3, v1

    :goto_1
    if-ge v3, v8, :cond_1

    aget v9, v7, v3

    invoke-virtual {v5, v9}, Landroid/util/SparseIntArray;->indexOfKey(I)I

    move-result v10

    if-ltz v10, :cond_0

    invoke-virtual {v5, v9}, Landroid/util/SparseIntArray;->get(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v6}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v4, v9, v10}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-virtual {v4}, Ljava/util/TreeMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v2

    .line 56
    :goto_2
    if-nez v0, :cond_4

    .line 57
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find a suitable decoder for format: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 53
    :cond_3
    invoke-virtual {v4}, Ljava/util/TreeMap;->firstEntry()Ljava/util/Map$Entry;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/media/MediaCodec;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v0

    goto :goto_2

    .line 60
    :cond_4
    invoke-virtual {v0, p1, v2, v2, v1}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 61
    return-object v0

    .line 53
    nop

    :array_0
    .array-data 4
        0x10
        0x13
    .end array-data
.end method

.method protected a(Laas;Labg;I)V
    .locals 10

    .prologue
    const-wide/16 v8, 0x3e8

    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v6, -0x1

    const/4 v2, 0x1

    .line 87
    iget v1, p0, Lade;->c:I

    .line 88
    iget v0, p0, Lade;->d:I

    .line 89
    invoke-static {p3}, Lade;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 90
    iget v1, p0, Lade;->d:I

    .line 91
    iget v0, p0, Lade;->c:I

    .line 95
    :cond_0
    new-array v3, v5, [I

    aput v1, v3, v4

    aput v0, v3, v2

    invoke-virtual {p1, v3}, Laas;->a([I)V

    .line 96
    iget-wide v0, p0, Lade;->e:J

    mul-long/2addr v0, v8

    invoke-virtual {p1, v0, v1}, Laas;->a(J)V

    .line 97
    invoke-virtual {p1, v5}, Laas;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 100
    iget-object v0, p0, Lade;->i:Ladf;

    if-nez v0, :cond_1

    .line 101
    new-instance v0, Ladf;

    invoke-virtual {p0}, Lade;->f()Landroid/media/MediaCodec;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/MediaCodec;->getOutputFormat()Landroid/media/MediaFormat;

    move-result-object v3

    invoke-direct {v0, v3}, Ladf;-><init>(Landroid/media/MediaFormat;)V

    iput-object v0, p0, Lade;->i:Ladf;

    .line 103
    :cond_1
    if-nez p3, :cond_3

    .line 104
    iget-object v0, p0, Lade;->i:Ladf;

    iget-object v2, p0, Lade;->g:Ljava/nio/ByteBuffer;

    invoke-static {v0, v2, v1}, Ladf;->a(Ladf;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V

    .line 113
    :goto_0
    invoke-virtual {p1}, Laas;->h()V

    .line 116
    if-eqz p2, :cond_2

    .line 117
    new-instance v0, Ladn;

    iget-boolean v1, p0, Lade;->f:Z

    invoke-direct {v0, v1}, Ladn;-><init>(Z)V

    invoke-virtual {p2, v0}, Labg;->a(Ljava/lang/Object;)V

    .line 118
    iget-wide v0, p0, Lade;->e:J

    mul-long/2addr v0, v8

    invoke-virtual {p2, v0, v1}, Labg;->a(J)V

    .line 120
    :cond_2
    return-void

    .line 106
    :cond_3
    iget-object v0, p0, Lade;->h:Ljava/nio/ByteBuffer;

    if-nez v0, :cond_4

    .line 107
    iget v0, p0, Lade;->c:I

    iget v3, p0, Lade;->d:I

    mul-int/2addr v0, v3

    shl-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lade;->h:Ljava/nio/ByteBuffer;

    .line 110
    :cond_4
    iget-object v0, p0, Lade;->i:Ladf;

    iget-object v3, p0, Lade;->g:Ljava/nio/ByteBuffer;

    iget-object v5, p0, Lade;->h:Ljava/nio/ByteBuffer;

    invoke-static {v0, v3, v5}, Ladf;->a(Ladf;Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;)V

    .line 111
    iget-object v0, p0, Lade;->h:Ljava/nio/ByteBuffer;

    sparse-switch p3, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported rotation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    iget v6, p0, Lade;->c:I

    move v5, v2

    :goto_1
    iget v2, p0, Lade;->c:I

    iget v3, p0, Lade;->d:I

    invoke-static/range {v0 .. v6}, Landroidx/media/filterfw/PixelUtils;->a(Ljava/nio/ByteBuffer;Ljava/nio/ByteBuffer;IIIII)V

    goto :goto_0

    :sswitch_1
    iget v3, p0, Lade;->c:I

    add-int/lit8 v3, v3, -0x1

    iget v4, p0, Lade;->d:I

    mul-int/2addr v4, v3

    iget v3, p0, Lade;->d:I

    neg-int v5, v3

    move v6, v2

    goto :goto_1

    :sswitch_2
    iget v2, p0, Lade;->d:I

    add-int/lit8 v4, v2, -0x1

    iget v5, p0, Lade;->d:I

    goto :goto_1

    :sswitch_3
    iget v2, p0, Lade;->c:I

    iget v3, p0, Lade;->d:I

    mul-int/2addr v2, v3

    add-int/lit8 v4, v2, -0x1

    iget v2, p0, Lade;->c:I

    neg-int v2, v2

    move v5, v6

    move v6, v2

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5a -> :sswitch_2
        0xb4 -> :sswitch_3
        0x10e -> :sswitch_1
    .end sparse-switch
.end method

.method protected a(Landroid/media/MediaCodec;[Ljava/nio/ByteBuffer;ILandroid/media/MediaCodec$BufferInfo;Z)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 69
    iget-wide v0, p4, Landroid/media/MediaCodec$BufferInfo;->presentationTimeUs:J

    iput-wide v0, p0, Lade;->e:J

    .line 70
    iput-boolean p5, p0, Lade;->f:Z

    .line 71
    aget-object v0, p2, p3

    iput-object v0, p0, Lade;->g:Ljava/nio/ByteBuffer;

    .line 72
    invoke-virtual {p0}, Lade;->h()V

    .line 73
    invoke-virtual {p0}, Lade;->g()V

    .line 76
    invoke-virtual {p0}, Lade;->a()Z

    .line 78
    invoke-virtual {p1, p3, v2}, Landroid/media/MediaCodec;->releaseOutputBuffer(IZ)V

    .line 80
    return v2
.end method
