.class public final Lgsx;
.super Ljava/lang/Object;


# instance fields
.field private final a:Lgtt;


# direct methods
.method constructor <init>(Lgtt;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lgsx;->a:Lgtt;

    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/maps/model/LatLng;)Landroid/graphics/Point;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lgsx;->a:Lgtt;

    invoke-interface {v0, p1}, Lgtt;->a(Lcom/google/android/gms/maps/model/LatLng;)Lgjb;

    move-result-object v0

    invoke-static {v0}, Lgje;->a(Lgjb;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Point;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public a(Landroid/graphics/Point;)Lcom/google/android/gms/maps/model/LatLng;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lgsx;->a:Lgtt;

    invoke-static {p1}, Lgje;->a(Ljava/lang/Object;)Lgjb;

    move-result-object v1

    invoke-interface {v0, v1}, Lgtt;->a(Lgjb;)Lcom/google/android/gms/maps/model/LatLng;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public a()Lcom/google/android/gms/maps/model/VisibleRegion;
    .locals 2

    :try_start_0
    iget-object v0, p0, Lgsx;->a:Lgtt;

    invoke-interface {v0}, Lgtt;->a()Lcom/google/android/gms/maps/model/VisibleRegion;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    :catch_0
    move-exception v0

    new-instance v1, Lgwu;

    invoke-direct {v1, v0}, Lgwu;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
