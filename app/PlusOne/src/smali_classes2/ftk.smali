.class public final Lftk;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private d:Lkdv;

.field private e:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lftl;",
            ">;"
        }
    .end annotation
.end field

.field private f:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private g:Landroid/graphics/Canvas;

.field private h:[Lhng;

.field private synthetic i:Lcom/google/android/apps/plus/stories/views/StoryLayout;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/stories/views/StoryLayout;Landroid/content/Context;III)V
    .locals 1

    .prologue
    .line 1081
    iput-object p1, p0, Lftk;->i:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1082
    const-class v0, Lkdv;

    invoke-static {p2, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    iput-object v0, p0, Lftk;->d:Lkdv;

    .line 1083
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lftk;->e:Ljava/util/List;

    .line 1084
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lftk;->f:Ljava/util/List;

    .line 1085
    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0}, Landroid/graphics/Canvas;-><init>()V

    iput-object v0, p0, Lftk;->g:Landroid/graphics/Canvas;

    .line 1086
    iput p3, p0, Lftk;->b:I

    iput p3, p0, Lftk;->c:I

    .line 1092
    div-int/lit8 v0, p4, 0x4

    invoke-static {v0, p5}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lftk;->a:I

    .line 1093
    invoke-direct {p0, p4}, Lftk;->a(I)V

    .line 1094
    return-void
.end method

.method private a([Lhng;F)F
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1213
    const/4 v0, 0x0

    .line 1214
    array-length v2, p1

    .line 1215
    if-lez v2, :cond_0

    .line 1216
    aget-object v3, p1, v1

    iget v3, v3, Lhng;->a:F

    cmpg-float v3, p2, v3

    if-gtz v3, :cond_1

    .line 1217
    aget-object v0, p1, v1

    iget v0, v0, Lhng;->b:F

    .line 1229
    :cond_0
    :goto_0
    return v0

    .line 1218
    :cond_1
    add-int/lit8 v3, v2, -0x1

    aget-object v3, p1, v3

    iget v3, v3, Lhng;->g:F

    cmpl-float v3, p2, v3

    if-ltz v3, :cond_2

    .line 1219
    add-int/lit8 v0, v2, -0x1

    aget-object v0, p1, v0

    iget v0, v0, Lhng;->h:F

    goto :goto_0

    .line 1221
    :cond_2
    array-length v2, p1

    :goto_1
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    .line 1222
    iget v4, v3, Lhng;->a:F

    cmpl-float v4, p2, v4

    if-ltz v4, :cond_3

    iget v4, v3, Lhng;->g:F

    cmpg-float v4, p2, v4

    if-gtz v4, :cond_3

    .line 1223
    invoke-virtual {v3, p2}, Lhng;->c(F)F

    move-result v0

    invoke-virtual {v3, v0}, Lhng;->b(F)F

    move-result v0

    goto :goto_0

    .line 1221
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private a(F)Lftl;
    .locals 13

    .prologue
    const/4 v10, 0x0

    .line 1191
    iget v0, p0, Lftk;->a:I

    int-to-float v0, v0

    add-float v1, p1, v0

    .line 1193
    sub-float v0, v1, p1

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v0, v2

    add-float v2, p1, v0

    .line 1195
    iget-object v0, p0, Lftk;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lftk;->d:Lkdv;

    iget v3, p0, Lftk;->a:I

    iget v4, p0, Lftk;->a:I

    invoke-interface {v0, v3, v4}, Lkdv;->b(II)Landroid/graphics/Bitmap;

    move-result-object v0

    move-object v8, v0

    .line 1196
    :goto_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xc

    if-lt v0, v3, :cond_0

    .line 1197
    const/4 v0, 0x1

    invoke-virtual {v8, v0}, Landroid/graphics/Bitmap;->setHasAlpha(Z)V

    .line 1199
    :cond_0
    iget-object v3, p0, Lftk;->h:[Lhng;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    array-length v5, v3

    move v0, v10

    :goto_1
    if-ge v0, v5, :cond_1

    aget-object v6, v3, v0

    invoke-virtual {v6}, Lhng;->b()Lhng;

    move-result-object v7

    iget v9, v6, Lhng;->g:F

    cmpg-float v9, v9, p1

    if-lez v9, :cond_4

    iget v9, v6, Lhng;->a:F

    cmpg-float v9, v9, p1

    if-gez v9, :cond_3

    iget v9, v6, Lhng;->g:F

    cmpl-float v9, v9, v1

    if-ltz v9, :cond_3

    invoke-virtual {v6, p1}, Lhng;->c(F)F

    move-result v0

    invoke-virtual {v6, p1}, Lhng;->c(F)F

    move-result v1

    invoke-virtual {v7, v0, v1}, Lhng;->a(FF)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_2
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lhng;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, [Lhng;

    .line 1200
    invoke-direct {p0, v7, v2}, Lftk;->a([Lhng;F)F

    move-result v0

    iget v1, p0, Lftk;->a:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    sub-float v11, v0, v1

    .line 1201
    iget-object v0, p0, Lftk;->i:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-static {v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->b(Lcom/google/android/apps/plus/stories/views/StoryLayout;)Landroid/graphics/Path;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    .line 1202
    array-length v12, v7

    move v9, v10

    :goto_3
    if-ge v9, v12, :cond_8

    aget-object v6, v7, v9

    .line 1203
    iget-object v0, p0, Lftk;->i:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-static {v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->b(Lcom/google/android/apps/plus/stories/views/StoryLayout;)Landroid/graphics/Path;

    move-result-object v0

    iget v1, v6, Lhng;->a:F

    sub-float/2addr v1, p1

    iget v2, v6, Lhng;->b:F

    sub-float/2addr v2, v11

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    iget v1, v6, Lhng;->c:F

    sub-float/2addr v1, p1

    iget v2, v6, Lhng;->d:F

    sub-float/2addr v2, v11

    iget v3, v6, Lhng;->e:F

    sub-float/2addr v3, p1

    iget v4, v6, Lhng;->f:F

    sub-float/2addr v4, v11

    iget v5, v6, Lhng;->g:F

    sub-float/2addr v5, p1

    iget v6, v6, Lhng;->h:F

    sub-float/2addr v6, v11

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 1202
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_3

    .line 1195
    :cond_2
    iget-object v0, p0, Lftk;->f:Ljava/util/List;

    invoke-interface {v0, v10}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v8, v0

    goto/16 :goto_0

    .line 1199
    :cond_3
    iget v9, v6, Lhng;->a:F

    cmpg-float v9, v9, p1

    if-gez v9, :cond_5

    iget v9, v6, Lhng;->g:F

    cmpg-float v9, v9, v1

    if-gtz v9, :cond_5

    invoke-virtual {v6, p1}, Lhng;->c(F)F

    move-result v6

    invoke-virtual {v7, v6}, Lhng;->e(F)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_4
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    :cond_5
    iget v9, v6, Lhng;->a:F

    cmpl-float v9, v9, p1

    if-ltz v9, :cond_6

    iget v9, v6, Lhng;->g:F

    cmpg-float v9, v9, v1

    if-gtz v9, :cond_6

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_6
    iget v9, v6, Lhng;->a:F

    cmpg-float v9, v9, v1

    if-gez v9, :cond_7

    iget v9, v6, Lhng;->g:F

    cmpl-float v9, v9, v1

    if-ltz v9, :cond_7

    invoke-virtual {v6, v1}, Lhng;->c(F)F

    move-result v0

    invoke-virtual {v7, v0}, Lhng;->d(F)V

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_7
    iget v6, v6, Lhng;->a:F

    cmpl-float v6, v6, v1

    if-gez v6, :cond_1

    goto :goto_4

    .line 1205
    :cond_8
    iget-object v0, p0, Lftk;->g:Landroid/graphics/Canvas;

    invoke-virtual {v0, v8}, Landroid/graphics/Canvas;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 1206
    iget-object v0, p0, Lftk;->g:Landroid/graphics/Canvas;

    sget-object v1, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v10, v1}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    .line 1207
    iget-object v0, p0, Lftk;->g:Landroid/graphics/Canvas;

    iget-object v1, p0, Lftk;->i:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-static {v1}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->b(Lcom/google/android/apps/plus/stories/views/StoryLayout;)Landroid/graphics/Path;

    move-result-object v1

    iget-object v2, p0, Lftk;->i:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-static {v2}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->d(Lcom/google/android/apps/plus/stories/views/StoryLayout;)Landroid/graphics/Paint;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    .line 1208
    new-instance v0, Lftl;

    invoke-direct {v0, v8, v11}, Lftl;-><init>(Landroid/graphics/Bitmap;F)V

    return-object v0
.end method

.method private a(I)V
    .locals 5

    .prologue
    .line 1128
    iget-object v0, p0, Lftk;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1129
    int-to-float v0, p1

    iget v1, p0, Lftk;->a:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    add-int/lit8 v1, v0, 0x1

    .line 1130
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1131
    iget-object v2, p0, Lftk;->d:Lkdv;

    iget v3, p0, Lftk;->a:I

    iget v4, p0, Lftk;->a:I

    invoke-interface {v2, v3, v4}, Lkdv;->b(II)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-direct {p0, v2}, Lftk;->a(Landroid/graphics/Bitmap;)V

    .line 1130
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1133
    :cond_0
    return-void
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 1113
    iget-object v0, p0, Lftk;->f:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1114
    return-void
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    .line 1101
    iget-object v0, p0, Lftk;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lftl;

    .line 1102
    iget-object v2, p0, Lftk;->d:Lkdv;

    iget-object v0, v0, Lftl;->a:Landroid/graphics/Bitmap;

    invoke-interface {v2, v0}, Lkdv;->a(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 1104
    :cond_0
    iget-object v0, p0, Lftk;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1106
    iget-object v0, p0, Lftk;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1107
    iget-object v2, p0, Lftk;->d:Lkdv;

    invoke-interface {v2, v0}, Lkdv;->a(Landroid/graphics/Bitmap;)V

    goto :goto_1

    .line 1109
    :cond_1
    iget-object v0, p0, Lftk;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1110
    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1136
    iget-object v0, p0, Lftk;->i:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-static {v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c(Lcom/google/android/apps/plus/stories/views/StoryLayout;)Lfrq;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lftk;->h:[Lhng;

    if-nez v0, :cond_1

    .line 1173
    :cond_0
    return-void

    .line 1140
    :cond_1
    iget-object v0, p0, Lftk;->i:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-static {v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->c(Lcom/google/android/apps/plus/stories/views/StoryLayout;)Lfrq;

    move-result-object v0

    invoke-virtual {v0}, Lfrq;->j()I

    move-result v0

    int-to-float v2, v0

    .line 1141
    iget-object v0, p0, Lftk;->i:Lcom/google/android/apps/plus/stories/views/StoryLayout;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/stories/views/StoryLayout;->getWidth()I

    move-result v0

    int-to-float v0, v0

    add-float v3, v2, v0

    .line 1144
    :goto_0
    iget v0, p0, Lftk;->b:I

    int-to-float v0, v0

    iget v4, p0, Lftk;->a:I

    int-to-float v4, v4

    sub-float v4, v2, v4

    cmpg-float v0, v0, v4

    if-gez v0, :cond_2

    iget-object v0, p0, Lftk;->e:Ljava/util/List;

    .line 1145
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1146
    iget-object v0, p0, Lftk;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lftl;

    iget-object v0, v0, Lftl;->a:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lftk;->a(Landroid/graphics/Bitmap;)V

    .line 1147
    iget v0, p0, Lftk;->b:I

    iget v4, p0, Lftk;->a:I

    add-int/2addr v0, v4

    iput v0, p0, Lftk;->b:I

    goto :goto_0

    .line 1151
    :cond_2
    :goto_1
    iget v0, p0, Lftk;->c:I

    int-to-float v0, v0

    iget v4, p0, Lftk;->a:I

    int-to-float v4, v4

    add-float/2addr v4, v3

    cmpl-float v0, v0, v4

    if-lez v0, :cond_3

    iget-object v0, p0, Lftk;->e:Ljava/util/List;

    .line 1152
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 1153
    iget-object v0, p0, Lftk;->e:Ljava/util/List;

    iget-object v4, p0, Lftk;->e:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-interface {v0, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lftl;

    iget-object v0, v0, Lftl;->a:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lftk;->a(Landroid/graphics/Bitmap;)V

    .line 1154
    iget v0, p0, Lftk;->c:I

    iget v4, p0, Lftk;->a:I

    sub-int/2addr v0, v4

    iput v0, p0, Lftk;->c:I

    goto :goto_1

    .line 1158
    :cond_3
    :goto_2
    iget v0, p0, Lftk;->b:I

    int-to-float v0, v0

    cmpl-float v0, v0, v2

    if-lez v0, :cond_4

    .line 1159
    iget v0, p0, Lftk;->b:I

    iget v4, p0, Lftk;->a:I

    sub-int/2addr v0, v4

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lftk;->a(F)Lftl;

    move-result-object v0

    iget v4, p0, Lftk;->b:I

    iget v5, p0, Lftk;->a:I

    sub-int/2addr v4, v5

    iput v4, p0, Lftk;->b:I

    iget-object v4, p0, Lftk;->e:Ljava/util/List;

    invoke-interface {v4, v1, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_2

    .line 1163
    :cond_4
    :goto_3
    iget v0, p0, Lftk;->c:I

    int-to-float v0, v0

    cmpg-float v0, v0, v3

    if-gez v0, :cond_5

    .line 1164
    iget v0, p0, Lftk;->c:I

    int-to-float v0, v0

    invoke-direct {p0, v0}, Lftk;->a(F)Lftl;

    move-result-object v0

    iget v2, p0, Lftk;->c:I

    iget v4, p0, Lftk;->a:I

    add-int/2addr v2, v4

    iput v2, p0, Lftk;->c:I

    iget-object v2, p0, Lftk;->e:Ljava/util/List;

    iget-object v4, p0, Lftk;->e:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-interface {v2, v4, v0}, Ljava/util/List;->add(ILjava/lang/Object;)V

    goto :goto_3

    .line 1168
    :cond_5
    :goto_4
    iget-object v0, p0, Lftk;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 1169
    iget-object v0, p0, Lftk;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lftl;

    .line 1170
    iget-object v2, v0, Lftl;->a:Landroid/graphics/Bitmap;

    iget v3, p0, Lftk;->b:I

    iget v4, p0, Lftk;->a:I

    mul-int/2addr v4, v1

    add-int/2addr v3, v4

    int-to-float v3, v3

    iget v0, v0, Lftl;->b:F

    const/4 v4, 0x0

    invoke-virtual {p1, v2, v3, v0, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1168
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4
.end method

.method public a([Lhng;)V
    .locals 0

    .prologue
    .line 1097
    iput-object p1, p0, Lftk;->h:[Lhng;

    .line 1098
    return-void
.end method
