.class public final Lbui;
.super Loxu;
.source "PG"


# instance fields
.field public a:[I

.field public b:J

.field public c:J

.field public d:I

.field public e:I

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v1, -0x1

    .line 524
    invoke-direct {p0}, Loxu;-><init>()V

    .line 527
    const/4 v0, 0x0

    iput v0, p0, Lbui;->f:I

    .line 530
    sget-object v0, Loxx;->a:[I

    iput-object v0, p0, Lbui;->a:[I

    .line 533
    iput-wide v2, p0, Lbui;->b:J

    .line 536
    iput-wide v2, p0, Lbui;->c:J

    .line 539
    iput v1, p0, Lbui;->d:I

    .line 542
    iput v1, p0, Lbui;->e:I

    .line 524
    return-void
.end method

.method public static c(Loxn;)Lbui;
    .locals 1

    .prologue
    .line 673
    new-instance v0, Lbui;

    invoke-direct {v0}, Lbui;-><init>()V

    invoke-virtual {v0, p0}, Lbui;->a(Loxn;)Lbui;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v1, 0x0

    const/4 v6, -0x1

    .line 580
    const/4 v0, 0x1

    iget v2, p0, Lbui;->f:I

    .line 582
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 583
    iget-object v2, p0, Lbui;->a:[I

    if-eqz v2, :cond_1

    iget-object v2, p0, Lbui;->a:[I

    array-length v2, v2

    if-lez v2, :cond_1

    .line 585
    iget-object v3, p0, Lbui;->a:[I

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v1, v4, :cond_0

    aget v5, v3, v1

    .line 587
    invoke-static {v5}, Loxo;->i(I)I

    move-result v5

    add-int/2addr v2, v5

    .line 585
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 589
    :cond_0
    add-int/2addr v0, v2

    .line 590
    iget-object v1, p0, Lbui;->a:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 592
    :cond_1
    iget-wide v2, p0, Lbui;->b:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_2

    .line 593
    const/4 v1, 0x3

    iget-wide v2, p0, Lbui;->b:J

    .line 594
    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 596
    :cond_2
    iget-wide v2, p0, Lbui;->c:J

    cmp-long v1, v2, v8

    if-eqz v1, :cond_3

    .line 597
    const/4 v1, 0x4

    iget-wide v2, p0, Lbui;->c:J

    .line 598
    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 600
    :cond_3
    iget v1, p0, Lbui;->d:I

    if-eq v1, v6, :cond_4

    .line 601
    const/4 v1, 0x5

    iget v2, p0, Lbui;->d:I

    .line 602
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 604
    :cond_4
    iget v1, p0, Lbui;->e:I

    if-eq v1, v6, :cond_5

    .line 605
    const/4 v1, 0x6

    iget v2, p0, Lbui;->e:I

    .line 606
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 608
    :cond_5
    iput v0, p0, Lbui;->ai:I

    .line 609
    return v0
.end method

.method public a(Loxn;)Lbui;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 617
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 618
    sparse-switch v0, :sswitch_data_0

    .line 622
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 623
    :sswitch_0
    return-object p0

    .line 628
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    iput v0, p0, Lbui;->f:I

    goto :goto_0

    .line 632
    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 633
    iget-object v0, p0, Lbui;->a:[I

    array-length v0, v0

    .line 634
    add-int/2addr v1, v0

    new-array v1, v1, [I

    .line 635
    iget-object v2, p0, Lbui;->a:[I

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 636
    iput-object v1, p0, Lbui;->a:[I

    .line 637
    :goto_1
    iget-object v1, p0, Lbui;->a:[I

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_1

    .line 638
    iget-object v1, p0, Lbui;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    .line 639
    invoke-virtual {p1}, Loxn;->a()I

    .line 637
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 642
    :cond_1
    iget-object v1, p0, Lbui;->a:[I

    invoke-virtual {p1}, Loxn;->g()I

    move-result v2

    aput v2, v1, v0

    goto :goto_0

    .line 646
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    iput-wide v0, p0, Lbui;->b:J

    goto :goto_0

    .line 650
    :sswitch_4
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    iput-wide v0, p0, Lbui;->c:J

    goto :goto_0

    .line 654
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    iput v0, p0, Lbui;->d:I

    goto :goto_0

    .line 658
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    iput v0, p0, Lbui;->e:I

    goto :goto_0

    .line 618
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v5, -0x1

    .line 558
    const/4 v0, 0x1

    iget v1, p0, Lbui;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 559
    iget-object v0, p0, Lbui;->a:[I

    if-eqz v0, :cond_0

    .line 560
    iget-object v1, p0, Lbui;->a:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 561
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->a(II)V

    .line 560
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 564
    :cond_0
    iget-wide v0, p0, Lbui;->b:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_1

    .line 565
    const/4 v0, 0x3

    iget-wide v2, p0, Lbui;->b:J

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 567
    :cond_1
    iget-wide v0, p0, Lbui;->c:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_2

    .line 568
    const/4 v0, 0x4

    iget-wide v2, p0, Lbui;->c:J

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 570
    :cond_2
    iget v0, p0, Lbui;->d:I

    if-eq v0, v5, :cond_3

    .line 571
    const/4 v0, 0x5

    iget v1, p0, Lbui;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 573
    :cond_3
    iget v0, p0, Lbui;->e:I

    if-eq v0, v5, :cond_4

    .line 574
    const/4 v0, 0x6

    iget v1, p0, Lbui;->e:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 576
    :cond_4
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 520
    invoke-virtual {p0, p1}, Lbui;->a(Loxn;)Lbui;

    move-result-object v0

    return-object v0
.end method
