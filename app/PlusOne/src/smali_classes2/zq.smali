.class final Lzq;
.super Lzr;
.source "PG"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private final d:Landroid/renderscript/RenderScript;

.field private e:Landroid/renderscript/Allocation;


# direct methods
.method public constructor <init>(Landroid/renderscript/RenderScript;)V
    .locals 1

    .prologue
    .line 801
    invoke-direct {p0}, Lzr;-><init>()V

    .line 799
    const/4 v0, 0x0

    iput-object v0, p0, Lzq;->e:Landroid/renderscript/Allocation;

    .line 802
    iput-object p1, p0, Lzq;->d:Landroid/renderscript/RenderScript;

    .line 803
    return-void
.end method

.method public static i()Z
    .locals 2

    .prologue
    .line 911
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 828
    const/16 v0, 0x20

    return v0
.end method

.method public a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 876
    iget-object v0, p0, Lzq;->e:Landroid/renderscript/Allocation;

    return-object v0
.end method

.method public a(Labf;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    .line 807
    invoke-virtual {p1}, Labf;->e()I

    move-result v0

    const/16 v2, 0x12d

    if-eq v0, v2, :cond_0

    invoke-virtual {p1}, Labf;->e()I

    move-result v0

    const/16 v2, 0xc8

    if-eq v0, v2, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot allocate allocation with a non-RGBA or non-float data type!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lzq;->a:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lzq;->a:[I

    array-length v0, v0

    if-le v0, v4, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot create an allocation with more than 2 dimensions!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 809
    :cond_2
    const/4 v0, 0x0

    .line 810
    invoke-virtual {p1}, Labf;->e()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 818
    :goto_0
    new-instance v2, Landroid/renderscript/Type$Builder;

    iget-object v3, p0, Lzq;->d:Landroid/renderscript/RenderScript;

    invoke-direct {v2, v3, v0}, Landroid/renderscript/Type$Builder;-><init>(Landroid/renderscript/RenderScript;Landroid/renderscript/Element;)V

    .line 819
    iget-object v0, p0, Lzq;->a:[I

    array-length v0, v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lzq;->a:[I

    const/4 v3, 0x0

    aget v0, v0, v3

    :goto_1
    invoke-virtual {v2, v0}, Landroid/renderscript/Type$Builder;->setX(I)Landroid/renderscript/Type$Builder;

    .line 820
    iget-object v0, p0, Lzq;->a:[I

    array-length v0, v0

    if-ne v0, v4, :cond_3

    iget-object v0, p0, Lzq;->a:[I

    aget v1, v0, v1

    :cond_3
    invoke-virtual {v2, v1}, Landroid/renderscript/Type$Builder;->setY(I)Landroid/renderscript/Type$Builder;

    .line 821
    invoke-virtual {v2}, Landroid/renderscript/Type$Builder;->create()Landroid/renderscript/Type;

    move-result-object v0

    .line 823
    iget-object v1, p0, Lzq;->d:Landroid/renderscript/RenderScript;

    invoke-static {v1, v0}, Landroid/renderscript/Allocation;->createTyped(Landroid/renderscript/RenderScript;Landroid/renderscript/Type;)Landroid/renderscript/Allocation;

    move-result-object v0

    iput-object v0, p0, Lzq;->e:Landroid/renderscript/Allocation;

    .line 824
    return-void

    .line 812
    :sswitch_0
    iget-object v0, p0, Lzq;->d:Landroid/renderscript/RenderScript;

    invoke-static {v0}, Landroid/renderscript/Element;->RGBA_8888(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    goto :goto_0

    .line 815
    :sswitch_1
    iget-object v0, p0, Lzq;->d:Landroid/renderscript/RenderScript;

    invoke-static {v0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    goto :goto_0

    :cond_4
    move v0, v1

    .line 819
    goto :goto_1

    .line 810
    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_1
        0x12d -> :sswitch_0
    .end sparse-switch
.end method

.method public a(Lzr;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 843
    invoke-virtual {p1}, Lzr;->a()I

    move-result v0

    .line 844
    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_0

    .line 845
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Lzr;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacw;

    .line 846
    invoke-virtual {p0}, Lzq;->h()I

    move-result v1

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 847
    iget-object v2, p0, Lzq;->a:[I

    aget v2, v2, v4

    iget-object v3, p0, Lzq;->a:[I

    aget v3, v3, v5

    invoke-static {v0, v1, v2, v3}, Labi;->a(Lacw;Ljava/nio/ByteBuffer;II)V

    .line 848
    iget-object v0, p0, Lzq;->e:Landroid/renderscript/Allocation;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/renderscript/Allocation;->copyFrom([B)V

    .line 870
    :goto_0
    invoke-virtual {p1}, Lzr;->d()V

    .line 871
    iput-boolean v4, p0, Lzq;->b:Z

    .line 872
    return-void

    .line 849
    :cond_0
    and-int/lit8 v1, v0, 0x10

    if-eqz v1, :cond_1

    .line 850
    const/16 v0, 0x10

    invoke-virtual {p1, v0}, Lzr;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 851
    iget-object v1, p0, Lzq;->e:Landroid/renderscript/Allocation;

    invoke-virtual {v1, v0}, Landroid/renderscript/Allocation;->copyFrom(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 852
    :cond_1
    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_4

    .line 853
    invoke-virtual {p1, v5}, Lzr;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 854
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->order()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v2

    if-eq v1, v2, :cond_2

    .line 855
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Trying to sync to the ByteBufferBacking with non-native byte order!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 859
    :cond_2
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasArray()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 860
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 866
    :goto_1
    iget-object v1, p0, Lzq;->e:Landroid/renderscript/Allocation;

    invoke-virtual {v1, v0}, Landroid/renderscript/Allocation;->copyFromUnchecked([B)V

    goto :goto_0

    .line 862
    :cond_3
    invoke-virtual {p0}, Lzq;->h()I

    move-result v1

    new-array v1, v1, [B

    .line 863
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 864
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    move-object v0, v1

    goto :goto_1

    .line 868
    :cond_4
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Cannot sync allocation backing!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public b()I
    .locals 1

    .prologue
    .line 833
    const/16 v0, 0x20

    return v0
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 838
    const/4 v0, 0x0

    return v0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 881
    return-void
.end method

.method public e()I
    .locals 1

    .prologue
    .line 885
    const/4 v0, 0x5

    return v0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 890
    const/4 v0, 0x1

    return v0
.end method

.method public g()V
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Lzq;->e:Landroid/renderscript/Allocation;

    if-eqz v0, :cond_0

    .line 896
    iget-object v0, p0, Lzq;->e:Landroid/renderscript/Allocation;

    invoke-virtual {v0}, Landroid/renderscript/Allocation;->destroy()V

    .line 897
    const/4 v0, 0x0

    iput-object v0, p0, Lzq;->e:Landroid/renderscript/Allocation;

    .line 899
    :cond_0
    return-void
.end method

.method public h()I
    .locals 5

    .prologue
    .line 903
    const/4 v1, 0x1

    .line 904
    iget-object v2, p0, Lzq;->a:[I

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 905
    mul-int/2addr v1, v4

    .line 904
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 907
    :cond_0
    invoke-virtual {p0}, Lzq;->k()I

    move-result v0

    mul-int/2addr v0, v1

    return v0
.end method
