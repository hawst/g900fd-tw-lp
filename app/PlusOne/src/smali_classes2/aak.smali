.class public abstract Laak;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field public static final PRIORITY_HIGH:I = 0x4b

.field public static final PRIORITY_LOW:I = 0x19

.field public static final PRIORITY_NORMAL:I = 0x32

.field private static final REQUEST_FLAG_CLOSE:I = 0x1

.field private static final REQUEST_FLAG_NONE:I


# instance fields
.field mAutoReleaseFrames:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Laap;",
            ">;"
        }
    .end annotation
.end field

.field public mConnectedInputPortArray:[Lacp;

.field mConnectedInputPorts:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lacp;",
            ">;"
        }
    .end annotation
.end field

.field public mConnectedOutputPortArray:[Lacv;

.field mConnectedOutputPorts:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lacv;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Lacs;

.field mCurrentTimestamp:J

.field mFilterGraph:Laan;

.field private mIsActive:Z

.field mIsSleeping:Ljava/util/concurrent/atomic/AtomicBoolean;

.field mLastScheduleTime:J

.field public mMinimumAvailableInputs:I

.field private mMinimumAvailableOutputs:I

.field private mName:Ljava/lang/String;

.field mRequests:I

.field mScheduleCount:I

.field mState:Laal;


# direct methods
.method public constructor <init>(Lacs;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    new-instance v0, Laal;

    invoke-direct {v0}, Laal;-><init>()V

    iput-object v0, p0, Laak;->mState:Laal;

    .line 61
    iput v2, p0, Laak;->mRequests:I

    .line 63
    iput v3, p0, Laak;->mMinimumAvailableInputs:I

    .line 64
    iput v3, p0, Laak;->mMinimumAvailableOutputs:I

    .line 66
    iput v2, p0, Laak;->mScheduleCount:I

    .line 67
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Laak;->mLastScheduleTime:J

    .line 69
    iput-boolean v3, p0, Laak;->mIsActive:Z

    .line 70
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Laak;->mIsSleeping:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 72
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Laak;->mCurrentTimestamp:J

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Laak;->mConnectedInputPorts:Ljava/util/HashMap;

    .line 75
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Laak;->mConnectedOutputPorts:Ljava/util/HashMap;

    .line 77
    iput-object v4, p0, Laak;->mConnectedInputPortArray:[Lacp;

    .line 78
    iput-object v4, p0, Laak;->mConnectedOutputPortArray:[Lacv;

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Laak;->mAutoReleaseFrames:Ljava/util/ArrayList;

    .line 94
    iput-object p2, p0, Laak;->mName:Ljava/lang/String;

    .line 95
    iput-object p1, p0, Laak;->mContext:Lacs;

    .line 96
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lacp;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Laak;->mConnectedInputPorts:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacp;

    return-object v0
.end method

.method final a(Laap;)V
    .locals 1

    .prologue
    .line 683
    iget-object v0, p0, Laak;->mAutoReleaseFrames:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 684
    return-void
.end method

.method public a(Lacp;)V
    .locals 0

    .prologue
    .line 235
    return-void
.end method

.method public a(Lacv;)V
    .locals 0

    .prologue
    .line 247
    return-void
.end method

.method final a(Ljava/lang/String;Laak;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 575
    invoke-virtual {p0, p1}, Laak;->b(Ljava/lang/String;)Lacv;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 576
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Attempting to connect already connected output port \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' of filter "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 578
    :cond_0
    invoke-virtual {p2, p3}, Laak;->a(Ljava/lang/String;)Lacp;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 579
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Attempting to connect already connected input port \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\' of filter "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 584
    :cond_1
    iget-object v0, p2, Laak;->mConnectedInputPorts:Ljava/util/HashMap;

    invoke-virtual {v0, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacp;

    if-nez v0, :cond_3

    invoke-virtual {p2}, Laak;->c()Lacx;

    move-result-object v0

    invoke-virtual {v0, p3}, Lacx;->a(Ljava/lang/String;)Lacy;

    move-result-object v1

    new-instance v0, Lacp;

    invoke-direct {v0, p2, p3, v1}, Lacp;-><init>(Laak;Ljava/lang/String;Lacy;)V

    iget-object v1, p2, Laak;->mConnectedInputPorts:Ljava/util/HashMap;

    invoke-virtual {v1, p3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 585
    :goto_0
    iget-object v0, p0, Laak;->mConnectedOutputPorts:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacv;

    if-nez v0, :cond_2

    invoke-virtual {p0}, Laak;->c()Lacx;

    move-result-object v0

    invoke-virtual {v0, p1}, Lacx;->b(Ljava/lang/String;)Lacy;

    move-result-object v2

    new-instance v0, Lacv;

    invoke-direct {v0, p0, p1, v2}, Lacv;-><init>(Laak;Ljava/lang/String;Lacy;)V

    iget-object v2, p0, Laak;->mConnectedOutputPorts:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 586
    :cond_2
    invoke-virtual {v0, v1}, Lacv;->a(Lacp;)V

    .line 587
    invoke-virtual {v1, v0}, Lacp;->b(Lacv;)V

    .line 590
    invoke-virtual {p2, v1}, Laak;->a(Lacp;)V

    .line 591
    invoke-virtual {p0, v0}, Laak;->a(Lacv;)V

    .line 594
    invoke-virtual {p0}, Laak;->x()V

    .line 595
    return-void

    :cond_3
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Lacv;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Laak;->mConnectedOutputPorts:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lacv;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Laak;->mName:Ljava/lang/String;

    return-object v0
.end method

.method public b(Lacp;)V
    .locals 0

    .prologue
    .line 259
    return-void
.end method

.method final b(Lacv;)V
    .locals 5

    .prologue
    .line 697
    invoke-virtual {p1}, Lacv;->e()Labb;

    move-result-object v0

    if-nez v0, :cond_0

    .line 699
    :try_start_0
    new-instance v0, Labc;

    invoke-direct {v0}, Labc;-><init>()V

    .line 700
    invoke-virtual {p1}, Lacv;->d()Lacp;

    move-result-object v1

    .line 701
    invoke-virtual {p1, v0}, Lacv;->a(Labc;)V

    .line 702
    invoke-virtual {v1, v0}, Lacp;->a(Labc;)V

    .line 703
    invoke-virtual {v1}, Lacp;->g()Laak;

    move-result-object v2

    .line 704
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Laak;->mName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Lacv;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "] -> "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v2, v2, Laak;->mName:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Lacp;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 706
    invoke-virtual {v0, v2}, Labc;->a(Ljava/lang/String;)Labb;

    move-result-object v0

    .line 707
    invoke-virtual {p1, v0}, Lacv;->a(Labb;)V

    .line 708
    invoke-virtual {v1, v0}, Lacp;->a(Labb;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 713
    :cond_0
    return-void

    .line 709
    :catch_0
    move-exception v0

    .line 710
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not open output port "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "!"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public c()Lacx;
    .locals 1

    .prologue
    .line 132
    new-instance v0, Lacx;

    invoke-direct {v0}, Lacx;-><init>()V

    return-object v0
.end method

.method public d()Lacs;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Laak;->mContext:Lacs;

    return-object v0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 151
    iget-boolean v0, p0, Laak;->mIsActive:Z

    return v0
.end method

.method public f()V
    .locals 2

    .prologue
    .line 173
    invoke-static {}, Labx;->a()Labx;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Labx;->f()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Labx;->g()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Attempting to modify filter state while runner is executing. Please pause or stop the runner first!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 174
    :cond_0
    iget-boolean v0, p0, Laak;->mIsActive:Z

    if-eqz v0, :cond_1

    .line 175
    const/4 v0, 0x0

    iput-boolean v0, p0, Laak;->mIsActive:Z

    .line 177
    :cond_1
    return-void
.end method

.method public g()V
    .locals 0

    .prologue
    .line 294
    return-void
.end method

.method public h()V
    .locals 0

    .prologue
    .line 305
    return-void
.end method

.method public abstract i()V
.end method

.method public j()V
    .locals 0

    .prologue
    .line 324
    return-void
.end method

.method public k()V
    .locals 0

    .prologue
    .line 334
    return-void
.end method

.method public l()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 346
    iget-object v0, p0, Laak;->mConnectedInputPortArray:[Lacp;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    move v2, v1

    .line 349
    :goto_0
    iget-object v3, p0, Laak;->mConnectedInputPortArray:[Lacp;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 350
    iget-object v3, p0, Laak;->mConnectedInputPortArray:[Lacp;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lacp;->h()Z

    move-result v3

    if-nez v3, :cond_1

    .line 360
    :cond_0
    :goto_1
    return v1

    .line 352
    :cond_1
    iget-object v3, p0, Laak;->mConnectedInputPortArray:[Lacp;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lacp;->d()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 353
    add-int/lit8 v2, v2, 0x1

    .line 349
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 356
    :cond_3
    iget v0, p0, Laak;->mMinimumAvailableInputs:I

    if-lt v2, v0, :cond_0

    .line 360
    :cond_4
    const/4 v1, 0x1

    goto :goto_1
.end method

.method public m()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 373
    iget-object v0, p0, Laak;->mConnectedOutputPortArray:[Lacv;

    array-length v0, v0

    if-lez v0, :cond_6

    move v0, v1

    move v2, v1

    .line 375
    :goto_0
    iget-object v4, p0, Laak;->mConnectedOutputPortArray:[Lacv;

    array-length v4, v4

    if-ge v0, v4, :cond_5

    .line 376
    iget-object v4, p0, Laak;->mConnectedOutputPortArray:[Lacv;

    aget-object v4, v4, v0

    iget-boolean v5, v4, Lacv;->a:Z

    if-eqz v5, :cond_0

    invoke-virtual {v4}, Lacv;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    :cond_0
    move v4, v3

    :goto_1
    if-nez v4, :cond_3

    .line 386
    :cond_1
    :goto_2
    return v1

    :cond_2
    move v4, v1

    .line 376
    goto :goto_1

    .line 378
    :cond_3
    iget-object v4, p0, Laak;->mConnectedOutputPortArray:[Lacv;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Lacv;->a()Z

    move-result v4

    if-eqz v4, :cond_4

    .line 379
    add-int/lit8 v2, v2, 0x1

    .line 375
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 382
    :cond_5
    iget v0, p0, Laak;->mMinimumAvailableOutputs:I

    if-lt v2, v0, :cond_1

    :cond_6
    move v1, v3

    .line 386
    goto :goto_2
.end method

.method public n()Z
    .locals 2

    .prologue
    .line 399
    iget-boolean v0, p0, Laak;->mIsActive:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Laak;->mState:Laal;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Laal;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o()Z
    .locals 1

    .prologue
    .line 414
    invoke-virtual {p0}, Laak;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Laak;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Laak;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public p()I
    .locals 1

    .prologue
    .line 426
    const/16 v0, 0x32

    return v0
.end method

.method public final q()Laat;
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Laak;->mFilterGraph:Laan;

    iget-object v0, v0, Laan;->c:Labx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laak;->mFilterGraph:Laan;

    iget-object v0, v0, Laan;->c:Labx;

    invoke-virtual {v0}, Labx;->k()Laat;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 446
    iget-object v0, p0, Laak;->mFilterGraph:Laan;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laak;->mFilterGraph:Laan;

    iget-object v0, v0, Laan;->c:Labx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Laak;->mFilterGraph:Laan;

    iget-object v0, v0, Laan;->c:Labx;

    invoke-virtual {v0}, Labx;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()V
    .locals 1

    .prologue
    .line 479
    iget v0, p0, Laak;->mRequests:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Laak;->mRequests:I

    .line 480
    return-void
.end method

.method public final t()V
    .locals 2

    .prologue
    .line 540
    iget-object v0, p0, Laak;->mIsSleeping:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 541
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 283
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Laak;->mName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()V
    .locals 2

    .prologue
    .line 549
    iget-object v0, p0, Laak;->mIsSleeping:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 550
    invoke-virtual {p0}, Laak;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 551
    iget-object v0, p0, Laak;->mFilterGraph:Laan;

    iget-object v0, v0, Laan;->c:Labx;

    invoke-virtual {v0}, Labx;->n()V

    .line 554
    :cond_0
    return-void
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 564
    iget-object v0, p0, Laak;->mFilterGraph:Laan;

    iget-object v0, v0, Laan;->c:Labx;

    invoke-virtual {v0}, Labx;->i()Z

    move-result v0

    return v0
.end method

.method final w()V
    .locals 4

    .prologue
    .line 630
    iget-object v1, p0, Laak;->mState:Laal;

    monitor-enter v1

    .line 631
    :try_start_0
    iget-object v0, p0, Laak;->mState:Laal;

    iget v0, v0, Laal;->a:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    .line 632
    invoke-virtual {p0}, Laak;->j()V

    .line 633
    iget-object v0, p0, Laak;->mIsSleeping:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 634
    iget-object v0, p0, Laak;->mState:Laal;

    const/4 v2, 0x4

    iput v2, v0, Laal;->a:I

    .line 635
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Laak;->mCurrentTimestamp:J

    .line 637
    :cond_0
    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method final x()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 778
    iget-object v0, p0, Laak;->mConnectedInputPorts:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    new-array v1, v2, [Lacp;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lacp;

    iput-object v0, p0, Laak;->mConnectedInputPortArray:[Lacp;

    .line 779
    iget-object v0, p0, Laak;->mConnectedOutputPorts:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    new-array v1, v2, [Lacv;

    invoke-interface {v0, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lacv;

    iput-object v0, p0, Laak;->mConnectedOutputPortArray:[Lacv;

    .line 780
    return-void
.end method
