.class public final Lbyi;
.super Ljava/lang/Object;
.source "PG"


# direct methods
.method public static a(Ljava/util/List;I)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lbmd;",
            ">;I)J"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    .line 37
    .line 38
    invoke-interface {p0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbmd;

    add-int/lit8 v1, p1, 0x1

    .line 39
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lbmd;

    .line 37
    invoke-virtual {v0}, Lbmd;->c()Look;

    move-result-object v2

    invoke-virtual {v1}, Lbmd;->c()Look;

    move-result-object v3

    const-string v4, "prevStyling"

    invoke-static {v2, v4, v5}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    const-string v4, "nextStyling"

    invoke-static {v3, v4, v5}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    iget-object v2, v2, Look;->c:Loom;

    iget-object v1, v1, Lbmd;->d:Lbmg;

    invoke-static {v2, v1}, Lbyi;->a(Loom;Lbmg;)J

    move-result-wide v4

    iget-object v1, v3, Look;->d:Loom;

    iget-object v0, v0, Lbmd;->d:Lbmg;

    invoke-static {v1, v0}, Lbyi;->a(Loom;Lbmg;)J

    move-result-wide v0

    invoke-static {v4, v5, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(Loom;Lbmg;)J
    .locals 2

    .prologue
    .line 48
    sget-object v0, Lbyj;->a:[I

    invoke-virtual {p1}, Lbmg;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 58
    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    .line 51
    :pswitch_0
    iget-object v0, p0, Loom;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0

    .line 54
    :pswitch_1
    iget-object v0, p0, Loom;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0

    .line 48
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
