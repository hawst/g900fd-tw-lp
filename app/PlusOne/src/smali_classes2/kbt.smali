.class public final Lkbt;
.super Lkba;
.source "PG"


# static fields
.field private static u:Z

.field private static v:I

.field private static w:I


# instance fields
.field private x:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lkba;-><init>(Landroid/content/Context;)V

    .line 67
    return-void
.end method


# virtual methods
.method public bridge synthetic a(I)V
    .locals 0

    .prologue
    .line 33
    invoke-super {p0, p1}, Lkba;->a(I)V

    return-void
.end method

.method protected a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 79
    invoke-super {p0, p1}, Lkba;->a(Landroid/content/Context;)V

    .line 80
    sget-boolean v0, Lkbt;->u:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d00b0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    const v2, 0x7f0b00ba

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    sput v0, Lkbt;->v:I

    const/high16 v0, 0x40000000    # 2.0f

    invoke-static {v1, v0}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    sput v0, Lkbt;->w:I

    const/4 v0, 0x1

    sput-boolean v0, Lkbt;->u:Z

    .line 82
    :cond_0
    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lkbt;->x:Landroid/view/View;

    .line 83
    iget-object v0, p0, Lkbt;->x:Landroid/view/View;

    sget v1, Lkbt;->v:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 84
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 88
    invoke-super {p0}, Lkba;->b()V

    .line 89
    iget-object v0, p0, Lkbt;->x:Landroid/view/View;

    invoke-virtual {p0, v0}, Lkbt;->addView(Landroid/view/View;)V

    .line 90
    return-void
.end method

.method public bridge synthetic d()V
    .locals 0

    .prologue
    .line 33
    invoke-super {p0}, Lkba;->d()V

    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 12

    .prologue
    .line 161
    const/4 v1, 0x0

    .line 162
    sget-object v0, Lkbt;->a:Llct;

    iget v5, v0, Llct;->m:I

    .line 163
    iget-object v0, p0, Lkbt;->b:Lkzw;

    invoke-virtual {v0}, Lkzw;->f()I

    move-result v6

    .line 164
    iget-object v0, p0, Lkbt;->e:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v7

    .line 167
    iget-object v0, p0, Lkbt;->h:Landroid/view/View;

    const/4 v2, 0x0

    add-int v3, v5, v7

    iget-object v4, p0, Lkbt;->h:Landroid/view/View;

    .line 170
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v3, v4

    iget-object v4, p0, Lkbt;->h:Landroid/view/View;

    .line 171
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    add-int/lit8 v4, v4, 0x0

    add-int v8, v5, v7

    .line 167
    invoke-virtual {v0, v2, v3, v4, v8}, Landroid/view/View;->layout(IIII)V

    .line 174
    const/4 v0, 0x0

    move v3, v0

    move v4, v1

    :goto_0
    iget v0, p0, Lkbt;->r:I

    if-ge v3, v0, :cond_2

    .line 176
    iget-object v0, p0, Lkbt;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 180
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, v4

    .line 181
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, v5

    .line 177
    invoke-virtual {v0, v4, v5, v1, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->layout(IIII)V

    .line 184
    if-nez v3, :cond_0

    .line 185
    iget-object v1, p0, Lkbt;->x:Landroid/view/View;

    .line 186
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v4

    .line 188
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredWidth()I

    move-result v8

    add-int/2addr v8, v4

    iget-object v9, p0, Lkbt;->x:Landroid/view/View;

    .line 189
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    add-int/2addr v8, v9

    iget-object v9, p0, Lkbt;->x:Landroid/view/View;

    .line 190
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    add-int/2addr v9, v5

    .line 185
    invoke-virtual {v1, v2, v5, v8, v9}, Landroid/view/View;->layout(IIII)V

    .line 192
    :cond_0
    iget-object v1, p0, Lkbt;->l:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 193
    add-int v2, v5, v7

    .line 194
    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v8

    sub-int/2addr v2, v8

    sget v8, Lkbt;->i:I

    sub-int v8, v2, v8

    .line 196
    iget-object v2, p0, Lkbt;->n:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 198
    sget v9, Lkbt;->i:I

    add-int/2addr v9, v4

    .line 201
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    add-int/2addr v10, v9

    .line 203
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    add-int/2addr v11, v8

    .line 206
    invoke-virtual {v2, v9, v8, v10, v11}, Landroid/view/View;->layout(IIII)V

    .line 213
    iget-object v2, p0, Lkbt;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 217
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    add-int/2addr v10, v9

    .line 218
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v11

    add-int/2addr v11, v8

    .line 214
    invoke-virtual {v2, v9, v8, v10, v11}, Landroid/view/View;->layout(IIII)V

    .line 221
    invoke-virtual {p0, v3}, Lkbt;->c(I)V

    .line 224
    invoke-virtual {p0}, Lkbt;->m()Z

    move-result v2

    if-nez v2, :cond_1

    .line 228
    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v2

    add-int/2addr v2, v9

    .line 229
    invoke-virtual {v1}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v10

    add-int/2addr v10, v8

    .line 225
    invoke-virtual {v1, v9, v8, v2, v10}, Landroid/widget/TextView;->layout(IIII)V

    .line 233
    :goto_1
    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredWidth()I

    move-result v0

    add-int v1, v4, v0

    .line 174
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    move v4, v1

    goto/16 :goto_0

    .line 231
    :cond_1
    invoke-virtual {p0, v3}, Lkbt;->d(I)V

    goto :goto_1

    .line 236
    :cond_2
    const/4 v0, -0x1

    if-eq v6, v0, :cond_3

    .line 237
    invoke-virtual {p0, v6}, Lkbt;->f(I)V

    .line 239
    :cond_3
    invoke-virtual {p0}, Lkbt;->d()V

    .line 240
    iget-object v0, p0, Lkbt;->e:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredWidth()I

    move-result v0

    int-to-float v1, v0

    iget-object v0, p0, Lkbt;->n:Ljava/util/ArrayList;

    const/4 v2, 0x0

    .line 241
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    int-to-float v0, v0

    div-float v0, v1, v0

    iput v0, p0, Lkbt;->t:F

    .line 242
    return-void
.end method

.method protected onMeasure(II)V
    .locals 11

    .prologue
    const/4 v10, -0x2

    const/4 v2, 0x0

    const/high16 v9, 0x40000000    # 2.0f

    .line 94
    iget-object v0, p0, Lkbt;->p:Landroid/widget/ImageView;

    invoke-virtual {v0, v10, v10}, Landroid/widget/ImageView;->measure(II)V

    .line 95
    iget v0, p0, Lkbt;->c:I

    div-int/lit8 v0, v0, 0x2

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    .line 98
    iget v0, p0, Lkbt;->c:I

    div-int/lit8 v0, v0, 0x2

    sget v1, Lkbt;->i:I

    shl-int/lit8 v1, v1, 0x1

    sub-int v6, v0, v1

    .line 99
    invoke-static {v6, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v7

    .line 102
    iget v0, p0, Lkbt;->c:I

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 106
    iget-object v1, p0, Lkbt;->x:Landroid/view/View;

    sget v3, Lkbt;->w:I

    iget v4, p0, Lkbt;->d:I

    invoke-virtual {v1, v3, v4}, Landroid/view/View;->measure(II)V

    .line 109
    iget-object v1, p0, Lkbt;->h:Landroid/view/View;

    iget v3, p0, Lkbt;->g:I

    invoke-virtual {v1, v0, v3}, Landroid/view/View;->measure(II)V

    move v1, v2

    move v3, v2

    .line 112
    :goto_0
    iget v0, p0, Lkbt;->r:I

    if-ge v1, v0, :cond_2

    .line 113
    iget v0, p0, Lkbt;->q:I

    if-ne v1, v0, :cond_1

    iget-object v0, p0, Lkbt;->p:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v0

    move v4, v0

    .line 115
    :goto_1
    iget-object v0, p0, Lkbt;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 117
    sub-int v4, v6, v4

    sget v8, Lkbt;->k:I

    div-int/lit8 v8, v8, 0x2

    sub-int/2addr v4, v8

    invoke-static {v4, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 122
    invoke-virtual {v0, v4, v10}, Landroid/widget/TextView;->measure(II)V

    .line 124
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v4

    if-le v4, v3, :cond_0

    .line 125
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v3

    .line 112
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    move v4, v2

    .line 113
    goto :goto_1

    :cond_2
    move v1, v2

    .line 129
    :goto_2
    iget v0, p0, Lkbt;->r:I

    if-ge v1, v0, :cond_3

    .line 131
    iget-object v0, p0, Lkbt;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    iget v4, p0, Lkbt;->d:I

    invoke-virtual {v0, v5, v4}, Lcom/google/android/libraries/social/media/ui/MediaView;->measure(II)V

    .line 134
    iget-object v0, p0, Lkbt;->l:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 136
    invoke-virtual {v0}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v4

    invoke-static {v4, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 138
    invoke-static {v3, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 142
    invoke-virtual {v0, v4, v6}, Landroid/widget/TextView;->measure(II)V

    .line 146
    iget-object v0, p0, Lkbt;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v7, v6}, Landroid/view/View;->measure(II)V

    .line 150
    iget-object v0, p0, Lkbt;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0, v10, v6}, Landroid/view/View;->measure(II)V

    .line 129
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 153
    :cond_3
    iget-object v0, p0, Lkbt;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/media/ui/MediaView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->getMeasuredHeight()I

    move-result v0

    sget-object v1, Lkbt;->a:Llct;

    iget v1, v1, Llct;->m:I

    add-int/2addr v0, v1

    .line 156
    iget v1, p0, Lkbt;->c:I

    invoke-virtual {p0, v1, v0}, Lkbt;->setMeasuredDimension(II)V

    .line 157
    return-void
.end method
