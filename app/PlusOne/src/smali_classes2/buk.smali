.class public final Lbuk;
.super Loxu;
.source "PG"


# instance fields
.field public a:[Lbum;

.field public b:[Lbuj;

.field public c:I

.field public d:[Lbul;

.field public e:Ljava/lang/String;

.field private f:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Loxu;-><init>()V

    .line 18
    iput v1, p0, Lbuk;->f:I

    .line 21
    sget-object v0, Lbum;->a:[Lbum;

    iput-object v0, p0, Lbuk;->a:[Lbum;

    .line 24
    sget-object v0, Lbuj;->a:[Lbuj;

    iput-object v0, p0, Lbuk;->b:[Lbuj;

    .line 27
    iput v1, p0, Lbuk;->c:I

    .line 30
    sget-object v0, Lbul;->a:[Lbul;

    iput-object v0, p0, Lbuk;->d:[Lbul;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lbuk;->e:Ljava/lang/String;

    .line 10
    return-void
.end method

.method public static a([B)Lbuk;
    .locals 1

    .prologue
    .line 207
    new-instance v0, Lbuk;

    invoke-direct {v0}, Lbuk;-><init>()V

    invoke-static {v0, p0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lbuk;

    return-object v0
.end method

.method public static c(Loxn;)Lbuk;
    .locals 1

    .prologue
    .line 213
    new-instance v0, Lbuk;

    invoke-direct {v0}, Lbuk;-><init>()V

    invoke-virtual {v0, p0}, Lbuk;->a(Loxn;)Lbuk;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 81
    const/4 v0, 0x1

    iget v2, p0, Lbuk;->f:I

    .line 83
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 84
    iget-object v2, p0, Lbuk;->a:[Lbum;

    if-eqz v2, :cond_1

    .line 85
    iget-object v3, p0, Lbuk;->a:[Lbum;

    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 86
    if-eqz v5, :cond_0

    .line 87
    const/4 v6, 0x2

    .line 88
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 85
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 92
    :cond_1
    iget-object v2, p0, Lbuk;->b:[Lbuj;

    if-eqz v2, :cond_3

    .line 93
    iget-object v3, p0, Lbuk;->b:[Lbuj;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 94
    if-eqz v5, :cond_2

    .line 95
    const/4 v6, 0x3

    .line 96
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 93
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 100
    :cond_3
    iget v2, p0, Lbuk;->c:I

    if-eqz v2, :cond_4

    .line 101
    const/4 v2, 0x4

    iget v3, p0, Lbuk;->c:I

    .line 102
    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 104
    :cond_4
    iget-object v2, p0, Lbuk;->d:[Lbul;

    if-eqz v2, :cond_6

    .line 105
    iget-object v2, p0, Lbuk;->d:[Lbul;

    array-length v3, v2

    :goto_2
    if-ge v1, v3, :cond_6

    aget-object v4, v2, v1

    .line 106
    if-eqz v4, :cond_5

    .line 107
    const/4 v5, 0x5

    .line 108
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 105
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 112
    :cond_6
    iget-object v1, p0, Lbuk;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 113
    const/4 v1, 0x6

    iget-object v2, p0, Lbuk;->e:Ljava/lang/String;

    .line 114
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 116
    :cond_7
    iput v0, p0, Lbuk;->ai:I

    .line 117
    return v0
.end method

.method public a(Loxn;)Lbuk;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 125
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 126
    sparse-switch v0, :sswitch_data_0

    .line 130
    invoke-static {p1, v0}, Loxx;->a(Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    :sswitch_0
    return-object p0

    .line 136
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    iput v0, p0, Lbuk;->f:I

    goto :goto_0

    .line 140
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 141
    iget-object v0, p0, Lbuk;->a:[Lbum;

    if-nez v0, :cond_2

    move v0, v1

    .line 142
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lbum;

    .line 143
    iget-object v3, p0, Lbuk;->a:[Lbum;

    if-eqz v3, :cond_1

    .line 144
    iget-object v3, p0, Lbuk;->a:[Lbum;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 146
    :cond_1
    iput-object v2, p0, Lbuk;->a:[Lbum;

    .line 147
    :goto_2
    iget-object v2, p0, Lbuk;->a:[Lbum;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_3

    .line 148
    iget-object v2, p0, Lbuk;->a:[Lbum;

    new-instance v3, Lbum;

    invoke-direct {v3}, Lbum;-><init>()V

    aput-object v3, v2, v0

    .line 149
    iget-object v2, p0, Lbuk;->a:[Lbum;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 150
    invoke-virtual {p1}, Loxn;->a()I

    .line 147
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 141
    :cond_2
    iget-object v0, p0, Lbuk;->a:[Lbum;

    array-length v0, v0

    goto :goto_1

    .line 153
    :cond_3
    iget-object v2, p0, Lbuk;->a:[Lbum;

    new-instance v3, Lbum;

    invoke-direct {v3}, Lbum;-><init>()V

    aput-object v3, v2, v0

    .line 154
    iget-object v2, p0, Lbuk;->a:[Lbum;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 158
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 159
    iget-object v0, p0, Lbuk;->b:[Lbuj;

    if-nez v0, :cond_5

    move v0, v1

    .line 160
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lbuj;

    .line 161
    iget-object v3, p0, Lbuk;->b:[Lbuj;

    if-eqz v3, :cond_4

    .line 162
    iget-object v3, p0, Lbuk;->b:[Lbuj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 164
    :cond_4
    iput-object v2, p0, Lbuk;->b:[Lbuj;

    .line 165
    :goto_4
    iget-object v2, p0, Lbuk;->b:[Lbuj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 166
    iget-object v2, p0, Lbuk;->b:[Lbuj;

    new-instance v3, Lbuj;

    invoke-direct {v3}, Lbuj;-><init>()V

    aput-object v3, v2, v0

    .line 167
    iget-object v2, p0, Lbuk;->b:[Lbuj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 168
    invoke-virtual {p1}, Loxn;->a()I

    .line 165
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 159
    :cond_5
    iget-object v0, p0, Lbuk;->b:[Lbuj;

    array-length v0, v0

    goto :goto_3

    .line 171
    :cond_6
    iget-object v2, p0, Lbuk;->b:[Lbuj;

    new-instance v3, Lbuj;

    invoke-direct {v3}, Lbuj;-><init>()V

    aput-object v3, v2, v0

    .line 172
    iget-object v2, p0, Lbuk;->b:[Lbuj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 176
    :sswitch_4
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    iput v0, p0, Lbuk;->c:I

    goto/16 :goto_0

    .line 180
    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 181
    iget-object v0, p0, Lbuk;->d:[Lbul;

    if-nez v0, :cond_8

    move v0, v1

    .line 182
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lbul;

    .line 183
    iget-object v3, p0, Lbuk;->d:[Lbul;

    if-eqz v3, :cond_7

    .line 184
    iget-object v3, p0, Lbuk;->d:[Lbul;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 186
    :cond_7
    iput-object v2, p0, Lbuk;->d:[Lbul;

    .line 187
    :goto_6
    iget-object v2, p0, Lbuk;->d:[Lbul;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 188
    iget-object v2, p0, Lbuk;->d:[Lbul;

    new-instance v3, Lbul;

    invoke-direct {v3}, Lbul;-><init>()V

    aput-object v3, v2, v0

    .line 189
    iget-object v2, p0, Lbuk;->d:[Lbul;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 190
    invoke-virtual {p1}, Loxn;->a()I

    .line 187
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 181
    :cond_8
    iget-object v0, p0, Lbuk;->d:[Lbul;

    array-length v0, v0

    goto :goto_5

    .line 193
    :cond_9
    iget-object v2, p0, Lbuk;->d:[Lbul;

    new-instance v3, Lbul;

    invoke-direct {v3}, Lbul;-><init>()V

    aput-object v3, v2, v0

    .line 194
    iget-object v2, p0, Lbuk;->d:[Lbul;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 198
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lbuk;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 126
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 49
    const/4 v1, 0x1

    iget v2, p0, Lbuk;->f:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 50
    iget-object v1, p0, Lbuk;->a:[Lbum;

    if-eqz v1, :cond_1

    .line 51
    iget-object v2, p0, Lbuk;->a:[Lbum;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 52
    if-eqz v4, :cond_0

    .line 53
    const/4 v5, 0x2

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 51
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 57
    :cond_1
    iget-object v1, p0, Lbuk;->b:[Lbuj;

    if-eqz v1, :cond_3

    .line 58
    iget-object v2, p0, Lbuk;->b:[Lbuj;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 59
    if-eqz v4, :cond_2

    .line 60
    const/4 v5, 0x3

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 58
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 64
    :cond_3
    iget v1, p0, Lbuk;->c:I

    if-eqz v1, :cond_4

    .line 65
    const/4 v1, 0x4

    iget v2, p0, Lbuk;->c:I

    invoke-virtual {p1, v1, v2}, Loxo;->a(II)V

    .line 67
    :cond_4
    iget-object v1, p0, Lbuk;->d:[Lbul;

    if-eqz v1, :cond_6

    .line 68
    iget-object v1, p0, Lbuk;->d:[Lbul;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 69
    if-eqz v3, :cond_5

    .line 70
    const/4 v4, 0x5

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 68
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 74
    :cond_6
    iget-object v0, p0, Lbuk;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 75
    const/4 v0, 0x6

    iget-object v1, p0, Lbuk;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 77
    :cond_7
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lbuk;->a(Loxn;)Lbuk;

    move-result-object v0

    return-object v0
.end method
