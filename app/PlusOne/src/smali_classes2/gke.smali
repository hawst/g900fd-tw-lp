.class final Lgke;
.super Lgkp;


# instance fields
.field final synthetic a:Lgkb;

.field private b:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method constructor <init>(Lgkb;)V
    .locals 2

    iput-object p1, p0, Lgke;->a:Lgkb;

    invoke-direct {p0}, Lgkp;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lgke;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method private b(JI)V
    .locals 3

    iget-object v0, p0, Lgke;->a:Lgkb;

    invoke-static {v0}, Lgkb;->h(Lgkb;)Ljava/util/Map;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lgke;->a:Lgkb;

    invoke-static {v0}, Lgkb;->h(Lgkb;)Ljava/util/Map;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgfy;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v1, p3}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v1}, Lgfy;->a(Ljava/lang/Object;)V

    :cond_0
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method private f(I)Z
    .locals 3

    invoke-static {}, Lgkb;->m()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lgke;->a:Lgkb;

    invoke-static {v0}, Lgkb;->i(Lgkb;)Lgfy;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgke;->a:Lgkb;

    invoke-static {v0}, Lgkb;->i(Lgkb;)Lgfy;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v2, p1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v2}, Lgfy;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lgke;->a:Lgkb;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lgkb;->b(Lgkb;Lgfy;)Lgfy;

    const/4 v0, 0x1

    monitor-exit v1

    :goto_0
    return v0

    :cond_0
    monitor-exit v1

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method


# virtual methods
.method public a(I)V
    .locals 5

    invoke-virtual {p0}, Lgke;->a()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lgkb;->k()Lgkq;

    move-result-object v0

    const-string v1, "ICastDeviceControllerListener.onDisconnected: %d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lgkq;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lgke;->a:Lgkb;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lgkb;->a(I)V

    goto :goto_0
.end method

.method public a(J)V
    .locals 1

    iget-object v0, p0, Lgke;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lgke;->b(JI)V

    goto :goto_0
.end method

.method public a(JI)V
    .locals 1

    iget-object v0, p0, Lgke;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lgke;->b(JI)V

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/cast/ApplicationMetadata;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 5

    iget-object v0, p0, Lgke;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lgke;->a:Lgkb;

    invoke-static {p1}, Lgkb;->a(Lcom/google/android/gms/cast/ApplicationMetadata;)Lcom/google/android/gms/cast/ApplicationMetadata;

    iget-object v0, p0, Lgke;->a:Lgkb;

    invoke-virtual {p1}, Lcom/google/android/gms/cast/ApplicationMetadata;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lgkb;->a(Lgkb;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lgke;->a:Lgkb;

    invoke-static {v0, p3}, Lgkb;->b(Lgkb;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {}, Lgkb;->l()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lgke;->a:Lgkb;

    invoke-static {v0}, Lgkb;->c(Lgkb;)Lgfy;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgke;->a:Lgkb;

    invoke-static {v0}, Lgkb;->c(Lgkb;)Lgfy;

    move-result-object v0

    new-instance v2, Lgkc;

    new-instance v3, Lcom/google/android/gms/common/api/Status;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-direct {v2, v3}, Lgkc;-><init>(Lcom/google/android/gms/common/api/Status;)V

    invoke-interface {v0, v2}, Lgfy;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lgke;->a:Lgkb;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lgkb;->a(Lgkb;Lgfy;)Lgfy;

    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/gms/internal/ig;)V
    .locals 3

    iget-object v0, p0, Lgke;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lgkb;->k()Lgkq;

    move-result-object v0

    const-string v1, "onApplicationStatusChanged"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lgkq;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lgke;->a:Lgkb;

    invoke-static {v0}, Lgkb;->e(Lgkb;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lgkh;

    invoke-direct {v1, p0, p1}, Lgkh;-><init>(Lgke;Lcom/google/android/gms/internal/ig;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/internal/il;)V
    .locals 3

    iget-object v0, p0, Lgke;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lgkb;->k()Lgkq;

    move-result-object v0

    const-string v1, "onDeviceStatusChanged"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lgkq;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lgke;->a:Lgkb;

    invoke-static {v0}, Lgkb;->e(Lgkb;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lgkg;

    invoke-direct {v1, p0, p1}, Lgkg;-><init>(Lgke;Lcom/google/android/gms/internal/il;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lgke;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lgkb;->k()Lgkq;

    move-result-object v0

    const-string v1, "Receive (type=text, ns=%s) %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lgkq;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lgke;->a:Lgkb;

    invoke-static {v0}, Lgkb;->e(Lgkb;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lgki;

    invoke-direct {v1, p0, p1, p2}, Lgki;-><init>(Lgke;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public a(Ljava/lang/String;[B)V
    .locals 5

    iget-object v0, p0, Lgke;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lgkb;->k()Lgkq;

    move-result-object v0

    const-string v1, "IGNORING: Receive (type=binary, ns=%s) <%d bytes>"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    array-length v4, p2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lgkq;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public a()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lgke;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lgke;->a:Lgkb;

    invoke-static {v1}, Lgkb;->b(Lgkb;)V

    goto :goto_0
.end method

.method public b(I)V
    .locals 5

    iget-object v0, p0, Lgke;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lgkb;->l()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lgke;->a:Lgkb;

    invoke-static {v0}, Lgkb;->c(Lgkb;)Lgfy;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lgke;->a:Lgkb;

    invoke-static {v0}, Lgkb;->c(Lgkb;)Lgfy;

    move-result-object v0

    new-instance v2, Lgkc;

    new-instance v3, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v3, p1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4}, Lgkc;-><init>(Lcom/google/android/gms/common/api/Status;B)V

    invoke-interface {v0, v2}, Lgfy;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lgke;->a:Lgkb;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lgkb;->a(Lgkb;Lgfy;)Lgfy;

    :cond_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lgke;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    return v0
.end method

.method public c()V
    .locals 3

    invoke-static {}, Lgkb;->k()Lgkq;

    move-result-object v0

    const-string v1, "Deprecated callback: \"onStatusreceived\""

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lgkq;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    return-void
.end method

.method public c(I)V
    .locals 1

    iget-object v0, p0, Lgke;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lgke;->f(I)Z

    goto :goto_0
.end method

.method public d(I)V
    .locals 1

    iget-object v0, p0, Lgke;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lgke;->f(I)Z

    goto :goto_0
.end method

.method public e(I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lgke;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lgke;->a:Lgkb;

    invoke-static {v0, v1}, Lgkb;->a(Lgkb;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lgke;->a:Lgkb;

    invoke-static {v0, v1}, Lgkb;->b(Lgkb;Ljava/lang/String;)Ljava/lang/String;

    invoke-direct {p0, p1}, Lgke;->f(I)Z

    iget-object v0, p0, Lgke;->a:Lgkb;

    invoke-static {v0}, Lgkb;->d(Lgkb;)Lgdt;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lgke;->a:Lgkb;

    invoke-static {v0}, Lgkb;->e(Lgkb;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lgkf;

    invoke-direct {v1, p0, p1}, Lgkf;-><init>(Lgke;I)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
