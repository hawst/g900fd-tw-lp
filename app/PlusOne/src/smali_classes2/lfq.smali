.class public final Llfq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lkfe;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Ljava/lang/String;Lkfg;ILjava/lang/Exception;Ljava/lang/String;)V
    .locals 10

    .prologue
    .line 45
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    invoke-interface {v0, p2}, Lhei;->a(Ljava/lang/String;)I

    move-result v1

    .line 46
    const-class v0, Llfn;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Llfn;

    .line 48
    const-wide/16 v2, 0x1

    const-string v4, "Rpc[start]: "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 49
    invoke-virtual {p3}, Lkfg;->f()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v5, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :goto_0
    invoke-virtual {p3}, Lkfg;->k()J

    move-result-wide v5

    .line 48
    invoke-interface/range {v0 .. v6}, Llfn;->a(IJLjava/lang/String;J)V

    .line 50
    const-wide/16 v2, 0x1

    const-string v4, "Rpc[end] - %s, bytes sent: %d, bytes received: %d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    .line 51
    invoke-virtual {p3}, Lkfg;->f()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    .line 52
    invoke-virtual {p3}, Lkfg;->n()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-virtual {p3}, Lkfg;->j()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    .line 51
    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 52
    invoke-virtual {p3}, Lkfg;->l()J

    move-result-wide v5

    .line 50
    invoke-interface/range {v0 .. v6}, Llfn;->a(IJLjava/lang/String;J)V

    .line 53
    return-void

    .line 49
    :cond_0
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;[BLjava/lang/String;)V
    .locals 0

    .prologue
    .line 33
    return-void
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 21
    const/4 v0, 0x0

    return v0
.end method

.method public b(Ljava/lang/String;[BLjava/lang/String;)V
    .locals 0

    .prologue
    .line 39
    return-void
.end method

.method public b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    return v0
.end method
