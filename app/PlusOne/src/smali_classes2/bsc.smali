.class public Lbsc;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbtp;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lbig;

.field private final d:Lbjp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjp",
            "<",
            "Lbhk;",
            ">;"
        }
    .end annotation
.end field

.field private final e:Lbjp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjp",
            "<",
            "Lbhl;",
            ">;"
        }
    .end annotation
.end field

.field private final f:Lbgf;

.field private final g:Lbte;

.field private final h:Lasy;

.field private final i:Ljava/util/concurrent/Executor;

.field private final j:Ljava/util/concurrent/Executor;

.field private final k:Lchl;

.field private final l:Lbjf;

.field private m:Lbtq;

.field private n:Lbsu;

.field private o:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-class v0, Lbsc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbsc;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lbig;Lbjp;Lbjp;Lbgf;Lbte;Lasy;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lchl;Lbjf;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lbig;",
            "Lbjp",
            "<",
            "Lbhk;",
            ">;",
            "Lbjp",
            "<",
            "Lbhl;",
            ">;",
            "Lbgf;",
            "Lbte;",
            "Lasy;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Executor;",
            "Lchl;",
            "Lbjf;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    const-string v0, "context"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbsc;->b:Landroid/content/Context;

    .line 89
    const-string v0, "mediaExtractorFactory"

    .line 90
    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbig;

    iput-object v0, p0, Lbsc;->c:Lbig;

    .line 91
    const-string v0, "audioDecoderPool"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjp;

    iput-object v0, p0, Lbsc;->d:Lbjp;

    .line 92
    const-string v0, "videoDecoderPool"

    invoke-static {p4, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjp;

    iput-object v0, p0, Lbsc;->e:Lbjp;

    .line 93
    const-string v0, "renderContext"

    invoke-static {p5, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgf;

    iput-object v0, p0, Lbsc;->f:Lbgf;

    .line 94
    const-string v0, "renderer"

    invoke-static {p6, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbte;

    iput-object v0, p0, Lbsc;->g:Lbte;

    .line 95
    const-string v0, "stateTracker"

    invoke-static {p7, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lasy;

    iput-object v0, p0, Lbsc;->h:Lasy;

    .line 96
    const-string v0, "mainThreadExecutor"

    invoke-static {p8, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lbsc;->i:Ljava/util/concurrent/Executor;

    .line 97
    const-string v0, "decoderExecutor"

    invoke-static {p9, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lbsc;->j:Ljava/util/concurrent/Executor;

    .line 98
    const-string v0, "playerThreadFactory"

    invoke-static {p10, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lchl;

    iput-object v0, p0, Lbsc;->k:Lchl;

    .line 99
    const-string v0, "bitmapFactory"

    invoke-static {p11, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjf;

    iput-object v0, p0, Lbsc;->l:Lbjf;

    .line 100
    return-void
.end method

.method static synthetic a(Lbsc;)Lasy;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lbsc;->h:Lasy;

    return-object v0
.end method

.method static synthetic a(Lbsc;Ljava/lang/Thread;)Ljava/lang/Thread;
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lbsc;->o:Ljava/lang/Thread;

    return-object p1
.end method

.method static synthetic a(Lbsc;Lbss;Lbge;)V
    .locals 3

    .prologue
    .line 39
    :try_start_0
    const-string v0, "renderSink"

    const/4 v1, 0x0

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    iget-object v0, p0, Lbsc;->f:Lbgf;

    new-instance v1, Lbse;

    invoke-direct {v1, p0, p1, p2}, Lbse;-><init>(Lbsc;Lbss;Lbge;)V

    invoke-virtual {v0, v1}, Lbgf;->a(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Lbgm; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    sget-object v1, Lbsc;->a:Ljava/lang/String;

    const-string v2, "render context not initialized"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Lbsc;->a:Ljava/lang/String;

    const-string v2, "unexpected exception from render context"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method static synthetic b(Lbsc;)Lbsu;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lbsc;->n:Lbsu;

    return-object v0
.end method

.method static synthetic c(Lbsc;)Lbjp;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lbsc;->e:Lbjp;

    return-object v0
.end method

.method static synthetic d(Lbsc;)Lbjp;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lbsc;->d:Lbjp;

    return-object v0
.end method

.method static synthetic e(Lbsc;)Lbtq;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lbsc;->m:Lbtq;

    return-object v0
.end method

.method static synthetic f(Lbsc;)Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lbsc;->i:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lbsc;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lbsc;)Ljava/lang/Thread;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lbsc;->o:Ljava/lang/Thread;

    return-object v0
.end method


# virtual methods
.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 149
    invoke-virtual {p0}, Lbsc;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 150
    iget-object v0, p0, Lbsc;->o:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 151
    const/4 v0, 0x0

    iput-object v0, p0, Lbsc;->o:Ljava/lang/Thread;

    .line 154
    iget-object v0, p0, Lbsc;->m:Lbtq;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lbsc;->m:Lbtq;

    invoke-interface {v0, v1, v1}, Lbtq;->a(IZ)V

    .line 158
    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lbsc;->n:Lbsu;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lbsc;->n:Lbsu;

    invoke-virtual {v0, p1, p2}, Lbsu;->a(J)V

    .line 179
    :cond_0
    return-void
.end method

.method public a(Lboi;)V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lbsc;->n:Lbsu;

    invoke-virtual {v0, p1}, Lbsu;->a(Lboi;)V

    .line 145
    return-void
.end method

.method public a(Lboi;Lbss;Lbge;)V
    .locals 18

    .prologue
    .line 116
    move-object/from16 v0, p0

    iget-object v3, v0, Lbsc;->o:Ljava/lang/Thread;

    const-string v4, "mPlayerThread"

    const-string v5, "Existing player thread still running."

    invoke-static {v3, v4, v5}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 117
    move-object/from16 v0, p0

    iget-object v3, v0, Lbsc;->k:Lchl;

    new-instance v4, Lbsd;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-direct {v4, v0, v1, v2}, Lbsd;-><init>(Lbsc;Lbss;Lbge;)V

    const-string v5, "player"

    invoke-interface {v3, v4, v5}, Lchl;->a(Ljava/lang/Runnable;Ljava/lang/String;)Ljava/lang/Thread;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lbsc;->o:Ljava/lang/Thread;

    .line 123
    new-instance v3, Lbsu;

    move-object/from16 v0, p0

    iget-object v4, v0, Lbsc;->b:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbsc;->c:Lbig;

    move-object/from16 v0, p0

    iget-object v6, v0, Lbsc;->d:Lbjp;

    move-object/from16 v0, p0

    iget-object v7, v0, Lbsc;->e:Lbjp;

    move-object/from16 v0, p0

    iget-object v8, v0, Lbsc;->f:Lbgf;

    move-object/from16 v0, p0

    iget-object v9, v0, Lbsc;->g:Lbte;

    move-object/from16 v0, p0

    iget-object v10, v0, Lbsc;->h:Lasy;

    move-object/from16 v0, p0

    iget-object v11, v0, Lbsc;->l:Lbjf;

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v14, v0, Lbsc;->j:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v15, v0, Lbsc;->i:Ljava/util/concurrent/Executor;

    new-instance v16, Lbsg;

    move-object/from16 v0, p0

    iget-object v12, v0, Lbsc;->o:Ljava/lang/Thread;

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v12}, Lbsg;-><init>(Lbsc;Ljava/lang/Thread;)V

    const/16 v17, 0x0

    move-object/from16 v12, p1

    invoke-direct/range {v3 .. v17}, Lbsu;-><init>(Landroid/content/Context;Lbig;Lbjp;Lbjp;Lbgf;Lbte;Lasy;Lbjf;Lboi;ZLjava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lbtq;Lbtc;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lbsc;->n:Lbsu;

    .line 138
    move-object/from16 v0, p0

    iget-object v3, v0, Lbsc;->o:Ljava/lang/Thread;

    const/16 v4, 0xa

    invoke-virtual {v3, v4}, Ljava/lang/Thread;->setPriority(I)V

    .line 139
    move-object/from16 v0, p0

    iget-object v3, v0, Lbsc;->o:Ljava/lang/Thread;

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 140
    return-void
.end method

.method public a(Lbtq;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lbsc;->m:Lbtq;

    .line 105
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lbsc;->n:Lbsu;

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lbsc;->n:Lbsu;

    invoke-virtual {v0}, Lbsu;->a()V

    .line 165
    :cond_0
    return-void
.end method

.method public c()V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lbsc;->n:Lbsu;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Lbsc;->n:Lbsu;

    invoke-virtual {v0}, Lbsu;->b()V

    .line 172
    :cond_0
    return-void
.end method

.method public d()V
    .locals 1

    .prologue
    .line 183
    iget-object v0, p0, Lbsc;->n:Lbsu;

    if-eqz v0, :cond_0

    .line 184
    iget-object v0, p0, Lbsc;->n:Lbsu;

    invoke-virtual {v0}, Lbsu;->d()V

    .line 186
    :cond_0
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lbsc;->o:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lbsc;->n:Lbsu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbsc;->n:Lbsu;

    .line 196
    invoke-virtual {v0}, Lbsu;->e()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lbsc;->n:Lbsu;

    invoke-virtual {v0}, Lbsu;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
