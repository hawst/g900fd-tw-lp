.class final Layy;
.super Landroid/view/animation/DecelerateInterpolator;
.source "PG"


# instance fields
.field private final a:F


# direct methods
.method public constructor <init>(FF)V
    .locals 0

    .prologue
    .line 42
    invoke-direct {p0, p1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    .line 43
    iput p2, p0, Layy;->a:F

    .line 44
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 3

    .prologue
    .line 48
    iget v0, p0, Layy;->a:F

    cmpl-float v0, p1, v0

    if-ltz v0, :cond_0

    .line 49
    iget v0, p0, Layy;->a:F

    .line 50
    invoke-super {p0, v0}, Landroid/view/animation/DecelerateInterpolator;->getInterpolation(F)F

    move-result v0

    iget v1, p0, Layy;->a:F

    sub-float v1, p1, v1

    .line 49
    const/high16 v2, 0x3f800000    # 1.0f

    sub-float/2addr v2, v0

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 52
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/view/animation/DecelerateInterpolator;->getInterpolation(F)F

    move-result v0

    goto :goto_0
.end method
