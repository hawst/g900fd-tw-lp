.class public final Lbmj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbmi;


# instance fields
.field public final a:F

.field public final b:F

.field public final c:F

.field public final d:F

.field public final e:F


# direct methods
.method public constructor <init>(FFFFF)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    const-string v0, "cropLeft"

    invoke-static {p1, v0, v3, v4}, Lcec;->a(FLjava/lang/CharSequence;FF)F

    move-result v0

    iput v0, p0, Lbmj;->a:F

    .line 53
    const-string v0, "cropRight"

    invoke-static {p2, v0, v3, v4}, Lcec;->a(FLjava/lang/CharSequence;FF)F

    move-result v0

    iput v0, p0, Lbmj;->b:F

    .line 54
    const-string v0, "cropTop"

    invoke-static {p3, v0, v3, v4}, Lcec;->a(FLjava/lang/CharSequence;FF)F

    move-result v0

    iput v0, p0, Lbmj;->c:F

    .line 55
    const-string v0, "cropBottom"

    invoke-static {p4, v0, v3, v4}, Lcec;->a(FLjava/lang/CharSequence;FF)F

    move-result v0

    iput v0, p0, Lbmj;->d:F

    .line 56
    iput p5, p0, Lbmj;->e:F

    .line 57
    add-float v0, p1, p2

    cmpg-float v0, v0, v4

    if-gez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "(cropLeft + cropRight) must be less than 1"

    invoke-static {v0, v3}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 58
    add-float v0, p3, p4

    cmpg-float v0, v0, v4

    if-gez v0, :cond_1

    :goto_1
    const-string v0, "(cropTop + cropBottom) must be less than 1"

    invoke-static {v1, v0}, Lcec;->a(ZLjava/lang/CharSequence;)V

    .line 59
    return-void

    :cond_0
    move v0, v2

    .line 57
    goto :goto_0

    :cond_1
    move v1, v2

    .line 58
    goto :goto_1
.end method

.method public static a(Loog;F)Lbmj;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 29
    iget-object v1, p0, Loog;->c:Ljava/lang/Float;

    if-eqz v1, :cond_0

    iget-object v1, p0, Loog;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    .line 30
    :goto_0
    iget-object v2, p0, Loog;->d:Ljava/lang/Float;

    if-eqz v2, :cond_1

    iget-object v2, p0, Loog;->d:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    .line 31
    :goto_1
    iget-object v3, p0, Loog;->e:Ljava/lang/Float;

    if-eqz v3, :cond_2

    iget-object v3, p0, Loog;->e:Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v3

    .line 32
    :goto_2
    iget-object v4, p0, Loog;->f:Ljava/lang/Float;

    if-eqz v4, :cond_3

    iget-object v0, p0, Loog;->f:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v4

    .line 33
    :goto_3
    new-instance v0, Lbmj;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lbmj;-><init>(FFFFF)V

    return-object v0

    :cond_0
    move v1, v0

    .line 29
    goto :goto_0

    :cond_1
    move v2, v0

    .line 30
    goto :goto_1

    :cond_2
    move v3, v0

    .line 31
    goto :goto_2

    :cond_3
    move v4, v0

    .line 32
    goto :goto_3
.end method


# virtual methods
.method public a(JJ)Lbmj;
    .locals 0

    .prologue
    .line 64
    return-object p0
.end method

.method public a(Landroid/graphics/Matrix;)V
    .locals 7

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/high16 v6, 0x3f000000    # 0.5f

    const/high16 v4, -0x41000000    # -0.5f

    const/4 v3, 0x0

    .line 75
    iget v0, p0, Lbmj;->a:F

    sub-float v0, v2, v0

    iget v1, p0, Lbmj;->b:F

    sub-float/2addr v0, v1

    .line 76
    iget v1, p0, Lbmj;->c:F

    sub-float v1, v2, v1

    iget v2, p0, Lbmj;->d:F

    sub-float/2addr v1, v2

    .line 80
    iget v2, p0, Lbmj;->e:F

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    .line 82
    invoke-virtual {p1, v0, v1, v3, v3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 83
    iget v0, p0, Lbmj;->a:F

    iget v1, p0, Lbmj;->d:F

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 92
    :goto_0
    return-void

    .line 86
    :cond_0
    invoke-virtual {p1, v4, v4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 87
    invoke-virtual {p1, v0, v1, v3, v3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 88
    iget v2, p0, Lbmj;->e:F

    neg-float v2, v2

    const/high16 v3, 0x43340000    # 180.0f

    mul-float/2addr v2, v3

    float-to-double v2, v2

    const-wide v4, 0x400921fb54442d18L    # Math.PI

    div-double/2addr v2, v4

    double-to-float v2, v2

    invoke-virtual {p1, v2}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 90
    iget v2, p0, Lbmj;->a:F

    mul-float/2addr v0, v6

    add-float/2addr v0, v2

    iget v2, p0, Lbmj;->d:F

    mul-float/2addr v1, v6

    add-float/2addr v1, v2

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    goto :goto_0
.end method
