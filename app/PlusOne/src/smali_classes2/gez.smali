.class public final Lgez;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lgfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgfo",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static b:Lgft;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgft",
            "<",
            "Lgky;",
            ">;"
        }
    .end annotation
.end field

.field private static c:Lgfs;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lgfs",
            "<",
            "Lgky;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private static d:Lgfb;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    new-instance v0, Lgft;

    invoke-direct {v0}, Lgft;-><init>()V

    sput-object v0, Lgez;->b:Lgft;

    new-instance v0, Lgfa;

    invoke-direct {v0}, Lgfa;-><init>()V

    sput-object v0, Lgez;->c:Lgfs;

    new-instance v0, Lgfo;

    sget-object v1, Lgez;->c:Lgfs;

    sget-object v2, Lgez;->b:Lgft;

    const/4 v3, 0x0

    new-array v3, v3, [Lggh;

    invoke-direct {v0, v1, v2, v3}, Lgfo;-><init>(Lgfs;Lgft;[Lggh;)V

    sput-object v0, Lgez;->a:Lgfo;

    new-instance v0, Lgkx;

    invoke-direct {v0}, Lgkx;-><init>()V

    sput-object v0, Lgez;->d:Lgfb;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    sget-object v0, Lgez;->d:Lgfb;

    invoke-static {}, Lgip;->a()Lgio;

    invoke-direct {p0, p1, v0}, Lgez;-><init>(Landroid/content/Context;Lgfb;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lgfb;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    invoke-direct {p0, p1}, Lgez;->a(Landroid/content/Context;)I

    return-void
.end method

.method private a(Landroid/content/Context;)I
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v0, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    const-string v1, "ClearcutLogger"

    const-string v2, "This can\'t happen."

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
