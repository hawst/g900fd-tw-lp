.class public final Lbqp;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field public a:Lbmm;

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    return-void
.end method

.method public constructor <init>(Lbqp;)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iget-object v0, p1, Lbqp;->a:Lbmm;

    iput-object v0, p0, Lbqp;->a:Lbmm;

    .line 51
    iget v0, p1, Lbqp;->b:I

    iput v0, p0, Lbqp;->b:I

    .line 52
    iget v0, p1, Lbqp;->c:I

    iput v0, p0, Lbqp;->c:I

    .line 53
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 57
    if-ne p1, p0, :cond_1

    .line 64
    :cond_0
    :goto_0
    return v0

    .line 60
    :cond_1
    instance-of v2, p1, Lbqp;

    if-nez v2, :cond_2

    move v0, v1

    .line 61
    goto :goto_0

    .line 63
    :cond_2
    check-cast p1, Lbqp;

    .line 64
    iget-object v2, p0, Lbqp;->a:Lbmm;

    iget-object v3, p1, Lbqp;->a:Lbmm;

    invoke-static {v2, v3}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lbqp;->b:I

    iget v3, p1, Lbqp;->b:I

    if-ne v2, v3, :cond_3

    iget v2, p0, Lbqp;->c:I

    iget v3, p1, Lbqp;->c:I

    if-eq v2, v3, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lbqp;->a:Lbmm;

    const/16 v1, 0x11

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Object;I)I

    move-result v0

    .line 72
    iget v1, p0, Lbqp;->b:I

    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, v1

    .line 73
    iget v1, p0, Lbqp;->c:I

    mul-int/lit8 v0, v0, 0x1f

    add-int/2addr v0, v1

    .line 74
    return v0
.end method
