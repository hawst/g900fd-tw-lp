.class public final Lgeh;
.super Ljava/lang/Object;

# interfaces
.implements Lgdu;


# instance fields
.field final a:Ljava/lang/Object;

.field final b:Lgkr;

.field final c:Lgep;

.field d:Lgeo;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lgeh;->a:Ljava/lang/Object;

    new-instance v0, Lgep;

    invoke-direct {v0, p0}, Lgep;-><init>(Lgeh;)V

    iput-object v0, p0, Lgeh;->c:Lgep;

    new-instance v0, Lgei;

    invoke-direct {v0, p0}, Lgei;-><init>(Lgeh;)V

    iput-object v0, p0, Lgeh;->b:Lgkr;

    iget-object v0, p0, Lgeh;->b:Lgkr;

    iget-object v1, p0, Lgeh;->c:Lgep;

    iput-object v1, v0, Lgka;->c:Lgkt;

    iget-object v1, v0, Lgka;->c:Lgkt;

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lgka;->d()V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()J
    .locals 4

    iget-object v1, p0, Lgeh;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lgeh;->b:Lgkr;

    invoke-virtual {v0}, Lgkr;->e()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lgfz;)Lggd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfz;",
            ")",
            "Lggd",
            "<",
            "Lgen;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lgeh;->a(Lgfz;Lorg/json/JSONObject;)Lggd;

    move-result-object v0

    return-object v0
.end method

.method public a(Lgfz;JILorg/json/JSONObject;)Lggd;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfz;",
            "JI",
            "Lorg/json/JSONObject;",
            ")",
            "Lggd",
            "<",
            "Lgen;",
            ">;"
        }
    .end annotation

    new-instance v1, Lgem;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move v6, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lgem;-><init>(Lgeh;Lgfz;JILorg/json/JSONObject;)V

    invoke-interface {p1, v1}, Lgfz;->b(Lgfx;)Lgfx;

    move-result-object v0

    return-object v0
.end method

.method public a(Lgfz;Lgeb;ZJLorg/json/JSONObject;)Lggd;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfz;",
            "Lgeb;",
            "ZJ",
            "Lorg/json/JSONObject;",
            ")",
            "Lggd",
            "<",
            "Lgen;",
            ">;"
        }
    .end annotation

    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    move-object v7, p6

    invoke-virtual/range {v0 .. v7}, Lgeh;->a(Lgfz;Lgeb;ZJ[JLorg/json/JSONObject;)Lggd;

    move-result-object v0

    return-object v0
.end method

.method public a(Lgfz;Lgeb;ZJ[JLorg/json/JSONObject;)Lggd;
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfz;",
            "Lgeb;",
            "ZJ[J",
            "Lorg/json/JSONObject;",
            ")",
            "Lggd",
            "<",
            "Lgen;",
            ">;"
        }
    .end annotation

    new-instance v1, Lgej;

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move-wide v6, p4

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v1 .. v9}, Lgej;-><init>(Lgeh;Lgfz;Lgeb;ZJ[JLorg/json/JSONObject;)V

    invoke-interface {p1, v1}, Lgfz;->b(Lgfx;)Lgfx;

    move-result-object v0

    return-object v0
.end method

.method public a(Lgfz;Lorg/json/JSONObject;)Lggd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfz;",
            "Lorg/json/JSONObject;",
            ")",
            "Lggd",
            "<",
            "Lgen;",
            ">;"
        }
    .end annotation

    new-instance v0, Lgek;

    invoke-direct {v0, p0, p1, p2}, Lgek;-><init>(Lgeh;Lgfz;Lorg/json/JSONObject;)V

    invoke-interface {p1, v0}, Lgfz;->b(Lgfx;)Lgfx;

    move-result-object v0

    return-object v0
.end method

.method public a(Lgeo;)V
    .locals 0

    iput-object p1, p0, Lgeh;->d:Lgeo;

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-object v1, p0, Lgeh;->b:Lgkr;

    iget-object v0, v1, Lgkr;->a:Lgkq;

    const-string v2, "message received: %s"

    new-array v3, v8, [Ljava/lang/Object;

    aput-object p2, v3, v7

    invoke-virtual {v0, v2, v3}, Lgkq;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v2, "type"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "requestId"

    const-wide/16 v4, -0x1

    invoke-virtual {v0, v3, v4, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v4

    const-string v3, "MEDIA_STATUS"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v2, "status"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-lez v2, :cond_1

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v1, v4, v5, v0}, Lgkr;->a(JLorg/json/JSONObject;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    iput-object v0, v1, Lgkr;->d:Lgef;

    invoke-virtual {v1}, Lgkr;->a()V

    invoke-virtual {v1}, Lgkr;->b()V

    iget-object v0, v1, Lgkr;->f:Lgkv;

    const/4 v2, 0x0

    invoke-virtual {v0, v4, v5, v2}, Lgkv;->a(JI)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, v1, Lgkr;->a:Lgkq;

    const-string v2, "Message is malformed (%s); ignoring: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v7

    aput-object p2, v3, v8

    invoke-virtual {v1, v2, v3}, Lgkq;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    :try_start_1
    const-string v3, "INVALID_PLAYER_STATE"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v2, v1, Lgkr;->a:Lgkq;

    const-string v3, "received unexpected error: Invalid Player State."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v6}, Lgkq;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v2, "customData"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    iget-object v0, v1, Lgkr;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkv;

    const/16 v6, 0x834

    invoke-virtual {v0, v4, v5, v6, v2}, Lgkv;->a(JILorg/json/JSONObject;)Z

    goto :goto_1

    :cond_3
    const-string v3, "LOAD_FAILED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string v2, "customData"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v2, v1, Lgkr;->e:Lgkv;

    const/16 v3, 0x834

    invoke-virtual {v2, v4, v5, v3, v0}, Lgkv;->a(JILorg/json/JSONObject;)Z

    goto :goto_0

    :cond_4
    const-string v3, "LOAD_CANCELLED"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v2, "customData"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v2, v1, Lgkr;->e:Lgkv;

    const/16 v3, 0x835

    invoke-virtual {v2, v4, v5, v3, v0}, Lgkv;->a(JILorg/json/JSONObject;)Z

    goto :goto_0

    :cond_5
    const-string v3, "INVALID_REQUEST"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v1, Lgkr;->a:Lgkq;

    const-string v3, "received unexpected error: Invalid Request."

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v6}, Lgkq;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v2, "customData"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    iget-object v0, v1, Lgkr;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lgkv;

    const/16 v6, 0x834

    invoke-virtual {v0, v4, v5, v6, v2}, Lgkv;->a(JILorg/json/JSONObject;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public b()J
    .locals 4

    iget-object v1, p0, Lgeh;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lgeh;->b:Lgkr;

    invoke-virtual {v0}, Lgkr;->f()J

    move-result-wide v2

    monitor-exit v1

    return-wide v2

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public b(Lgfz;)Lggd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfz;",
            ")",
            "Lggd",
            "<",
            "Lgen;",
            ">;"
        }
    .end annotation

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lgeh;->b(Lgfz;Lorg/json/JSONObject;)Lggd;

    move-result-object v0

    return-object v0
.end method

.method public b(Lgfz;Lorg/json/JSONObject;)Lggd;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lgfz;",
            "Lorg/json/JSONObject;",
            ")",
            "Lggd",
            "<",
            "Lgen;",
            ">;"
        }
    .end annotation

    new-instance v0, Lgel;

    invoke-direct {v0, p0, p1, p2}, Lgel;-><init>(Lgeh;Lgfz;Lorg/json/JSONObject;)V

    invoke-interface {p1, v0}, Lgfz;->b(Lgfx;)Lgfx;

    move-result-object v0

    return-object v0
.end method

.method public c()Lgef;
    .locals 2

    iget-object v1, p0, Lgeh;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lgeh;->b:Lgkr;

    invoke-virtual {v0}, Lgkr;->g()Lgef;

    move-result-object v0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lgeh;->b:Lgkr;

    iget-object v0, v0, Lgka;->b:Ljava/lang/String;

    return-object v0
.end method
