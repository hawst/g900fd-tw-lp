.class public final Leec;
.super Legi;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Llgs;


# instance fields
.field private N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

.field private ah:Lezm;

.field private ai:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private aj:Ljava/lang/Integer;

.field private ak:Ljava/lang/String;

.field private al:Ljava/lang/String;

.field private am:Ljava/lang/String;

.field private final an:Licq;

.field private final ao:Lfhh;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 55
    invoke-direct {p0}, Legi;-><init>()V

    .line 82
    new-instance v0, Licq;

    iget-object v1, p0, Leec;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    const v1, 0x7f0a064d

    .line 83
    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    move-result-object v0

    iput-object v0, p0, Leec;->an:Licq;

    .line 85
    new-instance v0, Leed;

    invoke-direct {v0, p0}, Leed;-><init>(Leec;)V

    iput-object v0, p0, Leec;->ao:Lfhh;

    .line 102
    new-instance v0, Lcsc;

    iget-object v1, p0, Leec;->av:Llqm;

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, v2}, Lcsc;-><init>(Lu;Llqr;I)V

    .line 103
    return-void
.end method

.method static synthetic a(Leec;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Leec;->aj:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic a(Leec;Ljava/lang/Integer;)Ljava/lang/Integer;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Leec;->aj:Ljava/lang/Integer;

    return-object p1
.end method

.method static synthetic a(Leec;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Leec;->ak:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Leec;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 55
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Leec;->U()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Leec;->an:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    :goto_0
    invoke-virtual {p0, p1}, Leec;->e(Landroid/view/View;)V

    invoke-virtual {p0}, Leec;->Z_()V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Leec;->an:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_0
.end method

.method static synthetic b(Leec;)Llnl;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Leec;->at:Llnl;

    return-object v0
.end method

.method static synthetic c(Leec;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Leec;->am:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Leec;)Lezm;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Leec;->ah:Lezm;

    return-object v0
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 363
    sget-object v0, Lhmw;->am:Lhmw;

    return-object v0
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 357
    iget-object v0, p0, Leec;->ah:Lezm;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 358
    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 357
    :cond_1
    iget-object v0, p0, Leec;->ah:Lezm;

    invoke-virtual {v0}, Lezm;->a()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 358
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public V()Z
    .locals 1

    .prologue
    .line 349
    iget-boolean v0, p0, Leec;->ac:Z

    if-eqz v0, :cond_0

    .line 350
    invoke-super {p0}, Legi;->a()Z

    move-result v0

    .line 352
    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Legi;->V()Z

    move-result v0

    goto :goto_0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 149
    iget-object v0, p0, Leec;->at:Llnl;

    invoke-virtual {p1, v0}, Landroid/view/LayoutInflater;->cloneInContext(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0400d9

    invoke-super {p0, v0, p2, p3, v1}, Legi;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v1

    .line 151
    iget-object v0, p0, Leec;->at:Llnl;

    invoke-virtual {v0}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0291

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 153
    new-instance v0, Ljvl;

    iget-object v3, p0, Leec;->at:Llnl;

    invoke-direct {v0, v3}, Ljvl;-><init>(Landroid/content/Context;)V

    iget v3, v0, Ljvl;->a:I

    .line 155
    const v0, 0x7f100304

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iput-object v0, p0, Leec;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    .line 157
    if-eqz p3, :cond_3

    .line 158
    const-string v0, "title"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leec;->ak:Ljava/lang/String;

    .line 159
    iget-object v0, p0, Leec;->am:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 160
    const-string v0, "view_id"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leec;->am:Ljava/lang/String;

    .line 162
    :cond_0
    const-string v0, "delete_request"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    const-string v0, "delete_request"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leec;->aj:Ljava/lang/Integer;

    .line 170
    :cond_1
    :goto_0
    iget-object v0, p0, Leec;->am:Ljava/lang/String;

    invoke-static {v0}, Ljvj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 171
    invoke-static {v0}, Ljvj;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leec;->al:Ljava/lang/String;

    .line 173
    new-instance v0, Lezm;

    iget-object v4, p0, Leec;->at:Llnl;

    iget-object v5, p0, Leec;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iget-object v6, p0, Leec;->am:Ljava/lang/String;

    invoke-direct {v0, v4, v9, v5, v6}, Lezm;-><init>(Landroid/content/Context;Landroid/database/Cursor;Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;Ljava/lang/String;)V

    iput-object v0, p0, Leec;->ah:Lezm;

    .line 174
    iget-object v0, p0, Leec;->ah:Lezm;

    invoke-virtual {v0, p0}, Lezm;->a(Landroid/view/View$OnClickListener;)V

    .line 175
    iget-object v0, p0, Leec;->ah:Lezm;

    invoke-virtual {v0, p0}, Lezm;->a(Landroid/view/View$OnLongClickListener;)V

    .line 177
    iget-object v0, p0, Leec;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0, v8}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Z)V

    .line 178
    iget-object v0, p0, Leec;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(I)V

    .line 180
    iget-object v0, p0, Leec;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    neg-int v2, v2

    invoke-virtual {v0, v7, v2, v7, v7}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->setPadding(IIII)V

    .line 181
    iget-object v0, p0, Leec;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0, v3}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(I)V

    .line 182
    iget-object v0, p0, Leec;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0, v8}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(Z)V

    .line 183
    iget-object v0, p0, Leec;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    const v2, 0x7f020415

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g(I)V

    .line 184
    iget-object v0, p0, Leec;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    iget-object v2, p0, Leec;->ah:Lezm;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Landroid/widget/ListAdapter;)V

    .line 185
    iget-object v0, p0, Leec;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Llkc;)V

    .line 187
    invoke-virtual {p0}, Leec;->w()Lbb;

    move-result-object v0

    iget-object v2, p0, Leec;->ai:Lbc;

    invoke-virtual {v0, v8, v9, v2}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    move-result-object v0

    .line 188
    invoke-virtual {v0}, Ldo;->t()V

    .line 192
    if-nez p3, :cond_2

    invoke-virtual {p0}, Leec;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "photo_picker_mode"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 193
    iget-object v0, p0, Leec;->R:Lcto;

    invoke-virtual {p0}, Leec;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "photo_picker_mode"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {v0, v2}, Lcto;->a(I)V

    .line 196
    :cond_2
    return-object v1

    .line 166
    :cond_3
    iget-object v0, p0, Leec;->am:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 167
    invoke-virtual {p0}, Leec;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "cluster_id"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Leec;->am:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 400
    packed-switch p1, :pswitch_data_0

    .line 425
    invoke-super {p0, p1, p2, p3}, Legi;->a(IILandroid/content/Intent;)V

    .line 429
    :cond_0
    :goto_0
    return-void

    .line 402
    :pswitch_0
    if-eqz p2, :cond_0

    .line 403
    invoke-virtual {p0}, Leec;->n()Lz;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lz;->setResult(ILandroid/content/Intent;)V

    .line 404
    invoke-virtual {p0}, Leec;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    goto :goto_0

    .line 410
    :pswitch_1
    if-eqz p3, :cond_0

    const-string v0, "view_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 411
    const-string v0, "view_id"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 413
    iget-object v1, p0, Leec;->am:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 414
    iget-object v1, p0, Leec;->am:Ljava/lang/String;

    invoke-static {v1}, Ljvj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 415
    invoke-static {v1}, Ljvj;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Leec;->al:Ljava/lang/String;

    .line 416
    iput-object v0, p0, Leec;->am:Ljava/lang/String;

    .line 417
    invoke-virtual {p0}, Leec;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Leec;->ai:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    move-result-object v0

    .line 418
    invoke-virtual {v0}, Ldo;->t()V

    goto :goto_0

    .line 400
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 114
    invoke-super {p0, p1}, Legi;->a(Landroid/os/Bundle;)V

    .line 116
    new-instance v0, Leee;

    invoke-direct {v0, p0}, Leee;-><init>(Leec;)V

    iput-object v0, p0, Leec;->ai:Lbc;

    .line 144
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 294
    const-string v0, "dialog_delete_folder"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Leec;->at:Llnl;

    iget-object v1, p0, Leec;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Leec;->al:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/apps/plus/service/EsService;->h(Landroid/content/Context;ILjava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Leec;->aj:Ljava/lang/Integer;

    .line 296
    iget-object v0, p0, Leec;->T:Lcmz;

    const v1, 0x7f0a0684

    invoke-virtual {p0, v1}, Leec;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcmz;->a(Ljava/lang/String;)V

    .line 300
    :goto_0
    return-void

    .line 298
    :cond_0
    invoke-super {p0, p1, p2}, Legi;->a(Landroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 201
    invoke-super {p0, p1}, Legi;->a(Loo;)V

    .line 202
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 203
    return-void
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    .line 314
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 315
    const v1, 0x7f1006cd

    if-ne v0, v1, :cond_1

    .line 316
    invoke-virtual {p0}, Leec;->p()Lae;

    move-result-object v0

    const-string v1, "dialog_delete_folder"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v1

    if-nez v1, :cond_0

    const v1, 0x7f0a0682

    invoke-virtual {p0, v1}, Leec;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a0683

    invoke-virtual {p0, v2}, Leec;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a0802

    invoke-virtual {p0, v3}, Leec;->e_(I)Ljava/lang/String;

    move-result-object v3

    const v4, 0x7f0a0597

    invoke-virtual {p0, v4}, Leec;->e_(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p0, v2}, Llgr;->a(Lu;I)V

    const-string v2, "dialog_delete_folder"

    invoke-virtual {v1, v0, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    .line 317
    :cond_0
    const/4 v0, 0x1

    .line 320
    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Legi;->a(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public aO_()V
    .locals 4

    .prologue
    .line 213
    invoke-super {p0}, Legi;->aO_()V

    .line 215
    iget-object v0, p0, Leec;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Leec;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a()V

    .line 219
    :cond_0
    iget-object v0, p0, Leec;->ao:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 221
    iget-object v0, p0, Leec;->aj:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 222
    iget-object v0, p0, Leec;->aj:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 223
    iget-object v0, p0, Leec;->aj:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 224
    iget-object v1, p0, Leec;->ao:Lfhh;

    iget-object v2, p0, Leec;->aj:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v3, p0, Leec;->P:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    invoke-virtual {v1, v2, v0}, Lfhh;->K(ILfib;)V

    .line 227
    :cond_1
    return-void
.end method

.method public ab_()V
    .locals 0

    .prologue
    .line 344
    invoke-super {p0}, Legi;->ab_()V

    .line 345
    return-void
.end method

.method protected b(Lhjk;)V
    .locals 1

    .prologue
    .line 325
    invoke-super {p0, p1}, Legi;->b(Lhjk;)V

    .line 327
    iget-object v0, p0, Leec;->ak:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Leec;->ak:Ljava/lang/String;

    invoke-interface {p1, v0}, Lhjk;->a(Ljava/lang/CharSequence;)V

    .line 331
    :cond_0
    iget-object v0, p0, Leec;->Y:Lctq;

    invoke-virtual {v0}, Lctq;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Leec;->aa:I

    and-int/lit8 v0, v0, 0x4

    if-nez v0, :cond_1

    .line 333
    invoke-virtual {p0, p1}, Leec;->c(Lhjk;)V

    .line 336
    :cond_1
    iget-boolean v0, p0, Leec;->ac:Z

    if-nez v0, :cond_2

    .line 338
    const v0, 0x7f1006cd

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 340
    :cond_2
    return-void
.end method

.method public c(Landroid/view/View;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 252
    instance-of v0, p1, Lcom/google/android/apps/plus/views/PhotoTileView;

    if-nez v0, :cond_0

    move v0, v1

    .line 289
    :goto_0
    return v0

    .line 256
    :cond_0
    check-cast p1, Lcom/google/android/apps/plus/views/PhotoTileView;

    .line 257
    iget-object v0, p0, Leec;->Y:Lctq;

    invoke-virtual {v0}, Lctq;->c()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 258
    iget-object v0, p0, Leec;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Leec;->at:Llnl;

    invoke-direct {v3, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v4, Lhmv;->dZ:Lhmv;

    .line 259
    invoke-virtual {v3, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v3

    .line 258
    invoke-interface {v0, v3}, Lhms;->a(Lhmr;)V

    .line 260
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PhotoTileView;->l()Lizu;

    move-result-object v0

    invoke-virtual {p0, v0}, Leec;->a(Lizu;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 262
    iget-object v0, p0, Leec;->at:Llnl;

    iget-object v3, p0, Leec;->P:Lhee;

    .line 263
    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    .line 262
    invoke-static {v0, v3}, Leyq;->h(Landroid/content/Context;I)Leyx;

    move-result-object v0

    .line 264
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PhotoTileView;->l()Lizu;

    move-result-object v3

    invoke-virtual {v0, v3}, Leyx;->a(Lizu;)Leyx;

    move-result-object v0

    .line 265
    invoke-virtual {p0}, Leec;->k()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "photo_picker_crop_mode"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v0, v3}, Leyx;->a(I)Leyx;

    move-result-object v0

    .line 267
    invoke-virtual {p0}, Leec;->k()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "photo_min_width"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v0, v3}, Leyx;->b(I)Leyx;

    move-result-object v0

    .line 268
    invoke-virtual {p0}, Leec;->k()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "photo_min_height"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v0, v3}, Leyx;->c(I)Leyx;

    move-result-object v0

    .line 269
    invoke-virtual {v0}, Leyx;->a()Landroid/content/Intent;

    move-result-object v0

    .line 270
    invoke-virtual {p0, v0, v1}, Leec;->a(Landroid/content/Intent;I)V

    :cond_1
    :goto_1
    move v0, v2

    .line 289
    goto :goto_0

    .line 273
    :cond_2
    new-instance v0, Ldew;

    iget-object v3, p0, Leec;->at:Llnl;

    iget-object v4, p0, Leec;->P:Lhee;

    .line 274
    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    invoke-direct {v0, v3, v4}, Ldew;-><init>(Landroid/content/Context;I)V

    iget-object v3, p0, Leec;->at:Llnl;

    .line 275
    invoke-virtual {v0, v3}, Ldew;->a(Landroid/content/Context;)Ldew;

    move-result-object v0

    .line 276
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PhotoTileView;->l()Lizu;

    move-result-object v3

    invoke-virtual {v0, v3}, Ldew;->a(Lizu;)Ldew;

    move-result-object v0

    iget-object v3, p0, Leec;->X:Lctz;

    .line 277
    invoke-virtual {v3}, Lctz;->a()Ljcn;

    move-result-object v3

    invoke-virtual {v0, v3}, Ldew;->a(Ljcn;)Ldew;

    move-result-object v0

    iget-object v3, p0, Leec;->Y:Lctq;

    .line 278
    invoke-virtual {v3}, Lctq;->d()Z

    move-result v3

    invoke-virtual {v0, v3}, Ldew;->a(Z)Ldew;

    move-result-object v0

    iget-object v3, p0, Leec;->am:Ljava/lang/String;

    .line 279
    invoke-virtual {v0, v3}, Ldew;->d(Ljava/lang/String;)Ldew;

    move-result-object v0

    iget-object v3, p0, Leec;->Y:Lctq;

    .line 280
    invoke-virtual {v3}, Lctq;->c()I

    move-result v3

    invoke-virtual {v0, v3}, Ldew;->b(I)Ldew;

    move-result-object v0

    .line 281
    invoke-virtual {v0, v1}, Ldew;->i(Z)Ldew;

    move-result-object v0

    .line 283
    invoke-virtual {p0}, Leec;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v3, "force_return_edit_list"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 282
    invoke-virtual {v0, v1}, Ldew;->g(Z)Ldew;

    move-result-object v0

    .line 284
    invoke-virtual {v0}, Ldew;->a()Landroid/content/Intent;

    move-result-object v1

    .line 285
    iget-object v0, p0, Leec;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Leec;->at:Llnl;

    invoke-direct {v3, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v4, Lhmv;->dZ:Lhmv;

    .line 286
    invoke-virtual {v3, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v3

    .line 285
    invoke-interface {v0, v3}, Lhms;->a(Lhmr;)V

    .line 287
    invoke-virtual {p0, v1, v2}, Leec;->a(Landroid/content/Intent;I)V

    goto :goto_1
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 237
    invoke-super {p0, p1}, Legi;->e(Landroid/os/Bundle;)V

    .line 239
    const-string v0, "view_id"

    iget-object v1, p0, Leec;->am:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    iget-object v0, p0, Leec;->ak:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 242
    const-string v0, "title"

    iget-object v1, p0, Leec;->ak:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    :cond_0
    iget-object v0, p0, Leec;->aj:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 246
    const-string v0, "delete_request"

    iget-object v1, p0, Leec;->aj:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 248
    :cond_1
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 207
    invoke-super {p0}, Legi;->g()V

    .line 208
    iget-object v0, p0, Leec;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Llii;->c(Landroid/view/View;)V

    .line 209
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 231
    invoke-super {p0}, Legi;->h()V

    .line 232
    iget-object v0, p0, Leec;->N:Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    invoke-static {v0}, Llii;->d(Landroid/view/View;)V

    .line 233
    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 304
    instance-of v1, p1, Lcom/google/android/apps/plus/views/PhotoTileView;

    if-eqz v1, :cond_0

    .line 305
    check-cast p1, Lcom/google/android/apps/plus/views/PhotoTileView;

    invoke-virtual {p0, p1}, Leec;->b(Lcom/google/android/apps/plus/views/PhotoTileView;)V

    .line 306
    invoke-virtual {p0, v0}, Leec;->e(I)V

    .line 307
    const/4 v0, 0x1

    .line 309
    :cond_0
    return v0
.end method

.method public z()V
    .locals 1

    .prologue
    .line 107
    invoke-super {p0}, Legi;->z()V

    .line 109
    iget-object v0, p0, Leec;->ao:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 110
    return-void
.end method
