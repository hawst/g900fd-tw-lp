.class public final Lfec;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lhmm;


# instance fields
.field N:Z

.field private O:Landroid/widget/TextView;

.field private P:Landroid/widget/TextView;

.field private Q:Ljmd;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 26
    invoke-direct {p0}, Llol;-><init>()V

    .line 32
    new-instance v0, Lhmf;

    iget-object v1, p0, Lfec;->av:Llqm;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lhmf;-><init>(Llqr;B)V

    .line 33
    return-void
.end method

.method static synthetic a(Lfec;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lfec;->O:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic b(Lfec;)Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lfec;->P:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 49
    iget-object v1, p0, Lfec;->at:Llnl;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 50
    const v2, 0x7f040232

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 51
    if-nez v1, :cond_0

    .line 78
    :goto_0
    return-object v0

    .line 55
    :cond_0
    iget-object v0, p0, Lfec;->at:Llnl;

    invoke-virtual {v0}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 57
    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lfec;->N:Z

    .line 59
    const v0, 0x7f100628

    .line 60
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/WarmWelcomeIllustrationView;

    .line 61
    new-instance v2, Lfed;

    invoke-direct {v2, p0}, Lfed;-><init>(Lfec;)V

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/WarmWelcomeIllustrationView;->a(Lgcc;)V

    .line 70
    const v0, 0x7f100629

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lfec;->O:Landroid/widget/TextView;

    .line 71
    const v0, 0x7f10062a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lfec;->P:Landroid/widget/TextView;

    .line 73
    const v0, 0x7f10062b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 74
    new-instance v2, Lhmi;

    invoke-direct {v2, p0}, Lhmi;-><init>(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 75
    new-instance v2, Lhmk;

    sget-object v3, Lona;->t:Lhmn;

    invoke-direct {v2, v3}, Lhmk;-><init>(Lhmn;)V

    invoke-static {v0, v2}, Lhmo;->a(Landroid/view/View;Lhmk;)Lhmk;

    move-object v0, v1

    .line 78
    goto :goto_0

    .line 57
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public ac_()Lhmk;
    .locals 2

    .prologue
    .line 95
    new-instance v0, Lhmk;

    sget-object v1, Lona;->u:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    return-object v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 41
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 42
    iget-object v0, p0, Lfec;->au:Llnh;

    const-class v1, Lhmm;

    invoke-virtual {v0, v1, p0}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 43
    iget-object v0, p0, Lfec;->au:Llnh;

    const-class v1, Ljmd;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljmd;

    iput-object v0, p0, Lfec;->Q:Ljmd;

    .line 44
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p0}, Lfec;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Ldhv;->f(Landroid/content/Context;)V

    .line 90
    iget-object v0, p0, Lfec;->Q:Ljmd;

    invoke-interface {v0}, Ljmd;->i()V

    .line 91
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 83
    invoke-super {p0}, Llol;->z()V

    .line 84
    invoke-virtual {p0}, Lfec;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Ldhv;->f(Landroid/content/Context;)V

    .line 85
    return-void
.end method
