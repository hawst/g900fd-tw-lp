.class public final Lhvu;
.super Landroid/widget/Button;
.source "PG"

# interfaces
.implements Lhmm;


# instance fields
.field private a:I

.field private b:Lhmk;

.field private c:Lhuz;

.field private d:Lhwp;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 40
    invoke-direct {p0, p1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    .line 55
    invoke-virtual {p0}, Lhvu;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 56
    invoke-static {v0}, Lhwp;->a(Landroid/content/Context;)Lhwp;

    move-result-object v1

    iput-object v1, p0, Lhvu;->d:Lhwp;

    .line 59
    invoke-virtual {p0, v3}, Lhvu;->setMinimumHeight(I)V

    .line 60
    invoke-virtual {p0, v3}, Lhvu;->setMinimumWidth(I)V

    .line 61
    invoke-virtual {p0, v3}, Lhvu;->setMinHeight(I)V

    .line 62
    invoke-virtual {p0, v3}, Lhvu;->setMinWidth(I)V

    .line 64
    const/16 v1, 0xb

    iget-object v2, p0, Lhvu;->d:Lhwp;

    iget v2, v2, Lhwp;->a:I

    invoke-static {v0, p0, v1, v2, v3}, Lkcr;->a(Landroid/content/Context;Landroid/widget/Button;III)V

    .line 68
    new-instance v0, Lhmk;

    sget-object v1, Lonm;->d:Lhmn;

    invoke-direct {v0, v1}, Lhmk;-><init>(Lhmn;)V

    iput-object v0, p0, Lhvu;->b:Lhmk;

    .line 41
    return-void
.end method


# virtual methods
.method public a(Lhum;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 84
    iput v5, p0, Lhvu;->a:I

    invoke-virtual {p0, v1}, Lhvu;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v1}, Lhvu;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, v5, v5, v5, v5}, Lhvu;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    invoke-virtual {p0, v5}, Lhvu;->setCompoundDrawablePadding(I)V

    invoke-virtual {p0, v1}, Lhvu;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0, v5}, Lhvu;->setClickable(Z)V

    iget-object v0, p0, Lhvu;->c:Lhuz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhvu;->c:Lhuz;

    invoke-virtual {v0, v1}, Lhuz;->a(Lhjy;)V

    .line 86
    :cond_0
    if-eqz p1, :cond_5

    .line 87
    invoke-virtual {p1}, Lhum;->a()I

    move-result v0

    iput v0, p0, Lhvu;->a:I

    .line 89
    iget v0, p0, Lhvu;->a:I

    if-gez v0, :cond_1

    .line 90
    iput v5, p0, Lhvu;->a:I

    .line 93
    :cond_1
    iget v0, p0, Lhvu;->a:I

    if-lez v0, :cond_4

    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v0

    iget v1, p0, Lhvu;->a:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhvu;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lhvu;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f110037

    iget v2, p0, Lhvu;->a:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    iget v4, p0, Lhvu;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhvu;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 95
    :goto_0
    const v0, 0x7f02018f

    invoke-virtual {p0, v0, v5, v5, v5}, Lhvu;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 97
    iget-object v0, p0, Lhvu;->d:Lhwp;

    iget v0, v0, Lhwp;->b:I

    invoke-virtual {p0, v0}, Lhvu;->setCompoundDrawablePadding(I)V

    .line 99
    invoke-virtual {p1}, Lhum;->b()Lhjy;

    move-result-object v0

    .line 101
    if-eqz v0, :cond_3

    .line 102
    iget-object v1, p0, Lhvu;->c:Lhuz;

    if-nez v1, :cond_2

    .line 103
    new-instance v1, Lhuz;

    invoke-direct {v1, p0}, Lhuz;-><init>(Landroid/view/View;)V

    iput-object v1, p0, Lhvu;->c:Lhuz;

    .line 106
    :cond_2
    iget-object v1, p0, Lhvu;->c:Lhuz;

    invoke-virtual {v1, v0}, Lhuz;->a(Lhjy;)V

    .line 112
    :cond_3
    :goto_1
    return-void

    .line 93
    :cond_4
    const-string v0, ""

    invoke-virtual {p0, v0}, Lhvu;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lhvu;->getContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0a058a

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lhvu;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 109
    :cond_5
    invoke-virtual {p0, v5}, Lhvu;->setHeight(I)V

    .line 110
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lhvu;->setVisibility(I)V

    goto :goto_1
.end method

.method public ac_()Lhmk;
    .locals 1

    .prologue
    .line 172
    iget-object v0, p0, Lhvu;->b:Lhmk;

    return-object v0
.end method
