.class public final Lmdg;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmlq;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32155
    invoke-direct {p0}, Loxq;-><init>()V

    .line 32158
    iput-object v0, p0, Lmdg;->apiHeader:Llyq;

    .line 32161
    iput-object v0, p0, Lmdg;->a:Lmlq;

    .line 32155
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 32178
    const/4 v0, 0x0

    .line 32179
    iget-object v1, p0, Lmdg;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 32180
    const/4 v0, 0x1

    iget-object v1, p0, Lmdg;->apiHeader:Llyq;

    .line 32181
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 32183
    :cond_0
    iget-object v1, p0, Lmdg;->a:Lmlq;

    if-eqz v1, :cond_1

    .line 32184
    const/4 v1, 0x2

    iget-object v2, p0, Lmdg;->a:Lmlq;

    .line 32185
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32187
    :cond_1
    iget-object v1, p0, Lmdg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 32188
    iput v0, p0, Lmdg;->ai:I

    .line 32189
    return v0
.end method

.method public a(Loxn;)Lmdg;
    .locals 2

    .prologue
    .line 32197
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 32198
    sparse-switch v0, :sswitch_data_0

    .line 32202
    iget-object v1, p0, Lmdg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 32203
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmdg;->ah:Ljava/util/List;

    .line 32206
    :cond_1
    iget-object v1, p0, Lmdg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 32208
    :sswitch_0
    return-object p0

    .line 32213
    :sswitch_1
    iget-object v0, p0, Lmdg;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 32214
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmdg;->apiHeader:Llyq;

    .line 32216
    :cond_2
    iget-object v0, p0, Lmdg;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 32220
    :sswitch_2
    iget-object v0, p0, Lmdg;->a:Lmlq;

    if-nez v0, :cond_3

    .line 32221
    new-instance v0, Lmlq;

    invoke-direct {v0}, Lmlq;-><init>()V

    iput-object v0, p0, Lmdg;->a:Lmlq;

    .line 32223
    :cond_3
    iget-object v0, p0, Lmdg;->a:Lmlq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 32198
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 32166
    iget-object v0, p0, Lmdg;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 32167
    const/4 v0, 0x1

    iget-object v1, p0, Lmdg;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32169
    :cond_0
    iget-object v0, p0, Lmdg;->a:Lmlq;

    if-eqz v0, :cond_1

    .line 32170
    const/4 v0, 0x2

    iget-object v1, p0, Lmdg;->a:Lmlq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32172
    :cond_1
    iget-object v0, p0, Lmdg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 32174
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 32151
    invoke-virtual {p0, p1}, Lmdg;->a(Loxn;)Lmdg;

    move-result-object v0

    return-object v0
.end method
