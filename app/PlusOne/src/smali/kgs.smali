.class public Lkgs;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;
.implements Llqi;
.implements Llrg;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lbc",
        "<TT;>;",
        "Llqi;",
        "Llrg;"
    }
.end annotation


# instance fields
.field private a:I

.field private final b:Lkgr;

.field private final c:Lkgp;

.field private d:Lhpa;

.field private e:Z


# direct methods
.method public constructor <init>(Lkgr;Lkgp;Llqr;)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const/16 v0, 0x1f4

    iput v0, p0, Lkgs;->a:I

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lkgs;->e:Z

    .line 72
    iput-object p1, p0, Lkgs;->b:Lkgr;

    .line 73
    invoke-virtual {p3, p0}, Llqr;->a(Llrg;)Llrg;

    .line 74
    iput-object p2, p0, Lkgs;->c:Lkgp;

    .line 75
    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<TT;>;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 83
    iget-object v0, p0, Lkgs;->b:Lkgr;

    check-cast v0, Lu;

    const v1, 0x7f0a057b

    .line 84
    invoke-virtual {v0, v1}, Lu;->e_(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 83
    invoke-static {v0, v4, v1, v2}, Llgt;->a(Ljava/lang/String;Ljava/lang/String;ZZ)Llgt;

    move-result-object v0

    .line 86
    iget-object v1, p0, Lkgs;->d:Lhpa;

    iget v2, p0, Lkgs;->a:I

    const-string v3, "progress_dialog_tag"

    invoke-virtual {v1, v0, v2, v3}, Lhpa;->a(Lt;ILjava/lang/String;)V

    .line 87
    return-object v4
.end method

.method public a(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 116
    new-instance v0, Lhpa;

    check-cast p1, Lz;

    .line 117
    invoke-virtual {p1}, Lz;->f()Lae;

    move-result-object v1

    invoke-direct {v0, v1}, Lhpa;-><init>(Lae;)V

    iput-object v0, p0, Lkgs;->d:Lhpa;

    .line 118
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<TT;>;)V"
        }
    .end annotation

    .prologue
    .line 112
    return-void
.end method

.method public a(Ldo;Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<TT;>;TT;)V"
        }
    .end annotation

    .prologue
    .line 99
    iget-object v0, p0, Lkgs;->d:Lhpa;

    const-string v1, "progress_dialog_tag"

    invoke-virtual {v0, v1}, Lhpa;->a(Ljava/lang/String;)V

    .line 100
    if-eqz p2, :cond_1

    .line 101
    iget-object v0, p0, Lkgs;->c:Lkgp;

    invoke-virtual {v0}, Lkgp;->c()V

    .line 102
    iget-boolean v0, p0, Lkgs;->e:Z

    if-nez v0, :cond_0

    .line 103
    iget-object v0, p0, Lkgs;->b:Lkgr;

    invoke-interface {v0}, Lkgr;->e()V

    .line 104
    const/4 v0, 0x1

    iput-boolean v0, p0, Lkgs;->e:Z

    .line 109
    :cond_0
    :goto_0
    return-void

    .line 107
    :cond_1
    iget-object v0, p0, Lkgs;->c:Lkgp;

    invoke-virtual {v0}, Lkgp;->d()V

    goto :goto_0
.end method
