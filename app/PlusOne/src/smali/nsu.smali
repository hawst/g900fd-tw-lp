.class public final Lnsu;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Integer;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Long;

.field private d:Ljava/lang/Long;

.field private e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 2074
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 2110
    const/4 v0, 0x0

    .line 2111
    iget-object v1, p0, Lnsu;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 2112
    const/4 v0, 0x1

    iget-object v1, p0, Lnsu;->b:Ljava/lang/Integer;

    .line 2113
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2115
    :cond_0
    iget-object v1, p0, Lnsu;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 2116
    const/4 v1, 0x2

    iget-object v2, p0, Lnsu;->a:Ljava/lang/Integer;

    .line 2117
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2119
    :cond_1
    iget-object v1, p0, Lnsu;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 2120
    const/4 v1, 0x3

    iget-object v2, p0, Lnsu;->c:Ljava/lang/Long;

    .line 2121
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2123
    :cond_2
    iget-object v1, p0, Lnsu;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 2124
    const/4 v1, 0x4

    iget-object v2, p0, Lnsu;->d:Ljava/lang/Long;

    .line 2125
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2127
    :cond_3
    iget-object v1, p0, Lnsu;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 2128
    const/4 v1, 0x5

    iget-object v2, p0, Lnsu;->e:Ljava/lang/String;

    .line 2129
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2131
    :cond_4
    iget-object v1, p0, Lnsu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2132
    iput v0, p0, Lnsu;->ai:I

    .line 2133
    return v0
.end method

.method public a(Loxn;)Lnsu;
    .locals 2

    .prologue
    .line 2141
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2142
    sparse-switch v0, :sswitch_data_0

    .line 2146
    iget-object v1, p0, Lnsu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2147
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnsu;->ah:Ljava/util/List;

    .line 2150
    :cond_1
    iget-object v1, p0, Lnsu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2152
    :sswitch_0
    return-object p0

    .line 2157
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnsu;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 2161
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lnsu;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 2165
    :sswitch_3
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnsu;->c:Ljava/lang/Long;

    goto :goto_0

    .line 2169
    :sswitch_4
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lnsu;->d:Ljava/lang/Long;

    goto :goto_0

    .line 2173
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnsu;->e:Ljava/lang/String;

    goto :goto_0

    .line 2142
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 2089
    iget-object v0, p0, Lnsu;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 2090
    const/4 v0, 0x1

    iget-object v1, p0, Lnsu;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2092
    :cond_0
    iget-object v0, p0, Lnsu;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2093
    const/4 v0, 0x2

    iget-object v1, p0, Lnsu;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2095
    :cond_1
    iget-object v0, p0, Lnsu;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2096
    const/4 v0, 0x3

    iget-object v1, p0, Lnsu;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 2098
    :cond_2
    iget-object v0, p0, Lnsu;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 2099
    const/4 v0, 0x4

    iget-object v1, p0, Lnsu;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 2101
    :cond_3
    iget-object v0, p0, Lnsu;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 2102
    const/4 v0, 0x5

    iget-object v1, p0, Lnsu;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2104
    :cond_4
    iget-object v0, p0, Lnsu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2106
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2070
    invoke-virtual {p0, p1}, Lnsu;->a(Loxn;)Lnsu;

    move-result-object v0

    return-object v0
.end method
