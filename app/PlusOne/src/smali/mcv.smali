.class public final Lmcv;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnlq;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 322
    invoke-direct {p0}, Loxq;-><init>()V

    .line 325
    iput-object v0, p0, Lmcv;->apiHeader:Llyr;

    .line 328
    iput-object v0, p0, Lmcv;->a:Lnlq;

    .line 322
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 345
    const/4 v0, 0x0

    .line 346
    iget-object v1, p0, Lmcv;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 347
    const/4 v0, 0x1

    iget-object v1, p0, Lmcv;->apiHeader:Llyr;

    .line 348
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 350
    :cond_0
    iget-object v1, p0, Lmcv;->a:Lnlq;

    if-eqz v1, :cond_1

    .line 351
    const/4 v1, 0x2

    iget-object v2, p0, Lmcv;->a:Lnlq;

    .line 352
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 354
    :cond_1
    iget-object v1, p0, Lmcv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 355
    iput v0, p0, Lmcv;->ai:I

    .line 356
    return v0
.end method

.method public a(Loxn;)Lmcv;
    .locals 2

    .prologue
    .line 364
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 365
    sparse-switch v0, :sswitch_data_0

    .line 369
    iget-object v1, p0, Lmcv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 370
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmcv;->ah:Ljava/util/List;

    .line 373
    :cond_1
    iget-object v1, p0, Lmcv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 375
    :sswitch_0
    return-object p0

    .line 380
    :sswitch_1
    iget-object v0, p0, Lmcv;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 381
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmcv;->apiHeader:Llyr;

    .line 383
    :cond_2
    iget-object v0, p0, Lmcv;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 387
    :sswitch_2
    iget-object v0, p0, Lmcv;->a:Lnlq;

    if-nez v0, :cond_3

    .line 388
    new-instance v0, Lnlq;

    invoke-direct {v0}, Lnlq;-><init>()V

    iput-object v0, p0, Lmcv;->a:Lnlq;

    .line 390
    :cond_3
    iget-object v0, p0, Lmcv;->a:Lnlq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 365
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 333
    iget-object v0, p0, Lmcv;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 334
    const/4 v0, 0x1

    iget-object v1, p0, Lmcv;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 336
    :cond_0
    iget-object v0, p0, Lmcv;->a:Lnlq;

    if-eqz v0, :cond_1

    .line 337
    const/4 v0, 0x2

    iget-object v1, p0, Lmcv;->a:Lnlq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 339
    :cond_1
    iget-object v0, p0, Lmcv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 341
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 318
    invoke-virtual {p0, p1}, Lmcv;->a(Loxn;)Lmcv;

    move-result-object v0

    return-object v0
.end method
