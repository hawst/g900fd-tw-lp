.class public final Lffq;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lleu;


# static fields
.field private static final a:J


# instance fields
.field private final b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 27
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x18

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lffq;->a:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lffq;->b:Landroid/content/Context;

    .line 38
    return-void
.end method


# virtual methods
.method public a()Llev;
    .locals 4

    .prologue
    .line 41
    new-instance v0, Ller;

    invoke-direct {v0}, Ller;-><init>()V

    const-string v1, "contact_stats"

    .line 42
    invoke-virtual {v0, v1}, Ller;->a(Ljava/lang/String;)Ller;

    move-result-object v0

    const/16 v1, 0x1b

    .line 43
    invoke-virtual {v0, v1}, Ller;->a(I)Ller;

    move-result-object v0

    const/16 v1, 0x1c

    .line 44
    invoke-virtual {v0, v1}, Ller;->b(I)Ller;

    move-result-object v0

    sget-wide v2, Lffq;->a:J

    .line 45
    invoke-virtual {v0, v2, v3}, Ller;->a(J)Ller;

    move-result-object v0

    .line 46
    invoke-virtual {v0}, Ller;->b()Llev;

    move-result-object v0

    return-object v0
.end method

.method public a(Lkfp;ILles;)V
    .locals 4

    .prologue
    .line 51
    iget-object v0, p0, Lffq;->b:Landroid/content/Context;

    const-class v1, Lhei;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 52
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "is_google_plus"

    .line 53
    invoke-interface {v0, v1}, Lhej;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    iget-object v0, p0, Lffq;->b:Landroid/content/Context;

    invoke-static {v0, p2}, Ldhv;->l(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 55
    iget-object v0, p0, Lffq;->b:Landroid/content/Context;

    invoke-static {v0, p2}, Ldhv;->d(Landroid/content/Context;I)J

    move-result-wide v0

    .line 57
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-ltz v2, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sub-long v0, v2, v0

    sget-wide v2, Lffq;->a:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 59
    :cond_0
    iget-object v0, p0, Lffq;->b:Landroid/content/Context;

    invoke-static {v0, p2, p1}, Lffr;->a(Landroid/content/Context;ILkfp;)V

    .line 63
    :cond_1
    return-void
.end method
