.class public final Looe;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Looo;

.field public c:Looa;

.field public d:Lonq;

.field public e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1307
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1332
    const/high16 v0, -0x80000000

    iput v0, p0, Looe;->a:I

    .line 1335
    iput-object v1, p0, Looe;->b:Looo;

    .line 1338
    iput-object v1, p0, Looe;->c:Looa;

    .line 1341
    iput-object v1, p0, Looe;->d:Lonq;

    .line 1307
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1369
    const/4 v0, 0x0

    .line 1370
    iget v1, p0, Looe;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 1371
    const/4 v0, 0x1

    iget v1, p0, Looe;->a:I

    .line 1372
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1374
    :cond_0
    iget-object v1, p0, Looe;->b:Looo;

    if-eqz v1, :cond_1

    .line 1375
    const/4 v1, 0x2

    iget-object v2, p0, Looe;->b:Looo;

    .line 1376
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1378
    :cond_1
    iget-object v1, p0, Looe;->c:Looa;

    if-eqz v1, :cond_2

    .line 1379
    const/4 v1, 0x3

    iget-object v2, p0, Looe;->c:Looa;

    .line 1380
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1382
    :cond_2
    iget-object v1, p0, Looe;->d:Lonq;

    if-eqz v1, :cond_3

    .line 1383
    const/4 v1, 0x4

    iget-object v2, p0, Looe;->d:Lonq;

    .line 1384
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1386
    :cond_3
    iget-object v1, p0, Looe;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 1387
    const/4 v1, 0x5

    iget-object v2, p0, Looe;->e:Ljava/lang/Boolean;

    .line 1388
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1390
    :cond_4
    iget-object v1, p0, Looe;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1391
    iput v0, p0, Looe;->ai:I

    .line 1392
    return v0
.end method

.method public a(Loxn;)Looe;
    .locals 2

    .prologue
    .line 1400
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1401
    sparse-switch v0, :sswitch_data_0

    .line 1405
    iget-object v1, p0, Looe;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1406
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Looe;->ah:Ljava/util/List;

    .line 1409
    :cond_1
    iget-object v1, p0, Looe;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1411
    :sswitch_0
    return-object p0

    .line 1416
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1417
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0x11

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-eq v0, v1, :cond_2

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12

    if-ne v0, v1, :cond_3

    .line 1436
    :cond_2
    iput v0, p0, Looe;->a:I

    goto :goto_0

    .line 1438
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Looe;->a:I

    goto :goto_0

    .line 1443
    :sswitch_2
    iget-object v0, p0, Looe;->b:Looo;

    if-nez v0, :cond_4

    .line 1444
    new-instance v0, Looo;

    invoke-direct {v0}, Looo;-><init>()V

    iput-object v0, p0, Looe;->b:Looo;

    .line 1446
    :cond_4
    iget-object v0, p0, Looe;->b:Looo;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1450
    :sswitch_3
    iget-object v0, p0, Looe;->c:Looa;

    if-nez v0, :cond_5

    .line 1451
    new-instance v0, Looa;

    invoke-direct {v0}, Looa;-><init>()V

    iput-object v0, p0, Looe;->c:Looa;

    .line 1453
    :cond_5
    iget-object v0, p0, Looe;->c:Looa;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1457
    :sswitch_4
    iget-object v0, p0, Looe;->d:Lonq;

    if-nez v0, :cond_6

    .line 1458
    new-instance v0, Lonq;

    invoke-direct {v0}, Lonq;-><init>()V

    iput-object v0, p0, Looe;->d:Lonq;

    .line 1460
    :cond_6
    iget-object v0, p0, Looe;->d:Lonq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1464
    :sswitch_5
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Looe;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    .line 1401
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1348
    iget v0, p0, Looe;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1349
    const/4 v0, 0x1

    iget v1, p0, Looe;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1351
    :cond_0
    iget-object v0, p0, Looe;->b:Looo;

    if-eqz v0, :cond_1

    .line 1352
    const/4 v0, 0x2

    iget-object v1, p0, Looe;->b:Looo;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1354
    :cond_1
    iget-object v0, p0, Looe;->c:Looa;

    if-eqz v0, :cond_2

    .line 1355
    const/4 v0, 0x3

    iget-object v1, p0, Looe;->c:Looa;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1357
    :cond_2
    iget-object v0, p0, Looe;->d:Lonq;

    if-eqz v0, :cond_3

    .line 1358
    const/4 v0, 0x4

    iget-object v1, p0, Looe;->d:Lonq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1360
    :cond_3
    iget-object v0, p0, Looe;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 1361
    const/4 v0, 0x5

    iget-object v1, p0, Looe;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1363
    :cond_4
    iget-object v0, p0, Looe;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1365
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1303
    invoke-virtual {p0, p1}, Looe;->a(Loxn;)Looe;

    move-result-object v0

    return-object v0
.end method
