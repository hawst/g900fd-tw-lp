.class public Lhqa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljlw;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljlw",
        "<",
        "Lhqa;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljlx;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljlx",
            "<",
            "Lhqa;",
            ">;"
        }
    .end annotation
.end field

.field private b:Lhqo;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    new-instance v0, Ljlv;

    invoke-direct {v0, p0}, Ljlv;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lhqa;->a:Ljlx;

    .line 22
    new-instance v0, Lhqb;

    invoke-direct {v0, p0, p1}, Lhqb;-><init>(Lhqa;Landroid/content/Context;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    .line 34
    invoke-virtual {v0, v1}, Lhqb;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 35
    return-void
.end method

.method static synthetic a(Lhqa;Lhqo;)Lhqo;
    .locals 0

    .prologue
    .line 14
    iput-object p1, p0, Lhqa;->b:Lhqo;

    return-object p1
.end method

.method static synthetic a(Lhqa;)Ljlx;
    .locals 1

    .prologue
    .line 14
    iget-object v0, p0, Lhqa;->a:Ljlx;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lhqa;->b:Lhqo;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lhqa;->b:Lhqo;

    invoke-interface {v0, p1}, Lhqo;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljlx;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljlx",
            "<",
            "Lhqa;",
            ">;"
        }
    .end annotation

    .prologue
    .line 39
    iget-object v0, p0, Lhqa;->a:Ljlx;

    return-object v0
.end method

.method public b(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 53
    const/4 v0, 0x0

    .line 54
    iget-object v1, p0, Lhqa;->b:Lhqo;

    if-eqz v1, :cond_0

    .line 55
    iget-object v0, p0, Lhqa;->b:Lhqo;

    invoke-interface {v0, p1}, Lhqo;->a(Ljava/lang/String;)V

    .line 56
    const/4 v0, 0x1

    .line 58
    :cond_0
    iget-object v1, p0, Lhqa;->a:Ljlx;

    invoke-interface {v1}, Ljlx;->a()V

    .line 59
    return v0
.end method

.method public c(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 66
    const/4 v0, 0x0

    .line 67
    iget-object v1, p0, Lhqa;->b:Lhqo;

    if-eqz v1, :cond_0

    .line 68
    iget-object v0, p0, Lhqa;->b:Lhqo;

    invoke-interface {v0, p1}, Lhqo;->b(Ljava/lang/String;)V

    .line 69
    const/4 v0, 0x1

    .line 71
    :cond_0
    iget-object v1, p0, Lhqa;->a:Ljlx;

    invoke-interface {v1}, Ljlx;->a()V

    .line 72
    return v0
.end method
