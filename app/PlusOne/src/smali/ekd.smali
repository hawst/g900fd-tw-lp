.class public final Lekd;
.super Leku;
.source "PG"

# interfaces
.implements Lhob;


# static fields
.field public static final N:[Ljava/lang/String;


# instance fields
.field private O:Ljava/lang/String;

.field private ad:Ljava/lang/String;

.field private ae:Lekh;

.field private af:Lhoc;

.field private ag:Ldvv;

.field private ah:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final ai:Lbc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbc",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 83
    const/16 v0, 0xa

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "person_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "gaia_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "avatar"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "packed_circle_ids"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "blocked"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "profile_type"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "in_same_visibility_group"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "verified"

    aput-object v2, v0, v1

    sput-object v0, Lekd;->N:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 217
    invoke-direct {p0}, Leku;-><init>()V

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lekd;->ah:Ljava/util/ArrayList;

    .line 107
    new-instance v0, Lhnw;

    new-instance v1, Lekj;

    invoke-direct {v1, p0}, Lekj;-><init>(Lekd;)V

    invoke-direct {v0, p0, v1}, Lhnw;-><init>(Llrn;Lhnv;)V

    .line 117
    new-instance v0, Leke;

    invoke-direct {v0, p0}, Leke;-><init>(Lekd;)V

    iput-object v0, p0, Lekd;->ai:Lbc;

    .line 218
    return-void
.end method

.method private X()V
    .locals 3

    .prologue
    .line 517
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 518
    invoke-static {}, Ljpe;->b()V

    .line 520
    :cond_0
    invoke-virtual {p0}, Lekd;->ae()V

    .line 521
    invoke-virtual {p0}, Lekd;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0592

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 522
    return-void
.end method

.method static synthetic a(Lekd;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lekd;->O:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lekd;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lekd;->O:Ljava/lang/String;

    return-object p1
.end method

.method private ah()V
    .locals 3

    .prologue
    .line 525
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 526
    invoke-static {}, Ljpe;->b()V

    .line 528
    :cond_0
    invoke-virtual {p0}, Lekd;->ae()V

    .line 529
    invoke-virtual {p0}, Lekd;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a098d

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 530
    invoke-virtual {p0}, Lekd;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    .line 531
    return-void
.end method

.method static synthetic b(Lekd;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 59
    iput-object p1, p0, Lekd;->ad:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lekd;)Llnl;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lekd;->at:Llnl;

    return-object v0
.end method

.method static synthetic c(Lekd;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lekd;->ah:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lekd;)Llnl;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lekd;->at:Llnl;

    return-object v0
.end method

.method static synthetic e(Lekd;)Lekh;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lekd;->ae:Lekh;

    return-object v0
.end method

.method static synthetic f(Lekd;)Llnl;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lekd;->at:Llnl;

    return-object v0
.end method


# virtual methods
.method public W()V
    .locals 3

    .prologue
    .line 375
    const v0, 0x7f0a0877

    invoke-virtual {p0, v0}, Lekd;->d(I)V

    .line 376
    iget-object v0, p0, Lekd;->Z:Lhee;

    invoke-interface {v0}, Lhee;->d()I

    move-result v0

    .line 378
    invoke-static {}, Ljpe;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 379
    invoke-static {}, Ljpe;->b()V

    .line 381
    :cond_0
    iget-object v1, p0, Lekd;->ag:Ldvv;

    iget-object v2, p0, Lekd;->O:Ljava/lang/String;

    .line 382
    invoke-virtual {v1, v0, v2}, Ldvv;->a(ILjava/lang/String;)Lhny;

    move-result-object v0

    .line 383
    iget-object v1, p0, Lekd;->af:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    .line 384
    return-void
.end method

.method public a()I
    .locals 1

    .prologue
    .line 465
    const/16 v0, 0xa

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 285
    invoke-super {p0, p1, p2, p3}, Leku;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 286
    iget-object v1, p0, Lekd;->ae:Lekh;

    invoke-virtual {p0, v1}, Lekd;->a(Landroid/widget/ListAdapter;)V

    .line 287
    return-object v0
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 256
    packed-switch p1, :pswitch_data_0

    .line 264
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 259
    :pswitch_0
    new-instance v0, Lhxg;

    iget-object v1, p0, Lekd;->at:Llnl;

    iget-object v2, p0, Lekd;->Z:Lhee;

    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    const/4 v3, 0x1

    sget-object v4, Lekd;->P:[Ljava/lang/String;

    iget-object v5, p0, Lekd;->O:Ljava/lang/String;

    .line 261
    invoke-static {v5}, Ldsm;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lhxg;-><init>(Landroid/content/Context;II[Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 256
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 397
    packed-switch p1, :pswitch_data_0

    .line 407
    invoke-super {p0, p1, p2, p3}, Leku;->a(IILandroid/content/Intent;)V

    .line 410
    :cond_0
    :goto_0
    return-void

    .line 399
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 400
    invoke-virtual {p0}, Lekd;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    iget-object v3, p0, Lekd;->ai:Lbc;

    invoke-virtual {v0, v1, v2, v3}, Lbb;->b(ILandroid/os/Bundle;Lbc;)Ldo;

    goto :goto_0

    .line 401
    :cond_1
    const/4 v0, 0x1

    if-ne p2, v0, :cond_0

    .line 402
    invoke-virtual {p0}, Lekd;->n()Lz;

    move-result-object v0

    invoke-static {v0}, Ldyj;->a(Landroid/content/Context;)V

    goto :goto_0

    .line 397
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 4

    .prologue
    .line 291
    iget-object v0, p0, Lekd;->ae:Lekh;

    invoke-virtual {v0, p1}, Lekh;->a(Landroid/database/Cursor;)V

    .line 292
    invoke-virtual {p0}, Lekd;->x()Landroid/view/View;

    invoke-virtual {p0}, Lekd;->aa()V

    .line 296
    new-instance v0, Lekf;

    invoke-direct {v0, p0}, Lekf;-><init>(Lekd;)V

    const-wide/16 v2, 0xfa

    invoke-static {v0, v2, v3}, Llsx;->a(Ljava/lang/Runnable;J)V

    .line 304
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 222
    invoke-super {p0, p1}, Leku;->a(Landroid/os/Bundle;)V

    .line 224
    iget-object v0, p0, Lekd;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 226
    if-eqz p1, :cond_0

    .line 227
    const-string v0, "selected_circle_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lekd;->O:Ljava/lang/String;

    .line 228
    const-string v0, "circle_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lekd;->ad:Ljava/lang/String;

    .line 229
    const-string v0, "shown_person_ids"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lekd;->ah:Ljava/util/ArrayList;

    .line 232
    :cond_0
    new-instance v0, Lekh;

    invoke-virtual {p0}, Lekd;->n()Lz;

    move-result-object v1

    invoke-direct {v0, p0, v1, v3}, Lekh;-><init>(Lekd;Landroid/content/Context;Landroid/database/Cursor;)V

    iput-object v0, p0, Lekd;->ae:Lekh;

    .line 234
    invoke-virtual {p0}, Lekd;->w()Lbb;

    move-result-object v0

    const/4 v1, 0x2

    iget-object v2, p0, Lekd;->ai:Lbc;

    invoke-virtual {v0, v1, v3, v2}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 235
    return-void
.end method

.method public a(Ldo;Landroid/database/Cursor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 269
    invoke-super {p0, p1, p2}, Leku;->a(Ldo;Landroid/database/Cursor;)V

    .line 270
    invoke-virtual {p1}, Ldo;->o()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 280
    :cond_0
    :goto_0
    return-void

    .line 272
    :pswitch_0
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lekd;->O:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 276
    const/4 v0, 0x1

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lekd;->ad:Ljava/lang/String;

    .line 277
    iget-object v0, p0, Lekd;->Q:Lhje;

    invoke-virtual {v0}, Lhje;->a()V

    goto :goto_0

    .line 270
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 59
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lekd;->a(Ldo;Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Lhjk;)V
    .locals 3

    .prologue
    .line 308
    invoke-super {p0, p1}, Leku;->a(Lhjk;)V

    .line 309
    iget-object v0, p0, Lekd;->ad:Ljava/lang/String;

    invoke-interface {p1, v0}, Lhjk;->a(Ljava/lang/CharSequence;)V

    .line 311
    iget-object v0, p0, Lekd;->O:Ljava/lang/String;

    const-string v1, "15"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 312
    const v0, 0x7f1006ae

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 313
    const v0, 0x7f1006af

    invoke-interface {p1, v0}, Lhjk;->b(I)Landroid/view/MenuItem;

    .line 315
    :cond_0
    const v0, 0x7f10053c

    new-instance v1, Litl;

    const-string v2, "plus_circles"

    invoke-direct {v1, v2}, Litl;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lhjk;->a(ILhjx;)Landroid/view/MenuItem;

    .line 316
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 2

    .prologue
    .line 482
    const-string v0, "RemoveCircleTaskLegacy"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 483
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lekd;->X()V

    .line 491
    :cond_0
    :goto_0
    return-void

    .line 483
    :cond_1
    invoke-direct {p0}, Lekd;->ah()V

    goto :goto_0

    .line 484
    :cond_2
    const-string v0, "RemoveCircleTask"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 485
    invoke-static {}, Ljpe;->b()V

    .line 486
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lekd;->X()V

    goto :goto_0

    :cond_3
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Ljpe;->b()V

    :cond_4
    iget-object v0, p0, Lekd;->ag:Ldvv;

    iget-object v1, p0, Lekd;->Z:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-virtual {v0, v1}, Ldvv;->a(I)Lhny;

    move-result-object v0

    iget-object v1, p0, Lekd;->af:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    goto :goto_0

    .line 487
    :cond_5
    const-string v0, "LoadCirclesTask"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 488
    invoke-static {}, Ljpe;->b()V

    .line 489
    invoke-direct {p0}, Lekd;->ah()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V
    .locals 2

    .prologue
    .line 342
    iget-object v0, p0, Lekd;->O:Ljava/lang/String;

    const-string v1, "15"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    invoke-virtual {p0, p1, p2, p4, p5}, Lekd;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 349
    :goto_0
    return-void

    .line 346
    :cond_0
    invoke-super/range {p0 .. p5}, Leku;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    goto :goto_0
.end method

.method public a(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 414
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 415
    const v2, 0x7f1006ae

    if-ne v1, v2, :cond_1

    .line 416
    iget-object v1, p0, Lekd;->O:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v1, Leki;

    invoke-direct {v1}, Leki;-><init>()V

    invoke-virtual {v1, p0, v0}, Leki;->a(Lu;I)V

    invoke-virtual {p0}, Lekd;->p()Lae;

    move-result-object v2

    const-string v3, "delete_circle_conf"

    invoke-virtual {v1, v2, v3}, Leki;->a(Lae;Ljava/lang/String;)V

    .line 423
    :cond_0
    :goto_0
    return v0

    .line 418
    :cond_1
    const v2, 0x7f1006af

    if-ne v1, v2, :cond_2

    .line 419
    iget-object v1, p0, Lekd;->Z:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-virtual {p0}, Lekd;->n()Lz;

    move-result-object v2

    iget-object v3, p0, Lekd;->O:Ljava/lang/String;

    invoke-static {v2, v1, v3}, Leyq;->e(Landroid/content/Context;ILjava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {p0, v1, v2}, Lekd;->a(Landroid/content/Intent;I)V

    goto :goto_0

    .line 423
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected al_()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    .line 320
    iget-object v0, p0, Lekd;->O:Ljava/lang/String;

    const-string v1, "15"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 321
    const v0, 0x7f0a09d2

    invoke-virtual {p0, v0}, Lekd;->e_(I)Ljava/lang/String;

    move-result-object v0

    .line 335
    :goto_0
    return-object v0

    .line 323
    :cond_0
    new-instance v0, Landroid/text/SpannableStringBuilder;

    invoke-direct {v0}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 324
    const v1, 0x7f0a07ef

    invoke-virtual {p0, v1}, Lekd;->e_(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 325
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v1

    .line 326
    const v2, 0x7f0a07f0

    invoke-virtual {p0, v2}, Lekd;->e_(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    .line 327
    new-instance v2, Lekg;

    invoke-direct {v2, p0}, Lekg;-><init>(Lekd;)V

    .line 334
    invoke-virtual {v0}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v3

    const/16 v4, 0x12

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)Z
    .locals 1

    .prologue
    .line 354
    invoke-virtual/range {p0 .. p5}, Lekd;->a(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;I)V

    .line 355
    const/4 v0, 0x1

    return v0
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 239
    invoke-super {p0, p1}, Leku;->c(Landroid/os/Bundle;)V

    .line 241
    invoke-virtual {p0}, Lekd;->h_()Llnh;

    move-result-object v0

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    iput-object v0, p0, Lekd;->af:Lhoc;

    .line 242
    invoke-virtual {p0}, Lekd;->h_()Llnh;

    move-result-object v0

    const-class v1, Ldvv;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvv;

    iput-object v0, p0, Lekd;->ag:Ldvv;

    .line 243
    return-void
.end method

.method protected d()V
    .locals 0

    .prologue
    .line 472
    return-void
.end method

.method protected e()V
    .locals 0

    .prologue
    .line 476
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 247
    invoke-super {p0, p1}, Leku;->e(Landroid/os/Bundle;)V

    .line 249
    const-string v0, "selected_circle_id"

    iget-object v1, p0, Lekd;->O:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    const-string v0, "circle_name"

    iget-object v1, p0, Lekd;->ad:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    const-string v0, "shown_person_ids"

    iget-object v1, p0, Lekd;->ah:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 252
    return-void
.end method
