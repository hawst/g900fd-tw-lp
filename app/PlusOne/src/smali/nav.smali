.class public final Lnav;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Lnyf;

.field private b:Logr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 178
    invoke-direct {p0}, Loxq;-><init>()V

    .line 181
    sget-object v0, Lnyf;->a:[Lnyf;

    iput-object v0, p0, Lnav;->a:[Lnyf;

    .line 184
    const/4 v0, 0x0

    iput-object v0, p0, Lnav;->b:Logr;

    .line 178
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 205
    .line 206
    iget-object v1, p0, Lnav;->a:[Lnyf;

    if-eqz v1, :cond_1

    .line 207
    iget-object v2, p0, Lnav;->a:[Lnyf;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 208
    if-eqz v4, :cond_0

    .line 209
    const/4 v5, 0x1

    .line 210
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 207
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 214
    :cond_1
    iget-object v1, p0, Lnav;->b:Logr;

    if-eqz v1, :cond_2

    .line 215
    const/4 v1, 0x2

    iget-object v2, p0, Lnav;->b:Logr;

    .line 216
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 218
    :cond_2
    iget-object v1, p0, Lnav;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 219
    iput v0, p0, Lnav;->ai:I

    .line 220
    return v0
.end method

.method public a(Loxn;)Lnav;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 228
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 229
    sparse-switch v0, :sswitch_data_0

    .line 233
    iget-object v2, p0, Lnav;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 234
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnav;->ah:Ljava/util/List;

    .line 237
    :cond_1
    iget-object v2, p0, Lnav;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 239
    :sswitch_0
    return-object p0

    .line 244
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 245
    iget-object v0, p0, Lnav;->a:[Lnyf;

    if-nez v0, :cond_3

    move v0, v1

    .line 246
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnyf;

    .line 247
    iget-object v3, p0, Lnav;->a:[Lnyf;

    if-eqz v3, :cond_2

    .line 248
    iget-object v3, p0, Lnav;->a:[Lnyf;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 250
    :cond_2
    iput-object v2, p0, Lnav;->a:[Lnyf;

    .line 251
    :goto_2
    iget-object v2, p0, Lnav;->a:[Lnyf;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 252
    iget-object v2, p0, Lnav;->a:[Lnyf;

    new-instance v3, Lnyf;

    invoke-direct {v3}, Lnyf;-><init>()V

    aput-object v3, v2, v0

    .line 253
    iget-object v2, p0, Lnav;->a:[Lnyf;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 254
    invoke-virtual {p1}, Loxn;->a()I

    .line 251
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 245
    :cond_3
    iget-object v0, p0, Lnav;->a:[Lnyf;

    array-length v0, v0

    goto :goto_1

    .line 257
    :cond_4
    iget-object v2, p0, Lnav;->a:[Lnyf;

    new-instance v3, Lnyf;

    invoke-direct {v3}, Lnyf;-><init>()V

    aput-object v3, v2, v0

    .line 258
    iget-object v2, p0, Lnav;->a:[Lnyf;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 262
    :sswitch_2
    iget-object v0, p0, Lnav;->b:Logr;

    if-nez v0, :cond_5

    .line 263
    new-instance v0, Logr;

    invoke-direct {v0}, Logr;-><init>()V

    iput-object v0, p0, Lnav;->b:Logr;

    .line 265
    :cond_5
    iget-object v0, p0, Lnav;->b:Logr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 229
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 189
    iget-object v0, p0, Lnav;->a:[Lnyf;

    if-eqz v0, :cond_1

    .line 190
    iget-object v1, p0, Lnav;->a:[Lnyf;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 191
    if-eqz v3, :cond_0

    .line 192
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 190
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 196
    :cond_1
    iget-object v0, p0, Lnav;->b:Logr;

    if-eqz v0, :cond_2

    .line 197
    const/4 v0, 0x2

    iget-object v1, p0, Lnav;->b:Logr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 199
    :cond_2
    iget-object v0, p0, Lnav;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 201
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 174
    invoke-virtual {p0, p1}, Lnav;->a(Loxn;)Lnav;

    move-result-object v0

    return-object v0
.end method
