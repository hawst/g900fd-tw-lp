.class final Lkna;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/os/Parcelable$Creator",
        "<",
        "Lkmz;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 214
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lkmz;
    .locals 6

    .prologue
    .line 218
    const/4 v2, 0x0

    .line 220
    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v1

    .line 221
    invoke-virtual {p1}, Landroid/os/Parcel;->readBundle()Landroid/os/Bundle;

    move-result-object v3

    .line 222
    const-class v0, Lkng;

    .line 223
    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    .line 222
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lkng;

    .line 225
    if-eqz v1, :cond_0

    .line 227
    :try_start_0
    new-instance v4, Locz;

    invoke-direct {v4}, Locz;-><init>()V

    .line 228
    invoke-static {v4, v1}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v1

    check-cast v1, Locz;

    .line 229
    invoke-static {v1}, Lknb;->a(Locz;)Lkmz;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 234
    :goto_0
    if-nez v1, :cond_1

    invoke-static {v3, v0}, Lknb;->a(Landroid/os/Bundle;Lkng;)Lkmz;

    move-result-object v0

    :goto_1
    return-object v0

    .line 230
    :catch_0
    move-exception v1

    .line 231
    const-string v4, "ApiaryActivity"

    const-string v5, "Failed to parse LinkPreviewResponse from Parcel"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    move-object v1, v2

    goto :goto_0

    :cond_1
    move-object v0, v1

    .line 234
    goto :goto_1
.end method

.method public a(I)[Lkmz;
    .locals 1

    .prologue
    .line 240
    new-array v0, p1, [Lkmz;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 214
    invoke-virtual {p0, p1}, Lkna;->a(Landroid/os/Parcel;)Lkmz;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 214
    invoke-virtual {p0, p1}, Lkna;->a(I)[Lkmz;

    move-result-object v0

    return-object v0
.end method
