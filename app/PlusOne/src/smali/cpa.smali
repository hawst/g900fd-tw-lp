.class public final Lcpa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lctm;


# static fields
.field private static final a:Ljava/text/SimpleDateFormat;

.field private static b:Z

.field private static c:I


# instance fields
.field private final d:Lcsp;

.field private final e:Lcsq;

.field private final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 28
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "LLL"

    .line 29
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lcpa;->a:Ljava/text/SimpleDateFormat;

    .line 28
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcsp;Lcsq;Ljava/util/Date;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-interface {p3}, Lcsq;->a()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v1

    :goto_0
    const-string v3, "RowItemProvider must not be empty"

    invoke-static {v0, v3}, Llsk;->a(ZLjava/lang/Object;)V

    .line 58
    invoke-virtual {p2}, Lcsp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/16 v3, 0x18

    if-gt v0, v3, :cond_0

    move v2, v1

    .line 59
    :cond_0
    invoke-virtual {p2}, Lcsp;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x25

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "RowInfo has too many ids: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 58
    invoke-static {v2, v0}, Llsk;->a(ZLjava/lang/Object;)V

    .line 60
    iput-object p2, p0, Lcpa;->d:Lcsp;

    .line 61
    iput-object p3, p0, Lcpa;->e:Lcsq;

    .line 63
    sget-object v0, Lcpa;->a:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0, p4}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcpa;->f:Ljava/lang/String;

    .line 65
    sget-boolean v0, Lcpa;->b:Z

    if-nez v0, :cond_1

    sput-boolean v1, Lcpa;->b:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0385

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Lcpa;->c:I

    .line 66
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 55
    goto :goto_0
.end method

.method public static b()I
    .locals 1

    .prologue
    .line 50
    const/16 v0, 0x18

    return v0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 109
    sget-object v0, Lcoq;->b:Lcoq;

    invoke-virtual {v0}, Lcoq;->ordinal()I

    move-result v0

    return v0
.end method

.method public a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 70
    .line 71
    if-nez p1, :cond_0

    .line 72
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04010f

    invoke-virtual {v0, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p1

    .line 75
    :cond_0
    const v0, 0x7f100375

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 76
    iget-object v1, p0, Lcpa;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 78
    const v0, 0x7f100376

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/photos/allphotos/adapter/WrappingLinearLayout;

    move v1, v2

    .line 79
    :goto_0
    iget-object v3, p0, Lcpa;->e:Lcsq;

    invoke-interface {v3}, Lcsq;->a()I

    move-result v3

    if-ge v1, v3, :cond_3

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/allphotos/adapter/WrappingLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    if-nez v4, :cond_2

    const/4 v3, 0x1

    :goto_1
    iget-object v5, p0, Lcpa;->e:Lcsq;

    sget v6, Lcpa;->c:I

    invoke-interface {v5, v1, v6, v4, p2}, Lcsq;->a(IILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    new-instance v5, Lcpc;

    sget v6, Lcpa;->c:I

    sget v7, Lcpa;->c:I

    invoke-direct {v5, v6, v7}, Lcpc;-><init>(II)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v4, v2}, Landroid/view/View;->setVisibility(I)V

    if-eqz v3, :cond_1

    invoke-virtual {v0, v4}, Lcom/google/android/apps/photos/allphotos/adapter/WrappingLinearLayout;->addView(Landroid/view/View;)V

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcpa;->e:Lcsq;

    invoke-interface {v1}, Lcsq;->a()I

    move-result v1

    :goto_2
    invoke-virtual {v0}, Lcom/google/android/apps/photos/allphotos/adapter/WrappingLinearLayout;->getChildCount()I

    move-result v2

    if-ge v1, v2, :cond_4

    invoke-virtual {v0, v1}, Lcom/google/android/apps/photos/allphotos/adapter/WrappingLinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 81
    :cond_4
    return-object p1
.end method

.method public a(Lctn;)V
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcpa;->d:Lcsp;

    invoke-interface {p1, v0}, Lctn;->a(Lcsp;)V

    .line 115
    return-void
.end method
