.class public final Lprz;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lprz;


# instance fields
.field private b:Lppf;

.field private c:Lpgg;

.field private d:Ljava/lang/String;

.field private e:[Lpxj;

.field private f:Ljava/lang/String;

.field private g:[Lphi;

.field private h:[Lpsg;

.field private i:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x0

    new-array v0, v0, [Lprz;

    sput-object v0, Lprz;->a:[Lprz;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 165
    invoke-direct {p0}, Loxq;-><init>()V

    .line 168
    iput-object v0, p0, Lprz;->b:Lppf;

    .line 171
    iput-object v0, p0, Lprz;->c:Lpgg;

    .line 176
    sget-object v0, Lpxj;->a:[Lpxj;

    iput-object v0, p0, Lprz;->e:[Lpxj;

    .line 181
    sget-object v0, Lphi;->a:[Lphi;

    iput-object v0, p0, Lprz;->g:[Lphi;

    .line 184
    sget-object v0, Lpsg;->a:[Lpsg;

    iput-object v0, p0, Lprz;->h:[Lpsg;

    .line 165
    return-void
.end method


# virtual methods
.method public a()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 233
    .line 234
    iget-object v0, p0, Lprz;->b:Lppf;

    if-eqz v0, :cond_a

    .line 235
    const/4 v0, 0x1

    iget-object v2, p0, Lprz;->b:Lppf;

    .line 236
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 238
    :goto_0
    iget-object v2, p0, Lprz;->c:Lpgg;

    if-eqz v2, :cond_0

    .line 239
    const/4 v2, 0x2

    iget-object v3, p0, Lprz;->c:Lpgg;

    .line 240
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 242
    :cond_0
    iget-object v2, p0, Lprz;->d:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 243
    const/4 v2, 0x3

    iget-object v3, p0, Lprz;->d:Ljava/lang/String;

    .line 244
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 246
    :cond_1
    iget-object v2, p0, Lprz;->e:[Lpxj;

    if-eqz v2, :cond_3

    .line 247
    iget-object v3, p0, Lprz;->e:[Lpxj;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    aget-object v5, v3, v2

    .line 248
    if-eqz v5, :cond_2

    .line 249
    const/4 v6, 0x4

    .line 250
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 247
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 254
    :cond_3
    iget-object v2, p0, Lprz;->f:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 255
    const/4 v2, 0x5

    iget-object v3, p0, Lprz;->f:Ljava/lang/String;

    .line 256
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 258
    :cond_4
    iget-object v2, p0, Lprz;->g:[Lphi;

    if-eqz v2, :cond_6

    .line 259
    iget-object v3, p0, Lprz;->g:[Lphi;

    array-length v4, v3

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_6

    aget-object v5, v3, v2

    .line 260
    if-eqz v5, :cond_5

    .line 261
    const/4 v6, 0x6

    .line 262
    invoke-static {v6, v5}, Loxo;->c(ILoxu;)I

    move-result v5

    add-int/2addr v0, v5

    .line 259
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 266
    :cond_6
    iget-object v2, p0, Lprz;->h:[Lpsg;

    if-eqz v2, :cond_8

    .line 267
    iget-object v2, p0, Lprz;->h:[Lpsg;

    array-length v3, v2

    :goto_3
    if-ge v1, v3, :cond_8

    aget-object v4, v2, v1

    .line 268
    if-eqz v4, :cond_7

    .line 269
    const/4 v5, 0x7

    .line 270
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 267
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 274
    :cond_8
    iget-object v1, p0, Lprz;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 275
    const/16 v1, 0x8

    iget-object v2, p0, Lprz;->i:Ljava/lang/Integer;

    .line 276
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 278
    :cond_9
    iget-object v1, p0, Lprz;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 279
    iput v0, p0, Lprz;->ai:I

    .line 280
    return v0

    :cond_a
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lprz;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 288
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 289
    sparse-switch v0, :sswitch_data_0

    .line 293
    iget-object v2, p0, Lprz;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 294
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lprz;->ah:Ljava/util/List;

    .line 297
    :cond_1
    iget-object v2, p0, Lprz;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 299
    :sswitch_0
    return-object p0

    .line 304
    :sswitch_1
    iget-object v0, p0, Lprz;->b:Lppf;

    if-nez v0, :cond_2

    .line 305
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lprz;->b:Lppf;

    .line 307
    :cond_2
    iget-object v0, p0, Lprz;->b:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 311
    :sswitch_2
    iget-object v0, p0, Lprz;->c:Lpgg;

    if-nez v0, :cond_3

    .line 312
    new-instance v0, Lpgg;

    invoke-direct {v0}, Lpgg;-><init>()V

    iput-object v0, p0, Lprz;->c:Lpgg;

    .line 314
    :cond_3
    iget-object v0, p0, Lprz;->c:Lpgg;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 318
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lprz;->d:Ljava/lang/String;

    goto :goto_0

    .line 322
    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 323
    iget-object v0, p0, Lprz;->e:[Lpxj;

    if-nez v0, :cond_5

    move v0, v1

    .line 324
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lpxj;

    .line 325
    iget-object v3, p0, Lprz;->e:[Lpxj;

    if-eqz v3, :cond_4

    .line 326
    iget-object v3, p0, Lprz;->e:[Lpxj;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 328
    :cond_4
    iput-object v2, p0, Lprz;->e:[Lpxj;

    .line 329
    :goto_2
    iget-object v2, p0, Lprz;->e:[Lpxj;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_6

    .line 330
    iget-object v2, p0, Lprz;->e:[Lpxj;

    new-instance v3, Lpxj;

    invoke-direct {v3}, Lpxj;-><init>()V

    aput-object v3, v2, v0

    .line 331
    iget-object v2, p0, Lprz;->e:[Lpxj;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 332
    invoke-virtual {p1}, Loxn;->a()I

    .line 329
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 323
    :cond_5
    iget-object v0, p0, Lprz;->e:[Lpxj;

    array-length v0, v0

    goto :goto_1

    .line 335
    :cond_6
    iget-object v2, p0, Lprz;->e:[Lpxj;

    new-instance v3, Lpxj;

    invoke-direct {v3}, Lpxj;-><init>()V

    aput-object v3, v2, v0

    .line 336
    iget-object v2, p0, Lprz;->e:[Lpxj;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 340
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lprz;->f:Ljava/lang/String;

    goto/16 :goto_0

    .line 344
    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 345
    iget-object v0, p0, Lprz;->g:[Lphi;

    if-nez v0, :cond_8

    move v0, v1

    .line 346
    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lphi;

    .line 347
    iget-object v3, p0, Lprz;->g:[Lphi;

    if-eqz v3, :cond_7

    .line 348
    iget-object v3, p0, Lprz;->g:[Lphi;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 350
    :cond_7
    iput-object v2, p0, Lprz;->g:[Lphi;

    .line 351
    :goto_4
    iget-object v2, p0, Lprz;->g:[Lphi;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_9

    .line 352
    iget-object v2, p0, Lprz;->g:[Lphi;

    new-instance v3, Lphi;

    invoke-direct {v3}, Lphi;-><init>()V

    aput-object v3, v2, v0

    .line 353
    iget-object v2, p0, Lprz;->g:[Lphi;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 354
    invoke-virtual {p1}, Loxn;->a()I

    .line 351
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 345
    :cond_8
    iget-object v0, p0, Lprz;->g:[Lphi;

    array-length v0, v0

    goto :goto_3

    .line 357
    :cond_9
    iget-object v2, p0, Lprz;->g:[Lphi;

    new-instance v3, Lphi;

    invoke-direct {v3}, Lphi;-><init>()V

    aput-object v3, v2, v0

    .line 358
    iget-object v2, p0, Lprz;->g:[Lphi;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 362
    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 363
    iget-object v0, p0, Lprz;->h:[Lpsg;

    if-nez v0, :cond_b

    move v0, v1

    .line 364
    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lpsg;

    .line 365
    iget-object v3, p0, Lprz;->h:[Lpsg;

    if-eqz v3, :cond_a

    .line 366
    iget-object v3, p0, Lprz;->h:[Lpsg;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 368
    :cond_a
    iput-object v2, p0, Lprz;->h:[Lpsg;

    .line 369
    :goto_6
    iget-object v2, p0, Lprz;->h:[Lpsg;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_c

    .line 370
    iget-object v2, p0, Lprz;->h:[Lpsg;

    new-instance v3, Lpsg;

    invoke-direct {v3}, Lpsg;-><init>()V

    aput-object v3, v2, v0

    .line 371
    iget-object v2, p0, Lprz;->h:[Lpsg;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 372
    invoke-virtual {p1}, Loxn;->a()I

    .line 369
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 363
    :cond_b
    iget-object v0, p0, Lprz;->h:[Lpsg;

    array-length v0, v0

    goto :goto_5

    .line 375
    :cond_c
    iget-object v2, p0, Lprz;->h:[Lpsg;

    new-instance v3, Lpsg;

    invoke-direct {v3}, Lpsg;-><init>()V

    aput-object v3, v2, v0

    .line 376
    iget-object v2, p0, Lprz;->h:[Lpsg;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 380
    :sswitch_8
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lprz;->i:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 289
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 191
    iget-object v1, p0, Lprz;->b:Lppf;

    if-eqz v1, :cond_0

    .line 192
    const/4 v1, 0x1

    iget-object v2, p0, Lprz;->b:Lppf;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 194
    :cond_0
    iget-object v1, p0, Lprz;->c:Lpgg;

    if-eqz v1, :cond_1

    .line 195
    const/4 v1, 0x2

    iget-object v2, p0, Lprz;->c:Lpgg;

    invoke-virtual {p1, v1, v2}, Loxo;->b(ILoxu;)V

    .line 197
    :cond_1
    iget-object v1, p0, Lprz;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 198
    const/4 v1, 0x3

    iget-object v2, p0, Lprz;->d:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 200
    :cond_2
    iget-object v1, p0, Lprz;->e:[Lpxj;

    if-eqz v1, :cond_4

    .line 201
    iget-object v2, p0, Lprz;->e:[Lpxj;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    .line 202
    if-eqz v4, :cond_3

    .line 203
    const/4 v5, 0x4

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 201
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 207
    :cond_4
    iget-object v1, p0, Lprz;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 208
    const/4 v1, 0x5

    iget-object v2, p0, Lprz;->f:Ljava/lang/String;

    invoke-virtual {p1, v1, v2}, Loxo;->a(ILjava/lang/String;)V

    .line 210
    :cond_5
    iget-object v1, p0, Lprz;->g:[Lphi;

    if-eqz v1, :cond_7

    .line 211
    iget-object v2, p0, Lprz;->g:[Lphi;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_7

    aget-object v4, v2, v1

    .line 212
    if-eqz v4, :cond_6

    .line 213
    const/4 v5, 0x6

    invoke-virtual {p1, v5, v4}, Loxo;->b(ILoxu;)V

    .line 211
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 217
    :cond_7
    iget-object v1, p0, Lprz;->h:[Lpsg;

    if-eqz v1, :cond_9

    .line 218
    iget-object v1, p0, Lprz;->h:[Lpsg;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_9

    aget-object v3, v1, v0

    .line 219
    if-eqz v3, :cond_8

    .line 220
    const/4 v4, 0x7

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 218
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 224
    :cond_9
    iget-object v0, p0, Lprz;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 225
    const/16 v0, 0x8

    iget-object v1, p0, Lprz;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 227
    :cond_a
    iget-object v0, p0, Lprz;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 229
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 161
    invoke-virtual {p0, p1}, Lprz;->a(Loxn;)Lprz;

    move-result-object v0

    return-object v0
.end method
