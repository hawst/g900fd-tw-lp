.class public final Lanh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lalf;
.implements Lalh;
.implements Lani;
.implements Latd;
.implements Lbiw;
.implements Lbri;
.implements Lbtd;
.implements Lbzg;


# static fields
.field private static final a:I


# instance fields
.field private final b:Landroid/content/ContentResolver;

.field private final c:Landroid/content/res/Resources;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x2

    sput v0, Lanh;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;Landroid/content/res/Resources;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 341
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 342
    const-string v0, "contentResolver"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    iput-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    .line 343
    const-string v0, "resources"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/res/Resources;

    iput-object v0, p0, Lanh;->c:Landroid/content/res/Resources;

    .line 344
    return-void
.end method


# virtual methods
.method public A()I
    .locals 3

    .prologue
    .line 551
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:maximum_output_duration_secs"

    const/16 v2, 0xf0

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public B()J
    .locals 4

    .prologue
    .line 556
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:post_sync_analysis_period_duration_ms"

    const-wide/32 v2, 0x5265c00

    invoke-static {v0, v1, v2, v3}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public C()J
    .locals 4

    .prologue
    .line 563
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:post_sync_analysis_budget_per_period_ms"

    const-wide/32 v2, 0x927c0

    invoke-static {v0, v1, v2, v3}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public D()J
    .locals 4

    .prologue
    .line 570
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:post_sync_analysis_delay_between_battery_level_checks_ms"

    const-wide/32 v2, 0x1b7740

    invoke-static {v0, v1, v2, v3}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public E()J
    .locals 4

    .prologue
    .line 577
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:post_sync_analysis_delay_between_screen_off_checks_ms"

    const-wide/32 v2, 0x1b7740

    invoke-static {v0, v1, v2, v3}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public F()J
    .locals 4

    .prologue
    .line 584
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:post_sync_analysis_delay_if_failed_ms"

    const-wide/32 v2, 0x1b7740

    invoke-static {v0, v1, v2, v3}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public G()J
    .locals 4

    .prologue
    .line 591
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:plugged_in_analysis_delay_between_analyses_ms"

    const-wide/32 v2, 0x927c0

    invoke-static {v0, v1, v2, v3}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public H()J
    .locals 4

    .prologue
    .line 598
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:plugged_in_analysis_single_run_max_duration_ms"

    const-wide/32 v2, 0x124f80

    invoke-static {v0, v1, v2, v3}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public I()J
    .locals 4

    .prologue
    .line 605
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:plugged_in_analysis_delay_between_new_content_checks_ms"

    const-wide/32 v2, 0x36ee80

    invoke-static {v0, v1, v2, v3}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public J()J
    .locals 4

    .prologue
    .line 612
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:plugged_in_analysis_delay_between_battery_level_checks_ms"

    const-wide/32 v2, 0x1b7740

    invoke-static {v0, v1, v2, v3}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public K()J
    .locals 4

    .prologue
    .line 619
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:plugged_in_analysis_delay_between_screen_off_checks_ms"

    const-wide/32 v2, 0x927c0

    invoke-static {v0, v1, v2, v3}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public L()J
    .locals 4

    .prologue
    .line 626
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:plugged_in_analysis_delay_before_initial_clean_up_ms"

    const-wide/32 v2, 0x1d4c0

    invoke-static {v0, v1, v2, v3}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public M()J
    .locals 4

    .prologue
    .line 633
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:plugged_in_analysis_delay_before_first_analysis_ms"

    const-wide/32 v2, 0x927c0

    invoke-static {v0, v1, v2, v3}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public N()I
    .locals 3

    .prologue
    .line 640
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:plugged_in_analysis_max_videos_per_run"

    const/16 v2, 0xa

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public O()I
    .locals 3

    .prologue
    .line 647
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:post_capture_analysis_max_retries_count"

    const/4 v2, 0x5

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public P()J
    .locals 4

    .prologue
    .line 654
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:post_capture_analysis_delay_between_retries_ms"

    const-wide/32 v2, 0x493e0

    invoke-static {v0, v1, v2, v3}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public Q()J
    .locals 4

    .prologue
    .line 661
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:post_capture_analysis_timeout_ms"

    const-wide/32 v2, 0x36ee80

    invoke-static {v0, v1, v2, v3}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public R()Z
    .locals 3

    .prologue
    .line 668
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:new_aam_plus_system_notification_enabled_v2"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public S()J
    .locals 4

    .prologue
    .line 675
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:new_aam_plus_system_notification_reappear_min_ms"

    const-wide/32 v2, 0x5265c00

    invoke-static {v0, v1, v2, v3}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public T()Z
    .locals 3

    .prologue
    .line 682
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:new_aam_plus_in_app_notification_enabled_v2"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public U()J
    .locals 4

    .prologue
    .line 689
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:new_aam_plus_in_app_notification_reappear_min_ms"

    const-wide/32 v2, 0x5265c00

    invoke-static {v0, v1, v2, v3}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public V()Z
    .locals 3

    .prologue
    .line 696
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:aam_events_logging_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public W()Z
    .locals 3

    .prologue
    .line 703
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:aam_events_logcat_logging_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public X()I
    .locals 3

    .prologue
    .line 710
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:save_size_limit"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public Y()Z
    .locals 3

    .prologue
    .line 714
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:post_capture_analysis_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public Z()Z
    .locals 3

    .prologue
    .line 721
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:plugged_in_analysis_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 493
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:asset_base_url"

    const-string v2, "https://android-movie-maker-assets.storage.googleapis.com/"

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lood;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 809
    if-nez p1, :cond_1

    move v1, v2

    .line 814
    :cond_0
    :goto_0
    return v1

    .line 812
    :cond_1
    iget-object v0, p1, Lood;->a:Ljava/lang/Integer;

    if-nez v0, :cond_2

    move v0, v1

    .line 814
    :goto_1
    invoke-virtual {p0}, Lanh;->ak()I

    move-result v3

    if-ge v3, v0, :cond_0

    move v1, v2

    goto :goto_0

    .line 812
    :cond_2
    iget-object v0, p1, Lood;->a:Ljava/lang/Integer;

    .line 813
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1
.end method

.method public aa()Z
    .locals 3

    .prologue
    .line 728
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:post_sync_analysis_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public ab()Z
    .locals 3

    .prologue
    .line 735
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:post_sync_service_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public ac()Z
    .locals 3

    .prologue
    .line 742
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:should_issue_gl_flush_if_background_analysis_stuck"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public ad()Z
    .locals 3

    .prologue
    .line 750
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:state_tracker_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public ae()I
    .locals 3

    .prologue
    .line 757
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:max_failed_analysis_attempts_count"

    const/4 v2, 0x5

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public af()I
    .locals 3

    .prologue
    .line 764
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:max_downloaded_photo_count"

    const/16 v2, 0x64

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public ag()I
    .locals 3

    .prologue
    .line 771
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:max_downloaded_video_count"

    const/16 v2, 0xa

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public ah()Z
    .locals 3

    .prologue
    .line 778
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:plugged_in_analysis_should_analyze_synced_content"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public ai()Z
    .locals 3

    .prologue
    .line 786
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:new_movie_from_cluster_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public aj()Z
    .locals 3

    .prologue
    .line 794
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:cloud_aam_editing_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public ak()I
    .locals 3

    .prologue
    .line 802
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:aam_renderer_version"

    const/4 v2, 0x2

    .line 803
    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x3

    .line 802
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    return v0
.end method

.method public b()I
    .locals 3

    .prologue
    .line 499
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:asset_download_connect_timeout_ms"

    const/16 v2, 0x1388

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public c()I
    .locals 3

    .prologue
    .line 505
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:asset_download_read_timeout_ms"

    const/16 v2, 0x1388

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public d()V
    .locals 3

    .prologue
    .line 349
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "dummy"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 350
    return-void
.end method

.method public e()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 354
    iget-object v1, p0, Lanh;->c:Landroid/content/res/Resources;

    const v2, 0x7f0a00c6

    .line 355
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 356
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lanh;->b:Landroid/content/ContentResolver;

    .line 357
    invoke-static {v2, v1, v0}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public f()Z
    .locals 3

    .prologue
    .line 365
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:panel_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public g()Ljava/lang/String;
    .locals 3

    .prologue
    .line 370
    iget-object v0, p0, Lanh;->c:Landroid/content/res/Resources;

    const v1, 0x7f0a00c7

    .line 371
    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 372
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v0, ""

    .line 373
    :goto_0
    return-object v0

    .line 372
    :cond_0
    iget-object v1, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v2, ""

    .line 373
    invoke-static {v1, v0, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public h()Z
    .locals 3

    .prologue
    .line 378
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:low_performance_mode_enabled"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public i()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 398
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:override_audio_decoder_names"

    const-string v2, ""

    .line 399
    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 401
    const-string v1, ","

    invoke-static {v0, v1}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 3

    .prologue
    .line 414
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:override_audio_encoder_name"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k()Z
    .locals 3

    .prologue
    .line 421
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:override_aac_low_complexity"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public l()Z
    .locals 3

    .prologue
    .line 429
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:clip_editor_hardware_codecs_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public m()I
    .locals 3

    .prologue
    .line 436
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:maximum_seekable_frame_size_pixels"

    const v2, 0xe1000

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public n()I
    .locals 3

    .prologue
    .line 443
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:video_decoder_count"

    const/4 v2, 0x2

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public o()Z
    .locals 3

    .prologue
    .line 449
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:reuse_audio_decoders_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public p()Z
    .locals 3

    .prologue
    .line 455
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:reuse_video_decoders_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public q()Z
    .locals 3

    .prologue
    .line 462
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:use_adaptive_decoder"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public r()Z
    .locals 3

    .prologue
    .line 470
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:session_saving_enabled"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public s()Ljava/lang/String;
    .locals 3

    .prologue
    .line 475
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:soundtrack_base_url_v2"

    const-string v2, "https://android-movie-maker-music-prod-10-95a6d62a0ddc.storage.googleapis.com/"

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public t()I
    .locals 3

    .prologue
    .line 481
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:music_http_connect_timeout_ms"

    const/16 v2, 0x1388

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public u()I
    .locals 3

    .prologue
    .line 487
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:music_http_read_timeout_ms"

    const/16 v2, 0x1388

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public v()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 511
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:music_locales"

    const-string v2, ""

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 513
    const-string v1, ","

    invoke-static {v0, v1}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public w()I
    .locals 3

    .prologue
    const/4 v0, 0x2

    .line 521
    iget-object v1, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v2, "moviemaker:playerview"

    invoke-static {v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 522
    const-string v2, "surfaceview"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 527
    :cond_0
    :goto_0
    return v0

    .line 524
    :cond_1
    const-string v2, "textureview"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 525
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public x()I
    .locals 3

    .prologue
    .line 533
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:maximum_input_uris"

    const/16 v2, 0x3e8

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public y()I
    .locals 3

    .prologue
    .line 539
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:maximum_input_video_duration_secs"

    const/16 v2, 0x4b0

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    mul-int/lit16 v0, v0, 0x3e8

    return v0
.end method

.method public z()I
    .locals 3

    .prologue
    .line 545
    iget-object v0, p0, Lanh;->b:Landroid/content/ContentResolver;

    const-string v1, "moviemaker:maximum_input_video_height"

    const/16 v2, 0x780

    invoke-static {v0, v1, v2}, Lhaf;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method
