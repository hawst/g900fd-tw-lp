.class public final Laif;
.super Landroid/os/Binder;
.source "PG"

# interfaces
.implements Laii;


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Lajv;

.field private final c:Laie;

.field private final d:Laie;

.field private final e:Lacs;

.field private final f:Ljfb;

.field private synthetic g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;Lald;Lamy;Lasy;Lakq;Lcdu;Landroid/app/NotificationManager;[Lajj;[Lajj;[Lajw;[Lajw;Lbjf;Ljfb;)V
    .locals 14

    .prologue
    .line 271
    iput-object p1, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 250
    new-instance v1, Ljava/lang/Object;

    invoke-direct {v1}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Laif;->a:Ljava/lang/Object;

    .line 272
    new-instance v1, Lact;

    invoke-direct {v1}, Lact;-><init>()V

    const/4 v2, 0x0

    iput-boolean v2, v1, Lact;->a:Z

    const/4 v2, 0x0

    iput-boolean v2, v1, Lact;->b:Z

    new-instance v2, Lacs;

    iget-object v3, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-direct {v2, v3, v1}, Lacs;-><init>(Landroid/content/Context;Lact;)V

    iput-object v2, p0, Laif;->e:Lacs;

    .line 273
    new-instance v1, Lakl;

    iget-object v6, p0, Laif;->e:Lacs;

    move-object v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v7, p8

    move-object/from16 v8, p10

    move-object/from16 v9, p9

    move-object/from16 v10, p11

    move-object/from16 v11, p5

    move-object/from16 v12, p6

    move-object/from16 v13, p12

    invoke-direct/range {v1 .. v13}, Lakl;-><init>(Landroid/content/Context;Lald;Lamy;Lasy;Lacs;[Lajj;[Lajw;[Lajj;[Lajw;Lakq;Lcdu;Lbjf;)V

    iput-object v1, p0, Laif;->b:Lajv;

    .line 287
    new-instance v1, Laik;

    move-object/from16 v0, p7

    invoke-direct {v1, v0}, Laik;-><init>(Landroid/app/NotificationManager;)V

    iput-object v1, p0, Laif;->c:Laie;

    .line 288
    new-instance v1, Laij;

    invoke-direct {v1}, Laij;-><init>()V

    iput-object v1, p0, Laif;->d:Laie;

    .line 289
    const-string v1, "movieMakerProvider"

    const/4 v2, 0x0

    move-object/from16 v0, p13

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljfb;

    iput-object v1, p0, Laif;->f:Ljfb;

    .line 290
    return-void
.end method

.method static synthetic a(Laif;)Lajv;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Laif;->b:Lajv;

    return-object v0
.end method

.method static synthetic b(Laif;)Lacs;
    .locals 1

    .prologue
    .line 245
    iget-object v0, p0, Laif;->e:Lacs;

    return-object v0
.end method

.method private c()Laie;
    .locals 1

    .prologue
    .line 416
    iget-object v0, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-static {v0}, Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;->a(Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;)Laqx;

    move-result-object v0

    invoke-virtual {v0}, Laqx;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 417
    iget-object v0, p0, Laif;->c:Laie;

    .line 419
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Laif;->d:Laie;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/net/Uri;Laic;)Lbkr;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 362
    iget-object v3, p0, Laif;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 363
    :try_start_0
    invoke-direct {p0}, Laif;->c()Laie;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 367
    :try_start_1
    iget-object v0, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-interface {v4, v0, p1, p2}, Laie;->a(Landroid/content/Context;Landroid/net/Uri;Laic;)V

    .line 368
    new-instance v0, Laih;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Laih;-><init>(Landroid/net/Uri;Lail;)V

    .line 369
    iget-object v1, p0, Laif;->b:Lajv;

    iget-object v5, p0, Laif;->f:Ljfb;

    .line 370
    invoke-virtual {v5, p1}, Ljfb;->e(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v5

    .line 369
    invoke-interface {v1, v5, p2, v0}, Lajv;->b(Landroid/net/Uri;Laic;Lahy;)V

    .line 371
    invoke-virtual {v0}, Laih;->c()V

    .line 372
    invoke-virtual {v0}, Laih;->b()Z
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v1

    .line 373
    :try_start_2
    invoke-virtual {v0}, Laih;->a()Lbkr;
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 378
    if-eqz v1, :cond_0

    .line 379
    :try_start_3
    iget-object v1, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-interface {v4, v1, p1, p2}, Laie;->b(Landroid/content/Context;Landroid/net/Uri;Laic;)V

    .line 380
    :goto_0
    monitor-exit v3

    return-object v0

    :cond_0
    iget-object v1, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-interface {v4, v1, p1, p2}, Laie;->d(Landroid/content/Context;Landroid/net/Uri;Laic;)V

    goto :goto_0

    .line 386
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 374
    :catch_0
    move-exception v0

    move v1, v2

    .line 375
    :goto_1
    const/4 v2, 0x1

    .line 376
    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 378
    :catchall_1
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 379
    :try_start_5
    iget-object v1, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-interface {v4, v1, p1, p2}, Laie;->b(Landroid/content/Context;Landroid/net/Uri;Laic;)V

    .line 383
    :goto_3
    throw v0

    .line 380
    :cond_1
    if-eqz v2, :cond_2

    .line 381
    iget-object v1, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-interface {v4, v1, p1, p2}, Laie;->c(Landroid/content/Context;Landroid/net/Uri;Laic;)V

    goto :goto_3

    .line 383
    :cond_2
    iget-object v1, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-interface {v4, v1, p1, p2}, Laie;->d(Landroid/content/Context;Landroid/net/Uri;Laic;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    .line 378
    :catchall_2
    move-exception v0

    move v1, v2

    goto :goto_2

    .line 374
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public a(Landroid/net/Uri;Laic;Lail;)Lbkr;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 298
    iget-object v3, p0, Laif;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 299
    :try_start_0
    invoke-direct {p0}, Laif;->c()Laie;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 303
    :try_start_1
    iget-object v0, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-interface {v4, v0, p1, p2}, Laie;->a(Landroid/content/Context;Landroid/net/Uri;Laic;)V

    .line 304
    new-instance v0, Laih;

    invoke-direct {v0, p1, p3}, Laih;-><init>(Landroid/net/Uri;Lail;)V

    .line 305
    iget-object v1, p0, Laif;->b:Lajv;

    iget-object v5, p0, Laif;->f:Ljfb;

    .line 306
    invoke-virtual {v5, p1}, Ljfb;->e(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v5

    .line 305
    invoke-interface {v1, v5, p2, v0}, Lajv;->a(Landroid/net/Uri;Laic;Lahy;)V

    .line 307
    invoke-virtual {v0}, Laih;->c()V

    .line 308
    invoke-virtual {v0}, Laih;->b()Z
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v1

    .line 309
    :try_start_2
    invoke-virtual {v0}, Laih;->a()Lbkr;
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 314
    if-eqz v1, :cond_0

    .line 315
    :try_start_3
    iget-object v1, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-interface {v4, v1, p1, p2}, Laie;->b(Landroid/content/Context;Landroid/net/Uri;Laic;)V

    .line 316
    :goto_0
    monitor-exit v3

    return-object v0

    :cond_0
    iget-object v1, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-interface {v4, v1, p1, p2}, Laie;->d(Landroid/content/Context;Landroid/net/Uri;Laic;)V

    goto :goto_0

    .line 322
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 310
    :catch_0
    move-exception v0

    move v1, v2

    .line 311
    :goto_1
    const/4 v2, 0x1

    .line 312
    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 314
    :catchall_1
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 315
    :try_start_5
    iget-object v1, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-interface {v4, v1, p1, p2}, Laie;->b(Landroid/content/Context;Landroid/net/Uri;Laic;)V

    .line 319
    :goto_3
    throw v0

    .line 316
    :cond_1
    if-eqz v2, :cond_2

    .line 317
    iget-object v1, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-interface {v4, v1, p1, p2}, Laie;->c(Landroid/content/Context;Landroid/net/Uri;Laic;)V

    goto :goto_3

    .line 319
    :cond_2
    iget-object v1, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-interface {v4, v1, p1, p2}, Laie;->d(Landroid/content/Context;Landroid/net/Uri;Laic;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    .line 314
    :catchall_2
    move-exception v0

    move v1, v2

    goto :goto_2

    .line 310
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public a(Landroid/net/Uri;Lbkr;Laic;Lail;)Lbkr;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 332
    iget-object v3, p0, Laif;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 333
    :try_start_0
    invoke-direct {p0}, Laif;->c()Laie;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 337
    :try_start_1
    iget-object v0, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-interface {v4, v0, p1, p3}, Laie;->a(Landroid/content/Context;Landroid/net/Uri;Laic;)V

    .line 338
    new-instance v0, Laih;

    invoke-direct {v0, p1, p4}, Laih;-><init>(Landroid/net/Uri;Lail;)V

    .line 339
    iget-object v1, p0, Laif;->b:Lajv;

    iget-object v5, p0, Laif;->f:Ljfb;

    .line 340
    invoke-virtual {v5, p1}, Ljfb;->e(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v5

    .line 339
    invoke-interface {v1, v5, p3, p2, v0}, Lajv;->a(Landroid/net/Uri;Laic;Lbkr;Lahy;)V

    .line 341
    invoke-virtual {v0}, Laih;->c()V

    .line 342
    invoke-virtual {v0}, Laih;->b()Z
    :try_end_1
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result v1

    .line 343
    :try_start_2
    invoke-virtual {v0}, Laih;->a()Lbkr;
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 348
    if-eqz v1, :cond_0

    .line 349
    :try_start_3
    iget-object v1, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-interface {v4, v1, p1, p3}, Laie;->b(Landroid/content/Context;Landroid/net/Uri;Laic;)V

    .line 350
    :goto_0
    monitor-exit v3

    return-object v0

    :cond_0
    iget-object v1, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-interface {v4, v1, p1, p3}, Laie;->d(Landroid/content/Context;Landroid/net/Uri;Laic;)V

    goto :goto_0

    .line 356
    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v0

    .line 344
    :catch_0
    move-exception v0

    move v1, v2

    .line 345
    :goto_1
    const/4 v2, 0x1

    .line 346
    :try_start_4
    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 348
    :catchall_1
    move-exception v0

    :goto_2
    if-eqz v1, :cond_1

    .line 349
    :try_start_5
    iget-object v1, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-interface {v4, v1, p1, p3}, Laie;->b(Landroid/content/Context;Landroid/net/Uri;Laic;)V

    .line 353
    :goto_3
    throw v0

    .line 350
    :cond_1
    if-eqz v2, :cond_2

    .line 351
    iget-object v1, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-interface {v4, v1, p1, p3}, Laie;->c(Landroid/content/Context;Landroid/net/Uri;Laic;)V

    goto :goto_3

    .line 353
    :cond_2
    iget-object v1, p0, Laif;->g:Lcom/google/android/apps/moviemaker/analyzer/AnalyzerService;

    invoke-interface {v4, v1, p1, p3}, Laie;->d(Landroid/content/Context;Landroid/net/Uri;Laic;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_3

    .line 348
    :catchall_2
    move-exception v0

    move v1, v2

    goto :goto_2

    .line 344
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public a()V
    .locals 1

    .prologue
    .line 391
    iget-object v0, p0, Laif;->b:Lajv;

    invoke-interface {v0}, Lajv;->a()V

    .line 392
    return-void
.end method

.method public b()V
    .locals 3

    .prologue
    .line 396
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Laig;

    invoke-direct {v1, p0}, Laig;-><init>(Laif;)V

    const-string v2, "MFF Context release thread"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 404
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 405
    return-void
.end method
