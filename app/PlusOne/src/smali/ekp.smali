.class final Lekp;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Leko;


# direct methods
.method constructor <init>(Leko;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lekp;->a:Leko;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 66
    iget-object v0, p0, Lekp;->a:Leko;

    iget-object v0, v0, Leko;->Z:Lhee;

    invoke-interface {v0}, Lhee;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 67
    const/4 v0, 0x0

    .line 75
    :goto_0
    return-object v0

    .line 70
    :cond_0
    iget-object v0, p0, Lekp;->a:Leko;

    invoke-virtual {v0}, Leko;->n()Lz;

    move-result-object v1

    .line 71
    invoke-virtual {v1}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    .line 72
    iget-object v3, p0, Lekp;->a:Leko;

    const-string v0, "destination_intent"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-static {v3, v0}, Leko;->a(Leko;Landroid/content/Intent;)Landroid/content/Intent;

    .line 73
    const-string v0, "circle_actor_data"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v2

    .line 74
    new-instance v0, Lfbj;

    iget-object v3, p0, Lekp;->a:Leko;

    iget-object v3, v3, Leko;->Z:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    .line 75
    invoke-static {v2}, Ldqu;->a([B)Ljava/util/List;

    move-result-object v2

    invoke-direct {v0, v1, v3, v2}, Lfbj;-><init>(Landroid/content/Context;ILjava/util/List;)V

    goto :goto_0
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 80
    iget-object v0, p0, Lekp;->a:Leko;

    const/4 v1, 0x0

    iput-boolean v1, v0, Leko;->R:Z

    .line 82
    iget-object v0, p0, Lekp;->a:Leko;

    invoke-static {v0}, Leko;->a(Leko;)Lekq;

    move-result-object v0

    invoke-virtual {v0, p1}, Lekq;->a(Landroid/database/Cursor;)V

    .line 83
    iget-object v0, p0, Lekp;->a:Leko;

    invoke-static {v0}, Leko;->a(Leko;)Lekq;

    move-result-object v0

    invoke-virtual {v0}, Lekq;->notifyDataSetChanged()V

    .line 84
    iget-object v0, p0, Lekp;->a:Leko;

    iget-object v1, p0, Lekp;->a:Leko;

    invoke-virtual {v1}, Leko;->x()Landroid/view/View;

    invoke-virtual {v0}, Leko;->aa()V

    .line 85
    return-void
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 89
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 62
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lekp;->a(Landroid/database/Cursor;)V

    return-void
.end method
