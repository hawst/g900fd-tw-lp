.class public Ljqe;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private c:I

.field private d:Z

.field private e:J

.field private f:Ljava/lang/String;

.field private g:I

.field private h:I

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    new-instance v0, Ljqe;

    invoke-direct {v0}, Ljqe;-><init>()V

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    const/16 v0, 0x3fff

    iput v0, p0, Ljqe;->c:I

    .line 23
    const/4 v0, 0x7

    iput v0, p0, Ljqe;->g:I

    .line 151
    return-void
.end method


# virtual methods
.method public a()Ljqe;
    .locals 4

    .prologue
    .line 31
    new-instance v0, Ljqe;

    invoke-direct {v0}, Ljqe;-><init>()V

    iget-object v1, p0, Ljqe;->a:Ljava/lang/String;

    .line 32
    invoke-virtual {v0, v1}, Ljqe;->a(Ljava/lang/String;)Ljqe;

    move-result-object v0

    iget-object v1, p0, Ljqe;->b:Ljava/util/Collection;

    .line 33
    invoke-virtual {v0, v1}, Ljqe;->a(Ljava/util/Collection;)Ljqe;

    move-result-object v0

    iget v1, p0, Ljqe;->c:I

    .line 34
    invoke-virtual {v0, v1}, Ljqe;->a(I)Ljqe;

    move-result-object v0

    iget-boolean v1, p0, Ljqe;->d:Z

    .line 35
    invoke-virtual {v0, v1}, Ljqe;->a(Z)Ljqe;

    move-result-object v0

    iget-wide v2, p0, Ljqe;->e:J

    .line 36
    invoke-virtual {v0, v2, v3}, Ljqe;->a(J)Ljqe;

    move-result-object v0

    iget-object v1, p0, Ljqe;->f:Ljava/lang/String;

    .line 37
    invoke-virtual {v0, v1}, Ljqe;->b(Ljava/lang/String;)Ljqe;

    move-result-object v0

    iget v1, p0, Ljqe;->g:I

    .line 38
    invoke-virtual {v0, v1}, Ljqe;->b(I)Ljqe;

    move-result-object v0

    iget v1, p0, Ljqe;->h:I

    .line 39
    invoke-virtual {v0, v1}, Ljqe;->c(I)Ljqe;

    move-result-object v0

    iget v1, p0, Ljqe;->i:I

    .line 40
    invoke-virtual {v0, v1}, Ljqe;->d(I)Ljqe;

    move-result-object v0

    return-object v0
.end method

.method public a(I)Ljqe;
    .locals 0

    .prologue
    .line 54
    iput p1, p0, Ljqe;->c:I

    .line 55
    return-object p0
.end method

.method public a(J)Ljqe;
    .locals 1

    .prologue
    .line 64
    iput-wide p1, p0, Ljqe;->e:J

    .line 65
    return-object p0
.end method

.method public a(Ljava/lang/String;)Ljqe;
    .locals 0

    .prologue
    .line 44
    iput-object p1, p0, Ljqe;->a:Ljava/lang/String;

    .line 45
    return-object p0
.end method

.method public a(Ljava/util/Collection;)Ljqe;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Ljqe;"
        }
    .end annotation

    .prologue
    .line 49
    iput-object p1, p0, Ljqe;->b:Ljava/util/Collection;

    .line 50
    return-object p0
.end method

.method public a(Z)Ljqe;
    .locals 0

    .prologue
    .line 59
    iput-boolean p1, p0, Ljqe;->d:Z

    .line 60
    return-object p0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Ljqe;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b(I)Ljqe;
    .locals 0

    .prologue
    .line 74
    iput p1, p0, Ljqe;->g:I

    .line 75
    return-object p0
.end method

.method public b(Ljava/lang/String;)Ljqe;
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Ljqe;->f:Ljava/lang/String;

    .line 70
    return-object p0
.end method

.method public c()Ljava/util/Collection;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Collection",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 93
    iget-object v0, p0, Ljqe;->b:Ljava/util/Collection;

    return-object v0
.end method

.method public c(I)Ljqe;
    .locals 0

    .prologue
    .line 79
    iput p1, p0, Ljqe;->h:I

    .line 80
    return-object p0
.end method

.method public synthetic clone()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0}, Ljqe;->a()Ljqe;

    move-result-object v0

    return-object v0
.end method

.method public d()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Ljqe;->c:I

    return v0
.end method

.method public d(I)Ljqe;
    .locals 0

    .prologue
    .line 84
    iput p1, p0, Ljqe;->i:I

    .line 85
    return-object p0
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 101
    iget-boolean v0, p0, Ljqe;->d:Z

    return v0
.end method

.method public f()J
    .locals 2

    .prologue
    .line 105
    iget-wide v0, p0, Ljqe;->e:J

    return-wide v0
.end method

.method public g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Ljqe;->f:Ljava/lang/String;

    return-object v0
.end method

.method public h()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Ljqe;->g:I

    return v0
.end method

.method public i()I
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Ljqe;->h:I

    return v0
.end method

.method public j()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Ljqe;->i:I

    return v0
.end method
