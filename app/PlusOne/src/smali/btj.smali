.class public Lbtj;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbte;


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Landroid/graphics/RectF;


# instance fields
.field private A:Lbgx;

.field private B:Lbgx;

.field private C:Lbsb;

.field private D:Lbsb;

.field private E:Lbfw;

.field private F:Lbfw;

.field private G:Lbfw;

.field private H:Lbfw;

.field private I:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lbhb;",
            ">;"
        }
    .end annotation
.end field

.field private J:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lbbs;",
            "Lbhb;",
            ">;"
        }
    .end annotation
.end field

.field private K:I

.field private L:I

.field private M:Ljava/lang/String;

.field private N:Layj;

.field private final c:Landroid/content/Context;

.field private final d:Lbgf;

.field private final e:Landroid/content/res/Resources;

.field private final f:Landroid/content/res/AssetManager;

.field private final g:Landroid/graphics/BitmapFactory$Options;

.field private final h:Lcgc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcgc",
            "<",
            "Landroid/graphics/Matrix;",
            ">;"
        }
    .end annotation
.end field

.field private final i:Lcgc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcgc",
            "<",
            "Lbgd;",
            ">;"
        }
    .end annotation
.end field

.field private final j:Lcgc;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcgc",
            "<[F>;"
        }
    .end annotation
.end field

.field private final k:[Lbtm;

.field private final l:Landroid/graphics/Matrix;

.field private final m:Layj;

.field private final n:Lcgw;

.field private final o:Lcgw;

.field private p:Lbfw;

.field private q:Lbfw;

.field private r:Lbhb;

.field private s:Lbgx;

.field private t:Lbgx;

.field private u:Lbgx;

.field private v:Lbgx;

.field private w:Lbgx;

.field private x:Lbgx;

.field private y:Lbgx;

.field private z:Lbgx;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v1, 0x0

    .line 64
    const-class v0, Lbtj;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lbtj;->a:Ljava/lang/String;

    .line 98
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0, v1, v1, v2, v2}, Landroid/graphics/RectF;-><init>(FFFF)V

    sput-object v0, Lbtj;->b:Landroid/graphics/RectF;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lbgf;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x8

    const/4 v1, 0x0

    const/4 v4, 0x4

    .line 178
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    new-array v0, v4, [Lbtm;

    iput-object v0, p0, Lbtj;->k:[Lbtm;

    move v0, v1

    .line 125
    :goto_0
    iget-object v2, p0, Lbtj;->k:[Lbtm;

    if-ge v0, v4, :cond_0

    .line 126
    iget-object v2, p0, Lbtj;->k:[Lbtm;

    new-instance v3, Lbtm;

    invoke-direct {v3}, Lbtm;-><init>()V

    aput-object v3, v2, v0

    .line 125
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 179
    :cond_0
    const-string v0, "context"

    invoke-static {p1, v0, v6}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lbtj;->c:Landroid/content/Context;

    .line 180
    const-string v0, "renderContext"

    invoke-static {p2, v0, v6}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgf;

    iput-object v0, p0, Lbtj;->d:Lbgf;

    .line 182
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lbtj;->e:Landroid/content/res/Resources;

    .line 183
    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    iput-object v0, p0, Lbtj;->f:Landroid/content/res/AssetManager;

    .line 185
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lbtj;->g:Landroid/graphics/BitmapFactory$Options;

    .line 186
    iget-object v0, p0, Lbtj;->g:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 188
    new-instance v0, Lcgw;

    sget-object v2, Lbtj;->a:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcgw;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbtj;->n:Lcgw;

    .line 189
    new-instance v0, Lcgw;

    sget-object v2, Lbtj;->a:Ljava/lang/String;

    invoke-direct {v0, v2}, Lcgw;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lbtj;->o:Lcgw;

    .line 191
    new-array v2, v5, [Landroid/graphics/Matrix;

    move v0, v1

    .line 192
    :goto_1
    if-ge v0, v5, :cond_1

    .line 193
    new-instance v3, Landroid/graphics/Matrix;

    invoke-direct {v3}, Landroid/graphics/Matrix;-><init>()V

    aput-object v3, v2, v0

    .line 192
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 195
    :cond_1
    new-instance v0, Lcgc;

    invoke-direct {v0, v2}, Lcgc;-><init>([Ljava/lang/Object;)V

    iput-object v0, p0, Lbtj;->h:Lcgc;

    .line 197
    new-array v2, v4, [Lbgd;

    move v0, v1

    .line 198
    :goto_2
    if-ge v0, v4, :cond_2

    .line 199
    new-instance v3, Lbgd;

    invoke-direct {v3}, Lbgd;-><init>()V

    aput-object v3, v2, v0

    .line 198
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 201
    :cond_2
    new-instance v0, Lcgc;

    invoke-direct {v0, v2}, Lcgc;-><init>([Ljava/lang/Object;)V

    iput-object v0, p0, Lbtj;->i:Lcgc;

    .line 203
    new-array v0, v4, [[F

    .line 204
    :goto_3
    if-ge v1, v4, :cond_3

    .line 205
    const/16 v2, 0x10

    new-array v2, v2, [F

    aput-object v2, v0, v1

    .line 204
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 207
    :cond_3
    new-instance v1, Lcgc;

    invoke-direct {v1, v0}, Lcgc;-><init>([Ljava/lang/Object;)V

    iput-object v1, p0, Lbtj;->j:Lcgc;

    .line 209
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lbtj;->l:Landroid/graphics/Matrix;

    .line 211
    new-instance v0, Lays;

    invoke-direct {v0}, Lays;-><init>()V

    invoke-virtual {v0}, Lays;->a()Layr;

    move-result-object v0

    iput-object v0, p0, Lbtj;->m:Layj;

    .line 212
    return-void
.end method

.method private a([F)Landroid/graphics/Matrix;
    .locals 1

    .prologue
    .line 351
    iget-object v0, p0, Lbtj;->h:Lcgc;

    invoke-virtual {v0}, Lcgc;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Matrix;

    .line 352
    invoke-static {v0, p1}, Lcfn;->a(Landroid/graphics/Matrix;[F)V

    .line 353
    return-object v0
.end method

.method static synthetic a(Lbtj;Layj;Layl;)Layj;
    .locals 2

    .prologue
    .line 62
    iget-object v0, p2, Layl;->i:Lbmg;

    sget-object v1, Lbmg;->c:Lbmg;

    if-ne v0, v1, :cond_2

    invoke-static {p2}, Lbag;->e(Layl;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lbtj;->f:Landroid/content/res/AssetManager;

    iget-object v1, p0, Lbtj;->e:Landroid/content/res/Resources;

    invoke-interface {p1, v0, v1}, Layj;->b(Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;

    move-result-object p1

    :cond_0
    :goto_0
    return-object p1

    :cond_1
    invoke-interface {p1}, Layj;->a()Layj;

    move-result-object p1

    goto :goto_0

    :cond_2
    invoke-static {p2}, Lbag;->e(Layl;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lbtj;->f:Landroid/content/res/AssetManager;

    iget-object v1, p0, Lbtj;->e:Landroid/content/res/Resources;

    invoke-interface {p1, v0, v1}, Layj;->a(Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;

    move-result-object p1

    goto :goto_0

    :cond_3
    iget-object v0, p2, Layl;->i:Lbmg;

    sget-object v1, Lbmg;->d:Lbmg;

    if-ne v0, v1, :cond_4

    invoke-interface {p1}, Layj;->b()Layj;

    move-result-object p1

    goto :goto_0

    :cond_4
    iget-object v0, p2, Layl;->i:Lbmg;

    sget-object v1, Lbmg;->e:Lbmg;

    if-ne v0, v1, :cond_0

    iget-object p1, p0, Lbtj;->m:Layj;

    goto :goto_0
.end method

.method private a(I)Lbhb;
    .locals 2

    .prologue
    .line 1057
    iget-object v0, p0, Lbtj;->I:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhb;

    .line 1058
    if-nez v0, :cond_0

    .line 1059
    if-eqz p1, :cond_1

    .line 1060
    iget-object v0, p0, Lbtj;->e:Landroid/content/res/Resources;

    iget-object v1, p0, Lbtj;->g:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v0, p1, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1061
    iget-object v0, p0, Lbtj;->d:Lbgf;

    invoke-virtual {v0, v1}, Lbgf;->a(Landroid/graphics/Bitmap;)Lbfw;

    move-result-object v0

    .line 1062
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 1066
    :goto_0
    iget-object v1, p0, Lbtj;->I:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1068
    :cond_0
    return-object v0

    .line 1064
    :cond_1
    iget-object v0, p0, Lbtj;->d:Lbgf;

    invoke-virtual {v0}, Lbgf;->d()Lbfw;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lbbs;)Lbhb;
    .locals 2

    .prologue
    .line 1073
    iget-object v0, p0, Lbtj;->J:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhb;

    .line 1074
    if-nez v0, :cond_0

    .line 1075
    if-eqz p1, :cond_1

    .line 1076
    iget-object v0, p0, Lbtj;->d:Lbgf;

    iget-object v1, p1, Lbbs;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lbgf;->a(Landroid/graphics/Bitmap;)Lbfw;

    move-result-object v0

    .line 1080
    :goto_0
    iget-object v1, p0, Lbtj;->J:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1082
    :cond_0
    return-object v0

    .line 1078
    :cond_1
    iget-object v0, p0, Lbtj;->d:Lbgf;

    invoke-virtual {v0}, Lbgf;->d()Lbfw;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Layj;Layl;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 695
    invoke-interface {p1, p2}, Layj;->f(Layl;)I

    move-result v0

    .line 696
    invoke-interface {p1, p2}, Layj;->h(Layl;)F

    move-result v1

    .line 697
    if-eqz v0, :cond_0

    cmpl-float v2, v1, v4

    if-lez v2, :cond_0

    .line 698
    invoke-direct {p0, v0}, Lbtj;->a(I)Lbhb;

    move-result-object v0

    .line 699
    invoke-interface {p1, p2}, Layj;->g(Layl;)Landroid/graphics/Matrix;

    move-result-object v2

    .line 701
    invoke-interface {p1, p2}, Layj;->i(Layl;)I

    move-result v3

    .line 698
    invoke-direct {p0, v0, v2, v1, v3}, Lbtj;->a(Lbhb;Landroid/graphics/Matrix;FI)V

    .line 704
    :cond_0
    invoke-interface {p1, p2}, Layj;->j(Layl;)I

    move-result v0

    .line 705
    invoke-interface {p1, p2}, Layj;->l(Layl;)F

    move-result v1

    .line 706
    if-eqz v0, :cond_1

    cmpl-float v2, v1, v4

    if-lez v2, :cond_1

    .line 707
    invoke-direct {p0, v0}, Lbtj;->a(I)Lbhb;

    move-result-object v0

    .line 708
    invoke-interface {p1, p2}, Layj;->k(Layl;)Landroid/graphics/Matrix;

    move-result-object v2

    .line 710
    invoke-interface {p1, p2}, Layj;->m(Layl;)I

    move-result v3

    .line 707
    invoke-direct {p0, v0, v2, v1, v3}, Lbtj;->a(Lbhb;Landroid/graphics/Matrix;FI)V

    .line 713
    :cond_1
    invoke-interface {p1, p2}, Layj;->n(Layl;)I

    move-result v0

    .line 714
    invoke-interface {p1, p2}, Layj;->p(Layl;)F

    move-result v1

    .line 715
    invoke-static {p2}, Lbag;->e(Layl;)Z

    move-result v2

    .line 716
    cmpl-float v3, v1, v4

    if-lez v3, :cond_3

    if-nez v2, :cond_2

    if-eqz v0, :cond_3

    .line 717
    :cond_2
    if-eqz v2, :cond_4

    .line 718
    invoke-direct {p0, p1, p2}, Lbtj;->b(Layj;Layl;)Lbhb;

    move-result-object v0

    .line 721
    :goto_0
    invoke-interface {p1, p2}, Layj;->o(Layl;)Landroid/graphics/Matrix;

    move-result-object v2

    .line 723
    invoke-interface {p1, p2}, Layj;->q(Layl;)I

    move-result v3

    .line 720
    invoke-direct {p0, v0, v2, v1, v3}, Lbtj;->a(Lbhb;Landroid/graphics/Matrix;FI)V

    .line 725
    :cond_3
    return-void

    .line 719
    :cond_4
    invoke-direct {p0, v0}, Lbtj;->a(I)Lbhb;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lbgn;Lbfw;F)V
    .locals 3

    .prologue
    .line 681
    iget-object v0, p0, Lbtj;->z:Lbgx;

    invoke-static {v0}, Lbgs;->a(Lbgx;)Lbgs;

    move-result-object v0

    .line 682
    const-string v1, "postaddition"

    invoke-virtual {v0, v1, p3}, Lbgs;->a(Ljava/lang/String;F)Lbgs;

    .line 683
    invoke-interface {p1}, Lbgn;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 684
    sget-object v1, Lcfn;->c:Landroid/graphics/Matrix;

    const/4 v2, 0x0

    invoke-static {p1, p2, v1, v2, v0}, Lbfu;->a(Lbgn;Lbhb;Landroid/graphics/Matrix;ILbgs;)V

    .line 692
    :goto_0
    return-void

    .line 690
    :cond_0
    invoke-static {p1, p2, v0}, Lbfu;->a(Lbgn;Lbhb;Lbgs;)V

    goto :goto_0
.end method

.method private a(Lbgs;IILayj;Layl;Lbof;Lbgx;)V
    .locals 8

    .prologue
    .line 1008
    int-to-float v0, p2

    int-to-float v1, p3

    div-float/2addr v0, v1

    iput v0, p5, Layl;->u:F

    .line 1014
    invoke-interface {p4, p5, p6}, Layj;->a(Layl;Lbof;)Landroid/graphics/Matrix;

    move-result-object v1

    .line 1015
    iget-object v0, p0, Lbtj;->l:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 1016
    iget-object v0, p0, Lbtj;->h:Lcgc;

    invoke-virtual {v0}, Lcgc;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Matrix;

    .line 1017
    invoke-interface {p4, p5}, Layj;->x(Layl;)Landroid/graphics/Matrix;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1018
    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 1019
    const-string v2, "primary_crop"

    invoke-virtual {p1, v2, v0}, Lbgs;->b(Ljava/lang/String;Landroid/graphics/Matrix;)Lbgs;

    move-result-object v2

    const-string v3, "primary_transform"

    .line 1020
    invoke-virtual {v2, v3, v1}, Lbgs;->a(Ljava/lang/String;Landroid/graphics/Matrix;)Lbgs;

    .line 1021
    iget-object v1, p0, Lbtj;->h:Lcgc;

    invoke-virtual {v1, v0}, Lcgc;->a(Ljava/lang/Object;)V

    .line 1025
    invoke-interface {p4, p5}, Layj;->e(Layl;)I

    move-result v4

    .line 1026
    if-eqz v4, :cond_1

    invoke-direct {p0, v4}, Lbtj;->a(I)Lbhb;

    move-result-object v0

    move-object v1, v0

    .line 1029
    :goto_0
    invoke-interface {p4, p5}, Layj;->a(Layl;)Z

    move-result v5

    .line 1030
    if-eqz v5, :cond_2

    .line 1031
    invoke-interface {p4, p5}, Layj;->c(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    move-object v2, v0

    .line 1034
    :goto_1
    iget-object v0, p0, Lbtj;->u:Lbgx;

    if-ne p7, v0, :cond_0

    .line 1035
    if-eqz v5, :cond_3

    .line 1036
    invoke-interface {p4, p5}, Layj;->d(Layl;)Landroid/graphics/Matrix;

    move-result-object v0

    move-object v3, v0

    .line 1038
    :goto_2
    invoke-interface {p4, p5}, Layj;->b(Layl;)Landroid/graphics/Matrix;

    move-result-object v6

    .line 1039
    iget-object v0, p0, Lbtj;->h:Lcgc;

    invoke-virtual {v0}, Lcgc;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Matrix;

    .line 1040
    invoke-interface {p4, p5}, Layj;->y(Layl;)Landroid/graphics/Matrix;

    move-result-object v7

    invoke-virtual {v7, v0}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    .line 1041
    invoke-virtual {v0, v6}, Landroid/graphics/Matrix;->preConcat(Landroid/graphics/Matrix;)Z

    .line 1042
    const-string v7, "secondary_bkgd_transform"

    invoke-virtual {p1, v7, v3}, Lbgs;->a(Ljava/lang/String;Landroid/graphics/Matrix;)Lbgs;

    move-result-object v3

    const-string v7, "secondary_crop"

    .line 1043
    invoke-virtual {v3, v7, v0}, Lbgs;->a(Ljava/lang/String;Landroid/graphics/Matrix;)Lbgs;

    move-result-object v3

    const-string v7, "secondary_transform"

    .line 1044
    invoke-virtual {v3, v7, v6}, Lbgs;->a(Ljava/lang/String;Landroid/graphics/Matrix;)Lbgs;

    move-result-object v3

    const-string v6, "secondary_opacity"

    .line 1045
    invoke-interface {p4, p5}, Layj;->w(Layl;)F

    move-result v7

    invoke-virtual {v3, v6, v7}, Lbgs;->a(Ljava/lang/String;F)Lbgs;

    .line 1046
    iget-object v3, p0, Lbtj;->h:Lcgc;

    invoke-virtual {v3, v0}, Lcgc;->a(Ljava/lang/Object;)V

    .line 1049
    :cond_0
    const-string v3, "lut_is_active"

    if-eqz v1, :cond_4

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {p1, v3, v0}, Lbgs;->a(Ljava/lang/String;I)Lbgs;

    move-result-object v0

    const-string v1, "sampler_lut"

    .line 1050
    invoke-direct {p0, v4}, Lbtj;->a(I)Lbhb;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lbgs;->a(Ljava/lang/String;Lbhb;)Lbgs;

    move-result-object v0

    const-string v1, "barrel_distortion_amount"

    .line 1051
    invoke-interface {p4, p5}, Layj;->t(Layl;)F

    move-result v3

    invoke-virtual {v0, v1, v3}, Lbgs;->b(Ljava/lang/String;F)Lbgs;

    move-result-object v1

    const-string v3, "use_blurred_background"

    if-eqz v5, :cond_5

    const/4 v0, 0x1

    .line 1052
    :goto_4
    invoke-virtual {v1, v3, v0}, Lbgs;->b(Ljava/lang/String;I)Lbgs;

    move-result-object v0

    const-string v1, "primary_bkgd_transform"

    .line 1053
    invoke-virtual {v0, v1, v2}, Lbgs;->b(Ljava/lang/String;Landroid/graphics/Matrix;)Lbgs;

    .line 1054
    return-void

    .line 1026
    :cond_1
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    .line 1031
    :cond_2
    sget-object v0, Lcfn;->a:Landroid/graphics/Matrix;

    move-object v2, v0

    goto :goto_1

    .line 1036
    :cond_3
    sget-object v0, Lcfn;->a:Landroid/graphics/Matrix;

    move-object v3, v0

    goto :goto_2

    .line 1049
    :cond_4
    const/4 v0, 0x0

    goto :goto_3

    .line 1051
    :cond_5
    const/4 v0, 0x0

    goto :goto_4
.end method

.method private a(Lbgs;Lbbs;)V
    .locals 3

    .prologue
    .line 957
    invoke-direct {p0, p2}, Lbtj;->a(Lbbs;)Lbhb;

    move-result-object v0

    .line 958
    const-string v1, "sampler_primary_color_correct"

    invoke-virtual {p1, v1, v0}, Lbgs;->a(Ljava/lang/String;Lbhb;)Lbgs;

    .line 959
    if-eqz p2, :cond_0

    .line 960
    const-string v0, "color_correct_active_a"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Lbgs;->a(Ljava/lang/String;I)Lbgs;

    move-result-object v0

    const-string v1, "color_correct_factor_a"

    iget v2, p2, Lbbs;->b:F

    .line 961
    invoke-virtual {v0, v1, v2}, Lbgs;->a(Ljava/lang/String;F)Lbgs;

    .line 966
    :goto_0
    return-void

    .line 963
    :cond_0
    const-string v0, "color_correct_active_a"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Lbgs;->a(Ljava/lang/String;I)Lbgs;

    move-result-object v0

    const-string v1, "color_correct_factor_a"

    const/4 v2, 0x0

    .line 964
    invoke-virtual {v0, v1, v2}, Lbgs;->a(Ljava/lang/String;F)Lbgs;

    goto :goto_0
.end method

.method private a(Lbhb;Landroid/graphics/Matrix;FI)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 760
    const/4 v0, 0x0

    .line 763
    sget-object v1, Lbtl;->a:[I

    add-int/lit8 v2, p4, -0x1

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    move v6, v8

    .line 785
    :goto_0
    iget-object v1, p0, Lbtj;->h:Lcgc;

    invoke-virtual {v1}, Lcgc;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Matrix;

    .line 786
    invoke-virtual {v4, p2}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 787
    sget-object v1, Lcfn;->b:Landroid/graphics/Matrix;

    invoke-virtual {v4, v1}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    .line 789
    invoke-static {v0}, Lbgs;->a(Lbgx;)Lbgs;

    move-result-object v7

    .line 790
    sget-object v0, Lbtl;->a:[I

    add-int/lit8 v1, p4, -0x1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    .line 807
    :goto_1
    iget-object v0, p0, Lbtj;->h:Lcgc;

    invoke-virtual {v0, v4}, Lcgc;->a(Ljava/lang/Object;)V

    .line 808
    return-void

    .line 765
    :pswitch_0
    iget-object v0, p0, Lbtj;->w:Lbgx;

    .line 766
    const/16 v6, 0xa

    .line 767
    goto :goto_0

    .line 769
    :pswitch_1
    iget-object v0, p0, Lbtj;->w:Lbgx;

    .line 770
    const/16 v6, 0xb

    .line 771
    goto :goto_0

    .line 773
    :pswitch_2
    iget-object v0, p0, Lbtj;->w:Lbgx;

    .line 774
    const/16 v6, 0xc

    .line 775
    goto :goto_0

    .line 777
    :pswitch_3
    iget-object v0, p0, Lbtj;->x:Lbgx;

    move v6, v8

    .line 778
    goto :goto_0

    .line 780
    :pswitch_4
    iget-object v0, p0, Lbtj;->y:Lbgx;

    move v6, v8

    goto :goto_0

    .line 794
    :pswitch_5
    iget-object v0, p0, Lbtj;->G:Lbfw;

    sget-object v3, Lcfn;->a:Landroid/graphics/Matrix;

    move-object v1, p1

    move-object v2, p1

    move v5, p3

    invoke-static/range {v0 .. v7}, Lbfu;->a(Lbgn;Lbhb;Lbhb;Landroid/graphics/Matrix;Landroid/graphics/Matrix;FILbgs;)V

    goto :goto_1

    .line 800
    :pswitch_6
    iget-object v1, p0, Lbtj;->G:Lbfw;

    .line 801
    iget-object v0, p0, Lbtj;->G:Lbfw;

    iget-object v2, p0, Lbtj;->E:Lbfw;

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lbtj;->F:Lbfw;

    iput-object v0, p0, Lbtj;->G:Lbfw;

    .line 802
    :goto_2
    iget-object v0, p0, Lbtj;->G:Lbfw;

    sget-object v3, Lcfn;->a:Landroid/graphics/Matrix;

    move-object v2, p1

    move v5, p3

    move v6, v8

    invoke-static/range {v0 .. v7}, Lbfu;->a(Lbgn;Lbhb;Lbhb;Landroid/graphics/Matrix;Landroid/graphics/Matrix;FILbgs;)V

    goto :goto_1

    .line 801
    :cond_0
    iget-object v0, p0, Lbtj;->E:Lbfw;

    iput-object v0, p0, Lbtj;->G:Lbfw;

    goto :goto_2

    .line 763
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch

    .line 790
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method

.method private a(Lbhb;Landroid/graphics/Matrix;Z[FLbgn;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 920
    invoke-interface {p1}, Lbhb;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 921
    iget-object v0, p0, Lbtj;->s:Lbgx;

    move-object v3, v0

    .line 926
    :goto_0
    if-eqz p3, :cond_2

    .line 927
    const-string v0, "target"

    invoke-static {p5, v0, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    const-string v0, "source"

    invoke-static {p1, v0, v4}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    if-eq p5, p1, :cond_1

    move v0, v1

    :goto_1
    const-string v4, "Attempting to copy source to target"

    invoke-static {v0, v4}, Lcec;->a(ZLjava/lang/CharSequence;)V

    new-instance v0, Lbhg;

    const/4 v4, 0x4

    invoke-direct {v0, v4, p4}, Lbhg;-><init>(I[F)V

    new-instance v4, Lbhg;

    const/4 v5, 0x3

    const/16 v6, 0xc

    new-array v6, v6, [F

    fill-array-data v6, :array_0

    invoke-direct {v4, v5, v6}, Lbhg;-><init>(I[F)V

    new-instance v5, Lbhe;

    invoke-direct {v5}, Lbhe;-><init>()V

    const-string v6, "a_position"

    invoke-virtual {v5, v6, v0}, Lbhe;->a(Ljava/lang/String;Lbhg;)Lbhe;

    move-result-object v0

    const-string v5, "a_texcoord"

    invoke-virtual {v0, v5, v4}, Lbhe;->a(Ljava/lang/String;Lbhg;)Lbhe;

    move-result-object v0

    invoke-virtual {v0}, Lbhe;->a()Lbhd;

    move-result-object v0

    invoke-static {v3}, Lbgs;->a(Lbgx;)Lbgs;

    move-result-object v3

    const-string v4, "texcoord_transform"

    invoke-virtual {v3, v4, p2}, Lbgs;->b(Ljava/lang/String;Landroid/graphics/Matrix;)Lbgs;

    move-result-object v4

    const-string v5, "sampler_source"

    invoke-virtual {v4, v5, p1}, Lbgs;->b(Ljava/lang/String;Lbhb;)Lbgs;

    invoke-static {v1, p5, v0, v2, v3}, Lbfz;->a(ILbgn;Lbhd;ILbgs;)V

    .line 931
    :goto_2
    return-void

    .line 923
    :cond_0
    iget-object v0, p0, Lbtj;->A:Lbgx;

    move-object v3, v0

    goto :goto_0

    :cond_1
    move v0, v2

    .line 927
    goto :goto_1

    .line 929
    :cond_2
    invoke-static {p5, p1, p2, v3}, Lbfu;->a(Lbgn;Lbhb;Landroid/graphics/Matrix;Lbgx;)V

    goto :goto_2

    .line 927
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private a(Lbhb;Landroid/graphics/Matrix;Z[FLbgn;Lbsb;FZ)V
    .locals 6

    .prologue
    .line 943
    iget-object v5, p0, Lbtj;->E:Lbfw;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lbtj;->a(Lbhb;Landroid/graphics/Matrix;Z[FLbgn;)V

    .line 948
    if-eqz p8, :cond_0

    .line 949
    iget-object v0, p0, Lbtj;->E:Lbfw;

    invoke-virtual {p6, v0, p5, p7}, Lbsb;->a(Lbhb;Lbgn;F)V

    .line 953
    :goto_0
    return-void

    .line 951
    :cond_0
    iget-object v0, p0, Lbtj;->E:Lbfw;

    invoke-virtual {p6, v0, p5, p7}, Lbsb;->b(Lbhb;Lbgn;F)V

    goto :goto_0
.end method

.method static synthetic a(Lbtj;Lbgn;)V
    .locals 4

    .prologue
    .line 62
    iget v0, p0, Lbtj;->K:I

    invoke-interface {p1}, Lbgn;->d()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget v0, p0, Lbtj;->L:I

    invoke-interface {p1}, Lbgn;->e()I

    move-result v1

    if-eq v0, v1, :cond_1

    :cond_0
    const-string v0, "Render sink dimensions changed without calling setNewSize. Got (%s, %s), expected (%s, %s)"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-interface {p1}, Lbgn;->d()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-interface {p1}, Lbgn;->e()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lbtj;->K:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lbtj;->L:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    sget-object v0, Lbtj;->a:Ljava/lang/String;

    :cond_1
    return-void
.end method

.method static synthetic a(Lbtj;Lbth;[Lbtm;Lbge;JLayj;Layl;)V
    .locals 22

    .prologue
    .line 62
    invoke-interface/range {p6 .. p6}, Layj;->d()Z

    move-result v3

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->E:Lbfw;

    move-object/from16 v0, p0

    iput-object v3, v0, Lbtj;->G:Lbfw;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->G:Lbfw;

    invoke-interface/range {p6 .. p6}, Layj;->e()[F

    move-result-object v4

    invoke-static {v3, v4}, Lbfz;->a(Lbgn;[F)V

    const/4 v3, 0x0

    move v12, v3

    :goto_0
    move-object/from16 v0, p1

    iget-object v3, v0, Lbth;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v12, v3, :cond_0

    move-object/from16 v0, p1

    iget-object v3, v0, Lbth;->a:Ljava/util/List;

    invoke-interface {v3, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lbof;

    aget-object v13, p2, v12

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->l:Landroid/graphics/Matrix;

    iget-object v4, v13, Lbtm;->a:Landroid/graphics/Matrix;

    invoke-virtual {v3, v4}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->v:Lbgx;

    invoke-static {v3}, Lbgs;->a(Lbgx;)Lbgs;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->i:Lcgc;

    invoke-virtual {v3}, Lcgc;->a()Ljava/lang/Object;

    move-result-object v3

    move-object v11, v3

    check-cast v11, Lbgd;

    move-object/from16 v0, p6

    move-object/from16 v1, p7

    invoke-interface {v0, v9, v1, v11}, Layj;->a(Lbof;Layl;Lbgd;)V

    const-string v3, "quad_transform"

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v11, v5}, Lbgs;->a(Ljava/lang/String;Lbgd;Z)Lbgs;

    invoke-interface/range {p3 .. p3}, Lbge;->d()I

    move-result v5

    invoke-interface/range {p3 .. p3}, Lbge;->e()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v10, v0, Lbtj;->t:Lbgx;

    move-object/from16 v3, p0

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v3 .. v10}, Lbtj;->a(Lbgs;IILayj;Layl;Lbof;Lbgx;)V

    invoke-virtual {v9}, Lbof;->c()Lbbs;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v3}, Lbtj;->a(Lbgs;Lbbs;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->o:Lcgw;

    const-string v5, "Setup effect and primary color correction"

    invoke-virtual {v3, v5}, Lcgw;->b(Ljava/lang/String;)V

    iget-object v3, v9, Lbof;->a:Lbhl;

    iget-object v6, v3, Lbhl;->c:Lbhb;

    iget-object v7, v13, Lbtm;->a:Landroid/graphics/Matrix;

    iget-boolean v8, v13, Lbtm;->b:Z

    iget-object v9, v13, Lbtm;->c:[F

    move-object/from16 v0, p0

    iget-object v10, v0, Lbtj;->p:Lbfw;

    move-object/from16 v5, p0

    invoke-direct/range {v5 .. v10}, Lbtj;->a(Lbhb;Landroid/graphics/Matrix;Z[FLbgn;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->o:Lcgw;

    const-string v5, "Copy external texture"

    invoke-virtual {v3, v5}, Lcgw;->b(Ljava/lang/String;)V

    const-string v3, "sampler_primary_frame"

    move-object/from16 v0, p0

    iget-object v5, v0, Lbtj;->p:Lbfw;

    invoke-virtual {v4, v3, v5}, Lbgs;->a(Ljava/lang/String;Lbhb;)Lbgs;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->G:Lbfw;

    const/4 v5, 0x0

    invoke-static {v3, v5, v4}, Lbfu;->a(Lbgn;ILbgs;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->i:Lcgc;

    invoke-virtual {v3, v11}, Lcgc;->a(Ljava/lang/Object;)V

    add-int/lit8 v3, v12, 0x1

    move v12, v3

    goto/16 :goto_0

    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->o:Lcgw;

    const-string v4, "Image shader processing"

    invoke-virtual {v3, v4}, Lcgw;->b(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move-object/from16 v2, p7

    invoke-direct {v0, v1, v2}, Lbtj;->a(Layj;Layl;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->G:Lbfw;

    invoke-interface/range {p6 .. p7}, Layj;->v(Layl;)F

    move-result v4

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v3, v4}, Lbtj;->a(Lbgn;Lbfw;F)V

    invoke-interface/range {p3 .. p5}, Lbge;->a(J)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->o:Lcgw;

    const-string v4, "Render into video sink"

    invoke-virtual {v3, v4}, Lcgw;->b(Ljava/lang/String;)V

    :goto_1
    return-void

    :cond_1
    move-object/from16 v0, p1

    iget-object v3, v0, Lbth;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_4

    move-object/from16 v0, p1

    iget-object v3, v0, Lbth;->a:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbof;

    const/4 v4, 0x0

    aget-object v4, p2, v4

    iget-object v14, v4, Lbtm;->a:Landroid/graphics/Matrix;

    iget-boolean v15, v4, Lbtm;->b:Z

    iget-object v0, v4, Lbtm;->c:[F

    move-object/from16 v16, v0

    iget-object v13, v3, Lbof;->a:Lbhl;

    invoke-virtual {v3}, Lbof;->c()Lbbs;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->l:Landroid/graphics/Matrix;

    invoke-virtual {v3, v14}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->t:Lbgx;

    invoke-static {v3}, Lbgs;->a(Lbgx;)Lbgs;

    move-result-object v4

    invoke-interface/range {p3 .. p3}, Lbge;->d()I

    move-result v5

    invoke-interface/range {p3 .. p3}, Lbge;->e()I

    move-result v6

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lbtj;->t:Lbgx;

    move-object/from16 v3, p0

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v3 .. v10}, Lbtj;->a(Lbgs;IILayj;Layl;Lbof;Lbgx;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v11}, Lbtj;->a(Lbgs;Lbbs;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->o:Lcgw;

    const-string v5, "Setup effect and primary color correction"

    invoke-virtual {v3, v5}, Lcgw;->b(Ljava/lang/String;)V

    invoke-static/range {p7 .. p7}, Lbag;->e(Layl;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v11, v0, Lbtj;->D:Lbsb;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->f:Landroid/content/res/AssetManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbtj;->e:Landroid/content/res/Resources;

    move-object/from16 v0, p6

    invoke-interface {v0, v3, v5}, Layj;->a(Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;

    move-result-object v3

    invoke-interface {v3}, Layj;->g_()Layc;

    move-result-object v3

    invoke-virtual {v11, v3}, Lbsb;->a(Layc;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->f:Landroid/content/res/AssetManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbtj;->e:Landroid/content/res/Resources;

    move-object/from16 v0, p6

    invoke-interface {v0, v3, v5}, Layj;->a(Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;

    move-result-object v3

    move-object/from16 v0, p7

    invoke-interface {v3, v0}, Layj;->u(Layl;)F

    move-result v12

    :goto_2
    invoke-virtual {v11}, Lbsb;->b()Layc;

    move-result-object v3

    sget-object v5, Layc;->a:Layc;

    if-ne v3, v5, :cond_3

    iget-object v6, v13, Lbhl;->c:Lbhb;

    move-object/from16 v0, p0

    iget-object v10, v0, Lbtj;->p:Lbfw;

    move-object/from16 v5, p0

    move-object v7, v14

    move v8, v15

    move-object/from16 v9, v16

    invoke-direct/range {v5 .. v10}, Lbtj;->a(Lbhb;Landroid/graphics/Matrix;Z[FLbgn;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->o:Lcgw;

    const-string v5, "Copy external texture"

    invoke-virtual {v3, v5}, Lcgw;->b(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->H:Lbfw;

    :goto_3
    const-string v5, "sampler_primary_frame"

    move-object/from16 v0, p0

    iget-object v6, v0, Lbtj;->p:Lbfw;

    invoke-virtual {v4, v5, v6}, Lbgs;->a(Ljava/lang/String;Lbhb;)Lbgs;

    move-result-object v5

    const-string v6, "sampler_primary_blurred_source"

    invoke-virtual {v5, v6, v3}, Lbgs;->a(Ljava/lang/String;Lbhb;)Lbgs;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->E:Lbfw;

    move-object/from16 v0, p0

    iput-object v3, v0, Lbtj;->G:Lbfw;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->G:Lbfw;

    const/4 v5, 0x0

    invoke-static {v3, v5, v4}, Lbfu;->a(Lbgn;ILbgs;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->o:Lcgw;

    const-string v4, "Image shader processing"

    invoke-virtual {v3, v4}, Lcgw;->b(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move-object/from16 v2, p7

    invoke-direct {v0, v1, v2}, Lbtj;->a(Layj;Layl;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->G:Lbfw;

    invoke-interface/range {p6 .. p7}, Layj;->v(Layl;)F

    move-result v4

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v3, v4}, Lbtj;->a(Lbgn;Lbfw;F)V

    invoke-interface/range {p3 .. p5}, Lbge;->a(J)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->o:Lcgw;

    const-string v4, "Render into video sink"

    invoke-virtual {v3, v4}, Lcgw;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_2
    move-object/from16 v0, p0

    iget-object v11, v0, Lbtj;->C:Lbsb;

    invoke-interface/range {p6 .. p6}, Layj;->g_()Layc;

    move-result-object v3

    invoke-virtual {v11, v3}, Lbsb;->a(Layc;)V

    invoke-interface/range {p6 .. p7}, Layj;->u(Layl;)F

    move-result v12

    goto/16 :goto_2

    :cond_3
    iget-object v6, v13, Lbhl;->c:Lbhb;

    move-object/from16 v0, p0

    iget-object v10, v0, Lbtj;->p:Lbfw;

    const/4 v13, 0x1

    move-object/from16 v5, p0

    move-object v7, v14

    move v8, v15

    move-object/from16 v9, v16

    invoke-direct/range {v5 .. v13}, Lbtj;->a(Lbhb;Landroid/graphics/Matrix;Z[FLbgn;Lbsb;FZ)V

    invoke-virtual {v11}, Lbsb;->c()Lbhb;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lbtj;->o:Lcgw;

    const-string v6, "Copy external texture and apply blur"

    invoke-virtual {v5, v6}, Lcgw;->b(Ljava/lang/String;)V

    goto :goto_3

    :cond_4
    move-object/from16 v0, p1

    iget-object v3, v0, Lbth;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_8

    move-object/from16 v0, p1

    iget-boolean v3, v0, Lbth;->b:Z

    if-eqz v3, :cond_8

    move-object/from16 v0, p1

    iget-object v3, v0, Lbth;->a:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lbof;

    move-object/from16 v0, p1

    iget-object v4, v0, Lbth;->a:Ljava/util/List;

    const/4 v5, 0x1

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lbof;

    const/4 v5, 0x0

    aget-object v5, p2, v5

    const/4 v6, 0x1

    aget-object v6, p2, v6

    iget-object v14, v5, Lbtm;->a:Landroid/graphics/Matrix;

    iget-boolean v15, v5, Lbtm;->b:Z

    iget-object v0, v5, Lbtm;->c:[F

    move-object/from16 v16, v0

    iget-object v0, v6, Lbtm;->a:Landroid/graphics/Matrix;

    move-object/from16 v17, v0

    iget-boolean v0, v6, Lbtm;->b:Z

    move/from16 v18, v0

    iget-object v0, v6, Lbtm;->c:[F

    move-object/from16 v19, v0

    iget-object v13, v3, Lbof;->a:Lbhl;

    iget-object v0, v4, Lbof;->a:Lbhl;

    move-object/from16 v20, v0

    invoke-virtual {v3}, Lbof;->c()Lbbs;

    move-result-object v11

    invoke-virtual {v4}, Lbof;->c()Lbbs;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->l:Landroid/graphics/Matrix;

    invoke-virtual {v3, v14}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->u:Lbgx;

    invoke-static {v3}, Lbgs;->a(Lbgx;)Lbgs;

    move-result-object v4

    invoke-interface/range {p3 .. p3}, Lbge;->d()I

    move-result v5

    invoke-interface/range {p3 .. p3}, Lbge;->e()I

    move-result v6

    const/4 v9, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lbtj;->u:Lbgx;

    move-object/from16 v3, p0

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v3 .. v10}, Lbtj;->a(Lbgs;IILayj;Layl;Lbof;Lbgx;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v11}, Lbtj;->a(Lbgs;Lbbs;)V

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lbtj;->a(Lbbs;)Lbhb;

    move-result-object v3

    const-string v5, "sampler_secondary_color_correct"

    invoke-virtual {v4, v5, v3}, Lbgs;->a(Ljava/lang/String;Lbhb;)Lbgs;

    if-eqz v12, :cond_5

    const-string v3, "color_correct_active_b"

    const/4 v5, 0x1

    invoke-virtual {v4, v3, v5}, Lbgs;->a(Ljava/lang/String;I)Lbgs;

    move-result-object v3

    const-string v5, "color_correct_factor_b"

    iget v6, v12, Lbbs;->b:F

    invoke-virtual {v3, v5, v6}, Lbgs;->a(Ljava/lang/String;F)Lbgs;

    :goto_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->o:Lcgw;

    const-string v5, "Setup effect and color correction"

    invoke-virtual {v3, v5}, Lcgw;->b(Ljava/lang/String;)V

    invoke-static/range {p7 .. p7}, Lbag;->e(Layl;)Z

    move-result v3

    if-eqz v3, :cond_6

    move-object/from16 v0, p0

    iget-object v11, v0, Lbtj;->D:Lbsb;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->D:Lbsb;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbtj;->f:Landroid/content/res/AssetManager;

    move-object/from16 v0, p0

    iget-object v6, v0, Lbtj;->e:Landroid/content/res/Resources;

    move-object/from16 v0, p6

    invoke-interface {v0, v5, v6}, Layj;->a(Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;

    move-result-object v5

    invoke-interface {v5}, Layj;->g_()Layc;

    move-result-object v5

    invoke-virtual {v3, v5}, Lbsb;->a(Layc;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->f:Landroid/content/res/AssetManager;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbtj;->e:Landroid/content/res/Resources;

    move-object/from16 v0, p6

    invoke-interface {v0, v3, v5}, Layj;->a(Landroid/content/res/AssetManager;Landroid/content/res/Resources;)Layj;

    move-result-object v3

    move-object/from16 v0, p7

    invoke-interface {v3, v0}, Layj;->u(Layl;)F

    move-result v12

    :goto_5
    invoke-virtual {v11}, Lbsb;->b()Layc;

    move-result-object v3

    sget-object v5, Layc;->a:Layc;

    if-ne v3, v5, :cond_7

    iget-object v6, v13, Lbhl;->c:Lbhb;

    move-object/from16 v0, p0

    iget-object v10, v0, Lbtj;->p:Lbfw;

    move-object/from16 v5, p0

    move-object v7, v14

    move v8, v15

    move-object/from16 v9, v16

    invoke-direct/range {v5 .. v10}, Lbtj;->a(Lbhb;Landroid/graphics/Matrix;Z[FLbgn;)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lbtj;->H:Lbfw;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->o:Lcgw;

    const-string v5, "Copy primary external texture"

    invoke-virtual {v3, v5}, Lcgw;->b(Ljava/lang/String;)V

    move-object/from16 v0, v20

    iget-object v6, v0, Lbhl;->c:Lbhb;

    move-object/from16 v0, p0

    iget-object v10, v0, Lbtj;->q:Lbfw;

    move-object/from16 v5, p0

    move-object/from16 v7, v17

    move/from16 v8, v18

    move-object/from16 v9, v19

    invoke-direct/range {v5 .. v10}, Lbtj;->a(Lbhb;Landroid/graphics/Matrix;Z[FLbgn;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->H:Lbfw;

    move-object/from16 v0, p0

    iget-object v5, v0, Lbtj;->o:Lcgw;

    const-string v6, "Copy secondary external texture"

    invoke-virtual {v5, v6}, Lcgw;->b(Ljava/lang/String;)V

    move-object v5, v11

    :goto_6
    const-string v6, "sampler_primary_frame"

    move-object/from16 v0, p0

    iget-object v7, v0, Lbtj;->p:Lbfw;

    invoke-virtual {v4, v6, v7}, Lbgs;->a(Ljava/lang/String;Lbhb;)Lbgs;

    move-result-object v6

    const-string v7, "sampler_primary_blurred_source"

    invoke-virtual {v6, v7, v5}, Lbgs;->a(Ljava/lang/String;Lbhb;)Lbgs;

    move-result-object v5

    const-string v6, "sampler_secondary_frame"

    move-object/from16 v0, p0

    iget-object v7, v0, Lbtj;->q:Lbfw;

    invoke-virtual {v5, v6, v7}, Lbgs;->a(Ljava/lang/String;Lbhb;)Lbgs;

    move-result-object v5

    const-string v6, "sampler_secondary_blurred_source"

    invoke-virtual {v5, v6, v3}, Lbgs;->a(Ljava/lang/String;Lbhb;)Lbgs;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->E:Lbfw;

    move-object/from16 v0, p0

    iput-object v3, v0, Lbtj;->G:Lbfw;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->G:Lbfw;

    const/4 v5, 0x0

    invoke-static {v3, v5, v4}, Lbfu;->a(Lbgn;ILbgs;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->o:Lcgw;

    const-string v4, "Image shader processing"

    invoke-virtual {v3, v4}, Lcgw;->b(Ljava/lang/String;)V

    move-object/from16 v0, p0

    move-object/from16 v1, p6

    move-object/from16 v2, p7

    invoke-direct {v0, v1, v2}, Lbtj;->a(Layj;Layl;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->G:Lbfw;

    invoke-interface/range {p6 .. p7}, Layj;->v(Layl;)F

    move-result v4

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v1, v3, v4}, Lbtj;->a(Lbgn;Lbfw;F)V

    invoke-interface/range {p3 .. p5}, Lbge;->a(J)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->o:Lcgw;

    const-string v4, "Render into video sink"

    invoke-virtual {v3, v4}, Lcgw;->b(Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_5
    const-string v3, "color_correct_active_b"

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Lbgs;->a(Ljava/lang/String;I)Lbgs;

    move-result-object v3

    const-string v5, "color_correct_factor_b"

    const/4 v6, 0x0

    invoke-virtual {v3, v5, v6}, Lbgs;->a(Ljava/lang/String;F)Lbgs;

    goto/16 :goto_4

    :cond_6
    move-object/from16 v0, p0

    iget-object v11, v0, Lbtj;->C:Lbsb;

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->C:Lbsb;

    invoke-interface/range {p6 .. p6}, Layj;->g_()Layc;

    move-result-object v5

    invoke-virtual {v3, v5}, Lbsb;->a(Layc;)V

    invoke-interface/range {p6 .. p7}, Layj;->u(Layl;)F

    move-result v12

    goto/16 :goto_5

    :cond_7
    iget-object v6, v13, Lbhl;->c:Lbhb;

    move-object/from16 v0, p0

    iget-object v10, v0, Lbtj;->p:Lbfw;

    const/4 v13, 0x1

    move-object/from16 v5, p0

    move-object v7, v14

    move v8, v15

    move-object/from16 v9, v16

    invoke-direct/range {v5 .. v13}, Lbtj;->a(Lbhb;Landroid/graphics/Matrix;Z[FLbgn;Lbsb;FZ)V

    invoke-virtual {v11}, Lbsb;->c()Lbhb;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v3, v0, Lbtj;->o:Lcgw;

    const-string v5, "Copy primary external texture and apply blur"

    invoke-virtual {v3, v5}, Lcgw;->b(Ljava/lang/String;)V

    move-object/from16 v0, v20

    iget-object v6, v0, Lbhl;->c:Lbhb;

    move-object/from16 v0, p0

    iget-object v10, v0, Lbtj;->q:Lbfw;

    const/4 v13, 0x0

    move-object/from16 v5, p0

    move-object/from16 v7, v17

    move/from16 v8, v18

    move-object/from16 v9, v19

    invoke-direct/range {v5 .. v13}, Lbtj;->a(Lbhb;Landroid/graphics/Matrix;Z[FLbgn;Lbsb;FZ)V

    invoke-virtual {v11}, Lbsb;->d()Lbhb;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lbtj;->o:Lcgw;

    const-string v6, "Copy secondary external texture and apply blur"

    invoke-virtual {v5, v6}, Lcgw;->b(Ljava/lang/String;)V

    move-object v5, v14

    goto/16 :goto_6

    :cond_8
    move-object/from16 v0, p1

    iget-object v3, v0, Lbth;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x37

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "too many quads for non-composite rendering: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcec;->a(Ljava/lang/CharSequence;)Ljava/lang/IllegalArgumentException;

    move-result-object v3

    throw v3
.end method

.method static synthetic a(Layl;Layj;)Z
    .locals 4

    .prologue
    .line 62
    if-eqz p1, :cond_0

    iget v0, p0, Layl;->f:I

    iget v1, p0, Layl;->g:I

    if-ne v0, v1, :cond_0

    invoke-interface {p1, p0}, Layj;->r(Layl;)I

    move-result v0

    if-lez v0, :cond_0

    iget-wide v0, p0, Layl;->b:J

    iget-wide v2, p0, Layl;->c:J

    sub-long/2addr v0, v2

    const v2, 0xf4240

    invoke-interface {p1, p0}, Layj;->r(Layl;)I

    move-result v3

    div-int/2addr v2, v3

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Layl;->b:J

    iput-wide v0, p0, Layl;->c:J

    iget v0, p0, Layl;->f:I

    iput v0, p0, Layl;->g:I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lbtj;)[Lbtm;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lbtj;->k:[Lbtm;

    return-object v0
.end method

.method private b(Layj;Layl;)Lbhb;
    .locals 3

    .prologue
    .line 728
    iget-object v0, p2, Layl;->p:Ljava/lang/String;

    iget-object v1, p0, Lbtj;->M:Ljava/lang/String;

    invoke-static {v0, v1}, Lbqh;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lbtj;->N:Layj;

    if-eq p1, v0, :cond_1

    .line 730
    :cond_0
    iget-object v0, p0, Lbtj;->r:Lbhb;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 731
    iget-object v0, p2, Layl;->p:Ljava/lang/String;

    iput-object v0, p0, Lbtj;->M:Ljava/lang/String;

    .line 732
    iput-object p1, p0, Lbtj;->N:Layj;

    .line 735
    :try_start_0
    iget-object v0, p0, Lbtj;->c:Landroid/content/Context;

    iget-object v1, p0, Lbtj;->G:Lbfw;

    .line 736
    invoke-interface {v1}, Lbfw;->d()I

    move-result v1

    iget-object v2, p0, Lbtj;->G:Lbfw;

    .line 737
    invoke-interface {v2}, Lbfw;->e()I

    move-result v2

    .line 735
    invoke-interface {p1, v0, p2, v1, v2}, Layj;->a(Landroid/content/Context;Layl;II)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 742
    :goto_0
    if-eqz v0, :cond_2

    iget-object v1, p0, Lbtj;->d:Lbgf;

    const/4 v2, 0x0

    .line 743
    invoke-virtual {v1, v0, v2}, Lbgf;->a(Landroid/graphics/Bitmap;I)Lbfw;

    move-result-object v0

    .line 744
    :goto_1
    iput-object v0, p0, Lbtj;->r:Lbhb;

    .line 747
    :cond_1
    iget-object v0, p0, Lbtj;->r:Lbhb;

    return-object v0

    .line 738
    :catch_0
    move-exception v0

    .line 739
    sget-object v1, Lbtj;->a:Ljava/lang/String;

    const-string v2, "Failed to render title"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 740
    const/4 v0, 0x0

    goto :goto_0

    .line 743
    :cond_2
    iget-object v0, p0, Lbtj;->d:Lbgf;

    .line 744
    invoke-virtual {v0}, Lbgf;->d()Lbfw;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 317
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lbtj;->I:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 318
    iget-object v0, p0, Lbtj;->I:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcgk;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 317
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 320
    :cond_0
    iget-object v0, p0, Lbtj;->I:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 321
    iput-object v2, p0, Lbtj;->I:Landroid/util/SparseArray;

    .line 322
    iget-object v0, p0, Lbtj;->J:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbhb;

    .line 323
    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    goto :goto_1

    .line 325
    :cond_1
    iget-object v0, p0, Lbtj;->J:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 326
    iput-object v2, p0, Lbtj;->J:Ljava/util/HashMap;

    .line 328
    iget-object v0, p0, Lbtj;->r:Lbhb;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 329
    iget-object v0, p0, Lbtj;->H:Lbfw;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 331
    iget-object v0, p0, Lbtj;->p:Lbfw;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 332
    iget-object v0, p0, Lbtj;->q:Lbfw;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 333
    iget-object v0, p0, Lbtj;->E:Lbfw;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 334
    iget-object v0, p0, Lbtj;->F:Lbfw;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 335
    iget-object v0, p0, Lbtj;->C:Lbsb;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 336
    iget-object v0, p0, Lbtj;->D:Lbsb;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 338
    iput-object v2, p0, Lbtj;->s:Lbgx;

    .line 339
    iput-object v2, p0, Lbtj;->t:Lbgx;

    .line 340
    iput-object v2, p0, Lbtj;->u:Lbgx;

    .line 341
    iput-object v2, p0, Lbtj;->v:Lbgx;

    .line 342
    iput-object v2, p0, Lbtj;->w:Lbgx;

    .line 343
    iput-object v2, p0, Lbtj;->x:Lbgx;

    .line 344
    iput-object v2, p0, Lbtj;->y:Lbgx;

    .line 345
    iput-object v2, p0, Lbtj;->z:Lbgx;

    .line 346
    iput-object v2, p0, Lbtj;->A:Lbgx;

    .line 347
    iput-object v2, p0, Lbtj;->B:Lbgx;

    .line 348
    return-void
.end method

.method public a(II)V
    .locals 1

    .prologue
    .line 887
    const-string v0, "width"

    invoke-static {p1, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    .line 888
    const-string v0, "height"

    invoke-static {p2, v0}, Lcec;->a(ILjava/lang/CharSequence;)I

    .line 889
    iget v0, p0, Lbtj;->K:I

    if-ne p1, v0, :cond_0

    iget v0, p0, Lbtj;->L:I

    if-ne p2, v0, :cond_0

    .line 911
    :goto_0
    return-void

    .line 893
    :cond_0
    iget-object v0, p0, Lbtj;->p:Lbfw;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 894
    iget-object v0, p0, Lbtj;->q:Lbfw;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 895
    iget-object v0, p0, Lbtj;->d:Lbgf;

    invoke-virtual {v0, p1, p2}, Lbgf;->b(II)Lbfw;

    move-result-object v0

    iput-object v0, p0, Lbtj;->p:Lbfw;

    .line 896
    iget-object v0, p0, Lbtj;->d:Lbgf;

    invoke-virtual {v0, p1, p2}, Lbgf;->b(II)Lbfw;

    move-result-object v0

    iput-object v0, p0, Lbtj;->q:Lbfw;

    .line 898
    iget-object v0, p0, Lbtj;->E:Lbfw;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 899
    iget-object v0, p0, Lbtj;->d:Lbgf;

    invoke-virtual {v0, p1, p2}, Lbgf;->b(II)Lbfw;

    move-result-object v0

    iput-object v0, p0, Lbtj;->E:Lbfw;

    .line 900
    iget-object v0, p0, Lbtj;->F:Lbfw;

    invoke-static {v0}, Lcgl;->a(Lcgk;)V

    .line 901
    iget-object v0, p0, Lbtj;->d:Lbgf;

    invoke-virtual {v0, p1, p2}, Lbgf;->b(II)Lbfw;

    move-result-object v0

    iput-object v0, p0, Lbtj;->F:Lbfw;

    .line 903
    iget-object v0, p0, Lbtj;->C:Lbsb;

    invoke-virtual {v0, p1, p2}, Lbsb;->a(II)V

    .line 904
    iget-object v0, p0, Lbtj;->D:Lbsb;

    invoke-virtual {v0, p1, p2}, Lbsb;->a(II)V

    .line 907
    const/4 v0, 0x0

    iput-object v0, p0, Lbtj;->M:Ljava/lang/String;

    .line 909
    iput p1, p0, Lbtj;->K:I

    .line 910
    iput p2, p0, Lbtj;->L:I

    goto :goto_0
.end method

.method public a(Landroid/graphics/RectF;)V
    .locals 5

    .prologue
    const/high16 v4, 0x3f800000    # 1.0f

    .line 240
    iget-object v0, p0, Lbtj;->l:Landroid/graphics/Matrix;

    sget-object v1, Lbtj;->b:Landroid/graphics/RectF;

    invoke-virtual {v0, p1, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    .line 241
    iget v0, p1, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    sub-float v1, v4, v1

    iget v2, p1, Landroid/graphics/RectF;->right:F

    iget v3, p1, Landroid/graphics/RectF;->top:F

    sub-float v3, v4, v3

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    .line 242
    return-void
.end method

.method public a(Layj;)V
    .locals 2

    .prologue
    .line 233
    const-string v0, "effect"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Layj;

    invoke-interface {v0}, Layj;->f_()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 234
    invoke-direct {p0, v0}, Lbtj;->a(I)Lbhb;

    goto :goto_0

    .line 236
    :cond_0
    return-void
.end method

.method public a(Lbth;Lbge;JLayj;Layl;)V
    .locals 13

    .prologue
    .line 251
    const-string v2, "effect"

    const/4 v3, 0x0

    move-object/from16 v0, p5

    invoke-static {v0, v2, v3}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    .line 252
    iget-object v2, p0, Lbtj;->o:Lcgw;

    iget-object v3, p1, Lbth;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    const/16 v5, 0x25

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v5, "renderFrame() with "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " frames"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcgw;->a(Ljava/lang/String;)V

    .line 254
    const/4 v2, 0x0

    move v4, v2

    :goto_0
    iget-object v2, p1, Lbth;->a:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v4, v2, :cond_2

    .line 255
    iget-object v2, p1, Lbth;->a:Ljava/util/List;

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lbof;

    .line 256
    iget-object v3, p0, Lbtj;->k:[Lbtm;

    aget-object v5, v3, v4

    .line 257
    iget-object v3, p0, Lbtj;->h:Lcgc;

    invoke-virtual {v3}, Lcgc;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Matrix;

    iput-object v3, v5, Lbtm;->a:Landroid/graphics/Matrix;

    .line 258
    iget-object v3, p0, Lbtj;->j:Lcgc;

    invoke-virtual {v3}, Lcgc;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [F

    iput-object v3, v5, Lbtm;->c:[F

    .line 260
    iget-object v3, v2, Lbof;->a:Lbhl;

    iget-object v3, v3, Lbhl;->d:[F

    invoke-direct {p0, v3}, Lbtj;->a([F)Landroid/graphics/Matrix;

    move-result-object v3

    .line 261
    invoke-virtual {v2}, Lbof;->a()[F

    move-result-object v6

    invoke-direct {p0, v6}, Lbtj;->a([F)Landroid/graphics/Matrix;

    move-result-object v6

    .line 263
    iget-object v7, v5, Lbtm;->a:Landroid/graphics/Matrix;

    iget-object v8, v5, Lbtm;->c:[F

    .line 264
    invoke-virtual {v2}, Lbof;->b()Lbxi;

    move-result-object v2

    .line 263
    if-nez v2, :cond_0

    invoke-virtual {v7, v6}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    const/4 v2, 0x0

    :goto_1
    iput-boolean v2, v5, Lbtm;->b:Z

    .line 266
    iget-object v2, p0, Lbtj;->h:Lcgc;

    invoke-virtual {v2, v6}, Lcgc;->a(Ljava/lang/Object;)V

    .line 267
    iget-object v2, p0, Lbtj;->h:Lcgc;

    invoke-virtual {v2, v3}, Lcgc;->a(Ljava/lang/Object;)V

    .line 254
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_0

    .line 263
    :cond_0
    invoke-virtual {v7}, Landroid/graphics/Matrix;->reset()V

    invoke-virtual {v2}, Lbxi;->c()Z

    move-result v9

    if-nez v9, :cond_1

    invoke-virtual {v2, v8}, Lbxi;->b([F)V

    const/4 v2, 0x1

    :goto_2
    invoke-virtual {v7, v6}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    invoke-virtual {v7, v3}, Landroid/graphics/Matrix;->postConcat(Landroid/graphics/Matrix;)Z

    goto :goto_1

    :cond_1
    const/high16 v8, 0x3f800000    # 1.0f

    invoke-virtual {v2}, Lbxi;->f()F

    move-result v9

    div-float/2addr v8, v9

    const/high16 v9, 0x3f800000    # 1.0f

    sub-float/2addr v9, v8

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    invoke-virtual {v2}, Lbxi;->d()F

    move-result v10

    neg-float v11, v9

    invoke-static {v10, v11, v9}, Lcfn;->a(FFF)F

    move-result v10

    add-float/2addr v10, v9

    invoke-virtual {v2}, Lbxi;->e()F

    move-result v2

    neg-float v11, v9

    invoke-static {v2, v11, v9}, Lcfn;->a(FFF)F

    move-result v2

    sub-float v2, v9, v2

    invoke-virtual {v7, v8, v8}, Landroid/graphics/Matrix;->postScale(FF)Z

    invoke-virtual {v7, v10, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    const/4 v2, 0x0

    goto :goto_2

    .line 270
    :cond_2
    iget-object v2, p0, Lbtj;->o:Lcgw;

    const-string v3, "Populate stabilized matrices"

    invoke-virtual {v2, v3}, Lcgw;->b(Ljava/lang/String;)V

    .line 275
    :try_start_0
    new-instance v2, Lbtk;

    move-object v3, p0

    move-object v4, p2

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object v7, p1

    move-wide/from16 v8, p3

    invoke-direct/range {v2 .. v9}, Lbtk;-><init>(Lbtj;Lbge;Layj;Layl;Lbth;J)V

    invoke-interface {p2, v2}, Lbge;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 301
    :goto_3
    const/4 v2, 0x0

    :goto_4
    iget-object v3, p1, Lbth;->a:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    .line 302
    iget-object v3, p0, Lbtj;->k:[Lbtm;

    aget-object v3, v3, v2

    .line 303
    iget-object v4, p0, Lbtj;->j:Lcgc;

    iget-object v5, v3, Lbtm;->c:[F

    invoke-virtual {v4, v5}, Lcgc;->a(Ljava/lang/Object;)V

    .line 304
    iget-object v4, p0, Lbtj;->h:Lcgc;

    iget-object v3, v3, Lbtm;->a:Landroid/graphics/Matrix;

    invoke-virtual {v4, v3}, Lcgc;->a(Ljava/lang/Object;)V

    .line 301
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 298
    :catch_0
    move-exception v2

    sget-object v2, Lbtj;->a:Ljava/lang/String;

    goto :goto_3

    .line 307
    :cond_3
    iget-object v2, p0, Lbtj;->n:Lcgw;

    invoke-virtual {v2}, Lcgw;->c()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 308
    iget-object v2, p0, Lbtj;->n:Lcgw;

    const-string v3, "render"

    invoke-virtual {v2, v3}, Lcgw;->b(Ljava/lang/String;)V

    .line 309
    iget-object v2, p0, Lbtj;->n:Lcgw;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcgw;->a(Z)V

    .line 311
    :cond_4
    iget-object v2, p0, Lbtj;->n:Lcgw;

    const-string v3, "Frames"

    invoke-virtual {v2, v3}, Lcgw;->a(Ljava/lang/String;)V

    .line 312
    iget-object v2, p0, Lbtj;->o:Lcgw;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcgw;->a(Z)V

    .line 313
    return-void
.end method

.method public b()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v2, 0x0

    const v4, 0x7f08002e

    const v3, 0x7f080032

    .line 216
    iget-object v0, p0, Lbtj;->I:Landroid/util/SparseArray;

    const-string v1, "mDrawableSourceCache"

    invoke-static {v0, v1, v2}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 217
    iget-object v0, p0, Lbtj;->J:Ljava/util/HashMap;

    const-string v1, "mAutoCorrectSourceCache"

    invoke-static {v0, v1, v2}, Lcgp;->b(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    .line 218
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lbtj;->I:Landroid/util/SparseArray;

    .line 219
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lbtj;->J:Ljava/util/HashMap;

    .line 221
    iget-object v0, p0, Lbtj;->d:Lbgf;

    invoke-virtual {v0}, Lbgf;->f()Lbgr;

    move-result-object v0

    const-string v1, "shaderCache"

    invoke-static {v0, v1, v2}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    const v1, 0x7f08000f

    invoke-virtual {v0, v4, v1}, Lbgr;->a(II)Lbgx;

    move-result-object v1

    iput-object v1, p0, Lbtj;->s:Lbgx;

    const v1, 0x7f080030

    const v2, 0x7f080012

    invoke-virtual {v0, v1, v2}, Lbgr;->a(II)Lbgx;

    move-result-object v1

    iput-object v1, p0, Lbtj;->t:Lbgx;

    const v1, 0x7f080031

    const v2, 0x7f080013

    invoke-virtual {v0, v1, v2}, Lbgr;->a(II)Lbgx;

    move-result-object v1

    iput-object v1, p0, Lbtj;->u:Lbgx;

    const v1, 0x7f08002f

    const v2, 0x7f080011

    invoke-virtual {v0, v1, v2}, Lbgr;->a(II)Lbgx;

    move-result-object v1

    iput-object v1, p0, Lbtj;->v:Lbgx;

    const v1, 0x7f080017

    invoke-virtual {v0, v3, v1}, Lbgr;->a(II)Lbgx;

    move-result-object v1

    iput-object v1, p0, Lbtj;->w:Lbgx;

    const v1, 0x7f080014

    invoke-virtual {v0, v3, v1}, Lbgr;->a(II)Lbgx;

    move-result-object v1

    iput-object v1, p0, Lbtj;->x:Lbgx;

    const v1, 0x7f080015

    invoke-virtual {v0, v3, v1}, Lbgr;->a(II)Lbgx;

    move-result-object v1

    iput-object v1, p0, Lbtj;->y:Lbgx;

    const v1, 0x7f08000e

    invoke-virtual {v0, v4, v1}, Lbgr;->a(II)Lbgx;

    move-result-object v1

    iput-object v1, p0, Lbtj;->z:Lbgx;

    const v1, 0x7f080016

    invoke-virtual {v0, v3, v1}, Lbgr;->a(II)Lbgx;

    move-result-object v1

    iput-object v1, p0, Lbtj;->B:Lbgx;

    const v1, 0x7f080010

    invoke-virtual {v0, v4, v1}, Lbgr;->a(II)Lbgx;

    move-result-object v0

    iput-object v0, p0, Lbtj;->A:Lbgx;

    .line 222
    new-instance v0, Lbsb;

    iget-object v1, p0, Lbtj;->d:Lbgf;

    iget-object v2, p0, Lbtj;->B:Lbgx;

    iget-object v3, p0, Lbtj;->A:Lbgx;

    invoke-direct {v0, v1, v2, v3}, Lbsb;-><init>(Lbgf;Lbgx;Lbgx;)V

    iput-object v0, p0, Lbtj;->C:Lbsb;

    .line 223
    new-instance v0, Lbsb;

    iget-object v1, p0, Lbtj;->d:Lbgf;

    iget-object v2, p0, Lbtj;->B:Lbgx;

    iget-object v3, p0, Lbtj;->A:Lbgx;

    invoke-direct {v0, v1, v2, v3}, Lbsb;-><init>(Lbgf;Lbgx;Lbgx;)V

    iput-object v0, p0, Lbtj;->D:Lbsb;

    .line 225
    iget-object v0, p0, Lbtj;->d:Lbgf;

    invoke-virtual {v0}, Lbgf;->d()Lbfw;

    move-result-object v0

    iput-object v0, p0, Lbtj;->r:Lbhb;

    .line 226
    iget-object v0, p0, Lbtj;->d:Lbgf;

    invoke-virtual {v0}, Lbgf;->d()Lbfw;

    move-result-object v0

    iput-object v0, p0, Lbtj;->H:Lbfw;

    .line 227
    iput v5, p0, Lbtj;->K:I

    .line 228
    iput v5, p0, Lbtj;->L:I

    .line 229
    return-void
.end method
