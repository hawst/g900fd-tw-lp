.class public final Ldjg;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmaa;",
        "Lmab;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final p:Z

.field private final q:Lkzl;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 36
    const-string v3, "editactivity"

    new-instance v4, Lmaa;

    invoke-direct {v4}, Lmaa;-><init>()V

    new-instance v5, Lmab;

    invoke-direct {v5}, Lmab;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 41
    iget-object v0, p0, Ldjg;->f:Landroid/content/Context;

    const-class v1, Lkzl;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    iput-object v0, p0, Ldjg;->q:Lkzl;

    .line 42
    iput-object p3, p0, Ldjg;->a:Ljava/lang/String;

    .line 43
    iput-object p4, p0, Ldjg;->b:Ljava/lang/String;

    .line 44
    iput-boolean p5, p0, Ldjg;->p:Z

    .line 45
    return-void
.end method


# virtual methods
.method protected a(Lmaa;)V
    .locals 4

    .prologue
    .line 49
    new-instance v0, Lnuy;

    invoke-direct {v0}, Lnuy;-><init>()V

    iput-object v0, p1, Lmaa;->a:Lnuy;

    .line 50
    iget-object v0, p1, Lmaa;->a:Lnuy;

    .line 52
    iget-object v1, p0, Ldjg;->a:Ljava/lang/String;

    iput-object v1, v0, Lnuy;->a:Ljava/lang/String;

    .line 53
    iget-object v1, p0, Ldjg;->b:Ljava/lang/String;

    iput-object v1, v0, Lnuy;->c:Ljava/lang/String;

    .line 54
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnuy;->d:Ljava/lang/Boolean;

    .line 55
    iget-boolean v1, p0, Ldjg;->p:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnuy;->b:Ljava/lang/Boolean;

    .line 56
    const/4 v1, 0x2

    iput v1, v0, Lnuy;->e:I

    .line 58
    new-instance v1, Loxz;

    invoke-direct {v1}, Loxz;-><init>()V

    iput-object v1, v0, Lnuy;->f:Loxz;

    .line 59
    iget-object v0, v0, Lnuy;->f:Loxz;

    iget-object v1, p0, Ldjg;->q:Lkzl;

    iget-object v2, p0, Ldjg;->f:Landroid/content/Context;

    iget v3, p0, Ldjg;->c:I

    invoke-interface {v1, v2, v3}, Lkzl;->a(Landroid/content/Context;I)[I

    move-result-object v1

    iput-object v1, v0, Loxz;->a:[I

    .line 60
    return-void
.end method

.method protected a(Lmab;)V
    .locals 6

    .prologue
    .line 64
    iget-object v0, p1, Lmab;->a:Lnwm;

    .line 66
    if-nez v0, :cond_1

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    iget-object v0, v0, Lnwm;->a:Logr;

    .line 72
    if-eqz v0, :cond_0

    .line 77
    iget-object v1, p0, Ldjg;->a:Ljava/lang/String;

    iget-object v0, v0, Logr;->i:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    new-instance v0, Ldjs;

    iget-object v1, p0, Ldjg;->f:Landroid/content/Context;

    iget v2, p0, Ldjg;->c:I

    iget-object v3, p0, Ldjg;->a:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Ldjs;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V

    .line 86
    invoke-virtual {v0}, Ldjs;->l()V

    goto :goto_0
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lmaa;

    invoke-virtual {p0, p1}, Ldjg;->a(Lmaa;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 19
    check-cast p1, Lmab;

    invoke-virtual {p0, p1}, Ldjg;->a(Lmab;)V

    return-void
.end method
