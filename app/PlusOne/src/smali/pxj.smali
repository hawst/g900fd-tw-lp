.class public final Lpxj;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpxj;


# instance fields
.field private b:Ljava/lang/Boolean;

.field private c:Ljava/lang/Boolean;

.field private d:Ljava/lang/String;

.field private e:Lpgx;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Lppf;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 9
    const/4 v0, 0x0

    new-array v0, v0, [Lpxj;

    sput-object v0, Lpxj;->a:[Lpxj;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 19
    iput-object v0, p0, Lpxj;->e:Lpgx;

    .line 28
    iput-object v0, p0, Lpxj;->i:Lppf;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 63
    const/4 v0, 0x0

    .line 64
    iget-object v1, p0, Lpxj;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 65
    const/4 v0, 0x1

    iget-object v1, p0, Lpxj;->b:Ljava/lang/Boolean;

    .line 66
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 68
    :cond_0
    iget-object v1, p0, Lpxj;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 69
    const/4 v1, 0x2

    iget-object v2, p0, Lpxj;->c:Ljava/lang/Boolean;

    .line 70
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 72
    :cond_1
    iget-object v1, p0, Lpxj;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 73
    const/4 v1, 0x3

    iget-object v2, p0, Lpxj;->d:Ljava/lang/String;

    .line 74
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    :cond_2
    iget-object v1, p0, Lpxj;->e:Lpgx;

    if-eqz v1, :cond_3

    .line 77
    const/4 v1, 0x4

    iget-object v2, p0, Lpxj;->e:Lpgx;

    .line 78
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    :cond_3
    iget-object v1, p0, Lpxj;->f:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 81
    const/4 v1, 0x5

    iget-object v2, p0, Lpxj;->f:Ljava/lang/String;

    .line 82
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 84
    :cond_4
    iget-object v1, p0, Lpxj;->g:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 85
    const/4 v1, 0x6

    iget-object v2, p0, Lpxj;->g:Ljava/lang/String;

    .line 86
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    :cond_5
    iget-object v1, p0, Lpxj;->h:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 89
    const/4 v1, 0x7

    iget-object v2, p0, Lpxj;->h:Ljava/lang/String;

    .line 90
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    :cond_6
    iget-object v1, p0, Lpxj;->i:Lppf;

    if-eqz v1, :cond_7

    .line 93
    const/16 v1, 0x9

    iget-object v2, p0, Lpxj;->i:Lppf;

    .line 94
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    :cond_7
    iget-object v1, p0, Lpxj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 97
    iput v0, p0, Lpxj;->ai:I

    .line 98
    return v0
.end method

.method public a(Loxn;)Lpxj;
    .locals 2

    .prologue
    .line 106
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 107
    sparse-switch v0, :sswitch_data_0

    .line 111
    iget-object v1, p0, Lpxj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 112
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpxj;->ah:Ljava/util/List;

    .line 115
    :cond_1
    iget-object v1, p0, Lpxj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 117
    :sswitch_0
    return-object p0

    .line 122
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpxj;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 126
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lpxj;->c:Ljava/lang/Boolean;

    goto :goto_0

    .line 130
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpxj;->d:Ljava/lang/String;

    goto :goto_0

    .line 134
    :sswitch_4
    iget-object v0, p0, Lpxj;->e:Lpgx;

    if-nez v0, :cond_2

    .line 135
    new-instance v0, Lpgx;

    invoke-direct {v0}, Lpgx;-><init>()V

    iput-object v0, p0, Lpxj;->e:Lpgx;

    .line 137
    :cond_2
    iget-object v0, p0, Lpxj;->e:Lpgx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 141
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpxj;->f:Ljava/lang/String;

    goto :goto_0

    .line 145
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpxj;->g:Ljava/lang/String;

    goto :goto_0

    .line 149
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpxj;->h:Ljava/lang/String;

    goto :goto_0

    .line 153
    :sswitch_8
    iget-object v0, p0, Lpxj;->i:Lppf;

    if-nez v0, :cond_3

    .line 154
    new-instance v0, Lppf;

    invoke-direct {v0}, Lppf;-><init>()V

    iput-object v0, p0, Lpxj;->i:Lppf;

    .line 156
    :cond_3
    iget-object v0, p0, Lpxj;->i:Lppf;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 107
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x4a -> :sswitch_8
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lpxj;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 34
    const/4 v0, 0x1

    iget-object v1, p0, Lpxj;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 36
    :cond_0
    iget-object v0, p0, Lpxj;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 37
    const/4 v0, 0x2

    iget-object v1, p0, Lpxj;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 39
    :cond_1
    iget-object v0, p0, Lpxj;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 40
    const/4 v0, 0x3

    iget-object v1, p0, Lpxj;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 42
    :cond_2
    iget-object v0, p0, Lpxj;->e:Lpgx;

    if-eqz v0, :cond_3

    .line 43
    const/4 v0, 0x4

    iget-object v1, p0, Lpxj;->e:Lpgx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 45
    :cond_3
    iget-object v0, p0, Lpxj;->f:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 46
    const/4 v0, 0x5

    iget-object v1, p0, Lpxj;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 48
    :cond_4
    iget-object v0, p0, Lpxj;->g:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 49
    const/4 v0, 0x6

    iget-object v1, p0, Lpxj;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 51
    :cond_5
    iget-object v0, p0, Lpxj;->h:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 52
    const/4 v0, 0x7

    iget-object v1, p0, Lpxj;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 54
    :cond_6
    iget-object v0, p0, Lpxj;->i:Lppf;

    if-eqz v0, :cond_7

    .line 55
    const/16 v0, 0x9

    iget-object v1, p0, Lpxj;->i:Lppf;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 57
    :cond_7
    iget-object v0, p0, Lpxj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 59
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Lpxj;->a(Loxn;)Lpxj;

    move-result-object v0

    return-object v0
.end method
