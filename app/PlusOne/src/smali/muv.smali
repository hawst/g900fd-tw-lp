.class public final Lmuv;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Integer;

.field private b:Lmux;

.field private c:Lmui;

.field private d:Lmuw;

.field private e:Lmuh;

.field private f:Lmuy;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8304
    invoke-direct {p0}, Loxq;-><init>()V

    .line 8309
    iput-object v0, p0, Lmuv;->b:Lmux;

    .line 8312
    iput-object v0, p0, Lmuv;->c:Lmui;

    .line 8315
    iput-object v0, p0, Lmuv;->d:Lmuw;

    .line 8318
    iput-object v0, p0, Lmuv;->e:Lmuh;

    .line 8321
    iput-object v0, p0, Lmuv;->f:Lmuy;

    .line 8304
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 8350
    const/4 v0, 0x0

    .line 8351
    iget-object v1, p0, Lmuv;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 8352
    const/4 v0, 0x1

    iget-object v1, p0, Lmuv;->a:Ljava/lang/Integer;

    .line 8353
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 8355
    :cond_0
    iget-object v1, p0, Lmuv;->b:Lmux;

    if-eqz v1, :cond_1

    .line 8356
    const/4 v1, 0x2

    iget-object v2, p0, Lmuv;->b:Lmux;

    .line 8357
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8359
    :cond_1
    iget-object v1, p0, Lmuv;->c:Lmui;

    if-eqz v1, :cond_2

    .line 8360
    const/4 v1, 0x3

    iget-object v2, p0, Lmuv;->c:Lmui;

    .line 8361
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8363
    :cond_2
    iget-object v1, p0, Lmuv;->d:Lmuw;

    if-eqz v1, :cond_3

    .line 8364
    const/4 v1, 0x4

    iget-object v2, p0, Lmuv;->d:Lmuw;

    .line 8365
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8367
    :cond_3
    iget-object v1, p0, Lmuv;->e:Lmuh;

    if-eqz v1, :cond_4

    .line 8368
    const/4 v1, 0x5

    iget-object v2, p0, Lmuv;->e:Lmuh;

    .line 8369
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8371
    :cond_4
    iget-object v1, p0, Lmuv;->f:Lmuy;

    if-eqz v1, :cond_5

    .line 8372
    const/4 v1, 0x6

    iget-object v2, p0, Lmuv;->f:Lmuy;

    .line 8373
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8375
    :cond_5
    iget-object v1, p0, Lmuv;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8376
    iput v0, p0, Lmuv;->ai:I

    .line 8377
    return v0
.end method

.method public a(Loxn;)Lmuv;
    .locals 2

    .prologue
    .line 8385
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 8386
    sparse-switch v0, :sswitch_data_0

    .line 8390
    iget-object v1, p0, Lmuv;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 8391
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmuv;->ah:Ljava/util/List;

    .line 8394
    :cond_1
    iget-object v1, p0, Lmuv;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8396
    :sswitch_0
    return-object p0

    .line 8401
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmuv;->a:Ljava/lang/Integer;

    goto :goto_0

    .line 8405
    :sswitch_2
    iget-object v0, p0, Lmuv;->b:Lmux;

    if-nez v0, :cond_2

    .line 8406
    new-instance v0, Lmux;

    invoke-direct {v0}, Lmux;-><init>()V

    iput-object v0, p0, Lmuv;->b:Lmux;

    .line 8408
    :cond_2
    iget-object v0, p0, Lmuv;->b:Lmux;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8412
    :sswitch_3
    iget-object v0, p0, Lmuv;->c:Lmui;

    if-nez v0, :cond_3

    .line 8413
    new-instance v0, Lmui;

    invoke-direct {v0}, Lmui;-><init>()V

    iput-object v0, p0, Lmuv;->c:Lmui;

    .line 8415
    :cond_3
    iget-object v0, p0, Lmuv;->c:Lmui;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8419
    :sswitch_4
    iget-object v0, p0, Lmuv;->d:Lmuw;

    if-nez v0, :cond_4

    .line 8420
    new-instance v0, Lmuw;

    invoke-direct {v0}, Lmuw;-><init>()V

    iput-object v0, p0, Lmuv;->d:Lmuw;

    .line 8422
    :cond_4
    iget-object v0, p0, Lmuv;->d:Lmuw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8426
    :sswitch_5
    iget-object v0, p0, Lmuv;->e:Lmuh;

    if-nez v0, :cond_5

    .line 8427
    new-instance v0, Lmuh;

    invoke-direct {v0}, Lmuh;-><init>()V

    iput-object v0, p0, Lmuv;->e:Lmuh;

    .line 8429
    :cond_5
    iget-object v0, p0, Lmuv;->e:Lmuh;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8433
    :sswitch_6
    iget-object v0, p0, Lmuv;->f:Lmuy;

    if-nez v0, :cond_6

    .line 8434
    new-instance v0, Lmuy;

    invoke-direct {v0}, Lmuy;-><init>()V

    iput-object v0, p0, Lmuv;->f:Lmuy;

    .line 8436
    :cond_6
    iget-object v0, p0, Lmuv;->f:Lmuy;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 8386
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 8326
    iget-object v0, p0, Lmuv;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 8327
    const/4 v0, 0x1

    iget-object v1, p0, Lmuv;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 8329
    :cond_0
    iget-object v0, p0, Lmuv;->b:Lmux;

    if-eqz v0, :cond_1

    .line 8330
    const/4 v0, 0x2

    iget-object v1, p0, Lmuv;->b:Lmux;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8332
    :cond_1
    iget-object v0, p0, Lmuv;->c:Lmui;

    if-eqz v0, :cond_2

    .line 8333
    const/4 v0, 0x3

    iget-object v1, p0, Lmuv;->c:Lmui;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8335
    :cond_2
    iget-object v0, p0, Lmuv;->d:Lmuw;

    if-eqz v0, :cond_3

    .line 8336
    const/4 v0, 0x4

    iget-object v1, p0, Lmuv;->d:Lmuw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8338
    :cond_3
    iget-object v0, p0, Lmuv;->e:Lmuh;

    if-eqz v0, :cond_4

    .line 8339
    const/4 v0, 0x5

    iget-object v1, p0, Lmuv;->e:Lmuh;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8341
    :cond_4
    iget-object v0, p0, Lmuv;->f:Lmuy;

    if-eqz v0, :cond_5

    .line 8342
    const/4 v0, 0x6

    iget-object v1, p0, Lmuv;->f:Lmuy;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 8344
    :cond_5
    iget-object v0, p0, Lmuv;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 8346
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 8300
    invoke-virtual {p0, p1}, Lmuv;->a(Loxn;)Lmuv;

    move-result-object v0

    return-object v0
.end method
