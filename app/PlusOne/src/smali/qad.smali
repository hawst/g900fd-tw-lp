.class public final Lqad;
.super Loxq;
.source "PG"


# instance fields
.field public a:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1858
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1861
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lqad;->a:[Ljava/lang/String;

    .line 1858
    return-void
.end method


# virtual methods
.method public a()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1877
    .line 1878
    iget-object v1, p0, Lqad;->a:[Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lqad;->a:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 1880
    iget-object v2, p0, Lqad;->a:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 1882
    invoke-static {v4}, Loxo;->b(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 1880
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1885
    :cond_0
    iget-object v0, p0, Lqad;->a:[Ljava/lang/String;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x1

    add-int/2addr v0, v1

    .line 1887
    :cond_1
    iget-object v1, p0, Lqad;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1888
    iput v0, p0, Lqad;->ai:I

    .line 1889
    return v0
.end method

.method public a(Loxn;)Lqad;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1897
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1898
    sparse-switch v0, :sswitch_data_0

    .line 1902
    iget-object v1, p0, Lqad;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1903
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqad;->ah:Ljava/util/List;

    .line 1906
    :cond_1
    iget-object v1, p0, Lqad;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1908
    :sswitch_0
    return-object p0

    .line 1913
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 1914
    iget-object v0, p0, Lqad;->a:[Ljava/lang/String;

    array-length v0, v0

    .line 1915
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 1916
    iget-object v2, p0, Lqad;->a:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1917
    iput-object v1, p0, Lqad;->a:[Ljava/lang/String;

    .line 1918
    :goto_1
    iget-object v1, p0, Lqad;->a:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    .line 1919
    iget-object v1, p0, Lqad;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 1920
    invoke-virtual {p1}, Loxn;->a()I

    .line 1918
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1923
    :cond_2
    iget-object v1, p0, Lqad;->a:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto :goto_0

    .line 1898
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 1866
    iget-object v0, p0, Lqad;->a:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1867
    iget-object v1, p0, Lqad;->a:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 1868
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 1867
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1871
    :cond_0
    iget-object v0, p0, Lqad;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1873
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1854
    invoke-virtual {p0, p1}, Lqad;->a(Loxn;)Lqad;

    move-result-object v0

    return-object v0
.end method
