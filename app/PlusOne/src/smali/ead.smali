.class public final Lead;
.super Lt;
.source "PG"

# interfaces
.implements Landroid/app/DatePickerDialog$OnDateSetListener;
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private N:I

.field private O:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1401
    invoke-direct {p0}, Lt;-><init>()V

    .line 1397
    const/4 v0, -0x1

    iput v0, p0, Lead;->N:I

    .line 1402
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 1405
    invoke-direct {p0}, Lt;-><init>()V

    .line 1397
    const/4 v0, -0x1

    iput v0, p0, Lead;->N:I

    .line 1407
    iput p1, p0, Lead;->N:I

    .line 1408
    return-void
.end method


# virtual methods
.method public c(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    .line 1412
    iget v0, p0, Lead;->N:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 1413
    const-string v0, "type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lead;->N:I

    .line 1414
    const-string v0, "cancelled"

    iget-boolean v1, p0, Lead;->O:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lead;->O:Z

    .line 1416
    :cond_0
    invoke-virtual {p0}, Lead;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "date_time"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    .line 1418
    invoke-virtual {p0}, Lead;->k()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "time_zone"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1417
    invoke-static {v2}, Lidk;->a(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v2

    .line 1419
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v5

    .line 1420
    invoke-virtual {v5, v2}, Ljava/util/Calendar;->setTimeZone(Ljava/util/TimeZone;)V

    .line 1421
    invoke-virtual {v5, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 1422
    new-instance v0, Landroid/app/DatePickerDialog;

    .line 1423
    invoke-virtual {p0}, Lead;->n()Lz;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v5, v2}, Ljava/util/Calendar;->get(I)I

    move-result v3

    const/4 v2, 0x2

    .line 1424
    invoke-virtual {v5, v2}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v2, 0x5

    invoke-virtual {v5, v2}, Ljava/util/Calendar;->get(I)I

    move-result v5

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Landroid/app/DatePickerDialog;-><init>(Landroid/content/Context;Landroid/app/DatePickerDialog$OnDateSetListener;III)V

    .line 1425
    iget v1, p0, Lead;->N:I

    if-nez v1, :cond_1

    .line 1426
    const/4 v1, -0x2

    const v2, 0x7f0a07fc

    invoke-virtual {p0, v2}, Lead;->e_(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/DatePickerDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 1428
    :cond_1
    return-object v0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 1459
    invoke-super {p0, p1}, Lt;->e(Landroid/os/Bundle;)V

    .line 1461
    const-string v0, "type"

    iget v1, p0, Lead;->N:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1462
    const-string v0, "cancelled"

    iget-boolean v1, p0, Lead;->O:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1463
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 1448
    invoke-virtual {p0}, Lead;->u_()Lu;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/EditEventFragment;

    .line 1449
    packed-switch p2, :pswitch_data_0

    .line 1455
    :goto_0
    return-void

    .line 1451
    :pswitch_0
    invoke-virtual {v0}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->U()V

    .line 1452
    const/4 v0, 0x1

    iput-boolean v0, p0, Lead;->O:Z

    goto :goto_0

    .line 1449
    nop

    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
    .end packed-switch
.end method

.method public onDateSet(Landroid/widget/DatePicker;III)V
    .locals 2

    .prologue
    .line 1433
    iget-boolean v0, p0, Lead;->O:Z

    if-eqz v0, :cond_0

    .line 1444
    :goto_0
    return-void

    .line 1437
    :cond_0
    iget v0, p0, Lead;->N:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 1438
    invoke-virtual {p0}, Lead;->u_()Lu;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/EditEventFragment;

    .line 1439
    invoke-virtual {v0, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->a(III)V

    goto :goto_0

    .line 1441
    :cond_1
    invoke-virtual {p0}, Lead;->u_()Lu;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/fragments/EditEventFragment;

    .line 1442
    invoke-virtual {v0, p2, p3, p4}, Lcom/google/android/apps/plus/fragments/EditEventFragment;->b(III)V

    goto :goto_0
.end method
