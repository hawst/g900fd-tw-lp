.class public final Lldm;
.super Landroid/view/View;
.source "PG"

# interfaces
.implements Lljh;


# static fields
.field private static a:Llct;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Landroid/text/StaticLayout;

.field private e:Landroid/text/StaticLayout;

.field private f:Landroid/graphics/Rect;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lldm;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lldm;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 45
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    sget-object v0, Lldm;->a:Llct;

    if-nez v0, :cond_0

    .line 51
    invoke-static {p1}, Llct;->a(Landroid/content/Context;)Llct;

    move-result-object v0

    sput-object v0, Lldm;->a:Llct;

    .line 53
    :cond_0
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lldm;->f:Landroid/graphics/Rect;

    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lldm;->g:Z

    .line 55
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 166
    iput-object v0, p0, Lldm;->b:Ljava/lang/String;

    .line 167
    iput-object v0, p0, Lldm;->c:Ljava/lang/String;

    .line 168
    iput-object v0, p0, Lldm;->d:Landroid/text/StaticLayout;

    .line 169
    iput-object v0, p0, Lldm;->e:Landroid/text/StaticLayout;

    .line 170
    const/4 v0, 0x1

    iput-boolean v0, p0, Lldm;->g:Z

    .line 171
    iget-object v0, p0, Lldm;->f:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 172
    return-void
.end method

.method public a(Lkzr;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    .line 58
    invoke-virtual {p1}, Lkzr;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lldm;->b:Ljava/lang/String;

    .line 59
    invoke-virtual {p1}, Lkzr;->b()I

    move-result v0

    .line 60
    invoke-virtual {p0}, Lldm;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 62
    invoke-static {}, Llsu;->a()Ljava/lang/StringBuilder;

    move-result-object v2

    .line 63
    if-le v0, v4, :cond_0

    .line 64
    const v3, 0x7f110025

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    .line 65
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    .line 64
    invoke-virtual {v1, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " - "

    .line 65
    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 67
    :cond_0
    const v0, 0x7f0a0481

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    invoke-static {v2}, Llsu;->a(Ljava/lang/StringBuilder;)Ljava/lang/String;

    move-result-object v0

    .line 69
    iget-object v1, p0, Lldm;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 70
    iput-object v0, p0, Lldm;->b:Ljava/lang/String;

    .line 74
    :goto_0
    return-void

    .line 72
    :cond_1
    iput-object v0, p0, Lldm;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Lkzv;)V
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p1}, Lkzv;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lldm;->b:Ljava/lang/String;

    .line 78
    return-void
.end method

.method public a(Z)V
    .locals 1

    .prologue
    .line 81
    iget-boolean v0, p0, Lldm;->g:Z

    if-eq v0, p1, :cond_0

    .line 82
    iput-boolean p1, p0, Lldm;->g:Z

    .line 83
    invoke-virtual {p0}, Lldm;->invalidate()V

    .line 85
    :cond_0
    return-void
.end method

.method public b()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lldm;->f:Landroid/graphics/Rect;

    return-object v0
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 135
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 137
    iget-boolean v0, p0, Lldm;->g:Z

    if-eqz v0, :cond_0

    .line 138
    sget-object v0, Lldm;->a:Llct;

    iget-object v0, v0, Llct;->H:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lldm;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lldm;->getHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 139
    sget-object v0, Lldm;->a:Llct;

    iget-object v0, v0, Llct;->ad:Landroid/graphics/drawable/NinePatchDrawable;

    sget-object v1, Lldm;->a:Llct;

    iget-object v1, v1, Llct;->H:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/NinePatchDrawable;->setBounds(Landroid/graphics/Rect;)V

    .line 140
    sget-object v0, Lldm;->a:Llct;

    iget-object v0, v0, Llct;->ad:Landroid/graphics/drawable/NinePatchDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/NinePatchDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 143
    :cond_0
    sget-object v0, Lldm;->a:Llct;

    iget v1, v0, Llct;->m:I

    .line 144
    sget-object v0, Lldm;->a:Llct;

    iget v0, v0, Llct;->m:I

    mul-int/lit8 v0, v0, 0x2

    .line 146
    iget-object v2, p0, Lldm;->d:Landroid/text/StaticLayout;

    if-eqz v2, :cond_1

    .line 147
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 148
    iget-object v2, p0, Lldm;->d:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 149
    neg-int v2, v1

    int-to-float v2, v2

    neg-int v3, v0

    int-to-float v3, v3

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 150
    iget-object v2, p0, Lldm;->d:Landroid/text/StaticLayout;

    invoke-virtual {v2}, Landroid/text/StaticLayout;->getHeight()I

    move-result v2

    add-int/2addr v0, v2

    .line 153
    :cond_1
    iget-object v2, p0, Lldm;->e:Landroid/text/StaticLayout;

    if-eqz v2, :cond_2

    .line 154
    int-to-float v2, v1

    int-to-float v3, v0

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 155
    iget-object v2, p0, Lldm;->e:Landroid/text/StaticLayout;

    invoke-virtual {v2, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    .line 156
    neg-int v1, v1

    int-to-float v1, v1

    neg-int v0, v0

    int-to-float v0, v0

    invoke-virtual {p1, v1, v0}, Landroid/graphics/Canvas;->translate(FF)V

    .line 157
    iget-object v0, p0, Lldm;->e:Landroid/text/StaticLayout;

    invoke-virtual {v0}, Landroid/text/StaticLayout;->getHeight()I

    .line 160
    :cond_2
    sget-object v0, Lldm;->a:Llct;

    iget v0, v0, Llct;->m:I

    .line 161
    sget-object v0, Lldm;->a:Llct;

    iget v0, v0, Llct;->m:I

    .line 162
    return-void
.end method

.method public onMeasure(II)V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 89
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 90
    sget-object v2, Lldm;->a:Llct;

    iget v2, v2, Llct;->m:I

    mul-int/lit8 v2, v2, 0x2

    .line 92
    sget-object v3, Lldm;->a:Llct;

    iget v3, v3, Llct;->m:I

    mul-int/lit8 v3, v3, 0x2

    sub-int v3, v1, v3

    .line 93
    iget-object v4, p0, Lldm;->f:Landroid/graphics/Rect;

    sget-object v5, Lldm;->a:Llct;

    iget v5, v5, Llct;->m:I

    iput v5, v4, Landroid/graphics/Rect;->top:I

    .line 94
    iget-object v4, p0, Lldm;->f:Landroid/graphics/Rect;

    iput v0, v4, Landroid/graphics/Rect;->left:I

    .line 96
    invoke-virtual {p0}, Lldm;->getContext()Landroid/content/Context;

    move-result-object v4

    .line 98
    const/16 v5, 0x27

    .line 99
    invoke-static {v4, v5}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v5

    .line 100
    const/16 v6, 0x24

    .line 101
    invoke-static {v4, v6}, Llhx;->a(Landroid/content/Context;I)Landroid/text/TextPaint;

    move-result-object v4

    .line 105
    iget-object v6, p0, Lldm;->b:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 106
    iget-object v6, p0, Lldm;->b:Ljava/lang/String;

    sget-object v7, Lldm;->a:Llct;

    iget v7, v7, Llct;->Y:I

    invoke-static {v5, v6, v3, v7}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v5

    iput-object v5, p0, Lldm;->d:Landroid/text/StaticLayout;

    .line 108
    iget-object v5, p0, Lldm;->d:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getWidth()I

    move-result v5

    .line 109
    if-lez v5, :cond_0

    move v0, v1

    .line 112
    :cond_0
    iget-object v5, p0, Lldm;->d:Landroid/text/StaticLayout;

    invoke-virtual {v5}, Landroid/text/StaticLayout;->getHeight()I

    move-result v5

    add-int/2addr v2, v5

    .line 114
    :cond_1
    iget-object v5, p0, Lldm;->c:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    .line 115
    iget-object v5, p0, Lldm;->c:Ljava/lang/String;

    sget-object v6, Lldm;->a:Llct;

    iget v6, v6, Llct;->Z:I

    invoke-static {v4, v5, v3, v6}, Llib;->a(Landroid/text/TextPaint;Ljava/lang/CharSequence;II)Landroid/text/StaticLayout;

    move-result-object v4

    iput-object v4, p0, Lldm;->e:Landroid/text/StaticLayout;

    .line 117
    iget-object v4, p0, Lldm;->e:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getWidth()I

    move-result v4

    .line 118
    if-le v4, v0, :cond_2

    move v0, v1

    .line 121
    :cond_2
    iget-object v4, p0, Lldm;->e:Landroid/text/StaticLayout;

    invoke-virtual {v4}, Landroid/text/StaticLayout;->getHeight()I

    move-result v4

    add-int/2addr v2, v4

    .line 124
    :cond_3
    iget-object v4, p0, Lldm;->f:Landroid/graphics/Rect;

    iget-object v5, p0, Lldm;->f:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    add-int/2addr v0, v5

    sget-object v3, Lldm;->a:Llct;

    iget v3, v3, Llct;->m:I

    mul-int/lit8 v3, v3, 0x2

    add-int/2addr v0, v3

    iput v0, v4, Landroid/graphics/Rect;->right:I

    .line 127
    sget-object v0, Lldm;->a:Llct;

    iget v0, v0, Llct;->m:I

    add-int/2addr v0, v2

    .line 128
    iget-object v2, p0, Lldm;->f:Landroid/graphics/Rect;

    iput v0, v2, Landroid/graphics/Rect;->bottom:I

    .line 130
    invoke-virtual {p0, v1, v0}, Lldm;->setMeasuredDimension(II)V

    .line 131
    return-void
.end method
