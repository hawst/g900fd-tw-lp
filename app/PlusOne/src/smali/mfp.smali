.class public final Lmfp;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnlu;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 970
    invoke-direct {p0}, Loxq;-><init>()V

    .line 973
    iput-object v0, p0, Lmfp;->apiHeader:Llyr;

    .line 976
    iput-object v0, p0, Lmfp;->a:Lnlu;

    .line 970
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 993
    const/4 v0, 0x0

    .line 994
    iget-object v1, p0, Lmfp;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 995
    const/4 v0, 0x1

    iget-object v1, p0, Lmfp;->apiHeader:Llyr;

    .line 996
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 998
    :cond_0
    iget-object v1, p0, Lmfp;->a:Lnlu;

    if-eqz v1, :cond_1

    .line 999
    const/4 v1, 0x2

    iget-object v2, p0, Lmfp;->a:Lnlu;

    .line 1000
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1002
    :cond_1
    iget-object v1, p0, Lmfp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1003
    iput v0, p0, Lmfp;->ai:I

    .line 1004
    return v0
.end method

.method public a(Loxn;)Lmfp;
    .locals 2

    .prologue
    .line 1012
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1013
    sparse-switch v0, :sswitch_data_0

    .line 1017
    iget-object v1, p0, Lmfp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1018
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmfp;->ah:Ljava/util/List;

    .line 1021
    :cond_1
    iget-object v1, p0, Lmfp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1023
    :sswitch_0
    return-object p0

    .line 1028
    :sswitch_1
    iget-object v0, p0, Lmfp;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 1029
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmfp;->apiHeader:Llyr;

    .line 1031
    :cond_2
    iget-object v0, p0, Lmfp;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1035
    :sswitch_2
    iget-object v0, p0, Lmfp;->a:Lnlu;

    if-nez v0, :cond_3

    .line 1036
    new-instance v0, Lnlu;

    invoke-direct {v0}, Lnlu;-><init>()V

    iput-object v0, p0, Lmfp;->a:Lnlu;

    .line 1038
    :cond_3
    iget-object v0, p0, Lmfp;->a:Lnlu;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1013
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 981
    iget-object v0, p0, Lmfp;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 982
    const/4 v0, 0x1

    iget-object v1, p0, Lmfp;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 984
    :cond_0
    iget-object v0, p0, Lmfp;->a:Lnlu;

    if-eqz v0, :cond_1

    .line 985
    const/4 v0, 0x2

    iget-object v1, p0, Lmfp;->a:Lnlu;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 987
    :cond_1
    iget-object v0, p0, Lmfp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 989
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 966
    invoke-virtual {p0, p1}, Lmfp;->a(Loxn;)Lmfp;

    move-result-object v0

    return-object v0
.end method
