.class public final Loeh;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Loei;

.field private c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 695
    invoke-direct {p0}, Loxq;-><init>()V

    .line 765
    const/4 v0, 0x0

    iput-object v0, p0, Loeh;->b:Loei;

    .line 695
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 787
    const/4 v0, 0x0

    .line 788
    iget-object v1, p0, Loeh;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 789
    const/4 v0, 0x1

    iget-object v1, p0, Loeh;->a:Ljava/lang/String;

    .line 790
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 792
    :cond_0
    iget-object v1, p0, Loeh;->b:Loei;

    if-eqz v1, :cond_1

    .line 793
    const/4 v1, 0x2

    iget-object v2, p0, Loeh;->b:Loei;

    .line 794
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 796
    :cond_1
    iget-object v1, p0, Loeh;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 797
    const/4 v1, 0x3

    iget-object v2, p0, Loeh;->c:Ljava/lang/Integer;

    .line 798
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 800
    :cond_2
    iget-object v1, p0, Loeh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 801
    iput v0, p0, Loeh;->ai:I

    .line 802
    return v0
.end method

.method public a(Loxn;)Loeh;
    .locals 2

    .prologue
    .line 810
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 811
    sparse-switch v0, :sswitch_data_0

    .line 815
    iget-object v1, p0, Loeh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 816
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loeh;->ah:Ljava/util/List;

    .line 819
    :cond_1
    iget-object v1, p0, Loeh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 821
    :sswitch_0
    return-object p0

    .line 826
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loeh;->a:Ljava/lang/String;

    goto :goto_0

    .line 830
    :sswitch_2
    iget-object v0, p0, Loeh;->b:Loei;

    if-nez v0, :cond_2

    .line 831
    new-instance v0, Loei;

    invoke-direct {v0}, Loei;-><init>()V

    iput-object v0, p0, Loeh;->b:Loei;

    .line 833
    :cond_2
    iget-object v0, p0, Loeh;->b:Loei;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 837
    :sswitch_3
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loeh;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 811
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 772
    iget-object v0, p0, Loeh;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 773
    const/4 v0, 0x1

    iget-object v1, p0, Loeh;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 775
    :cond_0
    iget-object v0, p0, Loeh;->b:Loei;

    if-eqz v0, :cond_1

    .line 776
    const/4 v0, 0x2

    iget-object v1, p0, Loeh;->b:Loei;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 778
    :cond_1
    iget-object v0, p0, Loeh;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 779
    const/4 v0, 0x3

    iget-object v1, p0, Loeh;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 781
    :cond_2
    iget-object v0, p0, Loeh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 783
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 691
    invoke-virtual {p0, p1}, Loeh;->a(Loxn;)Loeh;

    move-result-object v0

    return-object v0
.end method
