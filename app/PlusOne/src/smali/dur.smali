.class public final Ldur;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;

.field private static final e:[Ljava/lang/String;

.field private static final f:[Ljava/lang/String;

.field private static g:Ldur;


# instance fields
.field private final a:Lhei;

.field private final b:Ljgn;

.field private final h:Landroid/content/Context;

.field private final i:Lizs;

.field private final j:J

.field private final k:J

.field private l:J

.field private m:J

.field private n:I

.field private o:I

.field private p:Ldve;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 137
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "all_tiles.image_url"

    aput-object v1, v0, v2

    const-string v1, "sum(representation_type)"

    aput-object v1, v0, v3

    const-string v1, "media_attr"

    aput-object v1, v0, v4

    const-string v1, "data"

    aput-object v1, v0, v5

    const-string v1, "title"

    aput-object v1, v0, v6

    sput-object v0, Ldur;->c:[Ljava/lang/String;

    .line 145
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "all_photos.image_url"

    aput-object v1, v0, v2

    const-string v1, "sum(representation_type)"

    aput-object v1, v0, v3

    const-string v1, "media_attr"

    aput-object v1, v0, v4

    const-string v1, "data"

    aput-object v1, v0, v5

    const-string v1, "NULL as title"

    aput-object v1, v0, v6

    sput-object v0, Ldur;->d:[Ljava/lang/String;

    .line 196
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "media_cache.image_url"

    aput-object v1, v0, v2

    const-string v1, "filename"

    aput-object v1, v0, v3

    const-string v1, "size"

    aput-object v1, v0, v4

    const-string v1, "representation_type"

    aput-object v1, v0, v5

    sput-object v0, Ldur;->e:[Ljava/lang/String;

    .line 211
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "size"

    aput-object v1, v0, v2

    sput-object v0, Ldur;->f:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 278
    iput-object p1, p0, Ldur;->h:Landroid/content/Context;

    .line 279
    const-class v0, Lizs;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lizs;

    iput-object v0, p0, Ldur;->i:Lizs;

    .line 280
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    iput-object v0, p0, Ldur;->a:Lhei;

    .line 281
    iget-object v0, p0, Ldur;->h:Landroid/content/Context;

    const-class v1, Ljgn;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljgn;

    iput-object v0, p0, Ldur;->b:Ljgn;

    .line 282
    const-class v0, Lkdv;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    .line 283
    invoke-interface {v0}, Lkdv;->k()J

    move-result-wide v2

    iput-wide v2, p0, Ldur;->j:J

    .line 284
    invoke-interface {v0}, Lkdv;->j()J

    move-result-wide v0

    iput-wide v0, p0, Ldur;->k:J

    .line 285
    invoke-direct {p0, v4}, Ldur;->b(I)V

    .line 286
    invoke-direct {p0, v4}, Ldur;->c(I)V

    .line 287
    return-void
.end method

.method private a(ILjava/util/Set;Ljava/util/Set;)I
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)I"
        }
    .end annotation

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x3

    const/4 v9, 0x0

    const/4 v4, 0x0

    .line 550
    .line 552
    iget-object v0, p0, Ldur;->h:Landroid/content/Context;

    .line 553
    invoke-static {v0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 555
    new-array v2, v1, [Ljava/lang/String;

    const-string v1, "image_url"

    aput-object v1, v2, v9

    const-string v1, "filename"

    aput-object v1, v2, v3

    const-string v1, "size"

    aput-object v1, v2, v5

    .line 561
    const-string v1, "media_cache"

    const-string v3, "http_status = 200"

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    move-object v8, v4

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    move v1, v9

    .line 565
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 566
    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 567
    const/4 v4, 0x1

    invoke-interface {v2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 569
    const-string v5, "content"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 570
    invoke-interface {p3, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 574
    invoke-interface {p2, v3}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 575
    const/4 v5, 0x2

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 579
    int-to-long v6, v5

    invoke-direct {p0, v0, v4, v6, v7}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)Z

    move-result v5

    if-nez v5, :cond_3

    .line 580
    const-string v0, "MediaSyncManager"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 581
    const-string v0, "Error deleting cache file: "

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 584
    :cond_1
    :goto_1
    new-instance v0, Ldva;

    invoke-direct {v0}, Ldva;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 593
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 581
    :cond_2
    :try_start_1
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 586
    :cond_3
    add-int/lit8 v1, v1, 0x1

    .line 588
    const-string v4, "MediaSyncManager"

    const/4 v5, 0x3

    invoke-static {v4, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 589
    const-string v4, "Deleting unreferenced cache file: "

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_4

    invoke-virtual {v4, v3}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_4
    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v4}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 593
    :cond_5
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 596
    return v1
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;I)J
    .locals 4

    .prologue
    .line 398
    const-string v0, "SELECT COUNT(*) FROM (SELECT image_url as all_photos_image_url, timestamp as all_photos_timestamp FROM all_photos WHERE all_photos_image_url IS NOT NULL AND is_primary = 1 AND (has_edit_list = 1 OR local_content_uri IS NULL) LIMIT ? OFFSET ?) INNER JOIN media_cache ON (all_photos_image_url = media_cache.image_url) WHERE representation_type & ? != 0"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "-1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    .line 401
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 398
    invoke-static {p1, v0, v1}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(Ldur;Landroid/content/Context;)J
    .locals 6

    .prologue
    .line 66
    const-wide/16 v0, 0x0

    invoke-direct {p0}, Ldur;->d()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-wide v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {p1, v0}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "SELECT SUM(size) FROM media_cache"

    const/4 v5, 0x0

    invoke-static {v0, v1, v5}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    add-long/2addr v0, v2

    move-wide v2, v0

    goto :goto_0

    :cond_0
    return-wide v2
.end method

.method static synthetic a(Ldur;Landroid/database/sqlite/SQLiteDatabase;)J
    .locals 2

    .prologue
    .line 66
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;I)J

    move-result-wide v0

    return-wide v0
.end method

.method private a(IJ)Ldum;
    .locals 8

    .prologue
    .line 700
    iget-object v0, p0, Ldur;->h:Landroid/content/Context;

    .line 701
    invoke-static {v0, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 703
    iget-object v0, p0, Ldur;->p:Ldve;

    iget v0, v0, Ldve;->a:I

    if-ne v0, p1, :cond_1

    const/4 v0, 0x1

    .line 704
    :goto_0
    if-nez v0, :cond_0

    .line 708
    const-string v1, "SELECT SUM(size) FROM media_cache"

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v4

    .line 709
    iget-object v1, p0, Ldur;->p:Ldve;

    iget-wide v6, v1, Ldve;->e:J

    sub-long v6, v4, v6

    invoke-static {p2, p3, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide p2

    .line 711
    const-string v1, "MediaSyncManager"

    const/4 v3, 0x4

    invoke-static {v1, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 712
    iget-object v1, p0, Ldur;->a:Lhei;

    invoke-interface {v1, p1}, Lhei;->a(I)Lhej;

    move-result-object v1

    const-string v3, "account_name"

    invoke-interface {v1, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 713
    invoke-static {v1}, Lifu;->b(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x24

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Space used by "

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ": "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 718
    :cond_0
    new-instance v1, Ldum;

    invoke-direct {v1, p1, p2, p3, v0}, Ldum;-><init>(IJZ)V

    .line 720
    invoke-virtual {v1}, Ldum;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, v1

    .line 731
    :goto_1
    return-object v0

    .line 703
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 723
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Lduu;

    const/4 v4, 0x1

    const/16 v5, 0x32

    const/4 v6, 0x0

    invoke-direct {v3, p0, v4, v5, v6}, Lduu;-><init>(Ldur;III)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lduz;

    const/4 v4, 0x1

    const/16 v5, 0x32

    const/4 v6, 0x0

    invoke-direct {v3, p0, v4, v5, v6}, Lduz;-><init>(Ldur;III)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lduu;

    const/16 v4, 0xa

    const/16 v5, 0x32

    const/4 v6, 0x0

    invoke-direct {v3, p0, v4, v5, v6}, Lduu;-><init>(Ldur;III)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lduz;

    const/16 v4, 0xa

    const/16 v5, 0x32

    const/4 v6, 0x0

    invoke-direct {v3, p0, v4, v5, v6}, Lduz;-><init>(Ldur;III)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lduu;

    const/4 v4, 0x1

    const/4 v5, -0x1

    const/16 v6, 0x32

    invoke-direct {v3, p0, v4, v5, v6}, Lduu;-><init>(Ldur;III)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lduz;

    const/4 v4, 0x1

    const/4 v5, -0x1

    const/16 v6, 0x32

    invoke-direct {v3, p0, v4, v5, v6}, Lduz;-><init>(Ldur;III)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lduu;

    const/16 v4, 0xa

    const/4 v5, -0x1

    const/4 v6, 0x0

    invoke-direct {v3, p0, v4, v5, v6}, Lduu;-><init>(Ldur;III)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-direct {p0, v2, v3, v4}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;IZ)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    const/16 v3, 0xa

    const/4 v4, 0x1

    invoke-direct {p0, v2, v3, v4}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;IZ)Ljava/util/List;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvc;

    .line 724
    invoke-virtual {v0, v2, v1}, Ldvc;->a(Landroid/database/sqlite/SQLiteDatabase;Ldum;)V

    .line 726
    invoke-virtual {v1}, Ldum;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, v1

    .line 727
    goto/16 :goto_1

    :cond_4
    move-object v0, v1

    .line 731
    goto/16 :goto_1
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Ldur;
    .locals 3

    .prologue
    .line 271
    const-class v1, Ldur;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ldur;->g:Ldur;

    if-nez v0, :cond_0

    .line 272
    new-instance v0, Ldur;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Ldur;-><init>(Landroid/content/Context;)V

    sput-object v0, Ldur;->g:Ldur;

    .line 274
    :cond_0
    sget-object v0, Ldur;->g:Ldur;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 271
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Ldur;)Ldve;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Ldur;->p:Ldve;

    return-object v0
.end method

.method static a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1492
    packed-switch p0, :pswitch_data_0

    .line 1497
    :pswitch_0
    const-string v0, "Unknown"

    :goto_0
    return-object v0

    .line 1493
    :pswitch_1
    const-string v0, "None"

    goto :goto_0

    .line 1494
    :pswitch_2
    const-string v0, "Thumbnail"

    goto :goto_0

    .line 1495
    :pswitch_3
    const-string v0, "Large"

    goto :goto_0

    .line 1496
    :pswitch_4
    const-string v0, "Video"

    goto :goto_0

    .line 1492
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ldvd;I)Ljava/lang/String;
    .locals 15

    .prologue
    .line 971
    invoke-interface/range {p3 .. p3}, Ldvd;->d()Ljava/lang/String;

    move-result-object v11

    .line 972
    invoke-interface/range {p3 .. p3}, Ldvd;->h()I

    move-result v5

    .line 973
    invoke-interface/range {p3 .. p3}, Ldvd;->f()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 974
    invoke-interface/range {p3 .. p3}, Ldvd;->c()I

    move-result v10

    .line 975
    const-wide/16 v2, 0x20

    and-long/2addr v2, v6

    const-wide/16 v8, 0x0

    cmp-long v2, v2, v8

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    if-eq v10, v2, :cond_0

    const/4 v2, 0x1

    .line 977
    :goto_0
    invoke-interface/range {p3 .. p3}, Ldvd;->e()Ljava/lang/String;

    move-result-object v4

    .line 979
    iget-object v3, p0, Ldur;->p:Ldve;

    invoke-virtual {v3}, Ldve;->d()Z

    move-result v3

    .line 981
    if-eqz v2, :cond_16

    .line 982
    iget-object v2, p0, Ldur;->p:Ldve;

    iget v2, v2, Ldve;->a:I

    invoke-virtual {p0, v2, v11}, Ldur;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 983
    if-eqz v10, :cond_2

    .line 987
    const-string v5, "http_status = 200 AND image_url = ? AND representation_type & ? != 0"

    const/4 v2, 0x2

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object v11, v6, v2

    const/4 v2, 0x1

    const/16 v3, 0x8

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v2

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "filename"

    aput-object v3, v4, v2

    const/4 v2, 0x1

    const-string v3, "size"

    aput-object v3, v4, v2

    const-string v3, "media_cache"

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    :goto_1
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x0

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x1

    invoke-interface {v3, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    move-object/from16 v0, p1

    invoke-direct {p0, v0, v2, v4, v5}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v2

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v2

    .line 975
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 987
    :cond_1
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    move-object v2, v10

    .line 1095
    :goto_2
    return-object v2

    .line 991
    :cond_2
    invoke-interface/range {p3 .. p3}, Ldvd;->i()Z

    move-result v2

    if-nez v2, :cond_3

    .line 992
    const/4 v2, 0x0

    goto :goto_2

    .line 996
    :cond_3
    const/16 v10, 0x8

    .line 998
    invoke-interface/range {p3 .. p3}, Ldvd;->b()Ljava/lang/String;

    move-result-object v2

    .line 1002
    iget-object v8, p0, Ldur;->h:Landroid/content/Context;

    .line 1003
    invoke-interface/range {p3 .. p3}, Ldvd;->g()Lnym;

    move-result-object v9

    .line 1002
    invoke-static {v8, v4, v9}, Lcom/google/android/apps/plus/content/MovieMakerUtils;->a(Landroid/content/Context;Ljava/lang/String;Lnym;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 1004
    const/4 v3, 0x0

    move v4, v3

    move-object v3, v2

    .line 1008
    :goto_3
    and-int v2, v5, v10

    if-eqz v2, :cond_6

    const-string v2, "MediaSyncManager"

    const/4 v5, 0x2

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {v10}, Ldur;->a(I)Ljava/lang/String;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, 0x15

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->length()I

    move-result v9

    add-int/2addr v8, v9

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v8, "Already cached "

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " for: "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    iget-object v2, p0, Ldur;->p:Ldve;

    iget-object v2, v2, Ldve;->f:Ldul;

    if-eqz v2, :cond_5

    iget-object v2, p0, Ldur;->p:Ldve;

    iget-object v2, v2, Ldve;->f:Ldul;

    invoke-virtual {v2, v11, v10}, Ldul;->a(Ljava/lang/String;I)Ldun;

    move-result-object v2

    if-eqz v2, :cond_5

    iget-object v5, p0, Ldur;->p:Ldve;

    iget-wide v8, v5, Ldve;->i:J

    iget-wide v12, v2, Ldun;->e:J

    sub-long/2addr v8, v12

    iput-wide v8, v5, Ldve;->i:J

    :cond_5
    const/4 v2, 0x1

    :goto_4
    if-eqz v2, :cond_7

    .line 1009
    const/4 v2, 0x0

    goto :goto_2

    .line 1008
    :cond_6
    const/4 v2, 0x0

    goto :goto_4

    .line 1012
    :cond_7
    if-nez v3, :cond_8

    .line 1013
    const/4 v2, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {p0, v0, v1, v11, v2}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;I)V

    .line 1014
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 1017
    :cond_8
    iget-object v2, p0, Ldur;->h:Landroid/content/Context;

    invoke-static {v6, v7}, Ljvj;->a(J)Ljac;

    move-result-object v5

    invoke-static {v2, v3, v5}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v3

    .line 1019
    const/16 v7, 0x4402

    .line 1021
    if-eqz v4, :cond_9

    .line 1022
    const/16 v7, 0x6402

    .line 1026
    :cond_9
    sparse-switch v10, :sswitch_data_0

    .line 1042
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v4, 0x28

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "Invalid representation type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1028
    :sswitch_0
    const/4 v4, 0x2

    .line 1029
    const/high16 v2, 0x10000

    or-int/2addr v7, v2

    .line 1046
    :goto_5
    const-string v2, "MediaSyncManager"

    const/4 v5, 0x3

    invoke-static {v2, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 1047
    invoke-static {v10}, Ldur;->a(I)Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Ldur;->p:Ldve;

    iget-wide v8, v5, Ldve;->h:J

    iget-object v5, p0, Ldur;->p:Ldve;

    iget-wide v12, v5, Ldve;->i:J

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, 0x54

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/String;->length()I

    move-result v14

    add-int/2addr v6, v14

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v6, "Downloading "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, ": "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " total downloaded="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, " total left="

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1052
    :cond_a
    :try_start_1
    iget-object v2, p0, Ldur;->i:Lizs;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v2 .. v7}, Lizs;->a(Lizu;IIII)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/File;

    .line 1058
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v8

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v7

    move-object v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object v6, v11

    invoke-direct/range {v3 .. v10}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;JI)V

    .line 1060
    :goto_6
    iget-object v3, p0, Ldur;->p:Ldve;

    iget-wide v4, v3, Ldve;->h:J

    add-long/2addr v4, v8

    iput-wide v4, v3, Ldve;->h:J

    .line 1061
    iget-object v3, p0, Ldur;->p:Ldve;

    iget-wide v4, v3, Ldve;->i:J

    sub-long/2addr v4, v8

    iput-wide v4, v3, Ldve;->i:J

    .line 1062
    iget-object v3, p0, Ldur;->p:Ldve;

    move/from16 v0, p4

    invoke-virtual {v3, v10, v0}, Ldve;->b(II)V

    .line 1064
    iget-object v3, p0, Ldur;->p:Ldve;

    iget-wide v4, v3, Ldve;->i:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_b

    .line 1065
    invoke-direct {p0}, Ldur;->h()V

    .line 1068
    :cond_b
    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_2

    .line 1033
    :sswitch_1
    const/4 v4, 0x3

    .line 1034
    goto/16 :goto_5

    .line 1037
    :sswitch_2
    const/4 v4, 0x4

    .line 1038
    or-int/lit16 v7, v7, 0x800

    .line 1039
    goto/16 :goto_5

    .line 1058
    :cond_c
    const-string v3, "MediaSyncManager"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_d

    const-string v3, "Cache file not found after downloading: "

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_e

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    :cond_d
    :goto_7
    const-wide/16 v8, 0x0

    goto :goto_6

    :cond_e
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Lkdn; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lkde; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_7

    .line 1069
    :catch_0
    move-exception v2

    .line 1071
    const-string v3, "MediaSyncManager"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_f

    .line 1072
    const-string v3, "Could not download media: "

    invoke-static {v11}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-eqz v5, :cond_11

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 1074
    :cond_f
    :goto_8
    invoke-virtual {v2}, Lkdn;->b()I

    move-result v3

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {p0, v0, v1, v11, v3}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;I)V

    .line 1078
    invoke-virtual {v2}, Lkdn;->a()I

    move-result v3

    const/4 v4, 0x4

    if-eq v3, v4, :cond_10

    .line 1079
    invoke-virtual {v2}, Lkdn;->a()I

    move-result v3

    const/4 v4, 0x5

    if-ne v3, v4, :cond_14

    .line 1080
    :cond_10
    invoke-virtual {v2}, Lkdn;->b()I

    move-result v2

    .line 1082
    if-nez v2, :cond_12

    .line 1084
    iget-object v2, p0, Ldur;->p:Ldve;

    iget-object v2, v2, Ldve;->c:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    .line 1085
    new-instance v2, Ldux;

    const-string v3, "Unable to connect"

    invoke-direct {v2, v3}, Ldux;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1072
    :cond_11
    new-instance v4, Ljava/lang/String;

    invoke-direct {v4, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_8

    .line 1086
    :cond_12
    const/16 v3, 0x1f4

    if-eq v2, v3, :cond_13

    const/16 v3, 0x1f6

    if-eq v2, v3, :cond_13

    const/16 v3, 0x1f7

    if-eq v2, v3, :cond_13

    const/16 v3, 0x1f8

    if-ne v2, v3, :cond_14

    .line 1090
    :cond_13
    iget-object v2, p0, Ldur;->p:Ldve;

    iget-object v2, v2, Ldve;->c:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    .line 1091
    new-instance v2, Ldux;

    const-string v3, "Throttled by server"

    invoke-direct {v2, v3}, Ldux;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1095
    :cond_14
    const/4 v2, 0x0

    goto/16 :goto_2

    .line 1097
    :catch_1
    move-exception v2

    iget-object v2, p0, Ldur;->p:Ldve;

    iget-object v2, v2, Ldve;->c:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    .line 1098
    new-instance v2, Ldux;

    const-string v3, "Canceled"

    invoke-direct {v2, v3}, Ldux;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_15
    move v4, v3

    move-object v3, v2

    goto/16 :goto_3

    :cond_16
    move v4, v3

    move-object v3, v11

    goto/16 :goto_3

    .line 1026
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch
.end method

.method static synthetic a(Ldur;Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;Landroid/content/ContentValues;II)Ljava/lang/String;
    .locals 2

    .prologue
    .line 66
    new-instance v0, Ldus;

    invoke-direct {v0, p0, p4, p2}, Ldus;-><init>(Ldur;ILandroid/database/Cursor;)V

    iget v1, p0, Ldur;->o:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Ldur;->o:I

    invoke-direct {p0, v1}, Ldur;->c(I)V

    invoke-direct {p0, p1, p3, v0, p5}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ldvd;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;IZ)Ljava/util/List;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "IZ)",
            "Ljava/util/List",
            "<",
            "Ldvc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 784
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;Z)Ljava/util/List;

    move-result-object v0

    .line 785
    new-instance v7, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v7, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 786
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    .line 787
    new-instance v0, Lduw;

    const/4 v3, -0x1

    const/4 v4, 0x0

    move-object v1, p0

    move v2, p2

    move v6, p3

    invoke-direct/range {v0 .. v6}, Lduw;-><init>(Ldur;IIILjava/lang/String;Z)V

    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 790
    :cond_0
    return-object v7
.end method

.method static synthetic a([B)Lnym;
    .locals 1

    .prologue
    .line 66
    invoke-static {p0}, Ldur;->b([B)Lnym;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1238
    new-array v3, v9, [Ljava/lang/String;

    const-string v0, "image_url"

    aput-object v0, v3, v6

    const-string v0, "media_attr"

    aput-object v0, v3, v7

    const-string v0, "data"

    aput-object v0, v3, v8

    const-string v2, "all_photos"

    const-string v4, "all_photos.image_url IS NOT NULL AND is_primary = 1 AND (has_edit_list = 1 OR local_content_uri IS NULL)"

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 1239
    iget-object v0, p0, Ldur;->a:Lhei;

    iget-object v1, p0, Ldur;->p:Ldve;

    iget v1, v1, Ldve;->a:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-array v3, v9, [Ljava/lang/String;

    const-string v1, "image_url"

    aput-object v1, v3, v6

    const-string v1, "media_attr"

    aput-object v1, v3, v7

    const-string v1, "data"

    aput-object v1, v3, v8

    new-array v1, v7, [Ljava/lang/String;

    aput-object v0, v1, v6

    invoke-static {v6, v1}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-array v5, v10, [Ljava/lang/String;

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v5, v6

    aput-object v0, v5, v7

    aput-object v0, v5, v8

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v9

    const-string v2, "all_tiles"

    const-string v4, "type = ? AND (view_id = ? OR view_id IN (SELECT cluster_id FROM all_tiles WHERE view_id = ? AND type = ? AND media_attr & 512 == 0))"

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 1240
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;I)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 1438
    const-string v0, "MediaSyncManager"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1439
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x21

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v1, "HTTP error: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " imageUrl="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1442
    :cond_0
    invoke-virtual {p2}, Landroid/content/ContentValues;->clear()V

    .line 1443
    const-string v0, "image_url"

    invoke-virtual {p2, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1444
    const-string v0, "size"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1445
    const-string v0, "http_status"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1446
    const-string v0, "representation_type"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1449
    const-string v0, "filename"

    invoke-virtual {p2, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1451
    const-string v0, "media_cache"

    const-string v1, "image_url"

    invoke-virtual {p1, v0, v1, p2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertWithOnConflict(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;I)J

    .line 1453
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 9

    .prologue
    .line 1361
    iget-object v0, p0, Ldur;->h:Landroid/content/Context;

    const-class v1, Lkdv;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    invoke-interface {v0}, Lkdv;->e()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v0

    .line 1362
    invoke-virtual {v0, p4}, Lcom/google/android/libraries/social/filecache/FileCache;->getCachedFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 1363
    if-eqz v0, :cond_0

    .line 1364
    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v8, p5

    .line 1366
    invoke-direct/range {v1 .. v8}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;JI)V

    .line 1368
    :cond_0
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;JI)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 1373
    const-string v0, "MediaSyncManager"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1377
    invoke-static/range {p7 .. p7}, Ldur;->a(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x44

    invoke-static {p4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "insertCacheFile imageUrl="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " cacheFile="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " size="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " type="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1374
    :cond_0
    invoke-virtual {p2}, Landroid/content/ContentValues;->clear()V

    .line 1381
    const-string v0, "image_url"

    invoke-virtual {p2, v0, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1382
    const-string v0, "filename"

    invoke-virtual {p2, v0, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1383
    const-string v0, "size"

    invoke-static {p5, p6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1384
    const-string v0, "http_status"

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1385
    const-string v0, "representation_type"

    invoke-static/range {p7 .. p7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1387
    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    aput-object p4, v4, v6

    .line 1388
    const-string v1, "media_cache"

    sget-object v2, Ldur;->f:[Ljava/lang/String;

    const-string v3, "filename = ?"

    move-object v0, p1

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 1391
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1392
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 1393
    const-string v0, "media_cache"

    const-string v5, "filename = ?"

    invoke-virtual {p1, v0, p2, v5, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1394
    iget-object v0, p0, Ldur;->p:Ldve;

    iget-wide v4, v0, Ldve;->g:J

    sub-long v2, p5, v2

    add-long/2addr v2, v4

    iput-wide v2, v0, Ldve;->g:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1400
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 1401
    return-void

    .line 1396
    :cond_1
    :try_start_1
    const-string v0, "media_cache"

    const-string v2, "filename"

    invoke-virtual {p1, v0, v2, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1397
    iget-object v0, p0, Ldur;->p:Ldve;

    iget-wide v2, v0, Ldve;->g:J

    add-long/2addr v2, p5

    iput-wide v2, v0, Ldve;->g:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1400
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v5, 0x0

    .line 1282
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v6, v5

    move-object v7, v5

    .line 1284
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1291
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1292
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 1293
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 1295
    invoke-static {v0, v1}, Ljvj;->a(J)Ljac;

    move-result-object v7

    .line 1296
    iget-object v0, p0, Ldur;->h:Landroid/content/Context;

    invoke-static {v0, v3, v7}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v9

    .line 1301
    iget-object v0, p0, Ldur;->i:Lizs;

    const/4 v1, 0x2

    const/16 v2, 0x4402

    invoke-virtual {v0, v9, v1, v2}, Lizs;->a(Lizu;II)Ljava/lang/String;

    move-result-object v4

    .line 1303
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, v8

    invoke-direct/range {v0 .. v5}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1305
    sget-object v0, Ljac;->b:Ljac;

    if-ne v7, v0, :cond_1

    .line 1306
    const/4 v0, 0x2

    .line 1307
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Ldur;->b([B)Lnym;

    move-result-object v0

    .line 1306
    invoke-static {v0}, Llmz;->a(Lnym;)Ljava/lang/String;

    move-result-object v0

    .line 1308
    if-eqz v0, :cond_0

    .line 1309
    iget-object v1, p0, Ldur;->p:Ldve;

    iget v1, v1, Ldve;->a:I

    invoke-virtual {p0, v1, v3}, Ldur;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1314
    iget-object v1, p0, Ldur;->h:Landroid/content/Context;

    invoke-static {v1, v0, v7}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v7

    .line 1318
    iget-object v0, p0, Ldur;->i:Lizs;

    const/4 v1, 0x4

    const/16 v2, 0x4402

    invoke-virtual {v0, v7, v1, v2}, Lizs;->a(Lizu;II)Ljava/lang/String;

    move-result-object v4

    .line 1320
    const/16 v5, 0x8

    move-object v0, p0

    move-object v1, p1

    move-object v2, v8

    invoke-direct/range {v0 .. v5}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V

    .line 1323
    iget-object v0, p0, Ldur;->i:Lizs;

    const/4 v1, 0x4

    const/16 v2, 0x4402

    invoke-virtual {v0, v7, v1, v2}, Lizs;->b(Lizu;II)Ljava/lang/String;

    move-result-object v4

    .line 1325
    const/16 v5, 0x8

    move-object v0, p0

    move-object v1, p1

    move-object v2, v8

    invoke-direct/range {v0 .. v5}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1333
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1327
    :cond_1
    :try_start_1
    iget-object v0, p0, Ldur;->i:Lizs;

    const/4 v1, 0x3

    const/16 v2, 0x4402

    invoke-virtual {v0, v9, v1, v2}, Lizs;->a(Lizu;II)Ljava/lang/String;

    move-result-object v4

    .line 1329
    const/4 v5, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v2, v8

    invoke-direct/range {v0 .. v5}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 1333
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1334
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "[",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x0

    .line 638
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    aput-object p4, v2, v1

    move-object v0, p1

    move-object v1, p3

    move-object v3, p5

    move-object v4, p6

    move-object v6, v5

    move-object v7, v5

    .line 641
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 644
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 645
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 648
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 649
    return-void
.end method

.method private a(Ljava/util/Set;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 522
    .line 525
    iget-object v0, p0, Ldur;->h:Landroid/content/Context;

    const-class v2, Lkdv;

    invoke-static {v0, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    invoke-interface {v0}, Lkdv;->e()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v3

    .line 526
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 527
    new-instance v5, Ljava/io/File;

    invoke-virtual {v3, v0}, Lcom/google/android/libraries/social/filecache/FileCache;->getCacheFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 528
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 529
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 531
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 533
    goto :goto_0

    .line 535
    :cond_1
    const-string v0, "MediaSyncManager"

    const/4 v3, 0x4

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 536
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v3, 0x3a

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "[Delete Unknown] deleted: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", failed: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 538
    :cond_2
    return-void
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1478
    const-string v0, "content"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 1488
    :goto_0
    return v0

    .line 1481
    :cond_0
    iget-object v0, p0, Ldur;->h:Landroid/content/Context;

    const-class v3, Lkdv;

    invoke-static {v0, v3}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    invoke-interface {v0}, Lkdv;->e()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v0

    .line 1482
    new-instance v3, Ljava/io/File;

    invoke-virtual {v0, p2}, Lcom/google/android/libraries/social/filecache/FileCache;->getCacheFilePath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1483
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 1484
    goto :goto_0

    .line 1486
    :cond_1
    iget-object v0, p0, Ldur;->p:Ldve;

    iget-wide v4, v0, Ldve;->g:J

    sub-long/2addr v4, p3

    iput-wide v4, v0, Ldve;->g:J

    .line 1487
    const-string v0, "media_cache"

    const-string v3, "filename = ?"

    new-array v4, v2, [Ljava/lang/String;

    aput-object p2, v4, v1

    invoke-virtual {p1, v0, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move v0, v2

    .line 1488
    goto :goto_0
.end method

.method static synthetic a()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Ldur;->e:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Ldur;Landroid/database/sqlite/SQLiteDatabase;)J
    .locals 2

    .prologue
    .line 66
    const/16 v0, 0xa

    invoke-direct {p0, p1, v0}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;I)J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic b(Ldur;)Lhei;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Ldur;->a:Lhei;

    return-object v0
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;I)Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "I)",
            "Ljava/util/List",
            "<",
            "Ldvb;",
            ">;"
        }
    .end annotation

    .prologue
    .line 857
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;Z)Ljava/util/List;

    move-result-object v0

    .line 859
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 860
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 861
    new-instance v3, Lduv;

    invoke-direct {v3, p0, p2, v0}, Lduv;-><init>(Ldur;ILjava/lang/String;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 863
    :cond_0
    return-object v1
.end method

.method private static b([B)Lnym;
    .locals 1

    .prologue
    .line 1125
    :try_start_0
    new-instance v0, Lnym;

    invoke-direct {v0}, Lnym;-><init>()V

    invoke-static {v0, p0}, Loxu;->a(Loxu;[B)Loxu;

    move-result-object v0

    check-cast v0, Lnym;
    :try_end_0
    .catch Loxt; {:try_start_0 .. :try_end_0} :catch_0

    .line 1127
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 372
    iget-object v0, p0, Ldur;->h:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 373
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "one_off_download_count"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 374
    return-void
.end method

.method static synthetic b()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Ldur;->d:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Ldur;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Ldur;->h:Landroid/content/Context;

    return-object v0
.end method

.method private c(I)V
    .locals 2

    .prologue
    .line 381
    iget-object v0, p0, Ldur;->h:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 382
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "sync_download_count"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 383
    return-void
.end method

.method static synthetic c()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Ldur;->c:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Ldur;)J
    .locals 4

    .prologue
    .line 66
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Ldur;->m:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0xdbba0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-object v0, p0, Ldur;->h:Landroid/content/Context;

    const-class v1, Lkdv;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    invoke-interface {v0}, Lkdv;->e()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/filecache/FileCache;->computeCapacity()J

    move-result-wide v0

    iput-wide v0, p0, Ldur;->l:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Ldur;->m:J

    :cond_0
    iget-wide v0, p0, Ldur;->l:J

    return-wide v0
.end method

.method private d()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 290
    iget-object v0, p0, Ldur;->a:Lhei;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "logged_in"

    aput-object v3, v1, v2

    invoke-interface {v0, v1}, Lhei;->a([Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Ldur;)Ljava/util/List;
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Ldur;->d()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method private e()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    const/4 v13, 0x2

    const/4 v8, 0x1

    const/4 v12, 0x4

    const/4 v7, 0x0

    .line 415
    iget-object v0, p0, Ldur;->p:Ldve;

    invoke-virtual {v0}, Ldve;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 450
    :cond_0
    :goto_0
    return-void

    .line 419
    :cond_1
    iget-object v0, p0, Ldur;->p:Ldve;

    iget v0, v0, Ldve;->a:I

    iget-object v1, p0, Ldur;->h:Landroid/content/Context;

    invoke-static {v1, v0}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "SELECT count(*) FROM media_cache"

    invoke-static {v0, v1, v14}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    invoke-direct {p0, v0}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 421
    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-direct {p0}, Ldur;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Ldur;->h:Landroid/content/Context;

    invoke-static {v1, v0}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    iget-object v0, p0, Ldur;->a:Lhei;

    iget-object v3, p0, Ldur;->p:Ldve;

    iget v3, v3, Ldve;->a:I

    invoke-interface {v0, v3}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v3, "gaia_id"

    invoke-interface {v0, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-array v3, v8, [Ljava/lang/String;

    aput-object v0, v3, v7

    invoke-static {v7, v3}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-array v6, v12, [Ljava/lang/String;

    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v7

    aput-object v0, v6, v8

    aput-object v0, v6, v13

    const/4 v0, 0x3

    invoke-static {v13}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v0

    const-string v3, "all_tiles"

    const-string v4, "image_url"

    const-string v5, "type = ? AND (view_id = ? OR view_id IN (SELECT cluster_id FROM all_tiles WHERE view_id = ? AND type = ? AND media_attr & 512 == 0))"

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    const-string v3, "all_photos"

    const-string v4, "image_url"

    const-string v5, "all_photos.image_url IS NOT NULL AND is_primary = 1 AND (has_edit_list = 1 OR local_content_uri IS NULL)"

    move-object v0, p0

    move-object v6, v14

    invoke-direct/range {v0 .. v6}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Ldur;->h:Landroid/content/Context;

    const-class v1, Lkdv;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    invoke-interface {v0}, Lkdv;->e()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/filecache/FileCache;->getCacheFiles()Ljava/util/List;

    move-result-object v0

    new-instance v3, Ljava/util/HashSet;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/HashSet;-><init>(I)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_4
    invoke-direct {p0}, Ldur;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v7

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0, v2, v3}, Ldur;->a(ILjava/util/Set;Ljava/util/Set;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    goto :goto_3

    :cond_5
    invoke-direct {p0, v3}, Ldur;->a(Ljava/util/Set;)V

    const-string v0, "MediaSyncManager"

    invoke-static {v0, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-static {v10, v11}, Llse;->a(J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x36

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "[Delete Unreferenced] deleted: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", duration: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 423
    :cond_6
    iget-object v0, p0, Ldur;->p:Ldve;

    invoke-virtual {v0}, Ldve;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 427
    invoke-direct {p0}, Ldur;->i()V

    .line 429
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v2

    iget-object v0, p0, Ldur;->h:Landroid/content/Context;

    const-class v1, Lkdv;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkdv;

    invoke-interface {v0}, Lkdv;->e()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/filecache/FileCache;->getUsedSpace()J

    move-result-wide v4

    iget-object v0, p0, Ldur;->h:Landroid/content/Context;

    const-class v1, Lieh;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    sget-object v1, Lcum;->b:Lief;

    iget-object v6, p0, Ldur;->p:Ldve;

    iget v6, v6, Ldve;->a:I

    invoke-interface {v0, v1, v6}, Lieh;->c(Lief;I)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    add-long/2addr v2, v4

    cmp-long v0, v2, v0

    if-gez v0, :cond_7

    move v7, v8

    :cond_7
    if-nez v7, :cond_0

    .line 436
    iget-object v0, p0, Ldur;->p:Ldve;

    invoke-direct {p0}, Ldur;->f()Ldul;

    move-result-object v1

    iput-object v1, v0, Ldve;->f:Ldul;

    .line 438
    iget-object v0, p0, Ldur;->p:Ldve;

    invoke-virtual {v0}, Ldve;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 443
    :try_start_0
    invoke-direct {p0}, Ldur;->g()V
    :try_end_0
    .catch Ldux; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 445
    :catch_0
    move-exception v0

    .line 446
    const-string v1, "MediaSyncManager"

    invoke-static {v1, v12}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 447
    const-string v1, "Ending Media Sync: "

    invoke-virtual {v0}, Ldux;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    goto/16 :goto_0

    :cond_8
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private f()Ldul;
    .locals 12

    .prologue
    .line 656
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 657
    iget-object v0, p0, Ldur;->p:Ldve;

    iget-wide v0, v0, Ldve;->d:J

    iget-object v4, p0, Ldur;->p:Ldve;

    iget-wide v4, v4, Ldve;->g:J

    sub-long/2addr v0, v4

    .line 659
    iget-object v4, p0, Ldur;->p:Ldve;

    iget-wide v4, v4, Ldve;->i:J

    iget-object v6, p0, Ldur;->p:Ldve;

    .line 660
    invoke-virtual {v6}, Ldve;->e()J

    move-result-wide v6

    add-long/2addr v4, v6

    sub-long/2addr v4, v0

    .line 662
    const-string v6, "MediaSyncManager"

    const/4 v7, 0x4

    invoke-static {v6, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 663
    iget-object v6, p0, Ldur;->p:Ldve;

    iget-wide v6, v6, Ldve;->g:J

    const-wide/16 v8, 0x0

    .line 665
    invoke-static {v8, v9, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    new-instance v10, Ljava/lang/StringBuilder;

    const/16 v11, 0x5e

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v11, "usedSpace: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", available: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", needed: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 663
    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, v4, v0

    if-gtz v0, :cond_1

    .line 670
    const/4 v0, 0x0

    .line 685
    :goto_0
    return-object v0

    .line 673
    :cond_1
    new-instance v1, Ldul;

    invoke-direct {v1}, Ldul;-><init>()V

    .line 674
    invoke-direct {p0}, Ldur;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 676
    invoke-direct {p0, v0, v4, v5}, Ldur;->a(IJ)Ldum;

    move-result-object v0

    .line 677
    invoke-virtual {v1, v0}, Ldul;->a(Ldum;)V

    goto :goto_1

    .line 680
    :cond_2
    invoke-virtual {v1, v4, v5}, Ldul;->a(J)V

    .line 682
    invoke-virtual {v1}, Ldul;->a()V

    .line 684
    const-string v0, "MediaSyncManager"

    const/4 v4, 0x4

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 685
    invoke-virtual {v1}, Ldul;->toString()Ljava/lang/String;

    move-result-object v0

    .line 686
    invoke-static {v2, v3}, Llse;->a(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x29

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v4, v5

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v4, "[Create cache eviction plan] "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", duration: "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    move-object v0, v1

    .line 685
    goto :goto_0
.end method

.method static synthetic f(Ldur;)Ljgn;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Ldur;->b:Ljgn;

    return-object v0
.end method

.method static synthetic g(Ldur;)J
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Ldur;->k:J

    return-wide v0
.end method

.method private g()V
    .locals 8

    .prologue
    const/16 v7, 0x32

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 870
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 872
    iget-object v0, p0, Ldur;->p:Ldve;

    iget v0, v0, Ldve;->a:I

    .line 873
    iget-object v1, p0, Ldur;->h:Landroid/content/Context;

    .line 874
    invoke-static {v1, v0}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 876
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Ldut;

    invoke-direct {v4, p0, v5, v7}, Ldut;-><init>(Ldur;II)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Lduy;

    invoke-direct {v4, p0, v5, v7}, Lduy;-><init>(Ldur;II)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Ldut;

    invoke-direct {v4, p0, v6, v7}, Ldut;-><init>(Ldur;II)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Lduy;

    invoke-direct {v4, p0, v6, v7}, Lduy;-><init>(Ldur;II)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Ldut;

    invoke-direct {v4, p0, v5}, Ldut;-><init>(Ldur;I)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Lduy;

    invoke-direct {v4, p0, v5}, Lduy;-><init>(Ldur;I)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Ldut;

    invoke-direct {v4, p0, v6}, Ldut;-><init>(Ldur;I)V

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0, v1, v5}, Ldur;->b(Landroid/database/sqlite/SQLiteDatabase;I)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-direct {p0, v1, v6}, Ldur;->b(Landroid/database/sqlite/SQLiteDatabase;I)Ljava/util/List;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvb;

    .line 877
    invoke-virtual {v0, v1}, Ldvb;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 878
    iget-object v0, p0, Ldur;->p:Ldve;

    invoke-virtual {v0}, Ldve;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 879
    :cond_1
    const-string v0, "MediaSyncManager"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 884
    const-string v0, "[downloadMedia] duration: "

    invoke-static {v2, v3}, Llse;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 886
    :cond_2
    :goto_0
    return-void

    .line 884
    :cond_3
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic h(Ldur;)J
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Ldur;->j:J

    return-wide v0
.end method

.method private h()V
    .locals 8

    .prologue
    .line 1139
    iget-object v0, p0, Ldur;->p:Ldve;

    iget-wide v0, v0, Ldve;->d:J

    iget-object v2, p0, Ldur;->p:Ldve;

    iget-wide v2, v2, Ldve;->g:J

    sub-long/2addr v0, v2

    .line 1143
    iget-object v2, p0, Ldur;->p:Ldve;

    invoke-virtual {v2}, Ldve;->e()J

    move-result-wide v2

    sub-long v0, v2, v0

    .line 1145
    :cond_0
    :goto_0
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_3

    .line 1146
    iget-object v2, p0, Ldur;->p:Ldve;

    iget-object v2, v2, Ldve;->f:Ldul;

    if-nez v2, :cond_1

    .line 1147
    new-instance v0, Ldva;

    invoke-direct {v0}, Ldva;-><init>()V

    throw v0

    .line 1150
    :cond_1
    iget-object v2, p0, Ldur;->p:Ldve;

    iget-object v2, v2, Ldve;->f:Ldul;

    invoke-virtual {v2}, Ldul;->b()Ldun;

    move-result-object v2

    .line 1151
    if-nez v2, :cond_2

    .line 1152
    new-instance v0, Ldux;

    const-string v1, "Out of storage"

    invoke-direct {v0, v1}, Ldux;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1155
    :cond_2
    iget v3, v2, Ldun;->a:I

    .line 1156
    iget-object v4, p0, Ldur;->h:Landroid/content/Context;

    .line 1157
    invoke-static {v4, v3}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v3

    invoke-virtual {v3}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 1159
    iget-object v4, v2, Ldun;->d:Ljava/lang/String;

    iget-wide v6, v2, Ldun;->e:J

    invoke-direct {p0, v3, v4, v6, v7}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;J)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1160
    iget-wide v4, v2, Ldun;->e:J

    sub-long/2addr v0, v4

    .line 1161
    iget-object v3, p0, Ldur;->p:Ldve;

    iget v4, v3, Ldve;->m:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, Ldve;->m:I

    .line 1162
    iget-object v3, p0, Ldur;->p:Ldve;

    iget-wide v4, v3, Ldve;->n:J

    iget-wide v6, v2, Ldun;->e:J

    add-long/2addr v4, v6

    iput-wide v4, v3, Ldve;->n:J

    goto :goto_0

    .line 1165
    :cond_3
    return-void
.end method

.method private i()V
    .locals 13

    .prologue
    const/4 v12, 0x1

    const/4 v11, 0x0

    const/4 v4, 0x0

    .line 1176
    invoke-direct {p0}, Ldur;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1177
    iget-object v1, p0, Ldur;->h:Landroid/content/Context;

    .line 1178
    invoke-static {v1, v0}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1179
    iget-object v1, p0, Ldur;->h:Landroid/content/Context;

    const-class v2, Lkdv;

    invoke-static {v1, v2}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lkdv;

    invoke-interface {v1}, Lkdv;->e()Lcom/google/android/libraries/social/filecache/FileCache;

    move-result-object v9

    new-array v2, v12, [Ljava/lang/String;

    const-string v1, "filename"

    aput-object v1, v2, v11

    iget-object v1, p0, Ldur;->h:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    const-string v1, "media_cache"

    const-string v3, "http_status = 200"

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    :cond_0
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v3, "content"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v10, v3}, Llsb;->a(Landroid/content/ContentResolver;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_0

    const-string v3, "media_cache"

    const-string v5, "filename = ?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v2, v6, v7

    invoke-virtual {v0, v3, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    :try_start_1
    invoke-virtual {v9, v2}, Lcom/google/android/libraries/social/filecache/FileCache;->getCachedFile(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v0, Ldva;

    invoke-direct {v0}, Ldva;-><init>()V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1180
    :cond_3
    return-void
.end method

.method private j()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1217
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1219
    iget-object v0, p0, Ldur;->p:Ldve;

    const-wide/16 v4, 0x0

    iput-wide v4, v0, Ldve;->g:J

    .line 1221
    invoke-direct {p0}, Ldur;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1222
    iget-object v4, p0, Ldur;->h:Landroid/content/Context;

    .line 1223
    invoke-static {v4, v0}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v0

    invoke-virtual {v0}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 1224
    const-string v4, "media_cache"

    invoke-virtual {v0, v4, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1226
    invoke-direct {p0, v0}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    goto :goto_0

    .line 1229
    :cond_0
    const-string v0, "MediaSyncManager"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1230
    const-string v0, "[Rebuild media sync tables] duration: "

    invoke-static {v2, v3}, Llse;->a(J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    .line 1232
    :cond_1
    :goto_1
    return-void

    .line 1230
    :cond_2
    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method


# virtual methods
.method public a(ILjava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 1411
    iget-object v0, p0, Ldur;->h:Landroid/content/Context;

    invoke-static {v0, p2, p1}, Ljvd;->a(Landroid/content/Context;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    .line 1413
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1414
    const/4 v0, 0x0

    .line 1433
    :goto_0
    return-object v0

    .line 1417
    :cond_0
    const-string v1, "MediaSyncManager"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1418
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x28

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v2, v3

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "insertLocalVideo thumbnailUrl="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " localUri="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1422
    :cond_1
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 1423
    const-string v2, "image_url"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1424
    const-string v2, "filename"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1425
    const-string v2, "size"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1426
    const-string v2, "http_status"

    const/16 v3, 0xc8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1427
    const-string v2, "representation_type"

    const/16 v3, 0x8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1429
    iget-object v2, p0, Ldur;->h:Landroid/content/Context;

    .line 1430
    invoke-static {v2, p1}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v2

    invoke-virtual {v2}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 1431
    const-string v3, "media_cache"

    const-string v4, "filename"

    invoke-virtual {v2, v3, v4, v1}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto :goto_0
.end method

.method public declared-synchronized a(Ldvd;)Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 345
    monitor-enter p0

    :try_start_0
    new-instance v1, Ldve;

    iget-object v3, p0, Ldur;->h:Landroid/content/Context;

    invoke-interface {p1}, Ldvd;->a()I

    move-result v4

    new-instance v5, Ldvz;

    invoke-direct {v5}, Ldvz;-><init>()V

    new-instance v6, Landroid/content/SyncResult;

    invoke-direct {v6}, Landroid/content/SyncResult;-><init>()V

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    move-object v2, p0

    invoke-direct/range {v1 .. v9}, Ldve;-><init>(Ldur;Landroid/content/Context;ILdvz;Landroid/content/SyncResult;Lnyc;J)V

    iput-object v1, p0, Ldur;->p:Ldve;

    .line 349
    iget-object v1, p0, Ldur;->h:Landroid/content/Context;

    .line 350
    invoke-interface {p1}, Ldvd;->a()I

    move-result v2

    invoke-static {v1, v2}, Ldrg;->a(Landroid/content/Context;I)Ldrg;

    move-result-object v1

    .line 351
    invoke-virtual {v1}, Ldrg;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 353
    :try_start_1
    iget v2, p0, Ldur;->n:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Ldur;->n:I

    invoke-direct {p0, v2}, Ldur;->b(I)V

    .line 354
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const/4 v3, 0x3

    invoke-direct {p0, v1, v2, p1, v3}, Ldur;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/content/ContentValues;Ldvd;I)Ljava/lang/String;
    :try_end_1
    .catch Ldva; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ldux; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 367
    const/4 v1, 0x0

    :try_start_2
    iput-object v1, p0, Ldur;->p:Ldve;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    .line 356
    :catch_0
    move-exception v1

    :try_start_3
    invoke-direct {p0}, Ldur;->j()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 360
    const/4 v1, 0x0

    :try_start_4
    iput-object v1, p0, Ldur;->p:Ldve;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 345
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 362
    :catch_1
    move-exception v1

    const/4 v1, 0x0

    :try_start_5
    iput-object v1, p0, Ldur;->p:Ldve;

    goto :goto_0

    :catchall_1
    move-exception v0

    const/4 v1, 0x0

    iput-object v1, p0, Ldur;->p:Ldve;

    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0
.end method

.method a(Landroid/database/sqlite/SQLiteDatabase;Z)Ljava/util/List;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Z)",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 756
    iget-object v0, p0, Ldur;->a:Lhei;

    iget-object v1, p0, Ldur;->p:Ldve;

    iget v1, v1, Ldve;->a:I

    invoke-interface {v0, v1}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 758
    new-array v2, v6, [Ljava/lang/String;

    const-string v1, "cluster_id"

    aput-object v1, v2, v3

    .line 760
    new-array v4, v7, [Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    aput-object v0, v1, v3

    .line 761
    invoke-static {v3, v1}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v3

    .line 762
    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v4, v6

    .line 765
    const-string v3, "view_id = ? AND type = ? AND media_attr & 512 == 0"

    .line 766
    const-string v0, "view_order"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    if-eqz p2, :cond_0

    const-string v0, " DESC"

    :goto_0
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 768
    :goto_1
    const-string v1, "all_tiles"

    move-object v0, p1

    move-object v6, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 771
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 773
    :goto_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 774
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 777
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 766
    :cond_0
    const-string v0, " ASC"

    goto :goto_0

    :cond_1
    new-instance v7, Ljava/lang/String;

    invoke-direct {v7, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_1

    .line 777
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 779
    return-object v0
.end method

.method public declared-synchronized a(ILdvz;Landroid/content/SyncResult;)V
    .locals 11

    .prologue
    .line 298
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Ldur;->h:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v10

    .line 299
    const-string v0, "last_media_sync_time"

    const-wide/16 v2, 0x0

    invoke-interface {v10, v0, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 300
    iget-object v0, p0, Ldur;->h:Landroid/content/Context;

    .line 301
    invoke-static {v0, p1}, Lcuj;->a(Landroid/content/Context;I)Lnyc;

    move-result-object v7

    .line 302
    new-instance v1, Ldve;

    iget-object v3, p0, Ldur;->h:Landroid/content/Context;

    move-object v2, p0

    move v4, p1

    move-object v5, p2

    move-object v6, p3

    invoke-direct/range {v1 .. v9}, Ldve;-><init>(Ldur;Landroid/content/Context;ILdvz;Landroid/content/SyncResult;Lnyc;J)V

    iput-object v1, p0, Ldur;->p:Ldve;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 305
    :try_start_1
    invoke-direct {p0}, Ldur;->e()V
    :try_end_1
    .catch Ldva; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 319
    :try_start_2
    const-string v0, "MediaSyncManager"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Ldur;->p:Ldve;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x16

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "[Sync media complete] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    :cond_0
    iget-object v0, p0, Ldur;->p:Ldve;

    iget-wide v0, v0, Ldve;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 324
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_media_sync_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 327
    :cond_1
    iget-object v0, p0, Ldur;->p:Ldve;

    iget-object v0, v0, Ldve;->b:Ldvz;

    iget-object v1, p0, Ldur;->p:Ldve;

    iget v1, v1, Ldve;->k:I

    invoke-virtual {v0, v1}, Ldvz;->b(I)V

    .line 328
    iget-object v0, p0, Ldur;->p:Ldve;

    iget-object v0, v0, Ldve;->b:Ldvz;

    iget-object v1, p0, Ldur;->p:Ldve;

    iget v1, v1, Ldve;->l:I

    invoke-virtual {v0, v1}, Ldvz;->a(I)V

    .line 329
    iget-object v0, p0, Ldur;->p:Ldve;

    iget-object v0, v0, Ldve;->b:Ldvz;

    iget-object v1, p0, Ldur;->p:Ldve;

    iget v1, v1, Ldve;->j:I

    invoke-virtual {v0, v1}, Ldvz;->c(I)V

    .line 330
    iget-object v0, p0, Ldur;->p:Ldve;

    iget-object v0, v0, Ldve;->b:Ldvz;

    iget-object v1, p0, Ldur;->p:Ldve;

    iget-wide v2, v1, Ldve;->h:J

    invoke-virtual {v0, v2, v3}, Ldvz;->a(J)V

    .line 331
    iget-object v0, p0, Ldur;->p:Ldve;

    iget-object v0, v0, Ldve;->b:Ldvz;

    iget-object v1, p0, Ldur;->p:Ldve;

    iget v1, v1, Ldve;->m:I

    invoke-virtual {v0, v1}, Ldvz;->k(I)V

    .line 333
    const/4 v0, 0x0

    iput-object v0, p0, Ldur;->p:Ldve;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 334
    :goto_0
    monitor-exit p0

    return-void

    .line 307
    :catch_0
    move-exception v0

    :try_start_3
    invoke-direct {p0}, Ldur;->j()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 314
    :try_start_4
    invoke-direct {p0}, Ldur;->e()V
    :try_end_4
    .catch Ldva; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 319
    :goto_1
    :try_start_5
    const-string v0, "MediaSyncManager"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 320
    iget-object v0, p0, Ldur;->p:Ldve;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0x16

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "[Sync media complete] "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    :cond_2
    iget-object v0, p0, Ldur;->p:Ldve;

    iget-wide v0, v0, Ldve;->h:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_3

    .line 324
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last_media_sync_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 327
    :cond_3
    iget-object v0, p0, Ldur;->p:Ldve;

    iget-object v0, v0, Ldve;->b:Ldvz;

    iget-object v1, p0, Ldur;->p:Ldve;

    iget v1, v1, Ldve;->k:I

    invoke-virtual {v0, v1}, Ldvz;->b(I)V

    .line 328
    iget-object v0, p0, Ldur;->p:Ldve;

    iget-object v0, v0, Ldve;->b:Ldvz;

    iget-object v1, p0, Ldur;->p:Ldve;

    iget v1, v1, Ldve;->l:I

    invoke-virtual {v0, v1}, Ldvz;->a(I)V

    .line 329
    iget-object v0, p0, Ldur;->p:Ldve;

    iget-object v0, v0, Ldve;->b:Ldvz;

    iget-object v1, p0, Ldur;->p:Ldve;

    iget v1, v1, Ldve;->j:I

    invoke-virtual {v0, v1}, Ldvz;->c(I)V

    .line 330
    iget-object v0, p0, Ldur;->p:Ldve;

    iget-object v0, v0, Ldve;->b:Ldvz;

    iget-object v1, p0, Ldur;->p:Ldve;

    iget-wide v2, v1, Ldve;->h:J

    invoke-virtual {v0, v2, v3}, Ldvz;->a(J)V

    .line 331
    iget-object v0, p0, Ldur;->p:Ldve;

    iget-object v0, v0, Ldve;->b:Ldvz;

    iget-object v1, p0, Ldur;->p:Ldve;

    iget v1, v1, Ldve;->m:I

    invoke-virtual {v0, v1}, Ldvz;->k(I)V

    .line 333
    const/4 v0, 0x0

    iput-object v0, p0, Ldur;->p:Ldve;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_0

    .line 298
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 316
    :catch_1
    move-exception v0

    :try_start_6
    const-string v0, "MediaSyncManager"

    const-string v1, "***** Media cache table out of sync AGAIN!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_1

    .line 319
    :catchall_1
    move-exception v0

    :try_start_7
    const-string v1, "MediaSyncManager"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 320
    iget-object v1, p0, Ldur;->p:Ldve;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x16

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "[Sync media complete] "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    :cond_4
    iget-object v1, p0, Ldur;->p:Ldve;

    iget-wide v2, v1, Ldve;->h:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_5

    .line 324
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "last_media_sync_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 327
    :cond_5
    iget-object v1, p0, Ldur;->p:Ldve;

    iget-object v1, v1, Ldve;->b:Ldvz;

    iget-object v2, p0, Ldur;->p:Ldve;

    iget v2, v2, Ldve;->k:I

    invoke-virtual {v1, v2}, Ldvz;->b(I)V

    .line 328
    iget-object v1, p0, Ldur;->p:Ldve;

    iget-object v1, v1, Ldve;->b:Ldvz;

    iget-object v2, p0, Ldur;->p:Ldve;

    iget v2, v2, Ldve;->l:I

    invoke-virtual {v1, v2}, Ldvz;->a(I)V

    .line 329
    iget-object v1, p0, Ldur;->p:Ldve;

    iget-object v1, v1, Ldve;->b:Ldvz;

    iget-object v2, p0, Ldur;->p:Ldve;

    iget v2, v2, Ldve;->j:I

    invoke-virtual {v1, v2}, Ldvz;->c(I)V

    .line 330
    iget-object v1, p0, Ldur;->p:Ldve;

    iget-object v1, v1, Ldve;->b:Ldvz;

    iget-object v2, p0, Ldur;->p:Ldve;

    iget-wide v2, v2, Ldve;->h:J

    invoke-virtual {v1, v2, v3}, Ldvz;->a(J)V

    .line 331
    iget-object v1, p0, Ldur;->p:Ldve;

    iget-object v1, v1, Ldve;->b:Ldvz;

    iget-object v2, p0, Ldur;->p:Ldve;

    iget v2, v2, Ldve;->m:I

    invoke-virtual {v1, v2}, Ldvz;->k(I)V

    .line 333
    const/4 v1, 0x0

    iput-object v1, p0, Ldur;->p:Ldve;

    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0
.end method
