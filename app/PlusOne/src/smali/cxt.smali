.class final Lcxt;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lcxr;


# direct methods
.method constructor <init>(Lcxr;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcxt;->a:Lcxr;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 78
    const-string v0, "account_id"

    const/4 v1, -0x1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 80
    const-string v1, "view_id"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 81
    new-instance v2, Lemo;

    iget-object v3, p0, Lcxt;->a:Lcxr;

    invoke-virtual {v3}, Lcxr;->n()Lz;

    move-result-object v3

    iget-object v4, p0, Lcxt;->a:Lcxr;

    invoke-static {v4}, Lcxr;->a(Lcxr;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v0, v1, v4}, Lemo;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 12
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v1, 0x0

    const/4 v9, 0x2

    const/4 v0, 0x1

    .line 86
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-nez v2, :cond_1

    .line 87
    :cond_0
    iget-object v0, p0, Lcxt;->a:Lcxr;

    invoke-static {v0}, Lcxr;->b(Lcxr;)V

    .line 126
    :goto_0
    return-void

    .line 91
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 92
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-nez v2, :cond_2

    .line 94
    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 95
    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 96
    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 97
    const/4 v5, 0x5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 98
    const/4 v6, 0x6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 100
    iget-object v8, p0, Lcxt;->a:Lcxr;

    invoke-static {v8}, Lcxr;->c(Lcxr;)Ldec;

    move-result-object v8

    invoke-virtual {v8, v2}, Ldec;->a(Ljava/lang/String;)V

    .line 101
    iget-object v2, p0, Lcxt;->a:Lcxr;

    invoke-static {v2}, Lcxr;->c(Lcxr;)Ldec;

    move-result-object v2

    invoke-virtual {v2, v3}, Ldec;->b(Ljava/lang/String;)V

    .line 102
    iget-object v2, p0, Lcxt;->a:Lcxr;

    invoke-static {v2}, Lcxr;->c(Lcxr;)Ldec;

    move-result-object v2

    invoke-virtual {v2, v4}, Ldec;->c(Ljava/lang/String;)V

    .line 103
    iget-object v2, p0, Lcxt;->a:Lcxr;

    invoke-static {v2}, Lcxr;->c(Lcxr;)Ldec;

    move-result-object v2

    invoke-virtual {v2, v5}, Ldec;->d(Ljava/lang/String;)V

    .line 104
    iget-object v2, p0, Lcxt;->a:Lcxr;

    invoke-static {v2}, Lcxr;->c(Lcxr;)Ldec;

    move-result-object v2

    invoke-virtual {v2, v6, v7}, Ldec;->a(J)V

    .line 107
    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 108
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-ne v2, v9, :cond_3

    .line 110
    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 111
    invoke-interface {p1, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 112
    iget-object v4, p0, Lcxt;->a:Lcxr;

    invoke-static {v4}, Lcxr;->c(Lcxr;)Ldec;

    move-result-object v4

    invoke-virtual {v4, v2}, Ldec;->a(I)V

    .line 113
    iget-object v2, p0, Lcxt;->a:Lcxr;

    invoke-static {v2}, Lcxr;->c(Lcxr;)Ldec;

    move-result-object v2

    invoke-virtual {v2, v3}, Ldec;->b(I)V

    .line 115
    const/4 v2, 0x6

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-eqz v2, :cond_4

    move v2, v0

    .line 116
    :goto_1
    if-eqz v2, :cond_3

    .line 117
    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 118
    const/4 v3, 0x5

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_5

    .line 121
    :goto_2
    iget-object v1, p0, Lcxt;->a:Lcxr;

    invoke-static {v1}, Lcxr;->c(Lcxr;)Ldec;

    move-result-object v1

    invoke-virtual {v1, v2}, Ldec;->e(Ljava/lang/String;)V

    .line 122
    iget-object v1, p0, Lcxt;->a:Lcxr;

    invoke-static {v1}, Lcxr;->c(Lcxr;)Ldec;

    move-result-object v1

    invoke-virtual {v1, v0}, Ldec;->a(Z)V

    .line 125
    :cond_3
    iget-object v0, p0, Lcxt;->a:Lcxr;

    iget-object v1, p0, Lcxt;->a:Lcxr;

    invoke-static {v1}, Lcxr;->c(Lcxr;)Ldec;

    move-result-object v1

    invoke-static {v0, v1}, Lcxr;->a(Lcxr;Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_4
    move v2, v1

    .line 115
    goto :goto_1

    :cond_5
    move v0, v1

    .line 118
    goto :goto_2
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 131
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 75
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lcxt;->a(Landroid/database/Cursor;)V

    return-void
.end method
