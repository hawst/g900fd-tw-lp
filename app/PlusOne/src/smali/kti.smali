.class public final Lkti;
.super Lhny;
.source "PG"


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Lkfo;

.field private final d:Lktq;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 24
    const-string v0, "GetSquareTask"

    invoke-direct {p0, p1, v0}, Lhny;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 26
    iput p2, p0, Lkti;->a:I

    .line 27
    iput-object p3, p0, Lkti;->b:Ljava/lang/String;

    .line 28
    new-instance v0, Lkfo;

    iget v1, p0, Lkti;->a:I

    invoke-direct {v0, p1, v1}, Lkfo;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lkti;->c:Lkfo;

    .line 29
    const-class v0, Lktq;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lktq;

    iput-object v0, p0, Lkti;->d:Lktq;

    .line 30
    return-void
.end method


# virtual methods
.method protected a()Lhoz;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 34
    new-instance v1, Lktl;

    .line 35
    invoke-virtual {p0}, Lkti;->f()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lkti;->c:Lkfo;

    iget-object v3, p0, Lkti;->b:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3}, Lktl;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;)V

    .line 36
    invoke-virtual {v1}, Lktl;->l()V

    .line 38
    invoke-virtual {v1}, Lktl;->t()Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    :try_start_0
    iget-object v2, p0, Lkti;->d:Lktq;

    iget v3, p0, Lkti;->a:I

    .line 41
    invoke-virtual {v1}, Lktl;->D()Loxu;

    move-result-object v0

    check-cast v0, Lmdr;

    iget-object v0, v0, Lmdr;->a:Lntr;

    iget-object v0, v0, Lntr;->a:Lnsr;

    .line 40
    invoke-interface {v2, v3, v0}, Lktq;->a(ILnsr;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :cond_0
    new-instance v0, Lhoz;

    iget v2, v1, Lkff;->i:I

    iget-object v1, v1, Lkff;->k:Ljava/lang/Exception;

    invoke-direct {v0, v2, v1, v4}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    :goto_0
    return-object v0

    .line 42
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 43
    new-instance v0, Lhoz;

    const/4 v2, 0x0

    invoke-direct {v0, v2, v1, v4}, Lhoz;-><init>(ILjava/lang/Exception;Ljava/lang/String;)V

    goto :goto_0
.end method
