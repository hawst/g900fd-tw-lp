.class public abstract Ldyj;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ldyx;
.implements Lhob;
.implements Lhsw;
.implements Llgs;


# instance fields
.field public N:I

.field public O:Ldyq;

.field public P:[Ljava/lang/String;

.field public Q:[I

.field private R:Ldyq;

.field private S:Lcom/google/android/apps/plus/views/ImageTextButton;

.field private T:Landroid/view/View;

.field private U:Landroid/widget/TextView;

.field private V:Landroid/view/View;

.field private W:Landroid/widget/TextView;

.field private X:Landroid/view/View;

.field private Y:Landroid/widget/ImageView;

.field private Z:Landroid/widget/CheckBox;

.field private aa:Landroid/widget/ImageView;

.field private ab:Landroid/widget/CheckBox;

.field private ac:Landroid/widget/TextView;

.field private ad:Landroid/view/View;

.field private ae:Z

.field private af:Z

.field private ag:Z

.field private ah:Lhoc;

.field private ai:Ldvv;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 54
    invoke-direct {p0}, Llol;-><init>()V

    .line 180
    new-instance v0, Lhoc;

    iget-object v1, p0, Ldyj;->av:Llqm;

    invoke-direct {v0, p0, v1}, Lhoc;-><init>(Lu;Llqr;)V

    iput-object v0, p0, Ldyj;->ah:Lhoc;

    .line 186
    new-instance v0, Lhsx;

    iget-object v1, p0, Ldyj;->av:Llqm;

    invoke-direct {v0, v1, p0}, Lhsx;-><init>(Llqr;Lhsw;)V

    .line 187
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 430
    iget-object v1, p0, Ldyj;->Y:Landroid/widget/ImageView;

    iget-object v0, p0, Ldyj;->O:Ldyq;

    iget-boolean v0, v0, Ldyq;->e:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0203d3

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 433
    iget-object v1, p0, Ldyj;->Y:Landroid/widget/ImageView;

    iget-object v0, p0, Ldyj;->O:Ldyq;

    iget-boolean v0, v0, Ldyq;->e:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0a0711

    :goto_1
    invoke-virtual {p0, v0}, Ldyj;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 437
    invoke-direct {p0}, Ldyj;->ag()V

    .line 438
    return-void

    .line 430
    :cond_0
    const v0, 0x7f0203d2

    goto :goto_0

    .line 433
    :cond_1
    const v0, 0x7f0a0710

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 631
    const v0, 0x7f0a0aff

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    .line 633
    const/16 v1, 0x11

    invoke-virtual {v0, v1, v2, v2}, Landroid/widget/Toast;->setGravity(III)V

    .line 634
    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 635
    return-void
.end method

.method static synthetic a(Ldyj;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ldyj;->a()V

    return-void
.end method

.method private af()V
    .locals 2

    .prologue
    .line 441
    iget-object v1, p0, Ldyj;->aa:Landroid/widget/ImageView;

    iget-object v0, p0, Ldyj;->O:Ldyq;

    iget-boolean v0, v0, Ldyq;->f:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0a0713

    :goto_0
    invoke-virtual {p0, v0}, Ldyj;->e_(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 444
    iget-object v1, p0, Ldyj;->ac:Landroid/widget/TextView;

    iget-object v0, p0, Ldyj;->O:Ldyq;

    iget-boolean v0, v0, Ldyq;->f:Z

    if-eqz v0, :cond_1

    const v0, 0x7f0a0715

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 447
    invoke-direct {p0}, Ldyj;->ag()V

    .line 448
    return-void

    .line 441
    :cond_0
    const v0, 0x7f0a0714

    goto :goto_0

    .line 444
    :cond_1
    const v0, 0x7f0a0716

    goto :goto_1
.end method

.method private ag()V
    .locals 3

    .prologue
    .line 451
    iget-object v1, p0, Ldyj;->S:Lcom/google/android/apps/plus/views/ImageTextButton;

    iget-object v0, p0, Ldyj;->R:Ldyq;

    iget-object v2, p0, Ldyj;->O:Ldyq;

    invoke-virtual {v0, v2}, Ldyq;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/ImageTextButton;->setEnabled(Z)V

    .line 452
    return-void

    .line 451
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ah()V
    .locals 3

    .prologue
    .line 470
    invoke-virtual {p0}, Ldyj;->n()Lz;

    move-result-object v0

    const v1, 0x7f0a0592

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 471
    return-void
.end method

.method private ai()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 613
    iget-object v0, p0, Ldyj;->S:Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/ImageTextButton;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614
    const v0, 0x7f0a024d

    invoke-virtual {p0, v0}, Ldyj;->e_(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0a037a

    invoke-virtual {p0, v1}, Ldyj;->e_(I)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0a07fa

    invoke-virtual {p0, v2}, Ldyj;->e_(I)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0a07fd

    invoke-virtual {p0, v3}, Ldyj;->e_(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Llgr;->b(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)Llgr;

    move-result-object v0

    invoke-virtual {v0, p0, v4}, Llgr;->a(Lu;I)V

    invoke-virtual {p0}, Ldyj;->p()Lae;

    move-result-object v1

    const-string v2, "quit"

    invoke-virtual {v0, v1, v2}, Llgr;->a(Lae;Ljava/lang/String;)V

    .line 618
    :goto_0
    return-void

    .line 616
    :cond_0
    invoke-virtual {p0, v4}, Ldyj;->d(I)V

    goto :goto_0
.end method

.method private aj()V
    .locals 2

    .prologue
    .line 697
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 698
    invoke-static {}, Ljpe;->b()V

    .line 700
    :cond_0
    invoke-virtual {p0}, Ldyj;->Y()V

    .line 701
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldyj;->ag:Z

    .line 703
    invoke-direct {p0}, Ldyj;->ah()V

    .line 705
    iget-object v0, p0, Ldyj;->O:Ldyq;

    iget-object v1, p0, Ldyj;->R:Ldyq;

    iget-object v1, v1, Ldyq;->b:Ljava/lang/String;

    iput-object v1, v0, Ldyq;->b:Ljava/lang/String;

    .line 706
    iget-object v0, p0, Ldyj;->O:Ldyq;

    iget-object v1, p0, Ldyj;->R:Ldyq;

    iget-boolean v1, v1, Ldyq;->f:Z

    iput-boolean v1, v0, Ldyq;->f:Z

    .line 707
    invoke-virtual {p0}, Ldyj;->W()V

    .line 708
    invoke-direct {p0}, Ldyj;->af()V

    .line 709
    return-void
.end method

.method private ak()V
    .locals 1

    .prologue
    .line 712
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 713
    invoke-static {}, Ljpe;->b()V

    .line 715
    :cond_0
    invoke-virtual {p0}, Ldyj;->Y()V

    .line 716
    const/4 v0, 0x1

    iput-boolean v0, p0, Ldyj;->ag:Z

    .line 717
    iget-boolean v0, p0, Ldyj;->ae:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Ldyj;->af:Z

    if-eqz v0, :cond_2

    .line 718
    :cond_1
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Ldyj;->d(I)V

    .line 720
    :cond_2
    return-void
.end method

.method static synthetic b(Ldyj;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Ldyj;->Z:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic c(Ldyj;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ldyj;->af()V

    return-void
.end method

.method static synthetic d(Ldyj;)Landroid/widget/CheckBox;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Ldyj;->ab:Landroid/widget/CheckBox;

    return-object v0
.end method


# virtual methods
.method protected U()Z
    .locals 1

    .prologue
    .line 416
    const/4 v0, 0x1

    return v0
.end method

.method public V()Z
    .locals 1

    .prologue
    .line 724
    invoke-virtual {p0}, Ldyj;->ae()V

    .line 725
    const/4 v0, 0x1

    return v0
.end method

.method protected W()V
    .locals 2

    .prologue
    .line 420
    iget-object v0, p0, Ldyj;->U:Landroid/widget/TextView;

    iget-object v1, p0, Ldyj;->O:Ldyq;

    iget-object v1, v1, Ldyq;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 421
    invoke-direct {p0}, Ldyj;->ag()V

    .line 422
    return-void
.end method

.method protected X()V
    .locals 2

    .prologue
    .line 425
    iget-object v1, p0, Ldyj;->W:Landroid/widget/TextView;

    iget-object v0, p0, Ldyj;->O:Ldyq;

    iget v0, v0, Ldyq;->c:I

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f0a0708

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 426
    invoke-direct {p0}, Ldyj;->ag()V

    .line 427
    return-void

    .line 425
    :pswitch_0
    const v0, 0x7f0a0707

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0a0706

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0a0705

    goto :goto_0

    :pswitch_3
    const v0, 0x7f0a0704

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected Y()V
    .locals 2

    .prologue
    .line 462
    invoke-virtual {p0}, Ldyj;->p()Lae;

    move-result-object v0

    const-string v1, "req_pending"

    invoke-virtual {v0, v1}, Lae;->a(Ljava/lang/String;)Lu;

    move-result-object v0

    check-cast v0, Lt;

    .line 464
    if-eqz v0, :cond_0

    .line 465
    invoke-virtual {v0}, Lt;->a()V

    .line 467
    :cond_0
    return-void
.end method

.method public abstract Z()I
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 243
    const v0, 0x7f040066

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 245
    const v0, 0x7f1001da

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ImageTextButton;

    .line 246
    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/ImageTextButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 247
    const v0, 0x7f1001fa

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/ImageTextButton;

    iput-object v0, p0, Ldyj;->S:Lcom/google/android/apps/plus/views/ImageTextButton;

    .line 248
    iget-object v0, p0, Ldyj;->S:Lcom/google/android/apps/plus/views/ImageTextButton;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/ImageTextButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 249
    iget-object v0, p0, Ldyj;->S:Lcom/google/android/apps/plus/views/ImageTextButton;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/apps/plus/views/ImageTextButton;->setEnabled(Z)V

    .line 251
    const v0, 0x7f1001fc

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldyj;->T:Landroid/view/View;

    .line 252
    const v0, 0x7f1001fe

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldyj;->U:Landroid/widget/TextView;

    .line 253
    const v0, 0x7f100203

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldyj;->V:Landroid/view/View;

    .line 254
    const v0, 0x7f100205

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldyj;->W:Landroid/widget/TextView;

    .line 256
    const v0, 0x7f10020b

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Ldyj;->Z:Landroid/widget/CheckBox;

    .line 257
    const v0, 0x7f10020a

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ldyj;->Y:Landroid/widget/ImageView;

    .line 258
    const v0, 0x7f100209

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldyj;->X:Landroid/view/View;

    .line 260
    const v0, 0x7f100214

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Ldyj;->aa:Landroid/widget/ImageView;

    .line 261
    const v0, 0x7f100215

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Ldyj;->ab:Landroid/widget/CheckBox;

    .line 262
    const v0, 0x7f100217

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Ldyj;->ac:Landroid/widget/TextView;

    .line 263
    const v0, 0x7f100213

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Ldyj;->ad:Landroid/view/View;

    .line 265
    iget-object v0, p0, Ldyj;->O:Ldyq;

    if-nez v0, :cond_0

    .line 266
    invoke-virtual {p0, v1}, Ldyj;->c(Landroid/view/View;)V

    .line 272
    :goto_0
    return-object v1

    .line 268
    :cond_0
    invoke-virtual {p0, v1}, Ldyj;->e(Landroid/view/View;)V

    .line 269
    invoke-virtual {p0, v1}, Ldyj;->d(Landroid/view/View;)V

    goto :goto_0
.end method

.method public a(ILandroid/os/Bundle;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 557
    const-string v0, "velocity"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 558
    iget-object v0, p0, Ldyj;->O:Ldyq;

    iget-object v1, p0, Ldyj;->Q:[I

    aget v1, v1, p1

    iput v1, v0, Ldyq;->c:I

    .line 559
    invoke-virtual {p0}, Ldyj;->X()V

    .line 561
    :cond_0
    return-void
.end method

.method public a(IZLandroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 565
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 199
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 201
    iget-object v0, p0, Ldyj;->au:Llnh;

    const-class v1, Lhoc;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhoc;

    invoke-virtual {v0, p0}, Lhoc;->a(Lhob;)Lhoc;

    .line 203
    if-eqz p1, :cond_0

    .line 204
    const-string v0, "original_settings"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ldyq;

    iput-object v0, p0, Ldyj;->R:Ldyq;

    .line 206
    const-string v0, "settings"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ldyq;

    iput-object v0, p0, Ldyj;->O:Ldyq;

    .line 208
    const-string v0, "saved_volume_and_properties"

    .line 209
    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Ldyj;->ae:Z

    .line 212
    :cond_0
    invoke-virtual {p0}, Ldyj;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 213
    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Ldyj;->N:I

    .line 214
    return-void
.end method

.method public a(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 542
    const-string v0, "quit"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 543
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ldyj;->d(I)V

    .line 545
    :cond_0
    return-void
.end method

.method public a(Ldyq;)V
    .locals 2

    .prologue
    .line 399
    iput-object p1, p0, Ldyj;->O:Ldyq;

    .line 400
    iget-object v0, p0, Ldyj;->R:Ldyq;

    if-nez v0, :cond_0

    .line 401
    new-instance v0, Ldyq;

    iget-object v1, p0, Ldyj;->O:Ldyq;

    invoke-direct {v0, v1}, Ldyq;-><init>(Ldyq;)V

    iput-object v0, p0, Ldyj;->R:Ldyq;

    .line 403
    :cond_0
    invoke-virtual {p0}, Ldyj;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldyj;->e(Landroid/view/View;)V

    .line 404
    invoke-virtual {p0}, Ldyj;->x()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Ldyj;->d(Landroid/view/View;)V

    .line 405
    return-void
.end method

.method public a(Ljava/lang/String;Lhoz;Lhos;)V
    .locals 2

    .prologue
    .line 640
    const-string v0, "UpdateCircleTaskLegacy"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 641
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Ldyj;->aj()V

    .line 658
    :cond_0
    :goto_0
    return-void

    .line 641
    :cond_1
    invoke-direct {p0}, Ldyj;->ak()V

    goto :goto_0

    .line 642
    :cond_2
    const-string v0, "UpdateCircleTask"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 643
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 644
    invoke-static {}, Ljpe;->b()V

    .line 646
    :cond_3
    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-direct {p0}, Ldyj;->aj()V

    goto :goto_0

    :cond_4
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-static {}, Ljpe;->b()V

    :cond_5
    iget-object v0, p0, Ldyj;->ai:Ldvv;

    iget v1, p0, Ldyj;->N:I

    invoke-virtual {v0, v1}, Ldvv;->a(I)Lhny;

    move-result-object v0

    iget-object v1, p0, Ldyj;->ah:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    goto :goto_0

    .line 647
    :cond_6
    const-string v0, "LoadCirclesTask"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 648
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 649
    invoke-static {}, Ljpe;->b()V

    .line 651
    :cond_7
    invoke-direct {p0}, Ldyj;->ak()V

    goto :goto_0

    .line 652
    :cond_8
    const-string v0, "SetVolumeControlTask"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 653
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_9

    .line 654
    invoke-static {}, Ljpe;->b()V

    .line 656
    :cond_9
    invoke-virtual {p0}, Ldyj;->Y()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Ldyj;->af:Z

    invoke-virtual {p2}, Lhoz;->f()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-direct {p0}, Ldyj;->ah()V

    goto :goto_0

    :cond_a
    iget-boolean v0, p0, Ldyj;->ae:Z

    if-eqz v0, :cond_b

    iget-boolean v0, p0, Ldyj;->ag:Z

    if-eqz v0, :cond_0

    :cond_b
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Ldyj;->d(I)V

    goto :goto_0
.end method

.method public abstract aa()Ljava/lang/String;
.end method

.method protected ab()V
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v8, 0x1

    .line 489
    .line 492
    iget-object v0, p0, Ldyj;->O:Ldyq;

    iget v0, v0, Ldyq;->c:I

    iget-object v1, p0, Ldyj;->R:Ldyq;

    iget v1, v1, Ldyq;->c:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Ldyj;->O:Ldyq;

    iget-boolean v0, v0, Ldyq;->e:Z

    iget-object v1, p0, Ldyj;->R:Ldyq;

    iget-boolean v1, v1, Ldyq;->e:Z

    if-eq v0, v1, :cond_7

    .line 495
    :cond_0
    iget-object v0, p0, Ldyj;->O:Ldyq;

    iget v5, v0, Ldyq;->c:I

    .line 496
    new-instance v0, Ldqd;

    invoke-virtual {p0}, Ldyj;->n()Lz;

    move-result-object v1

    invoke-virtual {p0}, Ldyj;->c()I

    move-result v2

    .line 497
    invoke-virtual {p0}, Ldyj;->Z()I

    move-result v3

    invoke-virtual {p0}, Ldyj;->aa()Ljava/lang/String;

    move-result-object v4

    iget-object v6, p0, Ldyj;->O:Ldyq;

    iget-boolean v6, v6, Ldyq;->e:Z

    invoke-direct/range {v0 .. v6}, Ldqd;-><init>(Landroid/content/Context;IILjava/lang/String;IZ)V

    .line 499
    invoke-static {}, Ljpe;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 500
    invoke-static {}, Ljpe;->b()V

    .line 502
    :cond_1
    iget-object v1, p0, Ldyj;->ah:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    .line 503
    invoke-virtual {p0}, Ldyj;->ad()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0}, Ldyj;->ac()I

    move-result v0

    iget-object v2, p0, Ldyj;->R:Ldyq;

    iget v2, v2, Ldyq;->c:I

    iget-object v3, p0, Ldyj;->R:Ldyq;

    iget-boolean v3, v3, Ldyq;->e:Z

    iget-object v4, p0, Ldyj;->O:Ldyq;

    iget v4, v4, Ldyq;->c:I

    iget-object v5, p0, Ldyj;->O:Ldyq;

    iget-boolean v5, v5, Ldyq;->e:Z

    invoke-static {v0, v2, v3, v4, v5}, Lhmt;->a(IIZIZ)Lmxb;

    move-result-object v0

    const-string v2, "extra_notification_volume_change"

    invoke-static {v0}, Loxu;->a(Loxu;)[B

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    iget v2, p0, Ldyj;->N:I

    iget-object v0, p0, Ldyj;->au:Llnh;

    const-class v3, Lhms;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v3, Lhmr;

    iget-object v4, p0, Ldyj;->at:Llnl;

    invoke-direct {v3, v4, v2}, Lhmr;-><init>(Landroid/content/Context;I)V

    sget-object v2, Lhmv;->ag:Lhmv;

    invoke-virtual {v3, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    sget-object v3, Lhmw;->a:Lhmw;

    invoke-virtual {v2, v3}, Lhmr;->a(Lhmw;)Lhmr;

    move-result-object v2

    invoke-virtual {v2, v1}, Lhmr;->a(Landroid/os/Bundle;)Lhmr;

    move-result-object v1

    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    move v6, v8

    .line 506
    :goto_0
    iget-object v0, p0, Ldyj;->O:Ldyq;

    iget-object v0, v0, Ldyq;->b:Ljava/lang/String;

    iget-object v1, p0, Ldyj;->R:Ldyq;

    iget-object v1, v1, Ldyq;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Ldyj;->O:Ldyq;

    iget-boolean v0, v0, Ldyq;->f:Z

    iget-object v1, p0, Ldyj;->R:Ldyq;

    iget-boolean v1, v1, Ldyq;->f:Z

    if-eq v0, v1, :cond_4

    .line 510
    :cond_2
    invoke-virtual {p0}, Ldyj;->c()I

    move-result v1

    .line 511
    iget-object v0, p0, Ldyj;->O:Ldyq;

    iget-object v2, v0, Ldyq;->a:Ljava/lang/String;

    .line 512
    iget-object v0, p0, Ldyj;->O:Ldyq;

    iget-object v3, v0, Ldyq;->b:Ljava/lang/String;

    .line 513
    iget-object v0, p0, Ldyj;->O:Ldyq;

    iget-boolean v0, v0, Ldyq;->f:Z

    if-nez v0, :cond_6

    move v5, v8

    .line 516
    :goto_1
    invoke-static {}, Ljpe;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 517
    invoke-static {}, Ljpe;->b()V

    .line 519
    :cond_3
    iget-object v0, p0, Ldyj;->ai:Ldvv;

    const/4 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Ldvv;->a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lhny;

    move-result-object v0

    .line 521
    iget-object v1, p0, Ldyj;->ah:Lhoc;

    invoke-virtual {v1, v0}, Lhoc;->b(Lhny;)V

    move v7, v8

    .line 524
    :cond_4
    if-eqz v6, :cond_5

    if-eqz v7, :cond_5

    .line 525
    iput-boolean v8, p0, Ldyj;->ae:Z

    .line 528
    :cond_5
    const v0, 0x7f0a0717

    invoke-virtual {p0, v0}, Ldyj;->c(I)V

    .line 529
    return-void

    :cond_6
    move v5, v7

    .line 513
    goto :goto_1

    :cond_7
    move v6, v7

    goto :goto_0
.end method

.method public abstract ac()I
.end method

.method public abstract ad()Landroid/os/Bundle;
.end method

.method public ae()V
    .locals 0

    .prologue
    .line 606
    invoke-direct {p0}, Ldyj;->ai()V

    .line 607
    return-void
.end method

.method public b(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 549
    return-void
.end method

.method protected c()I
    .locals 1

    .prologue
    .line 276
    iget v0, p0, Ldyj;->N:I

    return v0
.end method

.method protected c(I)V
    .locals 3

    .prologue
    .line 455
    const/4 v0, 0x0

    .line 456
    invoke-virtual {p0, p1}, Ldyj;->e_(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    .line 455
    invoke-static {v0, v1, v2}, Lepl;->a(Ljava/lang/String;Ljava/lang/String;Z)Lepl;

    move-result-object v0

    .line 458
    invoke-virtual {p0}, Ldyj;->p()Lae;

    move-result-object v1

    const-string v2, "req_pending"

    invoke-virtual {v0, v1, v2}, Lepl;->a(Lae;Ljava/lang/String;)V

    .line 459
    return-void
.end method

.method public c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 191
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 193
    iget-object v0, p0, Ldyj;->au:Llnh;

    const-class v1, Lhoc;

    iget-object v2, p0, Ldyj;->ah:Lhoc;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 194
    iget-object v0, p0, Ldyj;->au:Llnh;

    const-class v1, Ldvv;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldvv;

    iput-object v0, p0, Ldyj;->ai:Ldvv;

    .line 195
    return-void
.end method

.method public c(Landroid/os/Bundle;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 553
    return-void
.end method

.method protected c(Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 280
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 281
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 282
    const v1, 0x7f10025f

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 283
    const v1, 0x7f100260

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 285
    const v0, 0x7f1001fb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 286
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 287
    return-void
.end method

.method public d(I)V
    .locals 2

    .prologue
    .line 598
    invoke-virtual {p0}, Ldyj;->n()Lz;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lz;->setResult(ILandroid/content/Intent;)V

    .line 599
    invoke-virtual {p0}, Ldyj;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    .line 600
    return-void
.end method

.method protected d(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 290
    const v0, 0x1020004

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 291
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 293
    const v0, 0x7f1001fb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 294
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 295
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 408
    const/4 v0, 0x1

    return v0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 218
    const-string v0, "settings"

    iget-object v1, p0, Ldyj;->O:Ldyq;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 219
    const-string v0, "original_settings"

    iget-object v1, p0, Ldyj;->R:Ldyq;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 220
    const-string v0, "saved_volume_and_properties"

    iget-boolean v1, p0, Ldyj;->ae:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 221
    invoke-super {p0, p1}, Llol;->e(Landroid/os/Bundle;)V

    .line 222
    return-void
.end method

.method protected e(Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/16 v4, 0x8

    .line 298
    iget-object v0, p0, Ldyj;->T:Landroid/view/View;

    new-instance v2, Ldyk;

    invoke-direct {v2, p0}, Ldyk;-><init>(Ldyj;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 309
    invoke-virtual {p0}, Ldyj;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 310
    invoke-virtual {p0}, Ldyj;->W()V

    .line 317
    :goto_0
    iget-object v0, p0, Ldyj;->V:Landroid/view/View;

    new-instance v2, Ldyl;

    invoke-direct {v2, p0}, Ldyl;-><init>(Ldyj;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 329
    invoke-virtual {p0}, Ldyj;->X()V

    .line 331
    invoke-virtual {p0}, Ldyj;->U()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 332
    iget-object v0, p0, Ldyj;->O:Ldyq;

    iget-boolean v0, v0, Ldyq;->d:Z

    if-eqz v0, :cond_2

    .line 333
    iget-object v0, p0, Ldyj;->Z:Landroid/widget/CheckBox;

    iget-object v2, p0, Ldyj;->O:Ldyq;

    iget-boolean v2, v2, Ldyq;->e:Z

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 334
    iget-object v0, p0, Ldyj;->Z:Landroid/widget/CheckBox;

    new-instance v2, Ldym;

    invoke-direct {v2, p0}, Ldym;-><init>(Ldyj;)V

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 341
    iget-object v0, p0, Ldyj;->X:Landroid/view/View;

    new-instance v2, Ldyn;

    invoke-direct {v2, p0}, Ldyn;-><init>(Ldyj;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 348
    invoke-direct {p0}, Ldyj;->a()V

    .line 371
    :goto_1
    invoke-virtual {p0}, Ldyj;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 372
    iget-object v0, p0, Ldyj;->ad:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 373
    const v0, 0x7f100210

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 374
    const v0, 0x7f100211

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 375
    const v0, 0x7f100212

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 376
    const v0, 0x7f100218

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 379
    :cond_0
    iget-object v2, p0, Ldyj;->ab:Landroid/widget/CheckBox;

    iget-object v0, p0, Ldyj;->O:Ldyq;

    iget-boolean v0, v0, Ldyq;->f:Z

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 380
    invoke-direct {p0}, Ldyj;->af()V

    .line 381
    iget-object v0, p0, Ldyj;->ab:Landroid/widget/CheckBox;

    new-instance v1, Ldyo;

    invoke-direct {v1, p0}, Ldyo;-><init>(Ldyj;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 389
    iget-object v0, p0, Ldyj;->ad:Landroid/view/View;

    new-instance v1, Ldyp;

    invoke-direct {v1, p0}, Ldyp;-><init>(Ldyj;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 396
    return-void

    .line 312
    :cond_1
    iget-object v0, p0, Ldyj;->T:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 313
    const v0, 0x7f1001ff

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 314
    const v0, 0x7f100200

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0

    .line 351
    :cond_2
    const v0, 0x7f10020c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 352
    invoke-virtual {p0}, Ldyj;->o()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b013d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 354
    iget-object v0, p0, Ldyj;->Z:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 355
    iget-object v0, p0, Ldyj;->Z:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 357
    iget-object v0, p0, Ldyj;->Y:Landroid/widget/ImageView;

    const v2, 0x7f0203d2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 358
    iget-object v0, p0, Ldyj;->Y:Landroid/widget/ImageView;

    const v2, 0x7f0a0710

    .line 359
    invoke-virtual {p0, v2}, Ldyj;->e_(I)Ljava/lang/String;

    move-result-object v2

    .line 358
    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 361
    const v0, 0x7f10020e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 362
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    .line 365
    :cond_3
    const v0, 0x7f100207

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 366
    const v0, 0x7f100208

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 367
    const v0, 0x7f10020f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 368
    iget-object v0, p0, Ldyj;->X:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    :cond_4
    move v0, v1

    .line 379
    goto/16 :goto_2
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 412
    const/4 v0, 0x1

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 622
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 623
    const v1, 0x7f1001da

    if-ne v0, v1, :cond_1

    .line 624
    invoke-direct {p0}, Ldyj;->ai()V

    .line 628
    :cond_0
    :goto_0
    return-void

    .line 625
    :cond_1
    const v1, 0x7f1001fa

    if-ne v0, v1, :cond_0

    .line 626
    invoke-virtual {p0}, Ldyj;->ab()V

    goto :goto_0
.end method
