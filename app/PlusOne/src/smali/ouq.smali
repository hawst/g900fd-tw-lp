.class public final Louq;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Lorq;

.field private c:Lorp;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10
    invoke-direct {p0}, Loxq;-><init>()V

    .line 15
    iput-object v0, p0, Louq;->b:Lorq;

    .line 18
    iput-object v0, p0, Louq;->c:Lorp;

    .line 10
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 38
    const/4 v0, 0x0

    .line 39
    iget-object v1, p0, Louq;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 40
    const/4 v0, 0x1

    iget-object v1, p0, Louq;->a:Ljava/lang/Boolean;

    .line 41
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 43
    :cond_0
    iget-object v1, p0, Louq;->b:Lorq;

    if-eqz v1, :cond_1

    .line 44
    const/4 v1, 0x2

    iget-object v2, p0, Louq;->b:Lorq;

    .line 45
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 47
    :cond_1
    iget-object v1, p0, Louq;->c:Lorp;

    if-eqz v1, :cond_2

    .line 48
    const/4 v1, 0x4

    iget-object v2, p0, Louq;->c:Lorp;

    .line 49
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 51
    :cond_2
    iget-object v1, p0, Louq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 52
    iput v0, p0, Louq;->ai:I

    .line 53
    return v0
.end method

.method public a(Loxn;)Louq;
    .locals 2

    .prologue
    .line 61
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 62
    sparse-switch v0, :sswitch_data_0

    .line 66
    iget-object v1, p0, Louq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 67
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Louq;->ah:Ljava/util/List;

    .line 70
    :cond_1
    iget-object v1, p0, Louq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    :sswitch_0
    return-object p0

    .line 77
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Louq;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 81
    :sswitch_2
    iget-object v0, p0, Louq;->b:Lorq;

    if-nez v0, :cond_2

    .line 82
    new-instance v0, Lorq;

    invoke-direct {v0}, Lorq;-><init>()V

    iput-object v0, p0, Louq;->b:Lorq;

    .line 84
    :cond_2
    iget-object v0, p0, Louq;->b:Lorq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 88
    :sswitch_3
    iget-object v0, p0, Louq;->c:Lorp;

    if-nez v0, :cond_3

    .line 89
    new-instance v0, Lorp;

    invoke-direct {v0}, Lorp;-><init>()V

    iput-object v0, p0, Louq;->c:Lorp;

    .line 91
    :cond_3
    iget-object v0, p0, Louq;->c:Lorp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 62
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 23
    iget-object v0, p0, Louq;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 24
    const/4 v0, 0x1

    iget-object v1, p0, Louq;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 26
    :cond_0
    iget-object v0, p0, Louq;->b:Lorq;

    if-eqz v0, :cond_1

    .line 27
    const/4 v0, 0x2

    iget-object v1, p0, Louq;->b:Lorq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29
    :cond_1
    iget-object v0, p0, Louq;->c:Lorp;

    if-eqz v0, :cond_2

    .line 30
    const/4 v0, 0x4

    iget-object v1, p0, Louq;->c:Lorp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 32
    :cond_2
    iget-object v0, p0, Louq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 34
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6
    invoke-virtual {p0, p1}, Louq;->a(Loxn;)Louq;

    move-result-object v0

    return-object v0
.end method
