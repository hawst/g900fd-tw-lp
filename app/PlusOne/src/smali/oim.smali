.class public final Loim;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Loim;


# instance fields
.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field private e:Ljava/lang/Boolean;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1171
    const/4 v0, 0x0

    new-array v0, v0, [Loim;

    sput-object v0, Loim;->a:[Loim;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1172
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1211
    const/4 v0, 0x0

    .line 1212
    iget-object v1, p0, Loim;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1213
    const/4 v0, 0x1

    iget-object v1, p0, Loim;->b:Ljava/lang/Integer;

    .line 1214
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1216
    :cond_0
    iget-object v1, p0, Loim;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1217
    const/4 v1, 0x2

    iget-object v2, p0, Loim;->c:Ljava/lang/String;

    .line 1218
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1220
    :cond_1
    const/4 v1, 0x3

    iget-object v2, p0, Loim;->d:Ljava/lang/String;

    .line 1221
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1222
    iget-object v1, p0, Loim;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 1223
    const/4 v1, 0x4

    iget-object v2, p0, Loim;->e:Ljava/lang/Boolean;

    .line 1224
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1226
    :cond_2
    iget-object v1, p0, Loim;->f:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 1227
    const/4 v1, 0x5

    iget-object v2, p0, Loim;->f:Ljava/lang/String;

    .line 1228
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1230
    :cond_3
    iget-object v1, p0, Loim;->g:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 1231
    const/4 v1, 0x6

    iget-object v2, p0, Loim;->g:Ljava/lang/String;

    .line 1232
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1234
    :cond_4
    iget-object v1, p0, Loim;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1235
    iput v0, p0, Loim;->ai:I

    .line 1236
    return v0
.end method

.method public a(Loxn;)Loim;
    .locals 2

    .prologue
    .line 1244
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1245
    sparse-switch v0, :sswitch_data_0

    .line 1249
    iget-object v1, p0, Loim;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1250
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loim;->ah:Ljava/util/List;

    .line 1253
    :cond_1
    iget-object v1, p0, Loim;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1255
    :sswitch_0
    return-object p0

    .line 1260
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loim;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 1264
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loim;->c:Ljava/lang/String;

    goto :goto_0

    .line 1268
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loim;->d:Ljava/lang/String;

    goto :goto_0

    .line 1272
    :sswitch_4
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Loim;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 1276
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loim;->f:Ljava/lang/String;

    goto :goto_0

    .line 1280
    :sswitch_6
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loim;->g:Ljava/lang/String;

    goto :goto_0

    .line 1245
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1189
    iget-object v0, p0, Loim;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1190
    const/4 v0, 0x1

    iget-object v1, p0, Loim;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1192
    :cond_0
    iget-object v0, p0, Loim;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1193
    const/4 v0, 0x2

    iget-object v1, p0, Loim;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1195
    :cond_1
    const/4 v0, 0x3

    iget-object v1, p0, Loim;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1196
    iget-object v0, p0, Loim;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 1197
    const/4 v0, 0x4

    iget-object v1, p0, Loim;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 1199
    :cond_2
    iget-object v0, p0, Loim;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 1200
    const/4 v0, 0x5

    iget-object v1, p0, Loim;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1202
    :cond_3
    iget-object v0, p0, Loim;->g:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 1203
    const/4 v0, 0x6

    iget-object v1, p0, Loim;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 1205
    :cond_4
    iget-object v0, p0, Loim;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1207
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 1168
    invoke-virtual {p0, p1}, Loim;->a(Loxn;)Loim;

    move-result-object v0

    return-object v0
.end method
