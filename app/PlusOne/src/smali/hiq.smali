.class public final Lhiq;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lhiq;->a:Landroid/os/Bundle;

    .line 39
    return-void
.end method


# virtual methods
.method public a(Z)Lhiq;
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lhiq;->a:Landroid/os/Bundle;

    const-string v1, "DomainRestrictionToggleMixin.RESTRICT_TO_DOMAIN"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 46
    return-object p0
.end method

.method public b(Z)Lhiq;
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lhiq;->a:Landroid/os/Bundle;

    const-string v1, "DomainRestrictionToggleMixin.DISABLE_DOMAIN_RESTRICTION_TOGGLE"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 54
    return-object p0
.end method
