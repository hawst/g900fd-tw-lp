.class public Lcvl;
.super Llol;
.source "PG"


# static fields
.field private static final N:Ldgu;

.field private static final O:Ldgu;


# instance fields
.field private P:Ldgr;

.field private Q:Lcvh;

.field private R:Ldeo;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    new-instance v0, Ldgq;

    const v1, 0x7f1006c4

    invoke-direct {v0, v1}, Ldgq;-><init>(I)V

    sput-object v0, Lcvl;->N:Ldgu;

    .line 22
    new-instance v0, Ldgq;

    const v1, 0x7f1006c5

    invoke-direct {v0, v1}, Ldgq;-><init>(I)V

    sput-object v0, Lcvl;->O:Ldgu;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 20
    invoke-direct {p0}, Llol;-><init>()V

    .line 30
    new-instance v0, Ldgn;

    iget-object v1, p0, Lcvl;->av:Llqm;

    sget-object v2, Ldgx;->Q:Ldgo;

    new-instance v3, Lcvm;

    invoke-direct {v3, p0}, Lcvm;-><init>(Lcvl;)V

    invoke-direct {v0, v1, v2, v3}, Ldgn;-><init>(Llqr;Ldgu;Ldgv;)V

    .line 37
    new-instance v0, Ldgn;

    iget-object v1, p0, Lcvl;->av:Llqm;

    sget-object v2, Lcvl;->N:Ldgu;

    new-instance v3, Lcvn;

    invoke-direct {v3, p0}, Lcvn;-><init>(Lcvl;)V

    invoke-direct {v0, v1, v2, v3}, Ldgn;-><init>(Llqr;Ldgu;Ldgv;)V

    .line 45
    new-instance v0, Ldgn;

    iget-object v1, p0, Lcvl;->av:Llqm;

    sget-object v2, Lcvl;->O:Ldgu;

    new-instance v3, Lcvo;

    invoke-direct {v3, p0}, Lcvo;-><init>(Lcvl;)V

    invoke-direct {v0, v1, v2, v3}, Ldgn;-><init>(Llqr;Ldgu;Ldgv;)V

    .line 53
    new-instance v0, Ldep;

    iget-object v1, p0, Lcvl;->av:Llqm;

    new-instance v2, Lcvp;

    invoke-direct {v2, p0}, Lcvp;-><init>(Lcvl;)V

    invoke-direct {v0, v1, v2}, Ldep;-><init>(Llqr;Ldeq;)V

    .line 59
    return-void
.end method

.method static synthetic a(Lcvl;)Ldeo;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcvl;->R:Ldeo;

    return-object v0
.end method

.method static synthetic b(Lcvl;)Lcvh;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcvl;->Q:Lcvh;

    return-object v0
.end method

.method static synthetic c(Lcvl;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 20
    iget-object v0, p0, Lcvl;->P:Ldgr;

    sget-object v1, Ldgx;->Q:Ldgo;

    invoke-virtual {v0, v1, v2}, Ldgr;->a(Ldgu;Z)V

    iget-object v0, p0, Lcvl;->P:Ldgr;

    sget-object v1, Lcvl;->N:Ldgu;

    invoke-virtual {v0, v1, v2}, Ldgr;->a(Ldgu;Z)V

    iget-object v0, p0, Lcvl;->P:Ldgr;

    sget-object v1, Lcvl;->O:Ldgu;

    invoke-virtual {v0, v1, v2}, Ldgr;->a(Ldgu;Z)V

    iget-object v0, p0, Lcvl;->R:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->I()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcvl;->P:Ldgr;

    sget-object v1, Lcvl;->N:Ldgu;

    invoke-virtual {v0, v1, v3}, Ldgr;->a(Ldgu;Z)V

    iget-object v0, p0, Lcvl;->P:Ldgr;

    sget-object v1, Lcvl;->O:Ldgu;

    invoke-virtual {v0, v1, v3}, Ldgr;->a(Ldgu;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcvl;->R:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->u()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcvl;->P:Ldgr;

    sget-object v1, Ldgx;->Q:Ldgo;

    invoke-virtual {v0, v1, v3}, Ldgr;->a(Ldgu;Z)V

    goto :goto_0
.end method


# virtual methods
.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 63
    invoke-super {p0, p1}, Llol;->c(Landroid/os/Bundle;)V

    .line 64
    iget-object v0, p0, Lcvl;->au:Llnh;

    const-class v1, Ldgr;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgr;

    iput-object v0, p0, Lcvl;->P:Ldgr;

    .line 65
    iget-object v0, p0, Lcvl;->au:Llnh;

    const-class v1, Lcvh;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcvh;

    iput-object v0, p0, Lcvl;->Q:Lcvh;

    .line 66
    iget-object v0, p0, Lcvl;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lcvl;->R:Ldeo;

    .line 67
    return-void
.end method
