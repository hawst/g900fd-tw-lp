.class public final Lmms;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Lmmm;

.field public e:Lmmd;

.field public f:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/high16 v0, -0x80000000

    .line 2558
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2592
    iput v0, p0, Lmms;->a:I

    .line 2599
    iput-object v1, p0, Lmms;->d:Lmmm;

    .line 2602
    iput-object v1, p0, Lmms;->e:Lmmd;

    .line 2605
    iput v0, p0, Lmms;->f:I

    .line 2558
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 2634
    const/4 v0, 0x0

    .line 2635
    iget v1, p0, Lmms;->a:I

    if-eq v1, v3, :cond_0

    .line 2636
    const/4 v0, 0x1

    iget v1, p0, Lmms;->a:I

    .line 2637
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2639
    :cond_0
    iget-object v1, p0, Lmms;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 2640
    const/4 v1, 0x2

    iget-object v2, p0, Lmms;->b:Ljava/lang/Integer;

    .line 2641
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2643
    :cond_1
    iget-object v1, p0, Lmms;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 2644
    const/4 v1, 0x3

    iget-object v2, p0, Lmms;->c:Ljava/lang/Integer;

    .line 2645
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2647
    :cond_2
    iget-object v1, p0, Lmms;->d:Lmmm;

    if-eqz v1, :cond_3

    .line 2648
    const/4 v1, 0x4

    iget-object v2, p0, Lmms;->d:Lmmm;

    .line 2649
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2651
    :cond_3
    iget-object v1, p0, Lmms;->e:Lmmd;

    if-eqz v1, :cond_4

    .line 2652
    const/4 v1, 0x5

    iget-object v2, p0, Lmms;->e:Lmmd;

    .line 2653
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2655
    :cond_4
    iget v1, p0, Lmms;->f:I

    if-eq v1, v3, :cond_5

    .line 2656
    const/4 v1, 0x6

    iget v2, p0, Lmms;->f:I

    .line 2657
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2659
    :cond_5
    iget-object v1, p0, Lmms;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2660
    iput v0, p0, Lmms;->ai:I

    .line 2661
    return v0
.end method

.method public a(Loxn;)Lmms;
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2669
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2670
    sparse-switch v0, :sswitch_data_0

    .line 2674
    iget-object v1, p0, Lmms;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2675
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmms;->ah:Ljava/util/List;

    .line 2678
    :cond_1
    iget-object v1, p0, Lmms;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2680
    :sswitch_0
    return-object p0

    .line 2685
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2686
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    if-eq v0, v5, :cond_2

    if-eq v0, v6, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-ne v0, v1, :cond_3

    .line 2693
    :cond_2
    iput v0, p0, Lmms;->a:I

    goto :goto_0

    .line 2695
    :cond_3
    iput v2, p0, Lmms;->a:I

    goto :goto_0

    .line 2700
    :sswitch_2
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmms;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 2704
    :sswitch_3
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lmms;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 2708
    :sswitch_4
    iget-object v0, p0, Lmms;->d:Lmmm;

    if-nez v0, :cond_4

    .line 2709
    new-instance v0, Lmmm;

    invoke-direct {v0}, Lmmm;-><init>()V

    iput-object v0, p0, Lmms;->d:Lmmm;

    .line 2711
    :cond_4
    iget-object v0, p0, Lmms;->d:Lmmm;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2715
    :sswitch_5
    iget-object v0, p0, Lmms;->e:Lmmd;

    if-nez v0, :cond_5

    .line 2716
    new-instance v0, Lmmd;

    invoke-direct {v0}, Lmmd;-><init>()V

    iput-object v0, p0, Lmms;->e:Lmmd;

    .line 2718
    :cond_5
    iget-object v0, p0, Lmms;->e:Lmmd;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2722
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2723
    if-eqz v0, :cond_6

    if-eq v0, v3, :cond_6

    if-eq v0, v4, :cond_6

    if-eq v0, v5, :cond_6

    if-eq v0, v6, :cond_6

    const/4 v1, 0x5

    if-eq v0, v1, :cond_6

    const/4 v1, 0x6

    if-eq v0, v1, :cond_6

    const/4 v1, 0x7

    if-eq v0, v1, :cond_6

    const/16 v1, 0x8

    if-eq v0, v1, :cond_6

    const/16 v1, 0x9

    if-eq v0, v1, :cond_6

    const/16 v1, 0xa

    if-eq v0, v1, :cond_6

    const/16 v1, 0xb

    if-eq v0, v1, :cond_6

    const/16 v1, 0xc

    if-eq v0, v1, :cond_6

    const/16 v1, 0xd

    if-eq v0, v1, :cond_6

    const/16 v1, 0xe

    if-eq v0, v1, :cond_6

    const/16 v1, 0xf

    if-eq v0, v1, :cond_6

    const/16 v1, 0x10

    if-eq v0, v1, :cond_6

    const/16 v1, 0x11

    if-ne v0, v1, :cond_7

    .line 2741
    :cond_6
    iput v0, p0, Lmms;->f:I

    goto/16 :goto_0

    .line 2743
    :cond_7
    iput v2, p0, Lmms;->f:I

    goto/16 :goto_0

    .line 2670
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 2610
    iget v0, p0, Lmms;->a:I

    if-eq v0, v2, :cond_0

    .line 2611
    const/4 v0, 0x1

    iget v1, p0, Lmms;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2613
    :cond_0
    iget-object v0, p0, Lmms;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2614
    const/4 v0, 0x2

    iget-object v1, p0, Lmms;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->c(II)V

    .line 2616
    :cond_1
    iget-object v0, p0, Lmms;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2617
    const/4 v0, 0x3

    iget-object v1, p0, Lmms;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->c(II)V

    .line 2619
    :cond_2
    iget-object v0, p0, Lmms;->d:Lmmm;

    if-eqz v0, :cond_3

    .line 2620
    const/4 v0, 0x4

    iget-object v1, p0, Lmms;->d:Lmmm;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2622
    :cond_3
    iget-object v0, p0, Lmms;->e:Lmmd;

    if-eqz v0, :cond_4

    .line 2623
    const/4 v0, 0x5

    iget-object v1, p0, Lmms;->e:Lmmd;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2625
    :cond_4
    iget v0, p0, Lmms;->f:I

    if-eq v0, v2, :cond_5

    .line 2626
    const/4 v0, 0x6

    iget v1, p0, Lmms;->f:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2628
    :cond_5
    iget-object v0, p0, Lmms;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2630
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2554
    invoke-virtual {p0, p1}, Lmms;->a(Loxn;)Lmms;

    move-result-object v0

    return-object v0
.end method
