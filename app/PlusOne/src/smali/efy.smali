.class public final Lefy;
.super Legi;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lbc;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Legi;",
        "Landroid/view/View$OnClickListener;",
        "Lbc",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private N:Lfbl;

.field private ah:Z

.field private ai:Ljava/lang/Integer;

.field private aj:I

.field private ak:Z

.field private al:Ljava/lang/String;

.field private final am:Licq;

.field private final an:Lfhh;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Legi;-><init>()V

    .line 68
    new-instance v0, Licq;

    iget-object v1, p0, Lefy;->av:Llqm;

    invoke-direct {v0, v1}, Licq;-><init>(Llqr;)V

    const v1, 0x7f0a064c

    .line 69
    invoke-virtual {v0, v1}, Licq;->b(I)Licq;

    move-result-object v0

    iput-object v0, p0, Lefy;->am:Licq;

    .line 72
    new-instance v0, Lhnw;

    new-instance v1, Lega;

    invoke-direct {v1, p0}, Lega;-><init>(Lefy;)V

    invoke-direct {v0, p0, v1}, Lhnw;-><init>(Llrn;Lhnv;)V

    .line 83
    new-instance v0, Lefz;

    invoke-direct {v0, p0}, Lefz;-><init>(Lefy;)V

    iput-object v0, p0, Lefy;->an:Lfhh;

    .line 365
    return-void
.end method

.method static synthetic a(Lefy;I)I
    .locals 0

    .prologue
    .line 52
    iput p1, p0, Lefy;->aj:I

    return p1
.end method

.method static synthetic a(Lefy;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 52
    iput-object p1, p0, Lefy;->al:Ljava/lang/String;

    return-object p1
.end method

.method private a(ILfib;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 350
    iget-object v0, p0, Lefy;->ai:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lefy;->ai:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 362
    :cond_0
    :goto_0
    return-void

    .line 353
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lefy;->ai:Ljava/lang/Integer;

    .line 355
    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lfib;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lefy;->Z:Z

    .line 356
    iget-boolean v0, p0, Lefy;->Z:Z

    if-eqz v0, :cond_2

    .line 357
    invoke-virtual {p0}, Lefy;->o()Landroid/content/res/Resources;

    move-result-object v0

    .line 358
    const v2, 0x7f0a07ac

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 359
    invoke-virtual {p0}, Lefy;->n()Lz;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 361
    :cond_2
    invoke-virtual {p0}, Lefy;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lefy;->d(Landroid/view/View;)V

    goto :goto_0

    :cond_3
    move v0, v1

    .line 355
    goto :goto_1
.end method

.method static synthetic a(Lefy;ILfib;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lefy;->a(ILfib;)V

    return-void
.end method

.method static synthetic a(Lefy;Z)Z
    .locals 0

    .prologue
    .line 52
    iput-boolean p1, p0, Lefy;->ak:Z

    return p1
.end method

.method private d(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 332
    if-nez p1, :cond_0

    .line 347
    :goto_0
    return-void

    .line 336
    :cond_0
    invoke-virtual {p0}, Lefy;->U()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 337
    iget-boolean v0, p0, Lefy;->ah:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lefy;->ai:Ljava/lang/Integer;

    if-nez v0, :cond_1

    .line 338
    iget-object v0, p0, Lefy;->am:Licq;

    sget-object v1, Lict;->c:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 345
    :goto_1
    invoke-virtual {p0}, Lefy;->Z_()V

    .line 346
    invoke-virtual {p0}, Lefy;->ag()V

    goto :goto_0

    .line 340
    :cond_1
    iget-object v0, p0, Lefy;->am:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_1

    .line 343
    :cond_2
    iget-object v0, p0, Lefy;->am:Licq;

    sget-object v1, Lict;->b:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    goto :goto_1
.end method


# virtual methods
.method public F_()Lhmw;
    .locals 1

    .prologue
    .line 225
    sget-object v0, Lhmw;->at:Lhmw;

    return-object v0
.end method

.method public J_()Z
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lefy;->ai:Ljava/lang/Integer;

    if-nez v0, :cond_0

    invoke-super {p0}, Legi;->J_()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public L_()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 230
    invoke-super {p0}, Legi;->L_()V

    .line 232
    iget-object v0, p0, Lefy;->ai:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 248
    :goto_0
    return-void

    .line 236
    :cond_0
    iput-boolean v2, p0, Lefy;->Z:Z

    .line 237
    iget-object v0, p0, Lefy;->al:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v2, v2, [Ljava/lang/String;

    invoke-static {v1, v2}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 238
    invoke-virtual {p0}, Lefy;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Lefy;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    invoke-static {v0, v1, v4}, Lcom/google/android/apps/plus/service/EsService;->f(Landroid/content/Context;ILjava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lefy;->ai:Ljava/lang/Integer;

    .line 245
    :goto_1
    invoke-virtual {p0}, Lefy;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lefy;->d(Landroid/view/View;)V

    .line 246
    iget-object v0, p0, Lefy;->au:Llnh;

    const-class v1, Lhms;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v1, Lhmr;

    iget-object v2, p0, Lefy;->at:Llnl;

    invoke-direct {v1, v2}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v2, Lhmv;->ek:Lhmv;

    .line 247
    invoke-virtual {v1, v2}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v1

    .line 246
    invoke-interface {v0, v1}, Lhms;->a(Lhmr;)V

    goto :goto_0

    .line 241
    :cond_1
    invoke-virtual {p0}, Lefy;->n()Lz;

    move-result-object v0

    iget-object v1, p0, Lefy;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    iget-object v2, p0, Lefy;->P:Lhee;

    .line 242
    invoke-interface {v2}, Lhee;->g()Lhej;

    move-result-object v2

    const-string v3, "gaia_id"

    invoke-interface {v2, v3}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lefy;->al:Ljava/lang/String;

    .line 241
    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/apps/plus/service/EsService;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lefy;->ai:Ljava/lang/Integer;

    goto :goto_1
.end method

.method protected U()Z
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lefy;->N:Lfbl;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 215
    :goto_0
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 214
    :cond_1
    iget-object v0, p0, Lefy;->N:Lfbl;

    invoke-virtual {v0}, Lfbl;->a()Landroid/database/Cursor;

    move-result-object v0

    goto :goto_0

    .line 215
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public V()Z
    .locals 2

    .prologue
    .line 393
    invoke-virtual {p0}, Lefy;->n()Lz;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lz;->setResult(I)V

    .line 394
    invoke-super {p0}, Legi;->V()Z

    move-result v0

    return v0
.end method

.method protected Y_()Z
    .locals 3

    .prologue
    .line 404
    invoke-virtual {p0}, Lefy;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "external"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public Z()Z
    .locals 1

    .prologue
    .line 399
    const/4 v0, 0x0

    return v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 124
    const v0, 0x7f0400de

    invoke-super {p0, p1, p2, p3, v0}, Legi;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;I)Landroid/view/View;

    move-result-object v1

    .line 126
    iget-object v0, p0, Lefy;->at:Llnl;

    invoke-virtual {v0}, Llnl;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0d0291

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    .line 129
    new-instance v0, Lfbl;

    iget-object v3, p0, Lefy;->at:Llnl;

    iget-object v4, p0, Lefy;->P:Lhee;

    .line 130
    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    iget-object v5, p0, Lefy;->al:Ljava/lang/String;

    invoke-direct {v0, v3, v6, v4, v5}, Lfbl;-><init>(Landroid/content/Context;Landroid/database/Cursor;ILjava/lang/String;)V

    iput-object v0, p0, Lefy;->N:Lfbl;

    .line 131
    iget-object v0, p0, Lefy;->N:Lfbl;

    invoke-virtual {v0, p0}, Lfbl;->a(Landroid/view/View$OnClickListener;)V

    .line 133
    const v0, 0x7f100304

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;

    .line 134
    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(I)V

    .line 135
    new-instance v2, Ljvl;

    iget-object v3, p0, Lefy;->at:Llnl;

    invoke-direct {v2, v3}, Ljvl;-><init>(Landroid/content/Context;)V

    iget v2, v2, Ljvl;->a:I

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(I)V

    .line 136
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->b(Z)V

    .line 137
    new-instance v2, Legb;

    invoke-direct {v2}, Legb;-><init>()V

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Llkg;)V

    .line 138
    iget-object v2, p0, Lefy;->N:Lfbl;

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->a(Landroid/widget/ListAdapter;)V

    .line 139
    const v2, 0x7f020415

    invoke-virtual {v0, v2}, Lcom/google/android/libraries/social/ui/views/columngridview/ColumnGridView;->g(I)V

    .line 141
    invoke-virtual {p0}, Lefy;->w()Lbb;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v6, p0}, Lbb;->a(ILandroid/os/Bundle;Lbc;)Ldo;

    .line 143
    invoke-direct {p0, v1}, Lefy;->d(Landroid/view/View;)V

    .line 144
    return-object v1
.end method

.method public a(ILandroid/os/Bundle;)Ldo;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 313
    new-instance v0, Lfbn;

    iget-object v1, p0, Lefy;->at:Llnl;

    iget-object v2, p0, Lefy;->P:Lhee;

    .line 314
    invoke-interface {v2}, Lhee;->d()I

    move-result v2

    iget-object v3, p0, Lefy;->al:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lfbn;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/List;)V

    return-object v0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 300
    if-eq p1, v0, :cond_0

    const/4 v1, 0x2

    if-ne p1, v1, :cond_1

    :cond_0
    if-eqz p2, :cond_1

    .line 303
    :goto_0
    if-eqz v0, :cond_2

    .line 304
    invoke-virtual {p0}, Lefy;->n()Lz;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lz;->setResult(ILandroid/content/Intent;)V

    .line 305
    invoke-virtual {p0}, Lefy;->n()Lz;

    move-result-object v0

    invoke-virtual {v0}, Lz;->finish()V

    .line 309
    :goto_1
    return-void

    .line 300
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 307
    :cond_2
    invoke-super {p0, p1, p2, p3}, Legi;->a(IILandroid/content/Intent;)V

    goto :goto_1
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 319
    const/4 v0, 0x1

    iput-boolean v0, p0, Lefy;->ah:Z

    .line 320
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 321
    :cond_0
    invoke-virtual {p0}, Lefy;->L_()V

    .line 323
    :cond_1
    iget-object v0, p0, Lefy;->N:Lfbl;

    invoke-virtual {v0, p1}, Lfbl;->b(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 324
    invoke-virtual {p0}, Lefy;->x()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lefy;->d(Landroid/view/View;)V

    .line 325
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 99
    invoke-super {p0, p1}, Legi;->a(Landroid/os/Bundle;)V

    .line 101
    if-eqz p1, :cond_2

    .line 102
    const-string v0, "refresh_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    const-string v0, "refresh_request"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lefy;->ai:Ljava/lang/Integer;

    .line 116
    :cond_0
    :goto_0
    iget-object v0, p0, Lefy;->al:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117
    const/4 v0, 0x1

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-static {v0, v1}, Ljvj;->a(I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lefy;->al:Ljava/lang/String;

    .line 119
    :cond_1
    return-void

    .line 106
    :cond_2
    invoke-virtual {p0}, Lefy;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "destination"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {p0}, Lefy;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "destination"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 109
    :pswitch_0
    invoke-virtual {p0}, Lefy;->ap()V

    goto :goto_0

    .line 107
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Ldo;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 329
    return-void
.end method

.method public bridge synthetic a(Ldo;Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 52
    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p2}, Lefy;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public a(Loo;)V
    .locals 1

    .prologue
    .line 194
    invoke-super {p0, p1}, Legi;->a(Loo;)V

    .line 195
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Loo;->c(Z)V

    .line 196
    return-void
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 156
    invoke-super {p0}, Legi;->aO_()V

    .line 158
    iget-object v0, p0, Lefy;->an:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(Lfhh;)V

    .line 160
    iget-object v0, p0, Lefy;->ai:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 161
    iget-object v0, p0, Lefy;->ai:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 162
    invoke-virtual {p0}, Lefy;->U()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 163
    iget-object v0, p0, Lefy;->am:Licq;

    sget-object v1, Lict;->a:Lict;

    invoke-virtual {v0, v1}, Licq;->a(Lict;)V

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    iget-object v0, p0, Lefy;->ai:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(I)Lfib;

    move-result-object v0

    .line 167
    iget-object v1, p0, Lefy;->ai:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lefy;->a(ILfib;)V

    goto :goto_0
.end method

.method protected b(Lhjk;)V
    .locals 2

    .prologue
    .line 200
    invoke-super {p0, p1}, Legi;->b(Lhjk;)V

    .line 202
    const v0, 0x7f10067b

    .line 203
    invoke-interface {p1, v0}, Lhjk;->a(I)Lhjc;

    move-result-object v0

    check-cast v0, Lhjv;

    .line 204
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhjv;->a(I)V

    .line 206
    const v0, 0x7f0a0ab2

    invoke-interface {p1, v0}, Lhjk;->d(I)V

    .line 207
    iget-boolean v0, p0, Lefy;->ak:Z

    if-eqz v0, :cond_0

    .line 208
    invoke-virtual {p0, p1}, Lefy;->c(Lhjk;)V

    .line 210
    :cond_0
    return-void
.end method

.method protected b_(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 374
    invoke-super {p0, p1}, Legi;->b_(Ljava/lang/String;)V

    .line 375
    iget v0, p0, Lefy;->aj:I

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 376
    iget-object v0, p0, Lefy;->at:Llnl;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Ljac;->a:Ljac;

    invoke-static {v0, v1, v2}, Lizu;->a(Landroid/content/Context;Landroid/net/Uri;Ljac;)Lizu;

    move-result-object v0

    .line 378
    iget-object v1, p0, Lefy;->P:Lhee;

    invoke-interface {v1}, Lhee;->d()I

    move-result v1

    .line 379
    iget-object v2, p0, Lefy;->at:Llnl;

    invoke-static {v2, v1}, Leyq;->h(Landroid/content/Context;I)Leyx;

    move-result-object v1

    .line 381
    invoke-virtual {v1, v0}, Leyx;->a(Lizu;)Leyx;

    move-result-object v0

    iget v1, p0, Lefy;->aj:I

    .line 382
    invoke-virtual {v0, v1}, Leyx;->a(I)Leyx;

    move-result-object v0

    .line 383
    invoke-virtual {v0}, Leyx;->a()Landroid/content/Intent;

    move-result-object v0

    .line 384
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lefy;->a(Landroid/content/Intent;I)V

    .line 386
    :cond_0
    return-void
.end method

.method public c(Landroid/view/View;)Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 252
    const v0, 0x7f100079

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 253
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v2

    .line 295
    :goto_0
    return v0

    .line 257
    :cond_0
    const v1, 0x7f10007a

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 259
    iget-object v4, p0, Lefy;->P:Lhee;

    invoke-interface {v4}, Lhee;->d()I

    move-result v4

    .line 260
    if-nez v1, :cond_2

    .line 261
    check-cast p1, Lcom/google/android/apps/plus/views/PhotoTileView;

    .line 262
    iget-object v1, p0, Lefy;->au:Llnh;

    const-class v5, Lhms;

    invoke-virtual {v1, v5}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhms;

    new-instance v5, Lhmr;

    iget-object v6, p0, Lefy;->at:Llnl;

    invoke-direct {v5, v6}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v6, Lhmv;->dZ:Lhmv;

    .line 263
    invoke-virtual {v5, v6}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v5

    .line 262
    invoke-interface {v1, v5}, Lhms;->a(Lhmr;)V

    .line 265
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PhotoTileView;->l()Lizu;

    move-result-object v1

    invoke-virtual {p0, v1}, Lefy;->a(Lizu;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 266
    invoke-virtual {p0}, Lefy;->n()Lz;

    move-result-object v1

    invoke-static {v1, v4}, Leyq;->h(Landroid/content/Context;I)Leyx;

    move-result-object v1

    .line 267
    invoke-virtual {p1}, Lcom/google/android/apps/plus/views/PhotoTileView;->l()Lizu;

    move-result-object v4

    invoke-virtual {v1, v4}, Leyx;->a(Lizu;)Leyx;

    move-result-object v1

    iget-object v4, p0, Lefy;->al:Ljava/lang/String;

    .line 268
    invoke-virtual {v1, v4}, Leyx;->a(Ljava/lang/String;)Leyx;

    move-result-object v1

    .line 269
    invoke-virtual {v1, v0}, Leyx;->b(Ljava/lang/String;)Leyx;

    move-result-object v0

    iget v1, p0, Lefy;->aj:I

    .line 270
    invoke-virtual {v0, v1}, Leyx;->a(I)Leyx;

    move-result-object v0

    .line 271
    invoke-virtual {p0}, Lefy;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v4, "photo_min_width"

    invoke-virtual {v1, v4, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Leyx;->b(I)Leyx;

    move-result-object v0

    .line 272
    invoke-virtual {p0}, Lefy;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v4, "photo_min_height"

    invoke-virtual {v1, v4, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Leyx;->c(I)Leyx;

    move-result-object v0

    .line 273
    invoke-virtual {v0}, Leyx;->a()Landroid/content/Intent;

    move-result-object v0

    .line 274
    invoke-virtual {p0, v0, v3}, Lefy;->a(Landroid/content/Intent;I)V

    :cond_1
    :goto_1
    move v0, v3

    .line 295
    goto :goto_0

    .line 276
    :cond_2
    if-ne v1, v3, :cond_3

    .line 277
    iget-object v0, p0, Lefy;->at:Llnl;

    .line 278
    invoke-static {}, Ljvj;->a()Ljava/lang/String;

    move-result-object v1

    .line 277
    invoke-static {v0, v4, v1}, Leyq;->b(Landroid/content/Context;ILjava/lang/String;)Ljuj;

    move-result-object v0

    .line 279
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljuj;->a(Ljava/lang/Integer;)Ljuj;

    move-result-object v0

    iget v1, p0, Lefy;->aj:I

    .line 280
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljuj;->c(Ljava/lang/Integer;)Ljuj;

    move-result-object v0

    iget v1, p0, Lefy;->aa:I

    .line 281
    invoke-virtual {v0, v1}, Ljuj;->e(I)Ljuj;

    move-result-object v0

    iget-boolean v1, p0, Lefy;->ac:Z

    .line 282
    invoke-virtual {v0, v1}, Ljuj;->b(Z)Ljuj;

    move-result-object v0

    iget-boolean v1, p0, Lefy;->ae:Z

    .line 283
    invoke-virtual {v0, v1}, Ljuj;->c(Z)Ljuj;

    move-result-object v0

    iget-object v1, p0, Lefy;->af:Ljava/lang/String;

    .line 284
    invoke-virtual {v0, v1}, Ljuj;->c(Ljava/lang/String;)Ljuj;

    move-result-object v0

    .line 285
    invoke-virtual {p0}, Lefy;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v4, "external"

    invoke-virtual {v1, v4, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljuj;->a(Z)Ljuj;

    move-result-object v0

    .line 286
    invoke-virtual {p0}, Lefy;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v4, "photo_min_width"

    invoke-virtual {v1, v4, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljuj;->a(I)Ljuj;

    move-result-object v0

    .line 287
    invoke-virtual {p0}, Lefy;->k()Landroid/os/Bundle;

    move-result-object v1

    const-string v4, "photo_min_height"

    invoke-virtual {v1, v4, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljuj;->b(I)Ljuj;

    move-result-object v0

    .line 288
    invoke-virtual {v0}, Ljuj;->a()Landroid/content/Intent;

    move-result-object v1

    .line 289
    iget-object v0, p0, Lefy;->au:Llnh;

    const-class v2, Lhms;

    invoke-virtual {v0, v2}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhms;

    new-instance v2, Lhmr;

    iget-object v4, p0, Lefy;->at:Llnl;

    invoke-direct {v2, v4}, Lhmr;-><init>(Landroid/content/Context;)V

    sget-object v4, Lhmv;->ea:Lhmv;

    .line 290
    invoke-virtual {v2, v4}, Lhmr;->a(Lhmv;)Lhmr;

    move-result-object v2

    .line 289
    invoke-interface {v0, v2}, Lhms;->a(Lhmr;)V

    .line 291
    const/4 v0, 0x2

    invoke-virtual {p0, v1, v0}, Lefy;->a(Landroid/content/Intent;I)V

    goto/16 :goto_1

    .line 293
    :cond_3
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const/16 v3, 0x25

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Whoa! We got a tile type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 186
    invoke-super {p0, p1}, Legi;->e(Landroid/os/Bundle;)V

    .line 187
    iget-object v0, p0, Lefy;->ai:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 188
    const-string v0, "refresh_request"

    iget-object v1, p0, Lefy;->ai:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 190
    :cond_0
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 174
    invoke-super {p0}, Legi;->g()V

    .line 175
    invoke-virtual {p0}, Lefy;->x()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Llii;->c(Landroid/view/View;)V

    .line 176
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 180
    invoke-super {p0}, Legi;->h()V

    .line 181
    invoke-virtual {p0}, Lefy;->x()Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Llii;->d(Landroid/view/View;)V

    .line 182
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 149
    invoke-super {p0}, Legi;->z()V

    .line 151
    iget-object v0, p0, Lefy;->an:Lfhh;

    invoke-static {v0}, Lcom/google/android/apps/plus/service/EsService;->b(Lfhh;)V

    .line 152
    return-void
.end method
