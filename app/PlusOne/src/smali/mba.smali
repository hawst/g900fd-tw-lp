.class public final Lmba;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmxt;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29887
    invoke-direct {p0}, Loxq;-><init>()V

    .line 29890
    iput-object v0, p0, Lmba;->apiHeader:Llyq;

    .line 29893
    iput-object v0, p0, Lmba;->a:Lmxt;

    .line 29887
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 29910
    const/4 v0, 0x0

    .line 29911
    iget-object v1, p0, Lmba;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 29912
    const/4 v0, 0x1

    iget-object v1, p0, Lmba;->apiHeader:Llyq;

    .line 29913
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 29915
    :cond_0
    iget-object v1, p0, Lmba;->a:Lmxt;

    if-eqz v1, :cond_1

    .line 29916
    const/4 v1, 0x2

    iget-object v2, p0, Lmba;->a:Lmxt;

    .line 29917
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29919
    :cond_1
    iget-object v1, p0, Lmba;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 29920
    iput v0, p0, Lmba;->ai:I

    .line 29921
    return v0
.end method

.method public a(Loxn;)Lmba;
    .locals 2

    .prologue
    .line 29929
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 29930
    sparse-switch v0, :sswitch_data_0

    .line 29934
    iget-object v1, p0, Lmba;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 29935
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmba;->ah:Ljava/util/List;

    .line 29938
    :cond_1
    iget-object v1, p0, Lmba;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 29940
    :sswitch_0
    return-object p0

    .line 29945
    :sswitch_1
    iget-object v0, p0, Lmba;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 29946
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmba;->apiHeader:Llyq;

    .line 29948
    :cond_2
    iget-object v0, p0, Lmba;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 29952
    :sswitch_2
    iget-object v0, p0, Lmba;->a:Lmxt;

    if-nez v0, :cond_3

    .line 29953
    new-instance v0, Lmxt;

    invoke-direct {v0}, Lmxt;-><init>()V

    iput-object v0, p0, Lmba;->a:Lmxt;

    .line 29955
    :cond_3
    iget-object v0, p0, Lmba;->a:Lmxt;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 29930
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 29898
    iget-object v0, p0, Lmba;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 29899
    const/4 v0, 0x1

    iget-object v1, p0, Lmba;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29901
    :cond_0
    iget-object v0, p0, Lmba;->a:Lmxt;

    if-eqz v0, :cond_1

    .line 29902
    const/4 v0, 0x2

    iget-object v1, p0, Lmba;->a:Lmxt;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 29904
    :cond_1
    iget-object v0, p0, Lmba;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 29906
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 29883
    invoke-virtual {p0, p1}, Lmba;->a(Loxn;)Lmba;

    move-result-object v0

    return-object v0
.end method
