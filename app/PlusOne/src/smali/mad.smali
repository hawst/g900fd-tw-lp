.class public final Lmad;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnvz;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6964
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6967
    iput-object v0, p0, Lmad;->apiHeader:Llyr;

    .line 6970
    iput-object v0, p0, Lmad;->a:Lnvz;

    .line 6964
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6987
    const/4 v0, 0x0

    .line 6988
    iget-object v1, p0, Lmad;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 6989
    const/4 v0, 0x1

    iget-object v1, p0, Lmad;->apiHeader:Llyr;

    .line 6990
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6992
    :cond_0
    iget-object v1, p0, Lmad;->a:Lnvz;

    if-eqz v1, :cond_1

    .line 6993
    const/4 v1, 0x2

    iget-object v2, p0, Lmad;->a:Lnvz;

    .line 6994
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6996
    :cond_1
    iget-object v1, p0, Lmad;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6997
    iput v0, p0, Lmad;->ai:I

    .line 6998
    return v0
.end method

.method public a(Loxn;)Lmad;
    .locals 2

    .prologue
    .line 7006
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 7007
    sparse-switch v0, :sswitch_data_0

    .line 7011
    iget-object v1, p0, Lmad;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 7012
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmad;->ah:Ljava/util/List;

    .line 7015
    :cond_1
    iget-object v1, p0, Lmad;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 7017
    :sswitch_0
    return-object p0

    .line 7022
    :sswitch_1
    iget-object v0, p0, Lmad;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 7023
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmad;->apiHeader:Llyr;

    .line 7025
    :cond_2
    iget-object v0, p0, Lmad;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7029
    :sswitch_2
    iget-object v0, p0, Lmad;->a:Lnvz;

    if-nez v0, :cond_3

    .line 7030
    new-instance v0, Lnvz;

    invoke-direct {v0}, Lnvz;-><init>()V

    iput-object v0, p0, Lmad;->a:Lnvz;

    .line 7032
    :cond_3
    iget-object v0, p0, Lmad;->a:Lnvz;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 7007
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6975
    iget-object v0, p0, Lmad;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 6976
    const/4 v0, 0x1

    iget-object v1, p0, Lmad;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6978
    :cond_0
    iget-object v0, p0, Lmad;->a:Lnvz;

    if-eqz v0, :cond_1

    .line 6979
    const/4 v0, 0x2

    iget-object v1, p0, Lmad;->a:Lnvz;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6981
    :cond_1
    iget-object v0, p0, Lmad;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6983
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6960
    invoke-virtual {p0, p1}, Lmad;->a(Loxn;)Lmad;

    move-result-object v0

    return-object v0
.end method
