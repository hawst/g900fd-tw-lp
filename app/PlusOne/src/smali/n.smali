.class final Ln;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field private synthetic a:Landroid/view/View;

.field private synthetic b:Ljava/lang/Object;

.field private synthetic c:Ljava/util/ArrayList;

.field private synthetic d:Lq;

.field private synthetic e:Z

.field private synthetic f:Lu;

.field private synthetic g:Lu;

.field private synthetic h:Ll;


# direct methods
.method constructor <init>(Ll;Landroid/view/View;Ljava/lang/Object;Ljava/util/ArrayList;Lq;ZLu;Lu;)V
    .locals 0

    .prologue
    .line 1234
    iput-object p1, p0, Ln;->h:Ll;

    iput-object p2, p0, Ln;->a:Landroid/view/View;

    iput-object p3, p0, Ln;->b:Ljava/lang/Object;

    iput-object p4, p0, Ln;->c:Ljava/util/ArrayList;

    iput-object p5, p0, Ln;->d:Lq;

    iput-boolean p6, p0, Ln;->e:Z

    iput-object p7, p0, Ln;->f:Lu;

    iput-object p8, p0, Ln;->g:Lu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 4

    .prologue
    .line 1237
    iget-object v0, p0, Ln;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 1239
    iget-object v0, p0, Ln;->b:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 1240
    iget-object v0, p0, Ln;->b:Ljava/lang/Object;

    iget-object v1, p0, Ln;->c:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Lau;->a(Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 1242
    iget-object v0, p0, Ln;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1244
    iget-object v0, p0, Ln;->h:Ll;

    iget-object v1, p0, Ln;->d:Lq;

    iget-boolean v2, p0, Ln;->e:Z

    iget-object v3, p0, Ln;->f:Lu;

    invoke-static {v0, v1, v2, v3}, Ll;->a(Ll;Lq;ZLu;)Lgf;

    move-result-object v0

    .line 1246
    invoke-virtual {v0}, Lgf;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1247
    iget-object v1, p0, Ln;->c:Ljava/util/ArrayList;

    iget-object v2, p0, Ln;->d:Lq;

    iget-object v2, v2, Lq;->d:Landroid/view/View;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1251
    :goto_0
    iget-object v1, p0, Ln;->b:Ljava/lang/Object;

    iget-object v2, p0, Ln;->c:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Lau;->b(Ljava/lang/Object;Ljava/util/ArrayList;)V

    .line 1254
    iget-object v1, p0, Ln;->h:Ll;

    iget-object v2, p0, Ln;->d:Lq;

    invoke-static {v1, v0, v2}, Ll;->a(Ll;Lgf;Lq;)V

    .line 1256
    iget-object v0, p0, Ln;->h:Ll;

    iget-object v0, p0, Ln;->d:Lq;

    iget-object v0, p0, Ln;->f:Lu;

    iget-object v0, p0, Ln;->g:Lu;

    iget-boolean v0, p0, Ln;->e:Z

    .line 1260
    :cond_0
    const/4 v0, 0x1

    return v0

    .line 1249
    :cond_1
    iget-object v1, p0, Ln;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Lgf;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method
