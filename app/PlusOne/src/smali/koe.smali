.class public Lkoe;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lhlv;


# static fields
.field private static b:Ljava/text/SimpleDateFormat;


# instance fields
.field public final a:J

.field private c:I

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput p1, p0, Lkoe;->c:I

    .line 32
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lkoe;->a:J

    .line 33
    return-void
.end method

.method public constructor <init>(IJ)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput p1, p0, Lkoe;->c:I

    .line 43
    iput-wide p2, p0, Lkoe;->a:J

    .line 44
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lkoe;->c:I

    return v0
.end method

.method public a(Landroid/content/Context;Lhlx;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lkoe;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lkoe;->d:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p2, p1}, Lhlx;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;)Lkoe;
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lkoe;->d:Ljava/lang/String;

    .line 64
    return-object p0
.end method

.method public a(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lhlw;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhlw;

    invoke-interface {v0, p1, p0}, Lhlw;->a(Landroid/content/Context;Lhlv;)V

    .line 51
    return-void
.end method

.method public a(Lhmp;)V
    .locals 0

    .prologue
    .line 69
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 73
    sget-object v0, Lkoe;->b:Ljava/text/SimpleDateFormat;

    if-nez v0, :cond_1

    .line 74
    const-class v1, Lkoe;

    monitor-enter v1

    .line 75
    :try_start_0
    sget-object v0, Lkoe;->b:Ljava/text/SimpleDateFormat;

    if-nez v0, :cond_0

    .line 76
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd HH:mm:ss.SSSZ"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    sput-object v0, Lkoe;->b:Ljava/text/SimpleDateFormat;

    .line 78
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 80
    :cond_1
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "LatencyEvent eventCode=%d timestmap=%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lkoe;->c:I

    .line 81
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Lkoe;->b:Ljava/text/SimpleDateFormat;

    new-instance v5, Ljava/util/Date;

    iget-wide v6, p0, Lkoe;->a:J

    invoke-direct {v5, v6, v7}, Ljava/util/Date;-><init>(J)V

    .line 82
    invoke-virtual {v4, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    .line 80
    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 78
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
