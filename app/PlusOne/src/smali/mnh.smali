.class public final Lmnh;
.super Loxq;
.source "PG"


# instance fields
.field public a:I

.field public b:I

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/high16 v0, -0x80000000

    .line 5389
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5406
    iput v0, p0, Lmnh;->a:I

    .line 5409
    iput v0, p0, Lmnh;->b:I

    .line 5389
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 5431
    const/4 v0, 0x0

    .line 5432
    iget v1, p0, Lmnh;->b:I

    if-eq v1, v2, :cond_0

    .line 5433
    const/4 v0, 0x1

    iget v1, p0, Lmnh;->b:I

    .line 5434
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5436
    :cond_0
    iget v1, p0, Lmnh;->a:I

    if-eq v1, v2, :cond_1

    .line 5437
    const/4 v1, 0x2

    iget v2, p0, Lmnh;->a:I

    .line 5438
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5440
    :cond_1
    iget-object v1, p0, Lmnh;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 5441
    const/4 v1, 0x3

    iget-object v2, p0, Lmnh;->c:Ljava/lang/String;

    .line 5442
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5444
    :cond_2
    iget-object v1, p0, Lmnh;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5445
    iput v0, p0, Lmnh;->ai:I

    .line 5446
    return v0
.end method

.method public a(Loxn;)Lmnh;
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 5454
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5455
    sparse-switch v0, :sswitch_data_0

    .line 5459
    iget-object v1, p0, Lmnh;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 5460
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmnh;->ah:Ljava/util/List;

    .line 5463
    :cond_1
    iget-object v1, p0, Lmnh;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5465
    :sswitch_0
    return-object p0

    .line 5470
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5471
    if-eqz v0, :cond_2

    if-eq v0, v3, :cond_2

    if-eq v0, v4, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    .line 5476
    :cond_2
    iput v0, p0, Lmnh;->b:I

    goto :goto_0

    .line 5478
    :cond_3
    iput v2, p0, Lmnh;->b:I

    goto :goto_0

    .line 5483
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 5484
    if-eqz v0, :cond_4

    if-eq v0, v3, :cond_4

    if-ne v0, v4, :cond_5

    .line 5487
    :cond_4
    iput v0, p0, Lmnh;->a:I

    goto :goto_0

    .line 5489
    :cond_5
    iput v2, p0, Lmnh;->a:I

    goto :goto_0

    .line 5494
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmnh;->c:Ljava/lang/String;

    goto :goto_0

    .line 5455
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 3

    .prologue
    const/high16 v2, -0x80000000

    .line 5416
    iget v0, p0, Lmnh;->b:I

    if-eq v0, v2, :cond_0

    .line 5417
    const/4 v0, 0x1

    iget v1, p0, Lmnh;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5419
    :cond_0
    iget v0, p0, Lmnh;->a:I

    if-eq v0, v2, :cond_1

    .line 5420
    const/4 v0, 0x2

    iget v1, p0, Lmnh;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 5422
    :cond_1
    iget-object v0, p0, Lmnh;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 5423
    const/4 v0, 0x3

    iget-object v1, p0, Lmnh;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 5425
    :cond_2
    iget-object v0, p0, Lmnh;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5427
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5385
    invoke-virtual {p0, p1}, Lmnh;->a(Loxn;)Lmnh;

    move-result-object v0

    return-object v0
.end method
