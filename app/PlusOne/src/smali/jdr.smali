.class final Ljdr;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/nio/channels/ReadableByteChannel;


# instance fields
.field private final a:Ljava/nio/channels/ReadableByteChannel;

.field private final b:Ljdu;

.field private final c:J

.field private d:J

.field private volatile e:Z

.field private final f:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Ljava/nio/channels/ReadableByteChannel;Ljdu;J)V
    .locals 1

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    const/4 v0, 0x0

    iput-boolean v0, p0, Ljdr;->e:Z

    .line 116
    new-instance v0, Ljds;

    invoke-direct {v0, p0}, Ljds;-><init>(Ljdr;)V

    iput-object v0, p0, Ljdr;->f:Ljava/lang/Runnable;

    .line 127
    iput-object p1, p0, Ljdr;->a:Ljava/nio/channels/ReadableByteChannel;

    .line 128
    iput-object p2, p0, Ljdr;->b:Ljdu;

    .line 129
    iput-wide p3, p0, Ljdr;->c:J

    .line 130
    return-void
.end method

.method static synthetic a(Ljdr;)J
    .locals 2

    .prologue
    .line 110
    iget-wide v0, p0, Ljdr;->d:J

    return-wide v0
.end method

.method static synthetic a(Ljdr;Z)Z
    .locals 0

    .prologue
    .line 110
    iput-boolean p1, p0, Ljdr;->e:Z

    return p1
.end method

.method static synthetic b(Ljdr;)J
    .locals 2

    .prologue
    .line 110
    iget-wide v0, p0, Ljdr;->c:J

    return-wide v0
.end method

.method static synthetic c(Ljdr;)Ljdu;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Ljdr;->b:Ljdu;

    return-object v0
.end method

.method static synthetic d(Ljdr;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Ljdr;->f:Ljava/lang/Runnable;

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Ljdr;->a:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/ReadableByteChannel;->close()V

    .line 162
    return-void
.end method

.method public isOpen()Z
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Ljdr;->a:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/ReadableByteChannel;->isOpen()Z

    move-result v0

    return v0
.end method

.method public read(Ljava/nio/ByteBuffer;)I
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 135
    iget-object v0, p0, Ljdr;->a:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v0, p1}, Ljava/nio/channels/ReadableByteChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 136
    iget-wide v2, p0, Ljdr;->d:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Ljdr;->d:J

    .line 138
    iget-wide v2, p0, Ljdr;->d:J

    iget-wide v4, p0, Ljdr;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Ljdr;->e:Z

    if-nez v1, :cond_1

    .line 139
    :cond_0
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_2

    .line 140
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    iget-object v2, p0, Ljdr;->f:Ljava/lang/Runnable;

    invoke-interface {v1, v2}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 151
    :cond_1
    :goto_0
    return v0

    .line 142
    :cond_2
    new-instance v1, Ljdt;

    invoke-direct {v1, p0}, Ljdt;-><init>(Ljdr;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    .line 148
    invoke-virtual {v1, v2}, Ljdt;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
