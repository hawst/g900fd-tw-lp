.class public Lfbx;
.super Lhyd;
.source "PG"

# interfaces
.implements Landroid/widget/SectionIndexer;


# instance fields
.field public final e:Lctz;

.field public final f:Landroid/view/LayoutInflater;

.field public final g:Lctq;

.field public h:Landroid/view/View$OnClickListener;

.field public i:Landroid/view/View$OnLongClickListener;

.field private j:Lfcd;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lhyd;-><init>(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 40
    new-instance v0, Lfcd;

    new-instance v1, Lfby;

    invoke-direct {v1, p0}, Lfby;-><init>(Lfbx;)V

    invoke-direct {v0, v1}, Lfcd;-><init>(Lfcf;)V

    iput-object v0, p0, Lfbx;->j:Lfcd;

    .line 46
    const-class v0, Lctz;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctz;

    iput-object v0, p0, Lfbx;->e:Lctz;

    .line 47
    const-class v0, Lfyp;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    .line 48
    const-class v0, Lepy;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    .line 49
    const-class v0, Lctq;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lctq;

    iput-object v0, p0, Lfbx;->g:Lctq;

    .line 50
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lfbx;->f:Landroid/view/LayoutInflater;

    .line 51
    return-void
.end method


# virtual methods
.method public a(Landroid/util/SparseArray;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lfce;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    iget-object v0, p0, Lfbx;->j:Lfcd;

    invoke-virtual {v0, p1}, Lfcd;->a(Landroid/util/SparseArray;)V

    .line 75
    return-void
.end method

.method public a(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lfbx;->h:Landroid/view/View$OnClickListener;

    .line 55
    return-void
.end method

.method public a(Landroid/view/View$OnLongClickListener;)V
    .locals 0

    .prologue
    .line 58
    iput-object p1, p0, Lfbx;->i:Landroid/view/View$OnLongClickListener;

    .line 59
    return-void
.end method

.method public a(Lcom/google/android/apps/plus/views/FastScrollContainer;)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lfbx;->j:Lfcd;

    invoke-virtual {v0, p1}, Lfcd;->a(Lcom/google/android/apps/plus/views/FastScrollContainer;)V

    .line 71
    return-void
.end method

.method public f()Lcom/google/android/apps/plus/views/FastScrollContainer;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lfbx;->j:Lfcd;

    invoke-virtual {v0}, Lfcd;->a()Lcom/google/android/apps/plus/views/FastScrollContainer;

    move-result-object v0

    return-object v0
.end method

.method public getPositionForSection(I)I
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lfbx;->j:Lfcd;

    invoke-virtual {v0, p1}, Lfcd;->getPositionForSection(I)I

    move-result v0

    return v0
.end method

.method public getSectionForPosition(I)I
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lfbx;->j:Lfcd;

    invoke-virtual {v0, p1}, Lfcd;->getSectionForPosition(I)I

    move-result v0

    return v0
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lfbx;->j:Lfcd;

    invoke-virtual {v0}, Lfcd;->getSections()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
