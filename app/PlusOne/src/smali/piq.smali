.class public final Lpiq;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpiq;


# instance fields
.field private b:Lpir;

.field private c:Lpis;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x0

    new-array v0, v0, [Lpiq;

    sput-object v0, Lpiq;->a:[Lpiq;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 290
    invoke-direct {p0}, Loxq;-><init>()V

    .line 309
    iput-object v0, p0, Lpiq;->b:Lpir;

    .line 312
    iput-object v0, p0, Lpiq;->c:Lpis;

    .line 315
    const/high16 v0, -0x80000000

    iput v0, p0, Lpiq;->d:I

    .line 290
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 335
    const/4 v0, 0x0

    .line 336
    iget-object v1, p0, Lpiq;->b:Lpir;

    if-eqz v1, :cond_0

    .line 337
    const/4 v0, 0x1

    iget-object v1, p0, Lpiq;->b:Lpir;

    .line 338
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 340
    :cond_0
    iget v1, p0, Lpiq;->d:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 341
    const/4 v1, 0x2

    iget v2, p0, Lpiq;->d:I

    .line 342
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 344
    :cond_1
    iget-object v1, p0, Lpiq;->c:Lpis;

    if-eqz v1, :cond_2

    .line 345
    const/4 v1, 0x3

    iget-object v2, p0, Lpiq;->c:Lpis;

    .line 346
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 348
    :cond_2
    iget-object v1, p0, Lpiq;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 349
    iput v0, p0, Lpiq;->ai:I

    .line 350
    return v0
.end method

.method public a(Loxn;)Lpiq;
    .locals 2

    .prologue
    .line 358
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 359
    sparse-switch v0, :sswitch_data_0

    .line 363
    iget-object v1, p0, Lpiq;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 364
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpiq;->ah:Ljava/util/List;

    .line 367
    :cond_1
    iget-object v1, p0, Lpiq;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 369
    :sswitch_0
    return-object p0

    .line 374
    :sswitch_1
    iget-object v0, p0, Lpiq;->b:Lpir;

    if-nez v0, :cond_2

    .line 375
    new-instance v0, Lpir;

    invoke-direct {v0}, Lpir;-><init>()V

    iput-object v0, p0, Lpiq;->b:Lpir;

    .line 377
    :cond_2
    iget-object v0, p0, Lpiq;->b:Lpir;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 381
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 382
    if-eqz v0, :cond_3

    const/4 v1, 0x1

    if-eq v0, v1, :cond_3

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    const/4 v1, 0x3

    if-eq v0, v1, :cond_3

    const/4 v1, 0x4

    if-eq v0, v1, :cond_3

    const/4 v1, 0x5

    if-eq v0, v1, :cond_3

    const/4 v1, 0x6

    if-eq v0, v1, :cond_3

    const/16 v1, 0x9

    if-eq v0, v1, :cond_3

    const/16 v1, 0xa

    if-eq v0, v1, :cond_3

    const/16 v1, 0xb

    if-eq v0, v1, :cond_3

    const/16 v1, 0xc

    if-eq v0, v1, :cond_3

    const/16 v1, 0xd

    if-eq v0, v1, :cond_3

    const/16 v1, 0x2d

    if-ne v0, v1, :cond_4

    .line 395
    :cond_3
    iput v0, p0, Lpiq;->d:I

    goto :goto_0

    .line 397
    :cond_4
    const/4 v0, 0x0

    iput v0, p0, Lpiq;->d:I

    goto :goto_0

    .line 402
    :sswitch_3
    iget-object v0, p0, Lpiq;->c:Lpis;

    if-nez v0, :cond_5

    .line 403
    new-instance v0, Lpis;

    invoke-direct {v0}, Lpis;-><init>()V

    iput-object v0, p0, Lpiq;->c:Lpis;

    .line 405
    :cond_5
    iget-object v0, p0, Lpiq;->c:Lpis;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 359
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Lpiq;->b:Lpir;

    if-eqz v0, :cond_0

    .line 321
    const/4 v0, 0x1

    iget-object v1, p0, Lpiq;->b:Lpir;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 323
    :cond_0
    iget v0, p0, Lpiq;->d:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 324
    const/4 v0, 0x2

    iget v1, p0, Lpiq;->d:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 326
    :cond_1
    iget-object v0, p0, Lpiq;->c:Lpis;

    if-eqz v0, :cond_2

    .line 327
    const/4 v0, 0x3

    iget-object v1, p0, Lpiq;->c:Lpis;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 329
    :cond_2
    iget-object v0, p0, Lpiq;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 331
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 286
    invoke-virtual {p0, p1}, Lpiq;->a(Loxn;)Lpiq;

    move-result-object v0

    return-object v0
.end method
