.class public final Lirx;
.super Lisb;
.source "PG"

# interfaces
.implements Lisv;


# instance fields
.field private a:I

.field private b:I

.field private synthetic c:Lcom/google/android/libraries/social/ingest/IngestActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/ingest/IngestActivity;)V
    .locals 1

    .prologue
    const/4 v0, -0x1

    .line 293
    iput-object p1, p0, Lirx;->c:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-direct {p0}, Lisb;-><init>()V

    .line 295
    iput v0, p0, Lirx;->a:I

    .line 296
    iput v0, p0, Lirx;->b:I

    return-void
.end method

.method private b(I)I
    .locals 1

    .prologue
    .line 299
    iget v0, p0, Lirx;->a:I

    if-eq p1, v0, :cond_0

    .line 300
    iput p1, p0, Lirx;->a:I

    .line 301
    iget-object v0, p0, Lirx;->c:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->b(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lisd;

    move-result-object v0

    invoke-virtual {v0, p1}, Lisd;->b(I)I

    move-result v0

    iput v0, p0, Lirx;->b:I

    .line 303
    :cond_0
    iget v0, p0, Lirx;->b:I

    return v0
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 333
    invoke-virtual {p0}, Lirx;->b()V

    .line 334
    return-void
.end method

.method public a(IZ)V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lirx;->c:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lcom/google/android/libraries/social/ingest/ui/IngestGridView;

    move-result-object v0

    invoke-direct {p0, p1}, Lirx;->b(I)I

    move-result v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/libraries/social/ingest/ui/IngestGridView;->setItemChecked(IZ)V

    .line 317
    return-void
.end method

.method public a(I)Z
    .locals 2

    .prologue
    .line 328
    iget-object v0, p0, Lirx;->c:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->a(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lcom/google/android/libraries/social/ingest/ui/IngestGridView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/social/ingest/ui/IngestGridView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v0

    invoke-direct {p0, p1}, Lirx;->b(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v0

    return v0
.end method

.method public b(IZ)V
    .locals 1

    .prologue
    .line 321
    iget-object v0, p0, Lirx;->c:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->g(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lise;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 322
    iget v0, p0, Lirx;->b:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lirx;->b:I

    iget-object v0, p0, Lirx;->c:Lcom/google/android/libraries/social/ingest/IngestActivity;

    invoke-static {v0}, Lcom/google/android/libraries/social/ingest/IngestActivity;->g(Lcom/google/android/libraries/social/ingest/IngestActivity;)Lise;

    move-result-object v0

    invoke-virtual {v0, p1}, Lise;->a(I)I

    move-result v0

    iput v0, p0, Lirx;->a:I

    :cond_0
    iget v0, p0, Lirx;->a:I

    invoke-super {p0, v0, p2}, Lisb;->b(IZ)V

    .line 324
    :cond_1
    return-void
.end method
