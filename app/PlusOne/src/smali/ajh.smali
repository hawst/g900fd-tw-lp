.class public final Lajh;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Lajv;


# instance fields
.field private final c:Lajv;

.field private final d:Lbgf;

.field private final e:Lbjp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lbjp",
            "<",
            "Lbhl;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lajv;Lbgf;Lbjp;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lajv;",
            "Lbgf;",
            "Lbjp",
            "<",
            "Lbhl;",
            ">;)V"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    const-string v0, "delegate"

    invoke-static {p1, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lajv;

    iput-object v0, p0, Lajh;->c:Lajv;

    .line 38
    const-string v0, "renderContext"

    invoke-static {p2, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbgf;

    iput-object v0, p0, Lajh;->d:Lbgf;

    .line 39
    const-string v0, "decoderPool"

    invoke-static {p3, v0, v1}, Lcec;->a(Ljava/lang/Object;Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lbjp;

    iput-object v0, p0, Lajh;->e:Lbjp;

    .line 40
    return-void
.end method

.method static synthetic a(Lajh;)Lbjp;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lajh;->e:Lbjp;

    return-object v0
.end method

.method private a(Landroid/net/Uri;Laic;Lahy;Lbkr;Z)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 54
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v1, v0}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 56
    :try_start_0
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 57
    iget-object v2, p0, Lajh;->d:Lbgf;

    new-instance v3, Laji;

    invoke-direct {v3, p0, v0, v1}, Laji;-><init>(Lajh;Ljava/util/concurrent/CountDownLatch;Ljava/util/concurrent/CountDownLatch;)V

    invoke-virtual {v2, v3}, Lbgf;->b(Ljava/lang/Runnable;)V

    .line 81
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 82
    if-eqz p5, :cond_0

    .line 83
    iget-object v0, p0, Lajh;->c:Lajv;

    invoke-interface {v0, p1, p2, p4, p3}, Lajv;->a(Landroid/net/Uri;Laic;Lbkr;Lahy;)V
    :try_end_0
    .catch Lbgm; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    :goto_0
    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 94
    :goto_1
    return-void

    .line 85
    :cond_0
    :try_start_1
    iget-object v0, p0, Lajh;->c:Lajv;

    invoke-interface {v0, p1, p2, p3}, Lajv;->a(Landroid/net/Uri;Laic;Lahy;)V
    :try_end_1
    .catch Lbgm; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 87
    :catch_0
    move-exception v0

    .line 88
    :try_start_2
    const-string v2, "render context not initialized for analysis"

    invoke-static {v2, v0}, Lcgp;->a(Ljava/lang/CharSequence;Ljava/lang/Throwable;)Ljava/lang/IllegalStateException;

    move-result-object v0

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 93
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0

    .line 90
    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 93
    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_1
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lajh;->c:Lajv;

    invoke-interface {v0}, Lajv;->a()V

    .line 105
    return-void
.end method

.method public a(Landroid/net/Uri;Laic;Lahy;)V
    .locals 6

    .prologue
    .line 44
    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lajh;->a(Landroid/net/Uri;Laic;Lahy;Lbkr;Z)V

    .line 45
    return-void
.end method

.method public a(Landroid/net/Uri;Laic;Lbkr;Lahy;)V
    .locals 6

    .prologue
    .line 49
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lajh;->a(Landroid/net/Uri;Laic;Lahy;Lbkr;Z)V

    .line 50
    return-void
.end method

.method public b(Landroid/net/Uri;Laic;Lahy;)V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lajh;->c:Lajv;

    invoke-interface {v0, p1, p2, p3}, Lajv;->b(Landroid/net/Uri;Laic;Lahy;)V

    .line 100
    return-void
.end method
