.class final Llff;
.super Ligg;
.source "PG"


# instance fields
.field public e:I

.field public f:I

.field public g:I

.field public h:Llff;

.field public i:Landroid/graphics/Bitmap;

.field public volatile j:I

.field private synthetic k:Llfe;


# direct methods
.method public constructor <init>(Llfe;III)V
    .locals 1

    .prologue
    .line 674
    iput-object p1, p0, Llff;->k:Llfe;

    invoke-direct {p0}, Ligg;-><init>()V

    .line 672
    const/4 v0, 0x1

    iput v0, p0, Llff;->j:I

    .line 675
    iput p2, p0, Llff;->e:I

    .line 676
    iput p3, p0, Llff;->f:I

    .line 677
    iput p4, p0, Llff;->g:I

    .line 678
    return-void
.end method


# virtual methods
.method public a(III)V
    .locals 0

    .prologue
    .line 731
    iput p1, p0, Llff;->e:I

    .line 732
    iput p2, p0, Llff;->f:I

    .line 733
    iput p3, p0, Llff;->g:I

    .line 734
    invoke-virtual {p0}, Llff;->m()V

    .line 735
    return-void
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 682
    invoke-static {}, Llfe;->c()Llfb;

    move-result-object v0

    invoke-interface {v0, p1}, Llfb;->a(Ljava/lang/Object;)Z

    .line 683
    return-void
.end method

.method protected aK_()Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 702
    iget v0, p0, Llff;->j:I

    const/16 v2, 0x8

    if-ne v0, v2, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lifu;->a(Z)V

    .line 706
    iget-object v0, p0, Llff;->k:Llfe;

    iget v0, v0, Llfe;->b:I

    iget v2, p0, Llff;->e:I

    sub-int/2addr v0, v2

    iget v2, p0, Llff;->g:I

    shr-int/2addr v0, v2

    .line 707
    iget-object v2, p0, Llff;->k:Llfe;

    iget v2, v2, Llfe;->c:I

    iget v3, p0, Llff;->f:I

    sub-int/2addr v2, v3

    iget v3, p0, Llff;->g:I

    shr-int/2addr v2, v3

    .line 708
    iget-object v3, p0, Llff;->k:Llfe;

    invoke-static {v3}, Llfe;->a(Llfe;)I

    move-result v3

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v3, p0, Llff;->k:Llfe;

    invoke-static {v3}, Llfe;->a(Llfe;)I

    move-result v3

    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    invoke-virtual {p0, v0, v2}, Llff;->a(II)V

    .line 710
    iget-object v0, p0, Llff;->i:Landroid/graphics/Bitmap;

    .line 711
    const/4 v2, 0x0

    iput-object v2, p0, Llff;->i:Landroid/graphics/Bitmap;

    .line 712
    iput v1, p0, Llff;->j:I

    .line 713
    return-object v0

    .line 702
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 722
    iget-object v0, p0, Llff;->k:Llfe;

    invoke-static {v0}, Llfe;->a(Llfe;)I

    move-result v0

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 727
    iget-object v0, p0, Llff;->k:Llfe;

    invoke-static {v0}, Llfe;->a(Llfe;)I

    move-result v0

    return v0
.end method

.method o()Z
    .locals 5

    .prologue
    .line 689
    :try_start_0
    invoke-static {}, Llfe;->c()Llfb;

    move-result-object v0

    invoke-interface {v0}, Llfb;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 690
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Llff;->k:Llfe;

    invoke-static {v2}, Llfe;->a(Llfe;)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 691
    const/4 v0, 0x0

    .line 693
    :cond_0
    iget-object v1, p0, Llff;->k:Llfe;

    invoke-static {v1}, Llfe;->b(Llfe;)Llfi;

    move-result-object v1

    iget v2, p0, Llff;->g:I

    iget v3, p0, Llff;->e:I

    iget v4, p0, Llff;->f:I

    invoke-interface {v1, v2, v3, v4, v0}, Llfi;->a(IIILandroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Llff;->i:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 695
    :goto_0
    iget-object v0, p0, Llff;->i:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public p()Llff;
    .locals 4

    .prologue
    .line 738
    iget v0, p0, Llff;->g:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Llff;->k:Llfe;

    iget v1, v1, Llfe;->a:I

    if-ne v0, v1, :cond_0

    .line 739
    const/4 v0, 0x0

    .line 744
    :goto_0
    return-object v0

    .line 741
    :cond_0
    iget-object v0, p0, Llff;->k:Llfe;

    invoke-static {v0}, Llfe;->a(Llfe;)I

    move-result v0

    iget v1, p0, Llff;->g:I

    add-int/lit8 v1, v1, 0x1

    shl-int/2addr v0, v1

    .line 742
    iget v1, p0, Llff;->e:I

    div-int/2addr v1, v0

    mul-int/2addr v1, v0

    .line 743
    iget v2, p0, Llff;->f:I

    div-int/2addr v2, v0

    mul-int/2addr v0, v2

    .line 744
    iget-object v2, p0, Llff;->k:Llfe;

    iget v3, p0, Llff;->g:I

    add-int/lit8 v3, v3, 0x1

    invoke-static {v2, v1, v0, v3}, Llfe;->a(Llfe;III)Llff;

    move-result-object v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 749
    const-string v0, "tile(%s, %s, %s / %s)"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Llff;->e:I

    iget-object v4, p0, Llff;->k:Llfe;

    .line 750
    invoke-static {v4}, Llfe;->a(Llfe;)I

    move-result v4

    div-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Llff;->f:I

    iget-object v4, p0, Llff;->k:Llfe;

    invoke-static {v4}, Llfe;->a(Llfe;)I

    move-result v4

    div-int/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Llff;->k:Llfe;

    invoke-static {v3}, Llfe;->c(Llfe;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Llff;->k:Llfe;

    iget v3, v3, Llfe;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    .line 749
    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
