.class public final Ljjh;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Landroid/content/SharedPreferences;

.field private final c:Lilb;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const-class v0, Lilb;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lilb;

    iput-object v0, p0, Ljjh;->c:Lilb;

    .line 32
    const-string v0, "com.google.android.libraries.social.notifications.GCM"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Ljjh;->b:Landroid/content/SharedPreferences;

    .line 33
    const-class v0, Ljiy;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljiy;

    invoke-interface {v0}, Ljiy;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ljjh;->a:Ljava/lang/String;

    .line 34
    return-void
.end method


# virtual methods
.method public a()Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 42
    .line 44
    iget-object v0, p0, Ljjh;->b:Landroid/content/SharedPreferences;

    const-string v1, "reg_id"

    const/4 v4, 0x0

    invoke-interface {v0, v1, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 45
    iget-object v1, p0, Ljjh;->b:Landroid/content/SharedPreferences;

    const-string v4, "reg_timestamp"

    const-wide/16 v6, 0x0

    invoke-interface {v1, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 47
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 48
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long v4, v6, v4

    const-wide/32 v6, 0x5265c00

    cmp-long v1, v4, v6

    if-lez v1, :cond_3

    :cond_0
    move v1, v3

    .line 50
    :goto_0
    if-eqz v1, :cond_5

    .line 53
    :try_start_0
    iget-object v1, p0, Ljjh;->c:Lilb;

    iget-object v4, p0, Ljjh;->a:Ljava/lang/String;

    invoke-interface {v1, v4}, Lilb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 54
    if-eqz v0, :cond_1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    if-nez v4, :cond_4

    :cond_1
    move-object v0, v1

    move v1, v3

    .line 61
    :goto_1
    iget-object v2, p0, Ljjh;->c:Lilb;

    invoke-interface {v2}, Lilb;->a()V

    .line 65
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 66
    iget-object v2, p0, Ljjh;->b:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 67
    const-string v3, "reg_id"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 68
    const-string v0, "reg_timestamp"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v2, v0, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 69
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_2
    :goto_2
    move v2, v1

    .line 72
    :goto_3
    return v2

    :cond_3
    move v1, v2

    .line 48
    goto :goto_0

    .line 59
    :catch_0
    move-exception v0

    iget-object v0, p0, Ljjh;->c:Lilb;

    invoke-interface {v0}, Lilb;->a()V

    goto :goto_3

    :catchall_0
    move-exception v0

    iget-object v1, p0, Ljjh;->c:Lilb;

    invoke-interface {v1}, Lilb;->a()V

    throw v0

    :cond_4
    move v1, v2

    goto :goto_1

    :cond_5
    move v1, v2

    goto :goto_2
.end method

.method public b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 79
    iget-object v0, p0, Ljjh;->b:Landroid/content/SharedPreferences;

    const-string v1, "reg_id"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
