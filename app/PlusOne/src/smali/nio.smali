.class public final Lnio;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnja;

.field public b:[Lnin;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2159
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2162
    const/4 v0, 0x0

    iput-object v0, p0, Lnio;->a:Lnja;

    .line 2165
    sget-object v0, Lnin;->a:[Lnin;

    iput-object v0, p0, Lnio;->b:[Lnin;

    .line 2159
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2186
    .line 2187
    iget-object v0, p0, Lnio;->a:Lnja;

    if-eqz v0, :cond_2

    .line 2188
    const/4 v0, 0x1

    iget-object v2, p0, Lnio;->a:Lnja;

    .line 2189
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2191
    :goto_0
    iget-object v2, p0, Lnio;->b:[Lnin;

    if-eqz v2, :cond_1

    .line 2192
    iget-object v2, p0, Lnio;->b:[Lnin;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 2193
    if-eqz v4, :cond_0

    .line 2194
    const/4 v5, 0x2

    .line 2195
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2192
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2199
    :cond_1
    iget-object v1, p0, Lnio;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2200
    iput v0, p0, Lnio;->ai:I

    .line 2201
    return v0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lnio;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2209
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2210
    sparse-switch v0, :sswitch_data_0

    .line 2214
    iget-object v2, p0, Lnio;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 2215
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lnio;->ah:Ljava/util/List;

    .line 2218
    :cond_1
    iget-object v2, p0, Lnio;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2220
    :sswitch_0
    return-object p0

    .line 2225
    :sswitch_1
    iget-object v0, p0, Lnio;->a:Lnja;

    if-nez v0, :cond_2

    .line 2226
    new-instance v0, Lnja;

    invoke-direct {v0}, Lnja;-><init>()V

    iput-object v0, p0, Lnio;->a:Lnja;

    .line 2228
    :cond_2
    iget-object v0, p0, Lnio;->a:Lnja;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2232
    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 2233
    iget-object v0, p0, Lnio;->b:[Lnin;

    if-nez v0, :cond_4

    move v0, v1

    .line 2234
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lnin;

    .line 2235
    iget-object v3, p0, Lnio;->b:[Lnin;

    if-eqz v3, :cond_3

    .line 2236
    iget-object v3, p0, Lnio;->b:[Lnin;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 2238
    :cond_3
    iput-object v2, p0, Lnio;->b:[Lnin;

    .line 2239
    :goto_2
    iget-object v2, p0, Lnio;->b:[Lnin;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 2240
    iget-object v2, p0, Lnio;->b:[Lnin;

    new-instance v3, Lnin;

    invoke-direct {v3}, Lnin;-><init>()V

    aput-object v3, v2, v0

    .line 2241
    iget-object v2, p0, Lnio;->b:[Lnin;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 2242
    invoke-virtual {p1}, Loxn;->a()I

    .line 2239
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 2233
    :cond_4
    iget-object v0, p0, Lnio;->b:[Lnin;

    array-length v0, v0

    goto :goto_1

    .line 2245
    :cond_5
    iget-object v2, p0, Lnio;->b:[Lnin;

    new-instance v3, Lnin;

    invoke-direct {v3}, Lnin;-><init>()V

    aput-object v3, v2, v0

    .line 2246
    iget-object v2, p0, Lnio;->b:[Lnin;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 2210
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2170
    iget-object v0, p0, Lnio;->a:Lnja;

    if-eqz v0, :cond_0

    .line 2171
    const/4 v0, 0x1

    iget-object v1, p0, Lnio;->a:Lnja;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 2173
    :cond_0
    iget-object v0, p0, Lnio;->b:[Lnin;

    if-eqz v0, :cond_2

    .line 2174
    iget-object v1, p0, Lnio;->b:[Lnin;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 2175
    if-eqz v3, :cond_1

    .line 2176
    const/4 v4, 0x2

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 2174
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2180
    :cond_2
    iget-object v0, p0, Lnio;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2182
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2155
    invoke-virtual {p0, p1}, Lnio;->a(Loxn;)Lnio;

    move-result-object v0

    return-object v0
.end method
