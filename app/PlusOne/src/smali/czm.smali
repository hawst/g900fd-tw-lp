.class public Lczm;
.super Llol;
.source "PG"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Ldgl;
.implements Lgcb;


# instance fields
.field private N:Ldeo;

.field private O:Ldel;

.field private P:Ldgi;

.field private Q:I

.field private R:Lczs;

.field private S:Lddl;

.field private T:Landroid/view/animation/Animation;

.field private U:Landroid/view/animation/Animation;

.field private V:Landroid/widget/ImageButton;

.field private W:Lcom/google/android/apps/plus/views/VideoProgressView;

.field private X:Z

.field private Y:Ljuk;

.field private final Z:Ljum;

.field private final aa:Ljun;

.field private final ab:Ljma;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljma",
            "<",
            "Ldeo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Llol;-><init>()V

    .line 56
    new-instance v0, Lczn;

    invoke-direct {v0, p0}, Lczn;-><init>(Lczm;)V

    iput-object v0, p0, Lczm;->Z:Ljum;

    .line 68
    new-instance v0, Lczo;

    invoke-direct {v0, p0}, Lczo;-><init>(Lczm;)V

    iput-object v0, p0, Lczm;->aa:Ljun;

    .line 97
    new-instance v0, Lczp;

    invoke-direct {v0, p0}, Lczp;-><init>(Lczm;)V

    iput-object v0, p0, Lczm;->ab:Ljma;

    return-void
.end method

.method private U()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 179
    iget-object v0, p0, Lczm;->au:Llnh;

    const-class v3, Lieh;

    invoke-virtual {v0, v3}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lieh;

    invoke-virtual {p0}, Lczm;->k()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "disable_chromecast"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_1

    sget-object v3, Ldxd;->m:Lief;

    iget v4, p0, Lczm;->Q:I

    invoke-interface {v0, v3, v4}, Lieh;->b(Lief;I)Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, Lczm;->Y:Ljuk;

    invoke-virtual {v0}, Ljuk;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lczm;->x()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    .line 222
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    .line 179
    goto :goto_0

    .line 184
    :cond_2
    iget-object v0, p0, Lczm;->N:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lczm;->N:Ldeo;

    .line 185
    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->a()Lizu;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 189
    const-string v0, "castSelf: Model Ready"

    invoke-direct {p0, v0}, Lczm;->a(Ljava/lang/String;)V

    .line 191
    iget-object v0, p0, Lczm;->N:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    iput-object v0, p0, Lczm;->S:Lddl;

    .line 192
    iget-object v0, p0, Lczm;->S:Lddl;

    invoke-interface {v0}, Lddl;->a()Lizu;

    move-result-object v0

    .line 194
    invoke-virtual {v0}, Lizu;->d()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 195
    iget-boolean v1, p0, Lczm;->X:Z

    if-eqz v1, :cond_3

    .line 196
    iget-object v1, p0, Lczm;->Y:Ljuk;

    invoke-virtual {p0}, Lczm;->k()Landroid/os/Bundle;

    move-result-object v3

    iget-object v4, p0, Lczm;->R:Lczs;

    invoke-virtual {v4}, Lczs;->a()I

    move-result v4

    invoke-virtual {v1, v3, v4, v0, v6}, Ljuk;->a(Landroid/os/Bundle;ILizu;Lizu;)V

    .line 197
    iget-object v0, p0, Lczm;->O:Ldel;

    invoke-virtual {v0, v2}, Ldel;->a(Z)V

    .line 201
    :goto_2
    invoke-direct {p0, v5}, Lczm;->c(I)V

    .line 219
    :goto_3
    iget-object v0, p0, Lczm;->S:Lddl;

    invoke-interface {v0}, Lddl;->F()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    invoke-virtual {p0}, Lczm;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1001ef

    const v3, 0x7f1001ee

    invoke-direct {p0, v0, v1, v3}, Lczm;->a(Landroid/view/View;II)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lczm;->V:Landroid/widget/ImageButton;

    iget-object v0, p0, Lczm;->V:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lczm;->V:Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    invoke-virtual {p0}, Lczm;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1001f1

    const v2, 0x7f1001f0

    invoke-direct {p0, v0, v1, v2}, Lczm;->a(Landroid/view/View;II)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/plus/views/VideoProgressView;

    iput-object v0, p0, Lczm;->W:Lcom/google/android/apps/plus/views/VideoProgressView;

    iget-object v0, p0, Lczm;->W:Lcom/google/android/apps/plus/views/VideoProgressView;

    invoke-virtual {v0, p0}, Lcom/google/android/apps/plus/views/VideoProgressView;->a(Lgcb;)V

    goto/16 :goto_1

    .line 199
    :cond_3
    iget-object v1, p0, Lczm;->Y:Ljuk;

    invoke-virtual {v1, v0}, Ljuk;->a(Lizu;)V

    goto :goto_2

    .line 202
    :cond_4
    iget-object v1, p0, Lczm;->S:Lddl;

    invoke-interface {v1}, Lddl;->g()Lnym;

    move-result-object v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lczm;->S:Lddl;

    invoke-interface {v1}, Lddl;->g()Lnym;

    move-result-object v1

    iget-object v1, v1, Lnym;->b:Lnyl;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lczm;->S:Lddl;

    .line 203
    invoke-interface {v1}, Lddl;->g()Lnym;

    move-result-object v1

    iget-object v1, v1, Lnym;->b:Lnyl;

    iget-object v1, v1, Lnyl;->b:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 205
    iget-object v1, p0, Lczm;->S:Lddl;

    invoke-interface {v1}, Lddl;->g()Lnym;

    move-result-object v1

    .line 206
    iget-object v1, v1, Lnym;->b:Lnyl;

    iget-object v1, v1, Lnyl;->b:Ljava/lang/String;

    .line 207
    invoke-virtual {p0}, Lczm;->n()Lz;

    move-result-object v3

    invoke-virtual {v0}, Lizu;->g()Ljac;

    move-result-object v4

    invoke-static {v3, v1, v4}, Lizu;->a(Landroid/content/Context;Ljava/lang/String;Ljac;)Lizu;

    move-result-object v1

    .line 208
    iget-boolean v3, p0, Lczm;->X:Z

    if-eqz v3, :cond_5

    .line 209
    iget-object v0, p0, Lczm;->Y:Ljuk;

    invoke-virtual {p0}, Lczm;->k()Landroid/os/Bundle;

    move-result-object v3

    iget-object v4, p0, Lczm;->R:Lczs;

    invoke-virtual {v4}, Lczs;->a()I

    move-result v4

    invoke-virtual {v0, v3, v4, v1, v6}, Ljuk;->a(Landroid/os/Bundle;ILizu;Lizu;)V

    .line 210
    iget-object v0, p0, Lczm;->O:Ldel;

    invoke-virtual {v0, v2}, Ldel;->a(Z)V

    .line 214
    :goto_4
    invoke-direct {p0, v5}, Lczm;->c(I)V

    goto/16 :goto_3

    .line 212
    :cond_5
    iget-object v1, p0, Lczm;->Y:Ljuk;

    invoke-virtual {v1, v0}, Ljuk;->a(Lizu;)V

    goto :goto_4

    .line 216
    :cond_6
    invoke-direct {p0, v2}, Lczm;->c(I)V

    goto/16 :goto_3
.end method

.method private a(Landroid/view/View;II)Landroid/view/View;
    .locals 1

    .prologue
    .line 258
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 259
    if-eqz v0, :cond_0

    .line 264
    :goto_0
    return-object v0

    .line 263
    :cond_0
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 264
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lczm;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lczm;->U()V

    return-void
.end method

.method static synthetic a(Lczm;I)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lczm;->c(I)V

    return-void
.end method

.method static synthetic a(Lczm;Z)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lczm;->a(Z)V

    return-void
.end method

.method static synthetic a(Lczm;ZZ)V
    .locals 2

    .prologue
    const/16 v0, 0x8

    .line 39
    iget-object v1, p0, Lczm;->W:Lcom/google/android/apps/plus/views/VideoProgressView;

    if-eqz v1, :cond_0

    if-eqz p2, :cond_2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lczm;->W:Lcom/google/android/apps/plus/views/VideoProgressView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/VideoProgressView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lczm;->W:Lcom/google/android/apps/plus/views/VideoProgressView;

    iget-object v1, p0, Lczm;->U:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/VideoProgressView;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lczm;->W:Lcom/google/android/apps/plus/views/VideoProgressView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/VideoProgressView;->getVisibility()I

    move-result v1

    if-eq v1, v0, :cond_0

    iget-object v0, p0, Lczm;->W:Lcom/google/android/apps/plus/views/VideoProgressView;

    iget-object v1, p0, Lczm;->T:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/VideoProgressView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lczm;->W:Lcom/google/android/apps/plus/views/VideoProgressView;

    if-eqz p1, :cond_3

    const/4 v0, 0x0

    :cond_3
    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/VideoProgressView;->setVisibility(I)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 169
    const-string v0, "CastFragment"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    invoke-virtual {p0}, Lczm;->hashCode()I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, 0xe

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " - "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 172
    :cond_0
    return-void
.end method

.method private a(Z)V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lczm;->V:Landroid/widget/ImageButton;

    if-nez v0, :cond_0

    .line 322
    :goto_0
    return-void

    .line 320
    :cond_0
    if-eqz p1, :cond_1

    const v0, 0x7f020472

    .line 321
    :goto_1
    iget-object v1, p0, Lczm;->V:Landroid/widget/ImageButton;

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    .line 320
    :cond_1
    const v0, 0x7f020477

    goto :goto_1
.end method

.method static synthetic b(Lczm;)V
    .locals 2

    .prologue
    const/16 v1, 0x8

    .line 39
    iget-object v0, p0, Lczm;->V:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lczm;->V:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lczm;->W:Lcom/google/android/apps/plus/views/VideoProgressView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lczm;->W:Lcom/google/android/apps/plus/views/VideoProgressView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/VideoProgressView;->setVisibility(I)V

    :cond_1
    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 361
    iget-boolean v0, p0, Lczm;->X:Z

    if-eq p1, v0, :cond_1

    .line 362
    iput-boolean p1, p0, Lczm;->X:Z

    .line 363
    if-eqz p1, :cond_2

    const-string v0, "activated"

    :goto_0
    invoke-direct {p0, v0}, Lczm;->a(Ljava/lang/String;)V

    .line 364
    if-eqz p1, :cond_0

    .line 365
    iget-object v0, p0, Lczm;->Y:Ljuk;

    iget-object v1, p0, Lczm;->aa:Ljun;

    invoke-virtual {v0, v1}, Ljuk;->a(Ljun;)V

    .line 367
    :cond_0
    invoke-direct {p0}, Lczm;->U()V

    .line 369
    :cond_1
    return-void

    .line 363
    :cond_2
    const-string v0, "deactivated"

    goto :goto_0
.end method

.method static synthetic c(Lczm;)Lddl;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lczm;->S:Lddl;

    return-object v0
.end method

.method private c(I)V
    .locals 3

    .prologue
    .line 288
    invoke-virtual {p0}, Lczm;->x()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_1

    .line 312
    :cond_0
    :goto_0
    return-void

    .line 291
    :cond_1
    invoke-virtual {p0}, Lczm;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f100468

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 292
    if-nez v0, :cond_3

    if-nez p1, :cond_3

    .line 293
    invoke-virtual {p0}, Lczm;->x()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f1001f2

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    .line 294
    iget-object v0, p0, Lczm;->N:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lczm;->N:Ldeo;

    invoke-virtual {v0}, Ldeo;->a()Lddl;

    move-result-object v0

    invoke-interface {v0}, Lddl;->F()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 295
    const v0, 0x7f100469

    .line 296
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 297
    const v2, 0x7f0a0b1b

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 298
    const v0, 0x7f10046a

    .line 299
    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 300
    const v2, 0x7f0a0b1d

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_2
    move-object v0, v1

    .line 304
    :cond_3
    if-eqz v0, :cond_0

    .line 305
    if-nez p1, :cond_4

    .line 306
    const-string v1, "set no cast visible"

    invoke-direct {p0, v1}, Lczm;->a(Ljava/lang/String;)V

    .line 310
    :goto_1
    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 308
    :cond_4
    const-string v1, "set no cast hidden"

    invoke-direct {p0, v1}, Lczm;->a(Ljava/lang/String;)V

    goto :goto_1
.end method

.method static synthetic d(Lczm;)Lcom/google/android/apps/plus/views/VideoProgressView;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lczm;->W:Lcom/google/android/apps/plus/views/VideoProgressView;

    return-object v0
.end method


# virtual methods
.method public A()V
    .locals 2

    .prologue
    .line 164
    iget-object v0, p0, Lczm;->N:Ldeo;

    invoke-virtual {v0}, Ldeo;->b()Ljlx;

    move-result-object v0

    iget-object v1, p0, Lczm;->ab:Ljma;

    invoke-interface {v0, v1}, Ljlx;->a(Ljma;)V

    .line 165
    invoke-super {p0}, Llol;->A()V

    .line 166
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 147
    const v0, 0x7f040062

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 352
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lczm;->b(Z)V

    .line 353
    return-void
.end method

.method public a(D)V
    .locals 1

    .prologue
    .line 347
    iget-object v0, p0, Lczm;->Y:Ljuk;

    invoke-virtual {v0, p1, p2}, Ljuk;->a(D)V

    .line 348
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 114
    invoke-super {p0, p1}, Llol;->a(Landroid/os/Bundle;)V

    .line 115
    invoke-virtual {p0}, Lczm;->n()Lz;

    move-result-object v0

    const v1, 0x7f050013

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lczm;->U:Landroid/view/animation/Animation;

    iget-object v0, p0, Lczm;->U:Landroid/view/animation/Animation;

    new-instance v1, Lczq;

    invoke-direct {v1, p0}, Lczq;-><init>(Lczm;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    invoke-virtual {p0}, Lczm;->n()Lz;

    move-result-object v0

    const v1, 0x7f050015

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lczm;->T:Landroid/view/animation/Animation;

    iget-object v0, p0, Lczm;->T:Landroid/view/animation/Animation;

    new-instance v1, Lczr;

    invoke-direct {v1, p0}, Lczr;-><init>(Lczm;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 117
    invoke-virtual {p0}, Lczm;->k()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "account_id"

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lczm;->Q:I

    .line 119
    invoke-virtual {p0}, Lczm;->n()Lz;

    move-result-object v0

    const-class v1, Ljuk;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljuk;

    iput-object v0, p0, Lczm;->Y:Ljuk;

    .line 121
    iget-object v0, p0, Lczm;->P:Ldgi;

    invoke-interface {v0, p0}, Ldgi;->a(Ldgl;)V

    .line 123
    iget-object v0, p0, Lczm;->N:Ldeo;

    invoke-virtual {v0}, Ldeo;->b()Ljlx;

    move-result-object v0

    iget-object v1, p0, Lczm;->ab:Ljma;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Ljlx;->a(Ljma;Z)V

    .line 124
    return-void
.end method

.method public aO_()V
    .locals 2

    .prologue
    .line 152
    invoke-super {p0}, Llol;->aO_()V

    .line 153
    iget-object v0, p0, Lczm;->Y:Ljuk;

    iget-object v1, p0, Lczm;->Z:Ljum;

    invoke-virtual {v0, v1}, Ljuk;->a(Ljum;)V

    .line 154
    return-void
.end method

.method public b()V
    .locals 1

    .prologue
    .line 357
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lczm;->b(Z)V

    .line 358
    return-void
.end method

.method public c()V
    .locals 0

    .prologue
    .line 374
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lczm;->au:Llnh;

    const-class v1, Ldeo;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldeo;

    iput-object v0, p0, Lczm;->N:Ldeo;

    .line 107
    iget-object v0, p0, Lczm;->au:Llnh;

    const-class v1, Ldgi;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldgi;

    iput-object v0, p0, Lczm;->P:Ldgi;

    .line 108
    iget-object v0, p0, Lczm;->au:Llnh;

    const-class v1, Ldel;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ldel;

    iput-object v0, p0, Lczm;->O:Ldel;

    .line 109
    iget-object v0, p0, Lczm;->au:Llnh;

    const-class v1, Lczs;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lczs;

    iput-object v0, p0, Lczm;->R:Lczs;

    .line 110
    return-void
.end method

.method public d()V
    .locals 0

    .prologue
    .line 379
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 383
    const/4 v0, 0x0

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const-wide/16 v6, 0x0

    .line 227
    iget-object v0, p0, Lczm;->S:Lddl;

    invoke-interface {v0}, Lddl;->a()Lizu;

    move-result-object v2

    .line 228
    iget-object v0, p0, Lczm;->Y:Ljuk;

    invoke-virtual {v0, v2}, Ljuk;->b(Lizu;)Z

    move-result v0

    .line 229
    if-eqz v0, :cond_1

    .line 230
    iget-object v0, p0, Lczm;->Y:Ljuk;

    invoke-virtual {v0}, Ljuk;->d()V

    move v0, v1

    .line 248
    :cond_0
    :goto_0
    invoke-direct {p0, v0}, Lczm;->a(Z)V

    .line 249
    return-void

    .line 233
    :cond_1
    iget-object v3, p0, Lczm;->S:Lddl;

    invoke-interface {v3}, Lddl;->i()Lnzb;

    move-result-object v3

    .line 234
    if-eqz v3, :cond_2

    .line 235
    iget-object v1, p0, Lczm;->Y:Ljuk;

    invoke-virtual {p0}, Lczm;->k()Landroid/os/Bundle;

    move-result-object v4

    iget-object v5, p0, Lczm;->R:Lczs;

    .line 236
    invoke-virtual {v5}, Lczs;->a()I

    move-result v5

    .line 235
    invoke-virtual {v1, v4, v5, v2, v3}, Ljuk;->a(Landroid/os/Bundle;ILizu;Lnzb;)Z

    move-result v1

    .line 237
    if-eqz v1, :cond_0

    .line 238
    iget-object v0, p0, Lczm;->W:Lcom/google/android/apps/plus/views/VideoProgressView;

    invoke-virtual {v0, v6, v7}, Lcom/google/android/apps/plus/views/VideoProgressView;->a(D)V

    .line 239
    iget-object v0, p0, Lczm;->W:Lcom/google/android/apps/plus/views/VideoProgressView;

    invoke-virtual {v0, v6, v7}, Lcom/google/android/apps/plus/views/VideoProgressView;->b(D)V

    .line 240
    const/4 v0, 0x1

    goto :goto_0

    .line 243
    :cond_2
    iget-object v2, p0, Lczm;->Y:Ljuk;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lczm;->Y:Ljuk;

    invoke-virtual {v2}, Ljuk;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 244
    invoke-direct {p0, v1}, Lczm;->c(I)V

    goto :goto_0
.end method

.method public z()V
    .locals 2

    .prologue
    .line 158
    invoke-super {p0}, Llol;->z()V

    .line 159
    iget-object v0, p0, Lczm;->Y:Ljuk;

    iget-object v1, p0, Lczm;->Z:Ljum;

    invoke-virtual {v0, v1}, Ljuk;->b(Ljum;)V

    .line 160
    return-void
.end method
