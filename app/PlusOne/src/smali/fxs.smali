.class public final Lfxs;
.super Lfye;
.source "PG"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/view/View$OnClickListener;
.implements Lfwv;
.implements Ljbb;
.implements Lljh;


# static fields
.field private static a:Z

.field private static b:Ljava/lang/String;

.field private static c:F

.field private static d:Landroid/graphics/drawable/Drawable;

.field private static e:I

.field private static f:Ljava/lang/String;

.field private static g:Ljava/lang/String;

.field private static h:I

.field private static i:I

.field private static j:I

.field private static k:I

.field private static l:I

.field private static m:Ljava/lang/String;

.field private static n:Ljava/lang/String;


# instance fields
.field private A:Landroid/view/View$OnClickListener;

.field private B:Lfxf;

.field private C:Lidh;

.field private D:Llah;

.field private E:Lfwu;

.field private F:I

.field private o:Lcom/google/android/apps/plus/views/EventThemeView;

.field private p:Lfyd;

.field private q:Lfwb;

.field private r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/widget/TextView;

.field private u:Landroid/widget/TextView;

.field private v:Ljava/lang/String;

.field private w:Z

.field private x:Landroid/widget/ImageView;

.field private y:Landroid/view/View;

.field private z:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const/4 v0, 0x0

    sput-boolean v0, Lfxs;->a:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 8

    .prologue
    const v7, 0x7f0203bf

    const/4 v6, 0x0

    const v5, 0x7f0d024c

    const/16 v4, 0x11

    const/4 v3, 0x1

    .line 113
    invoke-direct {p0, p1, p2, p3}, Lfye;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 115
    sget-boolean v0, Lfxs;->a:Z

    if-nez v0, :cond_0

    .line 116
    sput-boolean v3, Lfxs;->a:Z

    .line 118
    invoke-virtual {p0}, Lfxs;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 119
    const v1, 0x7f0d02bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 120
    sput v1, Lfxs;->i:I

    int-to-float v1, v1

    const v2, 0x7f0d02bc

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    mul-float/2addr v1, v2

    float-to-int v1, v1

    sput v1, Lfxs;->j:I

    .line 122
    const v1, 0x7f0d02b7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxs;->k:I

    .line 123
    const v1, 0x7f0d02c0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lfxs;->l:I

    .line 125
    const v1, 0x7f0a073a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lfxs;->b:Ljava/lang/String;

    .line 126
    const v1, 0x7f0b013a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lfxs;->e:I

    .line 127
    const v1, 0x7f0a073b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lfxs;->f:Ljava/lang/String;

    .line 128
    const v1, 0x7f0a073c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lfxs;->g:Ljava/lang/String;

    .line 129
    const v1, 0x7f0b013d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    sput v1, Lfxs;->h:I

    .line 130
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    sput v1, Lfxs;->c:F

    .line 131
    const v1, 0x7f0200ad

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    sput-object v1, Lfxs;->d:Landroid/graphics/drawable/Drawable;

    .line 133
    const v1, 0x7f0a02dd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lfxs;->m:Ljava/lang/String;

    .line 134
    const v1, 0x7f0a02de

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lfxs;->n:Ljava/lang/String;

    .line 137
    :cond_0
    invoke-virtual {p0}, Lfxs;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 139
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_1

    .line 140
    new-instance v1, Lfwb;

    invoke-direct {v1, p1}, Lfwb;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lfxs;->q:Lfwb;

    .line 141
    iget-object v1, p0, Lfxs;->q:Lfwb;

    invoke-virtual {p0, v1}, Lfxs;->addView(Landroid/view/View;)V

    .line 144
    :cond_1
    new-instance v1, Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-direct {v1, p1}, Lcom/google/android/apps/plus/views/EventThemeView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    .line 145
    iget-object v1, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/plus/views/EventThemeView;->l(Z)V

    .line 146
    iget-object v1, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {p0, v1}, Lfxs;->addView(Landroid/view/View;)V

    .line 148
    new-instance v1, Lfyd;

    invoke-direct {v1, p1, p2, p3}, Lfyd;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-object v1, p0, Lfxs;->p:Lfyd;

    .line 149
    iget-object v1, p0, Lfxs;->p:Lfyd;

    invoke-virtual {p0, v1}, Lfxs;->addView(Landroid/view/View;)V

    .line 151
    new-instance v1, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lfxs;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 152
    iget-object v1, p0, Lfxs;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v1, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(I)V

    .line 153
    iget-object v1, p0, Lfxs;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p0, v1}, Lfxs;->addView(Landroid/view/View;)V

    .line 155
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lfxs;->s:Landroid/widget/TextView;

    .line 156
    iget-object v1, p0, Lfxs;->s:Landroid/widget/TextView;

    invoke-static {p1, v1, v4}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 157
    iget-object v1, p0, Lfxs;->s:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lfxs;->addView(Landroid/view/View;)V

    .line 159
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lfxs;->x:Landroid/widget/ImageView;

    .line 160
    iget-object v1, p0, Lfxs;->x:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 161
    iget-object v1, p0, Lfxs;->x:Landroid/widget/ImageView;

    sget-object v2, Lfxs;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 162
    iput v7, p0, Lfxs;->F:I

    .line 163
    iget-object v1, p0, Lfxs;->x:Landroid/widget/ImageView;

    invoke-virtual {p0, v1}, Lfxs;->addView(Landroid/view/View;)V

    .line 165
    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lfxs;->y:Landroid/view/View;

    .line 166
    iget-object v1, p0, Lfxs;->y:Landroid/view/View;

    sget-object v2, Lfxs;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 167
    iget-object v1, p0, Lfxs;->y:Landroid/view/View;

    invoke-virtual {p0, v1}, Lfxs;->addView(Landroid/view/View;)V

    .line 169
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lfxs;->t:Landroid/widget/TextView;

    .line 170
    iget-object v1, p0, Lfxs;->t:Landroid/widget/TextView;

    .line 171
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 170
    invoke-virtual {v1, v6, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 172
    iget-object v1, p0, Lfxs;->t:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->setSingleLine()V

    .line 173
    iget-object v1, p0, Lfxs;->t:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 174
    iget-object v1, p0, Lfxs;->t:Landroid/widget/TextView;

    invoke-virtual {p0, v1}, Lfxs;->addView(Landroid/view/View;)V

    .line 176
    new-instance v1, Landroid/widget/TextView;

    invoke-direct {v1, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lfxs;->u:Landroid/widget/TextView;

    .line 177
    iget-object v1, p0, Lfxs;->u:Landroid/widget/TextView;

    .line 178
    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 177
    invoke-virtual {v1, v6, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 179
    iget-object v0, p0, Lfxs;->u:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 180
    iget-object v0, p0, Lfxs;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setGravity(I)V

    .line 181
    iget-object v0, p0, Lfxs;->u:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfxs;->addView(Landroid/view/View;)V

    .line 183
    new-instance v0, Landroid/widget/TextView;

    invoke-direct {v0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lfxs;->z:Landroid/widget/TextView;

    .line 184
    iget-object v0, p0, Lfxs;->z:Landroid/widget/TextView;

    const/16 v1, 0xd

    invoke-static {p1, v0, v1}, Llhx;->a(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 186
    iget-object v0, p0, Lfxs;->z:Landroid/widget/TextView;

    sget-object v1, Lfxs;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 187
    iget-object v0, p0, Lfxs;->z:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->setSingleLine()V

    .line 188
    iget-object v0, p0, Lfxs;->z:Landroid/widget/TextView;

    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 189
    iget-object v0, p0, Lfxs;->z:Landroid/widget/TextView;

    invoke-virtual {p0, v0}, Lfxs;->addView(Landroid/view/View;)V

    .line 190
    return-void
.end method


# virtual methods
.method protected a(II)I
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x1

    .line 318
    invoke-virtual {p0}, Lfxs;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 320
    iget-object v3, p0, Lfxs;->D:Llah;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lfxs;->D:Llah;

    invoke-virtual {v3}, Llah;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    move v5, v0

    .line 321
    :goto_0
    iget-object v3, p0, Lfxs;->D:Llah;

    if-nez v3, :cond_1

    move v3, v0

    .line 322
    :goto_1
    invoke-virtual {p0}, Lfxs;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v6, 0x7f0a059c

    new-array v7, v0, [Ljava/lang/Object;

    .line 323
    invoke-static {v3, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v2

    .line 322
    invoke-virtual {v4, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 325
    iget-object v0, p0, Lfxs;->E:Lfwu;

    invoke-virtual {p0, v0}, Lfxs;->b(Llip;)V

    .line 326
    new-instance v0, Lfwu;

    if-eqz v5, :cond_2

    sget-object v3, Lfuu;->g:Landroid/text/TextPaint;

    :goto_2
    if-eqz v5, :cond_3

    sget-object v4, Lfuu;->d:Landroid/graphics/drawable/NinePatchDrawable;

    :goto_3
    if-eqz v5, :cond_4

    sget-object v5, Lfuu;->e:Landroid/graphics/drawable/NinePatchDrawable;

    :goto_4
    move-object v6, p0

    move v7, p1

    move v8, p2

    invoke-direct/range {v0 .. v8}, Lfwu;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;Landroid/text/TextPaint;Landroid/graphics/drawable/NinePatchDrawable;Landroid/graphics/drawable/NinePatchDrawable;Lfwv;II)V

    iput-object v0, p0, Lfxs;->E:Lfwu;

    .line 334
    iget-object v0, p0, Lfxs;->E:Lfwu;

    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lfxs;->E:Lfwu;

    invoke-virtual {v1}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Rect;->width()I

    move-result v1

    sub-int v1, p1, v1

    iget-object v2, p0, Lfxs;->E:Lfwu;

    .line 335
    invoke-virtual {v2}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v2

    sub-int v2, p2, v2

    .line 334
    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->offsetTo(II)V

    .line 336
    iget-object v0, p0, Lfxs;->E:Lfwu;

    invoke-virtual {p0, v0}, Lfxs;->a(Llip;)V

    .line 338
    iget-object v0, p0, Lfxs;->E:Lfwu;

    invoke-virtual {v0}, Lfwu;->a()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    return v0

    :cond_0
    move v5, v2

    .line 320
    goto :goto_0

    .line 321
    :cond_1
    iget-object v3, p0, Lfxs;->D:Llah;

    invoke-virtual {v3}, Llah;->b()I

    move-result v3

    goto :goto_1

    .line 326
    :cond_2
    sget-object v3, Lfuu;->f:Landroid/text/TextPaint;

    goto :goto_2

    :cond_3
    sget-object v4, Lfuu;->b:Landroid/graphics/drawable/NinePatchDrawable;

    goto :goto_3

    :cond_4
    sget-object v5, Lfuu;->c:Landroid/graphics/drawable/NinePatchDrawable;

    goto :goto_4
.end method

.method public a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 460
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfxs;->w:Z

    .line 461
    iget-object v0, p0, Lfxs;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 462
    iput-object v1, p0, Lfxs;->v:Ljava/lang/String;

    .line 464
    iget-object v0, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventThemeView;->a()V

    .line 465
    iget-object v0, p0, Lfxs;->p:Lfyd;

    invoke-virtual {v0}, Lfyd;->a()V

    .line 468
    iget-object v0, p0, Lfxs;->y:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 469
    iget-object v0, p0, Lfxs;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 470
    iget-object v0, p0, Lfxs;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 471
    iget-object v0, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EventThemeView;->a(Ljbb;)V

    .line 473
    iget-object v0, p0, Lfxs;->q:Lfwb;

    if-eqz v0, :cond_0

    .line 474
    iget-object v0, p0, Lfxs;->q:Lfwb;

    invoke-virtual {v0, v1}, Lfwb;->a(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 475
    iget-object v0, p0, Lfxs;->q:Lfwb;

    invoke-virtual {v0, v1}, Lfwb;->a(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 476
    iget-object v0, p0, Lfxs;->q:Lfwb;

    invoke-virtual {v0}, Lfwb;->a()V

    .line 479
    :cond_0
    iput-object v1, p0, Lfxs;->A:Landroid/view/View$OnClickListener;

    .line 480
    return-void
.end method

.method protected a(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 575
    iget-boolean v0, p0, Lfxs;->w:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfxs;->E:Lfwu;

    if-eqz v0, :cond_0

    .line 576
    iget-object v0, p0, Lfxs;->E:Lfwu;

    invoke-virtual {v0, p1}, Lfwu;->a(Landroid/graphics/Canvas;)V

    .line 578
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/libraries/social/media/ui/MediaView;)V
    .locals 2

    .prologue
    .line 536
    iget-object v0, p0, Lfxs;->q:Lfwb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfxs;->v:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 537
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    .line 538
    iget-object v1, p0, Lfxs;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 539
    iget-object v0, p0, Lfxs;->q:Lfwb;

    iget-object v1, p0, Lfxs;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lfwb;->a(Ljava/lang/String;)V

    .line 540
    iget-object v0, p0, Lfxs;->q:Lfwb;

    invoke-virtual {v0, p0}, Lfwb;->a(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 541
    iget-object v0, p0, Lfxs;->q:Lfwb;

    invoke-virtual {v0, p0}, Lfwb;->a(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 543
    :cond_0
    return-void
.end method

.method public a(Lfwu;)V
    .locals 2

    .prologue
    .line 547
    iget-object v0, p0, Lfxs;->E:Lfwu;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lfxs;->B:Lfxf;

    if-eqz v0, :cond_1

    .line 549
    iget-object v0, p0, Lfxs;->q:Lfwb;

    if-eqz v0, :cond_0

    .line 550
    iget-object v0, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventThemeView;->clearAnimation()V

    .line 551
    iget-object v0, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EventThemeView;->setVisibility(I)V

    .line 552
    iget-object v0, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EventThemeView;->setAlpha(F)V

    .line 553
    invoke-virtual {p0}, Lfxs;->invalidate()V

    .line 556
    :cond_0
    iget-object v0, p0, Lfxs;->B:Lfxf;

    invoke-interface {v0}, Lfxf;->ac()V

    .line 558
    :cond_1
    return-void
.end method

.method public a(Lidh;Leaq;Llah;Ljava/lang/String;Landroid/view/View$OnClickListener;ZLfxf;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v0, 0x1

    const/16 v6, 0x8

    const/4 v1, 0x0

    .line 344
    iput-object p1, p0, Lfxs;->C:Lidh;

    .line 345
    iput-object p3, p0, Lfxs;->D:Llah;

    .line 347
    iget-object v2, p0, Lfxs;->D:Llah;

    if-nez v2, :cond_0

    .line 348
    new-instance v2, Llah;

    invoke-direct {v2}, Llah;-><init>()V

    iput-object v2, p0, Lfxs;->D:Llah;

    .line 351
    :cond_0
    iget-object v2, p0, Lfxs;->C:Lidh;

    invoke-virtual {v2}, Lidh;->i()Lpbj;

    move-result-object v2

    .line 354
    iget-object v3, p0, Lfxs;->C:Lidh;

    invoke-virtual {v3}, Lidh;->g()I

    move-result v3

    if-nez v3, :cond_4

    .line 355
    iget-object v0, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EventThemeView;->setVisibility(I)V

    .line 356
    iget-object v0, v2, Lpbj;->d:Llto;

    invoke-virtual {p0, v0}, Lfxs;->a(Llto;)V

    .line 357
    iget-object v0, p0, Lfxs;->p:Lfyd;

    invoke-virtual {v0, v6}, Lfyd;->setVisibility(I)V

    move v0, v1

    .line 378
    :goto_0
    iget-object v3, p0, Lfxs;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iget-object v4, p0, Lfxs;->C:Lidh;

    invoke-virtual {v4}, Lidh;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, p4}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 380
    iget-object v3, p0, Lfxs;->C:Lidh;

    invoke-virtual {v3}, Lidh;->k()Lpai;

    move-result-object v3

    .line 381
    if-eqz v3, :cond_1

    iget-object v4, v3, Lpai;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 382
    iget-object v3, v3, Lpai;->c:Ljava/lang/String;

    .line 383
    iget-object v4, p0, Lfxs;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v4, v3}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 384
    iget-object v4, p0, Lfxs;->t:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 387
    :cond_1
    iget-object v3, p0, Lfxs;->s:Landroid/widget/TextView;

    invoke-virtual {p1}, Lidh;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 389
    iget-object v3, p0, Lfxs;->z:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lfxs;->removeView(Landroid/view/View;)V

    .line 390
    iget-object v3, p0, Lfxs;->x:Landroid/widget/ImageView;

    invoke-virtual {p0, v3}, Lfxs;->removeView(Landroid/view/View;)V

    .line 391
    iget-object v3, p0, Lfxs;->y:Landroid/view/View;

    invoke-virtual {p0, v3}, Lfxs;->removeView(Landroid/view/View;)V

    .line 393
    if-eqz p5, :cond_2

    .line 394
    iget-object v3, p0, Lfxs;->x:Landroid/widget/ImageView;

    invoke-virtual {p0, v3}, Lfxs;->addView(Landroid/view/View;)V

    .line 395
    iget-object v3, p0, Lfxs;->y:Landroid/view/View;

    invoke-virtual {p0, v3}, Lfxs;->addView(Landroid/view/View;)V

    .line 397
    if-eqz p6, :cond_2

    .line 398
    iget-object v3, p0, Lfxs;->z:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lfxs;->addView(Landroid/view/View;)V

    .line 402
    :cond_2
    iput-object p5, p0, Lfxs;->A:Landroid/view/View$OnClickListener;

    .line 403
    iput-object p7, p0, Lfxs;->B:Lfxf;

    .line 405
    iget-object v3, v2, Lpbj;->a:Lpbe;

    .line 406
    if-eqz v3, :cond_6

    iget-object v4, v3, Lpbe;->e:Ljava/lang/Boolean;

    if-eqz v4, :cond_6

    iget-object v3, v3, Lpbe;->e:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_6

    .line 407
    iget-object v2, p0, Lfxs;->u:Landroid/widget/TextView;

    sget-object v3, Lfxs;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 408
    iget-object v2, p0, Lfxs;->u:Landroid/widget/TextView;

    sget v3, Lfxs;->e:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 409
    iget-object v2, p0, Lfxs;->u:Landroid/widget/TextView;

    sget-object v3, Lfxs;->d:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 410
    iget-object v2, p0, Lfxs;->u:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 423
    :goto_1
    iget-object v2, p0, Lfxs;->u:Landroid/widget/TextView;

    sget v3, Lfxs;->c:F

    invoke-virtual {v2, v1, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 425
    iget-object v1, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v1, p0}, Lcom/google/android/apps/plus/views/EventThemeView;->a(Ljbb;)V

    .line 426
    if-eqz v0, :cond_3

    .line 427
    iget-object v0, p0, Lfxs;->p:Lfyd;

    invoke-virtual {v0, p0}, Lfyd;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 429
    :cond_3
    iget-object v0, p0, Lfxs;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 430
    iget-object v0, p0, Lfxs;->y:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 432
    iget-boolean v0, p2, Leaq;->l:Z

    iput-boolean v0, p0, Lfxs;->w:Z

    .line 433
    return-void

    .line 362
    :cond_4
    iget-object v3, p0, Lfxs;->C:Lidh;

    invoke-virtual {v3}, Lidh;->b()Lozp;

    move-result-object v3

    .line 363
    iget-object v4, v2, Lpbj;->d:Llto;

    iget-object v4, v4, Llto;->b:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const v5, 0x77fcb496

    if-eq v4, v5, :cond_5

    .line 364
    iget-object v4, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v4, v1}, Lcom/google/android/apps/plus/views/EventThemeView;->setVisibility(I)V

    .line 365
    iget-object v4, v2, Lpbj;->d:Llto;

    invoke-virtual {p0, v4}, Lfxs;->a(Llto;)V

    .line 369
    :goto_2
    iget-object v4, p0, Lfxs;->p:Lfyd;

    invoke-virtual {v4, v1}, Lfyd;->setVisibility(I)V

    .line 370
    iget-object v4, p0, Lfxs;->p:Lfyd;

    invoke-virtual {v4, v3, v7, p4, v0}, Lfyd;->a(Lozp;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 373
    invoke-virtual {p0}, Lfxs;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 374
    const-class v4, Lhee;

    invoke-static {v3, v4}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    goto/16 :goto_0

    .line 367
    :cond_5
    iget-object v4, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v4, v6}, Lcom/google/android/apps/plus/views/EventThemeView;->setVisibility(I)V

    goto :goto_2

    .line 411
    :cond_6
    iget-object v3, v2, Lpbj;->b:Lpbd;

    iget-object v3, v3, Lpbd;->b:Lpbc;

    if-eqz v3, :cond_8

    iget-object v3, v2, Lpbj;->b:Lpbd;

    iget-object v3, v3, Lpbd;->b:Lpbc;

    iget-object v3, v3, Lpbc;->a:Ljava/lang/Boolean;

    if-eqz v3, :cond_8

    .line 413
    iget-object v3, p0, Lfxs;->u:Landroid/widget/TextView;

    iget-object v2, v2, Lpbj;->b:Lpbd;

    iget-object v2, v2, Lpbd;->b:Lpbc;

    iget-object v2, v2, Lpbc;->a:Ljava/lang/Boolean;

    .line 414
    invoke-static {v2}, Llsl;->a(Ljava/lang/Boolean;)Z

    move-result v2

    if-eqz v2, :cond_7

    sget-object v2, Lfxs;->g:Ljava/lang/String;

    .line 413
    :goto_3
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 416
    iget-object v2, p0, Lfxs;->u:Landroid/widget/TextView;

    sget v3, Lfxs;->h:I

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 417
    iget-object v2, p0, Lfxs;->u:Landroid/widget/TextView;

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 418
    iget-object v2, p0, Lfxs;->u:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1

    .line 414
    :cond_7
    sget-object v2, Lfxs;->f:Ljava/lang/String;

    goto :goto_3

    .line 420
    :cond_8
    iget-object v2, p0, Lfxs;->u:Landroid/widget/TextView;

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method public a(Llto;)V
    .locals 6

    .prologue
    .line 193
    iget-object v0, p0, Lfxs;->q:Lfwb;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p1, Llto;->c:[Lltp;

    if-eqz v0, :cond_0

    .line 194
    iget-object v1, p1, Llto;->c:[Lltp;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 195
    const-string v4, "MOV"

    iget v5, v3, Lltp;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "WITH_DATA"

    iget v5, v3, Lltp;->c:I

    .line 196
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, v3, Lltp;->d:Ljava/lang/String;

    const-string v5, "mp4"

    .line 197
    invoke-virtual {v4, v5}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 198
    iget-object v0, p0, Lfxs;->v:Ljava/lang/String;

    iget-object v1, v3, Lltp;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 199
    iget-object v0, v3, Lltp;->d:Ljava/lang/String;

    iput-object v0, p0, Lfxs;->v:Ljava/lang/String;

    .line 200
    iget-object v0, p0, Lfxs;->q:Lfwb;

    invoke-virtual {v0}, Lfwb;->a()V

    .line 208
    :cond_0
    iget-object v0, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/plus/views/EventThemeView;->a(Llto;)V

    .line 209
    return-void

    .line 194
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 494
    if-eqz p1, :cond_1

    const v0, 0x7f0203c1

    :goto_0
    iput v0, p0, Lfxs;->F:I

    .line 497
    iget-object v1, p0, Lfxs;->z:Landroid/widget/TextView;

    if-eqz p1, :cond_2

    sget-object v0, Lfxs;->n:Ljava/lang/String;

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 498
    iget-object v0, p0, Lfxs;->x:Landroid/widget/ImageView;

    iget v1, p0, Lfxs;->F:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 499
    iget-object v1, p0, Lfxs;->x:Landroid/widget/ImageView;

    if-eqz p1, :cond_3

    sget-object v0, Lfxs;->n:Ljava/lang/String;

    :goto_2
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 500
    iget-object v1, p0, Lfxs;->y:Landroid/view/View;

    if-eqz p1, :cond_4

    sget-object v0, Lfxs;->n:Ljava/lang/String;

    :goto_3
    invoke-virtual {v1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 502
    iget-object v0, p0, Lfxs;->B:Lfxf;

    if-eqz v0, :cond_0

    .line 503
    iget-object v0, p0, Lfxs;->B:Lfxf;

    invoke-interface {v0, p1}, Lfxf;->b(Z)V

    .line 505
    :cond_0
    return-void

    .line 494
    :cond_1
    const v0, 0x7f0203bf

    goto :goto_0

    .line 497
    :cond_2
    sget-object v0, Lfxs;->m:Ljava/lang/String;

    goto :goto_1

    .line 499
    :cond_3
    sget-object v0, Lfxs;->m:Ljava/lang/String;

    goto :goto_2

    .line 500
    :cond_4
    sget-object v0, Lfxs;->m:Ljava/lang/String;

    goto :goto_3
.end method

.method public b()V
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Lfxs;->p:Lfyd;

    invoke-virtual {v0}, Lfyd;->b()V

    .line 437
    return-void
.end method

.method public b(Z)V
    .locals 2

    .prologue
    .line 511
    iget-object v1, p0, Lfxs;->z:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 512
    return-void

    .line 511
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 440
    iget-object v0, p0, Lfxs;->q:Lfwb;

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Lfxs;->q:Lfwb;

    invoke-virtual {v0}, Lfwb;->a()V

    .line 443
    :cond_0
    return-void
.end method

.method public d()V
    .locals 2

    .prologue
    .line 446
    iget-object v0, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfxs;->q:Lfwb;

    if-eqz v0, :cond_0

    .line 447
    iget-object v0, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventThemeView;->clearAnimation()V

    .line 448
    iget-object v0, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/google/android/apps/plus/views/EventThemeView;->setAlpha(F)V

    .line 450
    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Lfxs;->q:Lfwb;

    if-eqz v0, :cond_0

    .line 454
    iget-object v0, p0, Lfxs;->q:Lfwb;

    invoke-virtual {v0}, Lfwb;->d()V

    .line 456
    :cond_0
    return-void
.end method

.method protected l_(I)Landroid/graphics/Point;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 563
    iget-object v0, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventThemeView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 564
    iget-object v0, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    .line 565
    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventThemeView;->getMeasuredWidth()I

    move-result v0

    sget v1, Lfxs;->k:I

    sub-int/2addr v0, v1

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget-object v1, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    .line 566
    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/EventThemeView;->getMeasuredHeight()I

    move-result v1

    sget v2, Lfxs;->k:I

    sub-int/2addr v1, v2

    invoke-static {v1, v3}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 564
    invoke-virtual {p0, v0, v1}, Lfxs;->a(II)I

    move-result v1

    .line 567
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0, p1, v1}, Landroid/graphics/Point;-><init>(II)V

    .line 570
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected measureChildren(II)V
    .locals 12

    .prologue
    const/high16 v11, -0x80000000

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v2, 0x0

    .line 213
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 214
    iget-object v1, p0, Lfxs;->A:Landroid/view/View$OnClickListener;

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    move v4, v1

    .line 216
    :goto_0
    if-nez v0, :cond_0

    .line 217
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 220
    :cond_0
    iget-object v1, p0, Lfxs;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    sget v3, Lfxs;->i:I

    sget v5, Lfxs;->i:I

    invoke-static {v1, v3, v8, v5, v8}, Lfxs;->a(Landroid/view/View;IIII)V

    .line 224
    iget-object v1, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/views/EventThemeView;->getVisibility()I

    move-result v1

    const/16 v3, 0x8

    if-eq v1, v3, :cond_7

    .line 225
    invoke-static {v0}, Ldrm;->a(I)I

    move-result v1

    .line 226
    iget-object v3, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-static {v3, v0, v8, v1, v8}, Lfxs;->a(Landroid/view/View;IIII)V

    .line 227
    iget-object v1, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-static {v1, v2, v2}, Lfxs;->a(Landroid/view/View;II)V

    .line 229
    iget-object v1, p0, Lfxs;->q:Lfwb;

    if-eqz v1, :cond_1

    .line 230
    iget-object v1, p0, Lfxs;->q:Lfwb;

    iget-object v3, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    .line 231
    invoke-virtual {v3}, Lcom/google/android/apps/plus/views/EventThemeView;->getMeasuredWidth()I

    move-result v3

    iget-object v5, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    .line 232
    invoke-virtual {v5}, Lcom/google/android/apps/plus/views/EventThemeView;->getMeasuredHeight()I

    move-result v5

    .line 230
    invoke-static {v1, v3, v8, v5, v8}, Lfxs;->a(Landroid/view/View;IIII)V

    .line 234
    iget-object v1, p0, Lfxs;->q:Lfwb;

    iget-object v3, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-static {v3, v2}, Lfxs;->a(Landroid/view/View;I)I

    move-result v3

    iget-object v5, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    const/4 v6, 0x1

    .line 235
    invoke-static {v5, v6}, Lfxs;->a(Landroid/view/View;I)I

    move-result v5

    .line 234
    invoke-static {v1, v3, v5}, Lfxs;->a(Landroid/view/View;II)V

    .line 238
    :cond_1
    iget-object v1, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-static {v1, v10}, Lfxs;->a(Landroid/view/View;I)I

    move-result v3

    .line 239
    iget-object v1, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-static {v1, v9}, Lfxs;->a(Landroid/view/View;I)I

    move-result v1

    .line 241
    :goto_1
    iget-object v5, p0, Lfxs;->p:Lfyd;

    invoke-virtual {v5}, Lfyd;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_6

    .line 242
    iget-object v5, p0, Lfxs;->p:Lfyd;

    invoke-static {v5, v0, v8, v2, v2}, Lfxs;->a(Landroid/view/View;IIII)V

    .line 243
    iget-object v0, p0, Lfxs;->p:Lfyd;

    invoke-static {v0, v2, v3}, Lfxs;->a(Landroid/view/View;II)V

    .line 245
    iget-object v0, p0, Lfxs;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iget-object v3, p0, Lfxs;->p:Lfyd;

    invoke-static {v3, v2}, Lfxs;->a(Landroid/view/View;I)I

    move-result v3

    sget v5, Lfxs;->k:I

    add-int/2addr v3, v5

    iget-object v5, p0, Lfxs;->p:Lfyd;

    .line 246
    invoke-static {v5, v10}, Lfxs;->a(Landroid/view/View;I)I

    move-result v5

    sget v6, Lfxs;->k:I

    add-int/2addr v5, v6

    .line 245
    invoke-static {v0, v3, v5}, Lfxs;->a(Landroid/view/View;II)V

    .line 249
    iget-object v0, p0, Lfxs;->p:Lfyd;

    invoke-static {v0, v10}, Lfxs;->a(Landroid/view/View;I)I

    move-result v3

    .line 251
    iget-object v0, p0, Lfxs;->p:Lfyd;

    .line 252
    invoke-static {v0, v9}, Lfxs;->a(Landroid/view/View;I)I

    move-result v0

    .line 251
    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 259
    :goto_2
    iget-object v0, p0, Lfxs;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-static {v0, v9}, Lfxs;->a(Landroid/view/View;I)I

    move-result v0

    sget v5, Lfxs;->k:I

    add-int/2addr v0, v5

    .line 260
    sget v5, Lfxs;->k:I

    sub-int v5, v1, v5

    .line 261
    sub-int v0, v5, v0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 263
    if-eqz v4, :cond_2

    .line 264
    iget-object v6, p0, Lfxs;->x:Landroid/widget/ImageView;

    invoke-static {v6, v2, v2, v2, v2}, Lfxs;->a(Landroid/view/View;IIII)V

    .line 267
    iget-object v6, p0, Lfxs;->x:Landroid/widget/ImageView;

    .line 268
    invoke-virtual {v6}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v6

    sget v7, Lfxs;->l:I

    add-int/2addr v6, v7

    sub-int/2addr v0, v6

    .line 267
    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 270
    iget-object v6, p0, Lfxs;->x:Landroid/widget/ImageView;

    iget-object v7, p0, Lfxs;->x:Landroid/widget/ImageView;

    .line 271
    invoke-virtual {v7}, Landroid/widget/ImageView;->getMeasuredWidth()I

    move-result v7

    sub-int/2addr v5, v7

    sget v7, Lfxs;->k:I

    add-int/2addr v7, v3

    .line 270
    invoke-static {v6, v5, v7}, Lfxs;->a(Landroid/view/View;II)V

    .line 275
    :cond_2
    iget-object v5, p0, Lfxs;->s:Landroid/widget/TextView;

    invoke-static {v5, v0, v11, v2, v2}, Lfxs;->a(Landroid/view/View;IIII)V

    .line 276
    iget-object v5, p0, Lfxs;->s:Landroid/widget/TextView;

    iget-object v6, p0, Lfxs;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 277
    invoke-static {v6, v9}, Lfxs;->a(Landroid/view/View;I)I

    move-result v6

    sget v7, Lfxs;->k:I

    add-int/2addr v6, v7

    sget v7, Lfxs;->k:I

    add-int/2addr v7, v3

    .line 276
    invoke-static {v5, v6, v7}, Lfxs;->a(Landroid/view/View;II)V

    .line 280
    iget-object v5, p0, Lfxs;->t:Landroid/widget/TextView;

    invoke-static {v5, v0, v11, v2, v2}, Lfxs;->a(Landroid/view/View;IIII)V

    .line 281
    iget-object v5, p0, Lfxs;->t:Landroid/widget/TextView;

    iget-object v6, p0, Lfxs;->s:Landroid/widget/TextView;

    .line 282
    invoke-static {v6, v2}, Lfxs;->a(Landroid/view/View;I)I

    move-result v6

    iget-object v7, p0, Lfxs;->s:Landroid/widget/TextView;

    .line 283
    invoke-static {v7, v10}, Lfxs;->a(Landroid/view/View;I)I

    move-result v7

    .line 281
    invoke-static {v5, v6, v7}, Lfxs;->a(Landroid/view/View;II)V

    .line 285
    iget-object v5, p0, Lfxs;->u:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getVisibility()I

    move-result v5

    const/16 v6, 0x8

    if-eq v5, v6, :cond_3

    .line 286
    iget-object v5, p0, Lfxs;->u:Landroid/widget/TextView;

    invoke-static {v5, v0, v11, v2, v2}, Lfxs;->a(Landroid/view/View;IIII)V

    .line 287
    iget-object v5, p0, Lfxs;->u:Landroid/widget/TextView;

    iget-object v6, p0, Lfxs;->t:Landroid/widget/TextView;

    .line 288
    invoke-static {v6, v2}, Lfxs;->a(Landroid/view/View;I)I

    move-result v6

    iget-object v7, p0, Lfxs;->t:Landroid/widget/TextView;

    .line 289
    invoke-static {v7, v10}, Lfxs;->a(Landroid/view/View;I)I

    move-result v7

    .line 287
    invoke-static {v5, v6, v7}, Lfxs;->a(Landroid/view/View;II)V

    .line 292
    :cond_3
    iget-object v5, p0, Lfxs;->t:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredHeight()I

    move-result v5

    iget-object v6, p0, Lfxs;->u:Landroid/widget/TextView;

    .line 293
    invoke-virtual {v6}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v6

    iget-object v7, p0, Lfxs;->s:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v7

    invoke-static {v6, v7}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 292
    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v5

    sget v6, Lfxs;->k:I

    add-int/2addr v5, v6

    .line 295
    sub-int/2addr v0, v5

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 297
    if-eqz v4, :cond_4

    .line 298
    iget-object v4, p0, Lfxs;->z:Landroid/widget/TextView;

    invoke-static {v4, v0, v11, v2, v2}, Lfxs;->a(Landroid/view/View;IIII)V

    .line 300
    iget-object v0, p0, Lfxs;->z:Landroid/widget/TextView;

    iget-object v4, p0, Lfxs;->x:Landroid/widget/ImageView;

    .line 301
    invoke-static {v4, v2}, Lfxs;->a(Landroid/view/View;I)I

    move-result v4

    sget v5, Lfxs;->l:I

    sub-int/2addr v4, v5

    iget-object v5, p0, Lfxs;->z:Landroid/widget/TextView;

    .line 302
    invoke-virtual {v5}, Landroid/widget/TextView;->getMeasuredWidth()I

    move-result v5

    sub-int/2addr v4, v5

    iget-object v5, p0, Lfxs;->x:Landroid/widget/ImageView;

    const/4 v6, 0x1

    .line 303
    invoke-static {v5, v6}, Lfxs;->a(Landroid/view/View;I)I

    move-result v5

    .line 300
    invoke-static {v0, v4, v5}, Lfxs;->a(Landroid/view/View;II)V

    .line 304
    new-array v0, v9, [Landroid/view/View;

    iget-object v4, p0, Lfxs;->z:Landroid/widget/TextView;

    aput-object v4, v0, v2

    const/4 v2, 0x1

    iget-object v4, p0, Lfxs;->x:Landroid/widget/ImageView;

    aput-object v4, v0, v2

    invoke-static {v0}, Lfye;->a([Landroid/view/View;)I

    move-result v2

    invoke-static {v2, v0}, Lfye;->a(I[Landroid/view/View;)V

    .line 306
    iget-object v0, p0, Lfxs;->y:Landroid/view/View;

    iget-object v2, p0, Lfxs;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 307
    invoke-static {v2, v9}, Lfxs;->a(Landroid/view/View;I)I

    move-result v2

    sub-int/2addr v1, v2

    .line 309
    invoke-virtual {p0}, Lfxs;->f()I

    move-result v2

    sub-int/2addr v2, v3

    .line 306
    invoke-static {v0, v1, v8, v2, v8}, Lfxs;->a(Landroid/view/View;IIII)V

    .line 311
    iget-object v0, p0, Lfxs;->y:Landroid/view/View;

    iget-object v1, p0, Lfxs;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    .line 312
    invoke-static {v1, v9}, Lfxs;->a(Landroid/view/View;I)I

    move-result v1

    .line 311
    invoke-static {v0, v1, v3}, Lfxs;->a(Landroid/view/View;II)V

    .line 315
    :cond_4
    return-void

    :cond_5
    move v4, v2

    .line 214
    goto/16 :goto_0

    .line 255
    :cond_6
    iget-object v0, p0, Lfxs;->r:Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    iget-object v5, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-static {v5, v2}, Lfxs;->a(Landroid/view/View;I)I

    move-result v5

    sget v6, Lfxs;->k:I

    add-int/2addr v5, v6

    iget-object v6, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    .line 256
    invoke-static {v6, v10}, Lfxs;->a(Landroid/view/View;I)I

    move-result v6

    sget v7, Lfxs;->j:I

    sub-int/2addr v6, v7

    .line 255
    invoke-static {v0, v5, v6}, Lfxs;->a(Landroid/view/View;II)V

    goto/16 :goto_2

    :cond_7
    move v1, v2

    move v3, v2

    goto/16 :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 484
    instance-of v0, p1, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lfxs;->B:Lfxf;

    if-eqz v0, :cond_1

    .line 485
    iget-object v0, p0, Lfxs;->B:Lfxf;

    check-cast p1, Lcom/google/android/libraries/social/avatars/ui/AvatarView;

    invoke-virtual {p1}, Lcom/google/android/libraries/social/avatars/ui/AvatarView;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lfxf;->b(Ljava/lang/String;)V

    .line 491
    :cond_0
    :goto_0
    return-void

    .line 486
    :cond_1
    iget-object v0, p0, Lfxs;->p:Lfyd;

    if-ne p1, v0, :cond_2

    iget-object v0, p0, Lfxs;->B:Lfxf;

    if-eqz v0, :cond_2

    .line 487
    iget-object v0, p0, Lfxs;->B:Lfxf;

    invoke-interface {v0}, Lfxf;->W()V

    goto :goto_0

    .line 488
    :cond_2
    iget-object v0, p0, Lfxs;->A:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lfxs;->A:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    goto :goto_0
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 1

    .prologue
    .line 530
    const/4 v0, 0x1

    return v0
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 516
    iget-object v0, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/views/EventThemeView;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 517
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 518
    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 519
    invoke-virtual {v0, v4}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 520
    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;->setStartOffset(J)V

    .line 521
    iget-object v1, p0, Lfxs;->o:Lcom/google/android/apps/plus/views/EventThemeView;

    invoke-virtual {v1, v0}, Lcom/google/android/apps/plus/views/EventThemeView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 522
    invoke-virtual {p1, v4}, Landroid/media/MediaPlayer;->setLooping(Z)V

    .line 523
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->start()V

    .line 525
    :cond_0
    return-void
.end method
