.class public final Lkzt;
.super Lllq;
.source "PG"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field c:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field d:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:I

.field h:Ljava/lang/String;

.field i:Z

.field j:Z


# direct methods
.method protected constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lllq;-><init>()V

    .line 38
    return-void
.end method

.method public constructor <init>(Lozi;)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 40
    invoke-direct {p0}, Lllq;-><init>()V

    .line 41
    iget-object v0, p1, Lozi;->d:Lpnk;

    if-eqz v0, :cond_1

    .line 42
    iget-object v0, p1, Lozi;->d:Lpnk;

    iget-object v0, v0, Lpnk;->a:Ljava/lang/String;

    iput-object v0, p0, Lkzt;->a:Ljava/lang/String;

    .line 47
    :goto_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkzt;->b:Ljava/util/ArrayList;

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkzt;->c:Ljava/util/ArrayList;

    .line 49
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lkzt;->d:Ljava/util/ArrayList;

    .line 50
    iget-object v0, p1, Lozi;->c:[Loya;

    if-eqz v0, :cond_2

    move v1, v2

    .line 51
    :goto_1
    iget-object v0, p1, Lozi;->c:[Loya;

    array-length v0, v0

    if-ge v1, v0, :cond_2

    .line 52
    iget-object v0, p1, Lozi;->c:[Loya;

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p1, Lozi;->c:[Loya;

    aget-object v0, v0, v1

    sget-object v4, Lpai;->a:Loxr;

    .line 53
    invoke-virtual {v0, v4}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p1, Lozi;->c:[Loya;

    aget-object v0, v0, v1

    sget-object v4, Lpai;->a:Loxr;

    invoke-virtual {v0, v4}, Loya;->a(Loxr;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lpai;

    .line 55
    iget-object v4, p0, Lkzt;->b:Ljava/util/ArrayList;

    iget-object v5, v0, Lpai;->d:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    iget-object v4, p0, Lkzt;->c:Ljava/util/ArrayList;

    iget-object v5, v0, Lpai;->c:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 57
    iget-object v4, p0, Lkzt;->d:Ljava/util/ArrayList;

    iget-object v0, v0, Lpai;->b:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 44
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lkzt;->a:Ljava/lang/String;

    goto :goto_0

    .line 62
    :cond_2
    iget-object v0, p1, Lozi;->f:Ljava/lang/String;

    iput-object v0, p0, Lkzt;->e:Ljava/lang/String;

    .line 63
    iget v0, p1, Lozi;->e:I

    iput v0, p0, Lkzt;->g:I

    .line 64
    iget-object v0, p1, Lozi;->b:Ljava/lang/String;

    iput-object v0, p0, Lkzt;->f:Ljava/lang/String;

    .line 65
    iget-object v0, p1, Lozi;->h:[Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 66
    iget-object v0, p1, Lozi;->h:[Ljava/lang/String;

    array-length v0, v0

    if-eqz v0, :cond_4

    move v0, v3

    :goto_2
    iput-boolean v0, p0, Lkzt;->i:Z

    .line 67
    iget-boolean v0, p0, Lkzt;->i:Z

    if-eqz v0, :cond_3

    .line 68
    iput-boolean v2, p0, Lkzt;->j:Z

    .line 69
    :goto_3
    iget-object v0, p1, Lozi;->h:[Ljava/lang/String;

    array-length v0, v0

    if-ge v2, v0, :cond_3

    .line 70
    const-string v0, "questions"

    iget-object v1, p1, Lozi;->h:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 71
    iput-boolean v3, p0, Lkzt;->j:Z

    .line 77
    :cond_3
    iget-object v0, p1, Lozi;->g:Ljava/lang/String;

    iput-object v0, p0, Lkzt;->h:Ljava/lang/String;

    .line 78
    return-void

    :cond_4
    move v0, v2

    .line 66
    goto :goto_2

    .line 69
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method public static a([B)Lkzt;
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 104
    if-nez p0, :cond_0

    .line 105
    const/4 v0, 0x0

    .line 121
    :goto_0
    return-object v0

    .line 108
    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 109
    new-instance v3, Lkzt;

    invoke-direct {v3}, Lkzt;-><init>()V

    .line 111
    invoke-static {v4}, Lkzt;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lkzt;->a:Ljava/lang/String;

    .line 112
    invoke-static {v4}, Lkzt;->d(Ljava/nio/ByteBuffer;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, v3, Lkzt;->b:Ljava/util/ArrayList;

    .line 113
    invoke-static {v4}, Lkzt;->d(Ljava/nio/ByteBuffer;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, v3, Lkzt;->c:Ljava/util/ArrayList;

    .line 114
    invoke-static {v4}, Lkzt;->d(Ljava/nio/ByteBuffer;)Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, v3, Lkzt;->d:Ljava/util/ArrayList;

    .line 115
    invoke-static {v4}, Lkzt;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lkzt;->e:Ljava/lang/String;

    .line 116
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    iput v0, v3, Lkzt;->g:I

    .line 117
    invoke-static {v4}, Lkzt;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lkzt;->f:Ljava/lang/String;

    .line 118
    invoke-static {v4}, Lkzt;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lkzt;->h:Ljava/lang/String;

    .line 119
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, v3, Lkzt;->i:Z

    .line 120
    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    if-ne v0, v1, :cond_2

    :goto_2
    iput-boolean v1, v3, Lkzt;->j:Z

    move-object v0, v3

    .line 121
    goto :goto_0

    :cond_1
    move v0, v2

    .line 119
    goto :goto_1

    :cond_2
    move v1, v2

    .line 120
    goto :goto_2
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lkzt;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 129
    iget-object v0, p0, Lkzt;->b:Ljava/util/ArrayList;

    return-object v0
.end method

.method public c()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lkzt;->d:Ljava/util/ArrayList;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lkzt;->e:Ljava/lang/String;

    return-object v0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lkzt;->f:Ljava/lang/String;

    return-object v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lkzt;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public g()Z
    .locals 1

    .prologue
    .line 157
    iget v0, p0, Lkzt;->g:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lkzt;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public i()Z
    .locals 1

    .prologue
    .line 165
    iget-boolean v0, p0, Lkzt;->i:Z

    return v0
.end method

.method public j()Z
    .locals 1

    .prologue
    .line 169
    iget-boolean v0, p0, Lkzt;->j:Z

    return v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    iget-object v0, p0, Lkzt;->h:Ljava/lang/String;

    return-object v0
.end method
