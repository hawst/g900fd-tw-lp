.class public final Ljuj;
.super Ljava/lang/Object;
.source "PG"


# instance fields
.field private final a:Landroid/content/Intent;

.field private final b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/Integer;

.field private h:Ljcn;

.field private i:Ljava/lang/Boolean;

.field private j:Ljava/lang/Boolean;

.field private k:Ljava/lang/Boolean;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/Boolean;

.field private n:Ljava/lang/Boolean;

.field private o:Ljava/lang/Integer;

.field private p:Ljava/lang/Integer;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/Boolean;

.field private s:Ljava/lang/String;

.field private t:Ljava/lang/Integer;

.field private u:Ljava/lang/Integer;

.field private v:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/Class;I)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<*>;I)V"
        }
    .end annotation

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1, p2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    .line 87
    iput p3, p0, Ljuj;->b:I

    .line 88
    return-void
.end method


# virtual methods
.method public a()Landroid/content/Intent;
    .locals 3

    .prologue
    .line 222
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 223
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "account_id"

    iget v2, p0, Ljuj;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 225
    iget-object v0, p0, Ljuj;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "cluster_id"

    iget-object v2, p0, Ljuj;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 229
    :cond_0
    iget-object v0, p0, Ljuj;->d:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 230
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "activity_id"

    iget-object v2, p0, Ljuj;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 233
    :cond_1
    iget-object v0, p0, Ljuj;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 234
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "photo_picker_mode"

    iget-object v2, p0, Ljuj;->e:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 237
    :cond_2
    iget-object v0, p0, Ljuj;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 238
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "photo_picker_type"

    iget-object v2, p0, Ljuj;->f:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 241
    :cond_3
    iget-object v0, p0, Ljuj;->h:Ljcn;

    if-eqz v0, :cond_4

    .line 242
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "photo_picker_selected"

    iget-object v2, p0, Ljuj;->h:Ljcn;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 245
    :cond_4
    iget-object v0, p0, Ljuj;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 246
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "photo_picker_crop_mode"

    iget-object v2, p0, Ljuj;->g:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 249
    :cond_5
    iget-object v0, p0, Ljuj;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 254
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "external"

    iget-object v2, p0, Ljuj;->i:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 257
    :cond_6
    iget-object v0, p0, Ljuj;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 258
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "is_for_get_content"

    iget-object v2, p0, Ljuj;->j:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 261
    :cond_7
    iget-object v0, p0, Ljuj;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 262
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "is_for_movie_maker_launch"

    iget-object v2, p0, Ljuj;->k:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 265
    :cond_8
    iget-object v0, p0, Ljuj;->l:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 266
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "movie_maker_session_id"

    iget-object v2, p0, Ljuj;->l:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 269
    :cond_9
    iget-object v0, p0, Ljuj;->m:Ljava/lang/Boolean;

    if-eqz v0, :cond_a

    .line 270
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "hide_header"

    iget-object v2, p0, Ljuj;->m:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 273
    :cond_a
    iget-object v0, p0, Ljuj;->o:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 274
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "photo_min_width"

    iget-object v2, p0, Ljuj;->o:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 277
    :cond_b
    iget-object v0, p0, Ljuj;->p:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 278
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "photo_min_height"

    iget-object v2, p0, Ljuj;->p:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 281
    :cond_c
    iget-object v0, p0, Ljuj;->q:Ljava/lang/String;

    if-eqz v0, :cond_d

    .line 282
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "auth_key"

    iget-object v2, p0, Ljuj;->q:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 285
    :cond_d
    iget-object v0, p0, Ljuj;->r:Ljava/lang/Boolean;

    if-eqz v0, :cond_e

    .line 290
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "show_autobackup_status"

    iget-object v2, p0, Ljuj;->r:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 293
    :cond_e
    iget-object v0, p0, Ljuj;->n:Ljava/lang/Boolean;

    if-eqz v0, :cond_f

    .line 294
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "share_only"

    iget-object v2, p0, Ljuj;->n:Ljava/lang/Boolean;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 297
    :cond_f
    iget-object v0, p0, Ljuj;->s:Ljava/lang/String;

    if-eqz v0, :cond_10

    .line 298
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "button_title_res_id"

    iget-object v2, p0, Ljuj;->s:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 301
    :cond_10
    iget-object v0, p0, Ljuj;->t:Ljava/lang/Integer;

    if-eqz v0, :cond_11

    .line 302
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "min_selection_count"

    iget-object v2, p0, Ljuj;->t:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 305
    :cond_11
    iget-object v0, p0, Ljuj;->u:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    .line 306
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "max_selection_count"

    iget-object v2, p0, Ljuj;->u:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 309
    :cond_12
    iget-object v0, p0, Ljuj;->v:Ljava/lang/Integer;

    if-eqz v0, :cond_13

    .line 310
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    const-string v1, "filter"

    iget-object v2, p0, Ljuj;->v:Ljava/lang/Integer;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 313
    :cond_13
    iget-object v0, p0, Ljuj;->a:Landroid/content/Intent;

    return-object v0
.end method

.method public a(I)Ljuj;
    .locals 1

    .prologue
    .line 168
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ljuj;->o:Ljava/lang/Integer;

    .line 169
    return-object p0
.end method

.method public a(Ljava/lang/Integer;)Ljuj;
    .locals 0

    .prologue
    .line 103
    iput-object p1, p0, Ljuj;->e:Ljava/lang/Integer;

    .line 104
    return-object p0
.end method

.method public a(Ljava/lang/String;)Ljuj;
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Ljuj;->c:Ljava/lang/String;

    .line 93
    return-object p0
.end method

.method public a(Ljcn;)Ljuj;
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Ljuj;->h:Ljcn;

    .line 147
    return-object p0
.end method

.method public a(Z)Ljuj;
    .locals 1

    .prologue
    .line 120
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ljuj;->i:Ljava/lang/Boolean;

    .line 121
    return-object p0
.end method

.method public b(I)Ljuj;
    .locals 1

    .prologue
    .line 180
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ljuj;->p:Ljava/lang/Integer;

    .line 181
    return-object p0
.end method

.method public b(Ljava/lang/Integer;)Ljuj;
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Ljuj;->f:Ljava/lang/Integer;

    .line 110
    return-object p0
.end method

.method public b(Ljava/lang/String;)Ljuj;
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Ljuj;->d:Ljava/lang/String;

    .line 98
    return-object p0
.end method

.method public b(Z)Ljuj;
    .locals 1

    .prologue
    .line 125
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ljuj;->j:Ljava/lang/Boolean;

    .line 126
    return-object p0
.end method

.method public c(I)Ljuj;
    .locals 1

    .prologue
    .line 204
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ljuj;->t:Ljava/lang/Integer;

    .line 205
    return-object p0
.end method

.method public c(Ljava/lang/Integer;)Ljuj;
    .locals 0

    .prologue
    .line 115
    iput-object p1, p0, Ljuj;->g:Ljava/lang/Integer;

    .line 116
    return-object p0
.end method

.method public c(Ljava/lang/String;)Ljuj;
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Ljuj;->l:Ljava/lang/String;

    .line 141
    return-object p0
.end method

.method public c(Z)Ljuj;
    .locals 1

    .prologue
    .line 130
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ljuj;->k:Ljava/lang/Boolean;

    .line 131
    return-object p0
.end method

.method public d(I)Ljuj;
    .locals 1

    .prologue
    .line 210
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ljuj;->u:Ljava/lang/Integer;

    .line 211
    return-object p0
.end method

.method public d(Ljava/lang/String;)Ljuj;
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Ljuj;->q:Ljava/lang/String;

    .line 175
    return-object p0
.end method

.method public d(Z)Ljuj;
    .locals 1

    .prologue
    .line 135
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ljuj;->n:Ljava/lang/Boolean;

    .line 136
    return-object p0
.end method

.method public e(I)Ljuj;
    .locals 1

    .prologue
    .line 216
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Ljuj;->v:Ljava/lang/Integer;

    .line 217
    return-object p0
.end method

.method public e(Ljava/lang/String;)Ljuj;
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Ljuj;->s:Ljava/lang/String;

    .line 199
    return-object p0
.end method

.method public e(Z)Ljuj;
    .locals 1

    .prologue
    .line 162
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ljuj;->m:Ljava/lang/Boolean;

    .line 163
    return-object p0
.end method

.method public f(Z)Ljuj;
    .locals 1

    .prologue
    .line 192
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Ljuj;->r:Ljava/lang/Boolean;

    .line 193
    return-object p0
.end method
