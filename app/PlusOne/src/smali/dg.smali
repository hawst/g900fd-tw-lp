.class final Ldg;
.super Ldv;
.source "PG"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ldv",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "TD;>;",
        "Ljava/lang/Runnable;"
    }
.end annotation


# instance fields
.field a:Z

.field private f:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TD;"
        }
    .end annotation
.end field

.field private g:Ljava/util/concurrent/CountDownLatch;

.field private synthetic h:Ldf;


# direct methods
.method constructor <init>(Ldf;)V
    .locals 2

    .prologue
    .line 40
    iput-object p1, p0, Ldg;->h:Ldf;

    invoke-direct {p0}, Ldv;-><init>()V

    .line 45
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Ldg;->g:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method


# virtual methods
.method protected varargs a()Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TD;"
        }
    .end annotation

    .prologue
    .line 51
    iget-object v0, p0, Ldg;->h:Ldf;

    invoke-virtual {v0}, Ldf;->e()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Ldg;->f:Ljava/lang/Object;

    .line 53
    iget-object v0, p0, Ldg;->f:Ljava/lang/Object;

    return-object v0
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TD;)V"
        }
    .end annotation

    .prologue
    .line 61
    :try_start_0
    iget-object v0, p0, Ldg;->h:Ldf;

    invoke-virtual {v0, p0, p1}, Ldf;->b(Ldg;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 63
    iget-object v0, p0, Ldg;->g:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 64
    return-void

    .line 63
    :catchall_0
    move-exception v0

    iget-object v1, p0, Ldg;->g:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
.end method

.method protected b()V
    .locals 2

    .prologue
    .line 71
    :try_start_0
    iget-object v0, p0, Ldg;->h:Ldf;

    iget-object v1, p0, Ldg;->f:Ljava/lang/Object;

    invoke-virtual {v0, p0, v1}, Ldf;->a(Ldg;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    iget-object v0, p0, Ldg;->g:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 74
    return-void

    .line 73
    :catchall_0
    move-exception v0

    iget-object v1, p0, Ldg;->g:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
.end method

.method protected synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 40
    invoke-virtual {p0}, Ldg;->a()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public run()V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    iput-boolean v0, p0, Ldg;->a:Z

    .line 80
    iget-object v0, p0, Ldg;->h:Ldf;

    invoke-virtual {v0}, Ldf;->c()V

    .line 81
    return-void
.end method
