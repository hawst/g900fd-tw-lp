.class public final Lmhm;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnvq;

.field public apiHeader:Llyq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6721
    invoke-direct {p0}, Loxq;-><init>()V

    .line 6724
    iput-object v0, p0, Lmhm;->apiHeader:Llyq;

    .line 6727
    iput-object v0, p0, Lmhm;->a:Lnvq;

    .line 6721
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 6744
    const/4 v0, 0x0

    .line 6745
    iget-object v1, p0, Lmhm;->apiHeader:Llyq;

    if-eqz v1, :cond_0

    .line 6746
    const/4 v0, 0x1

    iget-object v1, p0, Lmhm;->apiHeader:Llyq;

    .line 6747
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6749
    :cond_0
    iget-object v1, p0, Lmhm;->a:Lnvq;

    if-eqz v1, :cond_1

    .line 6750
    const/4 v1, 0x2

    iget-object v2, p0, Lmhm;->a:Lnvq;

    .line 6751
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6753
    :cond_1
    iget-object v1, p0, Lmhm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6754
    iput v0, p0, Lmhm;->ai:I

    .line 6755
    return v0
.end method

.method public a(Loxn;)Lmhm;
    .locals 2

    .prologue
    .line 6763
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 6764
    sparse-switch v0, :sswitch_data_0

    .line 6768
    iget-object v1, p0, Lmhm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 6769
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmhm;->ah:Ljava/util/List;

    .line 6772
    :cond_1
    iget-object v1, p0, Lmhm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6774
    :sswitch_0
    return-object p0

    .line 6779
    :sswitch_1
    iget-object v0, p0, Lmhm;->apiHeader:Llyq;

    if-nez v0, :cond_2

    .line 6780
    new-instance v0, Llyq;

    invoke-direct {v0}, Llyq;-><init>()V

    iput-object v0, p0, Lmhm;->apiHeader:Llyq;

    .line 6782
    :cond_2
    iget-object v0, p0, Lmhm;->apiHeader:Llyq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6786
    :sswitch_2
    iget-object v0, p0, Lmhm;->a:Lnvq;

    if-nez v0, :cond_3

    .line 6787
    new-instance v0, Lnvq;

    invoke-direct {v0}, Lnvq;-><init>()V

    iput-object v0, p0, Lmhm;->a:Lnvq;

    .line 6789
    :cond_3
    iget-object v0, p0, Lmhm;->a:Lnvq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 6764
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 6732
    iget-object v0, p0, Lmhm;->apiHeader:Llyq;

    if-eqz v0, :cond_0

    .line 6733
    const/4 v0, 0x1

    iget-object v1, p0, Lmhm;->apiHeader:Llyq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6735
    :cond_0
    iget-object v0, p0, Lmhm;->a:Lnvq;

    if-eqz v0, :cond_1

    .line 6736
    const/4 v0, 0x2

    iget-object v1, p0, Lmhm;->a:Lnvq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 6738
    :cond_1
    iget-object v0, p0, Lmhm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 6740
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 6717
    invoke-virtual {p0, p1}, Lmhm;->a(Loxn;)Lmhm;

    move-result-object v0

    return-object v0
.end method
