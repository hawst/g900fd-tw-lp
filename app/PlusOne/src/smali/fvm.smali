.class final Lfvm;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field private synthetic a:Landroid/view/ViewTreeObserver;

.field private synthetic b:Landroid/widget/ListView;

.field private synthetic c:Z

.field private synthetic d:Ljava/util/HashMap;

.field private synthetic e:Lfvi;


# direct methods
.method constructor <init>(Lfvi;Landroid/view/ViewTreeObserver;Landroid/widget/ListView;ZLjava/util/HashMap;)V
    .locals 0

    .prologue
    .line 380
    iput-object p1, p0, Lfvm;->e:Lfvi;

    iput-object p2, p0, Lfvm;->a:Landroid/view/ViewTreeObserver;

    iput-object p3, p0, Lfvm;->b:Landroid/widget/ListView;

    iput-boolean p4, p0, Lfvm;->c:Z

    iput-object p5, p0, Lfvm;->d:Ljava/util/HashMap;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 15

    .prologue
    const/4 v12, 0x0

    const/high16 v14, -0x31000000

    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 383
    iget-object v0, p0, Lfvm;->a:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 384
    iget-object v0, p0, Lfvm;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v8

    .line 385
    iget-object v0, p0, Lfvm;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getLastVisiblePosition()I

    move-result v1

    .line 390
    iget-boolean v0, p0, Lfvm;->c:Z

    if-eqz v0, :cond_4

    .line 391
    if-nez v8, :cond_3

    move v0, v7

    .line 392
    :goto_0
    add-int/lit8 v3, v1, 0x1

    iget-object v4, p0, Lfvm;->b:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getCount()I

    move-result v4

    if-ne v3, v4, :cond_0

    add-int/lit8 v1, v1, -0x1

    :cond_0
    move v9, v1

    move v10, v0

    :goto_1
    move v13, v10

    move v3, v14

    .line 398
    :goto_2
    if-gt v13, v9, :cond_b

    .line 399
    iget-object v0, p0, Lfvm;->e:Lfvi;

    iget-object v1, p0, Lfvm;->b:Landroid/widget/ListView;

    iget-object v4, p0, Lfvm;->d:Ljava/util/HashMap;

    invoke-virtual {v0, v1, v13, v4}, Lfvi;->a(Landroid/widget/ListView;ILjava/util/HashMap;)Ljava/lang/Float;

    move-result-object v0

    .line 400
    iget-object v1, p0, Lfvm;->b:Landroid/widget/ListView;

    sub-int v4, v13, v8

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 402
    if-nez v0, :cond_c

    .line 403
    cmpl-float v4, v3, v14

    if-eqz v4, :cond_5

    .line 404
    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    move-object v11, v0

    .line 423
    :goto_3
    iget-object v0, p0, Lfvm;->e:Lfvi;

    invoke-virtual {v0, v1}, Lfvi;->f(Landroid/view/View;)F

    move-result v3

    .line 424
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sub-float v4, v0, v3

    .line 426
    iget-boolean v0, p0, Lfvm;->c:Z

    if-eqz v0, :cond_9

    add-int/lit8 v0, v10, 0x3

    sub-int v5, v9, v10

    add-int/2addr v0, v5

    iget-object v5, p0, Lfvm;->b:Landroid/widget/ListView;

    .line 427
    invoke-virtual {v5}, Landroid/widget/ListView;->getCount()I

    move-result v5

    if-lt v0, v5, :cond_8

    move v0, v7

    .line 434
    :goto_4
    cmpl-float v5, v4, v2

    if-nez v5, :cond_1

    if-ne v13, v9, :cond_1

    if-nez v0, :cond_1

    .line 435
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget-object v5, p0, Lfvm;->b:Landroid/widget/ListView;

    invoke-virtual {v5}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    sub-float/2addr v4, v3

    .line 441
    :cond_1
    if-eqz v10, :cond_2

    cmpl-float v5, v4, v2

    if-nez v5, :cond_2

    if-ne v13, v10, :cond_2

    if-eqz v0, :cond_2

    .line 442
    const/high16 v0, -0x40800000    # -1.0f

    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    iget-object v5, p0, Lfvm;->b:Landroid/widget/ListView;

    .line 443
    invoke-virtual {v5}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v5

    int-to-float v5, v5

    add-float/2addr v4, v5

    sub-float v3, v4, v3

    mul-float v4, v0, v3

    .line 446
    :cond_2
    iget-object v0, p0, Lfvm;->e:Lfvi;

    const/4 v6, 0x0

    move v3, v2

    move v5, v2

    invoke-virtual/range {v0 .. v6}, Lfvi;->a(Landroid/view/View;FFFFLjava/lang/Runnable;)V

    .line 449
    invoke-virtual {v11}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v0, v1

    iget-object v1, p0, Lfvm;->b:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v1

    int-to-float v1, v1

    add-float/2addr v1, v0

    .line 398
    add-int/lit8 v0, v13, 0x1

    move v13, v0

    move v3, v1

    goto/16 :goto_2

    :cond_3
    move v0, v8

    .line 391
    goto/16 :goto_0

    :cond_4
    move v9, v1

    move v10, v8

    .line 395
    goto/16 :goto_1

    .line 406
    :cond_5
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v3

    iget-object v4, p0, Lfvm;->b:Landroid/widget/ListView;

    invoke-virtual {v4}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v4

    add-int/2addr v3, v4

    int-to-float v4, v3

    .line 407
    add-int/lit8 v3, v13, 0x1

    :goto_5
    if-gt v3, v9, :cond_6

    .line 408
    iget-object v5, p0, Lfvm;->e:Lfvi;

    iget-object v6, p0, Lfvm;->b:Landroid/widget/ListView;

    iget-object v11, p0, Lfvm;->d:Ljava/util/HashMap;

    invoke-virtual {v5, v6, v3, v11}, Lfvi;->a(Landroid/widget/ListView;ILjava/util/HashMap;)Ljava/lang/Float;

    move-result-object v5

    .line 409
    if-eqz v5, :cond_7

    .line 410
    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v0

    sub-float/2addr v0, v4

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    .line 417
    :cond_6
    if-nez v0, :cond_c

    .line 418
    neg-float v0, v4

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    move-object v11, v0

    goto/16 :goto_3

    .line 413
    :cond_7
    iget-object v5, p0, Lfvm;->b:Landroid/widget/ListView;

    sub-int v6, v3, v8

    invoke-virtual {v5, v6}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 414
    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v5

    iget-object v6, p0, Lfvm;->b:Landroid/widget/ListView;

    invoke-virtual {v6}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v6

    add-int/2addr v5, v6

    int-to-float v5, v5

    add-float/2addr v4, v5

    .line 407
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    :cond_8
    move v0, v12

    .line 427
    goto/16 :goto_4

    :cond_9
    add-int/lit8 v0, v10, 0x1

    sub-int v5, v9, v10

    add-int/2addr v0, v5

    iget-object v5, p0, Lfvm;->b:Landroid/widget/ListView;

    .line 428
    invoke-virtual {v5}, Landroid/widget/ListView;->getCount()I

    move-result v5

    if-lt v0, v5, :cond_a

    move v0, v7

    goto/16 :goto_4

    :cond_a
    move v0, v12

    goto/16 :goto_4

    .line 452
    :cond_b
    iget-object v0, p0, Lfvm;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 453
    return v7

    :cond_c
    move-object v11, v0

    goto/16 :goto_3
.end method
