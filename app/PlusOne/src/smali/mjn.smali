.class public final Lmjn;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnfv;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 26566
    invoke-direct {p0}, Loxq;-><init>()V

    .line 26569
    iput-object v0, p0, Lmjn;->apiHeader:Llyr;

    .line 26572
    iput-object v0, p0, Lmjn;->a:Lnfv;

    .line 26566
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 26589
    const/4 v0, 0x0

    .line 26590
    iget-object v1, p0, Lmjn;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 26591
    const/4 v0, 0x1

    iget-object v1, p0, Lmjn;->apiHeader:Llyr;

    .line 26592
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 26594
    :cond_0
    iget-object v1, p0, Lmjn;->a:Lnfv;

    if-eqz v1, :cond_1

    .line 26595
    const/4 v1, 0x2

    iget-object v2, p0, Lmjn;->a:Lnfv;

    .line 26596
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26598
    :cond_1
    iget-object v1, p0, Lmjn;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 26599
    iput v0, p0, Lmjn;->ai:I

    .line 26600
    return v0
.end method

.method public a(Loxn;)Lmjn;
    .locals 2

    .prologue
    .line 26608
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 26609
    sparse-switch v0, :sswitch_data_0

    .line 26613
    iget-object v1, p0, Lmjn;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 26614
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmjn;->ah:Ljava/util/List;

    .line 26617
    :cond_1
    iget-object v1, p0, Lmjn;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 26619
    :sswitch_0
    return-object p0

    .line 26624
    :sswitch_1
    iget-object v0, p0, Lmjn;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 26625
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmjn;->apiHeader:Llyr;

    .line 26627
    :cond_2
    iget-object v0, p0, Lmjn;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 26631
    :sswitch_2
    iget-object v0, p0, Lmjn;->a:Lnfv;

    if-nez v0, :cond_3

    .line 26632
    new-instance v0, Lnfv;

    invoke-direct {v0}, Lnfv;-><init>()V

    iput-object v0, p0, Lmjn;->a:Lnfv;

    .line 26634
    :cond_3
    iget-object v0, p0, Lmjn;->a:Lnfv;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 26609
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 26577
    iget-object v0, p0, Lmjn;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 26578
    const/4 v0, 0x1

    iget-object v1, p0, Lmjn;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26580
    :cond_0
    iget-object v0, p0, Lmjn;->a:Lnfv;

    if-eqz v0, :cond_1

    .line 26581
    const/4 v0, 0x2

    iget-object v1, p0, Lmjn;->a:Lnfv;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 26583
    :cond_1
    iget-object v0, p0, Lmjn;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 26585
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 26562
    invoke-virtual {p0, p1}, Lmjn;->a(Loxn;)Lmjn;

    move-result-object v0

    return-object v0
.end method
