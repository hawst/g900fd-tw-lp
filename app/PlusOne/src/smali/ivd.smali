.class public Livd;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Livo;


# instance fields
.field private final a:[Live;

.field private volatile b:Z

.field private volatile c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Livd;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/16 v4, 0x20

    const/4 v1, 0x0

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-array v0, v4, [Live;

    iput-object v0, p0, Livd;->a:[Live;

    .line 45
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    move v0, v1

    .line 48
    :goto_0
    iget-object v2, p0, Livd;->a:[Live;

    if-ge v0, v4, :cond_0

    .line 49
    iget-object v2, p0, Livd;->a:[Live;

    new-instance v3, Live;

    invoke-direct {v3}, Live;-><init>()V

    aput-object v3, v2, v0

    .line 50
    iget-object v2, p0, Livd;->a:[Live;

    aget-object v2, v2, v0

    new-instance v3, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v3}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v3, v2, Live;->a:Ljava/util/concurrent/atomic/AtomicReference;

    .line 48
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_0
    iput-boolean v1, p0, Livd;->b:Z

    .line 54
    iput-boolean v1, p0, Livd;->c:Z

    .line 55
    return-void
.end method


# virtual methods
.method public a(Livm;)V
    .locals 17

    .prologue
    .line 113
    const/4 v4, 0x0

    .line 122
    const/4 v3, 0x0

    .line 123
    const/4 v2, 0x0

    :goto_0
    move-object/from16 v0, p0

    iget-object v5, v0, Livd;->a:[Live;

    const/16 v5, 0x20

    if-ge v2, v5, :cond_6

    .line 124
    move-object/from16 v0, p0

    iget-object v5, v0, Livd;->a:[Live;

    aget-object v5, v5, v2

    iget-object v5, v5, Live;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v5

    .line 125
    if-eqz v5, :cond_0

    .line 126
    move-object/from16 v0, p0

    iget-object v6, v0, Livd;->a:[Live;

    .line 130
    move-object/from16 v0, p0

    iget-object v6, v0, Livd;->a:[Live;

    .line 131
    move-object/from16 v0, p0

    iget-object v6, v0, Livd;->a:[Live;

    .line 132
    move-object/from16 v0, p0

    iget-object v6, v0, Livd;->a:[Live;

    aget-object v6, v6, v2

    iget-object v6, v6, Live;->a:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v6}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v6

    if-ne v6, v5, :cond_5

    .line 135
    const-string v8, "%s currently has %s (%s), acquired %s ago"

    const/4 v3, 0x4

    new-array v9, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v6, 0x0

    aput-object v6, v9, v3

    const/4 v3, 0x1

    .line 137
    sget-object v6, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "%s@%s"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v12

    aput-object v12, v10, v11

    const/4 v11, 0x1

    invoke-static {v5}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v10, v11

    invoke-static {v6, v7, v10}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v9, v3

    const/4 v3, 0x2

    const/4 v5, 0x0

    aput-object v5, v9, v3

    const/4 v5, 0x3

    .line 140
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x11

    if-lt v3, v6, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v6

    .line 139
    :goto_1
    const-wide/16 v10, 0x3e8

    cmp-long v3, v6, v10

    if-gez v3, :cond_2

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "%d ns"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v11, v12

    invoke-static {v3, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_2
    aput-object v3, v9, v5

    .line 135
    move-object/from16 v0, p1

    invoke-interface {v0, v8, v9}, Livm;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 141
    const/4 v3, 0x1

    .line 123
    :cond_0
    :goto_3
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_0

    .line 140
    :cond_1
    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    goto :goto_1

    .line 139
    :cond_2
    const-wide/32 v10, 0xf4240

    cmp-long v3, v6, v10

    if-gez v3, :cond_3

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "%d us"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-wide/16 v14, 0x3e8

    div-long/2addr v6, v14

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v11, v12

    invoke-static {v3, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_3
    const-wide/32 v10, 0x3b9aca00

    cmp-long v3, v6, v10

    if-gez v3, :cond_4

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v10, "%d ms"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-wide/32 v14, 0xf4240

    div-long/2addr v6, v14

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v11, v12

    invoke-static {v3, v10, v11}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_4
    const-wide/32 v10, 0x3b9aca00

    div-long/2addr v6, v10

    const-wide/16 v10, 0x3c

    div-long v10, v6, v10

    const-wide/16 v12, 0x3c

    div-long v12, v10, v12

    const-wide/16 v14, 0x3c

    mul-long/2addr v14, v10

    sub-long/2addr v6, v14

    const-wide/16 v14, 0x3c

    mul-long/2addr v14, v12

    sub-long/2addr v10, v14

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v14, "%2d:%02d.%02d   "

    const/4 v15, 0x3

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    aput-object v12, v15, v16

    const/4 v12, 0x1

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v15, v12

    const/4 v10, 0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v15, v10

    invoke-static {v3, v14, v15}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 143
    :cond_5
    const/4 v4, 0x1

    goto/16 :goto_3

    .line 147
    :cond_6
    if-eqz v4, :cond_7

    .line 148
    const-string v2, "WARNING: Resources were modified during summarization."

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Livm;->a(Ljava/lang/String;)V

    .line 151
    :cond_7
    if-nez v3, :cond_8

    .line 152
    const-string v2, "No tracked resources are currently held."

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Livm;->a(Ljava/lang/String;)V

    .line 154
    :cond_8
    return-void
.end method
