.class public final Llal;
.super Lllq;
.source "PG"

# interfaces
.implements Lhuc;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:I


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lllq;-><init>()V

    return-void
.end method

.method public constructor <init>(Logh;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Lllq;-><init>()V

    .line 30
    if-nez p1, :cond_0

    .line 39
    :goto_0
    return-void

    .line 33
    :cond_0
    iget-object v0, p1, Logh;->a:Ljava/lang/String;

    iput-object v0, p0, Llal;->a:Ljava/lang/String;

    .line 34
    iget-object v0, p1, Logh;->b:Ljava/lang/String;

    iput-object v0, p0, Llal;->b:Ljava/lang/String;

    .line 35
    iget-object v0, p1, Logh;->d:Ljava/lang/String;

    iput-object v0, p0, Llal;->c:Ljava/lang/String;

    .line 36
    iget-object v0, p1, Logh;->c:Ljava/lang/String;

    iput-object v0, p0, Llal;->d:Ljava/lang/String;

    .line 37
    iget v0, p1, Logh;->e:I

    iput v0, p0, Llal;->e:I

    .line 38
    iget v0, p1, Logh;->f:I

    iput v0, p0, Llal;->f:I

    goto :goto_0
.end method

.method public static a([B)Llal;
    .locals 3

    .prologue
    .line 62
    if-nez p0, :cond_0

    .line 63
    const/4 v0, 0x0

    .line 76
    :goto_0
    return-object v0

    .line 66
    :cond_0
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 67
    new-instance v0, Llal;

    invoke-direct {v0}, Llal;-><init>()V

    .line 69
    invoke-static {v1}, Llal;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Llal;->a:Ljava/lang/String;

    .line 70
    invoke-static {v1}, Llal;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Llal;->b:Ljava/lang/String;

    .line 71
    invoke-static {v1}, Llal;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Llal;->c:Ljava/lang/String;

    .line 72
    invoke-static {v1}, Llal;->e(Ljava/nio/ByteBuffer;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Llal;->d:Ljava/lang/String;

    .line 73
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    iput v2, v0, Llal;->e:I

    .line 74
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    iput v1, v0, Llal;->f:I

    goto :goto_0
.end method

.method public static a(Logh;)[B
    .locals 4

    .prologue
    .line 42
    new-instance v0, Llal;

    invoke-direct {v0, p0}, Llal;-><init>(Logh;)V

    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0x80

    invoke-direct {v1, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    new-instance v2, Ljava/io/DataOutputStream;

    invoke-direct {v2, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iget-object v3, v0, Llal;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Llal;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, v0, Llal;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Llal;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, v0, Llal;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Llal;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget-object v3, v0, Llal;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Llal;->a(Ljava/io/DataOutputStream;Ljava/lang/String;)V

    iget v3, v0, Llal;->e:I

    invoke-virtual {v2, v3}, Ljava/io/DataOutputStream;->writeInt(I)V

    iget v0, v0, Llal;->f:I

    invoke-virtual {v2, v0}, Ljava/io/DataOutputStream;->writeInt(I)V

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    invoke-virtual {v2}, Ljava/io/DataOutputStream;->close()V

    return-object v0
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Llal;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Llal;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Llal;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Llal;->d:Ljava/lang/String;

    return-object v0
.end method

.method public e()I
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Llal;->e:I

    return v0
.end method

.method public f()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Llal;->f:I

    return v0
.end method
