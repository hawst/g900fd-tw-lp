.class public final Lie;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static a:Lii;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 247
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 248
    const/16 v1, 0xe

    if-lt v0, v1, :cond_0

    .line 249
    new-instance v0, Lih;

    invoke-direct {v0}, Lih;-><init>()V

    sput-object v0, Lie;->a:Lii;

    .line 255
    :goto_0
    return-void

    .line 250
    :cond_0
    const/16 v1, 0xb

    if-lt v0, v1, :cond_1

    .line 251
    new-instance v0, Lig;

    invoke-direct {v0}, Lig;-><init>()V

    sput-object v0, Lie;->a:Lii;

    goto :goto_0

    .line 253
    :cond_1
    new-instance v0, Lif;

    invoke-direct {v0}, Lif;-><init>()V

    sput-object v0, Lie;->a:Lii;

    goto :goto_0
.end method

.method public static a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 287
    instance-of v0, p0, Lep;

    if-eqz v0, :cond_0

    .line 288
    check-cast p0, Lep;

    invoke-interface {p0, p1}, Lep;->setActionView(Landroid/view/View;)Landroid/view/MenuItem;

    move-result-object v0

    .line 290
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lie;->a:Lii;

    invoke-interface {v0, p0, p1}, Lii;->a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/view/MenuItem;Lhj;)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 344
    instance-of v0, p0, Lep;

    if-eqz v0, :cond_0

    .line 345
    check-cast p0, Lep;

    invoke-interface {p0, p1}, Lep;->a(Lhj;)Lep;

    move-result-object p0

    .line 348
    :cond_0
    return-object p0
.end method

.method public static a(Landroid/view/MenuItem;)Landroid/view/View;
    .locals 1

    .prologue
    .line 322
    instance-of v0, p0, Lep;

    if-eqz v0, :cond_0

    .line 323
    check-cast p0, Lep;

    invoke-interface {p0}, Lep;->getActionView()Landroid/view/View;

    move-result-object v0

    .line 325
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lie;->a:Lii;

    invoke-interface {v0, p0}, Lii;->a(Landroid/view/MenuItem;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/view/MenuItem;I)V
    .locals 1

    .prologue
    .line 268
    instance-of v0, p0, Lep;

    if-eqz v0, :cond_0

    .line 269
    check-cast p0, Lep;

    invoke-interface {p0, p1}, Lep;->setShowAsAction(I)V

    .line 273
    :goto_0
    return-void

    .line 271
    :cond_0
    sget-object v0, Lie;->a:Lii;

    invoke-interface {v0, p0, p1}, Lii;->a(Landroid/view/MenuItem;I)V

    goto :goto_0
.end method

.method public static b(Landroid/view/MenuItem;I)Landroid/view/MenuItem;
    .locals 1

    .prologue
    .line 309
    instance-of v0, p0, Lep;

    if-eqz v0, :cond_0

    .line 310
    check-cast p0, Lep;

    invoke-interface {p0, p1}, Lep;->setActionView(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 312
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lie;->a:Lii;

    invoke-interface {v0, p0, p1}, Lii;->b(Landroid/view/MenuItem;I)Landroid/view/MenuItem;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Landroid/view/MenuItem;)Lhj;
    .locals 1

    .prologue
    .line 361
    instance-of v0, p0, Lep;

    if-eqz v0, :cond_0

    .line 362
    check-cast p0, Lep;

    invoke-interface {p0}, Lep;->a()Lhj;

    move-result-object v0

    .line 366
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 383
    instance-of v0, p0, Lep;

    if-eqz v0, :cond_0

    .line 384
    check-cast p0, Lep;

    invoke-interface {p0}, Lep;->expandActionView()Z

    move-result v0

    .line 386
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lie;->a:Lii;

    invoke-interface {v0, p0}, Lii;->b(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public static d(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 418
    instance-of v0, p0, Lep;

    if-eqz v0, :cond_0

    .line 419
    check-cast p0, Lep;

    invoke-interface {p0}, Lep;->isActionViewExpanded()Z

    move-result v0

    .line 421
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lie;->a:Lii;

    invoke-interface {v0, p0}, Lii;->c(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method
