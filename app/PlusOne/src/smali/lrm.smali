.class public Llrm;
.super Lt;
.source "PG"


# instance fields
.field public final P:Llqm;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Lt;-><init>()V

    .line 18
    new-instance v0, Llqm;

    invoke-direct {v0}, Llqm;-><init>()V

    iput-object v0, p0, Llrm;->P:Llqm;

    return-void
.end method


# virtual methods
.method public A()V
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Llrm;->P:Llqm;

    invoke-virtual {v0}, Llqm;->c()V

    .line 90
    invoke-super {p0}, Lt;->A()V

    .line 91
    return-void
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Llrm;->P:Llqm;

    invoke-virtual {v0, p3}, Llqm;->b(Landroid/os/Bundle;)V

    .line 34
    invoke-super {p0, p1, p2, p3}, Lt;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Llrm;->P:Llqm;

    invoke-virtual {v0, p1, p2, p3}, Llqm;->a(IILandroid/content/Intent;)V

    .line 96
    invoke-super {p0, p1, p2, p3}, Lt;->a(IILandroid/content/Intent;)V

    .line 97
    return-void
.end method

.method public a(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Llrm;->P:Llqm;

    invoke-virtual {v0, p1}, Llqm;->a(Landroid/app/Activity;)V

    .line 46
    invoke-super {p0, p1}, Lt;->a(Landroid/app/Activity;)V

    .line 47
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Llrm;->P:Llqm;

    invoke-virtual {v0, p1}, Llqm;->c(Landroid/os/Bundle;)V

    .line 27
    invoke-super {p0, p1}, Lt;->a(Landroid/os/Bundle;)V

    .line 28
    return-void
.end method

.method public a(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Llrm;->P:Llqm;

    invoke-virtual {v0, p1, p2}, Llqm;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 40
    invoke-super {p0, p1, p2}, Lt;->a(Landroid/view/View;Landroid/os/Bundle;)V

    .line 41
    return-void
.end method

.method public aO_()V
    .locals 1

    .prologue
    .line 58
    invoke-virtual {p0}, Llrm;->q()Lae;

    move-result-object v0

    invoke-static {v0}, Llrp;->a(Lae;)V

    .line 59
    iget-object v0, p0, Llrm;->P:Llqm;

    invoke-virtual {v0}, Llqm;->k()V

    .line 60
    invoke-super {p0}, Lt;->aO_()V

    .line 61
    return-void
.end method

.method public ae_()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Llrm;->P:Llqm;

    invoke-virtual {v0}, Llqm;->a()V

    .line 78
    invoke-super {p0}, Lt;->ae_()V

    .line 79
    return-void
.end method

.method public e(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Llrm;->P:Llqm;

    invoke-virtual {v0, p1}, Llqm;->d(Landroid/os/Bundle;)V

    .line 84
    invoke-super {p0, p1}, Lt;->e(Landroid/os/Bundle;)V

    .line 85
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Llrm;->P:Llqm;

    invoke-virtual {v0}, Llqm;->d()V

    .line 114
    invoke-super {p0}, Lt;->f()V

    .line 115
    return-void
.end method

.method public g()V
    .locals 1

    .prologue
    .line 51
    invoke-virtual {p0}, Llrm;->q()Lae;

    move-result-object v0

    invoke-static {v0}, Llrp;->a(Lae;)V

    .line 52
    iget-object v0, p0, Llrm;->P:Llqm;

    invoke-virtual {v0}, Llqm;->j()V

    .line 53
    invoke-super {p0}, Lt;->g()V

    .line 54
    return-void
.end method

.method public h()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Llrm;->P:Llqm;

    invoke-virtual {v0}, Llqm;->l()V

    .line 72
    invoke-super {p0}, Lt;->h()V

    .line 73
    return-void
.end method

.method public i_(Z)V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Llrm;->P:Llqm;

    invoke-virtual {v0, p1}, Llqm;->a(Z)V

    .line 108
    invoke-super {p0, p1}, Lt;->i_(Z)V

    .line 109
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Llrm;->P:Llqm;

    invoke-virtual {v0}, Llqm;->m()V

    .line 102
    invoke-super {p0}, Lt;->onLowMemory()V

    .line 103
    return-void
.end method

.method public z()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Llrm;->P:Llqm;

    invoke-virtual {v0}, Llqm;->b()V

    .line 66
    invoke-super {p0}, Lt;->z()V

    .line 67
    return-void
.end method
