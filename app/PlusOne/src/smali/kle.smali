.class public final Lkle;
.super Landroid/widget/ArrayAdapter;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lkkz;",
        ">;"
    }
.end annotation


# instance fields
.field private synthetic a:Lcom/google/android/libraries/social/sharekit/impl/MoodPickerFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/social/sharekit/impl/MoodPickerFragment;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 43
    iput-object p1, p0, Lkle;->a:Lcom/google/android/libraries/social/sharekit/impl/MoodPickerFragment;

    .line 44
    const v0, 0x7f0401dd

    invoke-static {p2}, Lkkz;->a(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {p0, p2, v0, v1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 45
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 54
    if-nez p2, :cond_0

    iget-object v0, p0, Lkle;->a:Lcom/google/android/libraries/social/sharekit/impl/MoodPickerFragment;

    invoke-static {v0}, Lcom/google/android/libraries/social/sharekit/impl/MoodPickerFragment;->b(Lcom/google/android/libraries/social/sharekit/impl/MoodPickerFragment;)Llnl;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0401dd

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 55
    :cond_0
    invoke-virtual {p0, p1}, Lkle;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkkz;

    .line 57
    const v1, 0x7f100581

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/social/media/ui/MediaView;

    .line 58
    iget-object v2, p0, Lkle;->a:Lcom/google/android/libraries/social/sharekit/impl/MoodPickerFragment;

    invoke-static {v2}, Lcom/google/android/libraries/social/sharekit/impl/MoodPickerFragment;->c(Lcom/google/android/libraries/social/sharekit/impl/MoodPickerFragment;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 59
    iget-object v2, p0, Lkle;->a:Lcom/google/android/libraries/social/sharekit/impl/MoodPickerFragment;

    invoke-static {v2}, Lcom/google/android/libraries/social/sharekit/impl/MoodPickerFragment;->d(Lcom/google/android/libraries/social/sharekit/impl/MoodPickerFragment;)Llnl;

    move-result-object v2

    invoke-virtual {v0, v2}, Lkkz;->b(Landroid/content/Context;)Lizu;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->a(Lizu;)V

    .line 60
    invoke-virtual {v1, v0}, Lcom/google/android/libraries/social/media/ui/MediaView;->setTag(Ljava/lang/Object;)V

    .line 61
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->h(Z)V

    .line 62
    invoke-virtual {v0}, Lkkz;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/libraries/social/media/ui/MediaView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 64
    const v1, 0x7f100582

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 65
    invoke-virtual {v0}, Lkkz;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 67
    return-object p2
.end method
