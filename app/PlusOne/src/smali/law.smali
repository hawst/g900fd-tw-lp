.class public final Llaw;
.super Lkgg;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lkgg",
        "<",
        "Lmfq;",
        "Lmfr;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:I

.field private final b:Lkzl;

.field private final c:Llae;

.field private final p:Ljava/lang/String;

.field private final q:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILlae;Ljava/lang/String;ILkfp;)V
    .locals 6

    .prologue
    .line 34
    new-instance v2, Lkfo;

    invoke-direct {v2, p1, p2, p6}, Lkfo;-><init>(Landroid/content/Context;ILkfp;)V

    const-string v3, "nearbystream"

    new-instance v4, Lmfq;

    invoke-direct {v4}, Lmfq;-><init>()V

    new-instance v5, Lmfr;

    invoke-direct {v5}, Lmfr;-><init>()V

    move-object v0, p0

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lkgg;-><init>(Landroid/content/Context;Lkfo;Ljava/lang/String;Loxu;Loxu;)V

    .line 36
    iput p2, p0, Llaw;->a:I

    .line 37
    iget-object v0, p0, Llaw;->f:Landroid/content/Context;

    const-class v1, Lkzl;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lkzl;

    iput-object v0, p0, Llaw;->b:Lkzl;

    .line 38
    iput-object p3, p0, Llaw;->c:Llae;

    .line 39
    iput-object p4, p0, Llaw;->p:Ljava/lang/String;

    .line 40
    if-lez p5, :cond_0

    :goto_0
    iput p5, p0, Llaw;->q:I

    .line 41
    return-void

    .line 40
    :cond_0
    const/16 p5, 0xa

    goto :goto_0
.end method


# virtual methods
.method protected a(Lmfq;)V
    .locals 5

    .prologue
    .line 45
    new-instance v0, Lnno;

    invoke-direct {v0}, Lnno;-><init>()V

    iput-object v0, p1, Lmfq;->a:Lnno;

    .line 46
    iget-object v0, p1, Lmfq;->a:Lnno;

    .line 47
    new-instance v1, Lnnp;

    invoke-direct {v1}, Lnnp;-><init>()V

    iput-object v1, v0, Lnno;->a:Lnnp;

    .line 48
    iget-object v1, v0, Lnno;->a:Lnnp;

    iget-object v2, p0, Llaw;->c:Llae;

    invoke-virtual {v2}, Llae;->f()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lnnp;->a:Ljava/lang/Integer;

    .line 49
    iget-object v1, v0, Lnno;->a:Lnnp;

    iget-object v2, p0, Llaw;->c:Llae;

    invoke-virtual {v2}, Llae;->g()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lnnp;->b:Ljava/lang/Integer;

    .line 50
    iget-object v1, p0, Llaw;->p:Ljava/lang/String;

    iput-object v1, v0, Lnno;->c:Ljava/lang/String;

    .line 51
    iget v1, p0, Llaw;->q:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lnno;->b:Ljava/lang/Integer;

    .line 53
    new-instance v1, Lnmr;

    invoke-direct {v1}, Lnmr;-><init>()V

    iput-object v1, v0, Lnno;->e:Lnmr;

    .line 54
    iget-object v1, v0, Lnno;->e:Lnmr;

    new-instance v2, Lofl;

    invoke-direct {v2}, Lofl;-><init>()V

    iput-object v2, v1, Lnmr;->a:Lofl;

    .line 55
    iget-object v1, v0, Lnno;->e:Lnmr;

    iget-object v1, v1, Lnmr;->a:Lofl;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v2, v1, Lofl;->e:Ljava/lang/Boolean;

    .line 56
    iget-object v1, v0, Lnno;->e:Lnmr;

    iget-object v1, v1, Lnmr;->a:Lofl;

    sget-object v2, Ljava/lang/Boolean;->FALSE:Ljava/lang/Boolean;

    iput-object v2, v1, Lofl;->b:Ljava/lang/Boolean;

    .line 57
    iget-object v1, v0, Lnno;->e:Lnmr;

    iget-object v1, v1, Lnmr;->a:Lofl;

    sget-object v2, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    iput-object v2, v1, Lofl;->c:Ljava/lang/Boolean;

    .line 58
    iget-object v1, v0, Lnno;->e:Lnmr;

    new-instance v2, Logu;

    invoke-direct {v2}, Logu;-><init>()V

    iput-object v2, v1, Lnmr;->b:Logu;

    .line 59
    iget-object v1, v0, Lnno;->e:Lnmr;

    iget-object v1, v1, Lnmr;->b:Logu;

    iget-object v2, p0, Llaw;->b:Lkzl;

    const/4 v3, 0x0

    iget v4, p0, Llaw;->a:I

    .line 60
    invoke-interface {v2, v3}, Lkzl;->a(Z)[I

    move-result-object v2

    iput-object v2, v1, Logu;->a:[I

    .line 61
    iget-object v1, v0, Lnno;->e:Lnmr;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    iput-object v2, v1, Lnmr;->c:Ljava/lang/Boolean;

    .line 63
    new-instance v1, Loxz;

    invoke-direct {v1}, Loxz;-><init>()V

    iput-object v1, v0, Lnno;->f:Loxz;

    .line 64
    iget-object v1, v0, Lnno;->f:Loxz;

    iget-object v2, p0, Llaw;->b:Lkzl;

    iget-object v3, p0, Llaw;->f:Landroid/content/Context;

    iget v4, p0, Llaw;->a:I

    invoke-interface {v2, v3, v4}, Lkzl;->a(Landroid/content/Context;I)[I

    move-result-object v2

    iput-object v2, v1, Loxz;->a:[I

    .line 65
    const/4 v1, 0x2

    iput v1, v0, Lnno;->d:I

    .line 66
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 23
    check-cast p1, Lmfq;

    invoke-virtual {p0, p1}, Llaw;->a(Lmfq;)V

    return-void
.end method
