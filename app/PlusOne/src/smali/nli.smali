.class public final Lnli;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 272
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 293
    const/4 v0, 0x0

    .line 294
    iget-object v1, p0, Lnli;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 295
    const/4 v0, 0x1

    iget-object v1, p0, Lnli;->a:Ljava/lang/String;

    .line 296
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 298
    :cond_0
    iget-object v1, p0, Lnli;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 299
    const/4 v1, 0x2

    iget-object v2, p0, Lnli;->b:Ljava/lang/Boolean;

    .line 300
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 302
    :cond_1
    iget-object v1, p0, Lnli;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 303
    iput v0, p0, Lnli;->ai:I

    .line 304
    return v0
.end method

.method public a(Loxn;)Lnli;
    .locals 2

    .prologue
    .line 312
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 313
    sparse-switch v0, :sswitch_data_0

    .line 317
    iget-object v1, p0, Lnli;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 318
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lnli;->ah:Ljava/util/List;

    .line 321
    :cond_1
    iget-object v1, p0, Lnli;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 323
    :sswitch_0
    return-object p0

    .line 328
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lnli;->a:Ljava/lang/String;

    goto :goto_0

    .line 332
    :sswitch_2
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lnli;->b:Ljava/lang/Boolean;

    goto :goto_0

    .line 313
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 281
    iget-object v0, p0, Lnli;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 282
    const/4 v0, 0x1

    iget-object v1, p0, Lnli;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 284
    :cond_0
    iget-object v0, p0, Lnli;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 285
    const/4 v0, 0x2

    iget-object v1, p0, Lnli;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 287
    :cond_1
    iget-object v0, p0, Lnli;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 289
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 268
    invoke-virtual {p0, p1}, Lnli;->a(Loxn;)Lnli;

    move-result-object v0

    return-object v0
.end method
