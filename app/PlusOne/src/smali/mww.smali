.class public final Lmww;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmww;


# instance fields
.field private b:[Lmwx;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5635
    const/4 v0, 0x0

    new-array v0, v0, [Lmww;

    sput-object v0, Lmww;->a:[Lmww;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5636
    invoke-direct {p0}, Loxq;-><init>()V

    .line 5639
    sget-object v0, Lmwx;->a:[Lmwx;

    iput-object v0, p0, Lmww;->b:[Lmwx;

    .line 5636
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 5657
    .line 5658
    iget-object v1, p0, Lmww;->b:[Lmwx;

    if-eqz v1, :cond_1

    .line 5659
    iget-object v2, p0, Lmww;->b:[Lmwx;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 5660
    if-eqz v4, :cond_0

    .line 5661
    const/4 v5, 0x1

    .line 5662
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 5659
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5666
    :cond_1
    iget-object v1, p0, Lmww;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5667
    iput v0, p0, Lmww;->ai:I

    .line 5668
    return v0
.end method

.method public a(Loxn;)Lmww;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5676
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 5677
    sparse-switch v0, :sswitch_data_0

    .line 5681
    iget-object v2, p0, Lmww;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 5682
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmww;->ah:Ljava/util/List;

    .line 5685
    :cond_1
    iget-object v2, p0, Lmww;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5687
    :sswitch_0
    return-object p0

    .line 5692
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 5693
    iget-object v0, p0, Lmww;->b:[Lmwx;

    if-nez v0, :cond_3

    move v0, v1

    .line 5694
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmwx;

    .line 5695
    iget-object v3, p0, Lmww;->b:[Lmwx;

    if-eqz v3, :cond_2

    .line 5696
    iget-object v3, p0, Lmww;->b:[Lmwx;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 5698
    :cond_2
    iput-object v2, p0, Lmww;->b:[Lmwx;

    .line 5699
    :goto_2
    iget-object v2, p0, Lmww;->b:[Lmwx;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_4

    .line 5700
    iget-object v2, p0, Lmww;->b:[Lmwx;

    new-instance v3, Lmwx;

    invoke-direct {v3}, Lmwx;-><init>()V

    aput-object v3, v2, v0

    .line 5701
    iget-object v2, p0, Lmww;->b:[Lmwx;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 5702
    invoke-virtual {p1}, Loxn;->a()I

    .line 5699
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 5693
    :cond_3
    iget-object v0, p0, Lmww;->b:[Lmwx;

    array-length v0, v0

    goto :goto_1

    .line 5705
    :cond_4
    iget-object v2, p0, Lmww;->b:[Lmwx;

    new-instance v3, Lmwx;

    invoke-direct {v3}, Lmwx;-><init>()V

    aput-object v3, v2, v0

    .line 5706
    iget-object v2, p0, Lmww;->b:[Lmwx;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 5677
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 5644
    iget-object v0, p0, Lmww;->b:[Lmwx;

    if-eqz v0, :cond_1

    .line 5645
    iget-object v1, p0, Lmww;->b:[Lmwx;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 5646
    if-eqz v3, :cond_0

    .line 5647
    const/4 v4, 0x1

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 5645
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5651
    :cond_1
    iget-object v0, p0, Lmww;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 5653
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5632
    invoke-virtual {p0, p1}, Lmww;->a(Loxn;)Lmww;

    move-result-object v0

    return-object v0
.end method
