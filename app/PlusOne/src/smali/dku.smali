.class public final Ldku;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmko;",
        "Lmkp;",
        ">;"
    }
.end annotation


# static fields
.field private static final q:[I


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final p:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 26
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0x19

    aput v2, v0, v1

    sput-object v0, Ldku;->q:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    .line 30
    const-string v3, "collectionupdate"

    new-instance v4, Lmko;

    invoke-direct {v4}, Lmko;-><init>()V

    new-instance v5, Lmkp;

    invoke-direct {v5}, Lmkp;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 33
    iput-object p3, p0, Ldku;->a:Ljava/lang/String;

    .line 34
    iput-object p4, p0, Ldku;->b:Ljava/lang/String;

    .line 35
    iput-boolean p5, p0, Ldku;->p:Z

    .line 36
    return-void
.end method


# virtual methods
.method protected a(Lmko;)V
    .locals 3

    .prologue
    .line 40
    new-instance v0, Lnai;

    invoke-direct {v0}, Lnai;-><init>()V

    iput-object v0, p1, Lmko;->a:Lnai;

    .line 41
    iget-object v0, p1, Lmko;->a:Lnai;

    .line 42
    iget-object v1, p0, Ldku;->a:Ljava/lang/String;

    iget-object v2, p0, Ldku;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Ljvj;->a(Ljava/lang/String;Ljava/lang/String;)Lmzo;

    move-result-object v1

    iput-object v1, v0, Lnai;->a:Lmzo;

    .line 43
    sget-object v1, Ldku;->q:[I

    iput-object v1, v0, Lnai;->b:[I

    .line 44
    new-instance v1, Lnab;

    invoke-direct {v1}, Lnab;-><init>()V

    iput-object v1, v0, Lnai;->c:Lnab;

    .line 45
    iget-object v0, v0, Lnai;->c:Lnab;

    iget-boolean v1, p0, Ldku;->p:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lnab;->a:Ljava/lang/Boolean;

    .line 46
    return-void
.end method

.method protected a(Lmkp;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 51
    iget-object v1, p1, Lmkp;->a:Lnaj;

    .line 52
    iget-object v2, v1, Lnaj;->a:Lmzp;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lnaj;->a:Lmzp;

    iget-object v2, v2, Lmzp;->a:Lmzq;

    if-eqz v2, :cond_1

    iget-object v2, v1, Lnaj;->a:Lmzp;

    iget-object v2, v2, Lmzp;->a:Lmzq;

    iget v2, v2, Lmzq;->a:I

    if-eq v2, v0, :cond_1

    .line 54
    new-instance v2, Lkfm;

    const-string v3, "HideFromHighlights failed: "

    iget-object v0, v1, Lnaj;->a:Lmzp;

    iget-object v0, v0, Lmzp;->a:Lmzq;

    iget-object v0, v0, Lmzq;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v3, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-direct {v2, v0}, Lkfm;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 58
    :cond_1
    iget-object v1, p0, Ldku;->f:Landroid/content/Context;

    iget v2, p0, Ldku;->c:I

    iget-object v3, p0, Ldku;->a:Ljava/lang/String;

    iget-boolean v4, p0, Ldku;->p:Z

    if-nez v4, :cond_2

    :goto_1
    invoke-static {v1, v2, v3, v0}, Ljvj;->a(Landroid/content/Context;ILjava/lang/String;Z)V

    .line 59
    return-void

    .line 58
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lmko;

    invoke-virtual {p0, p1}, Ldku;->a(Lmko;)V

    return-void
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lmkp;

    invoke-virtual {p0, p1}, Ldku;->a(Lmkp;)V

    return-void
.end method
