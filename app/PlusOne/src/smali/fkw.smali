.class public final Lfkw;
.super Lkgn;
.source "PG"

# interfaces
.implements Lkig;


# instance fields
.field private final Q:Lkif;

.field private R:Lhee;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Lkgn;-><init>()V

    .line 29
    new-instance v0, Lkif;

    iget-object v1, p0, Lfkw;->P:Llqm;

    invoke-direct {v0, p0, v1}, Lkif;-><init>(Lkgn;Llqr;)V

    iput-object v0, p0, Lfkw;->Q:Lkif;

    .line 73
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    .line 42
    iget-object v0, p0, Lfkw;->Q:Lkif;

    new-instance v1, Lfky;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Lfky;-><init>(I)V

    invoke-virtual {v0, v1}, Lkif;->a(Lu;)V

    .line 45
    iget-object v0, p0, Lfkw;->N:Llnl;

    const-class v1, Ljaa;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljaa;

    invoke-interface {v0}, Ljaa;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    iget-object v0, p0, Lfkw;->Q:Lkif;

    new-instance v1, Ljar;

    invoke-direct {v1}, Ljar;-><init>()V

    invoke-virtual {v0, v1}, Lkif;->a(Lu;)V

    .line 49
    :cond_0
    new-instance v0, Lizf;

    invoke-direct {v0}, Lizf;-><init>()V

    .line 50
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lfkw;->N:Llnl;

    const-class v3, Lcom/google/android/apps/plus/settings/SettingsActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 51
    const-string v2, "account_id"

    iget-object v3, p0, Lfkw;->R:Lhee;

    invoke-interface {v3}, Lhee;->d()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 52
    invoke-virtual {v0, v1}, Lizf;->b(Landroid/content/Intent;)V

    .line 53
    iget-object v1, p0, Lfkw;->Q:Lkif;

    invoke-virtual {v1, v0}, Lkif;->a(Lu;)V

    .line 54
    iget-object v0, p0, Lfkw;->Q:Lkif;

    new-instance v1, Lfky;

    const/4 v2, 0x4

    invoke-direct {v1, v2}, Lfky;-><init>(I)V

    invoke-virtual {v0, v1}, Lkif;->a(Lu;)V

    .line 56
    invoke-static {}, Lfvc;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 57
    new-instance v0, Lkir;

    invoke-direct {v0}, Lkir;-><init>()V

    .line 59
    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lfkw;->N:Llnl;

    const-class v3, Lcom/google/android/apps/plus/settings/GplusAboutSettingsActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 60
    const-string v2, "privacy_uri"

    const-string v3, "http://m.google.com/app/plus/serviceurl?type=privacy"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 62
    const-string v2, "terms_uri"

    const-string v3, "http://m.google.com/app/plus/serviceurl?type=tos"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    const v2, 0x7f0a088a

    invoke-virtual {p0, v2}, Lfkw;->e_(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, v1}, Lkir;->a(Ljava/lang/String;Landroid/content/Intent;)V

    .line 66
    iget-object v1, p0, Lfkw;->Q:Lkif;

    invoke-virtual {v1, v0}, Lkif;->a(Lu;)V

    .line 68
    :cond_1
    return-void
.end method

.method protected c(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 35
    invoke-super {p0, p1}, Lkgn;->c(Landroid/os/Bundle;)V

    .line 36
    iget-object v0, p0, Lfkw;->O:Llnh;

    const-class v1, Lkij;

    iget-object v2, p0, Lfkw;->Q:Lkif;

    invoke-virtual {v0, v1, v2}, Llnh;->a(Ljava/lang/Class;Ljava/lang/Object;)Llnh;

    .line 37
    iget-object v0, p0, Lfkw;->O:Llnh;

    const-class v1, Lhee;

    invoke-virtual {v0, v1}, Llnh;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhee;

    iput-object v0, p0, Lfkw;->R:Lhee;

    .line 38
    return-void
.end method
