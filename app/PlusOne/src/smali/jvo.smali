.class public final Ljvo;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static c:Ljvo;


# instance fields
.field final a:Landroid/content/Context;

.field final b:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private d:Lino;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Ljvo;->b:Ljava/util/Map;

    .line 39
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Ljvo;->a:Landroid/content/Context;

    .line 40
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Ljvo;
    .locals 2

    .prologue
    .line 32
    const-class v1, Ljvo;

    monitor-enter v1

    :try_start_0
    sget-object v0, Ljvo;->c:Ljvo;

    if-nez v0, :cond_0

    .line 33
    new-instance v0, Ljvo;

    invoke-direct {v0, p0}, Ljvo;-><init>(Landroid/content/Context;)V

    sput-object v0, Ljvo;->c:Ljvo;

    .line 35
    :cond_0
    sget-object v0, Ljvo;->c:Ljvo;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 32
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Ljvo;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 97
    return-void
.end method

.method public a(Landroid/net/Uri;Ljava/lang/String;Ljvt;)V
    .locals 4

    .prologue
    .line 64
    iget-object v1, p0, Ljvo;->b:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 65
    if-eqz v0, :cond_1

    .line 66
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p3, v0}, Ljvt;->onPanoramaTypeDetected(I)V

    .line 90
    :goto_1
    return-void

    .line 64
    :cond_0
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, v2}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    goto :goto_0

    .line 70
    :cond_1
    iget-object v0, p0, Ljvo;->d:Lino;

    if-nez v0, :cond_2

    iget-object v0, p0, Ljvo;->a:Landroid/content/Context;

    const-class v1, Lino;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lino;

    iput-object v0, p0, Ljvo;->d:Lino;

    :cond_2
    iget-object v0, p0, Ljvo;->d:Lino;

    new-instance v1, Ljvp;

    invoke-direct {v1, p0, p1, p2, p3}, Ljvp;-><init>(Ljvo;Landroid/net/Uri;Ljava/lang/String;Ljvt;)V

    invoke-interface {v0, p1, v1}, Lino;->a(Landroid/net/Uri;Linr;)V

    goto :goto_1
.end method
