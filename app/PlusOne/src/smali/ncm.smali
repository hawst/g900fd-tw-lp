.class public final Lncm;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Lnjp;

.field public j:Ljava/lang/Long;

.field public k:Lnbi;

.field public l:Ljava/lang/String;

.field public m:Lncr;

.field public n:[Ljava/lang/String;

.field public o:Ljava/lang/String;

.field private p:Ljava/lang/Boolean;

.field private q:Ljava/lang/String;

.field private r:Lncq;

.field private s:Lnco;

.field private t:Ljava/lang/Integer;

.field private u:Ljava/lang/Integer;

.field private v:Ljava/lang/Long;

.field private w:Ljava/lang/Long;

.field private x:Ljava/lang/Long;

.field private y:Lncl;

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 108
    invoke-direct {p0}, Loxq;-><init>()V

    .line 133
    iput-object v0, p0, Lncm;->r:Lncq;

    .line 140
    iput-object v0, p0, Lncm;->s:Lnco;

    .line 145
    iput-object v0, p0, Lncm;->i:Lnjp;

    .line 158
    iput-object v0, p0, Lncm;->k:Lnbi;

    .line 161
    iput-object v0, p0, Lncm;->y:Lncl;

    .line 166
    iput-object v0, p0, Lncm;->m:Lncr;

    .line 169
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lncm;->n:[Ljava/lang/String;

    .line 172
    const/high16 v0, -0x80000000

    iput v0, p0, Lncm;->z:I

    .line 108
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 265
    .line 266
    iget-object v0, p0, Lncm;->a:Ljava/lang/String;

    if-eqz v0, :cond_1a

    .line 267
    const/4 v0, 0x1

    iget-object v2, p0, Lncm;->a:Ljava/lang/String;

    .line 268
    invoke-static {v0, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 270
    :goto_0
    iget-object v2, p0, Lncm;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 271
    const/4 v2, 0x2

    iget-object v3, p0, Lncm;->b:Ljava/lang/String;

    .line 272
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 274
    :cond_0
    iget-object v2, p0, Lncm;->p:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 275
    const/4 v2, 0x3

    iget-object v3, p0, Lncm;->p:Ljava/lang/Boolean;

    .line 276
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 278
    :cond_1
    iget-object v2, p0, Lncm;->c:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 279
    const/4 v2, 0x4

    iget-object v3, p0, Lncm;->c:Ljava/lang/String;

    .line 280
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 282
    :cond_2
    iget-object v2, p0, Lncm;->d:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 283
    const/4 v2, 0x5

    iget-object v3, p0, Lncm;->d:Ljava/lang/String;

    .line 284
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 286
    :cond_3
    iget-object v2, p0, Lncm;->e:Ljava/lang/Boolean;

    if-eqz v2, :cond_4

    .line 287
    const/4 v2, 0x6

    iget-object v3, p0, Lncm;->e:Ljava/lang/Boolean;

    .line 288
    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Loxo;->k(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 290
    :cond_4
    iget-object v2, p0, Lncm;->f:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 291
    const/4 v2, 0x7

    iget-object v3, p0, Lncm;->f:Ljava/lang/String;

    .line 292
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 294
    :cond_5
    iget-object v2, p0, Lncm;->q:Ljava/lang/String;

    if-eqz v2, :cond_6

    .line 295
    const/16 v2, 0x8

    iget-object v3, p0, Lncm;->q:Ljava/lang/String;

    .line 296
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 298
    :cond_6
    iget-object v2, p0, Lncm;->r:Lncq;

    if-eqz v2, :cond_7

    .line 299
    const/16 v2, 0x9

    iget-object v3, p0, Lncm;->r:Lncq;

    .line 300
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 302
    :cond_7
    iget-object v2, p0, Lncm;->g:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 303
    const/16 v2, 0xa

    iget-object v3, p0, Lncm;->g:Ljava/lang/String;

    .line 304
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 306
    :cond_8
    iget-object v2, p0, Lncm;->h:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 307
    const/16 v2, 0xb

    iget-object v3, p0, Lncm;->h:Ljava/lang/String;

    .line 308
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 310
    :cond_9
    iget-object v2, p0, Lncm;->s:Lnco;

    if-eqz v2, :cond_a

    .line 311
    const/16 v2, 0xc

    iget-object v3, p0, Lncm;->s:Lnco;

    .line 312
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 314
    :cond_a
    iget-object v2, p0, Lncm;->t:Ljava/lang/Integer;

    if-eqz v2, :cond_b

    .line 315
    const/16 v2, 0xd

    iget-object v3, p0, Lncm;->t:Ljava/lang/Integer;

    .line 316
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 318
    :cond_b
    iget-object v2, p0, Lncm;->i:Lnjp;

    if-eqz v2, :cond_c

    .line 319
    const/16 v2, 0xe

    iget-object v3, p0, Lncm;->i:Lnjp;

    .line 320
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 322
    :cond_c
    iget-object v2, p0, Lncm;->u:Ljava/lang/Integer;

    if-eqz v2, :cond_d

    .line 323
    const/16 v2, 0xf

    iget-object v3, p0, Lncm;->u:Ljava/lang/Integer;

    .line 324
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Loxo;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 326
    :cond_d
    iget-object v2, p0, Lncm;->j:Ljava/lang/Long;

    if-eqz v2, :cond_e

    .line 327
    const/16 v2, 0x10

    iget-object v3, p0, Lncm;->j:Ljava/lang/Long;

    .line 328
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Loxo;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 330
    :cond_e
    iget-object v2, p0, Lncm;->k:Lnbi;

    if-eqz v2, :cond_f

    .line 331
    const/16 v2, 0x11

    iget-object v3, p0, Lncm;->k:Lnbi;

    .line 332
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 334
    :cond_f
    iget-object v2, p0, Lncm;->l:Ljava/lang/String;

    if-eqz v2, :cond_10

    .line 335
    const/16 v2, 0x12

    iget-object v3, p0, Lncm;->l:Ljava/lang/String;

    .line 336
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 338
    :cond_10
    iget-object v2, p0, Lncm;->m:Lncr;

    if-eqz v2, :cond_11

    .line 339
    const/16 v2, 0x13

    iget-object v3, p0, Lncm;->m:Lncr;

    .line 340
    invoke-static {v2, v3}, Loxo;->c(ILoxu;)I

    move-result v2

    add-int/2addr v0, v2

    .line 342
    :cond_11
    iget-object v2, p0, Lncm;->n:[Ljava/lang/String;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lncm;->n:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_13

    .line 344
    iget-object v3, p0, Lncm;->n:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_12

    aget-object v5, v3, v1

    .line 346
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 344
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 348
    :cond_12
    add-int/2addr v0, v2

    .line 349
    iget-object v1, p0, Lncm;->n:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 351
    :cond_13
    iget v1, p0, Lncm;->z:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_14

    .line 352
    const/16 v1, 0x15

    iget v2, p0, Lncm;->z:I

    .line 353
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 355
    :cond_14
    iget-object v1, p0, Lncm;->v:Ljava/lang/Long;

    if-eqz v1, :cond_15

    .line 356
    const/16 v1, 0x16

    iget-object v2, p0, Lncm;->v:Ljava/lang/Long;

    .line 357
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 359
    :cond_15
    iget-object v1, p0, Lncm;->w:Ljava/lang/Long;

    if-eqz v1, :cond_16

    .line 360
    const/16 v1, 0x17

    iget-object v2, p0, Lncm;->w:Ljava/lang/Long;

    .line 361
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 363
    :cond_16
    iget-object v1, p0, Lncm;->x:Ljava/lang/Long;

    if-eqz v1, :cond_17

    .line 364
    const/16 v1, 0x18

    iget-object v2, p0, Lncm;->x:Ljava/lang/Long;

    .line 365
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Loxo;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 367
    :cond_17
    iget-object v1, p0, Lncm;->o:Ljava/lang/String;

    if-eqz v1, :cond_18

    .line 368
    const/16 v1, 0x19

    iget-object v2, p0, Lncm;->o:Ljava/lang/String;

    .line 369
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 371
    :cond_18
    iget-object v1, p0, Lncm;->y:Lncl;

    if-eqz v1, :cond_19

    .line 372
    const/16 v1, 0x1a

    iget-object v2, p0, Lncm;->y:Lncl;

    .line 373
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 375
    :cond_19
    iget-object v1, p0, Lncm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 376
    iput v0, p0, Lncm;->ai:I

    .line 377
    return v0

    :cond_1a
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Loxn;)Lncm;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 385
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 386
    sparse-switch v0, :sswitch_data_0

    .line 390
    iget-object v1, p0, Lncm;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 391
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lncm;->ah:Ljava/util/List;

    .line 394
    :cond_1
    iget-object v1, p0, Lncm;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 396
    :sswitch_0
    return-object p0

    .line 401
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lncm;->a:Ljava/lang/String;

    goto :goto_0

    .line 405
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lncm;->b:Ljava/lang/String;

    goto :goto_0

    .line 409
    :sswitch_3
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lncm;->p:Ljava/lang/Boolean;

    goto :goto_0

    .line 413
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lncm;->c:Ljava/lang/String;

    goto :goto_0

    .line 417
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lncm;->d:Ljava/lang/String;

    goto :goto_0

    .line 421
    :sswitch_6
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lncm;->e:Ljava/lang/Boolean;

    goto :goto_0

    .line 425
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lncm;->f:Ljava/lang/String;

    goto :goto_0

    .line 429
    :sswitch_8
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lncm;->q:Ljava/lang/String;

    goto :goto_0

    .line 433
    :sswitch_9
    iget-object v0, p0, Lncm;->r:Lncq;

    if-nez v0, :cond_2

    .line 434
    new-instance v0, Lncq;

    invoke-direct {v0}, Lncq;-><init>()V

    iput-object v0, p0, Lncm;->r:Lncq;

    .line 436
    :cond_2
    iget-object v0, p0, Lncm;->r:Lncq;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 440
    :sswitch_a
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lncm;->g:Ljava/lang/String;

    goto :goto_0

    .line 444
    :sswitch_b
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lncm;->h:Ljava/lang/String;

    goto :goto_0

    .line 448
    :sswitch_c
    iget-object v0, p0, Lncm;->s:Lnco;

    if-nez v0, :cond_3

    .line 449
    new-instance v0, Lnco;

    invoke-direct {v0}, Lnco;-><init>()V

    iput-object v0, p0, Lncm;->s:Lnco;

    .line 451
    :cond_3
    iget-object v0, p0, Lncm;->s:Lnco;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 455
    :sswitch_d
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lncm;->t:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 459
    :sswitch_e
    iget-object v0, p0, Lncm;->i:Lnjp;

    if-nez v0, :cond_4

    .line 460
    new-instance v0, Lnjp;

    invoke-direct {v0}, Lnjp;-><init>()V

    iput-object v0, p0, Lncm;->i:Lnjp;

    .line 462
    :cond_4
    iget-object v0, p0, Lncm;->i:Lnjp;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 466
    :sswitch_f
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lncm;->u:Ljava/lang/Integer;

    goto/16 :goto_0

    .line 470
    :sswitch_10
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lncm;->j:Ljava/lang/Long;

    goto/16 :goto_0

    .line 474
    :sswitch_11
    iget-object v0, p0, Lncm;->k:Lnbi;

    if-nez v0, :cond_5

    .line 475
    new-instance v0, Lnbi;

    invoke-direct {v0}, Lnbi;-><init>()V

    iput-object v0, p0, Lncm;->k:Lnbi;

    .line 477
    :cond_5
    iget-object v0, p0, Lncm;->k:Lnbi;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 481
    :sswitch_12
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lncm;->l:Ljava/lang/String;

    goto/16 :goto_0

    .line 485
    :sswitch_13
    iget-object v0, p0, Lncm;->m:Lncr;

    if-nez v0, :cond_6

    .line 486
    new-instance v0, Lncr;

    invoke-direct {v0}, Lncr;-><init>()V

    iput-object v0, p0, Lncm;->m:Lncr;

    .line 488
    :cond_6
    iget-object v0, p0, Lncm;->m:Lncr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 492
    :sswitch_14
    const/16 v0, 0xa2

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 493
    iget-object v0, p0, Lncm;->n:[Ljava/lang/String;

    array-length v0, v0

    .line 494
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 495
    iget-object v2, p0, Lncm;->n:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 496
    iput-object v1, p0, Lncm;->n:[Ljava/lang/String;

    .line 497
    :goto_1
    iget-object v1, p0, Lncm;->n:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_7

    .line 498
    iget-object v1, p0, Lncm;->n:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 499
    invoke-virtual {p1}, Loxn;->a()I

    .line 497
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 502
    :cond_7
    iget-object v1, p0, Lncm;->n:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 506
    :sswitch_15
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 507
    if-eqz v0, :cond_8

    const/4 v1, 0x1

    if-eq v0, v1, :cond_8

    const/4 v1, 0x2

    if-ne v0, v1, :cond_9

    .line 510
    :cond_8
    iput v0, p0, Lncm;->z:I

    goto/16 :goto_0

    .line 512
    :cond_9
    iput v3, p0, Lncm;->z:I

    goto/16 :goto_0

    .line 517
    :sswitch_16
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lncm;->v:Ljava/lang/Long;

    goto/16 :goto_0

    .line 521
    :sswitch_17
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lncm;->w:Ljava/lang/Long;

    goto/16 :goto_0

    .line 525
    :sswitch_18
    invoke-virtual {p1}, Loxn;->e()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lncm;->x:Ljava/lang/Long;

    goto/16 :goto_0

    .line 529
    :sswitch_19
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lncm;->o:Ljava/lang/String;

    goto/16 :goto_0

    .line 533
    :sswitch_1a
    iget-object v0, p0, Lncm;->y:Lncl;

    if-nez v0, :cond_a

    .line 534
    new-instance v0, Lncl;

    invoke-direct {v0}, Lncl;-><init>()V

    iput-object v0, p0, Lncm;->y:Lncl;

    .line 536
    :cond_a
    iget-object v0, p0, Lncm;->y:Lncl;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 386
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x8a -> :sswitch_11
        0x92 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xa8 -> :sswitch_15
        0xb0 -> :sswitch_16
        0xb8 -> :sswitch_17
        0xc0 -> :sswitch_18
        0xca -> :sswitch_19
        0xd2 -> :sswitch_1a
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 179
    iget-object v0, p0, Lncm;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 180
    const/4 v0, 0x1

    iget-object v1, p0, Lncm;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 182
    :cond_0
    iget-object v0, p0, Lncm;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 183
    const/4 v0, 0x2

    iget-object v1, p0, Lncm;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 185
    :cond_1
    iget-object v0, p0, Lncm;->p:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 186
    const/4 v0, 0x3

    iget-object v1, p0, Lncm;->p:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 188
    :cond_2
    iget-object v0, p0, Lncm;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 189
    const/4 v0, 0x4

    iget-object v1, p0, Lncm;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 191
    :cond_3
    iget-object v0, p0, Lncm;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 192
    const/4 v0, 0x5

    iget-object v1, p0, Lncm;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 194
    :cond_4
    iget-object v0, p0, Lncm;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 195
    const/4 v0, 0x6

    iget-object v1, p0, Lncm;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 197
    :cond_5
    iget-object v0, p0, Lncm;->f:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 198
    const/4 v0, 0x7

    iget-object v1, p0, Lncm;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 200
    :cond_6
    iget-object v0, p0, Lncm;->q:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 201
    const/16 v0, 0x8

    iget-object v1, p0, Lncm;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 203
    :cond_7
    iget-object v0, p0, Lncm;->r:Lncq;

    if-eqz v0, :cond_8

    .line 204
    const/16 v0, 0x9

    iget-object v1, p0, Lncm;->r:Lncq;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 206
    :cond_8
    iget-object v0, p0, Lncm;->g:Ljava/lang/String;

    if-eqz v0, :cond_9

    .line 207
    const/16 v0, 0xa

    iget-object v1, p0, Lncm;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 209
    :cond_9
    iget-object v0, p0, Lncm;->h:Ljava/lang/String;

    if-eqz v0, :cond_a

    .line 210
    const/16 v0, 0xb

    iget-object v1, p0, Lncm;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 212
    :cond_a
    iget-object v0, p0, Lncm;->s:Lnco;

    if-eqz v0, :cond_b

    .line 213
    const/16 v0, 0xc

    iget-object v1, p0, Lncm;->s:Lnco;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 215
    :cond_b
    iget-object v0, p0, Lncm;->t:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 216
    const/16 v0, 0xd

    iget-object v1, p0, Lncm;->t:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 218
    :cond_c
    iget-object v0, p0, Lncm;->i:Lnjp;

    if-eqz v0, :cond_d

    .line 219
    const/16 v0, 0xe

    iget-object v1, p0, Lncm;->i:Lnjp;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 221
    :cond_d
    iget-object v0, p0, Lncm;->u:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    .line 222
    const/16 v0, 0xf

    iget-object v1, p0, Lncm;->u:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 224
    :cond_e
    iget-object v0, p0, Lncm;->j:Ljava/lang/Long;

    if-eqz v0, :cond_f

    .line 225
    const/16 v0, 0x10

    iget-object v1, p0, Lncm;->j:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 227
    :cond_f
    iget-object v0, p0, Lncm;->k:Lnbi;

    if-eqz v0, :cond_10

    .line 228
    const/16 v0, 0x11

    iget-object v1, p0, Lncm;->k:Lnbi;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 230
    :cond_10
    iget-object v0, p0, Lncm;->l:Ljava/lang/String;

    if-eqz v0, :cond_11

    .line 231
    const/16 v0, 0x12

    iget-object v1, p0, Lncm;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 233
    :cond_11
    iget-object v0, p0, Lncm;->m:Lncr;

    if-eqz v0, :cond_12

    .line 234
    const/16 v0, 0x13

    iget-object v1, p0, Lncm;->m:Lncr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 236
    :cond_12
    iget-object v0, p0, Lncm;->n:[Ljava/lang/String;

    if-eqz v0, :cond_13

    .line 237
    iget-object v1, p0, Lncm;->n:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_13

    aget-object v3, v1, v0

    .line 238
    const/16 v4, 0x14

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 237
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 241
    :cond_13
    iget v0, p0, Lncm;->z:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_14

    .line 242
    const/16 v0, 0x15

    iget v1, p0, Lncm;->z:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 244
    :cond_14
    iget-object v0, p0, Lncm;->v:Ljava/lang/Long;

    if-eqz v0, :cond_15

    .line 245
    const/16 v0, 0x16

    iget-object v1, p0, Lncm;->v:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 247
    :cond_15
    iget-object v0, p0, Lncm;->w:Ljava/lang/Long;

    if-eqz v0, :cond_16

    .line 248
    const/16 v0, 0x17

    iget-object v1, p0, Lncm;->w:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 250
    :cond_16
    iget-object v0, p0, Lncm;->x:Ljava/lang/Long;

    if-eqz v0, :cond_17

    .line 251
    const/16 v0, 0x18

    iget-object v1, p0, Lncm;->x:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(IJ)V

    .line 253
    :cond_17
    iget-object v0, p0, Lncm;->o:Ljava/lang/String;

    if-eqz v0, :cond_18

    .line 254
    const/16 v0, 0x19

    iget-object v1, p0, Lncm;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 256
    :cond_18
    iget-object v0, p0, Lncm;->y:Lncl;

    if-eqz v0, :cond_19

    .line 257
    const/16 v0, 0x1a

    iget-object v1, p0, Lncm;->y:Lncl;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 259
    :cond_19
    iget-object v0, p0, Lncm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 261
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 104
    invoke-virtual {p0, p1}, Lncm;->a(Loxn;)Lncm;

    move-result-object v0

    return-object v0
.end method
