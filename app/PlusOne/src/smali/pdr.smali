.class public final Lpdr;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/Double;

.field public b:Ljava/lang/Double;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Loya;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 8
    const v0, 0x1a18d28

    new-instance v1, Lpds;

    invoke-direct {v1}, Lpds;-><init>()V

    .line 13
    invoke-static {v0, v1}, Loxr;->a(ILoxs;)Loxr;

    .line 12
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 9
    invoke-direct {p0}, Loxq;-><init>()V

    .line 26
    const/4 v0, 0x0

    iput-object v0, p0, Lpdr;->f:Loya;

    .line 9
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 55
    const/4 v0, 0x0

    .line 56
    iget-object v1, p0, Lpdr;->a:Ljava/lang/Double;

    if-eqz v1, :cond_0

    .line 57
    const/4 v0, 0x1

    iget-object v1, p0, Lpdr;->a:Ljava/lang/Double;

    .line 58
    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 60
    :cond_0
    iget-object v1, p0, Lpdr;->b:Ljava/lang/Double;

    if-eqz v1, :cond_1

    .line 61
    const/4 v1, 0x2

    iget-object v2, p0, Lpdr;->b:Ljava/lang/Double;

    .line 62
    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 64
    :cond_1
    iget-object v1, p0, Lpdr;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 65
    const/4 v1, 0x3

    iget-object v2, p0, Lpdr;->c:Ljava/lang/String;

    .line 66
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 68
    :cond_2
    iget-object v1, p0, Lpdr;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 69
    const/4 v1, 0x4

    iget-object v2, p0, Lpdr;->d:Ljava/lang/String;

    .line 70
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 72
    :cond_3
    iget-object v1, p0, Lpdr;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 73
    const/4 v1, 0x5

    iget-object v2, p0, Lpdr;->e:Ljava/lang/String;

    .line 74
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 76
    :cond_4
    iget-object v1, p0, Lpdr;->f:Loya;

    if-eqz v1, :cond_5

    .line 77
    const/4 v1, 0x6

    iget-object v2, p0, Lpdr;->f:Loya;

    .line 78
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 80
    :cond_5
    iget-object v1, p0, Lpdr;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 81
    iput v0, p0, Lpdr;->ai:I

    .line 82
    return v0
.end method

.method public a(Loxn;)Lpdr;
    .locals 2

    .prologue
    .line 90
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 91
    sparse-switch v0, :sswitch_data_0

    .line 95
    iget-object v1, p0, Lpdr;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 96
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpdr;->ah:Ljava/util/List;

    .line 99
    :cond_1
    iget-object v1, p0, Lpdr;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    :sswitch_0
    return-object p0

    .line 106
    :sswitch_1
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lpdr;->a:Ljava/lang/Double;

    goto :goto_0

    .line 110
    :sswitch_2
    invoke-virtual {p1}, Loxn;->c()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lpdr;->b:Ljava/lang/Double;

    goto :goto_0

    .line 114
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpdr;->c:Ljava/lang/String;

    goto :goto_0

    .line 118
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpdr;->d:Ljava/lang/String;

    goto :goto_0

    .line 122
    :sswitch_5
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lpdr;->e:Ljava/lang/String;

    goto :goto_0

    .line 126
    :sswitch_6
    iget-object v0, p0, Lpdr;->f:Loya;

    if-nez v0, :cond_2

    .line 127
    new-instance v0, Loya;

    invoke-direct {v0}, Loya;-><init>()V

    iput-object v0, p0, Lpdr;->f:Loya;

    .line 129
    :cond_2
    iget-object v0, p0, Lpdr;->f:Loya;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 91
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 31
    iget-object v0, p0, Lpdr;->a:Ljava/lang/Double;

    if-eqz v0, :cond_0

    .line 32
    const/4 v0, 0x1

    iget-object v1, p0, Lpdr;->a:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 34
    :cond_0
    iget-object v0, p0, Lpdr;->b:Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 35
    const/4 v0, 0x2

    iget-object v1, p0, Lpdr;->b:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->a(ID)V

    .line 37
    :cond_1
    iget-object v0, p0, Lpdr;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 38
    const/4 v0, 0x3

    iget-object v1, p0, Lpdr;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 40
    :cond_2
    iget-object v0, p0, Lpdr;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 41
    const/4 v0, 0x4

    iget-object v1, p0, Lpdr;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 43
    :cond_3
    iget-object v0, p0, Lpdr;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 44
    const/4 v0, 0x5

    iget-object v1, p0, Lpdr;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 46
    :cond_4
    iget-object v0, p0, Lpdr;->f:Loya;

    if-eqz v0, :cond_5

    .line 47
    const/4 v0, 0x6

    iget-object v1, p0, Lpdr;->f:Loya;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 49
    :cond_5
    iget-object v0, p0, Lpdr;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 51
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lpdr;->a(Loxn;)Lpdr;

    move-result-object v0

    return-object v0
.end method
