.class public final Lhky;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lhky;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final a:Lhkv;

.field private final b:Lhld;

.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lhkz;

    invoke-direct {v0}, Lhkz;-><init>()V

    sput-object v0, Lhky;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    sget-object v0, Lhkv;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhkv;

    iput-object v0, p0, Lhky;->a:Lhkv;

    .line 23
    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lhld;->valueOf(Ljava/lang/String;)Lhld;

    move-result-object v0

    iput-object v0, p0, Lhky;->b:Lhld;

    .line 24
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lhky;->c:I

    .line 25
    return-void
.end method

.method public constructor <init>(Lhkv;Lhld;I)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lhky;->a:Lhkv;

    .line 17
    iput-object p2, p0, Lhky;->b:Lhld;

    .line 18
    iput p3, p0, Lhky;->c:I

    .line 19
    return-void
.end method


# virtual methods
.method public a()Lhkv;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lhky;->a:Lhkv;

    return-object v0
.end method

.method public b()Lhld;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Lhky;->b:Lhld;

    return-object v0
.end method

.method public c()I
    .locals 1

    .prologue
    .line 35
    iget v0, p0, Lhky;->c:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 44
    iget-object v0, p0, Lhky;->a:Lhkv;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 45
    iget-object v0, p0, Lhky;->b:Lhld;

    invoke-virtual {v0}, Lhld;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 46
    iget v0, p0, Lhky;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 47
    return-void
.end method
