.class public final Loxa;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/Float;

.field private c:Ljava/lang/Float;

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4456
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 4502
    const/4 v0, 0x0

    .line 4503
    iget-object v1, p0, Loxa;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4504
    const/4 v0, 0x1

    iget-object v1, p0, Loxa;->a:Ljava/lang/String;

    .line 4505
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4507
    :cond_0
    iget-object v1, p0, Loxa;->b:Ljava/lang/Float;

    if-eqz v1, :cond_1

    .line 4508
    const/4 v1, 0x2

    iget-object v2, p0, Loxa;->b:Ljava/lang/Float;

    .line 4509
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 4511
    :cond_1
    iget-object v1, p0, Loxa;->c:Ljava/lang/Float;

    if-eqz v1, :cond_2

    .line 4512
    const/4 v1, 0x3

    iget-object v2, p0, Loxa;->c:Ljava/lang/Float;

    .line 4513
    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 4515
    :cond_2
    iget-object v1, p0, Loxa;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 4516
    const/4 v1, 0x4

    iget-object v2, p0, Loxa;->d:Ljava/lang/String;

    .line 4517
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4519
    :cond_3
    iget-object v1, p0, Loxa;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 4520
    const/4 v1, 0x5

    iget-object v2, p0, Loxa;->e:Ljava/lang/Integer;

    .line 4521
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4523
    :cond_4
    iget-object v1, p0, Loxa;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 4524
    const/4 v1, 0x6

    iget-object v2, p0, Loxa;->f:Ljava/lang/Integer;

    .line 4525
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4527
    :cond_5
    iget-object v1, p0, Loxa;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 4528
    const/4 v1, 0x7

    iget-object v2, p0, Loxa;->g:Ljava/lang/String;

    .line 4529
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4531
    :cond_6
    iget-object v1, p0, Loxa;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4532
    iput v0, p0, Loxa;->ai:I

    .line 4533
    return v0
.end method

.method public a(Loxn;)Loxa;
    .locals 2

    .prologue
    .line 4541
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 4542
    sparse-switch v0, :sswitch_data_0

    .line 4546
    iget-object v1, p0, Loxa;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 4547
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Loxa;->ah:Ljava/util/List;

    .line 4550
    :cond_1
    iget-object v1, p0, Loxa;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4552
    :sswitch_0
    return-object p0

    .line 4557
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loxa;->a:Ljava/lang/String;

    goto :goto_0

    .line 4561
    :sswitch_2
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxa;->b:Ljava/lang/Float;

    goto :goto_0

    .line 4565
    :sswitch_3
    invoke-virtual {p1}, Loxn;->d()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Loxa;->c:Ljava/lang/Float;

    goto :goto_0

    .line 4569
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loxa;->d:Ljava/lang/String;

    goto :goto_0

    .line 4573
    :sswitch_5
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loxa;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 4577
    :sswitch_6
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Loxa;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 4581
    :sswitch_7
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Loxa;->g:Ljava/lang/String;

    goto :goto_0

    .line 4542
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x15 -> :sswitch_2
        0x1d -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 4475
    iget-object v0, p0, Loxa;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 4476
    const/4 v0, 0x1

    iget-object v1, p0, Loxa;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4478
    :cond_0
    iget-object v0, p0, Loxa;->b:Ljava/lang/Float;

    if-eqz v0, :cond_1

    .line 4479
    const/4 v0, 0x2

    iget-object v1, p0, Loxa;->b:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 4481
    :cond_1
    iget-object v0, p0, Loxa;->c:Ljava/lang/Float;

    if-eqz v0, :cond_2

    .line 4482
    const/4 v0, 0x3

    iget-object v1, p0, Loxa;->c:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IF)V

    .line 4484
    :cond_2
    iget-object v0, p0, Loxa;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 4485
    const/4 v0, 0x4

    iget-object v1, p0, Loxa;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4487
    :cond_3
    iget-object v0, p0, Loxa;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 4488
    const/4 v0, 0x5

    iget-object v1, p0, Loxa;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4490
    :cond_4
    iget-object v0, p0, Loxa;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 4491
    const/4 v0, 0x6

    iget-object v1, p0, Loxa;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 4493
    :cond_5
    iget-object v0, p0, Loxa;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 4494
    const/4 v0, 0x7

    iget-object v1, p0, Loxa;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 4496
    :cond_6
    iget-object v0, p0, Loxa;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 4498
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 4452
    invoke-virtual {p0, p1}, Loxa;->a(Loxn;)Loxa;

    move-result-object v0

    return-object v0
.end method
