.class public final Lexa;
.super Ljava/lang/Object;
.source "PG"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# instance fields
.field private a:Landroid/os/Handler;

.field private synthetic b:Lcom/google/android/apps/plus/phone/EsApplication;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/plus/phone/EsApplication;)V
    .locals 0

    .prologue
    .line 294
    iput-object p1, p0, Lexa;->b:Lcom/google/android/apps/plus/phone/EsApplication;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 5

    .prologue
    const/4 v4, 0x6

    .line 300
    new-instance v0, Lkny;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lkny;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lexa;->b:Lcom/google/android/apps/plus/phone/EsApplication;

    invoke-virtual {v0, v1}, Lkny;->a(Landroid/content/Context;)V

    .line 301
    iget-object v0, p0, Lexa;->b:Lcom/google/android/apps/plus/phone/EsApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsApplication;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-class v1, Lknq;

    invoke-static {v0, v1}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lknq;

    .line 302
    invoke-interface {v0, p2}, Lknq;->a(Ljava/lang/Throwable;)V

    .line 304
    iget-object v0, p0, Lexa;->b:Lcom/google/android/apps/plus/phone/EsApplication;

    invoke-virtual {v0}, Lcom/google/android/apps/plus/phone/EsApplication;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    if-eq v0, p1, :cond_1

    const/4 v0, 0x1

    .line 305
    :goto_0
    if-eqz v0, :cond_6

    .line 306
    const-string v0, "EsApplication"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 307
    const-string v0, "EsApplication"

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, 0x28

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v3, "Uncaught exception in background thread "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 314
    :cond_0
    invoke-static {}, Ldrg;->b()Z

    move-result v0

    move v1, v0

    move-object v0, p2

    .line 316
    :goto_1
    if-eqz v0, :cond_2

    if-nez v1, :cond_2

    .line 317
    instance-of v1, v0, Liac;

    .line 318
    invoke-virtual {v0}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_1

    .line 304
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 321
    :cond_2
    if-eqz v1, :cond_4

    .line 322
    const-string v0, "EsApplication"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 323
    const-string v0, "EsApplication"

    const-string v1, "An account has just been deactivated, which put background threads at a risk of failure. Letting this thread live."

    invoke-static {v0, v1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 342
    :cond_3
    :goto_2
    return-void

    .line 329
    :cond_4
    iget-object v0, p0, Lexa;->a:Landroid/os/Handler;

    if-nez v0, :cond_5

    .line 330
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lexa;->b:Lcom/google/android/apps/plus/phone/EsApplication;

    invoke-virtual {v1}, Lcom/google/android/apps/plus/phone/EsApplication;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lexa;->a:Landroid/os/Handler;

    .line 332
    :cond_5
    iget-object v0, p0, Lexa;->a:Landroid/os/Handler;

    new-instance v1, Lexb;

    invoke-direct {v1, p1, p2}, Lexb;-><init>(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2

    .line 340
    :cond_6
    invoke-static {}, Lcom/google/android/apps/plus/phone/EsApplication;->d()Ljava/lang/Thread$UncaughtExceptionHandler;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    goto :goto_2
.end method
