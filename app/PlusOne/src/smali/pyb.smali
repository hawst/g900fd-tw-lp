.class public final Lpyb;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lpyb;


# instance fields
.field public b:I

.field private c:Ljava/lang/Long;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 113
    const/4 v0, 0x0

    new-array v0, v0, [Lpyb;

    sput-object v0, Lpyb;->a:[Lpyb;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 114
    invoke-direct {p0}, Loxq;-><init>()V

    .line 119
    const/high16 v0, -0x80000000

    iput v0, p0, Lpyb;->b:I

    .line 114
    return-void
.end method


# virtual methods
.method public a()I
    .locals 4

    .prologue
    .line 136
    const/4 v0, 0x0

    .line 137
    iget-object v1, p0, Lpyb;->c:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 138
    const/4 v0, 0x1

    iget-object v1, p0, Lpyb;->c:Ljava/lang/Long;

    .line 139
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Loxo;->f(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 141
    :cond_0
    iget v1, p0, Lpyb;->b:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_1

    .line 142
    const/4 v1, 0x2

    iget v2, p0, Lpyb;->b:I

    .line 143
    invoke-static {v1, v2}, Loxo;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 145
    :cond_1
    iget-object v1, p0, Lpyb;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 146
    iput v0, p0, Lpyb;->ai:I

    .line 147
    return v0
.end method

.method public a(Loxn;)Lpyb;
    .locals 2

    .prologue
    .line 155
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 156
    sparse-switch v0, :sswitch_data_0

    .line 160
    iget-object v1, p0, Lpyb;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 161
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lpyb;->ah:Ljava/util/List;

    .line 164
    :cond_1
    iget-object v1, p0, Lpyb;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 166
    :sswitch_0
    return-object p0

    .line 171
    :sswitch_1
    invoke-virtual {p1}, Loxn;->f()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lpyb;->c:Ljava/lang/Long;

    goto :goto_0

    .line 175
    :sswitch_2
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 176
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0xf

    if-ne v0, v1, :cond_3

    .line 184
    :cond_2
    iput v0, p0, Lpyb;->b:I

    goto :goto_0

    .line 186
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lpyb;->b:I

    goto :goto_0

    .line 156
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 124
    iget-object v0, p0, Lpyb;->c:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 125
    const/4 v0, 0x1

    iget-object v1, p0, Lpyb;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->b(IJ)V

    .line 127
    :cond_0
    iget v0, p0, Lpyb;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 128
    const/4 v0, 0x2

    iget v1, p0, Lpyb;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 130
    :cond_1
    iget-object v0, p0, Lpyb;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 132
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 110
    invoke-virtual {p0, p1}, Lpyb;->a(Loxn;)Lpyb;

    move-result-object v0

    return-object v0
.end method
