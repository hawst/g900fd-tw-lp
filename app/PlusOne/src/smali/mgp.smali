.class public final Lmgp;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnap;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23812
    invoke-direct {p0}, Loxq;-><init>()V

    .line 23815
    iput-object v0, p0, Lmgp;->apiHeader:Llyr;

    .line 23818
    iput-object v0, p0, Lmgp;->a:Lnap;

    .line 23812
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 23835
    const/4 v0, 0x0

    .line 23836
    iget-object v1, p0, Lmgp;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 23837
    const/4 v0, 0x1

    iget-object v1, p0, Lmgp;->apiHeader:Llyr;

    .line 23838
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 23840
    :cond_0
    iget-object v1, p0, Lmgp;->a:Lnap;

    if-eqz v1, :cond_1

    .line 23841
    const/4 v1, 0x2

    iget-object v2, p0, Lmgp;->a:Lnap;

    .line 23842
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23844
    :cond_1
    iget-object v1, p0, Lmgp;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 23845
    iput v0, p0, Lmgp;->ai:I

    .line 23846
    return v0
.end method

.method public a(Loxn;)Lmgp;
    .locals 2

    .prologue
    .line 23854
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 23855
    sparse-switch v0, :sswitch_data_0

    .line 23859
    iget-object v1, p0, Lmgp;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 23860
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmgp;->ah:Ljava/util/List;

    .line 23863
    :cond_1
    iget-object v1, p0, Lmgp;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 23865
    :sswitch_0
    return-object p0

    .line 23870
    :sswitch_1
    iget-object v0, p0, Lmgp;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 23871
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Lmgp;->apiHeader:Llyr;

    .line 23873
    :cond_2
    iget-object v0, p0, Lmgp;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 23877
    :sswitch_2
    iget-object v0, p0, Lmgp;->a:Lnap;

    if-nez v0, :cond_3

    .line 23878
    new-instance v0, Lnap;

    invoke-direct {v0}, Lnap;-><init>()V

    iput-object v0, p0, Lmgp;->a:Lnap;

    .line 23880
    :cond_3
    iget-object v0, p0, Lmgp;->a:Lnap;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 23855
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 23823
    iget-object v0, p0, Lmgp;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 23824
    const/4 v0, 0x1

    iget-object v1, p0, Lmgp;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 23826
    :cond_0
    iget-object v0, p0, Lmgp;->a:Lnap;

    if-eqz v0, :cond_1

    .line 23827
    const/4 v0, 0x2

    iget-object v1, p0, Lmgp;->a:Lnap;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 23829
    :cond_1
    iget-object v0, p0, Lmgp;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 23831
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 23808
    invoke-virtual {p0, p1}, Lmgp;->a(Loxn;)Lmgp;

    move-result-object v0

    return-object v0
.end method
