.class public final Lolj;
.super Loxq;
.source "PG"


# instance fields
.field private a:Ljava/lang/Boolean;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;

.field private e:Ljava/lang/Integer;

.field private f:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 401
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 442
    const/4 v0, 0x0

    .line 443
    iget-object v1, p0, Lolj;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 444
    const/4 v0, 0x1

    iget-object v1, p0, Lolj;->a:Ljava/lang/Boolean;

    .line 445
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Loxo;->k(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 447
    :cond_0
    iget-object v1, p0, Lolj;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 448
    const/4 v1, 0x2

    iget-object v2, p0, Lolj;->b:Ljava/lang/Integer;

    .line 449
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 451
    :cond_1
    iget-object v1, p0, Lolj;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 452
    const/4 v1, 0x3

    iget-object v2, p0, Lolj;->c:Ljava/lang/Integer;

    .line 453
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 455
    :cond_2
    iget-object v1, p0, Lolj;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 456
    const/4 v1, 0x4

    iget-object v2, p0, Lolj;->d:Ljava/lang/Integer;

    .line 457
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 459
    :cond_3
    iget-object v1, p0, Lolj;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 460
    const/4 v1, 0x5

    iget-object v2, p0, Lolj;->e:Ljava/lang/Integer;

    .line 461
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 463
    :cond_4
    iget-object v1, p0, Lolj;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 464
    const/4 v1, 0x6

    iget-object v2, p0, Lolj;->f:Ljava/lang/Integer;

    .line 465
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Loxo;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 467
    :cond_5
    iget-object v1, p0, Lolj;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 468
    iput v0, p0, Lolj;->ai:I

    .line 469
    return v0
.end method

.method public a(Loxn;)Lolj;
    .locals 2

    .prologue
    .line 477
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 478
    sparse-switch v0, :sswitch_data_0

    .line 482
    iget-object v1, p0, Lolj;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 483
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lolj;->ah:Ljava/util/List;

    .line 486
    :cond_1
    iget-object v1, p0, Lolj;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 488
    :sswitch_0
    return-object p0

    .line 493
    :sswitch_1
    invoke-virtual {p1}, Loxn;->j()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lolj;->a:Ljava/lang/Boolean;

    goto :goto_0

    .line 497
    :sswitch_2
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lolj;->b:Ljava/lang/Integer;

    goto :goto_0

    .line 501
    :sswitch_3
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lolj;->c:Ljava/lang/Integer;

    goto :goto_0

    .line 505
    :sswitch_4
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lolj;->d:Ljava/lang/Integer;

    goto :goto_0

    .line 509
    :sswitch_5
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lolj;->e:Ljava/lang/Integer;

    goto :goto_0

    .line 513
    :sswitch_6
    invoke-virtual {p1}, Loxn;->m()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lolj;->f:Ljava/lang/Integer;

    goto :goto_0

    .line 478
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 418
    iget-object v0, p0, Lolj;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 419
    const/4 v0, 0x1

    iget-object v1, p0, Lolj;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->a(IZ)V

    .line 421
    :cond_0
    iget-object v0, p0, Lolj;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 422
    const/4 v0, 0x2

    iget-object v1, p0, Lolj;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->c(II)V

    .line 424
    :cond_1
    iget-object v0, p0, Lolj;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 425
    const/4 v0, 0x3

    iget-object v1, p0, Lolj;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->c(II)V

    .line 427
    :cond_2
    iget-object v0, p0, Lolj;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 428
    const/4 v0, 0x4

    iget-object v1, p0, Lolj;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->c(II)V

    .line 430
    :cond_3
    iget-object v0, p0, Lolj;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 431
    const/4 v0, 0x5

    iget-object v1, p0, Lolj;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->c(II)V

    .line 433
    :cond_4
    iget-object v0, p0, Lolj;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 434
    const/4 v0, 0x6

    iget-object v1, p0, Lolj;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Loxo;->c(II)V

    .line 436
    :cond_5
    iget-object v0, p0, Lolj;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 438
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 397
    invoke-virtual {p0, p1}, Lolj;->a(Loxn;)Lolj;

    move-result-object v0

    return-object v0
.end method
