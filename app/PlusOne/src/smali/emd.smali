.class public final Lemd;
.super Lhye;
.source "PG"


# instance fields
.field private final b:Ldp;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ldo",
            "<",
            "Landroid/database/Cursor;",
            ">.dp;"
        }
    .end annotation
.end field

.field private final c:I

.field private final d:[Ljava/lang/String;

.field private e:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;I[Ljava/lang/String;Ljava/lang/String;ZZZLjava/lang/String;I)V
    .locals 7

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lhye;-><init>(Landroid/content/Context;)V

    .line 47
    new-instance v0, Ldp;

    invoke-direct {v0, p0}, Ldp;-><init>(Ldo;)V

    iput-object v0, p0, Lemd;->b:Ldp;

    .line 48
    sget-object v0, Lcom/google/android/apps/plus/content/EsProvider;->d:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lemd;->a(Landroid/net/Uri;)V

    .line 49
    if-eqz p7, :cond_0

    const-string v0, "gaia_id IS NOT NULL"

    :goto_0
    invoke-virtual {p0, v0}, Lemd;->a(Ljava/lang/String;)V

    .line 50
    iput p2, p0, Lemd;->c:I

    .line 51
    iput-object p3, p0, Lemd;->d:[Ljava/lang/String;

    .line 53
    const-class v0, Lhei;

    invoke-static {p1, v0}, Llnh;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhei;

    .line 54
    invoke-interface {v0, p2}, Lhei;->a(I)Lhej;

    move-result-object v0

    const-string v1, "gaia_id"

    invoke-interface {v0, v1}, Lhej;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 56
    iget v0, p0, Lemd;->c:I

    move-object v2, p4

    move v3, p5

    move v4, p6

    move-object v5, p8

    move/from16 v6, p9

    invoke-static/range {v0 .. v6}, Lcom/google/android/apps/plus/content/EsProvider;->a(ILjava/lang/String;Ljava/lang/String;ZZLjava/lang/String;I)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lemd;->e:Landroid/net/Uri;

    .line 58
    return-void

    .line 49
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public C()Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 65
    invoke-virtual {p0}, Lemd;->n()Landroid/content/Context;

    move-result-object v0

    .line 66
    iget v1, p0, Lemd;->c:I

    invoke-static {v0, v1}, Ldsm;->b(Landroid/content/Context;I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 76
    :cond_0
    :goto_0
    return-object v4

    .line 70
    :cond_1
    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lemd;->e:Landroid/net/Uri;

    iget-object v2, p0, Lemd;->d:[Ljava/lang/String;

    invoke-virtual {p0}, Lemd;->k()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 72
    if-eqz v4, :cond_0

    .line 73
    iget-object v0, p0, Lemd;->b:Ldp;

    invoke-interface {v4, v0}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    goto :goto_0
.end method
