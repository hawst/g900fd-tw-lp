.class public final Lotg;
.super Loxq;
.source "PG"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field private c:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 367
    invoke-direct {p0}, Loxq;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 393
    const/4 v0, 0x0

    .line 394
    iget-object v1, p0, Lotg;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 395
    const/4 v0, 0x1

    iget-object v1, p0, Lotg;->a:Ljava/lang/String;

    .line 396
    invoke-static {v0, v1}, Loxo;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 398
    :cond_0
    iget-object v1, p0, Lotg;->c:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 399
    const/4 v1, 0x2

    iget-object v2, p0, Lotg;->c:Ljava/lang/Long;

    .line 400
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    invoke-static {v1}, Loxo;->k(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 402
    :cond_1
    iget-object v1, p0, Lotg;->b:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 403
    const/4 v1, 0x3

    iget-object v2, p0, Lotg;->b:Ljava/lang/String;

    .line 404
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 406
    :cond_2
    iget-object v1, p0, Lotg;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 407
    iput v0, p0, Lotg;->ai:I

    .line 408
    return v0
.end method

.method public a(Loxn;)Lotg;
    .locals 2

    .prologue
    .line 416
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 417
    sparse-switch v0, :sswitch_data_0

    .line 421
    iget-object v1, p0, Lotg;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 422
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lotg;->ah:Ljava/util/List;

    .line 425
    :cond_1
    iget-object v1, p0, Lotg;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 427
    :sswitch_0
    return-object p0

    .line 432
    :sswitch_1
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lotg;->a:Ljava/lang/String;

    goto :goto_0

    .line 436
    :sswitch_2
    invoke-virtual {p1}, Loxn;->h()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lotg;->c:Ljava/lang/Long;

    goto :goto_0

    .line 440
    :sswitch_3
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lotg;->b:Ljava/lang/String;

    goto :goto_0

    .line 417
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 4

    .prologue
    .line 378
    iget-object v0, p0, Lotg;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 379
    const/4 v0, 0x1

    iget-object v1, p0, Lotg;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 381
    :cond_0
    iget-object v0, p0, Lotg;->c:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 382
    const/4 v0, 0x2

    iget-object v1, p0, Lotg;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Loxo;->c(IJ)V

    .line 384
    :cond_1
    iget-object v0, p0, Lotg;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 385
    const/4 v0, 0x3

    iget-object v1, p0, Lotg;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 387
    :cond_2
    iget-object v0, p0, Lotg;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 389
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 363
    invoke-virtual {p0, p1}, Lotg;->a(Loxn;)Lotg;

    move-result-object v0

    return-object v0
.end method
