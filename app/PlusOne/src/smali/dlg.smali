.class public final Ldlg;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmfa;",
        "Lmfb;",
        ">;"
    }
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private p:Llai;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/util/ArrayList;Llai;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Llai;",
            ")V"
        }
    .end annotation

    .prologue
    .line 27
    const-string v3, "moderatetopics"

    new-instance v4, Lmfa;

    invoke-direct {v4}, Lmfa;-><init>()V

    new-instance v5, Lmfb;

    invoke-direct {v5}, Lmfb;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 29
    iput-object p3, p0, Ldlg;->a:Ljava/lang/String;

    .line 30
    iput-object p4, p0, Ldlg;->b:Ljava/util/ArrayList;

    .line 31
    iput-object p5, p0, Ldlg;->p:Llai;

    .line 32
    return-void
.end method


# virtual methods
.method protected a(Lmfa;)V
    .locals 5

    .prologue
    .line 36
    new-instance v0, Lnvl;

    invoke-direct {v0}, Lnvl;-><init>()V

    iput-object v0, p1, Lmfa;->a:Lnvl;

    .line 37
    iget-object v2, p1, Lmfa;->a:Lnvl;

    .line 38
    iget-object v0, p0, Ldlg;->a:Ljava/lang/String;

    iput-object v0, v2, Lnvl;->a:Ljava/lang/String;

    .line 39
    iget-object v0, p0, Ldlg;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Logp;

    iput-object v0, v2, Lnvl;->b:[Logp;

    .line 41
    const/4 v0, 0x0

    iget-object v1, p0, Ldlg;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 42
    new-instance v4, Logp;

    invoke-direct {v4}, Logp;-><init>()V

    .line 43
    iget-object v0, p0, Ldlg;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v4, Logp;->b:Ljava/lang/String;

    .line 44
    iget-object v0, v2, Lnvl;->b:[Logp;

    aput-object v4, v0, v1

    .line 41
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 46
    :cond_0
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 18
    check-cast p1, Lmfa;

    invoke-virtual {p0, p1}, Ldlg;->a(Lmfa;)V

    return-void
.end method

.method protected b()V
    .locals 4

    .prologue
    .line 50
    iget-object v0, p0, Ldlg;->f:Landroid/content/Context;

    iget v1, p0, Ldlg;->c:I

    iget-object v2, p0, Ldlg;->a:Ljava/lang/String;

    iget-object v3, p0, Ldlg;->p:Llai;

    invoke-static {v0, v1, v2, v3}, Llap;->a(Landroid/content/Context;ILjava/lang/String;Llai;)V

    .line 51
    return-void
.end method

.method protected bridge synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 18
    invoke-virtual {p0}, Ldlg;->b()V

    return-void
.end method
