.class public final Lmtu;
.super Loxq;
.source "PG"


# static fields
.field public static final a:[Lmtu;


# instance fields
.field private b:I

.field private c:Ljava/lang/String;

.field private d:[Ljava/lang/String;

.field private e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2829
    const/4 v0, 0x0

    new-array v0, v0, [Lmtu;

    sput-object v0, Lmtu;->a:[Lmtu;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2830
    invoke-direct {p0}, Loxq;-><init>()V

    .line 2833
    const/high16 v0, -0x80000000

    iput v0, p0, Lmtu;->b:I

    .line 2838
    sget-object v0, Loxx;->d:[Ljava/lang/String;

    iput-object v0, p0, Lmtu;->d:[Ljava/lang/String;

    .line 2830
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2865
    .line 2866
    iget v0, p0, Lmtu;->b:I

    const/high16 v2, -0x80000000

    if-eq v0, v2, :cond_4

    .line 2867
    const/4 v0, 0x1

    iget v2, p0, Lmtu;->b:I

    .line 2868
    invoke-static {v0, v2}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 2870
    :goto_0
    iget-object v2, p0, Lmtu;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 2871
    const/4 v2, 0x2

    iget-object v3, p0, Lmtu;->c:Ljava/lang/String;

    .line 2872
    invoke-static {v2, v3}, Loxo;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2874
    :cond_0
    iget-object v2, p0, Lmtu;->d:[Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lmtu;->d:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_2

    .line 2876
    iget-object v3, p0, Lmtu;->d:[Ljava/lang/String;

    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v3, v1

    .line 2878
    invoke-static {v5}, Loxo;->b(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 2876
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2880
    :cond_1
    add-int/2addr v0, v2

    .line 2881
    iget-object v1, p0, Lmtu;->d:[Ljava/lang/String;

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2883
    :cond_2
    iget-object v1, p0, Lmtu;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 2884
    const/4 v1, 0x4

    iget-object v2, p0, Lmtu;->e:Ljava/lang/String;

    .line 2885
    invoke-static {v1, v2}, Loxo;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2887
    :cond_3
    iget-object v1, p0, Lmtu;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2888
    iput v0, p0, Lmtu;->ai:I

    .line 2889
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmtu;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2897
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 2898
    sparse-switch v0, :sswitch_data_0

    .line 2902
    iget-object v1, p0, Lmtu;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 2903
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lmtu;->ah:Ljava/util/List;

    .line 2906
    :cond_1
    iget-object v1, p0, Lmtu;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2908
    :sswitch_0
    return-object p0

    .line 2913
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 2914
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xe

    if-eq v0, v1, :cond_2

    const/16 v1, 0x50

    if-eq v0, v1, :cond_2

    const/16 v1, 0x51

    if-eq v0, v1, :cond_2

    const/16 v1, 0x64

    if-eq v0, v1, :cond_2

    const/16 v1, 0x65

    if-eq v0, v1, :cond_2

    const/16 v1, 0x66

    if-eq v0, v1, :cond_2

    const/16 v1, 0x67

    if-eq v0, v1, :cond_2

    const/16 v1, 0x68

    if-eq v0, v1, :cond_2

    const/16 v1, 0x77

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa0

    if-eq v0, v1, :cond_2

    const/16 v1, 0x69

    if-eq v0, v1, :cond_2

    const/16 v1, 0x81

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x6f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x70

    if-eq v0, v1, :cond_2

    const/16 v1, 0x71

    if-eq v0, v1, :cond_2

    const/16 v1, 0x72

    if-eq v0, v1, :cond_2

    const/16 v1, 0x73

    if-eq v0, v1, :cond_2

    const/16 v1, 0x74

    if-eq v0, v1, :cond_2

    const/16 v1, 0x76

    if-eq v0, v1, :cond_2

    const/16 v1, 0x78

    if-eq v0, v1, :cond_2

    const/16 v1, 0x79

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x7f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x80

    if-eq v0, v1, :cond_2

    const/16 v1, 0x82

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x83

    if-eq v0, v1, :cond_2

    const/16 v1, 0x84

    if-eq v0, v1, :cond_2

    const/16 v1, 0x85

    if-eq v0, v1, :cond_2

    const/16 v1, 0x86

    if-eq v0, v1, :cond_2

    const/16 v1, 0x87

    if-eq v0, v1, :cond_2

    const/16 v1, 0x88

    if-eq v0, v1, :cond_2

    const/16 v1, 0x89

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8f

    if-eq v0, v1, :cond_2

    const/16 v1, 0x90

    if-eq v0, v1, :cond_2

    const/16 v1, 0x91

    if-eq v0, v1, :cond_2

    const/16 v1, 0x92

    if-eq v0, v1, :cond_2

    const/16 v1, 0x93

    if-eq v0, v1, :cond_2

    const/16 v1, 0x94

    if-eq v0, v1, :cond_2

    const/16 v1, 0x95

    if-eq v0, v1, :cond_2

    const/16 v1, 0x96

    if-eq v0, v1, :cond_2

    const/16 v1, 0x97

    if-eq v0, v1, :cond_2

    const/16 v1, 0x98

    if-eq v0, v1, :cond_2

    const/16 v1, 0x99

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9a

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9b

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9e

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9f

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa1

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa2

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa3

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa4

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa5

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa6

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa7

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xaa

    if-eq v0, v1, :cond_2

    const/16 v1, 0xab

    if-eq v0, v1, :cond_2

    const/16 v1, 0xac

    if-eq v0, v1, :cond_2

    const/16 v1, 0xad

    if-eq v0, v1, :cond_2

    const/16 v1, 0xae

    if-eq v0, v1, :cond_2

    const/16 v1, 0xaf

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb1

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb2

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb3

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb4

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb5

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb6

    if-eq v0, v1, :cond_2

    const/16 v1, 0xb7

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_2

    const/16 v1, 0xc9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xca

    if-eq v0, v1, :cond_2

    const/16 v1, 0xcb

    if-eq v0, v1, :cond_2

    const/16 v1, 0xcc

    if-eq v0, v1, :cond_2

    const/16 v1, 0xcd

    if-eq v0, v1, :cond_2

    const/16 v1, 0xce

    if-eq v0, v1, :cond_2

    const/16 v1, 0xcf

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd0

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd1

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd2

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd3

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd4

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd5

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd6

    if-eq v0, v1, :cond_2

    const/16 v1, 0xd7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12c

    if-eq v0, v1, :cond_2

    const/16 v1, 0x12d

    if-eq v0, v1, :cond_2

    const/16 v1, 0x75

    if-eq v0, v1, :cond_2

    const/16 v1, 0x190

    if-eq v0, v1, :cond_2

    const/16 v1, 0x191

    if-eq v0, v1, :cond_2

    const/16 v1, 0x192

    if-eq v0, v1, :cond_2

    const/16 v1, 0x193

    if-eq v0, v1, :cond_2

    const/16 v1, 0x194

    if-eq v0, v1, :cond_2

    const/16 v1, 0x195

    if-eq v0, v1, :cond_2

    const/16 v1, 0x196

    if-eq v0, v1, :cond_2

    const/16 v1, 0x199

    if-eq v0, v1, :cond_2

    const/16 v1, 0x197

    if-eq v0, v1, :cond_2

    const/16 v1, 0x198

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3e9

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3ea

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3eb

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3ec

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3ed

    if-eq v0, v1, :cond_2

    const/16 v1, 0x3ee

    if-ne v0, v1, :cond_3

    .line 3050
    :cond_2
    iput v0, p0, Lmtu;->b:I

    goto/16 :goto_0

    .line 3052
    :cond_3
    iput v3, p0, Lmtu;->b:I

    goto/16 :goto_0

    .line 3057
    :sswitch_2
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmtu;->c:Ljava/lang/String;

    goto/16 :goto_0

    .line 3061
    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v1

    .line 3062
    iget-object v0, p0, Lmtu;->d:[Ljava/lang/String;

    array-length v0, v0

    .line 3063
    add-int/2addr v1, v0

    new-array v1, v1, [Ljava/lang/String;

    .line 3064
    iget-object v2, p0, Lmtu;->d:[Ljava/lang/String;

    invoke-static {v2, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 3065
    iput-object v1, p0, Lmtu;->d:[Ljava/lang/String;

    .line 3066
    :goto_1
    iget-object v1, p0, Lmtu;->d:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    .line 3067
    iget-object v1, p0, Lmtu;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 3068
    invoke-virtual {p1}, Loxn;->a()I

    .line 3066
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3071
    :cond_4
    iget-object v1, p0, Lmtu;->d:[Ljava/lang/String;

    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    goto/16 :goto_0

    .line 3075
    :sswitch_4
    invoke-virtual {p1}, Loxn;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lmtu;->e:Ljava/lang/String;

    goto/16 :goto_0

    .line 2898
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 2845
    iget v0, p0, Lmtu;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 2846
    const/4 v0, 0x1

    iget v1, p0, Lmtu;->b:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 2848
    :cond_0
    iget-object v0, p0, Lmtu;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2849
    const/4 v0, 0x2

    iget-object v1, p0, Lmtu;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2851
    :cond_1
    iget-object v0, p0, Lmtu;->d:[Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2852
    iget-object v1, p0, Lmtu;->d:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 2853
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->a(ILjava/lang/String;)V

    .line 2852
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2856
    :cond_2
    iget-object v0, p0, Lmtu;->e:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 2857
    const/4 v0, 0x4

    iget-object v1, p0, Lmtu;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Loxo;->a(ILjava/lang/String;)V

    .line 2859
    :cond_3
    iget-object v0, p0, Lmtu;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 2861
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 2826
    invoke-virtual {p0, p1}, Lmtu;->a(Loxn;)Lmtu;

    move-result-object v0

    return-object v0
.end method
