.class public final Llyt;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lnfe;

.field public apiHeader:Llyr;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 27214
    invoke-direct {p0}, Loxq;-><init>()V

    .line 27217
    iput-object v0, p0, Llyt;->apiHeader:Llyr;

    .line 27220
    iput-object v0, p0, Llyt;->a:Lnfe;

    .line 27214
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 27237
    const/4 v0, 0x0

    .line 27238
    iget-object v1, p0, Llyt;->apiHeader:Llyr;

    if-eqz v1, :cond_0

    .line 27239
    const/4 v0, 0x1

    iget-object v1, p0, Llyt;->apiHeader:Llyr;

    .line 27240
    invoke-static {v0, v1}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 27242
    :cond_0
    iget-object v1, p0, Llyt;->a:Lnfe;

    if-eqz v1, :cond_1

    .line 27243
    const/4 v1, 0x2

    iget-object v2, p0, Llyt;->a:Lnfe;

    .line 27244
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27246
    :cond_1
    iget-object v1, p0, Llyt;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 27247
    iput v0, p0, Llyt;->ai:I

    .line 27248
    return v0
.end method

.method public a(Loxn;)Llyt;
    .locals 2

    .prologue
    .line 27256
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 27257
    sparse-switch v0, :sswitch_data_0

    .line 27261
    iget-object v1, p0, Llyt;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 27262
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Llyt;->ah:Ljava/util/List;

    .line 27265
    :cond_1
    iget-object v1, p0, Llyt;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27267
    :sswitch_0
    return-object p0

    .line 27272
    :sswitch_1
    iget-object v0, p0, Llyt;->apiHeader:Llyr;

    if-nez v0, :cond_2

    .line 27273
    new-instance v0, Llyr;

    invoke-direct {v0}, Llyr;-><init>()V

    iput-object v0, p0, Llyt;->apiHeader:Llyr;

    .line 27275
    :cond_2
    iget-object v0, p0, Llyt;->apiHeader:Llyr;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 27279
    :sswitch_2
    iget-object v0, p0, Llyt;->a:Lnfe;

    if-nez v0, :cond_3

    .line 27280
    new-instance v0, Lnfe;

    invoke-direct {v0}, Lnfe;-><init>()V

    iput-object v0, p0, Llyt;->a:Lnfe;

    .line 27282
    :cond_3
    iget-object v0, p0, Llyt;->a:Lnfe;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 27257
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 27225
    iget-object v0, p0, Llyt;->apiHeader:Llyr;

    if-eqz v0, :cond_0

    .line 27226
    const/4 v0, 0x1

    iget-object v1, p0, Llyt;->apiHeader:Llyr;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 27228
    :cond_0
    iget-object v0, p0, Llyt;->a:Lnfe;

    if-eqz v0, :cond_1

    .line 27229
    const/4 v0, 0x2

    iget-object v1, p0, Llyt;->a:Lnfe;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 27231
    :cond_1
    iget-object v0, p0, Llyt;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 27233
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 27210
    invoke-virtual {p0, p1}, Llyt;->a(Loxn;)Llyt;

    move-result-object v0

    return-object v0
.end method
