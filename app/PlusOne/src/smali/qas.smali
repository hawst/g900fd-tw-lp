.class public final Lqas;
.super Loxq;
.source "PG"


# instance fields
.field private a:I

.field private b:Lqau;

.field private c:Lqav;

.field private d:Lqaw;

.field private e:Lqat;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 895
    invoke-direct {p0}, Loxq;-><init>()V

    .line 1221
    const/high16 v0, -0x80000000

    iput v0, p0, Lqas;->a:I

    .line 1224
    iput-object v1, p0, Lqas;->b:Lqau;

    .line 1227
    iput-object v1, p0, Lqas;->c:Lqav;

    .line 1230
    iput-object v1, p0, Lqas;->d:Lqaw;

    .line 1233
    iput-object v1, p0, Lqas;->e:Lqat;

    .line 895
    return-void
.end method


# virtual methods
.method public a()I
    .locals 3

    .prologue
    .line 1259
    const/4 v0, 0x0

    .line 1260
    iget v1, p0, Lqas;->a:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    .line 1261
    const/4 v0, 0x1

    iget v1, p0, Lqas;->a:I

    .line 1262
    invoke-static {v0, v1}, Loxo;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1264
    :cond_0
    iget-object v1, p0, Lqas;->b:Lqau;

    if-eqz v1, :cond_1

    .line 1265
    const/4 v1, 0x2

    iget-object v2, p0, Lqas;->b:Lqau;

    .line 1266
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1268
    :cond_1
    iget-object v1, p0, Lqas;->c:Lqav;

    if-eqz v1, :cond_2

    .line 1269
    const/4 v1, 0x3

    iget-object v2, p0, Lqas;->c:Lqav;

    .line 1270
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1272
    :cond_2
    iget-object v1, p0, Lqas;->d:Lqaw;

    if-eqz v1, :cond_3

    .line 1273
    const/4 v1, 0x4

    iget-object v2, p0, Lqas;->d:Lqaw;

    .line 1274
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1276
    :cond_3
    iget-object v1, p0, Lqas;->e:Lqat;

    if-eqz v1, :cond_4

    .line 1277
    const/4 v1, 0x5

    iget-object v2, p0, Lqas;->e:Lqat;

    .line 1278
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1280
    :cond_4
    iget-object v1, p0, Lqas;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1281
    iput v0, p0, Lqas;->ai:I

    .line 1282
    return v0
.end method

.method public a(Loxn;)Lqas;
    .locals 2

    .prologue
    .line 1290
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1291
    sparse-switch v0, :sswitch_data_0

    .line 1295
    iget-object v1, p0, Lqas;->ah:Ljava/util/List;

    if-nez v1, :cond_1

    .line 1296
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lqas;->ah:Ljava/util/List;

    .line 1299
    :cond_1
    iget-object v1, p0, Lqas;->ah:Ljava/util/List;

    invoke-static {v1, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1301
    :sswitch_0
    return-object p0

    .line 1306
    :sswitch_1
    invoke-virtual {p1}, Loxn;->g()I

    move-result v0

    .line 1307
    if-eqz v0, :cond_2

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    const/4 v1, 0x3

    if-eq v0, v1, :cond_2

    const/4 v1, 0x4

    if-eq v0, v1, :cond_2

    const/4 v1, 0x5

    if-eq v0, v1, :cond_2

    const/4 v1, 0x6

    if-eq v0, v1, :cond_2

    const/4 v1, 0x7

    if-eq v0, v1, :cond_2

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    const/16 v1, 0x9

    if-eq v0, v1, :cond_2

    const/16 v1, 0xa

    if-ne v0, v1, :cond_3

    .line 1318
    :cond_2
    iput v0, p0, Lqas;->a:I

    goto :goto_0

    .line 1320
    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lqas;->a:I

    goto :goto_0

    .line 1325
    :sswitch_2
    iget-object v0, p0, Lqas;->b:Lqau;

    if-nez v0, :cond_4

    .line 1326
    new-instance v0, Lqau;

    invoke-direct {v0}, Lqau;-><init>()V

    iput-object v0, p0, Lqas;->b:Lqau;

    .line 1328
    :cond_4
    iget-object v0, p0, Lqas;->b:Lqau;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1332
    :sswitch_3
    iget-object v0, p0, Lqas;->c:Lqav;

    if-nez v0, :cond_5

    .line 1333
    new-instance v0, Lqav;

    invoke-direct {v0}, Lqav;-><init>()V

    iput-object v0, p0, Lqas;->c:Lqav;

    .line 1335
    :cond_5
    iget-object v0, p0, Lqas;->c:Lqav;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1339
    :sswitch_4
    iget-object v0, p0, Lqas;->d:Lqaw;

    if-nez v0, :cond_6

    .line 1340
    new-instance v0, Lqaw;

    invoke-direct {v0}, Lqaw;-><init>()V

    iput-object v0, p0, Lqas;->d:Lqaw;

    .line 1342
    :cond_6
    iget-object v0, p0, Lqas;->d:Lqaw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1346
    :sswitch_5
    iget-object v0, p0, Lqas;->e:Lqat;

    if-nez v0, :cond_7

    .line 1347
    new-instance v0, Lqat;

    invoke-direct {v0}, Lqat;-><init>()V

    iput-object v0, p0, Lqas;->e:Lqat;

    .line 1349
    :cond_7
    iget-object v0, p0, Lqas;->e:Lqat;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1291
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 2

    .prologue
    .line 1238
    iget v0, p0, Lqas;->a:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1239
    const/4 v0, 0x1

    iget v1, p0, Lqas;->a:I

    invoke-virtual {p1, v0, v1}, Loxo;->a(II)V

    .line 1241
    :cond_0
    iget-object v0, p0, Lqas;->b:Lqau;

    if-eqz v0, :cond_1

    .line 1242
    const/4 v0, 0x2

    iget-object v1, p0, Lqas;->b:Lqau;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1244
    :cond_1
    iget-object v0, p0, Lqas;->c:Lqav;

    if-eqz v0, :cond_2

    .line 1245
    const/4 v0, 0x3

    iget-object v1, p0, Lqas;->c:Lqav;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1247
    :cond_2
    iget-object v0, p0, Lqas;->d:Lqaw;

    if-eqz v0, :cond_3

    .line 1248
    const/4 v0, 0x4

    iget-object v1, p0, Lqas;->d:Lqaw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1250
    :cond_3
    iget-object v0, p0, Lqas;->e:Lqat;

    if-eqz v0, :cond_4

    .line 1251
    const/4 v0, 0x5

    iget-object v1, p0, Lqas;->e:Lqat;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 1253
    :cond_4
    iget-object v0, p0, Lqas;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 1255
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 891
    invoke-virtual {p0, p1}, Lqas;->a(Loxn;)Lqas;

    move-result-object v0

    return-object v0
.end method
