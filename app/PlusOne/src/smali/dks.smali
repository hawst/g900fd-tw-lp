.class public final Ldks;
.super Leuh;
.source "PG"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Leuh",
        "<",
        "Lmdo;",
        "Lmdp;",
        ">;"
    }
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private p:Lmdp;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Z)V
    .locals 6

    .prologue
    .line 34
    const-string v3, "getvideosdata"

    new-instance v4, Lmdo;

    invoke-direct {v4}, Lmdo;-><init>()V

    new-instance v5, Lmdp;

    invoke-direct {v5}, Lmdp;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    invoke-direct/range {v0 .. v5}, Leuh;-><init>(Landroid/content/Context;ILjava/lang/String;Loxu;Loxu;)V

    .line 36
    iput-object p3, p0, Ldks;->a:Ljava/lang/String;

    .line 37
    if-eqz p4, :cond_0

    const/16 v0, 0xc

    :goto_0
    iput v0, p0, Ldks;->b:I

    .line 40
    return-void

    .line 37
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lmdp;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 67
    if-eqz p0, :cond_0

    iget-object v1, p0, Lmdp;->a:Lnlr;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lmdp;->a:Lnlr;

    iget-object v1, v1, Lnlr;->a:Lnig;

    if-nez v1, :cond_1

    .line 71
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lmdp;->a:Lnlr;

    iget-object v1, v1, Lnlr;->a:Lnig;

    iget v1, v1, Lnig;->a:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method protected a(Lmdo;)V
    .locals 3

    .prologue
    .line 44
    new-instance v0, Lnlf;

    invoke-direct {v0}, Lnlf;-><init>()V

    iput-object v0, p1, Lmdo;->a:Lnlf;

    .line 45
    iget-object v0, p1, Lmdo;->a:Lnlf;

    .line 46
    new-instance v1, Lnky;

    invoke-direct {v1}, Lnky;-><init>()V

    iput-object v1, v0, Lnlf;->a:Lnky;

    .line 47
    iget-object v1, v0, Lnlf;->a:Lnky;

    new-instance v2, Lnkv;

    invoke-direct {v2}, Lnkv;-><init>()V

    iput-object v2, v1, Lnky;->a:Lnkv;

    .line 48
    iget-object v1, v0, Lnlf;->a:Lnky;

    iget-object v1, v1, Lnky;->a:Lnkv;

    iget-object v2, p0, Ldks;->a:Ljava/lang/String;

    iput-object v2, v1, Lnkv;->a:Ljava/lang/String;

    .line 49
    new-instance v1, Lnkt;

    invoke-direct {v1}, Lnkt;-><init>()V

    iput-object v1, v0, Lnlf;->b:Lnkt;

    .line 50
    iget-object v0, v0, Lnlf;->b:Lnkt;

    iget v1, p0, Ldks;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lnkt;->a:Ljava/lang/Integer;

    .line 51
    return-void
.end method

.method protected a(Lmdp;)V
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Ldks;->p:Lmdp;

    .line 56
    return-void
.end method

.method protected bridge synthetic a(Loxu;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lmdo;

    invoke-virtual {p0, p1}, Ldks;->a(Lmdo;)V

    return-void
.end method

.method public b()Lmdp;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Ldks;->p:Lmdp;

    return-object v0
.end method

.method protected synthetic b(Loxu;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lmdp;

    invoke-virtual {p0, p1}, Ldks;->a(Lmdp;)V

    return-void
.end method

.method public c()Z
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Ldks;->p:Lmdp;

    invoke-static {v0}, Ldks;->b(Lmdp;)Z

    move-result v0

    return v0
.end method
