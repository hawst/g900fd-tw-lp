.class public final Lmlm;
.super Loxq;
.source "PG"


# instance fields
.field public a:Lmlw;

.field public b:Lmok;

.field public c:[Lmmr;

.field public d:Lmlx;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 958
    invoke-direct {p0}, Loxq;-><init>()V

    .line 961
    iput-object v1, p0, Lmlm;->a:Lmlw;

    .line 964
    iput-object v1, p0, Lmlm;->b:Lmok;

    .line 967
    sget-object v0, Lmmr;->a:[Lmmr;

    iput-object v0, p0, Lmlm;->c:[Lmmr;

    .line 970
    iput-object v1, p0, Lmlm;->d:Lmlx;

    .line 958
    return-void
.end method


# virtual methods
.method public a()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 997
    .line 998
    iget-object v0, p0, Lmlm;->b:Lmok;

    if-eqz v0, :cond_4

    .line 999
    const/4 v0, 0x1

    iget-object v2, p0, Lmlm;->b:Lmok;

    .line 1000
    invoke-static {v0, v2}, Loxo;->c(ILoxu;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1002
    :goto_0
    iget-object v2, p0, Lmlm;->c:[Lmmr;

    if-eqz v2, :cond_1

    .line 1003
    iget-object v2, p0, Lmlm;->c:[Lmmr;

    array-length v3, v2

    :goto_1
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 1004
    if-eqz v4, :cond_0

    .line 1005
    const/4 v5, 0x3

    .line 1006
    invoke-static {v5, v4}, Loxo;->c(ILoxu;)I

    move-result v4

    add-int/2addr v0, v4

    .line 1003
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1010
    :cond_1
    iget-object v1, p0, Lmlm;->a:Lmlw;

    if-eqz v1, :cond_2

    .line 1011
    const/4 v1, 0x4

    iget-object v2, p0, Lmlm;->a:Lmlw;

    .line 1012
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1014
    :cond_2
    iget-object v1, p0, Lmlm;->d:Lmlx;

    if-eqz v1, :cond_3

    .line 1015
    const/4 v1, 0x5

    iget-object v2, p0, Lmlm;->d:Lmlx;

    .line 1016
    invoke-static {v1, v2}, Loxo;->c(ILoxu;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1018
    :cond_3
    iget-object v1, p0, Lmlm;->ah:Ljava/util/List;

    invoke-static {v1}, Loxx;->a(Ljava/util/List;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1019
    iput v0, p0, Lmlm;->ai:I

    .line 1020
    return v0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public a(Loxn;)Lmlm;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1028
    :cond_0
    :goto_0
    invoke-virtual {p1}, Loxn;->a()I

    move-result v0

    .line 1029
    sparse-switch v0, :sswitch_data_0

    .line 1033
    iget-object v2, p0, Lmlm;->ah:Ljava/util/List;

    if-nez v2, :cond_1

    .line 1034
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lmlm;->ah:Ljava/util/List;

    .line 1037
    :cond_1
    iget-object v2, p0, Lmlm;->ah:Ljava/util/List;

    invoke-static {v2, p1, v0}, Loxx;->a(Ljava/util/List;Loxn;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1039
    :sswitch_0
    return-object p0

    .line 1044
    :sswitch_1
    iget-object v0, p0, Lmlm;->b:Lmok;

    if-nez v0, :cond_2

    .line 1045
    new-instance v0, Lmok;

    invoke-direct {v0}, Lmok;-><init>()V

    iput-object v0, p0, Lmlm;->b:Lmok;

    .line 1047
    :cond_2
    iget-object v0, p0, Lmlm;->b:Lmok;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1051
    :sswitch_2
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Loxx;->b(Loxn;I)I

    move-result v2

    .line 1052
    iget-object v0, p0, Lmlm;->c:[Lmmr;

    if-nez v0, :cond_4

    move v0, v1

    .line 1053
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lmmr;

    .line 1054
    iget-object v3, p0, Lmlm;->c:[Lmmr;

    if-eqz v3, :cond_3

    .line 1055
    iget-object v3, p0, Lmlm;->c:[Lmmr;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1057
    :cond_3
    iput-object v2, p0, Lmlm;->c:[Lmmr;

    .line 1058
    :goto_2
    iget-object v2, p0, Lmlm;->c:[Lmmr;

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_5

    .line 1059
    iget-object v2, p0, Lmlm;->c:[Lmmr;

    new-instance v3, Lmmr;

    invoke-direct {v3}, Lmmr;-><init>()V

    aput-object v3, v2, v0

    .line 1060
    iget-object v2, p0, Lmlm;->c:[Lmmr;

    aget-object v2, v2, v0

    invoke-virtual {p1, v2}, Loxn;->a(Loxu;)V

    .line 1061
    invoke-virtual {p1}, Loxn;->a()I

    .line 1058
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1052
    :cond_4
    iget-object v0, p0, Lmlm;->c:[Lmmr;

    array-length v0, v0

    goto :goto_1

    .line 1064
    :cond_5
    iget-object v2, p0, Lmlm;->c:[Lmmr;

    new-instance v3, Lmmr;

    invoke-direct {v3}, Lmmr;-><init>()V

    aput-object v3, v2, v0

    .line 1065
    iget-object v2, p0, Lmlm;->c:[Lmmr;

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto :goto_0

    .line 1069
    :sswitch_3
    iget-object v0, p0, Lmlm;->a:Lmlw;

    if-nez v0, :cond_6

    .line 1070
    new-instance v0, Lmlw;

    invoke-direct {v0}, Lmlw;-><init>()V

    iput-object v0, p0, Lmlm;->a:Lmlw;

    .line 1072
    :cond_6
    iget-object v0, p0, Lmlm;->a:Lmlw;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1076
    :sswitch_4
    iget-object v0, p0, Lmlm;->d:Lmlx;

    if-nez v0, :cond_7

    .line 1077
    new-instance v0, Lmlx;

    invoke-direct {v0}, Lmlx;-><init>()V

    iput-object v0, p0, Lmlm;->d:Lmlx;

    .line 1079
    :cond_7
    iget-object v0, p0, Lmlm;->d:Lmlx;

    invoke-virtual {p1, v0}, Loxn;->a(Loxu;)V

    goto/16 :goto_0

    .line 1029
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public a(Loxo;)V
    .locals 5

    .prologue
    .line 975
    iget-object v0, p0, Lmlm;->b:Lmok;

    if-eqz v0, :cond_0

    .line 976
    const/4 v0, 0x1

    iget-object v1, p0, Lmlm;->b:Lmok;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 978
    :cond_0
    iget-object v0, p0, Lmlm;->c:[Lmmr;

    if-eqz v0, :cond_2

    .line 979
    iget-object v1, p0, Lmlm;->c:[Lmmr;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 980
    if-eqz v3, :cond_1

    .line 981
    const/4 v4, 0x3

    invoke-virtual {p1, v4, v3}, Loxo;->b(ILoxu;)V

    .line 979
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 985
    :cond_2
    iget-object v0, p0, Lmlm;->a:Lmlw;

    if-eqz v0, :cond_3

    .line 986
    const/4 v0, 0x4

    iget-object v1, p0, Lmlm;->a:Lmlw;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 988
    :cond_3
    iget-object v0, p0, Lmlm;->d:Lmlx;

    if-eqz v0, :cond_4

    .line 989
    const/4 v0, 0x5

    iget-object v1, p0, Lmlm;->d:Lmlx;

    invoke-virtual {p1, v0, v1}, Loxo;->b(ILoxu;)V

    .line 991
    :cond_4
    iget-object v0, p0, Lmlm;->ah:Ljava/util/List;

    invoke-static {v0, p1}, Loxx;->a(Ljava/util/List;Loxo;)V

    .line 993
    return-void
.end method

.method public synthetic b(Loxn;)Loxu;
    .locals 1

    .prologue
    .line 954
    invoke-virtual {p0, p1}, Lmlm;->a(Loxn;)Lmlm;

    move-result-object v0

    return-object v0
.end method
