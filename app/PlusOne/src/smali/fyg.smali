.class public Lfyg;
.super Ljava/lang/Object;
.source "PG"


# static fields
.field private static b:I

.field private static final c:[I

.field private static final d:[I


# instance fields
.field private A:Z

.field private final B:Landroid/view/View;

.field private final C:Lfyi;

.field private final D:Lfyh;

.field a:Ljava/lang/String;

.field private e:Landroid/graphics/drawable/Drawable;

.field private f:Landroid/graphics/drawable/Drawable;

.field private g:I

.field private h:I

.field private i:I

.field private j:Landroid/graphics/RectF;

.field private k:I

.field private final l:Landroid/widget/AbsListView;

.field private final m:Landroid/widget/TextView;

.field private n:Z

.field private o:I

.field private p:Landroid/graphics/Paint;

.field private q:I

.field private r:I

.field private s:Z

.field private t:[Ljava/lang/Object;

.field private u:Z

.field private v:Lfyj;

.field private w:I

.field private final x:Landroid/os/Handler;

.field private y:Landroid/widget/ListAdapter;

.field private z:Landroid/widget/SectionIndexer;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 68
    const/4 v0, 0x4

    sput v0, Lfyg;->b:I

    .line 78
    const/4 v0, 0x1

    new-array v0, v0, [I

    const v1, 0x10100a7

    aput v1, v0, v2

    sput-object v0, Lfyg;->c:[I

    .line 82
    new-array v0, v2, [I

    sput-object v0, Lfyg;->d:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/AbsListView;Landroid/widget/TextView;Landroid/view/View;Lfyi;Lfyh;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 100
    const/4 v0, -0x1

    iput v0, p0, Lfyg;->r:I

    .line 110
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lfyg;->x:Landroid/os/Handler;

    .line 124
    iput-object p2, p0, Lfyg;->l:Landroid/widget/AbsListView;

    .line 125
    iput-object p3, p0, Lfyg;->m:Landroid/widget/TextView;

    .line 126
    iput-object p4, p0, Lfyg;->B:Landroid/view/View;

    .line 127
    invoke-static {p5}, Llsk;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfyi;

    iput-object v0, p0, Lfyg;->C:Lfyi;

    .line 128
    invoke-static {p6}, Llsk;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lfyh;

    iput-object v0, p0, Lfyg;->D:Lfyh;

    .line 129
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [I

    fill-array-data v2, :array_0

    invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    const v2, 0x7f020102

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    iput-object v2, p0, Lfyg;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0380

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lfyg;->h:I

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0d0381

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    iput v2, p0, Lfyg;->g:I

    iput-boolean v4, p0, Lfyg;->A:Z

    const v2, 0x7f020105

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lfyg;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    iput-boolean v4, p0, Lfyg;->n:Z

    invoke-virtual {p0}, Lfyg;->e()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d037f

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lfyg;->k:I

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lfyg;->j:Landroid/graphics/RectF;

    new-instance v0, Lfyj;

    invoke-direct {v0, p0}, Lfyj;-><init>(Lfyg;)V

    iput-object v0, p0, Lfyg;->v:Lfyj;

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lfyg;->p:Landroid/graphics/Paint;

    iget-object v0, p0, Lfyg;->p:Landroid/graphics/Paint;

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lfyg;->p:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    iget-object v0, p0, Lfyg;->p:Landroid/graphics/Paint;

    iget v1, p0, Lfyg;->k:I

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    invoke-virtual {p1}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    new-array v1, v4, [I

    const v2, 0x1010036

    aput v2, v1, v5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/res/TypedArray;->getIndex(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    invoke-virtual {v1}, Landroid/content/res/ColorStateList;->getDefaultColor()I

    move-result v0

    iget-object v1, p0, Lfyg;->p:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lfyg;->p:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iput v5, p0, Lfyg;->w:I

    invoke-direct {p0}, Lfyg;->i()V

    .line 130
    return-void

    .line 129
    nop

    :array_0
    .array-data 4
        0x1010336
        0x1010339
    .end array-data
.end method

.method static synthetic a(Lfyg;)Landroid/view/View;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lfyg;->B:Landroid/view/View;

    return-object v0
.end method

.method private f()I
    .locals 3

    .prologue
    .line 181
    iget-object v0, p0, Lfyg;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    sget-object v1, Lfyh;->a:Lfyh;

    iget-object v2, p0, Lfyg;->D:Lfyh;

    invoke-virtual {v1, v2}, Lfyh;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v0, p0, Lfyg;->h:I

    :cond_0
    return v0
.end method

.method private g()I
    .locals 3

    .prologue
    .line 189
    iget-object v0, p0, Lfyg;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    sget-object v1, Lfyh;->a:Lfyh;

    iget-object v2, p0, Lfyg;->D:Lfyh;

    invoke-virtual {v1, v2}, Lfyh;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v1, p0, Lfyg;->h:I

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method private h()V
    .locals 5

    .prologue
    .line 198
    iget-object v0, p0, Lfyg;->e:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0}, Lfyg;->g()I

    move-result v1

    const/4 v2, 0x0

    invoke-direct {p0}, Lfyg;->f()I

    move-result v3

    iget v4, p0, Lfyg;->g:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 199
    iget-object v0, p0, Lfyg;->e:Landroid/graphics/drawable/Drawable;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 200
    return-void
.end method

.method private i()V
    .locals 2

    .prologue
    .line 245
    iget v0, p0, Lfyg;->w:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    sget-object v0, Lfyg;->c:[I

    .line 247
    :goto_0
    iget-object v1, p0, Lfyg;->e:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lfyg;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 248
    iget-object v1, p0, Lfyg;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 250
    :cond_0
    iget-object v1, p0, Lfyg;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lfyg;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->isStateful()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 251
    iget-object v1, p0, Lfyg;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 253
    :cond_1
    return-void

    .line 245
    :cond_2
    sget-object v0, Lfyg;->d:[I

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lfyg;->w:I

    return v0
.end method

.method public a(I)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x7d0

    const-wide/16 v4, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 133
    packed-switch p1, :pswitch_data_0

    .line 172
    :cond_0
    :goto_0
    iput p1, p0, Lfyg;->w:I

    .line 173
    invoke-direct {p0}, Lfyg;->i()V

    .line 174
    return-void

    .line 135
    :pswitch_0
    iget-object v0, p0, Lfyg;->x:Landroid/os/Handler;

    iget-object v1, p0, Lfyg;->v:Lfyj;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 136
    iget-object v0, p0, Lfyg;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    goto :goto_0

    .line 139
    :pswitch_1
    iget v0, p0, Lfyg;->w:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 140
    invoke-direct {p0}, Lfyg;->h()V

    .line 142
    :cond_1
    iget-object v0, p0, Lfyg;->x:Landroid/os/Handler;

    iget-object v1, p0, Lfyg;->v:Lfyj;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 143
    iget-object v0, p0, Lfyg;->m:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lfyg;->m:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 144
    iget-object v0, p0, Lfyg;->m:Landroid/widget/TextView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 145
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v3, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 146
    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 147
    invoke-virtual {v0, v4, v5}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 148
    iget-object v1, p0, Lfyg;->m:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 152
    :pswitch_2
    iget-object v0, p0, Lfyg;->x:Landroid/os/Handler;

    iget-object v1, p0, Lfyg;->v:Lfyj;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 154
    iget-object v0, p0, Lfyg;->m:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 155
    iget-object v0, p0, Lfyg;->m:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 158
    :cond_2
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 159
    invoke-virtual {v0, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 160
    invoke-virtual {v0, v4, v5}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 162
    iget-object v1, p0, Lfyg;->m:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 163
    iget-object v1, p0, Lfyg;->m:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 168
    :pswitch_3
    iget-object v0, p0, Lfyg;->B:Landroid/view/View;

    invoke-direct {p0}, Lfyg;->g()I

    move-result v1

    iget v2, p0, Lfyg;->i:I

    .line 169
    invoke-direct {p0}, Lfyg;->f()I

    move-result v3

    iget v4, p0, Lfyg;->i:I

    iget v5, p0, Lfyg;->g:I

    add-int/2addr v4, v5

    .line 168
    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->invalidate(IIII)V

    goto :goto_0

    .line 133
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 271
    iget v0, p0, Lfyg;->w:I

    if-nez v0, :cond_1

    .line 323
    :cond_0
    :goto_0
    return-void

    .line 276
    :cond_1
    iget v4, p0, Lfyg;->i:I

    .line 277
    iget-object v0, p0, Lfyg;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 278
    iget-object v3, p0, Lfyg;->v:Lfyj;

    .line 280
    const/4 v1, -0x1

    .line 281
    iget v5, p0, Lfyg;->w:I

    if-ne v5, v9, :cond_7

    .line 282
    invoke-virtual {v3}, Lfyj;->b()I

    move-result v3

    .line 283
    const/16 v1, 0x7f

    if-ge v3, v1, :cond_2

    .line 284
    iget-object v1, p0, Lfyg;->e:Landroid/graphics/drawable/Drawable;

    shl-int/lit8 v5, v3, 0x1

    invoke-virtual {v1, v5}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 287
    :cond_2
    iget-object v1, p0, Lfyg;->D:Lfyh;

    sget-object v5, Lfyh;->a:Lfyh;

    invoke-virtual {v1, v5}, Lfyh;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 289
    iget v0, p0, Lfyg;->h:I

    move v1, v2

    .line 294
    :goto_1
    iget-object v5, p0, Lfyg;->e:Landroid/graphics/drawable/Drawable;

    iget v6, p0, Lfyg;->g:I

    invoke-virtual {v5, v1, v2, v0, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 295
    const/4 v0, 0x1

    iput-boolean v0, p0, Lfyg;->A:Z

    move v0, v3

    .line 298
    :goto_2
    iget-object v1, p0, Lfyg;->f:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    .line 299
    iget-object v1, p0, Lfyg;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v1

    .line 300
    iget v3, v1, Landroid/graphics/Rect;->left:I

    .line 301
    iget v5, v1, Landroid/graphics/Rect;->bottom:I

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v1, v5, v1

    div-int/lit8 v1, v1, 0x2

    .line 302
    iget-object v5, p0, Lfyg;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v5}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v5

    .line 303
    iget v6, p0, Lfyg;->h:I

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v3, v6

    div-int/lit8 v6, v5, 0x2

    sub-int/2addr v3, v6

    .line 304
    iget-object v6, p0, Lfyg;->f:Landroid/graphics/drawable/Drawable;

    add-int/2addr v5, v3

    iget-object v7, p0, Lfyg;->l:Landroid/widget/AbsListView;

    .line 305
    invoke-virtual {v7}, Landroid/widget/AbsListView;->getHeight()I

    move-result v7

    sub-int/2addr v7, v1

    .line 304
    invoke-virtual {v6, v3, v1, v5, v7}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 306
    iget-object v1, p0, Lfyg;->f:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 309
    :cond_3
    int-to-float v1, v4

    invoke-virtual {p1, v8, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 310
    iget-object v1, p0, Lfyg;->e:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 311
    neg-int v1, v4

    int-to-float v1, v1

    invoke-virtual {p1, v8, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 314
    iget v1, p0, Lfyg;->w:I

    const/4 v3, 0x2

    if-ne v1, v3, :cond_5

    iget-boolean v1, p0, Lfyg;->u:Z

    if-eqz v1, :cond_5

    iget-object v1, p0, Lfyg;->m:Landroid/widget/TextView;

    if-nez v1, :cond_5

    .line 315
    iget-object v0, p0, Lfyg;->a:Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lfyg;->a(Landroid/graphics/Canvas;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 291
    :cond_4
    iget v1, p0, Lfyg;->h:I

    sub-int v1, v0, v1

    .line 292
    goto :goto_1

    .line 316
    :cond_5
    iget v1, p0, Lfyg;->w:I

    if-ne v1, v9, :cond_0

    .line 317
    if-nez v0, :cond_6

    .line 318
    invoke-virtual {p0, v2}, Lfyg;->a(I)V

    goto/16 :goto_0

    .line 320
    :cond_6
    iget-object v0, p0, Lfyg;->B:Landroid/view/View;

    invoke-direct {p0}, Lfyg;->g()I

    move-result v1

    invoke-direct {p0}, Lfyg;->f()I

    move-result v2

    iget v3, p0, Lfyg;->g:I

    add-int/2addr v3, v4

    invoke-virtual {v0, v1, v4, v2, v3}, Landroid/view/View;->invalidate(IIII)V

    goto/16 :goto_0

    :cond_7
    move v0, v1

    goto :goto_2
.end method

.method a(Landroid/graphics/Canvas;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 326
    iget-object v0, p0, Lfyg;->m:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 327
    iget-object v0, p0, Lfyg;->p:Landroid/graphics/Paint;

    .line 328
    invoke-virtual {v0}, Landroid/graphics/Paint;->descent()F

    move-result v1

    .line 329
    iget-object v2, p0, Lfyg;->j:Landroid/graphics/RectF;

    .line 330
    iget v3, v2, Landroid/graphics/RectF;->left:F

    iget v4, v2, Landroid/graphics/RectF;->right:F

    add-float/2addr v3, v4

    float-to-int v3, v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    iget v4, v2, Landroid/graphics/RectF;->bottom:F

    iget v2, v2, Landroid/graphics/RectF;->top:F

    add-float/2addr v2, v4

    float-to-int v2, v2

    div-int/lit8 v2, v2, 0x2

    iget v4, p0, Lfyg;->k:I

    div-int/lit8 v4, v4, 0x4

    add-int/2addr v2, v4

    int-to-float v2, v2

    sub-float v1, v2, v1

    invoke-virtual {p1, p2, v3, v1, v0}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 333
    :cond_0
    return-void
.end method

.method a(Landroid/widget/AbsListView;III)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 349
    iget v0, p0, Lfyg;->r:I

    if-eq v0, p4, :cond_0

    if-lez p3, :cond_0

    .line 350
    iput p4, p0, Lfyg;->r:I

    .line 351
    iget v0, p0, Lfyg;->r:I

    div-int/2addr v0, p3

    sget v3, Lfyg;->b:I

    if-lt v0, v3, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lfyg;->s:Z

    .line 353
    :cond_0
    iget-boolean v0, p0, Lfyg;->s:Z

    if-nez v0, :cond_3

    .line 354
    iget v0, p0, Lfyg;->w:I

    if-eqz v0, :cond_1

    .line 355
    invoke-virtual {p0, v2}, Lfyg;->a(I)V

    .line 382
    :cond_1
    :goto_1
    return-void

    :cond_2
    move v0, v2

    .line 351
    goto :goto_0

    .line 359
    :cond_3
    sub-int v0, p4, p3

    if-lez v0, :cond_5

    iget v0, p0, Lfyg;->w:I

    if-eq v0, v5, :cond_5

    .line 360
    iget v0, p0, Lfyg;->i:I

    .line 361
    iget-object v3, p0, Lfyg;->l:Landroid/widget/AbsListView;

    invoke-virtual {v3}, Landroid/widget/AbsListView;->getHeight()I

    move-result v3

    iget v4, p0, Lfyg;->g:I

    sub-int/2addr v3, v4

    mul-int/2addr v3, p2

    sub-int v4, p4, p3

    div-int/2addr v3, v4

    iput v3, p0, Lfyg;->i:I

    .line 363
    iget-boolean v3, p0, Lfyg;->A:Z

    if-eqz v3, :cond_4

    .line 364
    invoke-direct {p0}, Lfyg;->h()V

    .line 365
    iput-boolean v2, p0, Lfyg;->A:Z

    .line 367
    :cond_4
    iget v2, p0, Lfyg;->i:I

    if-eq v2, v0, :cond_5

    .line 368
    iget-object v0, p0, Lfyg;->B:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 371
    :cond_5
    iput-boolean v1, p0, Lfyg;->n:Z

    .line 372
    iget v0, p0, Lfyg;->o:I

    if-eq p2, v0, :cond_1

    .line 375
    iput p2, p0, Lfyg;->o:I

    .line 376
    iget v0, p0, Lfyg;->w:I

    if-eq v0, v5, :cond_1

    .line 377
    invoke-virtual {p0, v1}, Lfyg;->a(I)V

    .line 378
    iget-object v0, p0, Lfyg;->C:Lfyi;

    sget-object v1, Lfyi;->b:Lfyi;

    invoke-virtual {v0, v1}, Lfyi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 379
    iget-object v0, p0, Lfyg;->x:Landroid/os/Handler;

    iget-object v1, p0, Lfyg;->v:Lfyj;

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method

.method a(FF)Z
    .locals 2

    .prologue
    .line 597
    invoke-direct {p0}, Lfyg;->g()I

    move-result v0

    int-to-float v0, v0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    invoke-direct {p0}, Lfyg;->f()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, p1, v0

    if-gez v0, :cond_0

    iget v0, p0, Lfyg;->i:I

    int-to-float v0, v0

    cmpl-float v0, p2, v0

    if-ltz v0, :cond_0

    iget v0, p0, Lfyg;->i:I

    iget v1, p0, Lfyg;->g:I

    add-int/2addr v0, v1

    int-to-float v0, v0

    cmpg-float v0, p2, v0

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 536
    iget v0, p0, Lfyg;->w:I

    if-lez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    .line 537
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lfyg;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 538
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lfyg;->a(I)V

    .line 539
    const/4 v0, 0x1

    .line 542
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()V
    .locals 2

    .prologue
    .line 256
    iget-object v0, p0, Lfyg;->C:Lfyi;

    sget-object v1, Lfyi;->a:Lfyi;

    invoke-virtual {v0, v1}, Lfyi;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 257
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lfyg;->a(I)V

    .line 259
    :cond_0
    return-void
.end method

.method b(Landroid/view/MotionEvent;)Z
    .locals 14

    .prologue
    .line 546
    iget v0, p0, Lfyg;->w:I

    if-nez v0, :cond_0

    .line 547
    const/4 v0, 0x0

    .line 593
    :goto_0
    return v0

    .line 550
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 552
    if-nez v0, :cond_2

    .line 553
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-virtual {p0, v0, v1}, Lfyg;->a(FF)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 554
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lfyg;->a(I)V

    .line 555
    iget-object v0, p0, Lfyg;->y:Landroid/widget/ListAdapter;

    if-nez v0, :cond_1

    iget-object v0, p0, Lfyg;->l:Landroid/widget/AbsListView;

    if-eqz v0, :cond_1

    .line 556
    invoke-virtual {p0}, Lfyg;->e()V

    .line 559
    :cond_1
    const-wide/16 v0, 0x0

    const-wide/16 v2, 0x0

    const/4 v4, 0x3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    iget-object v1, p0, Lfyg;->l:Landroid/widget/AbsListView;

    invoke-virtual {v1, v0}, Landroid/widget/AbsListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 560
    const/4 v0, 0x1

    goto :goto_0

    .line 562
    :cond_2
    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    .line 563
    iget v0, p0, Lfyg;->w:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_17

    .line 564
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lfyg;->a(I)V

    .line 565
    iget-object v0, p0, Lfyg;->x:Landroid/os/Handler;

    .line 566
    iget-object v1, p0, Lfyg;->v:Lfyj;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 567
    iget-object v1, p0, Lfyg;->C:Lfyi;

    sget-object v2, Lfyi;->b:Lfyi;

    invoke-virtual {v1, v2}, Lfyi;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 568
    iget-object v1, p0, Lfyg;->v:Lfyj;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 570
    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    .line 572
    :cond_4
    const/4 v1, 0x2

    if-ne v0, v1, :cond_17

    .line 573
    iget v0, p0, Lfyg;->w:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_17

    .line 574
    iget-object v0, p0, Lfyg;->l:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getHeight()I

    move-result v1

    .line 576
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iget v2, p0, Lfyg;->g:I

    sub-int/2addr v0, v2

    add-int/lit8 v0, v0, 0xa

    .line 577
    if-gez v0, :cond_6

    .line 578
    const/4 v0, 0x0

    .line 582
    :cond_5
    :goto_1
    iget v2, p0, Lfyg;->i:I

    sub-int/2addr v2, v0

    invoke-static {v2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    const/4 v3, 0x2

    if-ge v2, v3, :cond_7

    .line 583
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 579
    :cond_6
    iget v2, p0, Lfyg;->g:I

    add-int/2addr v2, v0

    if-le v2, v1, :cond_5

    .line 580
    iget v0, p0, Lfyg;->g:I

    sub-int v0, v1, v0

    goto :goto_1

    .line 585
    :cond_7
    iput v0, p0, Lfyg;->i:I

    .line 587
    iget-boolean v0, p0, Lfyg;->n:Z

    if-eqz v0, :cond_13

    .line 588
    iget v0, p0, Lfyg;->i:I

    int-to-float v0, v0

    iget v2, p0, Lfyg;->g:I

    sub-int/2addr v1, v2

    int-to-float v1, v1

    div-float v8, v0, v1

    iget-object v0, p0, Lfyg;->l:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getCount()I

    move-result v7

    const/4 v0, 0x0

    iput-boolean v0, p0, Lfyg;->n:Z

    const/high16 v0, 0x3f800000    # 1.0f

    int-to-float v1, v7

    div-float/2addr v0, v1

    const/high16 v1, 0x41000000    # 8.0f

    div-float v9, v0, v1

    iget-object v10, p0, Lfyg;->t:[Ljava/lang/Object;

    if-eqz v10, :cond_e

    array-length v0, v10

    const/4 v1, 0x1

    if-le v0, v1, :cond_e

    array-length v11, v10

    int-to-float v0, v11

    mul-float/2addr v0, v8

    float-to-int v0, v0

    if-lt v0, v11, :cond_8

    add-int/lit8 v0, v11, -0x1

    :cond_8
    iget-object v1, p0, Lfyg;->z:Landroid/widget/SectionIndexer;

    invoke-interface {v1, v0}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v1

    add-int/lit8 v5, v0, 0x1

    add-int/lit8 v2, v11, -0x1

    if-ge v0, v2, :cond_1b

    iget-object v2, p0, Lfyg;->z:Landroid/widget/SectionIndexer;

    invoke-interface {v2, v5}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v2

    move v6, v2

    :goto_2
    if-ne v6, v1, :cond_1a

    move v2, v1

    move v3, v0

    :goto_3
    if-lez v3, :cond_19

    add-int/lit8 v2, v3, -0x1

    iget-object v3, p0, Lfyg;->z:Landroid/widget/SectionIndexer;

    invoke-interface {v3, v2}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v3

    if-eq v3, v1, :cond_a

    move v1, v2

    move v13, v3

    move v3, v2

    move v2, v13

    :goto_4
    if-nez v3, :cond_9

    iget-object v3, p0, Lfyg;->z:Landroid/widget/SectionIndexer;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Landroid/widget/SectionIndexer;->getSectionForPosition(I)I

    move-result v3

    :cond_9
    :goto_5
    add-int/lit8 v4, v5, 0x1

    :goto_6
    if-ge v4, v11, :cond_b

    iget-object v12, p0, Lfyg;->z:Landroid/widget/SectionIndexer;

    invoke-interface {v12, v4}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v12

    if-ne v12, v6, :cond_b

    add-int/lit8 v4, v4, 0x1

    add-int/lit8 v5, v5, 0x1

    goto :goto_6

    :cond_a
    if-nez v2, :cond_18

    const/4 v2, 0x0

    move v1, v0

    move v13, v3

    move v3, v2

    move v2, v13

    goto :goto_4

    :cond_b
    int-to-float v4, v1

    int-to-float v12, v11

    div-float/2addr v4, v12

    int-to-float v5, v5

    int-to-float v11, v11

    div-float/2addr v5, v11

    if-ne v1, v0, :cond_d

    sub-float v0, v8, v4

    cmpg-float v0, v0, v9

    if-gez v0, :cond_d

    move v0, v2

    :goto_7
    add-int/lit8 v1, v7, -0x1

    if-le v0, v1, :cond_c

    add-int/lit8 v0, v7, -0x1

    :cond_c
    move v1, v0

    :goto_8
    iget-object v0, p0, Lfyg;->l:Landroid/widget/AbsListView;

    instance-of v0, v0, Landroid/widget/ExpandableListView;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lfyg;->l:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ExpandableListView;

    iget v2, p0, Lfyg;->q:I

    add-int/2addr v1, v2

    invoke-static {v1}, Landroid/widget/ExpandableListView;->getPackedPositionForGroup(I)J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Landroid/widget/ExpandableListView;->getFlatListPosition(J)I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ExpandableListView;->setSelectionFromTop(II)V

    :goto_9
    if-ltz v3, :cond_16

    if-nez v10, :cond_11

    new-instance v0, Ljava/lang/AssertionError;

    new-instance v1, Ljava/lang/StringBuilder;

    const/16 v2, 0x46

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(I)V

    const-string v2, "sectionIndex="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for null sections. This should be impossible."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_d
    sub-int v0, v6, v2

    int-to-float v0, v0

    sub-float v1, v8, v4

    mul-float/2addr v0, v1

    sub-float v1, v5, v4

    div-float/2addr v0, v1

    float-to-int v0, v0

    add-int/2addr v0, v2

    goto :goto_7

    :cond_e
    int-to-float v0, v7

    mul-float/2addr v0, v8

    float-to-int v0, v0

    const/4 v3, -0x1

    move v1, v0

    goto :goto_8

    :cond_f
    iget-object v0, p0, Lfyg;->l:Landroid/widget/AbsListView;

    instance-of v0, v0, Landroid/widget/ListView;

    if-eqz v0, :cond_10

    iget-object v0, p0, Lfyg;->l:Landroid/widget/AbsListView;

    check-cast v0, Landroid/widget/ListView;

    iget v2, p0, Lfyg;->q:I

    add-int/2addr v1, v2

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_9

    :cond_10
    iget-object v0, p0, Lfyg;->l:Landroid/widget/AbsListView;

    iget v2, p0, Lfyg;->q:I

    add-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setSelection(I)V

    goto :goto_9

    :cond_11
    aget-object v0, v10, v3

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lfyg;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_12

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x20

    if-eq v0, v1, :cond_14

    :cond_12
    array-length v0, v10

    if-ge v3, v0, :cond_14

    const/4 v0, 0x1

    :goto_a
    iput-boolean v0, p0, Lfyg;->u:Z

    iget-object v0, p0, Lfyg;->m:Landroid/widget/TextView;

    if-eqz v0, :cond_13

    iget-boolean v0, p0, Lfyg;->u:Z

    if-eqz v0, :cond_15

    iget-object v0, p0, Lfyg;->m:Landroid/widget/TextView;

    iget-object v1, p0, Lfyg;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 590
    :cond_13
    :goto_b
    const/4 v0, 0x1

    goto/16 :goto_0

    .line 588
    :cond_14
    const/4 v0, 0x0

    goto :goto_a

    :cond_15
    iget-object v0, p0, Lfyg;->m:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_b

    :cond_16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lfyg;->u:Z

    goto :goto_b

    .line 593
    :cond_17
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_18
    move v13, v3

    move v3, v2

    move v2, v13

    goto/16 :goto_3

    :cond_19
    move v1, v0

    move v3, v0

    goto/16 :goto_4

    :cond_1a
    move v2, v1

    move v3, v0

    move v1, v0

    goto/16 :goto_5

    :cond_1b
    move v6, v7

    goto/16 :goto_2
.end method

.method c()V
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lfyg;->a(I)V

    .line 263
    return-void
.end method

.method d()Z
    .locals 1

    .prologue
    .line 266
    iget v0, p0, Lfyg;->w:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method e()V
    .locals 3

    .prologue
    .line 396
    iget-object v0, p0, Lfyg;->l:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ListAdapter;

    .line 397
    const/4 v1, 0x0

    iput-object v1, p0, Lfyg;->z:Landroid/widget/SectionIndexer;

    .line 398
    instance-of v1, v0, Landroid/widget/HeaderViewListAdapter;

    if-eqz v1, :cond_0

    move-object v1, v0

    .line 399
    check-cast v1, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v1}, Landroid/widget/HeaderViewListAdapter;->getHeadersCount()I

    move-result v1

    iput v1, p0, Lfyg;->q:I

    .line 400
    check-cast v0, Landroid/widget/HeaderViewListAdapter;

    invoke-virtual {v0}, Landroid/widget/HeaderViewListAdapter;->getWrappedAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    .line 403
    :cond_0
    iput-object v0, p0, Lfyg;->y:Landroid/widget/ListAdapter;

    .line 405
    instance-of v1, v0, Landroid/widget/SectionIndexer;

    if-eqz v1, :cond_1

    .line 406
    check-cast v0, Landroid/widget/SectionIndexer;

    iput-object v0, p0, Lfyg;->z:Landroid/widget/SectionIndexer;

    .line 407
    iget-object v0, p0, Lfyg;->z:Landroid/widget/SectionIndexer;

    invoke-interface {v0}, Landroid/widget/SectionIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lfyg;->t:[Ljava/lang/Object;

    .line 411
    :goto_0
    return-void

    .line 409
    :cond_1
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, " "

    aput-object v2, v0, v1

    iput-object v0, p0, Lfyg;->t:[Ljava/lang/Object;

    goto :goto_0
.end method
